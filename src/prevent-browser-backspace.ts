// Prevents back browser navigation when backspace is clicked and the focus is within a readonly input. FCA-2958 (fix for IE)
document.addEventListener('keydown', (event: KeyboardEvent) => {
  if (event && event.target) {
    const isBackspacePressed = event && event.key.toLowerCase() === 'backspace';
    const isReadOnlyInput = event.target && event.target instanceof HTMLInputElement ? event.target.readOnly : false;
    if (isBackspacePressed && isReadOnlyInput) {
      event.preventDefault();
    }
  }
});
