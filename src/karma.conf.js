// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-junit-reporter'),
      require('karma-mocha-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-sonarqube-unit-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage'),
      reports: ['html', 'lcovonly', 'cobertura'],
      fixWebpackSourcePaths: true
    },
    browserDisconnectTolerance: 6,
    captureTimeout: 180000,
    browserDisconnectTimeout: 10000,
    browserNoActivityTimeout: 180000,
    junitReporter: {
      outputFile: '../reports/test-results.xml',
      useBrowserName: false
    },

    mochaReporter: {
      output: 'minimal'
    },

    sonarQubeUnitReporter: {
      sonarQubeVersion: 'LATEST',
      outputFile: '../reports/sonarqube_test_report.xml',
      overrideTestDescription: true,
      useBrowserName: false,
      testFilePattern: '.spec.ts',
      testPaths: ['./']
    },

    reporters: ['mocha', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    reportSlowerThan: 1000,
    retryLimit: 5
  });
};
