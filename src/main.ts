/* eslint-disable no-console */

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { hmrBootstrap } from './hmr';
import './prevent-browser-backspace';
import './prevent-browser-drag-drop';

if (environment.production) {
  enableProdMode();
}

const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule, { preserveWhitespaces: true });

if (environment.hmr) {
  // Bootstraps the application in HMR mode
  // See the official Hot Module Replacement guide for more info
  // https://github.com/angular/angular-cli/wiki/stories-configure-hmr
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    console.error('HMR is not enabled for webpack-dev-server!');
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  bootstrap().catch(err => console.error(err));
}
