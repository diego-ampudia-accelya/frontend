export const environment = {
  environment: 'dev',
  production: false,
  hmr: false,
  // Randomly fail some of the requests. Useful for testing error handling behavior
  // NOTE: Use For development purposes only!
  enableChaosMode: false,
  chaosModeRate: 0.8
};
