export const environment = {
  environment: 'local',
  production: false,
  hmr: true,
  // Randomly fail some of the requests. Useful for testing error handling behavior
  // NOTE: Use For development purposes only!
  enableChaosMode: false,
  chaosModeRate: 0.8
};
