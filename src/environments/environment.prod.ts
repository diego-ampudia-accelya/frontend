export const environment = {
  environment: 'prod',
  production: true,
  hmr: false,
  // Randomly fail some of the requests. Useful for testing error handling behavior
  // NOTE: Use For development purposes only!
  enableChaosMode: false,
  chaosModeRate: 0.8
};
