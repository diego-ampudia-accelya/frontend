import * as api from 'primeng/api';

declare module 'primeng/api' {
  interface Message {
    severity?: string | 'info' | 'success' | 'warn' | 'error';
    summary?: string;
    detail?: string;
    id?: any;
    key?: string;
    life?: number;
    sticky?: boolean;
    closable?: boolean;
    data?: any;
  }
}
