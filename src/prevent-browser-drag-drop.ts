import { fromEvent } from 'rxjs';

// The default browser behavior when dropping a file is to open it in the same tab.
// It is better to disable it to avoid confusing the user.

const preventDefault = (event: DragEvent) => event.preventDefault();

fromEvent(window, 'dragover').subscribe(preventDefault);
fromEvent(window, 'drop').subscribe(preventDefault);
