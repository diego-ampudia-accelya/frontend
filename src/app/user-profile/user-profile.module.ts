import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';

import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserPropertiesCountriesComponent } from './components/user-properties-countries/user-properties-countries.component';
import { UserPropertiesComponent } from './components/user-properties/user-properties.component';
import { UserProfileService } from './services/user-profile.service';
import * as fromUserProfile from './store/reducers/user-profile.reducer';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [UserProfileComponent, UserPropertiesComponent, UserPropertiesCountriesComponent],
  imports: [
    UserProfileRoutingModule,
    SharedModule,
    CommonModule,
    NgbNavModule,
    StoreModule.forFeature(fromUserProfile.userProfileKey, fromUserProfile.reducer)
  ],
  providers: [ChangesDialogService, UserProfileService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserProfileModule {}
