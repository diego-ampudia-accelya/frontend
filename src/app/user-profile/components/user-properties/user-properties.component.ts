import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

import * as UserProfileActions from '../../store/actions/user-profile.actions';
import { AppState } from '~app/reducers';
import { FormUtil } from '~app/shared/helpers';
import { UserInfoEditable } from '~app/shared/models/user.model';
import { UserProfile, UserProfileStatus } from '~app/user-profile/models/user-profile.model';
import { getUserProfileValue } from '~app/user-profile/store/reducers/user-profile.reducer';

@Component({
  selector: 'bspl-user-properties',
  templateUrl: './user-properties.component.html',
  styleUrls: ['./user-properties.component.scss']
})
export class UserPropertiesComponent implements OnChanges, OnDestroy {
  @Input() user: UserProfile;
  @Input() isAirlineMulticountry: boolean;

  public form: FormGroup;

  public userIsActive: boolean;

  private formUtil: FormUtil;

  private destroy$ = new Subject();

  constructor(private fb: FormBuilder, private store: Store<AppState>) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnChanges(changes): void {
    if (changes.user?.currentValue) {
      this.buildForm();
      this.initializeStoreData();
      this.initializeListeners();

      this.userIsActive = this.user.status === UserProfileStatus.active;
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private buildForm() {
    this.form = this.formUtil.createGroup<UserInfoEditable>({
      name: [this.user.name || ''],
      organization: [this.user.contactInfo.organization || ''],
      telephone: [this.user.contactInfo.telephone || ''],
      country: [this.user.contactInfo.country || ''],
      city: [this.user.contactInfo.city || ''],
      address: [this.user.contactInfo.address || ''],
      postCode: [this.user.contactInfo.postCode || '']
    });
  }

  private initializeStoreData() {
    this.store.dispatch(UserProfileActions.load({ value: this.form.value }));
  }

  private initializeListeners() {
    this.form.statusChanges
      .pipe(distinctUntilChanged(), takeUntil(this.destroy$))
      .subscribe(() => this.store.dispatch(UserProfileActions.updateValidity({ isValid: this.form.valid })));

    this.form.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => this.store.dispatch(UserProfileActions.updateValue({ value })));

    this.store
      .select(getUserProfileValue)
      .pipe(takeUntil(this.destroy$))
      .subscribe(valueFromStore => this.form.patchValue(valueFromStore, { emitEvent: false }));
  }
}
