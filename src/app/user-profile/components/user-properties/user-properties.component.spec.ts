import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslatePipeMock } from '../../../test/translate.mock.pipe';
import { UserPropertiesComponent } from './user-properties.component';
import { UserType } from '~app/shared/models/user.model';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';
import { UserProfile, UserProfileStatus, UserProfileTemplate } from '~app/user-profile/models/user-profile.model';

// TODO
xdescribe('UserPropertiesComponent', () => {
  let component: UserPropertiesComponent;
  let fixture: ComponentFixture<UserPropertiesComponent>;

  const mockUser: UserProfile = {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    contactInfo: {
      address: 'B?Y?KDERE C.100-102',
      city: 'Estambul',
      country: 'TURKEY',
      email: 'martin.sautter@hallo.com;angelines.garcia@hallo.com;arancha.landa@hallo.com;pablo.zanuy@hallo.com;',
      organization: 'LUFTHANSA',
      postCode: '234',
      telephone: '0/212/2880768'
    },
    expiryDate: null,
    id: '69835985',
    level: 0,
    name: 'Usuario5985',
    parent: null,
    portalEmail: 'tihomir.arahangelov@accelya.com',
    registerDate: '2002-05-22',
    status: UserProfileStatus.active,
    template: UserProfileTemplate.streamlined,
    userType: UserType.AIRLINE,
    userTypeDetails: {
      iataCode: '220',
      name: 'AIRLINE NAME 220'
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [UserPropertiesComponent, TranslatePipeMock, EmptyPipe],
      providers: [FormBuilder],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPropertiesComponent);
    component = fixture.componentInstance;
    component.user = mockUser;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
