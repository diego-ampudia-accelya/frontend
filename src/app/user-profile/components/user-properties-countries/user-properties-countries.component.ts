import { Component, Inject, Input, OnChanges } from '@angular/core';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { intersectionBy } from 'lodash';

import { withHeadersBasedOnFields } from '~app/document/helpers/section-config.helper';
import { SimpleTableColumn } from '~app/shared/components';
import { BspDto } from '~app/shared/models/bsp.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { UserProfile } from '~app/user-profile/models/user-profile.model';

@Component({
  selector: 'bspl-user-properties-countries',
  templateUrl: 'user-properties-countries.component.html',
  styleUrls: ['user-properties-countries.component.scss']
})
export class UserPropertiesCountriesComponent implements OnChanges {
  @Input() user: UserProfile;
  @Input() loggedUserId: number;

  public columns: SimpleTableColumn<BspDto>[];
  public bspData: BspDto[] = [];

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    protected translationService: L10nTranslationService,
    private bspsDictionaryService: BspsDictionaryService
  ) {}

  public ngOnChanges(changes) {
    if (changes.user?.currentValue) {
      this.bspsDictionaryService
        .getAllBspsFromUser(this.loggedUserId)
        .subscribe(data => (this.bspData = intersectionBy(data, this.user.countries, 'isoCountryCode')));

      this.columns = withHeadersBasedOnFields(
        [{ field: 'isoCountryCode' }, { field: 'name' }],
        'ADM_ACM.basicInfo.countries.headers'
      );
    }
  }
}
