import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import * as authSelectors from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { ButtonDesign, FooterButton } from '~app/shared/components';
import { Permissions } from '~app/shared/constants';
import { Bsp } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { ChangesDialogService } from '~app/user-profile/changes-dialog/changes-dialog.service';
import { UserProfile } from '~app/user-profile/models/user-profile.model';
import { UserProfileService } from '~app/user-profile/services/user-profile.service';
import * as fromUserProfile from '~app/user-profile/store/reducers/user-profile.reducer';

@Component({
  selector: 'bspl-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, CanComponentDeactivate {
  public userPortalEmail$: Observable<string> = this.store.pipe(select(authSelectors.getUserEmail));
  public userBsp$: Observable<Bsp[]> = this.store.pipe(select(authSelectors.getUserBsps));
  public user$: Observable<UserProfile> = this.userProfileService.get();
  public loggedUser$: Observable<User> = this.store.pipe(select(authSelectors.getUser), first());

  public isApplyChangesEnabled$ = this.store.select(fromUserProfile.getIsApplyChangesEnabled);
  public isDiscardChangesEnabled$ = this.store.select(fromUserProfile.getIsChanged);

  public buttonDesign = ButtonDesign;
  public airlineIsMulticountry = false;

  constructor(
    private store: Store<AppState>,
    private changesDialogService: ChangesDialogService,
    private userProfileService: UserProfileService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.loggedUser$.subscribe(user => {
      this.airlineIsMulticountry =
        user.userType === UserType.AIRLINE && this.permissionsService.hasPermission(Permissions.readBsps);
    });
  }

  public discardChanges() {
    this.changesDialogService.confirmUnsavedChanges().subscribe();
  }

  public applyChanges() {
    this.changesDialogService.confirmApplyChanges().subscribe();
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }
}
