import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { UserProfile } from '../models/user-profile.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { MainUserInfo, UserInfoEditable, UserType } from '~app/shared/models/user.model';
import { ApiService, AppConfigurationService } from '~app/shared/services';

@Injectable()
export class UserProfileService extends ApiService<UserInfoEditable> {
  private airlineMulticountryPath = 'mc-user-profile';
  private defaultUserPath = 'user-profile';

  private airlineIsMulticountry = false;

  private loggedUser$ = this.store.select(getUser).pipe(first());

  constructor(
    public http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>,
    private permissionsService: PermissionsService
  ) {
    super(http, 'user');

    this.loggedUser$.subscribe(user => {
      this.airlineIsMulticountry =
        user.userType === UserType.AIRLINE && this.permissionsService.hasPermission(Permissions.readBsps);

      const finalEndpoint = this.airlineIsMulticountry ? this.airlineMulticountryPath : this.defaultUserPath;
      this.baseUrl = `${this.appConfiguration.baseApiPath}/user-management/${finalEndpoint}`;
    });
  }

  public get(): Observable<UserProfile> {
    return this.http.get<UserProfile>(this.baseUrl);
  }

  public save(userInfo: UserInfoEditable): Observable<UserProfile> {
    return this.http.put<UserProfile>(this.baseUrl, userInfo);
  }

  public getMainUsersByUserType(userType: number): Observable<MainUserInfo[]> {
    const url = `${this.appConfiguration.baseApiPath}/user-management/main-users/dictionary`;
    const params = { userType: userType.toString() };

    return this.http.get<MainUserInfo[]>(url, { params });
  }
}
