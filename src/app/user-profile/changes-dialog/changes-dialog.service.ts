import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { UserProfileService } from '../services/user-profile.service';
import * as UserProfileActions from '../store/actions/user-profile.actions';
import * as fromUserProfile from '../store/reducers/user-profile.reducer';
import { translations } from './translations';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ChangeModel,
  ChangesDialogComponent,
  ConfirmationDialogComponent,
  DialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import { UserInfoEditable } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private dataService: UserProfileService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirmApply(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirmDiscard(translations.unsavedChanges);
  }

  public confirmApply(question: { title: string; details: string }): Observable<FooterButton> {
    return this.store.pipe(
      select(fromUserProfile.getChanges),
      first(),
      switchMap(modifications =>
        iif(
          () => modifications.size > 0,
          defer(() => this.openApply(modifications, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  public confirmDiscard(question: { title: string; details: string }): Observable<FooterButton> {
    return this.store.pipe(
      select(fromUserProfile.getChanges),
      first(),
      switchMap(modifications =>
        iif(
          () => modifications.size > 0,
          defer(() => this.openDiscard(question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private openApply(
    modifications: Map<keyof UserInfoEditable, any>,
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Apply }]
      },
      message: question.details,
      changes: this.formatChanges(modifications),
      usePillsForChanges: true
    };

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      withLatestFrom(this.store.pipe(select(fromUserProfile.getUserProfileValue))),
      switchMap(([action, value]) => {
        let dialogResult$ = of(action);
        if (action === FooterButton.Apply) {
          dialogResult$ = this.apply(value, dialogConfig);
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private openDiscard(question: { title: string; details: string }): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Discard, buttonDesign: ButtonDesign.Primary }],
        reason: question.details
      }
    };

    return this.dialogService.open(ConfirmationDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(action => {
        const dialogResult$ = of(action);

        if (action === FooterButton.Discard) {
          this.store.dispatch(UserProfileActions.discardChanges());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(value: UserInfoEditable, dialogConfig: DialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(value).pipe(
        tap(() => this.store.dispatch(UserProfileActions.applyChangesSuccess({ value }))),
        tap(() => this.notificationService.showSuccess('headerUserInfo.changesDialog.applyChanges.success')),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }

  private formatChanges(modifications: Map<keyof UserInfoEditable, any>): ChangeModel[] {
    return Array.from(modifications.keys()).map(key => ({
      group: `headerUserInfo.changesDialog.sections.${this.getItemSection(key)}`,
      name: this.translationService.translate(`headerUserInfo.changesDialog.items.${key}`, {
        value: modifications.get(key)
      }),
      value: null
    }));
  }

  private getItemSection(item: keyof UserInfoEditable): string {
    const sections: { [section: string]: Array<keyof UserInfoEditable> } = {
      contactDetails: ['name', 'organization', 'telephone'],
      addressDetails: ['country', 'city', 'address', 'postCode']
    };

    return Object.keys(sections).find(section => sections[section].includes(item));
  }
}
