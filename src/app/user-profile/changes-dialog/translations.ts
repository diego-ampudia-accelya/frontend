export const translations = {
  applyChanges: {
    title: 'headerUserInfo.changesDialog.applyChanges.title',
    details: 'headerUserInfo.changesDialog.applyChanges.message'
  },
  unsavedChanges: {
    title: 'headerUserInfo.changesDialog.unsavedChanges.title',
    details: 'headerUserInfo.changesDialog.unsavedChanges.message'
  }
};
