import { BspDto } from '~app/shared/models/bsp.model';
import { UserType } from '~app/shared/models/user.model';

export interface UserProfile {
  id: string;
  name: string;
  bsp: BspDto;
  contactInfo: UserProfileContactInfo;
  expiryDate: string;
  level: any;
  parent: { id: number; name: string };
  portalEmail: string;
  registerDate: string;
  status: UserProfileStatus;
  template: UserProfileTemplate;
  userType: UserType;
  userTypeDetails: { iataCode: string; name: string };
  countries?: BspDto[];
}

export interface UserProfileContactInfo {
  address: string;
  city: string;
  country: string;
  email: string;
  organization: string;
  postCode: string;
  telephone: string;
}

export enum UserProfileTemplate {
  streamlined = 'STREAMLINED',
  efficient = 'EFFICIENT'
}

export enum UserProfileStatus {
  active = 'ACTIVE',
  inactive = 'INACTIVE'
}
