import { createAction, props } from '@ngrx/store';

import { UserInfoEditable } from '~app/shared/models/user.model';

export const load = createAction('[User Profile] Load', props<{ value: UserInfoEditable }>());
export const updateValue = createAction('[User Profile] Update Value', props<{ value: UserInfoEditable }>());
export const updateValidity = createAction(
  '[User Profile] Update Form Validity',
  props<{
    isValid: boolean;
  }>()
);

export const saveChanges = createAction('[User Profile] Save changes');
export const applyChangesSuccess = createAction(
  '[User Profile] Changes applied successfully',
  props<{
    value: UserInfoEditable;
  }>()
);
export const discardChanges = createAction('[User Profile] Discard changes');
