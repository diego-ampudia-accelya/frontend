import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { isEqual } from 'lodash';

import * as UserProfileActions from '../actions/user-profile.actions';
import { AppState } from '~app/reducers';
import { UserInfoEditable } from '~app/shared/models/user.model';

export const userProfileKey = 'userProfile';

export interface State extends AppState {
  userProfile: UserProfileState;
}

export interface UserProfileState {
  initialValue: UserInfoEditable;
  value: UserInfoEditable;
  isChanged: boolean;
  isInvalid: boolean;
}

export const initialState: UserProfileState = {
  initialValue: null,
  value: null,
  isChanged: false,
  isInvalid: false
};

const getChangesFromState = ({ initialValue, value }: Pick<UserProfileState, 'initialValue' | 'value'>) => {
  const changesMap = new Map<keyof UserInfoEditable, any>();
  Object.keys(value).forEach(key => {
    const valueHasChanged = !isEqual(value && value[key], initialValue && initialValue[key]);

    if (valueHasChanged) {
      changesMap.set(key as keyof UserInfoEditable, value[key]);
    }
  });

  return changesMap;
};

export const reducer = createReducer(
  initialState,
  on(UserProfileActions.load, (state, { value }) => ({
    ...state,
    initialValue: value,
    value
  })),
  on(UserProfileActions.updateValue, (state, { value }) => ({
    ...state,
    value,
    isChanged: !areUserProfileValuesEqual(state.initialValue, value)
  })),
  on(UserProfileActions.updateValidity, (state, { isValid }) => ({
    ...state,
    isInvalid: !isValid
  })),
  on(UserProfileActions.discardChanges, state => ({
    ...state,
    value: state.initialValue,
    isInvalid: false,
    isChanged: false
  })),
  on(UserProfileActions.applyChangesSuccess, (state, { value }) => ({
    ...state,
    initialValue: value,
    value,
    isChanged: false,
    isInvalid: false
  }))
);

export const getUserProfile = createFeatureSelector<State, UserProfileState>(userProfileKey);

export const getUserProfileInitialValue = createSelector(getUserProfile, state => state.initialValue);

export const getUserProfileValue = createSelector(getUserProfile, state => state.value);

export const getChanges = createSelector(getUserProfile, getChangesFromState);

export const getIsChanged = createSelector(getUserProfile, state => state.isChanged);
export const getIsInvalid = createSelector(getUserProfile, state => state.isInvalid);
export const getIsApplyChangesEnabled = createSelector(getUserProfile, state => state.isChanged && !state.isInvalid);

export const areUserProfileValuesEqual = (initialValue: UserInfoEditable, value: UserInfoEditable) =>
  getChangesFromState({ initialValue, value }).size === 0;
