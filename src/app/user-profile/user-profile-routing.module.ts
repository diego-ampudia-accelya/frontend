import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ROUTES } from '../shared/constants/routes';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { CanComponentDeactivateGuard } from '~app/core/services';
import { Permissions } from '~app/shared/constants/permissions';

const routes: Routes = [
  {
    path: '',
    component: UserProfileComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      tab: ROUTES.USER_PROFILE,
      requiredPermissions: [Permissions.readProfile, Permissions.readProfileAirlineMulticountry]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule {}
