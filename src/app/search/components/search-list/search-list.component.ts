import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import {
  SearchDocType,
  SearchListFilter,
  SearchListItem,
  SearchTransactionCode
} from '~app/search/models/search.model';
import { SearchListFilterFormatter } from '~app/search/services/search-list-filter-formatter.service';
import { SearchListService } from '~app/search/services/search-list.service';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { CurrencyDictionaryService, NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-search-list',
  templateUrl: './search-list.component.html',
  providers: [
    DefaultQueryStorage,
    { provide: QUERYABLE, useClass: SearchListService },
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [SearchListService] }
  ]
})
export class SearchListComponent implements OnInit {
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public formFactory: FormUtil;

  public predefinedFilters: Partial<SearchListFilter>;

  public docTypesList: DropdownOption<SearchDocType>[];
  public currencyList$: Observable<DropdownOption<Currency>[]>;

  constructor(
    public filterFormatter: SearchListFilterFormatter,
    public dataSource: QueryableDataSource<SearchListItem>,
    private queryStorage: DefaultQueryStorage,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService,
    private searchListService: SearchListService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private translationService: L10nTranslationService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildSearchForm();
    this.columns = this.buildColumns();

    const storedQuery = this.queryStorage.get() || defaultQuery;

    this.initializePredefinedFilters();
    this.initializeNotificationIfNoResults();
    this.loadData(storedQuery);

    this.populateFilterDropdowns();
  }

  public loadData(query: DataQuery): void {
    query = query || cloneDeep(defaultQuery);

    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onViewDocument(document: SearchListItem): void {
    // TODO temporal solution until BE is developed
    this.updateAcmAdmRequestTransactionCode(document);
    this.searchListService.navigateToDocumentLink(document);
  }

  private updateAcmAdmRequestTransactionCode(document: SearchListItem): void {
    const { status, transactionCode } = document;
    const isRequestStatus = status?.toUpperCase().includes('REQUEST');

    if (isRequestStatus) {
      const requestCodeMappings = {
        [SearchTransactionCode.ACMA]: SearchTransactionCode.ACM_REQUEST,
        [SearchTransactionCode.ADMA]: SearchTransactionCode.ADM_REQUEST
      };

      const transitionCodeRequest = requestCodeMappings[transactionCode];
      if (transitionCodeRequest) {
        document.transactionCode = transitionCodeRequest;
      }
    }
  }

  private initializePredefinedFilters(): void {
    this.predefinedFilters = { documentNumber: this.activatedRoute.snapshot.data.searchQuery };
  }

  private initializeNotificationIfNoResults(): void {
    this.dataSource.data$
      .pipe(
        first(),
        filter(data => !data?.length)
      )
      .subscribe(() => this.notificationService.showInformation('LIST.searchList.noResultsNotification'));
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<SearchListFilter>({
      documentNumber: null,
      documentType: null,
      status: null,
      currencyCode: null,
      dateOfIssue: null
    });
  }

  private populateFilterDropdowns(): void {
    this.docTypesList = Object.keys(SearchDocType)
      .map(key => ({
        value: SearchDocType[key],
        label: this.translationService.translate(`LIST.searchList.docTypes.${key}`)
      }))
      .sort((a, b) => a.label.localeCompare(b.label));

    this.currencyList$ = this.currencyDictionaryService.getDropdownOptions();
  }

  private buildColumns(): GridColumn[] {
    return [
      {
        prop: 'documentNumber',
        name: 'LIST.searchList.columns.documentNumber',
        resizeable: true,
        sortable: false,
        cellTemplate: 'commonLinkFromObjCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'transactionCode',
        name: 'LIST.searchList.columns.documentType',
        resizeable: true,
        sortable: false,
        flexGrow: 1
      },
      {
        prop: 'status',
        name: 'LIST.searchList.columns.status',
        resizeable: true,
        sortable: false,
        flexGrow: 1
      },
      {
        prop: 'currencyCode',
        name: 'LIST.searchList.columns.currencyCode',
        resizeable: true,
        sortable: false,
        flexGrow: 1
      },
      {
        prop: 'amount',
        name: 'LIST.searchList.columns.amount',
        resizeable: true,
        sortable: false,
        flexGrow: 1
      },
      {
        prop: 'dateOfIssue',
        name: 'LIST.searchList.columns.dateOfIssue',
        resizeable: true,
        sortable: false,
        cellTemplate: 'dayMonthYearCellTmpl',
        flexGrow: 1
      }
    ];
  }
}
