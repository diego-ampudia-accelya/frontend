/* eslint-disable @typescript-eslint/naming-convention */
import { Currency } from '~app/shared/models/currency.model';

export interface SearchListFilter {
  documentType: SearchDocType[];
  currencyCode: Currency[];
  status: string[];
  documentNumber: string;
  dateOfIssue: Date[];
}

export enum SearchDocType {
  ACDM = 'ACDM',
  REFUND = 'REFUND',
  HOT_DOCUMENT = 'HOT_DOCUMENT',
  PBD = 'PBD'
}

export enum SearchTransactionCode {
  ADMA = 'ADMA',
  ACMA = 'ACMA',
  ADMD = 'ADMD',
  ACMD = 'ACMD',
  SPDR = 'SPDR',
  SPCR = 'SPCR',
  ADNT = 'ADNT',
  ACNT = 'ACNT',
  REFUND_APPLICATION = 'REFUND_APPLICATION',
  REFUND_NOTICE = 'REFUND_NOTICE',
  TKTT = 'TKTT', // Hot Document
  ACM_REQUEST = 'ACM_REQUEST',
  ADM_REQUEST = 'ADM_REQUEST'
}

export interface SearchListItem {
  id: string;
  documentNumber: string;
  isoCountryCode: string;
  status: string; // TODO Clarify possible values with BE
  currencyCode: string;
  transactionCode: SearchTransactionCode; // TODO Clarify possible values with BE
  documentType: SearchDocType;
  amount: number;
  dateOfIssue: Date;
}
