import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Cacheable } from 'ts-cacheable';

import { SearchDocType, SearchListItem, SearchTransactionCode } from '../models/search.model';
import { DataQuery } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants';
import { formatPeriod } from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class SearchListService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/globalsearch/transaction-documents`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService, private router: Router) {}

  @Cacheable()
  private requestData<T>(url: string, dataQuery: Partial<DataQuery>): Observable<T> {
    const formattedQuery = this.formatQuery(dataQuery);

    return this.http.get<T>(url + formattedQuery.getQueryString());
  }

  public find(query: DataQuery<{ [key: string]: any }>): Observable<PagedData<SearchListItem>> {
    return this.requestData(this.baseUrl, query);
  }

  public navigateToDocumentLink(document: SearchListItem): void {
    this.router.navigate(this.getDocumentLink(document));
  }

  private formatQuery(query: Partial<DataQuery>): RequestQuery {
    const { documentNumber, documentType, status, currencyCode } = query.filterBy;
    const dateOfIssue = query.filterBy.dateOfIssue || [];
    const [periodFrom, periodTo] = dateOfIssue;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        documentNumber,
        documentType,
        status: status && status.toLocaleUpperCase(),
        currencyCode:
          currencyCode && (!Array.isArray(currencyCode) ? [currencyCode] : currencyCode).map(({ code }) => code),
        dateOfIssueFrom: periodFrom && formatPeriod(periodFrom),
        dateOfIssueTo: periodFrom && formatPeriod(periodTo || periodFrom)
      }
    });
  }

  private getDocumentLink(document: SearchListItem): string[] {
    let result: string[] = null;

    if (document.documentType === SearchDocType.PBD) {
      result = [ROUTES.POST_BILLING_DISPUTE_DETAILS.url, document.id];
    } else if (document.documentType === SearchDocType.HOT_DOCUMENT) {
      result = [ROUTES.DOCUMENT_VIEW.url, document.id];
    } else {
      switch (document.transactionCode) {
        case SearchTransactionCode.ADMA:
          result = [ROUTES.ADM_VIEW.url, document.id];
          break;
        case SearchTransactionCode.ACMA:
          result = [ROUTES.ACM_VIEW.url, document.id];
          break;
        case SearchTransactionCode.ACM_REQUEST:
          result = [ROUTES.ACM_REQUEST_VIEW.url, document.id];
          break;
        case SearchTransactionCode.ADM_REQUEST:
          result = [ROUTES.ADM_REQUEST_VIEW.url, document.id];
          break;
        case SearchTransactionCode.ADMD:
          result = [ROUTES.ADMD_VIEW.url, document.id];
          break;
        case SearchTransactionCode.ACMD:
          result = [ROUTES.ACMD_VIEW.url, document.id];
          break;
        case SearchTransactionCode.SPDR:
          result = [ROUTES.SPDR_VIEW.url, document.id];
          break;
        case SearchTransactionCode.SPCR:
          result = [ROUTES.SPCR_VIEW.url, document.id];
          break;
        case SearchTransactionCode.ADNT:
          result = [ROUTES.ADNT_VIEW.url, document.id];
          break;
        case SearchTransactionCode.ACNT:
          result = [ROUTES.ACNT_VIEW.url, document.id];
          break;
        case SearchTransactionCode.REFUND_APPLICATION:
          result = [ROUTES.REFUNDS_APP_VIEW.url, document.id];
          break;
        case SearchTransactionCode.REFUND_NOTICE:
          result = [ROUTES.REFUNDS_NOTICE_VIEW.url, document.id];
          break;
      }
    }

    return result;
  }
}
