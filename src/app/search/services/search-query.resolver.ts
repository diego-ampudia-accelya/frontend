import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable()
export class SearchQueryResolver implements Resolve<number> {
  public resolve(route: ActivatedRouteSnapshot): Observable<number> {
    const searchQuery = route.params.searchQuery;

    if (Number.isNaN(searchQuery)) {
      throw new Error(`Missing search query`);
    }

    return of(searchQuery);
  }
}
