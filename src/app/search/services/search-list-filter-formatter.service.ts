import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SearchDocType, SearchListFilter } from '../models/search.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';
import { Currency } from '~app/shared/models/currency.model';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class SearchListFilterFormatter implements FilterFormatter {
  public ignoredFilters: Array<keyof SearchListFilter> = [];

  constructor(private translation: L10nTranslationService) {}

  public format(filter: SearchListFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<SearchListFilter>> = {
      documentNumber: documentNumber => `${this.translate('documentNumber')} - ${documentNumber}`,
      documentType: (typeList: SearchDocType[]) =>
        `${this.translate('documentType')} - ${typeList.map(type => this.translateType(type)).join(', ')}`,
      status: status => `${this.translate('status')} - ${status}`,
      currencyCode: (currencyList: Currency[]) =>
        `${this.translate('currencyCode')} - ${currencyList.map(currency => currency.code).join(', ')}`,
      dateOfIssue: date => `${this.translate('dateOfIssue')} - ${rangeDateFilterTagMapper(date)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.searchList.filters.labels.' + key);
  }

  private translateType(type: SearchDocType): string {
    return this.translation.translate(`LIST.searchList.docTypes.${type}`);
  }
}
