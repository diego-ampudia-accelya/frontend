import { MenuTab, MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';

export function getSearchTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;

  return { label: tab.tabLabel as string, params: { searchQuery: routeData.searchQuery } };
}
