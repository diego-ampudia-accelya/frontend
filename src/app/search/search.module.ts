import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SearchListComponent } from './components/search-list/search-list.component';
import { SearchRoutingModule } from './search-routing.module';
import { SearchListFilterFormatter } from './services/search-list-filter-formatter.service';
import { SearchListService } from './services/search-list.service';
import { SearchQueryResolver } from './services/search-query.resolver';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [SearchListComponent],
  imports: [CommonModule, SearchRoutingModule, SharedModule],
  providers: [SearchQueryResolver, SearchListService, SearchListFilterFormatter]
})
export class SearchModule {}
