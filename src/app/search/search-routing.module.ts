import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchListComponent } from './components/search-list/search-list.component';
import { getSearchTabLabel } from './helpers/search-helper';
import { SearchQueryResolver } from './services/search-query.resolver';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: ROUTES.SEARCH_LIST.path,
    data: { tab: { ...ROUTES.SEARCH_LIST, getTabLabel: getSearchTabLabel } },
    resolve: {
      searchQuery: SearchQueryResolver
    },
    component: SearchListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule {}
