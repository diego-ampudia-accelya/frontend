import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdvancedPaymentListComponent } from './advanced-payment/advanced-payment-list/advanced-payment-list.component';
import { AdvancedPaymentRequestComponent } from './advanced-payment/advanced-payment-request/advanced-payment-request.component';
import { AdvancedPaymentViewComponent } from './advanced-payment/advanced-payment-view/advanced-payment-view.component';
import { SalesAdvancedPaymentResolver } from './advanced-payment/resolvers/sales-advanced-payment-resolver.service';
import { AnnualAccumulatedComponent } from './annual-accumulated/annual-accumulated.component';
import { BillingAnalysisComponent } from './billing-analysis/billing-analysis.component';
import { BillingAnalysisSetupGuard } from './billing-analysis/data/billing-analysis-setup.guard';
import { BillingAnalysisResolver } from './billing-analysis/data/billing-analysis.resolver';
import { BillingStatementComponent } from './billing-statement/billing-statement.component';
import { ComparativeComponent } from './comparative/comparative.component';
import { NonComparativeComponent } from './non-comparative/non-comparative.component';
import { ScuSummaryComponent } from './scu-summary/scu-summary.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  { path: '', redirectTo: 'billing-statement', pathMatch: 'full' },
  {
    path: `${ROUTES.BILLING_STATEMENT.path}`,
    component: BillingStatementComponent,
    data: {
      tab: ROUTES.BILLING_STATEMENT
    }
  },
  {
    path: `${ROUTES.BILLING_ANALYSIS_SETUP.path}`,
    canActivate: [BillingAnalysisSetupGuard]
  },
  {
    path: `${ROUTES.BILLING_ANALYSIS.path}`,
    component: BillingAnalysisComponent,
    resolve: {
      agentBsp: BillingAnalysisResolver
    },
    data: {
      tab: ROUTES.BILLING_ANALYSIS
    }
  },
  {
    path: ROUTES.COMPARATIVE.path,
    component: ComparativeComponent,
    data: {
      tab: ROUTES.COMPARATIVE
    }
  },
  {
    path: ROUTES.NON_COMPARATIVE.path,
    component: NonComparativeComponent,
    data: {
      tab: ROUTES.NON_COMPARATIVE
    }
  },
  {
    path: ROUTES.SCU_SUMMARY.path,
    component: ScuSummaryComponent,
    data: {
      tab: ROUTES.SCU_SUMMARY
    }
  },
  {
    path: ROUTES.ANNUAL_ACCUMULATED.path,
    component: AnnualAccumulatedComponent,
    data: {
      tab: ROUTES.ANNUAL_ACCUMULATED
    }
  },
  {
    path: ROUTES.ADVANCED_PAYMENT.path,
    component: AdvancedPaymentListComponent,
    data: {
      tab: ROUTES.ADVANCED_PAYMENT
    }
  },
  {
    path: ROUTES.ADVANCED_PAYMENT_REQUEST.path,
    component: AdvancedPaymentRequestComponent,
    data: {
      tab: ROUTES.ADVANCED_PAYMENT_REQUEST
    }
  },
  {
    path: ROUTES.ADVANCED_PAYMENT_VIEW.path,
    component: AdvancedPaymentViewComponent,
    data: {
      tab: { ...ROUTES.ADVANCED_PAYMENT_VIEW }
    },
    resolve: {
      item: SalesAdvancedPaymentResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule {}
