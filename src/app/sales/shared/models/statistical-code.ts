/* eslint-disable @typescript-eslint/naming-convention */
export enum StatisticalCode {
  Domestic = 'D',
  International = 'I',
  All = 'B'
}
