/* eslint-disable @typescript-eslint/naming-convention */
export enum PaymentMethod {
  Cash = 'Cash',
  PaymentCard = 'Payment Card',
  EasyPay = 'Easy Pay'
}
