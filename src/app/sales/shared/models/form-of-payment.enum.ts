export enum FormOfPayment {
  Cash = 'CA',
  PaymentCard = 'CC',
  EasyPay = 'EP',
  Exchange = 'EX',
  Void = 'VD',
  CreditTurnedCash = 'CM'
}
