import { FormOfPayment } from './form-of-payment.enum';
import { StatisticalCode } from './statistical-code';
import { Agent, Airline } from '~app/master-data/models';
import { AggregateStatementsBy } from '~app/sales/billing-statement/models';
import { PeriodOption } from '~app/shared/components';
import { AgentGroupSummary, AgentSummary, AirlineSummary, GdsSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface SalesFilter {
  bsp?: Bsp[] | Bsp;
  airline?: Airline[] | AirlineSummary[];
  agent?: Agent[] | AgentSummary[];
  periodRange?: PeriodOption[];
  documentType?: [];
  formOfPayment?: FormOfPayment[];
  statisticalCode?: StatisticalCode;
  currency?: Currency[];
  monthPeriod?: PeriodOption[];
  aggregateBy?: string;
  transactionCode?: string;
  agentGroup?: AgentGroupSummary[];
  reportingSystem?: GdsSummary;
}

export interface NonComparativeSalesFilter {
  bsp?: Bsp[] | Bsp;
  airline?: Airline[] | AirlineSummary[];
  agent?: Agent[] | AgentSummary[];
  periodRange?: PeriodOption[];
  documentType?: [];
  formOfPayment?: FormOfPayment[];
  statisticalCode?: StatisticalCode;
  currency?: Currency[];
  monthPeriod?: PeriodOption[];
  transactionCode?: string;
  agentGroup?: AgentGroupSummary[];
}

export interface ComparativeSalesFilter {
  bsp: Bsp[];
  airline?: AirlineSummary;
  agent?: AgentSummary[];
  statisticalCode?: StatisticalCode;
  currency?: Currency[];
  monthPeriod?: PeriodOption[];
  agentGroup?: AgentGroupSummary[];
}

export interface ComparativeSalesBEFilter {
  bspId: number[];
  airlineId: number;
  agentId: number[];
  year: number;
  month: number;
  statisticalCode: string;
  currencyId: number[];
  agentGroupId: number[];
}

export interface BillingStatementSalesFilter {
  bsp: Bsp[];
  airline?: AirlineSummary[];
  agent: AgentSummary[];
  statisticalCode: StatisticalCode;
  currency: Currency[];
  monthPeriod: PeriodOption[];
  agentGroup: AgentGroupSummary[];
  aggregateBy: AggregateStatementsBy;
  periodRange: PeriodOption[];
}

export interface BillingStatementSalesBEFilter {
  bspId: number[];
  airlineId: number[];
  agentId: number[];
  statisticalCode: string;
  currencyId: number[];
  agentGroupId: number[];
  aggregateBy: string;
  billingAnalysisEndingDateFrom: string;
  billingAnalysisEndingDateTo: string;
  periodYearMonth: string;
}
