export enum DownloadContentType {
  totals = 'TOTALS',
  details = 'DETAILS',
  extendedDetails = 'EXTENDED_DETAILS'
}

export enum DownloadExportAsType {
  txt = 'TXT',
  csv = 'CSV'
}

export interface ExportAsModel {
  content: Array<DownloadContentType>;
  exportAs: DownloadExportAsType;
}

export interface SalesDownloadFormModel {
  details: DownloadContentType;
  totals: DownloadContentType;
  export: DownloadExportAsType;
}
