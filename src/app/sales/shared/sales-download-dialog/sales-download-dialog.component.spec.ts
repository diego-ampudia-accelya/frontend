import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nTranslationModule } from 'angular-l10n';

import { DownloadContentType } from '../models';
import { SalesDownloadDialogComponent } from './sales-download-dialog.component';
import { SalesDownloadDialogService } from '~app/sales/shared/services/sales-download-dialog.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

const mockConfig = {
  data: {
    title: 'title',
    hasCancelButton: true,
    footerButtonsType: FooterButton.Download,
    isClosable: true
  }
};

describe('SalesDownloadDialogComponent', () => {
  let component: SalesDownloadDialogComponent;
  let fixture: ComponentFixture<SalesDownloadDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SalesDownloadDialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        SalesDownloadDialogService,
        DialogService,
        FormBuilder
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesDownloadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('updateCheckboxesValues', () => {
    it('should not update value when it is false', () => {
      (component as any).buildForm();
      (component as any).updateCheckboxesValues();

      component.form.get('details').setValue(false);
      component.form.get('totals').setValue(false);

      expect(component.form.get('details').value).toBe(false);
      expect(component.form.get('totals').value).toBe(false);
    });

    it('should update value when it is true', () => {
      (component as any).buildForm();
      (component as any).updateCheckboxesValues();

      component.form.get('details').setValue(true);
      component.form.get('totals').setValue(true);

      expect(component.form.get('details').value).toBe(DownloadContentType.details);
      expect(component.form.get('totals').value).toBe(DownloadContentType.totals);
    });
  });
});
