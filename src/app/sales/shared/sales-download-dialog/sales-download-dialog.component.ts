import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';

import { MasterDialogData } from '~app/master-shared/models/master.model';
import { DownloadContentType, SalesDownloadFormModel } from '~app/sales/shared/models/sales-download.model';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { DOWNLOAD_FILE_FORM } from '~app/shared/components/form-dialog-components/download-file/download-file.constants';
import { FormUtil } from '~app/shared/helpers';

@Component({
  selector: 'bspl-sales-download-dialog',
  templateUrl: './sales-download-dialog.component.html',
  styleUrls: ['./sales-download-dialog.component.scss']
})
export class SalesDownloadDialogComponent implements OnInit, OnDestroy {
  public radioButtonOptions = DOWNLOAD_FILE_FORM.RADIO_BUTTON_OPTIONS;
  public radioButtonGroup = new FormControl(this.radioButtonOptions[0].value);
  public form: FormGroup;

  protected dialogInjectedData: MasterDialogData;

  private destroy$ = new Subject();

  constructor(private config: DialogConfig, private fb: FormBuilder) {
    this.dialogInjectedData = this.config.data as MasterDialogData;
  }

  public ngOnInit(): void {
    this.buildForm();
    this.disableDownloadBtn();
    this.updateCheckboxesValues();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      details: this.config.data.isDetailed ? DownloadContentType.extendedDetails : DownloadContentType.details,
      totals: DownloadContentType.totals,
      export: this.radioButtonGroup
    });
  }

  // This is necessary when the user clicks on the checkboxes multiple times.
  // At first time checkbox value is string - 'TOTALS'/'DETAILS'.
  // When user activates the checkbox a second time, its value will be 'true' instead of 'TOTALS'/'DETAILS'
  // and user will get a 400 error because of 'content' parameter value.
  private updateCheckboxesValues(): void {
    this.form
      .get('details')
      .valueChanges.pipe(
        filter(value => value === true),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.form.get('details').setValue(DownloadContentType.details, { emitEvent: false }));

    this.form
      .get('totals')
      .valueChanges.pipe(
        filter(value => value === true),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.form.get('totals').setValue(DownloadContentType.totals, { emitEvent: false }));
  }

  private disableDownloadBtn(): void {
    const buttons = this.config.data.buttons as Array<any>;
    if (buttons && buttons.length) {
      const downloadBtn = buttons.find(btn => btn.type === FooterButton.Download);
      this.form.valueChanges.pipe(debounceTime(100), takeUntil(this.destroy$)).subscribe(() => {
        const detailsCtrl = FormUtil.get<SalesDownloadFormModel>(this.form, 'details').value;
        const totalsCtrl = FormUtil.get<SalesDownloadFormModel>(this.form, 'totals').value;
        downloadBtn.isDisabled = !detailsCtrl && !totalsCtrl;
      });
    }
  }
}
