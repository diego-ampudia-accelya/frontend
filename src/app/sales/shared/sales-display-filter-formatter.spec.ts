import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { FormOfPayment, StatisticalCode } from './models';
import { SalesDisplayFilterFormatter } from './sales-display-filter-formatter';
import { SalesDropdownOptionsService } from './services';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

describe('SalesDisplayFilterFormatter', () => {
  let formatter: SalesDisplayFilterFormatter;
  let dropdownServiceStub: jasmine.SpyObj<SalesDropdownOptionsService>;
  let periodPipeStub: jasmine.SpyObj<PeriodPipe>;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    dropdownServiceStub = createSpyObject(SalesDropdownOptionsService);
    periodPipeStub = createSpyObject(PeriodPipe);
    dropdownServiceStub.translateFormOfPayment.and.callFake(value => value + '-translated');
    dropdownServiceStub.translateStatisticalCode.and.callFake(value => value + '-translated');
    translationSpy.translate.and.callFake(identity);
    formatter = new SalesDisplayFilterFormatter(dropdownServiceStub, periodPipeStub, translationSpy);
  });

  it('should format sales type', () => {
    const filter = {
      statisticalCode: StatisticalCode.Domestic
    };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'sales.common.filters.labels.statisticalCode - D-translated',
        keys: ['statisticalCode']
      }
    ]);
  });

  it('should format formOfPayment', () => {
    const filter = {
      formOfPayment: [FormOfPayment.Cash]
    };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'sales.common.filters.labels.formsOfPayment - CA-translated',
        keys: ['formOfPayment']
      }
    ]);
  });

  it('should regular filter', () => {
    const filter = {
      someOtherFilter: 'test'
    };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'Some Other Filter - test',
        keys: ['someOtherFilter'],
        closable: true
      }
    ]);
  });
});
