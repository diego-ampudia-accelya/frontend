import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { FormOfPayment, StatisticalCode } from '../models';
import { SalesDropdownOptionsService } from './sales-dropdown-options.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { Agent, Airline, GlobalAirline } from '~app/master-data/models';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { AppState } from '~app/reducers';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { AgentGroupDictionaryService } from '~app/shared/services/dictionary/agent-group-dictionary.service';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('SalesDropdownOptionsService', () => {
  let service: SalesDropdownOptionsService;
  let translationServiceSpy: SpyObject<L10nTranslationService>;
  let notificationServiceSpy: SpyObject<NotificationService>;
  let mockStore: MockStore<AppState>;
  let airlineServiceSpy: SpyObject<AirlineDictionaryService>;
  let agentServiceSpy: SpyObject<AgentDictionaryService>;

  const usa = { id: 1, name: 'USA' } as Bsp;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore(),
        SalesDropdownOptionsService,
        mockProvider(L10nTranslationService),
        mockProvider(NotificationService),
        mockProvider(PeriodService),
        mockProvider(AirlineDictionaryService),
        mockProvider(AgentDictionaryService),
        mockProvider(CurrencyDictionaryService),
        mockProvider(AgentGroupDictionaryService),
        mockProvider(BspsDictionaryService),
        mockProvider(GdsDictionaryService),
        mockProvider(HttpClient)
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(SalesDropdownOptionsService);
    mockStore = TestBed.inject<any>(Store);
    airlineServiceSpy = TestBed.inject<any>(AirlineDictionaryService);
    agentServiceSpy = TestBed.inject<any>(AgentDictionaryService);
    translationServiceSpy = TestBed.inject<any>(L10nTranslationService);
    translationServiceSpy.translate.and.callFake(identity);
    notificationServiceSpy = TestBed.inject<any>(NotificationService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should return user BSPs when calling getBsps()', fakeAsync(() => {
    mockStore.overrideSelector(fromAuth.getUserBsps, [usa]);

    let actual: DropdownOption<Bsp>[];
    service.getBsps().subscribe(bsps => (actual = bsps));

    expect(actual).toEqual([{ value: usa, label: usa.name }]);
  }));

  it('should return user airline when calling getUserAirline()', fakeAsync(() => {
    const lufthansa = { id: 1, localName: 'Lufthansa', bsp: usa } as Airline;
    const globalAirline = { id: 1, airlines: [lufthansa] } as GlobalAirline;
    mockStore.overrideSelector(fromAuth.getGlobalAirlineForUser, globalAirline);

    const summary = { id: 1 } as AirlineSummary;
    airlineServiceSpy.summarize.and.returnValue(summary);

    let actual: AirlineSummary;
    service.getUserAirline(usa).subscribe(airline => (actual = airline));

    expect(airlineServiceSpy.summarize).toHaveBeenCalledWith(lufthansa);
    expect(actual).toEqual(summary);
  }));

  it('should return user agent when calling getUserAgent()', fakeAsync(() => {
    const tui = { id: 1, name: 'Tui', bsp: usa } as Agent;
    mockStore.overrideSelector(fromAuth.getAgentForUser, tui);

    const summary = { id: '1' } as AgentSummary;
    agentServiceSpy.summarize.and.returnValue(summary);

    let actual: AgentSummary;
    service.getUserAgent().subscribe(airline => (actual = airline));

    expect(agentServiceSpy.summarize).toHaveBeenCalledWith(tui);
    expect(actual).toEqual(summary);
  }));

  it('should show error when notifyOptionMissing() is called', () => {
    service.notifyOptionMissing('test');

    expect(notificationServiceSpy.showError).toHaveBeenCalled();
  });

  it('should return full list of statistical code options when All option is not excluded', () => {
    expect(service.getStatisticalCode().length).toBe(3);
  });

  it('should return only International and Domestic when All option is excluded', () => {
    expect(service.getStatisticalCode({ excludeAllStatisticalCodes: true }).length).toBe(2);
  });

  it('should return proper number of Form of Payment options', () => {
    expect(service.getFormOfPayment().length).toBe(6);
  });

  it('should translate StatisticalCode', () => {
    expect(service.translateStatisticalCode(StatisticalCode.Domestic)).toBe('sales.enums.salesTypeOptions.domestic');
  });

  it('should translate FormOfPayment', () => {
    expect(service.translateFormOfPayment(FormOfPayment.Cash)).toBe('sales.enums.formOfPayment.cash');
  });
});
