import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { memoize } from 'lodash';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { FormOfPayment, StatisticalCode } from '../models';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { AppState } from '~app/reducers';
import { PeriodOption } from '~app/shared/components';
import { DocumentType } from '~app/shared/enums';
import { ArrayHelper } from '~app/shared/helpers';
import {
  AgentDictionaryQueryFilters,
  AgentGroupSummary,
  AgentSummary,
  AirlineSummary,
  GdsSummary
} from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { BspPermissions } from '~app/shared/models/permissions.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';
import { AgentGroupDictionaryService } from '~app/shared/services/dictionary/agent-group-dictionary.service';
import { AirlineDictionaryService } from '~app/shared/services/dictionary/airline-dictionary.service';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { NotificationService } from '~app/shared/services/notification.service';

@Injectable({ providedIn: 'root' })
export class SalesDropdownOptionsService {
  constructor(
    protected translationService: L10nTranslationService,
    protected notificationService: NotificationService,
    protected store: Store<AppState>,
    private periodService: PeriodService,
    private airlineService: AirlineDictionaryService,
    private agentService: AgentDictionaryService,
    private agentGroupService: AgentGroupDictionaryService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private bspsDictionaryService: BspsDictionaryService,
    private gdsDictionaryService: GdsDictionaryService
  ) {
    this.translateFormOfPayment = memoize(this.translateFormOfPayment);
    this.translateStatisticalCode = memoize(this.translateStatisticalCode);
  }

  public getBsps(): Observable<DropdownOption<Bsp>[]> {
    return this.store.pipe(
      select(fromAuth.getUserBsps),
      first(),
      map(bsps => bsps.map(bsp => ({ value: bsp, label: bsp.name })))
    );
  }

  public getUserAirline(bsp: Bsp | BspDto): Observable<AirlineSummary> {
    return this.store.pipe(
      select(fromAuth.getGlobalAirlineForUser),
      first(),
      map(globalAirline => globalAirline && globalAirline.airlines.find(airline => airline.bsp.id === bsp.id)),
      map(airline => this.airlineService.summarize(airline))
    );
  }

  public getUserAirlinesByBsp(bsps: number[]): Observable<AirlineSummary[]> {
    return this.store.select(fromAuth.getUserAirlineByBsp, { bsps });
  }

  public getUserAgent(): Observable<AgentSummary> {
    return this.store.pipe(
      select(fromAuth.getAgentForUser),
      first(),
      map(agent => this.agentService.summarize(agent))
    );
  }

  public notifyOptionMissing(optionType: string): void {
    const msg = this.translationService.translate('sales.errors.missingDropdownOptions', { optionType });
    this.notificationService.showError(msg);
  }

  public getStatisticalCode(options: { excludeAllStatisticalCodes?: boolean } = {}): DropdownOption<StatisticalCode>[] {
    const { excludeAllStatisticalCodes } = options;
    let statisticalCodes = this.translateDropdownOptions([
      { value: StatisticalCode.International, label: 'sales.enums.salesTypeOptions.international' },
      { value: StatisticalCode.Domestic, label: 'sales.enums.salesTypeOptions.domestic' },
      { value: StatisticalCode.All, label: 'sales.enums.salesTypeOptions.allSalesTypes' }
    ]);

    if (excludeAllStatisticalCodes) {
      statisticalCodes = statisticalCodes.filter(option => option.value !== StatisticalCode.All);
    }

    return statisticalCodes;
  }

  public translateStatisticalCode(values: StatisticalCode): string {
    return this.findTranslatedDropdownOption(this.getStatisticalCode(), values);
  }

  public translateFormOfPayment(values: FormOfPayment): string {
    return this.findTranslatedDropdownOption(this.getFormOfPayment(), values);
  }

  public getDocumentTypes(): DropdownOption<DocumentType>[] {
    return ArrayHelper.toDropdownOptions(Object.values(DocumentType));
  }

  public getFormOfPayment(): DropdownOption<FormOfPayment>[] {
    return this.translateDropdownOptions([
      { value: FormOfPayment.Cash, label: 'sales.enums.formOfPayment.cash' },
      { value: FormOfPayment.PaymentCard, label: 'sales.enums.formOfPayment.paymentCard' },
      { value: FormOfPayment.EasyPay, label: 'sales.enums.formOfPayment.easyPay' },
      { value: FormOfPayment.Exchange, label: 'sales.enums.formOfPayment.exchange' },
      { value: FormOfPayment.Void, label: 'sales.enums.formOfPayment.void' },
      { value: FormOfPayment.CreditTurnedCash, label: 'sales.enums.formOfPayment.creditTurnedCash' }
    ]);
  }

  public getAirlines(bsp: Bsp | BspDto): Observable<DropdownOption<AirlineSummary>[]> {
    return this.airlineService.getDropdownOptions(bsp.id);
  }

  public getPeriods(bsp: Bsp | BspDto): Observable<PeriodOption[]> {
    return this.periodService.getByBsp(bsp.id);
  }

  public getAgentGroupsAgents(filter: AgentDictionaryQueryFilters): Observable<DropdownOption<AgentSummary>[]> {
    return this.agentService.getDropdownOptions(filter);
  }

  public getAgents(params: { bspId: number[] }): Observable<DropdownOption<AgentSummary>[]> {
    return this.agentService.getDropdownOptions(params);
  }

  public getAgentGroups(params: { bspId: number[] | number }): Observable<DropdownOption<AgentGroupSummary>[]> {
    return this.agentGroupService.getDropdownOptions(params);
  }

  public getCurrencies(bsp: Bsp | BspDto): Observable<DropdownOption<Currency>[]> {
    return this.currencyDictionaryService.getDropdownOptions(bsp.id);
  }

  public getFilteredCurrencies(params: { bspId: number[] }): Observable<DropdownOption<Currency>[]> {
    return this.currencyDictionaryService.getFilteredDropdownOptions(params);
  }

  public getReportingSystems(bsp: Bsp | BspDto): Observable<DropdownOption<GdsSummary>[]> {
    return this.gdsDictionaryService.getDropdownOptionsForFiltering([bsp.id]);
  }

  public getBspsByPermissions(bspPermissions: BspPermissions[], permissions: string[]): Observable<BspDto[]> {
    return this.bspsDictionaryService.getAllBspsByPermissions(bspPermissions, permissions);
  }

  private translateDropdownOptions<T>(options: DropdownOption<T>[]): DropdownOption<T>[] {
    return options.map(option => ({ ...option, label: this.translationService.translate(option.label) }));
  }

  private findTranslatedDropdownOption<T>(options: DropdownOption<T>[], value: T): string {
    const match = options.find(option => option.value === value);

    return match ? match.label : value.toString();
  }
}
