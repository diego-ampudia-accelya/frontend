import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { DownloadContentType, DownloadExportAsType, ExportAsModel } from '../models';

import { SalesDownloadDialogService } from './sales-download-dialog.service';
import { SalesDropdownOptionsService } from './sales-dropdown-options.service';
import { DialogService, FooterButton } from '~app/shared/components';

describe('SalesDownloadDialogService', () => {
  let service: SalesDownloadDialogService;
  let dialogServiceStub: SpyObject<DialogService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesDropdownOptionsService, mockProvider(L10nTranslationService), mockProvider(DialogService)]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(SalesDownloadDialogService);
    dialogServiceStub = TestBed.inject<any>(DialogService);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should return selected download options when Download is clicked and additional content is chosen', fakeAsync(() => {
    const formValue: any = {
      export: 'csv',
      totals: DownloadContentType.totals,
      details: DownloadContentType.details
    };
    dialogServiceStub.open.and.returnValue(
      of({
        clickedBtn: FooterButton.Download,
        contentComponentRef: { form: { value: formValue } }
      })
    );
    const expected: ExportAsModel = {
      exportAs: DownloadExportAsType.csv,
      content: [DownloadContentType.details, DownloadContentType.totals]
    };

    let actual: ExportAsModel;
    service.openDownloadDialog().subscribe(result => (actual = result));

    expect(actual).toEqual(expected);
    expect(dialogServiceStub.close).toHaveBeenCalled();
  }));

  it('should return selected download options when Download is clicked', fakeAsync(() => {
    const formValue: any = {
      export: 'csv'
    };
    dialogServiceStub.open.and.returnValue(
      of({
        clickedBtn: FooterButton.Download,
        contentComponentRef: { form: { value: formValue } }
      })
    );
    const expected: ExportAsModel = {
      exportAs: DownloadExportAsType.csv,
      content: []
    };

    let actual: ExportAsModel;
    service.openDownloadDialog().subscribe(result => (actual = result));

    expect(actual).toEqual(expected);
  }));
});
