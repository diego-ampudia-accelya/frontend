import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { compact } from 'lodash/fp';
import { Observable } from 'rxjs';
import { filter, first, map, tap } from 'rxjs/operators';

import { DownloadExportAsType, ExportAsModel, SalesDownloadFormModel } from '../models/sales-download.model';
import { SalesDownloadDialogComponent } from '../sales-download-dialog/sales-download-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
@Injectable({
  providedIn: 'root'
})
export class SalesDownloadDialogService {
  constructor(private dialogService: DialogService, private translationService: L10nTranslationService) {}

  public openDownloadDialog(isDetailed?: boolean): Observable<ExportAsModel> {
    return this.dialogService.open(SalesDownloadDialogComponent, this.downloadConfig(isDetailed || false)).pipe(
      filter(action => action.clickedBtn === FooterButton.Download),
      map(value => {
        const formValue = value.contentComponentRef.form.value;

        return this.convertToQuery(formValue);
      }),
      tap(() => this.dialogService.close()),
      first()
    );
  }

  private downloadConfig(isDetailed: boolean): DialogConfig {
    return {
      data: {
        title: isDetailed
          ? this.translationService.translate('common.detailedDowload')
          : this.translationService.translate('common.download'),
        hasCancelButton: true,
        footerButtonsType: FooterButton.Download,
        isClosable: true,
        isDetailed
      }
    };
  }

  private convertToQuery(value: SalesDownloadFormModel): ExportAsModel {
    return {
      exportAs: DownloadExportAsType[value.export],
      content: compact([value.details, value.totals])
    };
  }
}
