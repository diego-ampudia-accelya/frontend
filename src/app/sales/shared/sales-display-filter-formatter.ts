import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { defaults, isArray } from 'lodash';

import { SalesFilter } from './models/sales-filter.model';
import { SalesDropdownOptionsService } from './services';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { bspFilterTagMapper } from '~app/shared/helpers';
import { AgentGroupSummary, AgentSummary, AirlineSummary } from '~app/shared/models';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

const defaultOptions = { showAirline: true, showAgent: true, showAgentGroup: true };

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

/**
 * Formats filters according to the needs of the Billing Analysis component.
 */
@Injectable()
export class SalesDisplayFilterFormatter extends DefaultDisplayFilterFormatter {
  public get options(): Partial<SalesFormatterOptions> {
    return this._options;
  }

  public set options(value: Partial<SalesFormatterOptions>) {
    this._options = defaults(value, defaultOptions);
  }

  public ignoredFilters: Array<keyof SalesFilter> = [];

  private _options: SalesFormatterOptions = defaultOptions;

  constructor(
    private dropdownService: SalesDropdownOptionsService,
    private periodPipe: PeriodPipe,
    private translationService: L10nTranslationService
  ) {
    super();
  }

  /**
   * Transforms the fields periodFrom and periodTo into a single displayable filter.
   * Translates sales Type an form of payment fields to their proper display values
   *
   * @param filter The filter to be formatted
   */
  public format(filter: any): AppliedFilter[] {
    const {
      currency,
      statisticalCode,
      transactionCode,
      monthPeriod,
      periodRange,
      formOfPayment,
      agent,
      airline,
      bsp,
      agentGroup,
      reportingSystem,
      ...rest
    } = filter;

    const formattedFilters = super.format(rest);

    if (this.options.showAirline && airline) {
      const airlineFilter: AirlineSummary[] = isArray(airline) ? airline : [airline];
      const recordMapper = record => ` ${record.code}`;

      formattedFilters.push({
        label: `${this.translate('airlines')} - ${airlineFilter.map(recordMapper).join(', ')}`,
        keys: ['airline']
      });
    }

    if (this.options.showAgent && agent) {
      const agentFilter: AgentSummary[] = isArray(agent) ? agent : [agent];
      const recordMapper = record => `${record.code}`;

      formattedFilters.push({
        label: `${this.translate('agents')} - ${agentFilter.map(recordMapper).join(', ')}`,
        keys: ['agent']
      });
    }

    if (this.options.showAgentGroup && agentGroup) {
      const agentGroupFilter: AgentGroupSummary[] = isArray(agentGroup) ? agentGroup : [agentGroup];

      formattedFilters.push({
        label: `${this.translate('agentGroup')} - ${agentGroupFilter.map(record => `${record.code}`).join(', ')}`,
        keys: ['agentGroup']
      });
    }

    const filterMappers: Partial<FilterMappers<SalesFilter>> = {
      bsp: bspValue => `${this.translate('bsp')} - ${bspFilterTagMapper(bspValue)}`,
      formOfPayment: payment =>
        `${this.translate('formsOfPayment')} - ${payment.map(value =>
          this.dropdownService.translateFormOfPayment(value)
        )}`,
      statisticalCode: code =>
        `${this.translate('statisticalCode')} - ${this.dropdownService.translateStatisticalCode(code)}`,
      currency: currencies => `${this.translate('currency')} - ${currencies.map(curren => curren.code).join(', ')}`,
      transactionCode: transaction => `${this.translate('transactionCode')} - ${transaction}`,
      periodRange: periods => this.periodPipe.transform(periods),
      monthPeriod: periods => this.periodPipe.transform(periods, true),
      reportingSystem: rpsi => `${this.translate('reportingSystem')} - ${rpsi.name}`
    };

    const filterMapped = Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value !== null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({ keys: [item.key], label: item.mapper(item.value) }));

    return [...formattedFilters, ...filterMapped];
  }

  private translate(key: string): string {
    return this.translationService.translate('sales.common.filters.labels.' + key);
  }
}

export interface SalesFormatterOptions {
  showAirline: boolean;
  showAgent: boolean;
  showAgentGroup?: boolean;
}
