import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { SalesFilter } from '../shared/models/sales-filter.model';
import { SalesDownloadDialogService } from '../shared/services/sales-download-dialog.service';
import { BillingStatementComponent } from './billing-statement.component';
import { BillingStatementService } from './data/billing-statement.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { FloatingPanelComponent, PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

const query$ = of({
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery);

describe('BillingStatementComponent', () => {
  let component: BillingStatementComponent;
  let fixture: ComponentFixture<BillingStatementComponent>;

  const billingStatementServiceSpy = createSpyObject(BillingStatementService);
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const dropdownOptionsServiceSpy = createSpyObject(SalesDropdownOptionsService);
  const floatingPanelSpy = createSpyObject(FloatingPanelComponent);
  const dialogServiceSpy = createSpyObject(SalesDownloadDialogService);

  const mockPeriods: Period[] = [
    { id: 1, period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020', isoCountryCode: 'ES' },
    { id: 2, period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020', isoCountryCode: 'ES' }
  ];

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    }
  ];

  const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
    {
      value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
      label: '1111111 / AGENT 1111111'
    },
    {
      value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
      label: '2222222 / AGENT 2222222'
    }
  ];

  const mockCurrencyDropdownOptions: DropdownOption<Currency>[] = [
    {
      value: { id: 1, code: 'EUR', decimals: 2 },
      label: 'EUR'
    },
    {
      value: { id: 1, code: 'USD', decimals: 2 },
      label: 'USD'
    }
  ];

  translationServiceSpy.translate.and.returnValue('The period options are missing.');

  queryableDataSourceSpy.hasData$ = of(true);
  queryableDataSourceSpy.appliedQuery$ = query$;
  dropdownOptionsServiceSpy.getPeriods.and.returnValue(of(mockPeriods));
  dropdownOptionsServiceSpy.getCurrencies.and.returnValue(of([{ value: '', label: '' }]));
  dropdownOptionsServiceSpy.getFilteredCurrencies.and.returnValue(of(mockCurrencyDropdownOptions));
  dropdownOptionsServiceSpy.getAgents.and.returnValue(of(mockAgentDropdownOptions));
  dropdownOptionsServiceSpy.getAirlines.and.returnValue(of(mockAirlineDropdownOptions));
  dropdownOptionsServiceSpy.getUserAirlinesByBsp.and.returnValue(of(mockAirlineDropdownOptions));
  dropdownOptionsServiceSpy.getStatisticalCode.and.returnValue(of([{ value: '', label: '' }]));
  dropdownOptionsServiceSpy.getUserAgent.and.returnValue(of(null));
  dropdownOptionsServiceSpy.getUserAirline.and.returnValue(of(null));

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [Permissions.readBillingStatement, Permissions.lean],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readBillingStatement, Permissions.readNetRemitDocuments] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BillingStatementComponent, TranslatePipeMock],
      imports: [HttpClientTestingModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getUser, value: expectedUserDetails }]
        }),
        PermissionsService,
        { provide: BillingStatementService, useValue: billingStatementServiceSpy },
        FormBuilder,
        { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
        { provide: PeriodPipe, useClass: PeriodPipeMock },
        SalesDisplayFilterFormatter,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: SalesDropdownOptionsService, useValue: dropdownOptionsServiceSpy },
        { provide: SalesDownloadDialogService, useValue: dialogServiceSpy },
        mockProvider(ActivatedRoute)
      ]
    })
      .overrideComponent(BillingStatementComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            DecimalPipe,
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(BillingStatementComponent);
    component = fixture.componentInstance;

    component.totalsPanel = floatingPanelSpy;
    floatingPanelSpy.close.reset();

    // It's neccesary do it after floatingPanelSpy to ensure that it has been created
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should set correct columns on init as an AGENT', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'airline.iataCode',
        hidden: false,
        name: 'sales.billingStatement.columns.airlineCode',
        resizeable: true,
        hideWhenAggregateBy: 'AGENT',
        minWidth: 100,
        flexGrow: 0.6
      }),
      jasmine.objectContaining({
        prop: 'airline.localName',
        hidden: false,
        name: 'sales.billingStatement.columns.airlineName',
        resizeable: true,
        hideWhenAggregateBy: 'AGENT',
        minWidth: 200
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        hidden: true,
        name: 'sales.billingStatement.columns.agentCode',
        resizeable: true,
        hideWhenAggregateBy: 'AIRLINE',
        minWidth: 100,
        flexGrow: 0.6
      }),
      jasmine.objectContaining({
        prop: 'agent.name',
        name: 'sales.billingStatement.columns.agentName',
        resizeable: true,
        hidden: true,
        hideWhenAggregateBy: 'AIRLINE',
        minWidth: 200,
        flexGrow: 2
      })
    ]);

    let columns = [];
    component.ngOnInit();
    component.canQueryAgent = false;
    component.canQueryAirline = true;
    component['buildColumns']().subscribe(data => (columns = data));
    tick();
    expect(columns).toEqual(expectedColumns);
  }));

  it('should set correct columns on init as an AIRLINE', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'airline.iataCode',
        hidden: true,
        name: 'sales.billingStatement.columns.airlineCode',
        resizeable: true,
        hideWhenAggregateBy: 'AGENT',
        minWidth: 100,
        flexGrow: 0.6
      }),
      jasmine.objectContaining({
        prop: 'airline.localName',
        hidden: true,
        name: 'sales.billingStatement.columns.airlineName',
        resizeable: true,
        hideWhenAggregateBy: 'AGENT',
        minWidth: 200
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        hidden: false,
        name: 'sales.billingStatement.columns.agentCode',
        resizeable: true,
        hideWhenAggregateBy: 'AIRLINE',
        minWidth: 100,
        flexGrow: 0.6
      }),
      jasmine.objectContaining({
        prop: 'agent.name',
        name: 'sales.billingStatement.columns.agentName',
        resizeable: true,
        hidden: false,
        hideWhenAggregateBy: 'AIRLINE',
        minWidth: 200,
        flexGrow: 2
      })
    ]);

    let columns = [];
    component.ngOnInit();
    component.canQueryAgent = true;
    component.canQueryAirline = false;
    component['buildColumns']().subscribe(data => (columns = data));
    tick();
    expect(columns).toEqual(expectedColumns);
  }));

  it('should initialize BSP listener if user has LEAN permission', fakeAsync(() => {
    const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

    component.ngOnInit();
    component.searchForm.get('bsp').patchValue(mockBsp);
    tick();

    expect((component as any).selectedBsps).toEqual([mockBsp]);
  }));

  describe('filter dropdowns initialization', () => {
    beforeEach(() => {
      dropdownOptionsServiceSpy.getPeriods.calls.reset();
      dropdownOptionsServiceSpy.getFilteredCurrencies.calls.reset();
      dropdownOptionsServiceSpy.getAgents.calls.reset();
      dropdownOptionsServiceSpy.getAgentGroupsAgents.calls.reset();
      dropdownOptionsServiceSpy.getAirlines.calls.reset();
    });

    it('should initialize PERIOD options dropdown', fakeAsync(() => {
      const expectedPeriodOptions: any = [
        jasmine.objectContaining({ period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' }),
        jasmine.objectContaining({ period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' })
      ];

      component.ngOnInit();
      let options;
      component.periodOptions$.subscribe(data => (options = data));
      tick();

      expect(dropdownOptionsServiceSpy.getPeriods).toHaveBeenCalledWith(expectedUserDetails.bsps[0]);
      expect(options).toEqual(expectedPeriodOptions);
    }));

    // Test failed due to bug fix, will be fixed in a separate task
    xit('should initialize CURRENCY dropdown with selected BSPs', fakeAsync(() => {
      component.ngOnInit();

      tick();

      expect(dropdownOptionsServiceSpy.getFilteredCurrencies).toHaveBeenCalledWith({ bspId: [1] });
      expect(component.currencyOptions).toEqual(mockCurrencyDropdownOptions);
    }));

    // Test failed due to bug fix, will be fixed in a separate task
    xit('should initialize AGENT dropdown ', fakeAsync(() => {
      let agentDropdownOptions: DropdownOption<AgentSummary>[];

      component.ngOnInit();
      tick();

      component.agentDropdownOptions$.subscribe(options => (agentDropdownOptions = options));

      expect(dropdownOptionsServiceSpy.getAgents).toHaveBeenCalledWith({ bspId: [1] });
      expect(agentDropdownOptions).toEqual(mockAgentDropdownOptions);
    }));

    it('should initialize AIRLINE dropdown', fakeAsync(() => {
      let airlineDropdownOptions: DropdownOption<AirlineSummary>[];

      component.ngOnInit();
      tick();

      component.airlineDropdownOptions$.subscribe(options => (airlineDropdownOptions = options));

      expect(dropdownOptionsServiceSpy.getAirlines).toHaveBeenCalledWith(expectedUserDetails.bsps[0]);
      expect(airlineDropdownOptions).toEqual(mockAirlineDropdownOptions);
    }));
  });

  describe('set predefined filters', () => {
    // Test failed due to bug fix, will be fixed in a separate task
    xit('should set predefined filters if user has not LEAN permission', fakeAsync(() => {
      const mockCurrentPeriod: PeriodOption = { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' };
      component['hasLeanPermission'] = false;
      spyOn(PeriodUtils, 'findCurrentPeriod').and.returnValue(mockCurrentPeriod);
      spyOn(component as any, 'populateFilterDropdowns').and.returnValue(of({}));

      component.ngOnInit();
      tick();

      expect(component.predefinedFilters).toEqual({
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
        periodRange: [mockCurrentPeriod, mockCurrentPeriod]
      });
    }));

    it('should set predefined filters if user has LEAN permission', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(component.predefinedFilters).toBeUndefined();
    }));
  });

  describe('load data', () => {
    const mockCurrentPeriod: PeriodOption = { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' };
    const storedQuery: DataQuery<SalesFilter> = {
      ...defaultQuery
    };

    beforeEach(() => {
      spyOn(PeriodUtils, 'findCurrentPeriod').and.returnValue(mockCurrentPeriod);
      queryStorageSpy.get.and.returnValue(storedQuery);
    });

    // Test failed due to bug fix, will be fixed in a separate task
    xit('should add `periodRange` to query filter if user has LEAN permission and there is one filtered BSP', fakeAsync(() => {
      component.ngOnInit();
      tick();

      const expectedQuery = {
        filterBy: jasmine.objectContaining({
          ...storedQuery.filterBy,
          bsp: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }],
          periodRange: [mockCurrentPeriod, mockCurrentPeriod]
        }),
        paginateBy: storedQuery.paginateBy,
        sortBy: storedQuery.sortBy
      };

      expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
      expect(queryStorageSpy.save).toHaveBeenCalledWith(expectedQuery);
    }));
  });

  it('should open download dialog correctly on download', () => {
    const query: DataQuery<SalesFilter> = {
      ...defaultQuery
    };

    queryStorageSpy.get.and.returnValue(query);
    component.onDownload();

    expect(dialogServiceSpy.openDownloadDialog).toHaveBeenCalled();
  });
});
