import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import FileSaver from 'file-saver';
import { chain, cloneDeep, isNil } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { catchError, finalize, first, map, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';

import { BillingQueryActions } from '../billing-analysis/actions';
import { BillingStatementSalesFilter } from '../shared/models/sales-filter.model';
import { SalesDisplayFilterFormatter } from '../shared/sales-display-filter-formatter';
import { BillingStatementService } from './data/billing-statement.service';
import { BillingStatement, BillingStatementTotal } from './models';
import { AggregateStatementsBy } from './models/aggregate-statements-by.enum';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { SalesDownloadDialogService } from '~app/sales/shared/services/sales-download-dialog.service';
import { ButtonDesign, FloatingPanelComponent, PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { Permissions } from '~app/shared/constants/permissions';
import { SelectMode } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentGroupSummary, AgentSummary, AirlineSummary, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-billing-statement',
  templateUrl: './billing-statement.component.html',
  styleUrls: ['./billing-statement.component.scss'],
  providers: [
    DefaultQueryStorage,
    SalesDisplayFilterFormatter,
    { provide: QUERYABLE, useExisting: BillingStatementService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [BillingStatementService]
    }
  ]
})
export class BillingStatementComponent implements OnInit, OnDestroy {
  @ViewChild(FloatingPanelComponent, { static: true }) public totalsPanel: FloatingPanelComponent;
  @ViewChild('netToPayTemplate', { static: true }) private netToPayTemplate: TemplateRef<any>;

  public searchForm: FormGroup;
  public columns$: Observable<Array<TableColumn>>;

  public userBspOptions: DropdownOption<Bsp>[];
  public buttonDesign = ButtonDesign;

  public predefinedFilters: { bsp?: Bsp | Bsp[]; monthPeriod?: PeriodOption[]; periodRange?: PeriodOption[] };

  public canQueryAgent: boolean;
  public canQueryAirline: boolean;

  public agentGroupDropdownOptions$: Observable<DropdownOption[]>;
  public airlineDropdownOptions$: Observable<DropdownOption[]>;
  public agentDropdownOptions$: Observable<DropdownOption[]>;
  public currencyDropdownOptions$: Observable<DropdownOption<Currency>[]>;
  public currencyOptions: DropdownOption<Currency>[];
  public periodOptions$: Observable<PeriodOption[]>;

  public periodMonthPickerYearsBack: number;

  public statisticalCodeDropdownOptions: DropdownOption[];

  public totalsData: Observable<BillingStatementTotal[]>;
  public isTotalsLoading = true;

  public aggregationOptions = [
    {
      value: AggregateStatementsBy.None,
      label: 'sales.billingStatement.aggregation.none'
    },
    {
      value: AggregateStatementsBy.Airline,
      label: 'sales.billingStatement.aggregation.airline'
    },
    {
      value: AggregateStatementsBy.Agent,
      label: 'sales.billingStatement.aggregation.agent'
    }
  ];

  public AggregateStatementsBy = AggregateStatementsBy;
  public selectMode = SelectMode;
  public getAmountFormat = getAmountFormat;

  public noticeDisclaimer$: Observable<string> = this.dataSource?.appliedQuery$.pipe(
    map(query => (query && this.isFilterByCurrentPeriod() ? 'sales.tableNoticeMsg' : null))
  );

  public loggedUser: User;

  public isMonthPeriodPicker: boolean;
  public isBspFilterMultiple: boolean;

  public isUserAirline: boolean;
  public isUserAgent: boolean;
  public isUserAgentGroup: boolean;

  public predefinedUsersAirline: AirlineSummary[] = [];
  public predefinedUserAgent: AgentSummary[] = [];

  public formFactory: FormUtil;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private selectedBsps: Bsp[] = [];
  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  private hasLeanPermission: boolean;

  private fieldsToReset = {
    airline: 'agent',
    agent: 'airline'
  };

  private destroy$ = new Subject();

  private currentPeriodValue: PeriodOption[];

  public listActions$: Observable<{ action: GridTableActionType; disabled?: boolean }[]>;

  constructor(
    public dataSource: QueryableDataSource<BillingStatement>,
    public billingStatementService: BillingStatementService,
    public displayFilterFormatter: SalesDisplayFilterFormatter,
    protected formBuilder: FormBuilder,
    protected store: Store<AppState>,
    protected queryStorage: DefaultQueryStorage,
    protected dropdownOptionsService: SalesDropdownOptionsService,
    protected permissionsService: PermissionsService,
    private salesDownloadDialogService: SalesDownloadDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.initializePermissions();

    this.searchForm = this.buildSearchForm();
    this.columns$ = this.buildColumns();
    this.initializeBspFeatures();
    this.setPeriodPickerYearsBack();

    const storedQuery = this.queryStorage.get();
    if (storedQuery) {
      // Restore BSP selection if user change between open tabs and transform it to array
      const storedBsp = storedQuery.filterBy.bsp;
      const storedBspToArray = storedBsp ? (Array.isArray(storedBsp) ? storedBsp : [storedBsp]) : [];
      this.selectedBsps = storedBspToArray || this.selectedBsps;
    }

    this.populateFilterDropdowns();

    this.setPredefinedFilters();
    this.initializeDataQuery(storedQuery);
  }

  public onAggregationChangeClick(aggregateBy: AggregateStatementsBy) {
    this.resetAggregationRelatedFields(aggregateBy);
  }

  public loadData(query?: DataQuery<BillingStatementSalesFilter>): void {
    query = query || cloneDeep(defaultQuery);

    this.columns$ = this.buildColumns(query.filterBy.aggregateBy);

    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        // This way 'None' aggregation button is selected by default
        aggregateBy: AggregateStatementsBy.None,
        ...this.handleQueryFilter(query.filterBy)
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
    this.totalsPanel.close();
  }

  public onTotalsPanelOpened(): void {
    this.totalsData = this.getTotalsData();
  }

  public onTotalsPanelClosed(): void {
    this.totalsData = of([]);
  }

  public onDownload() {
    combineLatest([this.salesDownloadDialogService.openDownloadDialog(), this.dataSource.appliedQuery$])
      .pipe(
        first(),
        switchMap(([exportAs, dataQuery]) => this.billingStatementService.download(dataQuery, exportAs))
      )
      .subscribe(fileInfo => {
        FileSaver.saveAs(fileInfo.blob, fileInfo.fileName);
      });
  }

  private initializeDataQuery(
    storedQuery: DataQuery<{
      [key: string]: any;
    }>
  ): void {
    // It's necessary load periods before in order to load Data successfully
    this.periodOptions$.subscribe(data => {
      const currentPeriod = PeriodUtils.findCurrentPeriod(data || []);
      this.currentPeriodValue = currentPeriod ? [currentPeriod, currentPeriod] : null;

      this.loadData(storedQuery);
    });
  }

  /** Handles query filter adding `monthPeriod` or `periodRange` depending on filtered BSPs and user LEAN permission */
  private handleQueryFilter(queryFilter: Partial<BillingStatementSalesFilter>): Partial<BillingStatementSalesFilter> {
    // BSP filter is always an array if user has LEAN permission
    const filteredBsps = queryFilter.bsp || this.selectedBsps;

    return {
      ...this.predefinedFilters,
      ...queryFilter,
      monthPeriod: this.selectedBsps.length !== 1 ? queryFilter.monthPeriod || this.currentPeriodValue : null,
      periodRange: this.selectedBsps.length === 1 ? queryFilter.periodRange || this.currentPeriodValue : null,
      ...(this.isBspFilterMultiple && { bsp: filteredBsps }),
      ...(this.isUserAirline && { airline: this.predefinedUsersAirline.length ? this.predefinedUsersAirline : null }),
      ...(this.isUserAgent && { agent: this.predefinedUserAgent.length ? this.predefinedUserAgent : null })
    };
  }

  /** Set predefined filters adding `periodRange`, only available if user has not LEAN permission */
  private setPredefinedFilters(): void {
    if (this.hasLeanPermission) {
      return;
    }

    this.predefinedFilters = {
      bsp: this.selectedBsps[0],
      periodRange: this.currentPeriodValue
    };
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<BillingStatementSalesFilter>({
      bsp: [],
      airline: [],
      agent: [],
      currency: [],
      statisticalCode: [],
      periodRange: [],
      monthPeriod: [],
      aggregateBy: [],
      agentGroup: []
    });
  }

  private populateFilterDropdowns(): void {
    const bspSelectedIds = this.selectedBsps.map(({ id }) => id);
    const firstBsp = this.selectedBsps.length ? this.selectedBsps[0] : this.userBspOptions[0]?.value;
    const params = bspSelectedIds.length ? { bspId: bspSelectedIds } : null;
    this.statisticalCodeDropdownOptions = this.dropdownOptionsService.getStatisticalCode();

    this.agentGroupDropdownOptions$ = this.dropdownOptionsService.getAgentGroups(params);
    this.airlineDropdownOptions$ = this.dropdownOptionsService.getAirlines(firstBsp);
    this.agentDropdownOptions$ = this.getAgentDropDownByUserType(params);
    this.dropdownOptionsService
      .getFilteredCurrencies(params)
      .subscribe(currencies => (this.currencyOptions = currencies));
    this.periodOptions$ = this.dropdownOptionsService.getPeriods(firstBsp);

    const bspsToRetrieveAirlinesId = bspSelectedIds.length
      ? bspSelectedIds
      : this.userBspOptions.map(data => data.value.id);

    this.initializeUserFilters(bspsToRetrieveAirlinesId);
  }

  /**
   * Initialize user specific filters
   */
  private initializeUserFilters(bspIds: number[]): void {
    if (this.isUserAirline) {
      // If the user is an AIRLINE, add the airlineIds as a filter.
      this.dropdownOptionsService
        .getUserAirlinesByBsp(bspIds)
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          this.predefinedUsersAirline = data;
          this.displayFilterFormatter.options.showAirline = !data;
        });
    }

    if (this.isUserAgent) {
      // If the user is an AGENT, add the agentId as a filter.
      this.dropdownOptionsService
        .getUserAgent()
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          this.predefinedUserAgent = [data];
          this.displayFilterFormatter.options.showAgent = !data;
        });
    }
  }

  private getAgentDropDownByUserType(params: { bspId: number[] }): Observable<DropdownOption<AgentSummary>[]> {
    const isAgentGroupUser = this.permissionsService.hasUserType(UserType.AGENT_GROUP);

    return isAgentGroupUser ? this.getAgentGroupDropDown(params) : this.dropdownOptionsService.getAgents(params);
  }

  private getAgentGroupDropDown(params: { bspId: number[] }): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = Permissions.readAgentDictionary;
    const aGroupUserHasPermission = this.permissionsService.hasPermission(aGroupUserPermission);

    if (aGroupUserHasPermission) {
      const filter = { ...params, permission: aGroupUserPermission };

      return this.dropdownOptionsService.getAgentGroupsAgents(filter);
    } else {
      return of(null);
    }
  }

  private buildColumns(aggregateBy?: AggregateStatementsBy): Observable<GridColumn[]> {
    let columns: (GridColumn & { hideWhenAggregateBy?: AggregateStatementsBy })[] = [
      {
        prop: 'bsp.isoCountryCode',
        name: 'sales.billingStatement.columns.bsp',
        resizeable: true,
        minWidth: 150,
        flexGrow: 1
      },
      {
        prop: 'airline.iataCode',
        name: 'sales.billingStatement.columns.airlineCode',
        resizeable: true,
        hidden: !this.canQueryAirline,
        hideWhenAggregateBy: AggregateStatementsBy.Agent,
        minWidth: 100,
        flexGrow: 0.6
      },
      {
        prop: 'airline.localName',
        name: 'sales.billingStatement.columns.airlineName',
        resizeable: true,
        hidden: !this.canQueryAirline,
        hideWhenAggregateBy: AggregateStatementsBy.Agent,
        minWidth: 200,
        flexGrow: 2
      },
      {
        prop: 'agent.agentGroup.code',
        name: 'sales.billingStatement.columns.agentGroupCode',
        resizeable: true,
        hidden: !this.isUserAirline,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'agent.agentGroup.name',
        name: 'sales.billingStatement.columns.agentGroupName',
        resizeable: true,
        hidden: !this.isUserAirline,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'agent.iataCode',
        name: 'sales.billingStatement.columns.agentCode',
        resizeable: true,
        hidden: !this.canQueryAgent,
        hideWhenAggregateBy: AggregateStatementsBy.Airline,
        minWidth: 100,
        flexGrow: 0.6
      },
      {
        prop: 'agent.name',
        name: 'sales.billingStatement.columns.agentName',
        resizeable: true,
        hidden: !this.canQueryAgent,
        hideWhenAggregateBy: AggregateStatementsBy.Airline,
        minWidth: 200,
        flexGrow: 2
      },
      {
        prop: 'statisticalCode',
        name: 'sales.billingStatement.columns.salesType',
        resizeable: true,
        minWidth: 100,
        flexGrow: 0.6,
        pipe: {
          transform: value => this.dropdownOptionsService.translateStatisticalCode(value)
        }
      },
      {
        prop: 'currency.code',
        name: 'sales.billingStatement.columns.currency',
        resizeable: true,
        minWidth: 105,
        flexGrow: 0.5
      },
      {
        prop: 'netToPay',
        name: 'sales.billingStatement.columns.netToBePaid',
        resizeable: true,
        cellTemplate: this.netToPayTemplate,
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 1
      }
    ];

    columns = chain(columns)
      .filter(column => isNil(column.hideWhenAggregateBy) || aggregateBy !== column.hideWhenAggregateBy)
      .value();

    return of(columns);
  }

  /**
   * Clears selection in respective fields in accordance with the currently selected aggregation option.
   *
   * @param aggregateBy The currently selected aggregation option
   */
  private resetAggregationRelatedFields(aggregateBy: AggregateStatementsBy): void {
    // Reset any existing selection on the respective form field, depending on which aggregation option is chosen
    const aggregatedField = aggregateBy.toLowerCase();
    const fieldToReset = this.fieldsToReset[aggregatedField];

    if (this.searchForm.controls[fieldToReset]) {
      this.searchForm.controls[fieldToReset].patchValue([]);
    }
  }

  private getTotalsData(): Observable<BillingStatementTotal[]> {
    return this.dataSource.appliedQuery$.pipe(
      first(),
      tap(() => (this.isTotalsLoading = true)),
      switchMap(query => this.billingStatementService.getTotals(query)),
      catchError(() => of(null)),
      finalize(() => (this.isTotalsLoading = false))
    );
  }

  private isFilterByCurrentPeriod(): boolean {
    const storedQuery = this.queryStorage.get();

    return storedQuery?.filterBy.periodRange?.some(
      periodRange => this.currentPeriodValue?.[0]?.period === periodRange.period
    );
  }

  private setPeriodPickerYearsBack(): void {
    this.periodMonthPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.isUserAirline = this.loggedUser.userType === UserType.AIRLINE;
      this.isUserAgent = this.loggedUser.userType === UserType.AGENT;
      this.isUserAgentGroup = this.loggedUser.userType === UserType.AGENT_GROUP;
    });
  }

  private initializePermissions(): void {
    this.canQueryAgent = !this.permissionsService.hasUserType(UserType.AGENT);
    this.canQueryAirline = !this.permissionsService.hasUserType(UserType.AIRLINE);
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeBspFeatures(): void {
    if (this.hasLeanPermission) {
      this.initializeBspListener();
    }

    // Initialize BSP filter dropdown
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    this.initializeSelectedBsps();

    // Initialize BSP related flags
    this.isBspFilterMultiple = this.hasLeanPermission;
    this.isMonthPeriodPicker = this.selectedBsps.length !== 1;
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readBillingStatement;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private initializeBspListener(): void {
    FormUtil.get<BillingStatementSalesFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(value => {
        const arrayValue = Array.isArray(value) ? value : [value];
        this.selectedBsps = value ? arrayValue : [];
        this.isMonthPeriodPicker = this.selectedBsps.length !== 1;

        this.populateFilterDropdowns();
        this.updateFilterValues();
      });
  }

  /** Since this method is only available for Airline users with LEAN permission, we do not need to update Airline filter */
  private updateFilterValues(): void {
    this.updatePeriodFilterValue();
    this.updateAgentFilterValue();
    this.updateCurrencyFilterValue();
    if (this.canQueryAgent) {
      this.updateAgentGroupFilterValue();
    }
  }

  private updatePeriodFilterValue(): void {
    const periodRangeControl = FormUtil.get<BillingStatementSalesFilter>(this.searchForm, 'periodRange');
    const monthPeriodControl = FormUtil.get<BillingStatementSalesFilter>(this.searchForm, 'monthPeriod');

    if (this.selectedBsps.length === 1) {
      periodRangeControl.patchValue(periodRangeControl.value || this.currentPeriodValue);
      monthPeriodControl.reset();
    } else {
      monthPeriodControl.patchValue(monthPeriodControl.value || this.currentPeriodValue);
      periodRangeControl.reset();
    }
  }

  private updateCurrencyFilterValue(): void {
    // If there are not selected BSPs, dropdown has all currencies from all BSPs so there is no reason to patch currency control value
    if (!this.selectedBsps.length) {
      return;
    }

    const currencyControl = FormUtil.get<BillingStatementSalesFilter>(this.searchForm, 'currency');
    const currenciesSelected: Currency[] = currencyControl.value;

    if (currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyOptions.some(({ value }) => value.id === currency.id)
      );
      currencyControl.patchValue(currenciesToPatch);
    }
  }

  private updateAgentFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentControl = FormUtil.get<BillingStatementSalesFilter>(this.searchForm, 'agent');
    const agentsSelected: AgentSummary[] = agentControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.selectedBsps.some(({ id }) => agent.bsp?.id === id));
      agentControl.patchValue(agentsToPatch);
    }
  }

  private updateAgentGroupFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentGroupControl = FormUtil.get<BillingStatementSalesFilter>(this.searchForm, 'agentGroup');
    const agentsGroupSelected: AgentGroupSummary[] = agentGroupControl.value;

    if (agentsGroupSelected?.length) {
      const agentsGroupToPatch = agentsGroupSelected.filter(agent =>
        this.selectedBsps.some(({ id }) => agent.bsp?.id === id)
      );
      agentGroupControl.patchValue(agentsGroupToPatch);
    }
  }

  private initializeSelectedBsps(): void {
    const firstListBsp = this.userBspOptions[0].value;
    const defaultBsp = this.userBspOptions.find(
      ({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc
    )?.value;
    this.selectedBsps = [firstListBsp];

    if (this.userBspOptions.length > 1) {
      this.selectedBsps = this.hasLeanPermission ? [] : [defaultBsp || firstListBsp];
    }
  }

  public async onActionClick({ action, row }: { action: GridTableAction; row: BillingStatement }): Promise<void> {
    await this.setBillingAnalysisFilters(row);
    this.navigateToBillingAnalysisWithFilters(action);
  }

  private async setBillingAnalysisFilters(row: BillingStatement): Promise<void> {
    const bsp = this.userBspOptions.find(userBsp => userBsp.value.isoCountryCode === row.bsp.isoCountryCode).value;

    const newDefaultQuery: DataQuery = {
      sortBy: [],
      paginateBy: { page: 0, size: PAGINATION.DEFAULT_PAGE_SIZE, totalElements: 0 },
      filterBy: {
        bsp,
        periodRange: this.isMonthPeriodPicker ? this.searchForm.value.monthPeriod : this.searchForm.value.periodRange
      }
    };

    if (!this.isUserAirline) {
      newDefaultQuery.filterBy.airline = {
        id: row.airline?.id,
        name: row.airline?.localName,
        code: row.airline?.iataCode,
        designator: ''
      };
    }

    if (
      !this.isUserAgent &&
      !this.isUserAgentGroup &&
      this.searchForm.value.aggregateBy !== AggregateStatementsBy.Airline
    ) {
      newDefaultQuery.filterBy.agent = {
        id: row.agent?.id,
        name: row.agent?.name,
        code: row.agent?.iataCode
      };
    }

    if (this.isMonthPeriodPicker) {
      newDefaultQuery.filterBy.periodRange = await this.getPeriodRangeForBillingAnalysis(
        newDefaultQuery.filterBy.periodRange,
        bsp
      );
    }

    this.updateDefaultQuery(newDefaultQuery);
  }

  private async getPeriodRangeForBillingAnalysis(periodRange: PeriodOption[], bsp: Bsp): Promise<PeriodOption[]> {
    const period = periodRange?.[0]?.period;
    let options: PeriodOption[] = null;
    let result = periodRange;

    try {
      options = await this.dropdownOptionsService.getPeriods(bsp).toPromise();
    } catch {}

    if (period && options) {
      const newPeriod = options
        .filter(option => option.period.substring(0, 6) === period.substring(0, 6))
        .sort((a, b) => +a.period.substring(6) - +b.period.substring(6));

      if (newPeriod.length) {
        result = [newPeriod[0], newPeriod[newPeriod.length - 1]].filter(Boolean);
      }
    }

    return result;
  }

  private updateDefaultQuery(newDefaultQuery: DataQuery): void {
    this.store.dispatch(new BillingQueryActions.UpdateLastQuery(newDefaultQuery));
    this.store.dispatch(new BillingQueryActions.UpdateDefaultQuery(newDefaultQuery));
  }

  private navigateToBillingAnalysisWithFilters(action: GridTableAction): void {
    if (action.actionType === GridTableActionType.BillingAnalysis) {
      this.router.navigate(['billing-analysis'], { relativeTo: this.activatedRoute.parent });
    }
  }

  public onGetActionList(row: BillingStatement): void {
    this.listActions$ = this.store.select(getUser).pipe(
      first(),
      map(user => {
        const bspPermissions = user.bspPermissions?.find(
          bspPermission => bspPermission.bspId === row.bsp.id
        )?.permissions;
        const hasReadBillingAnalysisPermissionBsp = bspPermissions.includes(Permissions.readBillingAnalysis);
        const hasReadBillingAnalysisPermissionUser = this.permissionsService.hasPermission(
          Permissions.readBillingAnalysis
        );

        let disabled = false;

        if (!row.airline || !hasReadBillingAnalysisPermissionBsp || !hasReadBillingAnalysisPermissionUser) {
          disabled = true;
        }

        return [{ action: GridTableActionType.BillingAnalysis, disabled }];
      })
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
