import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BillingStatement, BillingStatementTotal } from '../models';
import { ExportAsModel } from '~app/sales/shared/models';
import {
  BillingStatementSalesBEFilter,
  BillingStatementSalesFilter
} from '~app/sales/shared/models/sales-filter.model';
import { PeriodOption } from '~app/shared/components';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import {
  downloadRequestOptions,
  extractId,
  formatDownloadResponse,
  formatMonthPeriod,
  formatPeriod
} from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable()
export class BillingStatementService implements Queryable<BillingStatement> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/sales-query/billing-statements`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  /**
   * Get all billing statements matching the request query.
   *
   * @param dataQuery Query containing the searching criteria
   */
  public find(dataQuery: DataQuery): Observable<PagedData<BillingStatement>> {
    return this.requestData<PagedData<BillingStatement>>(this.baseUrl, dataQuery);
  }

  /**
   * Get totals for the provided filtering criteria
   *
   * @param filterBy Filtering criteria
   */
  public getTotals(dataQuery: DataQuery<BillingStatementSalesFilter>): Observable<BillingStatementTotal[]> {
    return this.requestData<BillingStatementTotal[]>(`${this.baseUrl}/totals`, dataQuery, true);
  }

  public download(
    query: DataQuery<BillingStatementSalesFilter>,
    exportOptions: ExportAsModel
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, ...exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private requestData<T>(
    url: string,
    dataQuery: DataQuery<BillingStatementSalesFilter>,
    omitPaging: boolean = false
  ): Observable<T> {
    const formattedQuery = this.formatQuery(dataQuery);

    return this.http.get<T>(url + formattedQuery.getQueryString({ omitPaging }));
  }

  /**
   * Formats DataQuery to be compatible with BE API.
   *
   * @param dataQuery The query to be formatted.
   */
  private formatQuery(dataQuery: DataQuery<BillingStatementSalesFilter>): RequestQuery<BillingStatementSalesBEFilter> {
    const { airline, agent, aggregateBy, statisticalCode, currency, bsp, monthPeriod, agentGroup } = dataQuery.filterBy;
    const periodRange: PeriodOption[] = dataQuery.filterBy.periodRange || [];
    const [periodFrom, periodTo] = periodRange;

    return RequestQuery.fromDataQuery<BillingStatementSalesBEFilter>({
      ...dataQuery,
      filterBy: {
        bspId: bsp && (!Array.isArray(bsp) ? [bsp] : bsp).map(({ id }) => id),
        aggregateBy,
        statisticalCode,
        billingAnalysisEndingDateFrom: periodFrom && formatPeriod(periodFrom.dateFrom),
        billingAnalysisEndingDateTo: periodTo && formatPeriod(periodTo.dateTo),
        periodYearMonth: monthPeriod && formatMonthPeriod(monthPeriod[0].period),
        currencyId: extractId(currency),
        airlineId: extractId(airline),
        agentId: extractId(agent),
        agentGroupId: extractId(agentGroup)
      }
    });
  }
}
