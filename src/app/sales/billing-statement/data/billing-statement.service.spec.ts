import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { BillingStatementService } from '~app/sales/billing-statement/data/billing-statement.service';
import { DownloadContentType, DownloadExportAsType, ExportAsModel } from '~app/sales/shared/models';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions } from '~app/shared/helpers';
import { Bsp } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

describe('BillingStatementService', () => {
  let billingStatementService: BillingStatementService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;
  const bspSwitzerland = { id: 10545, name: 'Switzerland' } as Bsp;
  const filterBy = {
    periodRange: [
      {
        id: 23970,
        isoCountryCode: 'CH',
        period: '2019121',
        dateFrom: '12/01/2019',
        dateTo: '12/07/2019'
      },
      {
        id: 23970,
        isoCountryCode: 'CH',
        period: '2019121',
        dateFrom: '12/01/2019',
        dateTo: '12/07/2019'
      }
    ],
    aggregateBy: 'AIRLINE',
    airline: [{ id: 11115, localName: 'Air France' }],
    currency: [{ id: 10120, code: 'CHF' }],
    statisticalCode: 'I',
    bsp: bspSwitzerland
  };

  beforeEach(waitForAsync(() => {
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of({}));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '/api' });

    TestBed.configureTestingModule({
      providers: [
        BillingStatementService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ],
      imports: [HttpClientTestingModule]
    });
  }));

  beforeEach(() => {
    billingStatementService = TestBed.inject(BillingStatementService);
  });

  it('should create', () => {
    expect(billingStatementService).toBeDefined();
  });

  it('should send proper find request', fakeAsync(() => {
    const dataQuery = {
      filterBy,
      paginateBy: {
        size: 20,
        page: 0,
        totalElements: 1
      },
      sortBy: []
    };

    billingStatementService.find(dataQuery).subscribe();
    tick();

    const expectedUrl =
      '/api/sales-query/billing-statements?' +
      'page=0&size=20&' +
      'bspId=10545&' +
      'aggregateBy=AIRLINE&' +
      'statisticalCode=I&' +
      'billingAnalysisEndingDateFrom=2019-12-01&' +
      'billingAnalysisEndingDateTo=2019-12-07&' +
      'currencyId=10120&' +
      'airlineId=11115';

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper getTotals request', fakeAsync(() => {
    const dataQuery: DataQuery = {
      filterBy,
      paginateBy: {
        size: 20,
        page: 0,
        totalElements: 1
      },
      sortBy: []
    };

    billingStatementService.getTotals(dataQuery).subscribe();
    tick();

    const expectedUrl =
      '/api/sales-query/billing-statements/totals?' +
      'bspId=10545&' +
      'aggregateBy=AIRLINE&' +
      'statisticalCode=I&' +
      'billingAnalysisEndingDateFrom=2019-12-01&' +
      'billingAnalysisEndingDateTo=2019-12-07&' +
      'currencyId=10120&' +
      'airlineId=11115';

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper download request', fakeAsync(() => {
    const dataQuery: DataQuery = {
      filterBy,
      paginateBy: {
        size: 20,
        page: 0,
        totalElements: 1
      },
      sortBy: []
    };
    const exportAs: ExportAsModel = {
      exportAs: DownloadExportAsType.txt,
      content: [DownloadContentType.details, DownloadContentType.totals]
    };
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy.get.and.returnValue(of(httpResponse));

    billingStatementService.download(dataQuery, exportAs).subscribe();
    tick();

    const expectedUrl =
      '/api/sales-query/billing-statements/download?' +
      'bspId=10545&' +
      'aggregateBy=AIRLINE&' +
      'statisticalCode=I&' +
      'billingAnalysisEndingDateFrom=2019-12-01&' +
      'billingAnalysisEndingDateTo=2019-12-07&' +
      'currencyId=10120&' +
      'airlineId=11115&' +
      'exportAs=TXT&' +
      'content=DETAILS,TOTALS';

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, downloadRequestOptions);
  }));
});
