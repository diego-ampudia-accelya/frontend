import { Component, Input } from '@angular/core';

import { BillingStatementTotal } from '~app/sales/billing-statement/models/billing-statement-total.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-billing-statement-totals',
  templateUrl: './billing-statement-totals.component.html',
  styleUrls: ['./billing-statement-totals.component.scss']
})
export class BillingStatementTotalsComponent {
  @Input() totals: BillingStatementTotal[];

  public getAmountFormat = getAmountFormat;
}
