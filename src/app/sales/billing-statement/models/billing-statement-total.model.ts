import { Currency } from '~app/shared/models/currency.model';

export interface BillingStatementTotal {
  currency: Currency;
  netToPay: number;
}
