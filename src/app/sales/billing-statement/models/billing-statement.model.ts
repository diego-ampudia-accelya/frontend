import { StatisticalCode } from '~app/sales/shared/models';
import { AgentDto } from '~app/shared/models/agent.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface BillingStatement {
  agent: AgentDto;
  airline: {
    globalName: string;
    iataCode: string;
    id: number;
    localName: string;
  };
  bsp: BspDto;
  currency: Currency;
  netToPay: number;
  statisticalCode: StatisticalCode;
}
