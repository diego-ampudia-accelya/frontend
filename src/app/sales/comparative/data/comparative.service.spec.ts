import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { ComparativeService } from './comparative.service';
import { DownloadContentType, DownloadExportAsType, ExportAsModel } from '~app/sales/shared/models';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { downloadRequestOptions } from '~app/shared/helpers';
import { AppConfigurationService } from '~app/shared/services';

const query: DataQuery = {
  filterBy: {
    airline: [{ id: 6554485649, code: '081', name: 'Qantas Airways Ltd.', designator: 'QF' }],
    monthPeriod: [
      { id: 65542020041, isoCountryCode: 'A6', period: '2020041', dateFrom: '03/30/2020', dateTo: '04/05/2020' },
      { id: 65542020044, isoCountryCode: 'A6', period: '2020044', dateFrom: '04/20/2020', dateTo: '04/26/2020' }
    ],
    agent: [
      { id: 65540230930, name: 'AGENT TEST', code: '0230930' },
      { id: 65540230529, name: 'AGENT TEST', code: '0230529' }
    ],
    statisticalCode: 'D',
    currency: [{ id: 658568, code: 'AUD', decimals: 2 }],
    bsp: {
      id: 6554,
      isoCountryCode: 'A6',
      name: 'AUSTRALIA'
    }
  },
  sortBy: [{ attribute: 'agentSales', sortType: SortOrder.Desc }],
  paginateBy: { page: 0, size: 20 }
};

describe('ComparativeService', () => {
  let comparativeService: ComparativeService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of(httpResponse));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        ComparativeService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ]
    });

    comparativeService = TestBed.inject(ComparativeService);
  });

  it('should create', () => {
    expect(comparativeService).toBeDefined();
  });

  //TODO fix in technical task
  xit('should send proper request when find() is called with required filters only', fakeAsync(() => {
    const requestQuery: DataQuery = {
      filterBy: {
        airline: [{ id: 6554485649, code: '081', name: 'Qantas Airways Ltd.', designator: 'QF' }],
        monthPeriod: [
          { id: 65542020041, isoCountryCode: 'A6', period: '2020041', dateFrom: '03/30/2020', dateTo: '04/05/2020' },
          { id: 65542020044, isoCountryCode: 'A6', period: '2020044', dateFrom: '04/20/2020', dateTo: '04/26/2020' }
        ],
        bsp: [
          {
            id: 6554,
            isoCountryCode: 'A6',
            name: 'AUSTRALIA'
          }
        ]
      },
      paginateBy: { page: 0, size: 20 },
      sortBy: []
    };

    const expectedUrl =
      '/sales-query/gross-sales/comparatives?page=0&size=20&airlineId=6554485649&bspId=6554&year=2020&month=4';

    comparativeService.find(requestQuery).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  //TODO fix in technical task
  xit('should send proper request when find() is called with all filters', fakeAsync(() => {
    const expectedUrl =
      '/sales-query/gross-sales/comparatives?page=0&size=20&sort=agentSales,DESC&airlineId=6554485649&' +
      'bspId=6554&year=2020&month=4&statisticalCode=D&agentId=65540230930,65540230529&currencyId=658568';

    comparativeService.find(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  //TODO fix in technical task
  xit('should send proper request when getTotals() is called', fakeAsync(() => {
    const expectedUrl =
      '/sales-query/gross-sales/comparatives/totals?sort=agentSales,DESC&airlineId=6554485649' +
      '&bspId=6554&year=2020&month=4&statisticalCode=D&agentId=65540230930,65540230529&currencyId=658568';

    comparativeService.getTotals(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  //TODO fix in technical task
  xit('should send proper request when download() is called', fakeAsync(() => {
    const exportAs: ExportAsModel = {
      exportAs: DownloadExportAsType.txt,
      content: [DownloadContentType.details, DownloadContentType.totals]
    };
    const expectedUrl =
      '/sales-query/gross-sales/comparatives/download?sort=agentSales,DESC&airlineId=6554485649&' +
      'bspId=6554&year=2020&month=4&statisticalCode=D&agentId=65540230930,65540230529&currencyId=658568&exportAs=TXT&content=DETAILS,TOTALS';

    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy.get.and.returnValue(of(httpResponse));

    comparativeService.download(query, exportAs).subscribe();
    tick();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, downloadRequestOptions);
  }));
});
