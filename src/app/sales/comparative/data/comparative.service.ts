import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ComparativeTotals } from '../models/comparative-totals.model';
import { Comparative } from '../models/comparative.model';
import { ExportAsModel } from '~app/sales/shared/models';
import { ComparativeSalesBEFilter, ComparativeSalesFilter } from '~app/sales/shared/models/sales-filter.model';
import { PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, extractId, formatDownloadResponse } from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable()
export class ComparativeService implements Queryable<Comparative> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/sales-query/gross-sales/comparatives`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<ComparativeSalesFilter>): Observable<PagedData<Comparative>> {
    const formattedQuery = this.formatQuery(query);

    return this.http.get<PagedData<Comparative>>(this.baseUrl + formattedQuery.getQueryString());
  }

  public getTotals(query: DataQuery<ComparativeSalesFilter>): Observable<ComparativeTotals[]> {
    const formattedQuery = this.formatQuery(query);

    return this.http.get<ComparativeTotals[]>(
      this.baseUrl + '/totals' + formattedQuery.getQueryString({ omitPaging: true })
    );
  }

  public download(
    query: DataQuery<ComparativeSalesFilter>,
    exportOptions: ExportAsModel
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, ...exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<ComparativeSalesFilter>): RequestQuery<ComparativeSalesBEFilter> {
    const { airline, agent, currency, statisticalCode, bsp, agentGroup } = query.filterBy;
    const monthPeriod: PeriodOption[] = query.filterBy.monthPeriod || [];
    const periodOption = monthPeriod[0];
    let year = null;
    let month = null;
    if (periodOption) {
      const date = PeriodUtils.getDateFrom(periodOption.period);
      year = date.getFullYear();
      month = date.getMonth() + 1;
    }

    return RequestQuery.fromDataQuery<ComparativeSalesBEFilter>({
      ...query,
      filterBy: {
        airlineId: airline.id,
        bspId: extractId(bsp),
        year,
        month,
        statisticalCode,
        agentId: extractId(agent),
        currencyId: extractId(currency),
        agentGroupId: extractId(agentGroup)
      }
    });
  }
}
