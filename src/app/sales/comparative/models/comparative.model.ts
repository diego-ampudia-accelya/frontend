import { StatisticalCode } from '~app/sales/shared/models';
import { AgentDto } from '~app/shared/models/agent.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface Comparative {
  agent: AgentDto;
  agentSales: number;
  agentSalesTotal: number;
  bsp: BspDto;
  currency: Currency;
  percentage: number;
  statisticalCode: StatisticalCode;
}
