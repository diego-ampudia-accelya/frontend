import { Currency } from '~app/shared/models/currency.model';

export interface ComparativeTotals {
  currency: Currency;
  airlineSales: number;
  industrySales: number;
  percentage: number;
}
