import { PercentPipe } from '@angular/common';
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import FileSaver from 'file-saver';
import { cloneDeep, isEmpty } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { catchError, finalize, first, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';

import { ComparativeSalesFilter } from '../shared/models/sales-filter.model';
import { ComparativeService } from './data/comparative.service';
import { ComparativeTotals } from './models/comparative-totals.model';
import { Comparative } from './models/comparative.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { SalesDownloadDialogService } from '~app/sales/shared/services/sales-download-dialog.service';
import { ButtonDesign, FloatingPanelComponent, PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentGroupSummary, AgentSummary, AirlineSummary, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User } from '~app/shared/models/user.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

const LEAN_USERS_YEARS = 5;
const NON_LEAN_USERS_YEARS = 2;

@Component({
  selector: 'bspl-comparative',
  templateUrl: './comparative.component.html',
  styleUrls: ['./comparative.component.scss'],
  providers: [
    DefaultQueryStorage,
    SalesDisplayFilterFormatter,
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [ComparativeService]
    }
  ]
})
export class ComparativeComponent implements OnInit, OnDestroy {
  @ViewChild('amountTemplate', { static: true }) private amountTemplate: TemplateRef<any>;
  @ViewChild(FloatingPanelComponent, { static: true }) public totalsPanel: FloatingPanelComponent;

  public searchForm: FormGroup;
  public columns: GridColumn[];

  public buttonDesign = ButtonDesign;

  public periodMonthPickerYearsBack: number;
  public getAmountFormat = getAmountFormat;

  public totals$: Observable<ComparativeTotals[]>;
  private _loadingTotalSubject = new BehaviorSubject<boolean>(true);
  public isLoadingTotal$: Observable<boolean> = this._loadingTotalSubject.asObservable();

  public predefinedFilters: {
    bsp?: Bsp | Bsp[];
    monthPeriod?: PeriodOption[];
    airline: AirlineSummary;
  };

  public isBspFilterLocked: boolean;
  public hasLeanPermission: boolean;

  public userBspOptions: DropdownOption<Bsp>[] = [];
  public agentDropdownOptions$: Observable<DropdownOption[]>;
  public agentGroupDropdownOptions$: Observable<DropdownOption[]>;
  public statisticalCodeDropdownOptions: DropdownOption[];
  public currencyOptions: DropdownOption<Currency>[];
  public periodDropdownOptions: PeriodOption[];

  private periodOptions$: Observable<PeriodOption[]>;

  private loggedUser: User;
  private predefinedUsersAirline: AirlineSummary;
  private storedQuery: DataQuery<{ [key: string]: any }>;

  private selectedBsps: Bsp[];
  private defaultBsp: Bsp;

  private formFactory: FormUtil;

  private get currentPeriodValue(): PeriodOption[] {
    const currentPeriod = PeriodUtils.findCurrentPeriod(this.periodDropdownOptions || []);

    return currentPeriod ? [currentPeriod, currentPeriod] : null;
  }

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<Comparative>,
    public filterFormatter: SalesDisplayFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private dropdownOptionsService: SalesDropdownOptionsService,
    private percentPipe: PercentPipe,
    private formBuilder: FormBuilder,
    private comparativeService: ComparativeService,
    private salesDownloadDialogService: SalesDownloadDialogService,
    private permissionsService: PermissionsService,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.columns = this.getColumns();
    this.searchForm = this.buildSearchForm();
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    this.initializeLoggedUserFeatures();

    this.setPeriodPickerYearsBack();
  }

  public loadData(query: DataQuery<ComparativeSalesFilter>): void {
    query = query || cloneDeep(defaultQuery);

    if (isEmpty(query.sortBy)) {
      const sortBy = [{ attribute: 'agentSales', sortType: SortOrder.Desc }];
      query = { ...query, sortBy };
    }

    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.handleQueryFilter(query.filterBy)
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);

    this.totalsPanel.close();
  }

  public onTotalPanelOpened(): void {
    this.totals$ = this.getTotals();
  }

  public onTotalPanelClosed(): void {
    this.totals$ = of(null);
  }

  public onDownload(): void {
    combineLatest([this.salesDownloadDialogService.openDownloadDialog(), this.dataSource.appliedQuery$])
      .pipe(
        first(),
        switchMap(([exportAs, query]) => this.comparativeService.download(query, exportAs))
      )
      .subscribe(fileInfo => {
        FileSaver.saveAs(fileInfo.blob, fileInfo.fileName);
      });
  }

  private initializeLoggedUserFeatures(): void {
    this.store.pipe(select(getUser), first()).subscribe(loggedUser => {
      this.loggedUser = loggedUser;

      this.initializeBspFeatures();
      this.getStoredBsps();
      this.populateFilterDropdowns();
      this.setPredefinedFilters();
      this.initializeDataQuery();
    });
  }

  /** Restore BSP selection if user change between open tabs and transform it to array*/
  private getStoredBsps(): void {
    if (this.storedQuery) {
      this.selectedBsps = this.getArrayOfBsps(this.storedQuery.filterBy.bsp);
    }
  }

  private getArrayOfBsps(newBsp: Bsp | Bsp[]): Bsp[] {
    if (!newBsp) {
      return [];
    }

    return Array.isArray(newBsp) ? newBsp : [newBsp];
  }

  private populateFilterDropdowns(): void {
    const bspSelectedIds = this.selectedBsps?.length ? this.selectedBsps.map(bsp => bsp.id) : [];
    const firstBsp = this.selectedBsps?.length ? this.selectedBsps[0] : this.defaultBsp;
    const params = bspSelectedIds.length ? { bspId: bspSelectedIds } : null;

    this.statisticalCodeDropdownOptions = this.dropdownOptionsService.getStatisticalCode();
    this.agentGroupDropdownOptions$ = this.dropdownOptionsService.getAgentGroups(params);
    this.agentDropdownOptions$ = this.dropdownOptionsService.getAgents(params);
    this.dropdownOptionsService
      .getFilteredCurrencies(params)
      .subscribe(currencies => (this.currencyOptions = currencies));
    this.periodOptions$ = this.dropdownOptionsService.getPeriods(firstBsp);

    const bspsToRetrieveAirlinesId = bspSelectedIds.length
      ? bspSelectedIds
      : this.userBspOptions.map(data => data.value?.id);

    this.initializeUserFilters(bspsToRetrieveAirlinesId);
  }

  private initializeUserFilters(bspIds: number[]): void {
    // If the user is assigned to an airline, add the airline as a filter.
    this.dropdownOptionsService
      .getUserAirlinesByBsp(bspIds)
      .pipe(takeUntil(this.destroy$))
      .subscribe(userAirline => {
        if (userAirline) {
          this.predefinedUsersAirline = userAirline[0];
          this.filterFormatter.options = { showAirline: false };
        }
      });
  }

  private initializeBspFeatures(): void {
    if (this.hasLeanPermission) {
      this.initializeBspListener();
    }
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    const firstListBsp: Bsp = this.userBspOptions[0]?.value;
    // Default bsp with the corresponding isoCountryCode might not have permission to read comparative data,
    // in this case we should take the first value with suitable permission
    this.defaultBsp =
      this.userBspOptions.find(({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc)?.value ||
      firstListBsp;

    this.initializeSelectedBsps(firstListBsp);

    this.isBspFilterLocked = !this.hasLeanPermission || this.userBspOptions.length === 1;
  }

  private initializeBspListener(): void {
    FormUtil.get<ComparativeSalesFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(value => {
        this.selectedBsps = this.getArrayOfBsps(value);

        this.populateFilterDropdowns();
        this.updateFilterValues();
      });
  }

  private updateFilterValues(): void {
    this.updatePeriodFilterValue();
    this.updateAgentFilterValue();
    this.updateCurrencyFilterValue();
    this.updateAgentGroupFilterValue();
  }

  private updatePeriodFilterValue(): void {
    const bspsToGetperiod: Bsp =
      this.selectedBsps.length === 1 && this.hasLeanPermission ? this.selectedBsps[0] : this.defaultBsp;

    this.periodOptions$ = this.dropdownOptionsService.getPeriods(bspsToGetperiod);
  }

  private updateCurrencyFilterValue(): void {
    if (!this.selectedBsps.length) {
      return;
    }

    const currencyControl = FormUtil.get<ComparativeSalesFilter>(this.searchForm, 'currency');
    const currenciesSelected: Currency[] = currencyControl.value;

    if (currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyOptions.some(({ value }) => value.id === currency.id)
      );
      currencyControl.patchValue(currenciesToPatch);
    }
  }

  private updateAgentFilterValue(): void {
    if (!this.selectedBsps.length) {
      return;
    }

    const agentControl = FormUtil.get<ComparativeSalesFilter>(this.searchForm, 'agent');
    const agentsSelected: AgentSummary[] = agentControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.selectedBsps.some(({ id }) => agent.bsp?.id === id));
      agentControl.patchValue(agentsToPatch);
    }
  }

  private updateAgentGroupFilterValue(): void {
    if (!this.selectedBsps.length) {
      return;
    }

    const agentGroupControl = FormUtil.get<ComparativeSalesFilter>(this.searchForm, 'agentGroup');
    const agentsGroupSelected: AgentGroupSummary[] = agentGroupControl.value;

    if (agentsGroupSelected?.length) {
      const agentsGroupToPatch = agentsGroupSelected.filter(agent =>
        this.selectedBsps.some(({ id }) => agent.bsp?.id === id)
      );
      agentGroupControl.patchValue(agentsGroupToPatch);
    }
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readComparatives;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private initializeSelectedBsps(firstListBsp: Bsp): void {
    this.selectedBsps = [firstListBsp];

    if (this.userBspOptions.length > 1) {
      this.selectedBsps = this.hasLeanPermission ? [] : [this.defaultBsp || firstListBsp];
    }
  }

  private initializeDataQuery(): void {
    // It's necessary load periods before in order to load Data successfully
    this.periodOptions$.subscribe(data => {
      this.periodDropdownOptions = data;
      this.loadData(this.storedQuery);
    });
  }

  private handleQueryFilter(queryFilter: Partial<ComparativeSalesFilter>): Partial<ComparativeSalesFilter> {
    // BSP filter is always an array if user has LEAN permission
    const filteredBsps = queryFilter.bsp || this.selectedBsps;

    return {
      ...this.predefinedFilters,
      ...queryFilter,
      monthPeriod: queryFilter.monthPeriod ? queryFilter.monthPeriod : this.currentPeriodValue,
      airline: this.predefinedUsersAirline ? this.predefinedUsersAirline : null,
      ...(this.hasLeanPermission && { bsp: filteredBsps })
    };
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<ComparativeSalesFilter>({
      bsp: [],
      monthPeriod: [],
      agent: [],
      statisticalCode: [],
      currency: [],
      agentGroup: []
    });
  }

  private setPeriodPickerYearsBack(): void {
    this.periodMonthPickerYearsBack = this.hasLeanPermission ? LEAN_USERS_YEARS : NON_LEAN_USERS_YEARS;
  }

  private getColumns(): GridColumn[] {
    return [
      {
        prop: 'bsp.isoCountryCode',
        name: 'sales.comparatives.columns.bsp',
        resizeable: true,
        minWidth: 100,
        flexGrow: 0.4
      },
      {
        prop: 'agent.agentGroup.code',
        name: 'sales.comparatives.columns.agentGroupCode',
        resizeable: true,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'agent.agentGroup.name',
        name: 'sales.comparatives.columns.agentGroupName',
        resizeable: true,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'agent.iataCode',
        name: 'sales.comparatives.columns.agentCode',
        resizeable: true,
        flexGrow: 0.8
      },
      {
        prop: 'agent.name',
        name: 'sales.comparatives.columns.agentName',
        resizeable: true,
        flexGrow: 2
      },
      {
        prop: 'statisticalCode',
        name: 'sales.comparatives.columns.salesType',
        resizeable: true,
        flexGrow: 1,
        pipe: {
          transform: value => this.dropdownOptionsService.translateStatisticalCode(value)
        }
      },
      {
        prop: 'currency.code',
        name: 'sales.comparatives.columns.currency',
        resizeable: true,
        flexGrow: 0.6
      },
      {
        prop: 'agentSales',
        name: 'sales.comparatives.columns.agentSales',
        resizeable: true,
        flexGrow: 1,
        cellClass: 'text-right',
        cellTemplate: this.amountTemplate
      },
      {
        prop: 'agentSalesTotal',
        name: 'sales.comparatives.columns.agentSalesTotal',
        resizeable: true,
        flexGrow: 1,
        cellClass: 'text-right',
        cellTemplate: this.amountTemplate
      },
      {
        prop: 'percentage',
        name: 'sales.comparatives.columns.percentage',
        resizeable: true,
        flexGrow: 0.8,
        cellClass: 'text-right',
        pipe: { transform: v => this.percentPipe.transform(v / 100, '1.2-2') }
      }
    ];
  }

  private setPredefinedFilters(): void {
    if (this.hasLeanPermission) {
      return;
    }

    this.predefinedFilters = {
      ...this.predefinedFilters,
      bsp: this.selectedBsps[0],
      monthPeriod: this.currentPeriodValue,
      airline: this.predefinedUsersAirline
    };
  }

  private getTotals(): Observable<ComparativeTotals[]> {
    return this.dataSource.appliedQuery$.pipe(
      first(),
      tap(() => this._loadingTotalSubject.next(true)),
      switchMap(query => this.comparativeService.getTotals(query)),
      catchError(() => of(null)),
      finalize(() => this._loadingTotalSubject.next(false))
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
