import { DatePipe, PercentPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { ComparativeSalesFilter } from '../shared/models/sales-filter.model';
import { ComparativeComponent } from './comparative.component';
import { ComparativeService } from './data/comparative.service';
import { ComparativeTotals } from './models/comparative-totals.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { SalesDownloadDialogService } from '~app/sales/shared/services/sales-download-dialog.service';
import { FloatingPanelComponent } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('Comparative component', () => {
  let component: ComparativeComponent;
  let fixture: ComponentFixture<ComparativeComponent>;

  const comparativeServiceSpy = createSpyObject(ComparativeService);
  const dropdownOptionsServiceSpy = createSpyObject(SalesDropdownOptionsService);
  const floatingPanelSpy = createSpyObject(FloatingPanelComponent);
  const salesDownloadDialogServiceSpy = createSpyObject(SalesDownloadDialogService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);

  const totals: ComparativeTotals[] = [
    {
      currency: null,
      airlineSales: 0,
      industrySales: 0,
      percentage: 0
    }
  ];

  const query$ = of({
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  } as DataQuery);

  const mockPeriods: Period[] = [
    { id: 1, period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020', isoCountryCode: 'ES' },
    { id: 2, period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020', isoCountryCode: 'ES' }
  ];

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    }
  ];

  const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
    {
      value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
      label: '1111111 / AGENT 1111111'
    },
    {
      value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
      label: '2222222 / AGENT 2222222'
    }
  ];

  const mockCurrencyDropdownOptions: DropdownOption<Currency>[] = [
    {
      value: { id: 1, code: 'EUR', decimals: 2 },
      label: 'EUR'
    },
    {
      value: { id: 1, code: 'USD', decimals: 2 },
      label: 'USD'
    }
  ];

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [Permissions.readComparatives, Permissions.lean],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readComparatives] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    }
  };

  translationServiceSpy.translate.and.returnValue('The period options are missing.');

  queryableDataSourceSpy.hasData$ = of(true);
  queryableDataSourceSpy.appliedQuery$ = query$;
  queryStorageSpy.get(query$);
  dropdownOptionsServiceSpy.getPeriods.and.returnValue(of(mockPeriods));
  dropdownOptionsServiceSpy.getCurrencies.and.returnValue(of([{ value: '', label: '' }]));
  dropdownOptionsServiceSpy.getFilteredCurrencies.and.returnValue(of(mockCurrencyDropdownOptions));
  dropdownOptionsServiceSpy.getAgents.and.returnValue(of(mockAgentDropdownOptions));
  dropdownOptionsServiceSpy.getAirlines.and.returnValue(of(mockAirlineDropdownOptions));
  dropdownOptionsServiceSpy.getUserAirlinesByBsp.and.returnValue(of(mockAirlineDropdownOptions[0]));
  dropdownOptionsServiceSpy.getStatisticalCode.and.returnValue(of([{ value: '', label: '' }]));
  dropdownOptionsServiceSpy.getUserAgent.and.returnValue(of(null));
  dropdownOptionsServiceSpy.getUserAirline.and.returnValue(of(null));
  dropdownOptionsServiceSpy.getBspsByPermissions.and.returnValue(of(expectedUserDetails.bsps));

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ComparativeComponent, TranslatePipeMock],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getUser, value: expectedUserDetails }]
        }),
        PermissionsService,
        FormBuilder,
        mockProvider(PercentPipe),
        mockProvider(DatePipe),
        { provide: ComparativeService, useValue: comparativeServiceSpy },
        { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
        SalesDisplayFilterFormatter,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: SalesDropdownOptionsService, useValue: dropdownOptionsServiceSpy },
        { provide: SalesDownloadDialogService, useValue: salesDownloadDialogServiceSpy },
        { provide: PeriodPipe, useClass: PeriodPipeMock }
      ]
    })
      .overrideComponent(ComparativeComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ComparativeComponent);
    component = fixture.componentInstance;

    component.totalsPanel = floatingPanelSpy;
    floatingPanelSpy.close.reset();

    // It's neccesarry do it after floatingPanelSpy to ensure that it has been created
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should setInitialFilterValues', () => {
    const formExpected = {
      bsp: null,
      monthPeriod: null,
      agent: null,
      statisticalCode: null,
      currency: null,
      agentGroup: null
    };
    const result = component['buildSearchForm']();

    expect(formExpected).toEqual(result.value);
  });

  it('should init columns for the table when component is loaded', () => {
    component.ngOnInit();

    const expectedColumns: any[] = [
      jasmine.objectContaining({
        prop: 'bsp.isoCountryCode',
        name: 'sales.comparatives.columns.bsp',
        resizeable: true,
        minWidth: 100,
        flexGrow: 0.4
      }),
      jasmine.objectContaining({
        prop: 'agent.agentGroup.code',
        name: 'sales.comparatives.columns.agentGroupCode',
        resizeable: true,
        flexGrow: 0.8
      }),
      jasmine.objectContaining({
        prop: 'agent.agentGroup.name',
        name: 'sales.comparatives.columns.agentGroupName',
        resizeable: true,
        flexGrow: 0.8
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        name: 'sales.comparatives.columns.agentCode',
        resizeable: true,
        flexGrow: 0.8
      }),
      jasmine.objectContaining({
        prop: 'agent.name',
        name: 'sales.comparatives.columns.agentName',
        resizeable: true,
        flexGrow: 2
      }),
      jasmine.objectContaining({
        prop: 'statisticalCode',
        name: 'sales.comparatives.columns.salesType',
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'currency.code',
        name: 'sales.comparatives.columns.currency',
        resizeable: true,
        flexGrow: 0.6
      }),
      jasmine.objectContaining({
        prop: 'agentSales',
        name: 'sales.comparatives.columns.agentSales',
        resizeable: true,
        flexGrow: 1,
        cellClass: 'text-right'
      }),
      jasmine.objectContaining({
        prop: 'agentSalesTotal',
        name: 'sales.comparatives.columns.agentSalesTotal',
        resizeable: true,
        flexGrow: 1,
        cellClass: 'text-right'
      }),
      jasmine.objectContaining({
        prop: 'percentage',
        name: 'sales.comparatives.columns.percentage',
        resizeable: true,
        flexGrow: 0.8,
        cellClass: 'text-right'
      })
    ];

    expect(component.columns).toEqual(expectedColumns);
  });

  it('should set totals to null when total panel is closed', () => {
    spyOn<any>(component, 'getTotals').and.returnValue(of(totals));

    component.onTotalPanelClosed();
    let result;
    component.totals$.subscribe(data => (result = data));

    expect(result).toEqual(null);
  });

  it('should set totals when total panel is opened', fakeAsync(() => {
    component.totals$ = of(null);
    spyOn<any>(component, 'getTotals').and.returnValue(of(totals));

    component.onTotalPanelOpened();

    tick();
    let result;
    component.totals$.subscribe(data => (result = data));

    expect(result).toEqual(totals);
  }));

  it('should open download dialog correctly on download', () => {
    const query: DataQuery<ComparativeSalesFilter> = {
      ...defaultQuery
    };

    queryStorageSpy.get.and.returnValue(query);
    component.onDownload();

    expect(salesDownloadDialogServiceSpy.openDownloadDialog).toHaveBeenCalled();
  });

  describe('getStoredBsps', () => {
    beforeEach(() => {
      (component as any).dropdownOptionsService.getPeriods = jasmine.createSpy().and.returnValue(of([]));
    });

    it('should set stored bsps array to selected bsps', () => {
      (component as any).selectedBsps = [];
      (component as any).storedQuery = {
        filterBy: {
          bsp: [{ isoCountryCode: 'ES', id: 1 }]
        }
      };

      (component as any).getStoredBsps();

      expect((component as any).selectedBsps).toEqual([{ isoCountryCode: 'ES', id: 1 }]);
    });

    it('should set stored bsp to selected bsps', () => {
      (component as any).selectedBsps = [];
      (component as any).storedQuery = {
        filterBy: {
          bsp: { isoCountryCode: 'ES', id: 1 }
        }
      };

      (component as any).getStoredBsps();

      expect((component as any).selectedBsps).toEqual([{ isoCountryCode: 'ES', id: 1 }]);
    });

    it('should set empty array to selected bsps when stored bsp is empty', () => {
      (component as any).selectedBsps = [];
      (component as any).storedQuery = {
        filterBy: {}
      };

      (component as any).getStoredBsps();

      expect((component as any).selectedBsps).toEqual([]);
    });

    it('should NOT update selected bsps when stored query is empty', () => {
      (component as any).selectedBsps = [{ isoCountryCode: 'ES', id: 1 }];
      (component as any).storedQuery = null;

      (component as any).getStoredBsps();

      expect((component as any).selectedBsps).toEqual([{ isoCountryCode: 'ES', id: 1 }]);
    });
  });

  describe('updatePeriodFilterValue', () => {
    it('should call getPeriods with first selected bsp', fakeAsync(() => {
      (component as any).selectedBsps = [{ isoCountryCode: 'ES', id: 1 }];
      (component as any).defaultBsp = { isoCountryCode: 'IL', id: 2 };
      component.hasLeanPermission = true;
      (component as any).dropdownOptionsService.getPeriods = jasmine.createSpy().and.returnValue(of([]));

      (component as any).updatePeriodFilterValue();
      (component as any).periodOptions$.pipe(take(1)).subscribe();
      tick();

      expect((component as any).dropdownOptionsService.getPeriods).toHaveBeenCalledWith({
        isoCountryCode: 'ES',
        id: 1
      });
    }));

    it('should call getPeriods with default bsp when selected bsp not single', fakeAsync(() => {
      (component as any).selectedBsps = [
        { isoCountryCode: 'ES', id: 1 },
        { isoCountryCode: 'MT', id: 3 }
      ];
      (component as any).defaultBsp = { isoCountryCode: 'IL', id: 2 };
      component.hasLeanPermission = true;
      (component as any).dropdownOptionsService.getPeriods = jasmine.createSpy().and.returnValue(of([]));

      (component as any).updatePeriodFilterValue();
      (component as any).periodOptions$.pipe(take(1)).subscribe();
      tick();

      expect((component as any).dropdownOptionsService.getPeriods).toHaveBeenCalledWith({
        isoCountryCode: 'IL',
        id: 2
      });
    }));

    it('should call getPeriods with default bsp when hasLeanPermission is false', fakeAsync(() => {
      (component as any).selectedBsps = [
        { isoCountryCode: 'ES', id: 1 },
        { isoCountryCode: 'MT', id: 3 }
      ];
      (component as any).defaultBsp = { isoCountryCode: 'IL', id: 2 };
      component.hasLeanPermission = false;
      (component as any).dropdownOptionsService.getPeriods = jasmine.createSpy().and.returnValue(of([]));

      (component as any).updatePeriodFilterValue();
      (component as any).periodOptions$.pipe(take(1)).subscribe();
      tick();

      expect((component as any).dropdownOptionsService.getPeriods).toHaveBeenCalledWith({
        isoCountryCode: 'IL',
        id: 2
      });
    }));
  });
});
