import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { orderBy } from 'lodash';

import { ComparativeTotals } from '../models/comparative-totals.model';
import { SimpleTableColumn } from '~app/shared/components';
import { SelectMode } from '~app/shared/enums';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-comparative-totals',
  templateUrl: './comparative-totals.component.html',
  styleUrls: ['./comparative-totals.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComparativeTotalsComponent implements OnInit, OnChanges {
  @Input() totals: ComparativeTotals[];

  public columns: SimpleTableColumn<ComparativeTotals>[];
  public SelectMode = SelectMode;
  public selectedCurrency: Currency;

  public currencyOptions: DropdownOption<Currency>[];
  public getAmountFormat = getAmountFormat;

  public get data(): ComparativeTotals {
    let data = {};
    if (this.totals && this.totals.length && this.selectedCurrency) {
      data = this.totals.find(total => total.currency.id === this.selectedCurrency.id) || {};
    }

    return data as ComparativeTotals;
  }

  @ViewChild('actionHeaderTemplate', { static: true })
  private actionHeaderTemplate: TemplateRef<any>;

  @ViewChild('industryCellTemplate', { static: true })
  private industryCellTemplate: TemplateRef<any>;

  @ViewChild('emptyCellTemplate', { static: true })
  private emptyCellTemplate: TemplateRef<any>;

  @ViewChild('amountTemplate', { static: true })
  private amountTemplate: TemplateRef<any>;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.totals && changes.totals.currentValue) {
      this.currencyOptions = this.getCurrencyOptions(this.totals);
      this.selectedCurrency = this.currencyOptions[0] ? this.currencyOptions[0].value : null;
    }
  }

  public ngOnInit(): void {
    this.columns = this.getColumns();
  }

  public selectCurrency(currency: Currency): void {
    this.selectedCurrency = currency;
  }

  private getColumns(): SimpleTableColumn<ComparativeTotals>[] {
    const baseColumn: Partial<SimpleTableColumn<any>> = {
      customStyle: 'text-right',
      cellTemplate: this.amountTemplate
    };

    return [
      {
        headerCellTemplate: this.actionHeaderTemplate,
        field: null,
        customStyle: 'actions-column-cell',
        cellTemplate: this.emptyCellTemplate
      },
      { ...baseColumn, header: 'sales.comparatives.totals.myAirline', field: 'airlineSales' },
      { ...baseColumn, header: 'sales.comparatives.totals.totalIndustry', field: 'industrySales' },
      {
        customStyle: 'text-right',
        header: 'sales.comparatives.totals.industry',
        field: 'percentage',
        cellTemplate: this.industryCellTemplate
      }
    ];
  }

  private getCurrencyOptions(totals: ComparativeTotals[]): DropdownOption<Currency>[] {
    totals = totals || [];

    return orderBy(
      totals.map(({ currency }) => ({
        value: currency,
        label: currency.code
      })),
      'label'
    );
  }
}
