import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ComparativeTotals } from '../models/comparative-totals.model';
import { ComparativeTotalsComponent } from './comparative-totals.component';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { TranslatePipeMock } from '~app/test';

const totals: ComparativeTotals[] = [
  {
    currency: {
      id: 1,
      code: 'USD',
      decimals: 2
    },
    airlineSales: 123456,
    industrySales: 234567,
    percentage: 52.63
  },
  {
    currency: {
      id: 2,
      code: 'EUR',
      decimals: 2
    },
    airlineSales: 1111111,
    industrySales: 2222222,
    percentage: 50
  }
];

describe('ComparativeTotalsComponent', () => {
  let component: ComparativeTotalsComponent;
  let fixture: ComponentFixture<ComparativeTotalsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ComparativeTotalsComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparativeTotalsComponent);
    component = fixture.componentInstance;
    component.totals = totals;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the proper columns', () => {
    component.columns = null;
    component.ngOnInit();

    const expectedColumns: any[] = [
      jasmine.objectContaining({ field: null, customStyle: 'actions-column-cell' }),
      jasmine.objectContaining({
        customStyle: 'text-right',
        header: 'sales.comparatives.totals.myAirline',
        field: 'airlineSales'
      }),
      jasmine.objectContaining({
        customStyle: 'text-right',
        header: 'sales.comparatives.totals.totalIndustry',
        field: 'industrySales'
      }),
      jasmine.objectContaining({
        customStyle: 'text-right',
        header: 'sales.comparatives.totals.industry',
        field: 'percentage'
      })
    ];

    expect(component.columns).toEqual(jasmine.objectContaining(expectedColumns));
  });

  it('should set the selected currency', () => {
    component.selectedCurrency = null;
    const expected: Currency = {
      id: 1,
      code: 'EUR',
      decimals: 2
    };

    component.selectCurrency(expected);

    expect(component.selectedCurrency).toEqual(expected);
  });

  it('should generate the currency options (ordered by code ASC) based on the totals provided', () => {
    const expected: DropdownOption<Currency>[] = [
      {
        value: {
          id: 2,
          code: 'EUR',
          decimals: 2
        },
        label: 'EUR'
      },
      {
        value: {
          id: 1,
          code: 'USD',
          decimals: 2
        },
        label: 'USD'
      }
    ];

    component.ngOnChanges({ totals: new SimpleChange(null, component.totals, false) });

    expect(component.currencyOptions).toEqual(expected);
  });

  it('should preselect the first currency', () => {
    const expected: Currency = {
      id: 2,
      code: 'EUR',
      decimals: 2
    };

    component.ngOnChanges({ totals: new SimpleChange(null, component.totals, false) });

    expect(component.selectedCurrency).toEqual(expected);
  });
});
