/* eslint-disable @typescript-eslint/no-unused-vars */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AdvancedPaymentService } from './advanced-payment.service';
import { AppConfigurationService } from '~app/shared/services';

describe('AdvancedPaymentService', () => {
  let service: AdvancedPaymentService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AdvancedPaymentService, { provide: AppConfigurationService, useValue: { baseApiPath: '' } }]
    });

    service = TestBed.inject<AdvancedPaymentService>(AdvancedPaymentService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
