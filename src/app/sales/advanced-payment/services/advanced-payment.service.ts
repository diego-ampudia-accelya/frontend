import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AttachFileRequest, AttachFileResponse, DeleteFileRequest } from '../models/advanced-payment.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class AdvancedPaymentService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/advanced-payment-management`;
  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public downloadFile(attachmentId: number): Observable<{ blob: Blob; fileName: string }> {
    const requestOptions = {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    };

    return this.http.get<HttpResponse<any>>(`${this.baseUrl}/attachments/${attachmentId}`, requestOptions).pipe(
      map(response => {
        const contentDispositionHeader: string = response.headers.get('Content-Disposition');
        const fileName: string = contentDispositionHeader.match(/filename="(.*)"/)[1];
        const blob = new Blob([response.body], { type: response.headers.get('Content-Type') });

        return { blob, fileName };
      })
    );
  }

  public uploadFile(): Observable<void> {
    return EMPTY;
  }

  public deleteFile(input: DeleteFileRequest): Observable<void> {
    return this.http.delete<void>(
      `${this.baseUrl}/advanced-payments/${input.advancedPaymentId}/attachments/${input.attachmentId}`
    );
  }

  public assignAttachment(input: AttachFileRequest): Observable<AttachFileResponse> {
    return this.http.post<AttachFileResponse>(
      `${this.baseUrl}/advanced-payments/${input.advancedPaymentId}/attachments`,
      input.attachmentId
    );
  }
}
