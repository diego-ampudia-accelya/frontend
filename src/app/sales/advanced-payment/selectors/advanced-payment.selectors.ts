import { createFeatureSelector, createSelector } from '@ngrx/store';

import { AdvancedPaymentState, advancedPaymentStateFeatureKey } from '../store/advanced-payment.state';

export const selectAdvancedPaymentFeature = createFeatureSelector<AdvancedPaymentState>(advancedPaymentStateFeatureKey);

export const selectAdvancedPayment = createSelector(selectAdvancedPaymentFeature, state => state.advancedPayment);
