import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AdvancedPaymentFilters, AdvancedPaymentStatus } from '../models/advanced-payment.model';

import { AdvancedPaymentFilterFormatter } from './advanced-payment-formatter';
import { AgentSummary } from '~app/shared/models';

describe('AdvancedPaymentFilterFormatter', () => {
  let formatter: AdvancedPaymentFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new AdvancedPaymentFilterFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format agents if they exist', () => {
    const filter: AdvancedPaymentFilters = {
      agent: { id: '1', name: 'AGENT 78200001', code: '78200001' } as AgentSummary
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agent'],
        label: 'sales.advancedPayment.filters.agentCode - 78200001'
      }
    ]);
  });

  it('should format requestNumber if they exist', () => {
    const filter: AdvancedPaymentFilters = {
      requestNumber: 12345
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['requestNumber'],
        label: 'sales.advancedPayment.filters.requestNumber - 12345'
      }
    ]);
  });

  it('should format status if they exist', () => {
    const filter: AdvancedPaymentFilters = {
      status: AdvancedPaymentStatus.approved
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['status'],
        label: 'sales.advancedPayment.filters.status - Approved'
      }
    ]);
  });

  it('should format amountRange if they exist', () => {
    const filter: AdvancedPaymentFilters = {
      amountRange: [2, 3]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['amountRange'],
        label: 'sales.advancedPayment.filters.amount: 2 - 3'
      }
    ]);
  });

  it('should format request date if they exist with or without range', () => {
    // Date range has been specified
    const filterWithRequestDateRange: AdvancedPaymentFilters = {
      requestDateRange: [new Date('2022/07/14'), new Date('2022/07/28')]
    };
    // Unique date has been specified
    const filterWithUniqueRequestDate: AdvancedPaymentFilters = {
      requestDateRange: [new Date('2022/07/14')]
    };

    expect(formatter.format(filterWithRequestDateRange)).toEqual([
      {
        keys: ['requestDateRange'],
        label: 'sales.advancedPayment.filters.requestDate - 14/07/2022 - 28/07/2022'
      }
    ]);

    expect(formatter.format(filterWithUniqueRequestDate)).toEqual([
      {
        keys: ['requestDateRange'],
        label: 'sales.advancedPayment.filters.requestDate - 14/07/2022'
      }
    ]);
  });

  it('should format review date if they exist with or without range', () => {
    // Date range has been specified
    const filterWithReviewDateRange: AdvancedPaymentFilters = {
      reviewDateRange: [new Date('2022/07/14'), new Date('2022/07/28')]
    };
    // Unique date has been specified
    const filterWithUniqueReviewDate: AdvancedPaymentFilters = {
      reviewDateRange: [new Date('2022/07/14')]
    };

    expect(formatter.format(filterWithReviewDateRange)).toEqual([
      {
        keys: ['reviewDateRange'],
        label: 'sales.advancedPayment.filters.reviewDate - 14/07/2022 - 28/07/2022'
      }
    ]);

    expect(formatter.format(filterWithUniqueReviewDate)).toEqual([
      {
        keys: ['reviewDateRange'],
        label: 'sales.advancedPayment.filters.reviewDate - 14/07/2022'
      }
    ]);
  });

  it('should format reviewComment if they exist', () => {
    const filter: AdvancedPaymentFilters = {
      reviewComment: 'Comment about payment'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['reviewComment'],
        label: 'sales.advancedPayment.filters.reviewComment - Comment about payment'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: AdvancedPaymentFilters = {
      agent: null,
      requestNumber: null
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      requestNumber: 12345,
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['requestNumber'],
        label: 'sales.advancedPayment.filters.requestNumber - 12345'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
