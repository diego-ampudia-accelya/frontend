import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { AdvancedPaymentFilters, AdvancedPaymentStatus } from '../models/advanced-payment.model';
import { AdvancedPaymentService } from './advanced-payment.service';
import { DataQuery } from '~app/shared/components/list-view';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

const filterByQuery: AdvancedPaymentFilters = {
  agent: {
    id: '1234',
    name: 'AgentName',
    code: '123456'
  },
  requestNumber: 1234567,
  status: AdvancedPaymentStatus.approved,
  amountRange: [2, 3],
  requestDateRange: [new Date('2022/07/14'), new Date('2022/07/28')],
  reviewDateRange: [new Date('2022/07/20'), new Date('2022/07/28')],
  reviewComment: 'Comment'
};

describe('AdvancedPaymentService', () => {
  let advancedPaymentService: AdvancedPaymentService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of(httpResponse));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        AdvancedPaymentService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ]
    });

    advancedPaymentService = TestBed.inject(AdvancedPaymentService);
  });

  it('should create', () => {
    expect(advancedPaymentService).toBeDefined();
  });

  it('should send proper find request', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    const expectedUrl =
      '/advanced-payment-management/advanced-payments?page=0&size=20&agentId=1234&amountFrom=2&amountTo=3&requestDateFrom=2022-07-14&requestDateTo=2022-07-28&reviewDateFrom=2022-07-20&reviewDateTo=2022-07-28&requestNumber=1234567&status=Approved&reviewComment=Comment';

    advancedPaymentService.find(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl =
      '/advanced-payment-management/advanced-payments/download?agentId=1234&amountFrom=2&amountTo=3&requestDateFrom=2022-07-14&requestDateTo=2022-07-28&reviewDateFrom=2022-07-20&reviewDateTo=2022-07-28&requestNumber=1234567&status=Approved&reviewComment=Comment&exportAs=TXT';

    advancedPaymentService.download(query, exportOptions).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
