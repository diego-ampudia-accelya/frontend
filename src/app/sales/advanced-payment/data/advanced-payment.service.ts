import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdvancedPaymentRequest } from '../models/advanced-payment-request.model';
import { AdvancedPaymentView } from '../models/advanced-payment-view.model';
import { AdvancedPayment, AdvancedPaymentFilters, AdvancedPaymentFiltersBE } from '../models/advanced-payment.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable()
export class AdvancedPaymentService implements Queryable<AdvancedPayment> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/advanced-payment-management/advanced-payments`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<AdvancedPaymentFilters>): Observable<PagedData<AdvancedPayment>> {
    const formattedQuery = this.formatQuery(query);

    return this.http.get<PagedData<AdvancedPayment>>(this.baseUrl + formattedQuery.getQueryString());
  }

  public getOne(id: string): Observable<AdvancedPaymentView> {
    return this.http.get<AdvancedPaymentView>(`${this.baseUrl}/${id}`);
  }

  public create(request: AdvancedPaymentRequest): Observable<AdvancedPaymentView> {
    return this.http.post<AdvancedPaymentView>(this.baseUrl, request);
  }

  public download(
    query: DataQuery<AdvancedPaymentFilters>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions.toUpperCase() };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<AdvancedPaymentFilters>>): RequestQuery<AdvancedPaymentFiltersBE> {
    const { bsp, agent, amountRange, requestDateRange, reviewDateRange, ...rest } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        bspId: bsp && bsp.id,
        agentId: agent && agent.id,
        amountFrom: amountRange && amountRange[0],
        amountTo: amountRange && amountRange[1],
        requestDateFrom: requestDateRange && toShortIsoDate(requestDateRange[0]),
        requestDateTo:
          requestDateRange && toShortIsoDate(requestDateRange[1] ? requestDateRange[1] : requestDateRange[0]),
        reviewDateFrom: reviewDateRange && toShortIsoDate(reviewDateRange[0]),
        reviewDateTo: reviewDateRange && toShortIsoDate(reviewDateRange[1] ? reviewDateRange[1] : reviewDateRange[0]),
        ...rest
      }
    });
  }
}
