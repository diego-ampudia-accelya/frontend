import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AdvancedPaymentFilters } from '../models/advanced-payment.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper, rangeFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AdvancedPaymentFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: AdvancedPaymentFilters): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<AdvancedPaymentFilters>> = {
      agent: agent => `${this.translate('agentCode')} - ${agent.code}`,
      requestNumber: requestNumber => `${this.translate('requestNumber')} - ${requestNumber}`,
      status: status => `${this.translate('status')} - ${status}`,
      amountRange: amountRange => `${this.translate('amount')}: ${rangeFilterTagMapper(amountRange)}`,
      requestDateRange: requestDateRange =>
        `${this.translate('requestDate')} - ${rangeDateFilterTagMapper(requestDateRange)}`,
      reviewDateRange: reviewDateRange =>
        `${this.translate('reviewDate')} - ${rangeDateFilterTagMapper(reviewDateRange)}`,
      reviewComment: reviewComment => `${this.translate('reviewComment')} - ${reviewComment}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`sales.advancedPayment.filters.${key}`);
  }
}
