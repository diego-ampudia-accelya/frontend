import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdvancedPaymentView } from '../models/advanced-payment-view.model';
import { AdvancedPaymentStatus } from '../models/advanced-payment.model';
import { AdvancedPaymentStoreFacadeService } from '../store/advanced-payment-store-facade.service';
import { User, UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-advanced-payment-view',
  templateUrl: './advanced-payment-view.component.html',
  styleUrls: ['./advanced-payment-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdvancedPaymentViewComponent implements OnInit {
  public advancedPayment$: Observable<AdvancedPaymentView>;
  public user$: Observable<User>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private advancedPaymentStoreFacadeService: AdvancedPaymentStoreFacadeService
  ) {}

  public get isIATAUser$(): Observable<boolean> {
    return this.user$.pipe(map(u => u?.userType === UserType.IATA));
  }

  public get isUploadButtonDisabled$(): Observable<boolean> {
    return this.advancedPayment$.pipe(
      map(
        advancedPayment =>
          advancedPayment?.attachments?.length >= 3 || advancedPayment?.status !== AdvancedPaymentStatus.submitted
      )
    );
  }

  public get isActionsDisabled$(): Observable<boolean> {
    return this.advancedPayment$.pipe(
      map(advancedPayment => advancedPayment?.status !== AdvancedPaymentStatus.submitted)
    );
  }

  public ngOnInit(): void {
    const {
      data: { item }
    } = this.activatedRoute.snapshot;

    this.advancedPaymentStoreFacadeService.actions.setAdvancedPayment(item);
    this.advancedPayment$ = this.advancedPaymentStoreFacadeService.selectors.data.advancedPayment$;
    this.user$ = this.advancedPaymentStoreFacadeService.selectors.data.user$;
  }

  public handleUploadAttachment(advancedPaymentId: number): void {
    this.advancedPaymentStoreFacadeService.actions.uploadAttachmentOpenDialog(advancedPaymentId);
  }

  public handleDeleteAttachment(advancedPaymentId: number, attachmentId: number): void {
    this.advancedPaymentStoreFacadeService.actions.deleteAttachment({
      advancedPaymentId,
      attachmentId
    });
  }

  public handleDownloadAttachment(attachmentId: number): void {
    this.advancedPaymentStoreFacadeService.actions.downloadAttachment({ attachmentId });
  }
}
