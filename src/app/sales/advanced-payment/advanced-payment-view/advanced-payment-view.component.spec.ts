import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { MockComponent } from 'ng-mocks';
import { of } from 'rxjs';

import { AdvancedPaymentStoreFacadeService } from '../store/advanced-payment-store-facade.service';
import { AdvancedPaymentViewComponent } from './advanced-payment-view.component';
import { PropertyComponent, PropertyListComponent } from '~app/shared/components';
import { TranslatePipeMock } from '~app/test';

const activatedRouteStub = {
  snapshot: {
    data: {
      item: {}
    }
  }
};

describe('AdvancedPaymentViewComponent', () => {
  let component: AdvancedPaymentViewComponent;
  let fixture: ComponentFixture<AdvancedPaymentViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AdvancedPaymentViewComponent,
        TranslatePipeMock,
        MockComponent(PropertyComponent),
        MockComponent(PropertyListComponent)
      ],
      imports: [],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        {
          provide: AdvancedPaymentStoreFacadeService,
          useValue: {
            actions: {
              uploadAttachmentOpenDialog: () => {},
              deleteAttachment: () => {},
              downloadAttachment: () => {},
              setAdvancedPayment: () => {}
            },
            selectors: {
              data: {
                advancedPayment$: of(null),
                user$: of(null)
              }
            }
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedPaymentViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
