import { Component, OnInit, ViewChild } from '@angular/core';
import { filter, map } from 'rxjs/operators';

import { AdvancedPaymentStoreFacadeService } from '../store/advanced-payment-store-facade.service';
import { UploadDialogAction } from '~app/files/model';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { FileValidators, MAX_FILE_SIZE_10 } from '~app/shared/components/upload/file-validators';
import { UploadFileType } from '~app/shared/components/upload/upload-filetype.config';
import { UploadModes, UploadType } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { appConfiguration } from '~app/shared/services';

@Component({
  selector: 'bspl-advanced-payment-upload-dialog',
  templateUrl: './advanced-payment-upload-dialog.component.html',
  styleUrls: ['./advanced-payment-upload-dialog.component.scss']
})
export class AdvancedPaymentUploadDialogComponent implements OnInit {
  @ViewChild(UploadComponent) uploadComponentRef: UploadComponent;
  public canAddFiles = true;
  public hasUploaded = false;
  public uploadMode = UploadModes.Preview;
  public uploadPath = `${appConfiguration.baseUploadPath}/advanced-payment-management/attachments`;
  public uploadFileTypes = [
    UploadFileType.bmp,
    UploadFileType.gif,
    UploadFileType.jpg,
    UploadFileType.jpeg,
    UploadFileType.png,
    UploadFileType.pdf,
    UploadFileType.zip
  ];
  public uploadValidationRules = [FileValidators.maxFileSize(MAX_FILE_SIZE_10)];

  public files: File[] = [];

  constructor(
    public config: DialogConfig<DialogConfig>,
    private reactiveSubject: ReactiveSubject,
    private advancedPaymentStoreFacadeService: AdvancedPaymentStoreFacadeService
  ) {}

  public ngOnInit() {
    this.reactiveSubject.asObservable
      .pipe(
        map(({ clickedBtn }) => clickedBtn),
        filter(
          (clickedButton: FooterButton) =>
            clickedButton === FooterButton.Upload || clickedButton === FooterButton.Cancel
        ),
        map(action => {
          if (action === FooterButton.Upload) {
            this.uploadComponentRef.uploadFiles();
          }

          if (action === FooterButton.Cancel) {
            this.uploadComponentRef.pauseUpload();
            this.config.data.isClosable = true;
          }
        })
      )
      .subscribe();
  }

  public handleDropedFile(droppedFiles: File[]): void {
    this.files = [...this.files, ...droppedFiles];
  }

  public handleUploadFinished(files: FileUpload[]): void {
    if (!Array.isArray(files) || !files.length) {
      return;
    }

    this.hasUploaded = true;

    this.setButtonLoading(FooterButton.Upload, false);
    this.config.data.isClosable = true;
    this.files = [];
  }

  public handleUploadStarted(event: UploadType): void {
    if (event === UploadType.Retry) {
      this.uploadComponentRef.resetToInitialMode();
    }

    this.canAddFiles = false;
    this.config.data.isClosable = false;

    this.setButtonLoading(FooterButton.Upload, true);
    this.setButtonDisabled(FooterButton.Upload, true);
  }

  public handleResetInitialMode(): void {
    this.config.data.buttons = [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: FooterButton.Cancel
      },
      {
        title: 'BUTTON.DEFAULT.UPLOAD',
        buttonDesign: 'primary',
        type: FooterButton.Upload
      }
    ];

    this.reactiveSubject.emit({
      reset: UploadDialogAction.Reset
    });

    this.canAddFiles = true;
  }

  public handleFilesChange($event: File[]): void {
    this.files = $event;
  }

  public handleUploadFinishedResult($event: FileUpload[]): void {
    for (const attachment of $event) {
      const { responseData } = attachment;
      setTimeout(
        () =>
          this.advancedPaymentStoreFacadeService.actions.assignAttachment({
            advancedPaymentId: this.config.data.advancedPaymentId,
            attachmentId: responseData.id
          }),
        0.5 * 1000
      );
    }
  }

  public handleModeChange(mode: UploadModes): void {
    this.uploadMode = mode;
  }

  private getButton(buttonType: FooterButton): any {
    return this.config.data.buttons.find(button => button.type === buttonType);
  }

  private setButtonDisabled(buttonType: FooterButton, isDisabled: boolean) {
    const actionButton = this.getButton(buttonType);
    if (actionButton) {
      actionButton.isDisabled = isDisabled;
    }
  }

  private setButtonLoading(buttonType: FooterButton, isLoading: boolean) {
    const actionButton = this.getButton(buttonType);
    if (actionButton) {
      actionButton.isLoading = isLoading;
    }
  }
}
