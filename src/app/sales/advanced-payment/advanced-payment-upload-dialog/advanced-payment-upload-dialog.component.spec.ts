/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AdvancedPaymentStoreFacadeService } from '../store/advanced-payment-store-facade.service';
import { AdvancedPaymentUploadDialogComponent } from './advanced-payment-upload-dialog.component';
import { DialogConfig } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TranslatePipeMock } from '~app/test';

describe('AdvancedPaymentUploadDialogComponent', () => {
  let component: AdvancedPaymentUploadDialogComponent;
  let fixture: ComponentFixture<AdvancedPaymentUploadDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdvancedPaymentUploadDialogComponent, TranslatePipeMock],
      providers: [
        {
          provide: ReactiveSubject,
          useValue: {
            asObservable: of({})
          }
        },
        {
          provide: AdvancedPaymentStoreFacadeService,
          useValue: {
            actions: {
              assignAttachment: () => {}
            }
          }
        },
        {
          provide: DialogConfig,
          useValue: {
            data: {
              buttons: [],
              advancedPaymentId: -1
            }
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedPaymentUploadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
