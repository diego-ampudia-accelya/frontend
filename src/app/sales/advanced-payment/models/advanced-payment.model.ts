import { AgentSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';

export interface AdvancedPayment {
  id?: number;
  bsp?: BspDto;
  agent?: AgentDto;
  requestNumber?: number;
  status?: AdvancedPaymentStatus;
  amount?: number;
  requestDate?: string;
  reviewDate?: string;
  reviewComment?: string;
  subject?: string;
  description?: string;
}

export interface AdvancedPaymentFilters {
  id?: number;
  bsp?: BspDto;
  agent?: AgentSummary;
  requestNumber?: number;
  status?: AdvancedPaymentStatus;
  amountRange?: number[];
  requestDateRange?: Date[];
  reviewDateRange?: Date[];
  reviewComment?: string;
}

export interface AdvancedPaymentFiltersBE {
  bspId?: number;
  agentId?: string;
  requestNumber?: number;
  status?: AdvancedPaymentStatus;
  amountFrom?: number;
  amountTo?: number;
  requestDateFrom?: string;
  requestDateTo?: string;
  reviewDateFrom?: string;
  reviewDateTo?: string;
  reviewComment?: string;
}

export interface AgentDto {
  id: number;
  iataCode: string;
  name: string;
}

export enum AdvancedPaymentStatus {
  submitted = 'Submitted',
  rejected = 'Rejected',
  approved = 'Approved'
}

export interface FileUploadResponse {
  id: number;
}

export interface DeleteFileRequest {
  advancedPaymentId: number;
  attachmentId: number;
}

export interface DownloadFileRequest {
  attachmentId: number;
}

export interface AttachFileRequest {
  advancedPaymentId: number;
  attachmentId: number;
}

export interface AttachFileResponse {
  id: number;
  name: string;
  size: number;
}

export interface AdvancedPaymentUploadDialogConfig {
  advancedPaymentId: number;
}
