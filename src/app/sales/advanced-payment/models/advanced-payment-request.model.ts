export interface AdvancedPaymentRequest {
  amount: number;
  subject: string;
  description: string;
  attachments: string[];
}
