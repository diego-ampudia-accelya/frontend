import { AdvancedPaymentStatus, AgentDto } from './advanced-payment.model';
import { BspDto } from '~app/shared/models/bsp.model';

export interface AdvancedPaymentAttachment {
  id: number;
  name: string;
}

export interface AdvancedPaymentView {
  id: number;
  bsp: BspDto;
  agent: AgentDto;
  requestNumber: number;
  status: AdvancedPaymentStatus;
  amount: number;
  requestDate: string;
  reviewDate: string;
  reviewComment: string;
  subject: string;
  description: string;
  attachments: AdvancedPaymentAttachment[];
  type: string;
}
