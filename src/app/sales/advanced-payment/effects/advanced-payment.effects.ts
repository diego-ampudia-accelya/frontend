import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';

import * as fromActions from '../actions/advanced-payment.actions';
import { AdvancedPaymentUploadDialogComponent } from '../advanced-payment-upload-dialog/advanced-payment-upload-dialog.component';
import { AdvancedPaymentService as AdvancedPaymentQueryService } from '../data/advanced-payment.service';
import { AdvancedPaymentService } from '../services/advanced-payment.service';
import { AdvancedPaymentStoreFacadeService } from '../store/advanced-payment-store-facade.service';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class AdvancedPaymentEffects {
  public constructor(
    private readonly actions$: Actions,
    private readonly advancedPaymentService: AdvancedPaymentService,
    private readonly notificationService: NotificationService,
    private readonly translationService: L10nTranslationService,
    private readonly dialogService: DialogService,
    private readonly advancedPaymentQueryService: AdvancedPaymentQueryService,
    private readonly advancedPaymentStoreFacadeService: AdvancedPaymentStoreFacadeService
  ) {}

  public downloadAdvancedPaymentAttachment$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.downloadFileRequest>>(fromActions.downloadFileRequest),
      switchMap(({ payload }) =>
        this.advancedPaymentService.downloadFile(payload.attachmentId).pipe(
          tap(({ blob, fileName }) => FileSaver.saveAs(blob, fileName)),
          map(() => fromActions.downloadFileSuccess()),
          tap(() => {
            const successMessage = this.translationService.translate(
              'sales.advancedPayment.detail.download.message.success'
            );
            this.notificationService.showSuccess(successMessage);
          }),
          catchError((error: HttpErrorResponse) => {
            const errorMessage = this.translationService.translate(
              'sales.advancedPayment.detail.download.message.failure'
            );
            this.notificationService.showError(errorMessage);

            return of(fromActions.downloadFileFailure({ payload: { error } }));
          })
        )
      )
    )
  );

  public attachAdvancedPaymentAttachment$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.assignAttachmentRequest>>(fromActions.assignAttachmentRequest),
      mergeMap(({ payload }) =>
        this.advancedPaymentService.assignAttachment(payload).pipe(
          map(() => fromActions.assignAttachmentSuccess()),
          tap(() => {
            const successMessage = this.translationService.translate(
              'sales.advancedPayment.detail.attach.message.success'
            );
            this.notificationService.showSuccess(successMessage);
          }),
          catchError((error: HttpErrorResponse) => {
            const errorMessage = this.translationService.translate(
              'sales.advancedPayment.detail.attach.message.failure'
            );
            this.notificationService.showError(errorMessage);

            return of(fromActions.assignAttachmentFailure({ payload: { error } }));
          })
        )
      )
    )
  );

  public uploadAdvancedPaymentAttachmentOpenDialog$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.uploadAttachmentOpenDialogRequest>>(
          fromActions.uploadAttachmentOpenDialogRequest
        ),
        tap(({ payload: { advancedPaymentId } }) =>
          this.dialogService
            .open(AdvancedPaymentUploadDialogComponent, {
              data: {
                title: this.translationService.translate('sales.advancedPayment.detail.dialog.upload.title'),
                footerButtonsType: FooterButton.Upload,
                hasCancelButton: true,
                buttons: [
                  {
                    title: 'BUTTON.DEFAULT.CANCEL',
                    buttonDesign: 'tertiary',
                    type: FooterButton.Cancel
                  },
                  {
                    title: 'BUTTON.DEFAULT.UPLOAD',
                    buttonDesign: 'primary',
                    type: FooterButton.Upload
                  }
                ],
                isClosable: true,
                advancedPaymentId
              }
            })
            .subscribe(({ contentComponentRef }) => {
              if (contentComponentRef) {
                const {
                  hasUploaded,
                  config: {
                    data: { advancedPaymentId: id }
                  }
                } = contentComponentRef;

                if (hasUploaded) {
                  this.advancedPaymentStoreFacadeService.actions.fetchAdvancedPayment(id);
                }
              }
            })
        )
      ),
    { dispatch: false }
  );

  public deleteAdvancedPaymentAttachment$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.deleteAttachmentRequest>>(fromActions.deleteAttachmentRequest),
      switchMap(({ payload }) =>
        this.advancedPaymentService.deleteFile(payload).pipe(
          switchMap(() => {
            const successMessage = this.translationService.translate(
              'sales.advancedPayment.detail.delete.message.success'
            );
            this.notificationService.showSuccess(successMessage);

            return [
              fromActions.deleteAttachmentSuccess(),
              fromActions.fetchAdvancedPaymentRequest({
                payload: {
                  advancedPaymentId: payload.advancedPaymentId.toString()
                }
              })
            ];
          }),
          rethrowError(error => fromActions.deleteAttachmentFailure({ payload: { error } }))
        )
      )
    )
  );

  public fetchAdvancedPaymentEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.fetchAdvancedPaymentRequest>>(fromActions.fetchAdvancedPaymentRequest),
      switchMap(({ payload: { advancedPaymentId } }) =>
        this.advancedPaymentQueryService.getOne(advancedPaymentId).pipe(
          map(advancedPayment => fromActions.fetchAdvancedPaymentSuccess({ payload: { advancedPayment } })),
          rethrowError(error => fromActions.fetchAdvancedPaymentFailure({ payload: { error } }))
        )
      )
    )
  );
}
