import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/advanced-payment.actions';
import { initialState } from '../store/advanced-payment.state';

export const advancedPaymentReducer = createReducer(
  initialState,
  on(fromActions.setAdvancedPayment, (state, { payload: { advancedPayment } }) => ({
    ...state,
    advancedPayment
  })),

  on(fromActions.fetchAdvancedPaymentRequest, state => ({
    ...state,
    loading: {
      ...state.loading,
      fetchAdvancedPayment: true
    },
    errors: {
      ...state.error,
      fetchAdvancedPayment: null
    }
  })),
  on(fromActions.fetchAdvancedPaymentFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      fetchAdvancedPayment: false
    },
    errors: {
      ...state.error,
      fetchAdvancedPayment: error
    }
  })),
  on(fromActions.fetchAdvancedPaymentSuccess, (state, { payload: { advancedPayment } }) => ({
    ...state,
    advancedPayment,
    loading: {
      ...state.loading,
      fetchAdvancedPayment: false
    },
    errors: {
      ...state.error,
      fetchAdvancedPayment: null
    }
  }))
);
