import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/advanced-payment.actions';
import { AdvancedPaymentView } from '../models/advanced-payment-view.model';
import { AttachFileRequest, DeleteFileRequest, DownloadFileRequest } from '../models/advanced-payment.model';
import * as fromSelectors from '../selectors/advanced-payment.selectors';
import { AppState } from '~app/reducers';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable({
  providedIn: 'root'
})
export class AdvancedPaymentStoreFacadeService {
  constructor(private store: Store<AppState>) {}

  public get actions() {
    return {
      setAdvancedPayment: (advancedPayment: AdvancedPaymentView) =>
        this.store.dispatch(fromActions.setAdvancedPayment({ payload: { advancedPayment } })),
      fetchAdvancedPayment: (advancedPaymentId: string) =>
        this.store.dispatch(fromActions.fetchAdvancedPaymentRequest({ payload: { advancedPaymentId } })),
      downloadAttachment: (input: DownloadFileRequest) =>
        this.store.dispatch(fromActions.downloadFileRequest({ payload: input })),
      uploadAttachmentOpenDialog: (advancedPaymentId: number) =>
        this.store.dispatch(fromActions.uploadAttachmentOpenDialogRequest({ payload: { advancedPaymentId } })),
      deleteAttachment: (input: DeleteFileRequest) =>
        this.store.dispatch(fromActions.deleteAttachmentRequest({ payload: input })),
      assignAttachment: (input: AttachFileRequest) =>
        this.store.dispatch(fromActions.assignAttachmentRequest({ payload: input }))
    };
  }

  public get selectors() {
    return {
      data: {
        advancedPayment$: this.store.select(fromSelectors.selectAdvancedPayment),
        user$: this.store.select(fromAuth.getUser)
      },
      error: {},
      loading: {}
    };
  }
}
