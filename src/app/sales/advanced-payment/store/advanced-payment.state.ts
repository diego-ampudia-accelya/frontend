import { HttpErrorResponse } from '@angular/common/http';

import { AdvancedPaymentView } from '../models/advanced-payment-view.model';

export const advancedPaymentStateFeatureKey = 'advancedPaymentStateFeatureKey';

export interface AdvancedPaymentState {
  advancedPayment: AdvancedPaymentView | null;
  loading: {
    downloadFile: boolean;
    deleteFile: boolean;
    uploadFile: boolean;
    fetchAdvancedPayment: boolean;
  };
  error: {
    downloadFile: HttpErrorResponse | null;
    deleteFile: HttpErrorResponse | null;
    uploadFile: HttpErrorResponse | null;
    fetchAdvancedPayment: HttpErrorResponse | null;
  };
}

export const initialState: AdvancedPaymentState = {
  advancedPayment: null,
  loading: {
    downloadFile: false,
    deleteFile: false,
    uploadFile: false,
    fetchAdvancedPayment: false
  },
  error: {
    downloadFile: null,
    deleteFile: null,
    uploadFile: null,
    fetchAdvancedPayment: null
  }
};
