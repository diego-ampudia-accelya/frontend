/* eslint-disable @typescript-eslint/no-unused-vars */

import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { AdvancedPaymentStoreFacadeService } from './advanced-payment-store-facade.service';
import { advancedPaymentStateFeatureKey, initialState } from './advanced-payment.state';

describe('AdvancedPaymentStoreFacadeService', () => {
  let service: AdvancedPaymentStoreFacadeService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AdvancedPaymentStoreFacadeService,
        provideMockStore({
          initialState: {
            [advancedPaymentStateFeatureKey]: initialState
          }
        })
      ]
    });

    service = TestBed.inject<AdvancedPaymentStoreFacadeService>(AdvancedPaymentStoreFacadeService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
