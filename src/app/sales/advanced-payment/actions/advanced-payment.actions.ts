import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import { AdvancedPaymentView } from '../models/advanced-payment-view.model';
import {
  AttachFileRequest,
  DeleteFileRequest,
  DownloadFileRequest,
  FileUploadResponse
} from '../models/advanced-payment.model';

export const downloadFileRequest = createAction(
  '[Advanced Payment]Download File Request',
  props<{ payload: DownloadFileRequest }>()
);
export const downloadFileSuccess = createAction('[Advanced Payment]Download File Success');
export const downloadFileFailure = createAction(
  '[Advanced Payment]Download File Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const uploadAttachmentOpenDialogRequest = createAction(
  '[Advanced Payment]Upload File Request',
  props<{ payload: { advancedPaymentId: number } }>()
);
export const uploadAttachmentOpenDialogSuccess = createAction(
  '[Advanced Payment]Upload File Success',
  props<{ payload: FileUploadResponse }>()
);
export const uploadAttachmentOpenDialogFailure = createAction(
  '[Advanced Payment]Upload File Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const deleteAttachmentRequest = createAction(
  '[Advanced Payment]Delete File Request',
  props<{ payload: DeleteFileRequest }>()
);
export const deleteAttachmentSuccess = createAction('[Advanced Payment]Delete File Success');
export const deleteAttachmentFailure = createAction(
  '[Advanced Payment]Delete File Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const assignAttachmentRequest = createAction(
  '[Advanced Payment]Attach File Request',
  props<{ payload: AttachFileRequest }>()
);
export const assignAttachmentSuccess = createAction('[Advanced Payment]Attach File Success');
export const assignAttachmentFailure = createAction(
  '[Advanced Payment]Attach File Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const fetchAdvancedPaymentRequest = createAction(
  '[Advanced Payment]Fetch Advanced Payment Request',
  props<{ payload: { advancedPaymentId: string } }>()
);
export const fetchAdvancedPaymentSuccess = createAction(
  '[Advanced Payment]Fetch Advanced Payment Success',
  props<{ payload: { advancedPayment: AdvancedPaymentView } }>()
);
export const fetchAdvancedPaymentFailure = createAction(
  '[Advanced Payment]Fetch Advanced Payment Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const setAdvancedPayment = createAction(
  '[Advanced Payment]Set Advanced Payment',
  props<{ payload: { advancedPayment: AdvancedPaymentView } }>()
);
