import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AbstractControl, FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { AdvancedPaymentService } from '../data/advanced-payment.service';
import { AdvancedPaymentRequest } from '../models/advanced-payment-request.model';
import { AdvancedPaymentRequestComponent } from './advanced-payment-request.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { ROUTES } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { AgentUser, User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, NotificationService, TabService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('AdvancedPaymentRequestComponent', () => {
  let component: AdvancedPaymentRequestComponent;
  let fixture: ComponentFixture<AdvancedPaymentRequestComponent>;
  let mockStore: MockStore;

  const agentUser: Partial<AgentUser> = {
    userType: UserType.AGENT,
    agent: { id: 1, name: null, bsp: null, effectiveFrom: null, iataCode: '1234' }
  };

  const initialState = {
    router: null,
    auth: {
      user: agentUser
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdvancedPaymentRequestComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        FormBuilder,
        mockProvider(AgentDictionaryService),
        mockProvider(TabService),
        mockProvider(AdvancedPaymentService),
        mockProvider(L10nTranslationService),
        mockProvider(NotificationService),
        provideMockStore({ initialState })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedPaymentRequestComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set correct fields as required', () => {
    const controls: { [key in keyof Partial<AdvancedPaymentRequest>]: AbstractControl } = component.form.controls;
    const { description, subject, amount } = controls;

    description.setValue(null);
    expect(description.invalid).toBe(true);
    subject.setValue(null);
    expect(subject.invalid).toBe(true);
    amount.setValue(null);
    expect(amount.invalid).toBe(true);

    expect(component.uploadRequired).toBe(true);
  });

  it('should show errors before creating request', () => {
    spyOn(FormUtil, 'showControlState');

    component.createRequest();

    expect(component.form.invalid).toBe(true);
    expect(FormUtil.showControlState).toHaveBeenCalled();
  });

  it('should redirect back to query when discarding changes', () => {
    const queryUrl = ROUTES.ADVANCED_PAYMENT.url;
    component.discardChanges();

    expect(TestBed.inject(TabService).closeCurrentTabAndNavigate).toHaveBeenCalledWith(queryUrl);
  });

  it('should lock agent dropdown when user type is agent', () => {
    mockStore.overrideSelector(getUser, agentUser as User);
    component['initializeAgentDropdown']();

    expect(component.isAgentDropdownLocked).toBe(true);
  });

  it('should show current agent code when user type is agent', () => {
    mockStore.overrideSelector(getUser, agentUser as User);
    component['initializeAgentDropdown']();

    expect(component['agentControl'].value).toBe(agentUser.agent);
  });
});
