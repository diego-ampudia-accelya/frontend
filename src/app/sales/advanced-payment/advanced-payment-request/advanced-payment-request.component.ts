import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, Observable, of, Subject } from 'rxjs';
import { finalize, first, switchMap } from 'rxjs/operators';

import { AdvancedPaymentService } from '../data/advanced-payment.service';
import { AdvancedPaymentRequest } from '../models/advanced-payment-request.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FileValidator, FileValidators, MAX_FILE_SIZE_10 } from '~app/shared/components/upload/file-validators';
import { UploadFileType } from '~app/shared/components/upload/upload-filetype.config';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { ROUTES } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, appConfiguration, NotificationService, TabService } from '~app/shared/services';

@Component({
  selector: 'bspl-advanced-payment-request',
  templateUrl: './advanced-payment-request.component.html',
  styleUrls: ['./advanced-payment-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdvancedPaymentRequestComponent implements OnInit, OnDestroy {
  @ViewChild(UploadComponent) uploadComponent: UploadComponent;

  public form: FormGroup;
  public agentControl: FormControl;

  public uploadMode = UploadModes.Preview;
  public uploadPath: string;
  public uploadRequired: boolean;
  public uploadFileTypes: UploadFileType[];
  public uploadValidationRules: FileValidator[];

  public buttonDesign = ButtonDesign;
  public agentOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public isAgentDropdownLocked = false;
  public isCreateButtonLoading: boolean;

  private formUtil: FormUtil;

  private destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private tabService: TabService,
    private agentDictionaryService: AgentDictionaryService,
    private advancedPaymentService: AdvancedPaymentService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private cd: ChangeDetectorRef
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.initializeUploadComponent();
    this.buildForm();
    this.initializeAgentDropdown();
  }

  public createRequest(): void {
    if (this.form.invalid) {
      FormUtil.showControlState(this.form);
    } else {
      // Submission
      this.isCreateButtonLoading = true;

      this.uploadComponent
        .uploadFiles()
        .pipe(
          switchMap(allValidFilesAreUploaded =>
            allValidFilesAreUploaded ? this.advancedPaymentService.create(this.form.value) : EMPTY
          ),
          finalize(() => {
            this.isCreateButtonLoading = false;
            this.cd.markForCheck();
          })
        )
        .subscribe(
          response => {
            this.tabService.closeCurrentTabAndNavigate(`${ROUTES.ADVANCED_PAYMENT_VIEW.url}/${response.id}`);
            const message = this.translationService.translate('sales.advancedPayment.successMessage');
            this.notificationService.showSuccess(message);
          },
          (response: HttpErrorResponse) => {
            if (response.status === 400) {
              FormUtil.processErrorsOnForm(response.error, this.form);
            }
            const message = this.translationService.translate('sales.advancedPayment.errorMessage');
            this.notificationService.showError(message);
          }
        );
    }
  }

  public discardChanges(): void {
    this.tabService.closeCurrentTabAndNavigate(ROUTES.ADVANCED_PAYMENT.url);
  }

  private initializeAgentDropdown(): void {
    this.store
      .select(getUser)
      .pipe(first())
      .subscribe(user => {
        if (user.userType === UserType.AGENT) {
          // Extract agent from current user
          const currentAgent = user.agent;
          const currentAgentOption: DropdownOption<Agent> = { label: currentAgent.iataCode, value: currentAgent };
          // Set agent in dropdown and lock it
          this.agentOptions$ = of([currentAgentOption]) as any;
          this.agentControl.reset(currentAgent);
          this.isAgentDropdownLocked = true;
        } else {
          // TODO Not part of this story
          this.agentOptions$ = this.agentDictionaryService.getDropdownOptions();
        }
      });
  }

  private buildForm(): void {
    this.agentControl = this.fb.control(null);

    this.form = this.formUtil.createGroup<AdvancedPaymentRequest>({
      description: [null, Validators.required],
      subject: [null, Validators.required],
      amount: [null, Validators.required],
      attachments: [null]
    });
  }

  private initializeUploadComponent(): void {
    this.uploadPath = `${appConfiguration.baseUploadPath}/advanced-payment-management/attachments`;
    this.uploadFileTypes = [
      UploadFileType.bmp,
      UploadFileType.gif,
      UploadFileType.jpg,
      UploadFileType.jpeg,
      UploadFileType.png,
      UploadFileType.pdf,
      UploadFileType.zip
    ];
    this.uploadRequired = true;
    this.uploadValidationRules = [FileValidators.maxFileSize(MAX_FILE_SIZE_10)];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
