import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents, MockProviders } from 'ng-mocks';

import { AdvancedPaymentFilterFormatter } from '../data/advanced-payment-formatter';
import { AdvancedPaymentService } from '../data/advanced-payment.service';
import { AdvancedPaymentListComponent } from './advanced-payment-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { QueryableDataSourceStub } from '~app/master-data/periods/shared/periods-mocks';
import {
  DialogService,
  GridTableComponent,
  InputComponent,
  PropertyComponent,
  PropertyListComponent,
  SelectComponent
} from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { DefaultQueryStorage, ListViewComponent, QueryableDataSource } from '~app/shared/components/list-view';
import { RangeDropdownComponent } from '~app/shared/components/range-dropdown/range-dropdown.component';
import { ROUTES } from '~app/shared/constants';
import { AgentUser, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, TabService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('AdvancedPaymentListComponent', () => {
  let component: AdvancedPaymentListComponent;
  let fixture: ComponentFixture<AdvancedPaymentListComponent>;
  let router: Router;

  const agentUser: Partial<AgentUser> = {
    userType: UserType.AGENT,
    agent: { id: 1, name: null, bsp: null, effectiveFrom: null, iataCode: '1234' }
  };

  const initialState = {
    router: null,
    auth: {
      user: agentUser
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AdvancedPaymentListComponent,
        TranslatePipeMock,
        MockComponents(
          ListViewComponent,
          InputComponent,
          SelectComponent,
          RangeDropdownComponent,
          DatepickerComponent,
          GridTableComponent,
          PropertyListComponent,
          PropertyComponent
        )
      ],
      providers: [
        FormBuilder,
        MockProviders(
          AgentDictionaryService,
          TabService,
          AdvancedPaymentFilterFormatter,
          DialogService,
          DefaultQueryStorage,
          L10nTranslationService,
          AgentDictionaryService,
          AdvancedPaymentService,
          PermissionsService
        ),
        provideMockStore({ initialState })
      ],
      imports: [RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(AdvancedPaymentListComponent, {
        set: {
          providers: [{ provide: QueryableDataSource, useClass: QueryableDataSourceStub }]
        }
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedPaymentListComponent);
    component = fixture.componentInstance;

    router = TestBed.inject<any>(Router);
    spyOn(router, 'navigate');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onRowLinkClick should navigate to payment view component', () => {
    component.onRowLinkClick({ id: 123 });

    expect(router.navigate).toHaveBeenCalledWith([ROUTES.ADVANCED_PAYMENT_VIEW.url, 123]);
  });
});
