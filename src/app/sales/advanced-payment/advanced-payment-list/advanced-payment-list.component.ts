import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, takeUntil } from 'rxjs/operators';

import { AdvancedPaymentFilterFormatter } from '../data/advanced-payment-formatter';
import { AdvancedPaymentService } from '../data/advanced-payment.service';
import { AdvancedPayment, AdvancedPaymentFilters, AdvancedPaymentStatus } from '../models/advanced-payment.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { Permissions } from '~app/shared/constants/permissions';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil, toValueLabelObject } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-advanced-payment-list',
  templateUrl: './advanced-payment-list.component.html',
  styleUrls: ['./advanced-payment-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    AdvancedPaymentFilterFormatter,
    { provide: QUERYABLE, useExisting: AdvancedPaymentService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [AdvancedPaymentService]
    }
  ]
})
export class AdvancedPaymentListComponent implements OnInit, OnDestroy {
  public customLabels = { create: 'sales.advancedPayment.requestButton' };
  public columns: GridColumn[] = [];
  public searchForm: FormGroup;

  public statusOptions: DropdownOption[] = [];
  public agentDropdownOptions$: Observable<DropdownOption[]> = of([]);

  public canRequestPayment = this.permissionService.hasPermission(Permissions.createAdvancePayment);

  public totalItemsMessage: string;
  public isAgentUser: boolean;

  private isAgentGroupUser: boolean;
  private loggedUserId: number;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private formFactory: FormUtil;
  private destroy$ = new Subject();

  constructor(
    public displayFilterFormatter: AdvancedPaymentFilterFormatter,
    public dataSource: QueryableDataSource<AdvancedPaymentFilters>,
    private dialogService: DialogService,
    private queryStorage: DefaultQueryStorage,
    private translationService: L10nTranslationService,
    private agentDictionaryService: AgentDictionaryService,
    private formBuilder: FormBuilder,
    private advancedPaymentService: AdvancedPaymentService,
    private permissionService: PermissionsService,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeLoggedUserType();
    this.initializeDropdowns();
    this.initializeTotalItems();
    this.initializeColumns();
    this.searchForm = this.buildSearchForm();
    const storedQuery = this.queryStorage.get();
    this.loadData(storedQuery);
  }

  private initializeLoggedUserType(): void {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.isAgentUser = loggedUser.userType === UserType.AGENT;
      this.isAgentGroupUser = loggedUser.userType === UserType.AGENT_GROUP;
      this.loggedUserId = loggedUser.id;
    });
  }

  public loadData(query?: DataQuery) {
    query = query || cloneDeep(defaultQuery);
    this.dataSource.get(query);
    this.queryStorage.save(query);
  }

  public onDownload() {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('sales.advancedPayment.downloadDialogTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.advancedPaymentService
    });
  }

  public createPaymentRequest() {
    this.router.navigateByUrl(ROUTES.ADVANCED_PAYMENT_REQUEST.url);
  }

  public onRowLinkClick(document: AdvancedPayment): void {
    this.router.navigate([ROUTES.ADVANCED_PAYMENT_VIEW.url, document.id]);
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<AdvancedPaymentFilters>({
      agent: [],
      requestNumber: [],
      status: [],
      amountRange: [],
      requestDateRange: [],
      reviewDateRange: [],
      reviewComment: []
    });
  }

  private initializeColumns() {
    this.columns = this.buildColumns();
  }

  private buildColumns() {
    return [
      {
        prop: 'bsp.isoCountryCode',
        name: 'sales.advancedPayment.columns.bsp',
        flexGrow: 1
      },
      {
        prop: 'requestNumber',
        name: 'sales.advancedPayment.columns.requestNumber',
        cellTemplate: 'commonLinkFromObjCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'agent.iataCode',
        name: 'sales.advancedPayment.columns.agentCode',
        flexGrow: 1,
        hidden: this.isAgentUser
      },
      {
        prop: 'status',
        name: 'sales.advancedPayment.columns.status',
        draggable: false,
        cellTemplate: 'badgeInfoCellTmpl',
        badgeInfo: {
          hidden: (value: AdvancedPayment) => !value,
          type: (value: AdvancedPayment) => this.getBadgeInfoType(value.status),
          tooltipLabel: (value: AdvancedPayment) => this.getTranslatedStatus(value.status),
          showIconType: true
        },
        flexGrow: 1
      },
      {
        prop: 'amount',
        name: 'sales.advancedPayment.columns.amount',
        flexGrow: 1
      },
      {
        prop: 'requestDate',
        name: 'sales.advancedPayment.columns.issueDate',
        cellTemplate: 'dayMonthYearCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'reviewDate',
        name: 'sales.advancedPayment.columns.reviewDate',
        cellTemplate: 'dayMonthYearCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'reviewComment',
        name: 'sales.advancedPayment.columns.reviewComment',
        flexGrow: 3
      }
    ];
  }

  private getBadgeInfoType(status: AdvancedPaymentStatus): BadgeInfoType {
    let type: BadgeInfoType;

    switch (status) {
      case AdvancedPaymentStatus.submitted:
        type = BadgeInfoType.info;
        break;
      case AdvancedPaymentStatus.approved:
        type = BadgeInfoType.success;
        break;
      case AdvancedPaymentStatus.rejected:
        type = BadgeInfoType.error;
        break;
      default:
        type = BadgeInfoType.regular;
        break;
    }

    return type;
  }

  private initializeDropdowns() {
    this.statusOptions = Object.values(AdvancedPaymentStatus)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.getTranslatedStatus(label)
      }));

    if (!this.isAgentUser) {
      let param = null;

      if (this.isAgentGroupUser) {
        param = { agentGroupId: this.loggedUserId };
      }

      this.agentDropdownOptions$ = this.agentDictionaryService.getDropdownOptions(param);
    }
  }

  private getTranslatedStatus(status: AdvancedPaymentStatus): string {
    return this.translationService.translate(`sales.advancedPayment.status.${status.toLowerCase()}`);
  }

  private initializeTotalItems() {
    this.dataSource.appliedQuery$
      .pipe(
        takeUntil(this.destroy$),
        map(query => query.paginateBy.totalElements)
      )
      .subscribe(total => {
        this.totalItemsMessage = this.translationService.translate('sales.advancedPayment.totalItemsMessage', {
          total
        });
      });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
