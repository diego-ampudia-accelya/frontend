import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { AdvancedPaymentService } from '../data/advanced-payment.service';
import { AdvancedPaymentView } from '../models/advanced-payment-view.model';

@Injectable()
export class SalesAdvancedPaymentResolver {
  constructor(private advancedPaymentService: AdvancedPaymentService) {}

  public resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<AdvancedPaymentView> {
    return this.advancedPaymentService.getOne(activatedRouteSnapshot.params['id']);
  }
}
