import { CommonModule, DecimalPipe, PercentPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { SharedModule } from '~app/shared/shared.module';
import { AdvancedPaymentListComponent } from './advanced-payment/advanced-payment-list/advanced-payment-list.component';
import { AdvancedPaymentRequestComponent } from './advanced-payment/advanced-payment-request/advanced-payment-request.component';
import { AdvancedPaymentUploadDialogComponent } from './advanced-payment/advanced-payment-upload-dialog/advanced-payment-upload-dialog.component';
import { AdvancedPaymentViewComponent } from './advanced-payment/advanced-payment-view/advanced-payment-view.component';
import { AdvancedPaymentService } from './advanced-payment/data/advanced-payment.service';
import { AdvancedPaymentEffects } from './advanced-payment/effects/advanced-payment.effects';
import { advancedPaymentReducer } from './advanced-payment/reducers/advanced-payment.reducer';
import { SalesAdvancedPaymentResolver } from './advanced-payment/resolvers/sales-advanced-payment-resolver.service';
import { advancedPaymentStateFeatureKey } from './advanced-payment/store/advanced-payment.state';
import { AnnualAccumulatedTotalsComponent } from './annual-accumulated/annual-accumulated-totals/annual-accumulated-totals.component';
import { AnnualAccumulatedComponent } from './annual-accumulated/annual-accumulated.component';
import { BillingAnalysisComponent } from './billing-analysis/billing-analysis.component';
import { BillingAnalysisDetailedInfoComponent } from './billing-analysis/components/billing-analysis-detailed-info/billing-analysis-detailed-info.component';
import { BillingAnalysisDialogComponent } from './billing-analysis/components/billing-analysis-dialog/billing-analysis-dialog.component';
import { BillingAnalysisTotalsComponent } from './billing-analysis/components/billing-analysis-totals/billing-analysis-totals.component';
import { BillingAnalysisSetupGuard } from './billing-analysis/data/billing-analysis-setup.guard';
import { BillingAnalysisStore } from './billing-analysis/data/billing-analysis-store.service';
import { BillingAnalysisResolver } from './billing-analysis/data/billing-analysis.resolver';
import { BillingQueryEffects } from './billing-analysis/effects/billing-query.effects';
import { featureKey, reducers } from './billing-analysis/reducers';
import { BillingStatementTotalsComponent } from './billing-statement/billing-statement-totals/billing-statement-totals.component';
import { BillingStatementComponent } from './billing-statement/billing-statement.component';
import { BillingStatementService } from './billing-statement/data/billing-statement.service';
import { ComparativeTotalsComponent } from './comparative/comparative-totals/comparative-totals.component';
import { ComparativeComponent } from './comparative/comparative.component';
import { ComparativeService } from './comparative/data/comparative.service';
import { NonComparativeService } from './non-comparative/data/non-comparative.service';
import { NonComparativeTotalsComponent } from './non-comparative/non-comparative-totals/non-comparative-totals.component';
import { NonComparativeComponent } from './non-comparative/non-comparative.component';
import { SalesRoutingModule } from './sales-routing.module';
import { ScuSummaryService } from './scu-summary/data/scu-summary.service';
import { ScuSummaryComponent } from './scu-summary/scu-summary.component';
import { ScuTotalsTypePipe } from './scu-summary/scu-totals/scu-totals-type.pipe';
import { ScuTotalsComponent } from './scu-summary/scu-totals/scu-totals.component';
import { SalesDownloadDialogComponent } from './shared/sales-download-dialog/sales-download-dialog.component';
import { SalesDownloadDialogService } from './shared/services/sales-download-dialog.service';

@NgModule({
  declarations: [
    BillingStatementComponent,
    BillingAnalysisComponent,
    BillingAnalysisDialogComponent,
    BillingAnalysisTotalsComponent,
    BillingStatementTotalsComponent,
    NonComparativeComponent,
    ScuSummaryComponent,
    ComparativeComponent,
    ComparativeTotalsComponent,
    NonComparativeTotalsComponent,
    ScuTotalsComponent,
    ScuTotalsTypePipe,
    AnnualAccumulatedComponent,
    AnnualAccumulatedTotalsComponent,
    SalesDownloadDialogComponent,
    BillingAnalysisDetailedInfoComponent,
    AdvancedPaymentListComponent,
    AdvancedPaymentRequestComponent,
    AdvancedPaymentViewComponent,
    AdvancedPaymentUploadDialogComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    SalesRoutingModule,
    StoreModule.forFeature(featureKey, reducers),
    StoreModule.forFeature(advancedPaymentStateFeatureKey, advancedPaymentReducer),
    EffectsModule.forFeature([AdvancedPaymentEffects, BillingQueryEffects])
  ],
  providers: [
    DialogConfig,
    ReactiveSubject,
    BillingAnalysisSetupGuard,
    BillingAnalysisResolver,
    BillingStatementService,
    BillingAnalysisStore,
    ComparativeService,
    ScuSummaryService,
    AdvancedPaymentService,
    NonComparativeService,
    PercentPipe,
    DecimalPipe,
    AbsoluteDecimalPipe,
    SalesDownloadDialogService,
    SalesAdvancedPaymentResolver
  ]
})
export class SalesModule {}
