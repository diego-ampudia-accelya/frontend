import { chain } from 'lodash';

import { AnnualTotalDto, AnnualTotalEntry, AnnualTotalType } from '../models';

export const extractEntryByConvention = (dto: AnnualTotalDto, type: AnnualTotalType): AnnualTotalEntry => {
  const matchingProps: any = chain(dto)
    .pickBy((_, key) => key.startsWith(type))
    .mapKeys((_, key) => key.replace(type, '').toLowerCase())
    .value();

  return {
    ...matchingProps,
    type
  };
};

export const convertToTotalModel = (dto: AnnualTotalDto) => {
  dto.domesticPercentage = dto.domesticPercentage / 100;
  dto.internationalPercentage = dto.internationalPercentage / 100;
  dto.totalPercentage = dto.totalPercentage / 100;

  return {
    currency: dto.currency,
    data: [
      extractEntryByConvention(dto, AnnualTotalType.Domestic),
      extractEntryByConvention(dto, AnnualTotalType.International),
      extractEntryByConvention(dto, AnnualTotalType.Total)
    ]
  };
};
