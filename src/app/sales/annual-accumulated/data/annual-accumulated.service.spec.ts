import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import {
  AnnualAccumulated,
  AnnualDetailQuery,
  AnnualTotal,
  AnnualTotalDto,
  AnnualTotalEntry,
  AnnualTotalType
} from '../models';
import { AnnualAccumulatedService } from './annual-accumulated.service';
import { DataQuery } from '~app/shared/components/list-view/data-query';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('AnnualAccumulatedService', () => {
  let service: AnnualAccumulatedService;
  let httpStub: SpyObject<HttpClient>;
  const appConfigStub = createSpyObject(AppConfigurationService, {
    baseApiPath: ''
  });
  const usa = { id: 1, name: 'USA' } as Bsp;
  const united = { id: 2, name: 'United' } as AirlineSummary;
  const usd = { id: 3, code: 'USD' } as Currency;
  const eur = { id: 4, code: 'EUR' } as Currency;
  const december2019 = [{ period: '2019121' }];

  beforeEach(() => {
    httpStub = createSpyObject(HttpClient);
    httpStub.get.and.returnValue(of([]));

    TestBed.configureTestingModule({
      providers: [
        AnnualAccumulatedService,
        { provide: HttpClient, useValue: httpStub },
        { provide: AppConfigurationService, useValue: appConfigStub }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(AnnualAccumulatedService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  describe('find', () => {
    const januaryData: AnnualAccumulated = {
      month: 1,
      year: 2019,
      total: 10,
      domestic: 5,
      international: 5,
      currency: usd
    };
    const februaryData: AnnualAccumulated = {
      month: 2,
      year: 2019,
      total: 12,
      domestic: 6,
      international: 6,
      currency: usd
    };

    it('should send correct request when `bsp` is provided as a filter', fakeAsync(() => {
      const query: DataQuery = {
        ...defaultQuery,
        filterBy: {
          bsp: usa
        }
      };

      const expectedUrl = '/sales-query/gross-sales/annuals?bspId=1';
      service.find(query).subscribe();

      expect(httpStub.get).toHaveBeenCalledWith(expectedUrl);
    }));

    it('should send correct request when `airline` is provided as a filter', fakeAsync(() => {
      const query: DataQuery = {
        ...defaultQuery,
        filterBy: {
          bsp: usa,
          airline: united
        }
      };

      service.find(query).subscribe();

      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('airlineId=2'));
    }));

    it('should send correct request when `currency` is provided as a filter', fakeAsync(() => {
      const query: DataQuery = {
        ...defaultQuery,
        filterBy: {
          bsp: usa,
          currency: [usd, eur]
        }
      };

      service.find(query).subscribe();

      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('currencyId=3,4'));
    }));

    it('should send correct request when `monthPeriod` is provided as a filter', fakeAsync(() => {
      const query: DataQuery = {
        ...defaultQuery,
        filterBy: {
          bsp: usa,
          monthPeriod: december2019
        }
      };

      service.find(query).subscribe();

      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('month=12'));
      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('year=2019'));
    }));

    it('should sort results when sortBy is provided', fakeAsync(() => {
      const query: DataQuery = {
        ...defaultQuery,
        sortBy: [{ attribute: 'monthAndYear', sortType: SortOrder.Desc }],
        filterBy: {
          bsp: usa
        }
      };
      const results = [januaryData, februaryData];
      httpStub.get.and.returnValue(of(results));

      let actual: PagedData<AnnualAccumulated>;
      service.find(query).subscribe(r => (actual = r));

      const [first, second] = actual.records;
      expect(first.month).toEqual(februaryData.month);
      expect(second.month).toEqual(januaryData.month);
    }));

    it('should paginate results when paginateBy is provided', fakeAsync(() => {
      const query: DataQuery = {
        ...defaultQuery,
        paginateBy: { page: 1, size: 1 },
        filterBy: {
          bsp: usa
        }
      };
      const results = [januaryData, februaryData];
      httpStub.get.and.returnValue(of(results));

      let actual: PagedData<AnnualAccumulated>;
      service.find(query).subscribe(r => (actual = r));

      const expected: PagedData<AnnualAccumulated> = {
        pageSize: 1,
        pageNumber: 1,
        total: 2,
        totalPages: 2,
        records: [jasmine.objectContaining(februaryData) as any]
      };

      expect(actual).toEqual(expected);
    }));

    it('should return cached result when multiple requests with the same filter are performed', () => {
      const query: DataQuery = {
        ...defaultQuery,
        filterBy: {
          bsp: usa
        }
      };
      const results = [januaryData, februaryData];
      httpStub.get.and.returnValue(of(results));

      let firstActual: PagedData<AnnualAccumulated>;
      service.find(query).subscribe(r => (firstActual = r));

      let secondActual: PagedData<AnnualAccumulated>;
      service.find(query).subscribe(r => (secondActual = r));

      expect(httpStub.get).toHaveBeenCalledTimes(1);
      expect(firstActual).toEqual(secondActual);
    });
  });

  describe('getTotals', () => {
    it('should send correct request', fakeAsync(() => {
      const filter = {
        bsp: usa,
        airline: united,
        currency: [usd],
        monthPeriod: december2019
      };

      service.getTotals(filter).subscribe();

      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('/sales-query/gross-sales/annuals/totals'));
      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('bspId=1'));
      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('airlineId=2'));
      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('month=12'));
      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('year=2019'));
      expect(httpStub.get).toHaveBeenCalledWith(jasmine.stringMatching('currencyId=3'));
    }));

    it('should return formated results', fakeAsync(() => {
      const filter = {
        bsp: usa,
        airline: united,
        currency: [usd],
        monthPeriod: december2019
      };
      const results: AnnualTotalDto[] = [
        {
          currency: usd,
          domesticAirline: 10,
          domesticIndustry: 10,
          domesticPercentage: 100,
          internationalAirline: 20,
          internationalIndustry: 20,
          internationalPercentage: 100,
          totalAirline: 30,
          totalIndustry: 30,
          totalPercentage: 100
        }
      ];
      httpStub.get.and.returnValue(of(results));

      let actual: AnnualTotal[];
      service.getTotals(filter).subscribe(r => (actual = r));
      tick();

      const expected: AnnualTotal[] = [
        {
          currency: usd,
          data: [
            { type: AnnualTotalType.Domestic, airline: 10, industry: 10, percentage: 1 },
            { type: AnnualTotalType.International, airline: 20, industry: 20, percentage: 1 },
            { type: AnnualTotalType.Total, airline: 30, industry: 30, percentage: 1 }
          ]
        }
      ];
      expect(actual).toEqual(expected);
    }));
  });

  describe('getDetails', () => {
    const detailQuery: AnnualDetailQuery = {
      id: '1',
      bsp: usa,
      airline: united,
      currency: [usd],
      month: 12,
      year: 2019
    };

    it('should send proper request', fakeAsync(() => {
      service.getDetails(detailQuery).subscribe();

      expect(httpStub.get).toHaveBeenCalledWith('/sales-query/gross-sales/annuals/details', {
        params: { bspId: 1, airlineId: 2, currencyId: [3], month: 12, year: 2019 }
      });
    }));

    it('should call details endpoint only once when multiple requests with the same filter are performed', () => {
      service.getDetails(detailQuery).subscribe();
      service.getDetails(detailQuery).subscribe();

      expect(httpStub.get).toHaveBeenCalledTimes(1);
    });

    it('should return formated results', fakeAsync(() => {
      const result: AnnualTotalDto = {
        currency: usd,
        domesticAirline: 10,
        domesticIndustry: 10,
        domesticPercentage: 100,
        internationalAirline: 20,
        internationalIndustry: 20,
        internationalPercentage: 100,
        totalAirline: 30,
        totalIndustry: 30,
        totalPercentage: 100
      };
      httpStub.get.and.returnValue(of(result));

      let actual: AnnualTotalEntry[];
      service.getDetails(detailQuery).subscribe(r => (actual = r));
      tick();

      const expected: AnnualTotalEntry[] = [
        { type: AnnualTotalType.Domestic, airline: 10, industry: 10, percentage: 1 },
        { type: AnnualTotalType.International, airline: 20, industry: 20, percentage: 1 },
        { type: AnnualTotalType.Total, airline: 30, industry: 30, percentage: 1 }
      ];
      expect(actual).toEqual(expected);
    }));
  });
});
