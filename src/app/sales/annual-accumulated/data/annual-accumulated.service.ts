import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isEqual } from 'lodash';
import moment from 'moment-mini';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { generate } from 'shortid';

import { AnnualAccumulated, AnnualDetailQuery, AnnualTotal, AnnualTotalDto, AnnualTotalEntry } from '../models';
import { convertToTotalModel } from './annual-totals.converters';
import { PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ClientGridTableHelper } from '~app/shared/helpers';
import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class AnnualAccumulatedService implements Queryable<AnnualAccumulated> {
  private helper = new ClientGridTableHelper<AnnualAccumulated>();

  private cache = {
    filterBy: {},
    records: []
  };
  private detailsCache = new Map<string, AnnualTotalEntry[]>();

  private baseUrl: string;
  private detailsUrl: string;

  constructor(private http: HttpClient, appConfiguration: AppConfigurationService) {
    this.baseUrl = `${appConfiguration.baseApiPath}/sales-query/gross-sales/annuals`;
    this.detailsUrl = `${this.baseUrl}/details`;
  }

  public find(query: DataQuery): Observable<PagedData<AnnualAccumulated>> {
    let queryResults$ = of(this.cache.records);

    if (!isEqual(query.filterBy, this.cache.filterBy)) {
      queryResults$ = this.requestData<AnnualAccumulated[]>(this.baseUrl, query).pipe(
        map(response => this.normalizeResponse(response)),
        tap(normalizedResponse => {
          this.cache = { records: normalizedResponse, filterBy: query.filterBy };
          this.detailsCache.clear();
        })
      );
    }

    return queryResults$.pipe(
      map((response: AnnualAccumulated[]) => {
        // Apply paging and sorting
        const { filterBy, ...rest } = query;
        // TODO: add BE filter model as generic type to `fromDataQuery` factory method
        const clientQuery = RequestQuery.fromDataQuery(rest);

        return this.helper.query(response, clientQuery);
      })
    );
  }

  public getTotals(filterBy: any): Observable<AnnualTotal[]> {
    return this.requestData<AnnualTotalDto[]>(`${this.baseUrl}/totals`, { filterBy }).pipe(
      map(response => response.map(convertToTotalModel))
    );
  }

  public getDetails(detailQuery: AnnualDetailQuery): Observable<AnnualTotalEntry[]> {
    const cachedDetail = this.detailsCache.get(detailQuery.id);
    if (cachedDetail) {
      return of(cachedDetail);
    }

    const formattedFilters = {
      ...this.formatBasicFilters(detailQuery),
      month: detailQuery.month,
      year: detailQuery.year
    };

    return this.http.get<AnnualTotalDto>(this.detailsUrl, { params: formattedFilters as any }).pipe(
      map(detail => convertToTotalModel(detail).data),
      tap(detail => this.detailsCache.set(detailQuery.id, detail))
    );
  }

  private normalizeResponse(response: AnnualAccumulated[]) {
    response = response.map(record => ({
      ...record,
      id: generate(),
      monthAndYear: moment([record.year, record.month - 1])
    }));

    return response;
  }

  private requestData<T>(url: string, dataQuery: Partial<DataQuery>): Observable<T> {
    const formattedQuery = this.formatApiQuery(dataQuery);

    return this.http.get<T>(url + formattedQuery.getQueryString());
  }

  private formatApiQuery(query: Partial<DataQuery>): RequestQuery {
    const formattedBasicFilter = this.formatBasicFilters(query.filterBy as any);

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      filterBy: {
        ...formattedBasicFilter,
        ...this.formatMonthPeriod(query.filterBy.monthPeriod)
      }
    });
  }

  private formatBasicFilters(basicFilters: { bsp: Bsp; airline?: AirlineSummary; currency?: Currency[] }) {
    const { airline, currency, bsp } = basicFilters;

    return {
      bspId: bsp.id,
      airlineId: airline && airline.id,
      currencyId: currency && currency.map(item => item.id)
    };
  }

  private formatMonthPeriod(monthPeriod: PeriodOption[]): { year: number; month: number } {
    monthPeriod = monthPeriod || [];
    const periodOption = monthPeriod[0];
    let result = null;

    if (periodOption) {
      const date = PeriodUtils.getDateFrom(periodOption.period);
      result = {
        year: date.getFullYear(),
        month: date.getMonth() + 1
      };
    }

    return result;
  }
}
