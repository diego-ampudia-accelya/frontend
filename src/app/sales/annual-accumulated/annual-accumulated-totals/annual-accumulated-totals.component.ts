import { PercentPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { chain, first, isEmpty } from 'lodash';

import { AnnualTotal, AnnualTotalEntry, AnnualTotalType } from '../models';
import { SimpleTableColumn } from '~app/shared/components';
import { SelectMode } from '~app/shared/enums';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-annual-accumulated-totals',
  templateUrl: './annual-accumulated-totals.component.html',
  styleUrls: ['./annual-accumulated-totals.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnnualAccumulatedTotalsComponent implements OnInit, OnChanges {
  @Input() totalsByCurrency: AnnualTotal[];

  public currency: Currency;
  public currencyOptions: DropdownOption<Currency>[];
  public columns: SimpleTableColumn<AnnualTotalEntry>[];
  public selectedEntry: AnnualTotalEntry;

  public selectMode = SelectMode;

  private _data: AnnualTotalEntry[];
  public get data(): AnnualTotalEntry[] {
    return this._data;
  }
  public set data(value: AnnualTotalEntry[]) {
    const defaultValue = [{}] as any;
    this._data = isEmpty(value) ? defaultValue : value;
  }

  public getAmountFormat = getAmountFormat;

  private get activeTotal(): AnnualTotal {
    return this.totalsByCurrency && this.totalsByCurrency.find(total => total.currency.code === this.currency.code);
  }

  @ViewChild('amountTemplate', { static: true })
  private amountTemplate: TemplateRef<any>;

  @ViewChild('actionHeaderTemplate', { static: true })
  private actionHeaderTemplate: TemplateRef<any>;

  @ViewChild('actionCellTemplate', { static: true })
  private actionCellTemplate: TemplateRef<any>;

  public ngOnChanges(changes: SimpleChanges): void {
    const { totalsByCurrency } = changes;
    if (totalsByCurrency) {
      const totals: AnnualTotal[] = totalsByCurrency.currentValue || [];
      this.currencyOptions = chain(totals)
        .map(total => total.currency)
        .map(currency => ({ value: currency, label: currency.code }))
        .orderBy(option => option.label)
        .value();

      const firstCurrency = first(this.currencyOptions) && first(this.currencyOptions).value;
      this.selectCurrency(firstCurrency);
    }
  }

  public ngOnInit() {
    this.columns = this.getColumns();
  }

  public selectCurrency(currency: Currency): void {
    this.currency = currency;
    const defaultEntry = this.activeTotal && this.activeTotal.data.find(item => item.type === AnnualTotalType.Total);
    this.selectEntry(defaultEntry);
  }

  public selectEntry(item: AnnualTotalEntry): void {
    this.selectedEntry = item;
    this.data = item && [item];
  }

  public showAllEntries($event: Event): void {
    this.data = this.activeTotal && this.activeTotal.data;
    this.selectedEntry = null;
    $event.stopPropagation();
  }

  /**
   * Creates column configuration for simple table.
   * Because we use custom tempates it should be called during or after ngOnInit.
   */
  private getColumns(): SimpleTableColumn<AnnualTotalEntry>[] {
    const baseColumn: Partial<SimpleTableColumn<any>> = {
      customStyle: 'text-right',
      cellTemplate: this.amountTemplate
    };

    return [
      {
        headerCellTemplate: this.actionHeaderTemplate,
        field: 'type',
        customStyle: 'actions-column-cell',
        cellTemplate: this.actionCellTemplate
      },
      {
        ...baseColumn,
        header: 'sales.annualAccumulated.totals.myAirline',
        field: 'airline'
      },
      {
        ...baseColumn,
        header: 'sales.annualAccumulated.totals.industry',
        field: 'industry'
      },
      {
        header: 'sales.annualAccumulated.totals.industryPercentage',
        field: 'percentage',
        pipe: PercentPipe,
        pipeArgs: ['1.0-2'],
        customStyle: 'text-right'
      }
    ];
  }
}
