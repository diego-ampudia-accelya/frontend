import { PercentPipe } from '@angular/common';
import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockComponent, MockPipe } from 'ng-mocks';

import { AnnualTotal, AnnualTotalType } from '../models';

import { AnnualAccumulatedTotalsComponent } from './annual-accumulated-totals.component';
import { SelectComponent, SimpleTableComponent } from '~app/shared/components';

describe('AnnualAccumulatedTotalsComponent', () => {
  let component: AnnualAccumulatedTotalsComponent;
  let fixture: ComponentFixture<AnnualAccumulatedTotalsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AnnualAccumulatedTotalsComponent,
        MockPipe(PercentPipe),
        MockPipe(L10nTranslatePipe),
        MockComponent(SimpleTableComponent),
        MockComponent(SelectComponent)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualAccumulatedTotalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize column configuration with 4 columns', () => {
    expect(component.columns.length).toBe(4);
  });

  describe('when totalsByCurrency is empty', () => {
    it('should have one empty entry in `data` when totalsByCurrency is empty', () => {
      component.ngOnChanges({ totalsByCurrency: new SimpleChange(null, null, true) });
      expect(component.data).toEqual([{}] as any);
    });

    it('should have no selected currency when totalsByCurrency is empty', () => {
      component.ngOnChanges({ totalsByCurrency: new SimpleChange(null, null, true) });
      expect(component.currency).toBeFalsy();
    });

    it('should have empty currencyOptions when totalsByCurrency is empty', () => {
      component.ngOnChanges({ totalsByCurrency: new SimpleChange(null, null, true) });
      expect(component.currencyOptions).toEqual([]);
    });
  });

  describe('when totalsByCurrency is not empty', () => {
    const audDomestic = { airline: 1, industry: 5, percentage: 20, type: AnnualTotalType.Domestic };
    const audTotal = { airline: 3, industry: 10, percentage: 30, type: AnnualTotalType.Total };
    const usdTotal = { airline: 30, industry: 100, percentage: 30, type: AnnualTotalType.Total };
    const usd = { id: 10122, code: 'USD', decimals: 2 };
    const aud = { id: 10123, code: 'AUD', decimals: 2 };
    const totalsByCurrency: AnnualTotal[] = [
      {
        currency: usd,
        data: [
          { airline: 10, industry: 50, percentage: 20, type: AnnualTotalType.Domestic },
          { airline: 20, industry: 50, percentage: 40, type: AnnualTotalType.International },
          usdTotal
        ]
      },
      {
        currency: aud,
        data: [audDomestic, { airline: 2, industry: 5, percentage: 40, type: AnnualTotalType.International }, audTotal]
      }
    ];

    beforeEach(() => {
      component.totalsByCurrency = totalsByCurrency;
      component.ngOnChanges({ totalsByCurrency: new SimpleChange(null, totalsByCurrency, true) });
    });

    it('should set the Total entry as selectedEntry', () => {
      expect(component.selectedEntry.type).toBe(AnnualTotalType.Total);
    });

    it('should extract currencyOptions from totalsByCurrency', () => {
      expect(component.currencyOptions).toEqual([
        { value: aud, label: 'AUD' },
        { value: usd, label: 'USD' }
      ]);
    });

    it('should select the first available currency', () => {
      expect(component.currency).toBe(aud);
      expect(component.data).toEqual([audTotal]);
    });

    it('should select approprite data when the currency is changed', () => {
      component.selectCurrency(usd);

      expect(component.currency).toBe(usd);
      expect(component.data).toEqual([usdTotal]);
    });

    it('should display data for single entry when selectEntry is called', () => {
      const entryToSelect = audDomestic;

      component.selectEntry(entryToSelect);

      expect(component.data).toEqual([entryToSelect]);
      expect(component.selectedEntry).toEqual(entryToSelect);
    });

    it('should show data for all entries when showAllEntries is called', () => {
      component.showAllEntries(new Event(''));

      expect(component.data.length).toBe(3);
      expect(component.selectedEntry).toBe(null);
    });
  });
});
