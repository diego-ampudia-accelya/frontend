import { PercentPipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { cloneDeep, isEmpty, last } from 'lodash';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, finalize, first, map, switchMap, tap } from 'rxjs/operators';

import { SalesFilter } from '../shared/models/sales-filter.model';
import { AnnualAccumulatedService } from './data/annual-accumulated.service';
import {
  AnnualAccumulated,
  AnnualAccumulatedDetails,
  AnnualDetailQuery,
  AnnualTotal,
  AnnualTotalEntry
} from './models';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import {
  ButtonDesign,
  FloatingPanelComponent,
  PeriodOption,
  PeriodUtils,
  SimpleTableColumn
} from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SelectMode, SortOrder } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User } from '~app/shared/models/user.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-annual-accumulated',
  templateUrl: './annual-accumulated.component.html',
  styleUrls: ['./annual-accumulated.component.scss'],
  providers: [
    SalesDisplayFilterFormatter,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: AnnualAccumulatedService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [AnnualAccumulatedService]
    }
  ]
})
export class AnnualAccumulatedComponent implements OnInit {
  public columns: TableColumn[];
  public searchForm: FormGroup;
  public selectedBsp: Bsp;

  public periodOptions: PeriodOption[];
  public currencyDropdownOptions$: Observable<DropdownOption[]>;
  public userBspOptions: DropdownOption<Bsp>[] = [];

  public selectMode = SelectMode;
  public buttonDesign = ButtonDesign;
  private leanUsersYears = 5;
  private nonLeanUsersYears = 2;
  public periodMonthPickerYearsBack: number;

  public predefinedFilters: {
    bsp: Bsp;
    monthPeriod: PeriodOption[];
    year: number;
    month: number;
  };

  public totals: AnnualTotal[];
  public areTotalsLoading: boolean;

  @ViewChild(FloatingPanelComponent, { static: true })
  public totalsPanel: FloatingPanelComponent;

  public isLoadingRowDetails: boolean;
  public rowDetailsColumns: SimpleTableColumn<AnnualTotalEntry>[];
  public rowDetails: AnnualAccumulatedDetails;
  public getAmountFormat = getAmountFormat;

  public isBspFilterLocked: boolean;

  private loggedUser: User;
  private currentAirline: AirlineSummary;

  @ViewChild('actionCellTemplate', { static: true })
  private actionCellTemplate: TemplateRef<any>;

  @ViewChild('amountTemplate', { static: true })
  private amountTemplate: TemplateRef<any>;

  @ViewChild('amountRowDetailsTemplate', { static: true })
  private amountRowDetailsTemplate: TemplateRef<any>;

  public formFactory: FormUtil;

  constructor(
    public dataSource: QueryableDataSource<AnnualAccumulated>,
    public displayFilterFormatter: SalesDisplayFilterFormatter,
    private dropdownOptionsService: SalesDropdownOptionsService,
    private annualAccumulatedService: AnnualAccumulatedService,
    private permissionsService: PermissionsService,
    private store: Store<AppState>,
    protected formBuilder: FormBuilder,
    protected queryStorage: DefaultQueryStorage
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public async ngOnInit(): Promise<void> {
    this.rowDetailsColumns = this.getRowDetailColumns();
    this.columns = this.getColumns();
    this.searchForm = this.createForm();
    this.setPeriodPickerYearsBack();

    await this.initUserSpecifications();

    const storedQuery = this.queryStorage.get();
    if (storedQuery) {
      // Restore BSP selection
      this.selectedBsp = storedQuery.filterBy.bsp || this.selectedBsp;
    }

    await this.loadDropdownOptions(this.selectedBsp);
    this.loadData(storedQuery);
  }

  public async onTotalPanelOpened(): Promise<void> {
    if (!this.totals) {
      this.totals = await this.getTotals().toPromise();
    }
  }

  public async selectBsp(bsp: Bsp): Promise<void> {
    if (bsp) {
      this.selectedBsp = bsp;
      this.resetFieldsOnBspChange();
      await this.loadDropdownOptions(bsp);
    }
  }

  public loadData(query?: Partial<DataQuery>): void {
    if (isEmpty(this.periodOptions)) {
      return;
    }

    query = query || cloneDeep(defaultQuery);

    if (isEmpty(query.sortBy)) {
      query.sortBy = [{ attribute: 'monthAndYear', sortType: SortOrder.Asc }];
    }

    const dataQuery: Partial<DataQuery> = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy,
        airline: this.currentAirline,
        bsp: this.selectedBsp
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);

    this.totals = null;
    this.totalsPanel.close();
  }

  public async onRowToggle(event): Promise<void> {
    if (event.type === 'row') {
      const record: AnnualAccumulated = event.value;
      this.rowDetails = await this.getRowDetailInformation(record).toPromise();
    }
  }

  private resetFieldsOnBspChange(): void {
    FormUtil.get<SalesFilter>(this.searchForm, 'currency').reset();
  }

  private getRowDetailInformation(record: AnnualAccumulated): Observable<AnnualAccumulatedDetails> {
    const detailQuery: AnnualDetailQuery = {
      id: record.id,
      airline: this.currentAirline,
      bsp: this.selectedBsp,
      year: record.year,
      month: record.month,
      currency: [record.currency]
    };

    const emptyRowDetails = {
      record,
      data: [{} as AnnualTotalEntry]
    };

    return of(null).pipe(
      tap(() => {
        this.isLoadingRowDetails = true;
        this.rowDetails = emptyRowDetails;
      }),
      switchMap(() => this.annualAccumulatedService.getDetails(detailQuery)),
      map(data => ({ record, data })),
      catchError(() => of(emptyRowDetails)),
      finalize(() => (this.isLoadingRowDetails = false))
    );
  }

  private async loadDropdownOptions(bsp: Bsp): Promise<void> {
    this.currencyDropdownOptions$ = this.dropdownOptionsService.getCurrencies(bsp);

    const [periods, airline] = await forkJoin([
      this.dropdownOptionsService.getPeriods(bsp),
      this.dropdownOptionsService.getUserAirline(bsp)
    ]).toPromise();

    this.periodOptions = periods;
    this.currentAirline = airline;

    const lastCompleteMonth = PeriodUtils.findLastCompleteMonth(this.periodOptions, this.periodMonthPickerYearsBack);
    let monthPeriod: PeriodOption[] = [];
    if (lastCompleteMonth) {
      monthPeriod = [lastCompleteMonth.periods[0], last(lastCompleteMonth.periods)];
    }

    this.predefinedFilters = {
      ...this.predefinedFilters,
      monthPeriod
    };
  }

  private getTotals(): Observable<AnnualTotal[]> {
    return this.dataSource.appliedQuery$.pipe(
      first(),
      tap(() => (this.areTotalsLoading = true)),
      switchMap(({ filterBy }) => this.annualAccumulatedService.getTotals(filterBy)),
      catchError(() => of(null)),
      finalize(() => (this.areTotalsLoading = false))
    );
  }

  private async initUserSpecifications(): Promise<void> {
    this.loggedUser = await this.store.pipe(select(getUser), first()).toPromise();

    // Initialize BSP filter
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];
    this.updateBspFilter();
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readAnnualAccumulated;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private updateBspFilter(): void {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    const onlyOneBsp = this.userBspOptions.length === 1;

    if (this.userBspOptions.length) {
      const defaultBsp = this.userBspOptions.find(
        ({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc
      )?.value;

      const firstListBsp = this.userBspOptions[0]?.value;
      const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

      this.selectedBsp = filterValue;
      this.predefinedFilters = {
        ...this.predefinedFilters,
        bsp: this.selectedBsp
      };
    }

    // Flags
    this.isBspFilterLocked = !hasLeanPermission || this.userBspOptions.length === 1;
  }

  private createForm() {
    return this.formFactory.createGroup<SalesFilter>({
      bsp: [],
      monthPeriod: [],
      currency: []
    });
  }

  private setPeriodPickerYearsBack(): void {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.periodMonthPickerYearsBack = hasLeanPermission ? this.leanUsersYears : this.nonLeanUsersYears;
  }

  private getColumns(): TableColumn[] {
    const amountCellConfig = {
      sortable: true,
      resizeable: true,
      minWidth: 150,
      flexGrow: 1,
      cellTemplate: this.amountTemplate,
      cellClass: 'text-right'
    };

    return [
      {
        prop: 'monthAndYear',
        name: 'sales.annualAccumulated.columns.monthYear',
        sortable: true,
        resizeable: true,
        minWidth: 150,
        flexGrow: 1,
        pipe: { transform: v => v.format('MMMM / YYYY') }
      },
      {
        prop: 'currency.code',
        name: 'sales.annualAccumulated.columns.currency',
        sortable: true,
        resizeable: true,
        minWidth: 105,
        flexGrow: 0.6
      },
      {
        prop: 'international',
        name: 'sales.annualAccumulated.columns.international',
        ...amountCellConfig
      },
      {
        prop: 'domestic',
        name: 'sales.annualAccumulated.columns.domestic',
        ...amountCellConfig
      },
      {
        prop: 'total',
        name: 'sales.annualAccumulated.columns.allSalesTypes',
        ...amountCellConfig
      }
    ];
  }

  private getRowDetailColumns(): SimpleTableColumn<AnnualTotalEntry>[] {
    const baseColumn = {
      customStyle: 'text-right',
      cellTemplate: this.amountRowDetailsTemplate
    };

    return [
      { field: 'type', cellTemplate: this.actionCellTemplate, customStyle: 'actions-column-cell' },
      {
        ...baseColumn,
        header: 'sales.annualAccumulated.rowDetail.myAirline',
        field: 'airline'
      },
      {
        ...baseColumn,
        header: 'sales.annualAccumulated.rowDetail.industry',
        field: 'industry'
      },
      {
        header: 'sales.annualAccumulated.rowDetail.industryPercentage',
        field: 'percentage',
        pipe: PercentPipe,
        pipeArgs: ['1.2-2'],
        customStyle: 'text-right'
      }
    ];
  }
}
