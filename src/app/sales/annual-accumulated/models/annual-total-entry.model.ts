import { AnnualTotalType } from './annual-total-type.model';

export interface AnnualTotalEntry {
  type: AnnualTotalType;
  airline: number;
  industry: number;
  percentage: number;
}
