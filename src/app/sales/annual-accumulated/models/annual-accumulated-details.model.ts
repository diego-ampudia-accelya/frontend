import { AnnualAccumulated } from './annual-accumulated.model';
import { AnnualTotalEntry } from './annual-total-entry.model';

export interface AnnualAccumulatedDetails {
  record: AnnualAccumulated;
  data: AnnualTotalEntry[];
}
