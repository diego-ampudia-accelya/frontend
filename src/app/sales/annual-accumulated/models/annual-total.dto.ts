export interface AnnualTotalDto {
  currency: {
    id: number;
    code: string;
    decimals: number;
  };
  domesticAirline: number;
  domesticIndustry: number;
  domesticPercentage: number;
  internationalAirline: number;
  internationalIndustry: number;
  internationalPercentage: number;
  totalAirline: number;
  totalIndustry: number;
  totalPercentage: number;
}
