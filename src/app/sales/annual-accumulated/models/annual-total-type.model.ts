/* eslint-disable @typescript-eslint/naming-convention */
export enum AnnualTotalType {
  Domestic = 'domestic',
  International = 'international',
  Total = 'total'
}
