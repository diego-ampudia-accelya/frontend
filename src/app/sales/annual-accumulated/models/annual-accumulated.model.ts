import { Moment } from 'moment-mini';

import { Currency } from '~app/shared/models/currency.model';

export interface AnnualAccumulated {
  id?: string;
  monthAndYear?: Moment;
  month: number;
  year: number;
  currency: Currency;
  domestic: number;
  international: number;
  total: number;
}
