import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface AnnualDetailQuery {
  id: string;
  bsp: Bsp;
  airline: AirlineSummary;
  month: number;
  year: number;
  currency: Currency[];
}
