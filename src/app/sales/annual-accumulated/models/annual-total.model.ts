import { AnnualTotalEntry } from './annual-total-entry.model';
import { Currency } from '~app/shared/models/currency.model';

export interface AnnualTotal {
  currency: Currency;
  data: AnnualTotalEntry[];
}
