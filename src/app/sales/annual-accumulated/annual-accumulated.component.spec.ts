import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEqual } from 'lodash';
import { MessageService } from 'primeng/api';
import { of } from 'rxjs';

import { AnnualAccumulatedComponent } from './annual-accumulated.component';
import { AnnualAccumulatedService } from './data/annual-accumulated.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { PeriodOption } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { AppConfigurationService } from '~app/shared/services';
import { FloatingPanelComponentMockComponent, TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('AnnualAccumulatedComponent', () => {
  let component: AnnualAccumulatedComponent;
  let fixture: ComponentFixture<AnnualAccumulatedComponent>;
  const translationServiceStub = createSpyObject(L10nTranslationService);
  const messageServiceStub = createSpyObject(MessageService);
  const defaultQueryStorageStub = createSpyObject(DefaultQueryStorage);
  const annualAccumulatedServiceStub = createSpyObject(AnnualAccumulatedService);
  const queryableDataSourceStub = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(cloneDeep(defaultQuery))
  });
  const permissionsServiceSpy = createSpyObject(PermissionsService);

  const defaultPeriod: PeriodOption = {
    period: '2019071',
    dateFrom: '06/29/2019',
    dateTo: '07/07/2019'
  };

  const bspBulgaria = { id: 1, name: 'Bulgaria' } as Bsp;
  const initialState = {
    auth: { user: createAirlineUser() },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    }
  };

  const defaultBsp: Bsp = {
    id: 5,
    name: 'First bsp',
    isoCountryCode: '123',
    effectiveFrom: '2019-05-10'
  };

  const defaultDropdownBsp: DropdownOption<Bsp>[] = [
    {
      value: defaultBsp,
      label: 'Default bsp label'
    }
  ];

  const dropdownOptionsServiceStub = createSpyObject(SalesDropdownOptionsService);
  dropdownOptionsServiceStub.getPeriods.and.returnValue(of([defaultPeriod]));
  dropdownOptionsServiceStub.getUserAirline.and.returnValue(of(null));
  dropdownOptionsServiceStub.getCurrencies.and.returnValue(of(null));
  dropdownOptionsServiceStub.getBsps.and.returnValue(of(defaultDropdownBsp));

  const totalMock = {
    currency: { id: 698582, code: 'EUR', decimals: 2 },
    data: [
      { airline: 0, industry: 46309996.5, percentage: 0, type: 'domestic' },
      { percentage: 0.0001, airline: 18148, industry: 243860316.71, type: 'international' },
      { airline: 18148, industry: 290170313.21, percentage: 0.0001, type: 'total' }
    ]
  };

  beforeEach(() => {
    TestBed.overrideProvider(DefaultQueryStorage, {
      useValue: defaultQueryStorageStub
    }).overrideProvider(QueryableDataSource, { useValue: queryableDataSourceStub });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AnnualAccumulatedComponent, FloatingPanelComponentMockComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        AppConfigurationService,
        provideMockStore({ initialState }),
        {
          provide: L10nTranslationService,
          useValue: translationServiceStub
        },
        {
          provide: MessageService,
          useValue: messageServiceStub
        },
        {
          provide: PeriodPipe,
          useValue: PeriodPipeMock
        },
        {
          provide: SalesDropdownOptionsService,
          useValue: dropdownOptionsServiceStub
        },
        {
          provide: AnnualAccumulatedService,
          useValue: annualAccumulatedServiceStub
        },
        {
          provide: PermissionsService,
          useValue: permissionsServiceSpy
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualAccumulatedComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set selectedBsp, with stored query bsp, when queryStorage is defined', fakeAsync(() => {
    defaultQueryStorageStub.get.and.returnValue({ ...cloneDeep(defaultQuery), filterBy: { bsp: { ...bspBulgaria } } });

    component.ngOnInit();
    tick();

    expect(component.selectedBsp.name).toBe(bspBulgaria.name);
  }));

  it('should get totals, when totals are not defined', fakeAsync(() => {
    annualAccumulatedServiceStub.getTotals.and.returnValue(of([totalMock]));

    component.totals = null;
    component.onTotalPanelOpened();
    tick();

    expect(annualAccumulatedServiceStub.getTotals).toHaveBeenCalled();
    expect(isEqual(component.totals.shift(), totalMock)).toBe(true);
  }));

  //TODO: Excluding this test suit due to an error need to update this test suit in order to update it.
  //TODO: Need to add another test suit to cover resetFieldsOnBspChange() functinality.
  xit('should set selected bsp, when it is passed to the selectBsp function', fakeAsync(() => {
    component.selectBsp(bspBulgaria);
    tick();

    expect(isEqual(component.selectedBsp, bspBulgaria)).toBe(true);
    expect(dropdownOptionsServiceStub.getCurrencies).toHaveBeenCalled();
    expect(dropdownOptionsServiceStub.getPeriods).toHaveBeenCalled();
    expect(dropdownOptionsServiceStub.getUserAirline).toHaveBeenCalled();
  }));

  it('should set columns on init', () => {
    component.columns = [];

    const expectedColumns = [
      jasmine.objectContaining({
        prop: 'monthAndYear',
        name: 'sales.annualAccumulated.columns.monthYear',
        sortable: true,
        resizeable: true,
        minWidth: 150,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'currency.code',
        name: 'sales.annualAccumulated.columns.currency',
        sortable: true,
        resizeable: true,
        minWidth: 105,
        flexGrow: 0.6
      }),
      jasmine.objectContaining({
        prop: 'international',
        name: 'sales.annualAccumulated.columns.international',
        sortable: true,
        resizeable: true,
        minWidth: 150,
        flexGrow: 1,
        cellClass: 'text-right'
      }),
      jasmine.objectContaining({
        prop: 'domestic',
        name: 'sales.annualAccumulated.columns.domestic',
        sortable: true,
        resizeable: true,
        minWidth: 150,
        flexGrow: 1,
        cellClass: 'text-right'
      }),
      jasmine.objectContaining({
        prop: 'total',
        name: 'sales.annualAccumulated.columns.allSalesTypes',
        sortable: true,
        resizeable: true,
        minWidth: 150,
        flexGrow: 1,
        cellClass: 'text-right'
      })
    ];

    component.ngOnInit();

    expect(component.columns).toEqual(expectedColumns as TableColumn[]);
  });
});
