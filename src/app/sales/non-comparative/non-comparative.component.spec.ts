import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { SalesDownloadDialogService } from '../shared/services/sales-download-dialog.service';
import { NonComparativeService } from './data/non-comparative.service';
import { NonComparativeComponent } from './non-comparative.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { FloatingPanelComponent } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

const query$ = of({
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery);

describe('NonComparativeComponent', () => {
  let component: NonComparativeComponent;
  let fixture: ComponentFixture<NonComparativeComponent>;

  const nonComparativeServiceSpy = createSpyObject(NonComparativeService);
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const dropdownOptionsServiceSpy = createSpyObject(SalesDropdownOptionsService);
  const floatingPanelSpy = createSpyObject(FloatingPanelComponent);
  const dialogServiceSpy = createSpyObject(SalesDownloadDialogService);

  const mockPeriods: Period[] = [
    { id: 1, period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020', isoCountryCode: 'ES' },
    { id: 2, period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020', isoCountryCode: 'ES' }
  ];

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    }
  ];

  const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
    {
      value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
      label: '1111111 / AGENT 1111111'
    },
    {
      value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
      label: '2222222 / AGENT 2222222'
    }
  ];

  const mockCurrencyDropdownOptions: DropdownOption<Currency>[] = [
    {
      value: { id: 1, code: 'EUR', decimals: 2 },
      label: 'EUR'
    },
    {
      value: { id: 1, code: 'USD', decimals: 2 },
      label: 'USD'
    }
  ];

  translationServiceSpy.translate.and.returnValue('The period options are missing.');

  queryableDataSourceSpy.hasData$ = of(true);
  queryableDataSourceSpy.appliedQuery$ = query$;
  dropdownOptionsServiceSpy.getPeriods.and.returnValue(of(mockPeriods));
  dropdownOptionsServiceSpy.getCurrencies.and.returnValue(of([{ value: '', label: '' }]));
  dropdownOptionsServiceSpy.getFilteredCurrencies.and.returnValue(of(mockCurrencyDropdownOptions));
  dropdownOptionsServiceSpy.getAgents.and.returnValue(of(mockAgentDropdownOptions));
  dropdownOptionsServiceSpy.getAirlines.and.returnValue(of(mockAirlineDropdownOptions));
  dropdownOptionsServiceSpy.getUserAirlinesByBsp.and.returnValue(of(mockAirlineDropdownOptions));
  dropdownOptionsServiceSpy.getStatisticalCode.and.returnValue(of([{ value: '', label: '' }]));
  dropdownOptionsServiceSpy.getUserAgent.and.returnValue(of(null));
  dropdownOptionsServiceSpy.getUserAirline.and.returnValue(of(null));

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [Permissions.readNonComparatives, Permissions.lean],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readNonComparatives] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NonComparativeComponent, TranslatePipeMock],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getUser, value: expectedUserDetails }]
        }),
        PermissionsService,
        { provide: NonComparativeService, useValue: nonComparativeServiceSpy },
        FormBuilder,
        { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
        { provide: PeriodPipe, useClass: PeriodPipeMock },
        SalesDisplayFilterFormatter,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: SalesDropdownOptionsService, useValue: dropdownOptionsServiceSpy },
        { provide: SalesDownloadDialogService, useValue: dialogServiceSpy }
      ]
    })
      .overrideComponent(NonComparativeComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            DecimalPipe,
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(NonComparativeComponent);
    component = fixture.componentInstance;

    component.totalsPanel = floatingPanelSpy;
    floatingPanelSpy.close.reset();

    // It's neccesary do it after floatingPanelSpy to ensure that it has been created
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
