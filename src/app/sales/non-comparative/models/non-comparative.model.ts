import { StatisticalCode } from '~app/sales/shared/models';
import { AgentDto } from '~app/shared/models/agent.model';
import { Currency } from '~app/shared/models/currency.model';

export interface NonComparative {
  agent: AgentDto;
  airline: {
    id: number;
    iataCode: string;
    globalName: string;
    localName: string;
  };
  currency: Currency;
  formOfPaymentAmountCa: number;
  formOfPaymentAmountCc: number;
  formOfPaymentAmountCm: number;
  formOfPaymentAmountEp: number;
  formOfPaymentAmountEx: number;
  formOfPaymentAmountMs: number;
  formOfPaymentAmountVd: number;
  statisticalCode: StatisticalCode;
  ticketAmount: number;
}
