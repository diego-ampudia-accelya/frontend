export interface NonComparativeTotals {
  currency: {
    code: string;
    decimals: number;
    id: number;
  };
  formOfPaymentAmountCa: number;
  formOfPaymentAmountCc: number;
  formOfPaymentAmountCm: number;
  formOfPaymentAmountEp: number;
  formOfPaymentAmountMsCa: number;
  formOfPaymentAmountMsCc: number;
  ticketAmount: number;
}
