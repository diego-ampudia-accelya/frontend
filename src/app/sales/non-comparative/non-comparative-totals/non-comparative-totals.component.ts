import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { chain, first } from 'lodash';

import { NonComparativeTotals } from './non-comparative-totals.model';
import { SelectMode } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-non-comparative-totals',
  templateUrl: './non-comparative-totals.component.html',
  styleUrls: ['./non-comparative-totals.component.scss']
})
export class NonComparativeTotalsComponent implements OnChanges {
  @Input() totals: NonComparativeTotals[];

  public currencyOptions: DropdownOption[] = [];
  public currency: string;
  public get data(): NonComparativeTotals {
    const defaultValue = {} as NonComparativeTotals;

    return this.totals ? this.totals.find(total => total.currency.code === this.currency) : defaultValue;
  }

  public selectSmallMode = SelectMode.Small;
  public getAmountFormat = getAmountFormat;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.totals) {
      const totals: NonComparativeTotals[] = changes.totals.currentValue || [];
      this.currencyOptions = chain(totals)
        .map(total => total.currency.code)
        .orderBy(code => code)
        .map(code => ({ value: code, label: code }))
        .value();
      this.currency = first(this.currencyOptions) && first(this.currencyOptions).value;
    }
  }
}
