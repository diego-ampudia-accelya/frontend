import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { NonComparative } from '../models/non-comparative.model';
import { NonComparativeTotals } from '../non-comparative-totals/non-comparative-totals.model';
import { ExportAsModel } from '~app/sales/shared/models';
import { PeriodOption } from '~app/shared/components';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse, formatMonthPeriod, formatPeriod } from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable()
export class NonComparativeService implements Queryable<NonComparative> {
  private baseUrl: string;

  constructor(private http: HttpClient, appConfiguration: AppConfigurationService) {
    this.baseUrl = `${appConfiguration.baseApiPath}/sales-query/gross-sales/non-comparatives`;
  }

  public find(query: DataQuery): Observable<PagedData<NonComparative>> {
    return this.requestData(this.baseUrl, query);
  }

  public getTotals(filterBy: any): Observable<NonComparativeTotals[]> {
    return this.requestData(`${this.baseUrl}/totals`, { filterBy });
  }

  public download(query: DataQuery, exportOptions: ExportAsModel): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, ...exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private requestData<T>(url: string, dataQuery: Partial<DataQuery>): Observable<T> {
    const formattedQuery = this.formatQuery(dataQuery);

    return this.http.get<T>(url + formattedQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery>): RequestQuery {
    const { airline, agent, statisticalCode, currency, bsp, monthPeriod, agentGroup } = query.filterBy;
    const periodRange: PeriodOption[] = query.filterBy.periodRange || [];
    const [periodFrom, periodTo] = periodRange;

    const extractIds = (items: Array<{ id: number }>) => items && items.map(item => item.id);

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        bspId: bsp.id,
        statisticalCode,
        airlineId: extractIds(airline),
        agentId: extractIds(agent),
        currencyId: extractIds(currency),
        billingAnalysisEndingDateFrom: periodFrom && formatPeriod(periodFrom.dateFrom),
        billingAnalysisEndingDateTo: periodTo && formatPeriod(periodTo.dateTo),
        periodYearMonth: monthPeriod && formatMonthPeriod(monthPeriod[0].period),
        agentGroupId: extractIds(agentGroup)
      }
    });
  }
}
