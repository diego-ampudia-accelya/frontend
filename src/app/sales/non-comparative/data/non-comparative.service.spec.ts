import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { NonComparativeService } from './non-comparative.service';
import { DownloadContentType, DownloadExportAsType, ExportAsModel } from '~app/sales/shared/models';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions } from '~app/shared/helpers';
import { Bsp } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

describe('NonComparativeService', () => {
  let nonComparativeService: NonComparativeService;
  let httpSpy: SpyObject<HttpClient>;

  const bspHaiti = { id: 11978, name: 'Haiti' } as Bsp;

  const filterBy = {
    periodRange: [
      {
        id: 47882,
        isoCountryCode: 'HT',
        period: '2019111',
        dateFrom: '11/01/2019',
        dateTo: '11/07/2019',
        disabled: false
      },
      {
        id: 47885,
        isoCountryCode: 'HT',
        period: '2019114',
        dateFrom: '11/23/2019',
        dateTo: '11/30/2019',
        disabled: false
      }
    ],
    agent: [
      {
        id: 76431,
        iataCode: '8350002',
        name: 'Agency 8350002'
      }
    ],
    agentGroup: [
      {
        id: 764312,
        code: '8350003',
        name: 'Agent Group 8350003'
      }
    ],
    currency: [
      {
        id: 10657,
        code: 'HTG',
        decimals: 0,
        version: 0
      }
    ],
    statisticalCode: 'I',
    bsp: bspHaiti
  };

  beforeEach(() => {
    httpSpy = createSpyObject(HttpClient);
    TestBed.configureTestingModule({
      providers: [
        NonComparativeService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: { baseApiPath: '/api' } }
      ]
    });
  });

  beforeEach(() => {
    nonComparativeService = TestBed.inject(NonComparativeService);
  });

  it('should create', () => {
    expect(nonComparativeService).toBeDefined();
  });

  it('should send proper find request', fakeAsync(() => {
    const query = {
      filterBy,
      paginateBy: {
        size: 20,
        page: 0,
        totalElements: 1
      },
      sortBy: []
    };
    httpSpy.get.and.returnValue(of({}));

    nonComparativeService.find(query).subscribe();
    tick();

    const expectedUrl =
      '/api/sales-query/gross-sales/non-comparatives?' +
      'page=0&size=20&' +
      'bspId=11978&' +
      'statisticalCode=I&' +
      'agentId=76431&' +
      'currencyId=10657&' +
      'billingAnalysisEndingDateFrom=2019-11-01&' +
      'billingAnalysisEndingDateTo=2019-11-30&' +
      'agentGroupId=764312';
    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper totals request', fakeAsync(() => {
    httpSpy.get.and.returnValue(of({}));

    nonComparativeService.getTotals(filterBy).subscribe();
    tick();

    const expectedUrl =
      '/api/sales-query/gross-sales/non-comparatives/totals?' +
      'bspId=11978&' +
      'statisticalCode=I&' +
      'agentId=76431&' +
      'currencyId=10657&' +
      'billingAnalysisEndingDateFrom=2019-11-01&' +
      'billingAnalysisEndingDateTo=2019-11-30&' +
      'agentGroupId=764312';
    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper download request', fakeAsync(() => {
    const query: DataQuery = {
      filterBy,
      paginateBy: {
        size: 20,
        page: 0,
        totalElements: 1
      },
      sortBy: []
    };
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy.get.and.returnValue(of(httpResponse));
    const exportAs: ExportAsModel = {
      exportAs: DownloadExportAsType.txt,
      content: [DownloadContentType.details, DownloadContentType.totals]
    };

    nonComparativeService.download(query, exportAs).subscribe();
    tick();

    const expectedUrl =
      '/api/sales-query/gross-sales/non-comparatives/download?' +
      'bspId=11978&' +
      'statisticalCode=I&' +
      'agentId=76431&' +
      'currencyId=10657&' +
      'billingAnalysisEndingDateFrom=2019-11-01&' +
      'billingAnalysisEndingDateTo=2019-11-30&' +
      'agentGroupId=764312&' +
      'exportAs=TXT&' +
      'content=DETAILS,TOTALS';
    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, downloadRequestOptions);
  }));
});
