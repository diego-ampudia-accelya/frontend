import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import FileSaver from 'file-saver';
import { cloneDeep } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { catchError, finalize, first, map, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NonComparativeSalesFilter } from '../shared/models/sales-filter.model';
import { SalesDropdownOptionsService } from '../shared/services';
import { NonComparativeService } from './data/non-comparative.service';
import { NonComparative } from './models/non-comparative.model';
import { NonComparativeTotals } from './non-comparative-totals/non-comparative-totals.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import { SalesDownloadDialogService } from '~app/sales/shared/services/sales-download-dialog.service';
import { ButtonDesign, FloatingPanelComponent, PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { SelectMode } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentGroupSummary, AgentSummary, AirlineSummary, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User, UserType } from '~app/shared/models/user.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-non-comparative',
  templateUrl: './non-comparative.component.html',
  styleUrls: ['./non-comparative.component.scss'],
  providers: [
    DefaultQueryStorage,
    SalesDisplayFilterFormatter,
    { provide: QUERYABLE, useExisting: NonComparativeService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [NonComparativeService]
    }
  ]
})
export class NonComparativeComponent implements OnInit, OnDestroy {
  @ViewChild(FloatingPanelComponent, { static: true })
  public totalsPanel: FloatingPanelComponent;

  @ViewChild('amountTemplate', { static: true })
  private amountTemplate: TemplateRef<any>;

  public searchForm: FormGroup;
  public columns$: Observable<Array<TableColumn>>;

  public hasLeanPermission: boolean;
  public isUserAirline: boolean;
  public isUserAgent: boolean;
  public isMonthPeriodPicker: boolean;
  public isTotalsLoading: boolean;
  public isBspFilterLocked: boolean;

  public predefinedUsersAirline: AirlineSummary[] = [];
  public predefinedUserAgent: AgentSummary[] = [];

  public currentPeriod: PeriodOption;
  public selectedBsps: Bsp[];

  public statisticalCodeDropdownOptions: DropdownOption[];
  public userBspOptions: DropdownOption<Bsp>[] = [];
  public airlineDropdownOptions$: Observable<DropdownOption[]>;
  public agentDropdownOptions$: Observable<DropdownOption[]>;
  public agentGroupDropdownOptions$: Observable<DropdownOption[]>;
  public currencyOptions: DropdownOption<Currency>[];
  public periodDropdownOptions: PeriodOption[];
  private periodOptions$: Observable<PeriodOption[]>;

  public predefinedFilters: {
    bsp?: Bsp | Bsp[];
    monthPeriod?: PeriodOption[];
    periodRange?: PeriodOption[];
  };

  public buttonDesign = ButtonDesign;

  public selectMode = SelectMode;
  public totalsData: NonComparativeTotals[];

  public getAmountFormat = getAmountFormat;

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  public periodPickerYearsBack: number;

  private loggedUser: User;
  private storedQuery: DataQuery<{ [key: string]: any }>;

  private get currentPeriodValue(): PeriodOption[] {
    const currentPeriod = PeriodUtils.findCurrentPeriod(this.periodDropdownOptions || []);

    return currentPeriod ? [currentPeriod, currentPeriod] : null;
  }

  public noticeDisclaimer$: Observable<string> = this.dataSource?.appliedQuery$.pipe(
    map(query => (query && this.isFilterByCurrentPeriod() ? 'sales.tableNoticeMsg' : null))
  );

  public formFactory: FormUtil;

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<NonComparative>,
    public displayFilterFormatter: SalesDisplayFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private dropdownOptionsService: SalesDropdownOptionsService,
    private permissionsService: PermissionsService,
    private formBuilder: FormBuilder,
    private nonComparativeService: NonComparativeService,
    private salesDownloadDialogService: SalesDownloadDialogService,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.searchForm = this.buildSearchForm();
    this.storedQuery = this.queryStorage.get();

    this.initializePermissions();
    this.initializeLoggedUserFeatures();

    this.columns$ = this.buildColumns();

    this.setPeriodPickerYearsBack();
  }

  private initializeLoggedUserFeatures(): void {
    this.store.pipe(select(getUser), first()).subscribe(loggedUser => {
      this.loggedUser = loggedUser;

      this.initializeBspFeatures();
      this.getStoredBsps();
      this.populateFilterDropdowns();
      this.setPredefinedFilters();
      this.initializeDataQuery();
    });
  }

  /** Restore BSP selection if user change between open tabs and transform it to array*/
  private getStoredBsps(): void {
    if (this.storedQuery) {
      const storedBsp = this.storedQuery.filterBy.bsp;
      const storedBspToArray = storedBsp ? (Array.isArray(storedBsp) ? storedBsp : [storedBsp]) : [];
      this.selectedBsps = storedBspToArray || this.selectedBsps;
    }
  }

  /** Set predefined filters adding `periodRange`, only available if user has not LEAN permission */
  private setPredefinedFilters(): void {
    if (this.hasLeanPermission) {
      return;
    }

    this.predefinedFilters = {
      bsp: this.selectedBsps[0],
      periodRange: this.currentPeriodValue
    };
  }

  private initializePermissions(): void {
    this.isUserAirline = this.permissionsService.hasUserType(UserType.AIRLINE);
    this.isUserAgent = this.permissionsService.hasUserType(UserType.AGENT);
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeBspFeatures(): void {
    if (this.hasLeanPermission) {
      this.initializeBspListener();
    }
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    this.initializeSelectedBsps();

    this.isBspFilterLocked = !this.hasLeanPermission;
    this.isMonthPeriodPicker = this.selectedBsps.length !== 1;
  }

  private initializeBspListener(): void {
    FormUtil.get<NonComparativeSalesFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(async value => {
        this.selectedBsps = value ? (Array.isArray(value) ? value : [value]) : [];
        this.isMonthPeriodPicker = this.selectedBsps.length !== 1;

        this.populateFilterDropdowns();
        this.updateFilterValues();
      });
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readNonComparatives;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private initializeSelectedBsps(): void {
    const firstListBsp = this.userBspOptions[0]?.value;
    const defaultBsp = this.userBspOptions.find(
      ({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc
    )?.value;

    this.selectedBsps = [firstListBsp];

    if (this.userBspOptions.length > 1) {
      this.selectedBsps = this.hasLeanPermission ? [] : [defaultBsp || firstListBsp];
    }
  }

  private initializeDataQuery(): void {
    // It's necessary load periods before in order to load Data successfully
    this.periodOptions$.subscribe(data => {
      this.periodDropdownOptions = data;
      this.loadData(this.storedQuery);
    });
  }

  /** Since this method is only available for Airline users with LEAN permission, we do not need to update Airline filter */
  private updateFilterValues(): void {
    this.updatePeriodFilterValue();
    this.updateAgentFilterValue();
    this.updateCurrencyFilterValue();
    if (this.isUserAirline) {
      this.updateAgentGroupFilterValue();
    }
  }

  private updatePeriodFilterValue(): void {
    const periodRangeControl = FormUtil.get<NonComparativeSalesFilter>(this.searchForm, 'periodRange');
    const monthPeriodControl = FormUtil.get<NonComparativeSalesFilter>(this.searchForm, 'monthPeriod');

    if (this.selectedBsps.length === 1) {
      periodRangeControl.patchValue(periodRangeControl.value || this.currentPeriodValue);
      monthPeriodControl.reset();
    } else {
      monthPeriodControl.patchValue(monthPeriodControl.value || this.currentPeriodValue);
      periodRangeControl.reset();
    }
  }

  private updateCurrencyFilterValue(): void {
    // If there are not selected BSPs, dropdown has all currencies from all BSPs so there is no reason to patch currency control value
    if (!this.selectedBsps.length) {
      return;
    }

    const currencyControl = FormUtil.get<NonComparativeSalesFilter>(this.searchForm, 'currency');
    const currenciesSelected: Currency[] = currencyControl.value;

    if (currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyOptions.some(({ value }) => value.id === currency.id)
      );
      currencyControl.patchValue(currenciesToPatch);
    }
  }

  private updateAgentFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentControl = FormUtil.get<NonComparativeSalesFilter>(this.searchForm, 'agent');
    const agentsSelected: AgentSummary[] = agentControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.selectedBsps.some(({ id }) => agent.bsp?.id === id));
      agentControl.patchValue(agentsToPatch);
    }
  }

  private updateAgentGroupFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentGroupControl = FormUtil.get<NonComparativeSalesFilter>(this.searchForm, 'agentGroup');
    const agentsGroupSelected: AgentGroupSummary[] = agentGroupControl.value;

    if (agentsGroupSelected?.length) {
      const agentsGroupToPatch = agentsGroupSelected.filter(agent =>
        this.selectedBsps.some(({ id }) => agent.bsp?.id === id)
      );
      agentGroupControl.patchValue(agentsGroupToPatch);
    }
  }

  public loadData(query: DataQuery<NonComparativeSalesFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.handleQueryFilter(query.filterBy)
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);

    this.totalsPanel.close();
  }

  private handleQueryFilter(queryFilter: Partial<NonComparativeSalesFilter>): Partial<NonComparativeSalesFilter> {
    // BSP filter is always an array if user has LEAN permission
    const filteredBsps = (queryFilter.bsp || this.selectedBsps) as Bsp[];

    return {
      ...this.predefinedFilters,
      ...queryFilter,
      monthPeriod: this.selectedBsps.length !== 1 ? queryFilter.monthPeriod || this.currentPeriodValue : null,
      periodRange: this.selectedBsps.length === 1 ? queryFilter.periodRange || this.currentPeriodValue : null,
      ...(this.hasLeanPermission && { bsp: filteredBsps }),
      ...(this.isUserAirline && { airline: this.predefinedUsersAirline.length ? this.predefinedUsersAirline : null }),
      ...(this.isUserAgent && { agent: this.predefinedUserAgent.length ? this.predefinedUserAgent : null })
    };
  }

  private initializeUserFilters(bspIds: number[]): void {
    if (this.isUserAirline) {
      // If the user is an AIRLINE, add the airlineIds as a filter.
      this.dropdownOptionsService
        .getUserAirlinesByBsp(bspIds)
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          this.predefinedUsersAirline = data;
          this.displayFilterFormatter.options.showAirline = !data;
        });
    }

    if (this.isUserAgent) {
      // If the user is an AGENT, add the agentId as a filter.
      this.dropdownOptionsService
        .getUserAgent()
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          this.predefinedUserAgent = [data];
          this.displayFilterFormatter.options.showAgent = !data;
        });
    }
  }

  public async onTotalsPanelOpened(): Promise<void> {
    this.totalsData = await this.getTotalData().toPromise();
  }

  public onTotalsPanelClosed(): void {
    this.totalsData = null;
  }

  public onDownload() {
    combineLatest([this.salesDownloadDialogService.openDownloadDialog(), this.dataSource.appliedQuery$])
      .pipe(
        first(),
        switchMap(([exportAs, query]) => this.nonComparativeService.download(query, exportAs))
      )
      .subscribe(fileInfo => {
        FileSaver.saveAs(fileInfo.blob, fileInfo.fileName);
      });
  }

  private populateFilterDropdowns() {
    const bspSelectedIds = this.selectedBsps && this.selectedBsps.length ? this.selectedBsps.map(bsp => bsp.id) : [];
    const firstBsp = this.selectedBsps?.length ? this.selectedBsps[0] : this.userBspOptions[0]?.value;
    const params = bspSelectedIds.length ? { bspId: bspSelectedIds } : null;
    this.statisticalCodeDropdownOptions = this.dropdownOptionsService.getStatisticalCode();

    this.agentGroupDropdownOptions$ = this.dropdownOptionsService.getAgentGroups(params);
    this.airlineDropdownOptions$ = this.dropdownOptionsService.getAirlines(firstBsp);
    this.agentDropdownOptions$ = this.getAgentDropDownByUserType(params);
    this.dropdownOptionsService
      .getFilteredCurrencies(params)
      .subscribe(currencies => (this.currencyOptions = currencies));
    this.periodOptions$ = this.dropdownOptionsService.getPeriods(firstBsp);

    const bspsToRetrieveAirlinesId = bspSelectedIds.length
      ? bspSelectedIds
      : this.userBspOptions.map(data => data.value?.id);

    this.initializeUserFilters(bspsToRetrieveAirlinesId);
  }

  private getAgentDropDownByUserType(params: { bspId: number[] }): Observable<DropdownOption<AgentSummary>[]> {
    const isAgentGroupUser = this.permissionsService.hasUserType(UserType.AGENT_GROUP);

    return isAgentGroupUser ? this.getAgentGroupDropDown(params) : this.dropdownOptionsService.getAgents(params);
  }

  private getAgentGroupDropDown(queryfilters): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = Permissions.readAgentDictionary;
    const aGroupUserHasPermission = this.permissionsService.hasPermission(aGroupUserPermission);

    if (aGroupUserHasPermission) {
      const filter = { ...queryfilters, permission: aGroupUserPermission };

      return this.dropdownOptionsService.getAgentGroupsAgents(filter);
    } else {
      return of(null);
    }
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<NonComparativeSalesFilter>({
      bsp: [],
      airline: [],
      agent: [],
      agentGroup: [],
      currency: [],
      statisticalCode: [],
      periodRange: [],
      monthPeriod: []
    });
  }

  private buildColumns(): Observable<GridColumn[]> {
    const numericColumn: GridColumn = {
      resizeable: true,
      cellTemplate: this.amountTemplate,
      cellClass: 'text-right'
    };

    const columns: GridColumn[] = [
      {
        prop: 'bsp.isoCountryCode',
        name: 'sales.nonComparatives.columns.bsp',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      },
      {
        prop: 'airline.iataCode',
        name: 'sales.nonComparatives.columns.airlineCode',
        resizeable: true,
        hidden: this.isUserAirline,
        minWidth: 100,
        flexGrow: 0.3
      },
      {
        prop: 'airline.localName',
        name: 'sales.nonComparatives.columns.airlineName',
        resizeable: true,
        hidden: this.isUserAirline,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'agent.agentGroup.code',
        name: 'sales.nonComparatives.columns.agentGroupCode',
        resizeable: true,
        hidden: this.isUserAgent,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'agent.agentGroup.name',
        name: 'sales.nonComparatives.columns.agentGroupName',
        resizeable: true,
        hidden: this.isUserAgent,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'agent.iataCode',
        name: 'sales.nonComparatives.columns.agentCode',
        resizeable: true,
        hidden: this.isUserAgent,
        minWidth: 100,
        flexGrow: 0.3
      },
      {
        prop: 'agent.name',
        name: 'sales.nonComparatives.columns.agentName',
        resizeable: true,
        hidden: this.isUserAgent,
        minWidth: 150,
        flexGrow: 0.8
      },
      {
        prop: 'statisticalCode',
        name: 'sales.nonComparatives.columns.salesType',
        resizeable: true,
        minWidth: 105,
        flexGrow: 0.6,
        pipe: {
          transform: value => this.dropdownOptionsService.translateStatisticalCode(value)
        }
      },
      {
        prop: 'currency.code',
        name: 'sales.nonComparatives.columns.currency',
        resizeable: true,
        minWidth: 105,
        flexGrow: 0.4
      },
      {
        ...numericColumn,
        prop: 'formOfPaymentAmountCa',
        name: 'sales.nonComparatives.columns.totalCash',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 0.4
      },
      {
        ...numericColumn,
        prop: 'formOfPaymentAmountCc',
        name: 'sales.nonComparatives.columns.totalCredit',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 0.4
      },
      {
        ...numericColumn,
        prop: 'formOfPaymentAmountEp',
        name: 'sales.nonComparatives.columns.totalEasyPay',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 0.4
      },
      {
        ...numericColumn,
        prop: 'formOfPaymentAmountMsCa',
        name: 'sales.nonComparatives.columns.totalMiscCash',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 0.4
      },
      {
        ...numericColumn,
        prop: 'formOfPaymentAmountMsCc',
        name: 'sales.nonComparatives.columns.totalMiscCredit',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 0.4
      },
      {
        ...numericColumn,
        prop: 'formOfPaymentAmountCm',
        name: 'sales.nonComparatives.columns.totalCreditTurnedCash',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 0.4
      },
      {
        ...numericColumn,
        prop: 'ticketAmount',
        name: 'sales.nonComparatives.columns.total',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 0.4
      }
    ];

    return of(columns);
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private isFilterByCurrentPeriod(): boolean {
    const storedQuery = this.queryStorage.get();

    return storedQuery?.filterBy.periodRange?.some(periodRange => this.currentPeriod?.period === periodRange.period);
  }

  private getTotalData(): Observable<NonComparativeTotals[]> {
    return this.dataSource.appliedQuery$.pipe(
      first(),
      tap(() => (this.isTotalsLoading = true)),
      switchMap(query => this.nonComparativeService.getTotals(query.filterBy)),
      catchError(() => of(null)),
      finalize(() => (this.isTotalsLoading = false))
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
