import { DecimalPipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { SalesFilter } from '../shared/models/sales-filter.model';
import { SalesDisplayFilterFormatter } from '../shared/sales-display-filter-formatter';
import { SalesDropdownOptionsService } from '../shared/services';
import { SalesDownloadDialogService } from '../shared/services/sales-download-dialog.service';
import { ScuSummaryService } from './data/scu-summary.service';
import { ScuSummaryComponent } from './scu-summary.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { FloatingPanelComponent, PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { ROUTES } from '~app/shared/constants/routes';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('ScuSummaryComponent', () => {
  let component: ScuSummaryComponent;
  let fixture: ComponentFixture<ScuSummaryComponent>;
  const translationServiceSpy = jasmine.createSpyObj<L10nTranslationService>('L10nTranslationService', ['translate']);
  const salesDropdownServiceSpy = createSpyObject(SalesDropdownOptionsService);
  const scuSummaryServiceMock = createSpyObject(ScuSummaryService);
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const dialogServiceSpy = createSpyObject(SalesDownloadDialogService);
  const floatingPanelSpy = createSpyObject(FloatingPanelComponent);

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [Permissions.readScuSummary, Permissions.lean],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readScuSummary] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const mockPeriods: Period[] = [
    { id: 1, period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020', isoCountryCode: 'ES' },
    { id: 2, period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020', isoCountryCode: 'ES' }
  ];

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    }
  ];

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    core: {
      menu: {
        tabs: { dashboard: { ...ROUTES.DASHBOARD, id: 'dashboard' } },
        activeTabId: 'dashboard'
      },
      viewListsInfo: {}
    }
  };

  const mockAirlineOptions: AirlineSummary[] = [
    {
      id: 1,
      name: 'AIRLINE 001',
      code: '001',
      designator: 'L1'
    },
    {
      id: 2,
      name: 'AIRLINE 002',
      code: '002',
      designator: 'L2'
    }
  ];

  const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
    {
      value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
      label: '1111111 / AGENT 1111111'
    },
    {
      value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
      label: '2222222 / AGENT 2222222'
    }
  ];

  const mockAgentOptions: AgentSummary[] = [
    {
      id: '1',
      name: 'AGENT 1111111',
      code: '1111111'
    },
    {
      id: '2',
      name: 'AGENT 2222222',
      code: '2222222'
    }
  ];

  const query$ = of({
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  } as DataQuery);

  queryableDataSourceSpy.hasData$ = of(true);
  queryableDataSourceSpy.appliedQuery$ = query$;

  salesDropdownServiceSpy.getDocumentTypes.and.returnValue(of([]));
  salesDropdownServiceSpy.getFormOfPayment.and.returnValue(of([{ value: '', label: '' }]));
  salesDropdownServiceSpy.getStatisticalCode.and.returnValue(of([{ value: '', label: '' }]));
  salesDropdownServiceSpy.getAgents.and.returnValue(of(mockAgentDropdownOptions));
  salesDropdownServiceSpy.getAirlines.and.returnValue(of(mockAirlineDropdownOptions));
  salesDropdownServiceSpy.getPeriods.and.returnValue(of(mockPeriods));
  salesDropdownServiceSpy.getUserAirlinesByBsp.and.returnValue(of(mockAirlineOptions));
  salesDropdownServiceSpy.getUserAgent.and.returnValue(of(mockAgentOptions));
  salesDropdownServiceSpy.getAgentGroupsAgents.and.returnValue(of(mockAgentDropdownOptions));

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ScuSummaryComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        DecimalPipe,
        SalesDisplayFilterFormatter,
        PermissionsService,
        provideMockStore({ initialState, selectors: [{ selector: getUser, value: expectedUserDetails }] }),
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: PeriodPipe, useClass: PeriodPipeMock },
        { provide: ScuSummaryService, useValue: scuSummaryServiceMock },
        { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
        { provide: SalesDropdownOptionsService, useValue: salesDropdownServiceSpy },
        { provide: SalesDownloadDialogService, useValue: dialogServiceSpy }
      ]
    })
      .overrideComponent(ScuSummaryComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            DecimalPipe,
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScuSummaryComponent);
    component = fixture.componentInstance;
    component.totalsPanel = floatingPanelSpy;
    floatingPanelSpy.close.reset();

    // It's neccesarry do it after floatingPanelSpy to ensure that it has been created
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should set correct columns on init as an AGENT', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'bsp.isoCountryCode',
        name: 'sales.billingStatement.columns.bsp',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        name: 'sales.scuSummary.columns.agentCode',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'agent.name',
        name: 'sales.scuSummary.columns.agentName',
        resizeable: true,
        minWidth: 150,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'transactionCode',
        name: 'sales.scuSummary.columns.documentType',
        resizeable: true,
        cellTemplate: 'tooltipCellTmpl',
        minWidth: 115,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'statisticalCode',
        name: 'sales.scuSummary.columns.salesType',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'formOfPayment',
        name: 'sales.scuSummary.columns.formOfPayment',
        resizeable: true,
        minWidth: 120,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'totalTransactions',
        name: 'sales.scuSummary.columns.quantity',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 1
      })
    ]);

    let columns = [];
    component.ngOnInit();
    component.isUserAirline = false;
    component.isUserAgent = true;
    component['buildColumns']().subscribe(data => (columns = data));
    tick();
    expect(columns).toEqual(expectedColumns);
  }));

  it('should set correct columns on init as an AIRLINE', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'bsp.isoCountryCode',
        name: 'sales.billingStatement.columns.bsp',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        name: 'sales.scuSummary.columns.agentCode',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'agent.name',
        name: 'sales.scuSummary.columns.agentName',
        resizeable: true,
        minWidth: 150,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'transactionCode',
        name: 'sales.scuSummary.columns.documentType',
        resizeable: true,
        cellTemplate: 'tooltipCellTmpl',
        minWidth: 115,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'statisticalCode',
        name: 'sales.scuSummary.columns.salesType',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'formOfPayment',
        name: 'sales.scuSummary.columns.formOfPayment',
        resizeable: true,
        minWidth: 120,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'totalTransactions',
        name: 'sales.scuSummary.columns.quantity',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'airline.iataCode',
        name: 'sales.scuSummary.columns.airlineCode',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'airline.localName',
        name: 'sales.scuSummary.columns.airlineName',
        resizeable: true,
        minWidth: 150,
        flexGrow: 1
      })
    ]);

    let columns = [];
    component.ngOnInit();
    component.isUserAirline = true;
    component.isUserAgent = false;
    component['buildColumns']().subscribe(data => (columns = data));
    tick();
    expect(columns).toEqual(expectedColumns);
  }));

  it('should initialize BSP listener if user has LEAN permission', fakeAsync(() => {
    const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

    component.ngOnInit();
    component.searchForm.get('bsp').patchValue(mockBsp);
    tick();

    expect(component.selectedBsps).toEqual([mockBsp]);
  }));

  describe('filter dropdowns initialization', () => {
    beforeEach(() => {
      salesDropdownServiceSpy.getPeriods.calls.reset();
      salesDropdownServiceSpy.getFilteredCurrencies.calls.reset();
      salesDropdownServiceSpy.getAgents.calls.reset();
      salesDropdownServiceSpy.getAgentGroupsAgents.calls.reset();
      salesDropdownServiceSpy.getAirlines.calls.reset();
    });

    it('should initialize PERIOD options dropdown', fakeAsync(() => {
      const expectedPeriodOptions: any = [
        jasmine.objectContaining({ period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' }),
        jasmine.objectContaining({ period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' })
      ];

      component.ngOnInit();
      let options;
      component.periodOptions$.subscribe(data => (options = data));
      tick();

      expect(salesDropdownServiceSpy.getPeriods).toHaveBeenCalledWith(expectedUserDetails.bsps[0]);
      expect(options).toEqual(expectedPeriodOptions);
    }));

    it('should initialize AGENT dropdown ', fakeAsync(() => {
      let agentDropdownOptions: DropdownOption<AgentSummary>[];

      component.ngOnInit();
      tick();

      component.agentDropdownOptions$.subscribe(options => (agentDropdownOptions = options));

      expect(salesDropdownServiceSpy.getAgents).toHaveBeenCalledWith({ bspId: [1] });
      expect(agentDropdownOptions).toEqual(mockAgentDropdownOptions);
    }));

    it('should initialize AIRLINE dropdown', fakeAsync(() => {
      let airlineDropdownOptions: DropdownOption<AirlineSummary>[];

      component.ngOnInit();
      tick();

      component.airlineDropdownOptions$.subscribe(options => (airlineDropdownOptions = options));

      expect(salesDropdownServiceSpy.getAirlines).toHaveBeenCalledWith(expectedUserDetails.bsps[0]);
      expect(airlineDropdownOptions).toEqual(mockAirlineDropdownOptions);
    }));
  });

  describe('set predefined filters', () => {
    it('should set predefined filters if user has not LEAN permission', fakeAsync(() => {
      const mockCurrentPeriod: PeriodOption = { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' };

      component['hasLeanPermission'] = false;
      spyOn(PeriodUtils, 'findCurrentPeriod').and.returnValue(mockCurrentPeriod);

      component.ngOnInit();
      tick();

      expect(component.predefinedFilters).toEqual({
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
        periodRange: [mockCurrentPeriod, mockCurrentPeriod]
      });
    }));

    it('should set predefined filters if user has LEAN permission', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(component.predefinedFilters).toBeUndefined();
    }));
  });

  describe('load data', () => {
    const mockCurrentPeriod: PeriodOption = { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' };
    const storedQuery: DataQuery<SalesFilter> = {
      ...defaultQuery
    };

    beforeEach(() => {
      spyOn(PeriodUtils, 'findCurrentPeriod').and.returnValue(mockCurrentPeriod);
      queryStorageSpy.get.and.returnValue(storedQuery);
    });

    it('should add `periodRange` to query filter if user has LEAN permission and there is one filtered BSP', fakeAsync(() => {
      const expectedQuery = {
        filterBy: jasmine.objectContaining({
          ...storedQuery.filterBy,
          bsp: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }],
          periodRange: [mockCurrentPeriod, mockCurrentPeriod]
        }),
        paginateBy: storedQuery.paginateBy,
        sortBy: storedQuery.sortBy
      };

      component.ngOnInit();
      tick();

      expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
      expect(queryStorageSpy.save).toHaveBeenCalledWith(expectedQuery);
    }));
  });

  it('should open download dialog correctly on download', () => {
    const query: DataQuery<SalesFilter> = {
      ...defaultQuery
    };

    queryStorageSpy.get.and.returnValue(query);
    component.onDownload();

    expect(dialogServiceSpy.openDownloadDialog).toHaveBeenCalled();
  });

  it('should return bsp, periodRange and airline on handleQueryFilter if AIRLINE has LEAN permission and it has only one bsp selected', () => {
    const filterBy: Partial<SalesFilter> = {
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
    };

    const currentPeriod = component['currentPeriodValue'];

    const expectedFilterBy: Partial<SalesFilter> = {
      ...filterBy,
      periodRange: currentPeriod,
      monthPeriod: null,
      airline: mockAirlineOptions
    };

    const result = component['handleQueryFilter'](filterBy);

    expect(result).toEqual(expectedFilterBy);
  });

  it('should return monthRange on handleQueryFilter if AIRLINE has LEAN permission and it has any bsp selected', () => {
    const filterBy: Partial<SalesFilter> = {};
    component.selectedBsps = [];
    component.isBspFilterMultiple = true;

    const currentPeriod = component['currentPeriodValue'];

    const expectedFilterBy: Partial<SalesFilter> = {
      ...filterBy,
      periodRange: null,
      bsp: [],
      monthPeriod: currentPeriod,
      airline: mockAirlineOptions
    };

    const result = component['handleQueryFilter'](filterBy);

    expect(result).toEqual(expectedFilterBy);
  });

  it('should return bsp, periodRange and agent on handleQueryFilter if AGENT NOT has LEAN permission', () => {
    component['loggedUser'] = {
      ...expectedUserDetails,
      permissions: [Permissions.readScuSummary],
      userType: UserType.AGENT
    } as User;
    component.isUserAirline = false;
    component.isUserAgent = true;
    component.isBspFilterMultiple = false;
    component['hasLeanPermission'] = false;
    component.predefinedUsersAirline = [];
    component.predefinedUserAgent = mockAgentOptions;

    const filterBy: Partial<SalesFilter> = {
      bsp: expectedUserDetails.bsps[0]
    };

    const currentPeriod = component['currentPeriodValue'];

    const expectedFilterBy: Partial<SalesFilter> = {
      ...filterBy,
      periodRange: currentPeriod,
      monthPeriod: null,
      agent: mockAgentOptions
    };

    const result = component['handleQueryFilter'](filterBy);

    expect(result).toEqual(expectedFilterBy);
  });
});
