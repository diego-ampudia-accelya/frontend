import {
  ScuBreakdownType,
  ScuTotals,
  ScuTotalsByDocumentType,
  ScuTotalsByFormOfPayment,
  ScuTotalsType
} from '../models';
import { FormOfPayment } from '~app/sales/shared/models';

export const convertDocumentTypeTotals = (totals: any): ScuTotals => {
  const { breakdown = [], totalTransactions, totalIndustry } = totals;
  const propertyMap = {
    TICKETS: 'tickets',
    VOID_TICKETS: 'voidTickets',
    EMDS: 'emds',
    REFUNDS: 'refunds',
    ADMS: 'adms',
    ACMS: 'acms'
  };

  const breakdownByDocumentType: ScuTotalsByDocumentType = {
    industryPercentage: totalIndustry / 100,
    total: totalTransactions,
    ...breakdown.reduce((result, current) => {
      const property = propertyMap[current.documentType];
      result[property] = current.transactionsCount;

      return result;
    }, {})
  };

  return { data: [breakdownByDocumentType], breakdownType: ScuBreakdownType.DocumentType };
};

export const convertFormOfPaymentTotals = (totals: any): ScuTotals => ({
  data: [
    {
      ...convertTotalsEntry(totals.issuesAndAdms),
      type: ScuTotalsType.Issue,
      industryPercentage: totals.issuesAndAdmsIndustry / 100
    },
    {
      ...convertTotalsEntry(totals.refundsAndAcms),
      type: ScuTotalsType.Refund,
      industryPercentage: totals.refundsAndAcmsIndustry / 100
    },
    {
      ...convertTotalsEntry(totals.totals),
      type: ScuTotalsType.All,
      industryPercentage: totals.totalIndustry / 100
    }
  ],
  breakdownType: ScuBreakdownType.FormOfPayment
});

const convertTotalsEntry = (totalsEntry: any = {}) => {
  const { breakdown = [], totalTransactions, totalIndustry } = totalsEntry;
  const propertyMap: { [key: string]: keyof ScuTotalsByFormOfPayment } = {
    [FormOfPayment.Cash]: 'cash',
    [FormOfPayment.PaymentCard]: 'paymentCard',
    [FormOfPayment.CreditTurnedCash]: 'creditTurnedCash',
    [FormOfPayment.EasyPay]: 'easyPay',
    [FormOfPayment.Exchange]: 'exchange',
    [FormOfPayment.Void]: 'void'
  };

  return {
    industryPercentage: totalIndustry,
    total: totalTransactions,
    ...breakdown.reduce((result, current) => {
      const property = propertyMap[current.formOfPayment];
      result[property] = current.totalCount;

      return result;
    }, {})
  };
};
