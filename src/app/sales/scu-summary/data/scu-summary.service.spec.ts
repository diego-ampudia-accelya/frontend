import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { ScuSummaryService } from './scu-summary.service';
import { DownloadContentType, DownloadExportAsType, ExportAsModel } from '~app/sales/shared/models';
import { DataQuery } from '~app/shared/components/list-view';
import { AppConfigurationService } from '~app/shared/services';

const filterByQuery = {
  periodRange: [
    {
      id: 65542020041,
      period: '2020041',
      dateFrom: '03/30/2020',
      dateTo: '04/05/2020'
    },
    {
      id: 65542020044,
      period: '2020044',
      dateFrom: '04/20/2020',
      dateTo: '04/26/2020'
    }
  ],
  airline: [
    { id: 6554485649, name: 'QANTAS', code: '081', designator: 'QF' },
    { id: 6554485648, name: 'AIRLINE TEST', code: '080', designator: 'LO' }
  ],
  agent: [
    { id: 65540230010, name: 'AGENT TEST', code: '0230010' },
    { id: 65540230009, name: 'AGENT TEST', code: '0230009' }
  ],
  documentType: ['EMDS', 'TKTT'],
  formOfPayment: ['CA', 'CC'],
  statisticalCode: 'D',
  bsp: {
    id: 6554,
    isoCountryCode: 'A6',
    name: 'AUSTRALIA'
  }
};

describe('ScuSummaryService', () => {
  let scuSummaryService: ScuSummaryService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of(httpResponse));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        ScuSummaryService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ]
    });

    scuSummaryService = TestBed.inject(ScuSummaryService);
  });

  it('should create', () => {
    expect(scuSummaryService).toBeDefined();
  });

  it('should send proper find request', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };
    const expectedUrl =
      '/sales-query/scus?page=0&size=20&formOfPayment=CA,CC&bspId=6554&statisticalCode=D&transactionCode=EMDS,TKTT&' +
      'billingAnalysisEndingDateFrom=2020-03-30&billingAnalysisEndingDateTo=2020-04-26&' +
      'airlineId=6554485649,6554485648&agentId=65540230010,65540230009';

    scuSummaryService.find(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper request when getTotalsByDocumentType() is called', fakeAsync(() => {
    const expectedUrl =
      '/sales-query/scus/document-types/totals?formOfPayment=CA,CC&bspId=6554&statisticalCode=D&transactionCode=EMDS,TKTT&' +
      'billingAnalysisEndingDateFrom=2020-03-30&billingAnalysisEndingDateTo=2020-04-26&' +
      'airlineId=6554485649,6554485648&agentId=65540230010,65540230009';

    scuSummaryService.getTotalsByDocumentType(filterByQuery).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper request when getTotalsByFormOfPayment() is called', fakeAsync(() => {
    const expectedUrl =
      '/sales-query/scus/forms-of-payment/totals?formOfPayment=CA,CC&bspId=6554&statisticalCode=D&' +
      'transactionCode=EMDS,TKTT&billingAnalysisEndingDateFrom=2020-03-30&billingAnalysisEndingDateTo=2020-04-26&' +
      'airlineId=6554485649,6554485648&agentId=65540230010,65540230009';

    scuSummaryService.getTotalsByFormOfPayment(filterByQuery).subscribe();
    tick();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };
    const exportAs: ExportAsModel = {
      exportAs: DownloadExportAsType.txt,
      content: [DownloadContentType.details, DownloadContentType.totals]
    };
    const expectedUrl =
      '/sales-query/scus/download?formOfPayment=CA,CC&bspId=6554&statisticalCode=D&' +
      'transactionCode=EMDS,TKTT&billingAnalysisEndingDateFrom=2020-03-30&billingAnalysisEndingDateTo=2020-04-26&' +
      'airlineId=6554485649,6554485648&agentId=65540230010,65540230009&exportAs=TXT&content=DETAILS,TOTALS';

    scuSummaryService.download(query, exportAs).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
