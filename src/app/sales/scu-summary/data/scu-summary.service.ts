import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import moment from 'moment-mini';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ScuSummary, ScuTotals } from '../models';
import { convertDocumentTypeTotals, convertFormOfPaymentTotals } from './scu-total-converters';
import { ExportAsModel } from '~app/sales/shared/models';
import { PeriodOption } from '~app/shared/components';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, extractId, formatDownloadResponse, formatMonthPeriod } from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable()
export class ScuSummaryService implements Queryable<ScuSummary> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/sales-query/scus`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery): Observable<PagedData<ScuSummary>> {
    return this.requestData(this.baseUrl, query);
  }

  public getTotalsByDocumentType(filterBy: any): Observable<ScuTotals> {
    return this.requestData<any>(`${this.baseUrl}/document-types/totals`, { filterBy }).pipe(
      map(convertDocumentTypeTotals)
    );
  }

  public getTotalsByFormOfPayment(filterBy: any): Observable<ScuTotals> {
    return this.requestData<any>(`${this.baseUrl}/forms-of-payment/totals`, { filterBy }).pipe(
      map(convertFormOfPaymentTotals)
    );
  }

  public download(query: DataQuery, exportOptions: ExportAsModel): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, ...exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery>): RequestQuery {
    const { airline, agent, documentType, statisticalCode, formOfPayment, bsp, monthPeriod } = query.filterBy;
    const periodRange: PeriodOption[] = query.filterBy.periodRange || [];
    const [periodFrom, periodTo] = periodRange;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        formOfPayment,
        bspId: bsp && (!Array.isArray(bsp) ? [bsp] : bsp).map(({ id }) => id),
        statisticalCode,
        transactionCode: documentType,
        billingAnalysisEndingDateFrom: periodFrom && this.formatPeriod(periodFrom.dateFrom),
        billingAnalysisEndingDateTo: periodTo && this.formatPeriod(periodTo.dateTo),
        periodYearMonth: monthPeriod && formatMonthPeriod(monthPeriod[0].period),
        airlineId: extractId(airline),
        agentId: extractId(agent)
      }
    });
  }

  private requestData<T>(url: string, dataQuery: Partial<DataQuery>): Observable<T> {
    const formattedQuery = this.formatQuery(dataQuery);

    return this.http.get<T>(url + formattedQuery.getQueryString());
  }

  private formatPeriod(date: string): string {
    return date ? moment(date, 'MM/DD/YYYY').format('YYYY-MM-DD') : null;
  }
}
