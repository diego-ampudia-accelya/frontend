import { ScuBreakdownType, ScuTotals, ScuTotalsType } from '../models';

import { convertDocumentTypeTotals, convertFormOfPaymentTotals } from './scu-total-converters';
import { FormOfPayment } from '~app/sales/shared/models';

describe('ScuTotalConverters convertDocumentTypeTotals', () => {
  it('should convert the values properly', () => {
    const totals: any = {
      breakdown: [
        { documentType: 'TICKETS', transactionsCount: 51018 },
        { documentType: 'VOID_TICKETS', transactionsCount: 1361 },
        { documentType: 'EMDS', transactionsCount: 3221 },
        { documentType: 'REFUNDS', transactionsCount: 1412 },
        { documentType: 'ADMS', transactionsCount: 0 },
        { documentType: 'ACMS', transactionsCount: 0 }
      ],
      totalTransactions: 57012,
      totalIndustry: 100
    };
    const actual = convertDocumentTypeTotals(totals);
    const expected: ScuTotals = {
      data: [
        {
          industryPercentage: 1,
          total: 57012,
          tickets: 51018,
          voidTickets: 1361,
          emds: 3221,
          refunds: 1412,
          adms: 0,
          acms: 0
        }
      ],
      breakdownType: ScuBreakdownType.DocumentType
    };

    expect(actual).toEqual(expected);
  });
});

describe('ScuTotalConverters convertFormOfPaymentTotals', () => {
  it('should convert the values properly', () => {
    const totals: any = {
      issuesAndAdms: {
        totalTransactions: 55600,
        breakdown: [
          { formOfPayment: FormOfPayment.Cash, totalCount: 27319 },
          { formOfPayment: FormOfPayment.PaymentCard, totalCount: 22291 },
          { formOfPayment: FormOfPayment.CreditTurnedCash, totalCount: 0 },
          { formOfPayment: FormOfPayment.EasyPay, totalCount: 1 },
          { formOfPayment: FormOfPayment.Exchange, totalCount: 4628 },
          { formOfPayment: FormOfPayment.Void, totalCount: 1361 }
        ]
      },
      refundsAndAcms: {
        totalTransactions: 1412,
        breakdown: [
          { formOfPayment: FormOfPayment.Cash, totalCount: 635 },
          { formOfPayment: FormOfPayment.PaymentCard, totalCount: 777 },
          { formOfPayment: FormOfPayment.CreditTurnedCash, totalCount: 0 },
          { formOfPayment: FormOfPayment.EasyPay, totalCount: 0 },
          { formOfPayment: FormOfPayment.Exchange, totalCount: 0 },
          { formOfPayment: FormOfPayment.Void, totalCount: 0 }
        ]
      },
      totals: {
        totalTransactions: 57012,
        breakdown: [
          { formOfPayment: FormOfPayment.Cash, totalCount: 27954 },
          { formOfPayment: FormOfPayment.PaymentCard, totalCount: 23068 },
          { formOfPayment: FormOfPayment.CreditTurnedCash, totalCount: 0 },
          { formOfPayment: FormOfPayment.EasyPay, totalCount: 1 },
          { formOfPayment: FormOfPayment.Exchange, totalCount: 4628 },
          { formOfPayment: FormOfPayment.Void, totalCount: 1361 }
        ]
      },
      issuesAndAdmsIndustry: 97.52,
      refundsAndAcmsIndustry: 2.48,
      totalIndustry: 100
    };
    const actual = convertFormOfPaymentTotals(totals);
    const expected: ScuTotals = {
      data: [
        {
          industryPercentage: 0.9752,
          total: 55600,
          cash: 27319,
          paymentCard: 22291,
          creditTurnedCash: 0,
          easyPay: 1,
          exchange: 4628,
          void: 1361,
          type: ScuTotalsType.Issue
        },
        {
          industryPercentage: 0.0248,
          total: 1412,
          cash: 635,
          paymentCard: 777,
          creditTurnedCash: 0,
          easyPay: 0,
          exchange: 0,
          void: 0,
          type: ScuTotalsType.Refund
        },
        {
          industryPercentage: 1,
          total: 57012,
          cash: 27954,
          paymentCard: 23068,
          creditTurnedCash: 0,
          easyPay: 1,
          exchange: 4628,
          void: 1361,
          type: ScuTotalsType.All
        }
      ],
      breakdownType: ScuBreakdownType.FormOfPayment
    };

    expect(actual).toEqual(expected);
  });
});
