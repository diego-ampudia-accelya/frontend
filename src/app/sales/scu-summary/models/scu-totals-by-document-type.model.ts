export interface ScuTotalsByDocumentType {
  tickets: number;
  voidTickets: number;
  emds: number;
  refunds: number;
  adms: number;
  acms: number;
  total: number;
  industryPercentage: number;
}
