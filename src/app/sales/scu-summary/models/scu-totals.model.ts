import { ScuBreakdownType } from './scu-breakdown-type.enum';
import { ScuTotalsByDocumentType } from './scu-totals-by-document-type.model';
import { ScuTotalsByFormOfPayment } from './scu-totals-by-form-of-payment.model';

export interface ScuTotals {
  breakdownType: ScuBreakdownType;
  data: Array<ScuTotalsByDocumentType | ScuTotalsByFormOfPayment>;
}
