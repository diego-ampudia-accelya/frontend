export interface ScuTotalsByFormOfPayment {
  type: ScuTotalsType;
  cash: number;
  paymentCard: number;
  easyPay: number;
  total: number;
  exchange: number;
  void: number;
  creditTurnedCash: number;
  industryPercentage: number;
}

export enum ScuTotalsType {
  Issue = 'Issue',
  Refund = 'Refund',
  All = 'All'
}
