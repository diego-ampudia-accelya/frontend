import { DocumentType } from '~app/shared/enums';
import { AgentDto } from '~app/shared/models/agent.model';
import { BspDto } from '~app/shared/models/bsp.model';

export interface ScuSummary {
  agent: AgentDto;
  airline: { globalName: string; localName: string; id: number; iataCode: string };
  bsp: BspDto;
  formOfPayment: string;
  statisticalCode: string;
  totalTransactions: number;
  transactionCode: DocumentType;
}
