import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { cloneDeep } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { catchError, first, map, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import { SalesDownloadDialogService } from '~app/sales/shared/services/sales-download-dialog.service';
import { ButtonDesign, FloatingPanelComponent, PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { DocumentType, LongDocumentType, SelectMode } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User, UserType } from '~app/shared/models/user.model';
import { SalesFilter } from '../shared/models/sales-filter.model';
import { SalesDropdownOptionsService } from '../shared/services';
import { ScuSummaryService } from './data/scu-summary.service';
import { ScuBreakdownType, ScuSummary, ScuTotals } from './models';

const emptyTotals: Observable<ScuTotals> = of({
  breakdownType: ScuBreakdownType.DocumentType,
  data: []
});

@Component({
  selector: 'bspl-scu-summary',
  templateUrl: './scu-summary.component.html',
  styleUrls: ['./scu-summary.component.scss'],
  providers: [
    DefaultQueryStorage,
    SalesDisplayFilterFormatter,
    { provide: QUERYABLE, useExisting: ScuSummaryService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [ScuSummaryService]
    }
  ]
})
export class ScuSummaryComponent implements OnInit, OnDestroy {
  @ViewChild(FloatingPanelComponent, { static: true }) public totalsPanel: FloatingPanelComponent;
  public selectMode = SelectMode;
  public total$: Observable<ScuTotals> = emptyTotals;
  public isTotalLoading: boolean;

  public searchForm: FormGroup;
  public columns$: Observable<Array<GridColumn>>;

  public documentTypeOptions: DropdownOption[] = [];
  public formOfPaymentOptions: DropdownOption[] = [];
  public statisticalCodeOptions: DropdownOption[] = [];
  public userBspOptions: DropdownOption<Bsp>[] = [];
  public selectedBsps: Bsp[] = [];
  public buttonDesign = ButtonDesign;
  public periodPickerYearsBack: number;

  public currentPeriod: PeriodOption;

  public predefinedFilters: { bsp?: Bsp | Bsp[]; monthPeriod?: PeriodOption[]; periodRange?: PeriodOption[] };
  public predefinedUsersAirline: AirlineSummary[] = [];
  public predefinedUserAgent: AgentSummary[] = [];

  public isUserAgent: boolean;
  public isUserAirline: boolean;
  public isMonthPeriodPicker: boolean;
  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;
  private hasLeanPermission: boolean;
  private isUserAgentGroup: boolean;

  public airlineDropdownOptions$: Observable<DropdownOption[]>;
  public agentDropdownOptions$: Observable<DropdownOption[]>;
  public periodOptions$: Observable<PeriodOption[]>;
  public periodDropdownOptions: PeriodOption[];

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;

  public formFactory: FormUtil;

  public loggedUser: User;
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private get currentPeriodValue(): PeriodOption[] {
    const currentPeriod = PeriodUtils.findCurrentPeriod(this.periodDropdownOptions || []);

    return currentPeriod ? [currentPeriod, currentPeriod] : null;
  }

  public noticeDisclaimer$: Observable<string> = this.dataSource?.appliedQuery$.pipe(
    map(query => (query && this.isFilterByCurrentPeriod() ? 'sales.tableNoticeMsg' : 'sales.tableNoticeMsgDefault'))
  );

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<ScuSummary>,
    public displayFilterFormatter: SalesDisplayFilterFormatter,
    protected queryStorage: DefaultQueryStorage,
    protected dropdownOptionsService: SalesDropdownOptionsService,
    protected permissionsService: PermissionsService,
    private formBuilder: FormBuilder,
    private scuSummaryService: ScuSummaryService,
    private salesDownloadDialogService: SalesDownloadDialogService,
    private translationService: L10nTranslationService,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);

    this.initializeLoggedUser();
    this.initializePermissions();
  }

  public ngOnInit(): void {
    this.searchForm = this.buildSearchForm();
    this.columns$ = this.buildColumns();
    this.initializeBspFeatures();
    this.setPeriodPickerYearsBack();

    const storedQuery = this.queryStorage.get();
    if (storedQuery) {
      // Restore BSP selection if user change between open tabs
      this.selectedBsps = storedQuery.filterBy.bsp || this.selectedBsps;
    }

    this.populateFilterDropdowns();

    this.setPredefinedFilters();
    this.initializeDataQuery(storedQuery);
  }

  public loadData(query?: DataQuery) {
    query = query || cloneDeep(defaultQuery);

    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...(!this.hasLeanPermission && this.predefinedFilters),
        ...this.handleQueryFilter(query.filterBy)
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
    this.totalsPanel.close();
  }

  public onTotalPanelOpened(): void {
    this.total$ = this.getTotals();
  }

  public onTotalPanelClosed(): void {
    this.total$ = emptyTotals;
  }

  public onBreakdownTypeChanged(breakdownType: ScuBreakdownType): void {
    this.total$ = this.getTotals(breakdownType);
  }

  public onDownload() {
    combineLatest([this.salesDownloadDialogService.openDownloadDialog(), this.dataSource.appliedQuery$])
      .pipe(
        first(),
        switchMap(([exportAs, query]) => this.scuSummaryService.download(query, exportAs))
      )
      .subscribe(fileInfo => {
        FileSaver.saveAs(fileInfo.blob, fileInfo.fileName);
      });
  }

  /*
   * Handles query filter adding `monthPeriod` or `periodRange` depending on filtered BSPs
   *  and user LEAN permission and other properties necessary for Backend
   */
  private handleQueryFilter(queryFilter: Partial<SalesFilter>): Partial<SalesFilter> {
    // BSP filter is always an array if user has LEAN permission
    const filteredBsps = (queryFilter.bsp || this.selectedBsps) as Bsp[];

    return {
      ...queryFilter,
      monthPeriod: this.selectedBsps.length !== 1 ? queryFilter.monthPeriod || this.currentPeriodValue : null,
      periodRange: this.selectedBsps.length === 1 ? queryFilter.periodRange || this.currentPeriodValue : null,
      ...(this.isBspFilterMultiple && { bsp: filteredBsps }),
      ...(this.isUserAirline && { airline: this.predefinedUsersAirline.length ? this.predefinedUsersAirline : null }),
      ...(this.isUserAgent && { agent: this.predefinedUserAgent.length ? this.predefinedUserAgent : null })
    };
  }

  private initializeDataQuery(
    storedQuery: DataQuery<{
      [key: string]: any;
    }>
  ): void {
    // It's necessary load periods before in order to load Data successfully
    this.periodOptions$.subscribe(data => {
      this.periodDropdownOptions = data;
      this.loadData(storedQuery);
    });
  }

  /** Set predefined filters adding `periodRange`, only available if user has not LEAN permission */
  private setPredefinedFilters(): void {
    if (this.hasLeanPermission) {
      return;
    }

    this.predefinedFilters = {
      bsp: this.selectedBsps[0],
      periodRange: this.currentPeriodValue
    };
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<SalesFilter>({
      bsp: [],
      airline: [],
      agent: [],
      periodRange: [],
      monthPeriod: [],
      documentType: [],
      formOfPayment: [],
      statisticalCode: []
    });
  }

  private initializeBspFeatures(): void {
    if (this.hasLeanPermission) {
      this.initializeBspListener();
    }

    // Initialize BSP filter dropdown
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    this.initializeSelectedBsps();

    // Initialize BSP related flags
    this.isBspFilterLocked = !this.hasLeanPermission;
    this.isBspFilterMultiple = this.hasLeanPermission;
    this.isMonthPeriodPicker = this.selectedBsps.length !== 1;
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readScuSummary;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private initializeBspListener(): void {
    FormUtil.get<SalesFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(value => {
        const arrayValue = Array.isArray(value) ? value : [value];
        this.selectedBsps = value ? arrayValue : [];
        this.isMonthPeriodPicker = this.selectedBsps.length !== 1;

        this.populateFilterDropdowns();
        this.updateFilterValues();
      });
  }

  private populateFilterDropdowns(): void {
    const bspSelectedIds = this.selectedBsps.map(({ id }) => id);
    const firstBsp = this.selectedBsps.length ? this.selectedBsps[0] : this.userBspOptions[0]?.value;
    const params = bspSelectedIds.length ? { bspId: bspSelectedIds } : null;

    this.airlineDropdownOptions$ = this.dropdownOptionsService.getAirlines(firstBsp);
    this.agentDropdownOptions$ = this.getAgentDropDownByUserType(params);
    this.documentTypeOptions = this.dropdownOptionsService.getDocumentTypes();
    this.formOfPaymentOptions = this.dropdownOptionsService.getFormOfPayment();
    this.statisticalCodeOptions = this.dropdownOptionsService.getStatisticalCode();

    this.periodOptions$ = this.dropdownOptionsService.getPeriods(firstBsp);

    const bspsToRetrieveAirlinesId = bspSelectedIds.length
      ? bspSelectedIds
      : this.userBspOptions.map(data => data.value.id);

    this.initializeUserFilters(bspsToRetrieveAirlinesId);
  }

  /** Since this method is only available for Airline users with LEAN permission, we do not need to update Airline filter */
  private updateFilterValues(): void {
    this.updatePeriodFilterValue();
    this.updateAgentFilterValue();
  }

  private updatePeriodFilterValue(): void {
    const periodRangeControl = FormUtil.get<SalesFilter>(this.searchForm, 'periodRange');
    const monthPeriodControl = FormUtil.get<SalesFilter>(this.searchForm, 'monthPeriod');

    if (this.selectedBsps.length === 1) {
      periodRangeControl.patchValue(periodRangeControl.value || this.currentPeriodValue);
      monthPeriodControl.reset();
    } else {
      monthPeriodControl.patchValue(monthPeriodControl.value || this.currentPeriodValue);
      periodRangeControl.reset();
    }
  }

  private updateAgentFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentControl = FormUtil.get<SalesFilter>(this.searchForm, 'agent');
    const agentsSelected: AgentSummary[] = agentControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.selectedBsps.some(({ id }) => agent.bsp?.id === id));
      agentControl.patchValue(agentsToPatch);
    }
  }

  private initializeSelectedBsps(): void {
    const firstListBsp = this.userBspOptions[0].value;
    const defaultBsp = this.userBspOptions.find(
      ({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc
    )?.value;
    this.selectedBsps = [firstListBsp];

    if (this.userBspOptions.length > 1) {
      this.selectedBsps = this.hasLeanPermission ? [] : [defaultBsp || firstListBsp];
    }
  }

  private getTotals(breakdownType = ScuBreakdownType.DocumentType): Observable<ScuTotals> {
    return this.dataSource.appliedQuery$.pipe(
      first(),
      tap(() => (this.isTotalLoading = true)),
      switchMap(query => {
        let result$: Observable<ScuTotals>;
        const { filterBy = null } = query || {};

        if (breakdownType === ScuBreakdownType.DocumentType) {
          result$ = this.scuSummaryService.getTotalsByDocumentType(filterBy);
        } else if (breakdownType === ScuBreakdownType.FormOfPayment) {
          result$ = this.scuSummaryService.getTotalsByFormOfPayment(filterBy);
        }

        return result$.pipe(catchError(() => of({ breakdownType, data: [] })));
      }),
      tap(() => (this.isTotalLoading = false))
    );
  }

  private isFilterByCurrentPeriod(): boolean {
    const storedQuery = this.queryStorage.get();

    return storedQuery?.filterBy.periodRange?.some(periodRange => this.currentPeriod?.period === periodRange.period);
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private getAgentDropDownByUserType(params: { bspId: number[] }): Observable<DropdownOption<AgentSummary>[]> {
    return this.isUserAgentGroup ? this.getAgentGroupDropDown(params) : this.dropdownOptionsService.getAgents(params);
  }

  /**
   * Initialize user specific filters
   */
  protected initializeUserFilters(bspIds: number[]): void {
    if (this.isUserAirline) {
      // If the user is an AIRLINE, add the airlineIds as a filter.
      this.dropdownOptionsService
        .getUserAirlinesByBsp(bspIds)
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          this.predefinedUsersAirline = data;
          this.displayFilterFormatter.options.showAirline = !data;
        });
    }

    if (this.isUserAgent) {
      // If the user is an AGENT, add the agentId as a filter.
      this.dropdownOptionsService
        .getUserAgent()
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          this.predefinedUserAgent = [data];
          this.displayFilterFormatter.options.showAgent = !data;
        });
    }
  }

  private getAgentGroupDropDown(queryfilters): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = Permissions.readAgentDictionary;
    const aGroupUserHasPermission = this.permissionsService.hasPermission(aGroupUserPermission);

    if (aGroupUserHasPermission) {
      const filter = { ...queryfilters, permission: aGroupUserPermission };

      return this.dropdownOptionsService.getAgentGroupsAgents(filter);
    } else {
      return of(null);
    }
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.isUserAirline = this.loggedUser.userType === UserType.AIRLINE;
      this.isUserAgent = this.loggedUser.userType === UserType.AGENT;
      this.isUserAgentGroup = this.loggedUser.userType === UserType.AGENT_GROUP;
    });
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private buildColumns(): Observable<GridColumn[]> {
    const columns: Array<GridColumn> = [
      {
        prop: 'bsp.isoCountryCode',
        name: 'sales.billingStatement.columns.bsp',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      },
      {
        prop: 'airline.iataCode',
        name: 'sales.scuSummary.columns.airlineCode',
        hidden: this.isUserAirline,
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      },
      {
        prop: 'airline.localName',
        name: 'sales.scuSummary.columns.airlineName',
        hidden: this.isUserAirline,
        resizeable: true,
        minWidth: 150,
        flexGrow: 1
      },
      {
        prop: 'agent.iataCode',
        name: 'sales.scuSummary.columns.agentCode',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1
      },
      {
        prop: 'agent.name',
        name: 'sales.scuSummary.columns.agentName',
        resizeable: true,
        minWidth: 150,
        flexGrow: 1
      },
      {
        prop: 'transactionCode',
        name: 'sales.scuSummary.columns.documentType',
        resizeable: true,
        cellTemplate: 'tooltipCellTmpl',
        tooltip: (value: DocumentType) =>
          LongDocumentType[value]
            ? this.translationService.translate(`sales.scuSummary.tooltip.longDocumentType.${value}`)
            : '',
        minWidth: 115,
        flexGrow: 1
      },
      {
        prop: 'statisticalCode',
        name: 'sales.scuSummary.columns.salesType',
        resizeable: true,
        minWidth: 100,
        flexGrow: 1,
        pipe: {
          transform: value => this.dropdownOptionsService.translateStatisticalCode(value)
        }
      },
      {
        prop: 'formOfPayment',
        name: 'sales.scuSummary.columns.formOfPayment',
        resizeable: true,
        minWidth: 120,
        flexGrow: 1,
        pipe: {
          transform: value => this.dropdownOptionsService.translateFormOfPayment(value)
        }
      },
      {
        prop: 'totalTransactions',
        name: 'sales.scuSummary.columns.quantity',
        cellClass: 'text-right',
        minWidth: 150,
        flexGrow: 1
      }
    ];

    return of(columns);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
