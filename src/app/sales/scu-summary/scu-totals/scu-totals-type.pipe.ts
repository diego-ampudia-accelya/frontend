import { Pipe, PipeTransform } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ScuTotalsType } from '../models';

@Pipe({
  name: 'scuTotalsType',
  pure: true
})
export class ScuTotalsTypePipe implements PipeTransform {
  constructor(private translation: L10nTranslationService) {}

  public transform(type: any): string {
    const keys = {
      [ScuTotalsType.Issue]: 'sales.scuSummary.totals.issuesType',
      [ScuTotalsType.Refund]: 'sales.scuSummary.totals.refundsType',
      [ScuTotalsType.All]: 'sales.scuSummary.totals.allSCUsType'
    };

    return this.translation.translate(keys[type]);
  }
}
