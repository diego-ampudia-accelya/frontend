import { PercentPipe } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import {
  ScuBreakdownType,
  ScuTotals,
  ScuTotalsByDocumentType,
  ScuTotalsByFormOfPayment,
  ScuTotalsType
} from '../models';
import { SimpleTableColumn } from '~app/shared/components';
import { SelectMode } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Component({
  selector: 'bspl-scu-totals',
  templateUrl: './scu-totals.component.html',
  styleUrls: ['./scu-totals.component.scss']
})
export class ScuTotalsComponent implements OnChanges {
  @Input() total: ScuTotals;

  @Output() breakdownTypeChange = new EventEmitter<ScuBreakdownType>();

  public breakdownType: ScuBreakdownType = ScuBreakdownType.DocumentType;
  public selectedScuType: string = ScuTotalsType.All;

  public get showsSingleEntry(): boolean {
    return this.data && this.data.length <= 1;
  }

  public columns: SimpleTableColumn<any>[];

  private _data: any[];
  public get data(): any[] {
    return this._data;
  }
  public set data(value: any[]) {
    this._data = value;
    if (!this._data || this._data.length === 0) {
      this._data = [{}];
    }
  }

  public SelectMode = SelectMode;
  public ScuTotalsType = ScuTotalsType;
  public breakdownOptions: DropdownOption<ScuBreakdownType>[] = [
    {
      value: ScuBreakdownType.DocumentType,
      label: this.translationService.translate('sales.scuSummary.totals.documentType')
    },
    {
      value: ScuBreakdownType.FormOfPayment,
      label: this.translationService.translate('sales.scuSummary.totals.formOfPayment')
    }
  ];

  @ViewChild('actionHeaderTemplate', { static: true })
  private actionHeaderTemplate: TemplateRef<any>;

  @ViewChild('emptyCellTemplate', { static: true })
  private emptyCellTemplate: TemplateRef<any>;

  @ViewChild('actionCellTemplate', { static: true })
  private actionCellTemplate: TemplateRef<any>;

  constructor(private translationService: L10nTranslationService) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.total.currentValue) {
      const total: ScuTotals = changes.total.currentValue;
      this.breakdownType = total.breakdownType;
      if (total.breakdownType === ScuBreakdownType.DocumentType) {
        this.data = total.data;
        this.columns = this.getDocumentTypeColumns();
      }

      if (total.breakdownType === ScuBreakdownType.FormOfPayment) {
        this.data = total.data.filter((item: ScuTotalsByFormOfPayment) => item.type === ScuTotalsType.All);
        this.columns = this.getFormOfPaymentColumns();
      }
    }
  }

  public selectBreakdown(breakdownType: ScuBreakdownType): void {
    this.breakdownType = breakdownType;
    this.breakdownTypeChange.emit(breakdownType);
  }

  public selectEntry(item: ScuTotalsByFormOfPayment): void {
    if (this.breakdownType !== ScuBreakdownType.FormOfPayment) {
      return;
    }

    this.selectedScuType = item.type;
    this.data = [item];
  }

  public showAll($event: Event): void {
    if (this.breakdownType !== ScuBreakdownType.FormOfPayment) {
      return;
    }

    this.data = this.total.data;
    $event.stopPropagation();
  }

  public isSelectedScu(scuType: string): boolean {
    return scuType === this.selectedScuType;
  }

  private getFormOfPaymentColumns(): SimpleTableColumn<ScuTotalsByFormOfPayment>[] {
    const baseColumn: Partial<SimpleTableColumn<any>> = { customStyle: 'text-right' };

    return [
      {
        headerCellTemplate: this.actionHeaderTemplate,
        field: 'type',
        customStyle: 'actions-column-cell',
        cellTemplate: this.actionCellTemplate
      },
      { ...baseColumn, header: 'sales.scuSummary.totals.cash', field: 'cash' },
      { ...baseColumn, header: 'sales.scuSummary.totals.paymentCard', field: 'paymentCard' },
      {
        ...baseColumn,
        header: 'sales.scuSummary.totals.easyPay',
        field: 'easyPay'
      },
      {
        ...baseColumn,
        header: 'sales.scuSummary.totals.exchange',
        field: 'exchange'
      },
      {
        ...baseColumn,
        header: 'sales.scuSummary.totals.void',
        field: 'void'
      },
      {
        header: 'sales.scuSummary.totals.creditTurnedCash',
        field: 'creditTurnedCash',
        customStyle: 'last-regular-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.totalSCUs',
        field: 'total',
        customStyle: 'summary-column-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.industryPercentage',
        field: 'industryPercentage',
        pipe: PercentPipe,
        pipeArgs: ['1.0-2'],
        customStyle: 'summary-column-cell text-right'
      }
    ];
  }

  private getDocumentTypeColumns(): SimpleTableColumn<ScuTotalsByDocumentType>[] {
    return [
      {
        headerCellTemplate: this.actionHeaderTemplate,
        field: '' as any,
        customStyle: 'actions-column-cell',
        cellTemplate: this.emptyCellTemplate
      },
      {
        header: 'sales.scuSummary.totals.issues',
        headerTooltip: 'TKTT, TASF',
        field: 'tickets',
        customStyle: 'tooltip-header-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.cancelledTickets',
        headerTooltip: 'CANX',
        field: 'voidTickets',
        customStyle: 'tooltip-header-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.emds',
        headerTooltip: 'EMDA, EMDS',
        field: 'emds',
        customStyle: 'tooltip-header-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.refunds',
        headerTooltip: 'RFND, RFNC',
        field: 'refunds',
        customStyle: 'tooltip-header-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.adms',
        headerTooltip: 'ADMA, ADMD, ADNT, SPDR, SSAD',
        field: 'adms',
        customStyle: 'tooltip-header-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.acms',
        headerTooltip: 'ACMA, ACMD, ACNT, SPCR, SSAC',
        field: 'acms',
        customStyle: 'last-regular-cell tooltip-header-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.totalSCUs',
        field: 'total',
        customStyle: 'summary-column-cell text-right'
      },
      {
        header: 'sales.scuSummary.totals.industryPercentage',
        field: 'industryPercentage',
        pipe: PercentPipe,
        pipeArgs: ['1.0-2'],
        customStyle: 'summary-column-cell text-right'
      }
    ];
  }
}
