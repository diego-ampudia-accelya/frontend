import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslatePipe, L10nTranslationService } from 'angular-l10n';
import { MockComponent, MockPipe } from 'ng-mocks';

import { ScuBreakdownType, ScuTotals, ScuTotalsType } from '../models';

import { ScuTotalsTypePipe } from './scu-totals-type.pipe';
import { ScuTotalsComponent } from './scu-totals.component';
import { SelectComponent, SimpleTableComponent } from '~app/shared/components';

describe('ScuTotalsComponent', () => {
  let component: ScuTotalsComponent;
  let fixture: ComponentFixture<ScuTotalsComponent>;

  const totalByDocumentType: ScuTotals = {
    breakdownType: ScuBreakdownType.DocumentType,
    data: [
      {
        tickets: 10,
        voidTickets: 10,
        emds: 10,
        refunds: 10,
        adms: 10,
        acms: 10,
        total: 60,
        industryPercentage: 0.4
      }
    ]
  };
  const totalByFormOfPayment: ScuTotals = {
    breakdownType: ScuBreakdownType.FormOfPayment,
    data: [
      {
        type: ScuTotalsType.Issue,
        cash: 50,
        paymentCard: 15,
        easyPay: 10,
        exchange: 5,
        void: 5,
        creditTurnedCash: 5,
        total: 90,
        industryPercentage: 0.2
      },
      {
        type: ScuTotalsType.Refund,
        cash: 50,
        paymentCard: 15,
        easyPay: 10,
        exchange: 5,
        void: 5,
        creditTurnedCash: 5,
        total: 90,
        industryPercentage: 0.2
      },
      {
        type: ScuTotalsType.All,
        cash: 100,
        paymentCard: 30,
        easyPay: 20,
        exchange: 10,
        void: 10,
        creditTurnedCash: 10,
        total: 180,
        industryPercentage: 0.4
      }
    ]
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ScuTotalsComponent,
        MockPipe(L10nTranslatePipe),
        MockPipe(ScuTotalsTypePipe),
        MockComponent(SimpleTableComponent),
        MockComponent(SelectComponent)
      ],
      providers: [mockProvider(L10nTranslationService)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScuTotalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show breakdown by Document Type', () => {
    component.total = totalByDocumentType;
    component.ngOnChanges({ total: new SimpleChange(null, component.total, false) });

    expect(component.data.length).toBe(1);
    expect(component.showsSingleEntry).toBeTruthy();
    expect(component.breakdownType).toEqual(ScuBreakdownType.DocumentType);
  });

  it('should show breakdown by Form of Payment in collapsed state', () => {
    component.total = totalByFormOfPayment;
    component.ngOnChanges({ total: new SimpleChange(null, component.total, false) });

    expect(component.data.length).toBe(1);
    expect(component.showsSingleEntry).toBeTruthy();
    expect(component.breakdownType).toEqual(ScuBreakdownType.FormOfPayment);
  });

  it('should expand breakdown by Form of Payment', () => {
    const mockEvent = jasmine.createSpyObj<Event>('Event', ['stopPropagation']);
    component.total = totalByFormOfPayment;
    component.ngOnChanges({ total: new SimpleChange(null, component.total, false) });

    component.showAll(mockEvent);

    expect(component.data.length).toBe(3);
    expect(component.showsSingleEntry).toBeFalsy();
    expect(mockEvent.stopPropagation).toHaveBeenCalled();
  });

  it('should collapse breakdown by Form of payment', () => {
    const mockEvent = jasmine.createSpyObj<Event>('Event', ['stopPropagation']);
    component.total = totalByFormOfPayment;
    component.ngOnChanges({ total: new SimpleChange(null, component.total, false) });
    component.showAll(mockEvent);

    component.selectEntry(totalByFormOfPayment.data[0] as any);

    expect(component.data).toEqual([totalByFormOfPayment.data[0]]);
    expect(component.showsSingleEntry).toBeTruthy();
  });

  it('should maintain a single item in data when there is no info for breakdown by Document Type', () => {
    component.total = { breakdownType: ScuBreakdownType.DocumentType, data: [] };
    component.ngOnChanges({ total: new SimpleChange(null, component.total, false) });

    expect(component.data).toEqual([{}]);
    expect(component.showsSingleEntry).toBeTruthy();
    expect(component.breakdownType).toEqual(ScuBreakdownType.DocumentType);
  });

  it('should maintain a single item in data when there is no info for breakdown by Form of payment', () => {
    component.total = { breakdownType: ScuBreakdownType.FormOfPayment, data: [] };
    component.ngOnChanges({ total: new SimpleChange(null, component.total, false) });

    expect(component.data).toEqual([{}]);
    expect(component.showsSingleEntry).toBeTruthy();
    expect(component.breakdownType).toEqual(ScuBreakdownType.FormOfPayment);
  });
});
