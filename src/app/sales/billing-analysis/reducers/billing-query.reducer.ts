import { SummaryTicket } from '~app/document/models/summary-ticket.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { ServerError } from '~app/shared/errors';
import { BillingQueryActions, Types } from '../actions/billing-query.actions';
import { BillingAnalysis } from '../models';

export interface QueryState {
  defaultQuery: DataQuery;
  lastQuery: DataQuery;
  isMultipopulation: boolean;
  selectedDocuments: BillingAnalysis[];
  multipopulationDetails: SummaryTicket;
  issueType: 'adma' | 'acma';
  error: ServerError;
}

export const initialState: QueryState = {
  defaultQuery: {
    sortBy: [],
    paginateBy: { page: 0, size: PAGINATION.DEFAULT_PAGE_SIZE },
    filterBy: {
      bsp: null,
      agent: null,
      agentGroup: null,
      airline: null,
      periodRange: []
    }
  },
  lastQuery: {
    paginateBy: {},
    sortBy: [],
    filterBy: {
      bsp: null,
      agent: null,
      agentGroup: null,
      airline: null,
      periodRange: []
    }
  },
  isMultipopulation: false,
  selectedDocuments: [],
  multipopulationDetails: null,
  issueType: null,
  error: null
};

export function reducer(state = initialState, action: BillingQueryActions): QueryState {
  switch (action.type) {
    case Types.UpdateDefaultQuery:
      return { ...state, defaultQuery: action.payload };
    case Types.UpdateLastQuery:
      return { ...state, lastQuery: action.payload };
    case Types.UpdateSelectedDocuments:
      return { ...state, selectedDocuments: action.payload };
    case Types.UpdateIsMultipopulation:
      return { ...state, isMultipopulation: action.payload };
    case Types.ClearSelectedDocuments:
      return {
        ...state,
        selectedDocuments: state.selectedDocuments.map(document => ({ ...document, isSelected: false }))
      };
    case Types.LoadDocumentsData:
      return {
        ...state
      };
    case Types.LoadDocumentsDataSuccess:
      return {
        ...state,
        selectedDocuments: action.data.records
      };
    case Types.LoadDocumentsDataError:
      return {
        ...state,
        selectedDocuments: []
      };
    case Types.GetDocumentsDetails:
      return {
        ...state,
        issueType: action.issueType
      };
    case Types.GetDocumentsDetailsSuccess:
      return {
        ...state,
        multipopulationDetails: action.data
      };
    case Types.GetDocumentsDetailsError:
      return {
        ...state,
        multipopulationDetails: null,
        error: action.error
      };

    default:
      return state;
  }
}

export const getSelectedDocumentsCount = (state: QueryState) =>
  state.selectedDocuments.filter(document => document.isSelected).length;

export const getSelectedDocumentsIds = (state: QueryState) =>
  state.selectedDocuments.filter(document => document.isSelected).map(document => document.id);
