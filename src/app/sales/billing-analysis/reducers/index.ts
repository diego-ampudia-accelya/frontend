// ToDo: Refactor alias `fromBillingAnalysis` to something meaningful
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '~app/reducers';
import * as fromBillingAnalysis from './billing-query.reducer';

export const featureKey = 'sales.billingAnalysis';

export interface State extends AppState {
  [featureKey]: fromBillingAnalysis.QueryState;
}

export const reducers = fromBillingAnalysis.reducer;

export const salesStateSelector = createFeatureSelector<State, fromBillingAnalysis.QueryState>(featureKey);

export const defaultQuerySelector = createSelector(salesStateSelector, state => state.defaultQuery);

export const lastQuerySelector = createSelector(salesStateSelector, state => state.lastQuery);

export const getSelectedDocumentsCount = createSelector(
  salesStateSelector,
  fromBillingAnalysis.getSelectedDocumentsCount
);

export const getSelectedDocumentsIds = createSelector(salesStateSelector, fromBillingAnalysis.getSelectedDocumentsIds);

export const getDocumentsData = createSelector(salesStateSelector, state => state.selectedDocuments);

export const getIsMultipopulation = createSelector(salesStateSelector, state => state.isMultipopulation);

export const getMultipopulationDetailsError = createSelector(salesStateSelector, state => state.error);

export const getIssueType = createSelector(salesStateSelector, state => state.issueType);

export const getMultipopulationDocumentsDetails = createSelector(
  salesStateSelector,
  state => state.multipopulationDetails
);
