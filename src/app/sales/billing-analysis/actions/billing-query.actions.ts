/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
import { Action } from '@ngrx/store';
import { SummaryTicket } from '~app/document/models/summary-ticket.model';
import { DataQuery } from '~app/shared/components/list-view';
import { ServerError } from '~app/shared/errors';
import { PagedData } from '~app/shared/models/paged-data.model';
import { BillingAnalysis } from '../models';

export enum Types {
  UpdateDefaultQuery = '[BillingQuery] Update DefaultQuery',
  UpdateLastQuery = '[BillingQuery] Update LastQuery',
  UpdateSelectedDocuments = '[BillingQuery] Update SelectedDocuments',
  UpdateIsMultipopulation = '[BillingQuery] Update IsMultipopulation',
  ClearSelectedDocuments = '[BillingQuery] Clear SelectedDocuments',
  LoadDocumentsData = '[BillingQuery] Load Documents Data',
  LoadDocumentsDataSuccess = '[BillingQuery] Load Documents Data Success',
  LoadDocumentsDataError = '[BillingQuery] Load Documents Data Error',
  GetDocumentsDetails = '[BillingQuery] Get Billing Analysis Documents Details',
  GetDocumentsDetailsSuccess = '[BillingQuery] Get Billing Analysis Documents Details Success',
  GetDocumentsDetailsError = '[BillingQuery] Get Billing Analysis Documents Details Error'
}

export class UpdateDefaultQuery implements Action {
  readonly type = Types.UpdateDefaultQuery;
  constructor(public payload: DataQuery) {}
}
export class UpdateLastQuery implements Action {
  readonly type = Types.UpdateLastQuery;
  constructor(public payload: DataQuery) {}
}

export class UpdateSelectedDocuments implements Action {
  readonly type = Types.UpdateSelectedDocuments;
  constructor(public payload: BillingAnalysis[]) {}
}

export class UpdateIsMultipopulation implements Action {
  readonly type = Types.UpdateIsMultipopulation;
  constructor(public payload: boolean) {}
}

export class ClearSelectedDocuments implements Action {
  readonly type = Types.ClearSelectedDocuments;
}

export class LoadDocumentsData implements Action {
  readonly type = Types.LoadDocumentsData;
}

export class LoadDocumentsDataSuccess implements Action {
  readonly type = Types.LoadDocumentsDataSuccess;
  constructor(public data: PagedData<BillingAnalysis>) {}
}

export class LoadDocumentsDataError implements Action {
  readonly type = Types.LoadDocumentsDataError;
  constructor(public error: ServerError) {}
}

export class GetDocumentsDetails implements Action {
  readonly type = Types.GetDocumentsDetails;
  constructor(public issueType: 'adma' | 'acma') {}
}

export class GetDocumentsDetailsSuccess implements Action {
  readonly type = Types.GetDocumentsDetailsSuccess;
  constructor(public data: SummaryTicket) {}
}

export class GetDocumentsDetailsError implements Action {
  readonly type = Types.GetDocumentsDetailsError;
  constructor(public error: ServerError) {}
}

export type BillingQueryActions =
  | UpdateDefaultQuery
  | UpdateLastQuery
  | UpdateSelectedDocuments
  | UpdateIsMultipopulation
  | ClearSelectedDocuments
  | LoadDocumentsData
  | LoadDocumentsDataSuccess
  | LoadDocumentsDataError
  | GetDocumentsDetails
  | GetDocumentsDetailsSuccess
  | GetDocumentsDetailsError;
