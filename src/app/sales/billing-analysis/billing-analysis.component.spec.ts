import { DatePipe, DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { AuthState } from '~app/auth/reducers/auth.reducer';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DocumentService } from '~app/document/services/document.service';
import { StatisticalCode } from '~app/sales/shared/models';
import { SalesDisplayFilterFormatter } from '~app/sales/shared/sales-display-filter-formatter';
import {
  ButtonComponent,
  DialogService,
  GridTableComponent,
  PeriodPickerComponent,
  SelectComponent,
  SpinnerComponent
} from '~app/shared/components';
import { DefaultQueryStorage, ListViewComponent, QueryableDataSource } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants/routes';
import { RecursivePartial } from '~app/shared/helpers/recursive-partial';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { AppConfigurationService } from '~app/shared/services';
import { FloatingPanelComponentMockComponent, ListViewGridMockDirective, TranslatePipeMock } from '~app/test';
import * as fromBillingAnalysis from '../billing-analysis/reducers';
import { SalesDropdownOptionsService } from '../shared/services';
import { BillingAnalysisComponent } from './billing-analysis.component';
import { BillingAnalysisTotalsComponent } from './components/billing-analysis-totals/billing-analysis-totals.component';
import { BillingAnalysisStore } from './data/billing-analysis-store.service';
import { BillingAnalysisService } from './data/billing-analysis.service';
import { BillingAnalysis } from './models';
import { BillingAnalysisDetailsRow } from './models/billing-analysis-details-row.model';

const defaultAppState = {
  auth: {
    user: createIataUser()
  },
  sales: {
    billingAnalysis: {
      defaultQuery: {
        filterBy: {
          bsp: 'Testbsp',
          agent: '123 / Test agent'
        }
      },
      lastQuery: {
        filterBy: {
          bsp: 'Testbsp',
          agent: '123 / Test agent'
        }
      },
      loggedUser: {
        email: 'nfeagent@iata.org'
      }
    }
  }
};

const activatedRouteStub = {
  snapshot: {
    data: {
      agentBsp: { id: 1234, isoCountryCode: 'ES', name: 'SPAIN' }
    }
  }
};

const mockedPeriod = [
  { period: '2021079', dateFrom: '05/21/2021', dateTo: '06/21/2021' },
  { period: '2021079', dateFrom: '06/21/2021', dateTo: '07/21/2021' }
];

const mockedAirlines = [
  { value: 'Air Force One', label: 'Air Force One' },
  { value: 'Virgin Tsvete', label: 'Virgin Tsvete' }
];

const mockedAgents = [
  { value: 'Agent One', label: 'Agent One' },
  { value: 'Agent Two', label: 'Agent Two' }
];

const mockedCurrencies = [
  { value: 'EUR', label: 'EUR' },
  { value: 'USD', label: 'USD' }
];

const mockedStatisticalCode = [
  { value: StatisticalCode.International, label: 'International' },
  { value: StatisticalCode.Domestic, label: 'Domestic' }
];

const mockedDocumentTypes = [{ value: 'test1', label: 'test1' }];
const permissionsService = createSpyObject(PermissionsService);

describe('BillingAnalysisComponent', () => {
  let component: BillingAnalysisComponent;
  let fixture: ComponentFixture<BillingAnalysisComponent>;
  const spyRouter = createSpyObject(Router);

  const httpClientSpy = createSpyObject(HttpClient);
  const appConfigurationServiceSpy = createSpyObject(AppConfigurationService);
  const appState: RecursivePartial<{ auth: AuthState; sales: { billingAnalysis: unknown } }> = defaultAppState;
  const spyTranslationService = createSpyObject(L10nTranslationService);
  const spyPermissionsService = createSpyObject(PermissionsService);
  const spyDialogService: SpyObject<DialogService> = createSpyObject(DialogService);
  const spyDocumentService: SpyObject<DocumentService> = createSpyObject(DocumentService);
  const spySalesDropdownOptionsService = createSpyObject(SalesDropdownOptionsService);
  spySalesDropdownOptionsService.getPeriods.and.returnValue(of(mockedPeriod));
  spySalesDropdownOptionsService.getCurrencies.and.returnValue(of(mockedCurrencies));
  spySalesDropdownOptionsService.getAgents.and.returnValue(of(mockedAgents));
  spySalesDropdownOptionsService.getAirlines.and.returnValue(of(mockedAirlines));
  spySalesDropdownOptionsService.getStatisticalCode.and.returnValue(mockedStatisticalCode);
  spySalesDropdownOptionsService.getDocumentTypes.and.returnValue(mockedDocumentTypes);

  const billingAnalysisService = createSpyObject(BillingAnalysisService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BillingAnalysisComponent,
        TranslatePipeMock,
        FloatingPanelComponentMockComponent,
        ListViewGridMockDirective,
        TranslatePipeMock,
        HasPermissionPipe,
        MockComponents(
          ListViewComponent,
          ButtonComponent,
          SelectComponent,
          PeriodPickerComponent,
          GridTableComponent,
          SpinnerComponent,
          BillingAnalysisTotalsComponent
        )
      ],
      providers: [
        DateTimeFormatPipe,
        DatePipe,
        PeriodPipe,
        DecimalPipe,
        HasPermissionPipe,
        provideMockStore({
          initialState: appState,
          selectors: [
            { selector: fromBillingAnalysis.getIsMultipopulation, value: false },
            { selector: fromBillingAnalysis.getSelectedDocumentsCount, value: 0 },
            { selector: fromBillingAnalysis.getSelectedDocumentsIds, value: [] }
          ]
        }),
        provideMockDropdownOptionsService(),
        provideMockBillingAnalysisStore(),
        provideMockFormBuilder(),
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppConfigurationService, useValue: appConfigurationServiceSpy },
        { provide: BillingAnalysisService, useValue: billingAnalysisService },
        { provide: PermissionsService, useValue: permissionsService },
        { provide: SalesDropdownOptionsService, useValue: spySalesDropdownOptionsService },
        { provide: L10nTranslationService, useValue: spyTranslationService },
        { provide: DialogService, useValue: spyDialogService },
        { provide: DocumentService, useValue: spyDocumentService },
        { provide: PermissionsService, useValue: spyPermissionsService },
        { provide: Router, useValue: spyRouter },
        {
          provide: QueryableDataSource,
          useValue: {
            hasData$: of(false),
            appliedQuery$: of({ filterBy: { periodRange: mockedPeriod }, paginateBy: {}, sortBy: {} }),
            get: () => {}
          }
        },
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ]
    })
      .overrideComponent(BillingAnalysisComponent, {
        set: {
          providers: [
            { provide: SalesDisplayFilterFormatter, useValue: createSpyObject(SalesDisplayFilterFormatter) },
            { provide: DefaultQueryStorage, useValue: createSpyObject(DefaultQueryStorage) }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set predefinedFilters$', waitForAsync(() => {
    component.predefinedFilters$.pipe(first()).subscribe(filters => {
      expect(filters).toEqual(defaultAppState.sales.billingAnalysis.defaultQuery.filterBy);
    });
  }));

  it('should patch "search form" values', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(component.searchForm.patchValue).toHaveBeenCalledWith(
      defaultAppState.sales.billingAnalysis.defaultQuery.filterBy
    );
  }));

  it('should set dropdown options for "search form"', waitForAsync(() => {
    component.airlineDropdownOptions$.subscribe(options => {
      expect(options).toEqual(mockedAirlines);
    });

    component.agentDropdownOptions$.subscribe(options => {
      expect(options).toEqual(mockedAgents);
    });

    component.currencyDropdownOptions$.subscribe(options => {
      expect(options).toEqual(mockedCurrencies);
    });

    component.statisticalCodeDropdownOptions$.subscribe(options => {
      expect(options).toEqual(mockedStatisticalCode);
    });

    component.documentTypeOptions$.subscribe(options => {
      expect(options).toEqual(mockedDocumentTypes);
    });
  }));

  it('should open setup dialog', waitForAsync(() => {
    spyOn(component.floatingPanelComponent, 'close');

    component.openSetupDialog();

    // ToDo: assert the proper arguments have been passed with `toHaveBeenCalledWith`
    expect(spyDialogService.open).toHaveBeenCalledTimes(1);
    expect(component.floatingPanelComponent.close).toHaveBeenCalledTimes(1);
  }));

  it('should handle click on document number', waitForAsync(() => {
    const documentId = '123432234-sdewdewk';
    component.openDocument(documentId);

    expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.DOCUMENT_VIEW.url, '123432234-sdewdewk']);
  }));

  it('should call onRowToggle when the table expands a row', fakeAsync(() => {
    const event = { type: 'row', value: { id: '123' } as BillingAnalysis };
    const billingAnalysisDetailedRow = { id: '123' } as BillingAnalysisDetailsRow;

    billingAnalysisService.getAdditionalDetails.and.returnValue(of(billingAnalysisDetailedRow));
    component.onRowToggle(event);

    tick();

    expect(component.rowDetails).toEqual(jasmine.objectContaining(billingAnalysisDetailedRow));
  }));

  describe('onDownload', () => {
    it('should call openDownloadDialog', fakeAsync(() => {
      (component as any).salesDownloadDialogService.openDownloadDialog = jasmine.createSpy().and.returnValue(of({}));
      (component as any).billingAnalysisService.download = jasmine
        .createSpy()
        .and.returnValue(of({ blob: 'file', fileName: 'name' }));

      component.onDownload(false);
      tick();

      expect((component as any).salesDownloadDialogService.openDownloadDialog).toHaveBeenCalled();
    }));

    it('should call download', fakeAsync(() => {
      (component as any).salesDownloadDialogService.openDownloadDialog = jasmine
        .createSpy()
        .and.returnValue(of({ content: ['TOTALS'], exportAs: 'TXT' }));
      (component as any).billingAnalysisService.download = jasmine
        .createSpy()
        .and.returnValue(of({ blob: 'file', fileName: 'name' }));
      (component as any).dataSource.appliedQuery$ = of({ bsp: { id: 1 } });

      component.onDownload(false);
      tick();

      expect((component as any).billingAnalysisService.download).toHaveBeenCalledWith(
        { bsp: { id: 1 } },
        { content: ['TOTALS'], exportAs: 'TXT' }
      );
    }));
  });
});

function provideMockDropdownOptionsService() {
  const factory = () => {
    const service = createSpyObject(SalesDropdownOptionsService);

    service.getAgents.and.returnValue(of(mockedAgents));
    service.getAirlines.and.returnValue(of(mockedAirlines));
    service.getCurrencies.and.returnValue(of(mockedCurrencies));
    service.getStatisticalCode.and.returnValue(mockedStatisticalCode);
    service.getDocumentTypes.and.returnValue(mockedDocumentTypes);

    return service;
  };

  return { provide: SalesDropdownOptionsService, useFactory: factory };
}

function provideMockBillingAnalysisStore() {
  const factory = () => {
    const service = createSpyObject(BillingAnalysisStore);

    // mock last query
    service.getLastQuery$.and.returnValue(of(defaultAppState.sales.billingAnalysis.lastQuery));

    // mock default query
    service.getDefaultQuery$.and.returnValue(of(defaultAppState.sales.billingAnalysis.defaultQuery));

    return service;
  };

  return { provide: BillingAnalysisStore, useFactory: factory };
}

function provideMockFormBuilder() {
  const factory = () => {
    const formBuilder = createSpyObject(FormBuilder);
    formBuilder.group.and.callFake(() => createSpyObject(FormGroup));

    return formBuilder;
  };

  return { provide: FormBuilder, useFactory: factory };
}
