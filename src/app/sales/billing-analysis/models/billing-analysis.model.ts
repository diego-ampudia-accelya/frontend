import { FormOfPaymentType } from '~app/document/enums';
import { StatisticalCode } from '~app/sales/shared/models';
import { DocumentType } from '~app/shared/enums';

export interface BillingAnalysis {
  agentCode: string;
  airlineCode: string;
  billingAnalysisEndDate: string;
  period?: string;
  checkDigit: number;
  cashAmount: number;
  creditAmount: number;
  currencyCode: string;
  dateOfIssue: string;
  documentNumber: string;
  easyPayAmount: number;
  id: string;
  isoCountryCode: string;
  netToBePaid: number;
  statisticalCode: StatisticalCode;
  transactionCode: DocumentType;
  isSelected?: boolean;
}

export interface FormOfPayment {
  amount: number;
  creditCardEntity?: string;
  creditCardNumber?: string;
  maskedCreditCardNumber?: string;
  type: FormOfPaymentType;
}
