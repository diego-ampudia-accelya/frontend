export interface BillingAnalysisDefaultQuery {
  filterBy: {
    bsp: string;
    airline: string;
    agent: string;
    periodRange?: {
      periodFrom: string;
      periodTo: string;
    };
  };
}
