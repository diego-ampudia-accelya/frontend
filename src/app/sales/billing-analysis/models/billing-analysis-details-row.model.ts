import { FormOfPayment } from './billing-analysis.model';

export interface BillingAnalysisDetailsRow {
  id: string;
  airlineCode: string;
  agentCode: string;
  billingAnalysisEndDate: string;
  netFareAmount: number;
  isoCountryCode: string;
  couponUseIndicator: string;
  cashAmount: number;
  creditAmount: number;
  easyPayAmount: number;
  commissionRatePercent: number;
  commissionAmount: number;
  fareAdjustmentAmount: number;
  vatAmount: number;
  cancellationFees: number;
  conjunctionTickets: Array<{
    id: string;
    documentNumber: string;
  }>;
  taxes: Array<{
    type: string;
    amount: number;
  }>;
  destinationAirportCodes: string[];
  relatedDocumentNumbers: string[];
  formOfPayments: FormOfPayment[];
  tourCode?: string;
  transactionCode: string;
}
