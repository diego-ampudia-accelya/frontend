import { StatisticalCode } from '~app/sales/shared/models';
import { PeriodOption } from '~app/shared/components';
import { AgentGroupSummary, AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface BillingAnalysisFilters {
  bsp: Bsp;
  airline?: AirlineSummary;
  agent?: AgentSummary;
  agentGroup?: AgentGroupSummary;
  periodRange?: PeriodOption[];
  currency?: Currency[];
  statisticalCode?: StatisticalCode;
  transactionCode?: string[];
  isMultipopulation?: boolean;
}
