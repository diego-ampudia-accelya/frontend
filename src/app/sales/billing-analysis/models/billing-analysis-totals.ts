import { StatisticalCode } from '~app/sales/shared/models';
import { Currency } from '~app/shared/models/currency.model';

export interface BillingAnalysisTotals {
  cancellationFees: number;
  commissionAmount: number;
  commissionAmountOnCancellationPenalty: number;
  commissionableAmount: number;
  currency: Currency;
  netFareAmount: number;
  remittanceAmount: number;
  supplementaryAmount: number;
  taxOnCommission: number;
  taxes: number;
}

export interface BillingAnalysisTotalsFiltersBE {
  bspId: number;
  airlineId?: number | number[];
  agentId?: string;
  agentGroupId?: number;
  billingAnalysisEndingDateFrom: string;
  billingAnalysisEndingDateTo: string;
  transactionCode?: string[];
  statisticalCode?: StatisticalCode;
}
