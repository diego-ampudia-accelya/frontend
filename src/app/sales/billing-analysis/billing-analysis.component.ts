import { ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import FileSaver from 'file-saver';
import { isNil, omitBy } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { catchError, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  DialogService,
  FloatingPanelComponent,
  FooterButton,
  PeriodOption,
  PeriodUtils
} from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { DocumentType } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { AgentGroupSummary, AgentSummary, GdsSummary, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { UserType } from '~app/shared/models/user.model';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { getAmountFormat } from '~app/shared/utils/amount-format';
import * as fromBillingAnalysis from '../billing-analysis/reducers';
import { SalesFilter } from '../shared/models/sales-filter.model';
import { SalesDisplayFilterFormatter } from '../shared/sales-display-filter-formatter';
import { SalesDropdownOptionsService } from '../shared/services';
import { SalesDownloadDialogService } from '../shared/services/sales-download-dialog.service';
import { BillingQueryActions } from './actions';
import { BillingAnalysisDialogComponent } from './components/billing-analysis-dialog/billing-analysis-dialog.component';
import { BillingAnalysisStore } from './data/billing-analysis-store.service';
import { BillingAnalysisService } from './data/billing-analysis.service';
import { BillingAnalysis, BillingAnalysisFilters, BillingAnalysisTotals } from './models';
import { BillingAnalysisDetailsRow } from './models/billing-analysis-details-row.model';

@Component({
  selector: 'bspl-billing-analysis',
  templateUrl: './billing-analysis.component.html',
  styleUrls: ['./billing-analysis.component.scss'],
  providers: [
    DefaultQueryStorage,
    SalesDisplayFilterFormatter,
    { provide: QUERYABLE, useClass: BillingAnalysisService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [BillingAnalysisService]
    }
  ]
})
export class BillingAnalysisComponent implements OnInit, OnDestroy {
  @ViewChild(FloatingPanelComponent, { static: true }) public floatingPanelComponent: FloatingPanelComponent;
  @ViewChild('documentNumberLink', { static: true }) private documentNumberLinkTmpl: TemplateRef<any>;
  @ViewChild('amountTemplate', { static: true })
  private amountTemplate: TemplateRef<any>;

  public permissions = Permissions;
  public searchForm: FormGroup;
  public buttonDesign = ButtonDesign;
  public columns: GridColumn[];
  public airlineDropdownOptions$: Observable<DropdownOption[]>;
  public agentDropdownOptions$: Observable<DropdownOption[]>;
  public agentGroupDropdownOptions$: Observable<DropdownOption<AgentGroupSummary>[]>;
  public documentTypeOptions$: Observable<DropdownOption[]>;
  public statisticalCodeDropdownOptions$: Observable<DropdownOption[]>;
  public currencyDropdownOptions$: Observable<DropdownOption[]>;
  public reportingSystemOptions$: Observable<DropdownOption<GdsSummary>[]>;
  public periodDropdownOptions$: Observable<PeriodOption[]>;
  public legalNoticeMsg$: Observable<string>;
  public periodPickerYearsBack: number;
  public query: Partial<DataQuery> = {};
  public predefinedFilters$: Observable<any>;
  public documentType = DocumentType;
  public getAmountFormat = getAmountFormat;
  public isTotalLoading = false;
  public totals: BillingAnalysisTotals[];
  public isDetailsRowLoading = false;
  public rowDetails = {} as BillingAnalysisDetailsRow;
  public isActionButtonDisabled = false;
  public currenciesForTotal: DropdownOption[];
  public currentPeriod$: Observable<PeriodOption>;
  public bspSelected: Bsp;
  public formFactory: FormUtil;
  public isMultipopulation$ = this.appStore.pipe(select(fromBillingAnalysis.getIsMultipopulation));
  public selectedDocumentsCount$ = this.appStore.pipe(select(fromBillingAnalysis.getSelectedDocumentsCount));
  public selectedDocumentsIds$ = this.appStore.pipe(select(fromBillingAnalysis.getSelectedDocumentsIds));
  public multipopulationDetailsError$ = this.appStore.pipe(select(fromBillingAnalysis.getMultipopulationDetailsError));
  public data$ = this.appStore.pipe(select(fromBillingAnalysis.getDocumentsData));
  public hasAdmPermission = false;
  public hasAcmPermission = false;

  protected destroy$ = new Subject();

  private leanUsersYears = 5;
  private nonLeanUsersYears = 2;
  private bspQueryForAgents: Bsp;

  public get hasAgentPermissions(): boolean {
    return this.permissionsService.hasUserType(UserType.AGENT);
  }

  public get hasAirlinePermissions(): boolean {
    return this.permissionsService.hasUserType(UserType.AIRLINE);
  }

  constructor(
    public appConfiguration: AppConfigurationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    protected formBuilder: FormBuilder,
    public displayFilterFormatter: SalesDisplayFilterFormatter,
    public dataSource: QueryableDataSource<BillingAnalysis>,
    protected store: BillingAnalysisStore,
    private dropdownOptionsService: SalesDropdownOptionsService,
    private permissionsService: PermissionsService,
    private dateTimePipe: DateTimeFormatPipe,
    private billingAnalysisService: BillingAnalysisService,
    private salesDownloadDialogService: SalesDownloadDialogService,
    private cd: ChangeDetectorRef,
    private appStore: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.createForm();
    this.checkMultipopulation();
    this.bspQueryForAgents = this.activatedRoute.snapshot.data.agentBsp;
    this.initializeDropdownsForAgent();
    this.setPredefinedFilters();
    this.store.getLastQuery$().pipe(takeUntil(this.destroy$)).subscribe(this.loadQuery.bind(this));
    this.initializeDisclaimerMsg();
    this.setPeriodPickerYearsBack();
    this.hasAdmOrAcmPermission();

    if (this.hasAirlinePermissions) {
      this.initializeFiltersListener();
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public hasAdmOrAcmPermission(): void {
    this.hasAdmPermission = this.permissionsService.hasPermission(Permissions.issueActionAdm);
    this.hasAcmPermission = this.permissionsService.hasPermission(Permissions.issueActionAcm);
  }

  public search(query: DataQuery): void {
    // bsp is missing in the seach form, so preserve it from the previous query
    query.filterBy = {
      ...query.filterBy,
      bsp: this.bspSelected
    };
    this.query = query;
    // saving the query triggers loading the data
    this.store.save(query);
    this.floatingPanelComponent.close();
  }

  public onDownload(isDetailed: boolean): void {
    combineLatest([this.salesDownloadDialogService.openDownloadDialog(isDetailed), this.dataSource.appliedQuery$])
      .pipe(
        first(),
        switchMap(([exportAs, dataQuery]) => this.billingAnalysisService.download(dataQuery, exportAs))
      )
      .subscribe(fileInfo => {
        FileSaver.saveAs(fileInfo.blob, fileInfo.fileName);
      });
  }

  public toggleTotals(): void {
    this.floatingPanelComponent.toggle();
  }

  public async onTotalPanelOpened(): Promise<void> {
    this.totals = await this.getBillingAnalysisTotals().toPromise();
  }

  public onTotalPanelClosed(): void {
    this.totals = null;
  }

  public getCurrency(entry: BillingAnalysis): Observable<Currency> {
    return this.currencyDropdownOptions$.pipe(
      map(options => options.map(({ value }) => value).find(currency => currency.code === entry.currencyCode))
    );
  }

  public onSelectedRows(documents: BillingAnalysis[]): void {
    this.appStore.dispatch(new BillingQueryActions.UpdateSelectedDocuments(documents));
  }

  public onRowToggle(event: { type: string; value: BillingAnalysis }) {
    this.rowDetails = {} as BillingAnalysisDetailsRow;

    if (event.type === 'row') {
      const documentId = event?.value?.id;
      this.isDetailsRowLoading = true;
      this.billingAnalysisService
        .getAdditionalDetails(documentId)
        .pipe(
          tap(detailedRowInfo => {
            this.isDetailsRowLoading = false;
            this.rowDetails = detailedRowInfo;
          })
        )
        .subscribe();
    }
  }

  public openSetupDialog() {
    this.dialogService.open(BillingAnalysisDialogComponent, {
      data: {
        footerButtonsType: FooterButton.Search,
        mainButtonType: FooterButton.Search
      }
    });

    this.floatingPanelComponent.close();
  }

  public openDocument(id: string): void {
    this.router.navigate([ROUTES.DOCUMENT_VIEW.url, id]);
  }

  public clearSelectedDocuments(): void {
    this.appStore.dispatch(new BillingQueryActions.ClearSelectedDocuments());
  }

  public issueAdmaClick(): void {
    this.appStore.dispatch(new BillingQueryActions.GetDocumentsDetails('adma'));
  }

  public issueAcmaClick(): void {
    this.appStore.dispatch(new BillingQueryActions.GetDocumentsDetails('acma'));
  }

  /**
   * Loads the search form options.
   */
  protected loadDropdownOptions(bsp: Bsp): void {
    this.airlineDropdownOptions$ = this.dropdownOptionsService.getAirlines(bsp);
    this.agentDropdownOptions$ = this.getAgentDropDownByUserType(bsp);
    this.agentGroupDropdownOptions$ = this.dropdownOptionsService.getAgentGroups({ bspId: bsp.id });
    this.currencyDropdownOptions$ = this.dropdownOptionsService.getCurrencies(bsp);
    this.periodDropdownOptions$ = this.dropdownOptionsService.getPeriods(bsp);
    this.reportingSystemOptions$ = this.dropdownOptionsService.getReportingSystems(bsp);

    this.statisticalCodeDropdownOptions$ = of(
      this.dropdownOptionsService.getStatisticalCode({ excludeAllStatisticalCodes: true })
    );
    this.documentTypeOptions$ = of(this.dropdownOptionsService.getDocumentTypes());
  }

  protected setPredefinedFilters(): void {
    if (!this.hasAgentPermissions) {
      this.predefinedFilters$ = this.store.getDefaultQuery$().pipe(map(query => query && query.filterBy));
    } else {
      this.predefinedFilters$ = this.currentPeriod$.pipe(
        map(currentPeriod => ({
          bsp: this.bspQueryForAgents,
          periodRange: [currentPeriod, currentPeriod]
        }))
      );
    }
  }

  protected loadQuery(query: DataQuery): void {
    if (query?.filterBy?.bsp) {
      this.bspSelected = query?.filterBy?.bsp;
    } else {
      this.updateLastQueryDefaultBsp(query);
    }

    if (!this.hasAgentPermissions) {
      this.loadDropdownOptions(query?.filterBy?.bsp);
      this.getCurrentPeriod();
      this.loadData(query);
      this.query = query;
      setTimeout(() => {
        this.searchForm.patchValue(query.filterBy);
      }, 0);
    } else {
      this.loadQueryForAgent(query);
    }
  }

  private getUserDefaultBsp(): Bsp {
    let bsp: Bsp;
    this.appStore
      .select(getUserDefaultBsp)
      .pipe(first())
      .subscribe(defaultBsp => (bsp = defaultBsp));

    return bsp;
  }

  private updateLastQueryDefaultBsp(query: DataQuery) {
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy,
        bsp: this.getUserDefaultBsp()
      }
    };

    // Save last query with default bsp
    this.store.save(dataQuery);
  }

  private initializeDropdownsForAgent(): void {
    if (this.hasAgentPermissions) {
      this.loadDropdownOptions(this.bspQueryForAgents);
      this.getCurrentPeriod();
    }
  }

  private getCurrentPeriod(): void {
    this.currentPeriod$ = this.periodDropdownOptions$?.pipe(map(periods => PeriodUtils.findCurrentPeriod(periods)));
  }

  private initializeDisclaimerMsg(): void {
    this.legalNoticeMsg$ = combineLatest([this.dataSource?.appliedQuery$, this.currentPeriod$]).pipe(
      map(([query, currentPeriod]) => {
        const isFilterByCurrentPeriod = query.filterBy.periodRange?.some(
          periodRange => currentPeriod?.period === periodRange.period
        );

        return isFilterByCurrentPeriod
          ? 'sales.billingAnalysis.tableNoticeMsgCurrentPeriod'
          : 'sales.billingAnalysis.tableNoticeMsg';
      })
    );
  }

  private setPeriodPickerYearsBack(): void {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.periodPickerYearsBack = hasLeanPermission ? this.leanUsersYears : this.nonLeanUsersYears;
  }

  private loadQueryForAgent(query): void {
    const queryPeriod = query.filterBy.periodRange;
    this.currentPeriod$?.pipe(takeUntil(this.destroy$)).subscribe(currentPeriod => {
      const newDefaultQuery = {
        ...query,
        filterBy: omitBy(
          {
            ...query.filterBy,
            bsp: this.bspSelected ? this.bspSelected : this.bspQueryForAgents,
            periodRange: !queryPeriod.length ? [currentPeriod, currentPeriod] : queryPeriod
          },
          isNil
        )
      };
      this.query = newDefaultQuery;

      this.loadData(newDefaultQuery);
      this.searchForm.patchValue(newDefaultQuery.filterBy);
    });
  }

  private loadData(query: Partial<DataQuery>) {
    this.dataSource.get(query);
    this.appStore.dispatch(new BillingQueryActions.LoadDocumentsData());
  }

  private getAgentDropDownByUserType(bsp: Bsp): Observable<DropdownOption<AgentSummary>[]> {
    const queryfilters = { bspId: [bsp?.id] };
    const isAgentGroupUser = this.permissionsService.hasUserType(UserType.AGENT_GROUP);

    return isAgentGroupUser
      ? this.getAgentGroupDropDown(queryfilters)
      : this.dropdownOptionsService.getAgents(queryfilters);
  }

  private getAgentGroupDropDown(queryfilters): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = Permissions.readBillingAnalysis;
    const aGroupUserHasPermission = this.permissionsService.hasPermission(aGroupUserPermission);

    if (aGroupUserHasPermission) {
      const filter = { ...queryfilters, permission: aGroupUserPermission };

      return this.dropdownOptionsService.getAgentGroupsAgents(filter);
    } else {
      return of(null);
    }
  }

  private createForm() {
    this.searchForm = this.formFactory.createGroup<SalesFilter>({
      airline: [],
      agent: [],
      agentGroup: [],
      currency: [],
      statisticalCode: [],
      periodRange: [],
      transactionCode: [],
      reportingSystem: []
    });
  }

  private initializeColumns(): void {
    const canViewDocuments = this.permissionsService.hasPermission(Permissions.readDocumentDetails);

    this.columns = [
      {
        prop: 'transactionCode',
        name: 'sales.billingAnalysis.columns.documentType',
        sortable: true,
        resizeable: true,
        flexGrow: 0.8
      },
      {
        prop: 'documentNumber',
        name: 'sales.billingAnalysis.columns.documentNumber',
        // Show document numbers as links when user has the permission to view documents
        cellTemplate: canViewDocuments ? this.documentNumberLinkTmpl : null,
        sortable: true,
        resizeable: true,
        flexGrow: 0.6
      },
      {
        prop: 'reportingSystem',
        name: 'sales.billingAnalysis.columns.reportingSystem',
        sortable: true,
        resizeable: true,
        flexGrow: 0.6
      },
      {
        prop: 'airlineCode',
        name: 'sales.billingAnalysis.columns.airlineCode',
        sortable: true,
        resizeable: true,
        flexGrow: 0.6,
        hidden: this.hasAirlinePermissions
      },
      {
        prop: 'agentCode',
        name: 'sales.billingAnalysis.columns.agentCode',
        sortable: true,
        resizeable: true,
        flexGrow: 0.4,
        hidden: this.hasAgentPermissions
      },
      {
        prop: 'agentName',
        name: 'sales.billingAnalysis.columns.agentName',
        sortable: false,
        resizeable: true,
        flexGrow: 0.6,
        hidden: this.hasAgentPermissions
      },
      {
        prop: 'statisticalCode',
        name: 'sales.billingAnalysis.columns.salesType',
        sortable: true,
        resizeable: true,
        flexGrow: 0.6,
        pipe: {
          transform: value => this.dropdownOptionsService.translateStatisticalCode(value)
        }
      },
      {
        prop: 'dateOfIssue',
        name: 'sales.billingAnalysis.columns.issueDate',
        sortable: true,
        resizeable: true,
        flexGrow: 0.4,
        pipe: { transform: v => this.dateTimePipe.transform(v, 'date') }
      },
      {
        prop: 'period',
        name: 'sales.billingAnalysis.columns.period',
        sortable: true,
        resizeable: true,
        flexGrow: 0.4
      },
      {
        prop: 'currencyCode',
        name: 'sales.billingAnalysis.columns.currency',
        sortable: true,
        resizeable: true,
        flexGrow: 0.4
      },
      {
        prop: 'cashAmount',
        name: 'sales.billingAnalysis.columns.cash',
        sortable: true,
        resizeable: true,
        cellTemplate: this.amountTemplate,
        cellClass: 'text-right',
        flexGrow: 0.4
      },
      {
        prop: 'creditAmount',
        name: 'sales.billingAnalysis.columns.credit',
        sortable: true,
        resizeable: true,
        cellTemplate: this.amountTemplate,
        cellClass: 'text-right',
        flexGrow: 0.4
      },
      {
        prop: 'easyPayAmount',
        name: 'sales.billingAnalysis.columns.easyPay',
        sortable: true,
        resizeable: true,
        cellTemplate: this.amountTemplate,
        cellClass: 'text-right',
        flexGrow: 0.4
      },
      {
        prop: 'netToBePaid',
        name: 'sales.billingAnalysis.columns.netToBePaid',
        sortable: true,
        resizeable: true,
        cellTemplate: this.amountTemplate,
        cellClass: 'text-right',
        flexGrow: 0.4
      }
    ];
  }

  private checkMultipopulation(): void {
    this.isMultipopulation$.pipe(takeUntil(this.destroy$)).subscribe(isMultipopulation => {
      this.initializeColumns();

      if (isMultipopulation) {
        this.activateMultipopulation();
      } else {
        this.deactivateMultipopulation();
      }
    });
  }

  private deactivateMultipopulation(): void {
    const index = this.columns.findIndex(column => column.prop === 'isSelected');

    if (index !== -1) {
      this.columns.splice(index, 1);
    }
  }

  private activateMultipopulation(): void {
    this.columns.unshift({
      prop: 'isSelected',
      sortable: false,
      maxWidth: 40,
      resizeable: false,
      headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
      cellTemplate: 'checkboxWithDisableRowCellTmpl',
      cellClass: this.isChanged.bind(this)
    });
  }

  private isChanged({ row, column }): {
    'checkbox-control-changed': boolean;
    'checkbox-control': boolean;
  } {
    const { prop } = column;

    return {
      'checkbox-control-changed': row[prop] === 'isSelected',
      'checkbox-control': true
    };
  }

  private initializeFiltersListener() {
    const agentControl = FormUtil.get<BillingAnalysisFilters>(this.searchForm, 'agent');
    const agentGroupControl = FormUtil.get<BillingAnalysisFilters>(this.searchForm, 'agentGroup');

    agentGroupControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(agroupValueChange => {
      if (agroupValueChange) {
        //If Agent Group value, Agent dropdown only depends of Agent Group Id
        const agentGroupIdValue = { agentGroupId: agroupValueChange.id };
        this.agentDropdownOptions$ = this.dropdownOptionsService.getAgentGroupsAgents(agentGroupIdValue);

        //If Agent Group value changes must reset the Agent control
        if (agentControl.value && !agentGroupControl.pristine) {
          agentControl.patchValue(null);
        }
      }

      if (!agroupValueChange) {
        //If not Agent Group value, Agent dropdown only depends of BSP Id selected
        this.agentDropdownOptions$ = this.dropdownOptionsService.getAgents({
          bspId: [this.bspSelected.id]
        });
      }

      this.isActionButtonDisabled = !agroupValueChange && !agentControl.value;
      this.cd.detectChanges();
    });

    //If no value in Agent Control and Agent Group Control has value, search button must be enabled.
    agentControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(agentValueChange => {
      this.isActionButtonDisabled = !agentValueChange && !agentGroupControl.value;
      this.cd.detectChanges();
    });
  }

  private getBillingAnalysisTotals(): Observable<BillingAnalysisTotals[]> {
    return this.store.updateFilterForUser$().pipe(
      first(),
      tap(() => (this.isTotalLoading = true)),
      switchMap(query => {
        const result$: Observable<BillingAnalysisTotals[]> = this.billingAnalysisService.getTotals(query);

        return result$.pipe(catchError(() => of([])));
      }),
      tap(() => (this.isTotalLoading = false))
    );
  }
}
