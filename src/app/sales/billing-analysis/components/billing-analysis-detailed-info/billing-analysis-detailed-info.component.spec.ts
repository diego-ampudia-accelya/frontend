import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslateAsyncPipe } from 'angular-l10n';
import { MockDeclaration } from 'ng-mocks';

import { BillingAnalysisDetailsRow } from '../../models/billing-analysis-details-row.model';
import { BillingAnalysisDetailedInfoComponent } from './billing-analysis-detailed-info.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { FormOfPaymentType } from '~app/document/enums';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

describe('BillingAnalysisDetailedInfoComponent', () => {
  let component: BillingAnalysisDetailedInfoComponent;
  let fixture: ComponentFixture<BillingAnalysisDetailedInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        BillingAnalysisDetailedInfoComponent,
        MockDeclaration(L10nTranslateAsyncPipe),
        AbsoluteDecimalPipe
      ],
      providers: [mockProvider(PermissionsService)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingAnalysisDetailedInfoComponent);
    component = fixture.componentInstance;
    component.row = {} as BillingAnalysisDetailsRow;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should map formOfPayments for comment field', () => {
    const formOfPayments = [
      {
        amount: 234,
        creditCardEntity: 'AX',
        creditCardNumber: '234236323',
        maskedCreditCardNumber: '23434XXXXX',
        type: FormOfPaymentType.CC
      },
      {
        amount: 234,
        creditCardNumber: '234236323',
        maskedCreditCardNumber: '23434XXXXX',
        type: FormOfPaymentType.EX
      },
      {
        amount: 0,
        type: FormOfPaymentType.CA
      }
    ];

    const expected = [
      {
        creditCardNumber: '23434XXXXX',
        entity: 'AX',
        type: FormOfPaymentType.CC
      },
      {
        creditCardNumber: '234236323',
        entity: undefined,
        type: FormOfPaymentType.EX
      }
    ];

    expect(component.mapFormOfPayments(formOfPayments)).toEqual(jasmine.objectContaining(expected));
  });

  it('should not add not valid formOfPayments for comment field', () => {
    const formOfPayments = [
      {
        amount: 234,
        type: FormOfPaymentType.CA
      },
      {
        amount: 0,
        type: FormOfPaymentType.VD
      },
      {
        amount: 0,
        type: FormOfPaymentType.MSCA
      }
    ];

    const expected = [];

    expect(component.mapFormOfPayments(formOfPayments)).toEqual(jasmine.objectContaining(expected));
  });
});
