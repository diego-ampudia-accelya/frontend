import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { FormOfPayment } from '../../models';
import { BillingAnalysisDetailsRow } from '../../models/billing-analysis-details-row.model';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { FormOfPaymentType } from '~app/document/enums';
import { Permissions } from '~app/shared/constants/permissions';
import { DocumentType } from '~app/shared/enums';
import { Currency } from '~app/shared/models/currency.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-billing-analysis-detailed-info',
  templateUrl: './billing-analysis-detailed-info.component.html',
  styleUrls: ['./billing-analysis-detailed-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BillingAnalysisDetailedInfoComponent {
  @Input() row: BillingAnalysisDetailsRow;
  @Input() currency: Currency;

  @Output() openDocument = new EventEmitter();

  public documentType = DocumentType;
  public getAmountFormat = getAmountFormat;

  public canViewDocument = this.permissionsService.hasPermission(Permissions.readDocumentDetails);

  constructor(private permissionsService: PermissionsService) {}

  public mapFormOfPayments(
    formOfPayments: FormOfPayment[]
  ): { creditCardNumber?: string; entity?: string; type: FormOfPaymentType }[] {
    return (
      formOfPayments &&
      formOfPayments
        .filter(formOfPayment => this.hasValidFormOfPaymentType(formOfPayment.type))
        .map(payment => {
          let creditCardNumber: string;
          let entity: string;

          if (payment.type === FormOfPaymentType.EP || payment.type === FormOfPaymentType.CC) {
            creditCardNumber = payment.maskedCreditCardNumber;
            entity = payment.creditCardEntity;
          }

          if (payment.type === FormOfPaymentType.EX) {
            creditCardNumber = payment.creditCardNumber;
          }

          return {
            creditCardNumber,
            entity,
            type: payment.type
          };
        })
    );
  }

  private hasValidFormOfPaymentType(type: FormOfPaymentType): boolean {
    return (
      type === FormOfPaymentType.CC ||
      type === FormOfPaymentType.EP ||
      type === FormOfPaymentType.EX ||
      type === FormOfPaymentType.MSCC
    );
  }
}
