import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MockPipe } from 'ng-mocks';

import { BillingAnalysisTotals } from '../../models';
import { BillingAnalysisTotalsComponent } from './billing-analysis-totals.component';
import { DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';
import { TranslatePipeMock } from '~app/test';

const totals: BillingAnalysisTotals[] = [
  {
    currency: { id: 1, code: 'EUR', decimals: 4 },
    commissionableAmount: 111.111,
    taxes: 2222.2222,
    commissionAmountOnCancellationPenalty: 3333.3333,
    commissionAmount: 4444.4444,
    supplementaryAmount: 5555.555,
    taxOnCommission: 666.6666,
    cancellationFees: 7777.7777,
    remittanceAmount: 8888.888,
    netFareAmount: 9999.9999
  },
  {
    currency: { id: 2, code: 'ESP', decimals: 2 },
    commissionableAmount: 111.123,
    taxes: 2222.123,
    commissionAmountOnCancellationPenalty: 3333.123,
    commissionAmount: 4444.123,
    supplementaryAmount: 5555.123,
    taxOnCommission: 666.123,
    cancellationFees: 7777.123,
    remittanceAmount: 8888.123,
    netFareAmount: 9999.123
  }
];

describe('BillingAnalysisTotalsComponent', () => {
  let component: BillingAnalysisTotalsComponent;
  let fixture: ComponentFixture<BillingAnalysisTotalsComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BillingAnalysisTotalsComponent, TranslatePipeMock, MockPipe(EmptyPipe)],
      imports: [],
      schemas: [NO_ERRORS_SCHEMA],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingAnalysisTotalsComponent);
    component = fixture.componentInstance;
    component.items = totals;

    fixture.detectChanges();
  });

  it('should create and have proper default values', () => {
    expect(component).toBeTruthy();
    expect(component.visibleItem).toEqual({} as any);
  });

  it('should set an initial currency and visibleItem', () => {
    const expectedSelectedCurrency: Currency = totals[1].currency;

    component.items = totals;
    component.ngOnChanges({ items: new SimpleChange(undefined, totals, false) });

    expect(component.selectedCurrency).toEqual(expectedSelectedCurrency);
    expect(component.visibleItem).toEqual(totals[1]);
  });

  it('should generate currencyOptions (sorted by label) based on the provided items', () => {
    component.items = totals;
    component.ngOnChanges({ items: new SimpleChange(undefined, totals, false) });

    const expectedCurrencyOptions: DropdownOption<Currency>[] = [
      { value: { id: 2, code: 'ESP', decimals: 2 }, label: 'ESP' },
      { value: { id: 1, code: 'EUR', decimals: 4 }, label: 'EUR' }
    ];
    expect(component.currencyOptions).toEqual(expectedCurrencyOptions);
  });

  describe('changeCurrency()', () => {
    it('should set the selectedCurrency', () => {
      const currency: Currency = { id: 1, code: 'EUR', decimals: 2 };

      component.changeCurrency(currency);

      expect(component.selectedCurrency).toEqual(currency);
    });

    it('should set the visibleItem to an empty object when the items are null', () => {
      const currency: Currency = { id: -10, code: 'test non-existing', decimals: 2 };

      component.items = null;
      component.changeCurrency(currency);

      expect(component.visibleItem).toEqual({} as any);
    });

    it('should set the visibleItem to an empty object when called with null currency', () => {
      component.changeCurrency(null);

      expect(component.visibleItem).toEqual({} as any);
    });

    it('should set the visibleItem to an empty object when called with non-existing currency', () => {
      const currency: Currency = { id: -10, code: 'test non-existing', decimals: 2 };

      component.changeCurrency(currency);

      expect(component.visibleItem).toEqual({} as any);
    });

    it('should set the visibleItem to the found item by currency.code', () => {
      const currency: Currency = { id: -10, code: 'ESP', decimals: 2 };

      component.changeCurrency(currency);

      expect(component.visibleItem).toEqual(totals[1]);
    });
  });
});
