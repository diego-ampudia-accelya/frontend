import { DecimalPipe } from '@angular/common';
import { Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { orderBy } from 'lodash';

import { BillingAnalysisTotals } from '../../models';
import { SimpleTableColumn } from '~app/shared/components';
import { SelectMode } from '~app/shared/enums';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { getAmountFormat } from '~app/shared/utils/amount-format';

@Component({
  selector: 'bspl-billing-analysis-totals',
  templateUrl: './billing-analysis-totals.component.html',
  styleUrls: ['./billing-analysis-totals.component.scss'],
  providers: [DecimalPipe]
})
export class BillingAnalysisTotalsComponent implements OnInit, OnChanges {
  @Input() items: BillingAnalysisTotals[] = [];

  @ViewChild('amountTemplate', { static: true })
  private amountTemplate: TemplateRef<any>;

  public selectedCurrency: Currency;
  public selectMode = SelectMode;
  public getAmountFormat = getAmountFormat;
  public totalPerCurrenciesColumns: SimpleTableColumn[];

  private _currencyOptions: DropdownOption<Currency>[] = [];
  public get currencyOptions(): DropdownOption<Currency>[] {
    return this._currencyOptions;
  }

  private _visibleItem = {} as BillingAnalysisTotals;
  public get visibleItem(): BillingAnalysisTotals {
    return this._visibleItem;
  }

  public ngOnInit(): void {
    this.initTotalPerCurrenciesColumns();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.items) {
      this._currencyOptions = this.generateCurrencyOptions(this.items);
      const defaultCurrency = this.currencyOptions && this.currencyOptions[0] ? this.currencyOptions[0].value : null;

      this.changeCurrency(defaultCurrency);
    }
  }

  public changeCurrency(currency: Currency): void {
    this.selectedCurrency = currency;
    this._visibleItem =
      this.items?.find(item => item.currency.code === currency?.code) || ({} as BillingAnalysisTotals);
  }

  private initTotalPerCurrenciesColumns(): void {
    const amountCellConfig = { customStyle: 'text-right', cellTemplate: this.amountTemplate };
    this.totalPerCurrenciesColumns = [
      { ...amountCellConfig, field: 'commissionableAmount', header: 'sales.billingAnalysis.totalsTable.amount' },
      { ...amountCellConfig, field: 'taxes', header: 'sales.billingAnalysis.totalsTable.taxes' },
      { ...amountCellConfig, field: 'commissionAmount', header: 'sales.billingAnalysis.totalsTable.commission' },
      { ...amountCellConfig, field: 'supplementaryAmount', header: 'sales.billingAnalysis.totalsTable.fareAdjustment' },
      { ...amountCellConfig, field: 'taxOnCommission', header: 'sales.billingAnalysis.totalsTable.vatCommission' },
      {
        ...amountCellConfig,
        field: 'cancellationFees',
        header: 'sales.billingAnalysis.totalsTable.cancellationFees'
      },
      {
        cellTemplate: amountCellConfig.cellTemplate,
        customStyle: amountCellConfig.customStyle + ' net-fare-amount-cell',
        field: 'netFareAmount',
        header: 'sales.billingAnalysis.totalsTable.netFareAmount'
      },
      { ...amountCellConfig, field: 'remittanceAmount', header: 'sales.billingAnalysis.totalsTable.netToBePaid' }
    ];
  }

  private generateCurrencyOptions(totals: BillingAnalysisTotals[]): DropdownOption<Currency>[] {
    totals = totals || [];
    const orderKey: keyof DropdownOption = 'label';
    const options: DropdownOption<Currency>[] = totals.map(total => ({
      value: total.currency,
      label: total.currency.code
    }));

    return orderBy(options, orderKey);
  }
}
