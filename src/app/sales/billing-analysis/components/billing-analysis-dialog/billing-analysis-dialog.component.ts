import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { isNil, last, omitBy } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { first, map, takeUntil, tap } from 'rxjs/operators';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { DialogConfig, DialogService, FooterButton, PeriodOption, PeriodUtils } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DataQuery, QUERYABLE } from '~app/shared/components/list-view';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil } from '~app/shared/helpers';
import { AgentGroupSummary, AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { UserType } from '~app/shared/models/user.model';
import { BillingQueryActions } from '../../actions';
import { BillingAnalysisService } from '../../data/billing-analysis.service';
import { BillingAnalysisFilters } from '../../models/billing-analysis-filters.model';
import { billingAnalysisDialogValidator } from './validators/billing-analysis-dialog.validator';

@Component({
  selector: 'bspl-billing-analysis-dialog',
  templateUrl: './billing-analysis-dialog.component.html',
  styleUrls: ['./billing-analysis-dialog.component.scss'],
  providers: [{ provide: QUERYABLE, useClass: BillingAnalysisService }]
})
export class BillingAnalysisDialogComponent implements OnInit, OnDestroy {
  public readonly oneElement = 1;
  public form: FormGroup;
  public searchButton: ModalAction;
  public userBspOptions$: Observable<DropdownOption<Bsp>[]>;
  public airlineDropdownOptions$: Observable<DropdownOption<AirlineSummary>[]>;
  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public agentGroupDropdownOptions$: Observable<DropdownOption<AgentGroupSummary>[]>;
  public periodDropdownOptions$: Observable<PeriodOption[]>;
  public canQueryAgent: boolean;
  public canQueryAirline: boolean;
  public isAirlineUser: boolean;
  public periodPickerYearsBack: number;

  public get hasValidBsp(): boolean {
    return this.form.get('bsp').valid;
  }

  public predefinedFilters = {
    bsp: null,
    periodRange: null
  };

  public resetableControls = ['airline', 'agent', 'agentGroup'];
  private leanUsersYears = 5;
  private nonLeanUsersYears = 2;
  private formUtil: FormUtil;
  private destroy$ = new Subject();

  constructor(
    public formBuilder: FormBuilder,
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public translationService: L10nTranslationService,
    private store: Store<AppState>,
    private dropdownOptionsService: SalesDropdownOptionsService,
    private permissionsService: PermissionsService
  ) {
    this.config.data.title = 'sales.billingAnalysisDialog.title';
    this.formUtil = new FormUtil(this.formBuilder);
  }

  ngOnInit() {
    this.setUserPermissions();
    this.createForm();
    this.disableSearchButton();
    this.initializeUserBspOptions();
    this.setPeriodPickerYearsBack();
    this.clearValidatorsByUserType();
    this.handleFormValidityChange().subscribe();
    this.onBspChange().subscribe();
    this.onButtonClicked().subscribe();
    this.initializeFiltersListener();
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  public disableSearchButton() {
    this.searchButton = this.config.data.buttons.find(button => button.type === FooterButton.Search);
    if (this.searchButton) {
      this.searchButton.isDisabled = true;
    }
  }

  public onBspChange(): Observable<any> {
    return FormUtil.get<BillingAnalysisFilters>(this.form, 'bsp').valueChanges.pipe(
      tap(bsp => {
        if (bsp) {
          this.modifyFiltersValuesByBsp(bsp);
        }

        this.resetableControls.forEach(formControlName => {
          this.form.controls[formControlName].reset();
        });
      }),
      takeUntil(this.destroy$)
    );
  }

  public handleFormValidityChange(): Observable<any> {
    return this.form.statusChanges.pipe(
      tap(() => {
        if (this.searchButton) {
          this.searchButton.isDisabled = this.form.invalid;
        }
      }),
      takeUntil(this.destroy$)
    );
  }

  public setUserPermissions(): void {
    this.isAirlineUser = this.permissionsService.hasUserType(UserType.AIRLINE);
    this.canQueryAirline = !this.isAirlineUser;
    this.canQueryAgent =
      !this.permissionsService.hasUserType(UserType.AGENT) &&
      !this.permissionsService.hasUserType(UserType.AGENT_GROUP);
  }

  public modifyFiltersValuesByBsp(selectedBsp: Bsp) {
    this.predefinedFilters.bsp = selectedBsp;
    if (selectedBsp) {
      this.airlineDropdownOptions$ = this.dropdownOptionsService.getAirlines(selectedBsp);
      this.agentDropdownOptions$ = this.dropdownOptionsService.getAgents({ bspId: [selectedBsp?.id] });
      this.agentGroupDropdownOptions$ = this.dropdownOptionsService.getAgentGroups({ bspId: [selectedBsp?.id] });
      this.periodDropdownOptions$ = this.dropdownOptionsService.getPeriods(selectedBsp).pipe(
        tap(periodOptions => {
          const currentPeriod = PeriodUtils.findCurrentPeriod(periodOptions);
          if (currentPeriod) {
            this.form.controls.periodRange.setValue([currentPeriod, currentPeriod]);
          }

          return periodOptions;
        })
      );
    }
  }

  public onButtonClicked() {
    return this.reactiveSubject.asObservable.pipe(
      tap(action => {
        if (action.clickedBtn === FooterButton.Search) {
          const newDefaultQuery: DataQuery = {
            sortBy: [],
            paginateBy: { page: 0, size: PAGINATION.DEFAULT_PAGE_SIZE, totalElements: 0 },
            filterBy: omitBy(
              {
                bsp: this.form.value.bsp,
                agent: this.form.value.agent,
                agentGroup: this.form.value.agentGroup,
                airline: this.form.value.airline,
                periodRange: this.form.value.periodRange
              },
              isNil
            )
          };

          //Save isMultipopulation value on store to display rows checkbox
          const isMultipopulationControl = FormUtil.get<BillingAnalysisFilters>(this.form, 'isMultipopulation');
          this.store.dispatch(new BillingQueryActions.UpdateIsMultipopulation(isMultipopulationControl.value));

          // reset last query and set new default query
          this.store.dispatch(new BillingQueryActions.UpdateLastQuery(newDefaultQuery));
          this.store.dispatch(new BillingQueryActions.UpdateDefaultQuery(newDefaultQuery));

          this.dialogService.close();
        }
      })
    );
  }

  public hasAdmOrAcmPermission(): boolean {
    const hasIssueActionAdmPermission = this.permissionsService.hasPermission(Permissions.issueActionAdm);
    const hasIssueActionAcmPermission = this.permissionsService.hasPermission(Permissions.issueActionAcm);

    return hasIssueActionAdmPermission || hasIssueActionAcmPermission;
  }

  private clearValidatorsByUserType() {
    if (!this.canQueryAirline) {
      this.form.controls.airline.clearValidators();
      this.form.controls.airline.updateValueAndValidity();
    }
  }

  private initializeFiltersListener() {
    const agentControl = FormUtil.get<BillingAnalysisFilters>(this.form, 'agent');
    const agentGroupControl = FormUtil.get<BillingAnalysisFilters>(this.form, 'agentGroup');

    agentGroupControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(agroupValueChange => {
      if (agroupValueChange) {
        //If Agent Group value, Agent dropdown depends of Agent Group Id
        const agentGroupIdValue = { agentGroupId: agroupValueChange.id };
        this.agentDropdownOptions$ = this.dropdownOptionsService.getAgentGroupsAgents(agentGroupIdValue);

        //If Agent Group value changes must reset the Agent control
        if (agentControl.value) {
          agentControl.patchValue(null);
          agentControl.markAsPristine();
        }
      }

      if (!agroupValueChange) {
        //If not Agent Group value, Agent dropdown depends of BSP Id selected
        const bspIdValue = FormUtil.get<BillingAnalysisFilters>(this.form, 'bsp').value?.id;
        this.agentDropdownOptions$ = this.dropdownOptionsService.getAgents({
          bspId: [bspIdValue]
        });
      }
    });
  }

  private setPeriodPickerYearsBack(): void {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.periodPickerYearsBack = hasLeanPermission ? this.leanUsersYears : this.nonLeanUsersYears;
  }

  private createForm(): void {
    this.form = this.formUtil.createGroup<BillingAnalysisFilters>({
      bsp: [null, Validators.required],
      airline: [null, Validators.required],
      agentGroup: [],
      agent: [],
      periodRange: [],
      isMultipopulation: [false, Validators.required]
    });

    if (this.isAirlineUser) {
      this.form.setValidators(billingAnalysisDialogValidator());
    }
  }

  private initializeUserBspOptions(): void {
    this.userBspOptions$ = this.store.pipe(
      select(getUserBsps),
      first(),
      map(bsps =>
        bsps.map(bsp => ({
          value: bsp,
          label: bsp.name
        }))
      ),
      tap(bspCollection => {
        if (bspCollection.length === this.oneElement) {
          const bspDropdown: DropdownOption = last(bspCollection);

          this.predefinedFilters.bsp = bspDropdown.value;
          this.form.controls.bsp.setValue(bspDropdown.value);
        }
      }),
      takeUntil(this.destroy$)
    );
  }
}
