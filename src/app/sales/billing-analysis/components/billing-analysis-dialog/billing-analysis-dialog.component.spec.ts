import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA, Provider } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createComponentFactory, createSpyObject, Spectator } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SalesDropdownOptionsService } from '~app/sales/shared/services';
import { ButtonDesign, DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { Bsp } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { BillingQueryActions } from '../../actions';
import { BillingAnalysisDialogComponent } from './billing-analysis-dialog.component';

describe('BillingAnalysisDialogComponent', () => {
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const permissionsService = createSpyObject(PermissionsService);
  let store: MockStore<AppState>;
  let spectator: Spectator<BillingAnalysisDialogComponent>;
  const appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '/api' });
  const mockConfig = {
    data: {
      title: 'title',
      footerButtonsType: FooterButton.Dispute,
      buttons: [
        {
          title: 'test',
          buttonDesign: ButtonDesign.Primary,
          type: FooterButton.Search,
          isDisabled: false
        } as ModalAction
      ]
    }
  } as DialogConfig;

  const initialState = {
    router: null,
    auth: {
      user: UserType.IATA
    }
  };

  const bspBulgaria = { id: 1, name: 'Bulgaria' } as Bsp;

  const defaultFormValues = {
    bsp: bspBulgaria,
    airline: 'Bulgaria Air',
    agent: 'Balkan tourist',
    agentGroup: 'Balkan Group',
    periodRange: [
      { value: '191230', label: '191230' },
      { value: '191201', label: '191201' }
    ],
    isMultipopulation: false
  };

  const createComponent = createComponentFactory({
    imports: [HttpClientTestingModule],
    component: BillingAnalysisDialogComponent,
    declarations: [TranslatePipeMock],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [
      FormBuilder,
      DialogConfig,
      ReactiveSubject,
      MessageService,
      provideMockStore({ initialState }),
      provideMockDropdownOptionsService(),
      { provide: PermissionsService, useValue: permissionsService },
      { provide: DialogConfig, useValue: mockConfig },
      { provide: L10nTranslationService, useValue: translationServiceSpy },
      { provide: AppConfigurationService, useValue: appConfigurationSpy }
    ]
  });

  beforeEach(waitForAsync(() => {
    spectator = createComponent();
    store = TestBed.inject<any>(Store);
    spectator.component.config.data.buttons = [FooterButton.Search];
  }));

  it('should create component', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should reset specific form controls when bsp is changed', () => {
    spectator.component.form.setValue(defaultFormValues);
    spectator.component.onBspChange();

    const bspGermany = { id: 2, name: 'Germany' } as Bsp;
    spectator.component.form.controls.bsp.setValue(bspGermany);
    const { airline, agent, bsp } = spectator.component.form.controls;

    expect(bsp.value).toEqual(bspGermany);
    expect(airline.value).toBeNull();
    expect(agent.value).toBeNull();
  });

  it('should clear validators for airline control when logged user is Airline', () => {
    permissionsService.hasUserType.and.callFake((userType: UserType.AIRLINE) => true);
    spyOn(spectator.component.form.controls.airline, 'clearValidators');

    spectator.component.setUserPermissions();
    spectator.component['clearValidatorsByUserType']();

    expect(spectator.component.form.controls.airline.clearValidators).toHaveBeenCalled();
  });

  it('should dispatch an action to update default and last query', () => {
    spyOn(store, 'dispatch');
    spectator.component.onButtonClicked();
    spectator.component.reactiveSubject.emit({ clickedBtn: 'search' });

    const newDefaultQuery = {
      sortBy: [],
      paginateBy: { page: 0, size: PAGINATION.DEFAULT_PAGE_SIZE, totalElements: 0 },
      filterBy: {}
    };

    expect(store.dispatch).toHaveBeenCalledWith(new BillingQueryActions.UpdateLastQuery(newDefaultQuery));
    expect(store.dispatch).toHaveBeenCalledWith(new BillingQueryActions.UpdateDefaultQuery(newDefaultQuery));
  });
});

function provideMockDropdownOptionsService(): Provider {
  const factory = () => {
    const service = createSpyObject(SalesDropdownOptionsService);
    service.getAirlines.and.returnValue(of([]));
    service.getAgents.and.returnValue(of([]));
    service.getPeriods.and.returnValue(of([]));

    return service;
  };

  return { provide: SalesDropdownOptionsService, useFactory: factory };
}
