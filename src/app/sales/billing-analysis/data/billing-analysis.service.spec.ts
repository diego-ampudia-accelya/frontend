import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { BillingAnalysisService } from './billing-analysis.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { DownloadContentType, DownloadExportAsType, ExportAsModel } from '~app/sales/shared/models';
import { DataQuery } from '~app/shared/components/list-view';
import { Bsp } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services';

describe('BillingAnalysisService', () => {
  let service: BillingAnalysisService;
  const bspGermany = { id: 1, name: 'Germany', isoCountryCode: 'DE' } as Bsp;
  const httpClientSpy = createSpyObject(HttpClient);
  const periodServiceSpy = createSpyObject(PeriodService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BillingAnalysisService,
        AppConfigurationService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: PeriodService, useValue: periodServiceSpy }
      ]
    });

    service = TestBed.inject<any>(BillingAnalysisService);

    httpClientSpy.get.and.returnValue(of());
    periodServiceSpy.getByBsp.and.returnValue(of());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send request with properly formatted filters when all filters are specified', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: {
        bsp: bspGermany,
        airline: [
          {
            code: 123
          }
        ],
        agent: {
          code: 456
        },
        periodRange: [
          {
            id: 65542020041,
            period: '2020041',
            dateFrom: '03/30/2020',
            dateTo: '04/05/2020'
          },
          {
            id: 65542020044,
            period: '2020044',
            dateFrom: '04/20/2020',
            dateTo: '04/26/2020'
          }
        ],
        transactionCode: ['EMDS', 'TKTT'],
        currency: [{ code: 'EUR' }, { code: 'USD' }]
      },
      sortBy: [],
      paginateBy: { size: 20, page: 0 }
    };

    service.find(query).pipe(take(1)).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/globalsearch/billing-analysis'));
    expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('isoCountryCode%3A%3Aeq%3A%3ADE'));
    expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('airlineCode%3A%3Ain%3A%3A123'));
    expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agentCode%3A%3Ain%3A%3A456'));
    expect(httpClientSpy.get).toHaveBeenCalledWith(
      jasmine.stringMatching('transactionCode%3A%3Ain%3A%3AEMDS%3A%3ATKTT')
    );
    expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('currencyCode%3A%3Ain%3A%3AEUR%3A%3AUSD'));
    expect(httpClientSpy.get).toHaveBeenCalledWith(
      jasmine.stringMatching('billingAnalysisEndDate%3A%3Agte%3A%3A2020-04-05')
    );
    expect(httpClientSpy.get).toHaveBeenCalledWith(
      jasmine.stringMatching('billingAnalysisEndDate%3A%3Alte%3A%3A2020-04-26')
    );
  }));

  it('should send request with properly formatted filters when no filters are specified', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: {},
      sortBy: [],
      paginateBy: { size: 20, page: 0 }
    };

    service.find(query).pipe(take(1)).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/globalsearch/billing-analysis?page=0&size=20');
  }));

  it('should send proper download request with agent code', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: {
        agent: { code: 'agent-code' },
        agentGroup: { id: 3 },
        periodRange: [{ dateTo: '01/20/2020' }, { dateTo: '01/21/2020' }],
        bsp: { isoCountryCode: 'ES' },
        transactionCode: ['code']
      },
      paginateBy: {
        size: 20,
        page: 0,
        totalElements: 1
      },
      sortBy: []
    };
    const exportAs: ExportAsModel = {
      exportAs: DownloadExportAsType.txt,
      content: [DownloadContentType.details, DownloadContentType.totals]
    };

    service.download(query, exportAs).pipe(take(1)).subscribe();
    tick();

    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('/globalsearch/billing-analysis/download?')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('page=0&size=20&'));
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('filters='));
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('billingAnalysisEndDate%3A%3Agte%3A%3A2020-01-20%2C')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('agentCode%3A%3Ain%3A%3Aagent-code%2C')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('isoCountryCode%3A%3Aeq%3A%3AES%2C')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('transactionCode%3A%3Ain%3A%3Acode&')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('content=DETAILS,TOTALS&'));
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('exportAs=TXT'));
  }));

  it('should send proper download request with airline', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: {
        airline: [{ code: '11' }, { code: '22' }],
        periodRange: [{ dateTo: '01/20/2020' }, { dateTo: '01/21/2020' }],
        bsp: { isoCountryCode: 'ES' },
        transactionCode: ['code']
      },
      paginateBy: {
        size: 20,
        page: 0,
        totalElements: 1
      },
      sortBy: []
    };
    const exportAs: ExportAsModel = {
      exportAs: DownloadExportAsType.txt,
      content: [DownloadContentType.details, DownloadContentType.totals]
    };

    service.download(query, exportAs).pipe(take(1)).subscribe();
    tick();

    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('/globalsearch/billing-analysis/download?')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('page=0&size=20&'));
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('filters='));
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('billingAnalysisEndDate%3A%3Agte%3A%3A2020-01-20%2C')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('isoCountryCode%3A%3Aeq%3A%3AES%2C')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('airlineCode%3A%3Ain%3A%3A11%3A%3A22%2C')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(
      jasmine.stringMatching('transactionCode%3A%3Ain%3A%3Acode&')
    );
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('content=DETAILS,TOTALS&'));
    expect(httpClientSpy.get.calls.mostRecent().args).toContain(jasmine.stringMatching('exportAs=TXT'));
  }));
});
