import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, withLatestFrom } from 'rxjs/operators';

import { getUser, getUserBsps } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { Bsp } from '~app/shared/models/bsp.model';
import { UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class BillingAnalysisResolver implements Resolve<Bsp> {
  private user$ = this.store.pipe(select(getUser), first());

  constructor(private store: Store<AppState>) {}

  public resolve(): Observable<Bsp> {
    return this.store.pipe(
      select(getUserBsps),
      withLatestFrom(this.user$),
      map(([bsp, user]) => {
        if (user.userType === UserType.AGENT) {
          return bsp[0];
        }
      }),
      first()
    );
  }
}
