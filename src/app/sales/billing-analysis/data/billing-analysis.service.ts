import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isEmpty } from 'lodash';
import moment from 'moment-mini';
import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';
import { SummaryTicket } from '~app/document/models/summary-ticket.model';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { ExportAsModel } from '~app/sales/shared/models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { EQ, GTE, IN, LTE } from '~app/shared/constants/operations';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { toPeriodEntityData } from '~app/shared/utils/entity-period-mapper.utils';
import { BillingAnalysisFilters, BillingAnalysisTotals, BillingAnalysisTotalsFiltersBE } from '../models';
import { BillingAnalysisDetailsRow } from '../models/billing-analysis-details-row.model';
import { BillingAnalysis } from '../models/billing-analysis.model';

@Injectable({
  providedIn: 'root'
})
export class BillingAnalysisService implements Queryable<BillingAnalysis> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/globalsearch/billing-analysis`;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private periodDictionary: PeriodService
  ) {}

  @Cacheable()
  public getAdditionalDetails(documentId: string): Observable<BillingAnalysisDetailsRow> {
    const url = `${this.appConfiguration.baseApiPath}/document-enquiry/documents/${documentId}/billing-analysis`;

    return this.http.get<BillingAnalysisDetailsRow>(url);
  }

  public getTotals(query: DataQuery): Observable<BillingAnalysisTotals[]> {
    const url = `${this.appConfiguration.baseApiPath}/sales-query/billing-analysis/totals`;
    const formattedQuery = this.formatFilterForTotals(query);

    return this.http.get<BillingAnalysisTotals[]>(url + formattedQuery.getQueryString({ omitPaging: true }));
  }

  public find(query: DataQuery<BillingAnalysisFilters>): Observable<PagedData<BillingAnalysis>> {
    const dataQuery = { ...query, filterBy: this.formatRequestFilters(query.filterBy) };
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const reqQuery = RequestQuery.fromDataQuery(dataQuery);
    reqQuery.reduceFilterBy = true;

    return this.http.get<PagedData<BillingAnalysis>>(this.baseUrl + reqQuery.getQueryString()).pipe(
      switchMap(data =>
        combineLatest([
          of(data),
          data.records.length && query.filterBy?.bsp ? this.periodDictionary.getByBsp(query.filterBy.bsp.id) : of(null)
        ])
      ),
      map(([data, periods]) => (periods ? toPeriodEntityData<BillingAnalysis>(data, periods) : data))
    );
  }

  public download(
    query: DataQuery<BillingAnalysisFilters>,
    exportOptions: ExportAsModel
  ): Observable<{ blob: Blob; fileName: string }> {
    const dataQuery = {
      ...query,
      filterBy: this.formatRequestFilters(query.filterBy)
    };
    const requestQuery = RequestQuery.fromDataQuery(dataQuery);
    requestQuery.reduceFilterBy = true;
    const exportOptionsString = `&content=${exportOptions.content.join(',')}&exportAs=${exportOptions.exportAs}`;

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString() + exportOptionsString;

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public getBillingAnalysisMultipopulationDetails(documentIds: string[]): Observable<SummaryTicket> {
    const url = `${this.appConfiguration.baseApiPath}/document-enquiry/documents/summary?documentIds=${documentIds}`;

    return this.http.get<SummaryTicket>(url);
  }

  private formatFilterForTotals(
    query: DataQuery<BillingAnalysisFilters>
  ): RequestQuery<BillingAnalysisTotalsFiltersBE> {
    const { airline, agent, agentGroup, bsp, periodRange, transactionCode, statisticalCode } = query.filterBy;

    const billingAnalysisEndDate = !isEmpty(periodRange) && periodRange.map(period => this.formatPeriod(period.dateTo));
    const airlineTotals = !Array.isArray(airline) ? airline?.id : airline?.map(airl => airl.id);

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        bspId: bsp ? bsp.id : undefined,
        airlineId: airlineTotals,
        agentId: agent && agent.id,
        agentGroupId: !agent && agentGroup ? agentGroup.id : null,
        billingAnalysisEndingDateFrom: billingAnalysisEndDate[0],
        billingAnalysisEndingDateTo: billingAnalysisEndDate[1],
        transactionCode,
        statisticalCode
      }
    });
  }

  private formatRequestFilters(filters: Partial<BillingAnalysisFilters>): RequestQueryFilter[] {
    const formattedRequestFilters: RequestQueryFilter[] = [];

    /**
     * If both, agent and agent group filters exist, only agent should be sent.
     */
    const modifiedFilters = this.checkIfBothFiltersExist(filters);

    modifiedFilters.forEach(([key, value]) => {
      let attribute = key;
      let operation = EQ;
      let attributeValue = value;

      if (key === 'periodRange') {
        const billingAnalysisEndDate = !isEmpty(value) && value.map(period => this.formatPeriod(period.dateTo));

        attribute = 'billingAnalysisEndDate';

        formattedRequestFilters.push({
          attribute,
          operation: GTE,
          attributeValue: billingAnalysisEndDate[0]
        });

        formattedRequestFilters.push({
          attribute,
          operation: LTE,
          attributeValue: billingAnalysisEndDate[1]
        });

        return;
      }

      switch (key) {
        case 'agent':
          attribute = 'agentCode';
          operation = IN;
          attributeValue = value.code;
          break;
        case 'bsp':
          attribute = 'isoCountryCode';
          operation = EQ;
          attributeValue = value.isoCountryCode;
          break;
        case 'airline':
          attribute = 'airlineCode';
          operation = IN;
          attributeValue = Array.isArray(value) ? value.map(({ code }) => code).join(',') : value.code;
          break;
        case 'transactionCode':
          operation = IN;
          attributeValue = value.join(',');
          break;
        case 'currency':
          attribute = 'currencyCode';
          operation = IN;
          attributeValue = value.map(({ code }) => code).join(',');
          break;
        case 'agentGroup':
          attribute = 'agentGroupId';
          operation = EQ;
          attributeValue = value.id;
          break;
        case 'reportingSystem':
          attribute = 'reportingSystem';
          operation = EQ;
          attributeValue = value.gdsCode;
          break;
      }

      formattedRequestFilters.push({
        attribute,
        operation,
        attributeValue
      });
    });

    return formattedRequestFilters;
  }

  private checkIfBothFiltersExist(filters: Partial<BillingAnalysisFilters>): [string, any][] {
    let filtersArray: [string, any][] = Object.entries<any>(filters);

    const filterContainsAgent = filtersArray.some(([key, _]) => key === 'agent');
    const filterContainsAgentGroup = filtersArray.some(([key, _]) => key === 'agentGroup');

    if (filterContainsAgent && filterContainsAgentGroup) {
      filtersArray = filtersArray.filter(([key, _]) => key !== 'agentGroup');
    }

    return filtersArray;
  }

  private formatPeriod(date: string): string {
    return date ? moment(date, 'MM/DD/YYYY').format('YYYY-MM-DD') : null;
  }
}
