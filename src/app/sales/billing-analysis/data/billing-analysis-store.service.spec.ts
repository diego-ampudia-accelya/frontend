import { TestBed, waitForAsync } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { DataQuery } from '~app/shared/components/list-view';
import { featureKey, State } from '../reducers';
import { BillingAnalysisStore } from './billing-analysis-store.service';

describe('BillingAnalysisStoreService', () => {
  let storeService: BillingAnalysisStore;
  const mockDefaultQuery: DataQuery = {
    filterBy: { test: 1 },
    paginateBy: null,
    sortBy: []
  };
  const mockLastQuery: DataQuery = {
    filterBy: { test: 3 },
    paginateBy: null,
    sortBy: []
  };

  const initialState: Partial<State> = {
    [featureKey]: {
      defaultQuery: mockDefaultQuery,
      lastQuery: mockLastQuery,
      isMultipopulation: false,
      selectedDocuments: [],
      multipopulationDetails: null,
      issueType: null,
      error: null
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })]
    });

    storeService = new BillingAnalysisStore(TestBed.inject<any>(Store));
  });

  it('should return default query', waitForAsync(() => {
    storeService.getDefaultQuery$().subscribe(res => {
      expect(res).toEqual(mockDefaultQuery);
    });
  }));

  it('should return last query', waitForAsync(() => {
    storeService.getLastQuery$().subscribe(res => {
      expect(res).toEqual(mockLastQuery);
    });
  }));
});
