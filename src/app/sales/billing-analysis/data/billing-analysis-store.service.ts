import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { first, map, switchMap, withLatestFrom } from 'rxjs/operators';

import { BillingQueryActions } from '../actions';
import * as fromBillingAnalysis from '../reducers';
import { getAgentForUser, getLocalAirline, getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { RequestQueryBody } from '~app/shared/models/request-query-body.model';
import { User, UserType } from '~app/shared/models/user.model';

@Injectable()
export class BillingAnalysisStore {
  constructor(protected store: Store<AppState>) {}

  public save(query: RequestQueryBody): void {
    this.store.dispatch(new BillingQueryActions.UpdateLastQuery(query));
  }

  public getLastQuery$() {
    return this.store.pipe(select(fromBillingAnalysis.lastQuerySelector));
  }

  public getDefaultQuery$() {
    return this.store.pipe(select(fromBillingAnalysis.defaultQuerySelector));
  }

  public updateFilterForUser$(): Observable<DataQuery> {
    return this.getLoggedUser$().pipe(
      withLatestFrom(this.getLastQuery$()),
      switchMap(([user, lastQuery]) => {
        if (user.userType === UserType.AIRLINE) {
          const selectedBsp = lastQuery.filterBy.bsp;

          return this.store.pipe(
            select(getLocalAirline(selectedBsp)),
            map(localAirline => ({
              ...lastQuery,
              filterBy: {
                ...lastQuery.filterBy,
                airline: localAirline
              }
            }))
          );
        }

        if (user.userType === UserType.AGENT) {
          return this.store.pipe(
            select(getAgentForUser),
            map(agent => ({
              ...lastQuery,
              filterBy: {
                ...lastQuery.filterBy,
                agent
              }
            }))
          );
        }

        return of(lastQuery);
      })
    );
  }

  private getLoggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }
}
