import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { BillingAnalysisDialogComponent } from '../components/billing-analysis-dialog/billing-analysis-dialog.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ROUTES } from '~app/shared/constants/routes';
import { UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class BillingAnalysisSetupGuard implements CanActivate {
  private user$ = this.store.pipe(select(getUser));

  canActivate(router: ActivatedRouteSnapshot): Observable<boolean> {
    return this.user$.pipe(
      first(),
      tap(user => {
        if (user.userType === UserType.AGENT) {
          this.router.navigate([ROUTES.BILLING_ANALYSIS.url]);
        } else {
          this.dialogService
            .open(BillingAnalysisDialogComponent, {
              data: {
                footerButtonsType: FooterButton.Search,
                mainButtonType: FooterButton.Search
              }
            })
            .subscribe(action => {
              if (action.clickedBtn === FooterButton.Search) {
                this.router.navigate([ROUTES.BILLING_ANALYSIS.url]);
              }
            });
        }
      }),
      mapTo(false)
    );
  }

  constructor(private dialogService: DialogService, private router: Router, private store: Store<AppState>) {}
}
