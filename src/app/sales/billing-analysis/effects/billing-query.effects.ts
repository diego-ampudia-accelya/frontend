import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants';
import { NotificationService } from '~app/shared/services';
import { BillingQueryActions } from '../actions';
import { BillingAnalysisService } from '../data/billing-analysis.service';
import * as fromBillingAnalysis from '../reducers';

@Injectable()
export class BillingQueryEffects {
  $loadData = createEffect(() =>
    this.actions$.pipe(
      ofType(BillingQueryActions.Types.LoadDocumentsData),
      withLatestFrom(this.store.pipe(select(fromBillingAnalysis.lastQuerySelector))),
      map(([_, query]) => ({
        ...query,
        filterBy: {
          ...query.filterBy
        }
      })),
      switchMap(requestQuery => this.billingAnalysisService.find(requestQuery)),
      map(data => new BillingQueryActions.LoadDocumentsDataSuccess(data)),
      catchError(error => merge(of(new BillingQueryActions.LoadDocumentsDataError(error)), throwError(error)))
    )
  );

  $getDocumentsDetails = createEffect(() =>
    this.actions$.pipe(
      ofType(BillingQueryActions.Types.GetDocumentsDetails),
      withLatestFrom(
        this.store.pipe(select(fromBillingAnalysis.getSelectedDocumentsIds)),
        this.store.pipe(select(fromBillingAnalysis.getIssueType))
      ),
      switchMap(([_, ids, issueType]) =>
        this.billingAnalysisService.getBillingAnalysisMultipopulationDetails(ids).pipe(
          map(data => {
            if (issueType === 'adma') {
              this.router.navigate([ROUTES.ADM_ISSUE.url], {
                queryParams: { documentIds: ids }
              });
            } else {
              this.router.navigate([ROUTES.ACM_ISSUE.url], {
                queryParams: { documentIds: ids }
              });
            }

            return new BillingQueryActions.GetDocumentsDetailsSuccess(data);
          }),
          catchError((error: HttpErrorResponse) => {
            const errorCode = error?.error?.messages[0]?.messageCode;
            const errorMessage = this.getMultipopulationDocumentsDetailsError(errorCode);
            this.notificationService.showError(errorMessage);

            return of(new BillingQueryActions.GetDocumentsDetailsError(error));
          })
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private billingAnalysisService: BillingAnalysisService,
    private store: Store<AppState>,
    private router: Router,
    private readonly notificationService: NotificationService,
    private readonly translationService: L10nTranslationService
  ) {}

  private getMultipopulationDocumentsDetailsError(errorCode: string): string {
    let error = '';

    switch (errorCode) {
      case '290001':
        error = 'sales.billingAnalysis.multipopulationDocumentsDetails.errors.sameBsp';
        break;
      case '290002':
        error = 'sales.billingAnalysis.multipopulationDocumentsDetails.errors.sameType';
        break;
      case '290004':
        error = 'sales.billingAnalysis.multipopulationDocumentsDetails.errors.sameCurrency';
        break;
      case '290003':
        error = 'sales.billingAnalysis.multipopulationDocumentsDetails.errors.sameStat';
        break;
      case '290005':
        error = 'sales.billingAnalysis.multipopulationDocumentsDetails.errors.documentsQuantity';
        break;
      case '290006':
        error = 'sales.billingAnalysis.multipopulationDocumentsDetails.errors.documentType';
        break;
      default:
        break;
    }

    const errorMessage = this.translationService.translate(error);

    return errorMessage;
  }
}
