import { DataQuery } from '~app/shared/components/list-view';
import { toSubUserUpdateModel } from '~app/shared/helpers/user.helper';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { PagedData } from '~app/shared/models/paged-data.model';
import { SubUsersListFilter } from '~app/sub-users/containers/sub-users-list/sub-users-list-filter.model';
import * as fromSubUsersStatus from '~app/sub-users/reducers/sub-user-change-status-dialog.reducer';
import * as fromCreateSubUser from '~app/sub-users/reducers/sub-user-create-dialog.reducer';
import * as fromSubUserProfilePermissions from '~app/sub-users/reducers/sub-user-profile-permissions.reducer';
import * as fromSubUserProfile from '~app/sub-users/reducers/sub-user-profile.reducer';
import * as fromSubUsersList from '~app/sub-users/reducers/sub-users-list.reducer';
import * as fromSubUsersPermissions from '~app/sub-users/reducers/sub-users-permissions.reducer';
import * as fromSubUsersTemplates from '~app/sub-users/reducers/sub-users-templates.reducer';
import { UserPermission, UserPermissionLomuResponse } from '~app/sub-users/shared/permissions.models';
import {
  SubUser,
  SubUserCreateModel,
  SubUserEditable,
  SubUserLomu,
  SubUserStatus,
  SubUserTemplate,
  SubUserType,
  SubUserUpdateModel
} from '~app/sub-users/shared/sub-users.models';

export function getInitialSubUsersState() {
  return {
    auth: {
      user: createAirlineUser()
    },
    subUsers: {
      [fromSubUsersList.subUsersListFeatureKey]: fromSubUsersList.initialState,
      [fromSubUserProfile.subUserProfileFeatureKey]: fromSubUserProfile.initialState,
      [fromSubUserProfilePermissions.featureKey]: fromSubUserProfilePermissions.initialState,
      [fromCreateSubUser.createSubUserFeatureKey]: fromCreateSubUser.initialState,
      [fromSubUsersStatus.subUserStatusFeatureKey]: fromSubUsersStatus.initialState,
      [fromSubUsersTemplates.subUsersTemplatesFeatureKey]: fromSubUsersTemplates.initialState,
      [fromSubUsersPermissions.subUsersPermissionsFeatureKey]: fromSubUsersPermissions.initialState
    }
  };
}

export function getTestSubUser(): SubUser {
  return {
    id: 6005,
    portalEmail: 'frank_right@continental.airlines.com',
    level: 1,
    name: 'Frank Right',
    parent: { id: 69835949, name: 'Leslie Sims' },
    template: SubUserTemplate.Streamlined,
    registerDate: '2019-10-30',
    expiryDate: '2020-12-30',
    status: SubUserStatus.Active,
    userType: SubUserType.Airline,
    userTypeDetails: { iataCode: '005', name: 'CONTINENTAL AIRLINES' },
    contactInfo: {
      organization: 'CONTINENTAL AIRLINES',
      telephone: '284 751 8973',
      country: 'Spain',
      city: 'Madrid',
      address: '190 Lang Ridges',
      postCode: 'BY 51380'
    },
    permissions: [],
    editable: SubUserEditable.Enabled,
    isSelected: false
  };
}

export function getTestLomuSubUser(): SubUserLomu {
  return {
    id: 6005,
    bsp: { id: 6983, name: 'SPAIN', isoCountryCode: 'ES' },
    portalEmail: 'frank_right@continental.airlines.com',
    name: 'Frank Right',
    template: SubUserTemplate.Streamlined,
    registerDate: '2019-10-30',
    expiryDate: '2020-12-30',
    status: SubUserStatus.Active,
    userType: SubUserType.Airline,
    userTypeDetails: { iataCode: '005', name: 'CONTINENTAL AIRLINES' },
    contactInfo: {
      organization: 'CONTINENTAL AIRLINES',
      country: 'Spain',
      city: 'Madrid',
      address: '190 Lang Ridges',
      postCode: 'BY 51380'
    }
  };
}

export function getTestHosuSubUser(): SubUser {
  return {
    id: 6005,
    portalEmail: 'frank_right@continental.airlines.com',
    name: 'Frank Right',
    registerDate: '2019-10-30',
    expiryDate: '2020-12-30',
    status: SubUserStatus.Active,
    userType: SubUserType.Airline,
    userTypeDetails: { iataCode: '005', name: 'CONTINENTAL AIRLINES' },
    contactInfo: {
      organization: 'CONTINENTAL AIRLINES',
      telephone: '284 751 8973',
      country: 'Spain',
      city: 'Madrid',
      address: '190 Lang Ridges',
      postCode: 'BY 51380',
      email: 'frank_right@continental.airlines.com'
    },
    permissions: [],
    editable: SubUserEditable.Enabled,
    allCountries: true,
    countries: [],
    ilogin: 'Frank Right'
  };
}

export function getTestSubUserUpdate(): SubUserUpdateModel {
  const subUser = toSubUserUpdateModel(getTestSubUser());
  delete subUser.allCountries;
  delete subUser.countries;

  return subUser;
}

export function getTestSubUserWithoutTemplate(): SubUser {
  const subUser = getTestSubUser();
  delete subUser.template;

  return subUser;
}

export function getTestSubUserUpdateWithoutTemplate(): SubUserUpdateModel {
  const subUser = getTestSubUserUpdate();
  delete subUser.template;

  return subUser;
}

export function getTestUserPermissions(): UserPermission[] {
  return [
    {
      id: 10001,
      code: 'AGENTS_QUERY',
      name: 'Agents Query',
      category: { id: 102, code: 'MASTER_DATA', name: 'Master Data' },
      enabled: true,
      template: 'EFFICIENT'
    },
    {
      id: 10010,
      code: 'DOCUMENT_ENQUIRY',
      name: 'Document Enquiry',
      category: { id: 106, code: 'DOCUMENTS', name: 'Documents' },
      enabled: true,
      template: 'STREAMLINED'
    }
  ];
}

export function getTestUserLomuPermissions(): UserPermissionLomuResponse[] {
  return [
    {
      idUser: 1234,
      permissions: [
        {
          id: 10001,
          code: 'AGENTS_QUERY',
          name: 'Agents Query',
          catgory: { id: 102, code: 'MASTER_DATA', name: 'Master Data' },
          enabled: true,
          template: 'EFFICIENT'
        },
        {
          id: 10010,
          code: 'DOCUMENT_ENQUIRY',
          name: 'Document Enquiry',
          catgory: { id: 106, code: 'DOCUMENTS', name: 'Documents' },
          enabled: true,
          template: 'STREAMLINED'
        }
      ]
    }
  ];
}

export function getTestUserPermissionsData() {
  return {
    originalPermissions: [
      {
        name: 'Master Data',
        disabled: false,
        items: [
          {
            name: 'Agents Query',
            checked: true,
            value: {
              id: 10001,
              code: 'AGENTS_QUERY',
              name: 'Agents Query',
              category: { id: 102, code: 'MASTER_DATA', name: 'Master Data' },
              enabled: true,
              template: 'EFFICIENT'
            },
            hasChanged: false,
            disabled: false,
            tooltip: ''
          }
        ]
      },
      {
        name: 'Documents',
        disabled: false,
        items: [
          {
            name: 'Document Enquiry',
            checked: true,
            value: {
              id: 10010,
              code: 'DOCUMENT_ENQUIRY',
              name: 'Document Enquiry',
              category: { id: 106, code: 'DOCUMENTS', name: 'Documents' },
              enabled: true,
              template: 'STREAMLINED'
            },
            hasChanged: false,
            disabled: false,
            tooltip: ''
          }
        ]
      }
    ],
    modifiedPermissions: [],
    hasUnsavedChanges: false
  };
}

export function getTestSelectedPermissionItems() {
  return [
    {
      name: 'Master Data',
      disabled: false,
      items: [
        {
          name: 'Agents Query',
          checked: false,
          value: {
            id: 10001,
            code: 'AGENTS_QUERY',
            name: 'Agents Query',
            category: { id: 102, code: 'MASTER_DATA', name: 'Master Data' },
            enabled: true,
            template: 'EFFICIENT'
          },
          hasChanged: true,
          disabled: false,
          tooltip: ''
        }
      ],
      checked: false,
      checkboxDesign: 'normal',
      hasChanged: true
    },
    {
      name: 'Documents',
      disabled: false,
      items: [
        {
          name: 'Document Enquiry',
          checked: true,
          value: {
            id: 10010,
            code: 'DOCUMENT_ENQUIRY',
            name: 'Document Enquiry',
            category: { id: 106, code: 'DOCUMENTS', name: 'Documents' },
            enabled: true,
            template: 'STREAMLINED'
          },
          hasChanged: false,
          disabled: false,
          tooltip: ''
        }
      ],
      checked: true,
      checkboxDesign: 'normal'
    }
  ];
}

export function getFindTestQuery(sortBy: any[] = [], filterBy: any = {}): DataQuery<SubUsersListFilter> {
  return {
    paginateBy: { page: 0, size: 20, totalElements: 0 },
    sortBy: [...sortBy],
    filterBy: { ...filterBy }
  };
}

export function getSubUserResponse(subUserProperties: any = {}): SubUser {
  return {
    id: 698311111143,
    bsp: { id: 6983, name: 'SPAIN', isoCountryCode: 'ES' },
    portalEmail: 'john.smith@accelya.com',
    level: 1,
    name: '002SULE',
    parent: { id: '69836005', name: 'Carolina Copp' },
    template: 'EFFICIENT',
    registerDate: '2020-02-24',
    expiryDate: '2020-07-31',
    status: 'ACTIVE',
    userType: 'AIRLINE',
    userTypeDetails: { iataCode: '002', name: 'CARIBBEAN AIRWAYS' },
    contactInfo: {
      organization: 'IATA',
      telephone: '4949494',
      country: 'ES',
      city: 'NO:101/1 ELMADAG',
      address: 'Castellana',
      postCode: '494949',
      email: 'john.mith@accelya.com'
    },
    countries: [],
    allCountries: true,
    ...subUserProperties
  } as SubUser;
}

export function getFindTestResponse(subUserProperties: any = {}): PagedData<SubUser> {
  const subUser: any = getSubUserResponse(subUserProperties);

  return {
    pageNumber: 0,
    pageSize: 20,
    total: 1,
    totalPages: 1,
    records: [subUser]
  };
}

export function getSubUserCreateData(): SubUserCreateModel {
  return {
    template: SubUserTemplate.Efficient,
    portalEmail: 'test@test.com',
    name: 'test name',
    organization: 'test organization',
    telephone: 'test telephone',
    country: 'test country',
    city: 'test city',
    address: 'test address',
    postCode: 'test postCode'
  };
}
