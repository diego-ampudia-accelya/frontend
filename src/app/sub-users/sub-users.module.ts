import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SubUsersUnsavedChangesGuard } from './configuration/sub-users-unsaved-changes.guard';
import { CreateSubUserComponent } from './containers/create-sub-user/create-sub-user.component';
import { SaveSubUserTemplatesDialogComponent } from './containers/save-sub-user-templates-dialog/save-sub-user-templates-dialog.component';
import { SubUserDeactivateDialogComponent } from './containers/sub-user-deactivate-dialog/sub-user-deactivate-dialog.component';
import { SubUserHomuProfileComponent } from './containers/sub-user-homu-profile/sub-user-homu-profile.component';
import { SubUserHosuPermissionsDialogComponent } from './containers/sub-user-hosu-permissions-dialog/sub-user-hosu-permissions-dialog.component';
import { SubUserLomuPermissionsComponent } from './containers/sub-user-lomu-profile-permissions/sub-user-lomu-profile-permissions.component';
import { SubUserLomuProfileComponent } from './containers/sub-user-lomu-profile/sub-user-lomu-profile.component';
import { SubUserLomuPropertiesComponent } from './containers/sub-user-lomu-properties/sub-user-lomu-properties.component';
import { SubUserProfileComponent } from './containers/sub-user-profile/sub-user-profile.component';
import { SubUserPropertiesComponent } from './containers/sub-user-properties/sub-user-properties.component';
import { SubUserReactivateDialogComponent } from './containers/sub-user-reactivate-dialog/sub-user-reactivate-dialog.component';
import { SubUserTemplatesComponent } from './containers/sub-user-templates/sub-user-templates.component';
import { SubUsersHosuPermissionsComponent } from './containers/sub-users-hosu-permissions/sub-users-hosu-permissions.component';
import { SubUsersHosuComponent } from './containers/sub-users-hosu/sub-users-hosu.component';
import { SubUsersListComponent } from './containers/sub-users-list/sub-users-list.component';
import { SubUserHosuPermissionsEffects } from './effects/sub-user-hosu-permissions.effects';
import { SubUserLomuProfilePermissionsEffects } from './effects/sub-user-lomu-profile-permissions.effects';
import { SubUserLomuProfileEffects } from './effects/sub-user-lomu-profile.effects';
import { SubUserProfileEffects } from './effects/sub-user-profile.effects';
import { SubUserTemplatesEffects } from './effects/sub-user-templates.effects';
import { SubUserCreateResolver } from './shared/sub-user-create.resolver';
import { SubUserListResolver } from './shared/sub-user-list.resolver';
import { SubUsersRoutingModule } from '~app/sub-users/sub-users-routing.module';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUsersPermissionsEffects } from '~app/sub-users/effects/sub-users-permissions.effects';
import { SubUsersListEffects } from '~app/sub-users/effects/sub-users-list.effects';
import { SubUserStatusEffects } from '~app/sub-users/effects/sub-user-status.effects';
import { SubUserProfilePermissionsEffects } from '~app/sub-users/effects/sub-user-profile-permissions.effects';
import { SubUserCreateDialogEffects } from '~app/sub-users/effects/sub-user-create-dialog.effects';
import { SubUsersPermissionsComponent } from '~app/sub-users/containers/sub-users-permissions/sub-users-permissions.component';
import { SubUserPermissionsComponent } from '~app/sub-users/containers/sub-user-profile-permissions/sub-user-profile-permissions.component';
import { SaveSubUsersPermissionsDialogComponent } from '~app/sub-users/containers/save-sub-users-permissions-dialog/save-sub-users-permissions-dialog.component';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [
    SubUsersListComponent,
    SubUserTemplatesComponent,
    SubUserPermissionsComponent,
    SubUserLomuPermissionsComponent,
    SubUsersPermissionsComponent,
    CreateSubUserComponent,
    SaveSubUsersPermissionsDialogComponent,
    SaveSubUserTemplatesDialogComponent,
    SubUserDeactivateDialogComponent,
    SubUserReactivateDialogComponent,
    SubUserPropertiesComponent,
    SubUserProfileComponent,
    SubUsersHosuComponent,
    SubUserLomuProfileComponent,
    SubUserLomuPropertiesComponent,
    SubUserHosuPermissionsDialogComponent,
    SubUsersHosuPermissionsComponent,
    SubUserHomuProfileComponent
  ],
  imports: [
    CommonModule,
    SubUsersRoutingModule,
    SharedModule,
    StoreModule.forFeature(fromSubUsers.subUsersFeatureKey, fromSubUsers.reducers),
    EffectsModule.forFeature([
      SubUsersListEffects,
      SubUserProfileEffects,
      SubUserProfilePermissionsEffects,
      SubUserCreateDialogEffects,
      SubUserStatusEffects,
      SubUserTemplatesEffects,
      SubUsersPermissionsEffects,
      SubUserLomuProfileEffects,
      SubUserLomuProfilePermissionsEffects,
      SubUserHosuPermissionsEffects
    ])
  ],
  providers: [SubUsersUnsavedChangesGuard, SubUserListResolver, SubUserCreateResolver]
})
export class SubUsersModule {}
