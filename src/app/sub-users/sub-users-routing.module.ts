import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { SubUserHomuProfileComponent } from './containers/sub-user-homu-profile/sub-user-homu-profile.component';
import { SubUserLomuPermissionsComponent } from './containers/sub-user-lomu-profile-permissions/sub-user-lomu-profile-permissions.component';
import { SubUserLomuProfileComponent } from './containers/sub-user-lomu-profile/sub-user-lomu-profile.component';
import { SubUserLomuPropertiesComponent } from './containers/sub-user-lomu-properties/sub-user-lomu-properties.component';
import { SubUserPermissionsComponent } from './containers/sub-user-profile-permissions/sub-user-profile-permissions.component';
import { SubUsersHosuPermissionsComponent } from './containers/sub-users-hosu-permissions/sub-users-hosu-permissions.component';
import { SubUsersHosuComponent } from './containers/sub-users-hosu/sub-users-hosu.component';
import { SubUserCreateResolver } from './shared/sub-user-create.resolver';
import { SubUserListResolver } from './shared/sub-user-list.resolver';
import { CanComponentDeactivateGuard } from '~app/core/services';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { SubUsersUnsavedChangesGuard } from '~app/sub-users/configuration/sub-users-unsaved-changes.guard';
import { SubUserProfileComponent } from '~app/sub-users/containers/sub-user-profile/sub-user-profile.component';
import { SubUserPropertiesComponent } from '~app/sub-users/containers/sub-user-properties/sub-user-properties.component';
import { SubUserTemplatesComponent } from '~app/sub-users/containers/sub-user-templates/sub-user-templates.component';
import { SubUsersListComponent } from '~app/sub-users/containers/sub-users-list/sub-users-list.component';
import { SubUsersPermissionsComponent } from '~app/sub-users/containers/sub-users-permissions/sub-users-permissions.component';
import { SubUserProfileResolver } from '~app/sub-users/shared/sub-user-profile.resolver';

const subUserRoutes: Route[] = [
  {
    path: 'properties',
    component: SubUserPropertiesComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      title: 'subUsers.profile.tabNames.properties',
      requiredPermissions: Permissions.updateSubuser
    }
  },
  {
    path: 'permissions',
    component: SubUserPermissionsComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      title: 'subUsers.profile.tabNames.permissions',
      requiredPermissions: Permissions.readSubuserPermissions
    }
  },
  { path: ROUTES.SUB_USER_DETAILS.path, redirectTo: 'properties', pathMatch: 'full' }
];

const subUserLomuChildrenRoutes: Route[] = [
  {
    path: 'properties',
    component: SubUserLomuPropertiesComponent,
    data: {
      title: 'subUsers.profile.tabNames.properties',
      requiredPermissions: Permissions.readLomuProfile
    }
  },
  {
    path: 'permissions',
    component: SubUserLomuPermissionsComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      title: 'subUsers.profile.tabNames.permissions',
      requiredPermissions: Permissions.readLomuPerm
    }
  },
  { path: ROUTES.SUB_USER_DETAILS.path, redirectTo: 'properties', pathMatch: 'full' }
];

const subUserHomuChildrenRoutes: Route[] = [
  {
    path: 'permissions',
    component: SubUsersHosuPermissionsComponent,
    data: {
      title: 'subUsers.profile.tabNames.permissions',
      requiredPermissions: Permissions.readSubuserHosuPermissions
    }
  }
];

const subUserHosuPropertiesRoute: Route[] = [
  {
    path: 'properties',
    component: SubUserPropertiesComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      title: 'subUsers.profile.tabNames.properties',
      requiredPermissions: [Permissions.updateHosuSubuser, Permissions.createHosuSubuser]
    }
  },
  { path: ROUTES.SUB_USER_DETAILS.path, redirectTo: 'properties', pathMatch: 'full' }
];

const subUserHosuPermissionsRoute: Route = {
  path: 'permissions',
  component: SubUserPermissionsComponent,
  canDeactivate: [CanComponentDeactivateGuard],
  data: {
    title: 'subUsers.profile.tabNames.permissions',
    requiredPermissions: Permissions.readSubuserHosuPermissions
  }
};

const subUserHosuChildrenRoutes: Route[] = [...subUserHosuPropertiesRoute, subUserHosuPermissionsRoute];

const subUserHosuRoutes: Route[] = [
  {
    path: '',
    component: SubUsersListComponent,
    data: {
      title: 'MENU.HOSU_SUBUSERS.TABS.LIST'
    }
  },
  { path: ROUTES.HOSU_SUB_USER.path, redirectTo: '', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: ROUTES.SUB_USERS.path,
    component: SubUsersListComponent,
    resolve: {
      subUserType: SubUserListResolver
    },
    data: {
      tab: ROUTES.SUB_USERS
    }
  },
  {
    path: ROUTES.HOSU_SUB_USER.path,
    component: SubUsersHosuComponent,
    resolve: {
      subUserType: SubUserListResolver
    },
    data: {
      tab: ROUTES.HOSU_SUB_USER
    },
    children: subUserHosuRoutes
  },
  {
    path: ROUTES.LOMU_SUB_USER.path,
    component: SubUserLomuProfileComponent,
    data: {
      tab: ROUTES.LOMU_SUB_USER
    },
    children: subUserLomuChildrenRoutes
  },
  {
    path: ROUTES.SUB_USER_HOSU_CREATE.path,
    component: SubUserProfileComponent,
    resolve: {
      subUserType: SubUserCreateResolver
    },
    data: {
      tab: ROUTES.SUB_USER_HOSU_CREATE
    },
    children: subUserHosuPropertiesRoute
  },
  {
    path: ROUTES.SUB_USER_HOSU_DETAILS.path,
    component: SubUserProfileComponent,
    resolve: { subUser: SubUserProfileResolver },
    data: {
      tab: ROUTES.SUB_USER_HOSU_DETAILS
    },
    children: subUserHosuChildrenRoutes
  },
  {
    path: ROUTES.SUB_USER_TEMPLATES.path,
    canDeactivate: [SubUsersUnsavedChangesGuard],
    component: SubUserTemplatesComponent,
    data: {
      tab: ROUTES.SUB_USER_TEMPLATES
    }
  },
  {
    path: ROUTES.SUB_USER_PERMISSIONS.path,
    canDeactivate: [SubUsersUnsavedChangesGuard],
    component: SubUsersPermissionsComponent,
    data: {
      tab: ROUTES.SUB_USER_PERMISSIONS
    }
  },
  {
    path: ROUTES.SUB_USER_DETAILS.path,
    component: SubUserProfileComponent,
    resolve: { subUser: SubUserProfileResolver },
    data: {
      tab: ROUTES.SUB_USER_DETAILS
    },
    children: subUserRoutes
  },
  {
    path: ROUTES.HOMU_SUB_USER.path,
    component: SubUserHomuProfileComponent,
    data: {
      tab: ROUTES.HOMU_SUB_USER
    },
    children: subUserHomuChildrenRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubUsersRoutingModule {}
