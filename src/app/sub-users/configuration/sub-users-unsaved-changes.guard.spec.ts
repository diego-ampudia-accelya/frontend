import { SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { SubUsersUnsavedChangesGuard } from './sub-users-unsaved-changes.guard';
import { CanComponentDeactivate } from '~app/core/services';

describe('SubUsersUnsavedChangesGuard', () => {
  let guard: SubUsersUnsavedChangesGuard;
  let componentStub: SpyObject<CanComponentDeactivate>;

  beforeEach(() => {
    componentStub = jasmine.createSpyObj('Component', ['canDeactivate']);
    componentStub.canDeactivate.and.returnValue(of(true));

    guard = new SubUsersUnsavedChangesGuard();
  });

  it('should create', () => {
    expect(guard).toBeDefined();
  });

  it('should call component canDeactivate', () => {
    guard.canDeactivate(componentStub);

    expect(componentStub.canDeactivate).toHaveBeenCalled();
  });
});
