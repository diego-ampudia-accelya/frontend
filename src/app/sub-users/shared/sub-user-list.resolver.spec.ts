import { fakeAsync, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { SubUsersListActions } from '../actions';

import { SubUserListResolver } from './sub-user-list.resolver';
import { SubUserTypeByParent } from './sub-users.models';

describe('SubUserListResolver', () => {
  let resolver: SubUserListResolver;

  const initialState = {
    auth: {
      user: {
        permissions: ['rLean']
      }
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubUserListResolver, provideMockStore({ initialState })]
    });

    resolver = TestBed.inject(SubUserListResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeDefined();
  });

  it('should return HOSU parent if user has LEAN permission', fakeAsync(() => {
    let result: SubUserTypeByParent;

    resolver.resolve().subscribe(parent => (result = parent));
    expect(result).toBe(SubUserTypeByParent.hosu);
  }));

  it('should return LOSU parent if user has NOT LEAN permission', fakeAsync(() => {
    let result: SubUserTypeByParent;

    const mockStore = TestBed.inject(MockStore);

    mockStore.setState({
      auth: {
        user: {
          permissions: []
        }
      }
    });
    mockStore.refreshState();

    resolver.resolve().subscribe(parent => (result = parent));
    expect(result).toBe(SubUserTypeByParent.losu);
  }));

  it('should dispatch HOSU parent type as a side effect if user has LEAN permission', fakeAsync(() => {
    const mockStore = TestBed.inject(MockStore);

    spyOn(mockStore, 'dispatch').and.callThrough();

    resolver.resolve().subscribe();
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SubUsersListActions.changeUserTypeByParent({ subUserTypeByParent: SubUserTypeByParent.hosu })
    );
  }));
});
