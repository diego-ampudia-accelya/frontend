import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { SubUserProfileResolver } from './sub-user-profile.resolver';
import { ServerError } from '~app/shared/errors';
import { SubUsersProfileActions } from '~app/sub-users/actions';
import { getInitialSubUsersState } from '~app/sub-users/mocks/sub-users.test.mocks';
import * as fromSubUsers from '~app/sub-users/reducers';

const serverError = new ServerError({
  timestamp: null,
  errorCode: 500,
  errorMajorCode: '001',
  errorMessage: 'Something went wrong.',
  messages: []
});

describe('SubUserProfileResolver', () => {
  let resolver: SubUserProfileResolver;
  let storeMock: MockStore;
  let routerMock: SpyObject<Router>;
  let activatedRouteSnapshotMock: SpyObject<ActivatedRouteSnapshot>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState: getInitialSubUsersState() }),
        mockProvider(Router),
        mockProvider(ActivatedRouteSnapshot, { paramMap: { get: () => 6005 } })
      ]
    });
    resolver = TestBed.inject(SubUserProfileResolver);
    storeMock = TestBed.inject<any>(Store);
    routerMock = TestBed.inject<any>(Router);
    activatedRouteSnapshotMock = TestBed.inject<any>(ActivatedRouteSnapshot);
    spyOn(storeMock, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  describe('resolve', () => {
    it('should dispatch requestSubUser', fakeAsync(() => {
      resolver.resolve(activatedRouteSnapshotMock).subscribe();
      tick();
      expect(storeMock.dispatch).toHaveBeenCalledWith(SubUsersProfileActions.requestSubUser({ id: 6005 }));
    }));

    it('should not dispatch requestSubUser if no id', fakeAsync(() => {
      activatedRouteSnapshotMock.paramMap.get = () => null;
      resolver.resolve(activatedRouteSnapshotMock).subscribe();
      tick();
      expect(storeMock.dispatch).not.toHaveBeenCalledWith();
    }));

    it('should navigate to last successful route on error', fakeAsync(() => {
      storeMock.overrideSelector(fromSubUsers.getProfileSubUserRequestError, serverError);
      routerMock.getCurrentNavigation.and.returnValue({
        previousNavigation: {
          finalUrl: '/dashboard'
        }
      });

      resolver.resolve(activatedRouteSnapshotMock).subscribe();
      tick();

      expect(routerMock.navigateByUrl).toHaveBeenCalledWith('/dashboard');
    }));

    it('should navigate to ./ if there is no last successful route on error', fakeAsync(() => {
      storeMock.overrideSelector(fromSubUsers.getProfileSubUserRequestError, serverError);

      routerMock.getCurrentNavigation.and.returnValue({
        previousNavigation: null
      });

      resolver.resolve(activatedRouteSnapshotMock).subscribe();
      tick();

      expect(routerMock.navigate).toHaveBeenCalledWith(['./']);
    }));
  });
});
