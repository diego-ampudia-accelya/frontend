import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { Observable } from 'rxjs';
import { map, skipWhile } from 'rxjs/operators';

import { HosuPermissionUpdate, UserPermission, UserPermissionDto, UserPermissionUpdate } from './permissions.models';
import { SUB_USER_ENDPOINTS } from './sub-user-endpoints.config';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { GLOBALS } from '~app/shared/constants/globals';
import { dateMapper, downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import { SubUsersListFilter } from '~app/sub-users/containers/sub-users-list/sub-users-list-filter.model';
import * as fromSubUsers from '~app/sub-users/reducers';
import {
  SubUser,
  SubUserCreateModel,
  SubUserEditable,
  SubUserEditTemplateModel,
  SubUserHosuReactivateModel,
  SubUserReactivateModel,
  SubUserStatus,
  SubUserUpdateModel
} from '~app/sub-users/shared/sub-users.models';

@Injectable({
  providedIn: 'root'
})
export class SubUserService implements Queryable<SubUser> {
  private baseUrl: string;

  private sortMapper = [
    {
      from: 'parent.name',
      to: 'parentName'
    }
  ];

  constructor(
    private http: HttpClient,
    private appConfigurationService: AppConfigurationService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {
    this.buildBaseUrl();
  }

  public find(query: DataQuery<SubUsersListFilter>): Observable<PagedData<SubUser>> {
    const requestQuery = this.formatQuery(query);
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http
      .get<PagedData<SubUser>>(url)
      .pipe(map(res => ({ ...res, records: res.records.map(this.setListProperties) })));
  }

  public getBy(id: number) {
    return this.http.get<SubUser>(`${this.baseUrl}/${id}`);
  }

  public create(payload: SubUserCreateModel): Observable<SubUser> {
    return this.http.post<SubUser>(this.baseUrl, payload);
  }

  public createHosu(payload: SubUserUpdateModel): Observable<SubUser> {
    return this.http.post<SubUser>(this.baseUrl, payload);
  }

  public update(data: SubUserUpdateModel): Observable<SubUser> {
    let formatedData = data;
    if (data.countries?.length) {
      formatedData = this.formatData(data);
    }

    return this.http.put<SubUser>(`${this.baseUrl}/${data.id}`, formatedData);
  }

  public deactivateUser(subUserId: number, expiryDate: Date) {
    return this.http
      .put<SubUser>(`${this.baseUrl}/${subUserId}`, {
        expiryDate: moment(expiryDate).format(GLOBALS.DATE_FORMAT.SHORT_ISO)
      })
      .pipe(map(this.setListProperties));
  }

  public deactivateHosuUser(subUserId: number) {
    return this.http
      .put<SubUser>(`${this.baseUrl}/${subUserId}`, {
        status: SubUserStatus.Inactive
      })
      .pipe(map(this.setListProperties));
  }

  public reactivateUser(id: number, reactivateData: SubUserReactivateModel) {
    return this.http.put<SubUser>(`${this.baseUrl}/${id}`, reactivateData).pipe(map(this.setListProperties));
  }

  public reactivateHosuUser(id: number, reactivateData: SubUserHosuReactivateModel) {
    return this.http.put<SubUser>(`${this.baseUrl}/${id}`, reactivateData).pipe(map(this.setListProperties));
  }

  public updateUsersTemplates(
    subUsersTemplateData: SubUserEditTemplateModel[]
  ): Observable<SubUserEditTemplateModel[]> {
    return this.http.put<SubUserEditTemplateModel[]>(`${this.baseUrl}/templates`, subUsersTemplateData);
  }

  public getPermissionsBy(userId: number): Observable<UserPermission[]> {
    const locale = this.translationService.getLocale();

    return this.http
      .get<UserPermissionDto[]>(`${this.baseUrl}/${userId}/permissions`, {
        headers: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          'Accept-Language': locale.language || 'en-US'
        }
      })
      .pipe(map(this.toUserPermissions));
  }

  public getHosuPermissionsBy(userId: number, subUserHosuBspId: number): Observable<UserPermission[]> {
    return this.http
      .get<UserPermissionDto[]>(`${this.baseUrl}/${userId}/${subUserHosuBspId}/permissions`)
      .pipe(map(this.toUserPermissions));
  }

  public getHosusPermissionsByBsps(bspIds: number[]): Observable<UserPermission[]> {
    return this.http.post<UserPermission[]>(`${this.baseUrl}/multi-permissions`, bspIds);
  }

  public updatePermissionsByHosusAndBsps(
    hosus: SubUser[],
    bsps: BspDto[],
    permissions: UserPermission[],
    enabled: boolean
  ) {
    const data: HosuPermissionUpdate = {
      ids: hosus.map(hosu => hosu.id),
      bspIds: bsps.map(bsp => bsp.id),
      enabled,
      permissions
    };

    return this.http.put<UserPermissionDto[]>(`${this.baseUrl}/multi-permissions`, data);
  }

  public updateHosuPermissionsByUserId(id: number, bspId: number, permissions: UserPermission[]) {
    const data: UserPermissionUpdate[] = permissions.map(p => ({ id: p.id, enabled: p.enabled }));

    return this.http
      .put<UserPermissionDto[]>(`${this.baseUrl}/${id}/${bspId}/permissions`, data)
      .pipe(map(this.toUserPermissions));
  }

  public updatePermissionsByUserId(id: number, permissions: UserPermission[]) {
    const data: UserPermissionUpdate[] = permissions.map(p => ({ id: p.id, enabled: p.enabled }));

    return this.http
      .put<UserPermissionDto[]>(`${this.baseUrl}/${id}/permissions`, data)
      .pipe(map(this.toUserPermissions));
  }

  public download(
    query: DataQuery<SubUsersListFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat.toUpperCase() };
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public getBspsFromHosuSelection(hosuIds: number[]): Observable<BspDto[]> {
    return this.http.post<BspDto[]>(`${this.baseUrl}/bsps`, hosuIds);
  }

  // TODO: The API has a type. Remove if it is fixed.
  private toUserPermissions = (dtos: UserPermissionDto[]): UserPermission[] =>
    dtos.map(({ catgory, ...p }) => ({ ...p, category: catgory }));

  private buildBaseUrl() {
    this.store
      .pipe(
        select(fromSubUsers.getSubUserTypeByParent),
        skipWhile(values => !values || values.includes(null) || values.includes(undefined))
      )
      .subscribe(
        subUserTypeByParent =>
          (this.baseUrl = `${this.appConfigurationService.baseApiPath}${SUB_USER_ENDPOINTS[subUserTypeByParent]}`)
      );
  }

  private setListProperties(subUser: SubUser): SubUser {
    const editable = subUser.level === 1 || subUser.ilogin ? SubUserEditable.Enabled : SubUserEditable.Disabled;
    const disableSelection = editable === SubUserEditable.Disabled || subUser.status === SubUserStatus.Inactive;

    return {
      ...subUser,
      editable,
      disableSelection
    };
  }

  private formatQuery(query: DataQuery<SubUsersListFilter>): RequestQuery {
    const { portalEmail, level, name, parentName, template, registerDate, expiryDate, status, ilogin } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        portalEmail,
        level,
        name,
        parentName,
        template,
        status,
        registerDateFrom: dateMapper(registerDate?.[0]),
        registerDateTo: dateMapper(registerDate?.[1] ?? registerDate?.[0]),
        expiryDateFrom: dateMapper(expiryDate?.[0]),
        expiryDateTo: dateMapper(expiryDate?.[1] ?? expiryDate?.[0]),
        ilogin
      },
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
  }

  private formatData(data: SubUserUpdateModel): SubUserUpdateModel {
    // Only countries with allowed access should be sent
    const countriesFiltered = data.countries
      .filter(country => country.access)
      .map(co => ({ id: co.id, isoCountryCode: co.isoCountryCode }));

    return {
      ...data,
      countries: countriesFiltered
    };
  }
}
