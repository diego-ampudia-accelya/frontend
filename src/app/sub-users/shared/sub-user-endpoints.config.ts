import { SubUserTypeByParent } from './sub-users.models';

export const SUB_USER_ENDPOINTS: {
  [subUserTypeByParent in SubUserTypeByParent]: string;
} = {
  [SubUserTypeByParent.losu]: `/user-management/users`,
  [SubUserTypeByParent.hosu]: `/user-management/mc-users`
};
