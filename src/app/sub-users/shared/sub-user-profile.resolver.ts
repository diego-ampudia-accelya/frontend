import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { merge, Observable } from 'rxjs';
import { filter, first, tap } from 'rxjs/operators';

import { SubUser } from './sub-users.models';
import { AppState } from '~app/reducers';
import { SubUsersProfileActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Injectable({
  providedIn: 'root'
})
export class SubUserProfileResolver implements Resolve<SubUser> {
  constructor(private store: Store<AppState>, private router: Router) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<SubUser> {
    this.requestSubUser(route);

    const subUser$ = this.store.pipe(select(fromSubUsers.getProfileSubUser), filter(Boolean));
    const subUserError$ = this.store.pipe(
      select(fromSubUsers.getProfileSubUserRequestError),
      filter(Boolean),
      tap(() => this.handleNavigationOnError())
    );

    return merge(subUserError$, subUser$).pipe(first()) as Observable<SubUser>;
  }

  private requestSubUser(route: ActivatedRouteSnapshot) {
    const id = Number(route.paramMap.get('id'));

    if (id) {
      this.store.dispatch(SubUsersProfileActions.requestSubUser({ id }));
    }
  }

  private handleNavigationOnError() {
    const lastSuccessfulNavigation = this.router.getCurrentNavigation().previousNavigation;
    if (lastSuccessfulNavigation != null) {
      this.router.navigateByUrl(lastSuccessfulNavigation.finalUrl.toString());
    } else {
      this.router.navigate(['./']);
    }
  }
}
