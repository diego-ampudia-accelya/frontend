import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { SubUserTypeByParent } from './sub-users.models';
import { getPermissions } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { SubUsersListActions } from '~app/sub-users/actions';

@Injectable()
export class SubUserListResolver implements Resolve<SubUserTypeByParent> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<SubUserTypeByParent> {
    return this.store.pipe(select(getPermissions), first()).pipe(
      map(({ permissions }) => {
        const subUserTypeByParent = permissions.includes(Permissions.lean)
          ? SubUserTypeByParent.hosu
          : SubUserTypeByParent.losu;

        // Side effect
        this.store.dispatch(SubUsersListActions.changeUserTypeByParent({ subUserTypeByParent }));

        return subUserTypeByParent;
      })
    );
  }
}
