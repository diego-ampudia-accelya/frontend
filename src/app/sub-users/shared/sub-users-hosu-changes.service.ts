import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';
import { filter, first, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import {
  ChangeModel,
  ChangesDialogComponent,
  ChangesDialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import { SubUserHosuPermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Injectable({
  providedIn: 'root'
})
export class SubUsersHosuChangesService {
  public cancelUserPermissions$: Observable<Action>;

  public applyUserPermissions$: Observable<Action>;

  private permissionsStatusTextMap = new Map([
    ['true', this.translationService.translate('subUsers.profile.propertiesTab.checkboxStatusText.true')],
    ['false', this.translationService.translate('subUsers.profile.propertiesTab.checkboxStatusText.false')]
  ]);

  private changedHosuPermissions$: Observable<ChangeModel[]> = this.store.pipe(
    select(fromSubUsers.getHosuPermissionsChanges),
    withLatestFrom(this.store.select(fromSubUsers.getEnabled)),
    map(([changes, enabled]) =>
      changes.map((change: ChangeModel) => ({
        ...change,
        // Original value is overwritten to show Enabled/Disabled pill color properly
        originalValue: enabled,
        value: this.permissionsStatusTextMap.get(enabled.toString())
      }))
    )
  );

  private cancelPermissionsDialogConfig$: Observable<ChangesDialogConfig> = of({
    data: {
      title: 'subUsers.hosuPermissions.cancelChanges.title',
      footerButtonsType: [{ type: FooterButton.Discard }]
    },
    message: 'subUsers.hosuPermissions.cancelChanges.message',
    changes: [],
    usePillsForChanges: true
  });

  private applyPermissionsDialogConfig$: Observable<ChangesDialogConfig> = this.store
    .select(fromSubUsers.getEnabled)
    .pipe(
      first(),
      map(enabled => ({
        data: {
          title: 'subUsers.hosuPermissions.applyChanges.title',
          footerButtonsType: [{ type: FooterButton.Apply }]
        },
        message: enabled
          ? 'subUsers.hosuPermissions.applyChanges.messageGrant'
          : 'subUsers.hosuPermissions.applyChanges.messageRevoke',
        changes: []
      }))
    );

  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {
    this.cancelUserPermissions$ = this.handlePermissionsChange(this.cancelPermissionsDialogConfig$);

    this.applyUserPermissions$ = this.handlePermissionsChange(this.applyPermissionsDialogConfig$);
  }

  private handlePermissionsChange(dialogConfig$: Observable<ChangesDialogConfig>): Observable<Action> {
    return this.changedHosuPermissions$.pipe(
      withLatestFrom(dialogConfig$),
      first(),
      switchMap(([changes, dialogConfig]) =>
        this.dialogService.open(ChangesDialogComponent, { ...dialogConfig, changes })
      ),
      filter(action => action?.clickedBtn && action.contentComponentRef),
      tap(({ contentComponentRef }) => contentComponentRef.config?.serviceRef?.close()),
      map(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Apply:
            return SubUserHosuPermissionsActions.applyChanges();
          case FooterButton.Discard:
            return SubUserHosuPermissionsActions.discardChanges();
          default:
            return SubUserHosuPermissionsActions.cancel();
        }
      })
    );
  }
}
