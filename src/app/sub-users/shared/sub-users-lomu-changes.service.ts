import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ChangeModel,
  ChangesDialogComponent,
  ChangesDialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import { SubUsersLomuProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Injectable({
  providedIn: 'root'
})
export class SubUsersLomuChangesService {
  public applyUserPermissions$: Observable<Action>;

  public handlePermissionsChangeAndDeactivate$: Observable<boolean>;

  private permissionsStatusTextMap = new Map([
    ['true', this.translationService.translate('subUsers.profile.propertiesTab.checkboxStatusText.true')],
    ['false', this.translationService.translate('subUsers.profile.propertiesTab.checkboxStatusText.false')]
  ]);

  private changedLomuPermissions$: Observable<ChangeModel[]>;

  private loading$: Observable<boolean>;

  private handlePermissionsChange$: Observable<boolean>;

  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {
    this.initializeProperties();
  }

  private initializeProperties(): void {
    this.changedLomuPermissions$ = this.store.pipe(
      select(fromSubUsers.getLomuProfilePermissionsChanges),
      map(changes =>
        changes.map((change: ChangeModel) => ({
          ...change,
          value: this.permissionsStatusTextMap.get(change.value.toString())
        }))
      ),
      take(1)
    );

    this.handlePermissionsChange$ = this.handlePermissionsChange({
      data: {
        title: 'subUsers.editUser.titleUnsaved',
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      message: 'subUsers.editUser.descriptionUnsavedPermissions',
      changes: [],
      usePillsForChanges: true
    }).pipe(
      tap(action => this.store.dispatch(action)),
      map(
        ({ type }) =>
          type === SubUsersLomuProfilePermissionsActions.applyChanges.type ||
          type === SubUsersLomuProfilePermissionsActions.discardChanges.type
      )
    );

    this.loading$ = this.store.pipe(select(fromSubUsers.areProfilePermissionsLoading));

    this.handlePermissionsChangeAndDeactivate$ = combineLatest([this.handlePermissionsChange$, this.loading$]).pipe(
      filter(([_, loading]) => !loading),
      map(([shouldProceed]) => shouldProceed)
    );

    this.applyUserPermissions$ = this.handlePermissionsChange({
      data: {
        title: 'subUsers.editUser.titleApply',
        footerButtonsType: [{ type: FooterButton.Apply }]
      },
      message: 'subUsers.editUser.descriptionLomuPermissionsApply',
      changes: [],
      usePillsForChanges: true
    });
  }

  private handlePermissionsChange(dialogConfig: ChangesDialogConfig): Observable<Action> {
    return this.changedLomuPermissions$.pipe(
      switchMap(changes => this.dialogService.open(ChangesDialogComponent, { ...dialogConfig, changes })),
      filter(action => action?.clickedBtn && action.contentComponentRef),
      tap(({ contentComponentRef }) => contentComponentRef.config?.serviceRef?.close()),
      map(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Apply:
            return SubUsersLomuProfilePermissionsActions.applyChanges();
          case FooterButton.Discard:
            return SubUsersLomuProfilePermissionsActions.discardChanges();
          default:
            return SubUsersLomuProfilePermissionsActions.cancel();
        }
      })
    );
  }
}
