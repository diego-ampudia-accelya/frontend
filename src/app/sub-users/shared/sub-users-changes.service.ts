import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ChangeModel,
  ChangesDialogComponent,
  ChangesDialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import { SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Injectable({
  providedIn: 'root'
})
export class SubUsersChangesService {
  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {}

  private permissionsStatusTextMap = new Map([
    ['true', this.translationService.translate('subUsers.profile.propertiesTab.checkboxStatusText.true')],
    ['false', this.translationService.translate('subUsers.profile.propertiesTab.checkboxStatusText.false')]
  ]);

  private changedPermissions$: Observable<ChangeModel[]> = this.store.pipe(
    select(fromSubUsers.getProfilePermissionsChanges),
    map(changes =>
      changes.map((change: ChangeModel) => ({
        ...change,
        value: this.permissionsStatusTextMap.get(change.value.toString())
      }))
    ),
    take(1)
  );

  private loading$ = this.store.pipe(select(fromSubUsers.areProfilePermissionsLoading));

  public applyUserPermissions$: Observable<Action> = this.handlePermissionsChange({
    data: {
      title: 'subUsers.editUser.titleApply',
      footerButtonsType: [{ type: FooterButton.Apply }]
    },
    message: 'subUsers.editUser.descriptionPermissionsApply',
    changes: []
  });

  private handlePermissionsChange$: Observable<boolean> = this.handlePermissionsChange({
    data: {
      title: 'subUsers.editUser.titleUnsaved',
      footerButtonsType: [
        { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
        { type: FooterButton.Apply }
      ]
    },
    message: 'subUsers.editUser.descriptionUnsavedPermissions',
    changes: []
  }).pipe(
    tap(action => this.store.dispatch(action)),
    map(
      ({ type }) =>
        type === SubUsersProfilePermissionsActions.applyChanges.type ||
        type === SubUsersProfilePermissionsActions.discardChanges.type
    )
  );

  public handlePermissionsChangeAndDeactivate$ = combineLatest([this.handlePermissionsChange$, this.loading$]).pipe(
    filter(([_, loading]) => !loading),
    map(([shouldProceed]) => shouldProceed)
  );

  private handlePermissionsChange(dialogConfig: ChangesDialogConfig): Observable<Action> {
    return this.changedPermissions$.pipe(
      switchMap(changes => this.dialogService.open(ChangesDialogComponent, { ...dialogConfig, changes })),
      filter(action => action?.clickedBtn && action.contentComponentRef),
      tap(({ contentComponentRef }) => contentComponentRef.config?.serviceRef?.close()),
      map(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Apply:
            return SubUsersProfilePermissionsActions.applyChanges();
          case FooterButton.Discard:
            return SubUsersProfilePermissionsActions.discardChanges();
          default:
            return SubUsersProfilePermissionsActions.cancel();
        }
      })
    );
  }
}
