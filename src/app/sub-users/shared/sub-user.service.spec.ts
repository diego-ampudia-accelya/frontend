import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { of } from 'rxjs';

import { SubUserService } from './sub-user.service';
import { SubUserEditable, SubUserStatus, SubUserTemplate, SubUserTypeByParent } from './sub-users.models';
import { SortOrder } from '~app/shared/enums';
import { AppConfigurationService } from '~app/shared/services';
import {
  getFindTestQuery,
  getFindTestResponse,
  getInitialSubUsersState,
  getTestSubUser
} from '~app/sub-users/mocks/sub-users.test.mocks';
import * as fromSubUsers from '~app/sub-users/reducers';

describe('SubUserService LOSU', () => {
  let service: SubUserService;
  let httpClientSpy: SpyObject<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: L10nTranslationService, useValue: { getLocale: () => 'en' } },
        mockProvider(AppConfigurationService, { baseApiPath: 'api' }),
        mockProvider(HttpClient),
        provideMockStore({
          initialState: getInitialSubUsersState(),
          selectors: [{ selector: fromSubUsers.getSubUserTypeByParent, value: SubUserTypeByParent.losu }]
        })
      ]
    });

    service = TestBed.inject(SubUserService);

    httpClientSpy = TestBed.inject<any>(HttpClient);
    httpClientSpy.get.and.returnValue(of());
    httpClientSpy.post.and.returnValue(of());
    httpClientSpy.put.and.returnValue(of());
    httpClientSpy.delete.and.returnValue(of());
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    it('should call get with default sorting', fakeAsync(() => {
      const responseData = getFindTestResponse();
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(httpClientSpy.get).toHaveBeenCalledWith('api/user-management/users?page=0&size=20&sort=level,ASC');
      });
    }));

    it('Level 1 users should have editable set to ENABLED', fakeAsync(() => {
      const responseData = getFindTestResponse({ level: 1 });
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(res.records[0].editable).toEqual(SubUserEditable.Enabled);
      });
    }));

    it('Level 2 users should have editable set to DISABLED', fakeAsync(() => {
      const responseData = getFindTestResponse({ level: 2 });
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(res.records[0].editable).toEqual(SubUserEditable.Disabled);
      });
    }));

    it('Level 3 users should have editable set to DISABLED', fakeAsync(() => {
      const responseData = getFindTestResponse({ level: 3 });
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(res.records[0].editable).toEqual(SubUserEditable.Disabled);
      });
    }));

    it('Level 1 Active users should have disableSelection set to false', fakeAsync(() => {
      const responseData = getFindTestResponse({ level: 1, status: SubUserStatus.Active });
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(res.records[0].disableSelection).toEqual(false);
      });
    }));

    it('Level 1 Inactive users should have disableSelection set to true', fakeAsync(() => {
      const responseData = getFindTestResponse({ level: 1, status: SubUserStatus.Inactive });
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(res.records[0].disableSelection).toEqual(true);
      });
    }));

    it('Level 2 Active users should have disableSelection set to true', fakeAsync(() => {
      const responseData = getFindTestResponse({ level: 2, status: SubUserStatus.Active });
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(res.records[0].disableSelection).toEqual(true);
      });
    }));

    it('Level 2 Inactive users should have disableSelection set to true', fakeAsync(() => {
      const responseData = getFindTestResponse({ level: 2, status: SubUserStatus.Inactive });
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }])).subscribe(res => {
        expect(res.records[0].disableSelection).toEqual(true);
      });
    }));

    it('should call get, with filters for both registerDateFrom and registerDateTo if only one registerDate  is provided', fakeAsync(() => {
      const date = new Date();
      const dateString = moment(date).format('YYYY-MM-DD');
      const queryString = `api/user-management/users?page=0&size=20&sort=level,ASC&registerDateFrom=${dateString}&registerDateTo=${dateString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service
        .find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }], { registerDate: [date, null] }))
        .subscribe(() => {
          expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
        });
    }));

    it('should call get, with filters for registerDateFrom and registerDateTo ', fakeAsync(() => {
      const dateFrom = new Date('2020-02-20');
      const dateFromString = moment(dateFrom).format('YYYY-MM-DD');

      const dateTo = new Date('2020-02-20');
      const dateToString = moment(dateTo).format('YYYY-MM-DD');

      const queryString = `api/user-management/users?page=0&size=20&sort=level,ASC&registerDateFrom=${dateFromString}&registerDateTo=${dateToString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service
        .find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }], { registerDate: [dateFrom, dateTo] }))
        .subscribe(() => {
          expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
        });
    }));

    it('should call get, with filters for both expiryDateFrom and expiryDateTo if only one expiryDate is provided', fakeAsync(() => {
      const date = new Date();
      const dateString = moment(date).format('YYYY-MM-DD');
      const queryString = `api/user-management/users?page=0&size=20&sort=level,ASC&expiryDateFrom=${dateString}&expiryDateTo=${dateString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service
        .find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }], { expiryDate: [date, null] }))
        .subscribe(() => {
          expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
        });
    }));

    it('should call get, with filters for expiryDateFrom and expiryDateTo ', fakeAsync(() => {
      const dateFrom = new Date('2020-02-20');
      const dateFromString = moment(dateFrom).format('YYYY-MM-DD');

      const dateTo = new Date('2020-02-20');
      const dateToString = moment(dateTo).format('YYYY-MM-DD');

      const queryString = `api/user-management/users?page=0&size=20&sort=level,ASC&expiryDateFrom=${dateFromString}&expiryDateTo=${dateToString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service
        .find(getFindTestQuery([{ attribute: 'level', sortType: SortOrder.Asc }], { expiryDate: [dateFrom, dateTo] }))
        .subscribe(() => {
          expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
        });
    }));
  });

  it('getById should call get with proper address', fakeAsync(() => {
    service.getBy(123);

    expect(httpClientSpy.get).toHaveBeenCalledWith('api/user-management/users/123');
  }));

  it('create should call post with proper address and payload', fakeAsync(() => {
    const payload = {
      template: SubUserTemplate.Efficient,
      portalEmail: '',
      name: '',
      organization: '',
      telephone: '',
      country: '',
      city: '',
      address: '',
      postCode: ''
    };

    service.create(payload);

    expect(httpClientSpy.post).toHaveBeenCalledWith('api/user-management/users', payload);
  }));

  it('update should call put with proper address and payload', fakeAsync(() => {
    const subUser = getTestSubUser();

    const payload = {
      id: subUser.id,
      portalEmail: subUser.portalEmail,
      name: subUser.name,
      template: subUser.template,
      organization: subUser.contactInfo.organization,
      telephone: subUser.contactInfo.telephone,
      country: subUser.contactInfo.country,
      city: subUser.contactInfo.city,
      address: subUser.contactInfo.address,
      postCode: subUser.contactInfo.postCode
    };

    service.update(payload);

    expect(httpClientSpy.put).toHaveBeenCalledWith('api/user-management/users/6005', payload);
  }));

  it('deactivateUser should call post with proper address and payload', fakeAsync(() => {
    service.deactivateUser(6005, new Date('2020-02-20'));

    expect(httpClientSpy.put).toHaveBeenCalledWith('api/user-management/users/6005', { expiryDate: '2020-02-20' });
  }));

  it('reactivateUser should call post with proper address and payload', fakeAsync(() => {
    const reactivateData = {
      portalEmail: 'test@test.com',
      template: SubUserTemplate.Efficient,
      status: SubUserStatus.Active
    };

    service.reactivateUser(6005, reactivateData);

    expect(httpClientSpy.put).toHaveBeenCalledWith('api/user-management/users/6005', reactivateData);
  }));

  it('updateUsersTemplates should call post with proper address and payload', fakeAsync(() => {
    const templateData = [{ userId: 6005, template: SubUserTemplate.Streamlined }];
    service.updateUsersTemplates(templateData);

    expect(httpClientSpy.put).toHaveBeenCalledWith('api/user-management/users/templates', templateData);
  }));
});

describe('SubUserServiceService HOSU', () => {
  let service: SubUserService;
  let httpClientSpy: SpyObject<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: L10nTranslationService, useValue: { getLocale: () => 'en' } },
        mockProvider(AppConfigurationService, { baseApiPath: 'api' }),
        mockProvider(HttpClient),
        provideMockStore({
          initialState: getInitialSubUsersState(),
          selectors: [{ selector: fromSubUsers.getSubUserTypeByParent, value: SubUserTypeByParent.hosu }]
        })
      ]
    });

    service = TestBed.inject(SubUserService);

    httpClientSpy = TestBed.inject<any>(HttpClient);
    httpClientSpy.get.and.returnValue(of());
    httpClientSpy.post.and.returnValue(of());
    httpClientSpy.put.and.returnValue(of());
    httpClientSpy.delete.and.returnValue(of());
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    it('should call get with default sorting', fakeAsync(() => {
      const responseData = getFindTestResponse();
      httpClientSpy.get.and.returnValue(of(responseData));

      service.find(getFindTestQuery([])).subscribe(res => {
        expect(httpClientSpy.get).toHaveBeenCalledWith('api/user-management/mc-users?page=0&size=20');
      });
    }));

    it('should call get, with filters for both registerDateFrom and registerDateTo if only one registerDate  is provided', fakeAsync(() => {
      const date = new Date();
      const dateString = moment(date).format('YYYY-MM-DD');
      const queryString = `api/user-management/mc-users?page=0&size=20&registerDateFrom=${dateString}&registerDateTo=${dateString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service.find(getFindTestQuery([], { registerDate: [date, null] })).subscribe(() => {
        expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
      });
    }));

    it('should call get, with filters for registerDateFrom and registerDateTo ', fakeAsync(() => {
      const dateFrom = new Date('2020-02-20');
      const dateFromString = moment(dateFrom).format('YYYY-MM-DD');

      const dateTo = new Date('2020-02-20');
      const dateToString = moment(dateTo).format('YYYY-MM-DD');

      const queryString = `api/user-management/mc-users?page=0&size=20&registerDateFrom=${dateFromString}&registerDateTo=${dateToString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service.find(getFindTestQuery([], { registerDate: [dateFrom, dateTo] })).subscribe(() => {
        expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
      });
    }));

    it('should call get, with filters for both expiryDateFrom and expiryDateTo if only one expiryDate is provided', fakeAsync(() => {
      const date = new Date();
      const dateString = moment(date).format('YYYY-MM-DD');
      const queryString = `api/user-management/mc-users?page=0&size=20&expiryDateFrom=${dateString}&expiryDateTo=${dateString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service.find(getFindTestQuery([], { expiryDate: [date, null] })).subscribe(() => {
        expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
      });
    }));

    it('should call get, with filters for expiryDateFrom and expiryDateTo ', fakeAsync(() => {
      const dateFrom = new Date('2020-02-20');
      const dateFromString = moment(dateFrom).format('YYYY-MM-DD');

      const dateTo = new Date('2020-02-20');
      const dateToString = moment(dateTo).format('YYYY-MM-DD');

      const queryString = `api/user-management/mc-users?page=0&size=20&expiryDateFrom=${dateFromString}&expiryDateTo=${dateToString}`;

      httpClientSpy.get.and.returnValue(of(getFindTestResponse()));

      service.find(getFindTestQuery([], { expiryDate: [dateFrom, dateTo] })).subscribe(() => {
        expect(httpClientSpy.get).toHaveBeenCalledWith(queryString);
      });
    }));
  });

  it('getById should call get with proper address', fakeAsync(() => {
    service.getBy(123);

    expect(httpClientSpy.get).toHaveBeenCalledWith('api/user-management/mc-users/123');
  }));

  it('getById should call get with proper address for HOSU', fakeAsync(() => {
    service.getBy(123);

    expect(httpClientSpy.get).toHaveBeenCalledWith('api/user-management/mc-users/123');
  }));

  it('deactivateHosuUser should call put with correct payload', fakeAsync(() => {
    service.deactivateHosuUser(6005);

    expect(httpClientSpy.put).toHaveBeenCalledWith('api/user-management/mc-users/6005', {
      status: SubUserStatus.Inactive
    });
  }));
});
