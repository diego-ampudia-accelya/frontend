export interface PermissionCategory {
  id?: number;
  code: string;
  name: string;
}

export interface UserPermission {
  id: number;
  code: string;
  name: string;
  category: PermissionCategory;
  enabled: boolean;
  template: 'STREAMLINED' | 'EFFICIENT' | 'BASIC' | 'ENHANCED';
}

export interface UserPermissionLomu {
  id: number;
  code: string;
  name: string;
  catgory: PermissionCategory;
  enabled: boolean;
  template: 'STREAMLINED' | 'EFFICIENT' | 'BASIC' | 'ENHANCED';
}
export interface UserPermissionLomuResponse {
  idUser: number;
  permissions: UserPermissionLomu[];
}

export interface UserPermissionHosu {
  id: number;
  code: string;
  name: string;
  catgory: PermissionCategory;
  enabled: boolean;
  template: 'BASIC' | 'ENHANCED';
}

export type UserPermissionDto = Omit<UserPermission, 'category'> & { catgory: PermissionCategory };

export interface UserPermissionUpdate {
  id: number;
  enabled: boolean;
}

export interface HosuPermissionUpdate {
  ids: number[];
  bspIds: number[];
  enabled: boolean;
  permissions: UserPermission[];
}
