import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { AppState } from '~app/reducers';
import { SubUsersProfileActions } from '~app/sub-users/actions';

@Injectable()
export class SubUserCreateResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<boolean> {
    this.store.dispatch(SubUsersProfileActions.isCreatingHosuSubUser());

    return of(true);
  }
}
