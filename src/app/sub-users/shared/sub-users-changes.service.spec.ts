import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { SubUsersChangesService } from './sub-users-changes.service';
import { FooterButton } from '~app/shared/components';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import { getInitialSubUsersState } from '~app/sub-users/mocks/sub-users.test.mocks';

describe('SubUsersChangesService', () => {
  let service: SubUsersChangesService;
  let dialogServiceSpy: SpyObject<DialogService>;
  let store: MockStore;

  beforeEach(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    TestBed.configureTestingModule({
      providers: [
        mockProvider(L10nTranslationService),
        {
          provide: DialogService,
          useValue: dialogServiceSpy
        },
        provideMockStore({ initialState: getInitialSubUsersState() })
      ]
    });
    service = TestBed.inject(SubUsersChangesService);
    store = TestBed.inject(MockStore);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('handlePermissionsChangeAndDeactivate$', () => {
    it('should return true on FooterButton.Apply, and dispatch applyChanges', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Apply, contentComponentRef: {} }));

      service.handlePermissionsChangeAndDeactivate$.subscribe(result => {
        expect(result).toBe(true);
        expect(store.dispatch).toHaveBeenCalledWith(SubUsersProfilePermissionsActions.applyChanges());
      });

      tick();
    }));

    it('should return true on FooterButton.Discard, and dispatch subUserDetailsReset', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Discard, contentComponentRef: {} }));

      service.handlePermissionsChangeAndDeactivate$.subscribe(result => {
        expect(result).toBe(true);
        expect(store.dispatch).toHaveBeenCalledWith(SubUsersProfilePermissionsActions.discardChanges());
      });

      tick();
    }));

    it('should return false on FooterButton.Cancel, and dispatch cancelSubUserPermissionsChanges', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel, contentComponentRef: {} }));

      service.handlePermissionsChangeAndDeactivate$.subscribe(result => {
        expect(result).toBe(false);
        expect(store.dispatch).toHaveBeenCalledWith(SubUsersProfilePermissionsActions.cancel());
      });

      tick();
    }));
  });
});
