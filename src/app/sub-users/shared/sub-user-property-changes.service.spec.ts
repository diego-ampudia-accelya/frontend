import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { SubUserPropertyChangesService } from './sub-user-property-changes.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService, FooterButton } from '~app/shared/components';
import { SubUsersProfileActions } from '~app/sub-users/actions';
import { getInitialSubUsersState } from '~app/sub-users/mocks/sub-users.test.mocks';
import * as fromSubUsers from '~app/sub-users/reducers';

describe('SubUserPropertyChangesService', () => {
  let service: SubUserPropertyChangesService;
  let mockStore: MockStore;
  let dialogServiceSpy: SpyObject<DialogService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        provideMockStore({ initialState: getInitialSubUsersState() }),
        mockProvider(DialogService),
        mockProvider(L10nTranslationService, {
          translate: (key: string) => key
        })
      ]
    });
    service = TestBed.inject(SubUserPropertyChangesService);
    mockStore = TestBed.inject<any>(Store);
    mockStore.overrideSelector(fromSubUsers.getProfilePropertyChanges, []);
    mockStore.overrideSelector(fromSubUsers.hasPropertyValidationErrors, false);
    dialogServiceSpy = TestBed.inject<any>(DialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('confirmApplyPropertyChanges', () => {
    it('should result to false on Cancel', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel, contentComponentRef: {} }));
      let result: boolean;
      const isCreatingHosu = false;
      service
        .confirmApplyPropertyChanges(isCreatingHosu)
        .subscribe(confirmationResult => (result = confirmationResult));
      expect(result).toBeFalsy();
    }));

    it('should result to false on Discard', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Discard, contentComponentRef: {} }));
      let result: boolean;
      const isCreatingHosu = false;
      service
        .confirmApplyPropertyChanges(isCreatingHosu)
        .subscribe(confirmationResult => (result = confirmationResult));
      expect(result).toBeFalsy();
    }));

    it('should result to true on Apply', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Apply, contentComponentRef: {} }));
      let result: boolean;
      const isCreatingHosu = false;
      service
        .confirmApplyPropertyChanges(isCreatingHosu)
        .subscribe(confirmationResult => (result = confirmationResult));
      expect(result).toBeTruthy();
    }));

    it('should close the dialog', fakeAsync(() => {
      const contentComponentRef = { config: { serviceRef: { close: () => {} } } };
      const serviceRef = contentComponentRef.config.serviceRef;
      const isCreatingHosu = false;
      spyOn(serviceRef, 'close');
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel, contentComponentRef }));
      service.confirmApplyPropertyChanges(isCreatingHosu).subscribe();
      expect(serviceRef.close).toHaveBeenCalled();
    }));
  });

  describe('getDeactivateAction', () => {
    it('should getDeactivateAction calls dispatch function', fakeAsync(() => {
      spyOn<any>(service, 'handleDeactivationChange').and.returnValue(
        of(SubUsersProfileActions.updateSubUserProperties({ data: null }))
      );
      const dispatchSpy = spyOn(mockStore, 'dispatch').and.callThrough();
      let result;
      service.getDeactivateAction().subscribe(data => (result = data));
      tick();

      expect(result).toBeTruthy();
      expect(dispatchSpy).toHaveBeenCalledWith(SubUsersProfileActions.updateSubUserProperties({ data: null }));
    }));

    it('should result to discardPropertyChanges if no changes', fakeAsync(() => {
      spyOn<any>(service, 'openPropertyChangesDialog').and.returnValue(of(FooterButton.Discard));
      let result: Action;
      service['handleDeactivationChange'](null).subscribe(actionResult => (result = actionResult));
      expect(result).toEqual(SubUsersProfileActions.discardPropertyChanges());
    }));

    it('should result to cancelPropertyChanges on Cancel', fakeAsync(() => {
      spyOn<any>(service, 'openPropertyChangesDialog').and.returnValue(of(FooterButton.Cancel));

      let result: Action;
      service['handleDeactivationChange'](null).subscribe(actionResult => (result = actionResult));
      expect(result).toEqual(SubUsersProfileActions.cancelPropertyChanges());
    }));

    it('should result to on updateSubUserProperties Apply', fakeAsync(() => {
      spyOn<any>(service, 'openPropertyChangesDialog').and.returnValue(of(FooterButton.Apply));
      let result: Action;
      service['handleDeactivationChange'](null).subscribe(actionResult => (result = actionResult));
      expect(result).toEqual(SubUsersProfileActions.updateSubUserProperties({ data: null }));
    }));
  });

  it('should formatLabel return correct translation', () => {
    const result = service['formatLabel']('test');

    expect(result).toEqual('subUsers.profile.propertiesTab.labels.test');
  });

  it('should formatBoolean return correct translation when param is truly', () => {
    const result = service['formatBoolean'](true);

    expect(result).toEqual('subUsers.editUser.enabled');
  });

  it('should formatBoolean return correct translation when param is falsy', () => {
    const result = service['formatBoolean'](false);

    expect(result).toEqual('subUsers.editUser.disabled');
  });

  it('should formatItem return correct translation when item is an object', () => {
    const item = {
      name: 'testName',
      value: { name: 'testName', isoCountryCode: 'ES', access: true }
    };
    const result = service['formatItem'](item);

    expect(result).toEqual('ES: testName, subUsers.editUser.enabled');
  });

  it('should formatItem return correct translation when item is boolean value', () => {
    const item = {
      name: 'testName',
      value: true
    };
    const result = service['formatItem'](item);

    expect(result).toEqual('subUsers.profile.propertiesTab.labels.testName:  subUsers.editUser.enabled');
  });

  it('should formatItem return correct translation when item is string value', () => {
    const item = {
      name: 'testName',
      value: 'testValue'
    };
    const result = service['formatItem'](item);

    expect(result).toEqual('subUsers.profile.propertiesTab.labels.testName:  testValue');
  });
});
