/* eslint-disable @typescript-eslint/naming-convention */
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { UserPermission } from '~app/sub-users/shared/permissions.models';

export interface SubUserTypeDetails {
  iataCode: string;
  name: string;
}

export interface SubUser {
  id?: number;
  portalEmail: string;
  level?: number;
  name: string;
  parent?: {
    id: number;
    name: string;
  };
  template?: SubUserTemplate;
  registerDate: string; //or may be a Date()
  expiryDate: string; // or may be Date()
  status: SubUserStatus;
  userType: SubUserType; // it may be Enum
  userTypeDetails: SubUserTypeDetails;
  contactInfo: {
    organization: string;
    telephone: string;
    country: string;
    city: string;
    address: string;
    postCode: string;
    email?: string;
  };
  permissions: any[];
  editable?: SubUserEditable;
  disableSelection?: boolean;
  isSelected?: boolean;
  iataCode?: string;
  allCountries?: boolean;
  ilogin?: string;
  countries?: CountriesAccessPermissions[];
}

export interface SubUsersSearchForm {
  ilogin?: string;
  bsp?: Bsp;
  portalEmail: string;
  level: string[];
  name: string;
  parentName: string;
  template: SubUserTemplate;
  registerDate: Date[];
  expiryDate: Date[];
  status: SubUserStatus;
}

export interface SubUserUpdateModel {
  id?: number;
  template?: SubUserTemplate;
  portalEmail?: string;
  name?: string;
  organization?: string;
  telephone?: string;
  country?: string;
  city?: string;
  address?: string;
  postCode?: string;
  email?: string;
  countries?: CountriesAccessPermissions[];
  allCountries?: boolean;
}

export interface SubUserDetails {
  id: number;
  template?: SubUserTemplate;
  portalEmail?: string;
  name?: string;
  organization?: string;
  telephone?: string;
  country?: string;
  city?: string;
  address?: string;
  postCode?: string;
}

export interface CountriesAccessPermissions {
  id: number;
  isoCountryCode: string;
  name?: string;
  access?: boolean;
  editable?: SubUserEditable;
  disableSelection?: boolean;
  isChanged?: boolean;
}

export interface SubUserLomu {
  id: number;
  bsp: BspDto;
  portalEmail: string;
  name: string;
  template: SubUserTemplate;
  registerDate: string;
  expiryDate: string;
  status: SubUserStatus;
  userType: SubUserType;
  userTypeDetails: SubUserTypeDetails;
  contactInfo: {
    organization: string;
    country: string;
    city: string;
    address: string;
    postCode: string;
  };
}

export type SubUserCreateModel = Omit<SubUserDetails, 'id'>;

export enum SubUserTemplate {
  Streamlined = 'STREAMLINED',
  Efficient = 'EFFICIENT'
}

export enum HosuSubUserTemplate {
  Basic = 'BASIC',
  Enhanced = 'ENHANCED'
}

export enum SubUserStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export enum SubUserType {
  Agent = 'AGENT',
  Airline = 'AIRLINE',
  AgentGroup = 'AGENT GROUP'
}

export enum SubUserEditable {
  Enabled = 'ENABLED',
  Disabled = 'DISABLED'
}

export enum SubUserTypeByParent {
  losu = 'LOSU',
  hosu = 'HOSU'
}

export enum SubUserLevel {
  level1 = 1,
  level2 = 2,
  level3 = 3
}

export interface SubUserTemplateViewModel {
  id: number;
  portalEmail: string;
  name: string;
  template: SubUserTemplate;
}

export interface SubUserPermissionsOrigin {
  id: number;
  portalEmail: string;
  name: string;
  template: SubUserTemplate;
  permissions: UserPermission[];
}

export interface SubUserPermissionCategoryGroups {
  userId: number;
  categoryId: number;
  portalEmail: string;
  checked: boolean;
  checkboxDesign: string;
  disabled: boolean;
  hasChanges: boolean;
}

export interface SubUserPermission {
  id: number;
  portalEmail: string;
  name: string;
  hasChanged: boolean;
  disabled: boolean;
  checked: boolean;
  permission: {
    id: number;
    tooltip: string;
    enabled: boolean;
    category: {
      id: number;
    };
  };
}

export interface SubUserPermissionGroups {
  id: number;
  name: string;
  checked: boolean;
  checkboxDesign: string;
  hasChanges: boolean;
  disabled: boolean;
  tooltip: string;
  users: SubUserPermission[];
}

export interface SubUsersPermissionsViewModel {
  id: number;
  name: string;
  disabled: boolean;
  checked: boolean;
  checkboxDesign: string;
  expanded: boolean;
  hasChanges: boolean;
  categoryGroups: SubUserPermissionCategoryGroups[];
  permissions: SubUserPermissionGroups[];
}

export interface SubUsersPermissionsOriginModel {
  [key: number]: {
    [key: number]: {
      [key: number]: boolean;
    };
  };
}

export interface SubUsersChangedPermissionsPermission {
  id: number;
  name: string;
  hasChanges: boolean;
  user: {
    id: number;
    name: string;
    portalEmail: string;
    permission: {
      id: number;
      code: boolean;
      name: string;
      enabled: boolean;
      template: string;
      category: {
        id: number;
        code: boolean;
        name: string;
      };
    };
  };
}

export interface SubUsersChangedPermissionsGroup {
  id: number;
  name: string;
  hasChanges: boolean;
  permissions: SubUsersChangedPermissionsPermission[];
}

export interface SubUsersChangedPermissions {
  id: number;
  portalEmail: string;
  hasChanges: boolean;
  permissionGroups: SubUsersChangedPermissionsGroup[];
}

export interface SubUserEditTemplateModel {
  userId: number;
  template: SubUserTemplate;
}

export interface SubUserReactivateModel {
  portalEmail: string;
  template: SubUserTemplate;
  status: SubUserStatus;
}

export interface SubUserHosuReactivateModel {
  portalEmail: string;
  status: SubUserStatus;
  allCountries: boolean;
  countries: CountriesAccessPermissions[];
}

export interface SubUserHosuPermissionSearch {
  bsp: BspDto;
}
