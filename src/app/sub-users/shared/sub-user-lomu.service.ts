import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  UserPermission,
  UserPermissionDto,
  UserPermissionLomu,
  UserPermissionLomuResponse,
  UserPermissionUpdate
} from './permissions.models';
import { AppConfigurationService } from '~app/shared/services';
import { SubUserLomu } from '~app/sub-users/shared/sub-users.models';

@Injectable({
  providedIn: 'root'
})
export class SubUserLomuService {
  private baseUrl = `${this.appConfigurationService.baseApiPath}/user-management/users/lomus`;

  constructor(private http: HttpClient, private appConfigurationService: AppConfigurationService) {}

  public getSubUserLomu(bspId: number): Observable<SubUserLomu> {
    return this.http.get<SubUserLomu>(this.baseUrl, {
      params: {
        bspId: bspId.toString()
      }
    });
  }

  public getSubUserLomuPermissions(bspId: number): Observable<UserPermissionLomuResponse[]> {
    const url = `${this.baseUrl}/permissions`;

    return this.http.get<UserPermissionLomuResponse[]>(url, {
      params: {
        bspId: bspId.toString()
      }
    });
  }

  public updatePermissionsByUserId(id: number, permissions: UserPermission[]) {
    const data: UserPermissionUpdate[] = permissions.map(p => ({ id: p.id, enabled: p.enabled }));

    return this.http
      .put<UserPermissionDto[]>(`${this.baseUrl}/${id}/permissions`, data)
      .pipe(map(this.toUserPermissions));
  }

  private toUserPermissions = (dtos: UserPermissionDto[]): UserPermissionLomu[] =>
    dtos.map(({ catgory, ...p }) => ({ ...p, catgory }));
}
