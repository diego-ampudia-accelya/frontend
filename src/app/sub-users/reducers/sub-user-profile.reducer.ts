import { Action, createReducer, on } from '@ngrx/store';
import { isEmpty } from 'lodash';

import { ChangeModel } from '~app/shared/components';
import { ServerError } from '~app/shared/errors';
import { toSubUserUpdateModel } from '~app/shared/helpers/user.helper';
import { SubUsersProfileActions, SubUserStatusActions } from '~app/sub-users/actions';
import { SubUser, SubUserUpdateModel } from '~app/sub-users/shared/sub-users.models';

export const subUserProfileFeatureKey = 'subUserProfile';

export const propertyCategories = {
  template: 'template',
  portalEmail: 'contactDetails',
  name: 'contactDetails',
  organization: 'contactDetails',
  telephone: 'contactDetails',
  country: 'addressDetails',
  city: 'addressDetails',
  address: 'addressDetails',
  postCode: 'addressDetails',
  email: 'addressDetails',
  allCountries: 'countriesAccessCategory',
  countries: 'countriesAccessCategory'
};

export interface State {
  loading: boolean;
  deactivating: boolean;
  subUser: SubUser;
  subUserRequestError: ServerError;
  propertiesFormValue: SubUserUpdateModel;
  hasPropertyValidationErrors: boolean;
  isCreatingHosuMode: boolean;
  subUserPropertiesError: ServerError;
}

export const initialState: State = {
  loading: false,
  deactivating: false,
  subUser: null,
  subUserRequestError: null,
  propertiesFormValue: null,
  hasPropertyValidationErrors: false,
  isCreatingHosuMode: false,
  subUserPropertiesError: null
};

const subUserProfileReducer = createReducer(
  initialState,
  on(SubUsersProfileActions.isCreatingHosuSubUser, () => ({
    ...initialState,
    isCreatingHosuMode: true
  })),
  on(SubUsersProfileActions.requestSubUser, () => ({
    ...initialState,
    isCreatingHosuMode: false
  })),
  on(SubUsersProfileActions.updateSubUserProperties, state => ({
    ...state,
    loading: true
  })),
  on(
    SubUsersProfileActions.requestSubUserSuccess,
    SubUserStatusActions.deactivateSubUserSuccess,
    SubUserStatusActions.reactivateSubUserSuccess,
    (state, { subUser }) => ({
      ...state,
      subUser,
      loading: false
    })
  ),
  on(SubUsersProfileActions.updateSubUserPropertiesSuccess, (state, { subUser }) => ({
    ...state,
    loading: false,
    deactivating: false,
    propertiesFormValue: null,
    subUserPropertiesError: null,
    subUser
  })),
  on(SubUsersProfileActions.updateSubUserPropertiesError, (state, { error }) => ({
    ...state,
    loading: false,
    deactivating: false,
    subUserPropertiesError: error
  })),
  on(SubUsersProfileActions.requestSubUserError, (_state, { error }) => ({
    ...initialState,
    subUserRequestError: error
  })),
  on(SubUsersProfileActions.propertiesFormValueChange, (state, { isFormValid, formValue: propertiesFormValue }) => ({
    ...state,
    propertiesFormValue,
    hasPropertyValidationErrors: !isFormValid
  })),
  on(SubUsersProfileActions.propertiesDeactivationRequest, state => ({
    ...state,
    deactivating: true
  })),
  on(SubUsersProfileActions.discardPropertyChanges, state => ({
    ...state,
    deactivating: false,
    propertiesFormValue: null
  })),
  on(SubUsersProfileActions.createHosuSubUserError, (state, { error }) => ({
    ...state,
    loading: false,
    requestError: error
  })),
  on(SubUsersProfileActions.createHosuSubUser, state => ({
    ...state,
    loading: true
  })),
  on(SubUsersProfileActions.cancelPropertyChanges, state => ({
    ...state,
    deactivating: false
  })),
  on(SubUsersProfileActions.createHosuSubUserSuccess, () => initialState)
);

export function reducer(state: State | undefined, action: Action) {
  return subUserProfileReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;

export const getIsCreatingHosuMode = (state: State): boolean => state.isCreatingHosuMode;

export const getDeactivating = (state: State): boolean => state.deactivating;

export const getSubUser = (state: State): SubUser => state.subUser;

export const getSubUserPortalEmail = (state: State): string => state.subUser?.portalEmail || '';

export const getSubUserRequestError = (state: State): ServerError => state.subUserRequestError;

export const getPropertyChanges = (state: State): ChangeModel[] => {
  const subUserDetails = toSubUserUpdateModel(state.subUser);
  const { propertiesFormValue } = state;

  return Object.entries(propertiesFormValue || {}).reduce((changes, [name, value]) => {
    const originalValue = subUserDetails[name];

    if (value !== originalValue && Array.isArray(value)) {
      const changeToPush = {
        group: propertyCategories[name],
        name
      };

      // The elements that will be disabled are those that were already in originalValues but now are not in values.
      const elementsToDisabled = value.filter(val => !val.access && originalValue.some(org => org.id === val.id));

      // The elements that will be enabled are all those that are now in values and were not previously in originalValues.
      const elementsToEnabled = value.filter(val => val.access && originalValue.every(org => org.id !== val.id));

      const valueToPush = [...elementsToDisabled, ...elementsToEnabled];

      if (valueToPush.length) {
        changes = valueToPush.reduce((acc, val) => [...acc, { ...changeToPush, value: val }], changes);
      }
    }

    if (value !== originalValue && (typeof value === 'boolean' || typeof value !== 'object')) {
      changes = [
        ...changes,
        {
          group: propertyCategories[name],
          name,
          value
        }
      ];
    }

    return changes;
  }, []);
};

export const hasPropertyValidationErrors = (state: State): boolean => state.hasPropertyValidationErrors;

export const canApplyPropertyChanges = (state: State): boolean =>
  !isEmpty(getPropertyChanges(state)) && !state.hasPropertyValidationErrors;

export const getSubUserPropertiesError = (state: State): ServerError => state.subUserPropertiesError;
