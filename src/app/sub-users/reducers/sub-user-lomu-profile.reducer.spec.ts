import { ServerError } from '~app/shared/errors';
import { SubUsersLomuProfileActions } from '~app/sub-users/actions';
import { getTestLomuSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import { initialState, reducer, State } from '~app/sub-users/reducers/sub-user-lomu-profile.reducer';

describe('subUserLomuProfileReducer', () => {
  it('SubUsersProfileActions.requestSubUserLomu should set bspId and loading to true', () => {
    const action = SubUsersLomuProfileActions.requestSubUserLomu({ bspId: 1234 });
    const state = reducer({} as State, action);
    expect(state.bspId).toEqual(1234);
    expect(state.loading).toBeTruthy();
  });

  it(`Action requestSubUserLomuSuccess should set subUserLomu and loading to false`, () => {
    const action = SubUsersLomuProfileActions.requestSubUserLomuSuccess({ subUserLomu: getTestLomuSubUser() });
    const state = reducer({ loading: true, subUserLomu: null } as State, action);
    expect(state.loading).toBeFalsy();
    expect(state.subUserLomu).toEqual(getTestLomuSubUser());
  });

  it('SubUsersProfileActions.requestSubUserError should set subUserRequestError', () => {
    const action = SubUsersLomuProfileActions.requestSubUserLomuError({ error: {} as ServerError });
    const state = reducer({ ...initialState }, action);
    expect(state).toEqual({ ...initialState, subUserLomuRequestError: {} as ServerError });
  });
});
