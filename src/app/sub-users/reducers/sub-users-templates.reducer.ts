import { Action, createReducer, on } from '@ngrx/store';

import { SubUsersApiActions, SubUsersTemplatesActions } from '~app/sub-users/actions';
import {
  SubUser,
  SubUserEditTemplateModel,
  SubUserTemplate,
  SubUserTemplateViewModel
} from '~app/sub-users/shared/sub-users.models';

export const subUsersTemplatesFeatureKey = 'subUsersTemplates';

export interface State {
  originalTemplatesForm: { [key: string]: SubUserTemplate };
  modifiedTemplatesForm: { [key: string]: SubUserTemplate };
  selectedSubUsers: SubUserTemplateViewModel[];
  loading: boolean;
  shouldContinueNavigation: boolean;
}

export const initialState: State = {
  originalTemplatesForm: {},
  modifiedTemplatesForm: {},
  selectedSubUsers: [],
  loading: false,
  shouldContinueNavigation: true
};

const subUsersTemplatesReducer = createReducer(
  initialState,
  on(SubUsersTemplatesActions.editUsersTemplate, (state, { selectedSubUsers }) => ({
    ...state,
    selectedSubUsers
  })),
  on(SubUsersTemplatesActions.saveOriginalTemplatesForm, (state, { originalTemplatesForm }) => ({
    ...state,
    originalTemplatesForm
  })),
  on(SubUsersTemplatesActions.saveModifiedTemplatesForm, (state, { modifiedTemplatesForm }) => ({
    ...state,
    modifiedTemplatesForm
  })),
  on(SubUsersTemplatesActions.subUsersTemplatesOpenUnsavedChangesDialog, state => ({
    ...state,
    shouldContinueNavigation: false
  })),
  on(SubUsersTemplatesActions.discardSubUsersTemplatesChanges, state => ({
    ...state,
    shouldContinueNavigation: true
  })),
  on(SubUsersApiActions.applySubUserTemplatesChanges, state => ({
    ...state,
    loading: true
  })),
  on(SubUsersApiActions.updateSubUsersTemplatesSuccess, (state, { updatedSubUsersTemplatesData }) => ({
    ...state,
    selectedSubUsers: updateSubUsersTemplates(state.selectedSubUsers, updatedSubUsersTemplatesData),
    loading: false,
    shouldContinueNavigation: true
  })),
  on(SubUsersApiActions.updateSubUsersTemplatesError, state => ({
    ...state,
    loading: false
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return subUsersTemplatesReducer(state, action);
}

export const mapSelectedRowsToViewModel = (selectedUsers: SubUser[]) =>
  selectedUsers.map(user => ({
    id: user.id,
    template: user.template,
    portalEmail: user.portalEmail,
    name: user.name
  }));

export function getModifiedSubUsers(
  originalTemplatesForm: { [key: string]: SubUserTemplate },
  modifiedTemplatesForm: { [key: string]: SubUserTemplate },
  selectedSubUsers: SubUserTemplateViewModel[]
): SubUserTemplateViewModel[] {
  const modifiedIds = [];
  for (const key in originalTemplatesForm) {
    if (
      key !== 'bulkEfficient' &&
      key !== 'bulkStreamlined' &&
      originalTemplatesForm[key] !== modifiedTemplatesForm[key]
    ) {
      modifiedIds.push(key);
    }
  }

  return selectedSubUsers
    .filter(user => modifiedIds.includes(user.id.toString()))
    .map(user => ({ ...user, template: modifiedTemplatesForm[user.id] }));
}

export function updateSubUsersTemplates(
  subUsers: SubUserTemplateViewModel[],
  updatedSubUsers: SubUserEditTemplateModel[]
): SubUserTemplateViewModel[] {
  return subUsers.map(subUser => {
    const updatedItem = updatedSubUsers.find(updatedUser => updatedUser.userId === subUser.id);

    return {
      ...subUser,
      template: updatedItem?.template ?? subUser.template
    };
  });
}

export const getLoading = (state: State) => state.loading;

export const getTemplatesChanges = (state: State) =>
  getModifiedSubUsers(state.originalTemplatesForm, state.modifiedTemplatesForm, state.selectedSubUsers);
