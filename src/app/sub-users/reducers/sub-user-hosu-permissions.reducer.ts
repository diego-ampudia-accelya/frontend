import { Action, createReducer, on } from '@ngrx/store';
import { chain } from 'lodash';

import { UserPermission } from '../shared/permissions.models';
import { SubUser } from '../shared/sub-users.models';
import { ChangeModel } from '~app/shared/components';
import { SelectionList, SelectionListItem } from '~app/shared/components/selection-list/selection-list.models';
import { ServerError } from '~app/shared/errors';
import { BspDto } from '~app/shared/models/bsp.model';
import { SubUserHosuPermissionsActions } from '~app/sub-users/actions';

export const subUserHosuPermissionsFeatureKey = 'subUserHosuPermissions';

export interface State {
  loading: boolean;
  requestError: ServerError;
  bspsFromHosuSelection: BspDto[];
  selectedHosus: SubUser[];
  selectedHosuBsps: BspDto[];
  hosuPermissionsFromBspSelection: {
    original: SelectionList[];
    modified: SelectionList[];
    enabled: boolean;
    loading: boolean;
    hasError: boolean;
  };
}

export const initialState: State = {
  loading: false,
  requestError: null,
  bspsFromHosuSelection: [],
  selectedHosus: [],
  selectedHosuBsps: [],
  hosuPermissionsFromBspSelection: {
    original: [],
    modified: [],
    enabled: true,
    loading: false,
    hasError: false
  }
};

const showSubUserHosuPermissionsDialogReducer = createReducer(
  initialState,
  on(SubUserHosuPermissionsActions.fetchBspsFromHosuSelection, () => initialState),
  on(SubUserHosuPermissionsActions.fetchBspsFromHosuSelectionSuccess, (state, { bsps }) => ({
    ...state,
    bspsFromHosuSelection: bsps
  })),
  on(SubUserHosuPermissionsActions.updateSelectedHosus, (state, { selectedHosus }) => ({
    ...state,
    selectedHosus
  })),
  on(SubUserHosuPermissionsActions.updateSelectedHosuBsps, (state, { selectedHosuBsps }) => ({
    ...state,
    selectedHosuBsps
  })),
  on(SubUserHosuPermissionsActions.requestHosusPermissionsByBsps, state => ({
    ...state,
    hosuPermissionsFromBspSelection: {
      ...state.hosuPermissionsFromBspSelection,
      loading: true
    }
  })),
  on(SubUserHosuPermissionsActions.requestHosusPermissionsByBspsSuccess, (state, { permissionsResponse }) => {
    const original = mapPermissionsToSelectionList(permissionsResponse);

    return {
      ...state,
      hosuPermissionsFromBspSelection: {
        ...state.hosuPermissionsFromBspSelection,
        original,
        modified: original,
        loading: false
      }
    };
  }),
  on(SubUserHosuPermissionsActions.applyChangesSuccess, state => ({
    ...state,
    hosuPermissionsFromBspSelection: {
      ...state.hosuPermissionsFromBspSelection,
      loading: false
    }
  })),
  on(SubUserHosuPermissionsActions.requestHosusPermissionsByBspsError, () => ({
    ...initialState,
    hosuPermissionsFromBspSelection: {
      ...initialState.hosuPermissionsFromBspSelection,
      hasError: true,
      loading: false
    }
  })),
  on(SubUserHosuPermissionsActions.change, (state, { modifications }) => ({
    ...state,
    hosuPermissionsFromBspSelection: {
      ...state.hosuPermissionsFromBspSelection,
      modified: modifications
    }
  })),
  on(SubUserHosuPermissionsActions.updateEnabled, (state, { enabled }) => ({
    ...state,
    hosuPermissionsFromBspSelection: {
      ...state.hosuPermissionsFromBspSelection,
      enabled
    }
  })),
  on(SubUserHosuPermissionsActions.applyChanges, state => ({
    ...state,
    hosuPermissionsFromBspSelection: {
      ...state.hosuPermissionsFromBspSelection,
      loading: true
    }
  })),
  on(SubUserHosuPermissionsActions.applyChangesError, state => ({
    ...state,
    hosuPermissionsFromBspSelection: {
      ...state.hosuPermissionsFromBspSelection,
      loading: false
    }
  })),
  on(SubUserHosuPermissionsActions.discardChanges, state => ({
    ...state,
    hosuPermissionsFromBspSelection: {
      ...state.hosuPermissionsFromBspSelection,
      original: state.hosuPermissionsFromBspSelection.original,
      modified: state.hosuPermissionsFromBspSelection.original
    }
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return showSubUserHosuPermissionsDialogReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;

export const getRequestError = (state: State): ServerError => state.requestError;

export const getBspsFromHosuSelection = (state: State) => state.bspsFromHosuSelection;

export const getSelectedHosus = (state: State) => state.selectedHosus;

export const getSelectedHosuBsps = (state: State) => state.selectedHosuBsps;

export const getOriginal = (state: State) => state.hosuPermissionsFromBspSelection.original;

export const getModified = (state: State) => state.hosuPermissionsFromBspSelection.modified;

export const getPermissionsLoading = (state: State) => state.hosuPermissionsFromBspSelection.loading;

export const getEnabled = (state: State) => state.hosuPermissionsFromBspSelection.enabled;

export const hasChanges = (state: State) =>
  state.hosuPermissionsFromBspSelection.modified.some(permissionCategory => permissionCategory.hasChanged);

export function getChanges(state: State): ChangeModel[] {
  return state.hosuPermissionsFromBspSelection.modified
    .filter(catgory => catgory.hasChanged)
    .map(catgory => ({ ...catgory, items: catgory.items.filter(item => item.hasChanged) }))
    .reduce((changes, item) => {
      item.items.forEach((listItem: SelectionListItem) => {
        changes.push({
          group: item.name,
          name: listItem.name,
          value: String(listItem.checked),
          originalValue: listItem.checked
        });
      });

      return changes;
    }, []);
}

function mapPermissionsToSelectionList(permissions: UserPermission[]): SelectionList[] {
  return chain(permissions)
    .map(permission => ({
      name: permission.name,
      checked: false,
      value: permission,
      hasChanged: false,
      disabled: false
    }))
    .groupBy('value.catgory.name')
    .map((items, name) => {
      const disabled = items.every(item => item.disabled);

      return {
        name,
        disabled,
        items,
        tooltip: disabled ? 'subUsers.profile.permissionsTab.disabledTooltip' : ''
      };
    })
    .value();
}
