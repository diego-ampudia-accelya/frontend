import { Action, createReducer, on } from '@ngrx/store';
import { chain } from 'lodash';

import { ChangeModel } from '~app/shared/components';
import { SelectionList, SelectionListItem } from '~app/shared/components/selection-list/selection-list.models';
import { SubUsersLomuProfilePermissionsActions } from '~app/sub-users/actions';
import { UserPermission, UserPermissionLomu } from '~app/sub-users/shared/permissions.models';
import { SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

export const featureKey = 'subUserLomuProfilePermissions';

export interface State {
  userId: number;
  original: SelectionList[];
  modified: SelectionList[];
  loading: boolean;
  hasError: boolean;
}

export const initialState: State = {
  userId: null,
  original: [],
  modified: [],
  loading: false,
  hasError: false
};

const subUserLomuProfilePermissionsReducer = createReducer(
  initialState,
  on(SubUsersLomuProfilePermissionsActions.requestLomuPermissions, state => ({
    ...state,
    loading: true
  })),
  on(SubUsersLomuProfilePermissionsActions.requestLomuPermissionsSuccess, (state, { permissionsResponse }) => {
    const original = mapPermissionsToSelectionList(permissionsResponse[0].permissions);

    return {
      ...state,
      original,
      userId: permissionsResponse[0].idUser,
      modified: original,
      loading: false
    };
  }),
  on(SubUsersLomuProfilePermissionsActions.applyChangesSuccess, (state, { permissions }) => {
    const original = mapPermissionsToSelectionList(permissions);

    return {
      ...state,
      original,
      modified: original,
      loading: false
    };
  }),
  on(SubUsersLomuProfilePermissionsActions.requestLomuPermissionsError, () => ({
    ...initialState,
    hasError: true,
    loading: false
  })),
  on(SubUsersLomuProfilePermissionsActions.change, (state, { modifications }) => ({
    ...state,
    modified: modifications
  })),
  on(SubUsersLomuProfilePermissionsActions.applyChanges, state => ({ ...state, loading: true })),
  on(SubUsersLomuProfilePermissionsActions.applyChangesError, state => ({ ...state, loading: false })),
  on(SubUsersLomuProfilePermissionsActions.discardChanges, state => ({
    ...state,
    original: [...state.original],
    modified: state.original
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return subUserLomuProfilePermissionsReducer(state, action);
}

export const getOriginal = (state: State) => state.original;

export const getModified = (state: State) => state.modified;

export const getLoading = (state: State) => state.loading;

export const getLomuUserId = (state: State) => state.userId;

export const hasChanges = (state: State) => state.modified.some(permissionCategory => permissionCategory.hasChanged);

export function getChanges(state: State): ChangeModel[] {
  return state.modified
    .filter(catgory => catgory.hasChanged)
    .map(catgory => ({ ...catgory, items: catgory.items.filter(item => item.hasChanged) }))
    .reduce((changes, item) => {
      item.items.forEach((listItem: SelectionListItem) => {
        changes.push({
          group: item.name,
          name: listItem.name,
          value: String(listItem.checked),
          originalValue: listItem.checked
        });
      });

      return changes;
    }, []);
}

export function mapSelectionListToPermissions(selectionListItems: SelectionList[]): UserPermission[] {
  return chain(selectionListItems)
    .map(catgory => catgory.items)
    .flatten()
    .map(item => ({ ...item.value, enabled: item.checked }))
    .value();
}

function mapPermissionsToSelectionList(permissions: UserPermissionLomu[]): SelectionList[] {
  const isBasicUser = true;

  return chain(permissions)
    .map(permission => {
      const disabled = isBasicUser && permission.template === SubUserTemplate.Streamlined;

      return {
        name: permission.name,
        checked: permission.enabled,
        value: permission,
        hasChanged: false,
        disabled
      };
    })
    .groupBy('value.catgory.name')
    .map((items, name) => {
      const disabled = items.every(item => item.disabled);

      return {
        name,
        disabled,
        items,
        tooltip: disabled ? 'subUsers.profile.permissionsTab.disabledTooltip' : ''
      };
    })
    .value();
}
