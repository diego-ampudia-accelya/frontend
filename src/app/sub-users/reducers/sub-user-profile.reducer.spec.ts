import { ServerError } from '~app/shared/errors';
import { toSubUserUpdateModel } from '~app/shared/helpers/user.helper';
import { SubUsersProfileActions, SubUserStatusActions } from '~app/sub-users/actions';
import { getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import {
  getPropertyChanges,
  getSubUserPortalEmail,
  initialState,
  reducer,
  State
} from '~app/sub-users/reducers/sub-user-profile.reducer';

describe('subUserProfileReducer', () => {
  it('SubUsersProfileActions.requestSubUser should reset state', () => {
    const action = SubUsersProfileActions.requestSubUser;
    const state = reducer({} as State, action);
    expect(state).toEqual(initialState);
  });

  it('SubUsersProfileActions.updateSubUserProperties should set loading to true', () => {
    const action = SubUsersProfileActions.updateSubUserProperties({ data: null });
    const state = reducer({ loading: false } as State, action);
    expect(state.loading).toBeTruthy();
  });

  [
    SubUsersProfileActions.requestSubUserSuccess({ subUser: getTestSubUser() }),
    SubUserStatusActions.deactivateSubUserSuccess({ subUser: getTestSubUser() }),
    SubUserStatusActions.reactivateSubUserSuccess({ subUser: getTestSubUser() })
  ].forEach(action => {
    it(`${action.type} should set subUser and loading to false`, () => {
      const state = reducer({ loading: true, subUser: null } as State, action);
      expect(state.loading).toBeFalsy();
      expect(state.subUser).toEqual(getTestSubUser());
    });
  });

  it(`SubUsersProfileActions.isCreatingHosuSubUser should set isCreatingHosuMode to true`, () => {
    const action = SubUsersProfileActions.isCreatingHosuSubUser();
    const state = reducer({ isCreatingHosuMode: false } as State, action);
    expect(state.isCreatingHosuMode).toBeTruthy();
  });

  it('SubUsersProfileActions.updateSubUserPropertiesSuccess should set subUser and flags', () => {
    const action = SubUsersProfileActions.updateSubUserPropertiesSuccess({ subUser: getTestSubUser() });
    const state = reducer({} as State, action);
    expect(state).toEqual({
      loading: false,
      deactivating: false,
      propertiesFormValue: null,
      subUserPropertiesError: null,
      subUser: getTestSubUser()
    } as State);
  });

  it('SubUsersProfileActions.updateSubUserPropertiesError should set loading and deactivating to false', () => {
    const action = SubUsersProfileActions.updateSubUserPropertiesError({ error: {} as ServerError });
    const state = reducer({} as State, action);

    expect(state).toEqual({
      loading: false,
      deactivating: false,
      subUserPropertiesError: {} as ServerError
    } as State);
  });

  it('SubUsersProfileActions.requestSubUserError should set subUserRequestError', () => {
    const action = SubUsersProfileActions.requestSubUserError({ error: {} as ServerError });
    const state = reducer({ ...initialState }, action);
    expect(state).toEqual({ ...initialState, subUserRequestError: {} as ServerError });
  });

  it('SubUsersProfileActions.propertiesFormValueChange should set propertiesFormValue and hasPropertyValidationErrors', () => {
    const subUserUpdateData = toSubUserUpdateModel(getTestSubUser());
    const action = SubUsersProfileActions.propertiesFormValueChange({
      formValue: subUserUpdateData,
      isFormValid: true
    });
    const state = reducer({ ...initialState }, action);
    expect(state.propertiesFormValue).toEqual(subUserUpdateData);
    expect(state.hasPropertyValidationErrors).toBeFalsy();
  });

  it('SubUsersProfileActions.propertiesDeactivationRequest should set deactivating to true', () => {
    const action = SubUsersProfileActions.propertiesDeactivationRequest();
    const state = reducer({ deactivating: false } as State, action);
    expect(state.deactivating).toBeTruthy();
  });

  it('SubUsersProfileActions.discardPropertyChanges should set deactivating to false', () => {
    const action = SubUsersProfileActions.discardPropertyChanges();
    const state = reducer({ deactivating: true } as State, action);
    expect(state.deactivating).toBeFalsy();
  });

  describe('getSubUserPortalEmail', () => {
    it('should return sub-user portalEmail property', () => {
      const email = getSubUserPortalEmail({
        ...initialState,
        subUser: getTestSubUser()
      });

      expect(email).toBe('frank_right@continental.airlines.com');
    });

    it('should return empty string, when subUser has no value', () => {
      const email = getSubUserPortalEmail({ ...initialState });

      expect(email).toBe('');
    });
  });

  it('getPropertyChanges should covert difference to ChangeModel array', () => {
    const subUser = getTestSubUser();
    const changes = getPropertyChanges({
      ...initialState,
      subUser,
      propertiesFormValue: {
        ...toSubUserUpdateModel(subUser),
        portalEmail: 'test_change@test.com'
      }
    });

    expect(changes).toEqual([
      {
        group: 'contactDetails',
        name: 'portalEmail',
        value: 'test_change@test.com'
      }
    ]);
  });

  it('SubUsersProfileActions.createHosuSubUserError should set loading to false and set error', () => {
    const action = SubUsersProfileActions.createHosuSubUserError({ error: null });
    const state = reducer({ loading: true } as State, action);

    expect(state.loading).toBeFalsy();
  });

  it('SubUsersProfileActions.createHosuSubUser should set loading to true', () => {
    const action = SubUsersProfileActions.createHosuSubUser();
    const state = reducer({ loading: false } as State, action);

    expect(state.loading).toBeTruthy();
  });

  it('SubUsersProfileActions.cancelPropertyChanges should set deactivating to false', () => {
    const action = SubUsersProfileActions.cancelPropertyChanges();
    const state = reducer({ deactivating: true } as State, action);

    expect(state.deactivating).toBeFalsy();
  });

  it('SubUsersProfileActions.createHosuSubUserSuccess should set initialState', () => {
    const subUser = getTestSubUser();
    const action = SubUsersProfileActions.createHosuSubUserSuccess({ subUser });
    const state = reducer({ loading: true } as State, action);

    expect(state.loading).toBeFalsy();
    expect(state.subUser).toBeNull();
  });
});
