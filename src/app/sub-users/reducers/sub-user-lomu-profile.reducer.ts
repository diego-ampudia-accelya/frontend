import { Action, createReducer, on } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { SubUsersLomuProfileActions } from '~app/sub-users/actions';
import { SubUserLomu } from '~app/sub-users/shared/sub-users.models';

export const subUserLomuProfileFeatureKey = 'subUserLomuProfile';

export interface State {
  loading: boolean;
  subUserLomu: SubUserLomu;
  subUserLomuRequestError: ServerError;
  subUserLomuPropertiesError: ServerError;
  bspId: number;
}

export const initialState: State = {
  loading: false,
  subUserLomu: null,
  subUserLomuRequestError: null,
  subUserLomuPropertiesError: null,
  bspId: null
};

const subUserLomuProfileReducer = createReducer(
  initialState,
  on(SubUsersLomuProfileActions.requestSubUserLomu, (state, { bspId }) => ({
    ...state,
    loading: true,
    bspId
  })),
  on(SubUsersLomuProfileActions.requestSubUserLomuSuccess, (state, { subUserLomu }) => ({
    ...state,
    subUserLomu,
    loading: false
  })),
  on(SubUsersLomuProfileActions.requestSubUserLomuError, (_state, { error }) => ({
    ...initialState,
    subUserLomuRequestError: error
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return subUserLomuProfileReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;

export const getSubUserLomu = (state: State): SubUserLomu => state.subUserLomu;

export const getSubUserLomuBspId = (state: State): number => state.bspId;

export const getSubUserLomuRequestError = (state: State): ServerError => state.subUserLomuRequestError;
