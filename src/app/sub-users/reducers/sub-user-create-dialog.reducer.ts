import { Action, createReducer, on } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { SubUserCreateDialogActions } from '~app/sub-users/actions';

export const createSubUserFeatureKey = 'createSubUser';

export interface State {
  loading: boolean;
  requestError: ServerError;
}

export const initialState: State = {
  loading: false,
  requestError: null
};

const createSubUserDialogReducer = createReducer(
  initialState,
  on(
    SubUserCreateDialogActions.openSubUserCreateDialog,
    SubUserCreateDialogActions.createSubUserSuccess,
    () => initialState
  ),
  on(SubUserCreateDialogActions.createSubUserError, (state, { error }) => ({
    ...state,
    loading: false,
    requestError: error
  })),
  on(SubUserCreateDialogActions.createSubUser, state => ({ ...state, loading: true }))
);

export function reducer(state: State | undefined, action: Action) {
  return createSubUserDialogReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;
export const getRequestError = (state: State): ServerError => state.requestError;
