import { SubUsersLomuProfilePermissionsActions } from '~app/sub-users/actions';
import { getTestUserLomuPermissions } from '~app/sub-users/mocks/sub-users.test.mocks';
import { initialState, reducer, State } from '~app/sub-users/reducers/sub-user-lomu-profile-permissions.reducer';

describe('subUserLomuProfilePermissionsReducer', () => {
  it('SubUsersLomuProfilePermissionsActions.requestLomuPermissions should set loading to true', () => {
    const action = SubUsersLomuProfilePermissionsActions.requestLomuPermissions;
    const state = reducer({} as State, action);
    expect(state.loading).toBeTruthy();
  });

  it(`Action requestLomuPermissionsSuccess should set userId and loading to false`, () => {
    const action = SubUsersLomuProfilePermissionsActions.requestLomuPermissionsSuccess({
      permissionsResponse: getTestUserLomuPermissions()
    });
    const state = reducer({ loading: true } as State, action);
    expect(state.loading).toBeFalsy();
    expect(state.userId).toEqual(1234);
  });

  it('SubUsersLomuProfilePermissionsActions.requestLomuPermissionsError should set hasError', () => {
    const action = SubUsersLomuProfilePermissionsActions.requestLomuPermissionsError();
    const state = reducer({ ...initialState } as State, action);
    expect(state.hasError).toBeTruthy();
  });
});
