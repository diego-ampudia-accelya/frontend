import { SubUsersListActions, SubUserStatusActions } from '~app/sub-users/actions';
import { SubUsersListRequiredFilter } from '~app/sub-users/containers/sub-users-list/sub-users-list-filter.model';
import { getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import {
  getSelectedRows,
  getSelectedRowsCount,
  initialState,
  reducer
} from '~app/sub-users/reducers/sub-users-list.reducer';

describe('SubUsersList Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const state = reducer(initialState, action);

      expect(state).toBe(initialState);
    });
  });

  it('SubUsersListActions.changeRequiredFilter should set selectedBsp and requiredFilter', () => {
    const requiredFilter: SubUsersListRequiredFilter = {
      bsp: {
        id: 123,
        name: 'Spain',
        isoCountryCode: 'ES',
        effectiveFrom: ''
      },
      userTypeDetails: null
    };

    const action = SubUsersListActions.changeRequiredFilter({ requiredFilter });

    const state = reducer(initialState, action);

    expect(state.selectedBsp).toEqual(requiredFilter.bsp);
    expect(state.requiredFilter).toEqual(requiredFilter);
  });

  it('SubUsersListActions.changeQuery should set selectedBsp and requiredFilter', () => {
    const query = {
      filterBy: null,
      paginateBy: { size: 20, page: 0, totalElements: 0 },
      sortBy: []
    };

    const action = SubUsersListActions.changeQuery({ query });

    const state = reducer(initialState, action);

    expect(state.query.filterBy).toBe(null);
    expect(state.query.paginateBy.size).toBe(20);
  });

  it('SubUsersListActions.changeQuery should set paginateBy page', () => {
    const query = {
      filterBy: null,
      paginateBy: { size: 20, page: 3, totalElements: 0 },
      sortBy: []
    };

    const action = SubUsersListActions.changeQuery({ query });

    const state = reducer(initialState, action);

    expect(state.query.paginateBy.page).toBe(3);
  });

  it('SubUsersListActions.selectBsp should set selectedBsp', () => {
    const requiredFilter: SubUsersListRequiredFilter = {
      bsp: {
        id: 123,
        name: 'Spain',
        isoCountryCode: 'ES',
        effectiveFrom: ''
      },
      userTypeDetails: null
    };

    const action = SubUsersListActions.selectBsp();

    const state = reducer(
      {
        ...initialState,
        requiredFilter
      },
      action
    );

    expect(state.selectedBsp).toEqual(requiredFilter.bsp);
  });

  it('SubUsersListActions.loadData should set loading to true', () => {
    const action = SubUsersListActions.loadData();

    const state = reducer(initialState, action);

    expect(state.loading).toBeTruthy();
  });

  it('SubUsersListActions.loadDataSuccess should set loading to false and set data and paginateBy', () => {
    const data = {
      records: [getTestSubUser()],
      pageSize: 20,
      total: 1,
      totalPages: 1,
      pageNumber: 0
    };

    const action = SubUsersListActions.loadDataSuccess({ data });

    const state = reducer(initialState, action);

    expect(state.loading).toBeFalsy();
    expect(state.data).toEqual(data.records);
    expect(state.query.paginateBy).toEqual({
      page: 0,
      size: 20,
      totalElements: 1
    });
  });

  it('SubUsersListActions.loadDataError should set loading to false and set data to empty array', () => {
    const action = SubUsersListActions.loadDataError({ error: null });

    const state = reducer({ ...initialState, data: [getTestSubUser()] }, action);

    expect(state.loading).toBeFalsy();
    expect(state.data).toEqual([]);
  });

  it('SubUsersListActions.changeSelectedRows should set loading to false and set data', () => {
    const testSubUser = getTestSubUser();
    const action = SubUsersListActions.changeSelectedRows({ data: [testSubUser] });

    const state = reducer(initialState, action);

    expect(state.loading).toBeFalsy();
    expect(state.data).toEqual([testSubUser]);
  });

  it('SubUsersListActions.clearSelectedRows should reset isSelected property of all data elements', () => {
    const testSubUser = { ...getTestSubUser(), isSelected: true };
    const action = SubUsersListActions.clearSelectedRows();

    const state = reducer({ ...initialState, data: [testSubUser] }, action);

    expect(state.loading).toBeFalsy();
    expect(state.data[0].isSelected).toBeFalsy();
  });

  [
    SubUserStatusActions.deactivateSubUserSuccess({ subUser: getTestSubUser() }),
    SubUserStatusActions.reactivateSubUserSuccess({ subUser: getTestSubUser() })
  ].forEach(action => {
    it(`${action.type} should update the subUser by having the same id`, () => {
      const state = reducer(
        {
          ...initialState,
          data: [
            { ...getTestSubUser(), name: '' },
            { ...getTestSubUser(), name: '', id: 123 }
          ]
        },
        action
      );

      expect(state.data[0].name).toBe('Frank Right');
      expect(state.data[1].name).toBe('');
    });
  });

  it('getSelectedRows should return only the selected rows from state', () => {
    const selectedRow = { ...getTestSubUser(), isSelected: true };
    const selectedRows = getSelectedRows({
      ...initialState,
      data: [selectedRow, { ...getTestSubUser(), isSelected: false, id: 123 }]
    });

    expect(selectedRows).toEqual([selectedRow]);
  });

  it('getSelectedRowsCount should return the length of only the selected rows from state', () => {
    const count = getSelectedRowsCount({
      ...initialState,
      data: [
        { ...getTestSubUser(), isSelected: true },
        { ...getTestSubUser(), isSelected: false, id: 123 }
      ]
    });

    expect(count).toBe(1);
  });
});
