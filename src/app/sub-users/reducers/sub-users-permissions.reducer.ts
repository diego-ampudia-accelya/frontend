import { Action, createReducer, on } from '@ngrx/store';
import { chain, cloneDeep, find, forEach, forOwn, groupBy } from 'lodash';

import { CheckboxDesign } from '~app/shared/components';
import { SubUsersPermissionsActions } from '~app/sub-users/actions';
import {
  SubUserPermissionsOrigin,
  SubUsersChangedPermissions,
  SubUsersPermissionsOriginModel,
  SubUsersPermissionsViewModel,
  SubUserTemplate
} from '~app/sub-users/shared/sub-users.models';

export const subUsersPermissionsFeatureKey = 'subUsersPermissions';

export interface State {
  isProcessing: boolean;
  shouldContinueNavigation?: boolean;
  loggedUserTemplate: SubUserTemplate;
  permissions: {
    origin: SubUsersPermissionsOriginModel;
    viewModel: SubUsersPermissionsViewModel[];
    initialViewModel: SubUsersPermissionsViewModel[];
  };
}

export const initialState: State = {
  isProcessing: false,
  shouldContinueNavigation: true,
  loggedUserTemplate: SubUserTemplate.Streamlined, // todo: someday value should come from the me endpoint
  permissions: {
    origin: {},
    viewModel: [],
    initialViewModel: []
  }
};

const subUsersPermissionsReducer = createReducer(
  initialState,
  on(SubUsersPermissionsActions.fetchSubUserAndPermissions, state => ({
    ...state,
    isProcessing: true
  })),
  on(SubUsersPermissionsActions.openUnsavedSubUsersPermissionsChangesDialog, state => ({
    ...state,
    shouldContinueNavigation: false
  })),
  on(SubUsersPermissionsActions.fetchSubUserAndPermissionsSuccess, (state, { subUsers }) => {
    const vm = createSubUsersPermissionsViewModel(state, subUsers);
    const origin = createSubUsersPermissionsOrigin(subUsers);
    const vmState = setCheckboxesState(vm, origin);

    return {
      ...state,
      isProcessing: false,
      permissions: {
        origin,
        viewModel: vmState,
        initialViewModel: vmState
      }
    };
  }),
  on(SubUsersPermissionsActions.fetchSubUserAndPermissionsError, state => ({
    ...state,
    isProcessing: false
  })),
  on(SubUsersPermissionsActions.expandAll, SubUsersPermissionsActions.collapseAll, (state, { value }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    vm.forEach(category => (category.expanded = value));

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: vm,
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.selectAll, SubUsersPermissionsActions.deselectAll, (state, { value }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    const origin = state.permissions.origin;

    vm.forEach(category => {
      category.checked = value;

      category.categoryGroups.forEach(group => {
        group.checked = value;
      });

      category.permissions.forEach(permission => {
        permission.checked = value;

        permission.users.forEach(user => {
          if (!user.disabled) {
            user.permission.enabled = value;
            user.checked = value;
          }
        });
      });
    });

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: setCheckboxesState(vm, origin),
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.togglePanel, (state, { categoryId }) => {
    const vm = cloneDeep(state.permissions.viewModel);

    vm.forEach(category => {
      if (category.id === categoryId) {
        category.expanded = !category.expanded;
      }
    });

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: vm,
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.toggleByCategory, (state, { value, categoryId }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    const origin = state.permissions.origin;

    vm.forEach(category => {
      if (categoryId === category.id) {
        category.checked = value;

        category.categoryGroups.forEach(group => {
          group.checked = value;
        });

        category.permissions.forEach(permission => {
          permission.checked = value;

          permission.users.forEach(user => {
            if (!user.disabled) {
              user.permission.enabled = value;
              user.checked = value;
            }
          });
        });
      }
    });

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: setCheckboxesState(vm, origin),
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.toggleByPermission, (state, { value, permissionId }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    const origin = state.permissions.origin;

    vm.forEach(category => {
      category.permissions.forEach(permission => {
        if (permissionId === permission.id) {
          permission.checked = value;

          permission.users.forEach(user => {
            if (!user.disabled) {
              user.permission.enabled = value;
              user.checked = value;
            }
          });
        }
      });
    });

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: setCheckboxesState(vm, origin),
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.toggleByUser, (state, { value, userId }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    const origin = state.permissions.origin;
    vm.forEach(category => {
      category.categoryGroups.forEach(res => {
        if (res.userId === userId) {
          res.checked = value;
        }
      });
      category.permissions.forEach(permission => {
        permission.users.forEach(user => {
          if (user.id === userId && !user.disabled) {
            user.permission.enabled = value;
            user.checked = value;
          }
        });
      });
    });

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: setCheckboxesState(vm, origin),
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.toggleByUserAndCategory, (state, { value, group }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    const origin = state.permissions.origin;

    vm.forEach(category => {
      if (category.id === group.categoryId) {
        category.categoryGroups.forEach(res => {
          if (res.userId === group.userId) {
            res.checked = value;
          }
        });

        category.permissions.forEach(permission => {
          permission.users.forEach(user => {
            if (user.id === group.userId && !user.disabled) {
              user.permission.enabled = value;
              user.checked = value;
            }
          });
        });
      }
    });

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: setCheckboxesState(vm, origin),
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.togglePermission, (state, { value, user }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    const origin = state.permissions.origin;

    vm.forEach(category => {
      if (category.id === user.permission.category.id) {
        category.permissions.forEach(permission => {
          permission.users.forEach(res => {
            if (res.id === user.id && user.permission.id === res.permission.id) {
              res.permission.enabled = value;
              res.checked = value;
            }
          });
        });
      }
    });

    return {
      ...state,
      permissions: {
        origin: state.permissions.origin,
        viewModel: setCheckboxesState(vm, origin),
        initialViewModel: state.permissions.initialViewModel
      }
    };
  }),
  on(SubUsersPermissionsActions.discardSubUsersPermissionsChanges, state => {
    const vm = cloneDeep(state.permissions.initialViewModel);

    return {
      ...state,
      shouldContinueNavigation: true,
      permissions: {
        origin: state.permissions.origin,
        viewModel: vm,
        initialViewModel: vm
      }
    };
  }),
  on(SubUsersPermissionsActions.updateSubUsersPermissionsSuccess, (state, { users }) => {
    const vm = cloneDeep(state.permissions.viewModel);
    const origin = createSubUsersPermissionsOrigin(users);
    const vmState = setCheckboxesState(vm, origin);

    return {
      ...state,
      shouldContinueNavigation: true,
      permissions: {
        origin: {
          ...state.permissions.origin,
          ...origin
        },
        viewModel: vmState,
        initialViewModel: vmState
      }
    };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return subUsersPermissionsReducer(state, action);
}

export const isProcessing = (state: State): boolean => state.isProcessing;

export const allPermissionCategoriesExpanded = (state: State): boolean => {
  const vm = state.permissions.viewModel;

  return vm.every(category => category.expanded);
};

export const allSubUserPermissionsSelected = (state: State): { [key: number]: boolean } => {
  const vm = cloneDeep(state.permissions.viewModel);
  const groups = vm[0].categoryGroups;
  const userStatuses: { [key: number]: boolean } = {};

  groups.forEach(group => {
    const permissions = [];

    vm.forEach(category => {
      category.permissions.forEach(permission => {
        permission.users.forEach(res => {
          if (res.id === group.userId && !res.disabled) {
            permissions.push(res.permission.enabled);
          }
        });
      });
    });

    userStatuses[group.userId] = permissions.every(permission => permission);
  });

  return userStatuses;
};

export const allPermissionsSelected = (state: State): boolean => {
  const permissions = [];
  const vm = state.permissions.viewModel;

  vm.forEach(category => {
    category.permissions.forEach(permission => {
      permission.users.forEach(user => {
        if (!user.disabled) {
          permissions.push(user.checked);
        }
      });
    });
  });

  return permissions.every(permission => permission);
};

export const canApplyChanges = (state: State): boolean => {
  const vm = state.permissions.viewModel;

  return vm.some(category => category.hasChanges);
};

function createSubUsersPermissionsViewModel(
  state: State,
  subUsers: SubUserPermissionsOrigin[]
): SubUsersPermissionsViewModel[] {
  const vm = extractPermissionCategories(subUsers[0].permissions);

  return vm.map(category => {
    category.categoryGroups = subUsers.map(user => ({
      userId: user.id,
      categoryId: category.id,
      portalEmail: user.portalEmail,
      checked: false,
      hasChanges: false,
      checkboxDesign: CheckboxDesign.Normal
    }));

    category.permissions.map(permission => {
      subUsers.forEach(user => {
        const { id, portalEmail, name } = user;

        const userPermission = find(user.permissions, { id: permission.id });

        if (userPermission) {
          const isDisabled =
            (state.loggedUserTemplate === SubUserTemplate.Efficient &&
              userPermission.template === SubUserTemplate.Streamlined) ||
            (user.template === SubUserTemplate.Efficient && userPermission.template === SubUserTemplate.Streamlined);

          permission.users.push({
            id,
            portalEmail,
            name,
            disabled: isDisabled,
            hasChanged: false,
            checked: isDisabled ? false : userPermission.enabled,
            permission: userPermission
          });
        }
      });

      return permission;
    });

    return category;
  });
}

function extractPermissionCategories(permissions) {
  const categories = [];
  forOwn(groupBy(permissions, 'category.name'), (value, key) => {
    categories.push({
      id: value[0].category.id,
      expanded: false,
      name: key,
      disabled: false,
      categoryGroups: [],
      hasChanges: false,
      checked: false,
      permissions: value.map(permission => ({
        id: permission.id,
        name: permission.name,
        checked: false,
        hasChanges: false,
        disabled: false,
        users: []
      }))
    });
  });

  return categories;
}

function setCheckboxesState(
  vm: SubUsersPermissionsViewModel[],
  origin: SubUsersPermissionsOriginModel
): SubUsersPermissionsViewModel[] {
  vm.forEach(category => {
    category.permissions.forEach(permission => {
      permission.users.forEach(user => {
        if (origin[user.id]) {
          const original = origin[user.id][user.permission.category.id][user.permission.id];

          if (original !== user.permission.enabled) {
            user.hasChanged = true;
          }

          if (original === user.permission.enabled) {
            user.hasChanged = false;
          }
        }
      });
    });
  });

  const { usersPermissions, usersChanges, usersDisablePermissions } = getSubUserPermissionsChangeAndDisableState(vm);
  const { categoriesPermissions, categoriesChanges, categoriesDisablePermissions } =
    getCategoriesPermissionsChangeAndDisableState(vm);

  chain(vm)
    .forEach(category => {
      category.categoryGroups.forEach(group => {
        const permissionCheckbox = getCheckboxDesign(usersPermissions[group.userId][category.id]);

        group.checked = permissionCheckbox.checked;
        group.checkboxDesign = permissionCheckbox.checkboxDesign;

        group.hasChanges = usersChanges[group.userId][category.id].some(change => change);
        group.disabled = usersDisablePermissions[group.userId][category.id].every(disabled => disabled);

        if (group.disabled) {
          group.checked = false;
          group.checkboxDesign = CheckboxDesign.Normal;
        }
      });
    })
    .forEach(category => {
      let permissions = [];
      let changes = [];
      let disables = [];

      forEach(categoriesPermissions[category.id], res => {
        permissions = [...permissions, ...res];
      });

      forEach(categoriesChanges[category.id], res => {
        changes = [...changes, ...res];
      });

      forEach(categoriesDisablePermissions[category.id], res => {
        disables = [...disables, ...res];
      });

      const categoryCheckbox = getCheckboxDesign(permissions);
      category.checked = categoryCheckbox.checked;
      category.checkboxDesign = categoryCheckbox.checkboxDesign;
      category.hasChanges = changes.some(change => change);
      category.disabled = disables.every(disable => disable);

      if (category.disabled) {
        category.checked = false;
        category.checkboxDesign = CheckboxDesign.Normal;
      }

      category.permissions.forEach(permissionGroup => {
        const permissionCheckbox = getCheckboxDesign(categoriesPermissions[category.id][permissionGroup.id]);
        permissionGroup.hasChanges = categoriesChanges[category.id][permissionGroup.id].some(change => change);
        permissionGroup.disabled = categoriesDisablePermissions[category.id][permissionGroup.id].every(
          disabled => disabled
        );
        permissionGroup.checked = permissionCheckbox.checked;
        permissionGroup.checkboxDesign = permissionCheckbox.checkboxDesign;

        if (permissionGroup.disabled) {
          permissionGroup.checked = false;
          permissionGroup.checkboxDesign = CheckboxDesign.Normal;
        }
      });
    })
    .value();

  return vm;
}

function createSubUsersPermissionsOrigin(subUsers: SubUserPermissionsOrigin[]): SubUsersPermissionsOriginModel {
  const data = {};

  subUsers.forEach(user => {
    data[user.id] = {};

    user.permissions.forEach(permission => {
      data[user.id][permission.category.id] = {};
    });

    user.permissions.forEach(permission => {
      data[user.id][permission.category.id][permission.id] = permission.enabled;
    });
  });

  return data;
}

function getSubUserPermissionsChangeAndDisableState(vm: SubUsersPermissionsViewModel[]) {
  const usersPermissions = {};
  const usersChanges = {};
  const usersDisablePermissions = {};
  const groups = vm[0].categoryGroups || [];

  groups.forEach(group => {
    const userId = group.userId;

    usersPermissions[userId] = {};
    usersChanges[userId] = {};
    usersDisablePermissions[userId] = {};

    vm.forEach(category => {
      usersPermissions[userId][category.id] = [];
      usersChanges[userId][category.id] = [];
      usersDisablePermissions[userId][category.id] = [];
      let counter = 0;

      category.permissions.forEach(permission => {
        permission.users.forEach(user => {
          if (userId === user.id && !user.disabled) {
            usersPermissions[userId][category.id][counter] = user.permission.enabled;
            usersChanges[userId][category.id][counter] = user.hasChanged;
            usersDisablePermissions[userId][category.id][counter] = user.disabled;
            counter++;
          }
        });
      });
    });
  });

  return { usersPermissions, usersChanges, usersDisablePermissions };
}

function getCategoriesPermissionsChangeAndDisableState(vm: SubUsersPermissionsViewModel[]) {
  const categoriesPermissions = {};
  const categoriesDisablePermissions = {};
  const categoriesChanges = {};

  vm.forEach(category => {
    categoriesPermissions[category.id] = {};
    categoriesChanges[category.id] = {};
    categoriesDisablePermissions[category.id] = {};

    category.permissions.forEach(permission => {
      categoriesPermissions[category.id][permission.id] = [];
      categoriesChanges[category.id][permission.id] = [];
      categoriesDisablePermissions[category.id][permission.id] = [];

      let counter = 0;

      permission.users.forEach(user => {
        if (!user.disabled) {
          categoriesPermissions[category.id][permission.id][counter] = user.permission.enabled;
          categoriesChanges[category.id][permission.id][counter] = user.hasChanged;
          categoriesDisablePermissions[category.id][permission.id][counter] = user.disabled;
          counter++;
        }
      });
    });
  });

  return { categoriesPermissions, categoriesChanges, categoriesDisablePermissions };
}

function getCheckboxDesign(permissions): { checked: boolean; checkboxDesign: CheckboxDesign } {
  const checkedItemsCount = permissions.filter(item => item).length;
  let checked: boolean;
  let checkboxDesign: CheckboxDesign.Normal | CheckboxDesign.Minus;

  if (checkedItemsCount === permissions.length) {
    // All items are selected
    checked = true;
    checkboxDesign = CheckboxDesign.Normal;
  } else if (checkedItemsCount === 0) {
    // No items are checked
    checked = false;
    checkboxDesign = CheckboxDesign.Normal;
  } else {
    // Some items are selected
    checked = true;
    checkboxDesign = CheckboxDesign.Minus;
  }

  return {
    checked,
    checkboxDesign
  };
}

export const getChangedPermissions = (state: State): SubUsersChangedPermissions[] => {
  let users = getUsersAndPermissions(state.permissions.viewModel);
  users = users.filter(user => user.hasChanges);

  chain(users)
    .forEach(user => {
      user.permissionGroups = user.permissionGroups.filter(group => group.hasChanges);
    })
    .forEach(user => {
      user.permissionGroups.forEach(group => {
        group.permissions = group.permissions.filter(permission => permission.hasChanges);
      });
    })
    .value();

  return users;
};

function getCategoriesChanges(vm: SubUsersPermissionsViewModel[]): { [key: number]: boolean[] } {
  const data = {};
  const groups = vm.length > 0 ? vm[0].categoryGroups : [];

  groups.forEach(({ userId }) => {
    data[userId] = [];
    vm.forEach(category => {
      category.categoryGroups.forEach(group => {
        if (group.userId === userId) {
          data[userId].push(group.hasChanges);
        }
      });
    });
  });

  return data;
}

function getUsersAndPermissions(vm: SubUsersPermissionsViewModel[]): SubUsersChangedPermissions[] {
  const groups = vm.length > 0 ? vm[0].categoryGroups : [];
  const res = [];
  const userGroups = getCategoriesChanges(vm);

  groups.forEach((group, gi) => {
    res.push({
      id: group.userId,
      portalEmail: group.portalEmail,
      hasChanges: userGroups[group.userId].some(item => item),
      permissionGroups: []
    });

    vm.forEach((category, ci) => {
      const userCategory = category.categoryGroups.find(categoryGroup => categoryGroup.userId === group.userId);

      res[gi].permissionGroups.push({
        id: category.id,
        name: category.name,
        hasChanges: userCategory.hasChanges,
        permissions: []
      });

      category.permissions.forEach((permission, pi) => {
        const userPermission = permission.users.find(user => user.id === group.userId);

        res[gi].permissionGroups[ci].permissions.push({
          id: permission.id,
          name: permission.name,
          hasChanges: userPermission.hasChanged,
          user: {}
        });

        permission.users.forEach(user => {
          if (user.id === group.userId) {
            res[gi].permissionGroups[ci].permissions[pi].user = {
              id: user.id,
              name: user.name,
              portalEmail: user.portalEmail,
              permission: user.permission
            };
          }
        });
      });
    });
  });

  return res;
}

export const getModifiedSubUsersPermissions = (state: State): SubUserPermissionsOrigin[] => {
  let modifiedUsers = getUsersAndPermissions(state.permissions.viewModel);
  modifiedUsers = modifiedUsers.filter(user => user.hasChanges);

  const res = [];

  modifiedUsers.forEach((user, i) => {
    res.push({
      id: user.id,
      portalEmail: user.portalEmail,
      permissions: []
    });

    user.permissionGroups.forEach(group => {
      group.permissions.forEach(permission => {
        res[i].permissions.push(permission.user.permission);
      });
    });
  });

  return res;
};
