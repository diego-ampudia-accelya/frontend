import { Action, createReducer, on } from '@ngrx/store';
import { chain } from 'lodash';

import { ChangeModel } from '~app/shared/components';
import { SelectionList, SelectionListItem } from '~app/shared/components/selection-list/selection-list.models';
import { SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import { UserPermission } from '~app/sub-users/shared/permissions.models';
import { SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

export const featureKey = 'subUserProfilePermissions';

export interface State {
  userId: number;
  original: SelectionList[];
  modified: SelectionList[];
  loading: boolean;
  bspId: number;
}

export const initialState: State = {
  userId: null,
  original: [],
  modified: [],
  loading: false,
  bspId: null
};

const subUserProfilePermissionsReducer = createReducer(
  initialState,
  on(SubUsersProfilePermissionsActions.requestPermissions, () => ({ ...initialState, loading: true })),
  on(SubUsersProfilePermissionsActions.requestHosuPermissions, (state, { bspId }) => ({
    ...state,
    bspId,
    loading: true
  })),
  on(
    SubUsersProfilePermissionsActions.requestPermissionsSuccess,
    SubUsersProfilePermissionsActions.applyChangesSuccess,
    (state, { permissions, subUser }) => {
      const original = mapPermissionsToSelectionList(permissions, subUser.template);

      return {
        ...state,
        original,
        userId: subUser.id,
        modified: original,
        loading: false
      };
    }
  ),
  on(SubUsersProfilePermissionsActions.requestPermissionsError, () => ({
    ...initialState,
    hasError: true,
    loading: false
  })),
  on(SubUsersProfilePermissionsActions.change, (state, { modifications }) => ({
    ...state,
    modified: modifications
  })),
  on(SubUsersProfilePermissionsActions.applyChanges, state => ({ ...state, loading: true })),
  on(SubUsersProfilePermissionsActions.applyChangesError, state => ({ ...state, loading: false })),
  on(SubUsersProfilePermissionsActions.discardChanges, state => ({
    ...state,
    original: [...state.original],
    modified: state.original
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return subUserProfilePermissionsReducer(state, action);
}

export const getOriginal = (state: State) => state.original;

export const getModified = (state: State) => state.modified;

export const getLoading = (state: State) => state.loading;

export const getBspId = (state: State) => state.bspId;

export const hasChanges = (state: State) => state.modified.some(permissionCategory => permissionCategory.hasChanged);

export function getChanges(state: State): ChangeModel[] {
  return state.modified
    .filter(category => category.hasChanged)
    .map(category => ({ ...category, items: category.items.filter(item => item.hasChanged) }))
    .reduce((changes, item) => {
      item.items.forEach((listItem: SelectionListItem) => {
        changes.push({
          group: item.name,
          name: listItem.name,
          value: String(listItem.checked),
          originalValue: listItem.checked
        });
      });

      return changes;
    }, []);
}

export function mapSelectionListToPermissions(
  selectionListItems: SelectionList[],
  multiPermissionsEnabled = true
): UserPermission[] {
  return chain(selectionListItems)
    .map(category => category.items)
    .flatten()
    .map(item => ({ ...item.value, enabled: multiPermissionsEnabled ? item.checked : !item.checked }))
    .value();
}

function mapPermissionsToSelectionList(permissions: UserPermission[], userTemplate?: SubUserTemplate): SelectionList[] {
  const isEfficientUser = userTemplate === SubUserTemplate.Efficient;

  return chain(permissions)
    .map(permission => {
      const disabled = isEfficientUser && permission.template === SubUserTemplate.Streamlined;

      return {
        name: permission.name,
        checked: !disabled && permission.enabled,
        value: permission,
        hasChanged: false,
        disabled,
        tooltip: disabled ? 'subUsers.profile.permissionsTab.disabledTooltip' : ''
      };
    })
    .groupBy('value.category.name')
    .map((items, name) => {
      const disabled = items.every(item => item.disabled);

      return {
        name,
        disabled,
        items,
        tooltip: disabled ? 'subUsers.profile.permissionsTab.disabledTooltip' : ''
      };
    })
    .value();
}
