import { ServerError } from '~app/shared/errors';
import { SubUserCreateDialogActions } from '~app/sub-users/actions';
import { getSubUserCreateData, getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import { initialState, reducer } from '~app/sub-users/reducers/sub-user-create-dialog.reducer';

describe('subUserChangeStatusDialogReducer', () => {
  [
    SubUserCreateDialogActions.openSubUserCreateDialog(),
    SubUserCreateDialogActions.createSubUserSuccess({ subUser: getTestSubUser() })
  ].forEach(action => {
    it(`${action.type} should set initial state`, () => {
      const state = reducer({ loading: true, requestError: {} as ServerError }, action);

      expect(state).toEqual(initialState);
    });
  });

  it('SubUserStatusActions.createSubUserError should set state request error', () => {
    const action = SubUserCreateDialogActions.createSubUserError({ error: {} as ServerError });
    const state = reducer({ loading: true, requestError: null }, action);

    expect(state).toEqual({ loading: false, requestError: {} as ServerError });
  });

  it('SubUserStatusActions.createSubUser should set loading to true by using initialState', () => {
    const action = SubUserCreateDialogActions.createSubUser({ subUserData: getSubUserCreateData() });
    const state = reducer(initialState, action);

    expect(state.loading).toBeTruthy();
  });
});
