import { Action, createReducer, on } from '@ngrx/store';
import { cloneDeep } from 'lodash';

import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Bsp } from '~app/shared/models/bsp.model';
import { SubUsersListActions, SubUserStatusActions } from '~app/sub-users/actions';
import {
  SubUsersListFilter,
  SubUsersListRequiredFilter
} from '~app/sub-users/containers/sub-users-list/sub-users-list-filter.model';
import { SubUser, SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';

export const subUsersListFeatureKey = 'subUsersList';

export interface State {
  loading: boolean;
  data: SubUser[];
  selectedBsp: Bsp;
  query: DataQuery<SubUsersListFilter>;
  requiredFilter: SubUsersListRequiredFilter;
  subUserTypeByParent: SubUserTypeByParent;
}

export const initialState: State = {
  loading: false,
  data: [],
  selectedBsp: null,
  query: {
    ...cloneDeep(defaultQuery)
  },
  requiredFilter: {
    bsp: null,
    userTypeDetails: null
  },
  subUserTypeByParent: null
};

const subUsersListReducer = createReducer(
  initialState,
  on(SubUsersListActions.changeRequiredFilter, (state, { requiredFilter }) => ({
    ...state,
    selectedBsp: cloneDeep(requiredFilter.bsp),
    requiredFilter,
    query: {
      ...state.query,
      paginateBy: {
        ...initialState.query.paginateBy,
        size: state.query.paginateBy.size
      }
    }
  })),
  on(SubUsersListActions.changeQuery, (state, { query }) => ({
    ...state,
    query: {
      ...query,
      paginateBy: {
        ...query.paginateBy,
        page: state.query.paginateBy.page === query.paginateBy.page ? 0 : query.paginateBy.page
      }
    }
  })),
  on(SubUsersListActions.selectBsp, state => ({
    ...state,
    selectedBsp: cloneDeep(state.requiredFilter.bsp)
  })),
  on(SubUsersListActions.loadData, state => ({
    ...state,
    loading: true
  })),
  on(SubUsersListActions.loadDataSuccess, (state, { data }) => ({
    ...state,
    loading: false,
    data: data.records,
    query: {
      ...state.query,
      paginateBy: {
        page: data.pageNumber,
        size: data.pageSize,
        totalElements: data.total
      }
    }
  })),
  on(SubUsersListActions.loadDataError, state => ({
    ...state,
    loading: false,
    data: []
  })),
  on(SubUsersListActions.changeSelectedRows, (state, { data }) => ({
    ...state,
    data,
    loading: false
  })),
  on(SubUsersListActions.clearSelectedRows, state => ({
    ...state,
    data: state.data.map(subUser => ({ ...subUser, isSelected: false }))
  })),
  on(
    SubUserStatusActions.deactivateSubUserSuccess,
    SubUserStatusActions.deactivateHosuSubUserSuccess,
    SubUserStatusActions.reactivateHosuSubUserSuccess,
    SubUserStatusActions.reactivateSubUserSuccess,
    (state, { subUser }) => ({
      ...state,
      data: state.data.map(user => (user.id === subUser.id ? subUser : user))
    })
  ),
  on(SubUsersListActions.changeUserTypeByParent, (state, { subUserTypeByParent }) => ({
    ...state,
    subUserTypeByParent
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return subUsersListReducer(state, action);
}

export const getSelectedRows = (state: State) => state.data.filter(user => user.isSelected);

export const getSelectedRowsCount = (state: State) => getSelectedRows(state).length;

export const getSubUserTypeByParent = (state: State) => state.subUserTypeByParent;

export const getSelectedBsp = (state: State) => state.selectedBsp;
