import { SubUserStatusActions } from '~app/sub-users/actions';
import { getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import {
  getIsFormValid,
  getLoading,
  initialState,
  reducer
} from '~app/sub-users/reducers/sub-user-change-status-dialog.reducer';

describe('subUserChangeStatusDialogReducer', () => {
  it('SubUserStatusActions.openSubUserStatusDialog should set state by using initialState', () => {
    const action = SubUserStatusActions.openSubUserStatusDialog({ subUser: getTestSubUser() });

    const state = reducer({ loading: true, isFormValid: false }, action);

    expect(state).toEqual(initialState);
  });

  it('SubUserStatusActions.formStatusChanged should set isFormValid', () => {
    const action = SubUserStatusActions.formStatusChanged({ status: 'VALID' });

    const state = reducer({ loading: true, isFormValid: false }, action);

    expect(state.isFormValid).toBeTruthy();
  });

  it('SubUserStatusActions.deactivateSubUser should set loading to true', () => {
    const action = SubUserStatusActions.deactivateSubUser({ subUserId: 123, expiryDate: new Date() });

    const state = reducer({ loading: false, isFormValid: true }, action);

    expect(state.loading).toBeTruthy();
  });

  [
    SubUserStatusActions.deactivateSubUserSuccess({ subUser: getTestSubUser() }),
    SubUserStatusActions.deactivateSubUserError({ error: null }),
    SubUserStatusActions.reactivateSubUserSuccess({ subUser: getTestSubUser() }),
    SubUserStatusActions.reactivateSubUserError({ error: null })
  ].forEach(action => {
    it(`${action.type} should set loading to false`, () => {
      const state = reducer({ loading: true, isFormValid: true }, action);

      expect(state.loading).toBeFalsy();
    });
  });

  it('getLoading should return loading value', () => {
    expect(getLoading({ loading: false, isFormValid: true })).toBeFalsy();
    expect(getLoading({ loading: true, isFormValid: true })).toBeTruthy();
  });

  it('getIsFormValid should return isFormValid value', () => {
    expect(getIsFormValid({ loading: true, isFormValid: false })).toBeFalsy();
    expect(getIsFormValid({ loading: true, isFormValid: true })).toBeTruthy();
  });
});
