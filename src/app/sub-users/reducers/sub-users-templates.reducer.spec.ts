import { SubUsersApiActions, SubUsersTemplatesActions } from '~app/sub-users/actions';
import { getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import {
  getModifiedSubUsers,
  initialState,
  mapSelectedRowsToViewModel,
  reducer
} from '~app/sub-users/reducers/sub-users-templates.reducer';
import { SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

describe('subUsersTemplatesReducer', () => {
  it('SubUserStatusActions.createSubUser should set selectedSubUsers', () => {
    const selectedSubUsers = [
      {
        id: 123,
        portalEmail: 'test@test.com',
        name: 'test name',
        template: SubUserTemplate.Efficient
      }
    ];
    const action = SubUsersTemplatesActions.editUsersTemplate({ selectedSubUsers });

    const state = reducer({ ...initialState }, action);

    expect(state.selectedSubUsers).toEqual(selectedSubUsers);
  });

  it('SubUsersTemplatesActions.saveOriginalTemplatesForm should set originalTemplatesForm', () => {
    const originalTemplatesForm = {
      '73763001': SubUserTemplate.Streamlined,
      '73763002': SubUserTemplate.Efficient,
      bulkEfficient: null,
      bulkStreamlined: SubUserTemplate.Streamlined
    };
    const action = SubUsersTemplatesActions.saveOriginalTemplatesForm({ originalTemplatesForm });

    const state = reducer({ ...initialState }, action);

    expect(state.originalTemplatesForm).toEqual(originalTemplatesForm);
  });

  it('SubUsersTemplatesActions.saveModifiedTemplatesForm should set modifiedTemplatesForm', () => {
    const modifiedTemplatesForm = {
      '73763001': SubUserTemplate.Efficient,
      '73763002': SubUserTemplate.Efficient,
      bulkEfficient: SubUserTemplate.Efficient,
      bulkStreamlined: null
    };
    const action = SubUsersTemplatesActions.saveModifiedTemplatesForm({ modifiedTemplatesForm });

    const state = reducer({ ...initialState }, action);

    expect(state.modifiedTemplatesForm).toEqual(modifiedTemplatesForm);
  });

  it('SubUsersApiActions.applySubUserTemplatesChanges should set loading to true', () => {
    const action = SubUsersApiActions.applySubUserTemplatesChanges();

    const state = reducer({ ...initialState }, action);

    expect(state.loading).toBeTruthy();
  });

  it('SubUsersApiActions.updateSubUsersTemplatesError should set loading to false', () => {
    const action = SubUsersApiActions.updateSubUsersTemplatesError({ error: null });

    const state = reducer({ ...initialState, loading: true }, action);

    expect(state.loading).toBeFalsy();
  });

  it('SubUsersApiActions.updateSubUsersTemplatesSuccess should set loading to false', () => {
    const action = SubUsersApiActions.updateSubUsersTemplatesSuccess({ updatedSubUsersTemplatesData: [] });

    const state = reducer({ ...initialState, loading: true }, action);

    expect(state.loading).toBeFalsy();
  });

  it('SubUsersApiActions.updateSubUsersTemplatesSuccess should updates selectedSubUsers', () => {
    const selectedSubUsers = [
      {
        id: 123,
        portalEmail: 'test@test.com',
        name: 'test name',
        template: SubUserTemplate.Efficient
      },
      {
        id: 124,
        portalEmail: 'test1@test.com',
        name: 'test name',
        template: SubUserTemplate.Streamlined
      }
    ];

    const action = SubUsersApiActions.updateSubUsersTemplatesSuccess({
      updatedSubUsersTemplatesData: [
        {
          userId: 123,
          template: SubUserTemplate.Streamlined
        }
      ]
    });

    const state = reducer({ ...initialState, selectedSubUsers }, action);

    expect(state.selectedSubUsers[0].template).toBe(SubUserTemplate.Streamlined);
    expect(state.selectedSubUsers[1].template).toBe(SubUserTemplate.Streamlined);
  });

  it('mapSelectedRowsToViewModel should transform SubUser array to view model', () => {
    const subUser = getTestSubUser();
    const { id, template, portalEmail, name } = subUser;

    const result = mapSelectedRowsToViewModel([subUser]);

    expect(result).toEqual([{ id, template, portalEmail, name }]);
  });

  it('getModifiedSubUsers should extract changes', () => {
    const originalTemplatesForm = {
      1: SubUserTemplate.Streamlined,
      2: SubUserTemplate.Streamlined,
      bulkEfficient: null,
      bulkStreamlined: SubUserTemplate.Streamlined
    };

    const modifiedTemplatesForm = {
      1: SubUserTemplate.Streamlined,
      2: SubUserTemplate.Efficient,
      bulkEfficient: null,
      bulkStreamlined: null
    };

    const selectedSubUsers = [
      {
        id: 1,
        template: SubUserTemplate.Streamlined,
        portalEmail: 'test@test.com',
        name: 'test name'
      },
      {
        id: 2,
        template: SubUserTemplate.Streamlined,
        portalEmail: 'test_2@test.com',
        name: 'test name 2'
      }
    ];

    const result = getModifiedSubUsers(originalTemplatesForm, modifiedTemplatesForm, selectedSubUsers);

    expect(result).toEqual([
      { id: 2, template: SubUserTemplate.Efficient, portalEmail: 'test_2@test.com', name: 'test name 2' }
    ]);
  });
});
