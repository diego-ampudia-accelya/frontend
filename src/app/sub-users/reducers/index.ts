import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
import { isEqual } from 'lodash';

import { AppState } from '~app/reducers';
import * as fromSubUsersStatus from '~app/sub-users/reducers/sub-user-change-status-dialog.reducer';
import * as fromCreateSubUser from '~app/sub-users/reducers/sub-user-create-dialog.reducer';
import * as fromSubUsersHosuPermissions from '~app/sub-users/reducers/sub-user-hosu-permissions.reducer';
import * as fromSubUserLomuProfilePermissions from '~app/sub-users/reducers/sub-user-lomu-profile-permissions.reducer';
import * as fromSubUserLomuProfile from '~app/sub-users/reducers/sub-user-lomu-profile.reducer';
import * as fromSubUserProfilePermissions from '~app/sub-users/reducers/sub-user-profile-permissions.reducer';
import * as fromSubUserProfile from '~app/sub-users/reducers/sub-user-profile.reducer';
import * as fromSubUsersList from '~app/sub-users/reducers/sub-users-list.reducer';
import * as fromSubUsersPermissions from '~app/sub-users/reducers/sub-users-permissions.reducer';
import * as fromSubUsersTemplates from '~app/sub-users/reducers/sub-users-templates.reducer';

export const subUsersFeatureKey = 'subUsers';

export interface SubUsersState {
  [fromSubUsersList.subUsersListFeatureKey]: fromSubUsersList.State;
  [fromSubUserProfile.subUserProfileFeatureKey]: fromSubUserProfile.State;
  [fromSubUserLomuProfile.subUserLomuProfileFeatureKey]: fromSubUserLomuProfile.State;
  [fromSubUserProfilePermissions.featureKey]: fromSubUserProfilePermissions.State;
  [fromSubUserLomuProfilePermissions.featureKey]: fromSubUserLomuProfilePermissions.State;
  [fromCreateSubUser.createSubUserFeatureKey]: fromCreateSubUser.State;
  [fromSubUsersStatus.subUserStatusFeatureKey]: fromSubUsersStatus.State;
  [fromSubUsersTemplates.subUsersTemplatesFeatureKey]: fromSubUsersTemplates.State;
  [fromSubUsersPermissions.subUsersPermissionsFeatureKey]: fromSubUsersPermissions.State;
  [fromSubUsersHosuPermissions.subUserHosuPermissionsFeatureKey]: fromSubUsersHosuPermissions.State;
}

export interface State extends AppState {
  [subUsersFeatureKey]: SubUsersState;
}

export function reducers(state: SubUsersState | undefined, action: Action) {
  return combineReducers({
    [fromSubUsersList.subUsersListFeatureKey]: fromSubUsersList.reducer,
    [fromSubUserProfile.subUserProfileFeatureKey]: fromSubUserProfile.reducer,
    [fromSubUserLomuProfile.subUserLomuProfileFeatureKey]: fromSubUserLomuProfile.reducer,
    [fromSubUserProfilePermissions.featureKey]: fromSubUserProfilePermissions.reducer,
    [fromSubUserLomuProfilePermissions.featureKey]: fromSubUserLomuProfilePermissions.reducer,
    [fromCreateSubUser.createSubUserFeatureKey]: fromCreateSubUser.reducer,
    [fromSubUsersStatus.subUserStatusFeatureKey]: fromSubUsersStatus.reducer,
    [fromSubUsersTemplates.subUsersTemplatesFeatureKey]: fromSubUsersTemplates.reducer,
    [fromSubUsersPermissions.subUsersPermissionsFeatureKey]: fromSubUsersPermissions.reducer,
    [fromSubUsersHosuPermissions.subUserHosuPermissionsFeatureKey]: fromSubUsersHosuPermissions.reducer
  })(state, action);
}

export const getSubUsersState = createFeatureSelector<State, SubUsersState>(subUsersFeatureKey);

export const getListState = createSelector(getSubUsersState, state => state[fromSubUsersList.subUsersListFeatureKey]);

export const getListRequiredFilter = createSelector(getListState, state => state.requiredFilter);

export const getListQuery = createSelector(getListState, state => state.query);

export const getListLoading = createSelector(getListState, state => state.loading);

export const getListData = createSelector(getListState, state => state.data);

export const getHasData = createSelector(getListState, state => state.data && state.data.length > 0);

export const getSelectedRows = createSelector(getListState, fromSubUsersList.getSelectedRows);

export const getSelectedBsp = createSelector(getListState, fromSubUsersList.getSelectedBsp);

export const getSelectedRowsCount = createSelector(getListState, fromSubUsersList.getSelectedRowsCount);

export const getCurrentUserBsp = createSelector(getListState, state => state.selectedBsp.name);

export const getSubUserTypeByParent = createSelector(getListState, fromSubUsersList.getSubUserTypeByParent);

export const getSelectedRowsAsSubUsersTemplatesViewModel = createSelector(getSelectedRows, state =>
  fromSubUsersTemplates.mapSelectedRowsToViewModel(state)
);

export const getProfileState = createSelector(
  getSubUsersState,
  state => state[fromSubUserProfile.subUserProfileFeatureKey]
);

export const getProfileSubUser = createSelector(getProfileState, fromSubUserProfile.getSubUser);

export const getProfileLoading = createSelector(getProfileState, fromSubUserProfile.getLoading);

export const getProfileIsCreatingHosu = createSelector(getProfileState, fromSubUserProfile.getIsCreatingHosuMode);

export const getProfileDeactivating = createSelector(getProfileState, fromSubUserProfile.getDeactivating);

export const getProfileSubUserPortalEmail = createSelector(getProfileState, fromSubUserProfile.getSubUserPortalEmail);

export const getProfileSubUserRequestError = createSelector(getProfileState, fromSubUserProfile.getSubUserRequestError);

export const getProfilePropertyChanges = createSelector(getProfileState, fromSubUserProfile.getPropertyChanges);

export const getPropertiesFormValue = createSelector(getProfileState, state => state.propertiesFormValue);

export const hasPropertyValidationErrors = createSelector(
  getProfileState,
  fromSubUserProfile.hasPropertyValidationErrors
);

export const canApplyPropertyChanges = createSelector(getProfileState, fromSubUserProfile.canApplyPropertyChanges);

export const getProfileSubUserPropertiesError = createSelector(
  getProfileState,
  fromSubUserProfile.getSubUserPropertiesError
);

export const getSubUsersCreateDialogState = createSelector(
  getSubUsersState,
  state => state[fromCreateSubUser.createSubUserFeatureKey]
);

export const getSubUsersCreateDialogLoadingState = createSelector(
  getSubUsersCreateDialogState,
  fromCreateSubUser.getLoading
);

export const getSubUsersCreateDialogRequestError = createSelector(
  getSubUsersCreateDialogState,
  fromCreateSubUser.getRequestError
);

export const getSubUsersStatusState = createSelector(
  getSubUsersState,
  state => state[fromSubUsersStatus.subUserStatusFeatureKey]
);

export const getSubUsersStatusDialogLoadingState = createSelector(
  getSubUsersStatusState,
  fromSubUsersStatus.getLoading
);

export const getSubUsersStatusDialogIsFormValidState = createSelector(
  getSubUsersStatusState,
  fromSubUsersStatus.getIsFormValid
);

export const getSubUserTemplatesState = createSelector(
  getSubUsersState,
  state => state[fromSubUsersTemplates.subUsersTemplatesFeatureKey]
);

export const getSubUserOriginalTemplatesForm = createSelector(
  getSubUserTemplatesState,
  state => state.originalTemplatesForm
);

export const getSubUserModifiedTemplatesForm = createSelector(
  getSubUserTemplatesState,
  state => state.modifiedTemplatesForm
);

export const getSelectedSubUsers = createSelector(getSubUserTemplatesState, state => state.selectedSubUsers);

export const getShouldContinueTemplatesNavigationState = createSelector(
  getSubUserTemplatesState,
  state => state.shouldContinueNavigation
);

export const getSubUserTemplatesLoading = createSelector(getSubUserTemplatesState, fromSubUsersTemplates.getLoading);

export const getSubUserTemplatesChanges = createSelector(
  getSubUserTemplatesState,
  fromSubUsersTemplates.getTemplatesChanges
);

export const getSubUserTemplatesCanApplyChanges = createSelector(
  getSubUserTemplatesState,
  state => !isEqual(state.modifiedTemplatesForm, state.originalTemplatesForm)
);

export const getSubUsersPermissionsState = createSelector(
  getSubUsersState,
  state => state[fromSubUsersPermissions.subUsersPermissionsFeatureKey]
);

export const getSubUsersPermissionsViewModel = createSelector(
  getSubUsersPermissionsState,
  state => state.permissions.viewModel
);

export const allSubUserPermissionsSelected = createSelector(
  getSubUsersPermissionsState,
  fromSubUsersPermissions.allSubUserPermissionsSelected
);

export const isProcessing = createSelector(getSubUsersPermissionsState, fromSubUsersPermissions.isProcessing);

export const allPermissionCategoriesExpanded = createSelector(
  getSubUsersPermissionsState,
  fromSubUsersPermissions.allPermissionCategoriesExpanded
);

export const allPermissionsSelected = createSelector(
  getSubUsersPermissionsState,
  fromSubUsersPermissions.allPermissionsSelected
);

export const getSubUsersPermissionsCanApplyChanges = createSelector(
  getSubUsersPermissionsState,
  fromSubUsersPermissions.canApplyChanges
);

export const getChangedPermissions = createSelector(
  getSubUsersPermissionsState,
  fromSubUsersPermissions.getChangedPermissions
);

export const getModifiedSubUsersPermissions = createSelector(
  getSubUsersPermissionsState,
  fromSubUsersPermissions.getModifiedSubUsersPermissions
);

export const getShouldContinuePermissionsNavigationState = createSelector(
  getSubUsersPermissionsState,
  state => state.shouldContinueNavigation
);

export const getProfilePermissionsState = createSelector(
  getSubUsersState,
  state => state[fromSubUserProfilePermissions.featureKey]
);

export const getOriginalProfilePermissions = createSelector(
  getProfilePermissionsState,
  fromSubUserProfilePermissions.getOriginal
);

export const getModifiedProfilePermissions = createSelector(
  getProfilePermissionsState,
  fromSubUserProfilePermissions.getModified
);

export const areProfilePermissionsLoading = createSelector(
  getProfilePermissionsState,
  fromSubUserProfilePermissions.getLoading
);

export const canApplyProfilePermissionsChanges = createSelector(
  getProfilePermissionsState,
  fromSubUserProfilePermissions.hasChanges
);

export const getProfilePermissionsChanges = createSelector(
  getProfilePermissionsState,
  fromSubUserProfilePermissions.getChanges
);

export const getProfilePermissionsBspId = createSelector(
  getProfilePermissionsState,
  fromSubUserProfilePermissions.getBspId
);

//LOMU profile

export const getLomuProfileState = createSelector(
  getSubUsersState,
  state => state[fromSubUserLomuProfile.subUserLomuProfileFeatureKey]
);

export const getLomuProfileSubUser = createSelector(getLomuProfileState, fromSubUserLomuProfile.getSubUserLomu);

export const getLomuProfileLoading = createSelector(getLomuProfileState, fromSubUserLomuProfile.getLoading);

export const getLomuProfileSubUserRequestError = createSelector(
  getLomuProfileState,
  fromSubUserLomuProfile.getSubUserLomuRequestError
);

export const getLomuProfileBspId = createSelector(getLomuProfileState, fromSubUserLomuProfile.getSubUserLomuBspId);

//Lomu profile Permissions

export const getLomuProfilePermissionsState = createSelector(
  getSubUsersState,
  state => state[fromSubUserLomuProfilePermissions.featureKey]
);

export const areLomuProfilePermissionsLoading = createSelector(
  getLomuProfilePermissionsState,
  fromSubUserLomuProfilePermissions.getLoading
);

export const getOriginalLomuProfilePermissions = createSelector(
  getLomuProfilePermissionsState,
  fromSubUserLomuProfilePermissions.getOriginal
);

export const canApplyLomuProfilePermissionsChanges = createSelector(
  getLomuProfilePermissionsState,
  fromSubUserLomuProfilePermissions.hasChanges
);

export const getLomuProfilePermissionUserId = createSelector(
  getLomuProfilePermissionsState,
  fromSubUserLomuProfilePermissions.getLomuUserId
);

export const getModifiedLomuProfilePermissions = createSelector(
  getLomuProfilePermissionsState,
  fromSubUserLomuProfilePermissions.getModified
);

export const getLomuProfilePermissionsChanges = createSelector(
  getLomuProfilePermissionsState,
  fromSubUserLomuProfilePermissions.getChanges
);

//SubUser Hosu Permissions

export const getSubUserHosuPermissionState = createSelector(
  getSubUsersState,
  state => state[fromSubUsersHosuPermissions.subUserHosuPermissionsFeatureKey]
);

export const getBspsFromHosuSelection = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getBspsFromHosuSelection
);

export const getSelectedHosus = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getSelectedHosus
);

export const getSelectedHosuBsps = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getSelectedHosuBsps
);

export const getOriginalHosusPermissionsByBsps = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getOriginal
);

export const getModifiedHosusPermissionsByBsps = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getModified
);

export const areHosuPermissionsLoading = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getPermissionsLoading
);

export const canApplyHosuPermissionsChanges = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.hasChanges
);

export const getModifiedHosuPermissions = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getModified
);

export const getHosuPermissionsChanges = createSelector(
  getSubUserHosuPermissionState,
  fromSubUsersHosuPermissions.getChanges
);

export const getEnabled = createSelector(getSubUserHosuPermissionState, fromSubUsersHosuPermissions.getEnabled);
