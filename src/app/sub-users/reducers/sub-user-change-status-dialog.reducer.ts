import { Action, createReducer, on } from '@ngrx/store';

import { SubUserStatusActions } from '~app/sub-users/actions';

export const subUserStatusFeatureKey = 'subUserStatus';

export interface State {
  loading: boolean;
  isFormValid: boolean;
}

export const initialState: State = {
  loading: false,
  isFormValid: true
};

const subUserChangeStatusDialogReducer = createReducer(
  initialState,
  on(SubUserStatusActions.openSubUserStatusDialog, () => initialState),
  on(SubUserStatusActions.openSubUserHosuInfoDialog, () => initialState),
  on(SubUserStatusActions.formStatusChanged, (state, { status }) => ({
    ...state,
    isFormValid: status === 'VALID'
  })),
  on(SubUserStatusActions.deactivateSubUser, SubUserStatusActions.reactivateSubUser, state => ({
    ...state,
    loading: true
  })),
  on(
    SubUserStatusActions.deactivateSubUserSuccess,
    SubUserStatusActions.deactivateSubUserError,
    SubUserStatusActions.reactivateSubUserSuccess,
    SubUserStatusActions.reactivateSubUserError,
    state => ({
      ...state,
      loading: false
    })
  )
);

export function reducer(state: State | undefined, action: Action) {
  return subUserChangeStatusDialogReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;

export const getIsFormValid = (state: State): boolean => state.isFormValid;
