import { createAction, props } from '@ngrx/store';

import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { UserPermission } from '~app/sub-users/shared/permissions.models';
import { SubUser } from '~app/sub-users/shared/sub-users.models';

export const requestPermissions = createAction('[SubUser Profile Permissions] Request');

export const requestHosuPermissions = createAction(
  '[SubUser Hosu Profile Permissions] Request',
  props<{ bspId: number }>()
);

export const requestPermissionsSuccess = createAction(
  '[SubUser Profile Permissions] Request Success',
  props<{ permissions: UserPermission[]; subUser: SubUser }>()
);
export const requestPermissionsError = createAction('[SubUser Profile Permissions] Request Error');

export const change = createAction('[SubUser Profile Permissions] Change', props<{ modifications: SelectionList[] }>());

export const openApplyChanges = createAction('[SubUser Profile Permissions] Open Apply Changes');

export const applyChanges = createAction('[SubUser Profile Permissions] Apply Changes');

export const applyChangesSuccess = createAction(
  '[SubUser Profile Permissions] Apply Changes Success',
  props<{ permissions: UserPermission[]; subUser: SubUser }>()
);
export const applyChangesError = createAction('[SubUser Profile Permissions] Apply Changes Error');

export const discardChanges = createAction('[SubUser Profile Permissions] Discard Changes');

export const cancel = createAction('[SubUser Profile Permissions] Cancel');
