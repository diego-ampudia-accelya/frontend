import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { SubUser, SubUserCreateModel } from '~app/sub-users/shared/sub-users.models';

export const openSubUserCreateDialog = createAction('[SubUserCreateDialog] Open SubUser create dialog');

export const createSubUser = createAction(
  '[SubUserCreateDialog] Create SubUser',
  props<{ subUserData: SubUserCreateModel }>()
);

export const createSubUserSuccess = createAction(
  '[SubUserCreateDialog] Create SubUser Success',
  props<{ subUser: SubUser }>()
);

export const createSubUserError = createAction(
  '[SubUserCreateDialog] Create SubUser Error',
  props<{ error: ServerError }>()
);
