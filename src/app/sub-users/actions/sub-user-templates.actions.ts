import { createAction, props } from '@ngrx/store';

import { SubUserTemplate, SubUserTemplateViewModel } from '~app/sub-users/shared/sub-users.models';

export const editUsersTemplate = createAction(
  '[SubUsersTemplates] Get selected sub users templates',
  props<{ selectedSubUsers: SubUserTemplateViewModel[] }>()
);

export const saveOriginalTemplatesForm = createAction(
  '[SubUsersTemplates] Save original sub users templates',
  props<{ originalTemplatesForm: { [key: string]: SubUserTemplate } }>()
);

export const saveModifiedTemplatesForm = createAction(
  '[SubUsersTemplates] Save modified sub users templates',
  props<{ modifiedTemplatesForm: { [key: string]: SubUserTemplate } }>()
);

export const discardSubUsersTemplatesChanges = createAction('[SubUsersTemplates] Discard templates changes');

export const subUsersTemplatesOpenApplyChangesDialog = createAction(
  '[SubUsersTemplates] Open apply sub users templates dialog'
);

export const subUsersTemplatesOpenUnsavedChangesDialog = createAction(
  '[SubUsersTemplates] Open unsaved sub users templates dialog'
);
