import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { SubUserLomu } from '~app/sub-users/shared/sub-users.models';

export const requestSubUserLomu = createAction(
  '[SubUser LOMU Profile] Request SubUserLomu',
  props<{ bspId: number }>()
);

export const requestSubUserLomuSuccess = createAction(
  '[SubUser LOMU Profile] Request SubUserLomu Success',
  props<{ subUserLomu: SubUserLomu }>()
);

export const requestSubUserLomuError = createAction(
  '[SubUser LOMU Profile] Request SubUserLomu Error',
  props<{ error: ServerError }>()
);
