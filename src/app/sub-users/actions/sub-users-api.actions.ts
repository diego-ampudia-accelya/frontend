import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { SubUserEditTemplateModel } from '~app/sub-users/shared/sub-users.models';

export const requestSubUserTemplates = createAction(
  '[SubUser/API] Request SubUser Templates',
  props<{ ids: number[] }>()
);

export const applySubUserTemplatesChanges = createAction('[SubUser/API] Apply SubUser templates changes');

export const updateSubUsersTemplatesSuccess = createAction(
  '[SubUser/API] Update SubUsers templates Success',
  props<{ updatedSubUsersTemplatesData: SubUserEditTemplateModel[] }>()
);

export const updateSubUsersTemplatesError = createAction(
  '[SubUser/API] Update SubUsers templates Error',
  props<{ error: ServerError }>()
);
