import { createAction, props } from '@ngrx/store';

import { UserPermission } from '../shared/permissions.models';
import { SubUser } from '../shared/sub-users.models';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { ServerError } from '~app/shared/errors';
import { BspDto } from '~app/shared/models/bsp.model';

export const fetchBspsFromHosuSelection = createAction(
  '[SubUserHosuPermissionDialog] Fetching Bsps From selected Hosus',
  props<{ selectedHosus: number[] }>()
);

export const fetchBspsFromHosuSelectionSuccess = createAction(
  '[SubUserHosuPermissionDialog] Fetching Bsps from selected Hosus Success',
  props<{ bsps: BspDto[] }>()
);

export const fetchBspsFromHosuSelectionError = createAction(
  '[SubUserHosuPermissionDialog] Fetching Bsps from selected Hosus Error',
  props<{ error: ServerError }>()
);

export const updateSelectedHosus = createAction(
  '[SubUserHosuPermissionDialog] Updating selected Hosus',
  props<{ selectedHosus: SubUser[] }>()
);

export const updateSelectedHosuBsps = createAction(
  '[SubUserHosuPermissionDialog] Updating selected Hosu Bsps',
  props<{ selectedHosuBsps: BspDto[] }>()
);

export const requestHosusPermissionsByBsps = createAction('[SubUserHosuPermissions] Hosus Permissions by Bsps Request');

export const requestHosusPermissionsByBspsSuccess = createAction(
  '[SubUserHosuPermissions] Hosus Permissions by Bsps Request Success',
  props<{ permissionsResponse: UserPermission[] }>()
);

export const requestHosusPermissionsByBspsError = createAction(
  '[SubUserHosuPermissions] Hosus Permissions by Bsps Request Error'
);

export const change = createAction('[SubUserHosuPermissions] Change', props<{ modifications: SelectionList[] }>());

export const openCancelChanges = createAction('[SubUserHosuPermissions] Open Cancel Changes');

export const openApplyChanges = createAction('[SubUserHosuPermissions] Open Apply Changes');

export const applyChanges = createAction('[SubUserHosuPermissions] Apply Changes');

export const applyChangesSuccess = createAction('[SubUserHosuPermissions] Apply Changes Success');

export const applyChangesError = createAction('[SubUserHosuPermissions] Apply Changes Error');

export const discardChanges = createAction('[SubUserHosuPermissions] Discard Changes');

export const cancel = createAction('[SubUserHosuPermissions] Cancel');

export const updateEnabled = createAction('[SubUserHomuProfile] Updating enabled', props<{ enabled: boolean }>());
