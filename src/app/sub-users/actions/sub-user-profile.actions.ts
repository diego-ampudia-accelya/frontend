import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { SubUser, SubUserUpdateModel } from '~app/sub-users/shared/sub-users.models';

export const isCreatingHosuSubUser = createAction('[SubUser Profile] is Creating a Hosu SubUser');

export const createHosuSubUser = createAction('[SubUser Profile] Create Hosu SubUser');

export const createHosuSubUserSuccess = createAction(
  '[SubUser Profile] Create Hosu SubUser Success',
  props<{ subUser: SubUser }>()
);

export const createHosuSubUserError = createAction(
  '[SubUser Profile] Create Hosu SubUser Error',
  props<{ error: ServerError }>()
);

export const requestSubUser = createAction('[SubUser Profile] Request SubUser', props<{ id: number }>());

export const requestSubUserSuccess = createAction(
  '[SubUser Profile] Request SubUser Success',
  props<{ subUser: SubUser }>()
);

export const requestSubUserError = createAction(
  '[SubUser Profile] Request SubUser Error',
  props<{ error: ServerError }>()
);

export const propertiesFormValueChange = createAction(
  '[SubUser Profile] Properties Form Value Change',
  props<{ formValue: SubUserUpdateModel; isFormValid: boolean }>()
);

export const propertiesDeactivationRequest = createAction('[SubUser Profile] Properties Deactivation Request');

export const applyPropertyChanges = createAction('[SubUser Profile] Apply Property Changes');

export const discardPropertyChanges = createAction('[SubUser Profile] Discard Property Changes');

export const cancelPropertyChanges = createAction('[SubUser Profile] Cancel Property Changes');

export const updateSubUserProperties = createAction(
  '[SubUser Profile] Update SubUser Properties',
  props<{ data: SubUserUpdateModel }>()
);

export const updateSubUserPropertiesSuccess = createAction(
  '[SubUser Profile] Update SubUser Properties Success',
  props<{ subUser: SubUser }>()
);

export const updateSubUserPropertiesError = createAction(
  '[SubUser Profile] Update SubUser Properties Error',
  props<{ error: ServerError }>()
);
