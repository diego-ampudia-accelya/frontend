import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import {
  SubUser,
  SubUserHosuReactivateModel,
  SubUserReactivateModel,
  SubUserTypeByParent
} from '~app/sub-users/shared/sub-users.models';

export const openSubUserStatusDialog = createAction(
  '[SubUserStatusDialog] Open SubUser status dialog',
  props<{ subUser: SubUser }>()
);

export const openSubUserHosuInfoDialog = createAction(
  '[SubUserHosuInfodDialog] Open SubUser Hosu Info dialog when not Countries Selected'
);

export const openSubUserStatusBySubUserTypeDialog = createAction(
  '[SubUserStatusDialog] Open SubUser status dialog by Sub User Type',
  props<{ subUser: SubUser; subUserTypeByParent: SubUserTypeByParent }>()
);

export const deactivateSubUser = createAction(
  '[SubUserDeactivateDialog] Deactivate User',
  props<{ subUserId: number; expiryDate: Date }>()
);

export const deactivateHosuSubUser = createAction(
  '[HosuSubUserDeactivateDialog] Deactivate Hosu User',
  props<{ subUserId: number }>()
);

export const deactivateSubUserSuccess = createAction(
  '[SubUserDeactivateDialog] Deactivate User Success',
  props<{ subUser: SubUser }>()
);

export const deactivateHosuSubUserSuccess = createAction(
  '[HosuSubUserDeactivateDialog] Deactivate Hosu User Success',
  props<{ subUser: SubUser }>()
);

export const deactivateSubUserError = createAction(
  '[SubUserDeactivateDialog] Deactivate User Error',
  props<{ error: ServerError }>()
);

export const formStatusChanged = createAction(
  '[SubUserReactivateDialog] Form Status Changed',
  props<{ status: 'VALID' | 'INVALID' | 'PENDING' | 'DISABLED' }>()
);

export const reactivateSubUser = createAction(
  '[SubUserReactivateDialog] Reactivate User',
  props<{ id: number; reactivateData: SubUserReactivateModel }>()
);

export const reactivateHosuSubUser = createAction(
  '[SubUserHosuReactivateDialog] Reactivate Hosu User',
  props<{ subUserId: number; reactivateHosuData: SubUserHosuReactivateModel }>()
);

export const reactivateSubUserSuccess = createAction(
  '[SubUserReactivateDialog] Reactivate User Success',
  props<{ subUser: SubUser }>()
);

export const reactivateHosuSubUserSuccess = createAction(
  '[SubUserHosuReactivateDialog] Reactivate Hosu User Success',
  props<{ subUser: SubUser }>()
);

export const reactivateSubUserError = createAction(
  '[SubUserReactivateDialog] Reactivate User Error',
  props<{ error: ServerError }>()
);
