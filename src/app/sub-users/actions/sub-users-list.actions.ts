import { createAction, props } from '@ngrx/store';

import { DataQuery } from '~app/shared/components/list-view';
import { ServerError } from '~app/shared/errors';
import { PagedData } from '~app/shared/models/paged-data.model';
import {
  SubUsersListFilter,
  SubUsersListRequiredFilter
} from '~app/sub-users/containers/sub-users-list/sub-users-list-filter.model';
import { SubUser, SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';

export const changeRequiredFilter = createAction(
  '[SubUsersList] Change Required Filter',
  props<{ requiredFilter: SubUsersListRequiredFilter }>()
);

export const changeQuery = createAction(
  '[SubUsersList] Change Query',
  props<{ query: DataQuery<SubUsersListFilter> }>()
);

export const selectBsp = createAction('[SubUsersList] Select BSP');

export const loadData = createAction('[SubUsersList] Load Data');

export const loadDataSuccess = createAction('[SubUsersList] Load Data Success', props<{ data: PagedData<SubUser> }>());

export const loadDataError = createAction('[SubUsersList] Load Data Error', props<{ error: ServerError }>());

export const clearSelectedRows = createAction('[SubUsersList] Clear Selected Rows');

export const changeSelectedRows = createAction('[SubUsersList] Change Selected Rows', props<{ data: SubUser[] }>());

export const editSubUsersPermissions = createAction(
  '[SubUsersList] Edit Sub Users Permissions',
  props<{ selectedSubUsers: SubUser[] }>()
);

export const changeUserTypeByParent = createAction(
  '[SubUsersList] Change User Type By Parent',
  props<{ subUserTypeByParent: SubUserTypeByParent }>()
);
