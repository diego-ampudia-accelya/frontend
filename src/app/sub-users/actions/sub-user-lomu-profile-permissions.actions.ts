import { createAction, props } from '@ngrx/store';

import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { UserPermissionLomu, UserPermissionLomuResponse } from '~app/sub-users/shared/permissions.models';

export const requestLomuPermissions = createAction('[SubUser Lomu Profile Permissions] Request');

export const requestLomuPermissionsSuccess = createAction(
  '[SubUser Lomu Profile Permissions] Request Success',
  props<{ permissionsResponse: UserPermissionLomuResponse[] }>()
);
export const requestLomuPermissionsError = createAction('[SubUser Lomu Profile Permissions] Request Error');

export const change = createAction(
  '[SubUser Lomu Profile Permissions] Change',
  props<{ modifications: SelectionList[] }>()
);

export const openApplyChanges = createAction('[SubUser Lomu Profile Permissions] Open Apply Changes');

export const applyChanges = createAction('[SubUser Lomu Profile Permissions] Apply Changes');

export const applyChangesSuccess = createAction(
  '[SubUser Lomu Profile Permissions] Apply Changes Success',
  props<{ permissions: UserPermissionLomu[] }>()
);
export const applyChangesError = createAction('[SubUser Lomu Profile Permissions] Apply Changes Error');

export const discardChanges = createAction('[SubUser Lomu Profile Permissions] Discard Changes');

export const cancel = createAction('[SubUser Lomu Profile Permissions] Cancel');
