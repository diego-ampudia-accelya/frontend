import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import {
  SubUser,
  SubUserPermission,
  SubUserPermissionCategoryGroups,
  SubUserPermissionsOrigin
} from '~app/sub-users/shared/sub-users.models';

export const fetchSubUserAndPermissions = createAction(
  '[SubUsersPermissions] Fetch SubUsers And Permissions.',
  props<{ selectedSubUsers: SubUser[] }>()
);

export const fetchSubUserAndPermissionsSuccess = createAction(
  '[SubUsersPermissions] Fetch SubUsers And Permissions Success.',
  props<{ subUsers: SubUserPermissionsOrigin[] }>()
);

export const fetchSubUserAndPermissionsError = createAction(
  '[SubUsersPermissions] Fetch SubUsers And Permissions Error.'
);

export const collapseAll = createAction('[SubUsersPermissions] Collapse All Panels.', props<{ value: boolean }>());

export const expandAll = createAction('[SubUsersPermissions] Expand All Panels.', props<{ value: boolean }>());

export const togglePanel = createAction('[SubUsersPermissions] Toggle Panel.', props<{ categoryId: number }>());

export const selectAll = createAction('[SubUsersPermissions] Select All Permissions.', props<{ value: boolean }>());

export const deselectAll = createAction('[SubUsersPermissions] Deselect All Permissions.', props<{ value: boolean }>());

export const selectByUser = createAction(
  '[SubUsersPermissions] Select All Permissions By User.',
  props<{ subUser: SubUser }>()
);

export const deselectByUser = createAction(
  '[SubUsersPermissions] Deselect All Permissions By User.',
  props<{ subUser: SubUser }>()
);

export const toggleByCategory = createAction(
  '[SubUsersPermissions] Toggle Permissions By Category Groups.',
  props<{ value: boolean; categoryId: number }>()
);

export const toggleByPermission = createAction(
  '[SubUsersPermissions] Toggle Permissions By Permission Group.',
  props<{ value: boolean; permissionId: number }>()
);

export const toggleByUser = createAction(
  '[SubUsersPermissions] Toggle Permissions By User Id.',
  props<{ value: boolean; userId: number }>()
);

export const toggleByUserAndCategory = createAction(
  '[SubUsersPermissions] Toggle Permissions By User and Category.',
  props<{ value: boolean; group: SubUserPermissionCategoryGroups }>()
);

export const togglePermission = createAction(
  '[SubUsersPermissions] Toggle Permission.',
  props<{ value: boolean; user: SubUserPermission }>()
);

export const openApplySubUsersPermissionsChangesDialog = createAction(
  '[SubUsersPermissions] Open Apply Permissions Dialog.'
);

export const openUnsavedSubUsersPermissionsChangesDialog = createAction(
  '[SubUsersPermissions] Open Unsaved Permissions Dialog.'
);

export const applySubUsersPermissionsChanges = createAction('[SubUsersPermissions] Apply Permissions Changes.');

export const cancelSubUsersPermissionsChanges = createAction('[SubUsersPermissions] Cancel Permissions Changes.');

export const discardSubUsersPermissionsChanges = createAction('[SubUsersPermissions] Discard Permissions Changes.');

export const updateSubUsersPermissionsSuccess = createAction(
  '[SubUsersPermissions] Update Permissions Success.',
  props<{ users: SubUserPermissionsOrigin[] }>()
);
export const updateSubUsersPermissionsError = createAction(
  '[SubUsersPermissions] Update Permissions Error.',
  props<{ error: ServerError }>()
);
