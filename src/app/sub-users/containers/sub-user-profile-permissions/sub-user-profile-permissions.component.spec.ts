import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';

import { SubUserPermissionsComponent } from './sub-user-profile-permissions.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { createAirlineMultiCountryUser, createAirlineUser } from '~app/shared/mocks/airline-user';
import { SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUsersChangesService } from '~app/sub-users/shared/sub-users-changes.service';
import { SubUserHosuPermissionSearch } from '~app/sub-users/shared/sub-users.models';

describe('SubUserProfilePermissionsComponent', () => {
  let component: SubUserPermissionsComponent;
  let fixture: ComponentFixture<SubUserPermissionsComponent>;
  let mockStore: MockStore;

  const expectedMultiCountryAirlineUser = createAirlineMultiCountryUser();
  const expectedAirlineUser = createAirlineUser();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [SubUserPermissionsComponent, MockPipe(HasPermissionPipe)],
      providers: [
        provideMockStore({
          selectors: [
            { selector: fromSubUsers.getOriginalProfilePermissions, value: [] },
            { selector: fromSubUsers.areProfilePermissionsLoading, value: false },
            { selector: fromSubUsers.getProfilePermissionsBspId, value: null },
            { selector: fromAuth.getUser, value: expectedAirlineUser }
          ]
        }),
        mockProvider(L10nTranslationService, {
          translate: (key: string) => key
        }),
        mockProvider(SubUsersChangesService),
        FormBuilder,
        { provide: PermissionsService, useValue: { hasPermission: () => true } }
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUserPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    const builder = TestBed.inject(FormBuilder);
    const formUtil = new FormUtil(builder);

    component.form = formUtil.createGroup<SubUserHosuPermissionSearch>({
      bsp: null
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onPermissionChange should dispatch change action', () => {
    const dispatchSpy = spyOn(mockStore, 'dispatch').and.callThrough();

    component.onPermissionChange([]);

    expect(dispatchSpy).toHaveBeenCalledWith(SubUsersProfilePermissionsActions.change({ modifications: [] }));
  });

  describe('LEAN user', () => {
    it('MultiCountry variables must be initialized according to user BSPs ', fakeAsync(() => {
      const expectedBspList = expectedMultiCountryAirlineUser.bsps.map(bsp => toValueLabelObjectBsp(bsp));
      mockStore.overrideSelector(fromAuth.getUser, expectedMultiCountryAirlineUser);
      mockStore.refreshState();
      component.ngOnInit();

      tick();

      expect(component.isMultiCountryUser).toBeTruthy();
      expect(component.bspCountriesList).toEqual(expectedBspList);
    }));
  });
});
