import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUsersChangesService } from '~app/sub-users/shared/sub-users-changes.service';
import { SubUserHosuPermissionSearch } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-user-profile-permissions',
  templateUrl: './sub-user-profile-permissions.component.html',
  styleUrls: ['./sub-user-profile-permissions.component.scss']
})
export class SubUserPermissionsComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromSubUsers.areProfilePermissionsLoading));
  // Use defensive cloning to force the SelectionListComponent to work with the store
  public permissions$ = this.store.pipe(select(fromSubUsers.getOriginalProfilePermissions), map(cloneDeep));
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public Permissions = Permissions;
  public buttonDesign = ButtonDesign;

  public bspCountriesList: DropdownOption<BspDto>[];
  public isMultiCountryUser = false;
  public isFilterExpanded = true;
  public form: FormGroup;
  public bspSelected: Bsp;
  public tagValue: string;
  public isPermissionsSearched = false;
  public canEditPermissions = this.permissionsService.hasPermission(Permissions.updateSubuserPermissions);

  public filterIcon: string;

  private bspControl: AbstractControl;
  private userBsps: Bsp[];
  private bspId$ = this.store.pipe(select(fromSubUsers.getProfilePermissionsBspId), first());

  private iconLabels = {
    clear: 'mi md-18 clear',
    filter: 'mi md-18 filter_list'
  };

  private formUtil: FormUtil;

  private loggedUser$ = this.store.pipe(select(getUser), first());
  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private changesService: SubUsersChangesService,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private permissionsService: PermissionsService
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
    this.filterIcon = this.iconLabels.clear;
  }

  public ngOnInit(): void {
    this.initializeUserBsps();
  }

  public onPermissionChange(modifications: SelectionList[]) {
    // Use defensive cloning to force the SelectionListComponent to work with the store
    this.store.dispatch(SubUsersProfilePermissionsActions.change({ modifications: cloneDeep(modifications) }));
  }

  public canDeactivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromSubUsers.canApplyProfilePermissionsChanges),
      switchMap(hasChanges => (hasChanges ? this.changesService.handlePermissionsChangeAndDeactivate$ : of(true))),
      first()
    );
  }

  public toggleFilter(): void {
    this.isFilterExpanded = !this.isFilterExpanded;
    this.filterIcon = !this.isFilterExpanded ? this.iconLabels.filter : this.iconLabels.clear;
  }

  public viewFilter(): void {
    this.isFilterExpanded = true;
    this.filterIcon = this.iconLabels.clear;
  }

  public clearBspSelected(): void {
    this.isPermissionsSearched = false;
    this.isFilterExpanded = true;
    this.filterIcon = this.iconLabels.clear;
    this.bspSelected = null;
    this.bspControl.patchValue(this.bspSelected);
    this.searchPermissions();
  }

  public searchPermissions() {
    if (this.bspSelected) {
      this.isPermissionsSearched = true;
      this.isFilterExpanded = false;
      this.filterIcon = this.iconLabels.filter;
      this.store.dispatch(SubUsersProfilePermissionsActions.requestHosuPermissions({ bspId: this.bspSelected.id }));
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeForm() {
    this.form = this.formUtil.createGroup<SubUserHosuPermissionSearch>(
      {
        bsp: []
      },
      { updateOn: 'change' }
    );
  }

  private initializeUserBsps() {
    this.loggedUser$
      .pipe(
        tap(loggedUser => {
          const hasLeanPermission = loggedUser.permissions.some(perm => perm === Permissions.lean);

          this.userBsps = loggedUser?.bsps;
          this.isMultiCountryUser = this.userBsps?.length > 1 || hasLeanPermission;
          this.bspCountriesList = this.userBsps?.map(bsp => toValueLabelObjectBsp(bsp));
        }),
        switchMap(() => this.bspId$)
      )
      .subscribe(bspId => {
        if (!this.isMultiCountryUser) {
          this.isPermissionsSearched = true;
          this.store.dispatch(SubUsersProfilePermissionsActions.requestPermissions());
          this.canEditPermissions = this.permissionsService.hasPermission(Permissions.updateSubuserPermissions);
        } else {
          this.canEditPermissions = this.permissionsService.hasPermission(Permissions.updateHosuSubuserPermissions);
          this.initializeForm();
          this.initializeBspSelected(bspId);
          this.initializeBspListener();
        }
      });
  }

  private initializeBspSelected(bspId) {
    this.bspControl = FormUtil.get<SubUserHosuPermissionSearch>(this.form, 'bsp');

    if (bspId) {
      this.bspSelected = this.userBsps.find(userBsp => userBsp.id === bspId);
      this.tagValue = `${this.getFiltertag()} - ${this.bspSelected.isoCountryCode} ${this.bspSelected.name}`;
      this.bspControl.patchValue(this.bspSelected);
      this.searchPermissions();
    }
  }

  private initializeBspListener() {
    this.bspControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(bsp => {
      this.bspSelected = bsp;
      this.tagValue = `${this.getFiltertag()} - ${bsp?.isoCountryCode} ${bsp?.name}`;
    });
  }

  private getFiltertag() {
    return this.translationService.translate('subUsers.profile.permissionsTab.filterTag');
  }
}
