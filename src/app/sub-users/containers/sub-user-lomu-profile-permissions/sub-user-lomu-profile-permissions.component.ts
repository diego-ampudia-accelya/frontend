import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { Permissions } from '~app/shared/constants/permissions';
import { SubUsersLomuProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUsersLomuChangesService } from '~app/sub-users/shared/sub-users-lomu-changes.service';

@Component({
  selector: 'bspl-sub-user-lomu-profile-permissions',
  templateUrl: './sub-user-lomu-profile-permissions.component.html',
  styleUrls: ['./sub-user-lomu-profile-permissions.component.scss']
})
export class SubUserLomuPermissionsComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromSubUsers.areLomuProfilePermissionsLoading));
  public permissions$ = this.store.pipe(select(fromSubUsers.getOriginalLomuProfilePermissions), map(cloneDeep));
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public Permissions = Permissions;
  public canReadLomuPermissions = false;

  public canEditPermissions = this.permissionService.hasPermission(Permissions.updateLomuPerm);

  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private changesService: SubUsersLomuChangesService,
    private permissionService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.canReadLomuPermissions = this.permissionService.hasPermission(Permissions.readLomuPerm);
  }

  public onPermissionChange(modifications: SelectionList[]) {
    this.store.dispatch(SubUsersLomuProfilePermissionsActions.change({ modifications: cloneDeep(modifications) }));
  }

  public canDeactivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromSubUsers.canApplyLomuProfilePermissionsChanges),
      switchMap(hasChanges => (hasChanges ? this.changesService.handlePermissionsChangeAndDeactivate$ : of(true))),
      first()
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
