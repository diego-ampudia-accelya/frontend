/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { Permissions } from '~app/shared/constants/permissions';
import { AlertMessageType } from '~app/shared/enums';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserLomu, SubUserType } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-user-lomu-properties',
  templateUrl: './sub-user-lomu-properties.component.html',
  styleUrls: ['./sub-user-lomu-properties.component.scss']
})
export class SubUserLomuPropertiesComponent implements OnInit, OnDestroy {
  @ViewChild(AccordionComponent, { static: true }) accordion: AccordionComponent;
  @ViewChildren(AccordionItemComponent) accordionSections: QueryList<AccordionItemComponent>;

  public Permissions = Permissions;
  public subUserLomu: SubUserLomu;
  public SubUserType = SubUserType;
  public alertMessageType = AlertMessageType.info;
  public userTypeLabel: string;
  public statusLabel: string;
  public templateLabel: string;
  public codeNameLabel: string;
  public codeNameValue: string;

  public loading$ = this.store.pipe(select(fromSubUsers.getLomuProfileLoading));

  private subUserLomu$ = this.store.pipe(select(fromSubUsers.getLomuProfileSubUser));

  private destroyed$ = new Subject();

  constructor(private store: Store<AppState>) {}

  public ngOnInit(): void {
    this.subUserLomu$.pipe(takeUntil(this.destroyed$)).subscribe(subUserLomu => {
      this.subUserLomu = subUserLomu;

      this.initializeSubUser();
    });
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private initializeSubUser(): void {
    if (this.subUserLomu) {
      this.userTypeLabel = `subUsers.profile.propertiesTab.labels.users.${this.subUserLomu.userType}`;
      this.statusLabel = `subUsers.profile.propertiesTab.labels.statuses.${this.subUserLomu.status}`;
      this.templateLabel = `subUsers.profile.propertiesTab.labels.templates.${this.subUserLomu.template}`;
      this.codeNameValue = `${this.subUserLomu?.userTypeDetails?.iataCode} / ${this.subUserLomu?.userTypeDetails?.name}`;
    }
  }
}
