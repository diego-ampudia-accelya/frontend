import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '~app/reducers';
import { DialogConfig } from '~app/shared/components';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUsersChangedPermissions } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-save-sub-user-permissions-dialog',
  templateUrl: './save-sub-users-permissions-dialog.component.html',
  styleUrls: ['./save-sub-users-permissions-dialog.component.scss']
})
export class SaveSubUsersPermissionsDialogComponent {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public BadgeInfoType = BadgeInfoType;
  public changedPermissions$: Observable<SubUsersChangedPermissions[]> = this.store.pipe(
    select(fromSubUsers.getChangedPermissions)
  );
  public checkboxStatusTranslationNs = 'subUsers.profile.propertiesTab.checkboxStatusText.';

  constructor(public config: DialogConfig, private store: Store<AppState>) {}
}
