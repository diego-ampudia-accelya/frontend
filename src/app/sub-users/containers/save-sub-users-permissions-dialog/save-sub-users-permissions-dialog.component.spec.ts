import { TitleCasePipe } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { SaveSubUsersPermissionsDialogComponent } from './save-sub-users-permissions-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { getInitialSubUsersState } from '~app/sub-users/mocks/sub-users.test.mocks';
import { TranslatePipeMock } from '~app/test';

describe('SaveSubUsersPermissionsDialogComponent', () => {
  let component: SaveSubUsersPermissionsDialogComponent;
  let fixture: ComponentFixture<SaveSubUsersPermissionsDialogComponent>;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'subUsers.editUser.titleApply',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Apply }],
        buttons: [FooterButton.Cancel, FooterButton.Apply],
        description: 'subUsers.permissionsScreen.dialog.descriptionPermissionsApply'
      }
    } as DialogConfig;
    TestBed.configureTestingModule({
      declarations: [SaveSubUsersPermissionsDialogComponent, TranslatePipeMock, TitleCasePipe],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        provideMockStore({ initialState: getInitialSubUsersState() }),
        mockProvider(L10nTranslationService)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveSubUsersPermissionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
