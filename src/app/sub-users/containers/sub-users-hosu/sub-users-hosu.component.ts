import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { User } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-sub-users-hosu',
  templateUrl: './sub-users-hosu.component.html',
  styleUrls: ['./sub-users-hosu.component.scss']
})
export class SubUsersHosuComponent implements OnInit {
  public tabs: RoutedMenuItem[];

  public loggedUser$: Observable<User> = this.store.pipe(select(getUser));

  constructor(
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    protected store: Store<AppState>
  ) {}

  ngOnInit() {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }
}
