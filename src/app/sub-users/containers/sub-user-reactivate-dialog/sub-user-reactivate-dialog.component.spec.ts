import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { SubUserReactivateDialogComponent } from './sub-user-reactivate-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { SubUserStatusActions } from '~app/sub-users/actions';
import { getInitialSubUsersState, getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import { SubUserStatus } from '~app/sub-users/shared/sub-users.models';
import { TranslatePipeMock } from '~app/test';

describe('SubUserReactivateDialogComponent', () => {
  let component: SubUserReactivateDialogComponent;
  let fixture: ComponentFixture<SubUserReactivateDialogComponent>;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'subUsers.toggleStatusDialogs.reactivateUserTitle',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: FooterButton.Reactivate,
        subUser: getTestSubUser(),
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Reactivate
          }
        ]
      }
    } as DialogConfig;
    TestBed.configureTestingModule({
      declarations: [SubUserReactivateDialogComponent, TranslatePipeMock],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        provideMockStore({ initialState: getInitialSubUsersState() }),
        mockProvider(L10nTranslationService),
        FormBuilder,
        ReactiveSubject
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUserReactivateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('reactivateButton click', () => {
    it('and form is valid, should dispatch  SubUserStatusActions.reactivateSubUser', fakeAsync(() => {
      const testSubUser = getTestSubUser();
      component.subUser = testSubUser;
      const store = TestBed.inject(MockStore);
      spyOn(store, 'dispatch').and.callThrough();
      component.formData.setValue({ portalEmail: testSubUser.portalEmail });
      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit({
        clickedBtn: FooterButton.Reactivate
      });

      expect(store.dispatch).toHaveBeenCalledWith(SubUserStatusActions.formStatusChanged({ status: 'VALID' }));

      expect(store.dispatch).toHaveBeenCalledWith(
        SubUserStatusActions.reactivateSubUser({
          id: component.subUser.id,
          reactivateData: {
            portalEmail: testSubUser.portalEmail,
            template: testSubUser.template,
            status: SubUserStatus.Active
          }
        })
      );
    }));

    it('not clicked, should not dispatch  SubUserStatusActions.reactivateSubUser', fakeAsync(() => {
      const store = TestBed.inject(MockStore);
      spyOn(store, 'dispatch').and.callThrough();
      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit();

      expect(store.dispatch).not.toHaveBeenCalled();
    }));
  });

  describe('initializeFormData', () => {
    it('portalEmail should be set if subUser is provided', () => {
      expect(component.formData.value.portalEmail).toBe('frank_right@continental.airlines.com');
    });

    it('portalEmail should be undefined, if subUser is null', () => {
      component.subUser = null;
      component.ngOnInit();
      expect(component.formData.value.portalEmail).toBe(null);
    });
  });
});
