import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { SubUserStatusActions } from '~app/sub-users/actions';
import * as fromSubUser from '~app/sub-users/reducers';
import { SubUser, SubUserStatus, SubUserType, SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-user-reactivate-dialog',
  templateUrl: './sub-user-reactivate-dialog.component.html',
  styleUrls: ['./sub-user-reactivate-dialog.component.scss']
})
export class SubUserReactivateDialogComponent implements OnInit, OnDestroy {
  public subUser: SubUser = this.config.data.subUser;
  public subUserTypeByParent: SubUserTypeByParent = this.config.data.subUserTypeByParent;
  public reactivateInfoType: boolean = this.config.data.reactivateInfoType;
  public isHosuView = this.subUserTypeByParent === SubUserTypeByParent.hosu;
  public formData: FormGroup;
  public isUserAirline: boolean;

  public loading$;
  public isFormValid$;
  public reactivateButtonClick$;
  public closeButtonClick$;

  public get templateTranslation() {
    return this.translationService.translate(`subUsers.toggleStatusDialogs.templates.${this.subUser.template}`);
  }

  private reactivateButton: ModalAction;
  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    public store: Store<AppState>,
    private dialog: DialogService
  ) {}

  public ngOnInit(): void {
    this.isUserAirline = this.subUser?.userType === SubUserType.Airline;
    this.initializeObservables();
    this.initializeFormData();
    this.initializeSaveButton();
    this.initializeCloseButton();
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  private initializeObservables(): void {
    this.loading$ = this.store.pipe(
      select(fromSubUser.getSubUsersStatusDialogLoadingState),
      tap(loading => this.setLoading(loading)),
      takeUntil(this.destroy$)
    );
    this.isFormValid$ = this.store.pipe(
      select(fromSubUser.getSubUsersStatusDialogIsFormValidState),
      tap(isFormValid => this.setDisabled(!isFormValid)),
      takeUntil(this.destroy$)
    );
    this.reactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
      filter((res: any) => res?.clickedBtn === FooterButton.Reactivate),
      takeUntil(this.destroy$)
    );

    this.closeButtonClick$ = this.reactiveSubject.asObservable.pipe(
      filter((res: any) => res?.clickedBtn === FooterButton.Close),
      takeUntil(this.destroy$)
    );
  }

  private initializeFormData() {
    this.formData = this.formBuilder.group(
      {
        portalEmail: [this.subUser?.portalEmail, [Validators.required, Validators.maxLength(200)]]
      },
      { updateOn: 'change' }
    );

    this.formData.statusChanges
      .pipe(distinctUntilChanged(), takeUntil(this.destroy$))
      .subscribe(status => this.store.dispatch(SubUserStatusActions.formStatusChanged({ status })));
  }

  private initializeCloseButton() {
    this.closeButtonClick$.subscribe(() => this.dialog.close());
  }

  private initializeSaveButton() {
    this.reactivateButton = this.config.data.buttons.find(
      (button: ModalAction) => button.type === FooterButton.Reactivate
    );

    this.loading$.subscribe();
    this.isFormValid$.subscribe();

    this.reactivateButtonClick$.subscribe(() => {
      const id = this.subUser.id;
      if (this.isHosuView) {
        const { allCountries, countries } = this.subUser;
        const reactivateHosuData = {
          portalEmail: this.formData.value.portalEmail,
          status: SubUserStatus.Active,
          allCountries,
          countries
        };
        this.store.dispatch(SubUserStatusActions.reactivateHosuSubUser({ subUserId: id, reactivateHosuData }));
      } else {
        const { template } = this.subUser;
        const reactivateData = {
          portalEmail: this.formData.value.portalEmail,
          status: SubUserStatus.Active,
          template
        };
        this.store.dispatch(SubUserStatusActions.reactivateSubUser({ id, reactivateData }));
      }
    });
  }

  private setLoading(loading: boolean) {
    if (this.reactivateButton) {
      this.reactivateButton.isLoading = loading;
    }
  }

  private setDisabled(disabled: boolean) {
    if (this.reactivateButton) {
      this.reactivateButton.isDisabled = disabled;
    }
  }
}
