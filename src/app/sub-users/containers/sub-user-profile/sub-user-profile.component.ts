import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, iif, merge, Observable, of, Subject } from 'rxjs';
import { filter, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { or } from '~app/shared/utils';
import { SubUsersProfileActions, SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Component({
  selector: 'bspl-sub-user-profile',
  templateUrl: './sub-user-profile.component.html',
  styleUrls: ['./sub-user-profile.component.scss']
})
export class SubUserProfileComponent implements OnInit, OnDestroy {
  public userPortalEmail$: Observable<any>;

  public tabs: RoutedMenuItem[];

  public isApplyChangesEnabled$: Observable<boolean>;

  public currentSavableTab = null;

  public buttonLabel: string;

  private isCreatingHosu$ = this.store.pipe(select(fromSubUsers.getProfileIsCreatingHosu));

  private loggedUser$ = this.store.pipe(select(getUser), first());

  private destroyed$ = new Subject();

  private savableTabs = [
    {
      url: './properties',
      canApplyChangesSelector: fromSubUsers.canApplyPropertyChanges,
      applyChangesAction: SubUsersProfileActions.applyPropertyChanges()
    },
    {
      url: './permissions',
      canApplyChangesSelector: fromSubUsers.canApplyProfilePermissionsChanges,
      applyChangesAction: SubUsersProfilePermissionsActions.openApplyChanges()
    }
  ];

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder
  ) {}

  public applyChanges() {
    this.store.dispatch(this.currentSavableTab.applyChangesAction);
  }

  public ngOnInit(): void {
    this.getUserPortalEmail();

    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab.url === savableTab.url)
        )
      )
    ).pipe(map(values => or(...values)));
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));
    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        tap(activeSavableTab => {
          this.currentSavableTab = activeSavableTab;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private getUserPortalEmail() {
    this.userPortalEmail$ = this.isCreatingHosu$.pipe(
      tap(isCreating => {
        this.buttonLabel = isCreating ? 'common.create' : 'common.applyChanges';
      }),
      switchMap(isCreating =>
        iif(
          () => isCreating,
          this.loggedUser$.pipe(map(loggedUser => loggedUser.email)),
          this.store.pipe(select(fromSubUsers.getProfileSubUserPortalEmail))
        )
      ),
      takeUntil(this.destroyed$)
    );
  }
}
