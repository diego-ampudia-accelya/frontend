import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { MenuBuilder } from '~app/master-data/configuration';
import { SubUsersProfileActions } from '~app/sub-users/actions';
import { getInitialSubUsersState } from '~app/sub-users/mocks/sub-users.test.mocks';
import * as fromSubUsers from '~app/sub-users/reducers';
import { TranslatePipeMock } from '~app/test';
import { SubUserProfileComponent } from './sub-user-profile.component';

let routerMock;

describe('SubUserProfileComponent', () => {
  let component: SubUserProfileComponent;
  let fixture: ComponentFixture<SubUserProfileComponent>;
  let mockStore: MockStore;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/sub-users', './sub-users')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './sub-users',
      createUrlTree: () => {},
      isActive: (res = false) => res,
      isChanged: true
    };

    TestBed.configureTestingModule({
      declarations: [SubUserProfileComponent, TranslatePipeMock],
      providers: [
        provideMockStore({ initialState: getInitialSubUsersState() }),
        { provide: Router, useValue: routerMock },
        mockProvider(ActivatedRoute),
        mockProvider(MenuBuilder)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUserProfileComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('applyChanges should dispatch applyChangesAction', () => {
    component.currentSavableTab = {
      url: './properties',
      canApplyChangesSelector: fromSubUsers.canApplyPropertyChanges,
      applyChangesAction: SubUsersProfileActions.applyPropertyChanges()
    };
    component.applyChanges();
    expect(mockStore.dispatch).toHaveBeenCalledWith(SubUsersProfileActions.applyPropertyChanges());
  });

  it('isApplyChangesEnabled$ returns true on selectedTab match', fakeAsync(() => {
    mockStore.overrideSelector(fromSubUsers.canApplyPropertyChanges, true);
    component.currentSavableTab = {
      url: './properties',
      canApplyChangesSelector: fromSubUsers.canApplyPropertyChanges,
      applyChangesAction: SubUsersProfileActions.applyPropertyChanges()
    };

    let result: boolean;

    component.isApplyChangesEnabled$.subscribe(enabledResult => (result = enabledResult));

    expect(result).toBeTruthy();
  }));
});
