/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { filter, first, switchMap, takeUntil, withLatestFrom } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { Permissions } from '~app/shared/constants/permissions';
import { AlertMessageType, SortOrder } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { toSubUserUpdateModel } from '~app/shared/helpers/user.helper';
import { DropdownOption, GridColumn, ResponseErrorBE } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PaginationInfo } from '~app/shared/models/pagination-info.model';
import { SortingObject } from '~app/shared/models/sorting-object.model';
import { AirlineUser, User, UserType } from '~app/shared/models/user.model';
import { SubUsersProfileActions, SubUserStatusActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserPropertyChangesService } from '~app/sub-users/shared/sub-user-property-changes.service';
import {
  CountriesAccessPermissions,
  SubUser,
  SubUserEditable,
  SubUserStatus,
  SubUserTemplate,
  SubUserType,
  SubUserTypeByParent,
  SubUserUpdateModel
} from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-user-properties',
  templateUrl: './sub-user-properties.component.html',
  styleUrls: ['./sub-user-properties.component.scss']
})
export class SubUserPropertiesComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  @ViewChild(AccordionComponent, { static: true }) accordion: AccordionComponent;
  @ViewChildren(AccordionItemComponent) accordionSections: QueryList<AccordionItemComponent>;

  public Permissions = Permissions;
  public subUser: SubUser;
  public form: FormGroup;
  public SubUserType = SubUserType;
  public alertMessageType = AlertMessageType.info;
  public paging: PaginationInfo = { size: 20, page: 0 };
  public isMultiCountryUser = false;
  public countriesColumns: Array<GridColumn> = [];
  public isCheckboxChanged = false;
  public isAllCountriesChecked = false;
  public isAllCountriesDisabled = false;
  public isUserActive = false;
  public canEditTemplate = false;
  public userTypeLabel: string;
  public statusLabel: string;
  public templateLabel: string;
  public codeNameLabel: string;
  public codeNameValue: string;

  public templateOptions: DropdownOption[] = [
    {
      value: SubUserTemplate.Efficient,
      label: this.translationService.translate('subUsers.templateTypes.efficient')
    },
    {
      value: SubUserTemplate.Streamlined,
      label: this.translationService.translate('subUsers.templateTypes.streamlined')
    }
  ];

  public loading$ = this.store.pipe(select(fromSubUsers.getProfileLoading));

  public totalItemsMessage: string;

  public sorting: Array<SortingObject> = [{ attribute: 'isoCountryCode', sortType: SortOrder.Asc }];

  public isCreatingHosu = false;

  public isTemplateLocked = false;

  public showCodeName = true;

  private formUtil: FormUtil;

  private loggedUser: User;

  private usersBSPs: Bsp[] = [];
  private countries: CountriesAccessPermissions[] = [];
  private subUserTypeByParent = SubUserTypeByParent.losu;

  private hasChanges$ = this.store.pipe(select(fromSubUsers.canApplyPropertyChanges));
  private subUser$ = this.store.pipe(select(fromSubUsers.getProfileSubUser));

  private storedFormValue$ = this.store.pipe(select(fromSubUsers.getPropertiesFormValue));

  private isCreatingHosu$ = this.store.pipe(select(fromSubUsers.getProfileIsCreatingHosu), first());

  private loggedUser$ = this.store.pipe(select(getUser), first());

  private subUserRequestError$ = this.store.pipe(
    select(fromSubUsers.getProfileSubUserPropertiesError),
    filter(error => !!error)
  );

  private destroyed$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private store: Store<AppState>,
    private subUserPropertyChangesService: SubUserPropertyChangesService,
    private permissionService: PermissionsService
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeCountriesColumns();

    this.loggedUser$
      .pipe(withLatestFrom(this.isCreatingHosu$, this.subUser$, this.storedFormValue$), takeUntil(this.destroyed$))
      .subscribe(([loggedUser, isCreatingHosu, subUser, formValue]) => {
        const hasLeanPermission = loggedUser.permissions.some(perm => perm === Permissions.lean);

        this.loggedUser = loggedUser;
        this.usersBSPs = loggedUser.bsps;
        this.isMultiCountryUser = loggedUser.bsps.length > 1 || hasLeanPermission;
        this.isCreatingHosu = isCreatingHosu;
        this.subUser = subUser;

        this.initializeSubUser();
        this.initializeForm();

        if (this.isMultiCountryUser) {
          this.initializeValuesForMultiCountryUser();
        }

        if (this.form && !this.isCreatingHosu) {
          this.setFormValue(formValue);
        }

        this.initializeCodeNameLabel();
        this.initializeCodeNameValue();
      });
  }

  public changeUserStatus(): void {
    this.isAllCountriesDisabled = this.countries.every(country => !country.access);

    const conditionToOpenInfoModalReactivateHosu =
      this.isMultiCountryUser && this.subUser.status === SubUserStatus.Inactive && this.isAllCountriesDisabled;

    if (conditionToOpenInfoModalReactivateHosu) {
      this.reactivateHosuUsersNoCountriesSelected();
    } else {
      const countriesToChangeStatus = !this.isAllCountriesChecked
        ? this.countries.filter(country => country.access)
        : null;
      const subUserToChangeStatus = !this.isMultiCountryUser
        ? this.subUser
        : { ...this.subUser, allCountries: this.isAllCountriesChecked, countries: countriesToChangeStatus };

      this.store.dispatch(
        SubUserStatusActions.openSubUserStatusBySubUserTypeDialog({
          subUser: subUserToChangeStatus,
          subUserTypeByParent: this.subUserTypeByParent
        })
      );
    }
  }

  public canDeactivate(): Observable<boolean> {
    this.store.dispatch(SubUsersProfileActions.propertiesDeactivationRequest());

    return this.hasChanges$.pipe(
      switchMap(hasChanges => (hasChanges ? this.subUserPropertyChangesService.getDeactivateAction() : of(true))),
      first()
    );
  }

  public onCountryCheckBoxChange(rows: CountriesAccessPermissions[]): void {
    const isAtLeastOneDeselected = rows.some(row => !row.access);
    this.isAllCountriesChecked = !isAtLeastOneDeselected;
    this.countries = rows;
    this.dispatchValueChange();
  }

  public onAllCountriesCheckBoxChange(allCountries: boolean): void {
    const changedCountries = this.countries.map(country => ({ ...country, access: allCountries }));
    this.countries = changedCountries;
    this.isAllCountriesChecked = allCountries;
    this.dispatchValueChange();
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private initializeSubUser(): void {
    if (this.subUser) {
      this.isUserActive = this.subUser.status === SubUserStatus.Active;
      // TODO: Remove this else validation when the backend sends the DPC userType in the response
      if (this.subUser.userType) {
        this.userTypeLabel = `subUsers.profile.propertiesTab.labels.users.${this.subUser.userType}`;
      } else {
        this.userTypeLabel = 'subUsers.profile.propertiesTab.labels.users.DPC';
      }
      this.statusLabel = `subUsers.profile.propertiesTab.labels.statuses.${this.subUser.status}`;
      this.templateLabel = `subUsers.profile.propertiesTab.labels.templates.${this.subUser.template}`;
      this.canEditTemplate =
        this.permissionService.hasPermission(Permissions.updateSubuserTemplate) && !this.isMultiCountryUser;
      this.isTemplateLocked = !this.isUserActive || !this.canEditTemplate;
    }
  }

  private reactivateHosuUsersNoCountriesSelected() {
    this.store.dispatch(SubUserStatusActions.openSubUserHosuInfoDialog());
    const rows = this.countries.map(country => ({
      ...country,
      editable: SubUserEditable.Enabled,
      disableSelection: false
    }));
    this.countries = rows;
    this.isAllCountriesDisabled = false;
  }

  private initializeForm(): void {
    this.form = this.formUtil.createGroup<SubUserUpdateModel>(
      {
        portalEmail: ['', [Validators.required, Validators.maxLength(200)]],
        name: ['', [Validators.required, Validators.maxLength(49)]],
        organization: ['', [Validators.required, Validators.maxLength(30)]],
        telephone: ['', [Validators.required, Validators.maxLength(15)]],
        country: ['', [Validators.required, Validators.maxLength(26)]],
        city: ['', [Validators.required, Validators.maxLength(30)]],
        address: ['', [Validators.required, Validators.maxLength(30)]],
        postCode: ['', [Validators.required, Validators.maxLength(10)]]
      },
      { updateOn: 'change' }
    );

    if (!this.isCreatingHosu) {
      this.form.addControl('id', this.formBuilder.control('', Validators.required));
    }

    if (this.canEditTemplate) {
      this.form.addControl('template', this.formBuilder.control('', Validators.required));
      this.setAndLockTemplateSelection();
    }

    this.form.valueChanges.pipe(takeUntil(this.destroyed$)).subscribe(() => this.dispatchValueChange());
    this.subUserRequestError$.pipe(takeUntil(this.destroyed$)).subscribe(({ error }) => this.setFieldErrors(error));
  }

  private setAndLockTemplateSelection() {
    const conditionForLockTemplate =
      this.userIsOfType(SubUserType.Airline) &&
      this.loggedUser.template === SubUserTemplate.Streamlined &&
      this.subUser.template === SubUserTemplate.Streamlined;
    if (conditionForLockTemplate) {
      this.templateOptions.splice(0, 1);
      this.isTemplateLocked = true;
    }
  }

  private initializeValuesForMultiCountryUser(): void {
    if (this.isCreatingHosu) {
      this.userTypeLabel = `subUsers.profile.propertiesTab.labels.users.${this.loggedUser.userType.toUpperCase()}`;
      this.isUserActive = true;
    }

    this.paging = { ...this.paging, totalElements: this.usersBSPs.length };
    this.totalItemsMessage = this.translationService.translate('ADM_ACM.files.massload.totalItemsMessage', {
      total: this.usersBSPs.length
    });
    this.subUserTypeByParent = SubUserTypeByParent.hosu;
    this.initializeCountriesAccessRows();
  }

  private initializeCountriesAccessRows(): void {
    if (!this.isCreatingHosu) {
      this.isAllCountriesChecked = this.subUser ? this.subUser.allCountries : true;
    }

    this.isAllCountriesDisabled = !this.isUserActive;
    const editable = this.isUserActive ? SubUserEditable.Enabled : SubUserEditable.Disabled;
    const disableSelection = editable === SubUserEditable.Disabled;

    const bspRows = this.usersBSPs.map(bsp => ({
      id: bsp.id,
      isoCountryCode: bsp.isoCountryCode,
      name: bsp.name,
      access: this.isAllCountriesChecked,
      editable,
      disableSelection
    }));

    let rows = bspRows;

    if (!this.isAllCountriesChecked && !this.subUser?.allCountries && this.subUser?.countries?.length) {
      const initialCountries = this.subUser.countries;
      const idsCountriesWithAccess = initialCountries.map(country => country.id);

      rows = bspRows.map(usersBSP => ({
        ...usersBSP,
        access: idsCountriesWithAccess.some(countrieId => countrieId === usersBSP.id)
      }));
    }

    this.countries = rows;
  }

  private setFormValue(formValue: SubUserUpdateModel): void {
    let formToPatch = formValue || toSubUserUpdateModel(this.subUser);

    if (this.isMultiCountryUser && this.subUser) {
      formToPatch = {
        ...formToPatch,
        allCountries: this.subUser.allCountries,
        countries: this.subUser.allCountries ? null : this.countries
      };
    }

    this.form.reset(undefined, { emitEvent: false });
    this.form.patchValue(formToPatch, { emitEvent: false });
    FormUtil.showControlState(this.form);
  }

  private dispatchValueChange(): void {
    if (!this.isUserActive) {
      return;
    }

    let isFormValid = this.form.valid;
    let formValue = this.form.value;

    if (this.isMultiCountryUser) {
      const countriesToDispatch = this.countries
        .filter(country => country.access)
        .map(country => ({
          id: country.id,
          isoCountryCode: country.isoCountryCode,
          name: country.name,
          access: country.access
        }));

      const isAtLeastOneSelected = !!countriesToDispatch.length;

      isFormValid =
        this.form.valid && (this.isAllCountriesChecked || (!this.isAllCountriesChecked && isAtLeastOneSelected));

      formValue = {
        ...formValue,
        allCountries: this.isAllCountriesChecked,
        countries: this.isAllCountriesChecked ? null : countriesToDispatch
      };
    }

    this.store.dispatch(SubUsersProfileActions.propertiesFormValueChange({ formValue, isFormValid }));
  }

  private setFieldErrors(error: ResponseErrorBE): void {
    if (error.errorCode !== 400 || !error.messages?.length) {
      return;
    }

    // Validation error
    error.messages.forEach(({ message, messageParams }) => {
      const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
      const invalidControl = this.form.get(controlName);

      if (invalidControl) {
        invalidControl.setErrors({ requestError: { message } });
        FormUtil.showControlState(invalidControl);
      }
    });
  }

  private initializeCodeNameValue() {
    if (isEmpty(this.subUser?.userTypeDetails)) {
      this.showCodeName = false;
    }

    if (!this.isCreatingHosu) {
      this.codeNameValue = `${this.subUser?.userTypeDetails?.iataCode} / ${this.subUser?.userTypeDetails?.name}`;
    }

    if (this.isCreatingHosu && this.userIsOfType(SubUserType.Airline)) {
      const userAirline = this.loggedUser as AirlineUser;
      const airlineName = userAirline.globalAirline?.airlines[0]?.localName;
      this.codeNameValue = `${userAirline?.iataCode} / ${airlineName}`;
    }
  }

  private initializeCodeNameLabel() {
    let labelToReturn = 'subUsers.profile.propertiesTab.labels.iataCode';

    if (this.userIsOfType(SubUserType.Airline)) {
      labelToReturn = 'subUsers.profile.propertiesTab.labels.airlineCodeName';
    }

    if (this.userIsOfType(SubUserType.Agent)) {
      labelToReturn = 'subUsers.profile.propertiesTab.labels.agentCodeName';
    }

    if (this.userIsOfType(SubUserType.AgentGroup)) {
      labelToReturn = 'subUsers.profile.propertiesTab.labels.agentGroupId';
    }

    this.codeNameLabel = labelToReturn;
  }

  private userIsOfType(userType: SubUserType): boolean {
    let subUserType: UserType | SubUserType = this.subUser?.userType;

    if (this.isCreatingHosu) {
      subUserType = this.loggedUser.userType;
    }

    return subUserType?.toUpperCase() === userType.toUpperCase();
  }

  private initializeCountriesColumns(): void {
    const columns: GridColumn[] = [
      {
        name: `subUsers.profile.propertiesTab.countriesColumns.isoCountryCode`,
        prop: 'isoCountryCode',
        draggable: false,
        resizeable: true,
        width: 55
      },
      {
        name: `subUsers.profile.propertiesTab.countriesColumns.name`,
        prop: 'name',
        draggable: false,
        resizeable: true,
        width: 90
      },
      {
        name: `subUsers.profile.propertiesTab.countriesColumns.access`,
        prop: 'access',
        headerTemplate: 'checkboxHeaderTmpl',
        cellTemplate: 'defaultLabeledCheckboxCellTmpl',
        cellClass: this.isChanged.bind(this),
        resizeable: true,
        width: 90
      }
    ];
    this.countriesColumns = columns;
  }

  private isChanged({ row }: { row: CountriesAccessPermissions }): { [key: string]: boolean } {
    return {
      'checkbox-control-changed': row.isChanged && !row.access,
      'checkbox-control': true
    };
  }
}
