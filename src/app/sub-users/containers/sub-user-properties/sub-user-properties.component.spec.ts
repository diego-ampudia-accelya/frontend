import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { SubUserPropertiesComponent } from './sub-user-properties.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { createAirlineMultiCountryUser, createAirlineUser } from '~app/shared/mocks/airline-user';
import { ResponseErrorBE } from '~app/shared/models';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';
import { SubUsersProfileActions, SubUserStatusActions } from '~app/sub-users/actions';
import {
  getInitialSubUsersState,
  getTestSubUser,
  getTestSubUserUpdate,
  getTestSubUserUpdateWithoutTemplate,
  getTestSubUserWithoutTemplate
} from '~app/sub-users/mocks/sub-users.test.mocks';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserPropertyChangesService } from '~app/sub-users/shared/sub-user-property-changes.service';
import {
  SubUserStatus,
  SubUserTemplate,
  SubUserType,
  SubUserTypeByParent
} from '~app/sub-users/shared/sub-users.models';
import { TranslatePipeMock } from '~app/test';

describe('SubUserPropertiesComponent', () => {
  let component: SubUserPropertiesComponent;
  let fixture: ComponentFixture<SubUserPropertiesComponent>;
  let permissionsSpy: SpyObject<PermissionsService>;
  let mockStore: MockStore<AppState>;

  const expectedMultiCountryAirlineUser = createAirlineMultiCountryUser();
  const expectedAirlineUser = createAirlineUser();

  const expectedLeanFormValue = {
    portalEmail: 'frank_right@continental.airlines.com',
    name: 'Frank Right',
    organization: 'CONTINENTAL AIRLINES',
    telephone: '284 751 8973',
    country: 'Spain',
    city: 'Madrid',
    address: '190 Lang Ridges',
    postCode: 'BY 51380',
    id: 6005
  };

  const leanCountries = [
    {
      id: 125,
      isoCountryCode: 'ES',
      name: 'Name1',
      access: true
    },
    {
      id: 126,
      isoCountryCode: 'ES',
      name: 'Name2',
      access: false
    },
    {
      id: 127,
      isoCountryCode: 'ES',
      name: 'Name3',
      access: true
    }
  ];

  const expectedLeanCountries = [
    {
      id: 125,
      isoCountryCode: 'ES',
      name: 'Name1',
      access: true
    },
    {
      id: 127,
      isoCountryCode: 'ES',
      name: 'Name3',
      access: true
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SubUserPropertiesComponent, TranslatePipeMock, EmptyPipe, DateTimeFormatPipe],
      providers: [
        FormBuilder,
        provideMockStore({
          selectors: [{ selector: fromAuth.getUser, value: expectedAirlineUser }],
          initialState: getInitialSubUsersState()
        }),
        mockProvider(SubUserPropertyChangesService),
        mockProvider(L10nTranslationService, {
          translate: (key: string) => key
        }),
        mockProvider(PermissionsService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUserPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject(MockStore);

    spyOn(mockStore, 'dispatch').and.callThrough();

    permissionsSpy = TestBed.inject<any>(PermissionsService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedAirlineUser);
  });

  it('shoult initializeCodeNameValue when isCreatingHosu', () => {
    component.isCreatingHosu = true;

    component['initializeCodeNameValue']();

    expect(component.codeNameValue).toEqual('623 / CARIBBEAN AIRWAYS');
  });

  describe('isUserActive', () => {
    it('should be false, when subUser is null', () => {
      component.subUser = null;

      expect(component.isUserActive).toBeFalsy();
    });

    it('should be true, when subUser status is Active', fakeAsync(() => {
      const subUserActive = {
        ...getTestSubUser(),
        status: SubUserStatus.Active
      };
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, subUserActive);
      component.ngOnInit();

      tick();

      expect(component.isUserActive).toBeTruthy();
    }));

    it('should be false, when subUser status is Inactive', () => {
      component.subUser = {
        ...getTestSubUser(),
        status: SubUserStatus.Inactive
      };

      expect(component.isUserActive).toBeFalsy();
    });
  });

  describe('userTypeLabel', () => {
    it('should be empty string if subUser is null', fakeAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, null);
      component.ngOnInit();

      tick();

      expect(component.userTypeLabel).toBeUndefined();
    }));

    it('should be string concatenated userType value', fakeAsync(() => {
      const subUserAirline = {
        ...getTestSubUser(),
        userType: SubUserType.Airline
      };
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, subUserAirline);
      component.ngOnInit();

      tick();

      expect(component.userTypeLabel).toBe('subUsers.profile.propertiesTab.labels.users.AIRLINE');
    }));
  });

  describe('statusLabel', () => {
    it('should be empty string if subUser is null', fakeAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, null);
      component.ngOnInit();

      tick();

      expect(component.statusLabel).toBeUndefined();
    }));

    it('should be string concatenated status value', fakeAsync(() => {
      const subUserActive = {
        ...getTestSubUser(),
        status: SubUserStatus.Active
      };

      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, subUserActive);
      component.ngOnInit();

      tick();

      expect(component.statusLabel).toBe('subUsers.profile.propertiesTab.labels.statuses.ACTIVE');
    }));
  });

  describe('templateLabel', () => {
    it('should be empty string if subUser is null', fakeAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, null);
      component.ngOnInit();

      tick();

      expect(component.templateLabel).toBeUndefined();
    }));

    it('should be string concatenated template value', fakeAsync(() => {
      const subUserStreamlined = {
        ...getTestSubUser(),
        template: SubUserTemplate.Streamlined
      };

      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, subUserStreamlined);
      component.ngOnInit();

      tick();

      expect(component.templateLabel).toBe('subUsers.profile.propertiesTab.labels.templates.STREAMLINED');
    }));
  });

  describe('codeNameLabel', () => {
    it('should return default label, if subUser is null', fakeAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, null);
      component.ngOnInit();

      tick();

      expect(component.codeNameLabel).toBe('subUsers.profile.propertiesTab.labels.iataCode');
    }));

    it('should return airlineCodeName, if subUser is airline', fakeAsync(() => {
      const airlineSubUser = {
        ...getTestSubUser(),
        userType: SubUserType.Airline
      };

      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, airlineSubUser);
      component.ngOnInit();

      tick();
      expect(component.codeNameLabel).toBe('subUsers.profile.propertiesTab.labels.airlineCodeName');
    }));

    it('should return agentCodeName, if subUser is agent', fakeAsync(() => {
      const agentSubUser = {
        ...getTestSubUser(),
        userType: SubUserType.Agent
      };

      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, agentSubUser);
      component.ngOnInit();

      tick();

      expect(component.codeNameLabel).toBe('subUsers.profile.propertiesTab.labels.agentCodeName');
    }));
  });

  describe('canDeactivate', () => {
    let subUserPropertyChangesService: SpyObject<SubUserPropertyChangesService>;

    beforeEach(() => {
      subUserPropertyChangesService = TestBed.inject<any>(SubUserPropertyChangesService);

      mockStore.overrideSelector(fromSubUsers.getProfileDeactivating, false);
    });

    it('should dispatch propertiesDeactivationRequest', () => {
      component.canDeactivate();

      expect(mockStore.dispatch).toHaveBeenCalledWith(SubUsersProfileActions.propertiesDeactivationRequest());
    });

    it('should return true if it not has any changes', fakeAsync(() => {
      component['hasChanges$'] = of(false);
      let canDeactivate;
      component.canDeactivate().subscribe(result => (canDeactivate = result));

      expect(canDeactivate).toBeTruthy();
    }));

    it('should return false if service modal return false', fakeAsync(() => {
      let canDeactivate: boolean;
      component['hasChanges$'] = of(true);
      subUserPropertyChangesService.getDeactivateAction.and.returnValue(of(false));

      component.canDeactivate().subscribe(result => (canDeactivate = result));

      expect(mockStore.dispatch).toHaveBeenCalled();

      expect(canDeactivate).toBeFalsy();
    }));
  });

  it('changeUserStatus should dispatch openSubUserStatusBySubUserTypeDialog', () => {
    const subUser = getTestSubUser();
    component.subUser = subUser;

    component.changeUserStatus();
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SubUserStatusActions.openSubUserStatusBySubUserTypeDialog({
        subUser,
        subUserTypeByParent: SubUserTypeByParent.losu
      })
    );
  });

  it('should include template in form value when the user can edit templates', fakeAsync(() => {
    const expectedValue = getTestSubUserUpdate();
    permissionsSpy.hasPermission.and.returnValue(true);

    mockStore.overrideSelector(fromSubUsers.getProfileSubUser, getTestSubUser());
    component.ngOnInit();

    tick();

    expect(component.form.value).toEqual(expectedValue);
  }));

  it('should exclude template in form value when the user cannot edit templates', fakeAsync(() => {
    mockStore.overrideSelector(fromSubUsers.getProfileSubUser, getTestSubUser());
    component.ngOnInit();

    tick();
    const expectedValue = getTestSubUserUpdateWithoutTemplate();
    permissionsSpy.hasPermission.and.returnValue(false);

    component.ngOnInit();

    expect(component.form.value).toEqual(expectedValue);
  }));

  it('should exclude template in form value when the user does not have a template', () => {
    const subUser = getTestSubUserWithoutTemplate();
    const expectedValue = getTestSubUserUpdateWithoutTemplate();
    mockStore.overrideSelector(fromSubUsers.getProfileSubUser, subUser);
    permissionsSpy.hasPermission.and.returnValue(false);

    component.ngOnInit();

    expect(component.form.value).toEqual(expectedValue);
  });

  it('should return if userType of user logged when isCreatingHosu is true', () => {
    component.subUser = getTestSubUser();
    component.isCreatingHosu = true;
    const result = component['userIsOfType'](SubUserType.AgentGroup);

    expect(result).toBeFalsy();
  });

  it('should return if userType of subuser when isCreatingHosu is false', () => {
    component.subUser = getTestSubUser();
    component.isCreatingHosu = false;
    const result = component['userIsOfType'](SubUserType.Airline);

    expect(result).toBeTruthy();
  });

  describe('setFieldErrors', () => {
    beforeEach(waitForAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, getTestSubUser());
      permissionsSpy.hasPermission.and.returnValue(true);

      component.ngOnInit();
    }));

    it('should set form field errors when a server validation error is sent with a correct field param', () => {
      component['setFieldErrors']({
        errorCode: 400,
        messages: [
          {
            message: 'Wrong template',
            messageParams: [{ name: 'fieldName', value: 'template' }]
          }
        ]
      } as ResponseErrorBE);

      expect(component.form.get('template').hasError('requestError')).toBe(true);
    });

    it('should NOT set form field errors when a server validation error is sent without a correct field param', () => {
      component.form.addControl('email', component['formBuilder'].control('email'));

      component['setFieldErrors']({
        errorCode: 400,
        messages: [
          {
            message: 'Unknown message',
            messageParams: [{ name: 'fieldName', value: 'unknown' }]
          }
        ]
      } as ResponseErrorBE);

      expect(component.form.hasError('requestError')).toBe(false);
    });

    it('should NOT set form field errors when another server error is sent', fakeAsync(() => {
      component['setFieldErrors']({ errorCode: 500 } as ResponseErrorBE);
      expect(component.form.hasError('requestError')).toBe(false);
    }));
  });

  describe('LEAN user', () => {
    beforeEach(waitForAsync(() => {
      mockStore.overrideSelector(fromAuth.getUser, expectedMultiCountryAirlineUser);
      mockStore.overrideSelector(fromSubUsers.getProfileSubUser, getTestSubUser());
      mockStore.refreshState();

      component.ngOnInit();
      component.isUserActive = true;
    }));

    it('should dispatch with countries to null if allCountries checkbox are checked ', fakeAsync(() => {
      const formValue = {
        ...expectedLeanFormValue,
        allCountries: true,
        countries: null
      };

      component.onAllCountriesCheckBoxChange(true);

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        SubUsersProfileActions.propertiesFormValueChange({
          formValue,
          isFormValid: true
        })
      );
    }));

    it('should dispatch with certain countries if this countries checkbox are checked ', fakeAsync(() => {
      const formValue = {
        ...expectedLeanFormValue,
        allCountries: false,
        countries: expectedLeanCountries
      };

      component.onCountryCheckBoxChange(leanCountries);

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        SubUsersProfileActions.propertiesFormValueChange({
          formValue,
          isFormValid: true
        })
      );
    }));
  });
});
