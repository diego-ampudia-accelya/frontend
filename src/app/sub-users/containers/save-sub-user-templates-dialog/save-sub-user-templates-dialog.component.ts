import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { DialogConfig } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import * as fromSubUser from '~app/sub-users/reducers';

@Component({
  selector: 'bspl-save-sub-user-templates-dialog',
  templateUrl: './save-sub-user-templates-dialog.component.html',
  styleUrls: ['./save-sub-user-templates-dialog.component.scss']
})
export class SaveSubUserTemplatesDialogComponent implements OnInit, OnDestroy {
  public loading$;

  private destroy$ = new Subject();

  constructor(public config: DialogConfig, private store: Store<AppState>) {}

  public ngOnInit(): void {
    this.loading$ = this.store
      .pipe(
        select(fromSubUser.getSubUserTemplatesLoading),
        tap(loading => this.setLoading(loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public setLoading(value: boolean): void {
    this.config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = value));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
