import { TitleCasePipe } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { SaveSubUserTemplatesDialogComponent } from './save-sub-user-templates-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { getInitialSubUsersState } from '~app/sub-users/mocks/sub-users.test.mocks';
import { TranslatePipeMock } from '~app/test';

describe('SaveSubUserTemplatesDialogComponent', () => {
  let component: SaveSubUserTemplatesDialogComponent;
  let fixture: ComponentFixture<SaveSubUserTemplatesDialogComponent>;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'subUsers.templatesScreen.applyDialogTitle',
        footerButtonsType: [{ type: FooterButton.Apply }],
        buttons: [
          { type: FooterButton.Cancel, isDisabled: false },
          { type: FooterButton.Apply, isDisabled: false }
        ],
        description: 'subUsers.templatesScreen.applyDialogDescription',
        modifiedUsers: []
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      declarations: [SaveSubUserTemplatesDialogComponent, TranslatePipeMock, TitleCasePipe],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        provideMockStore({ initialState: getInitialSubUsersState() })
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveSubUserTemplatesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
