/* eslint-disable @typescript-eslint/naming-convention */
import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { cloneDeep, remove } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, first, takeUntil } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { SubUsersTemplatesActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserTemplate, SubUserTemplateViewModel } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-user-templates',
  templateUrl: './sub-user-templates.component.html',
  styleUrls: ['./sub-user-templates.component.scss']
})
export class SubUserTemplatesComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  @ViewChild('tableContainer', { static: true }) tableContainer: ElementRef;

  public Permissions = Permissions;
  public form: FormGroup;
  public originalTemplatesForm: { [key: string]: SubUserTemplate } = {};
  public SubUserTemplate = SubUserTemplate;
  public subUsersTemplatesData$: Observable<SubUserTemplateViewModel[]> = this.store.pipe(
    select(fromSubUsers.getSelectedSubUsers)
  );
  public canApplyChanges$: Observable<boolean> = this.store.pipe(
    select(fromSubUsers.getSubUserTemplatesCanApplyChanges)
  );
  public subUsersTemplatesData: SubUserTemplateViewModel[] = [];
  public hasScrollbar = false;
  public getCurrentUserBsp$ = this.store.pipe(select(fromSubUsers.getCurrentUserBsp));
  public canUpdateSubuserTemplate = false;

  private destroyNavigationSubscription$ = new Subject();
  private destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private permissionsService: PermissionsService
  ) {}

  @HostListener('window:resize')
  public onResize() {
    this.setHasScrollbar();
  }

  public ngOnInit() {
    this.subUsersTemplatesData$.pipe(takeUntil(this.destroy$)).subscribe(users => {
      this.initPermissions();
      this.subUsersTemplatesData = users;
      this.setInitialFormData();
      this.onFormChange();

      setTimeout(() => this.setHasScrollbar());
    });
  }

  public canDeactivate(): Observable<boolean> {
    this.destroyNavigationSubscription$.next();

    this.canApplyChanges$.pipe(first()).subscribe(canApplyChanges => {
      if (canApplyChanges) {
        this.store.dispatch(SubUsersTemplatesActions.subUsersTemplatesOpenUnsavedChangesDialog());
      }
    });

    return this.store.pipe(
      select(fromSubUsers.getShouldContinueTemplatesNavigationState),
      filter(shouldNavigate => !!shouldNavigate),
      takeUntil(this.destroyNavigationSubscription$)
    );
  }

  public setHasScrollbar() {
    const nativeElement = this.tableContainer.nativeElement;
    this.hasScrollbar = nativeElement.scrollWidth > nativeElement.clientWidth;
  }

  setInitialFormData() {
    const allEfficient = this.subUsersTemplatesData.every(user => user.template === SubUserTemplate.Efficient);
    const allStreamlined = this.subUsersTemplatesData.every(user => user.template === SubUserTemplate.Streamlined);

    const form = {
      bulkEfficient: [''],
      bulkStreamlined: ['']
    };
    this.subUsersTemplatesData.forEach(user => {
      form[user.id] = [user.template];
      this.originalTemplatesForm[user.id] = user.template;
    });

    this.form = this.formBuilder.group(form, { updateOn: 'change' });

    setTimeout(() => {
      if (allEfficient) {
        this.form.controls['bulkEfficient'].setValue(SubUserTemplate.Efficient);
      } else if (allStreamlined) {
        this.form.controls['bulkStreamlined'].setValue(SubUserTemplate.Streamlined);
      }

      this.subUsersTemplatesData.forEach(user => {
        this.form.controls[user.id].setValue(user.template);
      });

      this.store.dispatch(
        SubUsersTemplatesActions.saveOriginalTemplatesForm({ originalTemplatesForm: cloneDeep(this.form.value) })
      );
    });
  }

  onFormChange() {
    this.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(value => {
      const items = remove(Object.keys(value), item => item !== 'bulkEfficient' && item !== 'bulkStreamlined');

      let allEfficient = true;
      let allStreamlined = true;
      items.forEach(item => {
        if (value[item] !== SubUserTemplate.Efficient) {
          allEfficient = false;
        }
        if (value[item] !== SubUserTemplate.Streamlined) {
          allStreamlined = false;
        }
      });

      if (allEfficient) {
        this.form.controls['bulkEfficient'].setValue(SubUserTemplate.Efficient, { emitEvent: false });
      } else {
        this.form.controls['bulkEfficient'].setValue('', { emitEvent: false });
      }

      if (allStreamlined) {
        this.form.controls['bulkStreamlined'].setValue(SubUserTemplate.Streamlined, { emitEvent: false });
      } else {
        this.form.controls['bulkStreamlined'].setValue('', { emitEvent: false });
      }

      this.store.dispatch(
        SubUsersTemplatesActions.saveModifiedTemplatesForm({ modifiedTemplatesForm: cloneDeep(this.form.value) })
      );
    });
  }

  saveChanges() {
    this.store.dispatch(SubUsersTemplatesActions.subUsersTemplatesOpenApplyChangesDialog());
  }

  onBulkSelection(templateType: SubUserTemplate) {
    this.subUsersTemplatesData.forEach(user => {
      this.form.controls[user.id].setValue(templateType);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroyNavigationSubscription$.next();
  }

  private initPermissions() {
    this.permissionsService.permissions$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.canUpdateSubuserTemplate = this.permissionsService.hasPermission(Permissions.updateSubuserTemplate);
    });
  }
}
