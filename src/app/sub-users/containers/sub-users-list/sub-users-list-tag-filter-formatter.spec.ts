import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { SubUsersListTagFilterFormatter } from './sub-users-list-tag-filter-formatter';
import { SubUserStatus, SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

describe('SubUsersListTagFilterFormatter', () => {
  let formatter: SubUsersListTagFilterFormatter;
  let translationServiceSpy: SpyObject<L10nTranslationService>;

  beforeEach(() => {
    translationServiceSpy = createSpyObject(L10nTranslationService);
    translationServiceSpy.translate.and.callFake((translationKey: string) => translationKey);
    formatter = new SubUsersListTagFilterFormatter(translationServiceSpy);
  });

  it('should create', () => {
    expect(formatter).toBeTruthy();
  });

  it('should format portalEmail', () => {
    const filter = { portalEmail: 'email_part' };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.EMAIL: email_part',
        keys: ['portalEmail'],
        closable: true
      }
    ]);
  });

  it('should format level', () => {
    const filter = { level: ['1', '2'] };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.LEVEL: 1,2',
        keys: ['level'],
        closable: true
      }
    ]);
  });

  it('should format name', () => {
    const filter = { name: 'test name' };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.NAME: test name',
        keys: ['name'],
        closable: true
      }
    ]);
  });

  it('should format parentName', () => {
    const filter = { parentName: 'test parent name' };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.PARENT: test parent name',
        keys: ['parentName'],
        closable: true
      }
    ]);
  });

  it('should format template', () => {
    const filter = { template: SubUserTemplate.Streamlined };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.TEMPLATE: Streamlined',
        keys: ['template'],
        closable: true
      }
    ]);
  });

  it('should format status', () => {
    const filter = { status: SubUserStatus.Active };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.STATUS: Active',
        keys: ['status'],
        closable: true
      }
    ]);
  });

  it('should format single registerDate', () => {
    const filter = { registerDate: [new Date('2020-02-20'), null] };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.REGISTER_DATE: 20/02/2020',
        keys: ['registerDate'],
        closable: true
      }
    ]);
  });

  it('should format range registerDate', () => {
    const filter = { registerDate: [new Date('2020-02-20'), new Date('2020-02-21')] };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.REGISTER_DATE: 20/02/2020 - 21/02/2020',
        keys: ['registerDate'],
        closable: true
      }
    ]);
  });

  it('should format single expiryDate', () => {
    const filter = { expiryDate: [new Date('2020-02-20'), null] };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.EXPIRY_DATE: 20/02/2020',
        keys: ['expiryDate'],
        closable: true
      }
    ]);
  });

  it('should format range expiryDate', () => {
    const filter = { expiryDate: [new Date('2020-02-20'), new Date('2020-02-21')] };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.EXPIRY_DATE: 20/02/2020 - 21/02/2020',
        keys: ['expiryDate'],
        closable: true
      }
    ]);
  });

  it('should format ilogin', () => {
    const filter = { ilogin: 'Name to login' };

    expect(formatter.format(filter)).toEqual([
      {
        label: 'SUB_USERS.LIST.LBL.LOGIN: Name to login',
        keys: ['ilogin'],
        closable: true
      }
    ]);
  });
});
