import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { BehaviorSubject } from 'rxjs';

import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { SortOrder } from '~app/shared/enums';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { SubUserCreateDialogActions, SubUsersListActions, SubUserStatusActions } from '~app/sub-users/actions';
import { SubUsersListComponent } from '~app/sub-users/containers/sub-users-list/sub-users-list.component';
import { getInitialSubUsersState, getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import * as fromSubUsers from '~app/sub-users/reducers';
import { isProcessing } from '~app/sub-users/reducers';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';
import { SubUser, SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';
import { TranslatePipeMock } from '~app/test';

describe('LOSU SubUsersListComponent', () => {
  let component: SubUsersListComponent;
  let fixture: ComponentFixture<SubUsersListComponent>;
  let mockStore: MockStore<AppState>;

  const dialogServiceSpy = createSpyObject(DialogService);
  let router: Router;

  const userBspOptions = [
    { id: 1, name: 'Australia', isoCountryCode: '', effectiveFrom: '' },
    { id: 2, name: 'Germany', isoCountryCode: '', effectiveFrom: '' },
    { id: 3, name: 'Switzerland', isoCountryCode: '', effectiveFrom: '' }
  ];

  const queryMock = {
    filterBy: { bspId: 1 },
    sortBy: [{ attribute: 'level', sortType: SortOrder.Asc }],
    paginateBy: {
      size: 20,
      totalElements: 1,
      page: 1
    }
  } as DataQuery<SubUser>;

  const activatedRouteStubLosu = {
    snapshot: {
      data: { subUserTypeByParent: SubUserTypeByParent.losu }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [SubUsersListComponent, TranslatePipeMock, HasPermissionPipe],
      providers: [
        provideMockStore({
          initialState: getInitialSubUsersState(),
          selectors: [
            { selector: getUserBsps, value: userBspOptions },
            { selector: isProcessing, value: false }
          ]
        }),
        { provide: L10N_LOCALE, useValue: { language: 'en' } },
        FormBuilder,
        mockProvider(L10nTranslationService),
        mockProvider(DefaultQueryStorage, {
          get: () => queryMock
        }),
        mockProvider(QueryableDataSource),
        mockProvider(SubUserService),
        { provide: DialogService, useValue: dialogServiceSpy },
        mockProvider(PermissionsService, {
          permissions$: new BehaviorSubject<string[]>([]),
          hasPermission: () => true
        }),
        { provide: ActivatedRoute, useValue: activatedRouteStubLosu }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUsersListComponent);
    component = fixture.componentInstance;

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();

    router = TestBed.inject<any>(Router);
    spyOn(router, 'navigate');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
    expect(component.isHosuView).toBe(false);
    expect(component.title).toBe('SUB_USERS.LIST.TITLE');
  });

  it('changeBsp should dispatch SubUsersListActions changeRequiredFilter and selectBsp', () => {
    component.changeBsp(null);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SubUsersListActions.changeRequiredFilter({
        requiredFilter: { bsp: null, userTypeDetails: { iataCode: '623', name: 'Caribbean Airlines' } }
      })
    );
    expect(mockStore.dispatch).toHaveBeenCalledWith(SubUsersListActions.selectBsp());
  });

  it('onQueryChanged should dispatch SubUsersListActions.changeQuery', () => {
    component.onQueryChanged(null);

    expect(mockStore.dispatch).toHaveBeenCalledWith(SubUsersListActions.changeQuery({ query: null }));
  });

  it('create should dispatch SubUserCreateDialogActions.openSubUserCreateDialog', () => {
    component.create();

    expect(mockStore.dispatch).toHaveBeenCalledWith(SubUserCreateDialogActions.openSubUserCreateDialog());
  });

  it('onRowLinkClick should navigate to sub-user details', () => {
    component.onRowLinkClick({ id: 123 });

    expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_DETAILS.url, 123]);
  });

  describe('onTemplateClick', () => {
    it('should navigate to /sub-users/templates', fakeAsync(() => {
      component.onTemplateClick();

      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_TEMPLATES.url]);
    }));

    it('should navigate to details, when only one item is selected', fakeAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getSelectedRows, [getTestSubUser()]);

      component.onTemplateClick();

      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_DETAILS.url, 6005]);
    }));

    it('should navigate to templates if can not update sub-user, even when only one item is selected', fakeAsync(() => {
      component.canUpdateSubuser = false;

      mockStore.overrideSelector(fromSubUsers.getSelectedRows, [getTestSubUser()]);

      component.onTemplateClick();

      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_TEMPLATES.url]);
    }));
  });

  describe('onPermissionClick', () => {
    it('should navigate to details page, when only one item is selected', fakeAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getSelectedRows, [getTestSubUser()]);

      component.onPermissionClick();

      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_DETAILS.url, 6005, 'permissions']);
    }));

    it('should dispatch SubUsersListActions.editSubUsersPermissions, when more than one item is selected', fakeAsync(() => {
      mockStore.overrideSelector(fromSubUsers.getSelectedRows, [null, null]);

      component.onPermissionClick();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        SubUsersListActions.editSubUsersPermissions({ selectedSubUsers: [null, null] })
      );
    }));
  });

  describe('onGridActionClick', () => {
    it('should navigate to user details properties tab on UserProperties', () => {
      const action: GridTableAction = {
        actionType: GridTableActionType.UserProperties,
        methodName: null
      };
      component.onGridActionClick({ action, row: getTestSubUser() });

      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_DETAILS.url, 6005]);
    });

    it('should navigate to user details permissions tab on PermissionAndTemplate', () => {
      const action: GridTableAction = {
        actionType: GridTableActionType.PermissionAndTemplate,
        methodName: null
      };
      component.onGridActionClick({
        action,
        row: getTestSubUser()
      });

      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_DETAILS.url, 6005, 'permissions']);
    });

    it('should dispatch SubUserStatusActions.openSubUserStatusDialog on DeactivateUser', () => {
      const subUser = getTestSubUser();
      const subUserTypeByParent = SubUserTypeByParent.losu;
      const action: GridTableAction = {
        actionType: GridTableActionType.DeactivateUser,
        methodName: null
      };
      component.subUserTypeByParent = subUserTypeByParent;
      component.onGridActionClick({ action, row: subUser });

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        SubUserStatusActions.openSubUserStatusBySubUserTypeDialog({ subUser, subUserTypeByParent })
      );
    });

    it('should dispatch SubUserStatusActions.openSubUserStatusDialog on ReactivateUser', () => {
      const subUser = getTestSubUser();
      const action: GridTableAction = {
        actionType: GridTableActionType.ReactivateUser,
        methodName: null
      };
      component.onGridActionClick({ action, row: subUser });

      expect(mockStore.dispatch).toHaveBeenCalledWith(SubUserStatusActions.openSubUserStatusDialog({ subUser }));
    });
  });

  it('onSelectedRows should dispatch SubUsersListActions.changeSelectedRows', () => {
    const subUsers = [getTestSubUser()];
    component.onSelectedRows(subUsers);

    expect(mockStore.dispatch).toHaveBeenCalledWith(SubUsersListActions.changeSelectedRows({ data: subUsers }));
  });

  it('clearSelectedRows should dispatch SubUsersListActions.clearSelectedRows', () => {
    component.clearSelectedRows();

    expect(mockStore.dispatch).toHaveBeenCalledWith(SubUsersListActions.clearSelectedRows());
  });

  it('enableSelection column should not be added if enableSelection is false ', () => {
    spyOnProperty(component, 'enableSelection').and.returnValue(false);

    component.ngOnInit();

    expect(component.columns.find(col => col.prop === 'isSelected')).toBeFalsy();
  });

  it('portalEmail should be link, if can update the sub-user', fakeAsync(() => {
    const permissionsService = TestBed.inject<any>(PermissionsService);
    permissionsService.hasPermission = () => true;

    permissionsService.permissions$.next(['']);

    expect(component.columns.find(col => col.prop === 'portalEmail').cellTemplate).toBe('commonLinkFromObjCellTmpl');
  }));

  it('portalEmail should not be link, if can not update the sub-user', fakeAsync(() => {
    const permissionsService = TestBed.inject<any>(PermissionsService);
    permissionsService.hasPermission = () => false;

    permissionsService.permissions$.next(['']);

    expect(component.columns.find(col => col.prop === 'portalEmail').cellTemplate).not.toBe('linkCellTmplWithObj');
  }));

  it('download ', fakeAsync(() => {
    const downloadQuery = {
      ...defaultQuery
    };

    const data = {
      title: null,
      footerButtonsType: FooterButton.Download,
      downloadQuery
    };

    dialogServiceSpy.open.and.callThrough();
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(DownloadFileComponent, {
      data,
      apiService: component['subUserService']
    });
  }));

  describe('HosuView behavior', () => {
    beforeEach(() => {
      component.isHosuView = true;
    });

    it('should navigate if HOSU view ', fakeAsync(() => {
      component.create();
      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_HOSU_CREATE.url]);
    }));

    it('should navigate to HOSU detail route if HOSU view ', fakeAsync(() => {
      component.onRowLinkClick({ id: 123 });
      expect(router.navigate).toHaveBeenCalledWith([ROUTES.SUB_USER_HOSU_DETAILS.url, 123]);
    }));
  });
});
