import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { EQ, IN } from '~app/shared/constants/operations';
import { Permissions } from '~app/shared/constants/permissions';
import { dateMapper, joinMapper } from '~app/shared/helpers';
import { GridColumn } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { SubUserEditable, SubUserStatus, SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

@Injectable({
  providedIn: 'root'
})
export class SubUsersListConfig {
  public filterMappers = {
    portalEmail: {
      attribute: 'portalEmail',
      operation: EQ
    },
    level: {
      attribute: 'level',
      operation: IN,
      valueMapper: joinMapper
    },
    name: {
      attribute: 'name',
      operation: EQ
    },
    parent: {
      attribute: 'parent.name',
      operation: EQ
    },
    template: {
      attribute: 'template',
      operation: EQ
    },
    status: {
      attribute: 'status',
      operation: EQ
    },
    registerDate: {
      attribute: 'registerDate',
      operation: EQ,
      valueMapper: dateMapper
    },
    expiryDate: {
      attribute: 'expiryDate',
      operation: EQ,
      valueMapper: dateMapper
    }
  };

  public actions = [
    // Action available only for LOSU User
    {
      action: GridTableActionType.UserProperties,
      disabled: row => row.editable && row.editable === SubUserEditable.Disabled,
      requiredPermission: Permissions.updateSubuser
    },
    {
      action: GridTableActionType.PermissionAndTemplate,
      disabled: row => row.editable && row.editable === SubUserEditable.Disabled,
      requiredPermission: Permissions.readSubuserPermissions
    },
    {
      action: GridTableActionType.DeactivateUser,
      hidden: row => row.status === SubUserStatus.Active,
      disabled: row => row.editable && row.editable === SubUserEditable.Disabled,
      requiredPermission: Permissions.updateSubuser
    },
    {
      action: GridTableActionType.ReactivateUser,
      hidden: row => row.status === SubUserStatus.Inactive,
      disabled: row => row.editable && row.editable === SubUserEditable.Disabled,
      requiredPermission: Permissions.updateSubuser
    },
    // Action available only for HOSU User
    {
      action: GridTableActionType.UserProperties,
      disabled: row => row.editable && row.editable === SubUserEditable.Disabled,
      requiredPermission: Permissions.updateHosuSubuser
    },
    {
      action: GridTableActionType.PermissionAndTemplate,
      disabled: row => row.editable && row.editable === SubUserEditable.Disabled,
      requiredPermission: Permissions.readSubuserHosuPermissions
    },
    {
      action: GridTableActionType.DeactivateUser,
      hidden: row => row.status === SubUserStatus.Active && !row.expiryDate,
      requiredPermission: Permissions.updateHosuSubuser
    },
    {
      action: GridTableActionType.ReactivateUser,
      hidden: row => row.status === SubUserStatus.Inactive || (row.status === SubUserStatus.Active && row.expiryDate),
      disabled: row => row.editable && row.editable === SubUserEditable.Disabled,
      requiredPermission: Permissions.updateHosuSubuser
    }
  ];

  public statusOptions = [
    { value: SubUserStatus.Active, label: this.translationService.translate('SUB_USERS.LIST.LBL.ACTIVE') },
    { value: SubUserStatus.Inactive, label: this.translationService.translate('SUB_USERS.LIST.LBL.INACTIVE') }
  ];

  public templateOptions = [
    { value: SubUserTemplate.Efficient, label: this.translationService.translate('SUB_USERS.LIST.LBL.EFFICIENT') },
    { value: SubUserTemplate.Streamlined, label: this.translationService.translate('SUB_USERS.LIST.LBL.STREAMLINED') }
  ];

  public levelOptions = [
    {
      value: '1',
      label: this.translationService.translate('SUB_USERS.LIST.LBL.LEVEL_1')
    },
    {
      value: '2',
      label: this.translationService.translate('SUB_USERS.LIST.LBL.LEVEL_2')
    },
    {
      value: '3',
      label: this.translationService.translate('SUB_USERS.LIST.LBL.LEVEL_3')
    }
  ];

  constructor(private translationService: L10nTranslationService, private permissionsService: PermissionsService) {}

  columns = (isHosuUser: boolean): GridColumn[] => [
    {
      prop: 'ilogin',
      name: 'SUB_USERS.LIST.TH.LOGIN',
      minWidth: 300,
      hidden: !isHosuUser
    },
    {
      prop: 'portalEmail',
      name: 'SUB_USERS.LIST.TH.EMAIL',
      minWidth: 300
    },
    {
      prop: 'level',
      name: 'SUB_USERS.LIST.TH.LEVEL',
      width: 50,
      hidden: isHosuUser
    },
    {
      prop: 'name',
      name: 'SUB_USERS.LIST.TH.NAME'
    },
    {
      prop: 'parent.name',
      name: 'SUB_USERS.LIST.TH.PARENT',
      hidden: isHosuUser
    },
    {
      prop: 'template',
      name: 'SUB_USERS.LIST.TH.TEMPLATE',
      cellTemplate: 'templateTmpl',
      width: 100,
      // Only Agent / Airline users have templates
      hidden: isHosuUser || !this.permissionsService.hasUserType([UserType.AIRLINE, UserType.AGENT])
    },
    {
      prop: 'registerDate',
      name: 'SUB_USERS.LIST.TH.REGISTER_DATE',
      cellTemplate: 'dayMonthYearCellTmpl',
      width: 100
    },
    {
      prop: 'expiryDate',
      name: 'SUB_USERS.LIST.TH.EXPIRY_DATE',
      cellTemplate: 'dayMonthYearCellTmpl',
      width: 100
    },
    {
      prop: 'status',
      name: 'SUB_USERS.LIST.TH.STATUS',
      cellTemplate: 'statusStringTmpl',
      width: 80
    }
  ];
}
