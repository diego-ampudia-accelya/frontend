import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';

import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';
import { SubUsersSearchForm } from '~app/sub-users/shared/sub-users.models';

@Injectable({
  providedIn: 'root'
})
export class SubUsersListTagFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translation: L10nTranslationService) {
    super();
  }

  public format(filter: Partial<SubUsersSearchForm>): AppliedFilter[] {
    const { portalEmail, level, name, parentName, template, registerDate, expiryDate, status, ilogin, ...rest } =
      filter;
    const formattedFilters = super.format(rest);

    if (portalEmail) {
      formattedFilters.push({
        label: this.translation.translate('SUB_USERS.LIST.LBL.EMAIL') + ': ' + portalEmail,
        keys: ['portalEmail'],
        closable: true
      });
    }

    if (level) {
      formattedFilters.push({
        label: this.translation.translate('SUB_USERS.LIST.LBL.LEVEL') + ': ' + level,
        keys: ['level'],
        closable: true
      });
    }

    if (name) {
      formattedFilters.push({
        label: this.translation.translate('SUB_USERS.LIST.LBL.NAME') + ': ' + name,
        keys: ['name'],
        closable: true
      });
    }

    if (parentName) {
      formattedFilters.push({
        label: this.translation.translate('SUB_USERS.LIST.LBL.PARENT') + ': ' + parentName,
        keys: ['parentName'],
        closable: true
      });
    }

    if (template) {
      formattedFilters.push({
        label: this.translation.translate('SUB_USERS.LIST.LBL.TEMPLATE') + ': ' + capitalize(template),
        keys: ['template'],
        closable: true
      });
    }

    if (registerDate) {
      formattedFilters.push({
        label:
          this.translation.translate('SUB_USERS.LIST.LBL.REGISTER_DATE') +
          ': ' +
          rangeDateFilterTagMapper(registerDate),
        keys: ['registerDate'],
        closable: true
      });
    }

    if (expiryDate) {
      formattedFilters.push({
        label:
          this.translation.translate('SUB_USERS.LIST.LBL.EXPIRY_DATE') + ': ' + rangeDateFilterTagMapper(expiryDate),
        keys: ['expiryDate'],
        closable: true
      });
    }

    if (status) {
      formattedFilters.push({
        label: this.translation.translate('SUB_USERS.LIST.LBL.STATUS') + ': ' + capitalize(status),
        keys: ['status'],
        closable: true
      });
    }

    if (ilogin) {
      formattedFilters.push({
        label: this.translation.translate('SUB_USERS.LIST.LBL.LOGIN') + ': ' + ilogin,
        keys: ['ilogin'],
        closable: true
      });
    }

    return formattedFilters;
  }
}
