import { Bsp } from '~app/shared/models/bsp.model';
import { SubUserStatus, SubUserTypeDetails } from '~app/sub-users/shared/sub-users.models';

export interface SubUsersListFilter {
  ilogin?: string;
  bsp: Bsp;
  userTypeDetails: SubUserTypeDetails;
  level?: string[];
  portalEmail: string;
  name: string;
  parentName: string;
  template: string;
  registerDate: Date[];
  expiryDate: Date[];
  status: SubUserStatus;
}

export interface SubUsersListRequiredFilter {
  bsp: Bsp;
  userTypeDetails: SubUserTypeDetails;
}
