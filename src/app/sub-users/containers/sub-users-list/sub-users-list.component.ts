/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Inject, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, of, Subject, zip } from 'rxjs';
import { first, map, switchMap, take, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

import { getUser, getUserBsps, getUserIataDetails } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { SelectMode } from '~app/shared/enums';
import { GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import {
  SubUserCreateDialogActions,
  SubUserHosuPermissionsActions,
  SubUsersListActions,
  SubUserStatusActions,
  SubUsersTemplatesActions
} from '~app/sub-users/actions';
import { SubUsersListFilter } from '~app/sub-users/containers/sub-users-list/sub-users-list-filter.model';
import { SubUsersListTagFilterFormatter } from '~app/sub-users/containers/sub-users-list/sub-users-list-tag-filter-formatter';
import { SubUsersListConfig } from '~app/sub-users/containers/sub-users-list/sub-users-list.config';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';
import { SubUser, SubUserType, SubUserTypeByParent, SubUserTypeDetails } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-users-list',
  templateUrl: './sub-users-list.component.html',
  styleUrls: ['./sub-users-list.component.scss']
})
export class SubUsersListComponent implements OnInit, OnDestroy {
  @ViewChild('rowDetailLosuTemplate', { static: true }) rowDetailLosuTemplate: TemplateRef<any>;
  @ViewChild('rowDetailHosuTemplate', { static: true }) rowDetailHosuTemplate: TemplateRef<any>;

  public SelectMode = SelectMode;
  public SubUserType = SubUserType;
  public customLabels = { create: 'SUB_USERS.LIST.LBL.CREATE_USER' };
  public Permissions = Permissions;
  public canUpdateSubuser = false;
  public canUpdateSubuserTemplate = false;
  public canReadSubuserPermissions = false;
  public get enableSelection(): boolean {
    return this.canReadSubuserPermissions || this.canUpdateSubuserTemplate;
  }

  public columns: GridColumn[] = [];
  public primaryActions: Array<{ action: GridTableActionType }> = [];
  public query: DataQuery<SubUsersListFilter>;

  public subUserTypeByParent: SubUserTypeByParent;
  public isHosuView: boolean;
  public title: string;
  public customDetailTemplate: TemplateRef<any>;

  public bspOptions$: Observable<DropdownOption[]> = this.store.pipe(
    select(getUserBsps),
    map(res =>
      res.map(bsp => ({
        value: bsp,
        label: bsp.name
      }))
    )
  );

  public initialBsp$ = this.store.pipe(
    select(fromSubUsers.getListRequiredFilter),
    switchMap(requiredFilter =>
      requiredFilter.bsp
        ? of(requiredFilter.bsp)
        : this.store.pipe(
            select(getUserBsps),
            map(bsps => bsps[0])
          )
    ),
    first()
  );

  public form: FormGroup;

  public loading$ = this.store.pipe(select(fromSubUsers.getListLoading));
  public query$ = this.store.pipe(
    select(fromSubUsers.getListQuery),
    tap(query => (this.query = query))
  );
  public data$ = this.store.pipe(select(fromSubUsers.getListData));
  public selectedBsps$ = this.store.pipe(select(fromSubUsers.getSelectedBsp));
  public hasData$ = this.store.pipe(select(fromSubUsers.getHasData));
  public selectedRowsCount$ = this.store.pipe(select(fromSubUsers.getSelectedRowsCount));
  public selectedSubUsersTemplates$ = this.store.pipe(select(fromSubUsers.getSelectedRowsAsSubUsersTemplatesViewModel));
  public isProcessing$ = this.store.pipe(select(fromSubUsers.isProcessing));
  public totalItems$ = this.query$.pipe(map(query => query.paginateBy.totalElements));
  public isIataUser = false;
  public isDpcUser = false;

  private userTypeDetails: SubUserTypeDetails;
  private destroy$ = new Subject();
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  constructor(
    public listConfig: SubUsersListConfig,
    public subUsersListTagFilterFormatter: SubUsersListTagFilterFormatter,
    public translationService: L10nTranslationService,
    public permissionsService: PermissionsService,
    private formBuilder: FormBuilder,
    private router: Router,
    protected store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private dialogService: DialogService,
    private subUserService: SubUserService
  ) {
    this.initializeSubUserTypeByParent();
  }

  public ngOnInit() {
    this.initPermissions();
    this.initFilterForm();
    this.initRequiredFilter();
    this.initializeDetailedViewBySubUserType();
    this.initializeUserType();
  }

  public changeBsp(bsp: Bsp) {
    const requiredFilter = { bsp, userTypeDetails: this.userTypeDetails };
    this.store.dispatch(SubUsersListActions.changeRequiredFilter({ requiredFilter }));
    this.store.dispatch(SubUsersListActions.selectBsp());
  }

  public onQueryChanged(query: DataQuery<SubUsersListFilter>): void {
    this.store.dispatch(SubUsersListActions.changeQuery({ query }));
  }

  public create() {
    if (this.isHosuView) {
      this.router.navigate([ROUTES.SUB_USER_HOSU_CREATE.url]);
    } else {
      this.store.dispatch(SubUserCreateDialogActions.openSubUserCreateDialog());
    }
  }

  public onRowLinkClick(event: any) {
    if (this.isHosuView) {
      this.router.navigate([ROUTES.SUB_USER_HOSU_DETAILS.url, event.id]);
    } else {
      this.router.navigate([ROUTES.SUB_USER_DETAILS.url, event.id]);
    }
  }

  public onTemplateClick(): void {
    this.store
      .pipe(
        select(fromSubUsers.getSelectedRows),
        withLatestFrom(this.selectedSubUsersTemplates$),
        tap(([selectedUsers, selectedUsersTemplateViewModel]) => {
          if (selectedUsers.length === 1 && this.canUpdateSubuser) {
            this.router.navigate([ROUTES.SUB_USER_DETAILS.url, selectedUsers[0].id]);
          } else {
            this.store.dispatch(
              SubUsersTemplatesActions.editUsersTemplate({ selectedSubUsers: selectedUsersTemplateViewModel })
            );
            this.router.navigate([ROUTES.SUB_USER_TEMPLATES.url]);
          }
        }),
        take(1)
      )
      .subscribe();
  }

  public onPermissionClick(): void {
    this.store
      .pipe(
        select(fromSubUsers.getSelectedRows),
        tap(selectedSubUsers => {
          if (selectedSubUsers.length === 1) {
            this.router.navigate([ROUTES.SUB_USER_DETAILS.url, selectedSubUsers[0].id, 'permissions']);
          } else {
            this.store.dispatch(SubUsersListActions.editSubUsersPermissions({ selectedSubUsers }));
          }
        }),
        take(1)
      )
      .subscribe();
  }

  public onHosuPermissionClick(): void {
    this.store.pipe(select(fromSubUsers.getSelectedRows), take(1)).subscribe(selectedSubUsers => {
      const selectedHosuIds = selectedSubUsers.map(item => item.id);
      this.store.dispatch(SubUserHosuPermissionsActions.fetchBspsFromHosuSelection({ selectedHosus: selectedHosuIds }));
      this.store.dispatch(SubUserHosuPermissionsActions.updateSelectedHosus({ selectedHosus: selectedSubUsers }));
    });
  }

  public onGridActionClick({ action, row }: { action: GridTableAction; row: SubUser }): void {
    switch (action.actionType) {
      case GridTableActionType.UserProperties:
        if (this.isHosuView) {
          this.router.navigate([ROUTES.SUB_USER_HOSU_DETAILS.url, row.id]);
        } else {
          this.router.navigate([ROUTES.SUB_USER_DETAILS.url, row.id]);
        }
        break;
      case GridTableActionType.DeactivateUser:
        this.store.dispatch(
          SubUserStatusActions.openSubUserStatusBySubUserTypeDialog({
            subUser: row,
            subUserTypeByParent: this.subUserTypeByParent
          })
        );
        break;
      case GridTableActionType.ReactivateUser:
        if (this.isHosuView) {
          this.router.navigate([ROUTES.SUB_USER_HOSU_DETAILS.url, row.id]);
        } else {
          this.store.dispatch(
            SubUserStatusActions.openSubUserStatusDialog({
              subUser: row
            })
          );
        }
        break;
      case GridTableActionType.PermissionAndTemplate:
        if (this.isHosuView) {
          this.router.navigate([ROUTES.SUB_USER_HOSU_DETAILS.url, row.id, 'permissions']);
        } else {
          this.router.navigate([ROUTES.SUB_USER_DETAILS.url, row.id, 'permissions']);
        }
        break;
    }
  }

  public onSelectedRows(users: SubUser[]): void {
    this.store.dispatch(SubUsersListActions.changeSelectedRows({ data: users }));
  }

  public clearSelectedRows(): void {
    this.store.dispatch(SubUsersListActions.clearSelectedRows());
  }

  public ngOnDestroy(): void {
    this.clearSelectedRows();
    this.destroy$.next();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('SUB_USERS.LIST.TITLE_DOWNLOAD'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.query
      },
      apiService: this.subUserService
    });
  }

  private initializeDetailedViewBySubUserType(): void {
    this.customDetailTemplate = this.isHosuView ? this.rowDetailHosuTemplate : this.rowDetailLosuTemplate;
  }

  private initializeSubUserTypeByParent(): void {
    this.subUserTypeByParent = this.activatedRoute.snapshot.data.subUserType;
    this.isHosuView = this.subUserTypeByParent === SubUserTypeByParent.hosu;
    this.title = this.isHosuView ? 'SUB_USERS.LIST.TITLE_HOSU' : 'SUB_USERS.LIST.TITLE';
  }

  private initPermissions(): void {
    const permissionsService = this.permissionsService;
    const { updateSubuser, updateSubuserTemplate, readSubuserPermissions } = Permissions;

    permissionsService.permissions$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.canUpdateSubuser = permissionsService.hasPermission(updateSubuser);
      this.canUpdateSubuserTemplate = permissionsService.hasPermission(updateSubuserTemplate);
      this.canReadSubuserPermissions = permissionsService.hasPermission(readSubuserPermissions);

      this.initGridColumns();
      this.initPrimaryActions();
    });
  }

  private initGridColumns(): void {
    this.columns = [...this.listConfig.columns(this.isHosuView)];

    if (this.enableSelection || this.isHosuView) {
      this.columns.unshift({
        prop: 'isSelected',
        sortable: false,
        maxWidth: 40,
        resizeable: false,
        headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
        cellTemplate: 'checkboxWithDisableRowCellTmpl',
        cellClass: this.isChanged.bind(this)
      });
    }

    const portalEmailColumn = this.columns.find(column => column.prop === 'portalEmail');
    portalEmailColumn.cellTemplate = this.canUpdateSubuser ? 'commonLinkFromObjCellTmpl' : undefined;
  }

  private isChanged({ row, column }): {
    'checkbox-control-changed': boolean;
    'checkbox-control': boolean;
  } {
    const { prop } = column;

    return {
      'checkbox-control-changed': row[prop] === 'isSelected',
      'checkbox-control': true
    };
  }

  private initPrimaryActions(): void {
    this.primaryActions = this.listConfig.actions.filter(({ requiredPermission }) =>
      this.permissionsService.hasPermission(requiredPermission)
    );
  }

  private initFilterForm(): void {
    this.form = this.formBuilder.group({
      ilogin: [],
      portalEmail: [],
      level: [],
      name: [],
      parentName: [],
      template: [],
      registerDate: [],
      expiryDate: [],
      status: []
    });
  }

  private initRequiredFilter(): void {
    zip(
      this.initialBsp$,
      this.store.pipe(
        select(getUserIataDetails),
        map(({ iataCode, organizationName: name }) => ({ iataCode, name }))
      )
    )
      .pipe(first())
      .subscribe(([bsp, userTypeDetails]) => {
        this.userTypeDetails = userTypeDetails;

        const requiredFilter = { bsp, userTypeDetails };
        this.store.dispatch(SubUsersListActions.changeRequiredFilter({ requiredFilter }));
      });
  }

  private initializeUserType(): void {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.isIataUser = loggedUser.userType === UserType.IATA;
      this.isDpcUser = loggedUser.userType === UserType.DPC;
    });
  }
}
