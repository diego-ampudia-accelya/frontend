import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { SubUsersPermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import {
  SubUserPermission,
  SubUserPermissionCategoryGroups,
  SubUsersPermissionsViewModel
} from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-users-permissions',
  templateUrl: './sub-users-permissions.component.html',
  styleUrls: ['./sub-users-permissions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubUsersPermissionsComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  @ViewChild('tableContainer', { static: true }) tableContainer: ElementRef;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  public Permissions = Permissions;
  public hasChanges: boolean;
  public applyButtonEnabled$: Observable<boolean> = this.store.pipe(
    select(fromSubUsers.getSubUsersPermissionsCanApplyChanges)
  );

  public subUsersPermissionsViewModel$: Observable<SubUsersPermissionsViewModel[]> = this.store.pipe(
    select(fromSubUsers.getSubUsersPermissionsViewModel)
  );

  public allSubUserPermissionsSelected$: Observable<{ [key: number]: boolean }> = this.store.pipe(
    select(fromSubUsers.allSubUserPermissionsSelected)
  );

  public allPermissionCategoriesExpanded$: Observable<boolean> = this.store.pipe(
    select(fromSubUsers.allPermissionCategoriesExpanded)
  );

  public allPermissionsSelected$: Observable<boolean> = this.store.pipe(select(fromSubUsers.allPermissionsSelected));

  public getCurrentUserBsp$ = this.store.pipe(select(fromSubUsers.getCurrentUserBsp));

  public hasScrollbar = false;

  public canEdit: boolean;

  private cancelSubscription$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private translationService: L10nTranslationService
  ) {}

  @HostListener('window:resize')
  onResize() {
    this.setHasScrollbar();
  }

  public ngOnInit(): void {
    this.canEdit = this.permissionsService.hasPermission(Permissions.updateSubuserPermissions);
    this.applyButtonEnabled$.subscribe(res => {
      this.hasChanges = res;
    });
    setTimeout(() => this.setHasScrollbar());
  }

  public ngOnDestroy(): void {
    this.cancelSubscription$.next();
  }

  public setHasScrollbar(): void {
    const nativeElement = this.tableContainer.nativeElement;
    this.hasScrollbar = nativeElement.scrollWidth > nativeElement.clientWidth;
  }

  public expandAll(): void {
    this.store.dispatch(SubUsersPermissionsActions.expandAll({ value: true }));
  }

  public collapseAll(): void {
    this.store.dispatch(SubUsersPermissionsActions.collapseAll({ value: false }));
  }

  public togglePanel(categoryId: number): void {
    this.store.dispatch(SubUsersPermissionsActions.togglePanel({ categoryId }));
  }

  public selectAll(): void {
    this.store.dispatch(SubUsersPermissionsActions.selectAll({ value: true }));
  }

  public deselectAll(): void {
    this.store.dispatch(SubUsersPermissionsActions.deselectAll({ value: false }));
  }

  public toggleByCategory(value: boolean, categoryId: number): void {
    this.store.dispatch(SubUsersPermissionsActions.toggleByCategory({ value, categoryId }));
  }

  public toggleByPermission(value: boolean, permissionId: number): void {
    this.store.dispatch(SubUsersPermissionsActions.toggleByPermission({ value, permissionId }));
  }

  public toggleByUser(userId: number, value: boolean): void {
    this.store.dispatch(SubUsersPermissionsActions.toggleByUser({ value, userId }));
  }

  public toggleByUserAndCategory(value: boolean, group: SubUserPermissionCategoryGroups): void {
    this.store.dispatch(SubUsersPermissionsActions.toggleByUserAndCategory({ value, group }));
  }

  public togglePermission(value: boolean, user: SubUserPermission): void {
    this.store.dispatch(SubUsersPermissionsActions.togglePermission({ value, user }));
  }

  public saveChanges(): void {
    this.store.dispatch(SubUsersPermissionsActions.openApplySubUsersPermissionsChangesDialog());
  }

  public getItemId(item: { id: number }): number {
    return item.id;
  }

  public getDisabledTooltip(item: { disabled: boolean }): string {
    let tooltip: string;
    if (item.disabled && this.canEdit) {
      tooltip = this.translationService.translate('subUsers.profile.permissionsTab.disabledTooltip');
    }

    return tooltip;
  }

  public canDeactivate(): Observable<boolean> {
    this.cancelSubscription$.next();
    if (this.hasChanges) {
      this.store.dispatch(SubUsersPermissionsActions.openUnsavedSubUsersPermissionsChangesDialog());
    }

    return this.store.pipe(
      select(fromSubUsers.getShouldContinuePermissionsNavigationState),
      filter(shouldNavigate => !!shouldNavigate),
      takeUntil(this.cancelSubscription$)
    );
  }
}
