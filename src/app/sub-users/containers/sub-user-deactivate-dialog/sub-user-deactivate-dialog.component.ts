import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { SubUserStatusActions } from '~app/sub-users/actions';
import * as fromSubUser from '~app/sub-users/reducers';
import { SubUser, SubUserType, SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-sub-user-deactivate-dialog',
  templateUrl: './sub-user-deactivate-dialog.component.html',
  styleUrls: ['./sub-user-deactivate-dialog.component.scss']
})
export class SubUserDeactivateDialogComponent implements OnInit, OnDestroy {
  public subUser: SubUser = this.config.data.subUser;
  public subUserTypeByParent: SubUserTypeByParent = this.config.data.subUserTypeByParent;
  public isHosuView = this.subUserTypeByParent === SubUserTypeByParent.hosu;
  public formData: FormGroup;
  public isUserAirline: boolean;
  public loading$: Observable<boolean>;
  public minimumExpiryDate = new Date();
  public deactivateButtonClick$;
  public get templateTranslation() {
    return this.translationService.translate(`subUsers.toggleStatusDialogs.templates.${this.subUser.template}`);
  }
  public messageDescription: string;

  private deactivateButton: ModalAction;

  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    public store: Store<AppState>
  ) {
    this.loading$ = store.pipe(
      select(fromSubUser.getSubUsersStatusDialogLoadingState),
      tap(loading => this.setLoading(loading)),
      takeUntil(this.destroy$)
    );

    this.deactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
      filter((res: any) => res?.clickedBtn === FooterButton.Deactivate),
      takeUntil(this.destroy$)
    );
  }

  public ngOnInit(): void {
    this.isUserAirline = this.subUser.userType === SubUserType.Airline;
    const messageLabel = this.isHosuView
      ? 'subUsers.toggleStatusDialogs.descriptionHosuDeactivate'
      : 'subUsers.toggleStatusDialogs.descriptionDeactivate';
    this.messageDescription = this.translationService.translate(messageLabel);
    if (!this.isHosuView) {
      this.initializeFormData();
    }

    this.initializeSaveButton();
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  private initializeFormData() {
    this.formData = this.formBuilder.group({
      expiryDate: [new Date(), [Validators.required]]
    });
  }

  private initializeSaveButton() {
    this.deactivateButton = this.config.data.buttons.find(
      (button: ModalAction) => button.type === FooterButton.Deactivate
    );

    this.loading$.subscribe();

    this.deactivateButtonClick$.subscribe(() => {
      if (this.isHosuView) {
        this.store.dispatch(
          SubUserStatusActions.deactivateHosuSubUser({
            subUserId: this.subUser.id
          })
        );
      } else {
        this.store.dispatch(
          SubUserStatusActions.deactivateSubUser({
            subUserId: this.subUser.id,
            expiryDate: this.formData.value.expiryDate
          })
        );
      }
    });
  }

  private setLoading(loading: boolean) {
    if (this.deactivateButton) {
      this.deactivateButton.isLoading = loading;
    }
  }
}
