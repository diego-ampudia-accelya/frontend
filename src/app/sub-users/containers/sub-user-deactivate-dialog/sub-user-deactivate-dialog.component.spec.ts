import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { SubUserDeactivateDialogComponent } from './sub-user-deactivate-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { SubUserStatusActions } from '~app/sub-users/actions';
import { getInitialSubUsersState, getTestSubUser } from '~app/sub-users/mocks/sub-users.test.mocks';
import { SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';
import { TranslatePipeMock } from '~app/test';

describe('SubUserDeactivateDialogComponent', () => {
  let component: SubUserDeactivateDialogComponent;
  let fixture: ComponentFixture<SubUserDeactivateDialogComponent>;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'subUsers.toggleStatusDialogs.deactivateUserTitle',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: FooterButton.Deactivate,
        subUserTypeByParent: SubUserTypeByParent.losu,
        subUser: getTestSubUser(),
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Deactivate
          }
        ]
      }
    } as DialogConfig;
    TestBed.configureTestingModule({
      declarations: [SubUserDeactivateDialogComponent, TranslatePipeMock],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        provideMockStore({ initialState: getInitialSubUsersState() }),
        mockProvider(L10nTranslationService),
        FormBuilder,
        ReactiveSubject
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUserDeactivateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('Sub User LOSU deactivateButton click', () => {
    it('and form is valid, should dispatch  SubUserStatusActions.deactivateSubUser', fakeAsync(() => {
      component.subUser = { ...getTestSubUser() };
      const store = TestBed.inject(MockStore);
      spyOn(store, 'dispatch').and.callThrough();
      component.formData.setValue({ expiryDate: new Date() });
      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit({
        clickedBtn: FooterButton.Deactivate
      });

      expect(store.dispatch).toHaveBeenCalledWith(
        SubUserStatusActions.deactivateSubUser({
          subUserId: component.subUser.id,
          expiryDate: component.formData.value.expiryDate
        })
      );
    }));

    it('not clicked, should not dispatch  SubUserStatusActions.deactivateSubUser', fakeAsync(() => {
      const store = TestBed.inject(MockStore);
      spyOn(store, 'dispatch').and.callThrough();
      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit();

      expect(store.dispatch).not.toHaveBeenCalled();
    }));
  });

  describe('Sub User HOSU deactivateButton click', () => {
    it('Click Deactivate button - should dispatch SubUserStatusActions.deactivateHosuSubUser', fakeAsync(() => {
      component.subUser = { ...getTestSubUser() };
      component.subUserTypeByParent = SubUserTypeByParent.hosu;
      component.isHosuView = true;
      const store = TestBed.inject(MockStore);
      spyOn(store, 'dispatch').and.callThrough();

      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit({
        clickedBtn: FooterButton.Deactivate
      });

      expect(store.dispatch).toHaveBeenCalledWith(
        SubUserStatusActions.deactivateHosuSubUser({
          subUserId: component.subUser.id
        })
      );
    }));

    it('Deactivate button not clicked, should not dispatch  SubUserStatusActions.deactivateHosuSubUser', fakeAsync(() => {
      const store = TestBed.inject(MockStore);
      component.subUserTypeByParent = SubUserTypeByParent.hosu;
      component.isHosuView = true;
      spyOn(store, 'dispatch').and.callThrough();
      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit();

      expect(store.dispatch).not.toHaveBeenCalled();
    }));
  });
});
