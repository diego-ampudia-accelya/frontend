import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { filter, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { SelectMode } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { or } from '~app/shared/utils';
import { SubUsersLomuProfileActions, SubUsersLomuProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Component({
  selector: 'bspl-sub-user-lomu-profile',
  templateUrl: './sub-user-lomu-profile.component.html',
  styleUrls: ['./sub-user-lomu-profile.component.scss']
})
export class SubUserLomuProfileComponent implements OnInit, OnDestroy {
  public userPortalEmail$: Observable<string>;

  public tabs: RoutedMenuItem[];

  public isApplyChangesEnabled$: Observable<boolean>;

  public isApplyChangesVisible = false;

  public currentSavableTab = null;

  public bspListOptions: DropdownOption<BspDto>[];

  public bspSelected: BspDto;

  public selectMode = SelectMode;

  public bspIdSelected: number;

  private loggedUser$ = this.store.pipe(select(getUser), first());

  private destroyed$ = new Subject();

  private savableTabs = [
    {
      url: './properties'
    },
    {
      url: './permissions',
      canApplyChangesSelector: fromSubUsers.canApplyLomuProfilePermissionsChanges,
      applyChangesAction: SubUsersLomuProfilePermissionsActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateLomuPerm
    }
  ];

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    protected bspsDictionaryService: BspsDictionaryService,
    private permissionsService: PermissionsService
  ) {}

  public applyChanges(): void {
    this.store.dispatch(this.currentSavableTab.applyChangesAction);
  }

  public ngOnInit(): void {
    this.initializePortalEmail();
    this.initializeLomuSubUser();

    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        savableTab.canApplyChangesSelector
          ? this.store.pipe(
              select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
              map(canSave => canSave && this.currentSavableTab === savableTab)
            )
          : of(false)
      )
    ).pipe(map(values => or(...values)));

    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        switchMap(activeSavableTab => {
          this.currentSavableTab = activeSavableTab;

          return activeSavableTab && activeSavableTab.canApplyChangesSelector
            ? of(this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission))
            : of(false);
        }),
        tap(applyChangesVisible => (this.isApplyChangesVisible = applyChangesVisible)),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public onBspSelected(bsp) {
    this.bspIdSelected = bsp.id;
    this.store.dispatch(SubUsersLomuProfileActions.requestSubUserLomu({ bspId: this.bspIdSelected }));
    this.store.dispatch(SubUsersLomuProfilePermissionsActions.requestLomuPermissions());
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private initializePortalEmail() {
    this.userPortalEmail$ = this.store.pipe(
      select(fromSubUsers.getLomuProfileSubUser),
      map(subUserLomu => subUserLomu?.portalEmail),
      takeUntil(this.destroyed$)
    );
  }

  private initializeLomuSubUser() {
    const profilePermissions = [Permissions.readLomuProfile, Permissions.readLomuPerm];
    let loggedUser: User;

    this.loggedUser$
      .pipe(
        tap(logUser => {
          loggedUser = logUser;
        }),
        switchMap(() =>
          this.bspsDictionaryService.getAllBspsByPermissions(loggedUser.bspPermissions, profilePermissions)
        )
      )
      .subscribe(bspList => {
        this.bspListOptions = bspList.map(bsp => this.toValueLabelLomuBsp(bsp));
        this.bspSelected = this.bspListOptions.find(bsp => bsp.value.isoCountryCode === loggedUser.defaultIsoc).value;
        this.bspIdSelected = this.bspSelected.id;
        this.store.dispatch(SubUsersLomuProfileActions.requestSubUserLomu({ bspId: this.bspIdSelected }));
        this.store.dispatch(SubUsersLomuProfilePermissionsActions.requestLomuPermissions());
      });
  }

  private toValueLabelLomuBsp(bsp) {
    return {
      value: bsp,
      label: `${bsp.name}`
    };
  }
}
