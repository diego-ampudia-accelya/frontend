import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { Permissions } from '~app/shared/constants/permissions';
import { SubUserHosuPermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Component({
  selector: 'bspl-sub-users-hosu-permissions',
  templateUrl: './sub-users-hosu-permissions.component.html',
  styleUrls: ['./sub-users-hosu-permissions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubUsersHosuPermissionsComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromSubUsers.areHosuPermissionsLoading));
  public permissions$ = this.store.pipe(select(fromSubUsers.getOriginalHosusPermissionsByBsps), map(cloneDeep));
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public Permissions = Permissions;
  public canReadHosuPermissions: boolean;
  public canEditPermissions = this.permissionsService.hasPermission(Permissions.updateHosuSubuserPermissions);
  private destroy$ = new Subject();

  constructor(private store: Store<AppState>, private permissionsService: PermissionsService) {}

  public ngOnInit(): void {
    this.canReadHosuPermissions = this.permissionsService.hasPermission(Permissions.readSubuserHosuPermissions);
    this.requestHosusPermissionsByBsps();
  }

  public onPermissionChange(modifications: SelectionList[]) {
    this.store.dispatch(SubUserHosuPermissionsActions.change({ modifications: cloneDeep(modifications) }));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private requestHosusPermissionsByBsps(): void {
    this.store.dispatch(SubUserHosuPermissionsActions.requestHosusPermissionsByBsps());
  }
}
