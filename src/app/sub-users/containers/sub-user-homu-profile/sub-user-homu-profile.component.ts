import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { Permissions } from '~app/shared/constants/permissions';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { TabService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { or } from '~app/shared/utils';
import { SubUserHosuPermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

@Component({
  selector: 'bspl-sub-user-homu-profile',
  templateUrl: './sub-user-homu-profile.component.html',
  styleUrls: ['./sub-user-homu-profile.component.scss']
})
export class SubUserHomuProfileComponent implements OnInit, OnDestroy {
  public tabs: RoutedMenuItem[];

  public isApplyChangesEnabled$: Observable<boolean>;

  public isApplyChangesVisible = false;

  public currentSavableTab = null;

  public buttonDesignSecondary = ButtonDesign.Secondary;

  public selectedHosus$: Observable<string> = this.store.pipe(
    select(fromSubUsers.getSelectedHosus),
    map(hosus => 'HOSUs - ' + hosus.map(hosu => hosu.portalEmail).join(', '))
  );

  public selectedHosuBsps$: Observable<string> = this.store.pipe(
    select(fromSubUsers.getSelectedHosuBsps),
    map(selectedBsps => 'BSPs - ' + selectedBsps.map(bsp => toValueLabelObjectBsp(bsp).label).join(', '))
  );

  private destroyed$ = new Subject();

  private savableTabs = [
    {
      url: './permissions',
      canApplyChangesSelector: fromSubUsers.canApplyHosuPermissionsChanges,
      cancelChangesAction: SubUserHosuPermissionsActions.openCancelChanges(),
      applyChangesAction: SubUserHosuPermissionsActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateHosuSubuserPermissions
    }
  ];

  constructor(
    protected bspsDictionaryService: BspsDictionaryService,
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    private permissionsService: PermissionsService,
    private tabService: TabService
  ) {}

  public ngOnInit(): void {
    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        savableTab.canApplyChangesSelector
          ? this.store.pipe(
              select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
              map(canSave => canSave && this.currentSavableTab === savableTab)
            )
          : of(false)
      )
    ).pipe(map(values => or(...values)));

    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        switchMap(activeSavableTab => {
          this.currentSavableTab = activeSavableTab;

          return activeSavableTab && activeSavableTab.canApplyChangesSelector
            ? of(this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission))
            : of(false);
        }),
        tap(applyChangesVisible => (this.isApplyChangesVisible = applyChangesVisible)),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public applyChanges(enabled: boolean): void {
    this.updateEnabled(enabled);

    this.store.dispatch(this.currentSavableTab.applyChangesAction);
  }

  public updateEnabled(enabled: boolean) {
    this.store.dispatch(SubUserHosuPermissionsActions.updateEnabled({ enabled }));
  }

  public cancel(): void {
    if (this.existPermissionsModifications()) {
      this.store.dispatch(this.currentSavableTab.cancelChangesAction);
    } else {
      this.tabService.closeCurrentTab();
    }
  }

  public existPermissionsModifications(): boolean {
    let hasPermissionsModifications = false;
    this.isApplyChangesEnabled$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(isApplyChangesEnabled => (hasPermissionsModifications = isApplyChangesEnabled));

    return hasPermissionsModifications;
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
