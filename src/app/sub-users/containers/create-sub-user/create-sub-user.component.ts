import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { EnquiryDialogData } from '~app/document/models/enquiry-dialog-data.model';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil } from '~app/shared/helpers';
import { ResponseErrorBE } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { UserType } from '~app/shared/models/user.model';
import { SubUserCreateDialogActions } from '~app/sub-users/actions';
import * as fromSubUser from '~app/sub-users/reducers';
import { SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

@Component({
  selector: 'bspl-create-sub-user',
  templateUrl: './create-sub-user.component.html',
  styleUrls: ['./create-sub-user.component.scss']
})
export class CreateSubUserComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;

  public templatesDropdown: DropdownOption[] = [
    { value: SubUserTemplate.Efficient, label: this.translationService.translate('subUsers.templateTypes.efficient') },
    {
      value: SubUserTemplate.Streamlined,
      label: this.translationService.translate('subUsers.templateTypes.streamlined')
    }
  ];

  public isTemplateLocked = false;
  public canShowCreationInfo = true;
  public loading$;
  public createSubUserError$;
  public createButtonClick$;

  public dialogData: EnquiryDialogData = this.config.data as EnquiryDialogData;

  public canEditTemplate = this.permissionService.hasPermission(Permissions.updateSubuserTemplate);

  private loggedUser$ = this.store.pipe(select(getUser), first());

  private destroy$ = new Subject();

  private createButton: ModalAction;

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    public store: Store<AppState>,
    private permissionService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.initializeObservables();
    this.setInitialFormData();
    this.initializeCreateButton();
    this.initializeFormListeners();
    this.initializeUserForTemplate();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      this.createButton.isDisabled = status !== 'VALID';
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeObservables(): void {
    this.loading$ = this.store.pipe(select(fromSubUser.getSubUsersCreateDialogLoadingState), takeUntil(this.destroy$));
    this.createSubUserError$ = this.store.pipe(
      select(fromSubUser.getSubUsersCreateDialogRequestError),
      filter(error => !!error),
      takeUntil(this.destroy$)
    );
    this.createButtonClick$ = this.reactiveSubject.asObservable.pipe(
      filter(action => action?.clickedBtn === FooterButton.Create),
      takeUntil(this.destroy$)
    );
  }

  private setInitialFormData(): void {
    const userFormGroup = {
      template: ['', [Validators.required]],
      portalEmail: ['', [Validators.required, Validators.maxLength(200)]],
      name: ['', [Validators.required, Validators.maxLength(49)]],
      organization: ['', [Validators.required, Validators.maxLength(30)]],
      telephone: ['', [Validators.required, Validators.maxLength(15)]],
      country: ['', [Validators.required, Validators.maxLength(26)]],
      city: ['', [Validators.required, Validators.maxLength(30)]],
      address: ['', [Validators.required, Validators.maxLength(30)]],
      postCode: ['', [Validators.required, Validators.maxLength(10)]]
    };

    this.form = this.formBuilder.group(userFormGroup, { updateOn: 'change' });

    const template = this.canEditTemplate ? SubUserTemplate.Streamlined : SubUserTemplate.Efficient;
    this.form.patchValue({ template });
  }

  private initializeCreateButton(): void {
    this.createButton = this.dialogData.buttons.find(button => button.type === FooterButton.Create);
    this.createButton.isDisabled = true;
    this.loading$.subscribe(loading => (this.createButton.isLoading = loading));
  }

  private initializeFormListeners(): void {
    this.createButtonClick$.subscribe(() => {
      this.store.dispatch(SubUserCreateDialogActions.createSubUser({ subUserData: this.form.value }));
    });

    this.createSubUserError$.subscribe(({ error }) => this.setFieldErrors(error));
  }

  private initializeUserForTemplate() {
    this.loggedUser$.subscribe(loggedUser => {
      if (loggedUser.userType === UserType.AIRLINE) {
        this.canShowCreationInfo = false;
        const loggedUserTemplate =
          loggedUser.template === SubUserTemplate.Efficient ? this.templatesDropdown[0] : this.templatesDropdown[1];

        this.isTemplateLocked = true;
        this.templatesDropdown = [loggedUserTemplate];
      } else if (loggedUser.userType === UserType.IATA) {
        this.canShowCreationInfo = false;
        this.form.patchValue(
          { organization: this.translationService.translate('USERS.types_of_users.IATA') },
          { emitEvent: false }
        );
      }
    });
  }

  private setFieldErrors(error: ResponseErrorBE): void {
    if (error.errorCode !== 400 || !error.messages?.length) {
      return;
    }

    // Validation error
    error.messages.forEach(({ message, messageParams }) => {
      const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
      const invalidControl = this.form.get(controlName);

      if (invalidControl) {
        invalidControl.setErrors({ requestError: { message } });
        FormUtil.showControlState(invalidControl);
      }
    });
  }
}
