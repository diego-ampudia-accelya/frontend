import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { CreateSubUserComponent } from './create-sub-user.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { ResponseErrorBE } from '~app/shared/models';
import { SubUserCreateDialogActions } from '~app/sub-users/actions';
import { initialState as createSubUserInitialState } from '~app/sub-users/reducers/sub-user-create-dialog.reducer';
import { SubUserTemplate } from '~app/sub-users/shared/sub-users.models';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('CreateSubUserComponent', () => {
  let component: CreateSubUserComponent;
  let fixture: ComponentFixture<CreateSubUserComponent>;
  let mockStore: MockStore;
  let reactiveSubject: ReactiveSubject;
  let permissionService: SpyObject<PermissionsService>;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    },
    subUsers: {
      createSubUser: createSubUserInitialState
    }
  };

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Create,
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Create
          }
        ]
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CreateSubUserComponent, TranslatePipeMock],
      providers: [
        FormBuilder,
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject,
        provideMockStore({ initialState }),
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        },
        mockProvider(PermissionsService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSubUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    reactiveSubject = TestBed.inject(ReactiveSubject);
    permissionService = TestBed.inject<any>(PermissionsService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('createButtonClick$ should dispatch createSubUser', fakeAsync(() => {
    spyOn(mockStore, 'dispatch').and.callThrough();

    const testFormValue = {
      template: SubUserTemplate.Streamlined,
      portalEmail: 'test@test.com',
      name: 'test name',
      organization: 'test organization',
      telephone: 'test telephone',
      country: 'test country',
      city: 'test city',
      address: 'test address',
      postCode: 'test postCode'
    };

    component.form.setValue(testFormValue);

    reactiveSubject.emit({
      clickedBtn: FooterButton.Create
    });

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SubUserCreateDialogActions.createSubUser({ subUserData: testFormValue })
    );
  }));

  it('template to be set to Streamlined if currently logged user can edit templates', fakeAsync(() => {
    permissionService.hasPermission.and.returnValue(true);

    component.ngOnInit();

    expect(component.form.value.template).toBe(SubUserTemplate.Efficient);
  }));

  it('template to be set to efficient if currently logged user cannot edit templates', fakeAsync(() => {
    permissionService.hasPermission.and.returnValue(false);

    component.ngOnInit();

    expect(component.form.value.template).toBe(SubUserTemplate.Efficient);
  }));

  describe('setFieldErrors', () => {
    beforeEach(() => {
      const testFormValue = {
        template: SubUserTemplate.Streamlined,
        portalEmail: 'test@test.com',
        name: 'test name',
        organization: 'test organization',
        telephone: 'test telephone',
        country: 'test country',
        city: 'test city',
        address: 'test address',
        postCode: 'test postCode'
      };

      // Initialize form with valid values
      component.form.setValue(testFormValue);
    });

    it('should set form field errors when a server validation error is sent with a correct field param', () => {
      component['setFieldErrors']({
        errorCode: 400,
        messages: [
          {
            message: 'Wrong template',
            messageParams: [{ name: 'fieldName', value: 'template' }]
          }
        ]
      } as ResponseErrorBE);

      expect(component.form.get('template').hasError('requestError')).toBe(true);
    });

    it('should NOT set form field errors when a server validation error is sent without a correct field param', () => {
      component['setFieldErrors']({
        errorCode: 400,
        messages: [
          {
            message: 'Unknown message',
            messageParams: [{ name: 'fieldName', value: 'unknown' }]
          }
        ]
      } as ResponseErrorBE);

      expect(component.form.hasError('requestError')).toBe(false);
    });

    it('should NOT set form field errors when another server error is sent', () => {
      component['setFieldErrors']({ errorCode: 500 } as ResponseErrorBE);
      expect(component.form.hasError('requestError')).toBe(false);
    });
  });
});
