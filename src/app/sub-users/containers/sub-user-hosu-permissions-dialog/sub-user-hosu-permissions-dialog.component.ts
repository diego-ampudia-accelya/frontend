import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { getMenuTabs } from '~app/core/reducers';
import { EnquiryDialogData } from '~app/document/models/enquiry-dialog-data.model';
import { AppState } from '~app/reducers';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ROUTES } from '~app/shared/constants/routes';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { TabService } from '~app/shared/services';
import { SubUserHosuPermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';

export interface BspsSelectionViewModel {
  selectedBsps: BspDto[];
}

@Component({
  selector: 'bspl-sub-user-hosu-permissions-dialog',
  templateUrl: './sub-user-hosu-permissions-dialog.component.html'
})
export class SubUserHosuPermissionsDialogComponent implements OnInit, OnDestroy {
  public dialogData: EnquiryDialogData = this.config.data as EnquiryDialogData;
  public okButton: ModalAction;
  public okButtonClick$;
  public bspsFromHosuSelection: DropdownOption[];
  public form: FormGroup;

  private formFactory: FormUtil;
  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private store: Store<AppState>,
    private router: Router,
    private fb: FormBuilder,
    private dialogService: DialogService,
    private tabService: TabService
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.initializeOkClickListener();
    this.initializeCreateButton();
    this.initializeFormListeners();
    this.initializeBspDropdown();
    this.buildForm();
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  private buildForm() {
    this.form = this.formFactory.createGroup<BspsSelectionViewModel>({
      selectedBsps: [[], []]
    });
  }

  private initializeOkClickListener() {
    this.okButtonClick$ = this.reactiveSubject.asObservable.pipe(
      filter(action => action?.clickedBtn === FooterButton.Ok),
      takeUntil(this.destroy$)
    );
  }

  private initializeBspDropdown(): void {
    this.store
      .pipe(select(fromSubUsers.getBspsFromHosuSelection), takeUntil(this.destroy$), first())
      .subscribe(bspList => {
        this.bspsFromHosuSelection = bspList.map(toValueLabelObjectBsp);
      });
  }

  private initializeCreateButton(): void {
    this.okButton = this.dialogData.buttons.find(button => button.type === FooterButton.Ok);
  }

  private initializeFormListeners(): void {
    this.okButtonClick$.subscribe(() => {
      if (this.form.get('selectedBsps').value.length === 0) {
        this.selectAllHosuBsps();
      }

      this.saveSelectedHosuBsps();
      this.navigateToSubUserHosuPermissions();
      this.dialogService.close();
    });
  }

  private selectAllHosuBsps(): void {
    this.form.setValue({ selectedBsps: this.bspsFromHosuSelection.map(bsp => bsp.value) });
  }

  private saveSelectedHosuBsps(): void {
    this.store.dispatch(
      SubUserHosuPermissionsActions.updateSelectedHosuBsps({ selectedHosuBsps: this.form.get('selectedBsps').value })
    );
  }

  private navigateToSubUserHosuPermissions(): void {
    this.closeExistingSubUserHosuPermissionsTab();
    this.router.navigate([ROUTES.HOMU_SUB_USER.url]);
  }

  private closeExistingSubUserHosuPermissionsTab(): void {
    this.store.pipe(select(getMenuTabs), first()).subscribe(tabs => {
      const tabToClose = tabs.find(tab => tab.label === ROUTES.HOMU_SUB_USER.label);
      if (tabToClose) {
        this.tabService.closeTab(tabToClose);
      }
    });
  }
}
