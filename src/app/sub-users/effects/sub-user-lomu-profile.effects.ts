import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { SubUsersLomuProfileActions } from '~app/sub-users/actions';
import { SubUserLomuService } from '~app/sub-users/shared/sub-user-lomu.service';

@Injectable()
export class SubUserLomuProfileEffects {
  $requestSubUserLomu = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersLomuProfileActions.requestSubUserLomu),
      switchMap(({ bspId }) => this.subUserLomuService.getSubUserLomu(bspId)),
      map(subUserLomu => SubUsersLomuProfileActions.requestSubUserLomuSuccess({ subUserLomu })),
      rethrowError(error => SubUsersLomuProfileActions.requestSubUserLomuError({ error }))
    )
  );

  constructor(private actions$: Actions, private subUserLomuService: SubUserLomuService) {}
}
