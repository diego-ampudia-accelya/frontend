import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { SubUsersListActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';

@Injectable()
export class SubUsersListEffects {
  $changeFilter = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersListActions.changeRequiredFilter, SubUsersListActions.changeQuery),
      map(() => SubUsersListActions.loadData())
    )
  );

  $loadData = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersListActions.loadData),
      withLatestFrom(
        this.store.pipe(select(fromSubUsers.getListRequiredFilter)),
        this.store.pipe(select(fromSubUsers.getListQuery))
      ),
      map(([_, requiredFilter, query]) => ({
        ...query,
        filterBy: {
          ...query.filterBy,
          ...requiredFilter
        }
      })),
      switchMap(requestQuery => this.subUserService.find(requestQuery)),
      map(data => SubUsersListActions.loadDataSuccess({ data })),
      catchError(error => merge(of(SubUsersListActions.loadDataError({ error })), throwError(error)))
    )
  );

  constructor(private actions$: Actions, private subUserService: SubUserService, private store: Store<AppState>) {}
}
