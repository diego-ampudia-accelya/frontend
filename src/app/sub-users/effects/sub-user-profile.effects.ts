import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';
import { map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import { NotificationService, TabService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { SubUsersProfileActions, SubUsersTemplatesActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserPropertyChangesService } from '~app/sub-users/shared/sub-user-property-changes.service';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';
import { SubUserUpdateModel } from '~app/sub-users/shared/sub-users.models';

@Injectable()
export class SubUserProfileEffects {
  $requestSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfileActions.requestSubUser),
      switchMap(({ id }) => this.subUserService.getBy(id)),
      map(subUser => SubUsersProfileActions.requestSubUserSuccess({ subUser })),
      rethrowError(error => SubUsersProfileActions.requestSubUserError({ error }))
    )
  );

  $openPropertyChangesDialog = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfileActions.applyPropertyChanges),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getProfileIsCreatingHosu))),
      switchMap(([_, isCreatingHosu]) => this.getSubUserUpdateData(isCreatingHosu)),
      map(data =>
        data ? SubUsersProfileActions.updateSubUserProperties({ data }) : SubUsersProfileActions.cancelPropertyChanges()
      )
    )
  );

  $updateSubUserProperties = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfileActions.updateSubUserProperties),
      switchMap(({ data }) => this.subUserService.update(data)),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getSelectedSubUsers))),
      tap(([{ id, template }, selectedSubUsers]) => {
        // Side effect to update selected sub users for template tab
        const modifiedSelectedSubUsers = selectedSubUsers.map(subUser =>
          subUser.id === id ? { ...subUser, template } : subUser
        );

        this.store.dispatch(SubUsersTemplatesActions.editUsersTemplate({ selectedSubUsers: modifiedSelectedSubUsers }));
      }),
      map(([subUser, _]) => SubUsersProfileActions.updateSubUserPropertiesSuccess({ subUser })),
      rethrowError(error => SubUsersProfileActions.updateSubUserPropertiesError({ error }))
    )
  );

  $updateSubUserPropertiesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersProfileActions.updateSubUserPropertiesSuccess),
        tap(({ subUser }) => {
          const successMessage = this.translationService.translate('subUsers.editUser.editUserSuccess', {
            email: `<span class="link">${subUser.portalEmail}</span>`
          });

          this.notificationService.showSuccess(successMessage);
        })
      ),
    { dispatch: false }
  );

  $createSubUserHosu = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfileActions.createHosuSubUser),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getPropertiesFormValue))),
      switchMap(([_, data]) => this.subUserService.createHosu(data)),
      map(subUser => SubUsersProfileActions.createHosuSubUserSuccess({ subUser })),
      rethrowError(error => SubUsersProfileActions.createHosuSubUserError({ error }))
    )
  );

  $createSubUserHosuSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersProfileActions.createHosuSubUserSuccess),
        tap(({ subUser }) => {
          this.tabService.closeCurrentTabAndNavigate(ROUTES.HOSU_SUB_USER.url);
          this.notificationService.showSuccess(
            this.translationService.translate('SUB_USERS.MSG.CREATE.SUCCESS', {
              email: `<span class="link">${subUser.portalEmail}</span>`
            })
          );
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private subUserService: SubUserService,
    private store: Store<AppState>,
    private subUserPropertyChangesService: SubUserPropertyChangesService,
    private tabService: TabService
  ) {}

  private getSubUserUpdateData(isCreatingHosu: boolean): Observable<SubUserUpdateModel> {
    return this.subUserPropertyChangesService
      .confirmApplyPropertyChanges(isCreatingHosu)
      .pipe(
        switchMap(confirmed =>
          confirmed ? this.store.pipe(select(fromSubUsers.getPropertiesFormValue), take(1)) : of(null)
        )
      );
  }
}
