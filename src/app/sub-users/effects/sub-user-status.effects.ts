import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { L10nTranslationService } from 'angular-l10n';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { ButtonDesign, DialogService, FooterButton } from '~app/shared/components';
import { dateFilterTagMapper } from '~app/shared/helpers';
import { NotificationService } from '~app/shared/services/notification.service';
import { SubUsersProfileActions, SubUserStatusActions } from '~app/sub-users/actions';
import { SubUserDeactivateDialogComponent } from '~app/sub-users/containers/sub-user-deactivate-dialog/sub-user-deactivate-dialog.component';
import { SubUserReactivateDialogComponent } from '~app/sub-users/containers/sub-user-reactivate-dialog/sub-user-reactivate-dialog.component';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';
import { SubUser, SubUserStatus, SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';

@Injectable()
export class SubUserStatusEffects {
  $openSubUserStatusDialog = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserStatusActions.openSubUserStatusDialog),
        tap(({ subUser }) => this.openSubUserStatusDialog(subUser))
      ),
    { dispatch: false }
  );

  $openSubUserStatusDialogBySubUserType = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserStatusActions.openSubUserStatusBySubUserTypeDialog),
        tap(({ subUser, subUserTypeByParent }) => this.openSubUserStatusDialog(subUser, subUserTypeByParent))
      ),
    { dispatch: false }
  );

  $openSubUserHosuInfoDialog = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserStatusActions.openSubUserHosuInfoDialog),
        tap(() => this.openSubUserStatusDialog(null, null, true))
      ),
    { dispatch: false }
  );

  $deactivateSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserStatusActions.deactivateSubUser),
      switchMap(({ subUserId, expiryDate }) => this.subUserService.deactivateUser(subUserId, expiryDate)),
      map(subUser => SubUserStatusActions.deactivateSubUserSuccess({ subUser })),
      catchError(error => merge(of(SubUserStatusActions.deactivateSubUserError(error)), throwError(error)))
    )
  );

  $deactivateHosuSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserStatusActions.deactivateHosuSubUser),
      switchMap(({ subUserId }) => this.subUserService.deactivateHosuUser(subUserId)),
      map(subUser => SubUserStatusActions.deactivateHosuSubUserSuccess({ subUser })),
      catchError(error => {
        this.dialogService.close();

        return merge(of(SubUserStatusActions.deactivateSubUserError(error)), throwError(error));
      })
    )
  );

  $reactivateSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserStatusActions.reactivateSubUser),
      switchMap(({ id, reactivateData }) => this.subUserService.reactivateUser(id, reactivateData)),
      map(subUser => SubUserStatusActions.reactivateSubUserSuccess({ subUser })),
      catchError(error => merge(of(SubUserStatusActions.reactivateSubUserError(error)), throwError(error)))
    )
  );

  $reactivateHosuSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserStatusActions.reactivateHosuSubUser),
      switchMap(({ subUserId, reactivateHosuData }) =>
        this.subUserService.reactivateHosuUser(subUserId, reactivateHosuData)
      ),
      map(subUser => SubUserStatusActions.reactivateSubUserSuccess({ subUser })),
      catchError(error => merge(of(SubUserStatusActions.reactivateSubUserError(error)), throwError(error)))
    )
  );

  $deactivateSubUserSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserStatusActions.deactivateSubUserSuccess),
        tap(({ subUser }) => {
          const msg = this.translationService.translate('SUB_USERS.MSG.CHANGE_STATUS.DEACTIVATE.SUCCESS', {
            email: `<span class="link">${subUser.portalEmail}</span>`,
            expiryDate: dateFilterTagMapper(new Date(subUser.expiryDate))
          });

          this.notificationService.showSuccess(msg);
          this.dialogService.close();
        })
      ),
    { dispatch: false }
  );

  $deactivateHosuSubUserSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserStatusActions.deactivateHosuSubUserSuccess),
        tap(({ subUser }) => {
          const userLabel = subUser.portalEmail ? subUser.portalEmail : subUser.ilogin;
          const msg = this.translationService.translate('SUB_USERS.MSG.CHANGE_STATUS.DEACTIVATE_HOSU.SUCCESS', {
            email: `<span class="link">${userLabel}</span>`
          });

          this.notificationService.showSuccess(msg);
          this.dialogService.close();
        })
      ),
    { dispatch: false }
  );

  $reactivateSubUserSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserStatusActions.reactivateSubUserSuccess),
      tap(({ subUser }) => {
        const msg = this.translationService.translate('SUB_USERS.MSG.CHANGE_STATUS.REACTIVATE.SUCCESS', {
          email: `<span class="link">${subUser.portalEmail}</span>`
        });

        this.notificationService.showSuccess(msg);
        this.dialogService.close();
      }),
      map(({ subUser }) => SubUsersProfileActions.requestSubUserSuccess({ subUser }))
    )
  );

  $reactivateSubUserHosuSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserStatusActions.reactivateHosuSubUserSuccess),
      tap(({ subUser }) => {
        const msg = this.translationService.translate('SUB_USERS.MSG.CHANGE_STATUS.REACTIVATE.SUCCESS', {
          email: `<span class="link">${subUser.portalEmail}</span>`
        });

        this.notificationService.showSuccess(msg);
        this.dialogService.close();
      }),
      map(({ subUser }) => SubUsersProfileActions.requestSubUserSuccess({ subUser }))
    )
  );

  constructor(
    private actions$: Actions,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private subUserService: SubUserService,
    private notificationService: NotificationService
  ) {}

  private openSubUserStatusDialog(
    subUser?: SubUser,
    subUserTypeByParent: SubUserTypeByParent = SubUserTypeByParent.losu,
    reactivateInfoType = false
  ) {
    let title: string;
    let footerButtonsType: { type: FooterButton; buttonDesign?: ButtonDesign }[];
    let component: any;

    if (!reactivateInfoType && subUser.status === SubUserStatus.Active) {
      title =
        subUserTypeByParent === SubUserTypeByParent.losu
          ? 'subUsers.toggleStatusDialogs.deactivateUserTitle'
          : 'subUsers.toggleStatusDialogs.deactivateHosuUserTitle';
      footerButtonsType = [{ type: FooterButton.Deactivate }];
      component = SubUserDeactivateDialogComponent;
    }

    if (!reactivateInfoType && subUser.status === SubUserStatus.Inactive) {
      title =
        subUserTypeByParent === SubUserTypeByParent.losu
          ? 'subUsers.toggleStatusDialogs.reactivateUserTitle'
          : 'subUsers.toggleStatusDialogs.reactivateHosuUserTitle';
      footerButtonsType = [{ type: FooterButton.Reactivate }];
      component = SubUserReactivateDialogComponent;
    }

    if (reactivateInfoType) {
      title = 'subUsers.toggleStatusDialogs.reactivateHosuUserInfoTitle';
      footerButtonsType = [{ type: FooterButton.Close, buttonDesign: ButtonDesign.Tertiary }];
      component = SubUserReactivateDialogComponent;
    }

    return this.dialogService.open(component, {
      data: {
        title,
        subUserTypeByParent,
        hasCancelButton: !reactivateInfoType,
        isClosable: true,
        footerButtonsType,
        subUser,
        reactivateInfoType
      }
    });
  }
}
