import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { mapSelectionListToPermissions } from '../reducers/sub-user-profile-permissions.reducer';
import { SubUsersHosuChangesService } from '../shared/sub-users-hosu-changes.service';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { NotificationService, TabService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { SubUserHosuPermissionsActions } from '~app/sub-users/actions';
import { SubUserHosuPermissionsDialogComponent } from '~app/sub-users/containers/sub-user-hosu-permissions-dialog/sub-user-hosu-permissions-dialog.component';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';

@Injectable()
export class SubUserHosuPermissionsEffects {
  $fetchBspsFromHosuSelection = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserHosuPermissionsActions.fetchBspsFromHosuSelection),
        withLatestFrom(this.store.pipe(select(getUserBsps))),
        switchMap(([{ selectedHosus }, userBsps]) =>
          this.subUserService.getBspsFromHosuSelection(selectedHosus).pipe(
            tap(bsps => {
              bsps.map(bsp => {
                bsp.name =
                  userBsps.find(userBsp => userBsp.isoCountryCode === bsp.isoCountryCode)?.name || bsp.isoCountryCode;
              });

              return this.store.dispatch(SubUserHosuPermissionsActions.fetchBspsFromHosuSelectionSuccess({ bsps }));
            })
          )
        )
      ),
    { dispatch: false }
  );

  $fetchBspsFromHosuSelectionSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserHosuPermissionsActions.fetchBspsFromHosuSelectionSuccess),
        tap(() => {
          this.openSubUserHosuPermissionsDialog();
        })
      ),
    {
      dispatch: false
    }
  );

  requestHosusPermissionsByBsps$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserHosuPermissionsActions.requestHosusPermissionsByBsps),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getSelectedHosuBsps))),
      switchMap(([_, bsps]) =>
        this.subUserService.getHosusPermissionsByBsps(bsps.map(bsp => bsp.id)).pipe(
          map(permissions =>
            SubUserHosuPermissionsActions.requestHosusPermissionsByBspsSuccess({
              permissionsResponse: permissions
            })
          )
        )
      ),
      rethrowError(() => SubUserHosuPermissionsActions.requestHosusPermissionsByBspsError())
    )
  );

  openCancelChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserHosuPermissionsActions.openCancelChanges),
      switchMap(() => this.changesService.cancelUserPermissions$)
    )
  );

  openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserHosuPermissionsActions.openApplyChanges),
      switchMap(() => this.changesService.applyUserPermissions$)
    )
  );

  applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserHosuPermissionsActions.applyChanges),
      withLatestFrom(
        this.store.pipe(select(fromSubUsers.getModifiedHosusPermissionsByBsps)),
        this.store.pipe(select(fromSubUsers.getSelectedHosus)),
        this.store.pipe(select(fromSubUsers.getSelectedHosuBsps)),
        this.store.pipe(select(fromSubUsers.getEnabled))
      ),
      switchMap(([_, modified, hosus, bsps, enabled]) =>
        this.subUserService
          .updatePermissionsByHosusAndBsps(hosus, bsps, mapSelectionListToPermissions(modified, enabled), enabled)
          .pipe(map(() => SubUserHosuPermissionsActions.applyChangesSuccess()))
      ),
      rethrowError(() => SubUserHosuPermissionsActions.applyChangesError())
    )
  );

  $applyChangesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserHosuPermissionsActions.applyChangesSuccess),
        tap(() => {
          const message = this.translationService.translate('subUsers.hosuPermissions.applyChanges.success');
          this.notificationService.showSuccess(message);
        })
      ),
    {
      dispatch: false
    }
  );

  $applyChangesError = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserHosuPermissionsActions.applyChangesError),
        tap(() => {
          const message = this.translationService.translate('subUsers.hosuPermissions.applyChanges.error');
          this.notificationService.showError(message);
        })
      ),
    {
      dispatch: false
    }
  );

  $discardChanges = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserHosuPermissionsActions.discardChanges),
        tap(() => {
          this.tabService.closeCurrentTab();
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private subUserService: SubUserService,
    private store: Store<AppState>,
    private changesService: SubUsersHosuChangesService,
    private notificationService: NotificationService,
    private tabService: TabService
  ) {}

  private openSubUserHosuPermissionsDialog() {
    return this.dialogService.open(SubUserHosuPermissionsDialogComponent, {
      data: {
        title: this.translationService.translate('subUsers.hosuPermissions.bspSelectDialog.title'),
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Ok }]
      }
    });
  }
}
