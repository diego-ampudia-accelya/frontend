import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { forkJoin, merge, Observable, of, throwError } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { ButtonDesign, DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { SubUsersListActions, SubUsersPermissionsActions } from '~app/sub-users/actions';
import { SaveSubUsersPermissionsDialogComponent } from '~app/sub-users/containers/save-sub-users-permissions-dialog/save-sub-users-permissions-dialog.component';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';

@Injectable()
export class SubUsersPermissionsEffects {
  $editSubUsersPermissions = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersListActions.editSubUsersPermissions),
      switchMap(({ selectedSubUsers }) =>
        of(SubUsersPermissionsActions.fetchSubUserAndPermissions({ selectedSubUsers }))
      )
    )
  );

  $fetchSubUserAndPermissions = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersPermissionsActions.fetchSubUserAndPermissions),
      switchMap(({ selectedSubUsers }) => {
        const $selectedSubUsers = selectedSubUsers.map(user =>
          this.subUserService.getPermissionsBy(user.id).pipe(
            map(permissions => {
              const { id, portalEmail, name, template } = user;

              return {
                id,
                portalEmail,
                name,
                template,
                permissions
              };
            })
          )
        );

        return forkJoin($selectedSubUsers);
      }),
      map(subUsers => SubUsersPermissionsActions.fetchSubUserAndPermissionsSuccess({ subUsers })),
      rethrowError(() => SubUsersPermissionsActions.fetchSubUserAndPermissionsError())
    )
  );

  $fetchSubUserAndPermissionsSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersPermissionsActions.fetchSubUserAndPermissionsSuccess),
        tap(() => {
          this.router.navigate([ROUTES.SUB_USER_PERMISSIONS.url]);
        })
      ),
    {
      dispatch: false
    }
  );

  $openApplySubUsersPermissionsChangesDialog = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersPermissionsActions.openApplySubUsersPermissionsChangesDialog),
      switchMap(() => this.openApplySubUsersPermissionChangesDialog()),
      map(result => {
        if (result.clickedBtn === FooterButton.Apply) {
          this.dialogService.close();

          return SubUsersPermissionsActions.applySubUsersPermissionsChanges();
        }
        this.dialogService.close();

        return SubUsersPermissionsActions.cancelSubUsersPermissionsChanges();
      })
    )
  );

  $openUnsavedSubUsersPermissionsChangesDialog = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersPermissionsActions.openUnsavedSubUsersPermissionsChangesDialog),
      switchMap(() => this.openUnsavedSubUsersPermissionChangesDialog()),
      map(result => {
        const clickedBtn = result.clickedBtn;

        if (clickedBtn === FooterButton.Apply) {
          this.dialogService.close();

          return SubUsersPermissionsActions.applySubUsersPermissionsChanges();
        }

        if (clickedBtn === FooterButton.Discard) {
          this.dialogService.close();

          return SubUsersPermissionsActions.discardSubUsersPermissionsChanges();
        }
        this.dialogService.close();

        return SubUsersPermissionsActions.cancelSubUsersPermissionsChanges();
      })
    )
  );

  $applySubUsersPermissionsChanges = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersPermissionsActions.applySubUsersPermissionsChanges),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getModifiedSubUsersPermissions))),
      switchMap(res => {
        const users = res[1];

        const users$ = users.map(user => this.subUserService.updatePermissionsByUserId(user.id, user.permissions));

        return forkJoin(users$).pipe(
          map(() => SubUsersPermissionsActions.updateSubUsersPermissionsSuccess({ users })),
          catchError(error =>
            merge(of(SubUsersPermissionsActions.updateSubUsersPermissionsError({ error })), throwError(error))
          )
        );
      })
    )
  );

  $updateSubUsersPermissionsSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersPermissionsActions.updateSubUsersPermissionsSuccess),
        tap(() => {
          this.notificationService.showSuccess(
            this.translationService.translate('subUsers.permissionsScreen.msg.updated.success')
          );
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private subUserService: SubUserService,
    private router: Router,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private store: Store<AppState>,
    private notificationService: NotificationService
  ) {}

  private openApplySubUsersPermissionChangesDialog() {
    return this.dialogService
      .open(SaveSubUsersPermissionsDialogComponent, {
        data: {
          title: 'subUsers.editUser.titleApply',
          hasCancelButton: true,
          isClosable: true,
          footerButtonsType: [{ type: FooterButton.Apply }],
          description: 'subUsers.permissionsScreen.dialog.descriptionPermissionsApply'
        }
      })
      .pipe(tap(result => result.clickedBtn === FooterButton.Apply));
  }

  private openUnsavedSubUsersPermissionChangesDialog(): Observable<any> {
    return this.dialogService.open(SaveSubUsersPermissionsDialogComponent, {
      data: {
        title: 'subUsers.editUser.titleUnsaved',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ],
        description: 'subUsers.permissionsScreen.dialog.descriptionUnsaved'
      }
    });
  }
}
