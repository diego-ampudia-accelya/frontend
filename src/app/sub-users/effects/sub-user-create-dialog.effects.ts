import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { L10nTranslationService } from 'angular-l10n';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { DialogService, FooterButton } from '~app/shared/components';
import { NotificationService } from '~app/shared/services/notification.service';
import { SubUserCreateDialogActions, SubUsersListActions } from '~app/sub-users/actions';
import { CreateSubUserComponent } from '~app/sub-users/containers/create-sub-user/create-sub-user.component';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';

@Injectable()
export class SubUserCreateDialogEffects {
  $openSubUserModal = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUserCreateDialogActions.openSubUserCreateDialog),
        switchMap(() => this.openCreateSubUserDialog())
      ),
    { dispatch: false }
  );

  $createSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserCreateDialogActions.createSubUser),
      switchMap(({ subUserData }) => this.subUserService.create(subUserData)),
      map(subUser => SubUserCreateDialogActions.createSubUserSuccess({ subUser })),
      catchError(error => merge(of(SubUserCreateDialogActions.createSubUserError({ error })), throwError(error)))
    )
  );

  $createSubUserSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUserCreateDialogActions.createSubUserSuccess),
      tap(({ subUser }) => {
        this.dialogService.close();

        this.notificationService.showSuccess(
          this.translationService.translate('SUB_USERS.MSG.CREATE.SUCCESS', {
            email: `<span class="link">${subUser.portalEmail}</span>`
          })
        );
      }),
      map(() => SubUsersListActions.loadData())
    )
  );

  constructor(
    private actions$: Actions,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private subUserService: SubUserService,
    private notificationService: NotificationService
  ) {}

  private openCreateSubUserDialog() {
    return this.dialogService.open(CreateSubUserComponent, {
      data: {
        title: this.translationService.translate('subUsers.createUser.title'),
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Create }]
      }
    });
  }
}
