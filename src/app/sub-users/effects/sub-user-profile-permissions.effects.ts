import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { iif } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { UserPermission } from '../shared/permissions.models';
import { AppState } from '~app/reducers';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { mapSelectionListToPermissions } from '~app/sub-users/reducers/sub-user-profile-permissions.reducer';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';
import { SubUsersChangesService } from '~app/sub-users/shared/sub-users-changes.service';
import { SubUser, SubUserTypeByParent } from '~app/sub-users/shared/sub-users.models';

@Injectable()
export class SubUserProfilePermissionsEffects {
  requestProfilePermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfilePermissionsActions.requestPermissions),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getProfileSubUser))),
      switchMap(([_, subUser]) =>
        this.subUserService
          .getPermissionsBy(subUser.id)
          .pipe(map(permissions => this.requestPermissionsSuccess(permissions, subUser)))
      ),
      rethrowError(() => SubUsersProfilePermissionsActions.requestPermissionsError())
    )
  );

  requestHosuProfilePermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfilePermissionsActions.requestHosuPermissions),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getProfileSubUser))),
      switchMap(([subUserHosuBsp, subUser]) =>
        this.subUserService
          .getHosuPermissionsBy(subUser.id, subUserHosuBsp.bspId)
          .pipe(map(permissions => this.requestPermissionsSuccess(permissions, subUser)))
      ),
      rethrowError(() => SubUsersProfilePermissionsActions.requestPermissionsError())
    )
  );

  openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfilePermissionsActions.openApplyChanges),
      switchMap(() => this.changesService.applyUserPermissions$)
    )
  );

  applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersProfilePermissionsActions.applyChanges),
      withLatestFrom(
        this.store.pipe(select(fromSubUsers.getModifiedProfilePermissions)),
        this.store.pipe(select(fromSubUsers.getProfileSubUser)),
        this.store.pipe(select(fromSubUsers.getProfilePermissionsBspId)),
        this.store.pipe(select(fromSubUsers.getSubUserTypeByParent))
      ),
      switchMap(([_, modified, subUser, bspId, subUserTypeByParent]) =>
        iif(
          () => subUserTypeByParent === SubUserTypeByParent.losu,
          this.subUserService
            .updatePermissionsByUserId(subUser.id, mapSelectionListToPermissions(modified))
            .pipe(map(permissions => SubUsersProfilePermissionsActions.applyChangesSuccess({ permissions, subUser }))),
          this.subUserService
            .updateHosuPermissionsByUserId(subUser.id, bspId, mapSelectionListToPermissions(modified))
            .pipe(map(permissions => SubUsersProfilePermissionsActions.applyChangesSuccess({ permissions, subUser })))
        )
      ),
      rethrowError(() => SubUsersProfilePermissionsActions.applyChangesError())
    )
  );

  $applyChangesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersProfilePermissionsActions.applyChangesSuccess),
        withLatestFrom(this.store.pipe(select(fromSubUsers.getProfileSubUserPortalEmail))),
        tap(([_, email]) => {
          const message = this.translationService.translate('subUsers.editUser.editUserSuccess', {
            email: `<span class="link">${email}</span>`
          });
          this.notificationService.showSuccess(message);
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private subUserService: SubUserService,
    private changesService: SubUsersChangesService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  private requestPermissionsSuccess(permissions: UserPermission[], subUser: SubUser) {
    return SubUsersProfilePermissionsActions.requestPermissionsSuccess({
      permissions,
      subUser
    });
  }
}
