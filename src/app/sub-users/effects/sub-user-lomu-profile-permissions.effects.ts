import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { SubUsersLomuProfilePermissionsActions, SubUsersProfilePermissionsActions } from '~app/sub-users/actions';
import * as fromSubUsers from '~app/sub-users/reducers';
import { mapSelectionListToPermissions } from '~app/sub-users/reducers/sub-user-profile-permissions.reducer';
import { SubUserLomuService } from '~app/sub-users/shared/sub-user-lomu.service';
import { SubUsersLomuChangesService } from '~app/sub-users/shared/sub-users-lomu-changes.service';

@Injectable()
export class SubUserLomuProfilePermissionsEffects {
  requestProfilePermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersLomuProfilePermissionsActions.requestLomuPermissions),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getLomuProfileBspId))),
      switchMap(([_, bspId]) =>
        this.subUserLomuService.getSubUserLomuPermissions(bspId).pipe(
          map(permissions =>
            SubUsersLomuProfilePermissionsActions.requestLomuPermissionsSuccess({
              permissionsResponse: permissions
            })
          )
        )
      ),
      rethrowError(() => SubUsersProfilePermissionsActions.requestPermissionsError())
    )
  );

  openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersLomuProfilePermissionsActions.openApplyChanges),
      switchMap(() => this.changesService.applyUserPermissions$)
    )
  );

  applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersLomuProfilePermissionsActions.applyChanges),
      withLatestFrom(
        this.store.pipe(select(fromSubUsers.getModifiedLomuProfilePermissions)),
        this.store.pipe(select(fromSubUsers.getLomuProfilePermissionUserId))
      ),
      switchMap(([_, modified, subUserId]) =>
        this.subUserLomuService
          .updatePermissionsByUserId(subUserId, mapSelectionListToPermissions(modified))
          .pipe(map(permissions => SubUsersLomuProfilePermissionsActions.applyChangesSuccess({ permissions })))
      ),
      rethrowError(() => SubUsersProfilePermissionsActions.applyChangesError())
    )
  );

  $applyChangesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersLomuProfilePermissionsActions.applyChangesSuccess),
        withLatestFrom(this.store.pipe(select(fromSubUsers.getLomuProfileSubUser))),
        tap(([_, lomuSubUser]) => {
          const message = this.translationService.translate('subUsers.editUser.editUserSuccess', {
            email: `<span class="link">${lomuSubUser?.portalEmail}</span>`
          });
          this.notificationService.showSuccess(message);
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private changesService: SubUsersLomuChangesService,
    private subUserLomuService: SubUserLomuService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}
}
