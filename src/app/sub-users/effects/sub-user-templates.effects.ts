import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { merge, of, throwError } from 'rxjs';
import { catchError, first, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { ButtonDesign, DialogService, FooterButton } from '~app/shared/components';
import { NotificationService } from '~app/shared/services/notification.service';
import { SubUsersApiActions, SubUsersTemplatesActions } from '~app/sub-users/actions';
import { SaveSubUserTemplatesDialogComponent } from '~app/sub-users/containers/save-sub-user-templates-dialog/save-sub-user-templates-dialog.component';
import * as fromSubUsers from '~app/sub-users/reducers';
import { SubUserService } from '~app/sub-users/shared/sub-user.service';
import { SubUserTemplateViewModel } from '~app/sub-users/shared/sub-users.models';

@Injectable()
export class SubUserTemplatesEffects {
  $openApplySubUserTemplateChangesDialog = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersTemplatesActions.subUsersTemplatesOpenApplyChangesDialog),
        withLatestFrom(this.store.pipe(select(fromSubUsers.getSubUserTemplatesChanges))),
        switchMap(([_, changes]) => this.openApplyTemplatesChangesDialog(changes))
      ),
    { dispatch: false }
  );

  $openUnsavedSubUserTemplateChangesDialog = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersTemplatesActions.subUsersTemplatesOpenUnsavedChangesDialog),
        withLatestFrom(this.store.pipe(select(fromSubUsers.getSubUserTemplatesChanges))),
        switchMap(([_, changes]) => this.openUnsavedTemplatesChangesDialog(changes))
      ),
    { dispatch: false }
  );

  $discardSubUsersTemplatesChanges = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersTemplatesActions.discardSubUsersTemplatesChanges),
        tap(() => this.dialogService.close())
      ),
    { dispatch: false }
  );

  $applySubUsersTemplatesChanges = createEffect(() =>
    this.actions$.pipe(
      ofType(SubUsersApiActions.applySubUserTemplatesChanges),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getSubUserTemplatesChanges))),
      map(([_, changes]) => changes.map(({ id: userId, template }) => ({ userId, template }))),
      switchMap(data => this.subUserService.updateUsersTemplates(data)),
      map(response => SubUsersApiActions.updateSubUsersTemplatesSuccess({ updatedSubUsersTemplatesData: response })),
      catchError(error => merge(of(SubUsersApiActions.updateSubUsersTemplatesError(error)), throwError(error)))
    )
  );

  $updateSubUsersTemplatesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersApiActions.updateSubUsersTemplatesSuccess),
        tap(() => {
          this.dialogService.close();
          this.notificationService.showSuccess('subUsers.templatesScreen.updateSuccess');
        })
      ),
    { dispatch: false }
  );

  $updateSubUsersTemplatesError = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SubUsersApiActions.updateSubUsersTemplatesError),
        tap(() => this.dialogService.close())
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private subUserService: SubUserService,
    private dialogService: DialogService,
    private notificationService: NotificationService
  ) {}

  private openApplyTemplatesChangesDialog(modifiedUsers: SubUserTemplateViewModel[]) {
    return this.dialogService
      .open(SaveSubUserTemplatesDialogComponent, {
        data: {
          title: 'subUsers.templatesScreen.applyDialogTitle',
          footerButtonsType: [{ type: FooterButton.Apply }],
          description: 'subUsers.templatesScreen.applyDialogDescription',
          modifiedUsers
        }
      })
      .pipe(
        tap(({ clickedBtn }) => {
          if (clickedBtn === FooterButton.Apply) {
            this.store.dispatch(SubUsersApiActions.applySubUserTemplatesChanges());
          }
        }),
        first()
      );
  }

  private openUnsavedTemplatesChangesDialog(modifiedUsers: SubUserTemplateViewModel[]) {
    return this.dialogService
      .open(SaveSubUserTemplatesDialogComponent, {
        data: {
          title: 'subUsers.templatesScreen.unsavedDialogTitle',
          footerButtonsType: [
            { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
            { type: FooterButton.Apply }
          ],
          description: 'subUsers.templatesScreen.unsavedDialogDescription',
          modifiedUsers
        }
      })
      .pipe(
        tap(({ clickedBtn }) => {
          switch (clickedBtn) {
            case FooterButton.Apply:
              this.store.dispatch(SubUsersApiActions.applySubUserTemplatesChanges());
              break;
            case FooterButton.Discard:
              this.store.dispatch(SubUsersTemplatesActions.discardSubUsersTemplatesChanges());
              break;
          }
        }),
        first()
      );
  }
}
