import { Injectable } from '@angular/core';

import { LoginData } from '~app/training/models/loginData';
import { UserData } from '~app/training/models/userData';

@Injectable({
  providedIn: 'root'
})
export class UserDataLocalStorageService {
  storeUserData(userData: UserData, loginData: LoginData) {
    const tsud: any = JSON.parse(localStorage.getItem('tsud') || '{}');
    const dataKey = this.getKey(loginData);

    localStorage.setItem(
      'tsud',
      JSON.stringify({
        ...tsud,
        [dataKey]: userData
      })
    );
  }

  getUserData(loginData: LoginData): UserData {
    if (loginData) {
      return JSON.parse(localStorage.getItem('tsud') || '{}')[this.getKey(loginData)] || null;
    }

    return null;
  }

  getKey({ userName }): string {
    return (/agent/.test(userName) && 'agent') || (/airline/.test(userName) && 'airline') || 'iata';
  }
}
