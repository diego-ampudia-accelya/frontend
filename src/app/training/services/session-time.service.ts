import { Injectable, OnDestroy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { take } from 'rxjs/operators';

import { FeedbackData } from '../models/feedbackData';
import { AppState } from '~app/reducers';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { NotificationService } from '~app/shared/services/notification.service';
import { FinishPageActions, SessionActions } from '~app/training/actions';
import * as fromTrainingSite from '~app/training/reducers';

@Injectable({
  providedIn: 'root'
})
export class SessionTimeService implements OnDestroy {
  reminderSetTimeout: any;
  completeSetTimeout: any;
  feedbackSubmitTimeout: any;
  duration = appConfiguration.trainingSessionConfiguration.duration || 120; // minutes
  reminderOffset = appConfiguration.trainingSessionConfiguration.expiresIn || 5; // minutes
  feedbackSubmitTimer = 14; //minutes
  emptyFeedbackData: FeedbackData;
  userDataId$ = this.store.pipe(select(fromTrainingSite.getUserDataIdState), take(1));

  constructor(
    private notification: NotificationService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {}

  start(duration = this.duration, reminderOffset = this.reminderOffset) {
    this.clearTimeOut();

    // reminder offset
    if (reminderOffset && reminderOffset > 0) {
      this.reminderSetTimeout = setTimeout(() => {
        this.notification.showWarning(
          this.translationService.translate('trainingSite.msg.sessionExpirationReminder', {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            reminder_offset: reminderOffset
          }),
          {
            life: 15000
          }
        );
      }, this.convertMinutesToMilliseconds(duration - reminderOffset));
    }

    // complete session
    this.completeSetTimeout = setTimeout(() => {
      this.store.dispatch(SessionActions.sessionExpired());

      this.startFeedbackSubmitTimeout();
    }, this.convertMinutesToMilliseconds(duration));
  }

  startFeedbackSubmitTimeout() {
    this.userDataId$.subscribe(id => {
      this.feedbackSubmitTimeout = setTimeout(() => {
        const userDataId = id.toString();
        this.store.dispatch(FinishPageActions.submitFeedback({ userDataId }));
      }, this.convertMinutesToMilliseconds(this.feedbackSubmitTimer));
    });
  }

  convertMinutesToMilliseconds(minutes: number) {
    return minutes * 60 * 1000;
  }

  getResetTime(): string {
    const offset = 2; // +2 Madrid Spain

    const start = moment().utcOffset(offset);

    const end = moment().utcOffset(offset).set({
      hours: 8,
      minute: 0,
      second: 0
    });

    if (start > end) {
      end.add(1, 'day');
    }

    const duration = moment.duration(end.diff(start));

    return duration.hours() + ' hours and ' + duration.minutes() + ' minutes';
  }

  clearTimeOut() {
    if (this.reminderSetTimeout) {
      clearTimeout(this.reminderSetTimeout);
    }

    if (this.completeSetTimeout) {
      clearTimeout(this.completeSetTimeout);
    }

    if (this.feedbackSubmitTimeout) {
      clearTimeout(this.feedbackSubmitTimeout);
    }
  }

  ngOnDestroy(): void {
    this.clearTimeOut();
  }
}
