import { Injectable } from '@angular/core';
import { difference } from 'lodash';

import { User } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class TrainingSitePermissionsService {
  applyTrainingSitePermissions(user: User): void {
    user.permissions = difference(user.permissions, [
      'USER',
      'DELETE_USER_TEMPLATE',
      'MODIFY_GROUP_TEMPLATE',
      'CREATE_IATA_USER',
      'VIEW_IATA_USER',
      'MODIFY_IATA_USER',
      'CREATE_AIRLINE_USER',
      'VIEW_AIRLINE_USER',
      'MODIFY_AIRLINE_USER',
      'CREATE_AGENT_USER',
      'VIEW_AGENT_USER',
      'MODIFY_AGENT_USER',
      'VIEW_GDS_USER',
      'MODIFY_GDS_USER',
      'MANAGE_GROUP_PERMISSIONS',
      'CREATE_HIERARCHY',
      'MANAGE_GROUP',
      'CREATE_USER_TEMPLATE',
      'MANAGE_USER_PERMISSIONS',
      'DELETE_GROUP_TEMPLATE',
      'CREATE_GROUP_TEMPLATE',
      'CREATE_AGENT_USER',
      'CREATE_IATA_USER',
      'CREATE_GROUP',
      'MODIFY_USER_TEMPLATE',
      'user_maintenance',
      'reports',
      'reports',
      'bsp_query'
    ]);
  }
}
