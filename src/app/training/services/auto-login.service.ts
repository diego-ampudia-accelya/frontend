import { Injectable } from '@angular/core';

import { LoginData } from '~app/training/models/loginData';

@Injectable({
  providedIn: 'root'
})
export class AutoLoginService {
  public getData(): LoginData {
    if (location.pathname === '/training/start') {
      const search = (location.search + '').substr(1);
      const userName = atob(search);

      switch (search) {
        case 'bmZlLWlhdGEtdXNlckB0cmFpbmluZy5jb20=':
          return {
            userName,
            authorizationValue: 'bmZlLWlhdGEtdXNlckB0cmFpbmluZy5jb206OUVKd1dxVjNBOUshR25wUA=='
          };
        case 'bmZlLWFpcmxpbmUtdXNlckB0cmFpbmluZy5jb20=':
          return {
            userName,
            authorizationValue: 'bmZlLWFpcmxpbmUtdXNlckB0cmFpbmluZy5jb206clFOOTJCVUBxM2tRNDlxRA=='
          };
        case 'bmZlLWFnZW50LXVzZXJAdHJhaW5pbmcuY29t':
          return {
            userName,
            authorizationValue: 'bmZlLWFnZW50LXVzZXJAdHJhaW5pbmcuY29tOnFMOFZLJWVyVzhNY0x0czg='
          };
      }
    }

    return null;
  }
}
