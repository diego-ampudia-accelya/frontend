/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { LoginData } from '~app/training/models/loginData';
import { UserData } from '~app/training/models/userData';

@Injectable({
  providedIn: 'root'
})
export class UserDataApiService {
  constructor(protected http: HttpClient) {}

  public requestUserDataLogin(loginData: LoginData, userData: UserData): Observable<any> {
    const url = `${appConfiguration.baseApiPath}/training-portal-management/user-data`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Basic ${loginData.authorizationValue}`
      }),
      params: { showErrorMessage: 'false' }
    };

    return this.http.post(url, userData, httpOptions);
  }
}
