import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { FeedbackData } from '~app/training/models/feedbackData';

@Injectable({
  providedIn: 'root'
})
export class FeedbackApiService {
  constructor(protected http: HttpClient) {}

  public sendFeedback(feedbackData: FeedbackData): Observable<FeedbackData> {
    const url = `${appConfiguration.baseApiPath}/training-portal-management/feedbacks`;
    const httpOptions = {
      headers: new HttpHeaders({
        // eslint-disable-next-line @typescript-eslint/naming-convention
        'Content-Type': 'application/json'
      }),
      params: { showErrorMessage: 'false' }
    };

    return this.http.post<FeedbackData>(url, feedbackData, httpOptions);
  }
}
