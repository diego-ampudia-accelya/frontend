import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { LogoutPageComponent } from './logout-page.component';

describe('LogoutPageComponent', () => {
  let component: LogoutPageComponent;
  let fixture: ComponentFixture<LogoutPageComponent>;

  beforeEach(waitForAsync(() => {
    const initialState = {
      trainingSite: {
        userLoginData: {
          userName: '',
          authorizationValue: '',
          error: ''
        },
        userLogoutData: {
          showAutoLogoutMessage: false,
          loading: false
        },
        userDataState: {
          userData: {
            id: null,
            email: '',
            bspNames: [],
            code: '',
            firstName: '',
            lastName: ''
          },
          loading: false
        }
      }
    };

    TestBed.configureTestingModule({
      declarations: [LogoutPageComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [provideMockStore({ initialState })]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
