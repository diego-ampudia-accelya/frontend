import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { FinishPageActions } from '~app/training/actions';
import { FeedbackData } from '~app/training/models/feedbackData';
import * as fromTrainingSite from '~app/training/reducers';

@Component({
  selector: 'bspl-logout-page',
  templateUrl: './logout-page.component.html',
  styleUrls: ['./logout-page.component.css']
})
export class LogoutPageComponent implements OnInit, OnDestroy {
  public userDataId$: Observable<number>;
  public showAutoLogoutMessage = false;
  public loading$: Observable<boolean>;
  private destroy$ = new Subject();

  constructor(private store: Store<AppState>) {
    this.userDataId$ = store.pipe(select(fromTrainingSite.getUserDataIdState));
    this.loading$ = store.pipe(select(fromTrainingSite.getLogoutLoadingState));
  }

  ngOnInit() {
    this.store
      .pipe(select(fromTrainingSite.getShowAutoLogoutMessageState), filter(Boolean), takeUntil(this.destroy$))
      .subscribe(() => {
        this.showAutoLogoutMessage = true;
        this.store.dispatch(FinishPageActions.showAutoLogoutMessage());
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  onFeedbackFormSubmitted(feedbackData: FeedbackData) {
    this.store.dispatch(FinishPageActions.submitFeedback(feedbackData));
  }
}
