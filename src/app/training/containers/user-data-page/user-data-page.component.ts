import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { StartPageActions } from '~app/training/actions';
import { UserDataFormComponent } from '~app/training/components/user-data-form/user-data-form.component';
import { UserData } from '~app/training/models/userData';
import * as fromTrainingSite from '~app/training/reducers';
import { UserDataLocalStorageService } from '~app/training/services/user-data-local-storage.service';

@Component({
  selector: 'bspl-user-data-page',
  templateUrl: './user-data-page.component.html',
  styleUrls: ['./user-data-page.component.css']
})
export class UserDataPageComponent implements AfterViewInit, OnDestroy {
  @ViewChild(UserDataFormComponent, { static: true }) userDataFormComponent: UserDataFormComponent;

  public isAirlineUser$: Observable<boolean>;
  public isAgentUser$: Observable<boolean>;
  public loading$: Observable<boolean>;
  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef,
    private userDataLocalStorageService: UserDataLocalStorageService
  ) {
    this.isAirlineUser$ = this.store.pipe(select(fromTrainingSite.isAirlineUser));
    this.isAgentUser$ = this.store.pipe(select(fromTrainingSite.isAgentUser));
    this.loading$ = this.store.pipe(select(fromTrainingSite.getUserDataLoadingState));
  }

  ngAfterViewInit() {
    this.initUserDataFromAutofill();
  }

  initUserDataFromAutofill() {
    this.store.pipe(select(fromTrainingSite.getUserLoginDataState), takeUntil(this.destroy$)).subscribe(loginData => {
      const userData = this.userDataLocalStorageService.getUserData(loginData);

      if (userData) {
        this.userDataFormComponent.form.setValue(userData);
        this.changeDetectorRef.detectChanges();
      }
    });
  }

  onUserDataFormSubmitted(userData: UserData) {
    this.store.dispatch(StartPageActions.addUserData({ userData }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
