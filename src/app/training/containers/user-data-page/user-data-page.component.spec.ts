import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

import { UserDataPageComponent } from './user-data-page.component';
import { UserDataLocalStorageService } from '~app/training/services/user-data-local-storage.service';

const userDataLocalStorageServiceStub = createSpyObject(UserDataLocalStorageService);

describe('UserDataPageComponent', () => {
  let component: UserDataPageComponent;
  let fixture: ComponentFixture<UserDataPageComponent>;

  beforeEach(waitForAsync(() => {
    const initialState = {
      trainingSite: {
        userLoginData: {
          userName: '',
          authorizationValue: '',
          error: ''
        },
        userLogoutData: {
          showAutoLogoutMessage: false,
          loading: false
        },
        userDataState: {
          userData: {
            id: 12343214,
            email: '',
            bspNames: [],
            code: '',
            firstName: '',
            lastName: ''
          },
          loading: false
        }
      }
    };

    TestBed.configureTestingModule({
      declarations: [UserDataPageComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({ initialState }),
        {
          provide: UserDataLocalStorageService,
          useValue: userDataLocalStorageServiceStub
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDataPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
