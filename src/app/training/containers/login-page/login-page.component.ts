import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '~app/reducers';
import { LoginPageActions } from '~app/training/actions';
import { LoginData } from '~app/training/models/loginData';
import * as fromTrainingSite from '~app/training/reducers';

@Component({
  selector: 'bspl-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {
  public warningMessage$: Observable<string>;

  constructor(private store: Store<AppState>) {
    this.warningMessage$ = store.pipe(select(fromTrainingSite.getLoginErrorMessage));
  }

  onLoginFormSubmitted(loginData: LoginData) {
    this.store.dispatch(LoginPageActions.addUserLoginData(loginData));
  }
}
