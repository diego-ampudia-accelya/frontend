import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, concatMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AuthService } from '~app/auth/services/auth.service';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import { StartPageActions, UserDataApiActions } from '~app/training/actions';
import * as fromTrainingSite from '~app/training/reducers';
import { UserDataLocalStorageService } from '~app/training/services/user-data-local-storage.service';
import { UserDataApiService } from '~app/training/services/user-data.api.service';

@Injectable()
export class UserDataEffects {
  $addUserData = createEffect(() =>
    this.actions$.pipe(
      ofType(StartPageActions.addUserData),
      withLatestFrom(this.store.pipe(select(fromTrainingSite.getUserLoginDataState))),
      tap(([{ userData }, loginData]) => {
        this.userDataLocalStorageService.storeUserData(userData, loginData);
      }),
      switchMap(([{ userData }, loginData]) =>
        this.userDataApiService.requestUserDataLogin(loginData, userData).pipe(
          tap((res: any) => {
            if (res.token) {
              // this.tokenService.setToken(res.token as Token);
            }
          }),
          catchError((res: any) => {
            const responseError = res.error;
            let errorMessage = '';

            if (responseError.errorCode === 403 && responseError.errorMessage === 'Credentials are not valid') {
              errorMessage = 'trainingSite.login.invalid_credentials';
            } else {
              errorMessage = 'trainingSite.login.maximum_connections_exceeded';
            }

            return of({ errorMessage });
          })
        )
      ),
      concatMap(res => {
        if (!res.errorMessage) {
          return this.authService.loadUserProfile().pipe(
            // tap(user => (user ? this.store.dispatch(new Login({ user })) : this.tokenService.clear())),
            map(user => [res, user]),
            catchError(errorRes =>
              // this.tokenService.clear();

              of([res, { errorMessage: errorRes.error.errorMessage }])
            )
          );
        } else {
          return of([res, {}]);
        }
      }),
      map(([res, userProfileRes]) => {
        const errorMessage: string = res.errorMessage || (userProfileRes as any).errorMessage;

        if (errorMessage) {
          return UserDataApiActions.loginFailure({ errorMessage });
        }

        return UserDataApiActions.loginSuccess({ id: res.id });
      })
    )
  );

  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserDataApiActions.loginSuccess),
        tap(() => {
          this.router.navigate([ROUTES.DASHBOARD.url]);
        })
      ),
    { dispatch: false }
  );

  loginFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserDataApiActions.loginFailure),
        tap(() => {
          this.router.navigate([ROUTES.TRAINING_LOGIN.url]);
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<AppState>,
    private userDataApiService: UserDataApiService,
    private authService: AuthService,
    private userDataLocalStorageService: UserDataLocalStorageService
  ) {}
}
