import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';

import { ROUTES } from '~app/shared/constants/routes';
import { LoginPageActions } from '~app/training/actions';

@Injectable()
export class LoginEffects {
  $addUserLoginData = createEffect(
    () =>
      this.actions$.pipe(
        ofType(LoginPageActions.addUserLoginData),
        tap(() => this.router.navigateByUrl(ROUTES.TRAINING_START.url))
      ),
    { dispatch: false }
  );

  constructor(private actions$: Actions, private router: Router) {}
}
