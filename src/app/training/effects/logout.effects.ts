import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import { FeedbackApiActions, FinishPageActions, SessionActions } from '~app/training/actions';
// import { FeedbackData } from '~app/training/models/feedbackData';
import * as fromTrainingSite from '~app/training/reducers';
import { FeedbackApiService } from '~app/training/services/feedback.api.service';

@Injectable()
export class LogoutEffects {
  $submitFeedback = createEffect(() =>
    this.actions$.pipe(
      ofType(FinishPageActions.submitFeedback),
      withLatestFrom(this.store.pipe(select(fromTrainingSite.getLoginUrl))),
      switchMap(([feedbackData, loginUrl]) =>
        this.feedbackApiService.sendFeedback(feedbackData).pipe(
          map((res: any) => FeedbackApiActions.sendFeedbackSuccess({ id: res.id, loginUrl })),
          catchError(({ error: { errorMessage } }) =>
            of(FeedbackApiActions.sendFeedbackFailure({ errorMessage, loginUrl }))
          )
        )
      ),
      tap(() => {
        if (localStorage.getItem('welcomeDialogGotIt')) {
          localStorage.removeItem('welcomeDialogGotIt');
        }
        // this.tokenService.clear();
      })
    )
  );

  $sessionExpired = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SessionActions.sessionExpired),
        tap(() => {
          this.router.navigateByUrl(ROUTES.TRAINING_FINISH.url);
        })
      ),
    {
      dispatch: false
    }
  );

  $authLogout = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FeedbackApiActions.sendFeedbackSuccess, FeedbackApiActions.sendFeedbackFailure),
        tap(({ loginUrl }) => {
          if (loginUrl) {
            location.replace(location.origin + loginUrl);
          } else {
            this.router.navigateByUrl(ROUTES.TRAINING_LOGIN.url);
          }
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private feedbackApiService: FeedbackApiService,
    private router: Router,
    private store: Store<AppState>
  ) {}
}
