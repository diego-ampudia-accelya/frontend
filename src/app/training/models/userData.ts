export interface UserData {
  id: number;
  email: string;
  code: string;
  firstName: string;
  lastName: string;
  bspNames: string[];
}
