export interface LoginData {
  userName: string;
  authorizationValue: string;
}
