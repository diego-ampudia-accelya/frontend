export interface FeedbackData {
  userDataId: string;
  reason?: string;
  rating?: number;
  textualFeedback?: string;
}
