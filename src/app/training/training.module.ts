import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { LoginPageComponent } from './containers/login-page/login-page.component';
import { LogoutPageComponent } from './containers/logout-page/logout-page.component';
import { UserDataPageComponent } from './containers/user-data-page/user-data-page.component';
import { TrainingRoutingModule } from './training-routing.module';
import { SharedModule } from '~app/shared/shared.module';
import { LoginFormComponent } from '~app/training/components/login-form/login-form.component';
import { LogoutFormComponent } from '~app/training/components/logout-form/logout-form.component';
import { TrainingPageLayoutComponent } from '~app/training/components/training-page-layout/training-page-layout.component';
import { UserDataFormComponent } from '~app/training/components/user-data-form/user-data-form.component';
import { LoginEffects, LogoutEffects, UserDataEffects } from '~app/training/effects';
import * as fromTraining from '~app/training/reducers';

@NgModule({
  declarations: [
    LoginFormComponent,
    UserDataFormComponent,
    LogoutFormComponent,
    TrainingPageLayoutComponent,
    LoginPageComponent,
    LogoutPageComponent,
    UserDataPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TrainingRoutingModule,
    NgbRatingModule,
    StoreModule.forFeature(fromTraining.trainingSiteFeatureKey, fromTraining.reducers),
    EffectsModule.forFeature([LoginEffects, UserDataEffects, LogoutEffects])
  ],
  providers: []
})
export class TrainingModule {}
