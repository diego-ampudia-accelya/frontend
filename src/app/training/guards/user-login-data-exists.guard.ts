import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import * as fromTrainingSite from '~app/training/reducers';

@Injectable({
  providedIn: 'root'
})
export class UserLoginDataExistsGuard implements CanActivate {
  constructor(private store: Store<AppState>, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromTrainingSite.getLoginDataUserName),
      tap(userName => {
        if (!userName) {
          this.router.navigateByUrl(ROUTES.TRAINING_LOGIN.url);
        }
      }),
      switchMap(userName => of(!!userName)),
      take(1)
    );
  }
}
