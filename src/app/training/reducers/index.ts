import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import { AppState } from '~app/reducers';
import * as fromLogin from '~app/training/reducers/login.reducer';
import * as fromLogout from '~app/training/reducers/logout.reducer';
import * as fromUserData from '~app/training/reducers/user-data.reducer';

export const trainingSiteFeatureKey = 'trainingSite';

export interface TrainingSiteState {
  [fromLogin.userLoginDataFeatureKey]: fromLogin.State;
  [fromLogout.userLogoutDataFeatureKey]: fromLogout.State;
  [fromUserData.userDataFeatureKey]: fromUserData.State;
}

export interface State extends AppState {
  [trainingSiteFeatureKey]: TrainingSiteState;
}

export function reducers(state: TrainingSiteState | undefined, action: Action) {
  return combineReducers({
    [fromLogin.userLoginDataFeatureKey]: fromLogin.reducer,
    [fromLogout.userLogoutDataFeatureKey]: fromLogout.reducer,
    [fromUserData.userDataFeatureKey]: fromUserData.reducer
  })(state, action);
}

export const getTrainingSiteState = createFeatureSelector<State, TrainingSiteState>(trainingSiteFeatureKey);

export const getUserLoginDataState = createSelector(getTrainingSiteState, state => state.userLoginData);

export const getLoginUrl = createSelector(getUserLoginDataState, fromLogin.getLoginUrl);

export const getLoginDataUserName = createSelector(getUserLoginDataState, fromLogin.getUserName);

export const getLoginErrorMessage = createSelector(getUserLoginDataState, fromLogin.getErrorMessage);

export const isAirlineUser = createSelector(getUserLoginDataState, fromLogin.isAirlineUser);

export const isAgentUser = createSelector(getUserLoginDataState, fromLogin.isAgentUser);

export const getLoginRequestState = createSelector(getTrainingSiteState, state => ({
  ...state.userLoginData,
  ...state.userDataState
}));

export const getUserDataState = createSelector(getTrainingSiteState, state => state.userDataState);

export const getUserDataLoadingState = createSelector(getUserDataState, fromUserData.getLoading);

export const getUserDataIdState = createSelector(getUserDataState, fromUserData.getId);

export const getUserLogoutDataState = createSelector(getTrainingSiteState, state => state.userLogoutData);

export const getShowAutoLogoutMessageState = createSelector(
  getUserLogoutDataState,
  fromLogout.getShowAutoLogoutMessage
);

export const getLogoutLoadingState = createSelector(getUserLogoutDataState, fromLogout.getLoading);
