import { createReducer, on } from '@ngrx/store';

import { FeedbackApiActions, StartPageActions, UserDataApiActions } from '~app/training/actions';
import { UserData } from '~app/training/models/userData';

export const userDataFeatureKey = 'userDataState';

export interface State {
  userData: UserData;
  loading: boolean;
}

const initialSate: State = {
  userData: {
    id: null,
    email: '',
    bspNames: [],
    code: '',
    firstName: '',
    lastName: ''
  },
  loading: false
};

export const reducer = createReducer(
  initialSate,
  on(StartPageActions.addUserData, (state, { userData }) => ({
    ...state,
    userData,
    loading: true
  })),
  on(UserDataApiActions.loginFailure, state => ({
    ...state,
    loading: false
  })),
  on(UserDataApiActions.loginSuccess, (state, { id }) => ({
    ...state,
    loading: false,
    userData: { ...state.userData, id }
  })),
  on(FeedbackApiActions.sendFeedbackSuccess, FeedbackApiActions.sendFeedbackFailure, () => ({ ...initialSate }))
);

export const getLoading = (state: State): boolean => state.loading;

export const getId = (state: State): number => state.userData.id;
