import { createReducer, on } from '@ngrx/store';

import { FeedbackApiActions, FinishPageActions, SessionActions } from '~app/training/actions';

export const userLogoutDataFeatureKey = 'userLogoutData';

export interface State {
  showAutoLogoutMessage: boolean;
  loading: boolean;
}

const initialSate: State = {
  showAutoLogoutMessage: false,
  loading: false
};

export const reducer = createReducer(
  initialSate,
  on(FinishPageActions.submitFeedback, state => ({
    ...state,
    loading: true
  })),
  on(FinishPageActions.showAutoLogoutMessage, state => ({
    ...state,
    showAutoLogoutMessage: false
  })),
  on(SessionActions.sessionExpired, state => ({
    ...state,
    showAutoLogoutMessage: true
  })),
  on(FeedbackApiActions.sendFeedbackSuccess, FeedbackApiActions.sendFeedbackFailure, state => ({
    ...state,
    showAutoLogoutMessage: false,
    loading: false
  }))
);

export const getShowAutoLogoutMessage = (state: State): boolean => state.showAutoLogoutMessage;

export const getLoading = (state: State): boolean => state.loading;
