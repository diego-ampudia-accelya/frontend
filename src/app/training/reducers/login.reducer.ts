import { createReducer, on } from '@ngrx/store';

import { AutoLoginActions, FeedbackApiActions, LoginPageActions, UserDataApiActions } from '~app/training/actions';

export const userLoginDataFeatureKey = 'userLoginData';

export interface State {
  userName: string;
  authorizationValue: string;
  errorMessage: string;
  loginUrl: string;
}

const initialSate: State = {
  userName: '',
  authorizationValue: '',
  errorMessage: '',
  loginUrl: ''
};

export const reducer = createReducer(
  initialSate,
  on(LoginPageActions.addUserLoginData, (state, { userName, authorizationValue }) => ({
    ...state,
    error: '',
    loginUrl: '',
    userName,
    authorizationValue
  })),
  on(AutoLoginActions.autoLoginAddUserLoginData, (state, { userName, authorizationValue }) => ({
    ...state,
    error: '',
    loginUrl: `/training/start?${btoa(userName)}`,
    userName,
    authorizationValue
  })),
  on(UserDataApiActions.loginFailure, (state, { errorMessage }) => ({
    ...state,
    errorMessage
  })),
  on(FeedbackApiActions.sendFeedbackSuccess, FeedbackApiActions.sendFeedbackFailure, () => ({ ...initialSate }))
);

export const getUserName = (state: State): string => state.userName;

export const getErrorMessage = (state: State): string => state.errorMessage;

export const getLoginUrl = (state: State): string => state.loginUrl;

export const isAirlineUser = (state: State): boolean => !!state.userName.match(/airline/);

export const isAgentUser = (state: State): boolean => !!state.userName.match(/agent/);
