import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ROUTES } from '~app/shared/constants/routes';
import { LoginPageComponent } from '~app/training/containers/login-page/login-page.component';
import { LogoutPageComponent } from '~app/training/containers/logout-page/logout-page.component';
import { UserDataPageComponent } from '~app/training/containers/user-data-page/user-data-page.component';
import { UserLoginDataExistsGuard } from '~app/training/guards/user-login-data-exists.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ROUTES.TRAINING_LOGIN.path,
        component: LoginPageComponent
      },
      {
        path: ROUTES.TRAINING_START.path,
        component: UserDataPageComponent,
        canActivate: [UserLoginDataExistsGuard]
      },
      {
        path: ROUTES.TRAINING_FINISH.path,
        component: LogoutPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingRoutingModule {}
