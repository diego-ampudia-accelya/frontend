import { createAction, props } from '@ngrx/store';

import { LoginData } from '~app/training/models/loginData';

export const autoLoginAddUserLoginData = createAction('[Auto Login] Add User Login Data', props<LoginData>());
