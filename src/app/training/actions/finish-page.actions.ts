import { createAction, props } from '@ngrx/store';

import { FeedbackData } from '~app/training/models/feedbackData';

export const submitFeedback = createAction('[Finish Page] Submit FeedBack', props<FeedbackData>());

export const showAutoLogoutMessage = createAction('[Finish Page]  Show Auto Logout Message');
