import * as AutoLoginActions from '~app/training/actions/auto-login.actions';
import * as FeedbackApiActions from '~app/training/actions/feedback-api.actions';
import * as FinishPageActions from '~app/training/actions/finish-page.actions';
import * as LoginPageActions from '~app/training/actions/login-page.actions';
import * as SessionActions from '~app/training/actions/session.actions';
import * as StartPageActions from '~app/training/actions/start-page.actions';
import * as UserDataApiActions from '~app/training/actions/user-data-api.actions';

export {
  AutoLoginActions,
  LoginPageActions,
  StartPageActions,
  UserDataApiActions,
  FinishPageActions,
  FeedbackApiActions,
  SessionActions
};
