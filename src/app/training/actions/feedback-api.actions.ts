import { createAction, props } from '@ngrx/store';

export const FEEDBACK_API_SEND_SUCCESS = '[Feedback/API] Send Success';

export const FEEDBACK_API_SEND_FAILURE = '[Feedback/API] Send Failure';

export const sendFeedbackSuccess = createAction(FEEDBACK_API_SEND_SUCCESS, props<{ id: number; loginUrl: string }>());

export const sendFeedbackFailure = createAction(
  FEEDBACK_API_SEND_FAILURE,
  props<{ errorMessage: string; loginUrl: string }>()
);
