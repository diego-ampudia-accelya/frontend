import { createAction, props } from '@ngrx/store';

import { LoginData } from '~app/training/models/loginData';

export const addUserLoginData = createAction('[Login Page] Add User Login Data', props<LoginData>());
