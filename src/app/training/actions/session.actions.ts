import { createAction } from '@ngrx/store';

export const sessionExpired = createAction('[Session] Expired');
