import { createAction, props } from '@ngrx/store';

export const loginSuccess = createAction('[UserData/API] Login Success', props<{ id: number }>());

export const loginFailure = createAction('[UserData/API] Login Failure', props<{ errorMessage: any }>());
