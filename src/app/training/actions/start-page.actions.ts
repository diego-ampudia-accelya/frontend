import { createAction, props } from '@ngrx/store';

import { UserData } from '~app/training/models/userData';

export const addUserData = createAction('[Start Page] Add User Data', props<{ userData: UserData }>());
