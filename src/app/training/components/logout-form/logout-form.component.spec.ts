import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { LogoutFormComponent } from './logout-form.component';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

describe('LogoutFormComponent', () => {
  let component: LogoutFormComponent;
  let fixture: ComponentFixture<LogoutFormComponent>;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LogoutFormComponent, TranslatePipeMock],
      providers: [FormBuilder, { provide: L10nTranslationService, useValue: translationServiceSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
