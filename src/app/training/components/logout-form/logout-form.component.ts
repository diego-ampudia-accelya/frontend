import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { FormUtil } from '~app/shared/helpers';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { FeedbackData } from '~app/training/models/feedbackData';

@Component({
  selector: 'bspl-logout-form',
  templateUrl: './logout-form.component.html',
  styleUrls: ['./logout-form.component.scss']
})
export class LogoutFormComponent implements OnInit {
  @Input() showAutoLogoutMessage = false;
  @Input() set userDataId(value: number) {
    this._userDataId = value;

    if (this.form) {
      this.form.get('userDataId').setValue(value);
    }
  }

  @Output() public submitted = new EventEmitter<FeedbackData>();

  public form: FormGroup;

  public radioButtonOptions = [
    {
      value: 'continue_later',
      label: 'trainingSite.logout.reasons.continue_later'
    },
    {
      value: 'training_completed',
      label: 'trainingSite.logout.reasons.training_completed'
    },
    {
      value: 'cannot_practice',
      label: 'trainingSite.logout.reasons.cannot_practice'
    }
  ];

  public autoLogoutMessage = '';

  public ratingWasChanged = false;

  private _userDataId: number = null;

  constructor(private formBuilder: FormBuilder, private translationService: L10nTranslationService) {}

  ngOnInit() {
    this.autoLogoutMessage = this.translationService.translate('trainingSite.logout.autoLogoutMessage', {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      session_duration: appConfiguration.trainingSessionConfiguration.duration / 60
    });
    this.createForm();
  }

  createForm() {
    const formConfig: any = {
      userDataId: [this._userDataId],
      reason: [this.radioButtonOptions[0].value, { updateOn: 'change' }],
      rating: [0, { updateOn: 'change' }],
      textualFeedback: ['', { validators: [Validators.maxLength(10000)], updateOn: 'change' }]
    };

    this.form = this.formBuilder.group(formConfig);
  }

  onRateChange() {
    this.ratingWasChanged = true;
  }

  onFormSubmit(event: any) {
    event.preventDefault();

    FormUtil.showControlState(this.form);

    if (this.form.valid) {
      this.submitted.emit(this.form.value);
    }
  }
}
