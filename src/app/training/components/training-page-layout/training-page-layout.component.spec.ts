import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TrainingPageLayoutComponent } from './training-page-layout.component';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

describe('TrainingPageLayoutComponent', () => {
  let component: TrainingPageLayoutComponent;
  let fixture: ComponentFixture<TrainingPageLayoutComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TrainingPageLayoutComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingPageLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
