import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bspl-training-page-layout',
  templateUrl: './training-page-layout.component.html',
  styleUrls: ['./training-page-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrainingPageLayoutComponent {
  @Input() title = '';
  @Input() loading = false;
}
