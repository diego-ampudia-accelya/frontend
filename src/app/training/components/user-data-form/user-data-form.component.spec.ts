import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { UserDataFormComponent } from './user-data-form.component';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';
import { SessionTimeService } from '~app/training/services/session-time.service';

const createSpyObj = jasmine.createSpyObj;
const translationServiceSpy = createSpyObj('L10nTranslationService', ['translate']);
const sessionTimeServiceSpy = createSpyObj('SessionTimeService', ['start', 'getResetTime']);

describe('UserDataFormComponent', () => {
  let component: UserDataFormComponent;
  let fixture: ComponentFixture<UserDataFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserDataFormComponent, TranslatePipeMock],
      providers: [
        FormBuilder,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: SessionTimeService, useValue: sessionTimeServiceSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDataFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
