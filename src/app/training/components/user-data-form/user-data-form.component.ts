import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import bsps from '~app/shared/constants/bsps';
import { FormUtil } from '~app/shared/helpers';
import { UserData } from '~app/training/models/userData';
import { SessionTimeService } from '~app/training/services/session-time.service';

@Component({
  selector: 'bspl-user-data-form',
  templateUrl: './user-data-form.component.html',
  styleUrls: ['./user-data-form.component.scss']
})
export class UserDataFormComponent implements OnInit {
  @Input() showAirlineCode = false;

  @Input() showAgentCode = false;

  @Input() notificationMessage = '';

  @Output() submitted = new EventEmitter<UserData>();

  public form: FormGroup;
  public bspOptions = bsps.map(bspName => ({ value: bspName, label: bspName }));
  public isAirlineUser = false;
  public isAgentUser = false;

  public emailCustomErrorMessages = {
    email: 'trainingSite.user-data.email_error',
    required: 'trainingSite.user-data.email_error'
  };

  public bspNamesCustomErrorMessages = {
    required: 'trainingSite.user-data.bsps_error'
  };

  public airlineCodeCustomErrorMessages = {
    required: 'trainingSite.user-data.airline_code_error',
    pattern: 'trainingSite.user-data.airline_code_error'
  };

  public agentCodeCustomErrorMessages = {
    required: 'trainingSite.user-data.agent_code_error',
    pattern: 'trainingSite.user-data.agent_code_error'
  };

  constructor(
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private sessionTimeService: SessionTimeService
  ) {}

  ngOnInit() {
    this.notificationMessage = this.translationService.translate('trainingSite.user-data.training-site-info', {
      period: this.sessionTimeService.getResetTime()
    });
    this.createForm();
  }

  createForm() {
    let codeFieldValidators = [];

    if (this.showAgentCode) {
      codeFieldValidators = [Validators.pattern('^\\d{7}$'), Validators.required];
    } else if (this.showAirlineCode) {
      codeFieldValidators = [Validators.pattern('^\\d{3}$'), Validators.required];
    }

    const formConfig: any = {
      email: ['', { validators: [Validators.email, Validators.required], updateOn: 'change' }],
      bspNames: [null, { validators: [Validators.required], updateOn: 'change' }],
      code: ['', { validators: codeFieldValidators, updateOn: 'change' }],
      firstName: ['', { updateOn: 'change' }],
      lastName: ['', { updateOn: 'change' }]
    };

    this.form = this.formBuilder.group(formConfig);
  }

  onFormSubmit(event: Event) {
    event.preventDefault();

    FormUtil.showControlState(this.form);

    if (this.form.valid) {
      this.submitted.emit(this.form.value);
    }
  }
}
