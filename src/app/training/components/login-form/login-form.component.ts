import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FormUtil } from '~app/shared/helpers';
import { LoginData } from '~app/training/models/loginData';

@Component({
  selector: 'bspl-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Input() public warningMessage = '';
  @Output() submitted = new EventEmitter<LoginData>();

  public form: FormGroup;

  public get buttonLabel(): string {
    return this.warningMessage ? 'trainingSite.login.button.try_again_lbl' : 'trainingSite.login.button.lbl';
  }

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    const formConfig: any = {
      userName: ['', { validators: [Validators.required], updateOn: 'change' }],
      password: ['', { validators: [Validators.required], updateOn: 'change' }]
    };

    this.form = this.formBuilder.group(formConfig);
  }

  onFormSubmit(event: Event) {
    event.preventDefault();

    FormUtil.showControlState(this.form);

    if (this.form.valid) {
      const value = this.form.value;
      this.submitted.emit({
        userName: value.userName,
        authorizationValue: btoa(`${value.userName}:${value.password}`)
      });
    }
  }
}
