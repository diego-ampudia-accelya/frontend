import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { LoginFormComponent } from './login-form.component';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LoginFormComponent, TranslatePipeMock],
      providers: [FormBuilder],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('createForm should set the from property value', () => {
    component.form = null;
    component.createForm();
    expect(component.form).toBeTruthy();
  });

  it('createForm to create form with userName and password controls', () => {
    component.form = null;
    component.createForm();
    expect(Object.keys(component.form.controls)).toEqual(['userName', 'password']);
  });

  it('buttonLable should be button.lbl if warningMessage is empty', () => {
    component.warningMessage = '';
    fixture.detectChanges();
    expect(component.buttonLabel).toBe('trainingSite.login.button.lbl');
  });

  it('buttonLable should be button.lbl if warningMessage has value', () => {
    component.warningMessage = 'test message';
    fixture.detectChanges();
    expect(component.buttonLabel).toBe('trainingSite.login.button.try_again_lbl');
  });

  it('onFormSubmit should set the components touched', () => {
    const eventObjSpy = jasmine.createSpyObj('Event', ['preventDefault']);
    component.onFormSubmit(eventObjSpy);
    fixture.detectChanges();
    expect(component.form.get('userName').touched).toBeTruthy();
    expect(component.form.get('password').touched).toBeTruthy();
  });

  it('onFormSubmit should emit the submitted event if the form is valid', () => {
    const eventObjSpy = jasmine.createSpyObj('Event', ['preventDefault']);
    const credentials = {
      userName: 'user',
      password: 'pass'
    };

    component.form.setValue(credentials);
    spyOn(component.submitted, 'emit');

    component.onFormSubmit(eventObjSpy);

    expect(component.submitted.emit).toHaveBeenCalledWith({
      userName: 'user',
      authorizationValue: btoa('user:pass')
    });
  });
});
