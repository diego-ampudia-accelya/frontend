import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { AuthService } from './auth.service';
import { LOCATION_TOKEN } from './injection-tokens';
import { TokenService } from './token.service';
import { FakePermissions } from '~app/shared/constants';
import { createAirlineMultiCountryUser } from '~app/shared/mocks/airline-user';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AuthConfiguration } from '~app/shared/models';
import { AirlineUser, User } from '~app/shared/models/user.model';
import { AppConfigurationService, UsersProfileManagementService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

describe('AuthService', () => {
  let authService: AuthService;
  let userProfileServiceMock: SpyObject<UsersProfileManagementService>;
  let locationMock;
  let localBSPTimeMock: SpyObject<LocalBspTimeService>;

  beforeEach(() => {
    const auth = {
      login_uri: '/login',
      logout_uri: '/logout',
      logout_request_uri: '/logout',
      client_id: '123',
      client_secret: '234'
    } as AuthConfiguration;

    const locationReplaceMock = jasmine.createSpy('window.location.replace').and.callFake(() => {});
    locationMock = {
      replace: locationReplaceMock,
      origin: 'http://localhost:9876/'
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthService,
        mockProvider(TokenService),
        mockProvider(AppConfigurationService, {
          authConfiguration: auth
        }),
        mockProvider(UsersProfileManagementService),
        { provide: LOCATION_TOKEN, useValue: locationMock },
        mockProvider(LocalBspTimeService)
      ]
    }).compileComponents();

    authService = TestBed.inject(AuthService);
    userProfileServiceMock = TestBed.inject<any>(UsersProfileManagementService);
    userProfileServiceMock.getCurrentUserData.and.returnValue(of(createIataUser()));
    localBSPTimeMock = TestBed.inject<any>(LocalBspTimeService);
    localBSPTimeMock.setOffset.and.returnValue(of(null));
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should navigate to login_uri on redirectToLogin', () => {
    authService.redirectToLogin();

    expect(locationMock.replace).toHaveBeenCalledWith('/login');
  });

  it('should redirect to training login', () => {
    authService.redirectToTrainingLogin();

    expect(locationMock.replace).toHaveBeenCalledWith(jasmine.stringMatching('/training/login'));
  });

  it('should redirect to logout page', () => {
    authService.redirectToLogoutPage();

    expect(locationMock.replace).toHaveBeenCalledWith('/logout');
  });

  it('should return user when load profile', () => {
    const expected = {
      ...createIataUser(),
      isSuperUser: false,
      permissions: [
        FakePermissions.readOnlineHelpIATA,
        'USER',
        'user_maintenance',
        'rBsp',
        'bsp_currency_assignation_query',
        'bsp_gds_assignation_query',
        'training-login',
        'training-start',
        'training-finish',
        'reports',
        'not-found',
        'rBillAnlsTotals',
        'gEPdf'
      ]
    } as User;

    authService.loadUserProfile().subscribe((result: User) => {
      expect(jasmine.objectContaining<User>(result)).toEqual(jasmine.objectContaining<User>(expected));
    });
  });

  it('should set default BSP local time on user profile load', fakeAsync(() => {
    const mockUser: AirlineUser = {
      ...createAirlineMultiCountryUser(),
      defaultIsoc: 'AU'
    };

    userProfileServiceMock.getCurrentUserData.and.returnValue(of(mockUser));

    authService.loadUserProfile().subscribe();
    tick();

    // Calling local BSP time with default BSP id (AU)
    expect(localBSPTimeMock.setOffset).toHaveBeenCalledWith(10018);
  }));
});
