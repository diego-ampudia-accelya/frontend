import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Action } from '@ngrx/store';
import { of, throwError } from 'rxjs';

import { Logout, RedirectToLogin } from '../actions/auth.actions';
import { LOCATION_TOKEN } from './injection-tokens';
import { TokenService } from '~app/auth/services/token.service';
import { UnauthorizedError } from '~app/shared/errors';
import { AppConfigurationService } from '~app/shared/services';

describe('Token Service', () => {
  let service: TokenService;
  let httpClient: SpyObject<HttpClient>;
  let locationSpy: Location;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;
  let clock: jasmine.Clock;
  const now = new Date('2020-01-01');

  beforeEach(() => {
    locationSpy = {} as Location;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        TokenService,
        { provide: HttpClient, useValue: httpClient },
        { provide: LOCATION_TOKEN, useValue: locationSpy },
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, {
          authConfiguration: {
            url: 'https://auth.com',
            grant_type_authorization: 'grant_type_authorization',
            grant_type_refresh: 'grant_type_refresh',
            client_id: 'client_id',
            client_secret: 'client_secret',
            redirect_uri: 'redirect_uri'
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(TokenService);
    appConfigurationSpy = TestBed.inject<any>(AppConfigurationService);
    httpClient = TestBed.inject<any>(HttpClient);
    httpClient.post.and.returnValue(of(null));

    // Using jasmine.clock automatically implies fakeAsync in tests
    // See https://github.com/angular/zone.js/pull/1009
    clock = jasmine.clock();
    clock.mockDate(now);
    clock.install();
  });

  afterEach(() => {
    sessionStorage.clear();
    clock.uninstall();
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('initAccessToken', () => {
    it('should send a post request to get auth cookies when code parameter is found in the URL', () => {
      const code = encodeURIComponent('authorization code');
      const expectedRequest =
        'grant_type=grant_type_authorization&' +
        'client_id=client_id&' +
        'client_secret=client_secret&' +
        'redirect_uri=redirect_uri&' +
        `code=authorization+code`;
      locationSpy.search = `code=${code}`;

      service.initTokenCookies().subscribe();

      expect(httpClient.post).toHaveBeenCalledWith('https://auth.com', expectedRequest, jasmine.anything());
    });

    it('should redirect to login when code parameter is missing', () => {
      let authAction;

      locationSpy.search = `?param1=1&param2=2&empty`;
      service.initTokenCookies().subscribe(action => (authAction = action));

      expect(httpClient.post).not.toHaveBeenCalled();

      expect(authAction).toEqual(new RedirectToLogin());
    });
  });

  describe('refreshCookies', () => {
    it('should send a request for new access cookie token with the right params', () => {
      const expectedRequest = 'grant_type=grant_type_refresh&' + 'client_id=client_id&' + 'client_secret=client_secret';
      httpClient.post.and.returnValue(of(null));

      service.refreshTokenCookies().subscribe();

      expect(httpClient.post).toHaveBeenCalledWith('https://auth.com', expectedRequest, jasmine.anything());
    });

    it('should send a request for new access cookies when UnauthorizedError is returned', () => {
      httpClient.post.and.returnValue(throwError(new UnauthorizedError({ errorCode: 401, errorMessage: 'Error' })));
      spyOn(service, 'refreshTokenCookies').and.returnValue(of(null));

      service.requestTokenCookies('').subscribe();

      expect(service.refreshTokenCookies).toHaveBeenCalled();
    });

    it('should return Logout action if refreshTokenCookies call fails with UnauthorizedError', () => {
      let actualResult: Action;

      httpClient.post.and.returnValue(throwError(new UnauthorizedError({ errorCode: 401, errorMessage: 'Error' })));

      service.refreshTokenCookies().subscribe(result => (actualResult = result));

      expect(actualResult).toEqual(new Logout());
    });

    it('should return null when no auth configuration is provided', () => {
      (appConfigurationSpy as any).authConfiguration = null;

      let actual;
      service.refreshTokenCookies().subscribe(result => (actual = result));

      expect(actual).toBe(null);
    });
  });
});
