import { Injectable } from '@angular/core';

import { FakePermissions, Permissions } from '~app/shared/constants';
import { UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
/**
 * Determines the permissions of a user based on their user type.
 * TODO: Revise when the permissions are available in the token.
 */
export class MockedPermissionsService {
  private allUserPermissions = [
    'training-login',
    'training-start',
    'training-finish',
    'reports',
    'not-found',
    Permissions.readBillingAnalysisTotals,
    Permissions.generateEnquiryPdf
  ];
  private permissionsByUserType = {
    [UserType.AIRLINE]: [FakePermissions.readOnlineHelpAirline],
    [UserType.AGENT]: [FakePermissions.readOnlineHelpAgent],
    [UserType.AGENT_GROUP]: [FakePermissions.readOnlineHelpAgentGroup],
    [UserType.THIRD_PARTY]: [FakePermissions.readOnlineHelpThirdParty],
    [UserType.GDS]: [FakePermissions.readOnlineHelpGDS],
    [UserType.IATA]: [
      FakePermissions.readOnlineHelpIATA,
      'USER',
      'user_maintenance',

      // master data permissions
      'rBsp',
      'bsp_currency_assignation_query',
      'bsp_gds_assignation_query'
    ]
  };

  /**
   * Get the permissions based on user type
   *
   * @param userType User type
   * @returns the permission set
   */
  getPermissionsFor(userType: UserType): string[] {
    const permissionsByUserType = this.permissionsByUserType[userType] || [];

    return [...permissionsByUserType, ...this.allUserPermissions];
  }
}
