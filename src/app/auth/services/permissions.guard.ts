import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take, tap, withLatestFrom } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuTabActions } from '~app/core/actions';
import * as fromCore from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { MenuTab } from '~app/shared/models/menu-tab.model';

@Injectable({
  providedIn: 'root'
})
export class PermissionsGuard implements CanActivate, CanActivateChild {
  private activeTab$ = this.store.pipe(select(fromCore.getActiveTab));

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private permissionsService: PermissionsService,
    private dialogService: DialogService
  ) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.permissionsService.permissions$.pipe(
      withLatestFrom(this.activeTab$),
      map(([_, activeTab]) => {
        const hasAccess = this.hasAccess(route);
        if (!hasAccess) {
          const url = activeTab ? activeTab.url : ROUTES.DASHBOARD.url;
          this.router.navigateByUrl(url);

          this.dialogService.openPermissionErrorDialog();
        }

        return hasAccess;
      })
    );
  }

  public canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }

  public checkActiveTabAccess() {
    this.activeTab$
      .pipe(
        tap(tabConfig => {
          const hasAccess = this.tabHasAccess(tabConfig);

          if (!hasAccess) {
            this.store.dispatch(new MenuTabActions.Close(tabConfig));
            this.dialogService.openPermissionErrorDialog();
          }
        }),
        take(1)
      )
      .subscribe();
  }

  private hasAccess(route: ActivatedRouteSnapshot): boolean {
    let hasAccess = true;
    if (route.data.requiredPermissions) {
      hasAccess = this.permissionsService.hasPermission(route.data.requiredPermissions);
    } else {
      const tabConfig = this.findTabConfig(route);
      if (tabConfig) {
        hasAccess = this.tabHasAccess(tabConfig);
      }
    }

    return hasAccess;
  }

  private tabHasAccess(tabConfig: MenuTab) {
    const hasPermission = this.permissionsService.hasPermission(tabConfig.permissionId);
    const hasUserType = this.permissionsService.hasUserType(tabConfig.allowedUserTypes);

    return hasPermission && hasUserType;
  }

  private findTabConfig(route: ActivatedRouteSnapshot) {
    let current = route;
    let routeConfig = null;

    while (current != null && routeConfig == null) {
      routeConfig = current.data.tab;
      current = current.parent;
    }

    return routeConfig;
  }
}
