import { Injectable, OnDestroy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { UserType } from '~app/shared/models/user.model';

@Injectable()
export class PermissionsService implements OnDestroy {
  public bspPermissionsMap: { [key: number]: string[] } = {};
  public permissions: string[] = [];
  public permissions$ = new BehaviorSubject<string[]>([]);

  private destroyed$ = new Subject();
  private userType: UserType;

  constructor(private store: Store<AppState>) {
    this.initPermissions();
    this.initUserType();
  }

  private initPermissions() {
    this.store
      .pipe(
        select(fromAuth.getPermissions),
        filter(({ permissions }) => permissions && permissions.length > 0),
        tap(({ permissions, bspPermissions }) => {
          this.permissions = permissions;
          this.permissions$.next(permissions);

          bspPermissions.forEach(item => (this.bspPermissionsMap[item.bspId] = item.permissions));
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  private initUserType() {
    this.store
      .pipe(
        select(fromAuth.getUserType),
        filter(userType => userType != null),
        tap(userType => (this.userType = userType)),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public hasPermission(permissionsToCheck: string | string[], bspId?: number): boolean {
    if (permissionsToCheck == null) {
      return true;
    }

    const permissions = isNaN(bspId) ? this.permissions : this.bspPermissionsMap[bspId] || [];
    permissionsToCheck = this.normalizeToArray(permissionsToCheck);

    return permissionsToCheck.some(permission => permissions.includes(permission));
  }

  public hasUserType(userTypesToCheck: UserType | UserType[]): boolean {
    if (userTypesToCheck == null) {
      return true;
    }

    userTypesToCheck = this.normalizeToArray(userTypesToCheck);

    return userTypesToCheck.includes(this.userType);
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private normalizeToArray<T>(value: T | T[]): T[] {
    return Array.isArray(value) ? value : [value];
  }
}
