import { Component } from '@angular/core';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { Route, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';

import { PermissionsGuard } from './permissions.guard';
import { PermissionsService } from './permissions.service';
import * as fromCore from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { UserType } from '~app/shared/models/user.model';

@Component({ template: '' })
class DummyComponent {}

describe('PermissionsGuard', () => {
  let permissionsGuard: PermissionsGuard;
  let router: Router;
  let permissionsServiceSpy: SpyObject<PermissionsService>;
  let mockStore: MockStore<AppState>;
  let dialogService: DialogService;
  const permissions$: Observable<string[]> = of([Permissions.readBillingStatement]);

  const routes: Route[] = [
    {
      path: '',
      canActivateChild: [PermissionsGuard],
      children: [
        { path: '', component: DummyComponent },
        { path: 'dashboards', component: DummyComponent },
        {
          path: 'airline/my',
          data: {
            tab: {
              allowedUserTypes: UserType.AIRLINE
            }
          },
          component: DummyComponent,
          children: [
            {
              path: 'configuration',
              component: DummyComponent,
              data: { requiredPermissions: Permissions.readAirlineSettings }
            }
          ]
        },
        {
          path: 'sales/billing-statement',
          data: { requiredPermissions: Permissions.readBillingStatement },
          component: DummyComponent
        },
        { path: 'unprotected', component: DummyComponent }
      ]
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [DummyComponent],
      providers: [
        PermissionsGuard,
        provideMockStore({ selectors: [{ selector: fromCore.getActiveTab, value: null }] }),
        {
          provide: PermissionsService,
          useValue: createSpyObject(PermissionsService, { permissions$ })
        }
      ]
    });
  }));

  beforeEach(() => {
    permissionsGuard = TestBed.inject(PermissionsGuard);
    router = TestBed.inject<any>(Router);
    permissionsServiceSpy = TestBed.inject<any>(PermissionsService);
    dialogService = TestBed.inject<any>(DialogService);

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(permissionsGuard).toBeDefined();
  });

  it('should navigate to unprotected route', fakeAsync(() => {
    router.navigateByUrl('/unprotected');
    tick();

    expect(router.url).toBe('/unprotected');
  }));

  describe('when route is protected by tab configuration', () => {
    const url = '/airline/my';
    it('should navigate successfully when user has allowed user type', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(true);
      permissionsServiceSpy.hasUserType.and.returnValue(true);

      router.navigateByUrl(url);
      tick();

      expect(router.url).toBe(url);
    }));

    it('should redirect to /dashboard when the user does not have the allowed user type', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(true);
      permissionsServiceSpy.hasUserType.and.returnValue(false);
      spyOn(dialogService, 'openPermissionErrorDialog').and.returnValue(of());

      router.navigateByUrl(url);
      tick();

      expect(router.url).toBe(ROUTES.DASHBOARD.url);
      expect(dialogService.openPermissionErrorDialog).toHaveBeenCalled();
    }));
  });

  describe('when route is protected by route configuration', () => {
    const url = '/sales/billing-statement';

    it('should navigate successfully when user has the required permission', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(true);

      router.navigateByUrl(url);
      tick();

      expect(router.url).toBe(url);
    }));

    it('should redirect to /dashboard when the user does not have the required permission', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(false);
      spyOn(dialogService, 'openPermissionErrorDialog').and.returnValue(of());

      router.navigateByUrl(url);
      tick();

      expect(router.url).toBe(ROUTES.DASHBOARD.url);
      expect(dialogService.openPermissionErrorDialog).toHaveBeenCalled();
    }));
  });

  describe('when route is protected by route configuration and its parent is protected by tab configuration', () => {
    const url = '/airline/my/configuration';

    it('should navigate successfully when user has access', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(true);
      permissionsServiceSpy.hasUserType.and.returnValue(true);

      router.navigateByUrl(url);
      tick();

      expect(router.url).toBe(url);
    }));

    it('should redirect to /dashboard when the user does not have permissions required by route configuration', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(false);
      permissionsServiceSpy.hasUserType.and.returnValue(true);
      spyOn(dialogService, 'openPermissionErrorDialog').and.returnValue(of());

      router.navigateByUrl(url);
      tick();

      expect(router.url).toBe(ROUTES.DASHBOARD.url);
      expect(dialogService.openPermissionErrorDialog).toHaveBeenCalled();
    }));

    it('should redirect to /dashboard when the user does not have permissions required by tab configuration', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(true);
      permissionsServiceSpy.hasUserType.and.returnValue(false);

      router.navigateByUrl(url);
      tick();

      expect(router.url).toBe(ROUTES.DASHBOARD.url);
    }));
  });
});
