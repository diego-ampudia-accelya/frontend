import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Dictionary } from 'lodash';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthActions, LoginFailed, Logout, RedirectToLogin } from '../actions/auth.actions';
import { LOCATION_TOKEN } from './injection-tokens';
import { ServerError, UnauthorizedError } from '~app/shared/errors';
import { Token } from '~app/shared/models/token.model';
import { AppConfigurationService } from '~app/shared/services';

const contentTypeXWwwFormUrlencoded: any = { 'Content-Type': 'application/x-www-form-urlencoded' };

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    @Inject(LOCATION_TOKEN) private location: Location
  ) {}

  public initTokenCookies(): Observable<AuthActions> {
    const jweToken = this.getJweToken();
    let authAction = of(null);

    if (this.appConfiguration.authConfiguration) {
      if (jweToken) {
        authAction = this.requestTokenCookies(jweToken);
      } else {
        authAction = of(new RedirectToLogin());
      }
    }

    return authAction;
  }

  public requestTokenCookies(jweToken: string): Observable<AuthActions> {
    const body = this.buildCodeAccessBody(jweToken);

    return this.sendTokenCookiesRequest(body).pipe(
      catchError((error: ServerError) => {
        const isUnauthorizedError = error instanceof UnauthorizedError;

        return isUnauthorizedError ? this.refreshTokenCookies() : of(new LoginFailed());
      })
    );
  }

  public refreshTokenCookies(): Observable<AuthActions> {
    if (this.appConfiguration.authConfiguration) {
      const body = this.buildRefreshBody();

      return this.sendTokenCookiesRequest(body).pipe(
        catchError((error: ServerError) => {
          const isUnauthorizedError = error instanceof UnauthorizedError;

          return isUnauthorizedError ? of(new Logout()) : of(new LoginFailed());
        })
      );
    }

    return of(null);
  }

  public getRedirectToken(): Observable<Token> {
    const body = this.buildRefreshBody();

    return this.sendTokenCookiesRequest(body, this.appConfiguration.authConfiguration.urlRedirectToken).pipe(
      // Null on error indicates there is no token available
      catchError(() => of(null))
    );
  }

  /**
   * Gets code jwe token  from the URL if present.
   *
   * @returns an jwe token
   */
  private getJweToken(): string | null {
    const queryParams = new URLSearchParams(this.location.search);

    return queryParams.get('code');
  }

  /**
   * Create the body for the init  JWE token request
   */
  private buildCodeAccessBody(jweToken: string) {
    const config = this.appConfiguration.authConfiguration;

    return {
      grant_type: config.grant_type_authorization,
      client_id: config.client_id,
      client_secret: config.client_secret,
      redirect_uri: config.redirect_uri,
      code: jweToken
    };
  }

  /**
   * Create the body for the refresh-token request
   */
  private buildRefreshBody() {
    const config = this.appConfiguration.authConfiguration;

    return {
      grant_type: config.grant_type_refresh,
      client_id: config.client_id,
      client_secret: config.client_secret
    };
  }

  private sendTokenCookiesRequest(
    requestBody: Dictionary<string>,
    url = this.appConfiguration.authConfiguration?.url
  ): Observable<any> {
    const headers = new HttpHeaders(contentTypeXWwwFormUrlencoded);
    const bodyParams = new URLSearchParams(Object.entries(requestBody)).toString();

    return this.http.post<any>(url, bodyParams, {
      headers,
      params: { noAuth: 'true' }
    });
  }
}
