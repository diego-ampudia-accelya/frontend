export class ObjectStorage<T> {
  private cache: T;

  constructor(private key: string, private storage: Storage) {}

  public get(): T {
    if (this.cache == null) {
      const value = this.storage.getItem(this.key);
      this.cache = value != null ? JSON.parse(value) : null;
    }

    return this.cache;
  }

  public set(value: T): void {
    if (value == null) {
      this.storage.removeItem(this.key);
    } else {
      this.storage.setItem(this.key, JSON.stringify(value));
    }
    this.cache = value;
  }
}
