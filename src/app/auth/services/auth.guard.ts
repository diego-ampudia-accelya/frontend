import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';

type CanActivateResult = Observable<boolean> | Promise<boolean> | boolean;

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  private user$ = this.store.pipe(select(getUser));

  constructor(private store: Store<AppState>) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): CanActivateResult {
    return this.user$.pipe(
      filter(user => !!user),
      map(user => !!user)
    );
  }

  public canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): CanActivateResult {
    return this.canActivate(route, state);
  }
}
