import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';

@Injectable({
  providedIn: 'root'
})
export class UnauthenticatedGuard implements CanActivate {
  private isLoggedIn$ = this.store.pipe(select(fromAuth.isLoggedIn));

  constructor(private store: Store<AppState>, private router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    return this.isLoggedIn$.pipe(
      take(1),
      map(isLoggedIn => (isLoggedIn ? this.router.createUrlTree([ROUTES.DASHBOARD.url]) : true))
    );
  }
}
