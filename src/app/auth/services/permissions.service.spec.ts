import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { PermissionsService } from './permissions.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { UserType } from '~app/shared/models/user.model';

let permissionsService: PermissionsService;

describe('PermissionsService', () => {
  beforeEach(() => {
    const initialState = {
      auth: {
        user: createAirlineUser()
      }
    };

    TestBed.configureTestingModule({
      providers: [
        PermissionsService,
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromAuth.getPermissions,
              value: {
                permissions: ['rAgn', 'rAril'],
                bspPermissions: [
                  { bspId: 10001, permissions: ['rAgn', 'rAirl'] },
                  { bspId: 10002, permissions: ['rAgn'] },
                  { bspId: 10003, permissions: ['rAirl'] }
                ]
              }
            },
            {
              selector: fromAuth.getUserType,
              value: UserType.AIRLINE
            }
          ]
        })
      ]
    }).compileComponents();

    permissionsService = TestBed.inject(PermissionsService);
  });

  it('should be created', () => {
    expect(permissionsService).toBeTruthy();
  });

  describe('hasPermission', () => {
    describe('with no bspId privided', () => {
      it('should return false if permission argument is empty string', () => {
        expect(permissionsService.hasPermission('')).toBeFalsy();
      });

      it('should return true if permission argument is null', () => {
        expect(permissionsService.hasPermission(null)).toBeTruthy();
      });

      it('should return true if permission argument is undefined', () => {
        expect(permissionsService.hasPermission(undefined)).toBeTruthy();
      });

      it('should return true if permission argument is provided', () => {
        expect(permissionsService.hasPermission('rAgn')).toBeTruthy();
      });

      it('should return false if empty array is passes as first argument', () => {
        expect(permissionsService.hasPermission([])).toBeFalsy();
      });

      it('should return false if array argument contains items, which do not match', () => {
        expect(permissionsService.hasPermission(['x', 'y'])).toBeFalsy();
      });

      it('should return true if array argument contains items, which match', () => {
        expect(permissionsService.hasPermission(['rAgn', 'rAirl', 'y'])).toBeTruthy();
      });
    });

    describe('with privided bspId', () => {
      it('should return false if permission exists, but and bspId does not match', () => {
        expect(permissionsService.hasPermission('rAgn', 2)).toBeFalsy();
      });

      it('should return true if permission and bspId arguments match', () => {
        expect(permissionsService.hasPermission('rAgn', 10001)).toBeTruthy();
      });

      it('should return false if empty array is passes as first argument', () => {
        expect(permissionsService.hasPermission([], 10001)).toBeFalsy();
      });

      it('should return false if array argument contains items, which do not match', () => {
        expect(permissionsService.hasPermission(['x', 'y'], 10001)).toBeFalsy();
      });

      it('should return true if array argument contains items, which match', () => {
        expect(permissionsService.hasPermission(['rAgn', 'rAirl', 'y'], 10001)).toBeTruthy();
      });
    });
  });

  describe('hasUserType', () => {
    it('should return true if userTypesToCheck argument is null', () => {
      expect(permissionsService.hasUserType(null)).toBeTruthy();
    });

    it('should return true if userTypesToCheck argument is undefined', () => {
      expect(permissionsService.hasUserType(undefined)).toBeTruthy();
    });

    it('should return true if userTypesToCheck argument is provided', () => {
      expect(permissionsService.hasUserType(UserType.AIRLINE)).toBeTruthy();
    });

    it('should return false if empty array is passes as first argument', () => {
      expect(permissionsService.hasUserType([])).toBeFalsy();
    });

    it('should return false if array argument contains items, which do not match', () => {
      expect(permissionsService.hasUserType([UserType.AGENT, UserType.IATA])).toBeFalsy();
    });

    it('should return true if array argument contains items, which match', () => {
      expect(permissionsService.hasUserType([UserType.AIRLINE, UserType.AGENT])).toBeTruthy();
    });
  });
});
