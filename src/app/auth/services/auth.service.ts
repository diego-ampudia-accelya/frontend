import { Inject, Injectable } from '@angular/core';
import { difference, uniq } from 'lodash';
import { Observable } from 'rxjs';
import { map, mapTo, shareReplay, switchMap } from 'rxjs/operators';

import { LOCATION_TOKEN } from './injection-tokens';
import { MockedPermissionsService } from '~app/auth/services/mocked-permissions.service';
import { ROUTES } from '~app/shared/constants/routes';
import { User } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { UsersProfileManagementService } from '~app/shared/services/users-profile-management.service';
import { NoConcurrentRequests } from '~app/shared/utils';
import { TrainingSitePermissionsService } from '~app/training/services/training-site-permissions.service';

@Injectable()
export class AuthService {
  constructor(
    private userProfileService: UsersProfileManagementService,
    private mockedPermissionsService: MockedPermissionsService,
    private trainingSitePermissionsService: TrainingSitePermissionsService,
    private appConfigurationService: AppConfigurationService,
    @Inject(LOCATION_TOKEN) private location: Location,
    private localBspTimeService: LocalBspTimeService
  ) {}

  public redirectToLogin(): void {
    const loginUrl = this.appConfigurationService.authConfiguration.login_uri;
    // This check is required by a security evaluation tool
    if (loginUrl !== null && loginUrl.length !== 0) {
      this.location.replace(loginUrl);
    }
  }

  public redirectToTrainingLogin(): void {
    this.location.replace(`${location.origin}${ROUTES.TRAINING_LOGIN.url}`);
  }

  public redirectToLogoutPage(): void {
    const logoutUrl = this.appConfigurationService.authConfiguration.logout_uri;
    // This check is required by a security evaluation tool
    if (logoutUrl !== null && logoutUrl.length !== 0) {
      this.location.replace(logoutUrl);
    }
  }

  @NoConcurrentRequests()
  public loadUserProfile(): Observable<User> {
    return this.userProfileService.getCurrentUserData().pipe(
      map(user => this.getUserWithExtendedPermissions(user)),
      switchMap(user => {
        const defaultBspId = user.bsps.find(bsp => bsp.isoCountryCode === user.defaultIsoc)?.id;

        return this.localBspTimeService.setOffset(defaultBspId).pipe(mapTo(user));
      }),
      shareReplay()
    );
  }

  private getUserWithExtendedPermissions(user: User) {
    const userPermissions = user.permissions || [];
    const serviceUserPermissions = this.mockedPermissionsService.getPermissionsFor(user.userType) || [];

    user.permissions = uniq([...userPermissions, ...serviceUserPermissions]);

    if (this.appConfigurationService.isTrainingSite) {
      this.trainingSitePermissionsService.applyTrainingSitePermissions(user);
    }

    user.permissions = this.removeDisabledPermissions(user.permissions);

    if (user.bspPermissions) {
      user.bspPermissions.forEach(
        bspPermission => (bspPermission.permissions = this.removeDisabledPermissions(bspPermission.permissions))
      );
    }

    const isSuperUser = user.email === 'iata1@example.com';

    return { ...user, isSuperUser };
  }

  private removeDisabledPermissions(permissions: string[]): string[] {
    return difference(permissions, this.appConfigurationService.disabledPermissions);
  }
}
