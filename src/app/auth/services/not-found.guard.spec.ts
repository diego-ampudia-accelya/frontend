import { TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { mockProvider, SpyObject } from '@ngneat/spectator';

import { NotFoundGuard } from './not-found.guard';
import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';

describe('Not-found guard', () => {
  let notFoundGuard: NotFoundGuard;
  let router: SpyObject<Router>;
  let dialogService: SpyObject<DialogService>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [NotFoundGuard, mockProvider(Router), mockProvider(DialogService)]
    });

    notFoundGuard = TestBed.inject(NotFoundGuard);
    router = TestBed.inject<any>(Router);
    dialogService = TestBed.inject<any>(DialogService);
  }));

  it('should create', () => {
    expect(notFoundGuard).toBeTruthy();
  });

  it('should navigate to dashboard ', () => {
    notFoundGuard.canActivate();

    expect(router.navigateByUrl).toHaveBeenCalledWith(ROUTES.DASHBOARD.url);
  });

  it('should open not found dialog', () => {
    notFoundGuard.canActivate();

    expect(dialogService.openNotFoundDialog).toHaveBeenCalled();
  });
});
