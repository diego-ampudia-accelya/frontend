import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';

@Injectable({
  providedIn: 'root'
})
export class NotFoundGuard {
  constructor(private router: Router, private dialogService: DialogService) {}
  public canActivate(): boolean {
    this.dialogService.openNotFoundDialog();
    this.router.navigateByUrl(ROUTES.DASHBOARD.url);

    return true;
  }
}
