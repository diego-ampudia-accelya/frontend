import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { isObservable } from 'rxjs';

import { AuthGuard } from './auth.guard';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { createIataUser } from '~app/shared/mocks/iata-user';

describe('AuthGuard', () => {
  const initialState = {
    auth: {
      user: createIataUser()
    }
  };
  let guard: AuthGuard;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, provideMockStore({ initialState })]
    });

    guard = TestBed.inject(AuthGuard);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('canActivate() should return true if there is a user', fakeAsync(() => {
    const canActivateResult = guard.canActivate(null, null);
    let result: boolean;

    if (isObservable(canActivateResult)) {
      canActivateResult.subscribe(res => (result = res));
    }

    expect(result).toBe(true);
  }));

  it('canActivate() should return false if there is no user', fakeAsync(() => {
    mockStore.overrideSelector(getUser, null);
    const canActivateResult = guard.canActivate(null, null);
    let result: boolean;

    if (isObservable(canActivateResult)) {
      canActivateResult.subscribe(res => (result = res));
    }

    expect(result).toBeFalsy();
  }));

  it('canActivateChild() should return true if there is a user', fakeAsync(() => {
    const canActivateChildResult = guard.canActivateChild(null, null);
    let result: boolean;

    if (isObservable(canActivateChildResult)) {
      canActivateChildResult.subscribe(res => (result = res));
    }

    expect(result).toBe(true);
  }));

  it('canActivateChild() should return false if there is no user', fakeAsync(() => {
    mockStore.overrideSelector(getUser, null);
    const canActivateChildResult = guard.canActivateChild(null, null);
    let result: boolean;

    if (isObservable(canActivateChildResult)) {
      canActivateChildResult.subscribe(res => (result = res));
    }

    expect(result).toBeFalsy();
  }));
});
