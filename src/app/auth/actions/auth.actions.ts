/* eslint-disable @typescript-eslint/no-shadow */
import { Action } from '@ngrx/store';

import { User } from '~app/shared/models/user.model';

export enum AuthActionTypes {
  LoginAction = '[Auth] Login',
  LoginFailedAction = '[Auth] Login Failed',
  SelectLanguage = '[Auth] Select Language',
  LogoutAction = '[Auth] Logout',
  NavigateToClassicBspLink = '[Auth] NavigateToClassicBspLink',
  NavigateToClassicBspLinkSwitchAccount = '[Auth] NavigateToClassicBspLinkSwitchAccount',
  NavigateToClassicBspLinkIataImpersonation = '[Auth] NavigateToClassicBspLinkIataImpersonation',
  NavigateToClassicBspLinkAgentGroupImpersonation = '[Auth] NavigateToClassicBspLinkAgentGroupImpersonation',
  RedirectToLoginAction = '[Auth] RedirectToLogin',
  RedirectToTrainingLogin = '[Auth] RedirectToTrainingLogin',
  UpdateLanguageSuccess = '[Auth] Update Language Success'
}

export class Login implements Action {
  readonly type = AuthActionTypes.LoginAction;

  constructor(public payload: { user: User; skipLocaleUpdate?: boolean }) {}
}

export class LoginFailed implements Action {
  readonly type = AuthActionTypes.LoginFailedAction;
}

export class SelectLanguage implements Action {
  readonly type = AuthActionTypes.SelectLanguage;

  constructor(public payload: { language: string }) {}
}
export class UpdateLanguageSuccess implements Action {
  readonly type = AuthActionTypes.UpdateLanguageSuccess;
}

export class Logout implements Action {
  readonly type = AuthActionTypes.LogoutAction;
}

export class NavigateToClassicBspLink implements Action {
  readonly type = AuthActionTypes.NavigateToClassicBspLink;
}

export class NavigateToClassicBspLinkSwitchAccount implements Action {
  readonly type = AuthActionTypes.NavigateToClassicBspLinkSwitchAccount;

  constructor(public payload: { accountId: string }) {}
}

export class NavigateToClassicBspLinkIataImpersonation implements Action {
  readonly type = AuthActionTypes.NavigateToClassicBspLinkIataImpersonation;

  constructor(public payload: { userType: number; userCode: string }) {}
}

export class NavigateToClassicBspLinkAgentGroupImpersonation implements Action {
  readonly type = AuthActionTypes.NavigateToClassicBspLinkAgentGroupImpersonation;

  constructor(public payload: { agentGroupMemberCode: string; returnToAgentGroupProfile: boolean }) {}
}

export class RedirectToLogin implements Action {
  readonly type = AuthActionTypes.RedirectToLoginAction;
}

export class RedirectToTrainingLogin implements Action {
  readonly type = AuthActionTypes.RedirectToTrainingLogin;
}

export type AuthActions =
  | Login
  | LoginFailed
  | Logout
  | NavigateToClassicBspLink
  | RedirectToLogin
  | RedirectToTrainingLogin
  | SelectLanguage
  | UpdateLanguageSuccess;
