import { AuthActions, AuthActionTypes } from '~app/auth/actions/auth.actions';
import { User } from '~app/shared/models/user.model';

export interface AuthState {
  user: User;
}

export const initialState: AuthState = {
  user: undefined
};

export function reducer(state = initialState, action: AuthActions): AuthState {
  if (action.type === AuthActionTypes.LoginAction) {
    return {
      ...state,
      user: action.payload.user
    };
  }

  if (action.type === AuthActionTypes.LoginFailedAction) {
    return {
      ...state,
      user: null
    };
  }

  if (action.type === AuthActionTypes.RedirectToTrainingLogin) {
    return {
      ...state,
      user: undefined
    };
  }

  if (action.type === AuthActionTypes.SelectLanguage) {
    return {
      ...state,
      user: {
        ...state.user,
        language: action.payload.language
      }
    };
  }

  return state;
}
