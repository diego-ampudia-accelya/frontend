import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, iif, Observable, of } from 'rxjs';
import { map, switchMap, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

import { AuthActions } from '../actions';
import { getSelectedLanguage, getUserId } from '../selectors/auth.selectors';
import { TokenService } from '../services/token.service';
import {
  AuthActionTypes,
  Login,
  LoginFailed,
  Logout,
  NavigateToClassicBspLink,
  NavigateToClassicBspLinkAgentGroupImpersonation,
  NavigateToClassicBspLinkIataImpersonation,
  NavigateToClassicBspLinkSwitchAccount,
  RedirectToLogin,
  RedirectToTrainingLogin,
  SelectLanguage,
  UpdateLanguageSuccess
} from '~app/auth/actions/auth.actions';
import { AuthService } from '~app/auth/services/auth.service';
import { PermissionsGuard } from '~app/auth/services/permissions.guard';
import { ClassicBsplinkService } from '~app/core/services/classic-bsplink.service';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import { Token } from '~app/shared/models/token.model';
import { NotificationService, UsersProfileManagementService } from '~app/shared/services';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { SessionTimeService } from '~app/training/services/session-time.service';

@Injectable()
export class AuthEffects {
  private readonly DEFAULT_LANGUAGE: string = 'en';

  @Effect({ dispatch: false })
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.LoginAction),
    withLatestFrom(this.store.pipe(select(getSelectedLanguage))),
    tap(([{ payload }, language]) => {
      if (!payload.skipLocaleUpdate) {
        const locale = this.translationService.getLocale();

        this.translationService.onError().subscribe(error => {
          if (error) {
            this.translationService.setLocale({ ...locale, language: this.DEFAULT_LANGUAGE });
            this.notificationService.showError('headerUserInfo.updatedLanguageError');
          } else {
            this.translationService.setLocale({ ...locale, language });
          }
        });
      }

      if (appConfiguration && appConfiguration.isTrainingSite) {
        this.sessionTimeService.start();
      }

      this.permissionsGuard.checkActiveTabAccess();
    })
  );

  @Effect({ dispatch: false })
  loginFailed$ = this.actions$.pipe(
    ofType<LoginFailed>(AuthActionTypes.LoginFailedAction),
    tap(() => this.router.navigate([ROUTES.ERROR.url])),
    // Once a Logout is dispatched ignore all further LoginFailed actions
    takeUntil(this.actions$.pipe(ofType(AuthActionTypes.LogoutAction)))
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    // The store is reset to its initial state by one of the meta reducers.
    // Information in the store is not available at this point.
    ofType<Logout>(AuthActionTypes.LogoutAction),
    tap(() => {
      if (appConfiguration.disableLogoutRedirect) {
        return;
      }

      this.authService.redirectToLogoutPage();
    })
  );

  @Effect({ dispatch: false })
  navigateToClassicBspLink$ = this.actions$.pipe(
    // The store is reset to its initial state by one of the meta reducers.
    // Information in the store is not available at this point.
    ofType<NavigateToClassicBspLink>(AuthActionTypes.NavigateToClassicBspLink),
    switchMap(() => this.getToken()),
    tap(token => {
      this.classicBspLinkService.goToClassicBspLink(token);
    })
  );

  @Effect({ dispatch: false })
  navigateToClassicBspLinkSwitchAccount$ = this.actions$.pipe(
    // The store is reset to its initial state by one of the meta reducers.
    // Information in the store is not available at this point.
    ofType<NavigateToClassicBspLinkSwitchAccount>(AuthActionTypes.NavigateToClassicBspLinkSwitchAccount),
    switchMap(({ payload }) => combineLatest([of(payload), this.getToken()])),
    tap(([payload, token]) => {
      if (token) {
        this.classicBspLinkService.goToClassicBspLinkSwitchAccountPage(payload.accountId, token);
      } else {
        this.store.dispatch(new Logout());
      }
    })
  );

  @Effect({ dispatch: false })
  navigateToClassicBspLinkIataImpersonation$ = this.actions$.pipe(
    // The store is reset to its initial state by one of the meta reducers.
    // Information in the store is not available at this point.
    ofType<NavigateToClassicBspLinkIataImpersonation>(AuthActionTypes.NavigateToClassicBspLinkIataImpersonation),
    switchMap(({ payload }) => combineLatest([of(payload), this.getToken()])),
    tap(([payload, token]) => {
      if (token) {
        this.classicBspLinkService.goToClassicBspLinkIataImpersonation(payload.userType, payload.userCode, token);
        this.displayIataImpersonationSuccessMessage(payload);
      } else {
        this.store.dispatch(new Logout());
      }
    })
  );

  @Effect({ dispatch: false })
  navigateToClassicBspLinkAgentGroupImpersonation$ = this.actions$.pipe(
    // The store is reset to its initial state by one of the meta reducers.
    // Information in the store is not available at this point.
    ofType<NavigateToClassicBspLinkAgentGroupImpersonation>(
      AuthActionTypes.NavigateToClassicBspLinkAgentGroupImpersonation
    ),
    switchMap(({ payload }) => combineLatest([of(payload), this.getToken()])),
    tap(([payload, token]) => {
      if (token) {
        this.classicBspLinkService.goToClassicBspLinkAgentGroupImpersonation(
          payload.agentGroupMemberCode,
          payload.returnToAgentGroupProfile,
          token
        );
        this.displayAgentGroupImpersonationSuccessMessage(payload);
      } else {
        this.store.dispatch(new Logout());
      }
    })
  );

  @Effect({ dispatch: false })
  redirectToLogin$ = this.actions$.pipe(
    ofType<RedirectToLogin>(AuthActionTypes.RedirectToLoginAction),
    tap(() => this.authService.redirectToLogin())
  );

  @Effect({ dispatch: false })
  redirectToTrainingLogin$ = this.actions$.pipe(
    ofType<RedirectToTrainingLogin>(AuthActionTypes.RedirectToTrainingLogin),
    tap(() => {
      if (!location.pathname.endsWith(ROUTES.TRAINING_LOGIN.url)) {
        this.authService.redirectToTrainingLogin();
      }
    })
  );

  @Effect()
  setUserLanguage$ = this.actions$.pipe(
    ofType<SelectLanguage>(AuthActionTypes.SelectLanguage),
    switchMap(() => this.store.pipe(select(getUserId))),
    withLatestFrom(this.store.pipe(select(getSelectedLanguage))),
    switchMap(([id, selectedLanguage]) =>
      iif(() => id !== null, this.userProfileManagementService.setUserLanguage(id, selectedLanguage))
    ),
    map(() => new AuthActions.UpdateLanguageSuccess())
  );

  @Effect({ dispatch: false })
  updateLanguageSuccess$ = this.actions$.pipe(
    ofType<UpdateLanguageSuccess>(AuthActionTypes.UpdateLanguageSuccess),
    tap(() => {
      this.notificationService.showSuccess('headerUserInfo.updatedLanguageSuccessfully');
    })
  );

  constructor(
    private authService: AuthService,
    private actions$: Actions,
    private tokenService: TokenService,
    private sessionTimeService: SessionTimeService,
    private permissionsGuard: PermissionsGuard,
    private store: Store<AppState>,
    private translationService: L10nTranslationService,
    private userProfileManagementService: UsersProfileManagementService,
    private notificationService: NotificationService,
    private router: Router,
    private classicBspLinkService: ClassicBsplinkService
  ) {}

  private getToken(): Observable<Token> {
    return this.tokenService.getRedirectToken();
  }

  private getUserType(userTypeNumber: number): string {
    let userType = '';
    switch (userTypeNumber) {
      case 0:
        userType = this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.airline');
        break;
      case 1:
        userType = this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.agent');
        break;
      case 2:
        userType = this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.iata');
        break;
      case 3:
        userType = this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.dpc');
        break;
      case 5:
        userType = this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.gds');
        break;
      case 7:
        userType = this.translationService.translate(
          'headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.thirdParty'
        );
        break;
      case 8:
        userType = this.translationService.translate(
          'headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.agentGroupPrimaryAccount'
        );
        break;
      case 20:
        userType = this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.homu');
        break;
    }

    return userType;
  }

  private displayIataImpersonationSuccessMessage(payload: any): void {
    let successMessage = '';
    // For user types IATA (2) and DPC(3) dont need to display user code
    if (payload.userType !== 2 && payload.userType !== 3) {
      successMessage = this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.successMessage', {
        userType: this.getUserType(payload.userType).toUpperCase(),
        userCode: payload.userCode
      });
    } else {
      successMessage = this.translationService.translate(
        'headerUserInfo.loginAsAnyUserDialog.successMessageWithoutUserCode',
        {
          userType: this.getUserType(payload.userType).toUpperCase()
        }
      );
    }
    this.notificationService.showSuccess(successMessage);
  }

  private displayAgentGroupImpersonationSuccessMessage(payload: any): void {
    let successMessage: string;

    if (payload.returnToAgentGroupProfile) {
      successMessage = this.translationService.translate(
        'headerUserInfo.returnToAgentGroupProfileDialog.successMessage'
      );
    } else {
      successMessage = this.translationService.translate('headerUserInfo.loginAsAnyGroupMemberDialog.successMessage', {
        agentCode: payload.agentGroupMemberCode
      });
    }

    this.notificationService.showSuccess(successMessage);
  }
}
