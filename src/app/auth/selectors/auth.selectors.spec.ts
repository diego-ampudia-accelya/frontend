import * as selectors from './auth.selectors';
import { Permissions } from '~app/shared/constants';
import { createAgentUser } from '~app/shared/mocks/agent-user';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { Bsp } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';

describe('auth selectors', () => {
  const user: User = createIataUser();
  const state: selectors.State = {
    router: null,
    auth: { user }
  };

  describe('getUser', () => {
    it('should return null if `auth` does not exist', () => {
      expect(selectors.getUser({ ...state, auth: null })).toBeNull();
    });

    it('should return user if `auth` exists', () => {
      expect(selectors.getUser(state)).toEqual(user);
    });
  });

  describe('getUserId', () => {
    it('should return null if `user` does not exist', () => {
      expect(selectors.getUserId({ ...state, auth: { user: null } })).toBeNull();
    });

    it('should return user id if `user` exists', () => {
      expect(selectors.getUserId(state)).toEqual(10000);
    });
  });

  describe('getSelectedLanguage', () => {
    it('should return default language if `user` does not exist', () => {
      expect(selectors.getSelectedLanguage({ ...state, auth: { user: null } })).toEqual('en');
    });

    it('should return selected language if `user` exists', () => {
      expect(selectors.getSelectedLanguage(state)).toEqual('es');
    });
  });

  describe('getUserType', () => {
    it('should return null if `user` does not exist', () => {
      expect(selectors.getUserType({ ...state, auth: { user: null } })).toBeNull();
    });

    it('should return user type if `user` exists', () => {
      expect(selectors.getUserType(state)).toEqual(UserType.IATA);
    });
  });

  describe('getUserEmail', () => {
    it('should return null if `user` does not exist', () => {
      expect(selectors.getUserEmail({ ...state, auth: { user: null } })).toBeNull();
    });

    it('should return user email if `user` exists', () => {
      expect(selectors.getUserEmail(state)).toEqual('iata-user-nfe@accelya.com');
    });
  });

  describe('isAirlineUser', () => {
    it('should return false if `user` does not exist', () => {
      expect(selectors.isAirlineUser({ ...state, auth: { user: null } })).toBe(false);
    });

    it('should return false if user is not an AIRLINE', () => {
      expect(selectors.isAirlineUser(state)).toBe(false);
    });
  });

  describe('isAgentUser', () => {
    it('should return false if `user` does not exist', () => {
      expect(selectors.isAgentUser({ ...state, auth: { user: null } })).toBe(false);
    });

    it('should return false if user is not an AGENT', () => {
      expect(selectors.isAgentUser(state)).toBe(false);
    });
  });

  describe('isLoggedIn', () => {
    it('should return false if `user` does not exist', () => {
      expect(selectors.isLoggedIn({ ...state, auth: { user: null } })).toBe(false);
    });

    it('should return true (logged) if `user` exists', () => {
      expect(selectors.isLoggedIn(state)).toBe(true);
    });
  });

  describe('getPermissions', () => {
    const permissions = [Permissions.archiveFile];
    const bspPermissions = [{ bspId: 1, permissions }];

    it('should return empty permissions if `user` does not exist', () => {
      expect(selectors.getPermissions({ ...state, auth: { user: null } })).toEqual({
        permissions: [],
        bspPermissions: []
      });
    });

    it('should return empty permissions if they do not exists on `user`', () => {
      // `bspPermissions` property does not exists on `user`, so function should return an empty array
      expect(selectors.getPermissions({ ...state, auth: { user: { ...user, permissions } } })).toEqual({
        permissions,
        bspPermissions: []
      });
    });

    it('should get permissions and bsp permissions if `user` exists', () => {
      expect(selectors.getPermissions({ ...state, auth: { user: { ...user, permissions, bspPermissions } } })).toEqual({
        permissions,
        bspPermissions
      });
    });
  });

  describe('getUserBsps', () => {
    it('should return empty array when user is null', () => {
      expect(selectors.getUserBsps({ ...state, auth: { user: null } })).toEqual([]);
    });

    it('should return unique BSPs even if they are duplicated', () => {
      const duplicatedBsp: Bsp = {
        id: 10001,
        isoCountryCode: 'GR2',
        name: 'Greece',
        effectiveFrom: '2019-06-01'
      };

      expect(
        selectors.getUserBsps({ ...state, auth: { user: { ...user, bsps: [...user.bsps, duplicatedBsp] } } })
      ).not.toContain(duplicatedBsp);
    });

    it('should return sortered BSPs, in alphabetical order', () => {
      expect(selectors.getUserBsps(state).length).toBe(3);
      expect(selectors.getUserBsps(state)[0].name).toBe('Bahamas');
    });
  });

  describe('getUserDefaultBsp', () => {
    it('should return default BSP if it exists', () => {
      expect(selectors.getUserDefaultBsp({ ...state, auth: { user: { ...user, defaultIsoc: 'ES' } } })).toBeUndefined();
      expect(selectors.getUserDefaultBsp({ ...state, auth: { user: { ...user, defaultIsoc: 'GR' } } })).toEqual({
        id: 10006,
        isoCountryCode: 'GR',
        name: 'Greece',
        effectiveFrom: '2019-05-10',
        effectiveTo: '2019-05-31',
        version: 3
      });
    });
  });

  describe('getAllBspNamesJoined', () => {
    it('should join all BSP names', () => {
      expect(selectors.getAllBspNamesJoined(state)).toEqual('Bahamas,Bulgaria,Greece');
    });
  });

  describe('getGlobalAirlineForUser', () => {
    it('should return null if user is not an AIRLINE', () => {
      expect(selectors.getGlobalAirlineForUser(state)).toBeNull();
    });

    it('should return global airline if user is an AIRLINE', () => {
      expect(selectors.getGlobalAirlineForUser({ ...state, auth: { user: createAirlineUser() } })).toBeDefined();
    });
  });

  describe('getAgentForUser', () => {
    it('should return null if user is not an AGENT', () => {
      expect(selectors.getAgentForUser(state)).toBeNull();
    });

    it('should return agent if user is an AGENT', () => {
      expect(selectors.getAgentForUser({ ...state, auth: { user: createAgentUser() } })).toBeDefined();
    });
  });

  describe('getLocalAirline', () => {
    const bsp: Bsp = {
      id: 1,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01'
    };

    it('should return local airline if user global airline is available', () => {
      expect(selectors.getLocalAirline(bsp)({ ...state, auth: { user: createAirlineUser() } })).toBeDefined();
    });

    it('should return falsy value if user global airline is NOT available', () => {
      expect(selectors.getLocalAirline(bsp)(state)).toBeFalsy();
    });
  });
});
