import { createFeatureSelector, createSelector } from '@ngrx/store';
import { chain } from 'lodash';

import { AuthState } from '../reducers/auth.reducer';
import { AppState } from '~app/reducers';
import { Bsp } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';

const defaultLanguage = 'en';

const extractBsp = (user: User) => (user && user.bsps) || [];

export interface State extends AppState {
  auth: AuthState;
}

export const selectAuthState = createFeatureSelector<State, AuthState>('auth');

export const getUser = createSelector(selectAuthState, auth => (auth ? auth.user : null));

export const getUserId = createSelector(getUser, user => (user ? user.id : null));

export const getSelectedLanguage = createSelector(getUser, user => (user && user.language) || defaultLanguage);

export const getUserType = createSelector(getUser, user => (user ? user.userType : null));

export const getUserEmail = createSelector(getUser, user => (user ? user.email : null));

export const isAirlineUser = createSelector(getUserType, userType => userType === UserType.AIRLINE);

export const isAgentUser = createSelector(getUserType, userType => userType === UserType.AGENT);

export const isLoggedIn = createSelector(getUser, user => !!user);

export const getPermissions = createSelector(getUser, user => ({
  permissions: user?.permissions || [],
  bspPermissions: user?.bspPermissions || []
}));

/**
 * Creates a selector that checks if the current user has the specified permission or not.
 *
 * @param permission Permission to check
 * @returns `true` if the user has the permission and `false` otherwise
 */
export const hasPermission = (permission: string) =>
  createSelector(getPermissions, ({ permissions }) => permissions.includes(permission));

/**
 * Extracts a list of BSPs from the current user, in alphabetical order, based on groupMembership.
 *
 * @param user Current stored user
 * @returns A list of BSPs of the user when specified, and an empty list otherwise.
 */
export const getUserBsps = createSelector(getUser, user => {
  const bspNameKey: keyof Bsp = 'name';

  return chain(extractBsp(user)).uniqBy(bspNameKey).orderBy(bspNameKey).value();
});

export const getUserDefaultBsp = createSelector(getUser, getUserBsps, (user, bsps) =>
  bsps.find(bsp => bsp.isoCountryCode === user.defaultIsoc)
);

export const getUserBspId = createSelector(getUserDefaultBsp, bsp => bsp.id);

export const getAllBspNamesJoined = createSelector(getUserBsps, bsps => bsps.map(bsp => bsp.name).join(','));

export const getGlobalAirlineForUser = createSelector(getUser, user =>
  user.userType === UserType.AIRLINE ? user.globalAirline : null
);

// TODO: refactor, it is returning Airline and it is typed as a AirlineSummary
export const getUserAirlineByBsp = createSelector(getUser, (user, props) => {
  const bspIds: number[] = props.bsps;
  let airlines = [];
  if (user.userType === UserType.AIRLINE) {
    airlines = user.globalAirline.airlines.filter(airline => bspIds.includes(airline.bsp.id));
  }

  return airlines;
});

export const getAgentForUser = createSelector(getUser, user => (user.userType === UserType.AGENT ? user.agent : null));

export const getUserIataDetails = createSelector(getUser, user => {
  if (user) {
    if (user.userType === UserType.AGENT) {
      return {
        iataCode: user.iataCode,
        organizationName: user.organization
      };
    } else if (user.userType === UserType.AIRLINE) {
      return {
        iataCode: user.globalAirline.iataCode,
        organizationName: user.globalAirline.globalName
      };
    }

    return { iataCode: user.iataCode, organizationName: 'IATA' };
  } else {
    return null;
  }
});

export const getLocalAirline = (bsp: Bsp) =>
  createSelector(
    getGlobalAirlineForUser,
    globalAirline =>
      // Returning BSP related local airline
      globalAirline && globalAirline.airlines.find(airline => airline.bsp.isoCountryCode === bsp.isoCountryCode)
  );
