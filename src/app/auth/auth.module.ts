import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { LOCATION_TOKEN } from './services/injection-tokens';
import { AuthEffects } from '~app/auth/effects/auth.effects';
import * as fromAuth from '~app/auth/reducers/auth.reducer';

@NgModule({
  declarations: [],
  imports: [CommonModule, StoreModule.forFeature('auth', fromAuth.reducer), EffectsModule.forFeature([AuthEffects])],
  providers: [{ provide: LOCATION_TOKEN, useValue: window.location }]
})
export class AuthModule {}
