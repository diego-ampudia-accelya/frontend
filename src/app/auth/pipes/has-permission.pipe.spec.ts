import { BehaviorSubject } from 'rxjs';

import { HasPermissionPipe } from './has-permission.pipe';
import { Permissions } from '~app/shared/constants/permissions';

const permissionsServiceSpy = jasmine.createSpyObj('PermissionsService', ['hasPermission', 'hasUserType']);
permissionsServiceSpy.hasPermission.and.returnValue(false);
permissionsServiceSpy['permissions$'] = new BehaviorSubject<string[]>(['rAgn', 'rAirl']);
permissionsServiceSpy['permissions'] = ['rAgn', 'rAirl'];
permissionsServiceSpy['bspPermissionsMap'] = {
  1001: ['rAgn', 'rAirl']
};

describe('PermissionsPipe', () => {
  it('create an instance', () => {
    const pipe = new HasPermissionPipe(permissionsServiceSpy);
    expect(pipe).toBeTruthy();
  });

  it('transform should call PermissionsService hasPermission with permssion', () => {
    const pipe = new HasPermissionPipe(permissionsServiceSpy);
    pipe.transform(Permissions.readAgent);

    expect(permissionsServiceSpy.hasPermission).toHaveBeenCalledWith('rAgn', undefined);
  });

  it('transform should call PermissionsService hasPermission with permssion and bspId', () => {
    const pipe = new HasPermissionPipe(permissionsServiceSpy);
    pipe.transform(Permissions.readAgent, 10001);

    expect(permissionsServiceSpy.hasPermission).toHaveBeenCalledWith('rAgn', 10001);
  });

  it('transform should call PermissionsService hasPermission with permssion and bspId', () => {
    const pipe = new HasPermissionPipe(permissionsServiceSpy);
    pipe.transform([Permissions.readAgent, Permissions.readAirline], 10001);

    expect(permissionsServiceSpy.hasPermission).toHaveBeenCalledWith(
      [Permissions.readAgent, Permissions.readAirline],
      10001
    );
  });
});
