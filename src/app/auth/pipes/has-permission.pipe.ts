import { Pipe, PipeTransform } from '@angular/core';

import { PermissionsService } from '~app/auth/services/permissions.service';

@Pipe({
  name: 'hasPermission'
})
export class HasPermissionPipe implements PipeTransform {
  constructor(private permissionsService: PermissionsService) {}

  transform(permission: string | string[], bspId?: number): boolean {
    return this.permissionsService.hasPermission(permission, bspId);
  }
}
