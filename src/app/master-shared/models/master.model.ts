/* eslint-disable @typescript-eslint/naming-convention */
import { ActivatedRouteSnapshot } from '@angular/router';
import { TableColumn } from '@swimlane/ngx-datatable';

import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { GridTableComponent } from '~app/shared/components/grid-table/grid-table.component';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

// TODO: Can we set the appropriate types?
export interface MasterActionClick {
  event: any;
  row: any;
  action: { actionType: GridTableActionType }; // TODO Refactor propagation of action
}

// TODO: Enums should be PascalCase.
export enum MasterDataType {
  // Master Data
  CURRENCY = 'Currency',
  BSPCurrencyAssignation = 'BSPCurrencyAssignation',
  AIRLINE_TA = 'AIRLINE_TA',
  AGENT_TA = 'AGENT_TA',
  // ACDM
  Adm = 'adm',
  Acm = 'acm',
  Adnt = 'adnt',
  Acnt = 'acnt',
  Admq = 'adm-request', // TODO: Review ACDM Requests types (http://gitlab.nfedev.accelya.com/nfe/frontend/merge_requests/1723)
  Acmq = 'acm-request',
  // BSP Adjustment
  Spdr = 'spdr',
  Spcr = 'spcr',
  Admd = 'admd',
  Acmd = 'acmd',
  // Refund
  RefundApp = 'refund-application',
  RefundNotice = 'refund-notice',
  // User
  GDS = 'GDS',
  BSP = 'BSP',
  Agent = 'Agent',
  Airline = 'Airline',
  // Sales
  BillingStatement = 'BillingStatement',
  BillingAnalysis = 'BillingAnalysis',
  // TIP
  TipCard = 'tip-card',
  TipCardConsent = 'tip-card-consent',
  TipProduct = 'tip-product',
  TipProductConsent = 'tip-product-consent'
}

// This interface will represent the data that goes into Dialogs
export interface MasterDialogData {
  title: string;
  question?: string;
  footerButtonsType: FooterButton | FooterButton[];
  gridTable: GridTableComponent;
  rowTableContent: any;
  rowSourceTableContent: any;
  actionType: GridTableActionType;
  dataType: MasterDataType;
  activatedRoute?: ActivatedRouteSnapshot;
}

// Model for data sent in CRUD operations in MasterDialogForm
export interface MasterDataForRequest {
  id?: string | number;
  [key: string]: any;
}

export interface MasterConfig {
  // Actions are indexed by enum GridTableActionType and they can be routes or any (entryComponents)
  actions: { [key in GridTableActionType]?: string | any };
  tableConfig?: MasterTableConfig;
}

export interface MasterTableConfig {
  get: { readonly [key in MasterDataType]?: MasterTableConfigItem };
}

export interface MasterTableConfigItem {
  COLUMNS: Array<TableColumn>;
  ACTIONS: Array<string>;
  PRIMARY_ACTIONS: Array<unknown>;
  TITLE?: string;
}
