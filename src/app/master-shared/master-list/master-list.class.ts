import { Directive, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';

import {
  MasterActionClick,
  MasterConfig,
  MasterDataType,
  MasterDialogData,
  MasterTableConfigItem
} from '~app/master-shared/models/master.model';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { GridTableComponent } from '~app/shared/components/grid-table/grid-table.component';
import { MasterConfigHelper } from '~app/shared/config/master-config-helper.class';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class MasterList implements OnInit {
  @Input() masterDataType: MasterDataType;
  @Input() currentItem: any;

  public btnDesign = ButtonDesign; // Enum used in templates to give style
  // TODO parent class for dialog forms (not any) and change type to extends ?
  protected listConfig: MasterConfig;
  protected abstract gridTable: GridTableComponent;

  // Table properties
  public columns: Array<TableColumn>;
  public actions: Array<string>;
  public primaryActions: Array<unknown>;
  public title: string;

  constructor(
    protected dialogService: DialogService,
    protected translationService: L10nTranslationService,
    protected route?: ActivatedRoute,
    protected router?: Router
  ) {}

  public ngOnInit() {
    this.setDataType(); // Lists need to have set their MasterDataType (so far: used for retrieving the correct translations)
    this.setListConfig(); // Lists need to have set their MasterConfig (answers what to do when clicking buttons)
  }

  private setDataType() {
    // If type was not inferred in constructor we try to find it on routes
    // If not found anyway, we throw exception
    // Type inferred in constructor has priority over data set in route
    if (!this.masterDataType && this.route && this.route.snapshot.data.dataType) {
      this.masterDataType = this.route.snapshot.data.dataType;
    } else {
      if (!this.masterDataType) {
        throw new Error('No type was inferred');
      }
    }
  }

  // Decides what dialog to open when clicking an action (MasterActionClick)
  public onActionClick(actionContent: MasterActionClick): Observable<any> {
    const nextAction = this.listConfig.actions[actionContent.action.actionType];

    if (!nextAction) {
      throw new Error('No action was defined in master-config');
    }

    if (typeof nextAction === 'string') {
      // nextAction is actually a Route
      if (!this.router) {
        throw new Error('Optional service Router is needed and was not injected');
      }

      this.navigateTo(nextAction, actionContent);
    } else {
      // nextAction is actually a component that will be injected in a dialog
      return this.openDialog(nextAction, actionContent);
    }

    return null;
  }

  private setListConfig() {
    this.listConfig = MasterConfigHelper.getConfig(this.masterDataType);
    this.setTableProperties();
  }

  private setTableProperties() {
    if (!this.listConfig.tableConfig) {
      // TODO Eventually exceptions should be enabled to make configs consistent
      // throw new Error('Config file with constants not declared in master-config-helper');
    } else {
      const tableConfig: MasterTableConfigItem = this.listConfig.tableConfig.get[this.masterDataType];

      this.columns = tableConfig.COLUMNS;
      this.columns.forEach(column => {
        column.name = this.translationService.translate(column.name);
      });

      this.primaryActions = tableConfig.PRIMARY_ACTIONS;

      this.actions = tableConfig.ACTIONS;

      if (tableConfig.TITLE) {
        this.title = this.translationService.translate(tableConfig.TITLE);
      }
    }
  }

  private navigateTo(url: string, actionContent: MasterActionClick) {
    const actionType: GridTableActionType = actionContent.action.actionType;

    switch (actionType) {
      case GridTableActionType.Add:
        this.router.navigate([url]);
        break;
      case GridTableActionType.Edit:
      case GridTableActionType.Details:
      case GridTableActionType.Investigate:
      case GridTableActionType.Exception:
      case GridTableActionType.Authorize:
      case GridTableActionType.ManageTA:
        this.router.navigate([url, actionContent.row.id]);
        break;
      default:
        throw new Error('Navigation behavior for the action type was not defined');
    }
  }

  private openDialog(dialogFormComponent: any, actionContent: MasterActionClick): Observable<any> {
    let footerButtonsType: FooterButton;
    const dataType: MasterDataType = this.masterDataType;
    const actionType: GridTableActionType = actionContent.action.actionType;
    const resourceType = this.translationService.translate(`MENU.MASTER_DATA.${dataType}.LBL`.toUpperCase());
    const activatedRoute = this.route ? this.route.snapshot : null;
    const title = this.translationService.translate(`LIST.MASTER_DATA.DIALOG.TITLE.${actionType.toUpperCase()}`, {
      resourceType
    });

    let question: string;

    // In this switch we build the buttons that we are gonna have in dialogs
    switch (actionType) {
      case GridTableActionType.Add:
      case GridTableActionType.CreateChildGroup:
      case GridTableActionType.CreateParentGroup:
        footerButtonsType = FooterButton.Create;
        break;
      case GridTableActionType.Edit:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Update;
        break;
      case GridTableActionType.Remove:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Remove;
        break;
      case GridTableActionType.Delete:
        actionContent.event.stopPropagation();
        question = this.translationService.translate(`LIST.MASTER_DATA.DIALOG.QUESTION.${actionType.toUpperCase()}`, {
          resourceType
        });
        footerButtonsType = FooterButton.Delete;
        break;
      case GridTableActionType.DisputeACDM:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Dispute;
        break;
      case GridTableActionType.ApproveACDM:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Approve;
        break;
      case GridTableActionType.Reject:
      case GridTableActionType.RejectRequest:
      case GridTableActionType.RejectACDM:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Reject;
        break;
      case GridTableActionType.DeactivateACDM:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Deactivate;
        break;
      case GridTableActionType.ReactivateACDM:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Reactivate;
        break;
      case GridTableActionType.ForwardGdsADM:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.ForwardGds;
        break;
      case GridTableActionType.AuthorizeRequest:
        actionContent.event.stopPropagation();
        footerButtonsType = FooterButton.Authorize;
        break;
      default:
        throw new Error('Action type was not found');
    }

    // Final object passed to Dialog to be configured
    const data: MasterDialogData = {
      gridTable: this.gridTable, // Our table. This is used basically for refreshing it when data is added/removed/edited
      rowTableContent: actionContent.row || {}, // Our row. We get the object data from here (object id for ex)
      rowSourceTableContent: this.currentItem,
      title, // Dialog title
      question, // Dialog question
      footerButtonsType, // Buttons that the dialog shows
      actionType,
      dataType,
      activatedRoute // Snapshot of the route service
    };

    // We open the correct dialogFormComponent (got from listConfig: MasterConfig) with the previous built data
    return this.dialogService.open(
      dialogFormComponent,
      { data } // TODO Refactor DialogConfig
    );
  }
}
