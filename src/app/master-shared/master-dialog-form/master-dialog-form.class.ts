import { Directive, OnDestroy, QueryList } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { ErrorMessageService } from '~app/master-data/services/error-message.service';
import { MasterDataForRequest, MasterDialogData } from '~app/master-shared/models/master.model';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { InputComponent } from '~app/shared/components/input/input.component';
import { FormUtil } from '~app/shared/helpers';
import { ApiService } from '~app/shared/services/api.service';
import { NotificationService } from '~app/shared/services/notification.service';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class MasterDialogForm implements OnDestroy {
  protected subscriptions: Subscription[] = [];
  protected dialogInjectedData: MasterDialogData;
  public abstract form: FormGroup;
  protected abstract formConstants;
  protected showSuccessNotification = true;

  constructor(
    config: any, // Config data (not modeled yet) used in dialogs. Inside constructor we do casting to our generic model MasterDialogData
    protected dataService: ApiService<any>, // Service used for CRUD operations with our dataType
    protected reactiveSubject: ReactiveSubject,
    protected dialogService: DialogService,
    protected notificationService: NotificationService,
    protected errorMessageService: ErrorMessageService,
    protected translationService: L10nTranslationService
  ) {
    this.dialogInjectedData = config.data as MasterDialogData;
    this.prepareDynamicEndpoint();
  }

  // This methods listens any click (not Cancel clicks) in dialog buttons
  // Depending on type of button clicked, we perform different CRUD operations (create and edit on one hand, and delete on the other hand)
  setDialogClickCallback() {
    this.subscriptions.push(
      this.reactiveSubject.asObservable
        .pipe(filter(action => action.clickedBtn !== FooterButton.Cancel))
        .subscribe(action => {
          const dataForRequest = this.generateDataForRequestByAction(action); // We get the data ready to be sent on our request

          switch (action.clickedBtn) {
            case FooterButton.Create:
            case FooterButton.Update:
            case FooterButton.Save:
              if (this.form.invalid) {
                // Todo: Strange UX - Validation errors are displayed in a notification message under the dark modal overlay
                this.errorMessageService.processFormErrors(this.form, this.formConstants.ADD_EDIT_ERROR_COLUMN_MAP);

                FormUtil.showControlState(this.form);
              } else {
                this.validateInputsOnClick(this);
                this.saveData(dataForRequest, action);
              }
              break;

            case FooterButton.Remove:
              this.saveData(dataForRequest, action);
              break;
            case FooterButton.Delete:
              this.dataService
                .delete(dataForRequest.id as number) // Casting to number because in this CRUD method, the id parameter is number and not string
                .subscribe(
                  result => this.successCallback(action.clickedBtn, result),
                  () => this.errorCallback(null, action.clickedBtn)
                );
              break;
            default:
              throw new Error('Action type was not found');
          }
        })
    );
  }

  openConfirmationDialog(confirmationData, itemData) {
    this.dialogService
      .open(confirmationData.component, {
        data: confirmationData.data,
        itemData
      } as DialogConfig)
      .subscribe(event => {
        const action = event.clickedBtn;
        const actionType = action.type ? action.type : action;
        if (actionType === FooterButton.Done || FooterButton.Cancel) {
          event.contentComponentRef.closeDialog();
        }
      });
  }

  // This abstract method (which needs to be implemented on every child class) is the one that builds the object ready to be sent to backend.
  protected abstract generateDataForRequestByAction(action?: any): MasterDataForRequest;

  //* Since this class is extended by Entry Components, they do not have info in route service so we should
  //* provide it in case the endpoint is dynamic
  private prepareDynamicEndpoint() {
    const dataService: any = this.dataService;

    if (typeof dataService.setDynamicTokens === 'function' && this.dialogInjectedData.activatedRoute) {
      dataService.setDynamicTokens(this.dialogInjectedData.activatedRoute.params);
    }
  }

  private validateInputsOnClick(componentRef) {
    const bsplInputs: QueryList<InputComponent> = componentRef.bsplInputs || [];
    bsplInputs.forEach(input => {
      input.showState();
    });
  }

  // This is the logic that takes place every time we success on a CRUD operation
  protected successCallback(action: FooterButton, result?: any) {
    if (this.dialogInjectedData.gridTable) {
      this.dialogInjectedData.gridTable.getData();
    }

    this.dialogService.close();

    if (this.showSuccessNotification) {
      // TODO move this logic outside master-dialog-form
      if (action === 'remove' && result && result.user && result.user.membershipCount === 0) {
        this.notificationService.showSuccess(this.getNotificationMessage(action, false, true));
      } else {
        this.notificationService.showSuccess(this.getNotificationMessage(action));
      }
    }
  }

  // This is the logic that takes place every time we fail on a CRUD operation
  protected errorCallback(errors?, action?: FooterButton) {
    if (errors) {
      this.errorMessageService.processResponseErrors(errors, this.formConstants.ADD_EDIT_ERROR_COLUMN_MAP);
    } else {
      this.notificationService.showError(this.getNotificationMessage(action, true));
    }
  }

  // Generic method to retrieve notifications depending on action and errors
  private getNotificationMessage(action: FooterButton, isError = false, warningMessage = false) {
    const isWarning = !warningMessage ? 'OK' : 'WARNING';
    const typeOfNotification = isError && !warningMessage ? 'ERROR' : isWarning;

    const key = `LIST.MASTER_DATA.NOTIFICATIONS.${action}.${typeOfNotification}`.toUpperCase();
    const resourceType = this.translationService.translate(
      `MENU.MASTER_DATA.${this.dialogInjectedData.dataType}.LBL`.toUpperCase()
    );
    const params = { resourceType };

    return this.translationService.translate(key, params);
  }

  private saveData(dataForRequest: MasterDataForRequest, action: any) {
    return this.dataService.save(dataForRequest).subscribe(
      result => {
        this.successCallback(action.clickedBtn, result);
        // TODO: This should be refactored. Component specific logic shouldn't be handled in general class.
        const configData = action.contentComponentRef.config.data;
        if (configData.confirmationData) {
          const confirmationData = configData.confirmationData.data;
          confirmationData.gridTable = configData.gridTable;
          confirmationData.activatedRoute = configData.activatedRoute;
          confirmationData.rowTableContent = configData.rowTableContent ? configData.rowTableContent : null;
          this.openConfirmationDialog(configData.confirmationData, result);
        }
      },
      errors => this.errorCallback(errors)
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
