import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TermsAndConditionsComponent } from './terms-and-conditions.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: TermsAndConditionsComponent,
    data: { tab: ROUTES.TERMS }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermsAndConditionsRoutingModule {}
