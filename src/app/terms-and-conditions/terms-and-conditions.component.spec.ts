import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';

import { TermsAndConditionsComponent } from './terms-and-conditions.component';
import { AppConfigurationService } from '~app/shared/services';

describe('TermsAndConditionsComponent', () => {
  let component: TermsAndConditionsComponent;
  let fixture: ComponentFixture<TermsAndConditionsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TermsAndConditionsComponent],
      providers: [mockProvider(AppConfigurationService)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsAndConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call scrollIntoView for the selected element', () => {
    const element = fixture.debugElement.nativeElement.querySelector('#duration');

    spyOn(element, 'scrollIntoView');

    component.scrollToSection('duration');

    expect(element.scrollIntoView).toHaveBeenCalledWith({ behavior: 'smooth' });
  });
});
