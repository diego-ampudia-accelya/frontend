import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Inject,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';

import { QuickLink } from './quick-link.model';
import { AppConfigurationService } from '~app/shared/services';
import { isInVerticalViewport } from '~app/shared/utils';

@Component({
  selector: 'bspl-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss']
})
export class TermsAndConditionsComponent implements AfterViewInit {
  @ViewChild('mainContent', { static: true }) private mainContent: ElementRef<HTMLElement>;
  @ViewChildren('section') private sections: QueryList<ElementRef>;

  public links: Array<QuickLink>;

  public get currentSubTitle(): HTMLHeadElement {
    return this._currentSubTitle;
  }

  private subSectionTitles: HTMLHeadingElement[];

  private _currentSubTitle: HTMLHeadElement;

  constructor(
    public appConfiguration: AppConfigurationService,
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: Document,
    private cd: ChangeDetectorRef
  ) {}

  @HostListener('window:scroll')
  public onScroll() {
    this.markCurrentSubTitle();
  }

  public ngAfterViewInit(): void {
    this.subSectionTitles = Array.from(this.mainContent.nativeElement.querySelectorAll('h3'));
    this.links = this.generateLinks();

    this.cd.detectChanges();
  }

  public scrollToSection(id: string) {
    const element: HTMLElement = this.elementRef.nativeElement.querySelector(`#${id}`);

    element?.scrollIntoView({ behavior: 'smooth' });
  }

  public isScrolledToTop(): boolean {
    const topOffset = 100;

    return this.document.body.scrollTop <= topOffset && this.document.documentElement.scrollTop <= topOffset;
  }

  private generateLinks(): Array<QuickLink> {
    return this.sections
      .map(s => s.nativeElement)
      .map(section => {
        const mainTitle: HTMLElement = section.querySelector('h2');
        const subTitles = Array.from(section.querySelectorAll('h3'));

        return {
          title: mainTitle.innerText,
          id: mainTitle.id,
          children: subTitles?.map((subTitle: HTMLElement) => ({
            title: subTitle.innerText,
            id: subTitle.id
          }))
        };
      });
  }

  private markCurrentSubTitle(): void {
    const currentSubTitleClass = 'title-highlight';
    this._currentSubTitle = this.subSectionTitles.find(isInVerticalViewport);

    this.subSectionTitles.forEach(t => t.classList.remove(currentSubTitleClass));
    if (!this.isScrolledToTop()) {
      this.currentSubTitle?.classList.add(currentSubTitleClass);
    }
  }
}
