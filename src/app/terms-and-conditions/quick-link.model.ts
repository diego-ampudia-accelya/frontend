export interface QuickLink {
  title?: string;
  id?: string;
  children?: Array<{
    title: string;
    id: string;
  }>;
}
