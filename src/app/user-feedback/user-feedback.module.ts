import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { CardPageComponent } from '~app/shared/components/card-page/card-page.component';
import { SharedModule } from '~app/shared/shared.module';
import { UserDetailsFormComponent } from '~app/user-feedback/components/user-details-form/user-details-form.component';
import { UserFeedbackFormComponent } from '~app/user-feedback/components/user-feedback-form/user-feedback-form.component';
import { UserDetailsComponent } from '~app/user-feedback/containers/user-details/user-details.component';
import { UserFeedbackComponent } from '~app/user-feedback/containers/user-feedback/user-feedback.component';
import { UserDetailsEffects, UserFeedbackEffects } from '~app/user-feedback/effects';
import * as fromUserFeedback from '~app/user-feedback/reducers';
import { UserFeedbackRoutingModule } from '~app/user-feedback/user-feedback-routing.module';

@NgModule({
  declarations: [
    CardPageComponent,
    UserDetailsComponent,
    UserDetailsFormComponent,
    UserFeedbackComponent,
    UserFeedbackFormComponent
  ],
  imports: [
    CommonModule,
    UserFeedbackRoutingModule,
    SharedModule,
    NgbRatingModule,
    StoreModule.forFeature(fromUserFeedback.userFeedbackFeatureKey, fromUserFeedback.reducers),
    EffectsModule.forFeature([UserDetailsEffects, UserFeedbackEffects])
  ]
})
export class UserFeedbackModule {}
