import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { UserFeedbackData } from '~app/user-feedback/models/userFeedbackData';

export const submitUserFeedback = createAction(
  '[User Feedback] Submit UserFeedback',
  props<{ userFeedbackData: UserFeedbackData }>()
);

export const saveUserFeedbackSuccess = createAction(
  '[User Feedback API] Save UserFeedback success',
  props<{ userFeedbackData: UserFeedbackData }>()
);

export const saveUserFeedbackFailure = createAction(
  '[User Feedback API] Save UserFeedback failure',
  props<{ error: ServerError }>()
);
