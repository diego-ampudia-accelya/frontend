import { createAction, props } from '@ngrx/store';

import { ServerError } from '~app/shared/errors';
import { UserFeedbackDetails } from '~app/user-feedback/models/userFeedbackDetails';

export const submitUserFeedbackDetails = createAction(
  '[User Feedback Details] Submit UserFeedbackDetails',
  props<{ userFeedbackDetails: UserFeedbackDetails }>()
);

export const saveUserFeedbackDetailsSuccess = createAction(
  '[User Feedback Details API] Save UserFeedbackDetails success',
  props<{ userFeedbackDetails: UserFeedbackDetails }>()
);

export const saveUserFeedbackDetailsFailure = createAction(
  '[User Feedback Details API] Save UserFeedbackDetails failure',
  props<{ error: ServerError }>()
);
