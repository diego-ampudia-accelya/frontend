import * as UserDetailsActions from '~app/user-feedback/actions/user-details.actions';
import * as UserFeedbackActions from '~app/user-feedback/actions/user-feedback.actions';

export { UserDetailsActions, UserFeedbackActions };
