export interface UserFeedbackData {
  userDataId?: string;
  reason?: string;
  rating?: number;
  textualFeedback?: string;
  time?: string;
  id?: number;
}
