export interface UserFeedbackDetails {
  id: number;
  email: string;
  code: string;
  firstName: string;
  lastName: string;
  bspNames: string[];
  userAgent: string;
  time?: string;
  type?: string;
  userInternalId?: number;
}
