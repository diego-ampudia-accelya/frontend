import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FormUtil } from '~app/shared/helpers';
import { UserFeedbackData } from '~app/user-feedback/models/userFeedbackData';

@Component({
  selector: 'bspl-user-feedback-form',
  templateUrl: './user-feedback-form.component.html',
  styleUrls: ['./user-feedback-form.component.scss']
})
export class UserFeedbackFormComponent implements OnInit {
  @Output() public formSubmit = new EventEmitter<UserFeedbackData>();
  @Input() set userDataId(value: number) {
    this._userDataId = value;

    if (this.form) {
      this.form.get('userDataId').setValue(value);
    }
  }

  public form: FormGroup;

  public radioButtonOptions = [
    {
      value: 'continue_later',
      label: 'trainingSite.logout.reasons.continue_later'
    },
    {
      value: 'training_completed',
      label: 'trainingSite.logout.reasons.training_completed'
    },
    {
      value: 'cannot_practice',
      label: 'trainingSite.logout.reasons.cannot_practice'
    }
  ];

  public ratingWasChanged = false;

  private _userDataId: number = null;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    const formConfig: any = {
      userDataId: [this._userDataId],
      reason: [this.radioButtonOptions[0].value, { updateOn: 'change' }],
      rating: [0, { updateOn: 'change' }],
      textualFeedback: ['', { validators: [Validators.maxLength(10000)], updateOn: 'change' }]
    };

    this.form = this.formBuilder.group(formConfig);
  }

  onRateChange() {
    this.ratingWasChanged = true;
  }

  onFormSubmit(event: any) {
    event.preventDefault();

    FormUtil.showControlState(this.form);

    if (this.form.valid) {
      this.formSubmit.emit(this.form.value);
    }
  }
}
