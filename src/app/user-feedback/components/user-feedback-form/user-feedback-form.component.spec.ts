import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { TranslatePipeMock } from '~app/test/translate.mock.pipe';
import { UserFeedbackFormComponent } from '~app/user-feedback/components/user-feedback-form/user-feedback-form.component';

const createSpyObj = jasmine.createSpyObj;
const translationServiceSpy = createSpyObj('L10nTranslationService', ['translate']);

describe('UserFeedbackFormComponent', () => {
  let component: UserFeedbackFormComponent;
  let fixture: ComponentFixture<UserFeedbackFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserFeedbackFormComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [FormBuilder, { provide: L10nTranslationService, useValue: translationServiceSpy }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFeedbackFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
