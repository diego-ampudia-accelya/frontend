import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import bsps from '~app/shared/constants/bsps';
import { FormUtil } from '~app/shared/helpers';
import { UserFeedbackDetails } from '~app/user-feedback/models/userFeedbackDetails';

@Component({
  selector: 'bspl-user-details-form',
  templateUrl: './user-details-form.component.html',
  styleUrls: ['./user-details-form.component.scss']
})
export class UserDetailsFormComponent implements OnInit {
  @Input() public showAirlineCode = false;
  @Input() public showAgentCode = false;

  @Output() formSubmit = new EventEmitter<UserFeedbackDetails>();

  public form: FormGroup;
  public bspOptions = bsps.map(bspName => ({ value: bspName, label: bspName }));
  public isAirlineUser = false;
  public isAgentUser = false;

  public emailCustomErrorMessages = {
    email: 'trainingSite.user-data.email_error',
    required: 'trainingSite.user-data.email_error'
  };

  public bspNamesCustomErrorMessages = {
    required: 'trainingSite.user-data.bsps_error'
  };

  public airlineCodeCustomErrorMessages = {
    required: 'trainingSite.user-data.airline_code_error',
    pattern: 'trainingSite.user-data.airline_code_error'
  };

  public agentCodeCustomErrorMessages = {
    required: 'trainingSite.user-data.agent_code_error',
    pattern: 'trainingSite.user-data.agent_code_error'
  };

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    let codeFieldValidators = [];

    if (this.showAgentCode) {
      codeFieldValidators = [Validators.pattern('^\\d{7}$'), Validators.required];
    } else if (this.showAirlineCode) {
      codeFieldValidators = [Validators.pattern('^\\d{3}$'), Validators.required];
    }

    const formConfig: any = {
      email: ['', { validators: [Validators.email, Validators.required], updateOn: 'change' }],
      bspNames: [null, { validators: [Validators.required], updateOn: 'change' }],
      code: ['', { validators: codeFieldValidators, updateOn: 'change' }],
      firstName: ['', { updateOn: 'change' }],
      lastName: ['', { updateOn: 'change' }],
      userAgent: [navigator.userAgent]
    };

    this.form = this.formBuilder.group(formConfig);
  }

  onFormSubmit(event: Event) {
    event.preventDefault();

    FormUtil.showControlState(this.form);

    if (this.form.valid) {
      this.formSubmit.emit(this.form.value);
    }
  }
}
