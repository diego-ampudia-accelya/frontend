import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { TranslatePipeMock } from '~app/test/translate.mock.pipe';
import { UserDetailsFormComponent } from '~app/user-feedback/components/user-details-form/user-details-form.component';

const createSpyObj = jasmine.createSpyObj;
const translationServiceSpy = createSpyObj('L10nTranslationService', ['translate']);

describe('UserDetailsFormComponent', () => {
  let component: UserDetailsFormComponent;
  let fixture: ComponentFixture<UserDetailsFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserDetailsFormComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [FormBuilder, { provide: L10nTranslationService, useValue: translationServiceSpy }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
