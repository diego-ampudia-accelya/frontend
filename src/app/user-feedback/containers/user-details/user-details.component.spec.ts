import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { UserDetailsComponent } from '~app/user-feedback/containers/user-details/user-details.component';
import { UserFeedbackDetailsLocalStorageService } from '~app/user-feedback/services/user-feedback-details-local-storage.service';

const userFeedbackDetailsLocalStorageServiceSpy = createSpyObject(UserFeedbackDetailsLocalStorageService);
const routerSpy = createSpyObject(Router);

describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;

  beforeEach(waitForAsync(() => {
    const initialState = {
      auth: {
        user: createAirlineUser()
      },
      userFeedback: {
        userDetailsState: {
          userFeedbackDetails: {
            id: null,
            email: '',
            bspNames: [],
            code: '',
            firstName: '',
            lastName: '',
            userAgent: ''
          },
          loading: false
        }
      },
      userFeedbackFormState: {
        loading: false
      }
    };

    TestBed.configureTestingModule({
      declarations: [UserDetailsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({ initialState }),
        {
          provide: UserFeedbackDetailsLocalStorageService,
          useValue: userFeedbackDetailsLocalStorageServiceSpy
        },
        {
          provide: Router,
          useValue: routerSpy
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
