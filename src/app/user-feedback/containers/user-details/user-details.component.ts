import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { pick } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { filter, map, take, takeUntil, tap } from 'rxjs/operators';

import { getUserType, isAgentUser, isAirlineUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import { UserDetailsActions } from '~app/user-feedback/actions';
import { UserDetailsFormComponent } from '~app/user-feedback/components/user-details-form/user-details-form.component';
import { UserFeedbackDetails } from '~app/user-feedback/models/userFeedbackDetails';
import { getUserDetailsId, getUserDetailsLoading } from '~app/user-feedback/reducers';
import { UserFeedbackDetailsLocalStorageService } from '~app/user-feedback/services/user-feedback-details-local-storage.service';

@Component({
  selector: 'bspl-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(UserDetailsFormComponent, { static: true }) userDetailsFormComponent: UserDetailsFormComponent;

  public loading$: Observable<boolean> = this.store.pipe(select(getUserDetailsLoading));
  public isAirlineUser$: Observable<boolean> = this.store.pipe(select(isAirlineUser));
  public isAgentUser$: Observable<boolean> = this.store.pipe(select(isAgentUser));
  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private detailsStorageService: UserFeedbackDetailsLocalStorageService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router
  ) {}

  public onUserDetailsFormSubmit(userFeedbackDetails: UserFeedbackDetails) {
    this.store.dispatch(UserDetailsActions.submitUserFeedbackDetails({ userFeedbackDetails }));
  }

  public ngOnInit() {
    this.navigateToDashboardIfHasDetails();
  }

  public ngAfterViewInit() {
    this.autoPopulateDetailsForm();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private navigateToDashboardIfHasDetails() {
    this.store
      .pipe(
        select(getUserDetailsId),
        map(userFeedbackDetailsId => userFeedbackDetailsId != null),
        take(1)
      )
      .subscribe(hasDetails => {
        if (hasDetails) {
          this.router.navigateByUrl(ROUTES.DASHBOARD.url);
        }
      });
  }

  private autoPopulateDetailsForm() {
    this.store
      .pipe(
        select(getUserType),
        filter(Boolean),
        tap(userType => {
          const value = this.detailsStorageService.getUserFeedbackDetails(userType.toString());
          const form = this.userDetailsFormComponent && this.userDetailsFormComponent.form;

          if (form) {
            form.patchValue(pick(value, ['email', 'bspNames', 'code', 'firstName', 'lastName']));

            this.changeDetectorRef.detectChanges();
          }
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
}
