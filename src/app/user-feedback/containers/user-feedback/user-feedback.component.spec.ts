import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { UserFeedbackComponent } from '~app/user-feedback/containers/user-feedback/user-feedback.component';

describe('UserFeedbackComponent', () => {
  let component: UserFeedbackComponent;
  let fixture: ComponentFixture<UserFeedbackComponent>;
  const initialState = {
    auth: {
      user: createAirlineUser()
    },
    userFeedback: {
      userDetailsState: {
        userFeedbackDetails: {
          id: null,
          email: '',
          bspNames: [],
          code: '',
          firstName: '',
          lastName: '',
          userAgent: ''
        },
        loading: false
      }
    },
    userFeedbackFormState: {
      loading: false
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserFeedbackComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [provideMockStore({ initialState })]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
