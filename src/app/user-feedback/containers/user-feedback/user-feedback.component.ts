import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '~app/reducers';
import { UserFeedbackActions } from '~app/user-feedback/actions';
import { UserFeedbackData } from '~app/user-feedback/models/userFeedbackData';
import { getUserDetailsId, getUserFeedbackLoading } from '~app/user-feedback/reducers';

@Component({
  selector: 'bspl-user-feedback',
  templateUrl: './user-feedback.component.html',
  styleUrls: ['./user-feedback.component.scss']
})
export class UserFeedbackComponent {
  public loading$: Observable<boolean> = this.store.pipe(select(getUserFeedbackLoading));
  public userDataId$: Observable<number> = this.store.pipe(select(getUserDetailsId));

  constructor(private store: Store<AppState>) {}

  public onFeedbackFormSubmit(userFeedbackData: UserFeedbackData) {
    this.store.dispatch(UserFeedbackActions.submitUserFeedback({ userFeedbackData }));
  }
}
