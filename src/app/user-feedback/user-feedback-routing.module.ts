import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ROUTES } from '~app/shared/constants/routes';
import { UserDetailsComponent } from '~app/user-feedback/containers/user-details/user-details.component';
import { UserFeedbackComponent } from '~app/user-feedback/containers/user-feedback/user-feedback.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ROUTES.USER_FEEDBACK_DETAILS.path,
        component: UserDetailsComponent
      },
      {
        path: ROUTES.USER_FEEDBACK_FINISH.path,
        component: UserFeedbackComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserFeedbackRoutingModule {}
