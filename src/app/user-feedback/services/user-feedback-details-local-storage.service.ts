import { Injectable } from '@angular/core';

import { UserFeedbackDetails } from '~app/user-feedback/models/userFeedbackDetails';

const storageKey = 'fbud';

@Injectable({
  providedIn: 'root'
})
export class UserFeedbackDetailsLocalStorageService {
  storeUserFeedbackDetails(userDetails: UserFeedbackDetails, userType: string) {
    const value = JSON.stringify({
      ...JSON.parse(localStorage.getItem(storageKey) || '{}'),
      [userType]: userDetails
    });

    localStorage.setItem(storageKey, value);
  }

  getUserFeedbackDetails(userType: string): UserFeedbackDetails {
    if (userType) {
      return JSON.parse(localStorage.getItem(storageKey) || '{}')[userType] || null;
    }

    return null;
  }
}
