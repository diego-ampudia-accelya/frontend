import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { intersection, iteratee } from 'lodash';
import { of } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';

import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import { appConfiguration } from '~app/shared/services';
import { getUserDetailsId } from '~app/user-feedback/reducers';

@Injectable({
  providedIn: 'root'
})
export class UserFeedbackGuard implements CanActivate {
  private bspsCodes$ = this.store.pipe(
    select(getUserBsps),
    filter(bsps => bsps && bsps.length > 0),
    map(bsps => bsps.map(iteratee('isoCountryCode')))
  );

  private hasUserFeedbackDetails$ = this.store.pipe(
    select(getUserDetailsId),
    map(userFeedbackDetailsId => userFeedbackDetailsId != null)
  );

  constructor(private store: Store<AppState>, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const trainingBspCodes = appConfiguration.trainingBspCodes;

    if (trainingBspCodes.length === 0) {
      return true;
    }

    return this.bspsCodes$.pipe(
      switchMap(bspCodes => {
        const isTraining = intersection(trainingBspCodes, bspCodes).length > 0;

        if (isTraining) {
          return this.hasUserFeedbackDetails$.pipe(
            tap(hasDetails => {
              if (!hasDetails) {
                setTimeout(() => {
                  this.router.navigateByUrl(ROUTES.USER_FEEDBACK_DETAILS.url);
                });
              }
            }),
            map(() => true)
          );
        }

        return of(true);
      }),
      take(1)
    );
  }
}
