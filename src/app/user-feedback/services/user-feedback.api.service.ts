import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pick } from 'lodash';
import { Observable } from 'rxjs';

import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { UserFeedbackData } from '~app/user-feedback/models/userFeedbackData';
import { UserFeedbackDetails } from '~app/user-feedback/models/userFeedbackDetails';

@Injectable({
  providedIn: 'root'
})
export class UserFeedbackApiService {
  constructor(protected http: HttpClient) {}

  public postUserFeedbackDetails(userFeedbackDetails: UserFeedbackDetails): Observable<UserFeedbackDetails> {
    const url = `${appConfiguration.baseApiPath}/user-feedback/surveyed-users`;

    return this.http.post<UserFeedbackDetails>(url, userFeedbackDetails);
  }

  public postUserFeedback(userFeedbackData: UserFeedbackData): Observable<UserFeedbackData> {
    const url = `${appConfiguration.baseApiPath}/user-feedback/surveyed-users/${userFeedbackData.userDataId}/feedbacks`;
    const payload = pick(userFeedbackData, ['rating', 'reason', 'textualFeedback']);

    return this.http.post<UserFeedbackData>(url, payload);
  }
}
