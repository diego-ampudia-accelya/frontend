import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { ROUTES } from '~app/shared/constants/routes';
import { UserDetailsActions } from '~app/user-feedback/actions';
import { UserFeedbackDetails } from '~app/user-feedback/models/userFeedbackDetails';
import { UserFeedbackDetailsLocalStorageService } from '~app/user-feedback/services/user-feedback-details-local-storage.service';
import { UserFeedbackApiService } from '~app/user-feedback/services/user-feedback.api.service';

@Injectable()
export class UserDetailsEffects {
  $submitUserFeedbackDetails = createEffect(() =>
    this.actions$.pipe(
      ofType(UserDetailsActions.submitUserFeedbackDetails),
      switchMap(({ userFeedbackDetails }) =>
        this.userFeedbackApiService.postUserFeedbackDetails(userFeedbackDetails).pipe(
          tap(savedUserFeedbackDetails => this.addDetailsInCache(savedUserFeedbackDetails)),
          map(savedUserFeedbackDetails =>
            UserDetailsActions.saveUserFeedbackDetailsSuccess({ userFeedbackDetails: savedUserFeedbackDetails })
          ),
          catchError(error =>
            merge(of(UserDetailsActions.saveUserFeedbackDetailsFailure({ error })), throwError(error))
          )
        )
      )
    )
  );

  $saveUserFeedbackSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserDetailsActions.saveUserFeedbackDetailsSuccess),
        tap(() => this.navigateToDashboard())
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private userFeedbackApiService: UserFeedbackApiService,
    private detailsStorageService: UserFeedbackDetailsLocalStorageService,
    private router: Router
  ) {}

  private addDetailsInCache(savedUserFeedbackDetails: UserFeedbackDetails) {
    this.detailsStorageService.storeUserFeedbackDetails(savedUserFeedbackDetails, savedUserFeedbackDetails.type);
  }

  private navigateToDashboard() {
    this.router.navigateByUrl(ROUTES.DASHBOARD.url);
  }
}
