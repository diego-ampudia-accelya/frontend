import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Logout } from '~app/auth/actions/auth.actions';
import { UserFeedbackActions } from '~app/user-feedback/actions';
import { UserFeedbackApiService } from '~app/user-feedback/services/user-feedback.api.service';

@Injectable()
export class UserFeedbackEffects {
  $submitUserFeedbackDetails = createEffect(() =>
    this.actions$.pipe(
      ofType(UserFeedbackActions.submitUserFeedback),
      switchMap(({ userFeedbackData }) =>
        this.userFeedbackApiService.postUserFeedback(userFeedbackData).pipe(
          map(savedUserFeedbackData =>
            UserFeedbackActions.saveUserFeedbackSuccess({ userFeedbackData: savedUserFeedbackData })
          ),
          catchError(error => merge(of(UserFeedbackActions.saveUserFeedbackFailure(error)), throwError(error)))
        )
      )
    )
  );

  $saveUserFeedbackSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(UserFeedbackActions.saveUserFeedbackSuccess, UserFeedbackActions.saveUserFeedbackFailure),
      map(() => new Logout())
    )
  );

  constructor(private actions$: Actions, private userFeedbackApiService: UserFeedbackApiService) {}
}
