import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import { AppState } from '~app/reducers';
import * as fromUserDetails from '~app/user-feedback/reducers/user-details-reducer';
import * as fromUserFeedback from '~app/user-feedback/reducers/user-feedback-reducer';

export const userFeedbackFeatureKey = 'userFeedback';

export interface UserFeedbackState {
  [fromUserDetails.userDetailsFeatureKey]: fromUserDetails.State;
  [fromUserFeedback.userFeedbackFromFeatureKey]: fromUserFeedback.State;
}

export interface State extends AppState {
  [userFeedbackFeatureKey]: UserFeedbackState;
}

export function reducers(state: UserFeedbackState | undefined, action: Action) {
  return combineReducers({
    [fromUserDetails.userDetailsFeatureKey]: fromUserDetails.reducer,
    [fromUserFeedback.userFeedbackFromFeatureKey]: fromUserFeedback.reducer
  })(state, action);
}

export const getUserFeedbackState = createFeatureSelector<State, UserFeedbackState>(userFeedbackFeatureKey);

export const getUserDetailsState = createSelector(
  getUserFeedbackState,
  state => state && state[fromUserDetails.userDetailsFeatureKey]
);

export const getUserDetailsLoading = createSelector(getUserDetailsState, fromUserDetails.getLoading);

export const getUserDetailsId = createSelector(getUserDetailsState, fromUserDetails.getId);

export const getUserFeedbackFormState = createSelector(
  getUserFeedbackState,
  state => state && state[fromUserFeedback.userFeedbackFromFeatureKey]
);

export const getUserFeedbackLoading = createSelector(getUserFeedbackFormState, fromUserFeedback.getLoading);
