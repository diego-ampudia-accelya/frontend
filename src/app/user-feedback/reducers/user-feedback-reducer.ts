import { createReducer, on } from '@ngrx/store';

import { UserFeedbackActions } from '~app/user-feedback/actions';

export const userFeedbackFromFeatureKey = 'userFeedbackFormState';

export interface State {
  loading: boolean;
}

const initialSate: State = {
  loading: false
};

export const reducer = createReducer(
  initialSate,

  on(UserFeedbackActions.submitUserFeedback, state => ({
    ...state,
    loading: true
  })),
  on(UserFeedbackActions.saveUserFeedbackFailure, state => ({
    ...state,
    loading: false
  }))
);

export const getLoading = (state: State): boolean => Boolean(state && state.loading);
