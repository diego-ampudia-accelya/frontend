import { createReducer, on } from '@ngrx/store';

import { UserDetailsActions } from '~app/user-feedback/actions';
import { UserFeedbackDetails } from '~app/user-feedback/models/userFeedbackDetails';

export const userDetailsFeatureKey = 'userDetailsState';

export interface State {
  userFeedbackDetails: UserFeedbackDetails;
  loading: boolean;
}

const initialSate: State = {
  userFeedbackDetails: {
    id: null,
    email: '',
    bspNames: [],
    code: '',
    firstName: '',
    lastName: '',
    userAgent: ''
  },
  loading: false
};

export const reducer = createReducer(
  initialSate,

  on(UserDetailsActions.submitUserFeedbackDetails, state => ({
    ...state,
    loading: true
  })),
  on(UserDetailsActions.saveUserFeedbackDetailsFailure, state => ({
    ...state,
    loading: false
  })),
  on(UserDetailsActions.saveUserFeedbackDetailsSuccess, (state, { userFeedbackDetails }) => ({
    ...state,
    userFeedbackDetails
  }))
);

export const getLoading = (state: State): boolean => Boolean(state && state.loading);

export const getId = (state: State): number => state && state.userFeedbackDetails.id;
