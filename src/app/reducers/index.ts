import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { Action, ActionReducer, ActionReducerMap, createFeatureSelector, INIT, MetaReducer, UPDATE } from '@ngrx/store';
import { merge } from 'lodash';
import { localStorageSync } from 'ngrx-store-localstorage';

import { AuthActionTypes } from '~app/auth/actions/auth.actions';
import { currencyConversionFeatureKey } from '~app/master-data/iata/currency-conversion/store/reducers';
import { mainUsersFeatureKey } from '~app/master-data/iata/main-users/store/reducers';
import { featureKey as billingAnalysisFeatureKey } from '~app/sales/billing-analysis/reducers';
import { RouterStateUrl } from '~app/shared/utils/custom-router-state-serializer';
import { subUsersFeatureKey } from '~app/sub-users/reducers';
import { FEEDBACK_API_SEND_FAILURE, FEEDBACK_API_SEND_SUCCESS } from '~app/training/actions/feedback-api.actions';
import { userFeedbackFeatureKey } from '~app/user-feedback/reducers';

// Rehydratation changes all store references - https://github.com/btroncone/ngrx-store-localstorage/issues/128#issuecomment-613975060
const mergeReducer = (state: any, rehydratedState: any, action: any) => {
  if ((action.type === INIT || action.type === UPDATE) && rehydratedState) {
    state = merge(state, rehydratedState);
  }

  return state;
};

export interface AppState {
  router: RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<AppState> = {
  router: routerReducer
};

/**
 * Syncs the current store state with local storage.
 *
 * @param reducer The root reducer to decorate
 * @returns The new root reducer
 */
export function sessionStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: [
      'auth',
      'core',
      'airline',
      'trainingSite',
      'pbd',
      'refundAuthority',
      'admAcmAuthority',
      'ticketingAuthority',
      'refund',
      'acdm',
      'files',
      // TODO: Eliminate direct dependencies on feature modules to avoid bloating the main bundle
      billingAnalysisFeatureKey,
      userFeedbackFeatureKey,
      subUsersFeatureKey,
      currencyConversionFeatureKey,
      mainUsersFeatureKey
    ],
    storage: sessionStorage,
    rehydrate: true,
    mergeReducer
  })(reducer);
}

/**
 * A meta reducer that resets the state to its default value when the Logout action is dispatched
 *
 * @param reducer The root reducer to decorate
 * @returns The new root reducer
 */
export function clearState(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state, action: Action) {
    const actionType = action.type;

    switch (actionType) {
      case AuthActionTypes.LogoutAction:
      case AuthActionTypes.NavigateToClassicBspLink:
      case AuthActionTypes.NavigateToClassicBspLinkSwitchAccount:
      case FEEDBACK_API_SEND_SUCCESS:
      case FEEDBACK_API_SEND_FAILURE:
        state = undefined;
        break;
    }

    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<AppState>[] = [clearState, sessionStorageSyncReducer];

export const getRouterState = createFeatureSelector<RouterReducerState<RouterStateUrl>>('router');
