import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';

const translationPrefix = 'ADM_ACM.query';

export const SHARED = {
  get ACTIVE_COLUMNS() {
    return [
      {
        name: `${translationPrefix}.tableColumns.isoc`,
        prop: 'agent.bsp.isoCountryCode',
        draggable: false,
        resizeable: true,
        sortable: false,
        width: 40
      },
      {
        name: `${translationPrefix}.tableColumns.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: true,
        cellTemplate: 'commonLinkCellTmpl',
        width: 150
      },
      {
        name: `${translationPrefix}.tableColumns.airlCode`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.tableColumns.agentCode`,
        prop: 'agent.iataCode',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.tableColumns.status`,
        prop: 'acdmStatus',
        draggable: false,
        badgeInfo: null,
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        width: 160
      },
      {
        name: `${translationPrefix}.tableColumns.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: true,
        width: 70
      },
      {
        name: `${translationPrefix}.tableColumns.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: 'amountCellTmpl'
      },
      {
        name: `${translationPrefix}.tableColumns.issueDate`,
        prop: 'dateOfIssue',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 70
      },
      {
        name: `${translationPrefix}.tableColumns.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 70
      },
      {
        name: `${translationPrefix}.tableColumns.repDate`,
        prop: 'reportingDate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 70
      }
    ];
  },
  get DELETED_COLUMNS() {
    return [
      {
        name: `${translationPrefix}.tableColumns.isoc`,
        prop: 'agent.bsp.isoCountryCode',
        draggable: false,
        resizeable: true,
        sortable: false,
        width: 55
      },
      {
        name: `${translationPrefix}.tableColumns.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: true,
        width: 120
      },
      {
        name: `${translationPrefix}.tableColumns.airlCode`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.tableColumns.agentCode`,
        prop: 'agent.iataCode',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.tableColumns.status`,
        prop: 'acdmStatus',
        draggable: false,
        badgeInfo: null,
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        width: 160
      },
      {
        name: `${translationPrefix}.tableColumns.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: true,
        width: 70
      },
      {
        name: `${translationPrefix}.tableColumns.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: 'amountCellTmpl'
      },
      {
        name: `${translationPrefix}.tableColumns.issueDate`,
        prop: 'dateOfIssue',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        name: `${translationPrefix}.tableColumns.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 70
      }
    ];
  }
};

export const SEARCH_FORM_DEFAULT_VALUE: AdmAcmFilter = {
  bsp: null,
  ticketDocumentNumber: '',
  airlineIataCode: null,
  agentId: null,
  acdmStatus: null,
  currencyId: null,
  amountRange: null,
  concernsIndicator: null,
  dateOfIssue: null,
  period: null,
  reportingDate: null,
  sentToDpc: null,
  hasInternalComment: null,
  markStatus: null
};
