import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { BspAdjustmentListComponent } from './bsp-adjustment-list.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { creationSpdrPermission } from '~app/bsp-adjustment/shared/helpers/spcdr-permissions.config';
import { MenuBuilder } from '~app/master-data/configuration';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createAgentUser } from '~app/shared/mocks/agent-user';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { User, UserType } from '~app/shared/models/user.model';

describe('BspAdjustmentListComponent', () => {
  let fixture: ComponentFixture<BspAdjustmentListComponent>;
  let component: BspAdjustmentListComponent;

  const menuBuilderMock = jasmine.createSpyObj<MenuBuilder>('MenuBuilder', {
    buildMenuItemsFrom: [
      {
        route: '/bsp-adjustments/spdr/query/deleted',
        title: 'ADM_ACM.query.tabs.deletedTitle',
        isAccessible: true
      }
    ]
  });

  const activatedRouteStub = {
    snapshot: {
      data: {
        tab: ROUTES.SPDR_QUERY
      }
    }
  };

  const initialState = {
    auth: {}
  };

  let routerMock;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/deleted', './deleted')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './deleted'
    };

    TestBed.configureTestingModule({
      declarations: [BspAdjustmentListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        L10nTranslationService,
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: MenuBuilder, useValue: menuBuilderMock },
        provideMockStore({ selectors: [{ selector: fromAuth.isLoggedIn, value: true }], initialState }),
        { provide: Router, useValue: routerMock }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BspAdjustmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should be allowed issue ACDM according to user permissions', () => {
    component.admAcmType = MasterDataType.Spdr;
    component.isSpcdr = true;

    const users: User[] = [
      {
        ...createIataUser(),
        permissions: [creationSpdrPermission.createSpdrAgn, creationSpdrPermission.createSpdrAirl]
      } as User,
      {
        ...createAgentUser(),
        permissions: []
      } as User,
      {
        ...createAirlineUser(),
        permissions: []
      } as User
    ];

    users.forEach(user => {
      component.loggedUser = user;
      expect(component.canUserIssueBspAdjustment()).toBe(component.loggedUserIsOfType(UserType.IATA));
    });
  });
});
