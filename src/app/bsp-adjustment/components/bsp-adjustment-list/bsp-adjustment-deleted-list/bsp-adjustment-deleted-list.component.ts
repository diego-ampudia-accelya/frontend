import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { mapValues } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';
import { AcdmActionType, AdmAcmStatus } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import {
  getAcdmEditInternalCommentPermission,
  getAcdmReadInternalCommentPermission,
  getBspFilterRequiredPermissions
} from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { deletedKeys, deletedSelectors, State } from '~app/adm-acm/store/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  SEARCH_FORM_DEFAULT_VALUE,
  SHARED
} from '~app/bsp-adjustment/components/bsp-adjustment-list/bsp-adjustment-list.constants';
import { getAcdmdQueryPermission } from '~app/bsp-adjustment/shared/helpers/acdmd-permissions.config';
import { getSpcdrQueryPermission } from '~app/bsp-adjustment/shared/helpers/spcdr-permissions.config';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService, DownloadFileComponent, FooterButton, PeriodOption } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  TabService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-bsp-adjustment-deleted-list',
  templateUrl: './bsp-adjustment-deleted-list.component.html',
  styleUrls: ['./bsp-adjustment-deleted-list.component.scss'],
  providers: [
    AdmAcmFilterFormatter,
    AdmAcmService,
    AdmAcmDataService,
    {
      provide: AdmAcmConfigService,
      useFactory: createAdmAcmConfigService,
      deps: [AdmAcmDataService, AdmAcmDialogService, L10nTranslationService, HttpClient, Router, Store, TabService]
    }
  ]
})
export class BspAdjustmentDeletedListComponent
  extends ListSubtabs<AdmAcmIssueBE, AdmAcmFilter>
  implements OnInit, OnDestroy
{
  protected initialSortAttribute: keyof AdmAcmFilter = 'dateOfIssue';

  public admAcmType: MasterDataType;
  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)
  public isSpcdr = false; //* Indicates if it is an SPCDR (BSP Adjustment)

  public listActions$: Observable<{ action: GridTableActionType; disabled?: boolean; group?: string }[]>;
  public columns: Array<GridColumn> = [];
  public downloadQuery: Partial<DataQuery>;

  public deletionReason$: Observable<string>;
  public memoReason$: Observable<string>;

  public tableTitle: string;

  public searchForm: FormGroup;
  public airlineCodesList$: Observable<DropdownOption[]>;
  public agentCodesList$: Observable<DropdownOption[]>;
  public bspCountriesList: DropdownOption<BspDto>[];
  public currencyList: DropdownOption<Currency>[];
  public rtdnTypeList$: Observable<DropdownOption[]> = of([]);
  public periodOptions$: Observable<PeriodOption[]>;

  public isInternalCommentVisible = false;
  public isActionListVisible = false;
  public hasInternalCommentList: DropdownOption<boolean>[] = [];

  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;

  private hasLeanPermission: boolean;

  public userTypes = UserType;
  public loggedUser: User;
  public userType: UserType;
  public isAirlineLoggedUser: boolean;
  public isAgentLoggedUser: boolean;

  public hasEditInternalCommentPermission = false;
  private hasReadInternalCommentPermission = false;

  public periodIsDisabled = true;

  private bspControl: AbstractControl;
  private agentListControl: AbstractControl;
  private periodControl: AbstractControl;
  private currencyControl: AbstractControl;

  private isMultiCountryUser: boolean;
  private bspSelectedIds: number[] = [];

  private dialogQueryFormSubmitEmitter = new Subject<AcdmActionType>();

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  public periodPickerYearsBack: number;

  constructor(
    protected store: Store<AppState>,
    protected admAcmService: AdmAcmService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    public displayFormatter: AdmAcmFilterFormatter,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private formConfig: AdmAcmConfigService,
    private agentDictionaryService: AgentDictionaryService,
    private airlineDictionaryService: AirlineDictionaryService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private periodService: PeriodService,
    private dialogService: DialogService,
    private permissionsService: PermissionsService,
    private admAcmDialogService: AdmAcmDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(store, admAcmService, actions$);
    this.setConfig();
  }

  private setConfig(): void {
    const data = this.activatedRoute.snapshot.data;

    this.admAcmType = data.admAcmType;
    this.formConfig.admAcmType = data.admAcmType;
    this.tableTitle = `ADM_ACM.query.${this.admAcmType}_listTitle`;
    this.isAcdmd = this.admAcmType === MasterDataType.Admd || this.admAcmType === MasterDataType.Acmd;
    this.isSpcdr = this.admAcmType === MasterDataType.Spdr || this.admAcmType === MasterDataType.Spcr;
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>,
    DefaultProjectorFn<ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>>
  > {
    return deletedSelectors[this.admAcmType];
  }

  protected getListKey(): string {
    return deletedKeys[this.admAcmType];
  }

  protected adaptSearchQuery(query: DataQuery<AdmAcmFilter>): DataQuery<AdmAcmFilter> {
    return query.filterBy.acdmStatus
      ? query
      : { ...query, filterBy: { ...query.filterBy, acdmStatus: [AdmAcmStatus.deleted] } };
  }

  ngOnInit() {
    this.setInitialFilterValues();

    // Initial loading view
    this.loading$ = of(true);
    this.initializeColumns();

    combineLatest([this.initializeLoggedUser$(), this.permissionsService.permissions$.pipe(first())])
      .pipe(
        tap(() => this.initializePermissions()),
        switchMap(() => this.initializeLeanBspFilter$()),
        tap(() => super.ngOnInit()),
        tap(() => this.setResolversData()),
        tap(() => this.initializeColumns()),
        tap(() => this.updatePeriodControlDisability()),
        tap(() => this.updatePeriodFilterOptions()),
        tap(() => this.setPeriodPickerYearsBack()),
        takeUntil(this.destroy$)
      )
      .subscribe();

    this.initializeFilterVisibility();
  }

  private initializeLoggedUser$(): Observable<any> {
    return this.store.pipe(select(getUser), first()).pipe(
      tap(user => {
        this.loggedUser = user;
        this.userType = user.userType;
        this.isMultiCountryUser = user.bsps.length > 1;
      })
    );
  }

  private initializePermissions(): void {
    // Internal comment permissions
    this.hasReadInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmReadInternalCommentPermission(this.admAcmType)
    );
    this.hasEditInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmEditInternalCommentPermission(this.admAcmType)
    );

    this.isActionListVisible = this.hasEditInternalCommentPermission || this.userType === UserType.IATA;

    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private setResolversData(): void {
    //* Initialize specific property observables for row details
    this.deletionReason$ = this.selectedItem$.pipe(map(item => item?.deletionReason || ''));
    this.memoReason$ = this.selectedItem$.pipe(map(item => item?.issueReason || ''));

    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.downloadQuery = this.adaptSearchQuery(query)));
    this.dialogQueryFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.onQueryChanged());
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, getBspFilterRequiredPermissions(this.admAcmType))
      .pipe(
        tap(bspList => {
          // Update BSP selected ids and predefined filters with the default BSP
          const onlyOneBsp = bspList.length === 1;

          if (bspList.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);

            const firstListBsp = bspList[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelectedIds = [filterValue.id];

            // Agent users have not predefined filters
            if (this.userType !== UserType.AGENT) {
              this.predefinedFilters = {
                ...this.predefinedFilters,
                bsp: filterValue
              };
            }
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspOptions => (this.isBspFilterMultiple = this.hasLeanPermission && bspOptions?.length !== 1)),
        tap(bspOptions => (this.isBspFilterLocked = bspOptions.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  private initializeColumns(): void {
    this.columns = SHARED.DELETED_COLUMNS;

    const statusColumn = this.columns.find(col => col.prop === 'acdmStatus');
    if (statusColumn) {
      statusColumn.pipe = {
        transform: value => this.translationService.translate(`ADM_ACM.status.${value}`)
      };
    }

    if (this.userType === UserType.AGENT) {
      this.columns = this.columns.filter(column => column.prop !== 'agent.iataCode');
    }

    if (this.userType === UserType.AIRLINE) {
      this.columns = this.columns.filter(column => column.prop !== 'airline.iataCode');
    }

    if (this.hasReadInternalCommentPermission) {
      this.setDocumentNumberColumn();
    }

    this.setBspColumn();
  }

  private setBspColumn(): void {
    const bspColumn = this.columns.find(column => column.prop === 'agent.bsp.isoCountryCode');

    if (bspColumn) {
      bspColumn.sortable = this.hasLeanPermission;
    }
  }

  private setDocumentNumberColumn(): void {
    const documentNumberColumn = this.columns.find(column => column.prop === 'ticketDocumentNumber');

    if (documentNumberColumn) {
      documentNumberColumn.image = (acdm: AdmAcmIssueBE) =>
        acdm.internalComment
          ? {
              src: '/assets/images/utils/discuss.svg',
              tooltipLabel: this.translationService.translate(`ADM_ACM.query.internalCommentTooltip`)
            }
          : null;
    }
  }

  private setInitialFilterValues(): void {
    this.searchForm = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));

    this.bspControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'bsp');
    this.agentListControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'agentId');
    this.periodControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'period');
    this.currencyControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'currencyId');

    if (this.isAcdmd) {
      this.searchForm.get('concernsIndicator').disable();
    }
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      switch (this.userType) {
        case UserType.AGENT:
          this.initializeAirlineFilterDropdown();
          break;
        case UserType.AIRLINE:
          this.updateAgentList();
          break;
        case UserType.AGENT_GROUP:
          this.initializeAgentGroupFilterDropdown();
          this.initializeAirlineFilterDropdown();
          break;
        default:
          this.initializeAirlineFilterDropdown();
          this.updateAgentList();
      }

      this.initializeBspCountrieslistener();
      this.populateConfDropdowns();
      this.updateCurrencyFilterList();

      if (this.isInternalCommentVisible) {
        this.initializeInternalCommentFilterDropdown();
      }
    }
  }

  private initializeBspCountrieslistener(): void {
    this.bspControl.valueChanges
      .pipe(
        tap(bsps => this.updateBspSelectedIds(bsps)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.updateAgentList();
        this.updateCurrencyList();
        this.updatePeriodControlDisability();
        this.updatePeriodFilterOptions();
      });
  }

  private updateBspSelectedIds(bsps: Bsp): void {
    if (bsps) {
      this.bspSelectedIds = Array.isArray(bsps) ? bsps.map(bsp => bsp.id) : [bsps.id];
    } else {
      this.bspSelectedIds = [];
    }
  }

  private updateAgentList(): void {
    // Not allowing local airlines to update agent filter
    if (!this.isAirlineLoggedUser || this.isMultiCountryUser) {
      this.updateAgentFilterList();
      this.updateAgentListControlValue();
    }
  }

  private updateAgentFilterList(): void {
    const params = this.bspSelectedIds?.length ? { bspId: this.bspSelectedIds } : null;

    this.agentCodesList$ = this.agentDictionaryService.getDropdownOptions(params);
  }

  private updateAgentListControlValue(): void {
    const agentsSelected: AgentSummary[] = this.agentListControl.value;

    if (this.bspSelectedIds.length && agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.bspSelectedIds.some(bspId => agent.bsp?.id === bspId));
      this.agentListControl.patchValue(agentsToPatch);
    }
  }

  private updateCurrencyList(): void {
    if (this.isMultiCountryUser) {
      this.updateCurrencyFilterList();
      this.updateCurrencyListControlValue();
    }
  }

  private updateCurrencyFilterList(): void {
    const params = this.bspSelectedIds?.length ? { bspId: this.bspSelectedIds } : null;

    this.currencyDictionaryService
      .getFilteredDropdownOptions(params)
      .subscribe(currencyList => (this.currencyList = currencyList));
  }

  private updateCurrencyListControlValue(): void {
    const currenciesSelected: Currency[] = this.currencyControl.value;

    if (this.bspSelectedIds.length && currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyList.some(cur => cur.value.id === currency.id)
      );
      this.currencyControl.patchValue(currenciesToPatch);
    }
  }

  private updatePeriodControlDisability(): void {
    this.periodIsDisabled = this.isBspFilterMultiple && this.bspSelectedIds.length !== 1;
    if (this.periodIsDisabled) {
      this.periodControl.reset();
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private populateConfDropdowns(): void {
    if (!this.isAcdmd) {
      this.rtdnTypeList$ = this.formConfig.getAdmaFor();
    }
  }

  private initializeAirlineFilterDropdown(): void {
    this.airlineCodesList$ = this.airlineDictionaryService.getDropdownOptions();
  }

  private initializeAgentGroupFilterDropdown(): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = this.isSpcdr
      ? getSpcdrQueryPermission(this.admAcmType)
      : getAcdmdQueryPermission(this.admAcmType);
    const aGroupUserHasPermission = this.loggedUser.permissions.some(perm => aGroupUserPermission === perm);

    return aGroupUserHasPermission
      ? this.agentDictionaryService.getDropdownOptions({ permission: aGroupUserPermission })
      : of(null);
  }

  private updatePeriodFilterOptions(): void {
    if (this.bspSelectedIds.length === 1) {
      this.periodOptions$ = this.periodService.getByBsp(this.bspSelectedIds[0]);
    }
  }

  public onImageClick(event: { event: Event; row: AdmAcmIssueBE; column: GridColumn }): void {
    if (this.hasReadInternalCommentPermission) {
      const { row } = event;

      this.admAcmDialogService.openInternalCommentDialog(row, this.admAcmType);
    }
  }

  public onGetActionList(acdm: AdmAcmIssueBE & { markStatus: string }): void {
    this.listActions$ = this.admAcmService.getAcdmActionList(acdm.id);
  }

  public onActionClick({ action, row }): void {
    const actionType: GridTableActionType = action.actionType;
    const actionCommand = this.formConfig.getActionCommand(
      actionType,
      row,
      this.dialogQueryFormSubmitEmitter,
      this.getListKey()
    );

    actionCommand();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate(`ADM_ACM.query.${this.admAcmType}_downloadTitle`),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery,
        totalElements: this.downloadQuery.paginateBy.totalElements
      },
      apiService: this.admAcmService
    });
  }

  public onRowToggle(event): void {
    const acdmId: number = event?.value?.id;

    if (event?.type === 'row' && acdmId) {
      this.onSelectedItemChanged(acdmId);
    }
  }

  private initializeInternalCommentFilterDropdown(): void {
    this.hasInternalCommentList = [true, false].map(item => ({
      label: this.translationService.translate(`ADM_ACM.yesNoBooleanValue.${item}`),
      value: item
    }));
  }

  private initializeFilterVisibility(): void {
    this.isInternalCommentVisible = this.hasReadInternalCommentPermission;
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
