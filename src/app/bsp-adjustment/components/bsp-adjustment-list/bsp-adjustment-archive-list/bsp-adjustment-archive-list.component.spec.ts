import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { BspAdjustmentArchiveListComponent } from './bsp-adjustment-archive-list.component';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('BspAdjustmentArchiveListComponent', () => {
  let component: BspAdjustmentArchiveListComponent;
  let fixture: ComponentFixture<BspAdjustmentArchiveListComponent>;

  const admAcmServiceSpy = createSpyObject(AdmAcmService);

  const activatedRouteStub = {
    snapshot: {
      data: { admAcmType: MasterDataType.Spcr }
    }
  };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: ['rSpcr'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          'MENU.SPCDRS.QUERY.spcr': {},
          activeTabId: 'MENU.SPCDRS.QUERY.spcr'
        }
      },
      viewListsInfo: {}
    },
    acdm: { 'spcr-archive-list': fromListSubtabs.initialState }
  };
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BspAdjustmentArchiveListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({ initialState }),
        FormBuilder,
        PermissionsService,
        mockProvider(NotificationService),
        mockProvider(DialogService),
        mockProvider(ActivatedRoute),
        mockProvider(AgentDictionaryService),
        mockProvider(AirlineDictionaryService),
        mockProvider(CurrencyDictionaryService),
        mockProvider(PeriodService),
        mockProvider(ActivatedRoute, activatedRouteStub)
      ]
    })
      .overrideComponent(BspAdjustmentArchiveListComponent, {
        set: {
          providers: [
            AdmAcmFilterFormatter,
            AdmAcmDataService,
            mockProvider(AdmAcmConfigService),
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: AdmAcmService, useValue: admAcmServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(BspAdjustmentArchiveListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: include this test with maybe a ListSubtabs mock (`RangeError: Maximum call stack size exceeded`)
  xit('should create', () => {
    expect(component).toBeDefined();
  });
});
