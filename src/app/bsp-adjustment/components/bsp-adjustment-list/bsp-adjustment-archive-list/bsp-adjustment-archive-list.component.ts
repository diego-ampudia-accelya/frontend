import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { map } from 'rxjs/operators';

import { BspAdjustmentActiveListComponent } from '../bsp-adjustment-active-list/bsp-adjustment-active-list.component';
import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';
import { AdmAcmStatus } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { archivedKeys, archivedSelectors, State } from '~app/adm-acm/store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { AppState } from '~app/reducers';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus } from '~app/shared/models/mark-status.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService,
  TabService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-bsp-adjustment-archive-list',
  templateUrl: '../bsp-adjustment-active-list/bsp-adjustment-active-list.component.html',
  styleUrls: ['../bsp-adjustment-active-list/bsp-adjustment-active-list.component.scss'],
  providers: [
    AdmAcmFilterFormatter,
    AdmAcmService,
    AdmAcmDataService,
    {
      provide: AdmAcmConfigService,
      useFactory: createAdmAcmConfigService,
      deps: [AdmAcmDataService, AdmAcmDialogService, L10nTranslationService, HttpClient, Router, Store, TabService]
    }
  ]
})
export class BspAdjustmentArchiveListComponent extends BspAdjustmentActiveListComponent {
  public isArchivedSubTab = true;

  constructor(
    protected store: Store<AppState>,
    protected admAcmService: AdmAcmService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected notificationService: NotificationService,
    public displayFormatter: AdmAcmFilterFormatter,
    protected formConfig: AdmAcmConfigService,
    protected dialogService: DialogService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected fb: FormBuilder,
    protected agentDictionaryService: AgentDictionaryService,
    protected airlineDictionaryService: AirlineDictionaryService,
    protected currencyDictionaryService: CurrencyDictionaryService,
    protected periodService: PeriodService,
    protected permissionsService: PermissionsService,
    protected globalErrorHandlerService: ErrorHandler,
    protected admAcmDialogService: AdmAcmDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(
      store,
      admAcmService,
      actions$,
      translationService,
      notificationService,
      displayFormatter,
      router,
      activatedRoute,
      fb,
      formConfig,
      agentDictionaryService,
      airlineDictionaryService,
      currencyDictionaryService,
      periodService,
      dialogService,
      permissionsService,
      globalErrorHandlerService,
      admAcmDialogService,
      bspsDictionaryService
    );
  }
  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>,
    DefaultProjectorFn<ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>>
  > {
    return archivedSelectors[this.admAcmType];
  }

  protected getListKey(): string {
    return archivedKeys[this.admAcmType];
  }

  protected adaptSearchQuery(query: DataQuery<AdmAcmFilter>): DataQuery<AdmAcmFilter> {
    return {
      ...query,
      filterBy: {
        ...query.filterBy,
        // With "!" at the first status we are negating the following status for the API call
        acdmStatus: query.filterBy.acdmStatus || [`!${AdmAcmStatus.deleted}`],
        markStatus: [`${MarkStatus.Archive}`]
      }
    };
  }

  public onGetActionList({ id }) {
    this.listActions$ = this.admAcmService.getAcdmActionList(id).pipe(
      map(actions => {
        let modifiedActions: any = actions;

        const hasLoadingAction = modifiedActions.some(action => action.action === GridTableActionType.Loading);

        if (!hasLoadingAction && this.hasQueryPermission) {
          modifiedActions = [...modifiedActions, { action: GridTableActionType.MoveToActive, group: 'markStatus' }];
        }

        return this.groupActions(modifiedActions);
      })
    );
  }
}
