import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { urlCreate } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { getAcdmdCreationPermission } from '~app/bsp-adjustment/shared/helpers/acdmd-permissions.config';
import { getSpcdrCreationPermission } from '~app/bsp-adjustment/shared/helpers/spcdr-permissions.config';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { User } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-bsp-adjustment-list',
  templateUrl: './bsp-adjustment-list.component.html',
  styleUrls: ['./bsp-adjustment-list.component.scss']
})
export class BspAdjustmentListComponent implements OnInit {
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  public loggedUser: User;

  public tabs: RoutedMenuItem[];

  public admAcmType: MasterDataType;
  public isAcdmd: boolean; //* Indicates if it is an ACDMD (BSP Adjustment)
  public isSpcdr: boolean; //* Indicates if it is an SPCDR (BSP Adjustment)

  public title: string;
  public createBtnLbl: string;

  public btnDesign = ButtonDesign;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private menuBuilder: MenuBuilder
  ) {
    this.setConfig();
  }

  private setConfig() {
    const data = this.activatedRoute.snapshot.data;

    this.admAcmType = data.admAcmType;
    this.createBtnLbl = `ADM_ACM.query.${this.admAcmType}_issueButton`;
    this.title = `ADM_ACM.query.${this.admAcmType}_title`;
    this.isAcdmd = this.admAcmType === MasterDataType.Admd || this.admAcmType === MasterDataType.Acmd;
    this.isSpcdr = this.admAcmType === MasterDataType.Spdr || this.admAcmType === MasterDataType.Spcr;
  }

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.initializeTabs();
  }

  private initializeLoggedUser() {
    this.loggedUser$.subscribe(loggedUser => (this.loggedUser = loggedUser));
  }

  private initializeTabs() {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }

  public create() {
    this.router.navigate([urlCreate[this.admAcmType]]);
  }

  public canUserIssueBspAdjustment(): boolean {
    let userHasPermission = false;

    if (this.isAcdmd) {
      userHasPermission = !!this.loggedUser?.permissions.find(permission =>
        getAcdmdCreationPermission(this.admAcmType).includes(permission)
      );
    } else if (this.isSpcdr) {
      userHasPermission = !!this.loggedUser?.permissions.find(permission =>
        getSpcdrCreationPermission(this.admAcmType).includes(permission)
      );
    }

    return userHasPermission;
  }

  public loggedUserIsOfType(type): boolean {
    return type.toUpperCase() === this.loggedUser.userType.toUpperCase();
  }
}
