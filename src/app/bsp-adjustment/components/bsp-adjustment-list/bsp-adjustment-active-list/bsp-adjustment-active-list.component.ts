import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { mapValues } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';
import {
  AcdmActionType,
  AcdmOptionType,
  AdmAcmStatus,
  SendToDpcType,
  urlView
} from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import {
  getAcdmReadInternalCommentPermission,
  getBspFilterRequiredPermissions
} from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { activeKeys, activeSelectors, State } from '~app/adm-acm/store/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  SEARCH_FORM_DEFAULT_VALUE,
  SHARED
} from '~app/bsp-adjustment/components/bsp-adjustment-list/bsp-adjustment-list.constants';
import { getAcdmdQueryPermission } from '~app/bsp-adjustment/shared/helpers/acdmd-permissions.config';
import { getSpcdrQueryPermission } from '~app/bsp-adjustment/shared/helpers/spcdr-permissions.config';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ButtonDesign, DialogService, DownloadFileComponent, FooterButton, PeriodOption } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { ServerError } from '~app/shared/errors';
import { FormUtil, toValueLabelObject, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { Currency } from '~app/shared/models/currency.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus, MarkStatusFilter } from '~app/shared/models/mark-status.model';
import { User, UserType } from '~app/shared/models/user.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService,
  TabService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-bsp-adjustment-active-list',
  templateUrl: './bsp-adjustment-active-list.component.html',
  styleUrls: ['./bsp-adjustment-active-list.component.scss'],
  providers: [
    AdmAcmFilterFormatter,
    AdmAcmService,
    AdmAcmDataService,
    {
      provide: AdmAcmConfigService,
      useFactory: createAdmAcmConfigService,
      deps: [AdmAcmDataService, AdmAcmDialogService, L10nTranslationService, HttpClient, Router, Store, TabService]
    }
  ]
})
export class BspAdjustmentActiveListComponent
  extends ListSubtabs<AdmAcmIssueBE, AdmAcmFilter>
  implements OnInit, OnDestroy
{
  protected initialSortAttribute: keyof AdmAcmFilter = 'dateOfIssue';

  public admAcmType: MasterDataType;
  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)
  public isSpcdr = false; //* Indicates if it is an SPCDR (BSP Adjustment)
  public btnDesign = ButtonDesign;

  public listActions$: Observable<{ action: GridTableActionType; disabled?: boolean }[]>;
  public columns: Array<GridColumn> = [];
  public downloadQuery: Partial<DataQuery>;

  public tableTitle: string;
  public isArchivedSubTab = false;

  public isInternalCommentVisible = false;

  public searchForm: FormGroup;
  public airlineCodesList$: Observable<DropdownOption[]>;
  public agentCodesList$: Observable<DropdownOption[]>;
  public bspCountriesList: DropdownOption<BspDto>[];
  public currencyList: DropdownOption<Currency>[];
  public rtdnTypeList$: Observable<DropdownOption[]> = of([]);
  public periodOptions$: Observable<PeriodOption[]>;
  public statusList: DropdownOption[] = [];
  public sentToDpcList: DropdownOption[] = [];
  public markStatusList: DropdownOption[] = [];
  public hasInternalCommentList: DropdownOption<boolean>[] = [];

  public periodIsDisabled = true;

  private bspControl: AbstractControl;
  private agentListControl: AbstractControl;
  private periodControl: AbstractControl;
  private currencyControl: AbstractControl;

  private isMultiCountryUser: boolean;

  private bspSelectedIds: number[] = [];

  public userTypes = UserType;

  public loggedUser: User;

  public userType: UserType;

  public buttonMenuOptions$: Observable<ButtonMenuOption[]>;

  public hasQueryPermission = true;

  private hasReadInternalCommentPermission = false;

  private dialogQueryFormSubmitEmitter = new Subject<AcdmActionType>();

  private queryPermission: string;

  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;

  private hasLeanPermission: boolean;

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  public periodPickerYearsBack: number;
  constructor(
    protected store: Store<AppState>,
    protected admAcmService: AdmAcmService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected notificationService: NotificationService,
    public displayFormatter: AdmAcmFilterFormatter,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder,
    protected formConfig: AdmAcmConfigService,
    protected agentDictionaryService: AgentDictionaryService,
    protected airlineDictionaryService: AirlineDictionaryService,
    protected currencyDictionaryService: CurrencyDictionaryService,
    protected periodService: PeriodService,
    protected dialogService: DialogService,
    protected permissionsService: PermissionsService,
    protected globalErrorHandlerService: ErrorHandler,
    protected admAcmDialogService: AdmAcmDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(store, admAcmService, actions$, translationService, notificationService);
    this.setConfig();
  }

  private setConfig(): void {
    const { admAcmType } = this.activatedRoute.snapshot.data;

    this.admAcmType = admAcmType;
    this.formConfig.admAcmType = admAcmType;
    this.tableTitle = `ADM_ACM.query.${admAcmType}_listTitle`;
    this.isAcdmd = admAcmType === MasterDataType.Admd || admAcmType === MasterDataType.Acmd;
    this.isSpcdr = admAcmType === MasterDataType.Spdr || admAcmType === MasterDataType.Spcr;

    if (this.isSpcdr) {
      this.queryPermission = getSpcdrQueryPermission(admAcmType);
    } else if (this.isAcdmd) {
      this.queryPermission = getAcdmdQueryPermission(admAcmType);
    }
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>,
    DefaultProjectorFn<ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>>
  > {
    return activeSelectors[this.admAcmType];
  }

  protected getListKey(): string {
    return activeKeys[this.admAcmType];
  }

  protected adaptSearchQuery(query: DataQuery<AdmAcmFilter>): DataQuery<AdmAcmFilter> {
    return {
      ...query,
      filterBy: {
        ...query.filterBy,
        // With "!" at the first status we are negating the following status for the API call
        acdmStatus: query.filterBy.acdmStatus || [`!${AdmAcmStatus.deleted}`],
        markStatus: query.filterBy.markStatus || [`${MarkStatus.Read}, ${MarkStatus.Unread}`]
      }
    };
  }

  public ngOnInit(): void {
    this.setInitialFilterValues();

    // Initial loading view
    this.loading$ = of(true);
    this.initializeColumns();

    combineLatest([this.initializeLoggedUser$(), this.permissionsService.permissions$.pipe(first())])
      .pipe(
        tap(() => this.initializePermissions()),
        switchMap(() => this.initializeLeanBspFilter$()),
        tap(() => super.ngOnInit()),
        tap(() => this.setResolversData()),
        tap(() => this.initializeColumns()),
        tap(() => this.updatePeriodControlDisability()),
        tap(() => this.updatePeriodFilterOptions()),
        tap(() => this.setPeriodPickerYearsBack()),
        takeUntil(this.destroy$)
      )
      .subscribe();

    this.populateButtonMenuOptions();
    this.isInternalCommentVisible = this.hasReadInternalCommentPermission;
  }

  private setResolversData(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.downloadQuery = this.adaptSearchQuery(query)));
    this.dialogQueryFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.onQueryChanged());
  }

  private initializePermissions(): void {
    this.hasQueryPermission = this.permissionsService.hasPermission(this.queryPermission);

    // Internal comment permissions
    this.hasReadInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmReadInternalCommentPermission(this.admAcmType)
    );

    //Lean permissions
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeColumns(): void {
    this.columns = [...SHARED.ACTIVE_COLUMNS];

    if (this.hasQueryPermission) {
      this.columns = [this.getCheckboxColumn(), ...this.columns];
    }

    if (this.isAcdmd) {
      this.columns = this.columns.filter(column => column.prop !== 'concernsIndicator');
    }

    if (this.userType === UserType.AGENT) {
      this.columns = this.columns.filter(
        column => column.prop !== 'agent.bsp.name' && column.prop !== 'agent.iataCode'
      );
    }

    if (this.userType === UserType.AIRLINE) {
      this.columns = this.columns.filter(column => column.prop !== 'airline.iataCode');
    }

    this.initializeStatusBadgeInfo();

    this.setDocumentNumberColumn();

    this.setBspColumn();
  }

  private initializeStatusBadgeInfo(): void {
    const statusColumn = this.columns.find(col => col.prop === 'acdmStatus');
    if (statusColumn) {
      statusColumn.pipe = { transform: value => this.translationService.translate(`ADM_ACM.status.${value}`) };

      statusColumn.badgeInfo = {
        hidden: (value: AdmAcmIssueBE) => !value.sentToDpc,
        value: this.translationService.translate(`ADM_ACM.dpcBadgeInfo.label`),
        type: (value: AdmAcmIssueBE) => {
          switch (value.sentToDpc) {
            case SendToDpcType.pending:
              return BadgeInfoType.info;

            case SendToDpcType.sent:
              return BadgeInfoType.success;

            case SendToDpcType.n_a:
            default:
              return BadgeInfoType.regular;
          }
        },
        tooltipLabel: (value: AdmAcmIssueBE) =>
          value.sentToDpc
            ? this.translationService.translate(`ADM_ACM.dpcBadgeInfo.leyend.${value.sentToDpc.toUpperCase()}`, {
                type: this.translationService.translate(`ADM_ACM.type.${this.admAcmType}`)
              })
            : '',
        showIconType: true
      };
    }
  }

  private getCheckboxColumn(): GridColumn {
    return {
      prop: 'isRowSelected',
      maxWidth: 40,
      sortable: false,
      name: '',
      headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl'
    };
  }

  private setBspColumn(): void {
    const bspColumn = this.columns.find(column => column.prop === 'agent.bsp.isoCountryCode');

    if (bspColumn) {
      bspColumn.sortable = this.hasLeanPermission;
    }
  }

  private initializeLoggedUser$(): Observable<any> {
    return this.store.pipe(select(getUser), first()).pipe(
      tap(user => {
        this.loggedUser = user;
        this.userType = user.userType;
        this.isMultiCountryUser = user.bsps.length > 1;
      })
    );
  }
  private setInitialFilterValues(): void {
    this.searchForm = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));

    this.bspControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'bsp');
    this.agentListControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'agentId');
    this.periodControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'period');
    this.currencyControl = FormUtil.get<AdmAcmFilter>(this.searchForm, 'currencyId');

    if (this.isAcdmd) {
      this.searchForm.get('concernsIndicator').disable();
    }
  }

  public onFilterClick(isFilterShown: boolean): void {
    if (isFilterShown) {
      this.populateConfDropdowns();
      this.initializeAirlineFilterDropdown();
      this.initializeAgentFilterDropdown();
      this.updateCurrencyFilterList();
      this.initializeBspCountrieslistener();
    }
  }

  private initializeBspCountrieslistener() {
    this.bspControl.valueChanges
      .pipe(
        tap(bsps => this.updateBspSelectedIds(bsps)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.updateAgentList();
        this.updateCurrencyList();
        this.updatePeriodControlDisability();
        this.updatePeriodFilterOptions();
      });
  }

  private updateBspSelectedIds(bsps: Bsp): void {
    if (bsps) {
      this.bspSelectedIds = Array.isArray(bsps) ? bsps.map(bsp => bsp.id) : [bsps.id];
    } else {
      this.bspSelectedIds = [];
    }
  }

  private updateAgentList(): void {
    if (this.isMultiCountryUser) {
      this.updateAgentFilterList();
      this.updateAgentListControlValue();
    }
  }

  private updateAgentFilterList(): void {
    let param;

    if (this.bspSelectedIds.length) {
      param = { bspId: this.bspSelectedIds };
    }
    this.agentCodesList$ = this.agentDictionaryService.getDropdownOptions(param);
  }

  private updateAgentListControlValue(): void {
    const agentsSelected = this.agentListControl.value;
    if (this.bspSelectedIds.length && agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.bspSelectedIds.some(bspId => agent.bsp?.id === bspId));
      this.agentListControl.patchValue(agentsToPatch);
    }
  }

  private updateCurrencyList(): void {
    if (this.isMultiCountryUser) {
      this.updateCurrencyFilterList();
      this.updateCurrencyListControlValue();
    }
  }

  private updateCurrencyFilterList(): void {
    let param;

    if (this.bspSelectedIds.length) {
      param = { bspId: this.bspSelectedIds };
    }
    this.currencyDictionaryService
      .getFilteredDropdownOptions(param)
      .subscribe(currencyList => (this.currencyList = currencyList));
  }

  private updateCurrencyListControlValue(): void {
    const currenciesSelected = this.currencyControl.value;
    if (this.bspSelectedIds.length && currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyList.some(cur => cur.value.id === currency.id)
      );
      this.currencyControl.patchValue(currenciesToPatch);
    }
  }

  private updatePeriodControlDisability(): void {
    this.periodIsDisabled = this.isBspFilterMultiple && this.bspSelectedIds.length !== 1;
    if (this.periodIsDisabled) {
      this.periodControl.reset();
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private populateConfDropdowns() {
    if (!this.isAcdmd) {
      this.rtdnTypeList$ = this.formConfig.getAdmaFor();
    }

    this.statusList = Object.values(AdmAcmStatus)
      .filter(
        status =>
          status !== AdmAcmStatus.deleted &&
          status !== AdmAcmStatus.supervisionDeleted &&
          status !== AdmAcmStatus.pendingAuthorization
      )
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.translationService.translate(`ADM_ACM.status.${label}`)
      }));

    this.sentToDpcList = Object.values(SendToDpcType)
      .map(toValueLabelObject)
      .filter(sentStatus => sentStatus.value !== SendToDpcType.na);

    this.sentToDpcList.forEach(
      sentStatus =>
        (sentStatus.label = this.translationService.translate(`ADM_ACM.sentToDpcStatus.${sentStatus.label}`))
    );

    this.initializeMarkStatusDropdown();

    if (this.isInternalCommentVisible) {
      this.hasInternalCommentList = [true, false].map(item => ({
        label: this.translationService.translate(`ADM_ACM.yesNoBooleanValue.${item}`),
        value: item
      }));
    }
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, getBspFilterRequiredPermissions(this.admAcmType))
      .pipe(
        tap(bspList => {
          // Update BSP selected ids and predefined filters with the default BSP
          const onlyOneBsp = bspList.length === 1;

          if (bspList.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);

            const firstListBsp = bspList[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelectedIds = [filterValue.id];

            // Agent users have not predefined filters
            if (this.userType !== UserType.AGENT) {
              this.predefinedFilters = {
                ...this.predefinedFilters,
                bsp: filterValue
              };
            }
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspOptions => (this.isBspFilterMultiple = this.hasLeanPermission && bspOptions.length !== 1)),
        tap(bspOptions => (this.isBspFilterLocked = bspOptions.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  private initializeAirlineFilterDropdown() {
    this.airlineCodesList$ =
      this.loggedUser.userType !== UserType.AIRLINE ? this.airlineDictionaryService.getDropdownOptions() : of(null);
  }

  private initializeAgentFilterDropdown() {
    switch (this.loggedUser.userType) {
      case UserType.AGENT:
        this.agentCodesList$ = of(null);
        break;
      case UserType.AGENT_GROUP:
        this.agentCodesList$ = this.initializeAgentGroupFilterDropdown(this.loggedUser);
        break;
      default:
        this.updateAgentFilterList();
    }
  }

  private initializeAgentGroupFilterDropdown(user): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = this.isSpcdr
      ? getSpcdrQueryPermission(this.admAcmType)
      : getAcdmdQueryPermission(this.admAcmType);
    const aGroupUserHasPermission = user.permissions.some(perm => aGroupUserPermission === perm);

    return aGroupUserHasPermission
      ? this.agentDictionaryService.getDropdownOptions({ permission: aGroupUserPermission })
      : of(null);
  }

  private updatePeriodFilterOptions(): void {
    if (this.bspSelectedIds.length === 1) {
      this.periodOptions$ = this.periodService.getByBsp(this.bspSelectedIds[0]);
    }
  }

  private initializeMarkStatusDropdown() {
    this.markStatusList = Object.values(MarkStatusFilter)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.getTranslatedMarkStatus(label)
      }));
  }

  private getTranslatedMarkStatus(status: MarkStatusFilter): string {
    return this.translationService.translate(`ADM_ACM.markStatus.${status}`);
  }

  protected populateButtonMenuOptions(): void {
    this.buttonMenuOptions$ = this.admAcmService.getAcdmMenuOptions(this.isArchivedSubTab).pipe(
      map(options =>
        options.map(option => ({
          ...option,
          label: this.getTranslatedButtonMenuOptionLabel(option),
          group: this.setButtonMenuOptionGroup(option),
          command: this.setButtonMenuOptionCommand(option)
        }))
      )
    );
  }

  private getTranslatedButtonMenuOptionLabel(option: ButtonMenuOption): string {
    const pathTranslation = this.isArchivedSubTab
      ? `ADM_ACM.query.tableSelectionToolbar.buttonMenuOptionsArchive.${option.label}`
      : `ADM_ACM.query.tableSelectionToolbar.buttonMenuOptions.${option.label}`;

    return this.translationService.translate(pathTranslation);
  }

  private setButtonMenuOptionGroup(option: ButtonMenuOption): string {
    let group: string;

    switch (option.id) {
      case AcdmOptionType.Read:
      case AcdmOptionType.Unread:
      case AcdmOptionType.Archive:
        group = 'markAs';
        break;
      case AcdmOptionType.DownloadAttachments:
        group = 'download';
        break;
    }

    return group;
  }

  private setButtonMenuOptionCommand(option: ButtonMenuOption): () => void {
    let command: () => void;

    switch (option.id) {
      case AcdmOptionType.Read:
        if (this.isArchivedSubTab) {
          command = () => this.moveToActive();
        } else {
          command = () => this.setMarkStatus(MarkStatus.Read);
        }
        break;
      case AcdmOptionType.Unread:
        command = () => this.setMarkStatus(MarkStatus.Unread);
        break;
      case AcdmOptionType.Archive:
        command = () => this.markAsArchive();
        break;
      case AcdmOptionType.DownloadAttachments:
        command = () => this.bulkDownloadAttachments();
        break;
    }

    return command;
  }

  private bulkDownloadAttachments(): void {
    this.selectedRows$
      .pipe(
        first(),
        switchMap(rows => combineLatest([of(rows), this.admAcmService.bulkDownloadAttachments(rows)]))
      )
      .subscribe(
        ([rows, file]) => this.onDownloadSuccess(rows, file),
        error => this.onDownloadError(error)
      );
  }

  private onDownloadSuccess(rows: AdmAcmIssueBE[], file: { blob: Blob; fileName: string }): void {
    FileSaver.saveAs(file.blob, file.fileName);
    this.notificationService.showSuccess(
      this.translationService.translate('common.successDownloadAttachments', {
        items: rows.length
      })
    );
  }

  private onDownloadError(error: ServerError): void {
    if (error.status === 404) {
      this.notificationService.showError(this.translationService.translate('common.errorDownloadAttachments'));
    } else {
      this.globalErrorHandlerService.handleError(error);
    }
  }

  private download(detailed?: boolean) {
    const modalTitle = detailed
      ? `ADM_ACM.query.${this.admAcmType}_detailedDownloadTitle`
      : `ADM_ACM.query.${this.admAcmType}_downloadTitle`;

    const data = {
      title: this.translationService.translate(modalTitle),
      footerButtonsType: FooterButton.Download,
      downloadQuery: this.downloadQuery,
      totalElements: this.downloadQuery.paginateBy.totalElements,
      detailedDownload: detailed
    };

    this.dialogService.open(DownloadFileComponent, {
      data,
      apiService: this.admAcmService
    });
  }

  private setDocumentNumberColumn(): void {
    const documentNumberColumn = this.columns.find(column => column.prop === 'ticketDocumentNumber');

    if (documentNumberColumn) {
      if (this.hasReadInternalCommentPermission) {
        documentNumberColumn.image = (acdm: AdmAcmIssueBE) =>
          acdm.internalComment
            ? {
                src: '/assets/images/utils/discuss.svg',
                tooltipLabel: this.translationService.translate(`ADM_ACM.query.internalCommentTooltip`)
              }
            : null;
      }

      documentNumberColumn.icon = (acdm: AdmAcmIssueBE) => ({
        class: 'material-icons-outlined',
        name: 'remove_red_eye',
        tooltip: this.translationService.translate(`watching`),
        hidden: !acdm.watching
      });
    }
  }

  public onDownload(): void {
    this.download();
  }

  public onDetailedDownload(): void {
    this.download(true);
  }

  public onGetActionList({ id, markStatus, watching }) {
    this.listActions$ = this.admAcmService.getAcdmActionList(id).pipe(
      map(actions => {
        let modifiedActions: any = actions;

        const hasLoadingAction = actions.some(action => action.action === GridTableActionType.Loading);

        if (!hasLoadingAction && this.hasQueryPermission) {
          const readUnreadAction =
            markStatus === MarkStatus.Read ? GridTableActionType.MarkAsUnread : GridTableActionType.MarkAsRead;

          const watchingAction = watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;

          modifiedActions = [
            ...actions,
            { action: readUnreadAction, group: 'markStatus' },
            { action: GridTableActionType.Archive, group: 'markStatus' },
            { action: watchingAction, group: 'markStatus' }
          ];
        }

        return this.groupActions(modifiedActions);
      })
    );
  }

  /** `protected` since it's used in adm-acm-archived-list (child) */
  protected groupActions(
    actions: { action: GridTableActionType; disabled?: boolean; group?: string }[]
  ): { action: GridTableActionType; disabled?: boolean; group?: string }[] {
    // For now, we just need to group `internal comment` action
    return actions.map(action =>
      action.action === GridTableActionType.InternalComment ? { ...action, group: 'comment' } : action
    );
  }

  public onActionClick(event) {
    const actionType: GridTableActionType = event.action.actionType;
    const actionCommand = this.formConfig.getActionCommand(
      actionType,
      event.row,
      this.dialogQueryFormSubmitEmitter,
      this.getListKey()
    );

    actionCommand();
  }

  public onImageClick(event: { event: Event; row: AdmAcmIssueBE; column: GridColumn }): void {
    if (this.hasReadInternalCommentPermission) {
      const { row } = event;

      this.admAcmDialogService.openInternalCommentDialog(row, this.admAcmType);
    }
  }

  public onViewAdm(id) {
    const url = urlView[this.admAcmType];

    this.router.navigate([url, id]);
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
