import { ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { BspAdjustmentActiveListComponent } from './bsp-adjustment-active-list.component';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('BspAdjustmentActiveListComponent', () => {
  let component: BspAdjustmentActiveListComponent;
  let fixture: ComponentFixture<BspAdjustmentActiveListComponent>;

  const admAcmServiceSpy = createSpyObject(AdmAcmService);
  const admAcmConfigServiceSpy = createSpyObject(AdmAcmConfigService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const currencyDictionaryServiceSpy = createSpyObject(CurrencyDictionaryService);
  const periodServiceSpy = createSpyObject(PeriodService);
  const errorHandlerSpy = createSpyObject(ErrorHandler);
  const bspsDictionaryService = createSpyObject(BspsDictionaryService);

  admAcmServiceSpy.getAcdmMenuOptions.and.returnValue(of([]));

  const activatedRouteStub = {
    snapshot: {
      data: { admAcmType: MasterDataType.Spdr }
    }
  };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: ['rSpdr', 'rLean'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          'MENU.SPCDRS.QUERY.spdr': {},
          activeTabId: 'MENU.SPCDRS.QUERY.spdr'
        }
      },
      viewListsInfo: {}
    },
    acdm: { 'spdr-active-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BspAdjustmentActiveListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromAuth.getUser, value: expectedUserDetails },
            { selector: fromAuth.getUserType, value: expectedUserDetails.userType },
            { selector: fromAuth.getUserBsps, value: expectedUserDetails.bsps }
          ]
        }),
        FormBuilder,
        PermissionsService,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionaryServiceSpy },
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: ErrorHandler, useValue: errorHandlerSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryService }
      ]
    })
      .overrideComponent(BspAdjustmentActiveListComponent, {
        set: {
          providers: [
            AdmAcmFilterFormatter,
            AdmAcmDataService,
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: AdmAcmService, useValue: admAcmServiceSpy },
            { provide: AdmAcmConfigService, useValue: admAcmConfigServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    bspsDictionaryService.getAllBspsByPermissions.and.returnValue(of([]));

    fixture = TestBed.createComponent(BspAdjustmentActiveListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('No LEAN USER - should initialize columns without checkbox as AirlineUser', () => {
    component.hasQueryPermission = false;
    component.isBspFilterMultiple = false;
    const columnsExpected: any[] = [
      'agent.bsp.isoCountryCode',
      'ticketDocumentNumber',
      'agent.iataCode',
      'acdmStatus',
      'currency.code',
      'totalAmount',
      'dateOfIssue',
      'period',
      'reportingDate'
    ];
    component['initializeColumns']();

    expect(component.columns.map(col => col.prop)).toEqual(columnsExpected);
  });

  it('LEAN USER - should initialize columns with checkbox as AirlineUser', () => {
    component.hasQueryPermission = true;
    component.isBspFilterMultiple = true;

    const columnsExpected: any[] = [
      'isRowSelected',
      'agent.bsp.isoCountryCode',
      'ticketDocumentNumber',
      'agent.iataCode',
      'acdmStatus',
      'currency.code',
      'totalAmount',
      'dateOfIssue',
      'period',
      'reportingDate'
    ];

    component['initializeColumns']();

    expect(component.columns.map(col => col.prop)).toEqual(columnsExpected);
  });
});
