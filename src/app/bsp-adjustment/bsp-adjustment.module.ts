import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BspAdjustmentRoutingModule } from './bsp-adjustment-routing.module';
import { BspAdjustmentActiveListComponent } from './components/bsp-adjustment-list/bsp-adjustment-active-list/bsp-adjustment-active-list.component';
import { BspAdjustmentArchiveListComponent } from './components/bsp-adjustment-list/bsp-adjustment-archive-list/bsp-adjustment-archive-list.component';
import { BspAdjustmentDeletedListComponent } from './components/bsp-adjustment-list/bsp-adjustment-deleted-list/bsp-adjustment-deleted-list.component';
import { BspAdjustmentListComponent } from './components/bsp-adjustment-list/bsp-adjustment-list.component';
import { SharedModule } from '~app/shared/shared.module';
import { AdmAcmModule } from '~app/adm-acm/adm-acm.module';

@NgModule({
  declarations: [
    BspAdjustmentListComponent,
    BspAdjustmentActiveListComponent,
    BspAdjustmentDeletedListComponent,
    BspAdjustmentArchiveListComponent
  ],
  imports: [CommonModule, BspAdjustmentRoutingModule, SharedModule, AdmAcmModule]
})
export class BspAdjustmentModule {}
