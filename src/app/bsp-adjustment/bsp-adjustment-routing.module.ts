import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BspAdjustmentActiveListComponent } from './components/bsp-adjustment-list/bsp-adjustment-active-list/bsp-adjustment-active-list.component';
import { BspAdjustmentArchiveListComponent } from './components/bsp-adjustment-list/bsp-adjustment-archive-list/bsp-adjustment-archive-list.component';
import { BspAdjustmentDeletedListComponent } from './components/bsp-adjustment-list/bsp-adjustment-deleted-list/bsp-adjustment-deleted-list.component';
import { BspAdjustmentListComponent } from './components/bsp-adjustment-list/bsp-adjustment-list.component';
import { AdmAcmViewComponent } from '~app/adm-acm/components/adm-acm-view/adm-acm-view.component';
import { AdmAcmComponent } from '~app/adm-acm/components/adm-acm/adm-acm.component';
import { AdmAcmItemResolver } from '~app/adm-acm/resolvers/adm-acm-item-resolver.service';
import { getACDMTabLabel } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ROUTES } from '~app/shared/constants/routes';
import { AccessType } from '~app/shared/enums';

const getListSubtabsChildren = (acdmType: MasterDataType): Routes => [
  {
    path: 'active',
    component: BspAdjustmentActiveListComponent,
    data: {
      title: 'ADM_ACM.query.tabs.activeTitle',
      admAcmType: acdmType
    }
  },
  {
    path: 'archive',
    component: BspAdjustmentArchiveListComponent,
    data: {
      title: 'ADM_ACM.query.tabs.archiveTitle',
      admAcmType: acdmType
    }
  },
  {
    path: 'deleted',
    component: BspAdjustmentDeletedListComponent,
    data: {
      title: 'ADM_ACM.query.tabs.deletedTitle',
      admAcmType: acdmType
    }
  },
  { path: '', redirectTo: 'active', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: ROUTES.SPDR_QUERY.path,
    component: BspAdjustmentListComponent,
    data: {
      tab: ROUTES.SPDR_QUERY,
      admAcmType: MasterDataType.Spdr
    },
    children: getListSubtabsChildren(MasterDataType.Spdr)
  },
  {
    path: ROUTES.SPCR_QUERY.path,
    component: BspAdjustmentListComponent,
    data: {
      tab: ROUTES.SPCR_QUERY,
      admAcmType: MasterDataType.Spcr
    },
    children: getListSubtabsChildren(MasterDataType.Spcr)
  },
  {
    path: ROUTES.SPDR_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.SPDR_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Spdr
    }
  },
  {
    path: ROUTES.SPCR_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.SPCR_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Spcr
    }
  },
  {
    path: ROUTES.SPDR_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.SPDR_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Spdr
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.SPCR_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.SPCR_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Spcr
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ADMD_QUERY.path,
    component: BspAdjustmentListComponent,
    data: {
      tab: ROUTES.ADMD_QUERY,
      admAcmType: MasterDataType.Admd
    },
    children: getListSubtabsChildren(MasterDataType.Admd)
  },
  {
    path: ROUTES.ACMD_QUERY.path,
    component: BspAdjustmentListComponent,
    data: {
      tab: ROUTES.ACMD_QUERY,
      admAcmType: MasterDataType.Acmd
    },
    children: getListSubtabsChildren(MasterDataType.Acmd)
  },
  {
    path: ROUTES.ADMD_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ADMD_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Admd
    }
  },
  {
    path: ROUTES.ACMD_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ACMD_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Acmd
    }
  },
  {
    path: ROUTES.ADMD_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ADMD_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Admd
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  },
  {
    path: ROUTES.ACMD_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ACMD_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Acmd
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BspAdjustmentRoutingModule {}
