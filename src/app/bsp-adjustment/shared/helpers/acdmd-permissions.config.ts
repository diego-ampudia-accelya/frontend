import { MasterDataType } from '~app/master-shared/models/master.model';

export const creationAdmdPermission = {
  createAdmdAgn: 'cAdmdToAgn',
  createAdmdAirl: 'cAdmdToAirl'
};

export const creationAcmdPermission = {
  createAcmdAgn: 'cAcmdToAgn',
  createAcmdAirl: 'cAcmdToAirl'
};

export const queryAcdmdPermission = {
  readAdmd: 'rAdmd',
  readAcmd: 'rAcmd'
};

export function getAcdmdCreationAirlPermission(acdmdType: MasterDataType): string {
  return acdmdType === MasterDataType.Admd
    ? creationAdmdPermission.createAdmdAgn
    : creationAcmdPermission.createAcmdAgn;
}

export function getAcdmdCreationAgentPermission(acdmdType: MasterDataType): string {
  return acdmdType === MasterDataType.Admd
    ? creationAdmdPermission.createAdmdAirl
    : creationAcmdPermission.createAcmdAirl;
}

export function getAcdmdCreationPermission(acdmdType: MasterDataType): Array<string> {
  const permissions = acdmdType === MasterDataType.Admd ? { ...creationAdmdPermission } : { ...creationAcmdPermission };

  return Object.values(permissions);
}

export function getAcdmdQueryPermission(acdmdType: MasterDataType): string {
  return acdmdType === MasterDataType.Admd ? queryAcdmdPermission.readAdmd : queryAcdmdPermission.readAcmd;
}

export function getAllAcmdQueryPermissions(): Array<string> {
  const permissions = {
    ...creationAdmdPermission,
    readAdmd: queryAcdmdPermission.readAdmd
  };

  return Object.values(permissions);
}

export function getAllAdmdQueryPermissions(): Array<string> {
  const permissions = {
    ...creationAcmdPermission,
    readAcmd: queryAcdmdPermission.readAcmd
  };

  return Object.values(permissions);
}
