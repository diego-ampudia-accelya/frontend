import { MasterDataType } from '~app/master-shared/models/master.model';

export const creationSpdrPermission = {
  createSpdrAgn: 'cSpdrToAgn',
  createSpdrAirl: 'cSpdrToAirl'
};

export const creationSpcrPermission = {
  createSpcrAgn: 'cSpcrToAgn',
  createSpcrAirl: 'cSpcrToAirl'
};

export const querySpcdrPermission = {
  readSpdr: 'rSpdr',
  readSpcr: 'rSpcr'
};

export function getSpcdrCreationAirlPermission(acdmdType: MasterDataType): string {
  return acdmdType === MasterDataType.Spdr
    ? creationSpdrPermission.createSpdrAgn
    : creationSpcrPermission.createSpcrAgn;
}

export function getSpcdrCreationAgentPermission(acdmdType: MasterDataType): string {
  return acdmdType === MasterDataType.Spdr
    ? creationSpdrPermission.createSpdrAirl
    : creationSpcrPermission.createSpcrAirl;
}

export function getSpcdrCreationPermission(spcdrType: MasterDataType): Array<string> {
  const permissions = spcdrType === MasterDataType.Spdr ? { ...creationSpdrPermission } : { ...creationSpcrPermission };

  return Object.values(permissions);
}

export function getSpcdrQueryPermission(spcdrType: MasterDataType): string {
  return spcdrType === MasterDataType.Spdr ? querySpcdrPermission.readSpdr : querySpcdrPermission.readSpcr;
}

export function getAllSpdrQueryPermissions(): Array<string> {
  const permissions = {
    ...creationSpdrPermission,
    readSpdr: querySpcdrPermission.readSpdr
  };

  return Object.values(permissions);
}

export function getAllSpcrQueryPermissions(): Array<string> {
  const permissions = {
    ...creationSpdrPermission,
    readSpcr: querySpcdrPermission.readSpcr
  };

  return Object.values(permissions);
}
