import { DoughnutDataValue } from '~app/shared/components/chart/models/doughnut-chart.model';

export interface GeneralKpi {
  name: string;
  description: string;
  value: number;
  originalKey?: SubtabKpiName;
  listId?: string;
  permission?: string;
  seeAllVisible?: boolean;
}

export type SubtabKpiName = keyof (AcdmKpi & RefundKpi);

export interface AcdmKpi {
  totalDisputed: number;
  totalUrgentDisputed: number;
}

export interface RefundKpi {
  totalPending: number;
  totalFinalPending: number;
  totalUrgentFinalPending: number;
  totalResubmitted: number;
}

interface StatCommon {
  title: string;
  agregator: string;
  total: number;
  listId?: string;
}

export interface Stat extends StatCommon {
  data: Array<{ name: string; value: number }>;
}

export interface StatDoughnutChart extends StatCommon {
  data: Array<DoughnutDataValue>;
}

export interface Statistic {
  stats: Stat;
  kpis: Array<GeneralKpi>;
}

export interface StatisticKpi {
  kpis: Array<GeneralKpi>;
}

export interface IataMessageNews {
  data: Array<{ message: string }>;
}

export interface DocumentMetrics {
  /** Format: YYYY-MM */
  yearMonth: string;
  totals: Array<{ status: string; total: number }>;
}

export interface TopAgentMetrics {
  agentCode: string;
}

export interface GrossSalesAirlineIndustryMetrics {
  /** Format: YYYY-MM */
  yearMonth: string;
  airlineTotalAmount: number;
  industryTotalAmount: number;
}

export interface SearchMock {
  title: string;
  date: Date;
  tags: Array<string>;
}

export interface NavigationData {
  viewListId: string;
  url: string;
  filter: any;
  value: any;
}

export interface QuickLink {
  title: string;
  navigationData: NavigationData;
}

export interface ExternalNavigationLink {
  url: string;
  linkText: string;
  message: string;
}
