import { DocumentMetrics, GrossSalesAirlineIndustryMetrics, TopAgentMetrics } from '../models/dashboard.model';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';

/** Transforms document metrics from BE to data accepted by bar charts */
export const mapDocumentMetrics = (metrics: DocumentMetrics[]): BarChartData =>
  metrics.reduce(
    (dataAcc, metric, metricIndex) => {
      const labels = [...dataAcc.labels, metric.yearMonth];
      const values = metric.totals.reduce((valuesAcc, metricTotal) => {
        const { status, total } = metricTotal;

        const valueIndex = valuesAcc.findIndex(({ key }) => key === status);

        if (valueIndex < 0) {
          // `totals` array needs to be filled with 0's to represent the previous `yearMonth` values for this new `status`
          valuesAcc = [...valuesAcc, { key: status, totals: [...Array(metricIndex).fill(0), total] }];
        } else {
          const diff = metricIndex - valuesAcc[valueIndex].totals.length;

          // `totals` array needs to be filled here as well with 0's to represent the previous `yearMonth` values for this existing `status`
          valuesAcc[valueIndex].totals = [...valuesAcc[valueIndex].totals, ...Array(diff).fill(0), total];
        }

        return valuesAcc;
      }, dataAcc.values);

      return { labels, values };
    },
    { labels: [], values: [] } as BarChartData
  );

/** Transforms top agent metrics from BE to data accepted by bar charts */
export const mapTopAgentMetrics = <T extends TopAgentMetrics>(metrics: T[]): BarChartData =>
  metrics.reduce(
    (dataAcc, metric) => {
      // Restrict metric object to exactly two properties
      const [agentCode, total] = Object.values(metric);

      dataAcc.labels.push(agentCode);

      if (dataAcc.values.length) {
        dataAcc.values[0].totals.push(total);
      } else {
        dataAcc.values = [{ key: 'total', totals: [total] }];
      }

      return dataAcc;
    },
    { labels: [], values: [] } as BarChartData
  );

/** Transforms gross sales airline industry metrics from BE to data accepted by bar charts */
export const mapGrossSalesMetrics = (metrics: GrossSalesAirlineIndustryMetrics[]): BarChartData =>
  metrics.reduce(
    (dataAcc, metric) => {
      const { yearMonth, airlineTotalAmount, industryTotalAmount } = metric;

      dataAcc.labels.push(yearMonth);

      if (dataAcc.values.length) {
        dataAcc.values[0].totals.push(airlineTotalAmount);
        dataAcc.values[1].totals.push(industryTotalAmount);
      } else {
        dataAcc.values = [
          { key: 'airlineAmount', totals: [airlineTotalAmount] },
          { key: 'industryAmount', totals: [industryTotalAmount] }
        ];
      }

      return dataAcc;
    },
    { labels: [], values: [] } as BarChartData
  );
