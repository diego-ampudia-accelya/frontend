import { GeneralKpi, Statistic } from '../models/dashboard.model';

/** Transforms a BE response composed of an Array of Statistics and Kpis to and array of valid Kpis */
export const mapToKpi = (responsesArray: Array<Statistic | GeneralKpi>): GeneralKpi[] =>
  responsesArray
    .filter(Boolean)
    .map(response => {
      const stat = response as Statistic;
      let kpi = response as GeneralKpi;

      if (stat.kpis) {
        kpi = stat.kpis[0];
      }

      return kpi;
    })
    .filter(kpi => kpi != null);
