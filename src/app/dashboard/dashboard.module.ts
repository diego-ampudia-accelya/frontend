import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { AcdmDashboardComponent } from './components/adma-dashboard/acdm-dashboard.component';
import { AcdmDashboardFilterFormatter } from './components/adma-dashboard/services/acdm-dashboard-filter-formatter.service';
import { DashboardsComponent } from './components/dashboards/dashboards.component';
import { RefundDashboardComponent } from './components/refund-dashboard/refund-dashboard.component';
import { RefundDashboardFilterFormatter } from './components/refund-dashboard/services/refund-dashboard-filter-formatter.service';
import { SalesDashboardComponent } from './components/sales-dashboard/sales-dashboard.component';
import { SalesDashboardFilterFormatter } from './components/sales-dashboard/services/sales-dashboard-filter-formatter.service';
import { SalesDashboardPowerBiService } from './components/sales-dashboard/services/sales-dashboard-power-bi.service';
import { DashboardNavigationService } from './services/dashboard-navigation.service';
import * as fromDashboard from './store';
import { WelcomeDialogComponent } from './welcome-dialog/welcome-dialog.component';
import { SharedModule } from '~app/shared/shared.module';
import { EditDialogService } from '~app/shared/components/list-view/edit-dialog.service';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { DashboardRoutingModule } from '~app/dashboard/dashboard-routing.module';
import { GeneralDashboardComponent } from '~app/dashboard/components/general-dashboard/general-dashboard.component';

@NgModule({
  declarations: [
    DashboardsComponent,
    GeneralDashboardComponent,
    WelcomeDialogComponent,
    AcdmDashboardComponent,
    RefundDashboardComponent,
    SalesDashboardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    StoreModule.forFeature(fromDashboard.dashboardFeatureKey, fromDashboard.reducers)
  ],
  providers: [
    EditDialogService,
    DashboardService,
    DashboardNavigationService,
    AcdmDashboardFilterFormatter,
    RefundDashboardFilterFormatter,
    SalesDashboardFilterFormatter,
    SalesDashboardPowerBiService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {}
