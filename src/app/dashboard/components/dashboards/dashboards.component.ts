import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';

@Component({
  selector: 'bspl-dashboards',
  templateUrl: './dashboards.component.html',
  styleUrls: ['./dashboards.component.scss']
})
export class DashboardsComponent implements OnInit {
  public tabs: RoutedMenuItem[];

  constructor(private menuBuilder: MenuBuilder, private activatedRoute: ActivatedRoute) {}

  public ngOnInit(): void {
    this.initializeTabs();
  }

  private initializeTabs(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }
}
