import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationModule } from 'angular-l10n';

import { DashboardsComponent } from './dashboards.component';
import { MenuBuilder } from '~app/master-data/configuration';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('DashboardsComponent', () => {
  let component: DashboardsComponent;
  let fixture: ComponentFixture<DashboardsComponent>;

  const activatedRouteStub = {
    snapshot: {
      data: {}
    }
  };
  const menuBuilderSpy = createSpyObject(MenuBuilder);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: MenuBuilder, useValue: menuBuilderSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ],
      imports: [L10nTranslationModule.forRoot(l10nConfig)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
