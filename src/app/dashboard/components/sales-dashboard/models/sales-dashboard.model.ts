import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface SalesDashboardFilter {
  currency: Currency;
  bsp: BspDto[];
}
