import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { isEqual } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { filter, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import * as fromDashboard from '../../store';
import { SalesDashboardFilter } from './models/sales-dashboard.model';
import { SalesDashboardFilterFormatter } from './services/sales-dashboard-filter-formatter.service';
import { SalesDashboardPowerBiService } from './services/sales-dashboard-power-bi.service';
import * as SalesDashboardActions from './store/actions/sales-dashboard.actions';
import { getPermissions } from '~app/auth/selectors/auth.selectors';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { AppState } from '~app/reducers';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { PowerBITokenDto } from '~app/shared/components/power-bi/model/powerbi-token.model';
import { Permissions } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { BspPermissions } from '~app/shared/models/permissions.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-sales-dashboard',
  templateUrl: './sales-dashboard.component.html',
  styleUrls: ['./sales-dashboard.component.scss']
})
export class SalesDashboardComponent implements OnInit, OnDestroy {
  public subtabTitle = 'DASHBOARD.tabs.sales.subtabTitle';
  public form: FormGroup;

  public predefinedFilters: Partial<SalesDashboardFilter> = { currency: null };

  public isDashboardVisible: boolean;

  public grossSalesAirlineIndustryBarChartData: BarChartData;
  public topGrossSalesAgentsBarChartData: BarChartData;
  public grossSalesAirlineIndustryBarChartTitle = 'DASHBOARD.barCharts.sales.grossSalesAirlineIndustry.title';
  public topGrossSalesAgentsBarChartTitle = 'DASHBOARD.barCharts.sales.topGrossSalesAgents.title';
  public barChartsLoading: boolean;

  public isFilterValid$: Observable<boolean> = this.store.select(fromDashboard.getSalesDashboardIsValid);

  public bspCountriesList$: Observable<DropdownOption<BspDto>[]>;
  public currencyList$: Observable<DropdownOption<Currency>[]> = of([]);

  public powerbiToken$: Observable<PowerBITokenDto>;

  public isBspFilterLocked = true;
  public isCurrencyFilterLocked: boolean;

  // For now, only PowerBi dashboard is visible
  public isOldDashboardVisible = false;

  private grossSalesAirlineIndustryKey = 'grossSalesAirlineIndustry';
  private topGrossSalesAgentsKey = 'topGrossSalesAgents';

  private formUtil: FormUtil;

  private destroy$ = new Subject<any>();

  constructor(
    public displayFormatter: SalesDashboardFilterFormatter,
    private translationService: L10nTranslationService,
    private dashboardService: DashboardService,
    private bspsDictionaryService: BspsDictionaryService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private salesDashboardPowerBiService: SalesDashboardPowerBiService,
    private store: Store<AppState>,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.buildFilterForm();

    this.initializePowerBiToken();
    this.initializeFilterFormDropdowns();
    this.initializeFilterFormListeners();
  }

  public saveFilters(value?: SalesDashboardFilter): void {
    this.store.dispatch(SalesDashboardActions.updateValue({ value: value || this.form.value }));
  }

  public resetFilters(): void {
    this.store.dispatch(SalesDashboardActions.resetValue());
  }

  public removeTag(filterValue: AppliedFilter): void {
    const nulledValues = Object.assign({}, ...filterValue.keys.map(key => ({ [key]: null })));
    this.saveFilters({ ...this.form.value, ...nulledValues });
  }

  private buildFilterForm(): void {
    this.form = this.formUtil.createGroup<SalesDashboardFilter>({
      currency: [null, Validators.required],
      bsp: null
    });
  }

  private initializePowerBiToken(): void {
    this.powerbiToken$ = this.salesDashboardPowerBiService.getToken();
  }

  private initializeFilterFormDropdowns(): void {
    // Currency filter dropdown
    this.currencyList$ = this.currencyDictionaryService.getFilteredDropdownOptions().pipe(
      tap(options => (this.isCurrencyFilterLocked = options.length === 1)),
      tap(options => this.updateCurrencyFilterValue(options))
    );

    // BSP filter dropdown
    this.bspCountriesList$ = FormUtil.get<SalesDashboardFilter>(this.form, 'currency').valueChanges.pipe(
      filter(value => !!value),
      switchMap((currency: Currency) =>
        combineLatest([
          this.bspsDictionaryService.getBspsFromCurrency(currency.id),
          this.store.pipe(select(getPermissions))
        ])
      ),
      map(([bsps, userPermissions]) => this.filterBspsByPermission(bsps, userPermissions.bspPermissions)),
      tap(bsps => (this.isBspFilterLocked = bsps.length <= 1)),
      tap(bsps => this.updateBspFilterValue(bsps)),
      map(bsps => bsps.map(toValueLabelObjectBsp))
    );
  }

  private filterBspsByPermission(bsps: BspDto[], bspPermissions: BspPermissions[]): BspDto[] {
    const permission = Permissions.readSalesDash;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private updateBspFilterValue(bspList: BspDto[]): void {
    const bspControl = FormUtil.get<SalesDashboardFilter>(this.form, 'bsp');
    const bspsSelected: BspDto[] = bspControl.value;

    if (bspList.length === 1) {
      // Patch the only possible BSP value
      bspControl.patchValue(bspList);
    } else if (bspsSelected?.length) {
      // Patch values for multiple available BSPs
      const availableBsps = bspsSelected.filter(bsp =>
        bspList.some(({ isoCountryCode }) => bsp.isoCountryCode === isoCountryCode)
      );

      bspControl.patchValue(availableBsps);
    }
  }

  private updateCurrencyFilterValue(options: DropdownOption<Currency>[]): void {
    if (options.length === 1) {
      const currencyControl = FormUtil.get<SalesDashboardFilter>(this.form, 'currency');

      // Detect currency form value changes manually to populate `bspCountriesList` on component initialization
      this.cd.detectChanges();

      currencyControl.patchValue(options[0].value);
    }
  }

  private initializeFilterFormListeners(): void {
    this.initializeStoredFiltersListener();
    this.initializeFormValidityListener();
  }

  private initializeStoredFiltersListener(): void {
    this.store.pipe(select(fromDashboard.getSalesDashboardValue), takeUntil(this.destroy$)).subscribe(filtersValue => {
      const isFilterEmpty: boolean = Object.values(filtersValue).every(value => value === null);

      if (!isFilterEmpty && !isEqual(this.form.value, filtersValue)) {
        // Detect currency form value changes manually to populate `bspCountriesList` on component initialization
        this.cd.detectChanges();

        this.form.patchValue(filtersValue);
      }

      // Update dashboard flag
      this.isDashboardVisible = !isFilterEmpty;

      if (this.isDashboardVisible) {
        this.searchBarChartsData(filtersValue);
      }
    });
  }

  private initializeFormValidityListener(): void {
    this.form.statusChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.store.dispatch(SalesDashboardActions.updateValidity({ isValid: this.form.valid })));
  }

  private searchBarChartsData(filtersValue: SalesDashboardFilter): void {
    // Loading spinner is not displayed if there are not bar charts previously
    this.setBarChartsLoading(!!this.grossSalesAirlineIndustryBarChartData);

    this.dashboardService
      .getGrossSalesAirlineIndustryData(filtersValue)
      .pipe(
        map(data => this.translateBarChartData(data, this.grossSalesAirlineIndustryKey)),
        finalize(() => this.setBarChartsLoading(false))
      )
      .subscribe(data => (this.grossSalesAirlineIndustryBarChartData = data));

    this.dashboardService
      .getTopGrossSalesAgentsData(filtersValue)
      .pipe(
        map(data => this.translateBarChartData(data, this.topGrossSalesAgentsKey)),
        finalize(() => this.setBarChartsLoading(false))
      )
      .subscribe(data => (this.topGrossSalesAgentsBarChartData = data));
  }

  private setBarChartsLoading(loading: boolean): void {
    this.barChartsLoading = loading;
  }

  private translateBarChartData({ labels, values }: BarChartData, translationKey: string): BarChartData {
    return {
      labels,
      values: values.map(({ key, totals }) => ({
        key: this.translationService.translate(`DASHBOARD.barCharts.sales.${translationKey}.legend.${key}`),
        totals
      }))
    };
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
