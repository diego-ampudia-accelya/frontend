import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { SalesDashboardComponent } from './sales-dashboard.component';
import { SalesDashboardFilterFormatter } from './services/sales-dashboard-filter-formatter.service';
import { DashboardNavigationService } from '~app/dashboard/services/dashboard-navigation.service';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

// TODO Skipped since only powerbi dashboard will remain (peding future task for removal)
xdescribe('SalesDashboardComponent', () => {
  let component: SalesDashboardComponent;
  let fixture: ComponentFixture<SalesDashboardComponent>;

  const dashboardServiceSpy = createSpyObject(DashboardService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const currencyDictionaryServiceSpy = createSpyObject(CurrencyDictionaryService);

  const initialState = {
    auth: {
      user: { bspPermissions: [] }
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {},
    dashboard: {
      salesDashboard: { value: { currency: { id: 1 } }, isValid: true }
    }
  };

  const mockGrossSalesAirlineIndustryData: BarChartData = {
    labels: ['2021-10', '2021-11', '2021-12'],
    values: [
      { key: 'airlineAmount', totals: [0, 20, 89] },
      { key: 'industryAmount', totals: [138, 92, 82] }
    ]
  };
  const mockTopGrossSalesAgentsData: BarChartData = {
    labels: ['312432234', '234234324'],
    values: [{ key: 'total', totals: [2, 4] }]
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SalesDashboardComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        SalesDashboardFilterFormatter,
        FormBuilder,
        provideMockStore({ initialState }),
        mockProvider(DashboardNavigationService),
        { provide: DashboardService, useValue: dashboardServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionaryServiceSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesDashboardComponent);
    component = fixture.componentInstance;

    dashboardServiceSpy.getGrossSalesAirlineIndustryData.and.returnValue(of(mockGrossSalesAirlineIndustryData));
    dashboardServiceSpy.getTopGrossSalesAgentsData.and.returnValue(of(mockTopGrossSalesAgentsData));

    bspsDictionaryServiceSpy.getBspsFromCurrency.and.returnValue(of([]));
    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should fetch new data when filters are saved', fakeAsync(() => {
    component.saveFilters();
    tick();

    expect(dashboardServiceSpy.getGrossSalesAirlineIndustryData).toHaveBeenCalled();
    expect(dashboardServiceSpy.getTopGrossSalesAgentsData).toHaveBeenCalled();

    expect(component.grossSalesAirlineIndustryBarChartData).toEqual({
      labels: ['2021-10', '2021-11', '2021-12'],
      values: [
        { key: 'DASHBOARD.barCharts.sales.grossSalesAirlineIndustry.legend.airlineAmount', totals: [0, 20, 89] },
        { key: 'DASHBOARD.barCharts.sales.grossSalesAirlineIndustry.legend.industryAmount', totals: [138, 92, 82] }
      ]
    });
    expect(component.topGrossSalesAgentsBarChartData).toEqual({
      labels: ['312432234', '234234324'],
      values: [{ key: 'DASHBOARD.barCharts.sales.topGrossSalesAgents.legend.total', totals: [2, 4] }]
    });
  }));

  it('should remove specific tags', () => {
    spyOn(component, 'saveFilters');

    component.form.patchValue({ currency: { code: 'EUR' }, bsp: [{ id: 1 }] });
    expect(component.form.get('bsp').value).toBeTruthy();

    component.removeTag({ keys: ['bsp'], label: null });

    expect(component.saveFilters).toHaveBeenCalledWith({ currency: { code: 'EUR' }, bsp: null });
  });

  describe('BSP filter', () => {
    it('should lock BSP filter on initialization', () => {
      // Locked on initialization (no currency selected)
      expect(component.isBspFilterLocked).toBe(true);
    });

    it('should lock BSP filter if there is only 1 BSP option on currency change', fakeAsync(() => {
      spyOn<any>(component, 'filterBspsByPermission').and.returnValue([{ id: 1 }]);
      component.ngOnInit();

      component.form.patchValue({ currency: { code: 'EUR' } });
      tick();

      expect(component.isBspFilterLocked).toBe(true);
    }));

    it('should unlock BSP filter if there more than 1 BSP option on currency change', fakeAsync(() => {
      spyOn<any>(component, 'filterBspsByPermission').and.returnValue([{ id: 1 }, { id: 2 }]);
      component.ngOnInit();

      component.form.patchValue({ currency: { code: 'EUR' } });
      tick();

      expect(component.isBspFilterLocked).toBe(false);
    }));
  });

  describe('currency filter', () => {
    it('should be unlocked on initialization', () => {
      // Unlocked on initialization
      expect(component.isCurrencyFilterLocked).toBe(false);
    });

    it('should lock currency filter if there is only 1 currency option available', fakeAsync(() => {
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(
        of([{ value: { code: 'EUR' }, label: 'EUR' }])
      );

      component.ngOnInit();
      tick();

      expect(component.isCurrencyFilterLocked).toBe(true);
    }));

    it('should unlock currency filter if there more than 1 currency option', fakeAsync(() => {
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(
        of([
          { value: { code: 'EUR' }, label: 'EUR' },
          { value: { code: 'USD' }, label: 'USD' }
        ])
      );

      component.ngOnInit();
      tick();

      expect(component.isCurrencyFilterLocked).toBe(false);
    }));
  });
});
