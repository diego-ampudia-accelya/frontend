import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { PowerBITokenDto } from '~app/shared/components/power-bi/model/powerbi-token.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class SalesDashboardPowerBiService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-mgmt-integration/power-bi/sales-token`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public getToken(): Observable<PowerBITokenDto> {
    return this.http.get<PowerBITokenDto>(this.baseUrl);
  }
}
