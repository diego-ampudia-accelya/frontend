import { createReducer, on } from '@ngrx/store';

import { SalesDashboardFilter } from '../../models/sales-dashboard.model';
import * as SalesDashboardActions from '../actions/sales-dashboard.actions';

export const salesDashboardKey = 'salesDashboard';

export interface SalesDashboardState {
  value: SalesDashboardFilter;
  isValid: boolean;
}

const initialFormValue: SalesDashboardFilter = { currency: null, bsp: null };

export const initialState: SalesDashboardState = {
  value: initialFormValue,
  isValid: false
};

export const salesDashboardReducer = createReducer(
  initialState,
  on(SalesDashboardActions.updateValue, (state, { value }) => ({
    ...state,
    initialValue: value,
    value
  })),
  on(SalesDashboardActions.updateValidity, (state, { isValid }) => ({
    ...state,
    isValid
  })),
  on(SalesDashboardActions.resetValue, state => ({
    ...state,
    value: { ...initialFormValue, currency: state.value.currency }
  }))
);
