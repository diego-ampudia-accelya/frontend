import { createAction, props } from '@ngrx/store';

import { SalesDashboardFilter } from '../../models/sales-dashboard.model';

export const updateValue = createAction('[Sales Dashboard] Update Value', props<{ value: SalesDashboardFilter }>());
export const updateValidity = createAction('[Sales Dashboard] Update Form Validity', props<{ isValid: boolean }>());
export const resetValue = createAction('[Sales Dashboard] Reset Value');
