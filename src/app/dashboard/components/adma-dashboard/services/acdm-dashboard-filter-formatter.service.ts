import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AcdmDashboardFilter } from '../models/acdm-dashboard.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { bspFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AcdmDashboardFilterFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: AcdmDashboardFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<AcdmDashboardFilter>> = {
      bsps: bsps => `${this.translate('bsp')} - ${bspFilterTagMapper(bsps).join(', ')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({ keys: [item.key], label: item.mapper(item.value) }));
  }

  private translate(key: string): string {
    return this.translationService.translate('DASHBOARD.tabs.acdm.filters.labels.' + key);
  }
}
