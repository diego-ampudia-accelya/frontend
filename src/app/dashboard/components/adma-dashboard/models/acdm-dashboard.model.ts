import { BspDto } from '~app/shared/models/bsp.model';

export interface AcdmDashboardFilter {
  bsps: Array<BspDto>;
}
