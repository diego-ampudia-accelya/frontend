import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';
import { of } from 'rxjs';

import { AcdmDashboardComponent } from './acdm-dashboard.component';
import { AcdmDashboardFilterFormatter } from './services/acdm-dashboard-filter-formatter.service';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { AcdmKpi } from '~app/dashboard/models/dashboard.model';
import { DashboardNavigationService } from '~app/dashboard/services/dashboard-navigation.service';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('AdmaDashboardComponent', () => {
  let component: AcdmDashboardComponent;
  let fixture: ComponentFixture<AcdmDashboardComponent>;

  const dashboardServiceSpy = createSpyObject(DashboardService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);

  const initialState = {
    auth: {
      user: { bspPermissions: [] }
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {},
    dashboard: {
      acdmDashboard: { value: { bsps: [] }, isValid: true }
    }
  };

  const mockAcdmKpis: AcdmKpi[] = [{ totalDisputed: 23, totalUrgentDisputed: 11 }];
  const mockAdmDisputedData: BarChartData = {
    labels: ['2021-10', '2021-11', '2021-12'],
    values: [
      { key: 'DISPUTED', totals: [0, 20, 89] },
      { key: 'DISPUTE_APPROVED', totals: [138, 92, 82] },
      { key: 'DISPUTE_REJECTED', totals: [106, 52, 8] },
      { key: 'DELETED', totals: [92, 78, 50] }
    ]
  };
  const mockAdmTopDisputingAgentsData: BarChartData = {
    labels: ['312432234', '234234324'],
    values: [{ key: 'total', totals: [2, 4] }]
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AcdmDashboardComponent, MockPipe(HasPermissionPipe)],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        AcdmDashboardFilterFormatter,
        FormBuilder,
        provideMockStore({ initialState }),
        mockProvider(DashboardNavigationService),
        { provide: DashboardService, useValue: dashboardServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcdmDashboardComponent);
    component = fixture.componentInstance;

    dashboardServiceSpy.getAcdmKpis.and.returnValue(of(mockAcdmKpis));
    dashboardServiceSpy.getAdmDisputedData.and.returnValue(of(mockAdmDisputedData));
    dashboardServiceSpy.getAdmTopDisputingAgentsData.and.returnValue(of(mockAdmTopDisputingAgentsData));
    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(of([]));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should fetch new data when filters are saved', fakeAsync(() => {
    component.saveFilters();
    tick();

    expect(dashboardServiceSpy.getAcdmKpis).toHaveBeenCalled();
    expect(dashboardServiceSpy.getAdmDisputedData).toHaveBeenCalled();
    expect(dashboardServiceSpy.getAdmTopDisputingAgentsData).toHaveBeenCalled();

    expect(component.admDisputedBarChartData).toEqual({
      labels: ['2021-10', '2021-11', '2021-12'],
      values: [
        { key: 'ADM_ACM.status.DISPUTED', totals: [0, 20, 89] },
        { key: 'ADM_ACM.status.DISPUTE_APPROVED', totals: [138, 92, 82] },
        { key: 'ADM_ACM.status.DISPUTE_REJECTED', totals: [106, 52, 8] },
        { key: 'ADM_ACM.status.DELETED', totals: [92, 78, 50] }
      ]
    });
    expect(component.admTopDisputingAgentsBarChartData).toEqual({
      labels: ['312432234', '234234324'],
      values: [{ key: 'DASHBOARD.barCharts.acdm.admTopDisputingAgents.legend.total', totals: [2, 4] }]
    });
  }));

  it('should remove specific tags', () => {
    spyOn(component, 'saveFilters');

    component.form.patchValue({ bsps: [{ id: 1 }] });
    expect(component.form.get('bsps').value).toBeTruthy();

    component.removeTag({ keys: ['bsps'], label: null });

    expect(component.saveFilters).toHaveBeenCalledWith({ bsps: null });
  });
});
