import { createReducer, on } from '@ngrx/store';

import { AcdmDashboardFilter } from '../../models/acdm-dashboard.model';
import * as AcdmDashboardActions from '../actions/acdm-dashboard.actions';

export const acdmDashboardKey = 'acdmDashboard';

export interface AcdmDashboardState {
  value: AcdmDashboardFilter;
  isValid: boolean;
}

const initialFormValue: AcdmDashboardFilter = { bsps: null };

export const initialState: AcdmDashboardState = {
  value: initialFormValue,
  isValid: false
};

export const acdmDashboardReducer = createReducer(
  initialState,
  on(AcdmDashboardActions.updateValue, (state, { value }) => ({
    ...state,
    initialValue: value,
    value
  })),
  on(AcdmDashboardActions.updateValidity, (state, { isValid }) => ({
    ...state,
    isValid
  })),
  on(AcdmDashboardActions.resetValue, state => ({
    ...state,
    value: initialFormValue,
    isValid: false
  }))
);
