import { createAction, props } from '@ngrx/store';

import { AcdmDashboardFilter } from '../../models/acdm-dashboard.model';

export const updateValue = createAction('[Acdm Dashboard] Update Value', props<{ value: AcdmDashboardFilter }>());
export const updateValidity = createAction(
  '[Acdm Dashboard] Update Form Validity',
  props<{
    isValid: boolean;
  }>()
);
export const resetValue = createAction('[Acdm Dashboard] Reset Value');
