import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';
import { of } from 'rxjs';

import { RefundDashboardComponent } from './refund-dashboard.component';
import { RefundDashboardFilterFormatter } from './services/refund-dashboard-filter-formatter.service';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { RefundKpi } from '~app/dashboard/models/dashboard.model';
import { DashboardNavigationService } from '~app/dashboard/services/dashboard-navigation.service';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('RefundDashboardComponent', () => {
  let component: RefundDashboardComponent;
  let fixture: ComponentFixture<RefundDashboardComponent>;

  const dashboardServiceSpy = createSpyObject(DashboardService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);

  const initialState = {
    auth: {
      user: { bspPermissions: [] }
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {},
    dashboard: {
      refundDashboard: { value: { bsps: [] }, isValid: true }
    }
  };

  const mockRefundKpis: RefundKpi[] = [
    { totalPending: 23, totalFinalPending: 4, totalUrgentFinalPending: 3, totalResubmitted: 2 }
  ];
  const mockRaIssuedData: BarChartData = {
    labels: ['2021-10', '2021-11', '2021-12'],
    values: [
      { key: 'AUTHORIZED', totals: [0, 20, 89] },
      { key: 'UNDER_INVESTIGATION', totals: [138, 92, 82] },
      { key: 'REJECTED', totals: [106, 52, 8] },
      { key: 'PENDING', totals: [92, 78, 50] }
    ]
  };
  const mockRaTopIssuingAgentsData: BarChartData = {
    labels: ['312432234', '234234324'],
    values: [{ key: 'total', totals: [2, 4] }]
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundDashboardComponent, MockPipe(HasPermissionPipe)],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        RefundDashboardFilterFormatter,
        FormBuilder,
        provideMockStore({ initialState }),
        mockProvider(DashboardNavigationService),
        { provide: DashboardService, useValue: dashboardServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundDashboardComponent);
    component = fixture.componentInstance;

    dashboardServiceSpy.getRefundKpis.and.returnValue(of(mockRefundKpis));
    dashboardServiceSpy.getRaIssuedData.and.returnValue(of(mockRaIssuedData));
    dashboardServiceSpy.getRaTopIssuingAgentsData.and.returnValue(of(mockRaTopIssuingAgentsData));

    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(of([]));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should fetch new data when filters are saved', fakeAsync(() => {
    component.saveFilters();
    tick();

    expect(dashboardServiceSpy.getRefundKpis).toHaveBeenCalled();
    expect(dashboardServiceSpy.getRaIssuedData).toHaveBeenCalled();
    expect(dashboardServiceSpy.getRaTopIssuingAgentsData).toHaveBeenCalled();

    expect(component.raIssuedBarChartData).toEqual({
      labels: ['2021-10', '2021-11', '2021-12'],
      values: [
        { key: 'REFUNDS.status.AUTHORIZED', totals: [0, 20, 89] },
        { key: 'REFUNDS.status.UNDER_INVESTIGATION', totals: [138, 92, 82] },
        { key: 'REFUNDS.status.REJECTED', totals: [106, 52, 8] },
        { key: 'REFUNDS.status.PENDING', totals: [92, 78, 50] }
      ]
    });
    expect(component.raTopIssuingAgentsBarChartData).toEqual({
      labels: ['312432234', '234234324'],
      values: [{ key: 'DASHBOARD.barCharts.refund.raTopIssuingAgents.legend.total', totals: [2, 4] }]
    });
  }));

  it('should remove specific tags', () => {
    spyOn(component, 'saveFilters');

    component.form.patchValue({ bsps: [{ id: 1 }] });
    expect(component.form.get('bsps').value).toBeTruthy();

    component.removeTag({ keys: ['bsps'], label: null });

    expect(component.saveFilters).toHaveBeenCalledWith({ bsps: null });
  });
});
