import { BspDto } from '~app/shared/models/bsp.model';

export interface RefundDashboardFilter {
  bsps: Array<BspDto>;
}
