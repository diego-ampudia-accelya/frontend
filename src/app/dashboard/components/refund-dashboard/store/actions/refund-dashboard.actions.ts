import { createAction, props } from '@ngrx/store';

import { RefundDashboardFilter } from '../../models/refund-dashboard.model';

export const updateValue = createAction('[Refund Dashboard] Update Value', props<{ value: RefundDashboardFilter }>());
export const updateValidity = createAction(
  '[Refund Dashboard] Update Form Validity',
  props<{
    isValid: boolean;
  }>()
);
export const resetValue = createAction('[Refund Dashboard] Reset Value');
