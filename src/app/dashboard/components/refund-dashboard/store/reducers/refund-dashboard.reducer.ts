import { createReducer, on } from '@ngrx/store';

import { RefundDashboardFilter } from '../../models/refund-dashboard.model';
import * as RefundDashboardActions from '../actions/refund-dashboard.actions';

export const refundDashboardKey = 'refundDashboard';

export interface RefundDashboardState {
  value: RefundDashboardFilter;
  isValid: boolean;
}

const initialFormValue: RefundDashboardFilter = { bsps: null };

export const initialState: RefundDashboardState = {
  value: initialFormValue,
  isValid: false
};

export const refundDashboardReducer = createReducer(
  initialState,
  on(RefundDashboardActions.updateValue, (state, { value }) => ({
    ...state,
    initialValue: value,
    value
  })),
  on(RefundDashboardActions.updateValidity, (state, { isValid }) => ({
    ...state,
    isValid
  })),
  on(RefundDashboardActions.resetValue, state => ({
    ...state,
    value: initialFormValue,
    isValid: false
  }))
);
