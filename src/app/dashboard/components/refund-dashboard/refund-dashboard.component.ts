import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { finalize, first, map, takeUntil, tap } from 'rxjs/operators';

import * as fromDashboard from '../../store';
import { RefundDashboardFilter } from './models/refund-dashboard.model';
import { RefundDashboardFilterFormatter } from './services/refund-dashboard-filter-formatter.service';
import * as RefundDashboardActions from './store/actions/refund-dashboard.actions';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { GeneralKpi } from '~app/dashboard/models/dashboard.model';
import { DashboardNavigationService } from '~app/dashboard/services/dashboard-navigation.service';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { AppState } from '~app/reducers';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-refund-dashboard',
  templateUrl: './refund-dashboard.component.html',
  styleUrls: ['./refund-dashboard.component.scss']
})
export class RefundDashboardComponent implements OnInit, OnDestroy {
  public subtabTitle = 'DASHBOARD.tabs.refund.subtabTitle';
  public form: FormGroup;

  public kpis: GeneralKpi[];
  public kpisLoading: boolean;

  public raIssuedBarChartData: BarChartData;
  public raTopIssuingAgentsBarChartData: BarChartData;
  public raIssuedBarChartTitle = 'DASHBOARD.barCharts.refund.raMonthIssued.title';
  public raTopIssuingAgentsBarChartTitle = 'DASHBOARD.barCharts.refund.raTopIssuingAgents.title';
  public barChartsLoading: boolean;

  public isFilterEnabled: boolean;
  public isFilterValid$: Observable<boolean> = this.store.select(fromDashboard.getRefundDashboardIsValid);

  public bspCountriesList: DropdownOption<BspDto>[];

  private loggedUser: User;

  private formUtil: FormUtil;

  private destroy$ = new Subject<any>();

  constructor(
    public displayFormatter: RefundDashboardFilterFormatter,
    private translationService: L10nTranslationService,
    private dashboardNavigationService: DashboardNavigationService,
    private dashboardService: DashboardService,
    private bspsDictionaryService: BspsDictionaryService,
    private store: Store<AppState>,
    private fb: FormBuilder
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.initializeUserAndPermissions();
    this.buildFilterForm();
    this.initializeLeanBspFilter();
    this.initializeStoredFiltersListener();
    this.initializeFormValidityListener();
  }

  public buildFilterForm(): void {
    this.form = this.formUtil.createGroup<RefundDashboardFilter>({ bsps: null });
  }

  public saveFilters(value?: RefundDashboardFilter): void {
    this.store.dispatch(RefundDashboardActions.updateValue({ value: value || this.form.value }));
  }

  public resetFilters(): void {
    this.store.dispatch(RefundDashboardActions.resetValue());
  }

  public removeTag(filter: AppliedFilter): void {
    const nulledValues = Object.assign({}, ...filter.keys.map(key => ({ [key]: null })));
    this.saveFilters({ ...this.form.value, ...nulledValues });
  }

  public onKpiSeeAllClick(event: MouseEvent, kpi: GeneralKpi) {
    event.preventDefault();
    this.dashboardNavigationService.navigateBySubtabKpi(kpi, this.getBspsSelected());
  }

  private initializeUserAndPermissions(): void {
    this.store.pipe(select(getUser), first()).subscribe(user => {
      this.loggedUser = user;
    });
  }

  private initializeLeanBspFilter(): void {
    this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, [Permissions.readRefundDash])
      .pipe(
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspOptions => (this.isFilterEnabled = bspOptions.length > 1))
      )
      .subscribe(bspOptions => (this.bspCountriesList = bspOptions));
  }

  private initializeStoredFiltersListener(): void {
    this.store.pipe(select(fromDashboard.getRefundDashboardValue), takeUntil(this.destroy$)).subscribe(filtersValue => {
      this.form.patchValue(filtersValue, { emitEvent: false });

      this.searchKpis(filtersValue);
      this.searchBarChartsData(filtersValue);
    });
  }

  private searchKpis(filtersValue: RefundDashboardFilter): void {
    this.setKpisLoading(true);

    this.dashboardService
      .getRefundKpis(filtersValue.bsps?.map(bsp => bsp.id.toString()))
      .pipe(
        map(kpis => this.updateKpisSeeAllVisibility(kpis, filtersValue.bsps)),
        finalize(() => this.setKpisLoading(false))
      )
      .subscribe(kpis => (this.kpis = kpis));
  }

  private updateKpisSeeAllVisibility(kpis: GeneralKpi[], bspFilterSelected: BspDto[]): GeneralKpi[] {
    // Bsps filtered by user or all options available
    const bspsSelected: BspDto[] = bspFilterSelected || this.bspCountriesList.map(option => option.value);

    // We need to check if for any bspSelected, it has the permission required to access the query linked to the See All
    return kpis.map(kpi => ({
      ...kpi,
      seeAllVisible: bspsSelected.some(bsp =>
        this.loggedUser.bspPermissions.find(
          bspPermissions => bspPermissions.bspId === bsp.id && bspPermissions.permissions.includes(kpi.permission)
        )
      )
    }));
  }

  private setKpisLoading(loading: boolean): void {
    this.kpisLoading = loading;
  }

  private searchBarChartsData(filtersValue: RefundDashboardFilter): void {
    // Loading spinner is not displayed if there are not bar charts previously
    this.setBarChartsLoading(!!(this.raIssuedBarChartData || this.raTopIssuingAgentsBarChartData));

    const bspIds: string[] = filtersValue.bsps?.map(bsp => bsp.id.toString());

    this.dashboardService
      .getRaIssuedData(bspIds)
      .pipe(
        map(data => this.translateRaIssuedBarChartData(data)),
        finalize(() => this.setBarChartsLoading(false))
      )
      .subscribe(data => (this.raIssuedBarChartData = data));

    this.dashboardService
      .getRaTopIssuingAgentsData(bspIds)
      .pipe(
        map(data => this.translateRaTopIssuingAgentsBarChartData(data)),
        finalize(() => this.setBarChartsLoading(false))
      )
      .subscribe(data => (this.raTopIssuingAgentsBarChartData = data));
  }

  private setBarChartsLoading(loading: boolean): void {
    this.barChartsLoading = loading;
  }

  private translateRaIssuedBarChartData(data: BarChartData): BarChartData {
    const { labels, values } = data;

    return {
      labels,
      values: values.map(({ key, totals }) => ({
        key: this.translationService.translate(`REFUNDS.status.${key}`),
        totals
      }))
    };
  }

  private translateRaTopIssuingAgentsBarChartData(data: BarChartData): BarChartData {
    const { labels, values } = data;

    return {
      labels,
      values: values.map(({ key, totals }) => ({
        key: this.translationService.translate(`DASHBOARD.barCharts.refund.raTopIssuingAgents.legend.${key}`),
        totals
      }))
    };
  }

  private initializeFormValidityListener(): void {
    this.form.statusChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.store.dispatch(RefundDashboardActions.updateValidity({ isValid: this.form.valid })));
  }

  private getBspsSelected(): BspDto[] {
    return FormUtil.get<RefundDashboardFilter>(this.form, 'bsps').value;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
