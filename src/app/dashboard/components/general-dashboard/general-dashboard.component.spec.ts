import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { of } from 'rxjs';

import { GeneralKpi, QuickLink, StatDoughnutChart } from '../../models/dashboard.model';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GeneralDashboardComponent } from '~app/dashboard/components/general-dashboard/general-dashboard.component';
import { DashboardNavigationService } from '~app/dashboard/services/dashboard-navigation.service';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { DialogService } from '~app/shared/components';
import { Permissions } from '~app/shared/constants';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('GeneralDashboardComponent', () => {
  let component: GeneralDashboardComponent;
  let fixture: ComponentFixture<GeneralDashboardComponent>;

  const spyRouter = createSpyObject(Router);
  const permissionsServiceStub = createSpyObject(PermissionsService);
  let dashboardNavigationService: DashboardNavigationService;
  let dialogService: SpyObject<DialogService>;

  const initialState = {
    auth: {
      user: {
        userType: UserType.AIRLINE,
        globalAirline: {
          iataCode: '016',
          globalName: 'AIRLINE016'
        },
        permissions: [Permissions.lean],
        defaultIsoc: 'ES',
        bsps: [
          { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2021-01-01' },
          { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2021-01-01' }
        ]
      }
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GeneralDashboardComponent, HasPermissionPipe, TranslatePipeMock],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: L10N_LOCALE, useValue: { language: 'en' } },
        { provide: Router, useValue: spyRouter },
        mockProvider(DialogService),
        mockProvider(DashboardNavigationService),
        { provide: PermissionsService, useValue: permissionsServiceStub },
        mockProvider(DashboardService),
        mockProvider(AppConfigurationService, { isTrainingSite: true }),
        HasPermissionPipe,

        provideMockStore({ initialState }),
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(GeneralDashboardComponent);
    dialogService = TestBed.inject<any>(DialogService);
    dialogService.open.and.callFake(() => of({}));
    component = fixture.componentInstance;
    dashboardNavigationService = component['dashboardNavigationService'];

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open welcome dialog when the configuration is for training site', fakeAsync(() => {
    spyOn(component, 'openWelcomeDialog');

    component.ngOnInit();

    expect(component.openWelcomeDialog).toHaveBeenCalled();
  }));

  it('should call navigateByKpi when see all link  is clicked', () => {
    spyOn(dashboardNavigationService, 'navigateByKpi');
    const kpi = {
      name: 'Test',
      description: 'Test desc',
      value: 3
    } as GeneralKpi;
    const event = new MouseEvent('click');
    component.onKpiSeeAllClick(event, kpi);

    expect(dashboardNavigationService.navigateByKpi).toHaveBeenCalledWith(kpi);
  });

  it('should call navigateByStat when insights see all link  is clicked', () => {
    spyOn(dashboardNavigationService, 'navigateByStat');
    const stat: StatDoughnutChart = {
      title: 'Test',
      agregator: 'test',
      total: 10,
      data: [
        {
          key: 'test',
          y: 5
        }
      ]
    };
    const event = new MouseEvent('click');
    component.onInsightSeeAllClick(event, stat);

    expect(dashboardNavigationService.navigateByStat).toHaveBeenCalledWith(stat);
  });

  it('should call navigateByQuickLInk when quick link  is clicked', () => {
    spyOn(dashboardNavigationService, 'navigateByQuickLink');
    const stat: QuickLink = {
      title: 'Test',
      navigationData: {
        viewListId: 'reports',
        url: '/reports',
        filter: {},
        value: {}
      }
    };
    const event = new MouseEvent('click');
    component.onQuickLinkClick(event, stat);

    expect(dashboardNavigationService.navigateByQuickLink).toHaveBeenCalledWith(stat);
  });
});
