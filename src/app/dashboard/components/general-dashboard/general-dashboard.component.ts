import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { Card } from '~app/core/models/card.model';
import {
  ExternalNavigationLink,
  GeneralKpi,
  IataMessageNews,
  QuickLink,
  SearchMock,
  StatDoughnutChart
} from '~app/dashboard/models/dashboard.model';
import { DashboardNavigationService } from '~app/dashboard/services/dashboard-navigation.service';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { WelcomeDialogComponent } from '~app/dashboard/welcome-dialog/welcome-dialog.component';
import { DialogService, FooterButton } from '~app/shared/components';
import { InfoDialogComponent } from '~app/shared/components/form-dialog-components/info-dialog/info-dialog.component';
import { Permissions } from '~app/shared/constants';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Component({
  selector: 'bspl-general-dashboard',
  templateUrl: './general-dashboard.component.html',
  styleUrls: ['./general-dashboard.component.scss'],
  providers: [DashboardService, DashboardNavigationService]
})
export class GeneralDashboardComponent implements OnInit, OnDestroy {
  private imgPath = '../../../../assets/images/dashboard/';

  private stats1 = this.imgPath + 'stats1.png';
  private stats2 = this.imgPath + 'stats2.png';

  public showContent = true;

  public dashboard: Card[] = [
    new Card('ADM', this.stats1),
    new Card('Files', this.stats1),
    new Card('Refunds', this.stats2),
    new Card('Alerts', this.stats2)
  ];

  public airlineLogo$: Observable<string>;
  public kpis: Array<GeneralKpi>;
  public dashboardChartData: Array<StatDoughnutChart>;
  public userNews: IataMessageNews;
  public searchItems: Array<SearchMock>;
  public quickLinks: Array<QuickLink>;
  public externalLinks: Array<ExternalNavigationLink> = [];

  public hasLeanPermission: boolean;
  public isDefaultAdmInsightTmplVisible: boolean;

  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public dialogService: DialogService,
    private translationService: L10nTranslationService,
    private configService: DashboardService,
    private dashboardNavigationService: DashboardNavigationService,
    private appConfigurationService: AppConfigurationService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    if (this.appConfigurationService.isTrainingSite && !localStorage.getItem('welcomeDialogGotIt')) {
      this.openWelcomeDialog();
    }
    this.checkLeanPermissions();
    this.initializeDashboardData();
  }

  private checkLeanPermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  public openAdmInsightDialog() {
    this.dialogService
      .open(InfoDialogComponent, {
        data: {
          title: this.translationService.translate('DASHBOARD.admInsight.title'),
          contentTranslationKey: this.translationService.translate('DASHBOARD.admInsight.dialogInfo'),
          footerButtonsType: FooterButton.Close,
          isClosable: true,
          hasCancelButton: false
        }
      })
      .subscribe(action => {
        if (action.clickedBtn === FooterButton.Close) {
          this.dialogService.close();
        }
      });
  }

  public openWelcomeDialog() {
    this.showContent = false;

    this.dialogService
      .open(WelcomeDialogComponent, {
        data: {
          title: this.translationService.translate('DASHBOARD.DIALOGS.WELCOME.TITLE'),
          footerButtonsType: FooterButton.GotIt,
          isClosable: true,
          hasCancelButton: false
        }
      })
      .subscribe(() => {
        this.showContent = true;
      });
  }

  public onKpiSeeAllClick(event: MouseEvent, kpi: GeneralKpi) {
    event.preventDefault();
    this.dashboardNavigationService.navigateByKpi(kpi);
  }

  public onInsightSeeAllClick(event: MouseEvent, stat: StatDoughnutChart) {
    event.preventDefault();

    this.dashboardNavigationService.navigateByStat(stat);
  }

  public onQuickLinkClick(event: MouseEvent, quickLink: QuickLink) {
    event.preventDefault();
    this.dashboardNavigationService.navigateByQuickLink(quickLink);
  }

  private initializeDashboardData() {
    this.configService
      .getAirlineIataCode()
      .pipe(takeUntil(this.destroy$))
      .subscribe(iataCode => {
        this.airlineLogo$ = of(this.appConfigurationService.getAirlineLogoUrl(iataCode));
        this.externalLinks = this.configService.getExternalLinks(iataCode);

        // By checking the existence of `this.externalLinks[1]` we make sure that the airline does not have access to ADM insight
        this.isDefaultAdmInsightTmplVisible = this.hasLeanPermission && !this.externalLinks[1];
      });

    this.configService
      .getGeneralKpis()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.kpis = data;
      });

    this.configService
      .getDoughnutChartData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.dashboardChartData = data;
      });

    this.configService
      .getUserNews()
      .pipe(takeUntil(this.destroy$))
      .subscribe(news => {
        this.userNews = news;
      });

    this.configService
      .getLatestSearches()
      .pipe(takeUntil(this.destroy$))
      .subscribe(searchItems => {
        this.searchItems = searchItems;
      });

    this.dashboardNavigationService
      .getQuickLinks()
      .pipe(takeUntil(this.destroy$))
      .subscribe(quickLinks => {
        this.quickLinks = quickLinks;
      });
  }

  public ngOnDestroy(): void {
    try {
      this.dialogService.close();
    } catch (e) {
      /* handle error */
    }

    this.destroy$.next();
  }
}
