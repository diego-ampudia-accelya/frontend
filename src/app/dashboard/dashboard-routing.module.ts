import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AcdmDashboardComponent } from './components/adma-dashboard/acdm-dashboard.component';
import { DashboardsComponent } from './components/dashboards/dashboards.component';
import { GeneralDashboardComponent } from './components/general-dashboard/general-dashboard.component';
import { RefundDashboardComponent } from './components/refund-dashboard/refund-dashboard.component';
import { SalesDashboardComponent } from './components/sales-dashboard/sales-dashboard.component';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants';

const routes: Routes = [
  { path: '', redirectTo: ROUTES.DASHBOARD.url, pathMatch: 'full' },
  {
    path: ROUTES.DASHBOARD.path,
    component: DashboardsComponent,
    data: { tab: ROUTES.DASHBOARD },
    children: [
      {
        path: 'general',
        component: GeneralDashboardComponent,
        data: {
          title: 'DASHBOARD.tabs.general.subtabLabel'
        }
      },
      {
        path: 'acdm',
        component: AcdmDashboardComponent,
        data: {
          title: 'DASHBOARD.tabs.acdm.subtabLabel',
          requiredPermissions: [Permissions.readAcdmDash]
        }
      },
      {
        path: 'refund',
        component: RefundDashboardComponent,
        data: {
          title: 'DASHBOARD.tabs.refund.subtabLabel',
          requiredPermissions: [Permissions.readRefundDash]
        }
      },
      {
        path: 'sales',
        component: SalesDashboardComponent,
        data: {
          title: 'DASHBOARD.tabs.sales.subtabLabel',
          requiredPermissions: [Permissions.readSalesDash]
        }
      },
      { path: '', redirectTo: 'general', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
