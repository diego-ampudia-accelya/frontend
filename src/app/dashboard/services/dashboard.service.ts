import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, combineLatest, forkJoin, Observable, of, ReplaySubject, Subscription } from 'rxjs';
import { catchError, filter, first, map, switchMap, tap } from 'rxjs/operators';

import { SalesDashboardFilter } from '../components/sales-dashboard/models/sales-dashboard.model';
import { mapToKpi } from '../helpers/dashboard-kpi-mapper.helper';
import {
  mapDocumentMetrics,
  mapGrossSalesMetrics,
  mapTopAgentMetrics
} from '../helpers/dashboard-metrics-mapper.helper';
import { ADM_INSIGHT_URLS, EXTERNAL_LINKS } from '../models/dashboard.constants';
import {
  AcdmKpiListIdMapper,
  FILES_ID,
  PBD_ID,
  RefundKpiListIdMapper,
  REJECTED_DOCUMENTS_ID
} from './dashboard.config';
import { getUserBsps, getUserIataDetails, getUserType, isAirlineUser } from '~app/auth/selectors/auth.selectors';
import {
  AcdmKpi,
  DocumentMetrics,
  ExternalNavigationLink,
  GeneralKpi,
  GrossSalesAirlineIndustryMetrics,
  IataMessageNews,
  RefundKpi,
  SearchMock,
  StatDoughnutChart,
  Statistic
} from '~app/dashboard/models/dashboard.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { Permissions } from '~app/shared/constants/permissions';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

const emptyErrorHandler = () => of(null);
const addListId = (stat: Statistic, listId: string): Statistic => {
  stat.stats.listId = listId;
  stat.kpis = stat.kpis.map(kpi => {
    kpi.listId = listId;

    return kpi;
  });

  return stat;
};

@Injectable()
export class DashboardService {
  private _filesStatSubject = new ReplaySubject<Statistic>(1);
  private get filesStat$(): Observable<Statistic> {
    this.fetchData(this._filesStatSubject, this.fetchFilesStat);

    return this._filesStatSubject.asObservable();
  }

  private _refundAppsStatSubject = new ReplaySubject<Statistic>(1);
  private get refundAppsStat$(): Observable<Statistic> {
    this.fetchData(this._refundAppsStatSubject, this.fetchRefundAppsStat);

    return this._refundAppsStatSubject.asObservable();
  }

  private _acdmsStatSubject = new ReplaySubject<Statistic>(1);
  private get acdmsStat$(): Observable<Statistic> {
    this.fetchData(this._acdmsStatSubject, this.fetchAcdmsStat);

    return this._acdmsStatSubject.asObservable();
  }

  private _pbdsKpiSubject = new ReplaySubject<GeneralKpi>(1);
  private get pbdsKpi$(): Observable<GeneralKpi> {
    this.fetchData(this._pbdsKpiSubject, this.fetchPbdsStat);

    return this._pbdsKpiSubject.asObservable();
  }

  // Subject containing info about File Types. Public since its information is retrieved in DashboardNavigation
  public remittancesFileTypesSubject = new BehaviorSubject<Array<{ descriptor: string; description: string }>>(null);
  private _remittancesKpiSubject = new ReplaySubject<GeneralKpi>(1);
  private get remittancesKpi$(): Observable<GeneralKpi> {
    this.fetchData(this._remittancesKpiSubject, this.fetchRemittanceData);

    return this._remittancesKpiSubject.asObservable();
  }

  private _rejectedDocsKpiSubject = new ReplaySubject<GeneralKpi>(1);
  private get rejectedDocsKpi$(): Observable<GeneralKpi> {
    this.fetchData(this._rejectedDocsKpiSubject, this.fetchDocRejectedStat);

    return this._rejectedDocsKpiSubject.asObservable();
  }

  public userType$: Observable<UserType> = this.store.pipe(select(getUserType), first());

  constructor(
    public translationService: L10nTranslationService,
    private store: Store<AppState>,
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private appConfigurationService: AppConfigurationService
  ) {}

  private subscriptions = new Map<ReplaySubject<GeneralKpi | Statistic>, Subscription>([
    [this._filesStatSubject, null],
    [this._refundAppsStatSubject, null],
    [this._acdmsStatSubject, null],
    [this._pbdsKpiSubject, null],
    [this._remittancesKpiSubject, null],
    [this._rejectedDocsKpiSubject, null]
  ]);

  public getAirlineIataCode(): Observable<string> {
    return this.store.pipe(
      select(isAirlineUser),
      filter(isAirline => !!isAirline),
      switchMap(() =>
        this.store.pipe(
          select(getUserIataDetails),
          map(userDetails => userDetails && userDetails.iataCode)
        )
      )
    );
  }

  public getGeneralKpis(): Observable<GeneralKpi[]> {
    return this.userType$.pipe(
      switchMap(userType => {
        let kpis$ = of([]);

        switch (userType) {
          case UserType.IATA:
            kpis$ = this.getKpisForIATA();
            break;
          case UserType.AIRLINE:
            kpis$ = this.getKpisForAirline();
            break;
          case UserType.AGENT:
          case UserType.AGENT_GROUP:
            kpis$ = this.getKpisForAgent();
            break;
          case UserType.GDS:
            kpis$ = this.getKpisForGds();
            break;
        }

        return kpis$.pipe(map(stats => mapToKpi(stats)));
      })
    );
  }

  public getAcdmKpis(bspIds?: Array<string>): Observable<GeneralKpi[]> {
    const baseUrl = `${this.appConfigurationService.baseApiPath}/acdm-management/adms/kpis`;
    const params = { ...(bspIds !== undefined && { bspId: bspIds }) };

    return this.http.get<AcdmKpi>(baseUrl, { params }).pipe(
      map(acdmKpis =>
        Object.keys(acdmKpis)
          // We filter KPIs coming from BE that are not implemented in FE yet
          .filter(key => AcdmKpiListIdMapper[key])
          .map<GeneralKpi>((key: keyof AcdmKpi) => ({
            originalKey: key,
            name: `DASHBOARD.kpis.acdm.${key}.name`,
            description: `DASHBOARD.kpis.acdm.${key}.description`,
            value: acdmKpis[key],
            // List to navigate on See All click
            listId: AcdmKpiListIdMapper[key].listId,
            // Permission to view this list
            permission: AcdmKpiListIdMapper[key].permissionId
          }))
      )
    );
  }

  public getRefundKpis(bspIds?: Array<string>): Observable<GeneralKpi[]> {
    const baseUrl = `${this.appConfigurationService.baseApiPath}/refund-management/refund-applications/kpis`;
    const params = { ...(bspIds !== undefined && { bspId: bspIds }) };

    return this.http.get<RefundKpi>(baseUrl, { params }).pipe(
      map(refundKpis =>
        Object.keys(refundKpis)
          // We filter KPIs coming from BE that are not implemented in FE yet
          .filter(key => RefundKpiListIdMapper[key])
          .map<GeneralKpi>((key: keyof RefundKpi) => ({
            originalKey: key,
            name: `DASHBOARD.kpis.refund.${key}.name`,
            description: `DASHBOARD.kpis.refund.${key}.description`,
            value: refundKpis[key],
            // List to navigate on See All click
            listId: RefundKpiListIdMapper[key].listId,
            // Permission to view this list
            permission: RefundKpiListIdMapper[key].permissionId
          }))
      )
    );
  }

  public getDoughnutChartData(): Observable<Array<StatDoughnutChart> | []> {
    return this.userType$.pipe(
      switchMap(userType => {
        let statsDoughnut = of([]);

        if (userType !== UserType.IATA) {
          statsDoughnut = combineLatest([this.acdmsStat$, this.refundAppsStat$, this.filesStat$]).pipe(
            map(dataSet =>
              dataSet.filter(Boolean).map(data => {
                const statsDoughnutChart = data.stats as any;
                statsDoughnutChart.data = statsDoughnutChart.data.map(pair => ({ key: pair.name, y: pair.value }));

                return statsDoughnutChart as StatDoughnutChart;
              })
            )
          );
        }

        return statsDoughnut;
      })
    );
  }

  public getAdmDisputedData(bspIds?: Array<string>): Observable<BarChartData> {
    const url = `${this.appConfigurationService.baseApiPath}/acdm-management/adms/metrics/disputed`;
    const params = { ...(bspIds !== undefined && { bspId: bspIds }) };

    return this.http.get<DocumentMetrics[]>(url, { params }).pipe(map(metrics => mapDocumentMetrics(metrics)));
  }

  public getAdmTopDisputingAgentsData(bspIds?: Array<string>): Observable<BarChartData> {
    const url = `${this.appConfigurationService.baseApiPath}/acdm-management/adms/metrics/top-disputing-agents`;
    const params = { ...(bspIds !== undefined && { bspId: bspIds }) };

    return this.http
      .get<{ agentCode: string; totalDisputed: number }[]>(url, { params })
      .pipe(map(metrics => mapTopAgentMetrics(metrics)));
  }

  public getRaIssuedData(bspIds?: Array<string>): Observable<BarChartData> {
    const url = `${this.appConfigurationService.baseApiPath}/refund-management/refund-applications/metrics/issued`;
    const params = { ...(bspIds !== undefined && { bspId: bspIds }) };

    return this.http.get<DocumentMetrics[]>(url, { params }).pipe(map(metrics => mapDocumentMetrics(metrics)));
  }

  public getRaTopIssuingAgentsData(bspIds?: Array<string>): Observable<BarChartData> {
    const url = `${this.appConfigurationService.baseApiPath}/refund-management/refund-applications/metrics/top-issuing-agents`;
    const params = { ...(bspIds !== undefined && { bspId: bspIds }) };

    return this.http
      .get<{ agentCode: string; totalIssued: number }[]>(url, { params })
      .pipe(map(metrics => mapTopAgentMetrics(metrics)));
  }

  public getGrossSalesAirlineIndustryData(filterValue: SalesDashboardFilter): Observable<BarChartData> {
    const url = `${this.appConfigurationService.baseApiPath}/globalsearch/sales/metrics/gross-sales/airlines`;
    const params = this.formatSalesFilterParams(filterValue);

    return this.http
      .get<GrossSalesAirlineIndustryMetrics[]>(url, { params })
      .pipe(map(metrics => mapGrossSalesMetrics(metrics)));
  }

  public getTopGrossSalesAgentsData(filterValue: SalesDashboardFilter): Observable<BarChartData> {
    const url = `${this.appConfigurationService.baseApiPath}/globalsearch/sales/metrics/gross-sales/top-agents`;
    const params = this.formatSalesFilterParams(filterValue);

    return this.http
      .get<{ agentCode: string; totalAmount: number }[]>(url, { params })
      .pipe(map(metrics => mapTopAgentMetrics(metrics)));
  }

  public getUserNews(): Observable<IataMessageNews> {
    const baseUrl = `${this.appConfigurationService.baseApiPath}/iatamessages-management/iatamessages`;

    return this.http.get<IataMessageNews>(baseUrl).pipe(
      map(
        userNews =>
          ({
            data: userNews.data.map(userNew => ({ message: this.sanitizer.bypassSecurityTrustHtml(userNew.message) }))
          } as any)
      ),
      catchError(emptyErrorHandler)
    );
  }

  // TODO Mocked for the moment
  public getLatestSearches(): Observable<Array<SearchMock>> {
    return of([
      {
        title: 'ADMs Pending Billing',
        date: new Date(),
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Issue Date from 01/01/2019']
      },
      {
        title: 'ADMs Dispute Rejected',
        date: new Date(),
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Reporting Date to 25/11/2018']
      },
      {
        title: 'Refund Application Authorized',
        date: new Date(),
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Accept Date from 01/08/2019 to 31/08/2019']
      },
      {
        title: 'Refund Application Rejected',
        date: new Date(),
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Issue Date from 01/01/2019']
      }
    ]);
  }

  public getExternalLinks(airlineCode: string): Array<ExternalNavigationLink> {
    const admInsightUrl: string = ADM_INSIGHT_URLS[airlineCode];

    const netRemitLink: ExternalNavigationLink = {
      url: EXTERNAL_LINKS.GNR5_URL,
      linkText: this.translationService.translate('DASHBOARD.sections.externalLinks.linkNetRemitText'),
      message: this.translationService.translate('DASHBOARD.sections.externalLinks.linkNetRemitMessage')
    };

    const admInsightLink: ExternalNavigationLink = {
      url: admInsightUrl,
      linkText: this.translationService.translate('DASHBOARD.sections.externalLinks.linkADMInsightText'),
      message: this.translationService.translate('DASHBOARD.sections.externalLinks.linkADMInsightMessage')
    };

    return admInsightUrl ? [netRemitLink, admInsightLink] : [netRemitLink];
  }

  private getKpisForAgent(): Observable<Array<GeneralKpi | Statistic>> {
    return combineLatest([this.refundAppsStat$, this.remittancesKpi$, this.acdmsStat$, this.rejectedDocsKpi$]);
  }

  private getKpisForAirline(): Observable<Array<GeneralKpi | Statistic>> {
    return combineLatest([this.refundAppsStat$, this.acdmsStat$, this.pbdsKpi$, this.rejectedDocsKpi$]);
  }

  private getKpisForGds(): Observable<Array<GeneralKpi | Statistic>> {
    return combineLatest([this.acdmsStat$]);
  }

  /**
   This is a combinelatest of one to prepare the method for future stats that can be added for IATA users
   */
  private getKpisForIATA(): Observable<Array<GeneralKpi | Statistic>> {
    return combineLatest([this.refundAppsStat$]);
  }

  private formatSalesFilterParams(filterValue: SalesDashboardFilter): { [key: string]: string | string[] } {
    const { currency, bsp } = filterValue;

    return {
      ...(currency && { currencyCode: currency.code }),
      ...(bsp && { bspId: bsp.map(value => value.id.toString()) })
    };
  }

  private fetchData(subject: ReplaySubject<GeneralKpi | Statistic>, fetchFunction: (DashboardService) => Subscription) {
    const sub = this.subscriptions.get(subject);
    if (sub == null) {
      this.subscriptions.set(subject, fetchFunction(this));
    }
  }

  private fetchFilesStat(service: DashboardService): Subscription {
    const baseUrl = `${service.appConfigurationService.baseApiPath}/file-management/files/statistics`;

    return service.http
      .get<Statistic>(baseUrl)
      .pipe(
        map(stat => addListId(stat, FILES_ID)),
        catchError(emptyErrorHandler)
      )
      .subscribe(stats => {
        service._filesStatSubject.next(stats);
      });
  }

  private fetchRefundAppsStat(service: DashboardService): Subscription {
    const baseUrl = `${service.appConfigurationService.baseApiPath}/refund-management/refund-applications/statistics`;

    return service.http
      .get<Statistic>(baseUrl)
      .pipe(
        map(stat => addListId(stat, MasterDataType.RefundApp)),
        catchError(emptyErrorHandler)
      )
      .subscribe(stats => {
        service._refundAppsStatSubject.next(stats);
      });
  }

  private fetchAcdmsStat(service: DashboardService): Subscription {
    const baseUrl = `${service.appConfigurationService.baseApiPath}/acdm-management/acdms/statistics`;

    return service.http
      .get<Statistic>(baseUrl)
      .pipe(
        map(stat => addListId(stat, MasterDataType.Adm)),
        catchError(emptyErrorHandler)
      )
      .subscribe(stats => {
        service._acdmsStatSubject.next(stats);
      });
  }

  private fetchPbdsStat(service: DashboardService): Subscription {
    const baseUrl = `${service.appConfigurationService.baseApiPath}/post-billing-dispute/pbds/statistics`;

    return service.http
      .get<GeneralKpi>(baseUrl)
      .pipe(
        map(kpi => ({ ...kpi, listId: PBD_ID })),
        catchError(emptyErrorHandler)
      )
      .subscribe(stat => {
        service._pbdsKpiSubject.next(stat);
      });
  }

  private fetchRemittanceData(service: DashboardService): Subscription {
    return forkJoin([
      service.fetchRemittanceReportsStat(service),
      service.fetchRemittanceFileTypes(service)
    ]).subscribe();
  }

  private fetchRemittanceReportsStat(service: DashboardService): Observable<GeneralKpi> {
    const baseUrl = `${service.appConfigurationService.baseApiPath}/file-management/files/remittance`;

    return service.http.get<GeneralKpi>(baseUrl).pipe(
      map(kpi => ({ ...kpi, listId: FILES_ID })),
      tap(stat => service._remittancesKpiSubject.next(stat)),
      catchError(emptyErrorHandler)
    );
  }

  private fetchRemittanceFileTypes(service: DashboardService): Observable<{ descriptor: string; description: string }> {
    const baseUrl = `${service.appConfigurationService.baseApiPath}/file-management/remittance-file-types`;

    return service.store.select(getUserBsps).pipe(
      filter(bsps => !!bsps.length),
      first(),
      switchMap(bsps =>
        service.http
          .get<{ descriptor: string; description: string }>(`${baseUrl}?bspId=${bsps[0].id}`)
          .pipe(catchError(emptyErrorHandler))
      ),
      tap(fileTypes =>
        service.remittancesFileTypesSubject.next(fileTypes?.map(type => ({ ...type, id: type.descriptor })))
      )
    );
  }

  private fetchDocRejectedStat(service: DashboardService): Subscription {
    const baseUrl = `${service.appConfigurationService.baseApiPath}/documents/rejected/statistics`;

    return service.http
      .get<GeneralKpi>(baseUrl)
      .pipe(catchError(emptyErrorHandler))
      .subscribe(stat => {
        stat.listId = REJECTED_DOCUMENTS_ID;
        stat.permission = Permissions.readRejectedDocuments;

        service._rejectedDocsKpiSubject.next(stat);
      });
  }
}
