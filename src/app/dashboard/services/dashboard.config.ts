import { AcdmKpi, RefundKpi } from '../models/dashboard.model';
import { getViewAcdmPermission } from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { getRefundViewPermission } from '~app/refund/shared/helpers/refund-permissions.config';

export const FILES_ID = 'files';
export const REJECTED_DOCUMENTS_ID = 'rejected-documents';
export const PBD_ID = 'pbds';
export const AIRLINES_ID = 'airlines';
export const AGENTS_ID = 'agents';

// Each Kpi will redirect to a list and we also include the permission required to view this list
// All Kpis must be included in this list, otherwise it will not show up in the dashboard
export const AcdmKpiListIdMapper: { [key in keyof AcdmKpi]: { listId: string; permissionId: string } } = {
  totalDisputed: { listId: MasterDataType.Adm, permissionId: getViewAcdmPermission(MasterDataType.Adm) },
  totalUrgentDisputed: { listId: MasterDataType.Adm, permissionId: getViewAcdmPermission(MasterDataType.Adm) }
};

// Each Kpi will redirect to a list and we also include the permission required to view this list
// All Kpis must be included in this list, otherwise it will not show up in the dashboard
export const RefundKpiListIdMapper: { [key in keyof RefundKpi]: { listId: string; permissionId: string } } = {
  totalPending: { listId: MasterDataType.RefundApp, permissionId: getRefundViewPermission(MasterDataType.RefundApp) },
  totalFinalPending: {
    listId: MasterDataType.RefundApp,
    permissionId: getRefundViewPermission(MasterDataType.RefundApp)
  },
  totalUrgentFinalPending: {
    listId: MasterDataType.RefundApp,
    permissionId: getRefundViewPermission(MasterDataType.RefundApp)
  },
  totalResubmitted: {
    listId: MasterDataType.RefundApp,
    permissionId: getRefundViewPermission(MasterDataType.RefundApp)
  }
};
