import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import moment from 'moment-mini';
import { Observable, of } from 'rxjs';
import { first } from 'rxjs/operators';
import { SEARCH_FORM_DEFAULT_VALUE as ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE } from '~app/adm-acm/components/adm-acm-list/adm-acm-list.constants';
import { AdmAcmStatus } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { ViewListActions } from '~app/core/actions';
import { getMenuTabs } from '~app/core/reducers';
import { GeneralKpi, NavigationData, QuickLink, StatDoughnutChart } from '~app/dashboard/models/dashboard.model';
import { ErrorLevel } from '~app/document/rejected-documents/models/error-level.enum';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { SEARCH_FORM_DEFAULT_VALUE as REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE } from '~app/refund/components/refund-list/refund-list.constants';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { RefundFinal } from '~app/refund/models/refund.model';
import { Permissions } from '~app/shared/constants';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { TabService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import {
  AGENT_QUICK_LINKS,
  AIRLINE_QUICK_LINKS,
  COMMON_QUICK_LINKS,
  DPC_QUICK_LINKS,
  GDS_QUICK_LINKS,
  LIST_INFO
} from './dashboard-navigation.config';
import { FILES_ID, PBD_ID, REJECTED_DOCUMENTS_ID } from './dashboard.config';
import { DashboardService } from './dashboard.service';

@Injectable()
export class DashboardNavigationService {
  public userType: UserType;
  public user: User;
  public hasLeanPermission: boolean;

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private dashboardService: DashboardService,
    private tabService: TabService,
    private localBspTimeService: LocalBspTimeService
  ) {
    this.store.pipe(select(getUser), first()).subscribe(user => {
      this.userType = user.userType;
      this.user = user;

      // Permissions
      this.hasLeanPermission = user.permissions.some(perm => perm === Permissions.lean);
    });
  }

  public navigateByKpi(kpi: GeneralKpi): void {
    const navigationData = this.getKpiNavigationData(kpi.listId);

    if (navigationData) {
      this.navigateToList(navigationData);
    }
  }

  public navigateBySubtabKpi(kpi: GeneralKpi, bspsSelected: BspDto[]): void {
    const navigationData = this.getSubtabKpiNavigationData(kpi, bspsSelected);

    if (navigationData) {
      this.navigateToList(navigationData);
    }
  }

  public navigateByStat(stat: StatDoughnutChart) {
    const navigationData = this.getStatNavigationData(stat);

    if (navigationData) {
      this.navigateToList(navigationData);
    }
  }

  public navigateByQuickLink(quickLink: QuickLink) {
    this.navigateToList(quickLink.navigationData);
  }

  public getKpiNavigationData(listId: string): NavigationData {
    const listInfo = LIST_INFO[listId];

    switch (listId) {
      case MasterDataType.RefundApp:
        return {
          ...listInfo,
          ...this.getRefundAppFilterData()
        };
      case MasterDataType.Adm:
        return {
          ...listInfo,
          ...this.getADMsFilterData()
        };
      case REJECTED_DOCUMENTS_ID:
        return {
          ...listInfo,
          ...this.getRejectedQueryFilter()
        };
      case FILES_ID:
        return {
          ...listInfo,
          ...this.getFilesQueryFilter()
        };
      case PBD_ID:
        return {
          ...listInfo,
          ...this.getPbdQueryFilter()
        };
    }

    return null;
  }

  // TODO If there were KPIs from different endpoints with the same name, we would have to check kpi.listId as well
  public getSubtabKpiNavigationData(kpi: GeneralKpi, bspsSelected: BspDto[]): NavigationData {
    const listInfo = LIST_INFO[kpi.listId];
    let filter;
    let value;

    const acdmStatusDisputed = [AdmAcmStatus.disputed];

    switch (kpi.originalKey) {
      case 'totalDisputed':
        filter = { acdmStatus: acdmStatusDisputed };
        value = { ...ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE, acdmStatus: acdmStatusDisputed };
        break;

      case 'totalUrgentDisputed': {
        const disputeDate = this.getDateIntervalFilterValue(100, 51);

        filter = { acdmStatus: acdmStatusDisputed, disputeDate };
        value = { ...ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE, acdmStatus: acdmStatusDisputed, disputeDate };
        break;
      }

      case 'totalPending': {
        const status = [RefundStatus.pending, RefundStatus.resubmitted];
        filter = { status };
        value = { ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE, status };
        break;
      }

      case 'totalFinalPending': {
        const totalFinalPendingStatus = [RefundStatus.pending, RefundStatus.resubmitted];
        filter = { status: totalFinalPendingStatus, finalType: [RefundFinal.Final] };
        value = {
          ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE,
          status: totalFinalPendingStatus,
          finalType: [RefundFinal.Final]
        };
        break;
      }

      case 'totalUrgentFinalPending': {
        const totalUrgentFinalPendingStatus = [RefundStatus.pending, RefundStatus.resubmitted];

        filter = {
          status: totalUrgentFinalPendingStatus,
          finalType: [RefundFinal.Final],
          finalDaysLeftRange: [0, 9]
        };
        value = {
          ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE,
          ...filter
        };
        break;
      }

      case 'totalResubmitted': {
        const resubmittedStatus = [RefundStatus.resubmitted];

        filter = {
          status: resubmittedStatus
        };
        value = {
          ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE,
          ...filter
        };
        break;
      }

      default:
        return null;
    }

    if (bspsSelected?.length) {
      filter = { ...filter, bsp: bspsSelected };
      value = { ...value, bsp: bspsSelected };
    }

    return { ...listInfo, filter, value };
  }

  public getRefundAppFilterData() {
    let filter = null;
    let value = { ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE };

    switch (this.userType) {
      case UserType.AIRLINE:
      case UserType.IATA: {
        const dateOfApplication = this.getLastDaysFilterValue(7);

        if (this.hasLeanPermission) {
          const bsp = [this.getUserDefaultBsp()];

          filter = { bsp };
          value = { ...value, bsp };
        }

        filter = { ...filter, dateOfApplication };
        value = { ...value, dateOfApplication };

        return { filter, value };
      }
      case UserType.AGENT:
      case UserType.AGENT_GROUP: {
        const status = [RefundStatus.rejected];
        const dateOfRejection = this.getLastDaysFilterValue(7);

        filter = { status, dateOfRejection };
        value = { ...value, status, dateOfRejection };

        return { filter, value };
      }
    }

    return null;
  }

  public getADMsFilterData() {
    let filter = null;
    let value = { ...ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE };

    switch (this.userType) {
      case UserType.AIRLINE:
      case UserType.GDS: {
        const acdmStatus = [AdmAcmStatus.disputed];
        const disputeDate = this.getLastDaysFilterValue(7);

        if (this.hasLeanPermission) {
          const bsp = [this.getUserDefaultBsp()];

          filter = { bsp };
          value = { ...value, bsp };
        }

        filter = { ...filter, disputeDate, acdmStatus };
        value = { ...value, disputeDate, acdmStatus };

        return { filter, value };
      }
      case UserType.AGENT:
      case UserType.AGENT_GROUP: {
        const dateOfIssue = this.getLastDaysFilterValue(7);

        filter = { dateOfIssue };
        value = { ...value, dateOfIssue };

        return { filter, value };
      }
    }

    return null;
  }

  /** 'See All' links must set NavigationData filter and value properties to null to be properly cleaned by list-subtabs abstract class */
  public getStatNavigationData(stat: StatDoughnutChart): NavigationData {
    const listInfo = LIST_INFO[stat.listId];

    return listInfo ? { ...listInfo, filter: null, value: null } : null;
  }

  // TODO Mocked for now. Expected to be returned by API
  public getQuickLinks(): Observable<Array<QuickLink>> {
    let result: Array<QuickLink>;

    switch (this.userType) {
      case UserType.AIRLINE:
        result = [...COMMON_QUICK_LINKS, ...AIRLINE_QUICK_LINKS];
        break;
      case UserType.AGENT:
      case UserType.AGENT_GROUP:
        result = [...COMMON_QUICK_LINKS, ...AGENT_QUICK_LINKS];
        break;
      case UserType.GDS:
        result = GDS_QUICK_LINKS;
        break;
      case UserType.DPC:
        result = DPC_QUICK_LINKS;
        break;
      default:
        result = COMMON_QUICK_LINKS;
        break;
    }

    return of(result);
  }

  private navigateToList({ url, viewListId, filter, value }) {
    this.store.pipe(select(getMenuTabs), first()).subscribe(tabs => {
      const tabToClose = tabs.find(tab => tab.tabLabel === viewListId.split(':')[0]);

      if (tabToClose) {
        this.tabService.closeTab(tabToClose);
      }

      /** If filter is null, subtab query filter will be removed and updated to defaultQuery */
      if (viewListId && (filter === null || filter)) {
        this.store.dispatch(new ViewListActions.FilterChange({ viewListId, filter }));
        this.store.dispatch(new ViewListActions.FilterFormValueChange({ viewListId, value }));
      }

      if (url) {
        this.router.navigateByUrl(url);
      }
    });
  }

  private getRejectedQueryFilter() {
    const filter = { errorLevel: [ErrorLevel.Rejection, ErrorLevel.Modify] };

    return { filter };
  }

  private getFilesQueryFilter() {
    const uploadDate = this.getLastDaysFilterValue(7);
    let filter: any = { uploadDate };

    const fileTypes = this.dashboardService.remittancesFileTypesSubject.value;
    if (fileTypes && fileTypes.length) {
      filter = { ...filter, type: fileTypes };
    }

    return { filter };
  }

  private getPbdQueryFilter() {
    const creationDate = this.getLastDaysFilterValue(7);
    const filter = {
      creationDate,
      ...(this.hasLeanPermission && { bsp: [this.getUserDefaultBsp()] })
    };

    return { filter };
  }

  private getLastDaysFilterValue(lastDays: number): Date[] {
    return this.getDateIntervalFilterValue(lastDays, 0);
  }

  private getDateIntervalFilterValue(startNumberDays: number, endNumberDays?: number): Date[] {
    const currentDate = this.localBspTimeService.getDate();
    let endDate;

    const startDate = moment(currentDate).subtract(startNumberDays, 'days').startOf('day').toDate();
    if (!isNaN(endNumberDays)) {
      endDate = moment(currentDate).subtract(endNumberDays, 'days').endOf('day').toDate();
    }

    return endDate ? [startDate, endDate] : [startDate];
  }

  private getUserDefaultBsp(): Bsp {
    return this.user.bsps.find(bsp => bsp.isoCountryCode === this.user.defaultIsoc);
  }
}
