import { fakeAsync, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import moment from 'moment-mini';
import { of } from 'rxjs';

import { StatDoughnutChart } from '../models/dashboard.model';
import { DashboardService } from './dashboard.service';
import { ViewListActions } from '~app/core/actions';
import { DashboardNavigationService } from '~app/dashboard/services/dashboard-navigation.service';
import { Permissions } from '~app/shared/constants';
import { UserType } from '~app/shared/models/user.model';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

const expectedEndDate = moment().endOf('day').toDate();

const expectedStartDate = moment().startOf('day').subtract(7, 'days').toDate();

const kpiMockRefund = {
  name: 'Refund Applications',
  description: 'Issued in the last 7 days',
  value: 5,
  listId: 'refund-application'
};

const statMock = {
  title: 'ADMs',
  agregator: 'Status',
  total: 6,
  data: [
    { key: 'Billed', y: 2 },
    { key: 'Pending', y: 2 },
    { key: 'Disputed', y: 0 },
    { key: 'Approved Dispute', y: 2 }
  ],
  listId: 'adm'
};

const quickLinkMock = {
  title: 'Pending ADMs',
  navigationData: {
    viewListId: 'MENU.ACDMS.QUERY.adm:default',
    url: '/acdms/adm/query',
    filter: { acdmStatus: ['PENDING'] },
    value: {
      bspId: null,
      ticketDocumentNumber: '',
      airlineIataCode: null,
      agentId: null,
      acdmStatus: ['PENDING'],
      currencyId: null,
      concernsIndicator: null,
      dateOfIssue: null,
      reportingDate: null,
      sentToDpc: null
    }
  }
};

describe('DashboardNavigationService', () => {
  const spyRouter = createSpyObject(Router);
  let service: DashboardNavigationService;
  let mockStore: MockStore;
  let localBspTimeServiceMock: SpyObject<LocalBspTimeService>;
  let clock;
  const now = new Date('2020-10-20');
  const initialState = {
    auth: {
      user: {
        userType: UserType.AIRLINE,
        permissions: [Permissions.lean],
        defaultIsoc: 'ES',
        bsps: [
          { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2021-01-01' },
          { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2021-01-01' }
        ]
      }
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        DashboardNavigationService,
        { provide: DashboardService, useValue: { remittancesFileTypesSubject: { value: null } } },
        { provide: Router, useValue: spyRouter },
        provideMockStore({ initialState }),
        mockProvider(LocalBspTimeService)
      ]
    })
  );

  beforeEach(fakeAsync(() => {
    service = TestBed.inject(DashboardNavigationService);
    mockStore = TestBed.inject(MockStore);
    localBspTimeServiceMock = TestBed.inject<any>(LocalBspTimeService);
    localBspTimeServiceMock.getLocalBspTime.and.returnValue(of({ id: 3, localTime: new Date('2020-03-23') }));
    localBspTimeServiceMock.getDate.and.callFake(date => date);
    clock = jasmine.clock();
    clock.mockDate(now);
    clock.install();
  }));

  afterEach(() => {
    sessionStorage.clear();
    clock.uninstall();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getLastDaysFilterValue should return array of dates for the last 7 days', () => {
    const lasSevenDatesValue = service['getLastDaysFilterValue'](7);
    expect(lasSevenDatesValue[1]).toEqual(expectedEndDate);
    expect(lasSevenDatesValue[0]).toEqual(expectedStartDate);
  });

  describe('navigateByKpi', () => {
    it('should call navigateByUrl to refund list', () => {
      service.navigateByKpi(kpiMockRefund);

      expect(spyRouter.navigateByUrl).toHaveBeenCalledWith('/refunds/app/query');
    });
  });

  describe('navigateByStat', () => {
    it('should call navigateByUrl', () => {
      service.navigateByStat(statMock);

      expect(spyRouter.navigateByUrl).toHaveBeenCalledWith('/acdms/adm/query');
    });
  });

  describe('navigateByQuickLink', () => {
    it('should call navigateByUrl', () => {
      service.navigateByQuickLink(quickLinkMock);

      expect(spyRouter.navigateByUrl).toHaveBeenCalledWith('/acdms/adm/query');
    });
  });

  describe('navigateToList', () => {
    it('should change state filter to null for query reset if navigation data is obtained by stat', () => {
      const navigationData = service.getStatNavigationData(statMock);

      spyOn(mockStore, 'dispatch');

      service['navigateToList'](navigationData);

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterChange({ viewListId: navigationData.viewListId, filter: null })
      );
      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterFormValueChange({ viewListId: navigationData.viewListId, value: null })
      );
    });
  });

  describe('getKpiNavigationData', () => {
    it('should return null, if listId does not exist in configuration', () => {
      const navigationData = service.getKpiNavigationData('random');

      expect(navigationData).toBe(null);
    });

    it('should return refund-application list data', () => {
      const navigationData = service.getKpiNavigationData('refund-application');

      expect(navigationData.viewListId).toBe('MENU.REFUNDS.QUERY.app:default');
      expect(navigationData.url).toBe('/refunds/app/query');
    });

    it('should return adm list data', () => {
      const navigationData = service.getKpiNavigationData('adm');

      expect(navigationData.viewListId).toBe('MENU.ACDMS.QUERY.adm:default');
      expect(navigationData.url).toBe('/acdms/adm/query');
    });

    it('should return rejected-documents list data', () => {
      const navigationData = service.getKpiNavigationData('rejected-documents');

      expect(navigationData.viewListId).toBe('MENU.documents.rejectedDocuments.tabLabel:default');
      expect(navigationData.url).toBe('/documents/rejected');
    });

    it('should return files list data', () => {
      const navigationData = service.getKpiNavigationData('files');

      expect(navigationData.viewListId).toBe('MENU.files.reports.tabLabel:inbox');
      expect(navigationData.url).toBe('/files/reports-communications/reports');
    });
  });

  describe('getStatNavigationData', () => {
    it('should return null, if listId does not exist in configuration', () => {
      const navigationData = service.getStatNavigationData({ listId: 'random' } as StatDoughnutChart);

      expect(navigationData).toBe(null);
    });

    it('should return refund-application list data', () => {
      const navigationData = service.getStatNavigationData({ listId: 'refund-application' } as StatDoughnutChart);

      expect(navigationData.viewListId).toBe('MENU.REFUNDS.QUERY.app:default');
      expect(navigationData.url).toBe('/refunds/app/query');
    });

    it('should return files list data', () => {
      const navigationData = service.getStatNavigationData({ listId: 'files' } as StatDoughnutChart);

      expect(navigationData.viewListId).toBe('MENU.files.reports.tabLabel:inbox');
      expect(navigationData.url).toBe('/files/reports-communications/reports');
    });
  });

  describe('getRefundAppFilterData', () => {
    it('should return null, if no userType', () => {
      service.userType = null;
      const filterData = service.getRefundAppFilterData();

      expect(filterData).toBe(null);
    });

    it('should return filterData for Agent userType', () => {
      service.userType = UserType.AGENT;
      const { status, dateOfRejection } = service.getRefundAppFilterData().filter;

      expect(status).toEqual(['REJECTED']);
      expect(dateOfRejection).toEqual([expectedStartDate, expectedEndDate]);
    });

    it('should return filterData for Airline userType', () => {
      service.userType = UserType.AIRLINE;
      const { dateOfApplication } = service.getRefundAppFilterData().filter;

      expect(dateOfApplication).toEqual([expectedStartDate, expectedEndDate]);
    });
  });

  describe('getADMsFilterData', () => {
    it('should return null, if no userType', () => {
      service.userType = null;
      const filterData = service.getADMsFilterData();

      expect(filterData).toBe(null);
    });

    it('should return filterData for Agent userType', () => {
      service.userType = UserType.AGENT;
      const { dateOfIssue } = service.getADMsFilterData().filter;

      expect(dateOfIssue).toEqual([expectedStartDate, expectedEndDate]);
    });

    it('should return filterData for Airline userType', () => {
      service.userType = UserType.AIRLINE;
      const { disputeDate, acdmStatus } = service.getADMsFilterData().filter;

      expect(disputeDate).toEqual([expectedStartDate, expectedEndDate]);
      expect(acdmStatus).toEqual(['DISPUTED']);
    });
  });

  // TODO: test not LEAN permission branch
  it('should return PBD filter with BSP if user has LEAN permission', () => {
    const result: any = {
      filter: jasmine.objectContaining({
        bsp: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2021-01-01' }]
      })
    };

    spyOn<any>(service, 'getLastDaysFilterValue').and.returnValue([new Date()]);

    expect(service['getPbdQueryFilter']()).toEqual(result);
  });
});
