import { HttpClient } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { DocumentMetrics } from '../models/dashboard.model';
import { DashboardService } from '~app/dashboard/services/dashboard.service';
import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { AppConfigurationService } from '~app/shared/services';

describe('DashboardService', () => {
  let service: DashboardService;
  let httpClientSpy: SpyObject<HttpClient>;

  const initialState = {
    auth: {
      user: createAirlineUser()
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DashboardService,
        provideMockStore({ initialState }),
        mockProvider(HttpClient),
        mockProvider(L10nTranslationService),
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    });

    service = TestBed.inject(DashboardService);
    httpClientSpy = TestBed.inject<any>(HttpClient);
    httpClientSpy.get.and.returnValue(of({}));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get user news', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(of({ data: [] }));
    const url = '/iatamessages-management/iatamessages';
    service.getUserNews().subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith(url);
  }));

  it('should return airline user iata code', fakeAsync(() => {
    let iataCode;
    service.getAirlineIataCode().subscribe(airlineIataCode => (iataCode = airlineIataCode));
    tick();

    expect(iataCode).toEqual('623');
  }));

  it('should return latest searches', fakeAsync(() => {
    let result;
    const expectedResult = [
      jasmine.objectContaining({
        title: 'ADMs Pending Billing',
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Issue Date from 01/01/2019']
      }),
      jasmine.objectContaining({
        title: 'ADMs Dispute Rejected',
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Reporting Date to 25/11/2018']
      }),
      jasmine.objectContaining({
        title: 'Refund Application Authorized',
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Accept Date from 01/08/2019 to 31/08/2019']
      }),
      jasmine.objectContaining({
        title: 'Refund Application Rejected',
        tags: ['Bsp - ES', 'Airline Code - 623', 'Currency - EUR', 'Issue Date from 01/01/2019']
      })
    ];

    service.getLatestSearches().subscribe(latestSearches => (result = latestSearches));

    tick();

    expect(result).toEqual(expectedResult);
  }));

  it('should get kpis for airline if it is airline user', fakeAsync(() => {
    let a = 0;
    spyOn(DashboardService.prototype as any, 'getKpisForAirline').and.callFake(() => {
      a = 1;

      return of([]);
    });

    service.getGeneralKpis().subscribe();
    tick();

    expect(a).toBe(1);
  }));

  it('should get mapped ADM disputed bar chart data', fakeAsync(() => {
    let result: BarChartData;
    const mockDocumentMetrics: DocumentMetrics[] = [
      {
        yearMonth: '2021-11',
        totals: [
          { status: 'DISPUTE_APPROVED', total: 34 },
          { status: 'DISPUTED', total: 3 }
        ]
      },
      {
        yearMonth: '2021-12',
        totals: [
          { status: 'DISPUTE_REJECTED', total: 15 },
          { status: 'DISPUTED', total: 14 }
        ]
      }
    ];

    httpClientSpy.get.and.returnValue(of(mockDocumentMetrics));

    service.getAdmDisputedData().subscribe(data => (result = data));
    tick();

    expect(result).toEqual({
      labels: ['2021-11', '2021-12'],
      values: [
        { key: 'DISPUTE_APPROVED', totals: [34] },
        { key: 'DISPUTED', totals: [3, 14] },
        { key: 'DISPUTE_REJECTED', totals: [0, 15] }
      ]
    });
  }));

  it('should get mapped ADM top disputing 10 agents bar chart data', fakeAsync(() => {
    let result: BarChartData;
    const mockMetrics = [
      {
        agentCode: '34323243124',
        totalDisputed: 14
      },
      {
        agentCode: '23423432',
        totalDisputed: 23
      }
    ];

    httpClientSpy.get.and.returnValue(of(mockMetrics));

    service.getAdmTopDisputingAgentsData().subscribe(data => (result = data));
    tick();

    expect(result).toEqual({
      labels: ['34323243124', '23423432'],
      values: [{ key: 'total', totals: [14, 23] }]
    });
  }));

  it('should get mapped RA issued bar chart data', fakeAsync(() => {
    let result: BarChartData;
    const mockDocumentMetrics: DocumentMetrics[] = [
      {
        yearMonth: '2021-11',
        totals: [
          { status: 'UNDER_INVESTIGATION', total: 34 },
          { status: 'AUTHORIZED', total: 3 }
        ]
      },
      {
        yearMonth: '2021-12',
        totals: [
          { status: 'REJECTED', total: 15 },
          { status: 'AUTHORIZED', total: 14 }
        ]
      }
    ];

    httpClientSpy.get.and.returnValue(of(mockDocumentMetrics));

    service.getRaIssuedData().subscribe(data => (result = data));
    tick();

    expect(result).toEqual({
      labels: ['2021-11', '2021-12'],
      values: [
        { key: 'UNDER_INVESTIGATION', totals: [34] },
        { key: 'AUTHORIZED', totals: [3, 14] },
        { key: 'REJECTED', totals: [0, 15] }
      ]
    });
  }));

  it('should get mapped RA top issuing 10 agents bar chart data', fakeAsync(() => {
    let result: BarChartData;
    const mockMetrics = [
      {
        agentCode: '34323243124',
        totalIssued: 23
      },
      {
        agentCode: '23423432',
        totalIssued: 14
      }
    ];

    httpClientSpy.get.and.returnValue(of(mockMetrics));

    service.getRaTopIssuingAgentsData().subscribe(data => (result = data));
    tick();

    expect(result).toEqual({
      labels: ['34323243124', '23423432'],
      values: [{ key: 'total', totals: [23, 14] }]
    });
  }));
});
