import { QuickLink } from '../models/dashboard.model';
import { AGENTS_ID, AIRLINES_ID, FILES_ID, PBD_ID, REJECTED_DOCUMENTS_ID } from './dashboard.config';
import { SEARCH_FORM_DEFAULT_VALUE as ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE } from '~app/adm-acm/components/adm-acm-list/adm-acm-list.constants';
import { AdmAcmStatus } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { FileTypeDescription, FileTypeDescriptor } from '~app/files/shared/fileTypes.model';
import { FILE_LIST } from '~app/files/shared/reports-comunications-list.constants';
import { AGENTS } from '~app/master-data/agent/agents.constants';
import { AIRLINES_LIST } from '~app/master-data/airline/airline-list.constants';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { SEARCH_FORM_DEFAULT_VALUE_PBD } from '~app/pbd/shared/pbd-mocks.constants';
import { PbdStatus } from '~app/pbd/shared/pbd.model';
import { SEARCH_FORM_DEFAULT_VALUE as REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE } from '~app/refund/components/refund-list/refund-list.constants';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { ROUTES } from '~app/shared/constants';
import { EasyPayStatus, PaymentStatus } from '~app/shared/enums';
import { createTabViewListId } from '~app/shared/helpers/create-tab-view-list-id.helper';

export const LIST_INFO = {
  [MasterDataType.RefundApp]: {
    viewListId: createTabViewListId(ROUTES.REFUNDS_APP_QUERY.tabLabel),
    url: ROUTES.REFUNDS_APP_QUERY.url
  },
  [MasterDataType.Adm]: {
    viewListId: createTabViewListId(ROUTES.ADM_QUERY.tabLabel),
    url: ROUTES.ADM_QUERY.url
  },
  [REJECTED_DOCUMENTS_ID]: {
    viewListId: createTabViewListId(ROUTES.REJECTED_DOCUMENTS.tabLabel),
    url: ROUTES.REJECTED_DOCUMENTS.url
  },
  [FILES_ID]: {
    viewListId: createTabViewListId(ROUTES.REPORTS.tabLabel, 'inbox'),
    url: ROUTES.REPORTS.url
  },
  [PBD_ID]: {
    viewListId: createTabViewListId(ROUTES.POST_BILLING_DISPUTE_LIST.tabLabel),
    url: ROUTES.POST_BILLING_DISPUTE_LIST.url
  },
  [AIRLINES_ID]: {
    viewListId: createTabViewListId(ROUTES.AIRLINE.tabLabel),
    url: ROUTES.AIRLINE.url
  },
  [AGENTS_ID]: {
    viewListId: createTabViewListId(ROUTES.AGENT.tabLabel),
    url: ROUTES.AGENT.url
  }
};

const ADM_QUICK_LINKS: Array<QuickLink> = [
  {
    title: 'DASHBOARD.quickLinks.adms.issued',
    navigationData: {
      viewListId: LIST_INFO[MasterDataType.Adm].viewListId,
      url: LIST_INFO[MasterDataType.Adm].url,
      filter: { acdmStatus: [AdmAcmStatus.pending] },
      value: { ...ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE, acdmStatus: [AdmAcmStatus.pending] }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.adms.disputed',
    navigationData: {
      viewListId: LIST_INFO[MasterDataType.Adm].viewListId,
      url: LIST_INFO[MasterDataType.Adm].url,
      filter: { acdmStatus: [AdmAcmStatus.disputed] },
      value: { ...ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE, acdmStatus: [AdmAcmStatus.disputed] }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.adms.approvedDispute',
    navigationData: {
      viewListId: LIST_INFO[MasterDataType.Adm].viewListId,
      url: LIST_INFO[MasterDataType.Adm].url,
      filter: { acdmStatus: [AdmAcmStatus.approved] },
      value: { ...ADMS_LIST_SEARCH_FORM_DEFAULT_VALUE, acdmStatus: [AdmAcmStatus.approved] }
    }
  }
];

export const COMMON_QUICK_LINKS: Array<QuickLink> = [
  ...ADM_QUICK_LINKS,
  {
    title: 'DASHBOARD.quickLinks.refundApplications.pending',
    navigationData: {
      viewListId: LIST_INFO[MasterDataType.RefundApp].viewListId,
      url: LIST_INFO[MasterDataType.RefundApp].url,
      filter: { status: [RefundStatus.pending] },
      value: { ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE, status: [RefundStatus.pending] }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.refundApplications.rejected',
    navigationData: {
      viewListId: LIST_INFO[MasterDataType.RefundApp].viewListId,
      url: LIST_INFO[MasterDataType.RefundApp].url,
      filter: { status: [RefundStatus.rejected] },
      value: { ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE, status: [RefundStatus.rejected] }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.refundApplications.authorized',
    navigationData: {
      viewListId: LIST_INFO[MasterDataType.RefundApp].viewListId,
      url: LIST_INFO[MasterDataType.RefundApp].url,
      filter: { status: [RefundStatus.authorized] },
      value: { ...REFUND_LIST_SEARCH_FORM_DEFAULT_VALUE, status: [RefundStatus.authorized] }
    }
  }
];

export const AIRLINE_QUICK_LINKS: Array<QuickLink> = [
  {
    title: 'DASHBOARD.quickLinks.pbds.pendingAirline',
    navigationData: {
      viewListId: LIST_INFO[PBD_ID].viewListId,
      url: LIST_INFO[PBD_ID].url,
      filter: { statuses: [PbdStatus.PendingAirline] },
      value: { ...SEARCH_FORM_DEFAULT_VALUE_PBD, statuses: [PbdStatus.PendingAirline] }
    }
  }
];

export const AGENT_QUICK_LINKS: Array<QuickLink> = [
  {
    title: 'DASHBOARD.quickLinks.pbds.pendingAgent',
    navigationData: {
      viewListId: LIST_INFO[PBD_ID].viewListId,
      url: LIST_INFO[PBD_ID].url,
      filter: { statuses: [PbdStatus.PendingAgent] },
      value: { ...SEARCH_FORM_DEFAULT_VALUE_PBD, statuses: [PbdStatus.PendingAgent] }
    }
  }
];

export const DPC_QUICK_LINKS: Array<QuickLink> = [
  {
    title: 'DASHBOARD.quickLinks.reportsComunications.lastHotsLoaded',
    navigationData: {
      viewListId: LIST_INFO[FILES_ID].viewListId,
      url: LIST_INFO[FILES_ID].url,
      filter: {
        type: [{ descriptor: FileTypeDescriptor.HOTErrorsFile, description: FileTypeDescription.HOTErrorsFile }]
      },
      value: {
        ...FILE_LIST.SEARCH_FORM_DEFAULT_VALUE,
        type: [{ descriptor: FileTypeDescriptor.HOTErrorsFile, description: FileTypeDescription.HOTErrorsFile }]
      }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.reportsComunications.lastRetsGenerated',
    navigationData: {
      viewListId: LIST_INFO[FILES_ID].viewListId,
      url: LIST_INFO[FILES_ID].url,
      filter: {
        type: [
          { descriptor: FileTypeDescriptor.BSPlinkRetFile, description: FileTypeDescription.BSPlinkRetFile },
          { descriptor: FileTypeDescriptor.WEBlinkRETFile, description: FileTypeDescription.WEBlinkRETFile }
        ]
      },
      value: {
        ...FILE_LIST.SEARCH_FORM_DEFAULT_VALUE,
        type: [
          { descriptor: FileTypeDescriptor.BSPlinkRetFile, description: FileTypeDescription.BSPlinkRetFile },
          { descriptor: FileTypeDescriptor.WEBlinkRETFile, description: FileTypeDescription.WEBlinkRETFile }
        ]
      }
    }
  }
];

export const GDS_QUICK_LINKS: Array<QuickLink> = [
  ...ADM_QUICK_LINKS,
  // Master data > Airlines quick links
  {
    title: 'DASHBOARD.quickLinks.airlines.easyPayOptin',
    navigationData: {
      viewListId: LIST_INFO[AIRLINES_ID].viewListId,
      url: LIST_INFO[AIRLINES_ID].url,
      filter: { easyPayStatus: EasyPayStatus.OPTIN },
      value: { ...AIRLINES_LIST.SEARCH_FORM_DEFAULT_VALUE, easyPayStatus: EasyPayStatus.OPTIN }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.airlines.easyPayOptout',
    navigationData: {
      viewListId: LIST_INFO[AIRLINES_ID].viewListId,
      url: LIST_INFO[AIRLINES_ID].url,
      filter: { easyPayStatus: EasyPayStatus.OPTOUT },
      value: { ...AIRLINES_LIST.SEARCH_FORM_DEFAULT_VALUE, easyPayStatus: EasyPayStatus.OPTOUT }
    }
  },
  // Master data > Agents quick links
  {
    title: 'DASHBOARD.quickLinks.agents.cashFOP',
    navigationData: {
      viewListId: LIST_INFO[AGENTS_ID].viewListId,
      url: LIST_INFO[AGENTS_ID].url,
      filter: { cash: [PaymentStatus.ACTIVE] },
      value: { ...AGENTS.SEARCH_FORM_DEFAULT_VALUE, cash: [PaymentStatus.ACTIVE] }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.agents.easyPayFOP',
    navigationData: {
      viewListId: LIST_INFO[AGENTS_ID].viewListId,
      url: LIST_INFO[AGENTS_ID].url,
      filter: { easyPay: [PaymentStatus.ACTIVE] },
      value: { ...AGENTS.SEARCH_FORM_DEFAULT_VALUE, easyPay: [PaymentStatus.ACTIVE] }
    }
  },
  {
    title: 'DASHBOARD.quickLinks.agents.paymentCardFOP',
    navigationData: {
      viewListId: LIST_INFO[AGENTS_ID].viewListId,
      url: LIST_INFO[AGENTS_ID].url,
      filter: { paymentCard: [PaymentStatus.ACTIVE] },
      value: { ...AGENTS.SEARCH_FORM_DEFAULT_VALUE, paymentCard: [PaymentStatus.ACTIVE] }
    }
  }
];
