import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { User, UserType } from '~app/shared/models/user.model';
import { SessionTimeService } from '~app/training/services/session-time.service';

@Component({
  selector: 'bspl-welcome-dialog',
  templateUrl: './welcome-dialog.component.html',
  styleUrls: ['./welcome-dialog.component.scss']
})
export class WelcomeDialogComponent implements OnInit, OnDestroy {
  public usersType = UserType;
  public userType: UserType;
  public textLine: string;
  public period: string;
  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public translationService: L10nTranslationService,
    public dialogService: DialogService,
    private store: Store<AppState>,
    private sessionTimeService: SessionTimeService
  ) {}

  ngOnInit() {
    this.period = this.translationService.translate('DASHBOARD.DIALOGS.WELCOME.TXT.LIST.LINE_2', {
      period: this.sessionTimeService.getResetTime()
    });

    this.parseLoggedUser();
    this.onButtonClicked();
  }

  parseLoggedUser() {
    this.store.pipe(select(getUser), takeUntil(this.destroy$)).subscribe(user => {
      this.setUserData(user);
    });
  }

  setUserData(user: User) {
    this.userType = user.userType;

    switch (this.userType) {
      case UserType.AIRLINE:
        this.textLine = this.translationService.translate('DASHBOARD.DIALOGS.WELCOME.TXT.LIST.LINE_1.A', {
          code: user.iataCode,
          id: user.id
        });
        break;
      case UserType.AGENT:
        this.textLine = this.translationService.translate('DASHBOARD.DIALOGS.WELCOME.TXT.LIST.LINE_1.B', {
          code: user.iataCode,
          id: user.id
        });
        break;
      case UserType.IATA:
        this.textLine = this.translationService.translate('DASHBOARD.DIALOGS.WELCOME.TXT.LIST.LINE_1.C', {
          id: user.id
        });
        break;
    }
  }

  public onButtonClicked() {
    this.reactiveSubject.asObservable
      .pipe(
        tap(action => {
          if (action.clickedBtn === FooterButton.GotIt) {
            this.dialogService.close();
            localStorage.setItem('welcomeDialogGotIt', 'true');
          }
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
}
