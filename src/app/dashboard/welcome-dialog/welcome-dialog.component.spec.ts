import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { WelcomeDialogComponent } from './welcome-dialog.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { initialState } from '~app/master-data/periods/shared/periods-mocks';
import { AppState } from '~app/reducers';
import { DialogConfig } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { UserType } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services/notification.service';
import { TranslatePipeMock } from '~app/test';
import { SessionTimeService } from '~app/training/services/session-time.service';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('WelcomeDialogComponent', () => {
  let component: WelcomeDialogComponent;
  let fixture: ComponentFixture<WelcomeDialogComponent>;
  let store: MockStore<AppState>;
  const notificationServiceSpy = createSpyObject(NotificationService);

  const sessionServiceSpy = createSpyObject(SessionTimeService);

  const iataUser = createIataUser();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [WelcomeDialogComponent, TranslatePipeMock],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        DialogConfig,
        ReactiveSubject,
        provideMockStore({ initialState }),
        {
          provide: NotificationService,
          useValue: notificationServiceSpy
        },
        { provide: SessionTimeService, useValue: sessionServiceSpy },

        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeDialogComponent);
    component = fixture.componentInstance;
    store = TestBed.inject<any>(Store);
    store.overrideSelector(getUser, iataUser);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should parse logged user', () => {
    spyOn(component, 'setUserData');

    component.parseLoggedUser();

    expect(component.setUserData).toHaveBeenCalled();
    expect(component.userType).toEqual(UserType.IATA);
  });
});
