import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as acdmDashboardState from '../components/adma-dashboard/store/reducers/acdm-dashboard.reducer';
import * as refundDashboardState from '../components/refund-dashboard/store/reducers/refund-dashboard.reducer';
import * as salesDashboardState from '../components/sales-dashboard/store/reducers/sales-dashboard.reducer';
import { AppState } from '~app/reducers';

export const dashboardFeatureKey = 'dashboard';

export interface DashboardState {
  [acdmDashboardState.acdmDashboardKey]: acdmDashboardState.AcdmDashboardState;
  [refundDashboardState.refundDashboardKey]: refundDashboardState.RefundDashboardState;
  [salesDashboardState.salesDashboardKey]: salesDashboardState.SalesDashboardState;
}

export interface State extends AppState {
  [dashboardFeatureKey]: DashboardState;
}

export function reducers(state: DashboardState | undefined, action: Action) {
  return combineReducers({
    [acdmDashboardState.acdmDashboardKey]: acdmDashboardState.acdmDashboardReducer,
    [refundDashboardState.refundDashboardKey]: refundDashboardState.refundDashboardReducer,
    [salesDashboardState.salesDashboardKey]: salesDashboardState.salesDashboardReducer
  })(state, action);
}

export const getDashboardState = createFeatureSelector<State, DashboardState>(dashboardFeatureKey);

// ACDM dashboard
export const getAcdmDashboard = createSelector(getDashboardState, state => state[acdmDashboardState.acdmDashboardKey]);
export const getAcdmDashboardValue = createSelector(getAcdmDashboard, state => state.value);
export const getAcdmDashboardIsValid = createSelector(getAcdmDashboard, state => state.isValid);

// Refund dashboard
export const getRefundDashboard = createSelector(
  getDashboardState,
  state => state[refundDashboardState.refundDashboardKey]
);
export const getRefundDashboardValue = createSelector(getRefundDashboard, state => state.value);
export const getRefundDashboardIsValid = createSelector(getRefundDashboard, state => state.isValid);

// Sales dashboard
export const getSalesDashboard = createSelector(
  getDashboardState,
  state => state[salesDashboardState.salesDashboardKey]
);
export const getSalesDashboardValue = createSelector(getSalesDashboard, state => state.value);
export const getSalesDashboardIsValid = createSelector(getSalesDashboard, state => state.isValid);
