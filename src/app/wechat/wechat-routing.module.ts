import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WechatListComponent } from './components/wechat-list/wechat-list.component';
import { RegistrationSummaryComponent } from './components/wechat-settings/components/registration-summary/components/registration-summary.component';
import { WechatSettingsComponent } from './components/wechat-settings/components/wechat-settings.component';
import { getWechatSettingsTabLabel } from './components/wechat-settings/helpers/wechat-settings.helper';
import { WechatSettingsResolver } from './components/wechat-settings/resolvers/wechat-settings.resolver';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  { path: '', redirectTo: ROUTES.WECHAT_LIST.url, pathMatch: 'full' },
  {
    path: ROUTES.WECHAT_LIST.path,
    component: WechatListComponent,
    data: { tab: ROUTES.WECHAT_LIST }
  },
  {
    path: ROUTES.WECHAT_SETTINGS.path,
    component: WechatSettingsComponent,
    data: {
      tab: { ...ROUTES.WECHAT_SETTINGS, getTabLabel: getWechatSettingsTabLabel }
    },
    resolve: {
      regNumber: WechatSettingsResolver
    },
    children: [
      {
        path: 'summary',
        component: RegistrationSummaryComponent,
        data: {
          group: 'MENU.WECHAT.settings.menu.registration.groupName',
          title: 'MENU.WECHAT.settings.menu.registration.titles.summary.tabTitle',
          configuration: {
            title: 'MENU.WECHAT.settings.menu.registration.titles.summary.pageTitle',
            permissions: {
              read: Permissions.readWechat
            }
          },
          requiredPermissions: Permissions.readWechat
        },
        runGuardsAndResolvers: 'always'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WechatRoutingModule {}
