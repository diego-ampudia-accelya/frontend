import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { WechatFilters, WechatStatus } from '../models/wechat.model';

import { WechatFilterFormatter } from './wechat-formatter';

describe('WechatFilterFormatter', () => {
  let formatter: WechatFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new WechatFilterFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format registrant if they exist', () => {
    const filter: WechatFilters = {
      registrationRequestNumber: 'Registrant'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['registrationRequestNumber'],
        label: 'WECHAT.filters.weChatId - Registrant'
      }
    ]);
  });

  it('should format status if they exist', () => {
    const filter: WechatFilters = {
      status: WechatStatus.registered
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['status'],
        label: 'WECHAT.filters.status - Registered'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
