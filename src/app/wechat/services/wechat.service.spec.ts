import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { WechatFilters, WechatStatus } from '../models/wechat.model';
import { WechatService } from './wechat.service';
import { DataQuery } from '~app/shared/components/list-view';
import { AppConfigurationService } from '~app/shared/services';

const filterByQuery: WechatFilters = {
  registrationRequestNumber: 'Registrant',
  status: WechatStatus.pending
};

describe('WechatService', () => {
  let wechatService: WechatService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of(httpResponse));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        WechatService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ]
    });

    wechatService = TestBed.inject(WechatService);
  });

  it('should create', () => {
    expect(wechatService).toBeDefined();
  });

  it('should send proper find request', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    const expectedUrl = '/wechat-management/users?page=0&size=20&registrationRequestNumber=Registrant&status=Pending';

    wechatService.find(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));
});
