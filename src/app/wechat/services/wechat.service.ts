import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {
  RegAccessInfo,
  RegistrationSummary
} from '../components/wechat-settings/components/registration-summary/models/registration-summary.model';
import { Wechat, WechatFilters, WechatStatus } from '../models/wechat.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class WechatService implements Queryable<Wechat> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/wechat-management/users`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<WechatFilters>): Observable<PagedData<Wechat>> {
    const formattedQuery = RequestQuery.fromDataQuery(query);

    return this.http.get<PagedData<Wechat>>(this.baseUrl + formattedQuery.getQueryString());
  }

  public getById(id: string): Observable<RegistrationSummary> {
    return this.http.get<RegistrationSummary>(`${this.baseUrl}/${id}`);
  }

  public registerUser(userId: number, registrationReason: string): Observable<RegistrationSummary> {
    return this.http.put<RegistrationSummary>(`${this.baseUrl}/${userId}`, {
      status: WechatStatus.registered,
      registrationReason
    });
  }

  public cancelUser(userId: number, cancellationReason: string): Observable<RegistrationSummary> {
    return this.http.put<RegistrationSummary>(`${this.baseUrl}/${userId}`, {
      status: WechatStatus.cancelled,
      cancellationReason
    });
  }

  public updateRegAccessInfo(userId: string, modifications: Partial<RegAccessInfo>): Observable<RegistrationSummary> {
    return this.http.put<RegistrationSummary>(`${this.baseUrl}/${userId}`, modifications);
  }
}
