import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';

import { WechatStatus } from '../../models/wechat.model';
import { WechatFilterFormatter } from '../../services/wechat-formatter';
import { WechatService } from '../../services/wechat.service';
import { WechatListComponent } from './wechat-list.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { GridTableComponent, InputComponent, SelectComponent } from '~app/shared/components';
import {
  DataQuery,
  DefaultQueryStorage,
  ListViewComponent,
  QueryableDataSource
} from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { User, UserType } from '~app/shared/models/user.model';
import { TranslatePipeMock } from '~app/test';

const query = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery;

const query$ = of(query);

describe('BillingStatementComponent', () => {
  let component: WechatListComponent;
  let fixture: ComponentFixture<WechatListComponent>;

  const weChatServiceSpy = createSpyObject(WechatService);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'agent@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AGENT,
    permissions: [Permissions.readWechat],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readWechat] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        WechatListComponent,
        TranslatePipeMock,
        MockComponents(ListViewComponent, InputComponent, SelectComponent, GridTableComponent)
      ],
      imports: [RouterTestingModule],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getUser, value: expectedUserDetails }]
        }),
        { provide: WechatService, useValue: weChatServiceSpy },
        FormBuilder,
        { provide: QueryableDataSource, useValue: { hasData$: of(true), appliedQuery$: query$, get: () => {} } },
        WechatFilterFormatter,
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    })
      .overrideComponent(WechatListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: { hasData$: of(true), appliedQuery$: query$, get: () => {} } },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(WechatListComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('totalItemsMessage$', () => {
    it('should call translate with total elements', fakeAsync(() => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('10 items');

      component.totalItemsMessage$.subscribe();
      tick();

      expect((component as any).translationService.translate).toHaveBeenCalledWith('WECHAT.totalItemsMessage', {
        total: 10
      });
    }));

    it('should retun string with total elements', fakeAsync(() => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('10 items');
      let res;

      component.totalItemsMessage$.subscribe(str => (res = str));
      tick();

      expect(res).toBe('10 items');
    }));
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      (component as any).initializeDropdowns = jasmine.createSpy();
      (component as any).initializeColumns = jasmine.createSpy();
      (component as any).buildSearchForm = jasmine
        .createSpy()
        .and.returnValue(new FormGroup({ status: new FormControl() }));
      (component as any).queryStorage.get = jasmine.createSpy().and.returnValue(query);
      component.loadData = jasmine.createSpy();
    });

    it('should call initializeDropdowns', () => {
      component.ngOnInit();

      expect((component as any).initializeDropdowns).toHaveBeenCalled();
    });

    it('should call initializeColumns', () => {
      component.ngOnInit();

      expect((component as any).initializeColumns).toHaveBeenCalled();
    });

    it('should call buildSearchForm', () => {
      component.ngOnInit();

      expect((component as any).buildSearchForm).toHaveBeenCalled();
    });

    it('should set searchForm from buildSearchForm', () => {
      component.searchForm = new FormGroup({});

      component.ngOnInit();

      expect(component.searchForm.value).toEqual(new FormGroup({ status: new FormControl() }).value);
    });

    it('should call queryStorage.get', () => {
      component.ngOnInit();

      expect((component as any).queryStorage.get).toHaveBeenCalled();
    });

    it('should call loadData', () => {
      component.ngOnInit();

      expect(component.loadData).toHaveBeenCalledWith(query);
    });
  });

  describe('loadData', () => {
    it('should call dataSource.get with query', () => {
      (component as any).dataSource.get = jasmine.createSpy();

      component.loadData(query);

      expect((component as any).dataSource.get).toHaveBeenCalledWith(query);
    });

    it('should call queryStorage.save with query', () => {
      (component as any).queryStorage.save = jasmine.createSpy();

      component.loadData(query);

      expect((component as any).queryStorage.save).toHaveBeenCalledWith(query);
    });
    it('should call dataSource.get with default query', () => {
      (component as any).dataSource.get = jasmine.createSpy();

      component.loadData(null);

      expect((component as any).dataSource.get).toHaveBeenCalledWith(defaultQuery);
    });

    it('should call queryStorage.save with default  query', () => {
      (component as any).queryStorage.save = jasmine.createSpy();

      component.loadData(null);

      expect((component as any).queryStorage.save).toHaveBeenCalledWith(defaultQuery);
    });
  });

  it('onRowLinkClick - should navigate to wechat settings', () => {
    (component as any).router.navigate = jasmine.createSpy();

    component.onRowLinkClick({ id: 'doc-id', registrationRequestNumber: 'reg-number' });

    expect((component as any).router.navigate).toHaveBeenCalledWith([
      ROUTES.WECHAT_SETTINGS.url,
      'doc-id',
      'reg-number'
    ]);
  });

  describe('buildSearchForm', () => {
    it('should call createGroup', () => {
      (component as any).formFactory.createGroup = jasmine
        .createSpy()
        .and.returnValue(new FormGroup({ status: new FormControl() }));

      (component as any).buildSearchForm();

      expect((component as any).formFactory.createGroup).toHaveBeenCalledWith({
        registrationRequestNumber: [],
        status: []
      });
    });

    it('should return group', () => {
      (component as any).formFactory.createGroup = jasmine
        .createSpy()
        .and.returnValue(new FormGroup({ status: new FormControl() }));

      const res = (component as any).buildSearchForm();

      expect(res.value).toEqual(new FormGroup({ status: new FormControl() }).value);
    });
  });

  describe('initializeColumns', () => {
    it('should call buildColumns', () => {
      (component as any).buildColumns = jasmine.createSpy().and.returnValue([]);

      (component as any).initializeColumns();

      expect((component as any).buildColumns).toHaveBeenCalled();
    });

    it('should set columns', () => {
      const columns = [
        {
          prop: 'accountType',
          name: 'WECHAT.columns.accountType',
          flexGrow: 2
        }
      ];
      component.columns = [];
      (component as any).buildColumns = jasmine.createSpy().and.returnValue(columns);
      (component as any).ngOnInit = jasmine.createSpy();

      (component as any).initializeColumns();

      expect(component.columns).toEqual(columns);
    });
  });

  describe('buildColumns', () => {
    it('should return proper columns', () => {
      const res = (component as any).buildColumns();

      expect(res[0].prop).toBe('registrationRequestNumber');
      expect(res[1].prop).toBe('accountType');
      expect(res[2].prop).toBe('name');
      expect(res[3].prop).toBe('phone');
      expect(res[4].prop).toBe('email');
      expect(res[5].prop).toBe('status');
      expect(res[6].prop).toBe('applicationDate');
      expect(res[7].prop).toBe('processDate');
      expect(res[8].prop).toBe('processUser');
    });

    describe('status badgeInfo', () => {
      let badgeInfo;

      beforeEach(() => {
        (component as any).getBadgeInfoType = jasmine.createSpy().and.returnValue(BadgeInfoType.info);
        (component as any).getTranslatedStatus = jasmine.createSpy().and.returnValue('test label');

        const res = (component as any).buildColumns();
        badgeInfo = res[5].badgeInfo;
      });

      it('hidden - should set hidden', () => {
        let res = badgeInfo.hidden(null);
        expect(res).toBe(true);

        res = badgeInfo.hidden({ id: 'id' });
        expect(res).toBe(false);
      });

      it('type - should call getBadgeInfoType for type with status', () => {
        badgeInfo.type({ status: WechatStatus.pending });

        expect((component as any).getBadgeInfoType).toHaveBeenCalledWith(WechatStatus.pending);
      });

      it('type - should return type from getBadgeInfoType', () => {
        const res = badgeInfo.type({ status: WechatStatus.pending });

        expect(res).toBe(BadgeInfoType.info);
      });

      it('tooltipLabel - should call getBadgeInfoType for type with status', () => {
        badgeInfo.tooltipLabel({ status: WechatStatus.pending });

        expect((component as any).getTranslatedStatus).toHaveBeenCalledWith(WechatStatus.pending);
      });

      it('tooltipLabel - should return type from getBadgeInfoType', () => {
        const res = badgeInfo.tooltipLabel({ status: WechatStatus.pending });

        expect(res).toBe('test label');
      });
    });
  });

  describe('getBadgeInfoType', () => {
    it('should set getBadgeInfoType when Wechat status is Pending', () => {
      const statusPending = WechatStatus.pending;
      const expectedType = BadgeInfoType.info;

      const test = component['getBadgeInfoType'](statusPending);

      expect(test).toEqual(expectedType);
    });

    it('should set getBadgeInfoType when Wechat status is Canceled', () => {
      const statusCanceled = WechatStatus.cancelled;
      const expectedType = BadgeInfoType.regular;

      const test = component['getBadgeInfoType'](statusCanceled);

      expect(test).toEqual(expectedType);
    });

    it('should set getBadgeInfoType when Wechat status is Registered', () => {
      const statusRegistered = WechatStatus.registered;
      const expectedType = BadgeInfoType.success;

      const test = component['getBadgeInfoType'](statusRegistered);

      expect(test).toEqual(expectedType);
    });

    it('should set getBadgeInfoType when Wechat status is any', () => {
      const expectedType = BadgeInfoType.regular;

      const test = component['getBadgeInfoType'](null);

      expect(test).toEqual(expectedType);
    });
  });

  describe('initializeDropdowns', () => {
    it('should call getTranslatedStatus with label', () => {
      (component as any).getTranslatedStatus = jasmine.createSpy().and.returnValue('test');
      const times = Object.values(WechatStatus).length;

      (component as any).initializeDropdowns();

      expect((component as any).getTranslatedStatus).toHaveBeenCalledTimes(times);
    });

    it('should set statusOptions', () => {
      component.statusOptions = [];
      const res = [
        { value: WechatStatus.cancelled, label: 'test' },
        { value: WechatStatus.pending, label: 'test' },
        { value: WechatStatus.registered, label: 'test' }
      ];
      (component as any).getTranslatedStatus = jasmine.createSpy().and.returnValue('test');

      (component as any).initializeDropdowns();

      expect(component.statusOptions).toContain(res[0]);
      expect(component.statusOptions).toContain(res[1]);
      expect(component.statusOptions).toContain(res[2]);
    });
  });

  describe('getTranslatedStatus', () => {
    it('should call translate with status in lower case', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');

      (component as any).getTranslatedStatus('PENDING');

      expect((component as any).translationService.translate).toHaveBeenCalledWith('WECHAT.status.pending');
    });

    it('should return value of translate', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');

      const res = (component as any).getTranslatedStatus('PENDING');

      expect(res).toBe('test');
    });
  });
});
