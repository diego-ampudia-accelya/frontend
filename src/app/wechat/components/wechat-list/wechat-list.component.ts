import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Wechat, WechatFilters, WechatStatus } from '../../models/wechat.model';
import { WechatFilterFormatter } from '../../services/wechat-formatter';
import { WechatService } from '../../services/wechat.service';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil, toValueLabelObject } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';

@Component({
  selector: 'bspl-wechat-list',
  templateUrl: './wechat-list.component.html',
  styleUrls: ['./wechat-list.component.scss'],
  providers: [DefaultQueryStorage, QueryableDataSource, { provide: QUERYABLE, useExisting: WechatService }]
})
export class WechatListComponent implements OnInit {
  public columns: GridColumn[] = [];
  public searchForm: FormGroup;

  public statusOptions: DropdownOption[] = [];

  public totalItemsMessage$: Observable<string> = this.dataSource.appliedQuery$.pipe(
    map(query => query.paginateBy.totalElements),
    map(total =>
      this.translationService.translate('WECHAT.totalItemsMessage', {
        total
      })
    )
  );

  private formFactory: FormUtil;

  constructor(
    public displayFilterFormatter: WechatFilterFormatter,
    public dataSource: QueryableDataSource<WechatFilters>,
    private queryStorage: DefaultQueryStorage,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeDropdowns();
    this.initializeColumns();
    this.searchForm = this.buildSearchForm();
    const storedQuery = this.queryStorage.get();
    this.loadData(storedQuery);
  }

  public loadData(query?: DataQuery): void {
    query = query || cloneDeep(defaultQuery);
    this.dataSource.get(query);
    this.queryStorage.save(query);
  }

  public onRowLinkClick(document: Wechat): void {
    this.router.navigate([ROUTES.WECHAT_SETTINGS.url, document.id, document.registrationRequestNumber]);
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<WechatFilters>({
      registrationRequestNumber: [],
      status: []
    });
  }

  private initializeColumns(): void {
    this.columns = this.buildColumns();
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'registrationRequestNumber',
        name: 'WECHAT.columns.weChatId',
        cellTemplate: 'commonLinkFromObjCellTmpl',
        width: 180,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'accountType',
        name: 'WECHAT.columns.accountType',
        width: 150,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'name',
        name: 'WECHAT.columns.registrant',
        width: 160,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'phone',
        name: 'WECHAT.columns.mobile',
        width: 150,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'email',
        name: 'WECHAT.columns.email',
        width: 220,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'status',
        name: 'WECHAT.columns.status',
        draggable: false,
        cellTemplate: 'badgeInfoCellTmpl',
        badgeInfo: {
          hidden: (value: Wechat) => !value,
          type: (value: Wechat) => this.getBadgeInfoType(value.status),
          tooltipLabel: (value: Wechat) => this.getTranslatedStatus(value.status),
          showIconType: true
        },
        width: 110,
        resizeable: true
      },
      {
        prop: 'applicationDate',
        name: 'WECHAT.columns.applicationDate',
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 145,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'processDate',
        name: 'WECHAT.columns.processDate',
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 145,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'processUser',
        name: 'WECHAT.columns.processUser',
        width: 145,
        resizeable: true,
        draggable: false
      }
    ];
  }

  private getBadgeInfoType(status: WechatStatus): BadgeInfoType {
    let type: BadgeInfoType;

    switch (status) {
      case WechatStatus.pending:
        type = BadgeInfoType.info;
        break;
      case WechatStatus.registered:
        type = BadgeInfoType.success;
        break;
      case WechatStatus.cancelled:
        type = BadgeInfoType.regular;
        break;
      default:
        type = BadgeInfoType.regular;
        break;
    }

    return type;
  }

  private initializeDropdowns(): void {
    this.statusOptions = Object.values(WechatStatus)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.getTranslatedStatus(label)
      }));
  }

  private getTranslatedStatus(status: WechatStatus): string {
    return this.translationService.translate(`WECHAT.status.${status.toLowerCase()}`);
  }
}
