import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, finalize, first, map, mapTo, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';

import { getCurrentWechatUserId } from '../../../store';
import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { ReasonDialogComponent } from '../../reason-dialog/reason-dialog.component';
import { RegAccessInfo, RegistrationSummary, RegistrationSummaryStatus } from '../models/registration-summary.model';
import { getRegistrationSummaryIsLoading, getRegistrationSummaryValue, RegistrationSummaryActions } from '../store';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { FormUtil } from '~app/shared/helpers';
import { UserType } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';
import { WechatService } from '~app/wechat/services/wechat.service';

@Component({
  selector: 'bspl-registration-summary',
  templateUrl: './registration-summary.component.html',
  styleUrls: ['./registration-summary.component.scss']
})
export class RegistrationSummaryComponent implements OnInit, OnDestroy {
  public regSummary: RegistrationSummary;
  public isLoading$: Observable<boolean> = this.store.select(getRegistrationSummaryIsLoading);

  public isPending: boolean;
  public isRegistered: boolean;

  public isIata: boolean;
  public isAirline: boolean;
  public isAgent: boolean;

  public buttonDesign = ButtonDesign;

  public isApplyChangesButtonEnabled$: Observable<boolean>;

  public regAccessInfoForm: FormGroup;
  private reasonControl: FormControl;

  private formFactory: FormUtil;

  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private dialogService: DialogService,
    private wechatService: WechatService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private changesDialogService: ChangesDialogService,
    private fb: FormBuilder
  ) {}

  public ngOnInit(): void {
    this.formFactory = new FormUtil(this.fb);

    this.initializeUserTypeFlags();
    this.initializeRegAccessInfoForm();
    this.initializeApplyButtonEnableFlag();
    this.initializeStoreData();
  }

  public approveButtonClicked(): void {
    this.openReasonDialog(
      this.translationService.translate('MENU.WECHAT.settings.menu.registration.dialog.approve.title'),
      FooterButton.Approve
    ).subscribe(() => this.approve());
  }

  public rejectButtonClicked(): void {
    this.openReasonDialog(
      this.translationService.translate('MENU.WECHAT.settings.menu.registration.dialog.reject.title'),
      FooterButton.Reject
    ).subscribe(() => this.reject());
  }

  public deactivateClicked(): void {
    this.openReasonDialog(
      this.translationService.translate('MENU.WECHAT.settings.menu.registration.dialog.deactivate.title'),
      FooterButton.Deactivate
    ).subscribe(() => this.reject());
  }

  public applyChangesClicked(): void {
    this.store
      .select(getCurrentWechatUserId)
      .pipe(
        first(),
        switchMap(userId => {
          const modifications = this.getRegAccessInfoChanges();

          return this.changesDialogService.confirmUnsavedChanges(modifications).pipe(
            first(),
            filter(footerButtonClicked => footerButtonClicked === FooterButton.Apply),
            mapTo({ userId, modifications })
          );
        }),
        switchMap(({ userId, modifications }) =>
          this.wechatService
            .updateRegAccessInfo(userId, modifications)
            .pipe(finalize(() => this.closeDialogAndReloadData()))
        )
      )
      .subscribe(
        () => this.notifyActionWasPerformed(),
        () => this.notifyActionError()
      );
  }

  public openReasonDialog(title: string, footerButton: FooterButton): Observable<FooterButton> {
    this.reasonControl = new FormControl(null, [Validators.required]);

    const dialogConfig: DialogConfig = {
      data: {
        title,
        footerButtonsType: footerButton,
        reasonControl: this.reasonControl
      }
    };

    return this.dialogService.open(ReasonDialogComponent, dialogConfig).pipe(
      filter(({ clickedBtn }) => this.reasonControl.valid || clickedBtn === FooterButton.Cancel),
      first(),
      filter(({ clickedBtn }) => clickedBtn === footerButton),
      tap(() => (dialogConfig.data.buttons.find(button => button.type === footerButton).isDisabled = true))
    );
  }

  public approve(): void {
    this.wechatService
      .registerUser(this.regSummary.id, this.reasonControl.value)
      .pipe(finalize(() => this.closeDialogAndReloadData()))
      .subscribe(
        () => this.notifyActionWasPerformed(),
        () => this.notifyActionError()
      );
  }

  public reject(): void {
    this.wechatService
      .cancelUser(this.regSummary.id, this.reasonControl.value)
      .pipe(finalize(() => this.closeDialogAndReloadData()))
      .subscribe(
        () => this.notifyActionWasPerformed(),
        () => this.notifyActionError()
      );
  }

  public hasRegAccessInfoChanges(): boolean {
    return Object.keys(this.getRegAccessInfoChanges()).length > 0;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeUserTypeFlags(): void {
    this.store
      .select(getUserType)
      .pipe(first())
      .subscribe(userType => {
        this.isIata = userType === UserType.IATA;
        this.isAirline = userType === UserType.AIRLINE;
        this.isAgent = userType === UserType.AGENT;
      });
  }

  private initializeStoreData(): void {
    this.store
      .select(getCurrentWechatUserId)
      .pipe(takeUntil(this.destroy$))
      .subscribe(wechatUserId => this.store.dispatch(RegistrationSummaryActions.load({ id: wechatUserId })));

    this.store
      .select(getRegistrationSummaryValue)
      .pipe(
        filter(regSummary => !!regSummary),
        takeUntil(this.destroy$)
      )
      .subscribe(regSummary => {
        this.regSummary = regSummary;

        this.initializeStatusFlags();
        this.updateRegAccesInfoForm();
      });
  }

  private notifyActionWasPerformed(): void {
    this.notificationService.showSuccess('MENU.WECHAT.settings.menu.registration.notifications.success');
  }

  private notifyActionError(): void {
    this.notificationService.showError('MENU.WECHAT.settings.menu.registration.notifications.error');
  }

  private closeDialogAndReloadData(): void {
    this.dialogService.close();
    this.reloadRegSummaryData();
  }

  private reloadRegSummaryData(): void {
    this.store
      .select(getCurrentWechatUserId)
      .pipe(takeUntil(this.destroy$))
      .subscribe(wechatUserId => this.store.dispatch(RegistrationSummaryActions.reload({ id: wechatUserId })));
  }

  private initializeStatusFlags(): void {
    const status = this.regSummary.status;

    this.isPending = status === RegistrationSummaryStatus.Pending;
    this.isRegistered = status === RegistrationSummaryStatus.Registered;
  }

  private initializeApplyButtonEnableFlag(): void {
    this.isApplyChangesButtonEnabled$ = this.regAccessInfoForm.valueChanges.pipe(
      startWith(false),
      takeUntil(this.destroy$),
      map(() => this.hasRegAccessInfoChanges())
    );
  }

  private initializeRegAccessInfoForm(): void {
    this.regAccessInfoForm = this.formFactory.createGroup<RegAccessInfo>({
      businessNotification: null,
      dpcFileReport: null,
      billingReport: null,
      ticketingAuthority: null
    });
  }

  private updateRegAccesInfoForm(): void {
    this.regAccessInfoForm.patchValue(this.regSummary);
  }

  private getRegAccessInfoChanges(): Partial<RegAccessInfo> {
    const formValue: RegAccessInfo = this.regAccessInfoForm.value;
    const modifications: Partial<RegAccessInfo> = {};

    Object.keys(formValue)
      .filter(key => formValue[key] !== this.regSummary[key])
      .forEach(key => (modifications[key] = formValue[key]));

    return modifications;
  }
}
