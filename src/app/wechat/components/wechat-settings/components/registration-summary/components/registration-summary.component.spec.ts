import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { take } from 'rxjs/operators';

import { getCurrentWechatUserId } from '../../../store';
import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { RegistrationSummary } from '../models/registration-summary.model';
import { getRegistrationSummaryIsLoading, getRegistrationSummaryValue } from '../store';
import * as RegistrationSummaryActions from '../store/registration-summary.actions';
import { initialState, RegistrationSummaryState } from '../store/registration-summary.reducer';

import { RegistrationSummaryComponent } from './registration-summary.component';
import { WechatService } from '~app/wechat/services/wechat.service';
import { NotificationService } from '~app/shared/services';
import { DialogService } from '~app/shared/components';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('RegistrationSummaryComponent', () => {
  const cacheMock = new Map<string, RegistrationSummaryState>();
  const userMockId = '1';
  const regSummaryMock: Partial<RegistrationSummary> = { id: 1 };
  const regSummaryStateMock: Partial<RegistrationSummaryState> = { value: regSummaryMock as any };

  let component: RegistrationSummaryComponent;
  let fixture: ComponentFixture<RegistrationSummaryComponent>;

  beforeEach(async () => {
    cacheMock.set(userMockId, regSummaryStateMock as any);

    await TestBed.configureTestingModule({
      declarations: [RegistrationSummaryComponent],
      providers: [
        provideMockStore({
          initialState: { ...initialState, cache: new Map() },
          selectors: [
            {
              selector: getRegistrationSummaryValue,
              value: regSummaryMock
            },
            {
              selector: getRegistrationSummaryIsLoading,
              value: true
            },
            {
              selector: getCurrentWechatUserId,
              value: userMockId
            }
          ]
        }),
        mockProvider(WechatService),
        mockProvider(DialogService),
        mockProvider(NotificationService),
        mockProvider(ChangesDialogService),
        FormBuilder
      ],
      imports: [CommonModule, L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationSummaryComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should initialize store variables oninit', fakeAsync(() => {
    const mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');

    fixture.detectChanges();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(RegistrationSummaryActions.load({ id: '1' }));
  }));

  it('should get correct value from store for reg summary', fakeAsync(() => {
    fixture.detectChanges();
    tick();

    expect(component.regSummary).toEqual(regSummaryMock as RegistrationSummary);
  }));

  it('should get correct value from store for loading', fakeAsync(() => {
    let res;
    component.isLoading$.pipe(take(1)).subscribe(l => (res = l));

    fixture.detectChanges();
    tick();

    expect(res).toBe(true);
  }));
});
