import { createAction, props } from '@ngrx/store';

import { RegistrationSummary } from '../models/registration-summary.model';

import { RegistrationSummaryState } from './registration-summary.reducer';

export const load = createAction('[Registration Summary] Load', props<{ id: string }>());
export const reload = createAction('[Registration Summary] Reload', props<{ id: string }>());

export const loadFromCacheSuccess = createAction(
  '[Registration Summary] Load From Cache Success',
  props<{ regSummaryState: RegistrationSummaryState }>()
);

export const loadSuccess = createAction(
  '[Registration Summary] Load Success',
  props<{ userId: string; regSummary: RegistrationSummary }>()
);

export const loadError = createAction('[Registration Summary] Load Error');

export const modify = createAction('[Registration Summary] Modify', props<{ value: RegistrationSummary }>());

export const invalid = createAction('[Registration Summary] Invalid');

export const openApplyChanges = createAction('[Registration Summary] Open Apply Changes');

export const applyChangesSuccess = createAction(
  '[Registration Summary] Apply Changes Success',
  props<{ value: RegistrationSummary }>()
);

export const applyChangesError = createAction('[Registration Summary] Apply Changes Error', props<{ error }>());

export const discard = createAction('[Registration Summary] Discard');
