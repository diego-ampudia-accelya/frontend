import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as RegistrationSummaryActions from './registration-summary.actions';
import { reducer, RegistrationSummarySectionState } from './registration-summary.reducer';
import { AppState } from '~app/reducers';

export { RegistrationSummaryActions };

export const registrationSummaryKey = 'registration-summary';

export const registrationSummaryReducer = reducer;

export interface State extends AppState {
  [registrationSummaryKey]: RegistrationSummarySectionState;
}

export const getRegistrationSummarySectionState = createFeatureSelector<State, RegistrationSummarySectionState>(
  registrationSummaryKey
);

export const getRegistrationSummaryCache = createSelector(
  getRegistrationSummarySectionState,
  state => state && state.cache
);

export const getRegistrationSummaryValue = createSelector(
  getRegistrationSummarySectionState,
  state => state && state.value
);

export const getRegistrationSummaryIsLoading = createSelector(
  getRegistrationSummarySectionState,
  state => state && state.isLoading
);
