import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { combineLatest, iif, of } from 'rxjs';
import { first, switchMap, tap } from 'rxjs/operators';

import { getCurrentWechatUserId } from '../../../store';
import { RegistrationSummary } from '../models/registration-summary.model';

import { RegistrationSummaryState } from './registration-summary.reducer';
import { getRegistrationSummaryCache, RegistrationSummaryActions } from '.';
import { WechatService } from '~app/wechat/services/wechat.service';
import { AppState } from '~app/reducers';

@Injectable()
export class RegistrationSummaryEffects {
  public load$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(RegistrationSummaryActions.load),
        tap(({ id }) => this.loadSummary(id))
      ),
    { dispatch: false }
  );

  public reload$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(RegistrationSummaryActions.reload),
        tap(({ id }) => this.reloadSummary(id))
      ),
    { dispatch: false }
  );

  constructor(private store: Store<AppState>, private actions$: Actions, private wechatService: WechatService) {}

  private loadSummary(id: string): void {
    let isRegSummaryStored;
    let currentUserId;

    combineLatest([this.store.select(getRegistrationSummaryCache), this.store.select(getCurrentWechatUserId)])
      .pipe(
        first(),
        switchMap(([cache, userId]) => {
          isRegSummaryStored = cache.has(userId);
          currentUserId = userId;

          return iif(() => isRegSummaryStored, of(cache.get(userId)), this.wechatService.getById(id));
        })
      )
      .subscribe(regSummary => {
        if (isRegSummaryStored) {
          this.store.dispatch(
            RegistrationSummaryActions.loadFromCacheSuccess({ regSummaryState: regSummary as RegistrationSummaryState })
          );
        } else {
          this.store.dispatch(
            RegistrationSummaryActions.loadSuccess({
              userId: currentUserId,
              regSummary: regSummary as RegistrationSummary
            })
          );
        }
      });
  }

  private reloadSummary(id: string): void {
    this.wechatService.getById(id).subscribe(regSummary => {
      this.store.dispatch(
        RegistrationSummaryActions.loadSuccess({
          userId: id,
          regSummary: regSummary as RegistrationSummary
        })
      );
    });
  }
}
