import { createReducer, on } from '@ngrx/store';
import { isEqual } from 'lodash';

import { RegistrationSummary } from '../models/registration-summary.model';

import * as RegistrationSummaryActions from './registration-summary.actions';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';

export interface RegistrationSummarySectionState extends RegistrationSummaryState {
  cache: Map<string, RegistrationSummaryState>;
}

export interface RegistrationSummaryState {
  originalValue: RegistrationSummary;
  value: RegistrationSummary;
  isChanged: boolean;
  isLoading: boolean;
  isInvalid: boolean;
  errors: { error: ResponseErrorBE };
}

export const initialState: RegistrationSummarySectionState = {
  cache: new Map(),
  originalValue: null,
  value: null,
  isChanged: false,
  isLoading: false,
  isInvalid: false,
  errors: null
};

export const reducer = createReducer(
  initialState,
  on(RegistrationSummaryActions.load, state => ({
    ...state,
    isLoading: true
  })),
  on(RegistrationSummaryActions.reload, state => ({
    ...state,
    isLoading: true
  })),
  on(RegistrationSummaryActions.loadFromCacheSuccess, (state, { regSummaryState }) => ({
    ...state,
    isLoading: false,
    value: regSummaryState.value,
    originalValue: regSummaryState.originalValue,
    errors: regSummaryState.errors,
    isChanged: regSummaryState.isChanged,
    isInvalid: regSummaryState.isInvalid
  })),
  on(RegistrationSummaryActions.loadSuccess, (state, { userId, regSummary }) =>
    loadRegSummary(state, userId, regSummary)
  ),
  on(RegistrationSummaryActions.loadError, state => ({
    ...state,
    originalValue: null,
    value: null,
    isChanged: false,
    isLoading: false,
    settingConfigurations: [],
    isInvalid: false,
    errors: null
  })),
  on(RegistrationSummaryActions.modify, (state, { value }) => ({
    ...state,
    value: changeValue(state.value, value),
    isChanged: !isEqual(state.originalValue, { ...state.value, ...value }),
    isInvalid: false,
    errors: null
  })),
  on(RegistrationSummaryActions.invalid, state => ({
    ...state,
    isInvalid: true
  })),
  on(RegistrationSummaryActions.discard, state => ({
    ...state,
    isChanged: false,
    value: state.originalValue
  })),
  on(RegistrationSummaryActions.applyChangesSuccess, (state, { value }) => ({
    ...state,
    value,
    originalValue: value,
    isChanged: false,
    isInvalid: false,
    errors: null
  })),
  on(RegistrationSummaryActions.applyChangesError, (state, { error }) => ({
    ...state,
    isChanged: !error,
    isInvalid: !!error,
    errors: error
  }))
  // TODO When closing the tab or discarding, we should delete the data from the store
  // on(RegistrationSummaryActions.unload, (state, { userId, regSummary }) =>
  //   unloadRegSummary(state, userId, regSummary)
  // )
);

const loadRegSummary = (
  state: RegistrationSummarySectionState,
  userId: string,
  regSummary: RegistrationSummary
): RegistrationSummarySectionState => {
  const newState: RegistrationSummaryState = {
    value: regSummary,
    originalValue: regSummary,
    errors: null,
    isChanged: false,
    isInvalid: false,
    isLoading: false
  };

  state.cache.set(userId, newState);

  return { ...newState, cache: state.cache };
};

export const getRegistrationSummaryByUser = (
  userId: string,
  cache: Map<string, RegistrationSummaryState>
): RegistrationSummaryState => cache.get(userId);

export const getRegistrationSummaryChanges = ({ value, originalValue }: RegistrationSummaryState) => {
  const modifications = {};

  if (value && originalValue) {
    Object.keys(value)
      .filter(key => value[key] !== originalValue[key])
      .forEach(modifiedKey => {
        modifications[modifiedKey] = value[modifiedKey];
      });
  }

  return modifications;
};

export const changeValue = (currentValue: RegistrationSummary, newValue: RegistrationSummary) => ({
  ...currentValue,
  ...newValue
});
