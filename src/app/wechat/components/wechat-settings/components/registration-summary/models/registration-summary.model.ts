/* eslint-disable @typescript-eslint/naming-convention */
export interface RegistrationSummary {
  id: number;
  registrationRequestNumber: string;
  accountType: string;
  name: string;
  phone: string;
  email: string;
  status: RegistrationSummaryStatus;
  applicationDate: Date;
  processDate: Date;
  processUser: string;
  switchTo: string;
  reason: string;
  registrationReason: string;
  cancellationReason: string;
  businessNotification: boolean;
  dpcFileReport: boolean;
  billingReport: boolean;
  ticketingAuthority: boolean;
}

export interface RegAccessInfo {
  businessNotification: boolean;
  dpcFileReport: boolean;
  billingReport: boolean;
  ticketingAuthority: boolean;
}

export enum RegistrationSummaryStatus {
  Pending = 'Pending',
  Registered = 'Registered',
  Cancelled = 'Cancelled'
}
