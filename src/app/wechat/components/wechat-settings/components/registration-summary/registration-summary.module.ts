import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ReasonDialogComponent } from '../reason-dialog/reason-dialog.component';

import { RegistrationSummaryComponent } from './components/registration-summary.component';
import * as fromRegistrationSummary from './store/index';
import { RegistrationSummaryEffects } from './store/registration-summary.effects';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [RegistrationSummaryComponent, ReasonDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(
      fromRegistrationSummary.registrationSummaryKey,
      fromRegistrationSummary.registrationSummaryReducer
    ),
    EffectsModule.forFeature([RegistrationSummaryEffects])
  ]
})
export class RegistrationSummaryModule {}
