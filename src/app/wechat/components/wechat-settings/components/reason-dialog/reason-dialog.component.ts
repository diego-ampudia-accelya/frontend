import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { L10nLocale, L10N_LOCALE } from 'angular-l10n';

import { DialogConfig } from '~app/shared/components';

@Component({
  selector: 'bspl-reason-dialog',
  templateUrl: './reason-dialog.component.html'
})
export class ReasonDialogComponent implements OnInit {
  public reasonControl: FormControl;

  constructor(@Inject(L10N_LOCALE) public locale: L10nLocale, public config: DialogConfig) {}

  public ngOnInit(): void {
    this.reasonControl = this.config.data.reasonControl;
  }
}
