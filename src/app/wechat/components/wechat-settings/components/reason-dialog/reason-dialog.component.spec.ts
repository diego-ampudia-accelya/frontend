import { ComponentFixture, TestBed } from '@angular/core/testing';
import { L10nTranslationModule } from 'angular-l10n';

import { ReasonDialogComponent } from './reason-dialog.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

// TODO
xdescribe('ReasonDialogComponent', () => {
  let component: ReasonDialogComponent;
  let fixture: ComponentFixture<ReasonDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReasonDialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig)]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasonDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
