import { CommonModule } from '@angular/common';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import * as WechatSettingsActions from '../store/wechat-settings.actions';
import { initialState } from '../store/wechat-settings.reducer';

import { WechatSettingsComponent } from './wechat-settings.component';
import { AppState } from '~app/reducers';

describe('WechatSettingsComponent', () => {
  const activatedRouteSpy = { snapshot: { params: { ['user-id']: '1' } } };

  let component: WechatSettingsComponent;
  let fixture: ComponentFixture<WechatSettingsComponent>;

  let mockStore: MockStore<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WechatSettingsComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteSpy },
        provideMockStore({
          initialState
        })
      ],
      imports: [CommonModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WechatSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load user', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    component.ngOnInit();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(WechatSettingsActions.load({ userId: '1' }));
  }));
});
