import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { RegAccessInfo } from '../registration-summary/models/registration-summary.model';
import { ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable({ providedIn: 'root' })
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(modification: Partial<RegAccessInfo>): ChangesDialogConfig {
    const title = this.translation.translate('common.unsavedChangesDialogTitle');
    const message = this.translation.translate('common.unsavedChangesDialogMessage');

    return {
      data: {
        title,
        footerButtonsType: [{ type: FooterButton.Apply }]
      },
      changes: this.formatChanges(modification),
      message
    };
  }

  private formatChanges(modification: Partial<RegAccessInfo>): ChangeModel[] {
    const group = this.translation.translate('MENU.WECHAT.settings.menu.registration.titles.regAccessInfo.pageTitle');

    return Object.keys(modification).map(key => ({
      group,
      name: this.translateName(key),
      originalValue: modification[key],
      value: modification[key]
    }));
  }

  private translateName(name: string): string {
    return this.translation.translate(`MENU.WECHAT.settings.menu.registration.titles.regAccessInfo.labels.${name}`);
  }
}
