import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { RegAccessInfo } from '../registration-summary/models/registration-summary.model';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ChangesDialogComponent, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(private dialogService: DialogService, private configBuilder: ChangesDialogConfigService) {}

  public confirmUnsavedChanges(change: Partial<RegAccessInfo>): Observable<FooterButton> {
    return this.open(change);
  }

  private open(modification: Partial<RegAccessInfo>): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modification);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      tap(() => (dialogConfig.data.buttons.find(button => button.type === FooterButton.Apply).isDisabled = true)),
      map(action => action.clickedBtn)
    );
  }
}
