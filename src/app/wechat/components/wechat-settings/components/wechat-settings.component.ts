import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import * as WechatSettingsActions from '../store/wechat-settings.actions';
import { AppState } from '~app/reducers';

@Component({
  selector: 'bspl-wechat-settings',
  templateUrl: './wechat-settings.component.html'
})
export class WechatSettingsComponent implements OnInit {
  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute) {}

  public ngOnInit(): void {
    this.loadUser();
  }

  private loadUser(): void {
    const userId = this.activatedRoute.snapshot.params['user-id'];
    this.store.dispatch(WechatSettingsActions.load({ userId }));
  }
}
