import { MenuTab, MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';

export function getWechatSettingsTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;

  return { label: tab.tabLabel as string, params: { regNumber: routeData.regNumber } };
}
