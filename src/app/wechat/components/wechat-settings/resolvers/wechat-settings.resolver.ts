import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable()
export class WechatSettingsResolver implements Resolve<any> {
  constructor() {
    //This is intentional
  }

  public resolve(route: ActivatedRouteSnapshot): Observable<string> {
    const regNumber = route.params['reg-number'];

    return of(regNumber);
  }
}
