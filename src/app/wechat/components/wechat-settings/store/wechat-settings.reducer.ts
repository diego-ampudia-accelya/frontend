import { createReducer, on } from '@ngrx/store';

import * as WechatSettingsActions from './wechat-settings.actions';

export const generalSettingsKey = 'general-settings';

export interface GeneralSettingsState {
  currentUserId: string;
}

export const initialState: GeneralSettingsState = {
  currentUserId: null
};

export const reducer = createReducer(
  initialState,
  on(WechatSettingsActions.load, (state, { userId }) => ({
    ...state,
    currentUserId: userId
  }))
);
