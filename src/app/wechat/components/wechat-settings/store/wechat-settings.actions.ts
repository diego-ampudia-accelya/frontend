import { createAction, props } from '@ngrx/store';

export const load = createAction('[Wechat Settings] Load', props<{ userId: string }>());
