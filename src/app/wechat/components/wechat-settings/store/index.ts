import { combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromRegistrationSummary from '../components/registration-summary/store';
import { RegistrationSummaryState } from '../components/registration-summary/store/registration-summary.reducer';

import * as fromWechatSettings from './wechat-settings.reducer';
import { AppState } from '~app/reducers';

export const wechatSettingsKey = 'wechat-settings';

export interface State extends AppState {
  [wechatSettingsKey]: WechatSettingsState;
}

export interface WechatSettingsState {
  [fromWechatSettings.generalSettingsKey]: fromWechatSettings.GeneralSettingsState;
  [fromRegistrationSummary.registrationSummaryKey]: RegistrationSummaryState;
}

export const reducer = combineReducers({
  [fromWechatSettings.generalSettingsKey]: fromWechatSettings.reducer,
  [fromRegistrationSummary.registrationSummaryKey]: fromRegistrationSummary.registrationSummaryReducer
});

export const getWechatSettingsState = createFeatureSelector<State, WechatSettingsState>(wechatSettingsKey);

export const getGeneralSettingsState = createSelector(
  getWechatSettingsState,
  state => state[fromWechatSettings.generalSettingsKey]
);

export const getCurrentWechatUserId = createSelector(getGeneralSettingsState, state => state && state.currentUserId);
