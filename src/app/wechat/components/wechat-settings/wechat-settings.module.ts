import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { ChangesDialogConfigService } from './components/changes-dialog/changes-dialog-config.service';
import { ChangesDialogService } from './components/changes-dialog/changes-dialog.service';
import { RegistrationSummaryModule } from './components/registration-summary/registration-summary.module';
import { WechatSettingsComponent } from './components/wechat-settings.component';
import { WechatSettingsResolver } from './resolvers/wechat-settings.resolver';
import * as fromWechatSettings from './store';
import { ConfigurationModule } from '~app/master-data/configuration';

@NgModule({
  declarations: [WechatSettingsComponent],
  imports: [
    CommonModule,
    ConfigurationModule,
    RegistrationSummaryModule,
    StoreModule.forFeature(fromWechatSettings.wechatSettingsKey, fromWechatSettings.reducer)
  ],
  providers: [WechatSettingsResolver, ChangesDialogService, ChangesDialogConfigService]
})
export class WechatSettingsModule {}
