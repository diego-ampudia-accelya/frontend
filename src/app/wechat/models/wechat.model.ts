export interface Wechat {
  id: string;
  registrationRequestNumber?: string;
  accountType?: string;
  email?: string;
  status?: WechatStatus;
  name?: string;
  phone?: string;
  applicationDate?: string;
  processDate?: string;
  processUser?: string;
  switchTo?: string;
}

export interface WechatFilters {
  registrationRequestNumber?: string;
  status?: WechatStatus;
}

export enum WechatStatus {
  registered = 'Registered',
  pending = 'Pending',
  cancelled = 'Cancelled'
}
