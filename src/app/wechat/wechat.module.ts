import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WechatListComponent } from './components/wechat-list/wechat-list.component';
import { WechatSettingsModule } from './components/wechat-settings/wechat-settings.module';
import { WechatFilterFormatter } from './services/wechat-formatter';
import { WechatService } from './services/wechat.service';
import { WechatRoutingModule } from './wechat-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [WechatListComponent],
  imports: [CommonModule, RouterModule, SharedModule, WechatRoutingModule, WechatSettingsModule],
  providers: [WechatService, WechatFilterFormatter]
})
export class WechatModule {}
