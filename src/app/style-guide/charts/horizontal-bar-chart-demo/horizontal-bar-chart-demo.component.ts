import { Component } from '@angular/core';

import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';

@Component({
  selector: 'bspl-horizontal-bar-chart-demo',
  templateUrl: './horizontal-bar-chart-demo.component.html',
  styleUrls: ['./horizontal-bar-chart-demo.component.scss']
})
export class HorizontalBarChartDemoComponent {
  public barChartData: BarChartData = {
    labels: ['2017', '2018', '2019', '2020', '2021'],
    values: [
      { key: 'Serie A', totals: [0, 20, 89, 30, 14] },
      { key: 'Serie B', totals: [138, 92, 82, 50, 2] },
      { key: 'Serie C', totals: [106, 52, 8, 45, 100] },
      { key: 'Serie D', totals: [92, 78, 50, 13, 94] }
    ]
  };

  public barChartTitle = 'Horizontal bar chart';
  public stackedBarChartTitle = 'Horizontal stacked bar chart';
}
