import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartCardDemoComponent } from './chart-card-demo.component';

describe('ChartCardDemoComponent', () => {
  let component: ChartCardDemoComponent;
  let fixture: ComponentFixture<ChartCardDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChartCardDemoComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartCardDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
