import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { BarChartData } from '~app/shared/components/chart/models/bar-chart.model';
import { BarChartProperties, ChartCardSection } from '~app/shared/components/chart/models/chart-card.model';

@Component({
  selector: 'bspl-chart-card-demo',
  templateUrl: './chart-card-demo.component.html',
  styleUrls: ['./chart-card-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartCardDemoComponent implements OnInit {
  public verticalChartData: BarChartData;
  public horizontalChartData: BarChartData;

  public sections: ChartCardSection[];

  private barChartDataSectionA: BarChartData = {
    labels: ['2017', '2018', '2019', '2020', '2021'],
    values: [
      { key: 'Serie A', totals: [0, 20, 89, 30, 14] },
      { key: 'Serie B', totals: [138, 92, 82, 50, 2] },
      { key: 'Serie C', totals: [106, 52, 8, 45, 100] },
      { key: 'Serie D', totals: [92, 78, 50, 13, 94] }
    ]
  };

  private barChartDataSectionB: BarChartData = {
    labels: ['2017', '2018', '2019', '2020', '2021'],
    values: [
      { key: 'Serie E', totals: [106, 52, 8, 45, 100] },
      { key: 'Serie F', totals: [90, 12, 32, 20, 2] },
      { key: 'Serie G', totals: [138, 92, 82, 50, 2] },
      { key: 'Serie H', totals: [0, 20, 89, 30, 14] }
    ]
  };

  private barChartA: BarChartProperties = { data: this.barChartDataSectionA, stacked: true };
  private barChartB: BarChartProperties = { title: 'TEST', data: this.barChartDataSectionB, type: 'horizontalBar' };

  public ngOnInit(): void {
    this.verticalChartData = this.barChartDataSectionA;
    this.horizontalChartData = this.barChartDataSectionA;

    this.sections = [
      { name: 'Section A', barChart: this.barChartA },
      { name: 'Section B', barChart: this.barChartB }
    ];
  }
}
