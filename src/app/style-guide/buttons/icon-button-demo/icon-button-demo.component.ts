import { Component } from '@angular/core';

@Component({
  selector: 'bspl-icon-button-demo',
  templateUrl: './icon-button-demo.component.html',
  styleUrls: ['./icon-button-demo.component.scss']
})
export class IconButtonDemoComponent {}
