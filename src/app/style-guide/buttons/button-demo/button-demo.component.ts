import { Component } from '@angular/core';

import { ButtonDesign, DialogService, IconButtonType } from '~app/shared/components';

@Component({
  selector: 'bspl-button-demo',
  templateUrl: './button-demo.component.html',
  styleUrls: ['./button-demo.component.scss']
})
export class ButtonDemoComponent {
  public btnDesign = ButtonDesign;
  public btnIcon = IconButtonType;
  public btnLabel = 'Button';

  public tooltipText = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
    industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
    scrambled it to make a type specimen book.`;

  constructor(private dialogService: DialogService) {}

  public click(): void {
    this.dialogService.openInfoDialog('You clicked a button', 'Click!');
  }
}
