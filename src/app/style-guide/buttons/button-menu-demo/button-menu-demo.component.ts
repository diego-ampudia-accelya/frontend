import { Component } from '@angular/core';

@Component({
  selector: 'bspl-button-menu-demo',
  templateUrl: './button-menu-demo.component.html',
  styleUrls: ['./button-menu-demo.component.scss']
})
export class ButtonMenuDemoComponent {
  public buttonMenuSelectedOption = '';
  public buttonMenuOptions = [
    {
      label: 'Issue ADM',
      command: () => {
        this.buttonMenuSelectedOption = 'Issue ADM';
      }
    },
    {
      label: 'Issue ACM',
      command: () => {
        this.buttonMenuSelectedOption = 'Issue ACM';
      }
    },
    {
      label: 'Issue RN',
      command: () => {
        this.buttonMenuSelectedOption = 'Issue RN';
      }
    },
    {
      label: 'Issue RA',
      command: () => {
        this.buttonMenuSelectedOption = 'Issue RA';
      }
    }
  ];
}
