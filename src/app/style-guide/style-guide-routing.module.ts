import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ButtonDemoComponent } from './buttons/button-demo/button-demo.component';
import { ButtonMenuDemoComponent } from './buttons/button-menu-demo/button-menu-demo.component';
import { IconButtonDemoComponent } from './buttons/icon-button-demo/icon-button-demo.component';
import { ChartCardDemoComponent } from './charts/chart-card-demo/chart-card-demo.component';
import { HorizontalBarChartDemoComponent } from './charts/horizontal-bar-chart-demo/horizontal-bar-chart-demo.component';
import { VerticalBarChartDemoComponent } from './charts/vertical-bar-chart-demo/vertical-bar-chart-demo.component';
import { AutocompleteDemoComponent } from './forms/autocomplete-demo/autocomplete-demo.component';
import { CheckboxDemoComponent } from './forms/checkbox-demo/checkbox-demo.component';
import { DatePickerDemoComponent } from './forms/date-picker-demo/date-picker-demo.component';
import { InputDemoComponent } from './forms/input-demo/input-demo.component';
import { PeriodPickerDemoComponent } from './forms/period-picker-demo/period-picker-demo.component';
import { RadioButtonDemoComponent } from './forms/radio-button-demo/radio-button-demo.component';
import { RangeDropdownDemoComponent } from './forms/range-dropdown-demo/range-dropdown-demo.component';
import { SelectDemoComponent } from './forms/select-demo/select-demo.component';
import { TagsInputDemoComponent } from './forms/tags-input-demo/tags-input-demo.component';
import { ToggleDemoComponent } from './forms/toggle-demo/toggle-demo.component';
import { ValidatorsDemoComponent } from './forms/validators-demo/validators-demo.component';
import { ListViewDemoComponent } from './grid/list-view-demo/list-view-demo.component';
import { PaginationDemoComponent } from './grid/pagination-demo/pagination-demo.component';
import { SearchPanelDemoComponent } from './grid/search-panel-demo/search-panel-demo.component';
import { TagDemoComponent } from './grid/tag-demo/tag-demo.component';
import { AlertMessageDemoComponent } from './miscellaneous/alert-message-demo/alert-message-demo.component';
import { CardDemoComponent } from './miscellaneous/card-demo/card-demo.component';
import { DialogDemoComponent } from './miscellaneous/dialog-demo/dialog-demo.component';
import { FloatingPanelDemoComponent } from './miscellaneous/floating-panel-demo/floating-panel-demo.component';
import { NotificationDemoComponent } from './miscellaneous/notification-demo/notification-demo.component';
import { PropertyListDemoComponent } from './miscellaneous/property-list-demo/property-list-demo.component';
import { SelectionListDemoComponent } from './miscellaneous/selection-list-demo/selection-list-demo.component';
import { SpinnerDemoComponent } from './miscellaneous/spinner-demo/spinner-demo.component';
import { TabsDemoComponent } from './miscellaneous/tabs-demo/tabs-demo.component';
import { StyleGuideComponent } from '~app/style-guide/style-guide.component';
import { FileUploadDemoComponent } from '~app/style-guide/forms/file-upload-demo/file-upload-demo.component';

const routes: Routes = [
  {
    path: '',
    component: StyleGuideComponent,
    children: [
      // Miscellaneous
      {
        path: 'notifications',
        component: NotificationDemoComponent,
        data: {
          title: 'Notifications',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'alert-message',
        component: AlertMessageDemoComponent,
        data: {
          title: 'Alert Message',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'spinner',
        component: SpinnerDemoComponent,
        data: {
          title: 'Spinner',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'dialog',
        component: DialogDemoComponent,
        data: {
          title: 'Dialog',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'card',
        component: CardDemoComponent,
        data: {
          title: 'Card',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'floating-panel',
        component: FloatingPanelDemoComponent,
        data: {
          title: 'Floating Panel',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'selection-list',
        component: SelectionListDemoComponent,
        data: {
          title: 'Selection List',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'tabs',
        component: TabsDemoComponent,
        data: {
          title: 'Tabs',
          group: 'Miscellaneous'
        },
        children: [
          {
            path: 'property-list',
            component: PropertyListDemoComponent
          },
          {
            path: 'alert-message',
            component: AlertMessageDemoComponent
          }
        ]
      },
      {
        path: 'property-list',
        component: PropertyListDemoComponent,
        data: {
          title: 'Property List',
          group: 'Miscellaneous'
        }
      },
      {
        path: 'overflow-test',
        redirectTo: 'tabs',
        data: {
          title: 'A very long label to demonstrate overflow behavior',
          group: 'Miscellaneous'
        }
      },

      // Forms
      {
        path: 'input',
        component: InputDemoComponent,
        data: {
          title: 'Input',
          group: 'Forms'
        }
      },
      {
        path: 'validators',
        component: ValidatorsDemoComponent,
        data: {
          title: 'Validators',
          group: 'Forms'
        }
      },
      {
        path: 'select',
        component: SelectDemoComponent,
        data: {
          title: 'Select',
          group: 'Forms'
        }
      },
      {
        path: 'checkbox',
        component: CheckboxDemoComponent,
        data: {
          title: 'Checkbox',
          group: 'Forms'
        }
      },
      {
        path: 'toggle',
        component: ToggleDemoComponent,
        data: {
          title: 'Toggle',
          group: 'Forms'
        }
      },
      {
        path: 'radio-button',
        component: RadioButtonDemoComponent,
        data: {
          title: 'Radio Button',
          group: 'Forms'
        }
      },
      {
        path: 'period-picker',
        component: PeriodPickerDemoComponent,
        data: {
          title: 'Period Picker',
          group: 'Forms'
        }
      },

      {
        path: 'date-picker',
        component: DatePickerDemoComponent,
        data: {
          title: 'Date Picker',
          group: 'Forms'
        }
      },
      {
        path: 'autocomplete',
        component: AutocompleteDemoComponent,
        data: {
          title: 'Autocomplete',
          group: 'Forms'
        }
      },
      {
        path: 'file-upload',
        component: FileUploadDemoComponent,
        data: {
          title: 'File Upload',
          group: 'Forms'
        }
      },
      {
        path: 'tags-input',
        component: TagsInputDemoComponent,
        data: {
          title: 'Tags-input',
          group: 'Forms'
        }
      },
      {
        path: 'range-dropdown',
        component: RangeDropdownDemoComponent,
        data: {
          title: 'Range Dropdown',
          group: 'Forms'
        }
      },

      // Buttons
      {
        path: 'button',
        component: ButtonDemoComponent,
        data: {
          title: 'Button',
          group: 'Buttons'
        }
      },
      {
        path: 'icon-button',
        component: IconButtonDemoComponent,
        data: {
          title: 'Icon Button',
          group: 'Buttons'
        }
      },
      {
        path: 'button-menu',
        component: ButtonMenuDemoComponent,
        data: {
          title: 'Button Menu',
          group: 'Buttons'
        }
      },

      // Grid
      {
        path: 'list-view',
        component: ListViewDemoComponent,
        data: {
          title: 'List View',
          group: 'Grid'
        }
      },
      {
        path: 'pagination',
        component: PaginationDemoComponent,
        data: {
          title: 'Pagination',
          group: 'Grid'
        }
      },
      {
        path: 'tags',
        component: TagDemoComponent,
        data: {
          title: 'Tags',
          group: 'Grid'
        }
      },
      {
        path: 'search-panel',
        component: SearchPanelDemoComponent,
        data: {
          title: 'Search Panel',
          group: 'Grid'
        }
      },

      // Charts
      {
        path: 'vertical-bar',
        component: VerticalBarChartDemoComponent,
        data: {
          title: 'Vertical Bar',
          group: 'Charts'
        }
      },
      {
        path: 'horizontal-bar',
        component: HorizontalBarChartDemoComponent,
        data: {
          title: 'Horizontal Bar',
          group: 'Charts'
        }
      },
      {
        path: 'chart-card',
        component: ChartCardDemoComponent,
        data: {
          title: 'Chart Card',
          group: 'Charts'
        }
      },

      { path: '', pathMatch: 'full', redirectTo: 'button' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StyleGuideRoutingModule {}
