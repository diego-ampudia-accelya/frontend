import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'bspl-search-panel-demo',
  templateUrl: './search-panel-demo.component.html',
  styleUrls: ['./search-panel-demo.component.scss']
})
export class SearchPanelDemoComponent implements OnInit {
  public templateFormSearchCriteria: string;
  public reactiveFormSearchCriteria: string;
  public reactivePersonForm: FormGroup;
  public person = {
    firstName: 'John',
    lastName: 'Doe',
    dateOfBirth: new Date(),
    age: 42
  };

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.reactivePersonForm = this.formBuilder.group({
      firstName: ['Reactive John'],
      lastName: [],
      dateOfBirth: [new Date()],
      age: []
    });
  }

  public updateTemplateFormSearchCriteria(searchCriteria: any): void {
    this.templateFormSearchCriteria = JSON.stringify(searchCriteria);
  }

  public updateReactiveFormSearchCriteria(searchCriteria: any): void {
    this.reactiveFormSearchCriteria = JSON.stringify(searchCriteria);
  }
}
