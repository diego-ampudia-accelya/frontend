import { Component } from '@angular/core';

@Component({
  selector: 'bspl-pagination-demo',
  templateUrl: './pagination-demo.component.html',
  styleUrls: ['./pagination-demo.component.scss']
})
export class PaginationDemoComponent {
  paginationConfig = {
    currentPage: 1,
    itemsPerPage: 10,
    itemsPerPageOptions: [
      {
        value: 1,
        label: 5
      },
      {
        value: 2,
        label: 10
      },
      {
        value: 3,
        label: 25
      },
      {
        value: 4,
        label: 50
      }
    ],
    totalItems: 172,
    totalPages: 8
  };
}
