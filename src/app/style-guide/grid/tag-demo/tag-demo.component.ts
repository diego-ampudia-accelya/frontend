import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'bspl-tag-demo',
  templateUrl: './tag-demo.component.html',
  styleUrls: ['./tag-demo.component.scss']
})
export class TagDemoComponent {
  carsList = [
    {
      value: 1,
      label: 'Ford'
    },
    {
      value: 2,
      label: 'Hyundai'
    },
    {
      value: 3,
      label: 'Honda'
    },
    {
      value: 4,
      label: 'Kia',
      disabled: true
    },
    {
      value: 5,
      label: 'Mazda'
    },
    {
      value: 6,
      label: 'Nissan'
    },
    {
      value: 7,
      label: 'Mercedes'
    },
    {
      value: 8,
      label: 'Toyota'
    },
    {
      value: 9,
      label: 'Subaru'
    }
  ];

  tags: string[] = ['Tag 1', 'Tag 2', 'Tag 3', 'Tag 4'];
  tagInputValue = new FormControl([this.carsList[2], this.carsList[4], this.carsList[8]]);
  tagInputValue1 = new FormControl([], Validators.required);

  onTagDelete(value) {
    this.tags = this.tags.filter(e => e !== value);
  }
}
