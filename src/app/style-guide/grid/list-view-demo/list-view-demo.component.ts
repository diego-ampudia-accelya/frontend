import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { cloneDeep } from 'lodash';

import { peopleTableConfig } from './column.config';
import { PeopleFilterFormatter } from './people-filter-formatter';
import { PeopleQueryable, Person } from './people-queryable.service';
import {
  DataQuery,
  DefaultApiFilterFormatter,
  DefaultQueryStorage,
  QueryableDataSource,
  QUERY_STORAGE_ID_SUFFIX
} from '~app/shared/components/list-view';
import { SelectMode } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

/* eslint-disable no-console */
@Component({
  selector: 'bspl-list-view-demo',
  templateUrl: './list-view-demo.component.html',
  styleUrls: ['./list-view-demo.component.scss'],
  providers: [
    DefaultQueryStorage,
    PeopleFilterFormatter,
    DefaultApiFilterFormatter,
    { provide: QUERY_STORAGE_ID_SUFFIX, useValue: 'demo' },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [PeopleQueryable]
    }
  ]
})
export class ListViewDemoComponent implements OnInit {
  public selectedCountry: string;
  public predefinedFilters: Partial<Person>;
  public tableRows = [];

  public reactiveForm: FormGroup;
  public columns = [];
  public countries: DropdownOption[] = [
    { label: 'USA', value: 'USA' },
    { label: 'UK', value: 'UK' },
    { label: 'Germany', value: 'Germany' }
  ];
  public selectMode = SelectMode;

  constructor(
    private formBuilder: FormBuilder,
    public dataSource: QueryableDataSource<Person>,
    public queryStorage: DefaultQueryStorage,
    public peopleFilterFormatter: PeopleFilterFormatter
  ) {}

  public ngOnInit(): void {
    this.reactiveForm = this.formBuilder.group({
      firstName: [],
      lastName: [],
      age: [],
      dateOfBirth: [],
      validFrom: [],
      validTo: []
    });

    const storedQuery = this.queryStorage.get();
    if (storedQuery) {
      this.selectedCountry = storedQuery.filterBy && storedQuery.filterBy.country;
      this.setPredefinedFilters();
      this.loadData(storedQuery);
    } else {
      this.selectCountry(this.countries[0]);
    }

    this.setColumns();
  }

  public search(query: DataQuery<Person>): void {
    this.loadData(query);
  }

  public selectCountry(country: DropdownOption) {
    this.selectedCountry = country.value;
    this.setPredefinedFilters();
    this.loadData({});
  }

  public create() {
    console.log('Create clicked!');
  }

  public download() {
    console.log('Download Clicked');
  }

  public onModify(event) {
    this.tableRows = event;
  }

  private loadData(query: Partial<DataQuery>) {
    const dataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy,
        country: this.selectedCountry
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private setPredefinedFilters() {
    this.predefinedFilters = { age: 34 };
  }

  private setColumns() {
    this.columns = cloneDeep(peopleTableConfig);

    this.columns.unshift({
      prop: 'isSelected',
      sortable: false,
      width: 40,
      resizeable: false,
      headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
      cellTemplate: 'checkboxWithDisableRowCellTmpl',
      cellClass: this.isChanged.bind(this)
    });
  }

  private isChanged({ row, column }) {
    const { prop } = column;

    return {
      'checkbox-control-changed': row[prop] === 'isSelected',
      'checkbox-control': true
    };
  }
}
