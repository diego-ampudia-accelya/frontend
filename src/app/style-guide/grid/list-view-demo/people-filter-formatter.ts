import { Injectable } from '@angular/core';

import { Person } from './people-queryable.service';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';

@Injectable()
export class PeopleFilterFormatter extends DefaultDisplayFilterFormatter {
  public format(filter: Partial<Person>): AppliedFilter[] {
    const { validFrom, validTo, ...rest } = filter;
    const formatteFilters = super.format(rest);

    if (validFrom || validTo) {
      const label = [this.normalizeValue(validFrom) || '*', this.normalizeValue(validTo) || '*'].join(' - ');

      formatteFilters.push({
        label,
        keys: ['validFrom', 'validTo'],
        closable: true
      });
    }

    return formatteFilters;
  }
}
