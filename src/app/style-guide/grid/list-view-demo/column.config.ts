import { TableColumn } from '@swimlane/ngx-datatable';

export const peopleTableConfig: TableColumn[] = [
  {
    prop: 'firstName',
    name: 'First Name'
  },
  {
    prop: 'lastName',
    name: 'Last Name'
  },
  {
    prop: 'country',
    name: 'Country'
  },
  {
    prop: 'age',
    name: 'Age'
  },
  {
    prop: 'dateOfBirth',
    name: 'Date Of Birth'
  },
  {
    prop: 'validFrom',
    name: 'Valid From'
  },
  {
    prop: 'validTo',
    name: 'Valid To'
  }
];
