import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import people from './people.mock.json';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ClientGridTableHelper } from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';

export interface Person {
  firstName: string;
  lastName: string;
  age: number;
  dateOfBirth: string;
  validFrom: string;
  validTo: string;
  country: string;
}

@Injectable({
  providedIn: 'root'
})
export class PeopleQueryable implements Queryable<Person> {
  private allPeople: Person[] = people;

  private helper = new ClientGridTableHelper<Person>();

  find(dataQuery: DataQuery): Observable<PagedData<Person>> {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return of(this.helper.query([...this.allPeople], RequestQuery.fromDataQuery(dataQuery))).pipe(delay(1000));
  }
}
