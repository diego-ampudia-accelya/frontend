import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ButtonDemoComponent } from './buttons/button-demo/button-demo.component';
import { ButtonMenuDemoComponent } from './buttons/button-menu-demo/button-menu-demo.component';
import { IconButtonDemoComponent } from './buttons/icon-button-demo/icon-button-demo.component';
import { ChartCardDemoComponent } from './charts/chart-card-demo/chart-card-demo.component';
import { HorizontalBarChartDemoComponent } from './charts/horizontal-bar-chart-demo/horizontal-bar-chart-demo.component';
import { VerticalBarChartDemoComponent } from './charts/vertical-bar-chart-demo/vertical-bar-chart-demo.component';
import { AutocompleteDemoComponent } from './forms/autocomplete-demo/autocomplete-demo.component';
import { CheckboxDemoComponent } from './forms/checkbox-demo/checkbox-demo.component';
import { DatePickerDemoComponent } from './forms/date-picker-demo/date-picker-demo.component';
import { FileUploadDemoComponent } from './forms/file-upload-demo/file-upload-demo.component';
import { InputDemoComponent } from './forms/input-demo/input-demo.component';
import { PeriodPickerDemoComponent } from './forms/period-picker-demo/period-picker-demo.component';
import { RadioButtonDemoComponent } from './forms/radio-button-demo/radio-button-demo.component';
import { RangeDropdownDemoComponent } from './forms/range-dropdown-demo/range-dropdown-demo.component';
import { SelectDemoComponent } from './forms/select-demo/select-demo.component';
import { TagsInputDemoComponent } from './forms/tags-input-demo/tags-input-demo.component';
import { ToggleDemoComponent } from './forms/toggle-demo/toggle-demo.component';
import { ValidatorsDemoComponent } from './forms/validators-demo/validators-demo.component';
import { ListViewDemoComponent } from './grid/list-view-demo/list-view-demo.component';
import { PaginationDemoComponent } from './grid/pagination-demo/pagination-demo.component';
import { SearchPanelDemoComponent } from './grid/search-panel-demo/search-panel-demo.component';
import { TagDemoComponent } from './grid/tag-demo/tag-demo.component';
import { AlertMessageDemoComponent } from './miscellaneous/alert-message-demo/alert-message-demo.component';
import { CardDemoComponent } from './miscellaneous/card-demo/card-demo.component';
import { DialogDemoComponent } from './miscellaneous/dialog-demo/dialog-demo.component';
import { FloatingPanelDemoComponent } from './miscellaneous/floating-panel-demo/floating-panel-demo.component';
import { NotificationDemoComponent } from './miscellaneous/notification-demo/notification-demo.component';
import { PropertyListDemoComponent } from './miscellaneous/property-list-demo/property-list-demo.component';
import { SelectionListDemoComponent } from './miscellaneous/selection-list-demo/selection-list-demo.component';
import { SpinnerDemoComponent } from './miscellaneous/spinner-demo/spinner-demo.component';
import { TabsDemoComponent } from './miscellaneous/tabs-demo/tabs-demo.component';
import { StyleGuideComponent } from '~app/style-guide/style-guide.component';
import { StyleGuideRoutingModule } from '~app/style-guide/style-guide-routing.module';
import { GithubService } from '~app/style-guide/forms/autocomplete-demo/github.service';
import { SharedModule } from '~app/shared/shared.module';
import { NotificationService } from '~app/shared/services/notification.service';
import { CoreModule } from '~app/core/core.module';

@NgModule({
  declarations: [
    StyleGuideComponent,
    ListViewDemoComponent,
    SpinnerDemoComponent,
    FloatingPanelDemoComponent,
    PeriodPickerDemoComponent,
    IconButtonDemoComponent,
    DialogDemoComponent,
    FileUploadDemoComponent,
    NotificationDemoComponent,
    InputDemoComponent,
    DatePickerDemoComponent,
    SelectDemoComponent,
    ButtonDemoComponent,
    CheckboxDemoComponent,
    SelectionListDemoComponent,
    PaginationDemoComponent,
    AutocompleteDemoComponent,
    TagDemoComponent,
    TabsDemoComponent,
    RadioButtonDemoComponent,
    ButtonMenuDemoComponent,
    AlertMessageDemoComponent,
    CardDemoComponent,
    SearchPanelDemoComponent,
    PropertyListDemoComponent,
    TagsInputDemoComponent,
    ValidatorsDemoComponent,
    RangeDropdownDemoComponent,
    ToggleDemoComponent,
    VerticalBarChartDemoComponent,
    HorizontalBarChartDemoComponent,
    ChartCardDemoComponent
  ],
  imports: [CommonModule, StyleGuideRoutingModule, FormsModule, ReactiveFormsModule, SharedModule, CoreModule],
  providers: [GithubService, NotificationService],
  exports: [StyleGuideComponent]
})
export class StyleGuideModule {}
