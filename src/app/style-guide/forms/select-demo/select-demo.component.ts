import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { DropdownOption } from '~app/shared/models';

@Component({
  selector: 'bspl-select-demo',
  templateUrl: './select-demo.component.html',
  styleUrls: ['./select-demo.component.scss']
})
export class SelectDemoComponent {
  basicSelect = new FormControl(null);
  basicSelectFilled = new FormControl(1);
  basicSelectRequired = new FormControl(null, Validators.required);
  basicSelectDisabled = new FormControl({ value: null, disabled: true });

  multipleSelectRequired = new FormControl(null, Validators.required);
  multipleSelectLocked = new FormControl([1, 2, 3, 4]);

  requiredMultiSelect = new FormControl('', Validators.required);
  requiredMultiSelect1 = new FormControl('', Validators.required);
  requiredSelect = new FormControl(null, Validators.required);
  requiredSelect1 = new FormControl(null, Validators.required);

  public carsList: DropdownOption[] = [
    {
      value: 1,
      label: 'Ford'
    },
    {
      value: 2,
      label: 'Hyundai'
    },
    {
      value: 3,
      label: 'Honda'
    },
    {
      value: 4,
      label: 'Kia',
      disabled: true
    },
    {
      value: 5,
      label: 'Mazda'
    },
    {
      value: 6,
      label: 'Nissan'
    },
    {
      value: 7,
      label: 'Mercedes'
    },
    {
      value: 8,
      label: 'Toyota'
    },
    {
      value: 9,
      label: 'Subaru'
    }
  ];

  public shortCarsList = this.carsList.slice(0, 3);

  public loadingStateOptions: DropdownOption[];

  public loadingStateDropdownOpened(): void {
    this.loadingStateOptions = null;

    setTimeout(() => {
      this.loadingStateOptions = this.carsList;
    }, 3000);
  }
}
