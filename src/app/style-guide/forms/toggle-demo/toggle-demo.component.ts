import { Component } from '@angular/core';

@Component({
  selector: 'bspl-toggle-demo',
  templateUrl: './toggle-demo.component.html',
  styleUrls: ['./toggle-demo.component.scss']
})
export class ToggleDemoComponent {
  public onToggleChange(event) {}
}
