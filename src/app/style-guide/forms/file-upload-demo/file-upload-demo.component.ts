import { Component, ElementRef, ViewChild } from '@angular/core';

import { ButtonDesign } from '~app/shared/components';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { DropdownOption } from '~app/shared/models';

@Component({
  selector: 'bspl-file-upload-demo',
  templateUrl: './file-upload-demo.component.html',
  styleUrls: ['./file-upload-demo.component.scss']
})
export class FileUploadDemoComponent {
  @ViewChild('uploadRef', { static: true })
  private uploadComponent: UploadComponent;

  @ViewChild('logMessageContainer', { static: true })
  private logMessageContainer: ElementRef<HTMLElement>;

  public filePickerMode = UploadModes.Preview;
  public multipleFiles1 = [];
  public multipleFiles2 = [];
  public targetDropAreaFiles1 = [];
  public enableDragAndDrop = true;
  public isDisabled = false;

  public modeOptions: DropdownOption<UploadModes>[] = [
    { value: UploadModes.Initial, label: 'Initial' },
    { value: UploadModes.Preview, label: 'Preview' }
  ];

  public windowDropAreaFiles = [];

  public logMessages: string[] = [];

  public isUploading = false;
  public buttonDesign = ButtonDesign;

  public upload() {
    this.uploadComponent.uploadFiles().subscribe(result => this.log(`Upload result: ${result}`));
  }

  public reset() {
    this.logMessages = [];
    this.uploadComponent.reset();
  }

  public onUploadStarted(mode: UploadModes): void {
    this.isUploading = true;
    this.log(`Upload started: ${mode}`);
  }

  public onUploadFinished(files: any[]): void {
    this.isUploading = false;
    this.log(`Upload finished: ${this.toJson(files)}`);
  }

  public onFilesChanged(files: FileUpload[]): void {
    const details = files.map(file => this.getFileMessage(file));
    this.log(`Files changed: ${this.toJson(details)}`);
  }

  public onResetInitialMode(): void {
    this.log('Reset to initial mode');
  }

  public onModeChange(mode: UploadModes): void {
    this.log(`Mode changed: ${mode}`);
  }

  public addFilesToTargetDropArea1(files) {
    this.targetDropAreaFiles1 = files;
  }

  public addFilesToWindowDropArea(files) {
    this.windowDropAreaFiles = files;
  }

  public onFilesChange(files) {
    this.windowDropAreaFiles = files;
  }

  private log(message: string): void {
    this.logMessages.push(message);
    const container = this.logMessageContainer.nativeElement;
    setTimeout(() => container.scrollTo({ top: container.scrollHeight, behavior: 'smooth' }));
  }

  private getFileMessage(file: FileUpload) {
    // Ignore properties that are not serializable
    const { name, size, isValid, error, status, progress, responseData } = file;

    return {
      name,
      size,
      isValid,
      error,
      status,
      progress,
      responseData
    };
  }

  private toJson(obj: any): string {
    return JSON.stringify(obj, null, '  ');
  }
}
