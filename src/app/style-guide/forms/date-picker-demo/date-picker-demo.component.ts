import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import moment from 'moment-mini';

import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-date-picker-demo',
  templateUrl: './date-picker-demo.component.html',
  styleUrls: ['./date-picker-demo.component.scss']
})
export class DatePickerDemoComponent implements OnInit {
  datetimeValue = new Date('2018-12-21');
  range = {
    min: moment().startOf('day').toDate(),
    max: moment().startOf('day').add(1, 'year').toDate()
  };

  requiredDatetimeControl = new FormControl('', Validators.required);
  requiredDatetimeControlWithValue = new FormControl(new Date('2018-12-15'), [Validators.pattern('a-z')]);

  public localDate = new Date();
  public localPredefinedDate = new Date('2020-10-20');
  public bspCurrentDate = this.localBspTimeService.getDate();
  public bspPredefinedDate = this.localBspTimeService.getDate(this.localPredefinedDate);

  constructor(private localBspTimeService: LocalBspTimeService) {}

  ngOnInit() {
    this.requiredDatetimeControl.markAsTouched();
    this.requiredDatetimeControlWithValue.markAsTouched();
  }
}
