import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { InputComponent } from '~app/shared/components';

@Component({
  selector: 'bspl-input-demo',
  templateUrl: './input-demo.component.html',
  styleUrls: ['./input-demo.component.scss']
})
export class InputDemoComponent implements OnInit, AfterViewInit {
  @ViewChild('autofocusInput', { static: true }) autofocusInput: InputComponent;

  disabledInput = new FormControl({ value: '', disabled: true });
  requiredInput = new FormControl('', [Validators.required, Validators.email, Validators.maxLength(8)]);

  ngOnInit() {
    this.requiredInput.markAsTouched();
  }

  ngAfterViewInit() {
    if (this.autofocusInput) {
      this.autofocusInput.focusElement();
    }
  }
}
