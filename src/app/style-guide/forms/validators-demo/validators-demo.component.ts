import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { alphanumericValidator } from '~app/shared/validators';

@Component({
  selector: 'bspl-validators-demo',
  templateUrl: './validators-demo.component.html',
  styleUrls: ['./validators-demo.component.scss']
})
export class ValidatorsDemoComponent implements OnInit {
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  public ngOnInit(): void {
    this.buildForm();
    this.form.markAllAsTouched();
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      alphanumericNoWithSpace: ['Zebra 123', [alphanumericValidator(false)]],
      nonAlphanumeric: ['Hello!@#$%^&*()_+=/*-', [alphanumericValidator()]],
      alphanumericMinLetters: ['A123', [alphanumericValidator(true, 2)]],
      alphanumericMinDigits: ['A12', [alphanumericValidator(true, 0, 3)]]
    });
  }
}
