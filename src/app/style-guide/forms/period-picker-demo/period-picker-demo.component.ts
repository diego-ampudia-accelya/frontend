import { Component } from '@angular/core';

import periods2017 from './es-periods-2017-mock.json';
import periods2018 from './es-periods-2018-mock.json';
import periods2019 from './es-periods-2019-mock.json';
import periods2020 from './es-periods-2020-mock.json';
import { PeriodOption } from '~app/shared/components';

@Component({
  selector: 'bspl-period-picker-demo',
  templateUrl: './period-picker-demo.component.html',
  styleUrls: ['./period-picker-demo.component.scss']
})
export class PeriodPickerDemoComponent {
  public periods: PeriodOption[] = [...periods2017, ...periods2018, ...periods2019, ...periods2020];
  public basicPeriod = { period: '2019073' };
  public disabledPeriod = { period: '2019072' };
  public singleMonthPeriods = [
    {
      period: '2019071'
    },
    {
      period: '2019073'
    }
  ];
  public multiMonthPeriods = [];
  public monthPeriod = [];
}
