import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'bspl-range-dropdown-demo',
  templateUrl: './range-dropdown-demo.component.html',
  styleUrls: ['./range-dropdown-demo.component.scss']
})
export class RangeDropdownDemoComponent {
  public rangeControl = new FormControl();
  public rangeControl2 = new FormControl();
}
