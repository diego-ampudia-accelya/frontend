import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RangeDropdownDemoComponent } from './range-dropdown-demo.component';

describe('RangeDropdownDemoComponent', () => {
  let component: RangeDropdownDemoComponent;
  let fixture: ComponentFixture<RangeDropdownDemoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RangeDropdownDemoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeDropdownDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
