import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'bspl-radio-button-demo',
  templateUrl: './radio-button-demo.component.html',
  styleUrls: ['./radio-button-demo.component.scss']
})
export class RadioButtonDemoComponent {
  radioButtonOptions = [
    {
      value: 'txt',
      label: 'TXT'
    },
    {
      value: 'csv',
      label: 'CSV'
    }
  ];

  radioButtonGroup = new FormControl(this.radioButtonOptions[0].value);
}
