import { Component } from '@angular/core';

import { CheckboxDesign } from '~app/shared/components';

@Component({
  selector: 'bspl-checkbox-demo',
  templateUrl: './checkbox-demo.component.html',
  styleUrls: ['./checkbox-demo.component.scss']
})
export class CheckboxDemoComponent {
  public checkboxDesign = CheckboxDesign;

  requiredErrMsg = { error: 'This field is required' };

  checkboxChangeDetection(isChecked: boolean) {}
}
