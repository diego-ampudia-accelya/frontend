import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { multiValueValidator } from '~app/shared/validators/multi-value.validator';

@Component({
  selector: 'bspl-tags-input-demo',
  templateUrl: './tags-input-demo.component.html',
  styleUrls: ['./tags-input-demo.component.scss']
})
export class TagsInputDemoComponent implements OnInit {
  public form: FormGroup;
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      tagInput: new FormControl(['Test1', 'Test2'], [Validators.required]),
      disabledTagsInput: new FormControl({ value: ['test'], disabled: true }),
      editableTags: new FormControl(['Editable Tag']),
      editableTagsWithValidation: new FormControl(['test@test.com'], multiValueValidator(Validators.email))
    });
  }
}
