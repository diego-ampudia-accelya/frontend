import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { GithubService } from '~app/style-guide/forms/autocomplete-demo/github.service';

@Component({
  selector: 'bspl-autocomplete-demo',
  templateUrl: './autocomplete-demo.component.html',
  styleUrls: ['./autocomplete-demo.component.scss'],
  providers: [GithubService]
})
export class AutocompleteDemoComponent implements OnInit {
  users = [];
  currentPage = 1;
  searchQuery = '';
  loading = false;
  requiredAutocomplete = new FormControl(null, Validators.required);

  constructor(private githubService: GithubService) {}

  ngOnInit() {
    this.searchUsers('test');
  }

  searchUsers(query: string) {
    this.searchQuery = '';
    this.currentPage = 1;
    this.users = [];

    this.getUsers(query, this.currentPage);
  }

  getMoreUsers() {
    if (!this.searchQuery.length) {
      return;
    }

    this.getUsers(this.searchQuery, this.currentPage);
  }

  getUsers(query: string, page: number) {
    this.loading = true;

    this.githubService.find(query, page).subscribe(
      res => {
        this.searchQuery = query;

        if (res.totalPages > 1 && this.currentPage !== res.totalPages) {
          this.currentPage += 1;
          this.users = this.users.concat(res.items);
        } else {
          this.users = res.items;
        }

        this.loading = false;
      },
      err => {
        this.loading = false;
        alert(err.error.message ? err.error.message : 'Something bad happened, please try again later!');
      }
    );
  }
}
