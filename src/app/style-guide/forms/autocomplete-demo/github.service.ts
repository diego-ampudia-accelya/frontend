import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class GithubService {
  private http: HttpClient;

  constructor(handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }

  find(query: string, page: number) {
    const opts = {
      query,
      itemsPerPage: 20,
      currentPage: page
    };

    const url = `https://api.github.com/search/users?q=${opts.query}&per_page=${opts.itemsPerPage}&page=${opts.currentPage}`;

    return this.http.get<any>(url).pipe(
      map(res => {
        const total = res.total_count || 0;

        return {
          items: res.items,
          totalItems: total,
          itemsPerPage: opts.itemsPerPage,
          currentPage: opts.currentPage,
          totalPages: total > 0 ? Math.ceil(total / opts.itemsPerPage) : total
        };
      })
    );
  }
}
