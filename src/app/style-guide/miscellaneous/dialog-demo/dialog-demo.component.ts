import { Component } from '@angular/core';

import { DialogService } from '~app/shared/components';

@Component({
  selector: 'bspl-dialog-demo',
  templateUrl: './dialog-demo.component.html',
  styleUrls: ['./dialog-demo.component.scss']
})
export class DialogDemoComponent {
  constructor(public dialogService: DialogService) {}
}
