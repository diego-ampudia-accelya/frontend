import { Component } from '@angular/core';

@Component({
  selector: 'bspl-property-list-demo',
  templateUrl: './property-list-demo.component.html',
  styleUrls: ['./property-list-demo.component.scss']
})
export class PropertyListDemoComponent {}
