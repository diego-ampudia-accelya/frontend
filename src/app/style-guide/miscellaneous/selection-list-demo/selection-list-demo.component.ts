import { Component } from '@angular/core';
import { cloneDeep } from 'lodash';

import { SELECTION_LIST } from '~app/shared/components/selection-list/selection-list.mock';

@Component({
  selector: 'bspl-selection-list-demo',
  templateUrl: './selection-list-demo.component.html',
  styleUrls: ['./selection-list-demo.component.scss']
})
export class SelectionListDemoComponent {
  selectionList = cloneDeep(SELECTION_LIST);
}
