import { Component } from '@angular/core';

import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-notification-demo',
  templateUrl: './notification-demo.component.html',
  styleUrls: ['./notification-demo.component.scss']
})
export class NotificationDemoComponent {
  public message = 'A notification message';

  constructor(public notificationService: NotificationService) {}
}
