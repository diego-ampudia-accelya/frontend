import { Component } from '@angular/core';

@Component({
  selector: 'bspl-spinner-demo',
  templateUrl: './spinner-demo.component.html',
  styleUrls: ['./spinner-demo.component.scss']
})
export class SpinnerDemoComponent {
  isSpinnerVisible: boolean;
  isSpinnerWithRelativeToVisible: boolean;

  toggleSpinner() {
    this.isSpinnerVisible = !this.isSpinnerVisible;
  }

  showSpinerWithRelativeTo() {
    this.isSpinnerWithRelativeToVisible = true;
    setTimeout(() => (this.isSpinnerWithRelativeToVisible = false), 2000);
  }
}
