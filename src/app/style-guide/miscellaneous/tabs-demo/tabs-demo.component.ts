import { Component } from '@angular/core';

import { Tab } from '~app/shared/components';

@Component({
  selector: 'bspl-tabs-demo',
  templateUrl: './tabs-demo.component.html',
  styleUrls: ['./tabs-demo.component.scss']
})
export class TabsDemoComponent {
  tabs: Tab[] = [
    {
      title: 'Properties',
      active: true
    },
    {
      title: 'Configuration'
    },
    {
      title: 'Ticketing Authority'
    }
  ];

  // selectionList = cloneDeep(SELECTION_LIST);
  activeTab: Tab = this.tabs.find(tab => tab.active);
}
