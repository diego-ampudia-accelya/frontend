import { Component } from '@angular/core';

@Component({
  selector: 'bspl-card-demo',
  templateUrl: './card-demo.component.html',
  styleUrls: ['./card-demo.component.scss']
})
export class CardDemoComponent {}
