import { Component, ViewChild } from '@angular/core';

import { FloatingPanelComponent } from '~app/shared/components';

@Component({
  selector: 'bspl-floating-panel-demo',
  templateUrl: './floating-panel-demo.component.html',
  styleUrls: ['./floating-panel-demo.component.scss']
})
export class FloatingPanelDemoComponent {
  @ViewChild('closablePanel') closablePanel: FloatingPanelComponent;

  public toggleFloatingPanel(): void {
    this.closablePanel.toggle();
  }
}
