import { Component } from '@angular/core';

import { AlertMessageType } from '~app/shared/enums';

@Component({
  selector: 'bspl-alert-message-demo',
  templateUrl: './alert-message-demo.component.html',
  styleUrls: ['./alert-message-demo.component.scss']
})
export class AlertMessageDemoComponent {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  AlertMessageType = AlertMessageType;
}
