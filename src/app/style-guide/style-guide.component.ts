import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, UrlTree } from '@angular/router';
import { merge, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, map, mapTo } from 'rxjs/operators';

import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';

@Component({
  selector: 'bspl-style-guide',
  templateUrl: './style-guide.component.html',
  styleUrls: ['./style-guide.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StyleGuideComponent implements OnInit {
  public menuItems: RoutedMenuItem[];
  public activeItem$: Observable<RoutedMenuItem>;

  constructor(private menuBuilder: MenuBuilder, private activatedRoute: ActivatedRoute, private router: Router) {}

  public ngOnInit(): void {
    this.menuItems = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    const navigationEnd$ = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      mapTo(null)
    );

    this.activeItem$ = merge(navigationEnd$, of(null)).pipe(
      map(() => this.menuItems.find(item => this.router.isActive(this.getUrl(item), false))),
      distinctUntilChanged()
    );
  }

  public selectDemoItem({ item }: { item: RoutedMenuItem }): void {
    this.router.navigateByUrl(this.getUrl(item));
  }

  private getUrl(item: RoutedMenuItem): UrlTree {
    return this.router.createUrlTree([item.route], { relativeTo: this.activatedRoute });
  }
}
