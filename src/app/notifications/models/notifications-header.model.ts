/* eslint-disable @typescript-eslint/naming-convention */
import { BspDto } from '~app/shared/models/bsp.model';

export interface NotificationCount {
  count: number;
}

export interface NotificationHeader {
  id: string;
  bspIsoc: string;
  status: NotificationStatus;
  sourceType: NotificationSourceType;
  sourceNumber: string;
  sourceId: string;
  type: NotificationType;
  action: NotificationAction;
  createdAt: Date;
}

export enum NotificationStatus {
  read = 'READ',
  unread = 'UNREAD'
}

export enum NotificationAction {
  //ACDM actions
  ACM_ADM_ISSUE = 'ACM_ADM_ISSUE',
  ACM_ADM_STATUS_CHANGE = 'ACM_ADM_STATUS_CHANGE',
  ACM_ADM_REQUEST_ISSUE = 'ACM_ADM_REQUEST_ISSUE',
  ACM_ADM_REQUEST_MODIF = 'ACM_ADM_REQUEST_MODIF',
  ACM_ADM_DISPUTE = 'ACM_ADM_DISPUTE',
  ACM_ADM_REJECT_DISPUTE = 'ACM_ADM_REJECT_DISPUTE',
  ADM_DELETE_DISPUTE = 'ADM_DELETE_DISPUTE',
  BSP_ADJUSTMENT = 'BSP_ADJUSTMENT',
  NOT_ACTION_ADM_DISPUTE = 'NOT_ACTION_ADM_DISPUTE',

  //Refund
  RA_STATUS_CHANGE = 'RA_STATUS_CHANGE',
  RA_ISSUE = 'RA_ISSUE',

  // PBD
  PBD_STATUS_CHANGE_AGENT = 'PBD_STATUS_CHANGE_AGENT',
  PBD_STATUS_CHANGE_AIRLINE = 'PBD_STATUS_CHANGE_AIRLINE',
  PBD_ISSUE = 'PBD_ISSUE',

  //Ticketing Authority Settings
  TA_MODIFICATION = 'TA_MODIFICATION',

  //Files Settings
  NEW_FILES_DOWNLOAD = 'NEW_FILES_DOWNLOAD',
  IATA_NOTIFICATION = 'IATA_NOTIFICATION',

  //ADM Policy Settings
  ADM_POLICY_CHANGE = 'ADM_POLICY_CHANGE',

  // Agent Own Cards
  OWN_CARD_ABOUT_TO_EXPIRE = 'OWN_CARD_ABOUT_TO_EXPIRE'
}

export enum NotificationSourceType {
  RFND_RA = 'RFND-RA',
  PBD = 'PBD',
  TA = 'TA',
  FILE = 'FILE',
  ADM_POLICY = 'ADM-POLICY',
  SPDR = 'SPDR',
  SPCR = 'SPCR',
  ADMA = 'ADMA',
  ACMA = 'ACMA',
  ADMD = 'ADMD',
  ACMD = 'ACMD',
  ADNT = 'ADNT',
  ACNT = 'ACNT',
  ACM_REQUEST = 'ACM_REQUEST',
  ADM_REQUEST = 'ADM_REQUEST',
  OWN_CARD = 'OWN-CARD'
}

export enum NotificationType {
  ACDM = 'ACDM',
  REFUND = 'REFUND',
  PBD = 'PBD',
  TICKETING_AUTHORITY = 'TICKETING_AUTHORITY',
  ADM_POLICY = 'ADM_POLICY',
  FILE = 'UPLOADED_FILES',
  TIP = 'TIP'
}

export const notificationTypeToActionMapper: { [k in NotificationType]: NotificationAction[] } = {
  [NotificationType.ACDM]: [
    NotificationAction.ACM_ADM_ISSUE,
    NotificationAction.ACM_ADM_STATUS_CHANGE,
    NotificationAction.ACM_ADM_REQUEST_ISSUE,
    NotificationAction.ACM_ADM_REQUEST_MODIF,
    NotificationAction.ACM_ADM_DISPUTE,
    NotificationAction.ACM_ADM_REJECT_DISPUTE,
    NotificationAction.ADM_DELETE_DISPUTE,
    NotificationAction.BSP_ADJUSTMENT,
    NotificationAction.NOT_ACTION_ADM_DISPUTE
  ],
  [NotificationType.ADM_POLICY]: [NotificationAction.ADM_POLICY_CHANGE],
  [NotificationType.FILE]: [NotificationAction.NEW_FILES_DOWNLOAD, NotificationAction.IATA_NOTIFICATION],
  [NotificationType.PBD]: [
    NotificationAction.PBD_ISSUE,
    NotificationAction.PBD_STATUS_CHANGE_AGENT,
    NotificationAction.PBD_STATUS_CHANGE_AIRLINE
  ],
  [NotificationType.REFUND]: [NotificationAction.RA_ISSUE, NotificationAction.RA_STATUS_CHANGE],
  [NotificationType.TICKETING_AUTHORITY]: [NotificationAction.TA_MODIFICATION],
  [NotificationType.TIP]: [NotificationAction.OWN_CARD_ABOUT_TO_EXPIRE]
};

export interface NotificationsListFilter {
  bspIsoc: BspDto[];
  type: NotificationType[];
  sourceNumber: string;
  action: NotificationAction[];
  createdAt: Date[];
  status: NotificationStatus;
}
