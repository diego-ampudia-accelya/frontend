import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { NotificationsHeaderService } from '../services/notifications-header.service';
import { NotificationsListFilterFormatter } from '../services/notifications-list-filter-formatter.service';
import { NotificationsListComponent } from './notifications-list.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { TranslatePipeMock } from '~app/test';

describe('NotificationsListComponent', () => {
  let component: NotificationsListComponent;
  let fixture: ComponentFixture<NotificationsListComponent>;
  let queryableDataSourceMock: SpyObject<QueryableDataSource<any>>;
  let queryStorageMock: SpyObject<DefaultQueryStorage>;

  const query = {
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  } as DataQuery;

  const bspsList = [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }];

  const expectedUserDetails = {
    id: 10126,
    email: 'iata@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.IATA,
    permissions: ['rAdmCatRsn', 'uAdmCatRsn'],
    bspPermissions: [],
    bsps: bspsList
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    }
  };

  beforeEach(async () => {
    queryableDataSourceMock = createSpyObject(QueryableDataSource, { appliedQuery$: of(query), loading$: of(true) });
    queryableDataSourceMock.get.and.returnValue(of(query));

    queryStorageMock = createSpyObject(DefaultQueryStorage);
    queryStorageMock.get.and.returnValue(query);

    await TestBed.configureTestingModule({
      declarations: [NotificationsListComponent, TranslatePipeMock],
      providers: [
        mockProvider(NotificationsListFilterFormatter),
        mockProvider(NotificationsHeaderService),
        mockProvider(PermissionsService),
        mockProvider(BspsDictionaryService),
        mockProvider(L10nTranslationService),
        mockProvider(DatePipe),
        mockProvider(FormBuilder),
        provideMockStore({ initialState })
      ],
      imports: [HttpClientTestingModule]
    })
      .overrideComponent(NotificationsListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceMock },
            { provide: DefaultQueryStorage, useValue: queryStorageMock }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(() => {
    const bspDictionaryServiceMock = TestBed.inject(BspsDictionaryService) as SpyObject<BspsDictionaryService>;
    bspDictionaryServiceMock.getAllBspsByPermissions.and.returnValue(of(bspsList));

    const translationServiceMock = TestBed.inject(L10nTranslationService) as SpyObject<L10nTranslationService>;
    translationServiceMock.translate.and.returnValue('');

    const permissionsServiceMock = TestBed.inject(PermissionsService) as SpyObject<PermissionsService>;
    permissionsServiceMock.hasPermission.and.returnValue(true);

    fixture = TestBed.createComponent(NotificationsListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit initializations', () => {
    it('should load search query', () => {
      component.ngOnInit();

      expect(component.dataSource.get).toHaveBeenCalledWith(
        jasmine.objectContaining({
          filterBy: component.predefinedFilters,
          paginateBy: query.paginateBy,
          sortBy: query.sortBy
        })
      );
      expect(component['queryStorage'].save).toHaveBeenCalledWith(
        jasmine.objectContaining({
          filterBy: component.predefinedFilters,
          paginateBy: query.paginateBy,
          sortBy: query.sortBy
        })
      );
    });

    it('should initialize loading', () => {
      component.ngOnInit();

      expect(component.loading$).toBeDefined();
    });

    it('should initialize logged in user', () => {
      component.ngOnInit();

      expect(component['loggedUser']).toBeDefined();
      expect(component.userType).toBeDefined();
    });

    it('should initialize columns', () => {
      component.ngOnInit();

      expect(component.columns).toBeDefined();
    });

    it('should initialize searchForm', () => {
      component.ngOnInit();

      expect(component.searchForm).toBeDefined();
    });

    it('should load bsps', fakeAsync(() => {
      component.ngOnInit();
      tick();

      const bspsIds = component.bspCountriesList.map(option => option.value.id);
      expect(bspsList.every(bsp => bspsIds.includes(bsp.id))).toBe(true);
    }));

    it('should set isBspFilterMultiple correctly', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(component.bspCountriesList.length).toBe(1);
      expect(component.isBspFilterMultiple).toBe(false);
    }));

    it('should set isBspFilterMultiple correctly if one bsp', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(component.bspCountriesList.length).toBe(1);
      expect(component.isBspFilterLocked).toBe(true);
    }));

    it('should set isBspFilterMultiple correctly if two bsps', fakeAsync(() => {
      const bspDictionaryServiceMock = TestBed.inject(BspsDictionaryService) as SpyObject<BspsDictionaryService>;
      bspDictionaryServiceMock.getAllBspsByPermissions.and.returnValue(of([bspsList[0], bspsList[0]]));

      component.ngOnInit();
      tick();

      expect(component.bspCountriesList.length).toBe(2);
      expect(component.isBspFilterLocked).toBe(false);
    }));

    it('should set predefinedFilters with the bsp by default', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(component.predefinedFilters.bspIsoc).toEqual(bspsList);
    }));

    it('should not set predefinedFilters if user is agent', fakeAsync(() => {
      const storeMock = TestBed.inject(MockStore) as SpyObject<MockStore>;
      storeMock.overrideSelector(getUser, {
        ...expectedUserDetails,
        userType: UserType.AGENT
      });

      component.ngOnInit();
      tick();

      expect(component.userType).toBe(UserType.AGENT);
      expect(component.predefinedFilters).toBeUndefined();
    }));

    it('should populate filters', () => {
      component.ngOnInit();

      expect(component.typesList).toBeDefined();
      expect(component.actionsList$).toBeDefined();
      expect(component.statusList).toBeDefined();
    });
  });

  it('should set loadingBsps when Bsps are being retrieved', fakeAsync(() => {
    const spy = spyOn(component.loadingBsps$, 'next');

    component.ngOnInit();
    expect(spy).toHaveBeenCalledWith(true);
    expect(spy).toHaveBeenCalledWith(false);
  }));

  it('should mark notification as read when viewing it', () => {
    const notificationsHeaderServiceMock = TestBed.inject(
      NotificationsHeaderService
    ) as SpyObject<NotificationsHeaderService>;
    notificationsHeaderServiceMock.markNotificationAsRead.and.returnValue(of(true));
    const updateAcmAdmRequestNotificationSourceTypeSpy = spyOn<any>(
      component,
      'updateAcmAdmRequestNotificationSourceType'
    );

    component.onViewNotificationSource(null);
    expect(notificationsHeaderServiceMock.markNotificationAsRead).toHaveBeenCalled();
    expect(updateAcmAdmRequestNotificationSourceTypeSpy).toHaveBeenCalled();
  });

  it('should navigate to notification when viewing it', () => {
    const notificationsHeaderServiceMock = TestBed.inject(
      NotificationsHeaderService
    ) as SpyObject<NotificationsHeaderService>;
    notificationsHeaderServiceMock.markNotificationAsRead.and.returnValue(of(true));
    const updateAcmAdmRequestNotificationSourceTypeSpy = spyOn<any>(
      component,
      'updateAcmAdmRequestNotificationSourceType'
    );

    component.onViewNotificationSource(null);
    expect(notificationsHeaderServiceMock.navigateToNotificationLink).toHaveBeenCalled();
    expect(updateAcmAdmRequestNotificationSourceTypeSpy).toHaveBeenCalled();
  });

  it('should build columns', () => {
    expect(component['buildColumns']()).toEqual(
      jasmine.arrayContaining<any>([
        jasmine.objectContaining({ prop: 'bspIsoc' }),
        jasmine.objectContaining({ prop: 'sourceType' }),
        jasmine.objectContaining({ prop: 'action' }),
        jasmine.objectContaining({ prop: 'sourceNumber' }),
        jasmine.objectContaining({ prop: 'createdAt' }),
        jasmine.objectContaining({ prop: 'status' })
      ])
    );
  });

  it('should call next onDestroy', () => {
    const spy = spyOn(component['destroy$'], 'next');

    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });
});
