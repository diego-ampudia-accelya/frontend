import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import moment from 'moment-mini';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter, first, map, takeUntil, tap } from 'rxjs/operators';

import {
  NotificationAction,
  NotificationHeader,
  NotificationsListFilter,
  NotificationSourceType,
  NotificationStatus,
  NotificationType,
  notificationTypeToActionMapper
} from '../models/notifications-header.model';
import { NotificationsHeaderService } from '../services/notifications-header.service';
import { NotificationsListFilterFormatter } from '../services/notifications-list-filter-formatter.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-notifications-list',
  templateUrl: './notifications-list.component.html',
  styleUrls: ['./notifications-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    { provide: QUERYABLE, useClass: NotificationsHeaderService },
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [NotificationsHeaderService] }
  ]
})
export class NotificationsListComponent implements OnInit, OnDestroy {
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public formFactory: FormUtil;

  public bspCountriesList: DropdownOption<BspDto>[];
  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;

  public maxDate = moment().endOf('day').toDate();

  public userTypes = UserType;
  public userType: UserType;

  public typesList: DropdownOption<NotificationType>[];
  public actionsList$: Observable<DropdownOption<NotificationAction>[]>;
  public statusList: DropdownOption<NotificationStatus>[];

  public predefinedFilters: Partial<NotificationsListFilter>;

  public loading$: Observable<boolean>;
  public loadingBsps$ = new BehaviorSubject(true);

  private loggedUser: User;

  private bspControl: FormControl;
  private typeControl: FormControl;
  private hasLeanPermission: boolean;

  private destroy$ = new Subject();

  constructor(
    public filterFormatter: NotificationsListFilterFormatter,
    public dataSource: QueryableDataSource<NotificationHeader>,
    private notificationsHeaderService: NotificationsHeaderService,
    private permissionsService: PermissionsService,
    private bspsDictionaryService: BspsDictionaryService,
    private translationService: L10nTranslationService,
    private queryStorage: DefaultQueryStorage,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeLoading();

    this.initializeLoggedUser();
    this.initializePermissions();

    this.searchForm = this.buildSearchForm();
    this.columns = this.buildColumns();

    const storedQuery = this.queryStorage.get() || defaultQuery;

    this.initializeLeanBspFilter$()
      .pipe(tap(() => this.loadData(storedQuery)))
      .subscribe(bspOptions => (this.bspCountriesList = bspOptions));

    this.populateFilterDropdowns();
    this.initializeBspControlListener();
  }

  public loadData(query: DataQuery): void {
    query = query || cloneDeep(defaultQuery);

    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onViewNotificationSource(notification: NotificationHeader): void {
    this.markNotificationAsRead(notification);
    // TODO temporal solution until BE is developed
    this.updateAcmAdmRequestNotificationSourceType(notification);
    this.notificationsHeaderService.navigateToNotificationLink(notification);
  }

  public rowClassesModifier = (dataItem: NotificationHeader, classes: string[]): void => {
    if (dataItem.status === NotificationStatus.unread) {
      classes.push('row-unread');
    }
  };

  private updateAcmAdmRequestNotificationSourceType(notification: NotificationHeader): void {
    const { action, sourceType } = notification;
    const isRequestStatus = action?.toUpperCase().includes('REQUEST');

    if (isRequestStatus) {
      const requestCodeMappings = {
        [NotificationSourceType.ACMA]: NotificationSourceType.ACM_REQUEST,
        [NotificationSourceType.ADMA]: NotificationSourceType.ADM_REQUEST
      };

      const transitionCodeRequest = requestCodeMappings[sourceType];
      if (transitionCodeRequest) {
        notification.sourceType = transitionCodeRequest;
      }
    }
  }

  private markNotificationAsRead(notification: NotificationHeader): void {
    this.notificationsHeaderService.markNotificationAsRead(notification).subscribe();
  }

  private initializeLoggedUser(): void {
    this.store.pipe(select(getUser), first()).subscribe(user => {
      this.loggedUser = user;
      this.userType = user.userType;
    });
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeLoading(): void {
    this.loading$ = this.dataSource.loading$.pipe(map(queryLoading => queryLoading || this.loadingBsps$.value));
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    this.loadingBsps$.next(true);

    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, [Permissions.readNotificationSettings])
      .pipe(
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspOptions => (this.isBspFilterMultiple = this.hasLeanPermission && bspOptions.length !== 1)),
        tap(bspOptions => (this.isBspFilterLocked = bspOptions.length === 1)),
        tap(bspOptions => {
          // Update BSP selected ids and predefined filters with the default BSP
          const onlyOneBsp = bspOptions.length === 1;

          if (bspOptions.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspOptions.find(
              bspOption => bspOption.value.isoCountryCode === this.loggedUser.defaultIsoc
            );

            const firstListBsp = bspOptions[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            // Agent users have not predefined filters
            if (this.userType !== UserType.AGENT) {
              this.predefinedFilters = {
                ...this.predefinedFilters,
                bspIsoc: [filterValue.value]
              };
            }
          }
        }),
        tap(() => this.loadingBsps$.next(false))
      );
  }

  private buildSearchForm(): FormGroup {
    this.bspControl = new FormControl(null);
    this.typeControl = new FormControl(null);

    return this.formFactory.createGroup<NotificationsListFilter>({
      bspIsoc: this.bspControl,
      type: this.typeControl,
      sourceNumber: null,
      action: null,
      createdAt: null,
      status: null
    });
  }

  private populateFilterDropdowns(): void {
    this.typesList = Object.keys(NotificationType)
      .map(key => ({
        value: NotificationType[key],
        label: this.translationService.translate(`notificationsMenu.notificationType.${key}`)
      }))
      .sort((a, b) => a.label.localeCompare(b.label));

    this.actionsList$ = this.typeControl.valueChanges.pipe(
      map(types => types || []),
      map(types => {
        const allActions = Object.keys(NotificationAction) as NotificationAction[];
        const filteredActions = [];
        types.forEach(type => filteredActions.push(...notificationTypeToActionMapper[type]));

        return types.length === 0 ? allActions : filteredActions;
      }),
      map(filteredActions =>
        filteredActions
          .map(action => ({
            value: NotificationAction[action],
            label: this.translationService.translate(`LIST.notificationsList.filters.options.action.${action}`)
          }))
          .sort((a, b) => a.label.localeCompare(b.label))
      )
    );

    this.statusList = Object.keys(NotificationStatus)
      .map(key => ({
        value: NotificationStatus[key],
        label: this.translationService.translate(`notificationsMenu.notificationStatus.${key}`)
      }))
      .sort((a, b) => a.label.localeCompare(b.label));
  }

  private initializeBspControlListener(): void {
    this.bspControl.valueChanges
      .pipe(
        filter(newValue => !!newValue),
        takeUntil(this.destroy$)
      )
      .subscribe(newValue => {
        const valueAsArray = Array.isArray(newValue) ? newValue : [newValue];
        this.bspControl.setValue(this.isBspFilterMultiple ? valueAsArray : valueAsArray[0], { emitEvent: false });
      });
  }

  private buildColumns(): GridColumn[] {
    return [
      {
        prop: 'bspIsoc',
        name: 'LIST.notificationsList.columns.bspIsoc',
        resizeable: true,
        sortable: this.hasLeanPermission,
        flexGrow: 1
      },
      {
        prop: 'sourceType',
        name: 'LIST.notificationsList.columns.sourceType',
        resizeable: true,
        sortable: true,
        flexGrow: 1
      },
      {
        prop: 'action',
        name: 'LIST.notificationsList.columns.action',
        resizeable: true,
        sortable: true,
        pipe: {
          transform: value => this.translationService.translate(`LIST.notificationsList.notificationAction.${value}`)
        },
        flexGrow: 1
      },
      {
        prop: 'sourceNumber',
        name: 'LIST.notificationsList.columns.sourceSubject',
        resizeable: true,
        sortable: true,
        cellTemplate: 'commonLinkFromObjCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'createdAt',
        name: 'LIST.notificationsList.columns.createdAt',
        resizeable: true,
        sortable: true,
        pipe: { transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss') },
        flexGrow: 1
      },
      {
        prop: 'status',
        name: 'LIST.notificationsList.columns.status',
        resizeable: true,
        sortable: true,
        flexGrow: 1
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
