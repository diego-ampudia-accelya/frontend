import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotificationsListComponent } from './notifications-list/notifications-list.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    data: { tab: ROUTES.NOTIFICATIONS_LIST },
    component: NotificationsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule {}
