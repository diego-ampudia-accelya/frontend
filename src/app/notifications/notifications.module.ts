import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NotificationsListComponent } from './notifications-list/notifications-list.component';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationsListFilterFormatter } from './services/notifications-list-filter-formatter.service';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [NotificationsListComponent],
  imports: [CommonModule, NotificationsRoutingModule, SharedModule],
  providers: [NotificationsListFilterFormatter]
})
export class NotificationsModule {}
