import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { NotificationHeader, NotificationSourceType, NotificationStatus } from '../models/notifications-header.model';
import { NotificationsHeaderService } from './notifications-header.service';
import { ViewListActions } from '~app/core/actions';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { UserType } from '~app/shared/models/user.model';
import { WatcherDocType, WatcherModel } from '~app/shared/models/watcher.model';
import { AppConfigurationService, TabService } from '~app/shared/services';
import { TipStatus } from '~app/tip/tip-cards/models/tip-card.model';

describe('NotificationsHeaderService', () => {
  let service: NotificationsHeaderService;
  let mockStore: MockStore<AppState>;
  const initialState = {
    auth: {
      user: {
        userType: UserType.AIRLINE,
        permissions: [],
        defaultIsoc: 'ES',
        bsps: [
          { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2021-01-01' },
          { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2021-01-01' }
        ]
      }
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        mockProvider(TabService),
        provideMockStore({ initialState })
      ],
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    service = TestBed.inject(NotificationsHeaderService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call the right endpoint when sending find request', () => {
    spyOn(service['http'], 'get').and.returnValue(of(null));

    service.find({ filterBy: {}, paginateBy: {}, sortBy: null }).subscribe(() => {});

    expect(service['http'].get).toHaveBeenCalledWith('/notification-management/notifications');
  });

  it('should call the right endpoint when sending getCount request', () => {
    spyOn(service['http'], 'get').and.returnValue(of({ count: 0 }));

    service.getInfiniteBellCount().subscribe(() => {});

    expect(service['http'].get).toHaveBeenCalledWith('/notification-management/notifications/count');
  });

  it('should update bell count with number passed as param', fakeAsync(() => {
    const count = 3;
    let result;

    service
      .getInfiniteBellCount()
      .pipe(first())
      .subscribe(response => (result = response));

    service.updateBellCount(count);
    tick();
    expect(result).toBe(count.toString());
  }));

  it('should set 99+ when count is higher than 99', fakeAsync(() => {
    const count = 100;
    let result;

    service
      .getInfiniteBellCount()
      .pipe(first())
      .subscribe(response => (result = response));

    service.updateBellCount(count);
    tick();
    expect(result).toBe('99+');
  }));

  it('should request the current bell count when calling update with no param', fakeAsync(() => {
    spyOn(service, 'getInstantBellCount').and.returnValue(of('1'));

    service.getInfiniteBellCount().pipe(first()).subscribe();
    service.updateBellCount();
    tick();

    expect(service.getInstantBellCount).toHaveBeenCalled();
  }));

  it('should call the right endpoint with default params when getting notifications', () => {
    const defaultParams = {
      params: { status: NotificationStatus.unread, sort: `createdAt,${SortOrder.Desc}` }
    };
    spyOn(service['http'], 'get').and.returnValue(of({ total: 0 }));

    service.getNotifications().subscribe(() => {});

    expect(service['http'].get).toHaveBeenCalledWith('/notification-management/notifications', defaultParams);
  });

  it('should call the right endpoint and params when marking notification as read', () => {
    const mockedId = '1';
    const markAsReadParams = { status: NotificationStatus.read };

    spyOn(service['http'], 'put').and.returnValue(of(null));

    service.markNotificationAsRead({ id: mockedId } as NotificationHeader).subscribe(() => {});

    expect(service['http'].put).toHaveBeenCalledWith(
      '/notification-management/notifications/' + mockedId,
      markAsReadParams
    );
  });

  it('should return the right redirection URL from a notification', () => {
    let result;

    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.ACMA,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.ACM_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.ACMD,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.ACMD_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.ADMA,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.ADM_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.ADMD,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.ADMD_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.ACNT,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.ACNT_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.ADNT,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.ADNT_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.SPCR,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.SPCR_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.SPDR,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.SPDR_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.ADM_POLICY,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.ADM_POLICIES_QUERY.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.PBD,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.POST_BILLING_DISPUTE_LIST.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.RFND_RA,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.REFUNDS_APP_VIEW.url);
    result = service.getNotificationRedirectionUrl({
      sourceType: NotificationSourceType.FILE,
      sourceId: '1'
    } as NotificationHeader);
    expect(result[0]).toBe(ROUTES.REPORTS.url);
  });

  it('should call the right endpoint and params when updating watcher', () => {
    const mockedBspId = 1;
    const mockedPayload: WatcherModel = { documentId: 1, documentType: WatcherDocType.ACDM, watching: true };
    spyOn(service['http'], 'put').and.returnValue(of(null));

    service.updateWatcher(mockedPayload, mockedBspId).subscribe(() => {});

    expect(service['http'].put).toHaveBeenCalledWith(
      `/notification-management/bsps/${mockedBspId}/watches`,
      mockedPayload
    );
  });

  it('should call the right endpoint and params when getting watcher', () => {
    const mockedBspId = 1;
    const mockedDocumentId = 1;
    const mockedDocumentType = WatcherDocType.ACDM;
    spyOn(service['http'], 'get').and.returnValue(of(null));

    service.getWatcher(mockedBspId, mockedDocumentId, mockedDocumentType).subscribe(() => {});

    expect(service['http'].get).toHaveBeenCalledWith(`/notification-management/bsps/${mockedBspId}/watches`, {
      params: { documentType: mockedDocumentType, documentId: mockedDocumentId.toString() }
    });
  });

  describe('getNotificationRedirectionUrl', () => {
    it('should return ACM_VIEW url when sourceType is ACMA', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ACMA
      });

      expect(res).toEqual([ROUTES.ACM_VIEW.url, '05/24']);
    });

    it('should return ACMD_VIEW url when sourceType is ACMD', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ACMD
      });

      expect(res).toEqual([ROUTES.ACMD_VIEW.url, '05/24']);
    });

    it('should return ADM_VIEW url when sourceType is ADMA', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ADMA
      });

      expect(res).toEqual([ROUTES.ADM_VIEW.url, '05/24']);
    });

    it('should return ADMD_VIEW url when sourceType is ADMD', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ADMD
      });

      expect(res).toEqual([ROUTES.ADMD_VIEW.url, '05/24']);
    });

    it('should return ACNT_VIEW url when sourceType is ACNT', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ACNT
      });

      expect(res).toEqual([ROUTES.ACNT_VIEW.url, '05/24']);
    });

    it('should return ADNT_VIEW url when sourceType is ADNT', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ADNT
      });

      expect(res).toEqual([ROUTES.ADNT_VIEW.url, '05/24']);
    });

    it('should return SPCR_VIEW url when sourceType is SPCR', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.SPCR
      });

      expect(res).toEqual([ROUTES.SPCR_VIEW.url, '05/24']);
    });

    it('should return SPDR_VIEW url when sourceType is SPDR', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.SPDR
      });

      expect(res).toEqual([ROUTES.SPDR_VIEW.url, '05/24']);
    });

    it('should return ACM_REQUEST_VIEW url when sourceType is ACM_REQUEST', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ACM_REQUEST
      });

      expect(res).toEqual([ROUTES.ACM_REQUEST_VIEW.url, '05/24']);
    });

    it('should return ADM_REQUEST_VIEW url when sourceType is ADM_REQUEST', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.ADM_REQUEST
      });

      expect(res).toEqual([ROUTES.ADM_REQUEST_VIEW.url, '05/24']);
    });

    it('should return POST_BILLING_DISPUTE_LIST url when sourceType is PBD', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.PBD
      });

      expect(res).toEqual([ROUTES.POST_BILLING_DISPUTE_LIST.url, '05/24']);
    });

    it('should return REFUNDS_APP_VIEW url when sourceType is RFND_RA', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.RFND_RA
      });

      expect(res).toEqual([ROUTES.REFUNDS_APP_VIEW.url, '05/24']);
    });

    it('should return TIP_CARDS url when sourceType is OWN_CARD', () => {
      const res = (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.OWN_CARD
      });

      expect(res).toEqual([ROUTES.TIP_CARDS.url]);
    });

    it('should dispatch ViewListActions.FilterChange when sourceType is ADM_POLICY', () => {
      (service as any).getNotificationRedirectionUrl({
        sourceNumber: '05/24',
        sourceType: NotificationSourceType.ADM_POLICY
      });

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterChange({
          viewListId: `${ROUTES.ADM_POLICIES_QUERY.tabLabel}:default`,
          filter: { iataCode: ['05/24'] }
        })
      );

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterFormValueChange({
          viewListId: `${ROUTES.ADM_POLICIES_QUERY.tabLabel}:default`,
          value: { iataCode: ['05/24'] }
        })
      );
    });

    it('should dispatch ViewListActions.FilterChange and FilterFormValueChange when sourceType is FILE', () => {
      (service as any).getNotificationRedirectionUrl({
        sourceNumber: '05/24',
        sourceType: NotificationSourceType.FILE
      });

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterChange({
          viewListId: `${ROUTES.REPORTS.tabLabel}:inbox`,
          filter: { name: '05/24' }
        })
      );

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterFormValueChange({
          viewListId: `${ROUTES.REPORTS.tabLabel}:inbox`,
          value: { name: '05/24' }
        })
      );
    });

    it('should dispatch ViewListActions.FilterFormValueChange when sourceType is OWN_CARD', () => {
      (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.OWN_CARD
      });

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterFormValueChange({
          viewListId: `${ROUTES.TIP_CARDS.tabLabel}:default`,
          value: { status: TipStatus.Active, expiryDate: '05/24' }
        })
      );
    });

    it('should dispatch ViewListActions.FilterChange when sourceType is OWN_CARD', () => {
      (service as any).getNotificationRedirectionUrl({
        sourceId: '05/24',
        sourceType: NotificationSourceType.OWN_CARD
      });

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterChange({
          viewListId: `${ROUTES.TIP_CARDS.tabLabel}:default`,
          filter: { status: TipStatus.Active, expiryDate: '05/24' }
        })
      );
    });

    describe('getTicketingAuthorityUrl', () => {
      it('should return MY_AGENT url when userType is AGENT', () => {
        service['userType'] = UserType.AGENT;

        const res = (service as any).getTicketingAuthorityUrl({
          sourceId: '1234567890abcd'
        });

        expect(res).toEqual([ROUTES.MY_AGENT.url + '/ticketing-authority']);
      });

      it('should return MY_AIRLINE url when userType is AIRLINE', () => {
        service['userType'] = UserType.AIRLINE;

        const res = (service as any).getTicketingAuthorityUrl({
          sourceId: '1234567890abcd'
        });

        expect(res).toEqual([`${ROUTES.MY_AIRLINE.url}/1234567890/ticketing-authority`]);
      });

      it('should return MY_AGENT_GROUP url when userType is AGENT_GROUP', () => {
        service['userType'] = UserType.AGENT_GROUP;

        const res = (service as any).getTicketingAuthorityUrl({
          sourceId: '1234567890abcd'
        });

        expect(res).toEqual([`${ROUTES.MY_AGENT_GROUP.url}/ticketing-authority`]);
      });

      it('should return AIRLINE url when userType is GDS', () => {
        service['userType'] = UserType.GDS;

        const res = (service as any).getTicketingAuthorityUrl({
          sourceId: '1234567890abcd'
        });

        expect(res).toEqual([`${ROUTES.AIRLINE.url}/1234567890/ticketing-authority`]);
      });

      it('should return MY_AGENT_GROUP url when userType is IATA', () => {
        service['userType'] = UserType.IATA;

        const res = (service as any).getTicketingAuthorityUrl({
          sourceId: '1234567890abcd'
        });

        expect(res).toEqual([`${ROUTES.AIRLINE.url}/1234567890/ticketing-authority`]);
      });
    });
  });
});
