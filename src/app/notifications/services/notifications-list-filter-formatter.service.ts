import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { NotificationAction, NotificationsListFilter, NotificationType } from '../models/notifications-header.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { bspFilterTagMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class NotificationsListFilterFormatter implements FilterFormatter {
  public ignoredFilters: Array<keyof NotificationsListFilter> = [];

  constructor(private translation: L10nTranslationService) {}

  public format(filter: NotificationsListFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<NotificationsListFilter>> = {
      bspIsoc: bspValue => `${this.translate('bspisoc')} - ${bspFilterTagMapper(bspValue)}`,
      action: (actionList: NotificationAction[]) =>
        `${this.translate('action')} - ${actionList.map(action => this.translateAction(action)).join(', ')}`,
      type: (typeList: NotificationType[]) =>
        `${this.translate('type')} - ${typeList.map(type => this.translateType(type)).join(', ')}`,
      createdAt: periods => rangeDateFilterTagMapper(periods),
      sourceNumber: sourceNumber => `${this.translate('sourceSubject')} - ${sourceNumber}`,
      status: status => `${this.translate('status')} - ${status}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.notificationsList.filters.labels.' + key);
  }

  private translateAction(action: NotificationAction): string {
    return this.translation.translate(`LIST.notificationsList.filters.options.action.${action}`);
  }

  private translateType(type: NotificationType): string {
    return this.translation.translate(`notificationsMenu.notificationType.${type}`);
  }
}
