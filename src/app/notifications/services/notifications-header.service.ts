import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { interval, merge, Observable, of, Subject } from 'rxjs';
import { catchError, delay, first, map, mapTo, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { ViewListActions } from '~app/core/actions';
import {
  NotificationCount,
  NotificationHeader,
  NotificationSourceType,
  NotificationStatus
} from '~app/notifications/models/notifications-header.model';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { formatPeriod } from '~app/shared/helpers';
import { createTabViewListId } from '~app/shared/helpers/create-tab-view-list-id.helper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { UserType } from '~app/shared/models/user.model';
import { WatcherDocType, WatcherModel } from '~app/shared/models/watcher.model';
import { AppConfigurationService, TabService } from '~app/shared/services';
import { TipStatus } from '~app/tip/tip-cards/models/tip-card.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationsHeaderService {
  private baseEndpointUrl = `${this.appConfiguration.baseApiPath}/notification-management`;
  private baseNotificationsUrl = `${this.baseEndpointUrl}/notifications`;

  private updateBellCount$ = new Subject();
  private finishInfiniteCount$ = new Subject();
  private userType: UserType;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>,
    private tabService: TabService,
    private router: Router
  ) {
    this.store.pipe(select(getUser), first()).subscribe(user => {
      this.userType = user.userType;
    });
  }

  public find(query: DataQuery<{ [key: string]: any }>): Observable<PagedData<NotificationHeader>> {
    return this.requestData(`${this.baseNotificationsUrl}`, query);
  }

  private requestData<T>(url: string, dataQuery: Partial<DataQuery>): Observable<T> {
    const formattedQuery = this.formatQuery(dataQuery);

    return this.http.get<T>(url + formattedQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery>): RequestQuery {
    const { bspIsoc, action, type, status, sourceNumber } = query.filterBy;
    const createdAt = query.filterBy.createdAt || [];
    const [periodFrom, periodTo] = createdAt;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        bspIsoc: bspIsoc && (!Array.isArray(bspIsoc) ? [bspIsoc] : bspIsoc).map(({ isoCountryCode }) => isoCountryCode),
        action,
        type,
        status,
        sourceNumber,
        dateFrom: periodFrom && formatPeriod(periodFrom),
        dateTo: periodFrom && formatPeriod(periodTo || periodFrom)
      }
    });
  }

  public updateBellCount(count?: number): void {
    this.updateBellCount$.next(count ? this.transformCountIntoString(count) : null);
  }

  public getInfiniteBellCount(): Observable<string> {
    this.finishInfiniteCount$.next();

    return merge(this.updateBellCount$, interval(300000).pipe(startWith(0), mapTo(null))).pipe(
      switchMap(count => (count ? of(count) : this.getInstantBellCount())),
      takeUntil(this.finishInfiniteCount$)
    );
  }

  public getInstantBellCount(): Observable<string> {
    return this.http.get<NotificationCount>(`${this.baseNotificationsUrl}/count`).pipe(
      catchError(() => of({ count: 0 })),
      map(notificationCount => this.transformCountIntoString(notificationCount.count))
    );
  }

  private transformCountIntoString(count: number): string {
    return count > 99 ? '99+' : count.toString();
  }

  public getNotifications(): Observable<PagedData<NotificationHeader>> {
    const createdAt: keyof NotificationHeader = 'createdAt';

    return this.http
      .get<PagedData<NotificationHeader>>(`${this.baseNotificationsUrl}`, {
        params: { status: NotificationStatus.unread, sort: `${createdAt},${SortOrder.Desc}` }
      })
      .pipe(tap(notifications => this.updateBellCount(notifications.total)));
  }

  public markNotificationAsRead(notification: NotificationHeader): Observable<NotificationHeader> {
    return this.http
      .put<NotificationHeader>(`${this.baseNotificationsUrl}/${notification.id}`, {
        status: NotificationStatus.read
      })
      .pipe(tap(() => this.updateBellCount()));
  }

  public getNotificationRedirectionUrl(notification: NotificationHeader): string[] {
    let result: string[] = null;
    let viewListId: string;
    let filter: any;

    switch (notification.sourceType) {
      case NotificationSourceType.ACMA:
        result = [ROUTES.ACM_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ACMD:
        result = [ROUTES.ACMD_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ADMA:
        result = [ROUTES.ADM_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ADMD:
        result = [ROUTES.ADMD_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ACNT:
        result = [ROUTES.ACNT_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ADNT:
        result = [ROUTES.ADNT_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.SPCR:
        result = [ROUTES.SPCR_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.SPDR:
        result = [ROUTES.SPDR_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ACM_REQUEST:
        result = [ROUTES.ACM_REQUEST_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ADM_REQUEST:
        result = [ROUTES.ADM_REQUEST_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.ADM_POLICY:
        viewListId = createTabViewListId(ROUTES.ADM_POLICIES_QUERY.tabLabel);
        filter = { iataCode: [notification.sourceNumber] };

        this.store.dispatch(new ViewListActions.FilterChange({ viewListId, filter }));
        this.store.dispatch(new ViewListActions.FilterFormValueChange({ viewListId, value: filter }));
        result = [ROUTES.ADM_POLICIES_QUERY.url];
        break;
      case NotificationSourceType.PBD:
        result = [ROUTES.POST_BILLING_DISPUTE_LIST.url, notification.sourceId];
        break;
      case NotificationSourceType.RFND_RA:
        result = [ROUTES.REFUNDS_APP_VIEW.url, notification.sourceId];
        break;
      case NotificationSourceType.TA:
        result = this.getTicketingAuthorityUrl(notification);
        break;
      case NotificationSourceType.FILE:
        viewListId = createTabViewListId(ROUTES.REPORTS.tabLabel, 'inbox');
        filter = { name: notification.sourceNumber };

        this.store.dispatch(new ViewListActions.FilterChange({ viewListId, filter }));
        this.store.dispatch(new ViewListActions.FilterFormValueChange({ viewListId, value: filter }));
        result = [ROUTES.REPORTS.url];
        break;
      case NotificationSourceType.OWN_CARD:
        viewListId = createTabViewListId(ROUTES.TIP_CARDS.tabLabel);
        filter = { status: TipStatus.Active, expiryDate: notification.sourceId };

        this.store.dispatch(new ViewListActions.FilterChange({ viewListId, filter }));
        this.store.dispatch(new ViewListActions.FilterFormValueChange({ viewListId, value: filter }));
        result = [ROUTES.TIP_CARDS.url];
        break;
    }

    return result;
  }

  public navigateToNotificationLink(notification: NotificationHeader): void {
    const url = this.getNotificationRedirectionUrl(notification);

    if (url) {
      if (url.length === 1) {
        // Refresh current tab if we no need of redirection so new filters can be applied
        this.tabService
          .isCurrentTabOnUrl(url[0])
          .subscribe(isOnUrl => (isOnUrl ? this.tabService.refreshCurrentTab() : this.router.navigate(url)));
      } else {
        this.router.navigate(url);
      }
    }
  }

  public updateWatcher(payload: WatcherModel, bspId: number): Observable<WatcherModel> {
    // Delay fixes problem in BE https://nfe-bsplink.atlassian.net/browse/FCA-15374
    return this.http.put<WatcherModel>(`${this.baseEndpointUrl}/bsps/${bspId}/watches`, payload).pipe(delay(1000));
  }

  public getWatcher(bspId: number, documentId: number, documentType: WatcherDocType): Observable<WatcherModel> {
    return this.http.get<WatcherModel>(`${this.baseEndpointUrl}/bsps/${bspId}/watches`, {
      params: { documentType, documentId: documentId.toString() }
    });
  }

  private getTicketingAuthorityUrl(notification: NotificationHeader): string[] {
    let url = [''];
    let airlineId = '';

    if (this.userType === UserType.AIRLINE || this.userType === UserType.IATA || this.userType === UserType.GDS) {
      airlineId = notification.sourceId.substring(0, 10);
    }

    switch (this.userType) {
      case UserType.AGENT:
        url = [ROUTES.MY_AGENT.url + '/ticketing-authority'];
        break;
      case UserType.AIRLINE:
        url = [ROUTES.MY_AIRLINE.url + `/${airlineId}/ticketing-authority`];
        break;
      case UserType.AGENT_GROUP:
        url = [ROUTES.MY_AGENT_GROUP.url + '/ticketing-authority'];
        break;
      case UserType.GDS:
      case UserType.IATA:
        url = [ROUTES.AIRLINE.url + `/${airlineId}/ticketing-authority`];
        break;
      default:
        break;
    }

    return url;
  }
}
