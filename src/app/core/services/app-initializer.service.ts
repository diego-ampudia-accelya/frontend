import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { iif, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { GlobalCacheConfig } from 'ts-cacheable';

import { AuthActions, Login, LoginFailed, RedirectToTrainingLogin } from '~app/auth/actions/auth.actions';
import { AuthService } from '~app/auth/services/auth.service';
import { TokenService } from '~app/auth/services/token.service';
import { AppState } from '~app/reducers';
import { CacheConfiguration } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { AutoLoginActions } from '~app/training/actions';
import { AutoLoginService } from '~app/training/services/auto-login.service';

@Injectable({
  providedIn: 'root'
})
export class AppInitializerService {
  constructor(
    private authService: AuthService,
    private store: Store<AppState>,
    private tokenService: TokenService,
    private appConfiguration: AppConfigurationService,
    public autoLoginService: AutoLoginService
  ) {}

  public init(): Promise<any> {
    return this.appConfiguration
      .load()
      .pipe(
        tap(() => this.initCacheConfig(this.appConfiguration.cache)),
        switchMap(() => iif(() => this.appConfiguration.isTrainingSite, of(null), this.initTokenCookies())),
        switchMap(authAction => this.processNextAction(authAction)),
        tap(action => this.store.dispatch(action))
      )
      .toPromise();
  }

  public initTokenCookies(): Observable<AuthActions> {
    return this.tokenService.initTokenCookies();
  }

  public processNextAction(authAction: AuthActions): Observable<Action> {
    let actionToDispatch: Observable<Action>;

    if (authAction) {
      actionToDispatch = of(authAction);
    } else if (this.appConfiguration.isTrainingSite) {
      const data = this.autoLoginService.getData();

      actionToDispatch = data
        ? of(AutoLoginActions.autoLoginAddUserLoginData(data))
        : of<AuthActions>(new RedirectToTrainingLogin());
    } else {
      actionToDispatch = this.authService
        .loadUserProfile()
        .pipe(map(user => (user ? new Login({ user }) : new LoginFailed())));
    }

    return actionToDispatch;
  }

  private initCacheConfig(config: CacheConfiguration): void {
    GlobalCacheConfig.globalCacheKey = 'BSP_LINK';
    GlobalCacheConfig.maxAge = config.maxAge;
    GlobalCacheConfig.maxCacheCount = config.maxCacheCount;
  }
}

export function initApp(appInitializer: AppInitializerService) {
  return () => appInitializer.init();
}
