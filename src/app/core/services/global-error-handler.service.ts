import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';

import { ApplicationError, ServerError } from '~app/shared/errors';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';
import { NotificationService } from '~app/shared/services/notification.service';

@Injectable()
export class GlobalErrorHandlerService extends ErrorHandler {
  constructor(private injector: Injector) {
    super();
  }

  public handleError(error: Error & { rejection?: Error }): void {
    // The `ErrorHandler` is initialized early in the app lifecycle.
    // Dependencies cannot be resolved at construction time.
    // We need to resolve them manually using the `Injector` service,
    const notificationService = this.injector.get(NotificationService);
    const messageExtractor = this.injector.get(MessageExtractorService);
    const zone = this.injector.get(NgZone);

    // If the error originates from an unhandled promise rejection we get the rejection itself.
    error = error.rejection || error;

    // handleError is not executed inside NgZone.
    // We need to use NgZone.run explicitly.
    if (error instanceof ServerError) {
      const serverError = error;
      zone.run(() => {
        const message = messageExtractor.extract(serverError);
        const data = this.getMessageData(serverError);

        notificationService.showSystemError(message, data);
      });
    } else if (error instanceof ApplicationError) {
      zone.run(() => {
        notificationService.showError(error.message);
      });
    }

    // Forward the error to the base implementation to get a better stack trace.
    super.handleError(error);
  }

  private getMessageData({ error }) {
    const { errorMajorCode } = error;
    const errorCode = (error.errorCode === 500 && errorMajorCode) || '';
    const errorId = error.errorCode >= 500 ? error.errorId : '';

    return { errorCode, errorId };
  }
}
