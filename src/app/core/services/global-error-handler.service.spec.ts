import { ErrorHandler, Injector } from '@angular/core';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';

import { GlobalErrorHandlerService } from './global-error-handler.service';
import { ApplicationError, ServerError } from '~app/shared/errors';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';
import { ResponseErrorBE } from '~app/shared/models';
import { NotificationService } from '~app/shared/services';

describe('GlobalErrorHandlerService', () => {
  let service: GlobalErrorHandlerService;
  let notificationServiceSpy: SpyObject<NotificationService>;
  let messageExtractorServiceSpy: SpyObject<MessageExtractorService>;

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        GlobalErrorHandlerService,
        Injector,
        mockProvider(NotificationService),
        mockProvider(MessageExtractorService)
      ]
    })
  );

  beforeEach(() => {
    service = TestBed.inject(GlobalErrorHandlerService);
    notificationServiceSpy = TestBed.inject<any>(NotificationService);
    messageExtractorServiceSpy = TestBed.inject<any>(MessageExtractorService);

    spyOn(ErrorHandler.prototype, 'handleError');
    messageExtractorServiceSpy.extract.and.returnValue('msg');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should show an error with proper parameters in case of ApplicationError', fakeAsync(() => {
    const message = 'some error';

    service.handleError(new ApplicationError(message));

    expect(notificationServiceSpy.showError).toHaveBeenCalledWith(message);
  }));

  it('should omit the errorId when handling HTTP 4XX errors', fakeAsync(() => {
    const beError = {
      errorId: '123',
      errorCode: 456
    } as ResponseErrorBE;
    const error = new ServerError(beError);
    const expected = {
      errorCode: '',
      errorId: ''
    };

    service.handleError(error);

    expect(notificationServiceSpy.showSystemError).toHaveBeenCalledWith(jasmine.anything(), expected);
  }));

  it('should show an error with proper data in case of HTTP error 500', fakeAsync(() => {
    const beError = {
      errorMajorCode: '123',
      errorId: '456',
      errorCode: 500
    } as ResponseErrorBE;
    const error = new ServerError(beError);
    const expected = {
      errorCode: '123',
      errorId: '456'
    };

    service.handleError(error);

    expect(notificationServiceSpy.showSystemError).toHaveBeenCalledWith(jasmine.anything(), expected);
  }));
});
