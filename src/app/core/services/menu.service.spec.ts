import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { MenuService } from './menu.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Permissions } from '~app/shared/constants/permissions';
import { MenuStatus } from '~app/shared/enums';
import { MenuItem } from '~app/shared/models/menu-item.model';
import { UserType } from '~app/shared/models/user.model';

describe('MenuService', () => {
  let service: MenuService;
  let permissionsStub: SpyObject<PermissionsService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MenuService, mockProvider(PermissionsService, { permissions$: of([]) })]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MenuService);

    permissionsStub = TestBed.inject<any>(PermissionsService);
    permissionsStub.hasPermission.and.returnValue(true);
    permissionsStub.hasUserType.and.returnValue(true);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('getMenuItems', () => {
    it('should create proper menu items based on configuration', fakeAsync(() => {
      const menuConfig = {
        ITEM: {
          label: 'Item 1',
          tabLabel: 'Item 1 Tab',
          url: 'item-1',
          showMenu: true,
          permissionId: Permissions.issueActionAcm,
          allowedUserTypes: UserType.IATA
        }
      };

      const expectedItem: MenuItem = {
        key: 'ITEM',
        label: 'Item 1',
        tabID: 'Item 1 Tab',
        route: 'item-1',
        showMenu: true,
        permissionID: Permissions.issueActionAcm,
        allowedUserTypes: UserType.IATA,
        enabled: true,
        isClassicMenuItem: false,
        parent: null,
        children: [],
        check: MenuStatus.EMPTY,
        openOnClick: false
      };

      let actual: MenuItem[];
      service.getMenuItems(menuConfig).subscribe(items => (actual = items));

      const [actualItem] = actual;

      expect(actualItem).toEqual(expectedItem);
    }));

    it('should create a hierarchical structure based on menu configuration', fakeAsync(() => {
      const menuConfig = {
        PARENT: {
          label: 'Parent',
          showMenu: true
        },
        CHILD: {
          label: 'Child',
          showMenu: true,
          parent: 'PARENT'
        },
        SUBCHILD: {
          label: 'Subchild',
          showMenu: true,
          parent: 'CHILD'
        }
      };

      const expectedParent: any = jasmine.objectContaining({
        key: 'PARENT',
        label: 'Parent',
        enabled: true
      });
      const expectedChild: any = jasmine.objectContaining({
        key: 'CHILD',
        label: 'Child',
        enabled: true
      });
      const expectedSubchild: any = jasmine.objectContaining({
        key: 'SUBCHILD',
        label: 'Subchild',
        enabled: true
      });

      let actual: MenuItem[];
      service.getMenuItems(menuConfig).subscribe(items => (actual = items));

      const [actualParent] = actual;
      const [actualChild] = actualParent.children;
      const [actualSubchild] = actualChild.children;

      expect(actualParent).toEqual(expectedParent);
      expect(actualChild).toEqual(expectedChild);
      expect(actualChild.parent).toBe(actualParent);
      expect(actualSubchild).toEqual(expectedSubchild);
      expect(actualSubchild.parent).toBe(actualChild);
    }));

    it('should ignore items when their parent does not exist', fakeAsync(() => {
      const menuConfig = {
        PARENT: {
          label: 'Parent',
          showMenu: true
        },
        ORPHAN: {
          label: 'Orphan',
          showMenu: true,
          parent: 'NOT_FOUND'
        }
      };

      let actual: MenuItem[];
      service.getMenuItems(menuConfig).subscribe(items => (actual = items));

      const expected: any[] = [
        jasmine.objectContaining({
          key: 'PARENT'
        })
      ];

      expect(actual).toEqual(expected);
    }));

    it('should ignore items when `showMenu` is `false`', fakeAsync(() => {
      const menuConfig = {
        HIDDEN: {
          label: 'Hidden',
          showMenu: false
        }
      };

      let actual: MenuItem[];
      service.getMenuItems(menuConfig).subscribe(items => (actual = items));

      expect(actual).toEqual([]);
    }));

    it('should enable parent items when at least one child item is allowed', fakeAsync(() => {
      const menuConfig = {
        PARENT: {
          label: 'Parent',
          showMenu: true,
          permissionId: Permissions.protectedByChildren
        },
        CHILD: {
          label: 'Child',
          showMenu: true,
          parent: 'PARENT',
          permissionId: Permissions.protectedByChildren
        },
        FORBIDDEN_CHILD: {
          label: 'Forbidden Child',
          showMenu: true,
          parent: 'PARENT',
          permissionId: Permissions.forbidden
        },
        SUBCHILD: {
          label: 'Subchild',
          showMenu: true,
          parent: 'CHILD',
          permissionId: Permissions.readAgent
        }
      };

      const expectedParent: any = jasmine.objectContaining({
        key: 'PARENT',
        label: 'Parent',
        enabled: true
      });
      const expectedChild: any = jasmine.objectContaining({
        key: 'CHILD',
        label: 'Child',
        enabled: true
      });
      const expectedSubchild: any = jasmine.objectContaining({
        key: 'SUBCHILD',
        label: 'Subchild',
        enabled: true
      });
      const expectedForbiddenChild: any = jasmine.objectContaining({
        key: 'FORBIDDEN_CHILD',
        label: 'Forbidden Child',
        enabled: false
      });

      permissionsStub.hasPermission.and.callFake((permission: string) => permission === Permissions.readAgent);

      let actual: MenuItem[];
      service.getMenuItems(menuConfig).subscribe(items => (actual = items));

      const [actualParent] = actual;
      const [actualChild, forbiddenChild] = actualParent.children;
      const [actualSubchild] = actualChild.children;

      expect(actualParent).toEqual(expectedParent);
      expect(actualChild).toEqual(expectedChild);
      expect(forbiddenChild).toEqual(expectedForbiddenChild);
      expect(actualSubchild).toEqual(expectedSubchild);
    }));

    it('should disable parent items when no child item is allowed', fakeAsync(() => {
      const menuConfig = {
        PARENT: {
          label: 'Parent',
          showMenu: true,
          permissionId: Permissions.protectedByChildren
        },
        FORBIDDEN_CHILD: {
          label: 'Forbidden Child',
          showMenu: true,
          parent: 'PARENT',
          permissionId: Permissions.forbidden
        }
      };

      permissionsStub.hasPermission.and.returnValue(false);

      let actual: MenuItem[];
      service.getMenuItems(menuConfig).subscribe(items => (actual = items));

      expect(actual).toEqual([]);
    }));

    it('should omit top level items when they are not allowed', fakeAsync(() => {
      const menuConfig = {
        ITEM1: {
          label: 'Item 1',
          showMenu: true,
          permissionId: Permissions.forbidden
        },
        ITEM2: {
          label: 'Item 2',
          showMenu: true,
          permissionId: Permissions.forbidden
        },
        ENABLED: {
          label: 'Enabled',
          showMenu: true,
          permissionId: Permissions.readAgent
        }
      };

      permissionsStub.hasPermission.and.callFake((permission: string) => permission === Permissions.readAgent);

      let actual: MenuItem[];
      service.getMenuItems(menuConfig).subscribe(items => (actual = items));

      const expectedEnabledItem: any = jasmine.objectContaining({ key: 'ENABLED', enabled: true });
      expect(actual).toEqual([expectedEnabledItem]);
    }));
  });

  describe('findActiveTabConfig', () => {
    it('should find active tab', () => {
      const routerState: any = {
        url: 'test',
        root: {
          data: {},
          queryParams: null,
          firstChild: {
            data: {
              tab: { tabLabel: 'Test' }
            }
          }
        }
      };

      const expected: any = {
        url: 'test',
        tabLabel: 'Test',
        queryParams: null
      };

      expect(service.findActiveTabConfig(routerState)).toEqual(expected);
    });
  });
});
