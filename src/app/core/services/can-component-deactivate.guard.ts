import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { from, Observable, of } from 'rxjs';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({ providedIn: 'root' })
export class CanComponentDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  canDeactivate(component: CanComponentDeactivate): Observable<boolean> {
    let canDeactivate$ = of(true);
    if (component.canDeactivate) {
      const result = component.canDeactivate();
      canDeactivate$ = typeof result === 'boolean' ? of(result) : from(result);
    }

    return canDeactivate$;
  }
}
