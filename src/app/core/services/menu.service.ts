import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { SerializedRouterStateSnapshot } from '@ngrx/router-store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { ROUTES } from '~app/shared/constants/routes';
import { MenuStatus } from '~app/shared/enums/menu-status.enum';
import { MenuItem } from '~app/shared/models/menu-item.model';
import { MenuTab } from '~app/shared/models/menu-tab.model';

@Injectable()
export class MenuService {
  constructor(private permissionsService: PermissionsService) {}

  public getMenuItems(menuConfig: { [key: string]: any }): Observable<MenuItem[]> {
    return this.permissionsService.permissions$.pipe(
      map(() => this.buildMenu(menuConfig).filter(menuItem => menuItem.label && menuItem.enabled))
    );
  }

  private buildMenu(menuConfig: { [key: string]: any }): MenuItem[] {
    const myMenu: MenuItem[] = [];
    for (const [key, route] of Object.entries(menuConfig)) {
      const routeItem = route;
      const parentRoute = routeItem.parent;

      if (!routeItem.showMenu) {
        continue;
      }

      const menuItem: MenuItem = {
        key,
        label: routeItem.label,
        children: [],
        check: MenuStatus.EMPTY,
        tabID: routeItem.tabLabel,
        permissionID: routeItem.permissionId,
        allowedUserTypes: routeItem.allowedUserTypes,
        showMenu: routeItem.showMenu,
        route: routeItem.url,
        parent: null,
        enabled: false,
        isClassicMenuItem: Boolean(routeItem.isClassicMenuItem),
        openOnClick: Boolean(routeItem.openOnClick)
      };

      if (!parentRoute) {
        myMenu.push(menuItem);
      } else {
        const menuItemParent = this.searchMenuItem(parentRoute, 'key', myMenu);
        menuItem.parent = menuItemParent || null;

        if (menuItemParent) {
          menuItemParent.children.push(menuItem);
        }
      }

      if (this.hasPermission(menuItem)) {
        menuItem.enabled = true;
        this.enableParents(menuItem);
      }
    }

    return myMenu;
  }

  private searchMenuItem(value: string, prop: string, menuItems: MenuItem[] = []) {
    if (menuItems) {
      for (const item of menuItems) {
        if (item[prop] === value) {
          return item;
        }
        const menuItem = this.searchMenuItem(value, prop, item.children);

        if (menuItem) {
          return menuItem;
        }
      }
    }

    return null;
  }

  private hasPermission(menuItem: MenuItem): boolean {
    const isDashboardRoute = menuItem.route === ROUTES.DASHBOARD.url;
    const hasPermission = this.permissionsService.hasPermission(menuItem.permissionID);
    const hasUserType = this.permissionsService.hasUserType(menuItem.allowedUserTypes);

    return isDashboardRoute || (hasPermission && hasUserType);
  }

  private enableParents(item: MenuItem) {
    if (item.parent) {
      item.parent.enabled = true;
      this.enableParents(item.parent);
    }
  }

  /**
   * Find the proper tab configuration for the active route.
   *
   * @param routerState Current state of the router.
   * @returns Menu tab configuration.
   */
  public findActiveTabConfig(routerState: SerializedRouterStateSnapshot): MenuTab {
    const routesIterator = this.iterateActiveRoutes(routerState.root);

    return Array.from(routesIterator)
      .map(route => route.data)
      .filter(routeConfig => !!routeConfig.tab)
      .map(routeConfig => {
        const tabConfig: MenuTab = routeConfig.tab;

        return {
          ...tabConfig,
          tabLabel: tabConfig.getTabLabel ? tabConfig.getTabLabel(routeConfig) : tabConfig.tabLabel,
          queryParams: routerState.root.queryParams,
          url: routerState.url
        };
      })[0];
  }

  private *iterateActiveRoutes(root: ActivatedRouteSnapshot): IterableIterator<ActivatedRouteSnapshot> {
    for (let route = root; route != null; route = route.firstChild) {
      yield route;
    }
  }
}
