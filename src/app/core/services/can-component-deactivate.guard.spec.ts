import { fakeAsync, tick } from '@angular/core/testing';
import { SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { CanComponentDeactivate, CanComponentDeactivateGuard } from './can-component-deactivate.guard';

describe('CanComponentDeactivateGuard', () => {
  let guard: CanComponentDeactivateGuard;
  let componentStub: SpyObject<CanComponentDeactivate>;

  beforeEach(() => {
    componentStub = jasmine.createSpyObj('Component', ['canDeactivate']);
    componentStub.canDeactivate.and.returnValue(true);
    guard = new CanComponentDeactivateGuard();
  });

  it('should create', () => {
    expect(guard).toBeDefined();
  });

  it('should return `true` when component does not have a `canDeactivate` method', () => {
    const componentWithoutDeactivate: CanComponentDeactivate = { canDeactivate: null };

    let canDeactivate: boolean;
    guard.canDeactivate(componentWithoutDeactivate).subscribe(result => (canDeactivate = result));

    expect(canDeactivate).toEqual(true);
  });

  it('should transform boolean response to observable', () => {
    componentStub.canDeactivate.and.returnValue(false);

    let canDeactivate: boolean;
    guard.canDeactivate(componentStub).subscribe(result => (canDeactivate = result));

    expect(canDeactivate).toEqual(false);
  });

  it('should transform observable response to observable', () => {
    componentStub.canDeactivate.and.returnValue(of(false));

    let canDeactivate: boolean;
    guard.canDeactivate(componentStub).subscribe(result => (canDeactivate = result));

    expect(canDeactivate).toEqual(false);
  });

  it('should transform promise response to observable', fakeAsync(() => {
    componentStub.canDeactivate.and.returnValue(Promise.resolve(false));

    let canDeactivate: boolean;
    guard.canDeactivate(componentStub).subscribe(result => (canDeactivate = result));
    tick();

    expect(canDeactivate).toEqual(false);
  }));
});
