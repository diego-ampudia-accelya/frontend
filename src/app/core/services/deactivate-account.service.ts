import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { MasterDataAgentGroup } from '~app/master-data/models/agent-group.model';
import { appConfiguration } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class DeactivateAccountService {
  private baseUrl = `${appConfiguration.baseApiPath}/group-management/agent-groups`;
  private statusDeactivated = 'Deactivated';

  constructor(private http: HttpClient) {}

  public deactivateAgentGroupAccount(agentGroupId: number): Observable<MasterDataAgentGroup> {
    const requestUrl = `${this.baseUrl}/${agentGroupId}`;
    const requestBody = {
      status: this.statusDeactivated
    };

    return this.http.put<MasterDataAgentGroup>(requestUrl, requestBody);
  }
}
