import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { Token } from '~app/shared/models/token.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class ClassicBsplinkService {
  private readonly nonExistingBspLinkPage = 6999;
  private readonly switchAccountBspLinkPage = 6966;
  private readonly iataImpersonationtBspLinkPage = 6987;
  private readonly AgentGroupImpersonationBspLinkPage = 6988;
  private readonly AgentGroupImpersonationReturntBspLinkPage = 6989;
  private renderer: Renderer2;

  constructor(
    private appConfiguration: AppConfigurationService,
    private l10n: L10nTranslationService,
    private rendererFactory: RendererFactory2,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.renderer = this.rendererFactory.createRenderer(document, null);
  }

  public goToClassicBspLink(token?: Token, pageId = this.nonExistingBspLinkPage): void {
    if (token) {
      this.goToClassicBspLinkPage(token, pageId);
    } else {
      this.goToClassicBspLinkLoginPage();
    }
  }

  public goToClassicBspLinkPage(token: Token, pageId = this.nonExistingBspLinkPage): void {
    const { access_token, id_token, refresh_token } = token || {};
    const form = this.createForm(
      [
        this.createInput('id', pageId.toString()),
        this.createInput('accepted_language', this.l10n.getLocale()?.language),
        this.createInput('token', access_token),
        this.createInput('id_token', id_token),
        this.createInput('refresh_token', refresh_token)
      ],
      this.appConfiguration.classicBspLinkUrl
    );

    this.renderer.appendChild(this.document.body, form);
    form.submit();
    form.remove();
  }

  public goToClassicBspLinkLoginPage(): void {
    this.document.location.href = `${this.appConfiguration.classicBspLinkUrl}?goToBSPLink=true`;
  }

  public goToClassicBspLinkSwitchAccountPage(accountId: string, token: Token): void {
    const { access_token, id_token, refresh_token } = token || {};
    const form = this.createForm(
      [
        this.createInput('id', this.switchAccountBspLinkPage.toString()),
        this.createInput('account', '' + accountId),
        this.createInput('token', access_token),
        this.createInput('id_token', id_token),
        this.createInput('refresh_token', refresh_token)
      ],
      this.appConfiguration.classicBspLinkUrl
    );

    this.renderer.appendChild(this.document.body, form);
    form.submit();
    form.remove();
  }

  public goToClassicBspLinkIataImpersonation(userType: number, userCode: string, token: Token): void {
    const { access_token, id_token, refresh_token } = token || {};
    const form = this.createForm(
      [
        this.createInput('id', this.iataImpersonationtBspLinkPage.toString()),
        this.createInput('token', access_token),
        this.createInput('id_token', id_token),
        this.createInput('refresh_token', refresh_token),
        this.createInput('user_type', userType.toString()),
        this.createInput('user_code', userCode)
      ],
      this.appConfiguration.classicBspLinkUrl
    );

    this.renderer.appendChild(this.document.body, form);
    form.submit();
    form.remove();
  }

  public goToClassicBspLinkAgentGroupImpersonation(
    agentGroupMemberCode: string,
    returnToAgentGroupProfile: boolean,
    token: Token
  ): void {
    const { access_token, id_token, refresh_token } = token || {};
    let form: HTMLFormElement;

    if (returnToAgentGroupProfile) {
      form = this.createForm(
        [
          this.createInput('id', this.AgentGroupImpersonationReturntBspLinkPage.toString()),
          this.createInput('token', access_token),
          this.createInput('id_token', id_token),
          this.createInput('refresh_token', refresh_token)
        ],
        this.appConfiguration.classicBspLinkUrl
      );
    } else {
      form = this.createForm(
        [
          this.createInput('id', this.AgentGroupImpersonationBspLinkPage.toString()),
          this.createInput('token', access_token),
          this.createInput('id_token', id_token),
          this.createInput('refresh_token', refresh_token),
          this.createInput('user_code', agentGroupMemberCode)
        ],
        this.appConfiguration.classicBspLinkUrl
      );
    }

    this.renderer.appendChild(this.document.body, form);
    form.submit();
    form.remove();
  }

  private createForm(inputs: HTMLInputElement[], action: string): HTMLFormElement {
    const form: HTMLFormElement = this.renderer.createElement('form');
    form.method = 'POST';
    form.action = action;
    inputs.forEach(input => form.appendChild(input));

    return form;
  }

  private createInput(name: string, value: string): HTMLInputElement {
    const input: HTMLInputElement = this.renderer.createElement('input');
    input.type = 'hidden';
    input.value = value;
    input.name = name;

    return input;
  }
}
