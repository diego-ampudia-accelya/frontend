import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { defer, Observable } from 'rxjs';
import { finalize, first, map } from 'rxjs/operators';

import {
  ButtonDesign,
  ChangesDialogComponent,
  DialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import { MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';

@Injectable()
export class ChangesDialogService {
  constructor(private dialogService: DialogService, private translationService: L10nTranslationService) {}

  public confirmUnsavedChanges(tabLabel: string | MenuTabLabelParameterized): Observable<FooterButton> {
    return defer(() =>
      this.open({ title: 'documentUnsaved.dialog.title', details: 'documentUnsaved.dialog.details' }, tabLabel)
    );
  }

  private open(
    question: { title: string; details: string },
    tabLabel: string | MenuTabLabelParameterized
  ): Observable<FooterButton> {
    const label = (tabLabel as MenuTabLabelParameterized).label || (tabLabel as string);
    const documentType = this.translationService.translate(label, (tabLabel as MenuTabLabelParameterized).params);

    const primaryButtonTitle = this.translationService.translate('documentUnsaved.dialog.closeTab');

    const dialogConfig: DialogConfig = {
      data: {
        title: question.title,
        hasCancelButton: false,
        footerButtonsType: [
          { type: FooterButton.Cancel, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Continue, title: primaryButtonTitle, buttonDesign: ButtonDesign.Primary }
        ]
      },
      message: question.details,
      translationParams: {
        documentType
      }
    };

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      finalize(() => this.dialogService.close()),
      first()
    );
  }
}
