import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { AppInitializerService } from './app-initializer.service';
import { Login, LoginFailed, RedirectToTrainingLogin } from '~app/auth/actions/auth.actions';
import { AuthService } from '~app/auth/services/auth.service';
import { TokenService } from '~app/auth/services/token.service';
import { AppState } from '~app/reducers';
import { User } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';
import { NotificationService } from '~app/shared/services/notification.service';
import { AutoLoginActions } from '~app/training/actions';

describe('AppInitializerService', () => {
  let service: AppInitializerService;
  let mockStore: MockStore<AppState>;
  let authServiceSpy: SpyObject<AuthService>;
  let tokenServiceSpy: SpyObject<TokenService>;
  const notificationServiceSpy = createSpyObject(NotificationService);
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(waitForAsync(() => {
    authServiceSpy = createSpyObject(AuthService);
    authServiceSpy.loadUserProfile.and.returnValue(of(null));
    tokenServiceSpy = createSpyObject(TokenService);
    tokenServiceSpy.initTokenCookies.and.returnValue(of(null));

    TestBed.configureTestingModule({
      providers: [
        AppInitializerService,
        provideMockStore(),
        { provide: AuthService, useValue: authServiceSpy },
        { provide: TokenService, useValue: tokenServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        mockProvider(AppConfigurationService, {
          cache: {}
        })
      ]
    });
  }));

  beforeEach(() => {
    mockStore = TestBed.inject<any>(Store);
    service = TestBed.inject(AppInitializerService);
    appConfigurationSpy = TestBed.inject<any>(AppConfigurationService);
    appConfigurationSpy.load.and.returnValue(of({}));
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should dispatch LoginFailed action when no action or user is returned/loaded', fakeAsync(() => {
    authServiceSpy.loadUserProfile.and.returnValue(of(null));
    tokenServiceSpy.initTokenCookies.and.returnValue(of(null));

    spyOn(mockStore, 'dispatch');
    service.init();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(new LoginFailed());
  }));

  it('should dispatch RedirectToTrainingSiteLogin action when isTrainingSite is true', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');
    spyOnProperty(appConfigurationSpy, 'isTrainingSite', 'get').and.returnValue(true);

    service.init();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(new RedirectToTrainingLogin());
  }));

  it('should dispatch AutoLoginActions.autoLoginAddUserLoginData action when autoLoginService returns value', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');
    spyOnProperty(appConfigurationSpy, 'isTrainingSite', 'get').and.returnValue(true);

    spyOn(service.autoLoginService, 'getData').and.returnValue({
      userName: 'test',
      authorizationValue: 'test'
    });

    service.init();

    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      AutoLoginActions.autoLoginAddUserLoginData({
        userName: 'test',
        authorizationValue: 'test'
      })
    );
  }));

  it('should dispatch Login action when cookies request succeed (null value) and load user returns value', fakeAsync(() => {
    tokenServiceSpy.initTokenCookies.and.returnValue(of(null));

    const user: User = { active: true, firstName: 'John' } as User;
    authServiceSpy.loadUserProfile.and.returnValue(of(user));

    spyOn(mockStore, 'dispatch');

    service.init();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(new Login({ user }));
  }));
});
