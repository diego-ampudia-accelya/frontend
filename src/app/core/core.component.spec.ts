import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockComponent } from 'ng-mocks';
import { of } from 'rxjs';

import { MenuComponent } from './components/menu/menu.component';
import { NavigateToClassicBspLink } from '~app/auth/actions/auth.actions';
import { CoreComponent } from '~app/core/core.component';
import { MenuService } from '~app/core/services/menu.service';
import { DialogService, FooterButton } from '~app/shared/components';
import { MenuItem } from '~app/shared/models/menu-item.model';

describe('CoreComponent', () => {
  let menuServiceStub: SpyObject<MenuService>;
  let dialogServiceStub: SpyObject<DialogService>;
  let mockStore: MockStore;

  let component: CoreComponent;
  let fixture: ComponentFixture<CoreComponent>;

  beforeEach(waitForAsync(() => {
    dialogServiceStub = createSpyObject(DialogService);
    menuServiceStub = createSpyObject(MenuService);

    const initialState = {
      auth: {
        user: null
      },
      router: null,
      core: {
        menu: {
          tabs: {}
        }
      }
    };

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CoreComponent, MockComponent(MenuComponent)],
      providers: [
        MenuService,
        provideMockStore({ initialState }),
        { provide: MenuService, useValue: menuServiceStub },
        { provide: DialogService, useValue: dialogServiceStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');

    fixture = TestBed.createComponent(CoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('openRedirectToClassicBspLinkDialog', () => {
    beforeEach(() => {
      dialogServiceStub.open.and.returnValue(
        of({
          clickedBtn: FooterButton.Proceed,
          contentComponentRef: { config: { data: { buttons: [{ type: FooterButton.Proceed }] } } }
        })
      );
    });

    it('should redirect to Classic BSPLink when the Proceed option is selected', fakeAsync(() => {
      component.openRedirectToClassicBspLinkDialog();

      expect(mockStore.dispatch).toHaveBeenCalledWith(new NavigateToClassicBspLink());
    }));
  });

  describe('onMenuItemClick', () => {
    let classicBspLinkItem: MenuItem;

    beforeEach(() => {
      classicBspLinkItem = {
        key: 'CLASSIC_BSP_LINK',
        isClassicMenuItem: true
      } as MenuItem;
    });

    it('should open `Redirect to Classing BSPlink dialog` when the CLASSIC_BSP_LINK item is clicked', () => {
      spyOn(component, 'openRedirectToClassicBspLinkDialog');
      component.onMenuItemClick(classicBspLinkItem);

      expect(component.openRedirectToClassicBspLinkDialog).toHaveBeenCalled();
    });

    it('should NOT show loading indication when Classic BSPLink menu is clicked', fakeAsync(() => {
      dialogServiceStub.open.and.returnValue(of({}));
      component.onMenuItemClick(classicBspLinkItem);

      expect(classicBspLinkItem.loading).toBeFalsy();
      tick(1);
      expect(classicBspLinkItem.loading).toBeFalsy();
    }));
  });
});
