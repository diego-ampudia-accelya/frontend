/* eslint-disable @typescript-eslint/no-shadow */
import { Action } from '@ngrx/store';

import { MenuTab } from '~app/shared/models/menu-tab.model';

export enum Types {
  Open = '[Navigation Tabs] Open',
  OpenSuccess = '[Navigation Tabs] Open Success',
  Close = '[Navigation Tabs] Close',
  CloseConfirmed = '[Navigation Tabs] Close Confirmed',
  CloseSuccess = '[Navigation Tabs] Close Success',
  Refresh = '[Navigation Tabs] Refresh',
  RefreshSuccess = '[Navigation Tabs] Refresh Success',
  EnableEditMode = '[Navigation Tabs] Edit mode enabled',
  DisableEditMode = '[Navigation Tabs] Edit mode disabled'
}

export class Open implements Action {
  type = Types.Open;
  constructor(public payload: MenuTab) {}
}

export class OpenSuccess implements Action {
  type = Types.OpenSuccess;
  constructor(public payload: MenuTab) {}
}

export class Close implements Action {
  type = Types.Close;

  /**
   * Create Close action
   *
   * @param payload - The tab to be removed. If it doesn't exist, active tab is removed
   * @param click - Determines whether the action has been triggered by an user click or by the code itself
   */
  constructor(public payload?: MenuTab, public click?: boolean) {}
}

export class CloseConfirmed implements Action {
  type = Types.CloseConfirmed;

  /**
   * Create Close Confirmed action
   *
   * @param payload - The tab to be removed
   * @param click - Determines whether the action has been triggered by an user click or by the code itself
   */
  constructor(public payload: MenuTab, public click?: boolean) {}
}

export class CloseSuccess implements Action {
  type = Types.CloseSuccess;
  constructor(public payload: MenuTab) {}
}

export class Refresh implements Action {
  type = Types.Refresh;
  constructor(public payload: MenuTab) {}
}

export class RefreshSuccess implements Action {
  type = Types.RefreshSuccess;
  constructor(public payload: MenuTab) {}
}

export class EnableEditMode implements Action {
  type = Types.EnableEditMode;
  constructor(public payload: MenuTab) {}
}

export class DisableEditMode implements Action {
  type = Types.DisableEditMode;
  constructor(public payload: MenuTab) {}
}

export type All = Open | OpenSuccess | Close | CloseSuccess | Refresh | RefreshSuccess;
