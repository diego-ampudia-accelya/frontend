import * as MenuTabActions from '~app/core/actions/menu-tab.actions';
import * as ViewListActions from '~app/core/actions/view-list.actions';

export { MenuTabActions, ViewListActions };
