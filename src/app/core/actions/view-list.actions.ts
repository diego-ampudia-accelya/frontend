/* eslint-disable @typescript-eslint/no-shadow */
import { Action } from '@ngrx/store';

import {
  FilterChangeData,
  FilterFormValueChangeData,
  PageChangeData,
  QueryChangeData,
  SortChangeData
} from '~app/core/reducers/view-list.reducer';

export enum ActionTypes {
  FilterFormValueChange = '[ViewList] Filter Form Value Change',
  FilterChange = '[ViewList] Filter Change',
  PageChange = '[ViewList] Page Change',
  SortChange = '[ViewList] Sort Change',
  QueryChange = '[ViewList] Query Change'
}

export class FilterFormValueChange implements Action {
  readonly type = ActionTypes.FilterFormValueChange;
  constructor(public payload: FilterFormValueChangeData) {}
}

export class FilterChange implements Action {
  readonly type = ActionTypes.FilterChange;
  constructor(public payload: FilterChangeData) {}
}

export class SortChange implements Action {
  readonly type = ActionTypes.SortChange;
  constructor(public payload: SortChangeData) {}
}

export class PageChange implements Action {
  readonly type = ActionTypes.PageChange;
  constructor(public payload: PageChangeData) {}
}

export class QueryChange implements Action {
  readonly type = ActionTypes.QueryChange;
  constructor(public payload: QueryChangeData) {}
}

export type All = FilterFormValueChange | FilterChange | SortChange | PageChange | QueryChange;
