import { MenuTabActions } from '../actions';
import { reducer } from './menu-tab.reducer';
import { ROUTES } from '~app/shared/constants/routes';
import { MenuTab } from '~app/shared/models/menu-tab.model';

function findTabFromState(state, tab) {
  return state.tabs[Object.keys(state.tabs).find(tabKey => state.tabs[tabKey].url === tab.url)];
}

describe('Menu Tab Reducer', () => {
  describe('when processing a CloseSuccess action', () => {
    it('should remove closed tab', () => {
      const tab: MenuTab = ROUTES.AIRLINE;
      const tabLabel: string = typeof tab.tabLabel === 'string' ? tab.tabLabel : tab.tabLabel?.label;
      let updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));
      tab.id = 'MENU.MASTER_DATA.AIRLINE.LBL';
      updatedState = reducer(updatedState, new MenuTabActions.CloseSuccess(tab));

      expect(Object.values(updatedState.tabs).length).toBe(1);
      expect(updatedState.tabs[tabLabel]).toBeUndefined();
    });
  });

  describe('when processing an "OpenSuccess action', () => {
    it('should activate tab', () => {
      const tab = ROUTES.AIRLINE;

      const updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));

      expect(updatedState.activeTabId).toBe(tab.tabLabel);
    });

    it('should add tab to tabs collection', () => {
      const tab = ROUTES.AIRLINE;

      const updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));

      expect(updatedState.tabs[tab.tabLabel]).toBeDefined();
    });

    it('should set sequence number of new tabs', () => {
      const tab = ROUTES.AIRLINE;

      const updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));
      const actual = updatedState.tabs[tab.tabLabel];

      expect(actual.sequenceNumber).toBe(1);
    });

    it('should preserve existing items', () => {
      const tab = ROUTES.AIRLINE;

      const updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));

      expect(Object.values(updatedState.tabs).length).toBe(2);
    });

    it('should preserve sequence number when opening an existing tab if it has multiple property disabled', () => {
      const tab: MenuTab = ROUTES.DASHBOARD;
      const otherTab: MenuTab = ROUTES.AIRLINE;
      let updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));
      updatedState = reducer(updatedState, new MenuTabActions.OpenSuccess(otherTab));

      updatedState = reducer(updatedState, new MenuTabActions.OpenSuccess(tab));

      const dashboardTab = findTabFromState(updatedState, tab);

      expect(dashboardTab.sequenceNumber).toBe(0);
    });

    it('should update sequence number when opening a multiple tab of same type but different url', () => {
      const tab: MenuTab = { ...ROUTES.DOCUMENT_VIEW, url: '/documents/view/aB-10' };
      const otherTab: MenuTab = { ...ROUTES.DOCUMENT_VIEW, url: '/documents/view/aB-20' };
      let updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));
      updatedState = reducer(updatedState, new MenuTabActions.OpenSuccess(otherTab));

      const updatedTab = findTabFromState(updatedState, otherTab);
      expect(updatedTab.sequenceNumber).toBe(2);
    });

    it('should preserve sequence number when opening a multiple tab of same type and same url', () => {
      const tab: MenuTab = { ...ROUTES.DOCUMENT_ENQUIRY, url: '/documents/enquiry/123456789' };
      const otherTab: MenuTab = { ...ROUTES.DOCUMENT_ENQUIRY, url: '/documents/enquiry/123456789' };
      let updatedState = reducer(undefined, new MenuTabActions.OpenSuccess(tab));
      updatedState = reducer(updatedState, new MenuTabActions.OpenSuccess(otherTab));

      const tabOpenedCount = Object.keys(updatedState.tabs).length;
      expect(tabOpenedCount).toBe(2);
    });
  });
});
