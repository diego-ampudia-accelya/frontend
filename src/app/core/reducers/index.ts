import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromMenuTabs from './menu-tab.reducer';
import * as viewLists from './view-list.reducer';
import { AppState } from '~app/reducers';

export interface CoreState {
  menu: fromMenuTabs.MenuState;
  viewListsInfo: viewLists.ViewListState;
}

export interface CoreAppState extends AppState {
  core: CoreState;
}

export const reducers: ActionReducerMap<CoreState> = {
  menu: fromMenuTabs.reducer,
  viewListsInfo: viewLists.reducer
};

export const getCoreState = createFeatureSelector<CoreAppState, CoreState>('core');

export const getMenuState = createSelector(getCoreState, state => state.menu);

export const getTabById = createSelector(getMenuState, fromMenuTabs.getTabById);

export const getActiveTab = createSelector(getMenuState, fromMenuTabs.getActiveTab);

export const getMenuTabs = createSelector(getMenuState, fromMenuTabs.getTabs);

export const getNextAvailableTab = createSelector(getActiveTab, getMenuTabs, fromMenuTabs.findNextAvailable);

export const getViewListsState = createSelector(getCoreState, state => state.viewListsInfo);

export const getViewListStateByTabId = createSelector(getViewListsState, viewLists.getViewListByTabId);

export const getActiveViewListState = createSelector(getViewListsState, getMenuState, viewLists.getActiveViewList);

export const getViewListState = (viewListId: string) => createSelector(getViewListsState, state => state[viewListId]);

export const getViewListFormValue = (viewListId: string) =>
  createSelector(getViewListState(viewListId), state => state && state.filterFormValue);

export const getViewListFilter = (viewListId: string) =>
  createSelector(getViewListState(viewListId), state => state && state.filter);
