import { omit } from 'lodash';
import { MenuTabActions, ViewListActions } from '~app/core/actions';
import { PaginationInfo } from '~app/shared/models/pagination-info.model';
import { SortingObject } from '~app/shared/models/sorting-object.model';
import { MenuState } from './menu-tab.reducer';

export interface ViewListActionData {
  viewListId: string;
}

export interface FilterFormValueChangeData extends ViewListActionData {
  value: any;
}

export interface FilterChangeData extends ViewListActionData {
  filter: any;
}

export interface SortChangeData extends ViewListActionData {
  sorting: Array<SortingObject>;
}

export interface PageChangeData extends ViewListActionData {
  paging: PaginationInfo;
}

export interface ViewListInfo {
  filterFormValue: any;
  filter: any;
  sorting: Array<SortingObject>;
  paging: PaginationInfo;
}

export interface QueryChangeData extends ViewListActionData, ViewListInfo {}

export interface ViewListState {
  [key: string]: ViewListInfo;
}

const defaultState: ViewListState = {};

export const getViewListByTabId = (state: ViewListState, id: string) => {
  const viewLists = Object.entries(state).filter(([key, _]) => key.includes(id));
  const defaultViewList = viewLists.find(([key, _]) => key === `${id}:default`);

  return viewLists.length > 1 && defaultViewList ? defaultViewList : viewLists[0];
};

export const getActiveViewList = (viewList: ViewListState, menu: MenuState) =>
  getViewListByTabId(viewList, menu.activeTabId);

export function reducer(state = defaultState, action: ViewListActions.All | MenuTabActions.All): ViewListState {
  let viewListId: string;

  switch (action.type) {
    case ViewListActions.ActionTypes.FilterFormValueChange:
      viewListId = action.payload.viewListId;

      return {
        ...state,
        [viewListId]: {
          ...state[viewListId],
          filterFormValue: action.payload.value
        }
      };

    case ViewListActions.ActionTypes.FilterChange: {
      const filter = action.payload.filter;
      viewListId = action.payload.viewListId;

      return {
        ...state,
        [viewListId]: {
          ...state[viewListId],
          filter
        }
      };
    }
    case ViewListActions.ActionTypes.SortChange: {
      const sorting = action.payload.sorting;
      viewListId = action.payload.viewListId;

      return {
        ...state,
        [viewListId]: {
          ...state[viewListId],
          sorting
        }
      };
    }
    case ViewListActions.ActionTypes.PageChange: {
      const paging = action.payload.paging;
      viewListId = action.payload.viewListId;

      return {
        ...state,
        [viewListId]: {
          ...state[viewListId],
          paging
        }
      };
    }
    case ViewListActions.ActionTypes.QueryChange:
      viewListId = action.payload.viewListId;

      return {
        ...state,
        [viewListId]: {
          ...action.payload
        }
      };

    case MenuTabActions.Types.CloseSuccess: {
      const tab = action.payload;
      const tabLabel = typeof tab.tabLabel === 'string' ? tab.tabLabel : tab.tabLabel?.label;

      let resultState = { ...state };
      Object.keys(state).forEach(key => {
        if (key.startsWith(tabLabel)) {
          resultState = omit(resultState, key);
        }
      });

      return resultState;
    }
  }

  return state;
}
