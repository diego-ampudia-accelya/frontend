import { ROUTES } from '~app/shared/constants/routes';
import { MenuTab } from '~app/shared/models/menu-tab.model';
import { MenuTabActions } from '../actions';

export interface MenuState {
  tabs: { [key: string]: MenuTab };
  activeTabId: string;
}

const defaultTab: MenuTab = { ...ROUTES.DASHBOARD, sequenceNumber: 0, id: ROUTES.DASHBOARD.tabLabel };
const initialState: MenuState = {
  tabs: {
    [defaultTab.id]: defaultTab
  },
  activeTabId: defaultTab.id
};

export function areEqual(first: MenuTab, second: MenuTab): boolean {
  const tabsExist = first && second;

  return tabsExist && first.id === second.id;
}

export function findNextAvailable(tab: MenuTab, tabs: MenuTab[]): MenuTab {
  // Capture index of current tab
  const tabIndex = tabs.findIndex(other => areEqual(other, tab));

  // Remove current tab from activatable tabs.
  const activatableTabs = tabs.filter(other => !areEqual(other, tab));

  // Find previous activatable tab
  const previousIndex = Math.max(tabIndex - 1, 0);

  return activatableTabs[previousIndex];
}

function matchString(regexPattern: string, target: string) {
  const matched = regexPattern ? new RegExp(regexPattern).exec(target) : [];

  return matched ? matched.shift() : '';
}

export function determineTabId(tab: MenuTab): string {
  let tabId = typeof tab.tabLabel === 'string' ? tab.tabLabel : tab.tabLabel?.label;
  if (tab.uniqueIdFromUrlRegex) {
    tabId = matchString(tab.uniqueIdFromUrlRegex, tab.url);
    if (!tabId) {
      throw Error(`Could not extract id from ${tab.url} with pattern ${tab.uniqueIdFromUrlRegex}`);
    }
  }

  return tabId;
}

export const getActiveTab = (state: MenuState) => state.tabs[state.activeTabId];

export const getTabById = (state: MenuState, { id }) => state.tabs[id];

export const getTabs = (state: MenuState) =>
  Object.values(state.tabs).sort((a, b) => (a.sequenceNumber || 0) - (b.sequenceNumber || 0));

export function reducer(state = initialState, action: MenuTabActions.All): MenuState {
  let payload;
  let tabId;

  switch (action.type) {
    case MenuTabActions.Types.CloseSuccess: {
      // remove closed tab
      const { [action.payload.id]: removed, ...rest } = state.tabs;

      return {
        ...state,
        tabs: rest
      };
    }
    case MenuTabActions.Types.OpenSuccess: {
      // The sequenceNumber is used to maintain the order of tabs for display purposes.
      // New items get larger sequenceNumbers
      payload = action.payload;
      tabId = determineTabId(payload);

      const exists = state.tabs[tabId];
      const tabCount = Object.keys(state.tabs).length;

      const tab: MenuTab = {
        ...action.payload,
        id: tabId,
        sequenceNumber: exists ? exists.sequenceNumber : tabCount
      };

      return {
        ...state,
        activeTabId: tab.id,
        tabs: {
          ...state.tabs,
          [tab.id]: tab
        }
      };
    }
    case MenuTabActions.Types.EnableEditMode:
      payload = action.payload;
      tabId = determineTabId(payload);

      return {
        ...state,
        tabs: {
          ...state.tabs,
          [tabId]: {
            ...payload,
            editMode: true
          }
        }
      };

    case MenuTabActions.Types.DisableEditMode:
      payload = action.payload;
      tabId = determineTabId(payload);

      return {
        ...state,
        tabs: {
          ...state.tabs,
          [tabId]: {
            ...payload,
            editMode: false
          }
        }
      };
    default:
      return state;
  }
}
