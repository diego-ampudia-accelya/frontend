import { TitleCasePipe } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter, finalize, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import {
  Logout,
  NavigateToClassicBspLinkAgentGroupImpersonation,
  NavigateToClassicBspLinkIataImpersonation,
  SelectLanguage
} from '~app/auth/actions/auth.actions';
import { getSelectedLanguage, getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { getMenuTabs } from '~app/core/reducers';
import { DeactivateAccountService } from '~app/core/services/deactivate-account.service';
import { AdmAcmAuthorityListActions } from '~app/master-data/adm-acm-authority/store/actions';
import { SettingConfigurationActions } from '~app/master-data/configuration/store/actions';
import { RefundAuthorityListActions } from '~app/master-data/refund-authority/store/actions';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import { VariableRemittanceListActions } from '~app/master-data/variable-remittance/store/actions';
import {
  NotificationAction,
  NotificationHeader,
  NotificationSourceType
} from '~app/notifications/models/notifications-header.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ChangeLanguageDialogComponent } from '~app/shared/components/change-language-dialog/change-language-dialog.component';
import { DeactivateAccountDialogComponent } from '~app/shared/components/deactivate-account-dialog/deactivate-account-dialog.component';
import { InfoDialogComponent } from '~app/shared/components/form-dialog-components/info-dialog/info-dialog.component';
import { LoginAsAnyGroupMemberDialogComponent } from '~app/shared/components/login-as-any-group-member-dialog/login-as-any-group-member-dialog.component';
import { LoginAsAnyUserDialogComponent } from '~app/shared/components/login-as-any-user-dialog/login-as-any-user-dialog.component';
import { ReturnToOriginalProfileDialogComponent } from '~app/shared/components/return-to-original-profile-dialog/return-to-original-profile-dialog.component';
import { SwitchAccountDialogComponent } from '~app/shared/components/switch-account-dialog/switch-account-dialog.component';
import { FakePermissions, Permissions, ROUTES } from '~app/shared/constants';
import { Bsp } from '~app/shared/models/bsp.model';
import { AirlineUser, User, UserType } from '~app/shared/models/user.model';
import { TabService } from '~app/shared/services';
import { lowerAmountValidator } from '~app/shared/validators';

@Component({
  selector: 'bspl-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit, OnDestroy {
  @ViewChild('notificationsWrapper') notificationsWrapper: ElementRef;

  @Input() user: User;
  @Input() showActions = true;
  @Input() showProfile = true;
  @Input() isTrainingSite = false;
  @Input() environmentLabel = '';
  @Input() bsp: Bsp;
  @Input() isMultiCountryUser = false;

  @Output() logout = new EventEmitter();

  public openedUserMenu = false;
  public openedNotificationsMenu = false;

  public Permissions = Permissions;
  public FakePermissions = FakePermissions;
  public ROUTES = ROUTES;
  public userCanSwitchAccount: boolean;
  public userCanImpersonateMainUser: boolean;
  public userCanImpersonateGroupMember: boolean;
  public isAirlineUserTemplateVisible: boolean;
  public airlineUserTemplate: string;
  public userCanDeactivateAccount: boolean;
  public userAgentGroupIsImpersonating = false;
  public userIataIsImpersonating = false;

  public notificationsLoading: boolean;
  public notificationsCount: string;
  public notifications$: Observable<NotificationHeader[]>;
  public buttonDesign = ButtonDesign;
  public NotificationAction = NotificationAction;

  public searchInput: FormControl;

  private _loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this._loadingSubject.asObservable();

  private loggedUser: User;
  private destroy$ = new Subject();

  public get onlineHelpPermissions() {
    return [
      FakePermissions.readOnlineHelpIATA,
      FakePermissions.readOnlineHelpAirline,
      FakePermissions.readOnlineHelpAgent,
      FakePermissions.readOnlineHelpAgentGroup,
      FakePermissions.readOnlineHelpThirdParty,
      FakePermissions.readOnlineHelpGDS
    ];
  }

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public translationService: L10nTranslationService,
    public dialogService: DialogService,
    private permissionsService: PermissionsService,
    private titleCasePipe: TitleCasePipe,
    private store: Store<AppState>,
    private deactiveAcountService: DeactivateAccountService,
    private tabService: TabService,
    private notificationsHeaderService: NotificationsHeaderService,
    private router: Router
  ) {}

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.initializePermissions();
    this.initializeNotifications();
    this.initializeSearch();
  }

  public emitLogout(): void {
    this.openedUserMenu = false;
    this.logout.emit();
  }

  public onSwitchAccountClick(): void {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'headerUserInfo.switchAccountDialog.title',
        footerButtonsType: [{ type: FooterButton.Login, isDisabled: true }]
      }
    };

    this.dialogService.open(SwitchAccountDialogComponent, dialogConfig);
  }

  public onDeactivateAgentGroup(): void {
    this.openedUserMenu = false;

    const dialogConfig: DialogConfig = {
      data: {
        title: 'headerUserInfo.deactivateGroupDialog.title',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Apply, isDisabled: true }]
      }
    };

    this.dialogService
      .open(DeactivateAccountDialogComponent, dialogConfig)
      .pipe(
        first(),
        filter(action => action.clickedBtn === FooterButton.Apply),
        finalize(() => this.dialogService.close()),
        takeUntil(this.destroy$)
      )
      .subscribe(action => {
        if (action && action.clickedBtn === FooterButton.Apply) {
          this.dialogService.close();
          this.deactivateAgentGroupService();
        }
      });
  }

  public openLoginAsAnyGroupMemberDialog(returnToOriginalProfile: boolean): void {
    this.openedUserMenu = false;

    const config: DialogConfig = this.setLoginAsAnyGroupMemberDialogConfig(returnToOriginalProfile);

    this.dialogService
      .open(
        returnToOriginalProfile ? ReturnToOriginalProfileDialogComponent : LoginAsAnyGroupMemberDialogComponent,
        config
      )
      .pipe(
        filter(
          action =>
            action.clickedBtn === FooterButton.Login || action.clickedBtn === FooterButton.ReturnToAgentGroupProfile
        ),
        takeUntil(this.destroy$)
      )
      .subscribe(action => {
        this.setLoading(true);

        if (returnToOriginalProfile) {
          this.loginReturnToAgentGroupProfile();
        } else {
          this.loginAgentGroupUser(action);
        }

        this.closeTabs();
        this.dialogService.close();
      });
  }

  public redirectToNotificationsQuery(): void {
    this.router.navigateByUrl(ROUTES.NOTIFICATIONS_LIST.url);
    this.closeNotificationsMenu();
  }

  public onNotificationClick(notification: NotificationHeader): void {
    this.redirectOnNotificationClick(notification);
    this.markNotificationAsRead(notification);
    this.closeNotificationsMenu();
  }

  public toggleNotificationsMenu(): void {
    this.openedNotificationsMenu = !this.openedNotificationsMenu;
    this.notificationsLoading = this.openedNotificationsMenu;
  }

  public closeNotificationsMenu(): void {
    this.openedNotificationsMenu = false;
  }

  @HostListener('document:click', ['$event'])
  public onPageClick(event: MouseEvent): void {
    const target = event.target;
    // If user clicks outside notifications menu, the menu will close
    if (!this.notificationsWrapper?.nativeElement?.contains(target)) {
      this.closeNotificationsMenu();
    }
  }

  public onSearchClick(): void {
    if (this.searchInput.valid && this.searchInput.value) {
      this.router.navigate([ROUTES.SEARCH_LIST.url, this.searchInput.value]);
    }
  }

  private updateAcmAdmRequestNotificationSourceType(notification: NotificationHeader): void {
    const { action, sourceType } = notification;
    const isRequestStatus = action?.toUpperCase().includes('REQUEST');

    if (isRequestStatus) {
      const requestCodeMappings = {
        [NotificationSourceType.ACMA]: NotificationSourceType.ACM_REQUEST,
        [NotificationSourceType.ADMA]: NotificationSourceType.ADM_REQUEST
      };

      const transitionCodeRequest = requestCodeMappings[sourceType];
      if (transitionCodeRequest) {
        notification.sourceType = transitionCodeRequest;
      }
    }
  }

  private redirectOnNotificationClick(notification: NotificationHeader): void {
    // TODO temporal solution until BE is developed
    this.updateAcmAdmRequestNotificationSourceType(notification);
    this.notificationsHeaderService.navigateToNotificationLink(notification);
  }

  private markNotificationAsRead(notification: NotificationHeader): void {
    this.notificationsHeaderService.markNotificationAsRead(notification).subscribe();
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private initializeLoggedUser() {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });
  }

  private initializePermissions(): void {
    this.userCanSwitchAccount = this.permissionsService.hasPermission(Permissions.switchAccount);
    this.userCanDeactivateAccount = this.permissionsService.hasPermission(Permissions.deactivateAgentGroup);
    this.userCanImpersonateMainUser = this.permissionsService.hasPermission(Permissions.impersonateMainUser);
    this.userCanImpersonateGroupMember = this.permissionsService.hasPermission(Permissions.impersonateAgentGroupMember);

    const isAirlineUser = this.user?.userType === UserType.AIRLINE;
    const userAirline = this.user as AirlineUser;

    this.isAirlineUserTemplateVisible = isAirlineUser && userAirline?.globalTemplate !== null;

    if (this.isAirlineUserTemplateVisible) {
      this.airlineUserTemplate = this.translationService.translate(
        `headerUserInfo.airlineTemplate.${userAirline.globalTemplate}`
      );
    }

    const isAgentUser = this.user?.userType === UserType.AGENT;
    this.userAgentGroupIsImpersonating = this.userCanImpersonateGroupMember && isAgentUser;

    const isIataUser = this.user?.userType === UserType.IATA;
    this.userIataIsImpersonating = this.userCanImpersonateMainUser && !isIataUser;
  }

  private initializeNotifications(): void {
    this.initializeNotificationsCount();
    this.initializeNotificationsPanel();
  }

  private initializeNotificationsCount(): void {
    this.notificationsHeaderService
      .getInfiniteBellCount()
      .pipe(takeUntil(this.destroy$))
      .subscribe(count => (this.notificationsCount = count));
  }

  private initializeNotificationsPanel(): void {
    this.notifications$ = this.notificationsHeaderService.getNotifications().pipe(
      map(pagedData => pagedData.records.slice(0, 10)),
      tap(() => (this.notificationsLoading = false))
    );
  }

  private initializeSearch(): void {
    this.searchInput = new FormControl('', {
      validators: [lowerAmountValidator(9999999999)],
      updateOn: 'change'
    });
  }

  private setLoginAsAnyGroupMemberDialogConfig(returnToOriginalProfile: boolean): DialogConfig {
    return {
      data: {
        title: returnToOriginalProfile
          ? this.translationService.translate('headerUserInfo.returnToAgentGroupProfileDialog.title')
          : this.translationService.translate('headerUserInfo.loginAsAnyGroupMemberDialog.title'),
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [
          {
            type: returnToOriginalProfile ? FooterButton.ReturnToAgentGroupProfile : FooterButton.Login,
            isDisabled: returnToOriginalProfile ? false : true
          }
        ],
        dialogType: 'AgentGroup'
      }
    };
  }

  private loginAgentGroupUser(component: any): void {
    const selectedAgentGroupMember = component.contentComponentRef.form.value.selectedAgentGroupMember;

    this.store.dispatch(
      new NavigateToClassicBspLinkAgentGroupImpersonation({
        agentGroupMemberCode: selectedAgentGroupMember,
        returnToAgentGroupProfile: false
      })
    );
  }

  private loginReturnToAgentGroupProfile(): void {
    this.store.dispatch(
      new NavigateToClassicBspLinkAgentGroupImpersonation({
        agentGroupMemberCode: undefined,
        returnToAgentGroupProfile: true
      })
    );
  }

  public openLoginAsAnyUserDialog(returnToOriginalProfile: boolean): void {
    this.openedUserMenu = false;
    const config: DialogConfig = this.setLoginAsAnyUserDialogConfig(returnToOriginalProfile);

    this.dialogService
      .open(returnToOriginalProfile ? ReturnToOriginalProfileDialogComponent : LoginAsAnyUserDialogComponent, config)
      .pipe(
        filter(
          action =>
            action.clickedBtn === FooterButton.Login || action.clickedBtn === FooterButton.ReturnToOriginalProfile
        ),
        takeUntil(this.destroy$)
      )
      .subscribe(action => {
        this.setLoading(true);

        if (returnToOriginalProfile) {
          this.loginReturnToOriginalProfile();
        } else {
          this.loginAsMainUser(action);
        }

        this.closeTabs();
        this.dialogService.close();
      });
  }

  private setLoginAsAnyUserDialogConfig(returnToOriginalProfile: boolean): DialogConfig {
    return {
      data: {
        title: returnToOriginalProfile
          ? this.translationService.translate('headerUserInfo.returnToOriginalProfileDialog.title')
          : this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.title'),
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [
          {
            type: returnToOriginalProfile ? FooterButton.ReturnToOriginalProfile : FooterButton.Login,
            isDisabled: returnToOriginalProfile ? false : true
          }
        ],
        dialogType: 'Iata'
      }
    };
  }

  private confirmDeactivateAgentGroup(): void {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'headerUserInfo.deactivatedGroupDialog.title',
        contentTranslationKey: this.translationService.translate('headerUserInfo.deactivatedGroupDialog.description'),
        hasCancelButton: false,
        isClosable: false,
        footerButtonsType: [{ type: FooterButton.Ok }]
      }
    };

    this.dialogService
      .open(InfoDialogComponent, dialogConfig)
      .pipe(
        first(),
        filter(action => action.clickedBtn === FooterButton.Ok),
        finalize(() => this.dialogService.close()),
        takeUntil(this.destroy$)
      )
      .subscribe(action => {
        if (action && action.clickedBtn === FooterButton.Ok) {
          this.store.dispatch(new Logout());
        }
      });
  }

  private deactivateAgentGroupService(): void {
    this.deactiveAcountService.deactivateAgentGroupAccount(this.loggedUser.id).subscribe(() => {
      this.confirmDeactivateAgentGroup();
    });
  }

  public loginAsMainUser(component: any): void {
    const userTypeSelected = component.contentComponentRef.form.value.selectedUserType;
    const userCodeSelected = component.contentComponentRef.form.value.selectedMainUser.userCode;

    this.store.dispatch(
      new NavigateToClassicBspLinkIataImpersonation({ userType: userTypeSelected, userCode: userCodeSelected })
    );
  }

  // when returning to original profile the settings with changes needs to be resetted
  private resetSettingsState(): void {
    this.store.dispatch(SettingConfigurationActions.discard());
    this.store.dispatch(TicketingAuthorityListActions.discard());
    this.store.dispatch(RefundAuthorityListActions.discard());
    this.store.dispatch(AdmAcmAuthorityListActions.discard());
    this.store.dispatch(VariableRemittanceListActions.discard());
  }

  private loginReturnToOriginalProfile(): void {
    this.resetSettingsState();
    this.store.dispatch(new NavigateToClassicBspLinkIataImpersonation({ userType: 2, userCode: undefined }));
  }

  public openChangeLanguageDialog(): void {
    this.openedUserMenu = false;

    this.store
      .pipe(
        select(getSelectedLanguage),
        first(),
        switchMap(selectedLanguage => {
          const config: DialogConfig = {
            data: {
              title: this.translationService.translate('headerUserInfo.languageDialog.title'),
              hasCancelButton: true,
              isClosable: true,
              footerButtonsType: [{ type: FooterButton.Apply }],
              selectedLanguage
            }
          };

          return this.dialogService.open(ChangeLanguageDialogComponent, config);
        }),
        filter(action => action.clickedBtn === FooterButton.Apply),
        takeUntil(this.destroy$)
      )
      .subscribe(action => {
        this.store.dispatch(new SelectLanguage({ language: action.contentComponentRef.selectedLanguage }));

        this.dialogService.close();
      });
  }

  private closeTabs(): void {
    this.store.pipe(select(getMenuTabs), first()).subscribe(tabs => {
      const tabsToClose = tabs.filter(tab => tab.tabLabel !== 'DASHBOARD.dashboards.tabLabel');

      if (tabsToClose) {
        tabsToClose.forEach(tab => this.tabService.closeTab(tab));
      }
    });
  }

  private setLoading(loading: boolean): void {
    this._loadingSubject.next(loading);
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  public formatUserType(userType: UserType): string {
    const type = userType?.split('_').join(' ');

    return userType && userType === UserType.IATA ? userType.toUpperCase() : this.titleCasePipe.transform(type);
  }
}
