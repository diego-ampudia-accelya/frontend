import { TitleCasePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { HeaderComponent } from '~app/core/components/header/header.component';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { SwitchAccountDialogComponent } from '~app/shared/components/switch-account-dialog/switch-account-dialog.component';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { TabService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = jasmine.createSpyObj('L10nTranslationService', ['translate', 'onChange']);
translationServiceSpy.translate.and.returnValue({ subscribe: () => {} });

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let dialogService: DialogService;

  const initialState = {
    auth: {
      user: createAirlineUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent, HasPermissionPipe, TranslatePipeMock],
      imports: [RouterTestingModule, HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        TitleCasePipe,
        mockProvider(NotificationsHeaderService, {
          getInfiniteBellCount: () => of('1'),
          getNotifications: () => of(null)
        }),
        provideMockStore({ initialState }),
        PermissionsService,
        TabService,
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.inject<any>(DialogService);
    fixture.detectChanges();
  });

  afterEach(() => {});

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emitLogout should call  logout emit', () => {
    const logoutEmitSpy = spyOn(component.logout, 'emit');
    component.emitLogout();
    expect(logoutEmitSpy).toHaveBeenCalled();
  });

  it('openChangeLanguageDialog should call dialog service open', fakeAsync(() => {
    spyOn(dialogService, 'open').and.returnValue(of());
    component.openChangeLanguageDialog();
    tick();
    expect(dialogService.open).toHaveBeenCalled();
  }));

  it('Dialog apply button click should close dialog', fakeAsync(() => {
    spyOn(dialogService, 'open').and.returnValue(of({ clickedBtn: FooterButton.Apply, contentComponentRef: {} }));
    const closeSpy = spyOn(dialogService, 'close');
    component.openChangeLanguageDialog();
    tick();
    expect(closeSpy).toHaveBeenCalled();
  }));

  it('should call dialog service open on switch account click', fakeAsync(() => {
    const config: DialogConfig = {
      data: {
        title: 'headerUserInfo.switchAccountDialog.title',
        footerButtonsType: [{ type: FooterButton.Login, isDisabled: true }]
      }
    };

    spyOn(dialogService, 'open').and.returnValue(of());

    component.onSwitchAccountClick();
    tick();

    expect(dialogService.open).toHaveBeenCalledWith(SwitchAccountDialogComponent, config);
  }));

  it('should call initializeNotifications on ngOnInit', () => {
    spyOn<any>(component, 'initializeNotifications');

    component.ngOnInit();

    expect(component['initializeNotifications']).toHaveBeenCalled();
  });
});
