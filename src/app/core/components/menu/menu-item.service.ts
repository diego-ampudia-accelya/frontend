import { EventEmitter, Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { MenuItem } from '~app/shared/models/menu-item.model';
import { User, UserType } from '~app/shared/models/user.model';

@Injectable()
export class MenuItemService {
  public menuItemClick = new EventEmitter<MenuItem>();
  private loggedUserType: string;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  constructor(private store: Store<AppState>) {
    this.initializeLoggedUser();
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => {
      this.loggedUserType = loggedUser.userType;
    });
  }

  /** Organization label is changed here depending on user type */
  public changeOrganizationLabel(): string {
    let labeltoSet = 'MENU.MASTER_DATA.MY.LBL';

    switch (this.loggedUserType) {
      case UserType.AGENT:
        labeltoSet = 'MENU.MASTER_DATA.MY_AGENT.LBL';
        break;
      case UserType.AIRLINE:
        labeltoSet = 'MENU.MASTER_DATA.MY_AIRLINE.LBL';
        break;
      case UserType.THIRD_PARTY:
        labeltoSet = 'MENU.MASTER_DATA.MY_THIRD_PARTY.LBL';
        break;
      case UserType.AGENT_GROUP:
        labeltoSet = 'MENU.MASTER_DATA.MY_AGENT_GROUP.LBL';
        break;
      case UserType.IATA:
        labeltoSet = 'MENU.MASTER_DATA.MY_IATA.LBL';
        break;
      case UserType.GDS:
        labeltoSet = 'MENU.MASTER_DATA.MY_GDS.LBL';
        break;
      case UserType.DPC:
        labeltoSet = 'MENU.MASTER_DATA.MY_DPC.LBL';
        break;
    }

    return labeltoSet;
  }
}
