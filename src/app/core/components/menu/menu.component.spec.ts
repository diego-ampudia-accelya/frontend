import { Component, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { MockDirective } from 'ng-mocks';
import { of } from 'rxjs';

import { MenuTriggerDirective } from './shared/menu-trigger.directive';
import { MenuItemService } from '~app/core/components/menu/menu-item.service';
import { MenuComponent } from '~app/core/components/menu/menu.component';
import { MenuService } from '~app/core/services/menu.service';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { MenuItem } from '~app/shared/models/menu-item.model';

@Component({
  template: ` AppMockComponent `
})
export class AppMockComponent {}

export const mockedRoute: Routes = [
  { path: 'bar', component: AppMockComponent },
  {
    path: 'foo',
    component: AppMockComponent,
    children: [
      { path: 'first', component: AppMockComponent, children: [{ path: 'issue', component: AppMockComponent }] }
    ]
  },
  {
    path: 'foo/bar/my',
    component: AppMockComponent
  },
  {
    path: 'foo/bar/ms',
    component: AppMockComponent
  }
];

const initialState = {
  auth: {
    user: createAirlineUser()
  },
  router: null,
  core: {
    menu: {
      tabs: {}
    },
    viewListsInfo: {}
  }
};

describe('MenuComponent', () => {
  let comp: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;
  let router: Router;

  const menuRet: MenuItem[] = [
    {
      check: 0,
      enabled: true,
      parent: null,
      key: 'FOO',
      label: 'MENU.FOO.LBL',
      permissionID: null,
      showMenu: true,
      route: '/foo',
      tabID: null,
      children: [
        {
          check: 0,
          children: [],
          enabled: true,
          parent: null,
          key: 'FOO_FIRST',
          label: 'MENU.FOO.FIRST.LBL',
          permissionID: 'foo_first',
          showMenu: true,
          route: '/first',
          tabID: 'MENU.FOO.FIRST'
        },
        {
          check: 0,
          children: [],
          enabled: true,
          parent: null,
          key: 'FOO_ISSUE',
          label: 'MENU.FOO.FIRST.ISSUE.LBL',
          permissionID: 'foo_issue',
          showMenu: true,
          route: '/first/issue',
          tabID: 'MENU.FOO.ISSUE'
        }
      ]
    },
    {
      check: 0,
      children: [],
      enabled: false,
      parent: null,
      key: 'FOO-BAR-MY',
      label: 'MENU.FOO.BAR.MY.LBL',
      permissionID: null,
      showMenu: true,
      route: '/foo/bar/my',
      tabID: null
    },
    {
      check: 0,
      children: [],
      enabled: false,
      parent: null,
      key: 'FOO-BAR-MS',
      label: 'MENU.FOO.BAR.MS.LBL',
      permissionID: null,
      showMenu: true,
      route: '/foo/bar/ms',
      tabID: null
    },
    {
      check: 0,
      children: [],
      enabled: false,
      parent: null,
      key: 'BAR',
      label: 'MENU.BAR.LBL',
      permissionID: null,
      showMenu: true,
      route: '/bar',
      tabID: null
    },
    {
      check: 0,
      children: [],
      enabled: false,
      parent: null,
      key: 'NO_ROUTE',
      label: 'MENU.NO_ROUTE.LBL',
      permissionID: null,
      showMenu: true,
      route: '',
      tabID: null
    }
  ];

  const menuServiceStub = jasmine.createSpyObj<MenuService>('MenuService', ['getMenuItems']);
  menuServiceStub.getMenuItems.and.returnValue(of([]));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(mockedRoute)],
      declarations: [MenuComponent, AppMockComponent, MockDirective(MenuTriggerDirective)],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: MenuService, useValue: menuServiceStub },
        MenuItemService,
        provideMockStore({ initialState })
      ]
    });

    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(MenuComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();

    router.initialNavigation();
  });

  it('should load instance', () => {
    expect(comp).toBeTruthy();
  });

  describe('navigateTo', () => {
    beforeEach(() => {
      spyOn(router, 'navigate');
      spyOn(comp.menuItemClick, 'emit').and.callThrough();
    });

    it('should open item route when route is specified', () => {
      const itemWithRoute = menuRet[1];
      comp.menuItemsCloned = [itemWithRoute];
      fixture.detectChanges();

      comp.navigateTo(itemWithRoute);

      expect(router.navigate).toHaveBeenCalledWith([itemWithRoute.route]);
    });

    it('should not open item route when route is missing', () => {
      const itemWithoutRoute = menuRet[4];
      comp.menuItemsCloned = [itemWithoutRoute];
      fixture.detectChanges();

      comp.navigateTo(itemWithoutRoute);

      expect(router.navigate).not.toHaveBeenCalled();
    });

    it('should emit `menuItemClick` when the item has no children', () => {
      const itemWithoutChildren = menuRet[1];
      comp.menuItemsCloned = [itemWithoutChildren];
      fixture.detectChanges();

      comp.navigateTo(itemWithoutChildren);

      expect(comp.menuItemClick.emit).toHaveBeenCalledWith(itemWithoutChildren);
    });

    it('should not emit `menuItemClick` when the item has children', () => {
      const itemWithChildren = menuRet[0];
      comp.menuItemsCloned = [itemWithChildren];
      fixture.detectChanges();

      comp.navigateTo(itemWithChildren);

      expect(comp.menuItemClick.emit).not.toHaveBeenCalled();
    });
  });

  it('should FOO menu item be active with /foo route', fakeAsync(() => {
    router.navigate(['/foo']);
    tick();

    comp.menuItemsCloned = menuRet;
    comp.ngOnChanges({ menuItems: new SimpleChange(null, menuRet, false) });
    fixture.detectChanges();

    const activeItem = comp.menuItemsCloned.find(item => item.key === 'FOO');
    const inactiveItem = comp.menuItemsCloned.find(item => item.key === 'BAR');

    expect(activeItem.active).toBe(true);
    expect(inactiveItem.active).toBe(false);
  }));

  it('should FOO menu item be active with /foo/first route', fakeAsync(() => {
    router.navigate(['/foo/first']);
    tick();

    comp.menuItemsCloned = menuRet;
    comp.ngOnChanges({ menuItems: new SimpleChange(null, menuRet, false) });
    fixture.detectChanges();

    const activeItem = comp.menuItemsCloned.find(item => item.key === 'FOO');
    expect(activeItem.active).toBe(true);
  }));

  it('should FOO menu item be active with /foo/first/issue route', fakeAsync(() => {
    router.navigate(['/foo/first/issue']);
    tick();

    comp.menuItemsCloned = menuRet;
    comp.ngOnChanges({ menuItems: new SimpleChange(null, menuRet, false) });
    fixture.detectChanges();

    const activeItem = comp.menuItemsCloned.find(item => item.key === 'FOO');
    expect(activeItem.active).toBe(true);
  }));

  it('should FOO-BAR-MY menu item be active with /foo/bar/my route', fakeAsync(() => {
    router.navigate(['/foo/bar/my']);
    tick();

    comp.menuItemsCloned = menuRet;
    comp.ngOnChanges({ menuItems: new SimpleChange(null, menuRet, false) });
    fixture.detectChanges();

    const activeItem = comp.menuItemsCloned.find(item => item.key === 'FOO-BAR-MY');
    const inactiveMatchingItem = comp.menuItemsCloned.find(item => item.key === 'FOO');

    expect(activeItem.active).toBe(true);
    expect(inactiveMatchingItem.active).toBe(false);
  }));

  it('should FOO-BAR-MS menu item be active when we have another route with the same size', fakeAsync(() => {
    router.navigate(['/foo/bar/ms']);
    tick();

    comp.menuItemsCloned = menuRet;
    comp.ngOnChanges({ menuItems: new SimpleChange(null, menuRet, false) });
    fixture.detectChanges();

    const activeItem = comp.menuItemsCloned.find(item => item.key === 'FOO-BAR-MS');
    const inactiveRouteWithTheSameSize = comp.menuItemsCloned.find(item => item.key === 'FOO-BAR-MY');
    const inactiveRouteWithTheSamePrefix = comp.menuItemsCloned.find(item => item.key === 'FOO');

    expect(activeItem.active).toBe(true);
    expect(inactiveRouteWithTheSameSize.active).toBe(false);
    expect(inactiveRouteWithTheSamePrefix.active).toBe(false);
  }));

  it('should not activate menu item if it has no route specified', fakeAsync(() => {
    router.navigate(['/bar']);
    tick();

    comp.menuItemsCloned = menuRet;
    comp.ngOnChanges({ menuItems: new SimpleChange(null, menuRet, false) });
    fixture.detectChanges();

    const activeItem = comp.menuItemsCloned.find(item => item.key === 'BAR');
    const inactiveItem = comp.menuItemsCloned.find(item => item.key === 'NO_ROUTE');

    expect(activeItem.active).toBeTruthy();
    expect(inactiveItem.active).toBeFalsy();
  }));
});
