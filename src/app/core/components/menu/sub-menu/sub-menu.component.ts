import { ScrollDispatcher } from '@angular/cdk/overlay';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { isEmpty } from 'lodash';

import { MenuPosition } from '../shared/menu-position.enum';
import { MenuTriggerDirective } from '../shared/menu-trigger.directive';
import { MenuItemService } from '~app/core/components/menu/menu-item.service';
import { MenuItem } from '~app/shared/models/menu-item.model';

@Component({
  selector: 'bspl-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.scss'],
  providers: [ScrollDispatcher],
  encapsulation: ViewEncapsulation.None
})
export class SubMenuComponent {
  @Input() menuItems: MenuItem[] = [];
  @Input() menuPosition: MenuPosition = MenuPosition.Left;

  public MenuPosition = MenuPosition;
  public openedMenu: MenuTriggerDirective;

  constructor(private menuItemService: MenuItemService) {}

  public navigateTo(menuItem: MenuItem) {
    if (isEmpty(menuItem.children)) {
      this.menuItemService.menuItemClick.emit(menuItem);
    }
  }

  public onOpen(menuToOpen: MenuTriggerDirective, menuItem: MenuItem): void {
    // Make sure that we have a single sub-menu open at a time
    // and close opened menu on hover menus without children
    this.openedMenu?.close();

    if (menuItem.children?.length) {
      this.openedMenu = menuToOpen;
    }
  }

  public onClose(): void {
    this.openedMenu = null;
  }
}
