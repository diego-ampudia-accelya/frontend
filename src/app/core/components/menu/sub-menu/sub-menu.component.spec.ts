import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockDirective, MockPipe } from 'ng-mocks';

import { MenuTriggerDirective } from '../shared/menu-trigger.directive';
import { MenuItemService } from '~app/core/components/menu/menu-item.service';
import { SubMenuComponent } from '~app/core/components/menu/sub-menu/sub-menu.component';
import { MenuStatus } from '~app/shared/enums';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { MenuItem } from '~app/shared/models/menu-item.model';

describe('SubMenuComponent', () => {
  let component: SubMenuComponent;

  let fixture: ComponentFixture<SubMenuComponent>;
  let menuItemService: MenuItemService;

  const menuRet: MenuItem[] = [
    {
      check: MenuStatus.EMPTY,
      children: [
        {
          check: MenuStatus.EMPTY,
          children: [],
          enabled: true,
          parent: null,
          key: 'FOO_FIRST',
          label: 'MENU.FOO.FIRST.LBL',
          permissionID: 'foo_first',
          showMenu: true,
          route: '/first',
          tabID: 'MENU.FOO.FIRST'
        }
      ],
      enabled: true,
      parent: null,
      key: 'FOO',
      label: 'MENU.FOO.LBL',
      permissionID: null,
      showMenu: true,
      route: '/foo',
      tabID: null
    },
    {
      check: MenuStatus.EMPTY,
      children: [],
      enabled: true,
      parent: null,
      key: 'DOE',
      label: 'MENU.DOE.LBL',
      permissionID: null,
      showMenu: true,
      route: '/doe',
      tabID: null
    }
  ];

  const initialState = {
    auth: {
      user: createAirlineUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SubMenuComponent, MockPipe(L10nTranslatePipe), MockDirective(MenuTriggerDirective)],
      providers: [MenuItemService, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    menuItemService = TestBed.inject(MenuItemService);
    spyOn(menuItemService.menuItemClick, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit menuItemClick when calling navigateTo  on a menu item without children', () => {
    component.navigateTo(menuRet[0].children[0]);
    expect(menuItemService.menuItemClick.emit).toHaveBeenCalled();
  });

  it('should not emit menuItemClick when calling navigateTo on a menu item with children', () => {
    component.navigateTo(menuRet[0]);
    expect(menuItemService.menuItemClick.emit).not.toHaveBeenCalled();
  });

  it('should change `openedMenu` when `onOpen` is called if `menuItem` has children', () => {
    const menu = createSpyObject(MenuTriggerDirective);

    component.onOpen(menu, menuRet[0]);
    expect(component.openedMenu).toBe(menu);
  });

  it('should change NOT `openedMenu` when `onOpen` is called if `menuItem` has NOT any children', () => {
    const menu = createSpyObject(MenuTriggerDirective);

    component.onOpen(menu, menuRet[1]);
    expect(component.openedMenu).toBeUndefined();
  });

  it('should clear `openedMenu` when `onClose` is called', () => {
    const menu = createSpyObject(MenuTriggerDirective);

    component.onOpen(menu, menuRet[0]);
    component.onClose();

    expect(component.openedMenu).toBeFalsy();
  });

  it('should close previous menu when `onOpen` is called', () => {
    const previousMenu = createSpyObject(MenuTriggerDirective);
    const menu = createSpyObject(MenuTriggerDirective);

    component.onOpen(previousMenu, menuRet[0]);
    component.onOpen(menu, menuRet[1]);

    expect(previousMenu.close).toHaveBeenCalled();
  });
});
