import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren
} from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { cloneDeep, isEmpty } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { filter, shareReplay, takeUntil } from 'rxjs/operators';

import { MenuPosition } from './shared/menu-position.enum';
import { MenuTriggerDirective } from './shared/menu-trigger.directive';
import { MenuItemService } from '~app/core/components/menu/menu-item.service';
import { MenuItem } from '~app/shared/models/menu-item.model';

@Component({
  selector: 'bspl-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [MenuItemService]
})
export class MenuComponent implements OnChanges, OnDestroy, OnInit {
  @Input() menuItems: MenuItem[] = [];
  @Output() menuItemClick = new EventEmitter<MenuItem>();

  public MenuPosition = MenuPosition;
  public openedMenu: MenuTriggerDirective;
  public menuItemsCloned: MenuItem[] = [];

  @ViewChildren(MenuTriggerDirective)
  private menus: QueryList<MenuTriggerDirective<MenuItem>>;

  private destroyed$ = new Subject();

  private actionableItemClicked$: Observable<MenuItem> = this.menuItemService.menuItemClick.asObservable().pipe(
    filter((menuItem: MenuItem) => isEmpty(menuItem.children)),
    takeUntil(this.destroyed$)
  );

  private navigationEnded$: Observable<Event> = this.router.events.pipe(
    filter(event => event instanceof NavigationEnd),
    shareReplay(1),
    takeUntil(this.destroyed$)
  );

  constructor(protected router: Router, private menuItemService: MenuItemService) {}

  public ngOnInit() {
    this.changeOrganizationLabelAndCloneMenuItems();
    this.actionableItemClicked$.subscribe((menuItem: MenuItem) => {
      this.openedMenu?.close();
      this.menuItemClick.emit(menuItem);

      if (menuItem.route) {
        this.router.navigate([menuItem.route]);
      }
    });
    this.navigationEnded$.subscribe(() => this.updateActiveMenuItem(this.menuItemsCloned));
  }

  private changeOrganizationLabelAndCloneMenuItems() {
    const itemToChangeLabel: MenuItem = this.menuItems.find(item => item.key === 'MY_ORGANIZATION');

    if (itemToChangeLabel) {
      itemToChangeLabel.label = this.menuItemService.changeOrganizationLabel();
    }
    this.menuItemsCloned = cloneDeep(this.menuItems);
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.menuItems) {
      this.updateActiveMenuItem(this.menuItemsCloned);
    }
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  public navigateTo(menuItem: MenuItem): void {
    this.menuItemService.menuItemClick.emit(menuItem);
    const menu = this.menus.toArray().find(m => m.item === menuItem);
    menu?.open();
  }

  private updateActiveMenuItem(menuItems: MenuItem[]): void {
    const activeItem = this.findActiveItem(menuItems);

    menuItems.forEach(menuItem => {
      menuItem.active = activeItem && menuItem.key === activeItem.key;
    });
  }

  private findActiveItem(menuItems: MenuItem[]): MenuItem {
    let activeItem: MenuItem = this.flatRecursively(menuItems)
      .filter(item => item.route && this.router.isActive(item.route, false))
      .reduce((currentBest, item) => {
        if (!currentBest || item.route.length > currentBest.route.length) {
          currentBest = item;
        }

        return currentBest;
      }, null);

    // Find topmost parent
    while (activeItem && activeItem.parent) {
      activeItem = activeItem.parent ? activeItem.parent : activeItem;
    }

    return activeItem;
  }

  private flatRecursively(menuItems: MenuItem[]): MenuItem[] {
    const items = [...menuItems];
    for (const item of menuItems) {
      items.push(...this.flatRecursively(item.children));
    }

    return items;
  }
}
