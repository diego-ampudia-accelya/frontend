import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { Component, DebugElement, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';

import { MenuTriggerDirective } from './menu-trigger.directive';

@Component({
  selector: 'bspl-test-host',
  template: `
    <div
      #menu="bsplMenuTrigger"
      [bsplMenuTrigger]
      [menuTemplate]="menuTemplate"
      [menuTriggerEvent]="'hover'"
      [menuIsRoot]="true"
    ></div>
    <ng-template #menuTemplate></ng-template>
  `
})
class TestHostComponent {
  @ViewChild('menu') menu: MenuTriggerDirective;
}

describe('MenuTriggerDirective', () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let testHost: TestHostComponent;
  let menuTrigger: MenuTriggerDirective;
  let scrollDispatcher: ScrollDispatcher;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule, PortalModule],
      declarations: [TestHostComponent, MenuTriggerDirective],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;
    fixture.detectChanges();

    menuTrigger = testHost.menu;

    spyOn(menuTrigger.opened, 'emit').and.callThrough();
    spyOn(menuTrigger.closed, 'emit').and.callThrough();

    scrollDispatcher = TestBed.inject(ScrollDispatcher);
  });

  it('should create', () => {
    expect(menuTrigger).toBeDefined();
  });

  describe('open', () => {
    it('should emit `opened` event', fakeAsync(() => {
      menuTrigger.open();
      tick();

      expect(menuTrigger.opened.emit).toHaveBeenCalledTimes(1);
    }));

    it('should not set again `overlayRef` when already open', fakeAsync(() => {
      spyOn(menuTrigger['overlay'], 'create').and.callThrough();

      menuTrigger.open();
      menuTrigger.open();
      tick();

      expect(menuTrigger['overlay'].create).toHaveBeenCalledTimes(1);
    }));

    it('should NOT set `overlayRef` when disabled', fakeAsync(() => {
      spyOn(menuTrigger['overlay'], 'create').and.callThrough();

      menuTrigger.enabled = false;
      menuTrigger.open();
      tick();

      expect(menuTrigger['overlay'].create).not.toHaveBeenCalled();
      expect(menuTrigger['overlayRef']).toBeUndefined();
    }));
  });

  describe('close', () => {
    it('should emit `closed` event', fakeAsync(() => {
      menuTrigger.open();
      menuTrigger.close();
      tick(500);

      expect(menuTrigger.closed.emit).toHaveBeenCalled();
    }));

    it('should not emit `closed` event when the overlay is already closed', fakeAsync(() => {
      menuTrigger.close();

      expect(menuTrigger.closed.emit).not.toHaveBeenCalled();
    }));
  });

  it('should open when trigger element is hovered', fakeAsync(() => {
    const triggerElement = getTriggerElement();

    const mouseoverEvent = new MouseEvent('mouseover');
    triggerElement.dispatchEvent(mouseoverEvent);

    expect(menuTrigger.opened.emit).toHaveBeenCalledTimes(1);
  }));

  it('should close when overlay backdrop is hovered', fakeAsync(() => {
    menuTrigger.open();

    const backdrop = document.querySelector('.cdk-overlay-transparent-backdrop');
    const mouseoverEvent = new MouseEvent('mouseover');
    backdrop.dispatchEvent(mouseoverEvent);
    tick(500);

    expect(menuTrigger.closed.emit).toHaveBeenCalledTimes(1);
  }));

  it('should open when `openEvent` is `click` and trigger is clicked', fakeAsync(() => {
    menuTrigger.ngOnDestroy();
    menuTrigger.triggerEvent = 'click';
    menuTrigger.ngOnInit();

    const triggerElement = getTriggerElement();
    triggerElement.click();

    expect(menuTrigger.opened.emit).toHaveBeenCalledTimes(1);
  }));

  it('should close when scroll is detected', fakeAsync(() => {
    const scrolled$ = new Subject();
    spyOn(scrollDispatcher, 'scrolled').and.returnValue(scrolled$);
    menuTrigger.open();

    scrolled$.next();
    tick(500);

    expect(menuTrigger.closed.emit).toHaveBeenCalledTimes(1);
  }));

  it('should not create a backdrop for sub-menus', fakeAsync(() => {
    menuTrigger.isRoot = false;

    menuTrigger.open();

    const backdrop = document.querySelector('.cdk-overlay-transparent-backdrop');
    expect(backdrop).toBeFalsy();
  }));

  function getTriggerElement(): HTMLElement {
    const trigger = fixture.debugElement.query(By.directive(MenuTriggerDirective));

    return getElement(trigger);
  }

  function getElement<T = HTMLElement>(debugElement: DebugElement): T {
    return debugElement.nativeElement as T;
  }
});
