import { ConnectedPosition } from '@angular/cdk/overlay';

export const topMenuViewportMargin = 30;
const subMenuOffset = 15;

const bottomLeft: ConnectedPosition = {
  originX: 'start',
  originY: 'bottom',
  overlayX: 'start',
  overlayY: 'top',
  offsetY: 0
};
const bottomRight: ConnectedPosition = {
  originX: 'end',
  originY: 'bottom',
  overlayX: 'end',
  overlayY: 'top',
  offsetY: 0,
  offsetX: -topMenuViewportMargin
};

const afterDown: ConnectedPosition = {
  originX: 'end',
  originY: 'top',
  overlayX: 'start',
  overlayY: 'top',
  offsetY: -subMenuOffset
};

const beforeDown: ConnectedPosition = {
  originX: 'start',
  originY: 'top',
  overlayX: 'end',
  overlayY: 'top',
  offsetY: -subMenuOffset
};

export const allowedMenuPositions = {
  bottomLeft: [bottomLeft],
  bottomRight: [bottomRight],
  before: [beforeDown],
  after: [afterDown]
};
