import {
  CloseScrollStrategy,
  Overlay,
  OverlayConfig,
  OverlayRef,
  PositionStrategy,
  ScrollDispatcher,
  ScrollStrategy,
  ViewportRuler
} from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { combineLatest, fromEvent, merge, Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, mapTo, skipUntil, switchMap, takeUntil } from 'rxjs/operators';

import { allowedMenuPositions, topMenuViewportMargin } from './positions.config';

const scrollInactivityPeriod = 250;

// Prefix directive inputs/outputs to avoid collisions
/* eslint-disable @angular-eslint/no-output-rename */
/* eslint-disable @angular-eslint/no-input-rename */

@Directive({
  selector: '[bsplMenuTrigger]',
  exportAs: 'bsplMenuTrigger'
})
export class MenuTriggerDirective<T = any> implements OnDestroy, OnInit {
  @Input() menuTemplate: TemplateRef<void>;
  @Input() menuPosition: 'bottomRight' | 'bottomLeft' | 'after' | 'before' = 'bottomRight';

  @Input('menuEnabled') enabled = true;
  @Input('menuIsRoot') isRoot = false;
  @Input('menuTriggerEvent') triggerEvent: 'hover' | 'click' = 'hover';
  @Input('menuItem') item: T;

  @Output('menuOpened') opened = new EventEmitter();
  @Output('menuClosed') closed = new EventEmitter();

  private overlayRef: OverlayRef;
  private menuPortal: TemplatePortal;

  private destroyed$ = new Subject();

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    private scrollDispatcher: ScrollDispatcher,
    private ngZone: NgZone,
    private viewportRuler: ViewportRuler
  ) {}

  public ngOnInit(): void {
    this.shouldOpen$().subscribe(() => this.ngZone.run(() => this.open()));
    this.shouldClose$().subscribe(() => this.close());
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
    this.close();
  }

  public open() {
    if (!this.overlayRef && this.enabled) {
      this.menuPortal = new TemplatePortal(this.menuTemplate, this.viewContainerRef);
      const config = this.getOverlayConfig();
      this.overlayRef = this.overlay.create(config);
      this.overlayRef.attach(this.menuPortal);
    }

    this.opened.emit();
  }

  public close() {
    if (!this.overlayRef || !this.enabled) {
      return;
    }

    const overlayRef = this.overlayRef;
    // `close` is called immediately when the overlay is disposed causing two closed events to be emitted.
    // Clearing `this.overlayRef` before disposing avoids the issue.
    this.overlayRef = null;
    overlayRef.dispose();

    this.closed.emit();
  }

  private getOverlayConfig(): OverlayConfig {
    return new OverlayConfig({
      scrollStrategy: this.createScrollStrategy(),
      positionStrategy: this.createPositionStrategy(this.elementRef),
      // Only top level menus have a backdrop.
      hasBackdrop: this.isRoot,
      panelClass: ['main-nav-submenu-pane'],
      backdropClass: ['cdk-overlay-transparent-backdrop']
    });
  }

  private createPositionStrategy(overlayOrigin: ElementRef): PositionStrategy {
    const viewportMargin = this.isRoot ? topMenuViewportMargin : 0;

    return (
      this.overlay
        .position()
        .flexibleConnectedTo(overlayOrigin)
        .withPositions(allowedMenuPositions[this.menuPosition])
        .withFlexibleDimensions(true)
        .withGrowAfterOpen(true)
        .withViewportMargin(viewportMargin)
        // Disable push for the top-level menu to avoid overlapping the trigger.
        .withPush(!this.isRoot)
    );
  }

  private createScrollStrategy(): ScrollStrategy {
    return new CloseScrollStrategy(this.scrollDispatcher, this.ngZone, this.viewportRuler);
  }

  private shouldClose$(): Observable<void> {
    return this.opened.pipe(
      switchMap(() => {
        if (!this.overlayRef) {
          return of(false);
        }

        const isBackdropHovered$ = this.isElementHovered$(this.overlayRef.backdropElement).pipe(
          filter(Boolean),
          mapTo(null)
        );
        const isBackdropClicked$ = this.overlayRef.backdropClick();
        const close$ = this.triggerEvent === 'hover' ? isBackdropHovered$ : isBackdropClicked$;

        return merge(close$, this.overlayRef.detachments()).pipe(takeUntil(this.closed.asObservable()));
      })
    );
  }

  private shouldOpen$(): Observable<void> {
    let shouldOpen$ = of<any>();
    if (this.triggerEvent === 'hover') {
      // Open the overlay when the trigger element is hovered.
      // In addition, avoid opening any overlays while the user scrolls.
      // Open the overlay when the scrolling stops if the trigger element is still hovered.
      shouldOpen$ = combineLatest([this.isElementHovered$(this.elementRef.nativeElement), this.isScrolling$()]).pipe(
        filter(([isHovered, isScrolling]) => isHovered && !isScrolling)
      );
    } else if (this.triggerEvent === 'click') {
      shouldOpen$ = fromEvent(this.elementRef.nativeElement, 'click');
    }

    return shouldOpen$.pipe(mapTo(null), takeUntil(this.destroyed$));
  }

  private isElementHovered$(element: HTMLElement): Observable<boolean> {
    if (!element) {
      return of(false);
    }

    return merge(
      fromEvent(element, 'mouseover').pipe(mapTo(true)),
      fromEvent(element, 'mouseleave').pipe(mapTo(false))
    ).pipe(distinctUntilChanged());
  }

  private isScrolling$(): Observable<boolean> {
    const scrolled$ = this.scrollDispatcher.scrolled();
    const scrollEnd$ = scrolled$.pipe(debounceTime(scrollInactivityPeriod), mapTo(false));
    const scrollStart$ = scrolled$.pipe(skipUntil(scrollEnd$), mapTo(true));

    return merge(of(false), scrollStart$, scrollEnd$);
  }
}
