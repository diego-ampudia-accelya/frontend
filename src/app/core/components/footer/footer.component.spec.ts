import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { FooterComponent } from './footer.component';
import { AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let httpClientSpy: SpyObject<HttpClient>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [TranslatePipeMock, FooterComponent],
      providers: [mockProvider(AppConfigurationService), mockProvider(HttpClient)]
    }).compileComponents();

    httpClientSpy = TestBed.inject<any>(HttpClient);
    httpClientSpy.get.and.returnValue(of({}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
