import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';
import { environment } from '~env/environment';

import { AppVersion } from '~app/core/models';
import { ROUTES } from '~app/shared/constants/routes';
import { AppConfigurationService } from '~app/shared/services';

@Component({
  selector: 'bspl-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() showLinks = true;

  private isLocalEnv = environment.environment.includes('local');
  private isDevEnv = environment.environment.includes('dev');

  public showStyleGuideLink = this.isLocalEnv || this.isDevEnv;
  public termsRouteUrl = ROUTES.TERMS.url;
  public appVersion: AppVersion;

  constructor(public appConfiguration: AppConfigurationService, private http: HttpClient) {}

  public ngOnInit(): void {
    this.getAppVersion().subscribe(v => (this.appVersion = v));
  }

  @Cacheable()
  private getAppVersion(): Observable<AppVersion> {
    const url = `${this.appConfiguration.baseApiPath}/nfe-version`;

    return this.http.get<AppVersion>(url).pipe(catchError(() => of(null)));
  }
}
