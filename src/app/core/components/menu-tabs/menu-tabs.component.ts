import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { timer } from 'rxjs';

import { ScrollContainerComponent } from '../scroll-container/scroll-container.component';
import { MenuTab } from '~app/shared/models/menu-tab.model';

@Component({
  selector: 'bspl-menu-tabs',
  templateUrl: './menu-tabs.component.html',
  styleUrls: ['./menu-tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuTabsComponent implements OnChanges {
  @Input() items: MenuTab[];
  @Input() active: MenuTab;

  @Output() closed = new EventEmitter<MenuTab>();
  @Output() activated = new EventEmitter<MenuTab>();

  @ViewChild('scrollContainer', { static: true })
  private scrollContainer: ScrollContainerComponent;

  constructor(private translationService: L10nTranslationService) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.active) {
      // Wait for rendering of new items to complete.
      // Otherwise, there won't be and item to scroll into view
      timer().subscribe(() => this.scrollContainer.scrollIntoView(changes.active.currentValue.id));
    }
  }

  public activate(tab: MenuTab): void {
    if (tab === this.active) {
      return;
    }

    this.activated.emit(tab);
  }

  public close(tab: MenuTab, event: MouseEvent): void {
    event.stopPropagation();
    this.closed.emit(tab);
  }

  public tabMiddleBtnClicked(event: MouseEvent, tab: MenuTab, isFirstTab: boolean): void {
    if (!isFirstTab) {
      this.close(tab, event);
    }
  }

  public getTabLabel(tab: MenuTab): string {
    let tabLabel: any = tab.tabLabel;
    tabLabel = this.translationService.translate(tabLabel.label || tabLabel, tabLabel.params);

    return tabLabel;
  }

  public isBadgeVisible(tab: MenuTab): boolean {
    return tab.showBadge;
  }

  public getTabTooltip(tab: MenuTab): string {
    let tooltip;

    if (tab.tooltipLabel) {
      tooltip = this.translationService.translate(tab.tooltipLabel, { tabLabel: this.getTabLabel(tab) });
    }

    return tooltip;
  }
}
