import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { NavigateToClassicBspLink } from '~app/auth/actions/auth.actions';
import { AppState } from '~app/reducers';
import { AppConfigurationService } from '~app/shared/services';

@Component({
  selector: 'bspl-login-error',
  templateUrl: './login-error.component.html',
  styleUrls: ['./login-error.component.scss']
})
export class LoginErrorComponent {
  public get canGoToClassic(): boolean {
    return Boolean(this.appConfiguration.classicBspLinkUrl);
  }

  constructor(private store: Store<AppState>, private appConfiguration: AppConfigurationService) {}

  public goToClassic(): void {
    this.store.dispatch(new NavigateToClassicBspLink());
  }
}
