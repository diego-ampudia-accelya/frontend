import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { LoginErrorComponent } from './login-error.component';
import { NavigateToClassicBspLink } from '~app/auth/actions/auth.actions';
import { AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('LoginErrorComponent', () => {
  let component: LoginErrorComponent;
  let fixture: ComponentFixture<LoginErrorComponent>;

  let mockStore: MockStore;
  let appConfigurationMock: SpyObject<AppConfigurationService>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore(), mockProvider(AppConfigurationService)],
      declarations: [LoginErrorComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');

    appConfigurationMock = TestBed.inject<any>(AppConfigurationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch NavigateToClassicBspLink when goToClassic is called', () => {
    spyOnProperty(appConfigurationMock, 'classicBspLinkUrl').and.returnValue('test');

    component.goToClassic();

    expect(mockStore.dispatch).toHaveBeenCalledWith(new NavigateToClassicBspLink());
  });

  describe('canGoToClassic', () => {
    it('should return true when Classic BSPLink URL is known', () => {
      spyOnProperty(appConfigurationMock, 'classicBspLinkUrl').and.returnValue('test');

      expect(component.canGoToClassic).toBeTruthy();
    });

    it('should return false when Classic BSPLink URL is unknown', () => {
      spyOnProperty(appConfigurationMock, 'classicBspLinkUrl').and.returnValue('');

      expect(component.canGoToClassic).toBeFalsy();
    });
  });
});
