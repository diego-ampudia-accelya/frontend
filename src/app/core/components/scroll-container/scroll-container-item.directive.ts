import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[bsplScrollContainerItem]',
  exportAs: 'bsplScrollContainerItem'
})
export class ScrollContainerItemDirective {
  @Input('bsplScrollContainerItem') dataItem: any;

  constructor(public elementRef: ElementRef<HTMLElement>) {}
}
