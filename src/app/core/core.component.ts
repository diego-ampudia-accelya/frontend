import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { intersection, iteratee } from 'lodash';
import { Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

import { Logout, NavigateToClassicBspLink } from '~app/auth/actions/auth.actions';
import { getUser, getUserBsps } from '~app/auth/selectors/auth.selectors';
import { MenuTabActions } from '~app/core/actions';
import { getActiveTab, getMenuTabs } from '~app/core/reducers';
import { MenuService } from '~app/core/services/menu.service';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { InfoDialogComponent } from '~app/shared/components/form-dialog-components/info-dialog/info-dialog.component';
import { ROUTES } from '~app/shared/constants/routes';
import { MenuItem } from '~app/shared/models/menu-item.model';
import { MenuTab } from '~app/shared/models/menu-tab.model';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Component({
  selector: 'bspl-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent implements OnInit, OnDestroy {
  public user$ = this.store.pipe(select(getUser));

  public menuItems$ = this.menuService.getMenuItems(ROUTES);
  public tabs$ = this.store.pipe(select(getMenuTabs));
  public activeTab$ = this.store.pipe(select(getActiveTab));

  public environmentLabel = appConfiguration.environmentLabel;

  public bsp$ = this.store.pipe(
    select(getUserBsps),
    filter(bsps => bsps && bsps.length === 1),
    map(bsps => bsps[0])
  );

  public isMultiCountryUser$ = this.store.pipe(
    select(getUserBsps),
    map(bsps => Boolean(bsps && bsps.length > 1))
  );

  public get isTrainingSite() {
    return appConfiguration.isTrainingSite;
  }

  private isTraining = false;

  private destroyed$ = new Subject();

  private bspsCodes$ = this.store.pipe(
    select(getUserBsps),
    filter(bsps => bsps && bsps.length > 0),
    map(bsps => bsps.map(iteratee('isoCountryCode'))),
    takeUntil(this.destroyed$)
  );

  constructor(
    private menuService: MenuService,
    private store: Store<AppState>,
    private router: Router,
    private dialogService: DialogService
  ) {}

  public ngOnInit() {
    this.initIsTraining();
  }

  public ngOnDestroy() {
    this.destroyed$.next();
  }

  public logout(): void {
    if (appConfiguration.isTrainingSite) {
      this.router.navigateByUrl(ROUTES.TRAINING_FINISH.url);
    } else if (this.isTraining) {
      this.router.navigateByUrl(ROUTES.USER_FEEDBACK_FINISH.url);
    } else {
      this.store.dispatch(new Logout());
    }
  }

  public openTab(tab: MenuTab): void {
    this.store.dispatch(new MenuTabActions.Open(tab));
  }

  public closeTab(tab: MenuTab): void {
    this.store.dispatch(new MenuTabActions.Close(tab, true));
  }

  public onMenuItemClick(menuItem: MenuItem): void {
    if (menuItem.isClassicMenuItem) {
      if (menuItem.key === 'CLASSIC_BSP_LINK') {
        this.openRedirectToClassicBspLinkDialog();
      }
    }
  }

  public openRedirectToClassicBspLinkDialog(): void {
    this.dialogService
      .open(InfoDialogComponent, {
        data: {
          hasCancelButton: true,
          footerButtonsType: FooterButton.Proceed,
          title: 'navigateToClassicBspLink.title',
          contentTranslationKey: 'navigateToClassicBspLink.message'
        }
      })
      .pipe(
        filter(action => action.clickedBtn === FooterButton.Proceed),
        tap(dialogData => {
          const proceedBtn = dialogData.contentComponentRef.config.data.buttons.find(
            button => button.type === FooterButton.Proceed
          );
          proceedBtn.isLoading = true;
        }),
        tap(() => this.store.dispatch(new NavigateToClassicBspLink()))
      )
      .subscribe();
  }

  private initIsTraining() {
    this.bspsCodes$.subscribe(bspCodes => {
      const trainingBspCodes = appConfiguration.trainingBspCodes;

      if (trainingBspCodes.length > 0) {
        this.isTraining = intersection(trainingBspCodes, bspCodes).length > 0;

        if (this.isTraining) {
          this.environmentLabel = 'Training';
        }
      }
    });
  }
}
