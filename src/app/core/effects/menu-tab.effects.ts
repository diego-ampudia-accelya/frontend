import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { RouterNavigatedAction, ROUTER_NAVIGATED } from '@ngrx/router-store';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, forkJoin, from, of } from 'rxjs';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { MenuTabActions } from '../actions';
import { getActiveTab, getNextAvailableTab } from '../reducers';
import { ChangesDialogService } from '../services/changes-dialog.service';
import { MenuService } from '../services/menu.service';
import { areEqual } from '~app/core/reducers/menu-tab.reducer';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class MenuTabEffects {
  @Effect({ dispatch: false })
  open$ = this.actions$.pipe(
    ofType<MenuTabActions.Open>(MenuTabActions.Types.Open),
    tap(action => {
      this.router.navigateByUrl(action.payload.url);
    })
  );

  @Effect()
  close$ = this.actions$.pipe(
    ofType<MenuTabActions.Close>(MenuTabActions.Types.Close),
    withLatestFrom(this.store.pipe(select(getActiveTab))),
    switchMap(([{ payload, click }, active]) => {
      const tab = payload || active;

      return combineLatest([
        of({ tab, click }),
        // Checking if closed tab is persistent and user has triggered tab close, so we throw the unsaved changes dialog
        tab.persistent && click ? this.changesDialogService.confirmUnsavedChanges(tab.tabLabel) : of(null)
      ]);
    }),
    filter(([_, result]) => !result || result === FooterButton.Continue),
    map(([{ tab, click }, _]) => new MenuTabActions.CloseConfirmed(tab, click))
  );

  @Effect()
  closeConfirmed$ = this.actions$.pipe(
    ofType<MenuTabActions.CloseConfirmed>(MenuTabActions.Types.CloseConfirmed),
    withLatestFrom(this.store.pipe(select(getNextAvailableTab)), this.store.pipe(select(getActiveTab))),
    switchMap(([action, nextAvailable, active]) => {
      if (areEqual(action.payload, active)) {
        return from(this.router.navigateByUrl(nextAvailable.url)).pipe(
          map(isSuccess => (isSuccess ? action : null)),
          catchError(() => of(null))
        );
      }

      return of(action);
    }),
    filter<MenuTabActions.CloseConfirmed>(action => !!action),
    tap(({ payload, click }) => {
      // Checking if closed tab is persistent and user has triggered tab close, so we throw discard changes notification
      if (click && payload.persistent) {
        this.notificationService.showSuccess(this.translationService.translate('documentUnsaved.notification')); // Side effect
      }
    }),
    // Close the tab after the navigation has completed successfully
    map(({ payload }) => new MenuTabActions.CloseSuccess(payload))
  );

  @Effect()
  refresh$ = this.actions$.pipe(
    ofType<MenuTabActions.Refresh>(MenuTabActions.Types.Refresh),
    filter<MenuTabActions.Refresh>(action => !!action.payload),
    map(action => action.payload),
    switchMap(tab => {
      // Saving original config to restore it once navigation is done
      const originalConfig = {
        shouldAttach: this.router.routeReuseStrategy.shouldAttach,
        shouldDetach: this.router.routeReuseStrategy.shouldDetach,
        shouldReuseRoute: this.router.routeReuseStrategy.shouldReuseRoute,
        onSameUrlNavigation: this.router.onSameUrlNavigation,
        tab
      };

      // Overwritting routeReuseStrategy functions to avoid component cache
      this.router.routeReuseStrategy.shouldDetach = () => false;
      this.router.routeReuseStrategy.shouldAttach = () => false;
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      // Overwritting onSameUrlNavigation to reload page since url is the same as current one
      this.router.onSameUrlNavigation = 'reload';

      return forkJoin([of(originalConfig), this.router.navigateByUrl(tab.url)]);
    }),
    tap(([originalConfig]) => {
      // Nav done. Side effect to restore original config
      this.router.routeReuseStrategy.shouldAttach = originalConfig.shouldAttach;
      this.router.routeReuseStrategy.shouldDetach = originalConfig.shouldDetach;
      this.router.routeReuseStrategy.shouldReuseRoute = originalConfig.shouldReuseRoute;

      this.router.onSameUrlNavigation = originalConfig.onSameUrlNavigation;
    }),
    map(([originalConfig]) => new MenuTabActions.RefreshSuccess(originalConfig.tab))
  );

  @Effect()
  navigate$ = this.actions$.pipe(
    ofType<RouterNavigatedAction>(ROUTER_NAVIGATED),
    map(() => this.menuService.findActiveTabConfig(this.router.routerState.snapshot)),
    filter(tab => !!tab),
    map(tab => new MenuTabActions.OpenSuccess(tab))
  );

  constructor(
    private actions$: Actions<MenuTabActions.All>,
    private router: Router,
    private store: Store<AppState>,
    private menuService: MenuService,
    private changesDialogService: ChangesDialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}
}
