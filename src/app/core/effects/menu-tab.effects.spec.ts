import { TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { cold, hot } from 'jasmine-marbles';
import { identity, set } from 'lodash';
import { Observable, of, throwError } from 'rxjs';

import { MenuTabActions } from '../actions';
import { CoreAppState } from '../reducers';
import { ChangesDialogService } from '../services/changes-dialog.service';
import { MenuService } from '../services/menu.service';

import { MenuTabEffects } from './menu-tab.effects';
import { NotificationService } from '~app/shared/services';
import { ROUTES } from '~app/shared/constants/routes';
import { FooterButton } from '~app/shared/components';

describe('MenuTabEffects', () => {
  let effects: MenuTabEffects;
  let mockRouter: jasmine.SpyObj<Router>;
  let actions$: Observable<any>;
  let mockStore: MockStore<CoreAppState>;
  let mockMenuService: jasmine.SpyObj<MenuService>;

  let initialState: CoreAppState;

  const changesDialogServiceSpy = createSpyObject(ChangesDialogService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    initialState = {
      router: null,
      core: {
        menu: {
          tabs: {
            [ROUTES.DASHBOARD.tabLabel]: { ...ROUTES.DASHBOARD, sequenceNumber: 0, id: ROUTES.DASHBOARD.tabLabel },
            [ROUTES.AIRLINE.tabLabel]: { ...ROUTES.AIRLINE, sequenceNumber: 1, id: ROUTES.AIRLINE.tabLabel }
          },
          activeTabId: ROUTES.DASHBOARD.tabLabel
        },
        viewListsInfo: {}
      }
    };

    mockMenuService = jasmine.createSpyObj('MenuService', ['findActiveTabConfig']);

    mockRouter = jasmine.createSpyObj('Router', ['navigateByUrl']);
    // rxjs-marbles has issues with Promises, so we return an observable instead.
    mockRouter.navigateByUrl.and.returnValue(of(true));
    set(mockRouter, 'routerState', { snapshot: {} });

    TestBed.configureTestingModule({
      providers: [
        MenuTabEffects,
        provideMockActions(() => actions$),
        provideMockStore({ initialState }),
        { provide: Router, useValue: mockRouter },
        { provide: MenuService, useValue: mockMenuService },
        { provide: ChangesDialogService, useValue: changesDialogServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    });

    effects = TestBed.inject(MenuTabEffects);
    mockStore = TestBed.inject<any>(Store);

    translationServiceSpy.translate.and.callFake(identity);
  }));

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('when an Open action is dispatched', () => {
    it('should navigate to new tab', waitForAsync(() => {
      const action = new MenuTabActions.Open(ROUTES.AGENT);
      actions$ = hot('-a|', { a: action });

      effects.open$.subscribe(() => {
        expect(mockRouter.navigateByUrl).toHaveBeenCalledWith(ROUTES.AGENT.url);
      });
    }));
  });

  describe('when a Close action is dispatched', () => {
    beforeEach(() => {
      mockStore.setState({
        ...initialState,
        core: {
          ...initialState.core,
          menu: { ...initialState.core.menu, activeTabId: ROUTES.AIRLINE.tabLabel }
        }
      });
    });

    it('should dispatch CloseConfirmed action on close persistent tab when user has selected Close Tab', () => {
      const closeAction = new MenuTabActions.Close(ROUTES.ACM_ISSUE, true);
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed(ROUTES.ACM_ISSUE, true);

      changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Continue));
      actions$ = hot('-a|', { a: closeAction });

      const expected = cold('-a|', { a: closeConfirmedAction });
      expect(effects.close$).toBeObservable(expected);
    });

    it('should NOT dispatch CloseConfirmed action on close persistent tab when user has selected Cancel', waitForAsync(() => {
      let actual: Action;

      actions$ = of(new MenuTabActions.Close(ROUTES.ADM_ISSUE, true));
      changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));
      effects.close$.subscribe(action => (actual = action));

      expect(actual).toBeUndefined();
    }));

    it('should NOT dispatch CloseConfirmed action on close persistent tab when user has closed dialog', waitForAsync(() => {
      let actual: Action;

      actions$ = of(new MenuTabActions.Close(ROUTES.ADM_ISSUE, true));
      changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));
      effects.close$.subscribe(action => (actual = action));

      expect(actual).toBeUndefined();
    }));

    it('should dispatch CloseConfirmed action on close no persistent tabs', () => {
      const closeAction = new MenuTabActions.Close(ROUTES.AIRLINE, true);
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed(ROUTES.AIRLINE, true);

      actions$ = hot('-a|', { a: closeAction });

      const expected = cold('-a|', { a: closeConfirmedAction });
      expect(effects.close$).toBeObservable(expected);
    });

    it('should dispatch CloseConfirmed action with active tab when Close action have no payload', () => {
      const closeAction = new MenuTabActions.Close();
      const expectedTab = { ...ROUTES.AIRLINE, sequenceNumber: 1, id: ROUTES.AIRLINE.tabLabel };
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed(expectedTab);

      actions$ = hot('-a|', { a: closeAction });

      const expected = cold('-a|', { a: closeConfirmedAction });
      expect(effects.close$).toBeObservable(expected);
    });

    it('should navigate to newly activated tab when closing the active tab is confirmed', waitForAsync(() => {
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed({
        ...ROUTES.AIRLINE,
        id: ROUTES.AIRLINE.tabLabel
      });
      actions$ = hot('-a|', { a: closeConfirmedAction });

      effects.closeConfirmed$.subscribe(() => {
        expect(mockRouter.navigateByUrl).toHaveBeenCalledWith(ROUTES.DASHBOARD.url);
      });
    }));

    it('should dispatch CloseSuccess action when closing an inactive tab is confirmed', () => {
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed(ROUTES.DASHBOARD);
      const closeSuccessAction = new MenuTabActions.CloseSuccess(ROUTES.DASHBOARD);
      actions$ = hot('-a|', { a: closeConfirmedAction });

      const expected = cold('-a|', { a: closeSuccessAction });
      expect(effects.closeConfirmed$).toBeObservable(expected);
    });

    it('should dispatch CloseSuccess action when navigation succeeds', () => {
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed(ROUTES.AIRLINE);
      const closeSuccessAction = new MenuTabActions.CloseSuccess(ROUTES.AIRLINE);

      mockRouter.navigateByUrl.and.returnValue(of(true));
      actions$ = hot('-a|', { a: closeConfirmedAction });

      const expected = cold('-a|', { a: closeSuccessAction });

      expect(effects.closeConfirmed$).toBeObservable(expected);
    });

    it('should return empty observable when an error occurs during navigation', () => {
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed({
        ...ROUTES.AIRLINE,
        id: ROUTES.AIRLINE.tabLabel
      });

      mockRouter.navigateByUrl.and.returnValue(throwError(null));
      actions$ = hot('-a|', { a: closeConfirmedAction });

      const expected = cold('--|');

      expect(effects.closeConfirmed$).toBeObservable(expected);
    });

    it('should return empty observable when navigation is cancelled', () => {
      const closeConfirmedAction = new MenuTabActions.CloseConfirmed({
        ...ROUTES.AIRLINE,
        id: ROUTES.AIRLINE.tabLabel
      });

      mockRouter.navigateByUrl.and.returnValue(of(false));
      actions$ = cold('-a|', { a: closeConfirmedAction });

      const expected = cold('--|');

      expect(effects.closeConfirmed$).toBeObservable(expected);
    });

    it('should show success notification when close confirmed and navigation succeeds', waitForAsync(() => {
      mockRouter.navigateByUrl.and.returnValue(of(true));

      actions$ = of(new MenuTabActions.CloseConfirmed(ROUTES.ADM_ISSUE, true));
      effects.closeConfirmed$.subscribe();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith('documentUnsaved.notification');
    }));
  });

  describe('when a ROUTER_NAVIGATED action is dispatched', () => {
    it('should dispatch OpenSuccess action with newly selected tab', waitForAsync(() => {
      mockMenuService.findActiveTabConfig.and.returnValue(ROUTES.AIRLINE);
      const navigateAction: Action = { type: ROUTER_NAVIGATED };
      actions$ = hot('-a|', { a: navigateAction });

      const expected = new MenuTabActions.OpenSuccess(ROUTES.AIRLINE);

      effects.navigate$.subscribe(action => {
        expect(action).toEqual(expected);
      });
    }));

    it('should not dispatch actions when configuration is not found', waitForAsync(() => {
      mockMenuService.findActiveTabConfig.and.returnValue(null);
      const navigateAction: Action = { type: ROUTER_NAVIGATED };
      actions$ = hot('-a|', { a: navigateAction });
      const expected = cold('--|');

      expect(effects.navigate$).toBeObservable(expected);
    }));
  });
});
