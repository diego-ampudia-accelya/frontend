import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { ErrorHandler, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { DialogModule } from 'primeng/dialog';

import { LoginErrorComponent } from './components/login-error/login-error.component';
import { MenuTabsComponent } from './components/menu-tabs/menu-tabs.component';
import { MenuTriggerDirective } from './components/menu/shared/menu-trigger.directive';
import { ScrollContainerItemDirective } from './components/scroll-container/scroll-container-item.directive';
import { ScrollContainerComponent } from './components/scroll-container/scroll-container.component';
import { MenuTabEffects } from './effects/menu-tab.effects';
import { reducers } from './reducers';
import { ChangesDialogService } from './services/changes-dialog.service';
import { GlobalErrorHandlerService } from './services/global-error-handler.service';
import { SharedModule } from '~app/shared/shared.module';
import { MenuService } from '~app/core/services/menu.service';
import { CoreComponent } from '~app/core/core.component';
import { SubMenuComponent } from '~app/core/components/menu/sub-menu/sub-menu.component';
import { MenuComponent } from '~app/core/components/menu/menu.component';
import { FooterComponent } from '~app/core/components/footer/footer.component';

@NgModule({
  declarations: [
    CoreComponent,
    FooterComponent,
    MenuComponent,
    SubMenuComponent,
    MenuTabsComponent,
    ScrollContainerComponent,
    ScrollContainerItemDirective,
    MenuTriggerDirective,
    LoginErrorComponent
  ],
  imports: [
    DialogModule,
    CommonModule,
    RouterModule,
    SharedModule,
    StoreModule.forFeature('core', reducers),
    EffectsModule.forFeature([MenuTabEffects]),
    OverlayModule,
    PortalModule
  ],
  exports: [FooterComponent],
  providers: [
    TitleCasePipe,
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandlerService
    },
    MenuService,
    ChangesDialogService
  ]
})
export class CoreModule {}
