import { createAction, props } from '@ngrx/store';

import { TicketModel } from '~app/pbd/shared/pbd.model';
import { ServerError } from '~app/shared/errors';

export const documentNumberChange = createAction(
  '[Start PBD] Document Number change',
  props<{ documentNumber: string }>()
);

export const searchDocument = createAction('[Start PBD] Search Document', props<{ documentNumber: string }>());

export const searchDocumentSuccess = createAction(
  '[Start PBD] Search Document Success',
  props<{ documents: TicketModel[] }>()
);

export const searchDocumentFailure = createAction(
  '[Start PBD] Search Document Failure',
  props<{ error: ServerError }>()
);

export const selectDocument = createAction('[Start PBD] Select Document', props<{ selectedDocument: TicketModel }>());
