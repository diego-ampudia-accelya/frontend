import { createAction, props } from '@ngrx/store';

import { AgreementType, Pbd } from '~app/pbd/shared/pbd.model';

export const addPbdAmountData = createAction('[PBD Details] Add PBD Amount', props<{ pbdAmount: string }>());

export const pbdIssue = createAction('[PBD Details] Issue PBD');

export const pbdIssueConfirmed = createAction('[PBD Details] Issue PBD Confirmed', props<{ id: number }>());

export const cancelPbdIssue = createAction('[PBD Details] Cancel PBD Issue');

export const changePbdAgreement = createAction(
  '[PBD Details] Change PBD Agreement',
  props<{ pbd: Pbd; agreementType: AgreementType }>()
);
