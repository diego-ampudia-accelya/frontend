import { createAction, props } from '@ngrx/store';

import { Pbd } from '~app/pbd/shared/pbd.model';
import { ServerError } from '~app/shared/errors';

export const requestPbdIssue = createAction(
  '[Issue PBD Dialog] Request PBD Issue',
  props<{ pbd: Pbd; reason: string; fileIds: string[] }>()
);

export const pbdIssueSuccess = createAction('[Issue PBD Dialog] Issue PBD Success', props<{ pbd: Pbd }>());

export const pbdIssueFailure = createAction('[Issue PBD Dialog] Issue PBD Failure', props<{ error: ServerError }>());
