import { createAction, props } from '@ngrx/store';

import { Activity, ActivitySaveModel, AgreementType, Pbd } from '~app/pbd/shared/pbd.model';
import { ServerError } from '~app/shared/errors';

export const requestPbd = createAction('[PBD/API] Request PBD', props<{ id: number }>());

export const requestPbdSuccess = createAction('[PBD/API] Request PBD Success', props<{ pbd: Pbd }>());

export const requestPbdFailure = createAction('[PBD/API] Request PBD Failure', props<{ error: ServerError }>());

export const savePbdActivity = createAction('[PBD/API] Save  PBD Activity', props<ActivitySaveModel>());

export const savePbdActivitySuccess = createAction(
  '[PBD/API] Save  PBD Activity Success',
  props<{ activity: Activity; agreementType: AgreementType }>()
);

export const savePbdActivityFailure = createAction(
  '[PBD/API] Save  PBD Activity Failure',
  props<{ error: ServerError }>()
);

export const downloadAttachment = createAction('[PBD/API] Download Attachment', props<{ attachment: any }>());

export const downloadAttachmentSuccess = createAction('[PBD/API] Download Attachment Success');

export const downloadAttachmentFailure = createAction('[PBD/API] Download Attachment Failure');
