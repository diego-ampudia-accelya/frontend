import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable, of, Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';

import { PbdIssueDialogActions } from '~app/pbd/actions';
import { PbdIssueFormComponent } from '~app/pbd/components/pbd-issue-form/pbd-issue-form.component';
import * as fromPbd from '~app/pbd/reducers';
import { Pbd } from '~app/pbd/shared/pbd.model';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-pbd-issue-dialog',
  templateUrl: './pbd-issue-dialog.component.html'
})
export class PbdIssueDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  private destroy$ = new Subject();

  private get form(): FormGroup {
    return this.pbdIssueFormComponent.form;
  }

  private loading$: Observable<boolean> = this.store.pipe(
    select(fromPbd.getPbdIssueLoading),
    tap(loading => this.setLoading(loading)),
    takeUntil(this.destroy$)
  );

  private issueBtnClick$ = this.reactiveSubject.asObservable.pipe(
    filter((res: any) => res.clickedBtn === FooterButton.Issue)
  );

  private issueBtn: ModalAction;

  public pbd$: Observable<Pbd> = this.store.pipe(select(fromPbd.getPbdIssueData));

  @ViewChild(PbdIssueFormComponent, { static: true }) pbdIssueFormComponent: PbdIssueFormComponent;

  constructor(public config: DialogConfig, public reactiveSubject: ReactiveSubject, public store: Store<AppState>) {}

  public disableIssueBtn(value: boolean) {
    this.issueBtn.isDisabled = value;
  }

  public ngOnInit() {
    this.issueBtn = this.config.data.buttons.find((button: ModalAction) => button.type === FooterButton.Issue);
    this.disableIssueBtn(true);

    this.loading$.subscribe();

    this.issueBtnClick$
      .pipe(
        switchMap(() => this.uploadFiles()),
        filter(Boolean),
        tap(() => this.issuePbd()),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private uploadFiles(): Observable<boolean> {
    return this.pbdIssueFormComponent.uploadComponent.hasFilesInPreviewMode
      ? this.pbdIssueFormComponent.uploadFiles()
      : of(true);
  }

  private issuePbd() {
    const { pbd, reason, fileIds } = this.form.value;
    this.store.dispatch(PbdIssueDialogActions.requestPbdIssue({ pbd, reason, fileIds }));
  }

  private setLoading(loading: boolean) {
    this.issueBtn.isLoading = loading;
  }

  public ngAfterViewInit() {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      this.disableIssueBtn(status !== 'VALID');
    });
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
