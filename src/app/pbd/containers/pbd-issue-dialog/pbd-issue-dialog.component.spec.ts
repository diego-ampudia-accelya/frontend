import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { PbdIssueDialogComponent } from './pbd-issue-dialog.component';
import { PbdIssueFormComponent } from '~app/pbd/components/pbd-issue-form/pbd-issue-form.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('PbdIssueDialogComponent', () => {
  let component: PbdIssueDialogComponent;
  let fixture: ComponentFixture<PbdIssueDialogComponent>;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Issue,
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: 'cancel'
          },
          {
            type: 'issue'
          }
        ]
      }
    } as DialogConfig;

    const initialState = {
      router: null,
      pbd: {
        pbdState: {
          pbd: null
        },
        pbdIssueState: {
          loading: false
        },
        pbdStartState: {
          loading: false,
          error: null
        },
        pbdActivityState: {
          loading: false
        },
        pbdDetailsState: {
          loading: false
        }
      }
    };

    TestBed.configureTestingModule({
      declarations: [PbdIssueDialogComponent, PbdIssueFormComponent, TranslatePipeMock],
      providers: [
        FormBuilder,
        ReactiveFormsModule,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject,
        provideMockStore({ initialState })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdIssueDialogComponent);
    component = fixture.componentInstance;
    component.pbdIssueFormComponent.createForm();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
