import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { PbdStartDialogComponent } from '~app/pbd/containers/pbd-start-dialog/pbd-start-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('PbdStartDialogComponent', () => {
  let component: PbdStartDialogComponent;
  let fixture: ComponentFixture<PbdStartDialogComponent>;

  beforeEach(waitForAsync(() => {
    const initialState = {
      router: null,
      pbd: {
        pbdState: {
          pbd: null
        },
        pbdIssueState: {
          loading: false
        },
        pbdStartState: {
          loading: false,
          documents: [],
          error: null
        },
        pbdActivityState: {
          loading: false
        },
        pbdDetailsState: {
          loading: false
        }
      }
    };

    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Start,
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Start
          }
        ]
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [PbdStartDialogComponent, TranslatePipeMock, DateTimeFormatPipe],
      providers: [
        FormBuilder,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject,
        provideMockStore({ initialState })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdStartDialogComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
