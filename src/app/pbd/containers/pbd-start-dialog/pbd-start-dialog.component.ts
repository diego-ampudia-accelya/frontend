import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { get } from 'lodash';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { PbdStartDialogActions } from '~app/pbd/actions';
import * as fromPbd from '~app/pbd/reducers';
import { TicketModel } from '~app/pbd/shared/pbd.model';
import { AppState } from '~app/reducers';
import { DialogConfig, DialogConfigData, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ROUTES } from '~app/shared/constants/routes';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-pbd-start-dialog',
  templateUrl: './pbd-start-dialog.component.html',
  styleUrls: ['./pbd-start-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PbdStartDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public infoMsg = '';
  public maxDocumentNumberLength = 10;
  public selectedDocument: TicketModel = null;
  public documents$ = this.store.pipe(
    select(fromPbd.getPbdStartDocuments),
    tap(documents => {
      if (documents && documents.length > 1) {
        this.pbdStartBtn.isDisabled = true;
      }
    })
  );

  private documentNumberRegex = new RegExp(`^(?=.*[1-9])[0-9]{10}$`);

  get dialogData(): DialogConfigData {
    return this.config.data;
  }

  private destroy$ = new Subject();
  private pbdStartBtn: ModalAction;

  constructor(
    public formBuilder: FormBuilder,
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    private store: Store<AppState>,
    private router: Router,
    private changeDetector: ChangeDetectorRef,
    private messageExtractorService: MessageExtractorService
  ) {}

  public ngOnInit(): void {
    this.createForm();

    this.initStartBtn();
    this.initFormValueChangesSubscription();
    this.initStartBtnClickSubscription();
    this.initPbdIssueDataSubscription();
    this.initErrorSubscription();
    this.initLoadingSubscription();
  }

  private createForm(): void {
    this.form = this.formBuilder.group(
      {
        documentNumber: ['', [Validators.pattern(this.documentNumberRegex), Validators.required]]
      },
      { updateOn: 'change' }
    );
  }

  private initStartBtn(): void {
    this.pbdStartBtn = this.dialogData.buttons.find(button => button.type === FooterButton.Start);
    this.pbdStartBtn.isDisabled = true;
  }

  private initFormValueChangesSubscription(): void {
    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      setTimeout(() => {
        this.pbdStartBtn.isDisabled = !this.form.valid;
      });

      this.store.dispatch(PbdStartDialogActions.documentNumberChange({ documentNumber: value }));
      this.selectedDocument = null;
    });
  }

  private initStartBtnClickSubscription(): void {
    this.reactiveSubject.asObservable
      .pipe(
        filter(action => action.clickedBtn === FooterButton.Start),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const { documentNumber } = this.form.value;

        const actionToDispatch = this.selectedDocument
          ? PbdStartDialogActions.selectDocument({ selectedDocument: this.selectedDocument })
          : PbdStartDialogActions.searchDocument({ documentNumber });

        this.store.dispatch(actionToDispatch);
      });
  }

  private initPbdIssueDataSubscription(): void {
    this.store.pipe(select(fromPbd.getPbdIssueData), filter(Boolean), takeUntil(this.destroy$)).subscribe(() => {
      this.router.navigate([ROUTES.POST_BILLING_DISPUTE_ISSUE.url]);
      this.config.serviceRef.close();
    });
  }

  private initErrorSubscription(): void {
    this.store.pipe(select(fromPbd.getPbdStartError), takeUntil(this.destroy$)).subscribe(error => {
      if (!error) {
        this.infoMsg = '';
      } else {
        const errorCode = get(error, 'error.errorCode');

        if (errorCode === 404) {
          this.infoMsg = 'postBillingDisputes.dialogs.pbdStart.form.msg.error.noResults';
        } else {
          this.infoMsg = this.messageExtractorService.extract(error);
        }

        this.pbdStartBtn.isDisabled = true;
      }
    });
  }

  private initLoadingSubscription(): void {
    this.store
      .pipe(select(fromPbd.getPbdStartLoading), takeUntil(this.destroy$))
      .subscribe(loading => this.setLoading(loading));
  }

  private setLoading(loading: boolean): void {
    this.pbdStartBtn.isLoading = loading;
  }

  public selectCard(document: TicketModel): void {
    this.selectedDocument = document;
    this.pbdStartBtn.isDisabled = false;
    this.changeDetector.detectChanges();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
