import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { PbdListComponent } from './pbd-list.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { PbdListActions } from '~app/pbd/actions';
import { PbdStaticDropdownsService } from '~app/pbd/services/pbd-static-dropdowns.service';
import { PbdService } from '~app/pbd/services/pbd.service';
import { PbdListTagFilterFormatter } from '~app/pbd/shared/pbd-list-tag-filter-formatter';
import { SEARCH_FORM_DEFAULT_VALUE_PBD } from '~app/pbd/shared/pbd-mocks.constants';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { ROUTES } from '~app/shared/constants/routes';
import { SortOrder } from '~app/shared/enums';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  AppConfigurationService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

const pbdServiceSpy = createSpyObject(PbdService);
const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
const translationServiceSpy = createSpyObject(L10nTranslationService);
const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
const pbdListTagFilterFormatterSpy = createSpyObject(PbdListTagFilterFormatter);
const dialogServiceSpy = createSpyObject(DialogService);
const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
const currencyDictionaryServiceSpy = createSpyObject(CurrencyDictionaryService);
const routerSpy = createSpyObject(Router);
const defaultQueryStorageSpy = createSpyObject(DefaultQueryStorage);
const localBspTimeServiceSpy = createSpyObject(LocalBspTimeService);
const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);

const activatedRouteStub = {
  snapshot: {
    data: {
      loggedUser: {
        userType: ''
      }
    }
  }
};

const expectedUserDetails: Partial<User> = {
  id: 10126,
  email: 'airline1@example.com',
  firstName: 'firstName',
  lastName: 'lastName',
  userType: UserType.AIRLINE,
  defaultIsoc: 'ES',
  permissions: [Permissions.readPbd, Permissions.readPendingPbds, Permissions.lean],
  bspPermissions: [
    { bspId: 1, permissions: [Permissions.readPbd, Permissions.readPendingPbds] },
    { bspId: 2, permissions: [] }
  ],
  bsps: [
    { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
    { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
  ]
};

const query = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 0 }
} as DataQuery;

const mockCurrencyDropdownOptions: DropdownOption<Currency>[] = [
  {
    value: { id: 1, code: 'EUR', decimals: 2 },
    label: 'EUR'
  },
  {
    value: { id: 1, code: 'USD', decimals: 2 },
    label: 'USD'
  }
];

const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
  {
    value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
    label: '001 / AIRLINE 001'
  },
  {
    value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
    label: '002 / AIRLINE 002'
  }
];

describe('PbdListComponent', () => {
  let component: PbdListComponent;
  let fixture: ComponentFixture<PbdListComponent>;
  let mockStore: MockStore;
  let pbdStaticDropdownsServiceSpy;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    },
    pbd: {
      pbdState: {
        pbd: null
      },
      pbdIssueState: {
        loading: false
      },
      pbdStartState: {
        loading: false,
        error: null
      },
      pbdActivityState: {},
      pbdDetailsState: {
        loading: false
      }
    }
  };

  const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
    {
      value: {
        id: '1',
        name: 'AGENT 1111111',
        code: '1111111',
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
      },
      label: '1111111 / AGENT 1111111'
    },
    {
      value: {
        id: '2',
        name: 'AGENT 2222222',
        code: '2222222',
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
      },
      label: '2222222 / AGENT 2222222'
    }
  ];

  currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of(mockCurrencyDropdownOptions));
  airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));
  agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));
  translationServiceSpy.translate.and.callFake(key => key);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PbdListComponent, TranslatePipeMock, HasPermissionPipe],
      imports: [HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        provideMockStore({ initialState }),
        FormBuilder,
        DecimalPipe,
        HasPermissionPipe,
        PermissionsService,
        PbdStaticDropdownsService,
        {
          provide: Router,
          useValue: routerSpy
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRouteStub
        },
        {
          provide: AirlineDictionaryService,
          useValue: airlineDictionaryServiceSpy
        },
        {
          provide: AgentDictionaryService,
          useValue: agentDictionaryServiceSpy
        },
        {
          provide: CurrencyDictionaryService,
          useValue: currencyDictionaryServiceSpy
        },
        {
          provide: DialogService,
          useValue: dialogServiceSpy
        },
        {
          provide: PbdService,
          useValue: pbdServiceSpy
        },
        {
          provide: LocalBspTimeService,
          useValue: localBspTimeServiceSpy
        },
        {
          provide: BspsDictionaryService,
          useValue: bspsDictionaryServiceSpy
        },
        provideMockStore({ initialState, selectors: [{ selector: getUser, value: expectedUserDetails }] }),
        mockProvider(AppConfigurationService),
        mockProvider(NotificationsHeaderService),
        mockProvider(NotificationService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(PbdListComponent, {
        set: {
          providers: [
            {
              provide: PbdListTagFilterFormatter,
              useValue: pbdListTagFilterFormatterSpy
            },
            {
              provide: DefaultQueryStorage,
              useValue: defaultQueryStorageSpy
            },
            {
              provide: PbdService,
              useValue: pbdServiceSpy
            },
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
      of([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }])
    );

    fixture = TestBed.createComponent(PbdListComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject<any>(Store);
    pbdStaticDropdownsServiceSpy = TestBed.inject(PbdStaticDropdownsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('should set/hide columns ', () => {
    it('for iata user', () => {
      const user = { ...expectedUserDetails, userType: UserType.IATA } as User;
      mockStore.overrideSelector(getUser, user);

      const pbdColumnsExpected = [
        jasmine.objectContaining({
          prop: 'agent.bsp.isoCountryCode',
          name: 'LIST.postBillingDispute.columns.bsp',
          frozenLeft: true,
          resizeable: true,
          width: 75
        }),
        jasmine.objectContaining({
          prop: 'documentNumber',
          name: 'LIST.postBillingDispute.columns.documentNumber',
          cellTemplate: jasmine.anything(),
          width: 150
        }),
        jasmine.objectContaining({
          prop: 'transactionCode',
          name: 'LIST.postBillingDispute.columns.transactionCode',
          width: 80
        }),
        jasmine.objectContaining({
          prop: 'airline.iataCode',
          name: 'LIST.postBillingDispute.columns.airlineCode',
          width: 90,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'airline.localName',
          name: 'LIST.postBillingDispute.columns.airlineName',
          width: 200,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'agent.iataCode',
          name: 'LIST.postBillingDispute.columns.agentCode',
          width: 90,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'agent.name',
          name: 'LIST.postBillingDispute.columns.agentName',
          width: 200,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'translatedPbdStatus',
          name: 'LIST.postBillingDispute.columns.status',
          width: 250
        }),
        jasmine.objectContaining({
          prop: 'pbdDate',
          name: 'LIST.postBillingDispute.columns.pbdDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'resolutionDate',
          name: 'LIST.postBillingDispute.columns.resolutionDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'currency.code',
          name: 'LIST.postBillingDispute.columns.currency',
          width: 100
        }),
        jasmine.objectContaining({
          prop: 'maximumDisputableAmount',
          name: 'LIST.postBillingDispute.columns.maxDisputableAmount',
          cellClass: 'text-right'
        }),
        jasmine.objectContaining({
          prop: 'pbdAmount',
          name: 'LIST.postBillingDispute.columns.pbdAmount',
          cellClass: 'text-right'
        })
      ];

      component.ngOnInit();
      component['setColumns']();

      expect(component.columns).toEqual(pbdColumnsExpected);
    });

    it('for agent user', () => {
      const userAgent = { ...expectedUserDetails, userType: UserType.AGENT } as User;
      mockStore.overrideSelector(getUser, userAgent);

      const pbdColumnsExpected = [
        jasmine.objectContaining({
          prop: 'agent.bsp.isoCountryCode',
          name: 'LIST.postBillingDispute.columns.bsp',
          frozenLeft: true,
          resizeable: true,
          width: 75
        }),
        jasmine.objectContaining({
          prop: 'documentNumber',
          name: 'LIST.postBillingDispute.columns.documentNumber',
          cellTemplate: jasmine.anything(),
          width: 150
        }),
        jasmine.objectContaining({
          prop: 'transactionCode',
          name: 'LIST.postBillingDispute.columns.transactionCode',
          width: 80
        }),
        jasmine.objectContaining({
          prop: 'airline.iataCode',
          name: 'LIST.postBillingDispute.columns.airlineCode',
          width: 90,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'airline.localName',
          name: 'LIST.postBillingDispute.columns.airlineName',
          width: 200,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'agent.iataCode',
          name: 'LIST.postBillingDispute.columns.agentCode',
          width: 90,
          hidden: true
        }),
        jasmine.objectContaining({
          prop: 'agent.name',
          name: 'LIST.postBillingDispute.columns.agentName',
          width: 200,
          hidden: true
        }),
        jasmine.objectContaining({
          prop: 'translatedPbdStatus',
          name: 'LIST.postBillingDispute.columns.status',
          width: 250
        }),
        jasmine.objectContaining({
          prop: 'pbdDate',
          name: 'LIST.postBillingDispute.columns.pbdDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'resolutionDate',
          name: 'LIST.postBillingDispute.columns.resolutionDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'currency.code',
          name: 'LIST.postBillingDispute.columns.currency',
          width: 100
        }),
        jasmine.objectContaining({
          prop: 'maximumDisputableAmount',
          name: 'LIST.postBillingDispute.columns.maxDisputableAmount',
          cellClass: 'text-right'
        }),
        jasmine.objectContaining({
          prop: 'pbdAmount',
          name: 'LIST.postBillingDispute.columns.pbdAmount',
          cellClass: 'text-right'
        })
      ];

      component.ngOnInit();
      component['setColumns']();

      expect(component.columns).toEqual(pbdColumnsExpected);
    });

    it('for agent group user', () => {
      const userAgent = { ...expectedUserDetails, userType: UserType.AGENT_GROUP } as User;
      mockStore.overrideSelector(getUser, userAgent);

      const pbdColumnsExpected = [
        jasmine.objectContaining({
          prop: 'agent.bsp.isoCountryCode',
          name: 'LIST.postBillingDispute.columns.bsp',
          frozenLeft: true,
          resizeable: true,
          width: 75
        }),
        jasmine.objectContaining({
          prop: 'documentNumber',
          name: 'LIST.postBillingDispute.columns.documentNumber',
          cellTemplate: jasmine.anything(),
          width: 150
        }),
        jasmine.objectContaining({
          prop: 'transactionCode',
          name: 'LIST.postBillingDispute.columns.transactionCode',
          width: 80
        }),
        jasmine.objectContaining({
          prop: 'airline.iataCode',
          name: 'LIST.postBillingDispute.columns.airlineCode',
          width: 90,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'airline.localName',
          name: 'LIST.postBillingDispute.columns.airlineName',
          width: 200,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'agent.iataCode',
          name: 'LIST.postBillingDispute.columns.agentCode',
          width: 90,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'agent.name',
          name: 'LIST.postBillingDispute.columns.agentName',
          width: 200,
          hidden: true
        }),
        jasmine.objectContaining({
          prop: 'translatedPbdStatus',
          name: 'LIST.postBillingDispute.columns.status',
          width: 250
        }),
        jasmine.objectContaining({
          prop: 'pbdDate',
          name: 'LIST.postBillingDispute.columns.pbdDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'resolutionDate',
          name: 'LIST.postBillingDispute.columns.resolutionDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'currency.code',
          name: 'LIST.postBillingDispute.columns.currency',
          width: 100
        }),
        jasmine.objectContaining({
          prop: 'maximumDisputableAmount',
          name: 'LIST.postBillingDispute.columns.maxDisputableAmount',
          cellClass: 'text-right'
        }),
        jasmine.objectContaining({
          prop: 'pbdAmount',
          name: 'LIST.postBillingDispute.columns.pbdAmount',
          cellClass: 'text-right'
        })
      ];

      component.ngOnInit();
      component['setColumns']();

      expect(component.columns).toEqual(pbdColumnsExpected);
    });

    it('for airline user', () => {
      const userAgent = { ...expectedUserDetails, userType: UserType.AIRLINE } as User;
      mockStore.overrideSelector(getUser, userAgent);

      const pbdColumnsExpected = [
        jasmine.objectContaining({
          prop: 'agent.bsp.isoCountryCode',
          name: 'LIST.postBillingDispute.columns.bsp',
          frozenLeft: true,
          resizeable: true,
          width: 75
        }),
        jasmine.objectContaining({
          prop: 'documentNumber',
          name: 'LIST.postBillingDispute.columns.documentNumber',
          cellTemplate: jasmine.anything(),
          width: 150
        }),
        jasmine.objectContaining({
          prop: 'transactionCode',
          name: 'LIST.postBillingDispute.columns.transactionCode',
          width: 80
        }),
        jasmine.objectContaining({
          prop: 'airline.iataCode',
          name: 'LIST.postBillingDispute.columns.airlineCode',
          width: 90,
          hidden: true
        }),
        jasmine.objectContaining({
          prop: 'airline.localName',
          name: 'LIST.postBillingDispute.columns.airlineName',
          width: 200,
          hidden: true
        }),
        jasmine.objectContaining({
          prop: 'agent.iataCode',
          name: 'LIST.postBillingDispute.columns.agentCode',
          width: 90,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'agent.name',
          name: 'LIST.postBillingDispute.columns.agentName',
          width: 200,
          hidden: false
        }),
        jasmine.objectContaining({
          prop: 'translatedPbdStatus',
          name: 'LIST.postBillingDispute.columns.status',
          width: 250
        }),
        jasmine.objectContaining({
          prop: 'pbdDate',
          name: 'LIST.postBillingDispute.columns.pbdDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'resolutionDate',
          name: 'LIST.postBillingDispute.columns.resolutionDate',
          cellTemplate: jasmine.anything(),
          width: 110
        }),
        jasmine.objectContaining({
          prop: 'currency.code',
          name: 'LIST.postBillingDispute.columns.currency',
          width: 100
        }),
        jasmine.objectContaining({
          prop: 'maximumDisputableAmount',
          name: 'LIST.postBillingDispute.columns.maxDisputableAmount',
          cellClass: 'text-right'
        }),
        jasmine.objectContaining({
          prop: 'pbdAmount',
          name: 'LIST.postBillingDispute.columns.pbdAmount',
          cellClass: 'text-right'
        })
      ];

      component.ngOnInit();
      component['setColumns']();

      expect(component.columns).toEqual(pbdColumnsExpected);
    });
  });

  it('should navigate to pbd details on row click', () => {
    component.onRowLinkClick(5);

    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith(ROUTES.POST_BILLING_DISPUTE_DETAILS.url + 5);
  });

  it('should set custom label on ngOnInit', () => {
    component.customLabels = null;

    component.ngOnInit();

    expect(component.customLabels).toEqual({ create: 'LIST.postBillingDispute.buttons.create' });
  });

  it('should check it user is of specific type', () => {
    component.userType = UserType.AGENT;

    expect(component.userIsOfType(UserType.AGENT)).toBe(true);
  });

  it('should dispatch startPbd on Create if userType is Agent', () => {
    spyOn(mockStore, 'dispatch');
    component.userType = UserType.AGENT;

    component.create();

    expect(mockStore.dispatch).toHaveBeenCalledWith(PbdListActions.startPbd());
  });

  it('should dispatch startPbd on create if userType is Agent Group', () => {
    spyOn(mockStore, 'dispatch');
    component.userType = UserType.AGENT_GROUP;

    component.create();

    expect(mockStore.dispatch).toHaveBeenCalledWith(PbdListActions.startPbd());
  });

  it('should call upload method on create if userType is Airline', () => {
    spyOn(component, 'upload');

    component.userType = UserType.AIRLINE;

    component.create();

    expect(component.upload).toHaveBeenCalled();
  });

  it('should set default sorting if there is no any sorted column', () => {
    component.loadData();

    const expectedQuery = {
      ...query,
      filterBy: {
        ...query.filterBy,
        bsp: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }]
      },
      sortBy: [
        {
          attribute: 'resolutionDate',
          sortType: SortOrder.Desc
        }
      ]
    };

    expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
  });

  it('should use query passed as param not the default one', () => {
    const newQuery = {
      ...query,
      filterBy: {
        documentNumber: '000'
      }
    };

    const expectedQuery = {
      ...newQuery,
      filterBy: {
        ...newQuery.filterBy,
        bsp: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }]
      },
      sortBy: [
        {
          attribute: 'resolutionDate',
          sortType: SortOrder.Desc
        }
      ]
    };

    component.loadData(newQuery);

    expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
  });

  it('should initialize Permissions', () => {
    component.ngOnInit();

    expect(component['hasLeanPermission']).toBeTruthy();
    expect(component['hasPbdReadPendingsPermission']).toBeTruthy();
    expect(component['hasPbdReadQueryPermission']).toBeTruthy();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should setYearsBack when user has LEAN permission', () => {
    component['setYearsBack']();

    expect(localBspTimeServiceSpy.getDate).toHaveBeenCalled();
  });

  it('should initialize search form', () => {
    expect(component['filterForm'].value).toEqual(SEARCH_FORM_DEFAULT_VALUE_PBD);
  });

  it('should open download dialog correctly on download', () => {
    defaultQueryStorageSpy.get.and.returnValue(query);
    component.download();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should set Pending status when user hasPbdReadPendingsPermission', () => {
    component['hasPbdReadQueryPermission'] = false;
    const methodSpy = spyOn(pbdStaticDropdownsServiceSpy, 'getPendingStatusesDropdownOptions').and.callThrough();

    component['initializeStatusFilterDropdown']();

    expect(methodSpy).toHaveBeenCalled();
  });

  it('should set Pending status when user hasPbdReadQueryPermission', () => {
    component['hasPbdReadPendingsPermission'] = false;

    const methodSpy = spyOn(pbdStaticDropdownsServiceSpy, 'getFinalizedStatusesDropdownOptions').and.callThrough();

    component['initializeStatusFilterDropdown']();

    expect(methodSpy).toHaveBeenCalled();
  });

  describe('populateFilterDropdowns', () => {
    it('should populateFilterDropdowns when user is an AIRLINE', () => {
      const updateAgentFilterListSpy = spyOn<any>(component, 'updateAgentFilterList').and.callThrough();

      component['populateFilterDropdowns']();

      expect(updateAgentFilterListSpy).toHaveBeenCalled();
    });

    it('should populateFilterDropdowns when user is an AGENT', () => {
      const userAgent = {
        ...expectedUserDetails,
        userType: UserType.AGENT
      } as User;

      mockStore.overrideSelector(getUser, userAgent);

      const initializeAirlineFilterDropdownSpy = spyOn<any>(
        component,
        'initializeAirlineFilterDropdown'
      ).and.callThrough();

      component.ngOnInit();
      component['populateFilterDropdowns']();

      expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    });

    it('should populateFilterDropdowns when user is an AGENT_GROUP', () => {
      const userAgent = {
        ...expectedUserDetails,
        userType: UserType.AGENT_GROUP
      } as User;

      mockStore.overrideSelector(getUser, userAgent);

      const initializeAgentGroupFilterDropdownSpy = spyOn<any>(
        component,
        'initializeAirlineFilterDropdown'
      ).and.callThrough();

      component.ngOnInit();
      component['populateFilterDropdowns']();

      expect(initializeAgentGroupFilterDropdownSpy).toHaveBeenCalled();
    });

    it('should populateFilterDropdowns when user is an IATA', () => {
      const userAgent = {
        ...expectedUserDetails,
        userType: UserType.IATA
      } as User;

      mockStore.overrideSelector(getUser, userAgent);

      const initializeAirlineFilterDropdownSpy = spyOn<any>(
        component,
        'initializeAirlineFilterDropdown'
      ).and.callThrough();

      component.ngOnInit();
      component['populateFilterDropdowns']();

      expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    });
  });

  it('should initialize BSP listener if user has LEAN permission', fakeAsync(() => {
    const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };
    spyOn<any>(component, 'updateCurrencyList');
    spyOn<any>(component, 'updateAgentList');
    component.ngOnInit();
    component.filterForm.get('bsp').patchValue(mockBsp);
    tick();

    expect(component.bspSelectedIds).toEqual([2]);
  }));

  it('should update Agents filters if user has LEAN permission', () => {
    const updateAgentFilterListSpy = spyOn<any>(component, 'updateAgentFilterList').and.callThrough();
    const updateAgentListControlValueSpy = spyOn<any>(component, 'updateAgentListControlValue').and.callThrough();

    component['updateAgentList']();

    expect(updateAgentFilterListSpy).toHaveBeenCalled();
    expect(updateAgentListControlValueSpy).toHaveBeenCalled();
  });

  it('should update Currency if user has LEAN permission', () => {
    const updateCurrencyListControlValueSpy = spyOn<any>(component, 'updateCurrencyListControlValue').and.callThrough();
    spyOn<any>(component, 'updateCurrencyFilterList$').and.returnValue(of(mockCurrencyDropdownOptions));
    component.currenciesOptions$ = of([]);
    component.currenciesOptions = [];
    component.bspSelectedIds = [1];

    component['updateCurrencyList']();
    let result;
    component.currenciesOptions$.subscribe(data => (result = data));

    expect(result).toEqual(mockCurrencyDropdownOptions);
    expect(updateCurrencyListControlValueSpy).toHaveBeenCalled();
  });

  it('should updateCurrencyFilterList$ if it has bsp selected', () => {
    component.currenciesOptions$ = of([]);
    component.currenciesOptions = [];
    component.bspSelectedIds = [1];

    component['updateCurrencyList']();

    expect(currencyDictionaryServiceSpy.getFilteredDropdownOptions).toHaveBeenCalledWith({ bspId: [1] });
  });

  it('should initializeLeanBspFilter$', fakeAsync(() => {
    component.ngOnInit();
    tick();

    expect(component.bspSelectedIds).toEqual([1]);
    expect(component.predefinedFilters).toEqual({
      bsp: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }]
    });
    expect(component.isBspFilterLocked).toBeTruthy();
    expect(component.bspCountriesList).toEqual([
      { value: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' }, label: 'ES - SPAIN' }
    ]);
  }));

  it('should initializeAirlineFilterDropdown', () => {
    component.airlineOptions$ = of([]);

    component['initializeAirlineFilterDropdown']();
    let result;

    component.airlineOptions$.subscribe(data => (result = data));

    expect(result).toEqual(mockAirlineDropdownOptions);
  });

  it('should updateAgentFilterList if it has bsp selected', () => {
    component['updateAgentFilterList']();

    expect(currencyDictionaryServiceSpy.getFilteredDropdownOptions).toHaveBeenCalledWith({ bspId: [1] });
  });

  it('should updateAgentListControlValue if it has bsp selected', () => {
    const agentSelected = {
      id: '2',
      name: 'AGENT 2222222',
      code: '2222222',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
    };
    component['agentListControl'].patchValue(agentSelected);

    component['updateAgentListControlValue']();

    expect(component['agentListControl'].value).toEqual(agentSelected);
  });

  it('should updateCurrencyListControlValue if it has bsp selected', () => {
    const agentSelected = { id: 1, code: 'USD', decimals: 2 };
    component['agentListControl'].patchValue(agentSelected);

    component['updateCurrencyListControlValue']();

    expect(component['agentListControl'].value).toEqual(agentSelected);
  });

  it('should initializeAgentGroupFilterDropdown', () => {
    component.agentOptions$ = of([]);
    component['hasPbdReadQueryPermission'] = true;

    component['initializeAgentGroupFilterDropdown']();
    let result;

    component.agentOptions$.subscribe(data => (result = data));

    expect(result).toEqual(mockAgentDropdownOptions);
  });
});
