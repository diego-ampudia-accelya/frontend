/* eslint-disable @typescript-eslint/naming-convention */
import { DecimalPipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEmpty, mapValues } from 'lodash';
import moment from 'moment-mini';
import { Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { PbdListActions } from '~app/pbd/actions';
import { PbdStaticDropdownsService } from '~app/pbd/services/pbd-static-dropdowns.service';
import { PbdService } from '~app/pbd/services/pbd.service';
import { PbdListTagFilterFormatter } from '~app/pbd/shared/pbd-list-tag-filter-formatter';
import { SEARCH_FORM_DEFAULT_VALUE_PBD } from '~app/pbd/shared/pbd-mocks.constants';
import { Pbd, PbdSearchForm } from '~app/pbd/shared/pbd.model';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { SortOrder } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { toValueLabelObjectBsp } from '~app/shared/helpers/filters.helper';
import { AgentDictionaryQueryFilters, AgentSummary, CurrencyDictionaryQueryFilters } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { WatcherDocType, WatcherModel } from '~app/shared/models/watcher.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-pbd-list',
  templateUrl: './pbd-list.component.html',
  styleUrls: ['./pbd-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    PbdListTagFilterFormatter,
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [PbdService]
    }
  ]
})
export class PbdListComponent implements OnInit, OnDestroy {
  public columns = [];
  public filterForm: FormGroup;
  public predefinedFilters: PbdSearchForm;
  public userType: UserType;
  public loggedUser: User;
  public customLabels: { [key: string]: any };
  public listActions: Array<{ action: GridTableActionType; disabled?: boolean; group?: string }>;

  public statusOptions: DropdownOption[] = [];
  public airlineOptions$: Observable<DropdownOption[]>;
  public agentOptions$: Observable<DropdownOption[]>;
  public trncOptions: DropdownOption[] = [];
  public bspCountriesList: DropdownOption[] = [];
  public currenciesOptions$: Observable<DropdownOption<Currency>[]>;
  public currenciesOptions: DropdownOption<Currency>[];

  public Permissions = Permissions;

  public maxDate = this.localBspTimeService.getDate();
  public minDate: Date;
  public maxResolutionDate = this.localBspTimeService.getDate(moment().add(1, 'year').toDate());

  public isBspFilterLocked: boolean;

  private hasPbdReadQueryPermission = false;
  private hasPbdReadPendingsPermission = false;
  private hasLeanPermission = false;

  private bspIdControl: AbstractControl;
  private agentListControl: AbstractControl;
  private currencyControl: AbstractControl;

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;

  public bspSelectedIds: number[] = [];

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<Pbd>,
    public pbdListTagFilterFormatter: PbdListTagFilterFormatter,
    public dialogService: DialogService,
    public translationService: L10nTranslationService,
    public activatedRoute: ActivatedRoute,
    protected decimalPipe: DecimalPipe,
    private queryStorage: DefaultQueryStorage,
    private formBuilder: FormBuilder,
    private pbdService: PbdService,
    private pbdStaticDropdownsService: PbdStaticDropdownsService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private router: Router,
    private permissionsService: PermissionsService,
    private airlineDictionaryService: AirlineDictionaryService,
    private agentDictionaryService: AgentDictionaryService,
    private store: Store<AppState>,
    private localBspTimeService: LocalBspTimeService,
    private bspsDictionaryService: BspsDictionaryService,
    private notificationsHeaderService: NotificationsHeaderService,
    private notificationService: NotificationService
  ) {}

  public ngOnInit(): void {
    this.initSearchForm();

    this.initializeLoggedUser$()
      .pipe(
        tap(() => this.initializePermissions()),
        switchMap(() => this.initializeLeanBspFilter$()),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.setResolversData();
        this.setYearsBack();
        this.setColumns();
        this.populateFilterDropdowns();
      });

    this.customLabels = { create: 'LIST.postBillingDispute.buttons.create' };

    if (this.hasLeanPermission) {
      this.initializeBspCountrieslistener();
    }
  }

  public setResolversData(): void {
    const storedQuery = this.queryStorage.get();
    this.loadData(storedQuery);
  }

  public initSearchForm(): void {
    this.filterForm = this.formBuilder.group(mapValues(SEARCH_FORM_DEFAULT_VALUE_PBD, value => [value]));

    this.bspIdControl = FormUtil.get<PbdSearchForm>(this.filterForm, 'bsp');
    this.agentListControl = FormUtil.get<PbdSearchForm>(this.filterForm, 'agentCodes');
    this.currencyControl = FormUtil.get<PbdSearchForm>(this.filterForm, 'currencyCode');
  }

  public loadData(query?: DataQuery<Pbd>): void {
    query = query || cloneDeep(defaultQuery);

    if (isEmpty(query.sortBy)) {
      const sortByKey: keyof Pbd = 'resolutionDate';
      const sortBy = [{ attribute: sortByKey, sortType: SortOrder.Desc }];
      query = { ...query, sortBy };
    }

    const dataQuery: Partial<DataQuery> = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public download(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('BUTTON.DEFAULT.DOWNLOAD'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.pbdService
    });
  }

  public userIsOfType(type: string): boolean {
    return type && this.userType && type.toUpperCase() === this.userType.toUpperCase();
  }

  public create(): void {
    switch (this.userType) {
      case UserType.AGENT:
      case UserType.AGENT_GROUP:
        this.store.dispatch(PbdListActions.startPbd());
        break;

      case UserType.AIRLINE:
        this.upload();
        break;
    }
  }

  public upload(): void {
    throw new Error('Upload() not implemented yet');
  }

  public onRowLinkClick(id: number): void {
    this.router.navigateByUrl(ROUTES.POST_BILLING_DISPUTE_DETAILS.url + id);
  }

  public onGetActionList(pbd: Pbd): void {
    const watchingAction = pbd.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;
    this.listActions = [{ action: watchingAction }];
  }

  public onActionClick({ action, row }): void {
    const documentId = (row as Pbd).id;
    const bspId = (row as Pbd).bsp.id;

    switch (action.actionType) {
      case GridTableActionType.StartWatching:
      case GridTableActionType.StopWatching:
        this.notificationsHeaderService
          .updateWatcher(
            {
              documentId,
              documentType: WatcherDocType.PBD,
              watching: action.actionType === GridTableActionType.StartWatching
            },
            bspId
          )
          .subscribe(response => this.onUpdateWatcherSuccess(response));
        break;
    }
  }

  private initializePermissions(): void {
    this.hasPbdReadQueryPermission = this.permissionsService.hasPermission(this.Permissions.readPbd);
    this.hasPbdReadPendingsPermission = this.permissionsService.hasPermission(this.Permissions.readPendingPbds);
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeLoggedUser$(): Observable<any> {
    return this.store.pipe(select(getUser), first()).pipe(
      tap(user => {
        this.loggedUser = user;
        this.userType = user.userType;
      })
    );
  }

  private setYearsBack(): void {
    const years = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;

    this.minDate = this.localBspTimeService.getDate(moment().subtract(years, 'years').toDate());
  }

  private onUpdateWatcherSuccess(data: WatcherModel): void {
    let translationKey;
    const watching = data.watching;

    this.loadData();

    if (watching) {
      translationKey = 'singleStartWatchingSuccess';
    } else {
      translationKey = 'singleStopWatchingSuccess';
    }

    const message = this.translationService.translate(translationKey);

    this.notificationService.showSuccess(message);
  }

  private initializeStatusFilterDropdown(): void {
    if (this.hasPbdReadPendingsPermission) {
      this.statusOptions = [
        ...this.statusOptions,
        ...this.pbdStaticDropdownsService.getPendingStatusesDropdownOptions()
      ];
    }

    if (this.hasPbdReadQueryPermission) {
      this.statusOptions = [
        ...this.statusOptions,
        ...this.pbdStaticDropdownsService.getFinalizedStatusesDropdownOptions()
      ];
    }
  }

  private setColumns(): void {
    const translationPrefix = 'LIST.postBillingDispute.columns';

    this.columns = [
      {
        prop: 'agent.bsp.isoCountryCode',
        name: `${translationPrefix}.bsp`,
        frozenLeft: true,
        resizeable: true,
        width: 75
      },
      {
        prop: 'documentNumber',
        name: `${translationPrefix}.documentNumber`,
        cellTemplate: 'commonLinkCellTmpl',
        width: 150,
        icon: (pbd: Pbd) => ({
          class: 'material-icons-outlined',
          name: 'remove_red_eye',
          tooltip: this.translationService.translate(`watching`),
          hidden: !pbd.watching
        })
      },
      {
        prop: 'transactionCode',
        name: `${translationPrefix}.transactionCode`,
        width: 80
      },
      {
        prop: 'airline.iataCode',
        name: `${translationPrefix}.airlineCode`,
        width: 90,
        hidden: this.userType === UserType.AIRLINE
      },
      {
        prop: 'airline.localName',
        name: `${translationPrefix}.airlineName`,
        width: 200,
        hidden: this.userType === UserType.AIRLINE
      },
      {
        prop: 'agent.iataCode',
        name: `${translationPrefix}.agentCode`,
        width: 90,
        hidden: this.userType === UserType.AGENT
      },
      {
        prop: 'agent.name',
        name: `${translationPrefix}.agentName`,
        width: 200,
        hidden: this.userType === UserType.AGENT || this.userType === UserType.AGENT_GROUP
      },
      {
        prop: 'translatedPbdStatus',
        name: `${translationPrefix}.status`,
        width: 250
      },
      {
        prop: 'pbdDate',
        name: `${translationPrefix}.pbdDate`,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 110
      },
      {
        prop: 'resolutionDate',
        name: `${translationPrefix}.resolutionDate`,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 110
      },
      {
        prop: 'currency.code',
        name: `${translationPrefix}.currency`,
        width: 100
      },
      {
        prop: 'maximumDisputableAmount',
        name: `${translationPrefix}.maxDisputableAmount`,
        cellClass: 'text-right'
      },
      {
        prop: 'pbdAmount',
        name: `${translationPrefix}.pbdAmount`,
        cellClass: 'text-right'
      }
    ];
  }

  private populateFilterDropdowns(): void {
    switch (this.userType) {
      case UserType.AGENT:
        this.initializeAirlineFilterDropdown();
        break;
      case UserType.AIRLINE:
        this.updateAgentFilterList();
        break;
      case UserType.AGENT_GROUP:
        this.initializeAgentGroupFilterDropdown();
        this.initializeAirlineFilterDropdown();
        break;
      default:
        this.initializeAirlineFilterDropdown();
        this.updateAgentFilterList();
    }

    this.currenciesOptions$ = this.updateCurrencyFilterList$();

    this.initializeStatusFilterDropdown();

    this.trncOptions = this.pbdStaticDropdownsService.getTransactionCodesDropdownOptions();
  }

  private initializeBspCountrieslistener(): void {
    this.bspIdControl.valueChanges
      .pipe(
        tap(bsps => this.updateBspSelectedIds(bsps)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.updateAgentList();
        this.updateCurrencyList();
      });
  }

  private updateBspSelectedIds(bsps: BspDto): void {
    if (bsps) {
      this.bspSelectedIds = Array.isArray(bsps) ? bsps.map(bsp => bsp.id) : [bsps.id];
    } else {
      this.bspSelectedIds = [];
    }
  }

  private updateAgentList(): void {
    if (this.hasLeanPermission) {
      this.updateAgentFilterList();
      this.updateAgentListControlValue();
    }
  }

  private updateCurrencyList(): void {
    if (this.hasLeanPermission) {
      this.currenciesOptions$ = this.updateCurrencyFilterList$().pipe(
        tap(currencies => (this.currenciesOptions = currencies)),
        tap(() => this.updateCurrencyListControlValue())
      );
    }
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, [this.Permissions.readPbd])
      .pipe(
        tap(bspList => {
          // Update BSP selected ids and predefined filters with the default BSP
          const onlyOneBsp = bspList.length === 1;

          if (bspList.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);
            const firstListBsp = bspList[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelectedIds = [filterValue?.id];

            this.predefinedFilters = {
              ...this.predefinedFilters,
              bsp: [filterValue]
            };
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspOptions => (this.isBspFilterLocked = bspOptions.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  private initializeAirlineFilterDropdown(): void {
    this.airlineOptions$ = this.airlineDictionaryService.getDropdownOptions();
  }

  private updateAgentFilterList(): void {
    let param: AgentDictionaryQueryFilters;

    if (this.bspSelectedIds?.length) {
      param = { bspId: this.bspSelectedIds };
    }

    this.agentOptions$ = this.agentDictionaryService.getDropdownOptions(param);
  }

  private updateAgentListControlValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.bspSelectedIds.length) {
      return;
    }

    const agentsSelected: AgentSummary[] = this.agentListControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.bspSelectedIds.some(id => agent.bsp?.id === id));
      this.agentListControl.patchValue(agentsToPatch);
    }
  }

  private updateCurrencyFilterList$(): Observable<DropdownOption<Currency>[]> {
    let param: CurrencyDictionaryQueryFilters;

    if (this.bspSelectedIds?.length) {
      param = { bspId: this.bspSelectedIds };
    }

    return this.currencyDictionaryService.getFilteredDropdownOptions(param);
  }

  private updateCurrencyListControlValue(): void {
    // If there are not selected BSPs, dropdown has all currencies from all BSPs so there is no reason to patch currency control value
    if (!this.bspSelectedIds.length) {
      return;
    }

    const currenciesSelected: Currency[] = this.currencyControl.value;

    if (currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currenciesOptions.some(({ value }) => value.id === currency.id)
      );
      this.currencyControl.patchValue(currenciesToPatch);
    }
  }

  private initializeAgentGroupFilterDropdown(): void {
    this.agentOptions$ = this.hasPbdReadQueryPermission
      ? this.agentDictionaryService.getDropdownOptions({ permission: this.Permissions.readPbd })
      : of(null);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
