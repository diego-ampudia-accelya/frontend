import { ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter, map, shareReplay, tap } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { PbdApiActions, PbdDetailsActions } from '~app/pbd/actions';
import { ActivityCommentFormComponent } from '~app/pbd/components/activity-comment-form/activity-comment-form.component';
import { getPbd, getPbdDetailsStateLoading } from '~app/pbd/reducers';
import { PbdService } from '~app/pbd/services/pbd.service';
import { ActivityFilterType, ActivityType, AgreementType, Pbd, PbdStatus } from '~app/pbd/shared/pbd.model';
import { AppState } from '~app/reducers';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { Permissions } from '~app/shared/constants/permissions';
import { UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-pbd-details',
  templateUrl: './pbd-details.component.html',
  styleUrls: ['./pbd-details.component.scss']
})
export class PbdDetailsComponent implements OnInit, OnDestroy {
  public pbd$: Observable<Pbd>;
  public loggedUserType$: Observable<UserType>;
  public isIssueRoute = false;
  public isFormValid: boolean;
  public activityFilterValue = ActivityFilterType.All;
  public userTypes = UserType;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public Permissions = Permissions;
  public headerHeight: number;

  @ViewChild('commentForm') commentForm: ActivityCommentFormComponent;
  @ViewChild(AccordionComponent) accordion: AccordionComponent;
  @ViewChildren(AccordionItemComponent) accordionSections: QueryList<AccordionItemComponent>;

  public loading$: Observable<boolean> = this.store.pipe(
    select(getPbdDetailsStateLoading),
    tap(loading => {
      if (!loading && this.commentForm) {
        this.commentForm.resetForm();
      }
    })
  );

  private activatedRouteParamsSubscription: Subscription;

  constructor(
    public activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private cd: ChangeDetectorRef,
    private pbdService: PbdService,
    protected permissionsService: PermissionsService
  ) {}

  public ngOnInit() {
    this.activatedRouteParamsSubscription = this.activatedRoute.params.subscribe(params => {
      const { id } = params;
      this.pbd$ = this.store.pipe(select(getPbd, { id }), filter(Boolean), shareReplay(1)) as Observable<Pbd>;
      const hasCreatePermission = this.permissionsService.hasPermission(Permissions.pbdStart);
      this.isIssueRoute = !id && hasCreatePermission;
      this.getLoggedUserType();
    });
  }

  public isCommentingEnabled({ status }): boolean {
    return status === PbdStatus.PendingAgent || status === PbdStatus.PendingAirline;
  }

  public getLoggedUserType() {
    this.loggedUserType$ = this.activatedRoute.data.pipe(map(data => data.loggedUserType));
  }

  public getFilteredActivities(pbd: Pbd) {
    return this.pbdService.getFilteredActivities(pbd.activities, this.activityFilterValue);
  }

  public onAmountChange(pbdAmount: string) {
    this.store.dispatch(PbdDetailsActions.addPbdAmountData({ pbdAmount }));
  }

  public setFormValidity(isValid: boolean) {
    this.isFormValid = isValid;
    this.cd.detectChanges();
  }

  public onIssue() {
    this.store.dispatch(PbdDetailsActions.pbdIssue());
  }

  public onAgreementChange(event: { pbd: Pbd; agreementType: AgreementType }) {
    this.store.dispatch(PbdDetailsActions.changePbdAgreement(event));
  }

  public onFilterChange(event: ActivityFilterType) {
    this.activityFilterValue = event;
  }

  public onCommentSend(event: { comment: string; pbd: Pbd; fileIds: string[] }) {
    this.store.dispatch(
      PbdApiActions.savePbdActivity({
        pbdId: event.pbd.id,
        reason: event.comment,
        activityType: ActivityType.Commented,
        attachments: event.fileIds
      })
    );
  }

  public onDownload(attachment) {
    this.store.dispatch(
      PbdApiActions.downloadAttachment({
        attachment
      })
    );
  }

  public onHeaderHeightChange(height: number) {
    this.headerHeight = height;
    this.cd.detectChanges();
  }

  public ngOnDestroy() {
    if (this.activatedRouteParamsSubscription) {
      this.activatedRouteParamsSubscription.unsubscribe();
    }
  }
}
