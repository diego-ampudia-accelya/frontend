import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of, Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';

import { PbdApiActions } from '~app/pbd/actions';
import { PbdActivityFormComponent } from '~app/pbd/components/pbd-activity-form/pbd-activity-form.component';
import * as fromPbd from '~app/pbd/reducers';
import { AgreementType, Pbd } from '~app/pbd/shared/pbd.model';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-pbd-activity-dialog',
  templateUrl: './pbd-activity-dialog.component.html'
})
export class PbdActivityDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  private destroy$ = new Subject();

  private loading$ = this.store.pipe(
    select(fromPbd.getActivityPbdLoading),
    tap(loading => this.setLoading(loading)),
    takeUntil(this.destroy$)
  );

  private activityBtnClick$ = this.reactiveSubject.asObservable.pipe(
    filter((action: any) => {
      const clickedBtn = action.clickedBtn;

      return clickedBtn === FooterButton.Agree || clickedBtn === FooterButton.Disagree;
    })
  );

  public pbd: Pbd;

  public agreementType: AgreementType;

  @ViewChild(PbdActivityFormComponent, { static: true }) pbdActivityForm: PbdActivityFormComponent;

  public fileIds = [];

  public result: any;

  public get form() {
    return this.pbdActivityForm.form;
  }

  public get data() {
    return this.config && this.config.data;
  }

  private _agreeBtn: ModalAction;
  public get agreeBtn(): ModalAction {
    if (!this._agreeBtn) {
      this._agreeBtn = this.data.buttons.find(
        (button: ModalAction) => button.type === FooterButton.Agree || button.type === FooterButton.Disagree
      );
    }

    return this._agreeBtn;
  }

  constructor(public config: DialogConfig, public reactiveSubject: ReactiveSubject, public store: Store<AppState>) {
    if (this.data) {
      this.pbd = this.data.pbd;
      this.agreementType = this.data.agreementType;
    }
  }

  public ngOnInit() {
    this.disableAgreeBtn(true);

    this.loading$.subscribe();

    this.activityBtnClick$
      .pipe(
        switchMap(() => this.uploadFiles()),
        filter(Boolean),
        tap(() => this.savePbdActivity()),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private uploadFiles(): Observable<boolean> {
    return this.pbdActivityForm.uploadComponent.hasFilesInPreviewMode ? this.pbdActivityForm.uploadFiles() : of(true);
  }

  private savePbdActivity() {
    this.store.dispatch(PbdApiActions.savePbdActivity(this.form.value));
  }

  private setLoading(loading: boolean) {
    this.agreeBtn.isLoading = loading;
  }

  public ngAfterViewInit() {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      this.disableAgreeBtn(status !== 'VALID');
    });
  }

  public disableAgreeBtn(value: boolean) {
    this.agreeBtn.isDisabled = value;
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
