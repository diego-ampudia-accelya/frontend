import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { PbdActivityDialogComponent } from './pbd-activity-dialog.component';
import { PbdActivityFormComponent } from '~app/pbd/components/pbd-activity-form/pbd-activity-form.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('PbdActivityDialogComponent', () => {
  let component: PbdActivityDialogComponent;
  let fixture: ComponentFixture<PbdActivityDialogComponent>;

  beforeEach(waitForAsync(() => {
    const initialState = {
      router: null,
      pbd: {
        pbdState: {
          pbd: null
        },
        pbdIssueState: {
          loading: false
        },
        pbdStartState: {
          loading: false,
          error: null
        },
        pbdActivityState: {
          loading: false
        },
        pbdDetailsState: {
          loading: false
        }
      }
    };

    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Agree,
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Agree
          }
        ]
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      declarations: [PbdActivityDialogComponent, PbdActivityFormComponent, TranslatePipeMock],
      providers: [
        FormBuilder,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject,
        provideMockStore({ initialState })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdActivityDialogComponent);
    component = fixture.componentInstance;
    component.pbdActivityForm.createForm();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
