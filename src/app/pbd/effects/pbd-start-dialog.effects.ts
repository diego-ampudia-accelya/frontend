import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { forkJoin, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { PbdStartDialogActions } from '~app/pbd/actions';
import { PbdService } from '~app/pbd/services/pbd.service';
import { CurrencyDictionaryService, NotificationService } from '~app/shared/services';

@Injectable()
export class PbdStartDialogEffects {
  $searchDocument = createEffect(() =>
    this.actions$.pipe(
      ofType(PbdStartDialogActions.searchDocument),
      switchMap(({ documentNumber }) =>
        this.pbdService.getBy(documentNumber).pipe(
          switchMap(documents => {
            if (documents.length) {
              return forkJoin([
                ...documents.map(document => {
                  const bspId = Number(document.id.substring(0, 4));

                  return this.currencyService
                    .getOne(bspId, document.currency.code)
                    .pipe(map(currency => ({ ...document, currency })));
                })
              ]);
            } else {
              throw new Error();
            }
          }),
          switchMap(documents => {
            const actions: Action[] = [];

            if (documents.length === 1) {
              actions.push(PbdStartDialogActions.selectDocument({ selectedDocument: documents[0] }));
            }

            return [...actions, PbdStartDialogActions.searchDocumentSuccess({ documents })];
          }),
          catchError(error => of(PbdStartDialogActions.searchDocumentFailure({ error })))
        )
      )
    )
  );

  $searchDocumentError = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PbdStartDialogActions.searchDocumentFailure),
        tap(error => {
          if (!error.error?.message) {
            const message = this.translationService.translate('postBillingDisputes.dialogs.pbdStart.notFoundError');
            this.notificationService.showError(message);
          }
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private pbdService: PbdService,
    private currencyService: CurrencyDictionaryService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService
  ) {}
}
