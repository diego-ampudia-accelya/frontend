import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';

import { PbdListActions } from '~app/pbd/actions';
import { PbdStartDialogComponent } from '~app/pbd/containers/pbd-start-dialog/pbd-start-dialog.component';
import { DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class PbdListEffects {
  $searchDocument = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PbdListActions.startPbd),
        tap(() => {
          this.dialogService.open(PbdStartDialogComponent, {
            data: {
              title: 'postBillingDisputes.dialogs.pbdStart.title',
              hasCancelButton: true,
              isClosable: true,
              footerButtonsType: [{ type: FooterButton.Start }],
              mainButtonType: FooterButton.Start
            }
          });
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(private actions$: Actions, private dialogService: DialogService) {}
}
