import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';

import { MenuTabActions } from '~app/core/actions';
import { getTabById } from '~app/core/reducers';
import { PbdDetailsActions, PbdIssueDialogActions } from '~app/pbd/actions';
import { PbdIssueConfirmationDialogComponent } from '~app/pbd/components/pbd-issue-confirmation-dialog/pbd-issue-confirmation-dialog.component';
import { PbdIssueDialogComponent } from '~app/pbd/containers/pbd-issue-dialog/pbd-issue-dialog.component';
import { PbdDetailsEffects } from '~app/pbd/effects/pbd-details.effects';
import { ActivityService } from '~app/pbd/services/activity.service';
import { PbdService } from '~app/pbd/services/pbd.service';
import { DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { MenuTab } from '~app/shared/models/menu-tab.model';
import { NotificationService } from '~app/shared/services';

describe('PbdDetailsEffects', () => {
  const initialState = { core: { menu: { tabs: {} } } };
  let effects: PbdDetailsEffects;
  let actions$: Observable<Action>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let routerSpy: SpyObject<Router>;
  let mockStore: MockStore;

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    dialogServiceSpy.open.and.returnValue(of({}));

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        PbdDetailsEffects,
        provideMockActions(() => actions$),
        { provide: DialogService, useValue: dialogServiceSpy },
        mockProvider(L10nTranslationService),
        mockProvider(PbdService),
        mockProvider(ActivityService),
        mockProvider(NotificationService),
        mockProvider(Router),
        provideMockStore({ initialState })
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(PbdDetailsEffects);
    mockStore = TestBed.inject<any>(Store);

    routerSpy = TestBed.inject<any>(Router);
    routerSpy.navigateByUrl.and.returnValue(Promise.resolve(true));
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('$pbdIssue', () => {
    it('should call open dialog', fakeAsync(() => {
      actions$ = of(PbdDetailsActions.pbdIssue());

      effects.$pbdIssue.subscribe(() => {
        expect(dialogServiceSpy.open).toHaveBeenCalledWith(PbdIssueDialogComponent, {
          data: {
            title: 'postBillingDisputes.dialogs.pbdIssue.title',
            hasCancelButton: true,
            isClosable: true,
            footerButtonsType: [{ type: FooterButton.Issue }]
          }
        });
      });
    }));
  });

  describe('$pbdIssueSuccess', () => {
    it('should call open and close dialog', fakeAsync(() => {
      actions$ = of(PbdIssueDialogActions.pbdIssueSuccess({ pbd: null }));

      effects.$pbdIssueSuccess.subscribe(() => {
        expect(dialogServiceSpy.close).toHaveBeenCalledTimes(2);
        expect(dialogServiceSpy.open).toHaveBeenCalledWith(PbdIssueConfirmationDialogComponent, {
          data: {
            title: 'postBillingDisputes.dialogs.pbdIssueConfirmation.title',
            hasCancelButton: false,
            isClosable: true,
            footerButtonsType: [{ type: FooterButton.Ok_tertiary }],
            pbd: null
          }
        });
      });
    }));

    it('should return PbdDetailsActions.pbdIssueConfirmed', fakeAsync(() => {
      actions$ = of(PbdIssueDialogActions.pbdIssueSuccess({ pbd: null }));

      effects.$pbdIssueSuccess.subscribe(action => {
        expect(action.type).toBe(PbdDetailsActions.pbdIssueConfirmed.type);
      });
    }));
  });

  describe('$pbdIssueConfirmed', () => {
    it('should call router navigateByUrl to pbd list, if no id provided', fakeAsync(() => {
      actions$ = of(PbdDetailsActions.pbdIssueConfirmed({ id: undefined }));

      effects.$pbdIssueConfirmed.subscribe();

      expect(routerSpy.navigateByUrl).toHaveBeenCalledWith(ROUTES.POST_BILLING_DISPUTE_LIST.url);
    }));

    it('should call router navigateByUrl to pbd details component, if id provided', fakeAsync(() => {
      actions$ = of(PbdDetailsActions.pbdIssueConfirmed({ id: 1 }));

      effects.$pbdIssueConfirmed.subscribe();

      expect(routerSpy.navigateByUrl).toHaveBeenCalledWith(ROUTES.POST_BILLING_DISPUTE_DETAILS.url + 1);
    }));

    it('should return MenuTabActions.Close', fakeAsync(() => {
      mockStore.overrideSelector(getTabById, {} as MenuTab);
      actions$ = of(PbdDetailsActions.pbdIssueConfirmed({ id: undefined }));
      let actualAction: MenuTabActions.Close;

      effects.$pbdIssueConfirmed.subscribe(action => (actualAction = action));
      tick();

      expect(actualAction.type).toBe(new MenuTabActions.Close(null).type);
    }));
  });
});
