import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { PbdIssueDialogActions } from '~app/pbd/actions';
import { PbdService } from '~app/pbd/services/pbd.service';

@Injectable()
export class PbdIssueDialogEffects {
  $createPbd = createEffect(() =>
    this.actions$.pipe(
      ofType(PbdIssueDialogActions.requestPbdIssue),
      switchMap(({ pbd, reason, fileIds }) =>
        this.pbdService.create(pbd, reason, fileIds).pipe(
          map(resultPbd => PbdIssueDialogActions.pbdIssueSuccess({ pbd: resultPbd })),
          catchError(error => merge(of(PbdIssueDialogActions.pbdIssueFailure({ error })), throwError(error)))
        )
      )
    )
  );

  constructor(private actions$: Actions, private pbdService: PbdService) {}
}
