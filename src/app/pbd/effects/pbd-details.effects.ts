import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { from, merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap, take, tap } from 'rxjs/operators';
import { MenuTabActions } from '~app/core/actions';
import { getTabById } from '~app/core/reducers';
import { PbdApiActions, PbdDetailsActions, PbdIssueDialogActions } from '~app/pbd/actions';
import { PbdActivityConfirmationDialogComponent } from '~app/pbd/components/pbd-activity-confirmation-dialog/pbd-activity-confirmation-dialog.component';
import { PbdIssueConfirmationDialogComponent } from '~app/pbd/components/pbd-issue-confirmation-dialog/pbd-issue-confirmation-dialog.component';
import { PbdActivityDialogComponent } from '~app/pbd/containers/pbd-activity-dialog/pbd-activity-dialog.component';
import { PbdIssueDialogComponent } from '~app/pbd/containers/pbd-issue-dialog/pbd-issue-dialog.component';
import { ActivityService } from '~app/pbd/services/activity.service';
import { PbdService } from '~app/pbd/services/pbd.service';
import { Activity, AgreementType, Pbd } from '~app/pbd/shared/pbd.model';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { ServerError } from '~app/shared/errors';
import { NotificationService } from '~app/shared/services/notification.service';

@Injectable()
export class PbdDetailsEffects {
  $pbdIssue = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PbdDetailsActions.pbdIssue),
        switchMap(() => this.openIssueDialog())
      ),
    { dispatch: false }
  );

  $pbdIssueSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(PbdIssueDialogActions.pbdIssueSuccess),
      tap(() => this.dialogService.close()),
      switchMap(({ pbd }) =>
        this.openIssuePbdConfirmationDialog(pbd).pipe(
          take(1),
          tap(() => this.dialogService.close()),
          map(() => PbdDetailsActions.pbdIssueConfirmed({ id: pbd?.id }))
        )
      )
    )
  );

  $pbdIssueConfirmed = createEffect(() =>
    this.actions$.pipe(
      ofType(PbdDetailsActions.pbdIssueConfirmed),
      switchMap(({ id }) =>
        from(
          this.router.navigateByUrl(
            id != null ? ROUTES.POST_BILLING_DISPUTE_DETAILS.url + id : ROUTES.POST_BILLING_DISPUTE_LIST.url
          )
        )
      ),
      switchMap(() => this.store.pipe(select(getTabById, { id: ROUTES.POST_BILLING_DISPUTE_ISSUE.tabLabel }), take(1))),
      map(tab => new MenuTabActions.Close(tab))
    )
  );

  $requestPbd = createEffect(() =>
    this.actions$.pipe(
      ofType(PbdApiActions.requestPbd),
      switchMap(({ id }) =>
        this.pbdService.getPbdById(id).pipe(
          map(pbd => PbdApiActions.requestPbdSuccess({ pbd })),
          catchError(error => merge(of(PbdApiActions.requestPbdFailure({ error })), throwError(error)))
        )
      )
    )
  );

  $requestPbdFailure = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PbdApiActions.requestPbdFailure),
        tap(({ error }) => {
          const lastSuccessfulNavigation = this.router.getCurrentNavigation()?.previousNavigation;
          if (lastSuccessfulNavigation != null) {
            this.router.navigateByUrl(lastSuccessfulNavigation.finalUrl.toString());
          } else {
            this.router.navigate(['./']);
          }

          if (error.status === 403) {
            this.dialogService.openPermissionErrorDialog();
          }
        })
      ),
    { dispatch: false }
  );

  $changePbdAgreement = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PbdDetailsActions.changePbdAgreement),
        tap(({ pbd, agreementType }) => this.openActivityDialog(pbd, agreementType))
      ),
    { dispatch: false }
  );

  $savePbdActivity = createEffect(() =>
    this.actions$.pipe(
      ofType(PbdApiActions.savePbdActivity),
      switchMap(({ pbdId, reason, activityType, agreementType, attachments }) =>
        this.activityService.create(pbdId, activityType, agreementType, reason, attachments).pipe(
          tap(result => {
            if (result.id && agreementType) {
              this.dialogService.close();
            }
          }),
          switchMap(activity => [
            PbdApiActions.savePbdActivitySuccess({ activity, agreementType }),
            PbdApiActions.requestPbd({ id: pbdId })
          ]),
          catchError((error: ServerError) =>
            merge(of(PbdApiActions.savePbdActivityFailure({ error })), throwError(error))
          )
        )
      )
    )
  );

  $savePbdActivitySuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PbdApiActions.savePbdActivitySuccess),
        tap(({ activity, agreementType }) => {
          if (agreementType) {
            this.openPbdActivityConfirmationDialog(activity, agreementType).subscribe(() => {
              this.dialogService.close();
            });
          }
        })
      ),
    {
      dispatch: false
    }
  );

  $downloadAttachment = createEffect(() =>
    this.actions$.pipe(
      ofType(PbdApiActions.downloadAttachment),
      switchMap(({ attachment }) =>
        this.activityService.download(attachment).pipe(
          tap(response => {
            FileSaver.saveAs(response.blob, response.fileName);
          }),
          switchMap(() => of(PbdApiActions.downloadAttachmentSuccess())),
          catchError(error => merge(of(PbdApiActions.downloadAttachmentFailure()), throwError(error)))
        )
      )
    )
  );

  $downloadAttachmentSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PbdApiActions.downloadAttachmentSuccess),
        tap(() =>
          this.notificationService.showSuccess(
            this.translationService.translate(`postBillingDisputes.pbdDetails.messages.downloadedSuccess`)
          )
        )
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private pbdService: PbdService,
    private activityService: ActivityService,
    private notificationService: NotificationService,
    private router: Router,
    private store: Store<AppState>
  ) {}

  private openIssueDialog() {
    return this.dialogService.open(PbdIssueDialogComponent, {
      data: {
        title: 'postBillingDisputes.dialogs.pbdIssue.title',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Issue }]
      }
    });
  }

  private openIssuePbdConfirmationDialog(pbd: Pbd) {
    return this.dialogService.open(PbdIssueConfirmationDialogComponent, {
      data: {
        title: 'postBillingDisputes.dialogs.pbdIssueConfirmation.title',
        hasCancelButton: false,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Ok_tertiary }],
        pbd
      }
    });
  }

  private openActivityDialog(pbd: Pbd, agreementType: AgreementType) {
    return this.dialogService.open(PbdActivityDialogComponent, {
      data: {
        title: `postBillingDisputes.dialogs.activity.titles.${agreementType}`,
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: this.getActivityDialogFooterButton(agreementType) }],
        pbd,
        agreementType
      }
    });
  }

  private getActivityDialogFooterButton(agreementType: AgreementType): FooterButton {
    switch (agreementType) {
      case AgreementType.AgreeWithAgent:
      case AgreementType.AgreeWithAirline:
        return FooterButton.Agree;
      default:
        return FooterButton.Disagree;
    }
  }

  private openPbdActivityConfirmationDialog(activity: Activity, agreementType: AgreementType) {
    return this.dialogService.open(PbdActivityConfirmationDialogComponent, {
      data: {
        title: `postBillingDisputes.dialogs.activityConfirmation.titles.${agreementType}`,
        hasCancelButton: false,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Ok_tertiary }],
        activity
      }
    });
  }
}
