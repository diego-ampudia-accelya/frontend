import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { Pbd } from '~app/pbd/shared/pbd.model';
import { FileValidators } from '~app/shared/components/upload/file-validators';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { blankStringValidator } from '~app/shared/validators/blank-string.validator';

@Component({
  selector: 'bspl-pbd-activity-comment-form',
  templateUrl: './activity-comment-form.component.html',
  styleUrls: ['./activity-comment-form.component.scss']
})
export class ActivityCommentFormComponent implements OnInit, OnDestroy {
  @Input() pbd: Pbd;

  @Input() disabled = false;

  @Input() isLoading = false;

  @Output() send = new EventEmitter<{ pbd: Pbd; comment: string; fileIds?: number[] }>();

  @ViewChild(UploadComponent, { static: true }) uploadComponent: UploadComponent;

  public validationRules = [FileValidators.maxFileSize(), FileValidators.minFileSize()];

  public form: FormGroup;

  public textAreaIsFocused = false;

  public uploadMode = UploadModes.Preview;

  public uploadPath = `${appConfiguration.baseUploadPath}/post-billing-dispute/files`;

  private destroy$ = new Subject();

  public get comment() {
    return (this.form && this.form.value.comment) || '';
  }

  public get active(): boolean {
    return Boolean(this.textAreaIsFocused || this.comment || this.uploadComponent.hasFiles || this.isLoading);
  }

  public get isPostBtnDisabled(): boolean {
    const formIsValid = this.form && this.form.valid;

    return Boolean(this.uploadIsInProgress || !formIsValid);
  }

  public get isCancelBtnDisabled(): boolean {
    const formIsEmpty = !(this.comment.trim() || this.uploadComponent.hasFiles);

    return Boolean(this.uploadIsInProgress || formIsEmpty);
  }

  private uploadIsInProgress = false;

  constructor(private formBuilder: FormBuilder) {}

  public ngOnInit() {
    this.form = this.formBuilder.group({
      comment: ['', [Validators.required, blankStringValidator]],
      fileIds: [null]
    });
  }

  public setTextAreaIsFocused(value: boolean) {
    this.textAreaIsFocused = value;
  }

  public setUploadIsInProgress(value: boolean) {
    this.uploadIsInProgress = value;
  }

  public onCancelClick() {
    this.resetForm();
  }

  public onPostClick() {
    if (this.uploadComponent.hasFilesInPreviewMode) {
      this.uploadFiles();
    } else {
      this.emitSend();
    }
  }

  private uploadFiles() {
    this.uploadComponent
      .uploadFiles()
      .pipe(filter(Boolean), takeUntil(this.destroy$))
      .subscribe(() => this.emitSend());
  }

  private emitSend() {
    if (this.form.valid) {
      const { comment, fileIds } = this.form.value;

      this.send.emit({
        pbd: this.pbd,
        comment,
        fileIds
      });
    }
  }

  public resetForm() {
    this.form.setValue({ comment: '', fileIds: null });
    this.uploadComponent.reset();
    this.textAreaIsFocused = false;
    this.uploadIsInProgress = false;
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
