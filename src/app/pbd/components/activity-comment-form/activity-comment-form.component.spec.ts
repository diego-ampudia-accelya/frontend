import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationModule } from 'angular-l10n';

import { ActivityCommentFormComponent } from './activity-comment-form.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { SharedModule } from '~app/shared/shared.module';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

describe('ActivityCommentFormComponent', () => {
  let component: ActivityCommentFormComponent;
  let fixture: ComponentFixture<ActivityCommentFormComponent>;

  const translationServiceSpy = jasmine.createSpyObj('L10nTranslationService', ['translate']);
  translationServiceSpy.translate.and.returnValue('translated');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityCommentFormComponent, TranslatePipeMock],
      imports: [SharedModule, L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityCommentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
