/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input } from '@angular/core';

import { PbdEntity } from '~app/pbd/shared/pbd.model';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-pbd-entity-link',
  templateUrl: './pbd-entity-link.component.html'
})
export class PbdEntityLinkComponent {
  public ROUTES = ROUTES;

  public Permissions = Permissions;

  @Input() public pbdEntity: PbdEntity;

  @Input() public showCode = false;

  public get permission(): string {
    switch (this.pbdEntity?.userType) {
      case UserType.AGENT:
        return Permissions.readAgent;
      case UserType.AIRLINE:
        return Permissions.readAirline;
      default:
        return '';
    }
  }

  public get linkData(): string[] {
    const id = this.pbdEntity?.id?.toString() || '';

    switch (this.pbdEntity?.userType) {
      case UserType.AGENT:
        return [ROUTES.AGENT_PROFILE.url, id];
      case UserType.AIRLINE:
        return [ROUTES.AIRLINE_PROFILE.url, id];
      default:
        return [''];
    }
  }

  get text(): string {
    const pbdEntity = this.pbdEntity;

    if (pbdEntity) {
      const { iataCode, name } = pbdEntity;

      return this.showCode ? `${iataCode} / ${name}` : name;
    }

    return '';
  }
}
