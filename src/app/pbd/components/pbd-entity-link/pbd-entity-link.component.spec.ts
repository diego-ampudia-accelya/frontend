import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';

import { PbdEntityLinkComponent } from './pbd-entity-link.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { UserType } from '~app/shared/models/user.model';

const agentEntity = {
  id: 123,
  iataCode: 'test code',
  name: 'test name',
  userType: UserType.AGENT
};

const airlineEntity = {
  ...agentEntity,
  userType: UserType.AIRLINE
};

describe('PbdEntityLinkComponent', () => {
  let component: PbdEntityLinkComponent;
  let fixture: ComponentFixture<PbdEntityLinkComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PbdEntityLinkComponent, HasPermissionPipe],
      providers: [mockProvider(PermissionsService)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdEntityLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('permission', () => {
    it('should return Permissions.readAgent for Agent Entity', () => {
      component.pbdEntity = agentEntity;

      expect(component.permission).toBe(Permissions.readAgent);
    });

    it('should return Permissions.readAirline for Airline Entity', () => {
      component.pbdEntity = airlineEntity;

      expect(component.permission).toBe(Permissions.readAirline);
    });
  });

  describe('linkData', () => {
    it('should return array with empty string if no entity', () => {
      expect(component.linkData).toEqual(['']);
    });

    it('should return url data for /master-data/agent/123', () => {
      component.pbdEntity = agentEntity;

      expect(component.linkData).toEqual([ROUTES.AGENT_PROFILE.url, '123']);
    });

    it('should return url data for /master-data/airline/123', () => {
      component.pbdEntity = airlineEntity;

      expect(component.linkData).toEqual([ROUTES.AIRLINE_PROFILE.url, '123']);
    });
  });

  describe('text', () => {
    it('should return empty string if no entity is provided', () => {
      expect(component.text).toBe('');
    });

    it('should return code and name if showCode is true', () => {
      component.pbdEntity = agentEntity;
      component.showCode = true;
      expect(component.text).toBe('test code / test name');
    });

    it('should return only name if showCode is false', () => {
      component.pbdEntity = agentEntity;
      component.showCode = false;
      expect(component.text).toBe('test name');
    });
  });
});
