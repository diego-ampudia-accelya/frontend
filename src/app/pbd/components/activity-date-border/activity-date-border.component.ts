import { Component, Input } from '@angular/core';

@Component({
  selector: 'bspl-pbd-activity-date-border',
  templateUrl: './activity-date-border.component.html',
  styleUrls: ['./activity-date-border.component.scss']
})
export class ActivityDateBorderComponent {
  @Input() date: string;
}
