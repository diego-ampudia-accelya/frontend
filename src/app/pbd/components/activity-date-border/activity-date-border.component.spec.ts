import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActivityDateBorderComponent } from './activity-date-border.component';

describe('ActivityDateBorderComponent', () => {
  let component: ActivityDateBorderComponent;
  let fixture: ComponentFixture<ActivityDateBorderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityDateBorderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDateBorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
