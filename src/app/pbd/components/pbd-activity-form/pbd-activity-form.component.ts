import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';

import { AgreementType, Pbd } from '~app/pbd/shared/pbd.model';
import { TextareaComponent } from '~app/shared/components';
import { FileValidators } from '~app/shared/components/upload/file-validators';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { FormUtil } from '~app/shared/helpers';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { blankStringValidator } from '~app/shared/validators/blank-string.validator';

@Component({
  selector: 'bspl-pbd-activity-form',
  templateUrl: './pbd-activity-form.component.html',
  styleUrls: ['./pbd-activity-form.component.scss']
})
export class PbdActivityFormComponent implements OnInit, AfterViewInit {
  @Input() public pbd: Pbd;

  @Input() public agreementType: AgreementType;

  @Output() uploadFinished = new EventEmitter<any>();

  @Output() uploadStarted = new EventEmitter<any>();

  @ViewChild(TextareaComponent, { static: true }) textArea: TextareaComponent;

  @ViewChild(UploadComponent) uploadComponent: UploadComponent;

  public validationRules = [FileValidators.maxFileSize(), FileValidators.minFileSize()];

  public uploadPath = `${appConfiguration.baseUploadPath}/post-billing-dispute/files`;

  public uploadMode = UploadModes.Preview;

  public form: FormGroup;

  public get agentInfoVisible(): boolean {
    return (
      this.agreementType === AgreementType.AgreeWithAgent || this.agreementType === AgreementType.DisagreeWithAgent
    );
  }

  public get airlineInfoVisible(): boolean {
    return (
      this.agreementType === AgreementType.AgreeWithAirline || this.agreementType === AgreementType.DisagreeWithAirline
    );
  }

  public get infoText(): string {
    return `postBillingDisputes.dialogs.activity.info.${this.agreementType}`;
  }

  public get amountLbl(): string {
    const currency = (this.pbd && this.pbd.currency?.code) || '';

    return this.translationService.translate('postBillingDisputes.dialogs.activity.form.amount', { currency });
  }

  public get reasonPlaceholder(): string {
    return `postBillingDisputes.dialogs.activity.form.reason.placeholder.${this.agreementType}`;
  }

  constructor(private formBuilder: FormBuilder, private translationService: L10nTranslationService) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    const pbdId = (this.pbd && this.pbd.id) || null;
    this.form = this.formBuilder.group({
      pbdId: [pbdId],
      agreementType: [this.agreementType],
      reason: ['', { validators: [Validators.required, blankStringValidator], updateOn: 'change' }],
      fileIds: [null]
    });
  }

  ngAfterViewInit() {
    if (this.textArea) {
      this.textArea.focusElement();
    }
  }

  showControlState() {
    FormUtil.showControlState(this.form);
  }

  onUploadStarted() {
    this.uploadStarted.emit();
  }

  onUploadFinished() {
    this.uploadFinished.emit();
  }

  uploadFiles(): Observable<any> {
    return this.uploadComponent.uploadFiles();
  }
}
