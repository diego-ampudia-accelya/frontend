import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { PbdActivityFormComponent } from './pbd-activity-form.component';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('PbdActivityFormComponent', () => {
  let component: PbdActivityFormComponent;
  let fixture: ComponentFixture<PbdActivityFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PbdActivityFormComponent, TranslatePipeMock],
      providers: [FormBuilder, { provide: L10nTranslationService, useValue: translationServiceSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdActivityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
