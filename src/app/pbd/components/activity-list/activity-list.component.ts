import { Component, EventEmitter, Input, Output } from '@angular/core';
import moment from 'moment-mini';

import { Activity, ActivityType, Pbd } from '~app/pbd/shared/pbd.model';

@Component({
  selector: 'bspl-pbd-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})
export class ActivityListComponent {
  @Input() activities: Activity[];
  @Input() pbd: Pbd;
  @Output() download = new EventEmitter<{ pbdId: number; fileId: number; activityId: number }>();

  public isWorkflowItem(activity: Activity) {
    return activity.type.toUpperCase() !== ActivityType.Commented;
  }

  public getDate(activity: Activity) {
    return moment(activity.creationDate).format('DD/MM/YYYY');
  }

  public isNewActivityDay(activity: Activity, index: number) {
    if (index === this.activities.length - 1) {
      return true;
    } else {
      return !this.isSameDay(new Date(activity.creationDate), new Date(this.activities[index + 1].creationDate));
    }
  }

  isSameDay(firstDate: Date, secondDate: Date) {
    return (
      firstDate.getFullYear() === secondDate.getFullYear() &&
      firstDate.getMonth() === secondDate.getMonth() &&
      firstDate.getDate() === secondDate.getDate()
    );
  }

  onDownload(attachment) {
    this.download.emit({ pbdId: this.pbd.id, ...attachment });
  }
}
