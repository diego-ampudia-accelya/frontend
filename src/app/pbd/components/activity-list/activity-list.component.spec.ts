import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActivityListComponent } from './activity-list.component';
import { PBD_ACTIVITIES } from '~app/pbd/shared/pbd-mocks.constants';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

describe('ActivityListComponent', () => {
  let component: ActivityListComponent;
  let fixture: ComponentFixture<ActivityListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityListComponent, BytesFormatPipe, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityListComponent);
    component = fixture.componentInstance;
    component.activities = PBD_ACTIVITIES;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return true if the item is of type workflow', () => {
    const activity = PBD_ACTIVITIES[0];
    const result = component.isWorkflowItem(activity);

    expect(result).toBeTruthy();
  });
});
