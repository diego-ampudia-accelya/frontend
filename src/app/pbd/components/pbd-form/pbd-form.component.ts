/* eslint-disable @typescript-eslint/naming-convention */
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { Pbd } from '~app/pbd/shared/pbd.model';
import { GLOBALS } from '~app/shared/constants/globals';
import { ROUTES } from '~app/shared/constants/routes';
import { UserType } from '~app/shared/models/user.model';
import { greaterValidator } from '~app/shared/validators/greater.validator';
import { lowerAmountValidator } from '~app/shared/validators/lower-amount.validator';

@Component({
  selector: 'bspl-pbd-form',
  templateUrl: './pbd-form.component.html',
  styleUrls: ['./pbd-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PbdFormComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public pbd: Pbd;

  @Output() public amountChange = new EventEmitter<string>();

  @Output() public formValid = new EventEmitter<boolean>();

  public ROUTES = ROUTES;

  public UserType = UserType;

  public formData: FormGroup;

  public get pbdAmountLabel() {
    const key = this.translationService.translate('postBillingDisputes.pbdDetails.generalLabels.pbdAmount');

    return `${key}, ${this.pbd.currency.code}`;
  }

  private amountControl: FormControl;

  private destroy$ = new Subject();

  constructor(private translationService: L10nTranslationService, private formBuilder: FormBuilder) {}

  public ngOnInit(): void {
    if (this.pbd && !this.pbd.id) {
      this.setInitialFormData();
      this.handleAmountChange();
      this.emitFormValid();
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.pbd?.previousValue &&
      changes.pbd.currentValue.documentNumber !== changes.pbd.previousValue.documentNumber
    ) {
      this.updateAmountControl();
    }
  }

  public handleAmountChange() {
    this.formData
      .get('amount')
      .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$))
      .subscribe(value => {
        this.formData.updateValueAndValidity();
        if (!isNaN(value)) {
          const formattedValue = Number(value).toFixed(this.pbd.currency?.decimals);
          this.amountControl.patchValue(formattedValue, { emitEvent: false });
          const updatedAmount = this.formData.valid ? formattedValue : null;
          this.amountChange.emit(updatedAmount);
        }
        this.formValid.emit(this.formData.valid);
      });
  }

  public setInitialFormData() {
    this.amountControl = new FormControl(this.pbd.pbdAmount, Validators.required);
    this.formData = this.formBuilder.group({ amount: this.amountControl }, { updateOn: 'blur' });
    this.setAmountValidators();
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  private emitFormValid() {
    const condition = this.formData.valid && this.amountControl.value;
    this.formValid.emit(condition);
  }

  private updateAmountControl() {
    const amountControl = this.formData?.get('amount');
    if (amountControl) {
      amountControl.patchValue(null);
      amountControl.markAsPristine();
      amountControl.markAsUntouched();
      this.setAmountValidators();
      amountControl.updateValueAndValidity();
    }
  }

  private setAmountValidators() {
    this.amountControl.setValidators([
      greaterValidator(0),
      lowerAmountValidator(this.pbd.maximumDisputableAmount, this.pbd.currency?.decimals),
      Validators.pattern(GLOBALS.PATTERNS.DECIMALS_PATTERN),
      Validators.required
    ]);
  }
}
