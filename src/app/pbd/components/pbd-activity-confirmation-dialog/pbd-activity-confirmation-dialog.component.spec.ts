import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { PbdActivityConfirmationDialogComponent } from './pbd-activity-confirmation-dialog.component';
import { DialogConfig } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('PbdActivityConfirmationDialogComponent', () => {
  let component: PbdActivityConfirmationDialogComponent;
  let fixture: ComponentFixture<PbdActivityConfirmationDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PbdActivityConfirmationDialogComponent, TranslatePipeMock],
      providers: [DialogConfig, ReactiveSubject, { provide: L10nTranslationService, useValue: translationServiceSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdActivityConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
