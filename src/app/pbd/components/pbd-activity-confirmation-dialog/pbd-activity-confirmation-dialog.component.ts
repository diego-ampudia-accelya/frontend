import { Component } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { Activity, AgreementType, PbdStatus } from '~app/pbd/shared/pbd.model';
import { DialogConfig } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';

@Component({
  selector: 'bspl-pbd-activity-confirmation-dialog',
  templateUrl: './pbd-activity-confirmation-dialog.component.html',
  styleUrls: ['./pbd-activity-confirmation-dialog.component.scss']
})
export class PbdActivityConfirmationDialogComponent {
  public activity: Activity;
  public agreementType: AgreementType;

  public get status() {
    const status = (this.activity && this.activity.status) || '';

    return this.translationService.translate(`postBillingDisputes.statuses.${status}`);
  }

  public get processStatusInfo(): string {
    const status = this.activity && this.activity.status;
    const reached = status === PbdStatus.PendingAgent || status === PbdStatus.PendingAirline ? 'notReached' : 'reached';

    return `postBillingDisputes.dialogs.activityConfirmation.processStatusInfo.${reached}`;
  }

  public get showNextStepInfo(): boolean {
    if (this.activity) {
      switch (this.activity.status) {
        case PbdStatus.AgreeToAgent:
        case PbdStatus.AgreeToAirline:
        case PbdStatus.AgreementNotReached:
          return true;
      }
    }

    return false;
  }

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    private translationService: L10nTranslationService
  ) {
    const configData = config && config.data;

    if (configData) {
      this.activity = configData.activity;
      this.agreementType = configData.agreementType;
    }
  }
}
