/* eslint-disable @typescript-eslint/naming-convention */
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { TimeLeftForAgreementService } from '~app/pbd/services/time-left-for-agreement.service';
import { AgreementType, Pbd, PbdStatus, SecondaryHeaderActionsViewModel } from '~app/pbd/shared/pbd.model';
import { Permissions } from '~app/shared/constants/permissions';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { WatcherDocType } from '~app/shared/models/watcher.model';
import { NotificationService } from '~app/shared/services';
import { AgreementMenuService } from './agreement-menu.service';

@Component({
  selector: 'bspl-pbd-header',
  templateUrl: './pbd-header.component.html',
  styleUrls: ['./pbd-header.component.scss']
})
export class PbdHeaderComponent implements OnChanges, AfterViewInit {
  @ViewChild('statusHeader') statusHeader: ElementRef;

  @Input() pbd: Pbd;
  @Input() issueButtonEnabled: boolean;
  @Input() issueButtonVisible: boolean;
  @Input() loggedUserType: UserType;

  @Output() issue = new EventEmitter<any>();

  @Output() agreementChange = new EventEmitter<{ pbd: Pbd; agreementType: AgreementType }>();
  @Output() heightChange = new EventEmitter<number>();

  public pbdInprogress: boolean;
  public timeLeftForAgreement: string;
  public buttonMenuOptions: ButtonMenuOption[];
  public showHeaderSecondaryActions: boolean;
  public headerSecondaryActions: Array<SecondaryHeaderActionsViewModel>;

  public isWatchingAllowed: boolean;
  public watching: boolean;

  public selectedAgreementType: AgreementType;
  public Permissions = Permissions;

  constructor(
    private agreementMenuService: AgreementMenuService,
    private timeLeftForAgreementService: TimeLeftForAgreementService,
    private notificationsHeaderService: NotificationsHeaderService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService
  ) {}

  public ngOnChanges(changes: SimpleChanges): void {
    const previousPbd = changes.pbd?.previousValue;
    const currentPbdChanged = changes.pbd?.currentValue;

    if (currentPbdChanged) {
      if (!!currentPbdChanged.id && currentPbdChanged.id !== previousPbd?.id) {
        this.initializeWatchingFlagAndActions();
      }

      if (previousPbd?.status !== currentPbdChanged.status) {
        this.initializeButtonMenuOptions();
        this.initializeInProgressPbdStatus();
      }

      if (
        currentPbdChanged.id &&
        previousPbd?.timeLeftForAgreementInHours !== currentPbdChanged.timeLeftForAgreementInHours
      ) {
        this.initializeTimeLeftForAgreement();
      }
    }
  }

  public ngAfterViewInit(): void {
    this.heightChange.emit(this.statusHeader.nativeElement.offsetHeight);
  }

  public onAgreementButtonSelect() {
    const pbd = this.pbd;
    const agreementType = this.agreementMenuService.selectedAgreementType;

    this.agreementChange.emit({ pbd, agreementType });
  }

  public toggleHeaderSecondaryActionsMenu(event: Event): void {
    if (event) {
      event.stopPropagation();
      this.showHeaderSecondaryActions = !this.showHeaderSecondaryActions;
    } else {
      this.showHeaderSecondaryActions = false;
    }
  }

  public onActionClick(secondaryAction: SecondaryHeaderActionsViewModel) {
    switch (secondaryAction.action) {
      case GridTableActionType.StartWatching:
      case GridTableActionType.StopWatching:
        this.notificationsHeaderService
          .updateWatcher(
            {
              documentId: this.pbd.id,
              documentType: WatcherDocType.PBD,
              watching: secondaryAction.action === GridTableActionType.StartWatching
            },
            this.pbd.agent.bsp.id
          )
          .subscribe(({ watching }) => this.onUpdateWatcherSuccess(watching));
        break;
    }

    this.showHeaderSecondaryActions = false;
  }

  private onUpdateWatcherSuccess(watching: boolean): void {
    this.watching = watching;
    this.initializeSecondaryHeaderActions();
    this.notifyUpdateWatcherSuccess(watching);
  }

  private notifyUpdateWatcherSuccess(watching: boolean): void {
    const translationKey = watching ? 'startWatchingDocument' : 'stopWatchingDocument';
    this.notificationService.showSuccess(this.translationService.translate(translationKey));
  }

  private initializeWatchingFlagAndActions(): void {
    const bspId = Number(this.pbd.agent.id.toString().substring(0, 4));
    const documentId = this.pbd.documentId ? this.pbd.documentId : this.pbd.id;
    this.notificationsHeaderService.getWatcher(bspId, Number(documentId), WatcherDocType.PBD).subscribe(watcher => {
      this.watching = watcher.watching;
      this.initializeSecondaryHeaderActions();
    });
  }

  private initializeSecondaryHeaderActions(): void {
    const watcherAction = this.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;
    const watcherLabel = this.watching ? 'stopWatching' : 'startWatching';

    this.headerSecondaryActions = [
      {
        label: this.translationService.translate(watcherLabel),
        action: watcherAction
      }
    ];

    this.isWatchingAllowed = true;
    this.showHeaderSecondaryActions = false;
  }

  private initializeButtonMenuOptions(): void {
    this.buttonMenuOptions = this.agreementMenuService.createButtonsMenu(this.loggedUserType, this.pbd.status);
  }

  private initializeTimeLeftForAgreement(): void {
    this.timeLeftForAgreement = this.timeLeftForAgreementService.getTimeLeftForAgreementText(
      this.pbd.timeLeftForAgreementInHours
    );
  }

  private initializeInProgressPbdStatus(): void {
    this.pbdInprogress = this.pbd.status === PbdStatus.PendingAgent || this.pbd.status === PbdStatus.PendingAirline;
  }
}
