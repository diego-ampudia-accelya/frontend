/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AgreementType, PbdStatus } from '~app/pbd/shared/pbd.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AgreementMenuService {
  public selectedAgreementType: AgreementType;

  private MENU_BUTTONS = {
    AGREED_WITH_AIRLINE_BTN: {
      label: this.translationService.translate('postBillingDisputes.pbdDetails.buttonLabels.agreeWithAirline'),
      command: () => {
        this.selectedAgreementType = AgreementType.AgreeWithAirline;
      }
    },
    AGREED_WITH_AGENT_BTN: {
      label: this.translationService.translate('postBillingDisputes.pbdDetails.buttonLabels.agreeWithAgent'),
      command: () => {
        this.selectedAgreementType = AgreementType.AgreeWithAgent;
      }
    },
    DISAGREED_WITH_AIRLINE_BTN: {
      label: this.translationService.translate('postBillingDisputes.pbdDetails.buttonLabels.disagreeWithAirline'),
      command: () => {
        this.selectedAgreementType = AgreementType.DisagreeWithAirline;
      }
    },
    DISAGREED_WITH_AGENT_BTN: {
      label: this.translationService.translate('postBillingDisputes.pbdDetails.buttonLabels.disagreeWithAgent'),
      command: () => {
        this.selectedAgreementType = AgreementType.DisagreeWithAgent;
      }
    }
  };
  constructor(private translationService: L10nTranslationService) {}

  public createButtonsMenu(loggedUserType: UserType, pbdStatus: PbdStatus): ButtonMenuOption[] {
    if (loggedUserType === UserType.AGENT || loggedUserType === UserType.AGENT_GROUP) {
      return this.createButtonsForAgent(pbdStatus);
    } else if (loggedUserType === UserType.AIRLINE) {
      return this.createButtonsForAirline(pbdStatus);
    }
  }

  private createButtonsForAirline(pbdStatus: PbdStatus): ButtonMenuOption[] {
    switch (pbdStatus) {
      case PbdStatus.PendingAgent:
        return [this.MENU_BUTTONS.AGREED_WITH_AGENT_BTN];
      case PbdStatus.PendingAirline:
        return [this.MENU_BUTTONS.AGREED_WITH_AGENT_BTN, this.MENU_BUTTONS.DISAGREED_WITH_AGENT_BTN];
      default:
        return [];
    }
  }

  private createButtonsForAgent(pbdStatus: PbdStatus): ButtonMenuOption[] {
    switch (pbdStatus) {
      case PbdStatus.PendingAgent:
        return [this.MENU_BUTTONS.AGREED_WITH_AIRLINE_BTN, this.MENU_BUTTONS.DISAGREED_WITH_AIRLINE_BTN];
      case PbdStatus.PendingAirline:
        return [this.MENU_BUTTONS.AGREED_WITH_AIRLINE_BTN];
      default:
        return [];
    }
  }
}
