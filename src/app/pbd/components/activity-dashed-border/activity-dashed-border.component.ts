import { Component } from '@angular/core';

@Component({
  selector: 'bspl-pbd-activity-dashed-border',
  templateUrl: './activity-dashed-border.component.html',
  styleUrls: ['./activity-dashed-border.component.scss']
})
export class ActivityDashedBorderComponent {}
