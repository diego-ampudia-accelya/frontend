import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActivityDashedBorderComponent } from './activity-dashed-border.component';

describe('ActivityDashedBorderComponent', () => {
  let component: ActivityDashedBorderComponent;
  let fixture: ComponentFixture<ActivityDashedBorderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityDashedBorderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDashedBorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
