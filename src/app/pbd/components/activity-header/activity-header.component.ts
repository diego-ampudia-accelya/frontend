import { Component, EventEmitter, Output } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ActivityHeaderService } from './activity-header.service';
import { ActivityFilterType } from '~app/pbd/shared/pbd.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';

@Component({
  selector: 'bspl-pbd-activity-header',
  templateUrl: './activity-header.component.html',
  styleUrls: ['./activity-header.component.scss']
})
export class ActivityHeaderComponent {
  @Output() filterSelect = new EventEmitter<any>();

  // TODO: This should be taken from the store, which is in the scope of the next task
  public btnLabel = this.translationService.translate(
    `postBillingDisputes.activityPanel.buttonLabels.${ActivityFilterType.All}`
  );
  public buttonMenuOptions: ButtonMenuOption[] = this.activityHeaderService.getFilterItems();
  constructor(
    private translationService: L10nTranslationService,
    private activityHeaderService: ActivityHeaderService
  ) {}

  onFilterButtonSelect(event) {
    this.filterSelect.emit(event.id);

    // TODO: Dispatch an event to the store with the filter change. Its in the scope of the next task
    this.btnLabel = event.label;
  }
}
