import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ActivityFilterType } from '~app/pbd/shared/pbd.model';

@Injectable({
  providedIn: 'root'
})
export class ActivityHeaderService {
  constructor(private translationService: L10nTranslationService) {}
  getFilterItems() {
    return [
      {
        label: this.translationService.translate(
          `postBillingDisputes.activityPanel.buttonLabels.${ActivityFilterType.All}`
        ),
        id: ActivityFilterType.All
      },
      {
        label: this.translationService.translate(
          `postBillingDisputes.activityPanel.buttonLabels.${ActivityFilterType.Workflow}`
        ),
        id: ActivityFilterType.Workflow
      },
      {
        label: this.translationService.translate(
          `postBillingDisputes.activityPanel.buttonLabels.${ActivityFilterType.Comments}`
        ),
        id: ActivityFilterType.Comments
      },
      {
        label: this.translationService.translate(
          `postBillingDisputes.activityPanel.buttonLabels.${ActivityFilterType.Attachments}`
        ),
        id: ActivityFilterType.Attachments
      }
    ];
  }
}
