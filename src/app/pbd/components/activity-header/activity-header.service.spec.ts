import { NO_ERRORS_SCHEMA } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { ActivityHeaderService } from './activity-header.service';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('Service: ActivityHeader', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TranslatePipeMock],
      providers: [ActivityHeaderService, { provide: L10nTranslationService, useValue: translationServiceSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  it('should ...', inject([ActivityHeaderService], (service: ActivityHeaderService) => {
    expect(service).toBeTruthy();
  }));
});
