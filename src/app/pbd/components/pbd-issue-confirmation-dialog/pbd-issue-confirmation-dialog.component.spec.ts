import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PbdIssueConfirmationDialogComponent } from './pbd-issue-confirmation-dialog.component';
import { PBDS_IATA_AGENT } from '~app/pbd/shared/pbd-mocks.constants';
import { DialogConfig } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DayMonthYearPipe } from '~app/shared/pipes/day-month-year.pipe';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

describe('PbdIssueConfirmationDialogComponent', () => {
  let component: PbdIssueConfirmationDialogComponent;
  let fixture: ComponentFixture<PbdIssueConfirmationDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PbdIssueConfirmationDialogComponent, TranslatePipeMock, DayMonthYearPipe],
      providers: [DialogConfig, ReactiveSubject],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdIssueConfirmationDialogComponent);
    component = fixture.componentInstance;
    component.pbd = PBDS_IATA_AGENT[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
