import { Component } from '@angular/core';

import { Pbd } from '~app/pbd/shared/pbd.model';
import { DialogConfig } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';

@Component({
  selector: 'bspl-pbd-issue-confirmation-dialog',
  templateUrl: './pbd-issue-confirmation-dialog.component.html',
  styleUrls: ['./pbd-issue-confirmation-dialog.component.scss']
})
export class PbdIssueConfirmationDialogComponent {
  public pbd: Pbd = this.config?.data?.pbd;

  constructor(public config: DialogConfig, public reactiveSubject: ReactiveSubject) {}
}
