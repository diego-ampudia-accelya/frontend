import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { ActivityCommentItemComponent } from './activity-comment-item.component';
import { ToPbdEntityPipe } from '~app/pbd/pipes/to-pbd-entity.pipe';
import { PBD_ACTIVITIES } from '~app/pbd/shared/pbd-mocks.constants';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('ActivityCommentItemComponent', () => {
  let component: ActivityCommentItemComponent;
  let fixture: ComponentFixture<ActivityCommentItemComponent>;
  const bytesFormatPipeSpy = createSpyObject(BytesFormatPipe);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityCommentItemComponent, TranslatePipeMock, ToPbdEntityPipe],
      providers: [
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: BytesFormatPipe, useValue: bytesFormatPipeSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityCommentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.commentItem = PBD_ACTIVITIES[0];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
