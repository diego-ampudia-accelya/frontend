import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';
import moment from 'moment-mini';
import { Activity, ActivityType } from '~app/pbd/shared/pbd.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';

@Component({
  selector: 'bspl-pbd-activity-comment-item',
  templateUrl: './activity-comment-item.component.html',
  styleUrls: ['./activity-comment-item.component.scss']
})
export class ActivityCommentItemComponent implements OnInit {
  @Input() commentItem: Activity;

  @Output() download = new EventEmitter<{ attachment: any }>();

  constructor(private translationService: L10nTranslationService, private bytesFormatPipe: BytesFormatPipe) {}

  public ngOnInit() {
    this.commentItem = { ...this.commentItem, attachments: this.transformSize(this.commentItem?.attachments) };
  }

  public get header() {
    const item = this.commentItem;
    let action = '';

    if (item.type === ActivityType.Commented && item.reason.length === 0) {
      action = this.translationService.translate('postBillingDisputes.pbdDetails.activityLabels.ATTACHED');
    } else {
      action = this.translationService.translate(
        `postBillingDisputes.pbdDetails.activityLabels.${item?.type?.toUpperCase()}`
      );
    }

    return `${capitalize(this.commentItem.author?.type)} ${action}`;
  }

  public get time() {
    return moment(this.commentItem.creationDate).format('HH:mm');
  }

  public downloadAttachment(attachment: any) {
    const item: any = {
      fileId: attachment.id,
      activityId: this.commentItem.id
    };
    this.download.emit(item);
  }

  private transformSize(atachments: any[]): any[] {
    return atachments?.map(attach => ({ ...attach, size: this.bytesFormatPipe.transform(attach.size) }));
  }
}
