import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { PbdIssueFormComponent } from './pbd-issue-form.component';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('PbdIssueFormComponent', () => {
  let component: PbdIssueFormComponent;
  let fixture: ComponentFixture<PbdIssueFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PbdIssueFormComponent, TranslatePipeMock],
      providers: [FormBuilder, { provide: L10nTranslationService, useValue: translationServiceSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbdIssueFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
