import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';

import { Pbd } from '~app/pbd/shared/pbd.model';
import { TextareaComponent } from '~app/shared/components';
import { FileValidators } from '~app/shared/components/upload/file-validators';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { FormUtil } from '~app/shared/helpers';
import { appConfiguration } from '~app/shared/services';
import { blankStringValidator } from '~app/shared/validators/blank-string.validator';

@Component({
  selector: 'bspl-pbd-issue-form',
  templateUrl: './pbd-issue-form.component.html',
  styleUrls: ['./pbd-issue-form.component.scss']
})
export class PbdIssueFormComponent implements OnInit, AfterViewInit {
  public get amountLbl(): string {
    const currency = (this.pbd && this.pbd.currency.code) || '';

    return this.translationService.translate('postBillingDisputes.dialogs.pbdIssue.form.amount', { currency });
  }

  public uploadMode = UploadModes.Preview;

  public validationRules = [FileValidators.maxFileSize(), FileValidators.minFileSize()];

  public uploadPath = `${appConfiguration.baseUploadPath}/post-billing-dispute/files`;

  private _pbd: Pbd;
  @Input() set pbd(value: Pbd) {
    this._pbd = value;
    this.setPbdFormValue();
  }
  get pbd(): Pbd {
    return this._pbd;
  }

  @ViewChild(UploadComponent) uploadComponent: UploadComponent;

  @ViewChild(TextareaComponent, { static: true }) textArea: TextareaComponent;

  @Output() uploadFinished = new EventEmitter<any>();

  @Output() uploadStarted = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder, private translationService: L10nTranslationService) {}

  public form: FormGroup;

  public ngOnInit() {
    this.createForm();
  }

  public createForm() {
    this.form = this.formBuilder.group({
      pbd: [null],
      reason: ['', { validators: [Validators.required, blankStringValidator], updateOn: 'change' }],
      fileIds: [null]
    });
  }

  public ngAfterViewInit() {
    if (this.textArea) {
      this.textArea.focusElement();
    }
    this.setPbdFormValue();
  }

  public showControlState() {
    FormUtil.showControlState(this.form);
  }

  public onUploadStarted() {
    this.uploadStarted.emit();
  }

  public onUploadFinished() {
    this.uploadFinished.emit();
  }

  public uploadFiles(): Observable<any> {
    return this.uploadComponent.uploadFiles();
  }

  private setPbdFormValue() {
    if (this.form) {
      this.form.get('pbd').setValue(this._pbd);
    }
  }
}
