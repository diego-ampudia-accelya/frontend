import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';
import { Activity, ActivityType } from '~app/pbd/shared/pbd.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';

@Component({
  selector: 'bspl-process-card',
  templateUrl: './process-card.component.html',
  styleUrls: ['./process-card.component.scss']
})
export class ProcessCardComponent implements OnInit {
  @Input() activity: Activity;

  @Output() download = new EventEmitter<{ attachment: any }>();

  constructor(private translationService: L10nTranslationService, private bytesFormatPipe: BytesFormatPipe) {}

  public ngOnInit() {
    this.activity = { ...this.activity, attachments: this.transformSize(this.activity.attachments) };
  }

  public get subTitle() {
    if (this.isActivityOfTypeSystem) {
      return this.translationService.translate(`postBillingDisputes.pbdDetails.activityLabels.AUTOMATIC_UPDATE`);
    } else {
      return `${this.capitalizeWord(this.activity.author.type)} ${this.activity.author.iataCode}`;
    }
  }

  public get isActivityOfTypeSystem() {
    return this.activity.type === ActivityType.System;
  }

  public capitalizeWord(word: string): string {
    return capitalize(word);
  }

  public downloadAttachment(attachment: any) {
    const item: any = {
      fileId: attachment.id,
      activityId: this.activity.id
    };

    this.download.emit(item);
  }

  private transformSize(atachments: any[]): any[] {
    return atachments.map(attach => ({ ...attach, size: this.bytesFormatPipe.transform(attach.size) }));
  }
}
