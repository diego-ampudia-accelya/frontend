import { Component, EventEmitter, Input, Output } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';

import { TimeLeftForAgreementService } from '~app/pbd/services/time-left-for-agreement.service';
import { Activity, ActivityType, Pbd, PbdStatus } from '~app/pbd/shared/pbd.model';

@Component({
  selector: 'bspl-process-section',
  templateUrl: './process-section.component.html',
  styleUrls: ['./process-section.component.scss']
})
export class ProcessSectionComponent {
  @Input() pbd: Pbd;
  @Output() download = new EventEmitter<{ pbdId: number; fileId: number; activityId: number }>();

  public get hasTimeLeftForAgreement(): boolean {
    const status = this.pbd && this.pbd.status;

    return status === PbdStatus.PendingAgent || status === PbdStatus.PendingAirline;
  }

  public get timeLeftForAgreement() {
    if (this.pbd && this.pbd.id) {
      const timeLeft = this.timeLeftForAgreementService.getTimeLeftForAgreementText(
        this.pbd.timeLeftForAgreementInHours
      );
      const text = this.translationService.translate('postBillingDisputes.pbdDetails.messages.timeLeftForAgreement');

      return `${timeLeft} ${text}`;
    }
  }

  public get alertMessageType() {
    return this.pbd && this.pbd.timeLeftForAgreementInHours > 24 ? 'info' : 'warning';
  }

  constructor(
    private translationService: L10nTranslationService,
    private timeLeftForAgreementService: TimeLeftForAgreementService
  ) {}

  public getActivityTitle(activity: Activity): string {
    if (activity.type === ActivityType.System && activity.status === PbdStatus.AgreementNotReached) {
      return this.translationService.translate(`postBillingDisputes.pbdDetails.activityLabels.AGREEMENT_NOT_REACHED`);
    } else if (activity.type === ActivityType.System && activity.status === PbdStatus.AgreeToAirline) {
      return this.translationService.translate(`postBillingDisputes.pbdDetails.activityLabels.AGREE_TO_AIRLINE`);
    }
    const activityText = this.translationService.translate(
      `postBillingDisputes.pbdDetails.activityLabels.${activity.type.toUpperCase()}`
    );

    return `${capitalize(activity.author.type)} ${activityText}`;
  }

  public onDownload(attachment: any) {
    this.download.emit({ pbdId: this.pbd.id, ...attachment });
  }
}
