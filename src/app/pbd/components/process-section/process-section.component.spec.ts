import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcessSectionComponent } from './process-section.component';

xdescribe('ProcessSectionComponent', () => {
  let component: ProcessSectionComponent;
  let fixture: ComponentFixture<ProcessSectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProcessSectionComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
