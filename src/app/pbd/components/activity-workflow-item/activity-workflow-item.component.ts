import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';
import moment from 'moment-mini';
import { Activity, ActivityType } from '~app/pbd/shared/pbd.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';

@Component({
  selector: 'bspl-pbd-activity-workflow-item',
  templateUrl: './activity-workflow-item.component.html',
  styleUrls: ['./activity-workflow-item.component.scss']
})
export class ActivityWorkflowItemComponent implements OnInit {
  @Input() workflowItem: Activity;

  @Output() download = new EventEmitter<{ attachment: any }>();

  constructor(private translationService: L10nTranslationService, private bytesFormatPipe: BytesFormatPipe) {}

  public ngOnInit() {
    this.workflowItem = { ...this.workflowItem, attachments: this.transformSize(this.workflowItem?.attachments) };
  }

  public get header() {
    if (this.workflowItem?.type === ActivityType.System) {
      return this.translationService.translate(`postBillingDisputes.pbdDetails.activityLabels.SYSTEM`);
    }
    const action = this.translationService.translate(
      `postBillingDisputes.pbdDetails.activityLabels.${this.workflowItem?.type?.toUpperCase()}`
    );

    return `${capitalize(this.workflowItem.author?.type)} ${action}`;
  }

  public get time() {
    return moment(this.workflowItem.creationDate).format('HH:mm');
  }

  public get status() {
    const statusKey = this.workflowItem.status || 'UNKNOWN';

    return this.translationService.translate(`postBillingDisputes.statuses.${statusKey.toUpperCase()}`);
  }

  public downloadAttachment(attachment: any) {
    const item: any = {
      fileId: attachment.id,
      activityId: this.workflowItem.id
    };
    this.download.emit(item);
  }

  private transformSize(atachments: any[]): any[] {
    return atachments?.map(attach => ({ ...attach, size: this.bytesFormatPipe.transform(attach.size) }));
  }
}
