import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, identity } from 'lodash';

import { ActivityWorkflowItemComponent } from './activity-workflow-item.component';
import { ToPbdEntityPipe } from '~app/pbd/pipes/to-pbd-entity.pipe';
import { PBD_ACTIVITIES } from '~app/pbd/shared/pbd-mocks.constants';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

const translationServiceSpy = createSpyObject(L10nTranslationService);
translationServiceSpy.translate.and.callFake(identity);

describe('ActivityWorkflowItemComponent', () => {
  let component: ActivityWorkflowItemComponent;
  let fixture: ComponentFixture<ActivityWorkflowItemComponent>;

  const bytesFormatPipeSpy = createSpyObject(BytesFormatPipe);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityWorkflowItemComponent, BytesFormatPipe, TranslatePipeMock, ToPbdEntityPipe],
      providers: [
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: BytesFormatPipe, useValue: bytesFormatPipeSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityWorkflowItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.workflowItem = cloneDeep(PBD_ACTIVITIES[0]);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('get status()', () => {
    it('should return `unknown` message when status is missing', () => {
      component.workflowItem.status = null;

      expect(component.status).toEqual('postBillingDisputes.statuses.UNKNOWN');
    });
  });
});
