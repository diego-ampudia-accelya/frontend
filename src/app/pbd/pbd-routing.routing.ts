import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PbdResolver } from './guards/pbd.resolver';
import { PbdDetailsComponent } from '~app/pbd/containers/pbd-details/pbd-details.component';
import { PbdListComponent } from '~app/pbd/containers/pbd-list/pbd-list.component';
import { ROUTES } from '~app/shared/constants/routes';
import { LoggedUserTypeResolver } from '~app/shared/services/logged-user-type-resolver.service';

export function getPbdTabLabel(tabConfig: any): string {
  const transactionRelatedDocumentNumber = tabConfig.pbd.documentNumber;

  return `PBD - ${transactionRelatedDocumentNumber}`;
}

const routes: Routes = [
  {
    path: '',
    component: PbdListComponent,
    data: {
      tab: ROUTES.POST_BILLING_DISPUTE_LIST
    }
  },
  {
    path: ROUTES.POST_BILLING_DISPUTE_ISSUE.path,
    component: PbdDetailsComponent,
    data: {
      tab: ROUTES.POST_BILLING_DISPUTE_ISSUE
    },
    resolve: {
      pbd: PbdResolver,
      loggedUserType: LoggedUserTypeResolver
    }
  },
  {
    path: ROUTES.POST_BILLING_DISPUTE_DETAILS.path,
    component: PbdDetailsComponent,
    data: {
      tab: {
        ...ROUTES.POST_BILLING_DISPUTE_DETAILS,
        getTabLabel: getPbdTabLabel
      }
    },
    resolve: {
      loggedUserType: LoggedUserTypeResolver,
      pbd: PbdResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PbdRoutingModule {}
