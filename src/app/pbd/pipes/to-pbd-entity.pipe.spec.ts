import { ToPbdEntityPipe } from './to-pbd-entity.pipe';
import { UserType } from '~app/shared/models/user.model';

describe('ToPbdEntityPipe', () => {
  let pipe: ToPbdEntityPipe;

  beforeEach(() => {
    pipe = new ToPbdEntityPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  describe('transform', () => {
    it('should be able to convert item with id, code and name and provided userType', () => {
      const originalValue = {
        id: 123,
        iataCode: 'test code',
        name: 'test name'
      };

      expect(pipe.transform(originalValue, UserType.AGENT)).toEqual({
        ...originalValue,
        userType: UserType.AGENT
      });
    });

    it('should be able to convert item with id, code, organization, and type', () => {
      const originalValue = {
        id: 123,
        iataCode: 'test code',
        organization: 'test name',
        type: UserType.AGENT
      };

      expect(pipe.transform(originalValue)).toEqual({
        id: 123,
        iataCode: 'test code',
        name: 'test name',
        userType: UserType.AGENT
      });
    });

    it('should be able to convert item and take name with priority over organization', () => {
      const originalValue = {
        id: 123,
        iataCode: 'test code',
        name: 'test name',
        organization: 'test organization',
        type: UserType.AGENT
      };

      expect(pipe.transform(originalValue)).toEqual({
        id: 123,
        iataCode: 'test code',
        name: 'test name',
        userType: UserType.AGENT
      });
    });

    it('should be able to convert item and take provided argument for userType with priority', () => {
      const originalValue = {
        id: 123,
        iataCode: 'test code',
        name: 'test name',
        type: UserType.AGENT
      };

      expect(pipe.transform(originalValue, UserType.AIRLINE)).toEqual({
        id: 123,
        iataCode: 'test code',
        name: 'test name',
        userType: UserType.AIRLINE
      });
    });
  });
});
