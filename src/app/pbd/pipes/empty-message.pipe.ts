import { Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';

import { ROUTES } from '~app/shared/constants/routes';

@Pipe({
  name: 'emptyMessage'
})
export class EmptyMessagePipe implements PipeTransform {
  constructor(private translationService: L10nTranslationService, private router: Router) {}
  transform(value: any): string {
    // new pbd issuing
    if (this.router.url === ROUTES.POST_BILLING_DISPUTE_ISSUE.url) {
      return value && value.toString().length >= 0
        ? value
        : this.translationService.translate('postBillingDisputes.pbdDetails.generalLabels.notIssuedYet');
    }

    return value || '';
  }
}
