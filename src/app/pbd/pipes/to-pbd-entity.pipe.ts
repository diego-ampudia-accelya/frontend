import { Pipe, PipeTransform } from '@angular/core';

import { PbdEntity } from '~app/pbd/shared/pbd.model';
import { UserType } from '~app/shared/models/user.model';

@Pipe({
  name: 'toPbdEntity'
})
export class ToPbdEntityPipe implements PipeTransform {
  transform(value: any, userType?: UserType): PbdEntity {
    const { id, iataCode, name, localName, organization, type } = value || {};

    return {
      id,
      iataCode,
      name: name || localName || organization,
      userType: userType || type
    };
  }
}
