import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { PbdResolver } from './pbd.resolver';
import { PbdApiActions } from '~app/pbd/actions';
import { Pbd } from '~app/pbd/shared/pbd.model';

describe('PBD resolver', () => {
  let pbdResolver: PbdResolver;
  let activatedRouteMock: SpyObject<ActivatedRouteSnapshot>;
  let mockStore: MockStore;

  const pbdIssueMock: Pbd = {
    id: null,
    documentId: '55555',
    documentNumber: '5555',
    transactionCode: 'TKTT',
    airline: {
      id: 1,
      iataCode: '220',
      localName: 'Test'
    },
    agent: {
      id: 1,
      iataCode: '0000',
      name: 'Agent Test'
    },
    maximumDisputableAmount: 30
  };

  const initialState = {
    pbd: {
      pbdState: {
        ids: ['5555'],
        entities: {
          5555: {
            ...pbdIssueMock,
            id: 5555
          }
        },
        pbdIssueData: pbdIssueMock
      }
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PbdResolver, provideMockStore({ initialState })]
    });

    pbdResolver = TestBed.inject(PbdResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should be created', () => {
    expect(pbdResolver).toBeTruthy();
  });

  it('should return new empty pbd if there is no id in route', fakeAsync(() => {
    activatedRouteMock = createSpyObject(ActivatedRouteSnapshot, { params: {} });

    pbdResolver.resolve(activatedRouteMock).subscribe(pbd => {
      expect(pbd).toEqual(pbdIssueMock);
    });
  }));

  it('should dispatch action to request and load pbd in store  ', fakeAsync(() => {
    activatedRouteMock = createSpyObject(ActivatedRouteSnapshot, { params: { id: 5555 } });
    spyOn(mockStore, 'dispatch');

    pbdResolver.resolve(activatedRouteMock).subscribe();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(PbdApiActions.requestPbd({ id: 5555 }));
  }));

  it('should return loaded pbd from store ', fakeAsync(() => {
    activatedRouteMock = createSpyObject(ActivatedRouteSnapshot, { params: { id: 5555 } });
    spyOn(mockStore, 'dispatch');

    pbdResolver.resolve(activatedRouteMock).subscribe(pbd => {
      expect(pbd).toEqual({ ...pbdIssueMock, id: 5555 });
    });
  }));
});
