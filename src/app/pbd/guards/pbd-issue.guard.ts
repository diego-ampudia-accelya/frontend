import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { first, map, tap } from 'rxjs/operators';

import { getPbdIssueData } from '~app/pbd/reducers';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';

@Injectable()
export class PbdIssueGuard implements CanActivate {
  public pbdIssueData$ = this.store.pipe(select(getPbdIssueData));

  constructor(private store: Store<AppState>, private router: Router) {}

  canActivate() {
    return this.pbdIssueData$.pipe(
      tap(pbdIssueData => {
        if (!pbdIssueData) {
          this.router.navigateByUrl(ROUTES.POST_BILLING_DISPUTE_LIST.url);
        }
      }),
      map(Boolean),
      first()
    );
  }
}
