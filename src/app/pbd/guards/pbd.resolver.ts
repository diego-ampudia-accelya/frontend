import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { PbdApiActions } from '~app/pbd/actions';
import { getPbd } from '~app/pbd/reducers';
import { Pbd } from '~app/pbd/shared/pbd.model';
import { AppState } from '~app/reducers';

@Injectable()
export class PbdResolver implements Resolve<Pbd> {
  constructor(private store: Store<AppState>) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<Pbd> {
    const { id } = route.params;

    if (id) {
      this.store.dispatch(PbdApiActions.requestPbd({ id }));
    }

    return this.store.pipe(select(getPbd, { id }), filter(Boolean), first()) as Observable<Pbd>;
  }
}
