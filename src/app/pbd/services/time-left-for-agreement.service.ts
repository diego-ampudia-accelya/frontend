import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

@Injectable({
  providedIn: 'root'
})
export class TimeLeftForAgreementService {
  constructor(private translationService: L10nTranslationService) {}

  public getTimeLeftForAgreementText(timeLeftInHours: number): string {
    const daysLabel = this.translationService.translate('postBillingDisputes.pbdDetails.generalLabels.days');
    const hoursLabel = this.translationService.translate('postBillingDisputes.pbdDetails.generalLabels.hours');
    const dayLabel = this.translationService.translate('postBillingDisputes.pbdDetails.generalLabels.day');
    const hourLabel = this.translationService.translate('postBillingDisputes.pbdDetails.generalLabels.hour');
    const timeLeftInDays = Math.floor(timeLeftInHours / 24);

    if (timeLeftInHours >= 0 && timeLeftInHours < 1) {
      return this.translationService.translate('postBillingDisputes.pbdDetails.generalLabels.lessThanHour');
    } else if (timeLeftInHours === 1) {
      return `${timeLeftInHours} ${hourLabel}`;
    } else if (timeLeftInHours > 1 && timeLeftInHours < 24) {
      return `${timeLeftInHours} ${hoursLabel}`;
    } else if (timeLeftInDays === 1) {
      return `${timeLeftInDays} ${dayLabel}`;
    } else if (timeLeftInDays > 1) {
      return `${timeLeftInDays} ${daysLabel}`;
    }

    return '';
  }
}
