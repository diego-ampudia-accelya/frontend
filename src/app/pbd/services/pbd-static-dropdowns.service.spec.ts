import { inject, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { PbdStaticDropdownsService } from './pbd-static-dropdowns.service';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('Service: PbdStaticDropdowns', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PbdStaticDropdownsService, { provide: L10nTranslationService, useValue: translationServiceSpy }]
    });
  });

  it('should ...', inject([PbdStaticDropdownsService], (service: PbdStaticDropdownsService) => {
    expect(service).toBeTruthy();
  }));
});
