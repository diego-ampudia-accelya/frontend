import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  Activity,
  ActivityFilterType,
  ActivityType,
  DocumentMessages,
  Pbd,
  PbdIssueModel,
  PbdSearchForm,
  PbdSearchFormBE,
  TicketModel
} from '~app/pbd/shared/pbd.model';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';
import { ApiService } from '~app/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class PbdService extends ApiService<Pbd> {
  constructor(
    http: HttpClient,
    private translationService: L10nTranslationService,
    private appConfiguration: AppConfigurationService
  ) {
    super(http, 'pbd');
    this.baseUrl = `${this.appConfiguration.baseApiPath}/post-billing-dispute/pbds`;
  }

  // eslint-disable-next-line @typescript-eslint/member-ordering
  private sortMapper = [
    { from: 'agent.bsp.isoCountryCode', to: 'bspIsoCountryCode' },
    { from: 'airline.iataCode', to: 'airlineIataCode' },
    { from: 'airline.localName', to: 'airlineLocalName' },
    { from: 'currency.code', to: 'currencyCode' },
    { from: 'agent.iataCode', to: 'agentIataCode' },
    { from: 'agent.name', to: 'agentName' },
    { from: 'translatedPbdStatus', to: 'status' }
  ];

  public find(query: DataQuery<PbdSearchForm>): Observable<PagedData<Pbd>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<Pbd>>(this.baseUrl + requestQuery.getQueryString()).pipe(
      map(items => {
        items.records.forEach(item => {
          item.translatedPbdStatus = this.translationService.translate(`postBillingDisputes.statuses.${item.status}`);

          return item.translatedPbdStatus;
        });

        return items;
      })
    );
  }

  public download(
    query: DataQuery<PbdSearchForm>,
    format: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}/download`;
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, type: format.toUpperCase() };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(url + requestQuery.getQueryString({ omitPaging: true }), downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  public getBy(documentNumber: string): Observable<TicketModel[]> {
    return this.http.get<TicketModel[]>(`${this.appConfiguration.baseApiPath}/post-billing-dispute/documents`, {
      params: { documentNumber }
    });
  }

  public create(pbdIssueData: Pbd, reason: string, fileIds: string[]): Observable<Pbd> {
    if (!pbdIssueData || !reason) {
      return of(null);
    }
    const requestData = this.toPbdIssueModel(pbdIssueData, reason, fileIds);

    return this.save(requestData) as Observable<Pbd>;
  }

  public getMessage(msgCode: DocumentMessages): string {
    let msg = '';

    switch (msgCode) {
      case DocumentMessages.NoResults:
        msg = this.translationService.translate('postBillingDisputes.dialogs.pbdStart.form.msg.error.noResults');
        break;
      case DocumentMessages.AlreadyDisputed:
        msg = this.translationService.translate('postBillingDisputes.dialogs.pbdStart.form.msg.error.disputed');
        break;
      case DocumentMessages.NotRelated:
        msg = this.translationService.translate('postBillingDisputes.dialogs.pbdStart.form.msg.error.notRelated');
        break;
      case DocumentMessages.ExceedDisputePeriod:
        msg = this.translationService.translate('postBillingDisputes.dialogs.pbdStart.form.msg.error.older');
        break;
      case DocumentMessages.SuspendedArline:
        msg = this.translationService.translate('postBillingDisputes.dialogs.pbdStart.form.msg.error.suspended');
        break;
      case DocumentMessages.DocumentNotMatch:
        msg = this.translationService.translate('postBillingDisputes.dialogs.pbdStart.form.msg.error.notMatch');
        break;
      default:
        msg = this.translationService.translate('GENERIC_HTTP_ERROR');
        break;
    }

    return msg;
  }

  public getPbdById(id: number): Observable<Pbd> {
    return this.getOne(id).pipe(
      map(pbd => {
        pbd.activities = this.normalizeAuthorType(pbd.activities);
        pbd.workflowActivities = this.sortWorkflowActivitiesByDateAscending(pbd.activities);
        pbd.activities = this.sortActivitiesByDateDescending(pbd.activities);
        pbd.translatedPbdStatus = this.translationService.translate(`postBillingDisputes.statuses.${pbd.status}`);

        return pbd;
      })
    );
  }

  public getFilteredActivities(activities: Activity[], filterValue: ActivityFilterType) {
    return (activities || []).filter(activity => {
      const activityType = activity.type;

      switch (filterValue) {
        case ActivityFilterType.Comments:
          return activityType === ActivityType.Commented;
        case ActivityFilterType.Workflow:
          return (
            activityType === ActivityType.Issued ||
            activityType === ActivityType.Agreed ||
            activityType === ActivityType.Disagreed ||
            activityType === ActivityType.System
          );
        case ActivityFilterType.Attachments:
          return (activity.attachments || []).length > 0;
        default:
          return true;
      }
    });
  }

  private formatQuery(query: DataQuery<PbdSearchForm>): RequestQuery<PbdSearchFormBE> {
    const { bsp, airlineCodes, agentCodes, pbdCreationDate, pbdResolutionDate, currencyCode, ...rest } = query.filterBy;

    return RequestQuery.fromDataQuery<PbdSearchFormBE>({
      ...query,
      sortBy: query.sortBy && formatSortBy(query.sortBy, this.sortMapper),
      filterBy: {
        bspIsoCountryCode: bsp && bsp.map(item => item.isoCountryCode),
        airlineIataCode: airlineCodes && airlineCodes.map(item => item.code),
        agentIataCode: agentCodes && agentCodes.map(item => item.code),
        currencyCode: currencyCode && currencyCode.map(item => item.code),
        pbdDateFrom: pbdCreationDate && toShortIsoDate(pbdCreationDate[0]),
        pbdDateTo: pbdCreationDate && toShortIsoDate(pbdCreationDate[1] ? pbdCreationDate[1] : pbdCreationDate[0]),
        resolutionDateFrom: pbdResolutionDate && toShortIsoDate(pbdResolutionDate[0]),
        resolutionDateTo:
          pbdResolutionDate && toShortIsoDate(pbdResolutionDate[1] ? pbdResolutionDate[1] : pbdResolutionDate[0]),
        ...rest
      }
    });
  }

  private toPbdIssueModel(pbd: Pbd, reason: string, fileIds: string[]): PbdIssueModel {
    const { documentId, pbdAmount, airline, transactionCode, period, dateOfIssue } = pbd;

    return {
      documentId,
      pbdAmount,
      reason,
      airlineCode: airline.iataCode,
      transactionCode,
      period,
      dateOfIssue,
      attachments: fileIds
    };
  }

  private sortActivitiesByDateDescending(activities: Activity[]): Activity[] {
    return [...(activities || [])].sort((a, b) => {
      const aCreationDate = new Date(a.creationDate);
      const bCreationDate = new Date(b.creationDate);

      return bCreationDate.getTime() - aCreationDate.getTime();
    });
  }

  private sortWorkflowActivitiesByDateAscending(activities: Activity[]) {
    if (activities && activities.length > 0) {
      return activities
        .filter(activity => activity.type.toUpperCase() !== ActivityType.Commented)
        .sort((a, b) => {
          const aCreationDate = new Date(a.creationDate);
          const bCreationDate = new Date(b.creationDate);

          return aCreationDate.getTime() - bCreationDate.getTime();
        });
    }

    return [];
  }

  private normalizeAuthorType(activities: Activity[]): Activity[] {
    return (activities || []).map(activity => {
      if (activity.author) {
        activity.author.type = activity.author.type.toLowerCase() as UserType;
      }

      return activity;
    });
  }
}
