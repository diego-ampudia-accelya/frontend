import { TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { TimeLeftForAgreementService } from './time-left-for-agreement.service';

const lessThanHourText = 'postBillingDisputes.pbdDetails.generalLabels.lessThanHour';
const hourText = 'postBillingDisputes.pbdDetails.generalLabels.hour';
const hoursText = 'postBillingDisputes.pbdDetails.generalLabels.hours';
const dayText = 'postBillingDisputes.pbdDetails.generalLabels.day';
const daysText = 'postBillingDisputes.pbdDetails.generalLabels.days';

describe('TimeLeftForAgreementService', () => {
  let service: TimeLeftForAgreementService;
  let translationServiceSpy: SpyObject<L10nTranslationService>;

  beforeEach(() => {
    translationServiceSpy = createSpyObject(L10nTranslationService);
    translationServiceSpy.translate.and.callFake((key: string) => key);

    TestBed.configureTestingModule({
      providers: [TimeLeftForAgreementService, { provide: L10nTranslationService, useValue: translationServiceSpy }]
    });

    service = TestBed.inject(TimeLeftForAgreementService);
  });

  it('should create service', () => {
    expect(service).toBeTruthy();
  });

  it('should return proper text when the value is 0', () => {
    const result = service.getTimeLeftForAgreementText(0);

    expect(result).toEqual(lessThanHourText);
  });

  it('should return proper text when the value is between 0 and 1', () => {
    const result = service.getTimeLeftForAgreementText(0.9);

    expect(result).toEqual(lessThanHourText);
  });

  it('should return proper text when the value is 1', () => {
    const result = service.getTimeLeftForAgreementText(1);

    expect(result).toEqual(`1 ${hourText}`);
  });

  it('should return proper text when the value is between 1 and 24', () => {
    const result = service.getTimeLeftForAgreementText(23);

    expect(result).toEqual(`23 ${hoursText}`);
  });

  it('should return proper text when the value is 24', () => {
    const result = service.getTimeLeftForAgreementText(24);

    expect(result).toEqual(`1 ${dayText}`);
  });

  it('should return proper text when the value is more than 24', () => {
    const result = service.getTimeLeftForAgreementText(240);

    expect(result).toEqual(`10 ${daysText}`);
  });

  it('should return empty string for unsupported value', () => {
    const result = service.getTimeLeftForAgreementText(-10);

    expect(result).toEqual('');
  });
});
