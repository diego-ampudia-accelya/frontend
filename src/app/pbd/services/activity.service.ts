import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Activity, ActivityType, AgreementType } from '~app/pbd/shared/pbd.model';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  private baseUrl = `${this.appConfigurationService.baseApiPath}/post-billing-dispute/pbds/`;

  constructor(private http: HttpClient, private appConfigurationService: AppConfigurationService) {}

  public create(
    pbdId: number,
    activityType: ActivityType,
    agreementType: AgreementType,
    reason: string,
    attachments: string[]
  ): Observable<Activity> {
    return this.http.post<Activity>(
      `${this.baseUrl}${pbdId}/activities`,
      {
        type: activityType || this.toActivityType(agreementType),
        reason,
        attachments
      },
      {
        params: { showErrorMessage: 'false' }
      }
    );
  }

  public update(payload: Activity): Observable<Activity> {
    const opt = {
      params: {
        showErrorMessage: 'false'
      }
    };

    return this.http.put<Activity>(`${this.baseUrl}${payload.pbdId}/activities`, payload, opt);
  }

  public download({ pbdId, fileId, activityId }): Observable<any> {
    const url = `${this.baseUrl}${pbdId}/activities/${activityId}/attachments/${fileId}`;

    return this.http.get(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private toActivityType(agreementType: AgreementType): ActivityType {
    return agreementType === AgreementType.AgreeWithAirline || agreementType === AgreementType.AgreeWithAgent
      ? ActivityType.Agreed
      : ActivityType.Disagreed;
  }
}
