import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { of } from 'rxjs';

import { PbdService } from './pbd.service';
import { Activity, ActivityType, DocumentMessages, Pbd, PbdIssueModel, PbdStatus } from '~app/pbd/shared/pbd.model';
import { ActivityFilterType } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('PbdService', () => {
  let service: PbdService;
  let httpSpy: SpyObject<HttpClient>;
  let translationSpy: SpyObject<L10nTranslationService>;

  const pbd: Pbd = {
    id: 123,
    documentId: 'document-id',
    documentNumber: 'document-number',
    transactionCode: 'TKTT',
    airline: { id: 1, iataCode: '007', localName: 'Secret Airlines' },
    agent: { id: 1, iataCode: '02345', name: 'James Bond' },
    bsp: {
      id: 1,
      name: 'Great Britain',
      isoCountryCode: 'UK'
    },
    status: PbdStatus.AgreementNotReached,
    maximumDisputableAmount: 100,
    pbdAmount: 50,
    dateOfIssue: '2020-06-01',
    period: 2020061,
    activities: [
      { pbdId: 123, id: 1, type: ActivityType.Disagreed, creationDate: moment('2020-06-03').toDate() },
      { pbdId: 123, id: 2, type: ActivityType.Issued, creationDate: moment('2020-06-01').toDate() },
      { pbdId: 123, id: 3, type: ActivityType.System, creationDate: moment('2020-06-02').toDate() }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PbdService,
        mockProvider(HttpClient),
        mockProvider(L10nTranslationService),
        mockProvider(AppConfigurationService, { baseApiPath: 'api' })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(PbdService);

    httpSpy = TestBed.inject<any>(HttpClient);
    httpSpy.get.and.returnValue(of());
    httpSpy.post.and.returnValue(of());
    httpSpy.put.and.returnValue(of());

    translationSpy = TestBed.inject<any>(L10nTranslationService);
    translationSpy.translate.and.callFake((key: string) => `translated-${key}`);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    it('should send proper request', fakeAsync(() => {
      const query: DataQuery = {
        filterBy: {
          transactionCode: ['TKTT']
        },
        sortBy: [{ attribute: 'translatedPbdStatus', sortType: SortOrder.Asc }],
        paginateBy: { page: 0, size: 20 }
      };
      httpSpy.get.and.returnValue(of({ records: [] }));

      service.find(query).subscribe();
      const expectedUrl = 'api/post-billing-dispute/pbds?page=0&size=20&sort=status,ASC&transactionCode=TKTT';

      expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
    }));

    it('should translate pbd status', fakeAsync(() => {
      const query: DataQuery = {
        filterBy: {},
        sortBy: [],
        paginateBy: { page: 0, size: 20 }
      };
      httpSpy.get.and.returnValue(of({ records: [pbd] }));

      let actual: PagedData<Pbd>;
      service.find(query).subscribe(result => (actual = result));

      expect(actual.records as any[]).toEqual([
        jasmine.objectContaining({
          id: 123,
          translatedPbdStatus: 'translated-postBillingDisputes.statuses.AGREEMENT_NOT_REACHED'
        })
      ]);
    }));
  });

  describe('download', () => {
    it('should send proper request when query criterion are provided', fakeAsync(() => {
      const query: DataQuery = {
        filterBy: {
          transactionCode: ['TKTT', 'RFND']
        },
        paginateBy: { size: 20, page: 0 },
        sortBy: []
      };

      const exportOptions: DownloadFormat = DownloadFormat.CSV;
      const expectedUrl = 'api/post-billing-dispute/pbds/download?transactionCode=TKTT,RFND&type=CSV';

      service.download(query, exportOptions).subscribe();

      expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));

    it('should send proper request when an empty query is provided', fakeAsync(() => {
      const query: DataQuery = {
        filterBy: [],
        paginateBy: { size: 20, page: 0 },
        sortBy: []
      };

      const exportOptions: DownloadFormat = DownloadFormat.CSV;
      const expectedUrl = 'api/post-billing-dispute/pbds/download?type=CSV';

      service.download(query, exportOptions).subscribe();

      expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));
  });

  describe('getBy', () => {
    it('should send a correct request', fakeAsync(() => {
      const documentNumber = '1234567';

      service.getBy(documentNumber).subscribe();

      expect(httpSpy.get).toHaveBeenCalledWith('api/post-billing-dispute/documents', { params: { documentNumber } });
    }));
  });

  describe('create', () => {
    it('should return null when reason is not provided', fakeAsync(() => {
      let actual: Pbd;
      service.create({} as Pbd, null, []).subscribe(result => (actual = result));

      expect(actual).toBe(null);
    }));

    it('should return null when pbdIssueData is not provided', fakeAsync(() => {
      let actual: Pbd;
      service.create(null, 'reason', []).subscribe(result => (actual = result));

      expect(actual).toBe(null);
    }));

    it('should map pbdIssueData', fakeAsync(() => {
      const newPbd: Pbd = { ...pbd, id: null };
      service.create(newPbd, 'reason', ['1', '2', '3']).subscribe();
      const expected: PbdIssueModel = {
        documentId: 'document-id',
        pbdAmount: 50,
        reason: 'reason',
        airlineCode: '007',
        transactionCode: 'TKTT',
        period: 2020061,
        dateOfIssue: '2020-06-01',
        attachments: ['1', '2', '3']
      };

      expect(httpSpy.post).toHaveBeenCalledWith('api/post-billing-dispute/pbds', expected);
    }));
  });

  describe('getMessage', () => {
    it('should return translated message when message code is `NoResults`', () => {
      expect(service.getMessage(DocumentMessages.NoResults)).toBe(
        'translated-postBillingDisputes.dialogs.pbdStart.form.msg.error.noResults'
      );
    });

    it('should return translated message when message code is `AlreadyDisputed`', () => {
      expect(service.getMessage(DocumentMessages.AlreadyDisputed)).toBe(
        'translated-postBillingDisputes.dialogs.pbdStart.form.msg.error.disputed'
      );
    });

    it('should return translated message when message code is `NotRelated`', () => {
      expect(service.getMessage(DocumentMessages.NotRelated)).toBe(
        'translated-postBillingDisputes.dialogs.pbdStart.form.msg.error.notRelated'
      );
    });

    it('should return translated message when message code is `ExceedDisputePeriod`', () => {
      expect(service.getMessage(DocumentMessages.ExceedDisputePeriod)).toBe(
        'translated-postBillingDisputes.dialogs.pbdStart.form.msg.error.older'
      );
    });

    it('should return translated message when message code is `SuspendedArline`', () => {
      expect(service.getMessage(DocumentMessages.SuspendedArline)).toBe(
        'translated-postBillingDisputes.dialogs.pbdStart.form.msg.error.suspended'
      );
    });

    it('should return translated message when message code is `DocumentNotMatch`', () => {
      expect(service.getMessage(DocumentMessages.DocumentNotMatch)).toBe(
        'translated-postBillingDisputes.dialogs.pbdStart.form.msg.error.notMatch'
      );
    });

    it('should return translated message when message code is not known', () => {
      expect(service.getMessage(-1 as DocumentMessages)).toBe('translated-GENERIC_HTTP_ERROR');
    });
  });

  describe('getById', () => {
    it('should send a proper request', fakeAsync(() => {
      service.getPbdById(123).subscribe();

      expect(httpSpy.get).toHaveBeenCalledWith('api/post-billing-dispute/pbds/123');
    }));

    it('should sort activities by date descending', fakeAsync(() => {
      httpSpy.get.and.returnValue(of(pbd));

      let actual: Pbd;
      service.getPbdById(123).subscribe(result => (actual = result));

      expect(actual.activities as any[]).toEqual([
        jasmine.objectContaining({ id: 1 }),
        jasmine.objectContaining({ id: 3 }),
        jasmine.objectContaining({ id: 2 })
      ]);
    }));

    it('should sort workflow activities by date ascending', fakeAsync(() => {
      httpSpy.get.and.returnValue(of(pbd));

      let actual: Pbd;
      service.getPbdById(123).subscribe(result => (actual = result));

      expect(actual.workflowActivities as any[]).toEqual([
        jasmine.objectContaining({ id: 2 }),
        jasmine.objectContaining({ id: 3 }),
        jasmine.objectContaining({ id: 1 })
      ]);
    }));

    it('should exclude activities with status Commented from workflow activities', fakeAsync(() => {
      const pbdWithComments: Pbd = {
        ...pbd,
        activities: [
          ...pbd.activities,
          { pbdId: 123, id: 4, type: ActivityType.Commented, creationDate: moment('2020-06-02').toDate() }
        ]
      };
      httpSpy.get.and.returnValue(of(pbdWithComments));

      let actual: Pbd;
      service.getPbdById(123).subscribe(result => (actual = result));

      expect(actual.workflowActivities as any[]).toEqual([
        jasmine.objectContaining({ id: 2 }),
        jasmine.objectContaining({ id: 3 }),
        jasmine.objectContaining({ id: 1 })
      ]);
    }));

    it('should return empty activities and workflow activities when no activities exist', fakeAsync(() => {
      const pbdWithoutActivities: Pbd = { ...pbd, activities: null };
      httpSpy.get.and.returnValue(of(pbdWithoutActivities));

      service.getPbdById(123).subscribe(result => {
        expect(result.activities).toEqual([]);
        expect(result.workflowActivities).toEqual([]);
      });
    }));

    it('should translate status', fakeAsync(() => {
      httpSpy.get.and.returnValue(of(pbd));

      let actual: Pbd;
      service.getPbdById(123).subscribe(result => (actual = result));

      expect(actual.translatedPbdStatus).toBe('translated-postBillingDisputes.statuses.AGREEMENT_NOT_REACHED');
    }));
  });

  describe('getFilteredActivities', () => {
    const activities: Activity[] = [
      { pbdId: 123, id: 1, type: ActivityType.Disagreed, creationDate: moment('2020-06-03').toDate() },
      { pbdId: 123, id: 2, type: ActivityType.Issued, creationDate: moment('2020-06-01').toDate() },
      { pbdId: 123, id: 3, type: ActivityType.System, creationDate: moment('2020-06-02').toDate() },
      {
        pbdId: 123,
        id: 4,
        type: ActivityType.Commented,
        creationDate: moment('2020-06-04').toDate(),
        attachments: [{}]
      },
      { pbdId: 123, id: 5, type: ActivityType.Agreed, creationDate: moment('2020-06-05').toDate() }
    ];

    it('should return all activities', () => {
      const actual = service.getFilteredActivities(activities, ActivityFilterType.All);
      expect(actual.length).toBe(5);
    });

    it('should return only comments', () => {
      const actual = service.getFilteredActivities(activities, ActivityFilterType.Comments);
      expect(actual.length).toBe(1);
    });

    it('should return all workflow activities', () => {
      const actual = service.getFilteredActivities(activities, ActivityFilterType.Workflow);
      expect(actual.length).toBe(4);
    });

    it('should return all activities with attachments', () => {
      const actual = service.getFilteredActivities(activities, ActivityFilterType.Attachments);
      expect(actual.length).toBe(1);
    });

    it('should return an empty array when no activities are provided', () => {
      const actual = service.getFilteredActivities(null, ActivityFilterType.All);
      expect(actual).toEqual([]);
    });
  });
});
