import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { ActivityService } from './activity.service';
import { Activity, ActivityType, AgreementType } from '~app/pbd/shared/pbd.model';
import { AppConfigurationService } from '~app/shared/services';

const baseUrl = '/post-billing-dispute/pbds/';

describe('ActivityService', () => {
  let service: ActivityService;
  let httpClientSpy: SpyObject<HttpClient>;
  let appConfigurationServiceSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    appConfigurationServiceSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        ActivityService,
        mockProvider(HttpClient),
        { provide: AppConfigurationService, useValue: appConfigurationServiceSpy }
      ]
    });

    service = TestBed.inject(ActivityService);
    httpClientSpy = TestBed.inject<any>(HttpClient);

    httpClientSpy.post.and.returnValue(of({}));
    httpClientSpy.put.and.returnValue(of({}));
    httpClientSpy.get.and.returnValue(of({}));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('create()', () => {
    it('should send post request with proper url', fakeAsync(() => {
      service.create(1, null, null, null, []).subscribe();
      const expectedUrl = `${baseUrl}1/activities`;

      expect(httpClientSpy.post).toHaveBeenCalledWith(expectedUrl, jasmine.anything(), jasmine.anything());
    }));

    it('should send post request with proper request options', fakeAsync(() => {
      service.create(1, null, null, null, []).subscribe();
      const expectedOptions = { params: { showErrorMessage: 'false' } };

      expect(httpClientSpy.post).toHaveBeenCalledWith(jasmine.anything(), jasmine.anything(), expectedOptions);
    }));

    it('should send post request with proper data when called with `activityType`', fakeAsync(() => {
      const expectedURL = '/post-billing-dispute/pbds/1/activities';
      const expectedData = {
        type: ActivityType.Agreed,
        reason: 'reason',
        attachments: ['1', '2']
      };
      const expectedParams = {
        params: {
          showErrorMessage: 'false'
        }
      };

      service.create(1, ActivityType.Agreed, AgreementType.DisagreeWithAgent, 'reason', ['1', '2']).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(expectedURL, expectedData, expectedParams);
    }));

    it('should send post request with proper data when called with `agreementType` `Disagreed*` and without `activityType`', fakeAsync(() => {
      const expectedURL = '/post-billing-dispute/pbds/1/activities';
      const expectedData = {
        type: ActivityType.Disagreed,
        reason: 'reason',
        attachments: ['1', '2']
      };
      const expectedParams = {
        params: {
          showErrorMessage: 'false'
        }
      };

      service.create(1, null, AgreementType.DisagreeWithAgent, 'reason', ['1', '2']).subscribe();

      expect(httpClientSpy.post).toHaveBeenCalledWith(expectedURL, expectedData, expectedParams);
    }));

    it('should send post request with proper data when called with `agreementType` `Agreed*` and without `activityType`', fakeAsync(() => {
      const expectedURL = '/post-billing-dispute/pbds/1/activities';
      const expectedData = {
        type: ActivityType.Agreed,
        reason: 'reason',
        attachments: ['1', '2']
      };
      const expectedParams = {
        params: {
          showErrorMessage: 'false'
        }
      };

      service.create(1, null, AgreementType.AgreeWithAirline, 'reason', ['1', '2']).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(expectedURL, expectedData, expectedParams);
    }));
  });

  describe('update()', () => {
    it('should send put request with proper url', fakeAsync(() => {
      service.update({ pbdId: 1 } as Activity).subscribe();
      const expectedUrl = `${baseUrl}1/activities`;

      expect(httpClientSpy.put).toHaveBeenCalledWith(expectedUrl, jasmine.anything(), jasmine.anything());
    }));

    it('should send put request with proper request options', fakeAsync(() => {
      service.update({} as Activity).subscribe();
      const expectedOptions = { params: { showErrorMessage: 'false' } };

      expect(httpClientSpy.put).toHaveBeenCalledWith(jasmine.anything(), jasmine.anything(), expectedOptions);
    }));

    it('should send put request with proper data', fakeAsync(() => {
      const payload = {
        pbdId: 1,
        reason: 'reason'
      } as Activity;

      service.update(payload).subscribe();

      expect(httpClientSpy.put).toHaveBeenCalledWith(jasmine.anything(), payload, jasmine.anything());
    }));
  });

  describe('download()', () => {
    it('should send proper request when download() is called', fakeAsync(() => {
      const expectedUrl = '/post-billing-dispute/pbds/1/activities/3/attachments/2';

      service.download({ pbdId: 1, fileId: 2, activityId: 3 }).subscribe();

      expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));
  });
});
