import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Injectable({
  providedIn: 'root'
})
export class PbdStaticDropdownsService {
  constructor(private translationService: L10nTranslationService) {}
  getPendingStatusesDropdownOptions(): DropdownOption[] {
    return [
      {
        value: 'PENDING_AIRLINE',
        label: this.translationService.translate('postBillingDisputes.statuses.PENDING_AIRLINE')
      },
      {
        value: 'PENDING_AGENT',
        label: this.translationService.translate('postBillingDisputes.statuses.PENDING_AGENT')
      }
    ];
  }

  getFinalizedStatusesDropdownOptions(): DropdownOption[] {
    return [
      {
        value: 'AGREE_TO_AIRLINE',
        label: this.translationService.translate('postBillingDisputes.statuses.AGREE_TO_AIRLINE')
      },
      {
        value: 'AGREE_TO_AGENT',
        label: this.translationService.translate('postBillingDisputes.statuses.AGREE_TO_AGENT')
      },
      {
        value: 'AGREEMENT_NOT_REACHED',
        label: this.translationService.translate('postBillingDisputes.statuses.AGREEMENT_NOT_REACHED')
      }
    ];
  }

  getTransactionCodesDropdownOptions(): DropdownOption[] {
    return [
      {
        value: 'TKTT',
        label: 'TKTT'
      },
      {
        value: 'EMDA',
        label: 'EMDA'
      },
      {
        value: 'EMDS',
        label: 'EMDS'
      },
      {
        value: 'ADMA',
        label: 'ADMA'
      }
    ];
  }
}
