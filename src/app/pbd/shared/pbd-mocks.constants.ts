import { Activity, ActivityType, Pbd, PbdSearchForm, PbdStatus } from './pbd.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { UserType } from '~app/shared/models/user.model';

export const PBDS_IATA_AGENT: Pbd[] = [
  {
    id: 1,
    documentId: '3698516642',
    documentNumber: '3698516642',
    transactionCode: 'TKTT',
    airline: {
      id: 1,
      iataCode: '220',
      localName: 'Lufthansa'
    },
    agent: {
      id: 1,
      iataCode: '8120317',
      name: 'TUI/FLEX FLIGHT CENTER ZRH',
      bsp: { id: 12 }
    },
    bsp: {
      id: 1,
      name: 'Germany',
      isoCountryCode: 'GER'
    },
    status: PbdStatus.PendingAirline,
    pbdDate: '2021-10-01',
    resolutionDate: '2022-10-01',
    timeLeftForAgreementInHours: 11,
    currency: { id: 23, code: 'EU', decimals: 3 },
    maximumDisputableAmount: 1902.55,
    pbdAmount: 1800,
    dateOfIssue: new Date().toISOString(),
    activities: [
      {
        id: 1,
        pbdId: 1,
        status: PbdStatus.PendingAirline,
        author: {
          id: 1,
          type: UserType.AGENT,
          iataCode: '8120196',
          organization: 'Peter Robinson Switzerland Ltd'
        },
        reason:
          'Slow unstable internet connection triggered a transaction failure. During the transaction prices have changed.',
        attachments: [
          {
            name: 'TicketAirline',
            type: 'pdf',
            size: 5265734
          },
          {
            name: 'Invoice',
            type: 'pdf',
            size: 3426756
          }
        ],
        creationDate: new Date(2019, 7, 4, 1, 6),
        type: ActivityType.Issued
      }
    ]
  },
  {
    id: 2,
    documentNumber: '3698516642',
    documentId: '3698516642',
    transactionCode: 'TKTT',
    airline: {
      id: 1,
      iataCode: '220',
      localName: 'Lufthansa'
    },
    agent: {
      id: 1,
      iataCode: '8120317',
      name: 'TUI/FLEX FLIGHT CENTER ZRH',
      bsp: { id: 3 }
    },
    bsp: {
      id: 1,
      name: 'Germany',
      isoCountryCode: 'GER'
    },
    status: PbdStatus.AgreeToAgent,
    pbdDate: '2021-10-02',
    resolutionDate: '2022-10-01',
    timeLeftForAgreementInHours: 11,
    currency: { id: 23, code: 'US', decimals: 3 },
    maximumDisputableAmount: 1902.55,
    pbdAmount: 1800,
    dateOfIssue: new Date().toISOString(),
    activities: [
      {
        id: 1,
        pbdId: 2,
        status: PbdStatus.PendingAirline,
        author: {
          id: 1,
          type: UserType.AGENT,
          iataCode: '8120196',
          organization: 'Peter Robinson Switzerland Ltd'
        },
        reason:
          'Slow unstable internet connection triggered a transaction failure. During the transaction prices have changed.',
        attachments: [
          {
            name: 'TicketAirline',
            type: 'pdf',
            size: 5265734
          },
          {
            name: 'Invoice',
            type: 'pdf',
            size: 3426756
          }
        ],
        creationDate: new Date(2019, 8, 4, 1, 6),
        type: ActivityType.Issued
      },
      {
        id: 1,
        pbdId: 2,
        status: PbdStatus.PendingAgent,
        author: {
          id: 1,
          type: UserType.AIRLINE,
          iataCode: '220',
          organization: 'Lufthansa'
        },
        reason: 'No, you are completely wrong! Our systems are perfect!',
        attachments: [
          {
            name: 'SystemLog',
            type: 'txt',
            size: 1234233
          }
        ],
        creationDate: new Date(2019, 8, 5, 17, 16),
        type: ActivityType.Disagreed
      },
      {
        id: 11,
        pbdId: 2,
        author: {
          id: 1,
          type: UserType.AGENT,
          iataCode: '8120196',
          organization: 'Peter Robinson Switzerland Ltd'
        },
        reason: 'This is my last comment. Take it or leave it.',
        attachments: [
          {
            name: 'TicketAirline',
            type: 'pdf',
            size: 5265734
          },
          {
            name: 'Invoice',
            type: 'pdf',
            size: 3426756
          },
          {
            name: 'TicketAirline',
            type: 'pdf',
            size: 5265734
          },
          {
            name: 'Invoice',
            type: 'pdf',
            size: 3426756
          }
        ],
        creationDate: new Date(2019, 8, 6, 1, 6),
        type: ActivityType.Commented
      },
      {
        id: 1,
        pbdId: 2,
        status: PbdStatus.PendingAirline,
        author: {
          id: 1,
          type: UserType.AGENT,
          iataCode: '8120196',
          organization: 'Peter Robinson Switzerland Ltd'
        },
        reason: 'I have debugged your logs and I have seen the error on line 42. Check it again ASAP!',
        attachments: [
          {
            name: 'CatPicture',
            type: 'jpeg',
            size: 52657343
          }
        ],
        creationDate: new Date(2019, 8, 6, 13, 36),
        type: ActivityType.Disagreed
      },
      {
        id: 1,
        pbdId: 2,
        author: {
          id: 1,
          type: UserType.AGENT,
          iataCode: '8120196',
          organization: 'Peter Robinson Switzerland Ltd'
        },
        reason: 'This is my last comment 2. Take it or leave it.',
        attachments: [],
        creationDate: new Date(2019, 8, 6, 17, 16),
        type: ActivityType.Commented
      },
      {
        id: 1,
        pbdId: 2,
        author: {
          id: 1,
          type: UserType.AGENT,
          iataCode: '8120196',
          organization: 'Peter Robinson Switzerland Ltd'
        },
        attachments: [
          {
            name: 'TicketAirline',
            type: 'pdf',
            size: 5265734
          },
          {
            name: 'Invoice',
            type: 'pdf',
            size: 3426756
          },
          {
            name: 'TicketAirline',
            type: 'pdf',
            size: 5265734
          },
          {
            name: 'Invoice',
            type: 'pdf',
            size: 3426756
          }
        ],
        creationDate: new Date(2019, 8, 8, 1, 6),
        type: ActivityType.Commented
      },
      {
        id: 1,
        pbdId: 2,
        status: PbdStatus.AgreeToAgent,
        author: {
          id: 1,
          type: UserType.AIRLINE,
          iataCode: '220',
          organization: 'Lufthansa'
        },
        reason: 'Oh you are right! It was my mistake. Here are your money',
        attachments: [
          {
            name: 'Invoice',
            type: 'pdf',
            size: 3426756
          }
        ],
        creationDate: new Date(2019, 8, 12, 15, 11),
        type: ActivityType.Agreed
      }
    ]
  }
];

export const PBDS_BE_RESPONSE_IATA_AGENT: PagedData<Pbd> = {
  records: PBDS_IATA_AGENT,
  pageSize: 20,
  total: 4,
  totalPages: 1,
  pageNumber: 1
};

export const PBDS_AIRLINE: Pbd[] = [];

export const PBD_ACTIVITIES: Activity[] = [
  {
    id: 1,
    pbdId: 15,
    status: PbdStatus.AgreeToAirline,
    author: {
      id: 1,
      type: UserType.AGENT,
      iataCode: '8120196',
      organization: 'Peter Robinson Switzerland Ltd'
    },
    reason: 'Oh you are right! It was my mistake. Here are your money',
    attachments: [
      {
        name: 'Invoice',
        type: 'pdf',
        size: 3426756
      }
    ],
    creationDate: new Date(2019, 8, 12, 15, 11),
    type: ActivityType.Agreed
  }
];

export const PBDS_BE_RESPONSE_AIRLINE: PagedData<Pbd> = {
  records: PBDS_AIRLINE,
  pageSize: 20,
  total: 4,
  totalPages: 1,
  pageNumber: 1
};

export const STATUSES: DropdownOption[] = [
  {
    value: 'PENDING_AIRLINE',
    label: 'postBillingDisputes.statuses.PENDING_AIRLINE'
  },
  {
    value: 'PENDING_AGENT',
    label: 'postBillingDisputes.statuses.PENDING_AGENT'
  },
  {
    value: 'AGREE_TO_AIRLINE',
    label: 'postBillingDisputes.statuses.AGREE_TO_AIRLINE'
  },
  {
    value: 'AGREE_TO_AGENT',
    label: 'postBillingDisputes.statuses.AGREE_TO_AGENT'
  },
  {
    value: 'AGREEMENT_NOT_REACHED',
    label: 'postBillingDisputes.statuses.AGREEMENT_NOT_REACHED'
  }
];

export const TRNC: DropdownOption[] = [
  {
    value: 'TKTT',
    label: 'TKTT'
  },
  {
    value: 'EMDA',
    label: 'EMDA'
  },
  {
    value: 'EMDS',
    label: 'EMDS'
  },
  {
    value: 'ADMA',
    label: 'ADMA'
  }
];

export const AIRLINE_CODES_NAMES: DropdownOption[] = [
  {
    value: '135',
    label: '135 / Bulgaria Air'
  },
  {
    value: '221',
    label: '221 / Jet Blue'
  },
  {
    value: '220',
    label: '220 / Lufthansa'
  }
];

export const AGENT_CODES_NAMES: DropdownOption[] = [
  {
    value: '8120191',
    label: '8120191 / Kris Petrov Travels'
  },
  {
    value: '8120196',
    label: '8120196 / Peter Robinson Switzerland Ltd'
  },
  {
    value: '8120116',
    label: '8120116 / Sunny Beach Holidays'
  }
];

export const COUNTRIES: DropdownOption[] = [
  {
    value: 'Bulgaria',
    label: 'Bulgaria'
  },
  {
    value: 'France',
    label: 'France'
  },
  {
    value: 'Germany',
    label: 'Germany'
  },
  {
    value: 'Spain',
    label: 'Spain'
  }
];

export const CURRENCIES: DropdownOption[] = [
  {
    value: 'EUR',
    label: 'EUR'
  }
];

export const DOCUMENTS_MOCK = [
  {
    id: '3698516642-23-Q0hlbDIyMDNfMjAxOTA1MjdfMTkwNTI3XzAxLjIyMC56aXA=',
    airline: {
      id: 10252,
      localName: 'Lufthansa',
      bsp: {
        id: 10151,
        isoCountryCode: 'CH',
        name: 'Switzerland'
      },
      vatNumber: 'vat#forLufthansa',
      globalAirline: {
        id: 10250,
        iataCode: '220',
        globalName: 'Lufthansa',
        logo: 'LufthansaLogo'
      }
    },
    agent: {
      id: 12039,
      iataCode: '8120317',
      name: 'TUI/FLEX FLIGHT CENTER ZRH',
      vatNumber: 'VAT_VirginAustralia'
    },
    gds: {
      id: 10043,
      gdsCode: 'SABR',
      name: 'SABRE'
    },
    isoCountryCode: 'CH',
    billingAnalysisEndingDate: '2019-05-31',
    auditCouponAgentCode: '8120317-2',
    commissionPercent: 0,
    currencyType: 'CHF',
    numberOfDecimals: 2,
    cashAmount: 1253,
    creditAmount: 0,
    easyPayAmount: 0,
    transaction: {
      airlineCode: '220',
      reportingSystemIdentifier: 'SABR'
    },
    documentIdentification: {
      dateOfIssue: new Date().toISOString(),
      documentNumber: '3698516642',
      checkDigit: '0',
      couponUseIndicator: 'FFFF',
      agentCode: '8120317',
      transactionCode: 'TKTT',
      originCityCodes: 'GVAGVA',
      pnrReference: 'YQJGHB/AA'
    },
    stdAmounts: {
      commissionableAmount: 1253,
      ticketAmount: 1902.55,
      taxes: [
        {
          type: 'YQ',
          amount: 535
        },
        {
          type: 'YR',
          amount: 16
        },
        {
          type: 'CH',
          amount: 26.7
        },
        {
          type: 'DE',
          amount: 10.7
        },
        {
          type: 'RA',
          amount: 48
        },
        {
          type: 'CN',
          amount: 13.15
        }
      ],
      sumAdditionalTaxes: {
        type: 'XT',
        amount: 98.55
      },
      totalTaxes: 649.55
    },
    commissions: [
      {
        statisticalCode: 'I'
      }
    ],
    issueInformationForSales: [
      {
        iataNumber: '0',
        endorsementsRestrictions: 'FARE RESTRICTION MAY APPLY'
      }
    ],
    itineraryData: [
      {
        segmentIdentifier: 1,
        stopoverCode: 'X',
        notValidBeforeDate: '06JUL',
        notValidAfterDate: '06JUL',
        originAirportCode: 'GVA',
        destinationAirportCode: 'FRA',
        carrier: 'LH',
        flightNumber: '5765',
        reservationBookingDesignator: 'W',
        flightDate: '06JUL',
        flightDepartureTime: '1455',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'WNCCSR'
      },
      {
        segmentIdentifier: 2,
        stopoverCode: 'O',
        notValidBeforeDate: '06JUL',
        notValidAfterDate: '06JUL',
        originAirportCode: 'FRA',
        destinationAirportCode: 'SHE',
        carrier: 'LH',
        flightNumber: ' 782',
        reservationBookingDesignator: 'W',
        flightDate: '06JUL',
        flightDepartureTime: '1750',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'WNCCSR'
      },
      {
        segmentIdentifier: 3,
        stopoverCode: 'X',
        notValidBeforeDate: '23AUG',
        notValidAfterDate: '23AUG',
        originAirportCode: 'SHE',
        destinationAirportCode: 'FRA',
        carrier: 'LH',
        flightNumber: ' 783',
        reservationBookingDesignator: 'P',
        flightDate: '23AUG',
        flightDepartureTime: '1305',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'PNCPCSR'
      },
      {
        segmentIdentifier: 4,
        notValidBeforeDate: '23AUG',
        notValidAfterDate: '23AUG',
        originAirportCode: 'FRA',
        destinationAirportCode: 'GVA',
        carrier: 'LH',
        flightNumber: '5768',
        reservationBookingDesignator: 'P',
        flightDate: '23AUG',
        flightDepartureTime: '2100',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'PNCPCSR'
      }
    ],
    documentAmounts: {
      fare: {
        currency: 'CHF',
        amount: '  1253.00'
      },
      total: {
        currency: 'CHF',
        amount: '  1902.55'
      },
      ticketingModeIndicator: '/',
      servicingAirlineProviderIdentifier: '0011',
      fareCalculationModeIndicator: '2',
      fareCalculationPricingIndicator: '0'
    },
    passengerInformation: {
      name: 'BIAN/CHENG'
    },
    fareCalculationArea: 'GVA LH X/FRA LH SHE342.31LH X/FRA LH GVA911.82NUC1254.13END ROE0.999094',
    formOfPaymentAdditionalInformation: [
      {
        paymentSequenceNumber: 1,
        paymentInformation: 'INV'
      }
    ],
    formOfPayment: {
      type: 'CA',
      typeDescription: 'CASH',
      paymentInformation: 'INV',
      paymentAmount: 1902.55
    },
    remittanceArea: {
      sumCash: 1253,
      sumCredit: 0,
      sumEasyPay: 0,
      commissionPercent: 0,
      sumTaxAmount: 649.55
    },
    accountInformation: {
      vatGstAmount: 0,
      agentCommissionPercent: 0,
      agentCommissionAmount: 0,
      fareAdjustmentAmount: 0,
      cancellationFees: 0,
      cancellationFeesCommission: 0,
      onlineBillingStatementForAgent: 1902.55,
      balancePayableToAirline: 1902.55,
      netFareAmount: 1253
    }
  },
  {
    id: '3435724556-28-Q0hlbDA0NzVfMjAxOTAzMjRfMTkwMzI0XzAxLjA0Ny56aXA=',
    airline: {
      id: 10245,
      localName: 'TAP Portugal',
      bsp: {
        id: 10151,
        isoCountryCode: 'CH',
        name: 'Switzerland'
      },
      vatNumber: 'vat#forTAPPortugal',
      globalAirline: {
        id: 10244,
        iataCode: '047',
        globalName: 'TAP Portugal',
        logo: 'LogoTAPPortugal'
      }
    },
    agent: {
      id: 12039,
      iataCode: '8120317',
      name: 'TUI/FLEX FLIGHT CENTER ZRH',
      vatNumber: 'VAT_VirginAustralia'
    },
    gds: {
      id: 10043,
      gdsCode: 'SABR',
      name: 'SABRE'
    },
    isoCountryCode: 'CH',
    billingAnalysisEndingDate: '2019-03-23',
    auditCouponAgentCode: '8120317-2',
    commissionPercent: 0,
    currencyType: 'CHF',
    numberOfDecimals: 2,
    cashAmount: 1018,
    creditAmount: 0,
    easyPayAmount: 0,
    transaction: {
      airlineCode: '047',
      reportingSystemIdentifier: 'SABR'
    },
    documentIdentification: {
      dateOfIssue: '2019-03-23',
      documentNumber: '3435724556',
      checkDigit: '4',
      couponUseIndicator: 'FFFF',
      agentCode: '8120317',
      transactionCode: 'TKTT',
      originCityCodes: 'RECREC',
      pnrReference: 'GGSDAY/AA'
    },
    stdAmounts: {
      commissionableAmount: 1018,
      ticketAmount: 1127.5,
      taxes: [
        {
          type: 'CH',
          amount: 35
        },
        {
          type: 'BR',
          amount: 32.5
        },
        {
          type: 'PT',
          amount: 13.2
        },
        {
          type: 'YP',
          amount: 28.8
        }
      ],
      sumAdditionalTaxes: {
        type: 'XT',
        amount: 42
      },
      totalTaxes: 109.5
    },
    commissions: [
      {
        statisticalCode: 'I'
      }
    ],
    issueInformationForSales: [
      {
        iataNumber: '0',
        endorsementsRestrictions: 'FARE REST APPLY'
      }
    ],
    itineraryData: [
      {
        segmentIdentifier: 1,
        stopoverCode: 'X',
        notValidBeforeDate: '02MAY',
        notValidAfterDate: '02MAY',
        originAirportCode: 'REC',
        destinationAirportCode: 'LIS',
        carrier: 'TP',
        flightNumber: '  12',
        reservationBookingDesignator: 'E',
        flightDate: '02MAY',
        flightDepartureTime: '2245',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'EBRCLI0E'
      },
      {
        segmentIdentifier: 2,
        stopoverCode: 'O',
        notValidBeforeDate: '03MAY',
        notValidAfterDate: '03MAY',
        originAirportCode: 'LIS',
        destinationAirportCode: 'ZRH',
        carrier: 'TP',
        flightNumber: ' 926',
        reservationBookingDesignator: 'E',
        flightDate: '03MAY',
        flightDepartureTime: '1330',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'EBRCLI0E'
      },
      {
        segmentIdentifier: 3,
        stopoverCode: 'X',
        notValidBeforeDate: '02AUG',
        notValidAfterDate: '02AUG',
        originAirportCode: 'ZRH',
        destinationAirportCode: 'LIS',
        carrier: 'TP',
        flightNumber: ' 931',
        reservationBookingDesignator: 'K',
        flightDate: '02AUG',
        flightDepartureTime: '1330',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'KBRCLI0E'
      },
      {
        segmentIdentifier: 4,
        notValidBeforeDate: '02AUG',
        notValidAfterDate: '02AUG',
        originAirportCode: 'LIS',
        destinationAirportCode: 'REC',
        carrier: 'TP',
        flightNumber: '  11',
        reservationBookingDesignator: 'K',
        flightDate: '02AUG',
        flightDepartureTime: '1705',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'KBRCLI0E'
      }
    ],
    documentAmounts: {
      fare: {
        currency: 'USD',
        amount: '  1025.00'
      },
      equivalentFarePaid: {
        currency: 'CHF',
        amount: '  1018.00'
      },
      total: {
        currency: 'CHF',
        amount: '  1127.50'
      },
      ticketingModeIndicator: '/',
      servicingAirlineProviderIdentifier: '0011',
      fareCalculationModeIndicator: '2',
      fareCalculationPricingIndicator: '0'
    },
    passengerInformation: {
      name: 'DACOSTA/OZANILDA MRS'
    },
    fareCalculationArea: 'REC TP X/LIS TP ZRH440.00TP X/LIS TP REC585.00NUC1025.00END ROE1.00',
    formOfPaymentAdditionalInformation: [
      {
        paymentSequenceNumber: 1,
        paymentInformation: 'INV'
      }
    ],
    formOfPayment: {
      type: 'CA',
      typeDescription: 'CASH',
      paymentInformation: 'INV',
      paymentAmount: 1127.5
    },
    remittanceArea: {
      sumCash: 1018,
      sumCredit: 0,
      sumEasyPay: 0,
      commissionPercent: 0,
      sumTaxAmount: 109.5
    },
    accountInformation: {
      vatGstAmount: 0,
      agentCommissionPercent: 0,
      agentCommissionAmount: 0,
      fareAdjustmentAmount: 0,
      cancellationFees: 0,
      cancellationFeesCommission: 0,
      onlineBillingStatementForAgent: 1127.5,
      balancePayableToAirline: 1127.5,
      netFareAmount: 1018
    }
  },
  {
    id: '3698516643-24-Q0hlbDIyMDNfMjAxOTA1MjdfMTkwNTI3XzAxLjIyMC56aXA=',
    airline: {
      id: 10252,
      localName: 'Lufthansa',
      bsp: {
        id: 10151,
        isoCountryCode: 'CH',
        name: 'Switzerland'
      },
      vatNumber: 'vat#forLufthansa',
      globalAirline: {
        id: 10250,
        iataCode: '220',
        globalName: 'Lufthansa',
        logo: 'LufthansaLogo'
      }
    },
    agent: {
      id: 12039,
      iataCode: '8120317',
      name: 'TUI/FLEX FLIGHT CENTER ZRH',
      vatNumber: 'VAT_VirginAustralia'
    },
    gds: {
      id: 10043,
      gdsCode: 'SABR',
      name: 'SABRE'
    },
    isoCountryCode: 'CH',
    billingAnalysisEndingDate: '2019-05-31',
    auditCouponAgentCode: '8120317-2',
    commissionPercent: 0,
    currencyType: 'CHF',
    numberOfDecimals: 2,
    cashAmount: 940,
    creditAmount: 0,
    easyPayAmount: 0,
    transaction: {
      airlineCode: '220',
      reportingSystemIdentifier: 'SABR'
    },
    documentIdentification: {
      dateOfIssue: '26/05/2019',
      documentNumber: '3698516643',
      checkDigit: '1',
      couponUseIndicator: 'FFFF',
      agentCode: '8120317',
      transactionCode: 'TKTT',
      originCityCodes: 'GVAGVA',
      pnrReference: 'YQJGHB/AA'
    },
    stdAmounts: {
      commissionableAmount: 940,
      ticketAmount: 1576.4,
      taxes: [
        {
          type: 'YQ',
          amount: 535
        },
        {
          type: 'YR',
          amount: 16
        },
        {
          type: 'CH',
          amount: 26.7
        },
        {
          type: 'DE',
          amount: 10.7
        },
        {
          type: 'RA',
          amount: 48
        }
      ],
      sumAdditionalTaxes: {
        type: 'XT',
        amount: 85.4
      },
      totalTaxes: 636.4
    },
    commissions: [
      {
        statisticalCode: 'I'
      }
    ],
    issueInformationForSales: [
      {
        iataNumber: '0',
        endorsementsRestrictions: 'FARE RESTRICTION MAY APPLY'
      }
    ],
    itineraryData: [
      {
        segmentIdentifier: 1,
        stopoverCode: 'X',
        notValidBeforeDate: '06JUL',
        notValidAfterDate: '06JUL',
        originAirportCode: 'GVA',
        destinationAirportCode: 'FRA',
        carrier: 'LH',
        flightNumber: '5765',
        reservationBookingDesignator: 'W',
        flightDate: '06JUL',
        flightDepartureTime: '1455',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'WNCCSR/CH25'
      },
      {
        segmentIdentifier: 2,
        stopoverCode: 'O',
        notValidBeforeDate: '06JUL',
        notValidAfterDate: '06JUL',
        originAirportCode: 'FRA',
        destinationAirportCode: 'SHE',
        carrier: 'LH',
        flightNumber: ' 782',
        reservationBookingDesignator: 'W',
        flightDate: '06JUL',
        flightDepartureTime: '1750',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'WNCCSR/CH25'
      },
      {
        segmentIdentifier: 3,
        stopoverCode: 'X',
        notValidBeforeDate: '23AUG',
        notValidAfterDate: '23AUG',
        originAirportCode: 'SHE',
        destinationAirportCode: 'FRA',
        carrier: 'LH',
        flightNumber: ' 783',
        reservationBookingDesignator: 'P',
        flightDate: '23AUG',
        flightDepartureTime: '1305',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'PNCPCSR/CH25'
      },
      {
        segmentIdentifier: 4,
        notValidBeforeDate: '23AUG',
        notValidAfterDate: '23AUG',
        originAirportCode: 'FRA',
        destinationAirportCode: 'GVA',
        carrier: 'LH',
        flightNumber: '5768',
        reservationBookingDesignator: 'P',
        flightDate: '23AUG',
        flightDepartureTime: '2100',
        flightBookingStatus: 'OK',
        baggageAllowance: '2PC',
        fareTicketDesignator: 'PNCPCSR/CH25'
      }
    ],
    documentAmounts: {
      fare: {
        currency: 'CHF',
        amount: '   940.00'
      },
      total: {
        currency: 'CHF',
        amount: '  1576.40'
      },
      ticketingModeIndicator: '/',
      servicingAirlineProviderIdentifier: '0011',
      fareCalculationModeIndicator: '2',
      fareCalculationPricingIndicator: '0'
    },
    passengerInformation: {
      name: 'XU/RUILI CHD CHD',
      specificData: '21SEP2013'
    },
    fareCalculationArea: 'GVA LH X/FRA LH SHE256.73LH X/FRA LH GVA683.86NUC940.59END ROE0.999094',
    formOfPaymentAdditionalInformation: [
      {
        paymentSequenceNumber: 1,
        paymentInformation: 'INV'
      }
    ],
    formOfPayment: {
      type: 'CA',
      typeDescription: 'CASH',
      paymentInformation: 'INV',
      paymentAmount: 1576.4
    },
    remittanceArea: {
      sumCash: 940,
      sumCredit: 0,
      sumEasyPay: 0,
      commissionPercent: 0,
      sumTaxAmount: 636.4
    },
    accountInformation: {
      vatGstAmount: 0,
      agentCommissionPercent: 0,
      agentCommissionAmount: 0,
      fareAdjustmentAmount: 0,
      cancellationFees: 0,
      cancellationFeesCommission: 0,
      onlineBillingStatementForAgent: 1576.4,
      balancePayableToAirline: 1576.4,
      netFareAmount: 940
    }
  },
  {
    id: '3419241420-26-Q0hlbDA0NzVfMjAxOTAzMjRfMTkwMzI0XzAxLjA0Ny56aXA=',
    airline: {
      id: 10245,
      localName: 'TAP Portugal',
      bsp: {
        id: 10151,
        isoCountryCode: 'CH',
        name: 'Switzerland'
      },
      vatNumber: 'vat#forTAPPortugal',
      globalAirline: {
        id: 10244,
        iataCode: '047',
        globalName: 'TAP Portugal',
        logo: 'LogoTAPPortugal'
      }
    },
    agent: {
      id: 12039,
      iataCode: '8120317',
      name: 'TUI/FLEX FLIGHT CENTER ZRH',
      vatNumber: 'VAT_VirginAustralia'
    },
    gds: {
      id: 10039,
      gdsCode: 'GDSL',
      name: 'GALILEO INTERNATIONAL'
    },
    isoCountryCode: 'CH',
    billingAnalysisEndingDate: '2019-03-23',
    auditCouponAgentCode: '8120317-2',
    commissionPercent: 0,
    currencyType: 'CHF',
    numberOfDecimals: 2,
    cashAmount: 223,
    creditAmount: 0,
    easyPayAmount: 0,
    transaction: {
      airlineCode: '047',
      reportingSystemIdentifier: 'GDSL'
    },
    documentIdentification: {
      dateOfIssue: '2019-03-23',
      documentNumber: '3419241420',
      checkDigit: '6',
      couponUseIndicator: 'FFFF',
      agentCode: '8120317',
      transactionCode: 'TKTT',
      originCityCodes: 'ZRHZRH',
      pnrReference: 'OCWWP8/1G'
    },
    stdAmounts: {
      commissionableAmount: 223,
      ticketAmount: 456.2,
      taxes: [
        {
          type: 'CH',
          amount: 35
        },
        {
          type: 'PT',
          amount: 13.2
        },
        {
          type: 'YP',
          amount: 28.8
        },
        {
          type: 'A9',
          amount: 10.45
        },
        {
          type: 'MA',
          amount: 9.75
        },
        {
          type: 'YQ',
          amount: 136
        }
      ],
      sumAdditionalTaxes: {
        type: 'XT',
        amount: 185
      },
      totalTaxes: 233.2
    },
    commissions: [
      {
        statisticalCode: 'I'
      }
    ],
    issueInformationForSales: [
      {
        iataNumber: '0',
        endorsementsRestrictions: 'FARE RESTR APPLY/NON REF'
      }
    ],
    itineraryData: [
      {
        segmentIdentifier: 1,
        stopoverCode: 'X',
        notValidBeforeDate: '29JUN',
        notValidAfterDate: '29JUN',
        originAirportCode: 'ZRH',
        destinationAirportCode: 'LIS',
        carrier: 'TP',
        flightNumber: '929',
        reservationBookingDesignator: 'A',
        flightDate: '29JUN',
        flightDepartureTime: '0620',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'A23BSC0C'
      },
      {
        segmentIdentifier: 2,
        notValidBeforeDate: '29JUN',
        notValidAfterDate: '29JUN',
        originAirportCode: 'LIS',
        destinationAirportCode: 'RAK',
        carrier: 'TP',
        flightNumber: '1454',
        reservationBookingDesignator: 'A',
        flightDate: '29JUN',
        flightDepartureTime: '0925',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'A23BSC0C'
      },
      {
        segmentIdentifier: 3,
        stopoverCode: 'X',
        notValidBeforeDate: '07JUL',
        notValidAfterDate: '07JUL',
        originAirportCode: 'RAK',
        destinationAirportCode: 'LIS',
        carrier: 'TP',
        flightNumber: '1451',
        reservationBookingDesignator: 'K',
        flightDate: '07JUL',
        flightDepartureTime: '1625',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'K23BSC0C'
      },
      {
        segmentIdentifier: 4,
        notValidBeforeDate: '07JUL',
        notValidAfterDate: '07JUL',
        originAirportCode: 'LIS',
        destinationAirportCode: 'ZRH',
        carrier: 'TP',
        flightNumber: '928',
        reservationBookingDesignator: 'K',
        flightDate: '07JUL',
        flightDepartureTime: '1900',
        flightBookingStatus: 'OK',
        baggageAllowance: '1PC',
        fareTicketDesignator: 'K23BSC0C'
      }
    ],
    documentAmounts: {
      fare: {
        currency: 'CHF',
        amount: '223.00'
      },
      total: {
        currency: 'CHF',
        amount: '456.20'
      },
      ticketingModeIndicator: '/',
      servicingAirlineProviderIdentifier: '7733',
      fareCalculationModeIndicator: '0',
      bookingAgentIdentification: 'M7GSR',
      bookingEntityOutletType: 'U',
      fareCalculationPricingIndicator: 'G'
    },
    passengerInformation: {
      name: 'DELLATORRE/THOMASMR'
    },
    fareCalculationArea: 'ZRH TP X/LIS TP RAK 113.09 TP X/LIS TP ZRH 110.08 NUC223.17END ROE0.999187',
    formOfPaymentAdditionalInformation: [
      {
        paymentSequenceNumber: 1,
        paymentInformation: 'INVOICE'
      }
    ],
    formOfPayment: {
      type: 'CA',
      typeDescription: 'CASH',
      paymentInformation: 'INVOICE',
      paymentAmount: 456.2
    },
    remittanceArea: {
      sumCash: 223,
      sumCredit: 0,
      sumEasyPay: 0,
      commissionPercent: 0,
      sumTaxAmount: 233.2
    },
    accountInformation: {
      vatGstAmount: 0,
      agentCommissionPercent: 0,
      agentCommissionAmount: 0,
      fareAdjustmentAmount: 0,
      cancellationFees: 0,
      cancellationFeesCommission: 0,
      onlineBillingStatementForAgent: 456.2,
      balancePayableToAirline: 456.2,
      netFareAmount: 223
    }
  }
];

export const SEARCH_FORM_DEFAULT_VALUE_PBD: PbdSearchForm = {
  documentNumber: null,
  transactionCode: null,
  airlineCodes: null,
  agentCodes: null,
  status: null,
  pbdResolutionDate: null,
  currencyCode: null,
  pbdCreationDate: null,
  bsp: null
};
