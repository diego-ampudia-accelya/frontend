/* eslint-disable @typescript-eslint/naming-convention */
import { Agent, Airline } from '~app/master-data/models';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';

export interface Pbd {
  id?: number;
  documentNumber: string;
  documentId?: string;
  transactionCode?: string;
  dateOfIssue?: string;
  pbdDate?: string;
  resolutionDate?: string;
  agent?: {
    id: number;
    iataCode: string;
    name: string;
    bsp?: BspDto;
  };
  airline?: {
    id: number;
    iataCode: string;
    localName: string;
  };
  pbdAmount?: number;
  maximumDisputableAmount?: number;
  currency?: Currency;
  timeLeftForAgreementInHours?: number;
  status?: PbdStatus;
  translatedPbdStatus?: string;

  activities?: Activity[];
  workflowActivities?: Activity[];
  period?: number;

  bsp?: Partial<Bsp>;
  watching?: boolean;
}

export interface PbdEntity {
  id: number;
  iataCode: string;
  name?: string;
  localName?: string;
  userType: UserType;
}

export interface TicketModel {
  id: string;
  documentNumber: string;
  transactionCode: string;
  dateOfIssue: string;
  period?: number;
  airline: Airline;
  agent: Agent;
  currency?: Currency;
  maximumDisputableAmount: number;
}

export interface PbdIssueModel {
  documentId: string;
  pbdAmount: number;
  reason: string;
  airlineCode: string;
  transactionCode: string;
  period: number;
  dateOfIssue: string;
  attachments: string[];
}
export interface PbdSearchForm {
  bsp?: BspDto[];
  documentNumber?: number;
  transactionCode?: PbdDocumentType;
  airlineCodes?: AirlineSummary[];
  agentCodes?: AgentSummary[];
  status?: PbdStatus[];
  currencyCode?: Currency[];
  pbdCreationDate?: Date[];
  pbdResolutionDate?: Date[];
}

export interface PbdSearchFormBE {
  bspIsoCountryCode?: string[];
  documentNumber?: number;
  transactionCode?: PbdDocumentType;
  airlineIataCode?: string[];
  agentIataCode?: string[];
  status?: PbdStatus[];
  currencyCode?: string[];
  pbdDateFrom?: string;
  pbdDateTo?: string;
  resolutionDateFrom?: string;
  resolutionDateTo?: string;
}

export enum DocumentMessages {
  NoResults = 100,
  NotRelated = 101,
  AlreadyDisputed = 102,
  ExceedDisputePeriod = 103,
  SuspendedArline = 104,
  DocumentNotMatch = 105
}

export enum PbdStatus {
  PendingAirline = 'PENDING_AIRLINE',
  PendingAgent = 'PENDING_AGENT',
  AgreeToAirline = 'AGREE_TO_AIRLINE',
  AgreeToAgent = 'AGREE_TO_AGENT',
  AgreementNotReached = 'AGREEMENT_NOT_REACHED'
}

export enum AgreementType {
  AgreeWithAgent = 'AGREED_WITH_AGENT',
  DisagreeWithAgent = 'DISAGREED_WITH_AGENT',
  AgreeWithAirline = 'AGREED_WITH_AIRLINE',
  DisagreeWithAirline = 'DISAGREED_WITH_AIRLINE'
}

export interface Activity {
  id?: number;
  pbdId: number;
  status?: PbdStatus;
  author?: {
    id: number;
    type: UserType;
    iataCode: string;
    organization: string;
  };
  reason?: string;
  attachments?: any[];
  creationDate?: Date;
  type: ActivityType;
}

export interface ActivitySaveModel {
  pbdId: number;
  reason: string;
  agreementType?: AgreementType;
  activityType?: ActivityType;
  attachments?: string[];
}

export enum ActivityType {
  Agreed = 'AGREED',
  Disagreed = 'DISAGREED',
  Commented = 'COMMENTED',
  Issued = 'ISSUED', // the first PbdActivity. PBD just created,
  System = 'SYSTEM'
}

export enum ActivityFilterType {
  All = 'ALL',
  Workflow = 'WORKFLOW_EVENTS',
  Comments = 'COMMENTS',
  Attachments = 'ATTACHMENTS'
}

export interface SecondaryHeaderActionsViewModel {
  action: GridTableActionType;
  label: string;
}

export enum PbdDocumentType {
  EMDS = 'EMDS',
  EMDA = 'EMDA',
  TKTT = 'TKTT',
  ADMA = 'ADMA'
}
