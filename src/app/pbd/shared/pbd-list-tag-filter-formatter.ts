import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';

import { PbdSearchForm } from './pbd.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class PbdListTagFilterFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: PbdSearchForm): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<PbdSearchForm>> = {
      bsp: bsp => `${this.translate('bsp')} - ${bsp.map(bs => `${bs.name} (${bs.isoCountryCode})`).join(', ')}`,
      documentNumber: documentNumber => `${this.translate('documentNumber')} - ${documentNumber}`,
      transactionCode: transactionCode => `${this.translate('transactionCode')} - ${transactionCode}`,
      airlineCodes: airlineCodes =>
        `${this.translate('airlineCodeName')} - ${airlineCodes.map(airl => `${airl.code}/${airl.name}`).join(', ')}`,
      agentCodes: agentCodes =>
        `${this.translate('agentCodeName')} - ${agentCodes.map(agt => `${agt.code}/${agt.name}`).join(', ')}`,
      status: status => `${this.translate('status')} - ${status.map(sta => this.translatedPbdStatus(sta)).join(', ')}`,
      currencyCode: currencyCode => `${this.translate('currency')} - ${currencyCode.map(curr => curr.code).join(', ')}`,
      pbdCreationDate: pbdCreationDate => `${this.translate('pbdDate')} - ${rangeDateFilterTagMapper(pbdCreationDate)}`,
      pbdResolutionDate: pbdResolutionDate =>
        `${this.translate('resolutionDate')} - ${rangeDateFilterTagMapper(pbdResolutionDate)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({ keys: [item.key], label: item.mapper(item.value) }));
  }

  private translatedPbdStatus(key: string): string {
    return this.translationService.translate('postBillingDisputes.statuses.' + key);
  }

  private translate(key: string): string {
    return this.translationService.translate('LIST.postBillingDispute.filters.labels.' + key);
  }
}
