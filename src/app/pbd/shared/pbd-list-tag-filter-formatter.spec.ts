import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { PbdListTagFilterFormatter } from './pbd-list-tag-filter-formatter';
import { PbdDocumentType, PbdSearchForm, PbdStatus } from './pbd.model';

describe('PbdListTagFilterFormatter', () => {
  let formatter: PbdListTagFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new PbdListTagFilterFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format transactionCode if they exist', () => {
    const filterTransactionCode: PbdSearchForm = {
      transactionCode: PbdDocumentType.TKTT
    };

    expect(formatter.format(filterTransactionCode)).toEqual([
      {
        keys: ['transactionCode'],
        label: 'LIST.postBillingDispute.filters.labels.transactionCode - TKTT'
      }
    ]);
  });

  it('should format airlineCodes if they exist', () => {
    const filterAirlineCode: PbdSearchForm = {
      airlineCodes: [
        {
          id: 11,
          name: 'Iberia',
          code: '345',
          designator: 'D'
        }
      ]
    };

    expect(formatter.format(filterAirlineCode)).toEqual([
      {
        keys: ['airlineCodes'],
        label: 'LIST.postBillingDispute.filters.labels.airlineCodeName - 345/Iberia'
      }
    ]);
  });

  it('should format agentCodeName if they exist', () => {
    const filterAgentCode: PbdSearchForm = {
      agentCodes: [
        {
          id: '25',
          name: 'Agent',
          code: '678'
        }
      ]
    };

    expect(formatter.format(filterAgentCode)).toEqual([
      {
        keys: ['agentCodes'],
        label: 'LIST.postBillingDispute.filters.labels.agentCodeName - 678/Agent'
      }
    ]);
  });

  it('should format status if they exist', () => {
    const filterStatus: PbdSearchForm = {
      status: [PbdStatus.AgreeToAgent]
    };

    expect(formatter.format(filterStatus)).toEqual([
      {
        keys: ['status'],
        label: 'LIST.postBillingDispute.filters.labels.status - postBillingDisputes.statuses.AGREE_TO_AGENT'
      }
    ]);
  });

  it('should format currencyCode if they exist', () => {
    const filterCurrency: PbdSearchForm = {
      currencyCode: [
        {
          id: 34,
          code: 'EUR',
          decimals: 2
        }
      ]
    };

    expect(formatter.format(filterCurrency)).toEqual([
      {
        keys: ['currencyCode'],
        label: 'LIST.postBillingDispute.filters.labels.currency - EUR'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
