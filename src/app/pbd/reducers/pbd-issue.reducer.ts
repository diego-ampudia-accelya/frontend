import { createReducer, on } from '@ngrx/store';

import { PbdIssueDialogActions } from '~app/pbd/actions';

export const pbdIssueFeatureKey = 'pbdIssueState';

export interface State {
  loading: boolean;
}

const initialSate: State = {
  loading: false
};

export const reducer = createReducer(
  initialSate,
  on(PbdIssueDialogActions.requestPbdIssue, state => ({
    ...state,
    loading: true
  })),
  on(PbdIssueDialogActions.pbdIssueSuccess, PbdIssueDialogActions.pbdIssueFailure, state => ({
    ...state,
    loading: false
  }))
);

export const getLoading = (state: State): boolean => state.loading;
