import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';

import { PbdApiActions, PbdDetailsActions, PbdListActions, PbdStartDialogActions } from '~app/pbd/actions';
import { Pbd, TicketModel } from '~app/pbd/shared/pbd.model';

export const pbdDataFeatureKey = 'pbdState';

export interface State extends EntityState<Pbd> {
  pbdIssueData: Pbd;
}

export const adapter: EntityAdapter<Pbd> = createEntityAdapter<Pbd>();

const initialState: State = adapter.getInitialState({
  pbdIssueData: null
});

export const reducer = createReducer(
  initialState,
  on(PbdListActions.startPbd, state => ({
    ...state,
    pbdIssueData: null
  })),
  on(PbdStartDialogActions.selectDocument, (state, { selectedDocument }) => ({
    ...state,
    pbdIssueData: parseDocumentToPbd(selectedDocument)
  })),
  on(PbdDetailsActions.addPbdAmountData, (state, { pbdAmount }) => ({
    ...state,
    pbdIssueData: { ...state.pbdIssueData, pbdAmount }
  })),
  on(PbdDetailsActions.pbdIssueConfirmed, state => ({
    ...state,
    pbdIssueData: null
  })),
  on(PbdApiActions.requestPbdSuccess, (state, { pbd }) => adapter.upsertOne(pbd, state))
);

function parseDocumentToPbd(document: TicketModel): Pbd {
  const {
    id,
    documentNumber,
    transactionCode,
    dateOfIssue,
    period,
    airline,
    agent,
    currency,
    maximumDisputableAmount
  } = document;

  return {
    id: null,
    documentId: id,
    documentNumber,
    transactionCode,
    period,
    airline: {
      id: airline.id,
      iataCode: airline.globalAirline.iataCode,
      localName: airline.localName
    },
    agent: {
      id: agent.id,
      iataCode: agent.iataCode,
      name: agent.name
    },
    status: null,
    resolutionDate: null,
    currency,
    maximumDisputableAmount,
    dateOfIssue: new Date(dateOfIssue).toISOString(),
    pbdDate: null,
    pbdAmount: null,
    activities: [],
    workflowActivities: []
  };
}

const { selectEntities } = adapter.getSelectors();

export const selectPbdEntities = selectEntities;

export const getPbdIssueData = (state: State): Pbd => state.pbdIssueData;

export const getPbd = (state: State, id?: number) => (id ? selectPbdEntities(state)?.[id] : state.pbdIssueData);
