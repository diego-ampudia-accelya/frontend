import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromPbdActivity from '~app/pbd/reducers/pbd-activity.reducer';
import * as fromPbdDetails from '~app/pbd/reducers/pbd-details.reducer';
import * as fromPbdIssue from '~app/pbd/reducers/pbd-issue.reducer';
import * as fromPbdStart from '~app/pbd/reducers/pbd-start.reducer';
import * as fromPbd from '~app/pbd/reducers/pbd.reducer';
import { AppState } from '~app/reducers';

export const pbdFeatureKey = 'pbd';

export interface PbdState {
  [fromPbd.pbdDataFeatureKey]: fromPbd.State;
  [fromPbdIssue.pbdIssueFeatureKey]: fromPbdIssue.State;
  [fromPbdStart.pbdStartFeatureKey]: fromPbdStart.State;
  [fromPbdActivity.pbdActivityFeatureKey]: fromPbdActivity.State;
  [fromPbdDetails.pbdDetailsFeatureKey]: fromPbdDetails.State;
}

export interface State extends AppState {
  [pbdFeatureKey]: PbdState;
}

export function reducers(state: PbdState | undefined, action: Action) {
  return combineReducers({
    [fromPbd.pbdDataFeatureKey]: fromPbd.reducer,
    [fromPbdIssue.pbdIssueFeatureKey]: fromPbdIssue.reducer,
    [fromPbdStart.pbdStartFeatureKey]: fromPbdStart.reducer,
    [fromPbdActivity.pbdActivityFeatureKey]: fromPbdActivity.reducer,
    [fromPbdDetails.pbdDetailsFeatureKey]: fromPbdDetails.reducer
  })(state, action);
}

export const getPbdState = createFeatureSelector<State, PbdState>(pbdFeatureKey);

export const getPbdDataState = createSelector(getPbdState, state => state.pbdState);

export const getPbdIssueData = createSelector(getPbdDataState, fromPbd.getPbdIssueData);

export const getPbdEntities = createSelector(getPbdDataState, fromPbd.selectPbdEntities);

export const getPbdIssueState = createSelector(getPbdState, state => state.pbdIssueState);

export const getPbdIssueLoading = createSelector(getPbdIssueState, fromPbdIssue.getLoading);

export const getPbdStartState = createSelector(getPbdState, state => state.pbdStartState);

export const getPbdStartLoading = createSelector(getPbdStartState, fromPbdStart.getLoading);

export const getPbdStartDocuments = createSelector(getPbdStartState, fromPbdStart.getDocuments);

export const getPbdStartError = createSelector(getPbdStartState, fromPbdStart.getError);

export const getActivityPbdState = createSelector(getPbdState, state => state.pbdActivityState);

export const getActivityPbdLoading = createSelector(getActivityPbdState, fromPbdActivity.getLoading);

export const getPbdDetailsState = createSelector(getPbdState, state => state.pbdDetailsState);

export const getPbdDetailsStateLoading = createSelector(getPbdDetailsState, fromPbdDetails.getLoading);

export const getPbd = createSelector(getPbdDataState, (state: fromPbd.State, props: { id: number }) =>
  fromPbd.getPbd(state, props.id)
);
