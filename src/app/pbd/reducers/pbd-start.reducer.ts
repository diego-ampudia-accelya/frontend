import { createReducer, on } from '@ngrx/store';

import { PbdListActions, PbdStartDialogActions } from '~app/pbd/actions';
import { TicketModel } from '~app/pbd/shared/pbd.model';
import { ServerError } from '~app/shared/errors';

export const pbdStartFeatureKey = 'pbdStartState';

export interface State {
  loading: boolean;
  documents: TicketModel[];
  error: ServerError;
}

const initialSate: State = {
  loading: false,
  documents: [],
  error: null
};

export const reducer = createReducer(
  initialSate,
  on(PbdListActions.startPbd, PbdStartDialogActions.documentNumberChange, () => ({
    ...initialSate
  })),
  on(PbdStartDialogActions.searchDocument, state => ({
    ...state,
    loading: true,
    documents: [],
    error: null
  })),
  on(PbdStartDialogActions.searchDocumentSuccess, (state, { documents }) => ({
    ...state,
    documents,
    loading: false,
    error: null
  })),
  on(PbdStartDialogActions.searchDocumentFailure, (state, { error }) => ({
    ...state,
    documents: [],
    loading: false,
    error
  }))
);

export const getLoading = (state: State): boolean => state.loading;

export const getDocuments = (state: State): TicketModel[] => state.documents;

export const getError = (state: State): ServerError => state.error;
