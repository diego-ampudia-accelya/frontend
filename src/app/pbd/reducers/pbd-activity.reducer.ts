import { createReducer, on } from '@ngrx/store';

import { PbdApiActions } from '~app/pbd/actions';

export const pbdActivityFeatureKey = 'pbdActivityState';

export interface State {
  loading: boolean;
}

const initialSate: State = {
  loading: false
};

export const reducer = createReducer(
  initialSate,
  on(PbdApiActions.savePbdActivity, state => ({
    ...state,
    loading: true
  })),
  on(PbdApiActions.savePbdActivitySuccess, PbdApiActions.savePbdActivityFailure, state => ({
    ...state,
    loading: false
  }))
);

export const getLoading = (state: State): boolean => state.loading;
