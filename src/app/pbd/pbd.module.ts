import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ActivityCommentFormComponent } from './components/activity-comment-form/activity-comment-form.component';
import { ActivityCommentItemComponent } from './components/activity-comment-item/activity-comment-item.component';
import { ActivityDashedBorderComponent } from './components/activity-dashed-border/activity-dashed-border.component';
import { ActivityDateBorderComponent } from './components/activity-date-border/activity-date-border.component';
import { ActivityHeaderComponent } from './components/activity-header/activity-header.component';
import { ActivityListComponent } from './components/activity-list/activity-list.component';
import { ActivityWorkflowItemComponent } from './components/activity-workflow-item/activity-workflow-item.component';
import { PbdActivityConfirmationDialogComponent } from './components/pbd-activity-confirmation-dialog/pbd-activity-confirmation-dialog.component';
import { PbdActivityFormComponent } from './components/pbd-activity-form/pbd-activity-form.component';
import { PbdEntityLinkComponent } from './components/pbd-entity-link/pbd-entity-link.component';
import { PbdFormComponent } from './components/pbd-form/pbd-form.component';
import { PbdHeaderComponent } from './components/pbd-header/pbd-header.component';
import { PbdIssueConfirmationDialogComponent } from './components/pbd-issue-confirmation-dialog/pbd-issue-confirmation-dialog.component';
import { ProcessCardComponent } from './components/process-card/process-card.component';
import { ProcessSectionComponent } from './components/process-section/process-section.component';
import { PbdActivityDialogComponent } from './containers/pbd-activity-dialog/pbd-activity-dialog.component';
import { PbdIssueGuard } from './guards/pbd-issue.guard';
import { PbdResolver } from './guards/pbd.resolver';
import { PbdRoutingModule } from './pbd-routing.routing';
import { EmptyMessagePipe } from './pipes/empty-message.pipe';
import { ToPbdEntityPipe } from './pipes/to-pbd-entity.pipe';
import { SharedModule } from '~app/shared/shared.module';
import { DayMonthYearPipe } from '~app/shared/pipes/day-month-year.pipe';
import * as fromPbd from '~app/pbd/reducers';
import { PbdDetailsEffects, PbdIssueDialogEffects, PbdListEffects, PbdStartDialogEffects } from '~app/pbd/effects';
import { PbdStartDialogComponent } from '~app/pbd/containers/pbd-start-dialog/pbd-start-dialog.component';
import { PbdListComponent } from '~app/pbd/containers/pbd-list/pbd-list.component';
import { PbdIssueDialogComponent } from '~app/pbd/containers/pbd-issue-dialog/pbd-issue-dialog.component';
import { PbdDetailsComponent } from '~app/pbd/containers/pbd-details/pbd-details.component';
import { PbdIssueFormComponent } from '~app/pbd/components/pbd-issue-form/pbd-issue-form.component';

@NgModule({
  declarations: [
    PbdListComponent,
    PbdStartDialogComponent,
    PbdHeaderComponent,
    ProcessSectionComponent,
    ProcessCardComponent,
    PbdDetailsComponent,
    PbdFormComponent,
    ActivityListComponent,
    ActivityWorkflowItemComponent,
    ActivityCommentItemComponent,
    ActivityDashedBorderComponent,
    ActivityDateBorderComponent,
    ActivityHeaderComponent,
    EmptyMessagePipe,
    DayMonthYearPipe,
    PbdFormComponent,
    PbdIssueFormComponent,
    PbdIssueDialogComponent,
    PbdIssueConfirmationDialogComponent,
    PbdActivityFormComponent,
    PbdActivityDialogComponent,
    PbdActivityConfirmationDialogComponent,
    ActivityCommentFormComponent,
    PbdEntityLinkComponent,
    ToPbdEntityPipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    PbdRoutingModule,
    StoreModule.forFeature(fromPbd.pbdFeatureKey, fromPbd.reducers),
    EffectsModule.forFeature([PbdStartDialogEffects, PbdDetailsEffects, PbdIssueDialogEffects, PbdListEffects])
  ],
  providers: [PbdIssueGuard, PbdResolver],
  exports: []
})
export class PbdModule {}
