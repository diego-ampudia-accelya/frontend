import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'period'
})
export class PeriodPipeMock implements PipeTransform {
  public transform(value: any, ...args: any[]): any {
    return value;
  }
}
