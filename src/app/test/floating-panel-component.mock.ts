import { Component } from '@angular/core';

import { FloatingPanelComponent } from '~app/shared/components';

@Component({
  selector: 'bspl-floating-panel',
  template: '',
  providers: [{ provide: FloatingPanelComponent, useClass: FloatingPanelComponentMockComponent }]
})
export class FloatingPanelComponentMockComponent {
  close() {}
}
