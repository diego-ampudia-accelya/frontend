import { Directive } from '@angular/core';

import { ListViewGridDirective } from '~app/shared/components/list-view';

@Directive({
  selector: '[bsplListViewGrid]',
  providers: [{ provide: ListViewGridDirective, useClass: ListViewGridMockDirective }]
})
export class ListViewGridMockDirective {
  public grid = { rows: [{ id: 123, documentNumber: 456 }] };
}
