import { Component, OnDestroy } from '@angular/core';
import {
  Event,
  NavigationCancel,
  NavigationError,
  ResolveEnd,
  ResolveStart,
  RouteConfigLoadEnd,
  RouteConfigLoadStart,
  Router
} from '@angular/router';
import { Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'bspl-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnDestroy {
  public isRouteLoading$ = this.router.events.pipe(map(event => this.isRouteLoading(event)));
  private destroy$ = new Subject();

  constructor(private router: Router) {
    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter(event => event instanceof NavigationError),
        tap(() => {
          const lastSuccessfulNavigation = this.router.getCurrentNavigation()?.previousNavigation;
          if (lastSuccessfulNavigation == null) {
            this.router.navigate(['./']);
          }
        })
      )
      .subscribe();
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  private isRouteLoading(event: Event): boolean {
    if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
      return true;
    } else if (
      event instanceof RouteConfigLoadEnd ||
      event instanceof ResolveEnd ||
      event instanceof NavigationCancel ||
      event instanceof NavigationError
    ) {
      return false;
    }
  }
}
