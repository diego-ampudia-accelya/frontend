import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AdmAcmConfirmationDialogComponent } from '~app/adm-acm/components/dialogs/adm-acm-confirmation-dialog/adm-acm-confirmation-dialog.component';
import { AdmAcmErrorDialogComponent } from '~app/adm-acm/components/dialogs/adm-acm-error-dialog/adm-acm-error-dialog.component';
import { AdmAcmNotFoundDialogComponent } from '~app/adm-acm/components/dialogs/adm-acm-not-found-dialog/adm-acm-not-found-dialog.component';
import { ActivityPanelModule } from '~app/shared/components/activity-panel/activity-panel.module';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { SharedModule } from '~app/shared/shared.module';
import { AdmAcmRoutingModule } from './adm-acm-routing.module';
import { AdmAcmAmountTaxComponent } from './components/adm-acm-amount-tax/adm-acm-amount-tax.component';
import { AdmAcmAmountComponent } from './components/adm-acm-amount/adm-acm-amount.component';
import { AdmAcmBasicInfoComponent } from './components/adm-acm-basic-info/adm-acm-basic-info.component';
import { AdmAcmCategorizationReasonsListComponent } from './components/adm-acm-categorization-reasons/adm-acm-categorization-reason-list/adm-acm-categorization-reasons-list.component';
import { AdmAcmCategorizationRulesDetailsFormComponent } from './components/adm-acm-categorization-rules/adm-acm-categorization-rules-details-form/adm-acm-categorization-rules-details-form.component';
import { AdmAcmCategorizationRulesListComponent } from './components/adm-acm-categorization-rules/adm-acm-categorization-rules-list/adm-acm-categorization-rules-list.component';
import { AdmAcmCategorizationRulesRerunClearanceDialogComponent } from './components/adm-acm-categorization-rules/adm-acm-categorization-rules-rerun-clearance-dialog/adm-acm-categorization-rules-rerun-clearance-dialog.component';
import { AdmAcmCategorizationRulesViewComponent } from './components/adm-acm-categorization-rules/adm-acm-categorization-rules-view/adm-acm-categorization-rules-view.component';
import { AdmAcmDetailsComponent } from './components/adm-acm-details/adm-acm-details.component';
import { AdmAcmEnquiryDocumentsListComponent } from './components/adm-acm-enquiry-documents-list/adm-acm-enquiry-documents-list.component';
import { AdmAcmFilesComponent } from './components/adm-acm-files/adm-acm-files.component';
import { AdmAcmMassloadAttachmentListComponent } from './components/adm-acm-files/adm-acm-massload-attachments-list/adm-acm-massload-attachment-list.component';
import { AdmAcmMassloadFileListComponent } from './components/adm-acm-files/adm-acm-massload-file-list/adm-acm-massload-file-list.component';
import { AdmAcmMassloadFileTotalsComponent } from './components/adm-acm-files/adm-acm-massload-file-list/adm-acm-massload-file-totals/adm-acm-massload-file-totals.component';
import { StopProcessingDialogComponent } from './components/adm-acm-files/adm-acm-massload-file-list/dialogs/stop-processing-dialog/stop-processing-dialog.component';
import { AdmAcmGlobalReportComponent } from './components/adm-acm-global-report/adm-acm-global-report.component';
import { AdmGlobalRequestDialogComponent } from './components/adm-acm-global-report/adm-global-request-dialog/adm-global-request-dialog.component';
import { GlobalAdmReportFilterFormatter } from './components/adm-acm-global-report/services/global-adm-report-filter-formatter';
import { AdmAcmHeaderStatusComponent } from './components/adm-acm-header-status/adm-acm-header-status.component';
import { AdmAcmActiveListComponent } from './components/adm-acm-list/adm-acm-active-list/adm-acm-active-list.component';
import { AdmAcmArchivedListComponent } from './components/adm-acm-list/adm-acm-archived-list/adm-acm-archived-list.component';
import { AdmAcmDeletedListComponent } from './components/adm-acm-list/adm-acm-deleted-list/adm-acm-deleted-list.component';
import { AdmAcmListComponent } from './components/adm-acm-list/adm-acm-list.component';
import { AdmAcmPolicyListComponent } from './components/adm-acm-policy-list/adm-acm-policy-list.component';
import { AdmAcmViewComponent } from './components/adm-acm-view/adm-acm-view.component';
import { AdmAcmComponent } from './components/adm-acm/adm-acm.component';
import { AdmPolicyGlobalFileComponent } from './components/adm-policy-global-file/adm-policy-global-file.component';
import { AdmPolicyGlobalFileService } from './components/adm-policy-global-file/services/adm-policy-global-file.service';
import { AdmAcmActivityHistoryDialogComponent } from './components/dialogs/adm-acm-activity-history-dialog/adm-acm-activity-history-dialog.component';
import { AdmAcmBspSelectionDialogComponent } from './components/dialogs/adm-acm-bsp-selection-dialog/adm-acm-bsp-selection-dialog.component';
import { AdmAcmCategorizationReasonsDialogComponent } from './components/dialogs/adm-acm-categorization-reasons-dialog/adm-acm-categorization-reasons-dialog.component';
import { AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent } from './components/dialogs/adm-acm-categorization-reasons-reactivate-deactivate-dialog/adm-acm-categorization-reasons-reactivate-deactivate-dialog.component';
import { AdmAcmCategorizationReasonsRerunClearanceDialogComponent } from './components/dialogs/adm-acm-categorization-reasons-rerun-clearance-dialog/adm-acm-categorization-reasons-rerun-clearance-dialog.component';
import { AdmAcmCategorizationRulesApplyChangesDialogComponent } from './components/dialogs/adm-acm-categorization-rules-apply-changes-dialog/adm-acm-categorization-rules-apply-changes-dialog.component';
import { AdmAcmDocumentEnquiryDialogComponent } from './components/dialogs/adm-acm-document-enquiry-dialog/adm-acm-document-enquiry-dialog.component';
import { AdmAcmInternalCommentDialogComponent } from './components/dialogs/adm-acm-internal-comment-dialog/adm-acm-internal-comment-dialog.component';
import { ForwardedAdmGlobalFileDialogComponent } from './components/dialogs/forwarded-adm-global-file-dialog/forwarded-adm-global-file-dialog.component';
import { AdmAcmDocumentEnquiryResolver } from './resolvers/adm-acm-document-enquiry.resolver';
import { AdmAcmItemResolver } from './resolvers/adm-acm-item-resolver.service';
import { AdmAcmRuleResolver } from './resolvers/adm-acm-rule.resolver';
import { AdmAcmCategorizationReasonsService } from './services/adm-acm-categorization-reasons.service';
import { AdmAcmCategorizationRulesService } from './services/adm-acm-categorization-rules.service';
import { AdmAcmDialogService } from './services/adm-acm-dialog.service';
import { AdmAcmDocumentEnquiryGuard } from './services/adm-acm-document-enquiry.guard';
import { AdmAcmDocumentEnquiryService } from './services/adm-acm-document-enquiry.service';
import { AdmAcmMassloadFileService } from './services/adm-acm-massload-file.service';
import { AdmPolicyGlobalFileGuard } from './services/adm-policy-global-file.guard';
import { ForwardedAdmGlobalFileGuard } from './services/forwarded-adm-global-file.guard';
import { AdmAcmContactEffects } from './store/effects/adm-acm-contact.effects';
import * as fromAcdm from './store/reducers';

@NgModule({
  declarations: [
    // ACDM component
    AdmAcmComponent,
    AdmAcmHeaderStatusComponent,
    AdmAcmBasicInfoComponent,
    AdmAcmDetailsComponent,
    AdmAcmAmountComponent,
    AdmAcmAmountTaxComponent,
    AdmAcmViewComponent,
    // ACDM list
    AdmAcmListComponent,
    AdmAcmActiveListComponent,
    AdmAcmArchivedListComponent,
    AdmAcmDeletedListComponent,
    AdmAcmEnquiryDocumentsListComponent,
    // ACDM policy
    AdmAcmPolicyListComponent,
    // ACDM files
    AdmAcmFilesComponent,
    AdmAcmMassloadFileListComponent,
    AdmAcmMassloadFileTotalsComponent,
    AdmAcmMassloadAttachmentListComponent,
    // ACDM dialogs
    AdmAcmConfirmationDialogComponent,
    AdmAcmErrorDialogComponent,
    AdmAcmNotFoundDialogComponent,
    StopProcessingDialogComponent,
    AdmAcmInternalCommentDialogComponent,
    AdmAcmBspSelectionDialogComponent,
    AdmAcmDocumentEnquiryDialogComponent,
    AdmAcmActivityHistoryDialogComponent,
    AdmAcmCategorizationReasonsDialogComponent,
    AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent,
    AdmAcmCategorizationReasonsRerunClearanceDialogComponent,
    AdmAcmCategorizationRulesApplyChangesDialogComponent,
    ForwardedAdmGlobalFileDialogComponent,
    // ACDM Categorization Rules
    AdmAcmCategorizationRulesListComponent,
    AdmAcmCategorizationRulesDetailsFormComponent,
    AdmAcmCategorizationRulesViewComponent,
    AdmAcmCategorizationRulesRerunClearanceDialogComponent,
    // ACDM Categorization Reasons
    AdmAcmCategorizationReasonsListComponent,
    // Global ADM Report
    AdmAcmGlobalReportComponent,
    AdmGlobalRequestDialogComponent,
    // ADM Policy Global File
    AdmPolicyGlobalFileComponent
  ],
  imports: [
    CommonModule,
    AdmAcmRoutingModule,
    ActivityPanelModule,
    SharedModule,
    StoreModule.forFeature(fromAcdm.acdmFeatureKey, fromAcdm.reducers),
    EffectsModule.forFeature([AdmAcmContactEffects])
  ],
  providers: [
    AdmAcmItemResolver,
    AdmAcmDialogService,
    AdmAcmMassloadFileService,
    ReactiveSubject,
    AdmAcmCategorizationRulesService,
    AdmAcmDocumentEnquiryGuard,
    AdmAcmDocumentEnquiryService,
    AdmAcmDocumentEnquiryResolver,
    AdmAcmCategorizationReasonsService,
    GlobalAdmReportFilterFormatter,
    AdmAcmRuleResolver,
    AdmPolicyGlobalFileGuard,
    AdmPolicyGlobalFileService,
    ForwardedAdmGlobalFileGuard
  ]
})
export class AdmAcmModule {}
