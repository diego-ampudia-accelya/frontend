import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmAcmComponent } from '~app/adm-acm/components/adm-acm/adm-acm.component';
import { AdmAcmItemResolver } from '~app/adm-acm/resolvers/adm-acm-item-resolver.service';
import { AdmAcmDocumentEnquiryGuard } from '~app/adm-acm/services/adm-acm-document-enquiry.guard';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ROUTES } from '~app/shared/constants/routes';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { AdmAcmCategorizationReasonsListComponent } from './components/adm-acm-categorization-reasons/adm-acm-categorization-reason-list/adm-acm-categorization-reasons-list.component';
import { AdmAcmCategorizationRulesDetailsFormComponent } from './components/adm-acm-categorization-rules/adm-acm-categorization-rules-details-form/adm-acm-categorization-rules-details-form.component';
import { AdmAcmCategorizationRulesListComponent } from './components/adm-acm-categorization-rules/adm-acm-categorization-rules-list/adm-acm-categorization-rules-list.component';
import { AdmAcmCategorizationRulesViewComponent } from './components/adm-acm-categorization-rules/adm-acm-categorization-rules-view/adm-acm-categorization-rules-view.component';
import { AdmAcmEnquiryDocumentsListComponent } from './components/adm-acm-enquiry-documents-list/adm-acm-enquiry-documents-list.component';
import { AdmAcmFilesComponent } from './components/adm-acm-files/adm-acm-files.component';
import { AdmAcmMassloadAttachmentListComponent } from './components/adm-acm-files/adm-acm-massload-attachments-list/adm-acm-massload-attachment-list.component';
import { AdmAcmMassloadFileListComponent } from './components/adm-acm-files/adm-acm-massload-file-list/adm-acm-massload-file-list.component';
import { AdmAcmGlobalReportComponent } from './components/adm-acm-global-report/adm-acm-global-report.component';
import { AdmAcmActiveListComponent } from './components/adm-acm-list/adm-acm-active-list/adm-acm-active-list.component';
import { AdmAcmArchivedListComponent } from './components/adm-acm-list/adm-acm-archived-list/adm-acm-archived-list.component';
import { AdmAcmDeletedListComponent } from './components/adm-acm-list/adm-acm-deleted-list/adm-acm-deleted-list.component';
import { AdmAcmListComponent } from './components/adm-acm-list/adm-acm-list.component';
import { AdmAcmPolicyListComponent } from './components/adm-acm-policy-list/adm-acm-policy-list.component';
import { AdmAcmViewComponent } from './components/adm-acm-view/adm-acm-view.component';
import { AdmAcmDocumentEnquiryResolver } from './resolvers/adm-acm-document-enquiry.resolver';
import { AdmAcmRuleResolver } from './resolvers/adm-acm-rule.resolver';
import { AdmPolicyGlobalFileGuard } from './services/adm-policy-global-file.guard';
import { ForwardedAdmGlobalFileGuard } from './services/forwarded-adm-global-file.guard';
import { getACDMTabLabel } from './shared/helpers/adm-acm-general.helper';
import { acdmFilesPermissions } from './shared/helpers/adm-acm-permissions.config';

const getListSubtabsChildren = (acdmType: MasterDataType): Routes => [
  {
    path: 'active',
    component: AdmAcmActiveListComponent,
    data: {
      title: 'ADM_ACM.query.tabs.activeTitle',
      admAcmType: acdmType
    }
  },
  {
    path: 'archive',
    component: AdmAcmArchivedListComponent,
    data: {
      title: 'ADM_ACM.query.tabs.archiveTitle',
      admAcmType: acdmType
    }
  },
  {
    path: 'deleted',
    component: AdmAcmDeletedListComponent,
    data: {
      title: 'ADM_ACM.query.tabs.deletedTitle',
      admAcmType: acdmType
    }
  },
  { path: '', redirectTo: 'active', pathMatch: 'full' }
];

const acdmFilesSubtabsChildren: Routes = [
  {
    path: 'massload',
    component: AdmAcmMassloadFileListComponent,
    data: {
      title: 'ADM_ACM.files.massload.tabLabel',
      requiredPermissions: acdmFilesPermissions.readEcCnx
    }
  },
  {
    path: 'massload-attachments',
    component: AdmAcmMassloadAttachmentListComponent,
    data: { title: 'ADM_ACM.files.massloadAttachments.tabLabel', requiredPermissions: acdmFilesPermissions.readEv }
  },
  { path: '', redirectTo: 'massload', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: ROUTES.ADM_QUERY.path,
    component: AdmAcmListComponent,
    data: {
      tab: ROUTES.ADM_QUERY,
      admAcmType: MasterDataType.Adm
    },
    children: getListSubtabsChildren(MasterDataType.Adm)
  },
  {
    path: ROUTES.ADNT_QUERY.path,
    component: AdmAcmListComponent,
    data: {
      tab: ROUTES.ADNT_QUERY,
      admAcmType: MasterDataType.Adnt
    },
    children: getListSubtabsChildren(MasterDataType.Adnt)
  },
  {
    path: ROUTES.ADM_REQUEST_QUERY.path,
    component: AdmAcmListComponent,
    data: {
      tab: ROUTES.ADM_REQUEST_QUERY,
      admAcmType: MasterDataType.Admq
    },
    children: getListSubtabsChildren(MasterDataType.Admq)
  },
  {
    path: ROUTES.ACM_QUERY.path,
    component: AdmAcmListComponent,
    data: {
      tab: ROUTES.ACM_QUERY,
      admAcmType: MasterDataType.Acm
    },
    children: getListSubtabsChildren(MasterDataType.Acm)
  },
  {
    path: ROUTES.ACNT_QUERY.path,
    component: AdmAcmListComponent,
    data: {
      tab: ROUTES.ACNT_QUERY,
      admAcmType: MasterDataType.Acnt
    },
    children: getListSubtabsChildren(MasterDataType.Acnt)
  },
  {
    path: ROUTES.ACM_REQUEST_QUERY.path,
    component: AdmAcmListComponent,
    data: {
      tab: ROUTES.ACM_REQUEST_QUERY,
      admAcmType: MasterDataType.Acmq
    },
    children: getListSubtabsChildren(MasterDataType.Acmq)
  },
  {
    path: ROUTES.ADM_POLICIES_QUERY.path,
    component: AdmAcmPolicyListComponent,
    data: {
      tab: ROUTES.ADM_POLICIES_QUERY,
      admAcmType: MasterDataType.Adm
    }
  },
  {
    path: ROUTES.ACDM_CATEGORIZATION_RULES.path,
    component: AdmAcmCategorizationRulesListComponent,
    data: { tab: ROUTES.ACDM_CATEGORIZATION_RULES }
  },
  {
    path: ROUTES.ACDM_CATEGORIZATION_RULES_CREATE.path,
    component: AdmAcmCategorizationRulesDetailsFormComponent,
    data: { tab: ROUTES.ACDM_CATEGORIZATION_RULES_CREATE }
  },
  {
    path: ROUTES.ACDM_CATEGORIZATION_RULES_EDIT.path,
    component: AdmAcmCategorizationRulesDetailsFormComponent,
    data: { tab: ROUTES.ACDM_CATEGORIZATION_RULES_EDIT },
    resolve: {
      acdmRule: AdmAcmRuleResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ACDM_CATEGORIZATION_RULES_VIEW.path,
    component: AdmAcmCategorizationRulesViewComponent,
    data: { tab: ROUTES.ACDM_CATEGORIZATION_RULES_VIEW }
  },
  {
    path: ROUTES.ACDM_FILES.path,
    component: AdmAcmFilesComponent,
    data: { tab: ROUTES.ACDM_FILES },
    children: acdmFilesSubtabsChildren
  },
  {
    path: ROUTES.ACDM_DOCUMENT_ENQUIRY.path,
    canActivate: [AdmAcmDocumentEnquiryGuard]
  },
  {
    path: ROUTES.ACDM_DOCUMENT_ENQUIRY_LIST.path,
    component: AdmAcmEnquiryDocumentsListComponent,
    resolve: {
      documentNumber: AdmAcmDocumentEnquiryResolver
    },
    data: {
      tab: ROUTES.ACDM_DOCUMENT_ENQUIRY_LIST
    }
  },
  {
    path: ROUTES.ACDM_CATEGORIZATION_REASONS.path,
    component: AdmAcmCategorizationReasonsListComponent,
    data: {
      tab: ROUTES.ACDM_CATEGORIZATION_REASONS
    }
  },
  {
    path: ROUTES.ADM_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ADM_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Adm
    }
  },
  {
    path: ROUTES.ACM_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ACM_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Acm
    }
  },
  {
    path: ROUTES.ADM_ISSUE_PENDING.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ADM_ISSUE_PENDING,
      access: AccessType.create,
      admAcmType: MasterDataType.Adm,
      issuingPendingSupervision: true
    }
  },
  {
    path: ROUTES.ACM_ISSUE_PENDING.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ACM_ISSUE_PENDING,
      access: AccessType.create,
      admAcmType: MasterDataType.Acm,
      issuingPendingSupervision: true
    }
  },
  {
    path: ROUTES.ADM_REQUEST_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ADM_REQUEST_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Admq
    }
  },
  {
    path: ROUTES.ACM_REQUEST_ISSUE.path,
    component: AdmAcmComponent,
    data: {
      tab: ROUTES.ACM_REQUEST_ISSUE,
      access: AccessType.create,
      admAcmType: MasterDataType.Acmq
    }
  },
  {
    path: ROUTES.ADM_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ADM_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Adm
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ADNT_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ADNT_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Adnt
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ACM_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ACM_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Acm
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ADM_REQUEST_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ADM_REQUEST_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Admq
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ACNT_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ACNT_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Acnt
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ACM_REQUEST_VIEW.path,
    component: AdmAcmViewComponent,
    data: {
      tab: { ...ROUTES.ACM_REQUEST_VIEW, getTabLabel: getACDMTabLabel },
      access: AccessType.read,
      admAcmType: MasterDataType.Acmq
    },
    resolve: {
      item: AdmAcmItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.ADM_EDIT_REACTIVATE.path,
    component: AdmAcmComponent,
    data: {
      tab: { ...ROUTES.ADM_EDIT_REACTIVATE, getTabLabel: getACDMTabLabel },
      access: AccessType.edit,
      admAcmType: MasterDataType.Adm
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  },
  {
    path: ROUTES.ACM_EDIT_REACTIVATE.path,
    component: AdmAcmComponent,
    data: {
      tab: { ...ROUTES.ACM_EDIT_REACTIVATE, getTabLabel: getACDMTabLabel },
      access: AccessType.edit,
      admAcmType: MasterDataType.Acm
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  },
  {
    path: ROUTES.ADM_EDIT_REJECT.path,
    component: AdmAcmComponent,
    data: {
      tab: { ...ROUTES.ADM_EDIT_REJECT, getTabLabel: getACDMTabLabel },
      access: AccessType.edit,
      admAcmType: MasterDataType.Adm
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  },
  {
    path: ROUTES.ACM_EDIT_REJECT.path,
    component: AdmAcmComponent,
    data: {
      tab: { ...ROUTES.ACM_EDIT_REJECT, getTabLabel: getACDMTabLabel },
      access: AccessType.edit,
      admAcmType: MasterDataType.Acm
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  },
  {
    path: ROUTES.ADM_SUPERVISION.path,
    component: AdmAcmComponent,
    data: {
      tab: { ...ROUTES.ADM_SUPERVISION, getTabLabel: getACDMTabLabel },
      access: AccessType.edit,
      admAcmType: MasterDataType.Adm
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  },
  {
    path: ROUTES.ACM_SUPERVISION.path,
    component: AdmAcmComponent,
    data: {
      tab: { ...ROUTES.ACM_SUPERVISION, getTabLabel: getACDMTabLabel },
      access: AccessType.edit,
      admAcmType: MasterDataType.Acm
    },
    resolve: {
      item: AdmAcmItemResolver
    }
  },
  {
    path: ROUTES.GLOBAL_ADM_REPORTS_QUERY.path,
    component: AdmAcmGlobalReportComponent,
    data: {
      tab: ROUTES.GLOBAL_ADM_REPORTS_QUERY
    }
  },
  {
    path: ROUTES.ADM_POLICY_GLOBAL_FILE.path,
    canActivate: [AdmPolicyGlobalFileGuard]
  },
  {
    path: `${ROUTES.FORWARDED_ADM_GLOBAL_FILE.path}`,
    canActivate: [ForwardedAdmGlobalFileGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmAcmRoutingModule {}
