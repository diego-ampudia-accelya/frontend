export interface AdmAcmMassloadAttachmentFilter {
  filename: string;
  documentNumber: string;
  uploadDate: Date[];
  userName: string;
  email: string;
}
