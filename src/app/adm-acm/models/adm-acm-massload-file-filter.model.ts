import { AdmAcmMassloadFileStatus } from './adm-acm-massload-file.model';

export interface AdmAcmMassloadFileFilter {
  filename: string;
  uploadDate: Date[];
  requestDate: Date[];
  userName: string;
  email: string;
  status: AdmAcmMassloadFileStatus[];
}
