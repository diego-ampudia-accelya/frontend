import { TransactionCode } from './adm-acm-issue-shared-aux.model';

export interface AdmAcmMassloadAttachment {
  id: number;
  filename: string;
  documentNumber: string;
  documentId: number;
  documentTrnc: TransactionCode;
  uploadDate: Date;
  userName: string;
  email: string;
}
