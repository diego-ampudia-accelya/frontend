import { MasterDataType } from '~app/master-shared/models/master.model';
import { ROUTES } from '~app/shared/constants/routes';

export interface Calculation {
  commission: number;
  fare: number;
  supplementaryCommission: number; //* We send 0 if by config there is no supplementaryCommission field
  tax: number;
  taxOnCommission: number;
  cancellationPenalty: number;
  miscellaneousFee: number;
}

export interface TaxMiscellaneousFee {
  agentAmount: number; //* We send 0 if by config there is no agentAmount field
  airlineAmount: number; //* We send 0 if by config there is no airlineAmount field
  taxDifferenceAmount: number;
  type: string;
}

export interface TaxMiscellaneousTicketFee {
  amount: number;
  type: string;
}

export interface RelatedTicketDocument {
  relatedTicketDocumentNumber: string;
  documentId: string;
}

export enum ConcernIndicator {
  issue = 'ISSUE',
  refund = 'REFUND',
  exchange = 'EXCHANGE',
  emd = 'EMD'
}

export enum AdmAcmStatus {
  pending = 'PENDING',
  disputed = 'DISPUTED',
  approved = 'DISPUTE_APPROVED',
  rejected = 'DISPUTE_REJECTED',
  deactivated = 'DEACTIVATED',
  deleted = 'DELETED',
  pendingRequest = 'PENDING_REQUEST',
  authorizeRequest = 'AUTHORIZED_REQUEST',
  rejectRequest = 'REJECTED_REQUEST',
  billingDisputed = 'BILLING_DISPUTED',
  pendingAuthorization = 'PENDING_AUTHORIZATION',
  supervisionDeleted = 'SUPERVISION_DELETED'
}

export interface Reason {
  id: number;
  reason: string;
  action: ReasonAction;
  date?: string;
}

export enum ReasonAction {
  issue = 'ISSUE',
  dispute = 'DISPUTE',
  disputeRejected = 'DISPUTE_REJECT'
}

export enum TransactionCode {
  adm = 'ADMA',
  acm = 'ACMA',
  spdr = 'SPDR',
  spcr = 'SPCR',
  'adm-request' = 'ADMA',
  'acm-request' = 'ACMA',
  admd = 'ADMD',
  acmd = 'ACMD',
  adnt = 'ADNT',
  acnt = 'ACNT'
}

export enum AcdmType {
  // ACDM
  ADMA = 'adm',
  ACMA = 'acm',
  ADNT = 'adnt',
  ACNT = 'acnt',
  ADMQ = 'adm-request',
  ACMQ = 'acm-request',
  // BSP Adjustment
  SPDR = 'spdr',
  SPCR = 'spcr',
  ADMD = 'admd',
  ACMD = 'acmd'
}

export enum AcdmActionType {
  issue = 'issue',
  details = 'details',
  dispute = 'dispute',
  reject = 'reject',
  approve = 'approve',
  deactivate = 'deactivate',
  reactivate = 'reactivate',
  forwardGds = 'forwardGds',
  authorizeRequest = 'authorizeRequest',
  rejectRequest = 'rejectRequest',
  delete = 'delete',
  editReactivateACDM = 'editReactivate',
  editRejectACDM = 'editReject',
  supervise = 'supervise',
  issuePendingSupervision = 'issuePendingSupervision',
  archive = 'archive',
  markAsRead = 'markAsRead',
  markAsUnread = 'markAsUnread',
  moveToActive = 'moveToActive',
  internalComment = 'internalComment',
  activity = 'activity',
  startWatching = 'startWatching',
  stopWatching = 'stopWatching'
}

export enum BEAcdmActionType {
  loading = 'loading',
  dispute = 'DISPUTE',
  reject = 'REJECT DISPUTE',
  approve = 'APPROVE DISPUTE',
  deactivate = 'DEACTIVATE',
  reactivate = 'REACTIVATE',
  forwardGds = 'FORWARD TO GDS',
  authorizeRequest = 'AUTHORIZE REQUEST',
  rejectRequest = 'REJECT REQUEST',
  delete = 'DELETE',
  details = 'DETAILS',
  editReject = 'EDIT & REJECT DISPUTE',
  editReactivate = 'EDIT & REACTIVATE',
  edit = 'EDIT',
  authorize = 'AUTHORIZE',
  disputePostBilling = 'DISPUTE POST-BILLING',
  deletePostBilling = 'DELETE POST-BILLING',
  supervise = 'SUPERVISE',
  internalComment = 'INTERNAL_COMMENT',
  activity = 'ACTIVITY'
}

export enum AcdmOptionType {
  Loading = 'LOADING',
  Read = 'READ',
  Unread = 'UNREAD',
  Archive = 'ARCHIVE',
  DownloadAttachments = 'DOWNLOAD_ATTACHMENTS'
}

export enum TicketTransactionCode {
  RFND = 'RFND',
  RFNC = 'RFNC',
  TKTT = 'TKTT',
  EMDA = 'EMDA',
  EMDS = 'EMDS'
}

export enum TicketNetReportingIndicator {
  NR = 'NR'
}

export enum TicketTaxType {
  CP = 'CP',
  MF = 'MF'
}

export enum AcdmNumberingMode {
  automatic = 'automatic',
  manual = 'manual'
}

export enum SendToDpcType {
  sent = 'SENT',
  pending = 'PENDING',
  n_a = 'N_A',
  na = 'N/A'
}

export enum MarkStatus {
  read = 'READ',
  pending = 'UNREAD'
}

export const urlList = {
  adm: ROUTES.ADM_QUERY.url,
  acm: ROUTES.ACM_QUERY.url,
  spdr: ROUTES.SPDR_QUERY.url,
  spcr: ROUTES.SPCR_QUERY.url,
  'adm-request': ROUTES.ADM_REQUEST_QUERY.url,
  'acm-request': ROUTES.ACM_REQUEST_QUERY.url,
  admd: ROUTES.ADMD_QUERY.url,
  acmd: ROUTES.ACMD_QUERY.url
};

export const urlView = {
  adm: ROUTES.ADM_VIEW.url,
  acm: ROUTES.ACM_VIEW.url,
  spdr: ROUTES.SPDR_VIEW.url,
  spcr: ROUTES.SPCR_VIEW.url,
  'adm-request': ROUTES.ADM_REQUEST_VIEW.url,
  adm_request: ROUTES.ADM_REQUEST_VIEW.url,
  'acm-request': ROUTES.ACM_REQUEST_VIEW.url,
  acm_request: ROUTES.ACM_REQUEST_VIEW.url,
  admd: ROUTES.ADMD_VIEW.url,
  acmd: ROUTES.ACMD_VIEW.url,
  adnt: ROUTES.ADNT_VIEW.url,
  acnt: ROUTES.ACNT_VIEW.url,

  //Neccessary for Search by Document Number
  adma: ROUTES.ADM_VIEW.url,
  acma: ROUTES.ACM_VIEW.url
};

export function getViewUrlFromTransactionCode(transactionCode: TransactionCode): string {
  const transactionCodeKey = Object.keys(TransactionCode).find(key => TransactionCode[key] === transactionCode);

  return urlView[transactionCodeKey];
}

export function getActiveQueryUrl(acdmType: MasterDataType): string {
  return `${urlList[acdmType]}/active`;
}

export function getArchiveQueryUrl(acdmType: MasterDataType): string {
  return `${urlList[acdmType]}/archive`;
}

export function getDeleteQueryUrl(acdmType: MasterDataType): string {
  return `${urlList[acdmType]}/deleted`;
}

export const urlCreate = {
  adm: ROUTES.ADM_ISSUE.url,
  acm: ROUTES.ACM_ISSUE.url,
  spdr: ROUTES.SPDR_ISSUE.url,
  spcr: ROUTES.SPCR_ISSUE.url,
  'adm-request': ROUTES.ADM_REQUEST_ISSUE.url,
  'acm-request': ROUTES.ACM_REQUEST_ISSUE.url,
  admd: ROUTES.ADMD_ISSUE.url,
  acmd: ROUTES.ACMD_ISSUE.url
};

const indexedUrls: { [k in AcdmActionType]?: { [u in MasterDataType]?: string } } = {
  [AcdmActionType.details]: urlView,
  [AcdmActionType.editReactivateACDM]: {
    [MasterDataType.Adm]: ROUTES.ADM_EDIT_REACTIVATE.url,
    [MasterDataType.Acm]: ROUTES.ACM_EDIT_REACTIVATE.url
  },
  [AcdmActionType.editRejectACDM]: {
    [MasterDataType.Adm]: ROUTES.ADM_EDIT_REACTIVATE.url,
    [MasterDataType.Acm]: ROUTES.ACM_EDIT_REACTIVATE.url
  },
  [AcdmActionType.supervise]: {
    [MasterDataType.Adm]: ROUTES.ADM_SUPERVISION.url,
    [MasterDataType.Acm]: ROUTES.ACM_SUPERVISION.url
  },
  [AcdmActionType.issue]: {
    [MasterDataType.Adm]: ROUTES.ADM_ISSUE.url,
    [MasterDataType.Acm]: ROUTES.ACM_ISSUE.url
  },
  [AcdmActionType.issuePendingSupervision]: {
    [MasterDataType.Adm]: ROUTES.ADM_ISSUE_PENDING.url,
    [MasterDataType.Acm]: ROUTES.ACM_ISSUE_PENDING.url
  }
};

export const getUrlAcdmIndexed = (actionType: AcdmActionType, dataType: MasterDataType): string => {
  let url: any = indexedUrls[actionType];

  if (url) {
    url = url[dataType];
  }

  return url || '';
};

export enum AirlineConfigParamName {
  acdmModify = 'acdm_modify',
  acdmDailyDownload = 'acdm_dailydownload',
  acdmDailyDownlBsp = 'acdm_dailydownl_bsp',
  acdmDailyDownlStat = 'acdm_dailydownl_stat',
  acdmDailyDownlDisp = 'acdm_dailydownl_disp',
  acdmDownlAttachment = 'acdm_downlattachment',
  acdmDailyDownlQ = 'acdm_dailydownl_q',
  acdmDailyDownlPbd = 'acdm_dailydownl_pbd',
  admq = 'admq',
  acmq = 'acmq',
  handlingFee = 'HANDLING_FEE',
  penaltyCharge = 'PENALTY_CHARGE',
  acdntDailyDownlDis = 'acdnt_dailydownl_dis',
  acdntDispute = 'acdnt_dispute',
  acdntDailyDownlSta = 'acdnt_dailydownl_sta'
}
