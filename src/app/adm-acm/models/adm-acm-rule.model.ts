import { AirlineSummary } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';

export interface AdmAcmRule {
  id?: number;
  name?: string;
  airlineCode?: string;
  isoCountryCodes?: string[];
  concernsIndicator?: TransactionRuleType | AnyOption;
  reasonForMemoField?: RmicType;
  pattern?: string;
  position?: PositionRuleType;
  primaryReason?: string;
  primaryReasonId?: number;
  subReason?: string;
  subReasonId?: number;
  reasonStatus?: ReasonStatusRuleType;
  status?: StatusRuleType;
  readonly?: boolean;
}

export interface AdmAcmRuleFilter {
  name?: string;
  bsps?: BspDto[];
  airlineCode?: AirlineSummary;
  concernsIndicator?: TransactionRuleType | AnyOption;
  reasonForMemoField?: RmicType;
  pattern?: string;
  primaryReason?: PrimaryReasonRule;
  subReason?: SubReasonRule[];
  status?: StatusRuleType;
  position?: PositionRuleType;
}

export interface AcmAdmRuleFilterBE {
  id?: number;
  name?: string;
  airlineCode?: string;
  isoCountryCodes?: string[];
  concernsIndicator: TransactionRuleType | AnyOption;
  reasonForMemoField: RmicType;
  pattern?: string;
  position?: PositionRuleType;
  primaryReason?: string;
  primaryReasonId?: number;
  subReason?: string;
  subReasonId: number[];
  reasonStatus?: ReasonStatusRuleType;
  status?: StatusRuleType;
  readonly?: boolean;
}
export interface AcmAdmRuleDetail {
  id?: number;
  airlineCode: AirlineSummary;
  status: StatusRuleType;
  isoCountryCodes: BspDto[];
  concernsIndicator?: TransactionRuleType | AnyOption;
  reasonForMemoField?: RmicType;
  position: PositionRuleType;
  primaryReasonId: number;
  subReasonId: number;
  pattern: string;
}

export interface AcmAdmRuleChanges {
  airlineCode: AirlineSummary;
  status: StatusRuleType;
  isoCountryCodes: BspDto[];
  concernsIndicator: TransactionRuleType | AnyOption;
  reasonForMemoField: RmicType;
  position: PositionRuleType;
  primaryReasonId: string;
  subReasonId: string;
  pattern: string;
}

export interface AcmAdmRuleDetailApplyChangeViewModel {
  deactivationType: boolean;
}

export interface AcmAdmRuleDetailBE {
  airlineCode: string;
  status: StatusRuleType;
  isoCountryCodes: string[] | AllOption;
  concernsIndicator: TransactionRuleType | AnyOption;
  reasonForMemoField: RmicType;
  position: PositionRuleType;
  primaryReasonId: number;
  subReasonId: number;
  pattern: string;
}

export interface AdmAcmRulesRerunClearanceDialogViewModel {
  bsp: Bsp;
  airline: AirlineSummary;
  issueDate?: string;
}

export interface PrimaryReasonRule {
  id: number;
  primaryReason: string;
  status: string;
}
export interface SubReasonRule {
  id: number;
  subReason: string;
  status: string;
}

export enum TransactionRuleType {
  ANY = 'ANY',
  ISSUE = 'Issue',
  REFUND = 'Refund',
  EXCHANGE = 'Exchange',
  EMD = 'EMD'
}

export enum PositionRuleType {
  STARTS_WITH = 'Starts with',
  CONTAINS = 'Contains'
}

export enum StatusRuleType {
  ACTIVE = 'Active',
  NON_ACTIVE = 'Non-Active'
}

export enum ReasonStatusRuleType {
  ENABLED = 'Enabled',
  DISABLED = 'Disabled'
}

export enum RmicType {
  RMIC = 'RMIC',
  RMIN = 'RMIN'
}

export enum AllOption {
  LABEL = 'All',
  VALUE = 'ALL'
}

export enum AnyOption {
  LABEL = 'Any Related Transaction',
  VALUE = 'ANY'
}
