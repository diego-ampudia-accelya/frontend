import { Observable } from 'rxjs';

import { Contact } from './adm-acm-issue-be-aux.model';
import {
  AdmAcmStatus,
  AirlineConfigParamName,
  Calculation,
  ConcernIndicator,
  Reason,
  RelatedTicketDocument,
  SendToDpcType,
  TaxMiscellaneousFee
} from './adm-acm-issue-shared-aux.model';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AgentContact } from '~app/master-data/models/agent-contact.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

//* Main View Sections
export interface StatusHeaderViewModel {
  ticketDocumentNumber: number;
  status: StatusViewModel;
  bspName: string;
  agentCode: string; //* To be connected with AgentViewModel.iataCode
  acdmaFor?: string;
  issueDate: string;
  amount: string; //* String because will include already the currency (related with totalAmount)
}

export interface StatusViewModel {
  status: AdmAcmStatus;
  sentToDpc: SendToDpcType;
}

export interface BasicInformationViewModel {
  ticketDocumentNumber: number;
  reportingDate: string; // TODO To be confirmed. It will be => today + 15 days on creation (on edition comming from BE)
  bspName: string;
  type: string; //* Fixed for now as 'ADM'
  airline: AirlineViewModel;
  agent: AgentViewModel;
  airlineContact: ContactViewModel;
  admSettings: AdmSettingViewModel;
}

export interface DetailViewModel {
  disputeContact: string;
  disputeTime: string;
  gdsForwards: ForwardGdsViewModel[];
  reasons: Reason[];
  reasonsDetails: ReasonDetailViewModel;
  relatedDocuments: RelatedDocumentViewModel;
}

export interface AmountViewModel {
  agentCalculations: Calculation;
  airlineCalculations: Calculation;
  differenceCalculations: Calculation;
  agentTotal: number;
  airlineTotal: number;
  totalAmount: number;
}

export interface TaxViewModel {
  taxMiscellaneousFees: TaxMiscellaneousFee[];
}
//* --- End of Main View Sections

/**
 * Defines the type used to indicate the sign and visibility of every calculation field for Amount List
 */
export type AmountCalculationFieldConfig = {
  [field in keyof Calculation]: { sign: 1 | -1; visible: Observable<boolean> };
};

//* User has an airline associated (get airline name from AirlineController endpoint using id associated with current user)
//! Vat number and contact details are filled by user (do not use data comming from Airline)
export interface AirlineViewModel {
  id: number;
  iataCode: string; //* Airline field (comming from Airline endpoint) // Equivalent to Airline code
  localName: string;
  globalName: string;
  vatNumber: string;
  acdmVatNumber?: string;
  bsp: BspDto;
  contact: Contact;
  address: AirlineAddress;
}

export interface AgentViewModel {
  id: number;
  iataCode: string; //* Equivalent to agent code
  name: string;
  vatNumber: string;
  acdmVatNumber?: string;
  bsp: BspDto;
  contact?: AgentContact;
  address: AgentAddress;
}

export interface ContactViewModel {
  contactName: string;
  phoneFaxNumber: string;
  email: string;
}

export interface AdmSettingViewModel {
  concernsIndicator: ConcernIndicator; //* ISSUE, REFUND (does not applied for 12th), EXCHANGE, EMD
  taxOnCommissionType: TocaTypeViewModel;
  currency: CurrencyViewModel;
  statisticalCode: string; //* INT (label: International), DOM (label: Domestic)
  netReporting: boolean;
}

export interface ReasonDetailViewModel {
  reasonForIssue?: string;
  reasonForMemo: string;
  reasonForMemoIssuanceCode: string;
}

export interface RelatedDocumentViewModel {
  dateOfIssueRelatedDocument: string;
  relatedTicketDocuments: RelatedTicketDocument[];
}

export interface CurrencyViewModel {
  code: string;
  decimals: number;
  id: number;
  isDefault: boolean;
}
export interface TocaTypeViewModel {
  bspAcdmConfigId: number;
  bspId: number;
  description: string;
  id: number;
  sequenceNumber: number;
  type: string;
}

export interface DisputeViewModel {
  status: AdmAcmStatus;
  disputeReason?: number;
  reason: string;
  disputeContact?: string;
}

export interface AuthorizeRejectReqViewModel {
  status: AdmAcmStatus;
  reason?: string;
}

// TODO Add other BSP conf fields when needed
export interface AdmAcmBspConfig {
  positiveVat: boolean;
  admLatencyDays: number;
  acmLatencyDays: number;
  admGdsForward: boolean;
  acdmSpam: boolean;
  freeStat: boolean;
  defaultStat: string;
  acdmToca: boolean;
  airlineVatnumber: boolean;
  agentVatNumber: boolean;
  acdmCaptureMode: string;
  acmNumberPrefix: string;
  admNumberPrefix: string;
  acmNumberLength: number;
  admNumberLength: number;
}

export interface AdmAcmAirlineConfig {
  descriptorId: number;
  parameters: AirlineConfigParameters[];
  scopeId: string;
  scopeType: string;
  service: string;
}

export interface AirlineConfigParameters {
  descriptorId: number;
  id: number;
  name: AirlineConfigParamName;
  readonly: boolean;
  type: string;
  value: string;
}

export interface DeactivateViewModel {
  status: AdmAcmStatus;
  reason: string;
}

export interface ForwardGdsViewModel {
  forwardDate: string;
  forwardReason: string;
  gds: GdsViewModel;
}

export interface GdsViewModel {
  id: number;
  gdsCode: string;
  name: string;
  refundsEnabled?: boolean;
}

export interface SecondaryHeaderActionsViewModel {
  label: string;
  action: GridTableActionType;
}
