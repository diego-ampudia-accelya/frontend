import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { AirlineContact } from '~app/master-data/models/airline.contact.model';
import { BspDto } from '~app/shared/models/bsp.model';

/**
 * Interface to map BE response
 */

export interface AdmAcmPolicyModel {
  id: number;
  url: string;
  email: string;
  comment: string;
  lastUpdate: string;
  bsp: BspDto;
  airline: AirlinePolicyModel;
}

interface BspPolicyModel {
  version: number;
}

interface AirlinePolicyModel {
  id: number;
  iataCode: string;
  bsp: BspPolicyModel;
  localName: string;
  designator: string;
  address: AirlineAddress;
  contact: AirlineContact;
}
