export interface ForwardedAdmGlobalFileFilters {
  airline?: string;
  date?: Date[];
}
