import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

export interface AdmAcmReasons {
  id?: number;
  primaryReason?: string;
  primaryReasonId?: string;
  subReason?: string;
  active?: boolean;
  airlineCode?: string;
  bspIds?: number[];
  fromIssueDate?: string;
  toIssueDate?: string;
}

export interface AdmAcmReasonsFilter {
  primaryReason?: AdmAcmReasonsPrimaryReason;
  subReason?: string;
  active?: boolean;
}

export interface AdmAcmReasonsPrimaryReason {
  id: string;
  primaryReason: string;
}

export interface AdmAcmReasonsCreateDialogViewModel {
  reasonType: boolean;
  newPrimaryReason: string;
  existingPrimaryReason: string;
  subReason: string;
}

export interface AdmAcmReasonsDeactivateReactivateDialogViewModel {
  deactivationType?: boolean;
  primaryReason: string;
  subReason: string;
}
export interface AdmAcmReasonsRerunClearanceDialogViewModel {
  bsp: Bsp;
  airline: AirlineSummary;
  issueDate?: string;
}

export interface AdmAcmReasonsCreateDialog {
  primaryReason?: string;
  subReason: string;
}

export enum StatusReasonsType {
  active = 'active',
  inactive = 'inactive'
}

export enum AllOption {
  LABEL = 'All',
  VALUE = 'ALL'
}
