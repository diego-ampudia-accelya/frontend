export interface AdmAcmMassloadFile {
  id: number;
  filename: string;
  userName: string;
  email: string;
  status: AdmAcmMassloadFileStatus;
  uploadDate?: Date;
  requestDate?: Date;
  adma?: number;
  acma?: number;
  total?: number;
  evaluationFileId?: string;
}

export interface AdmAcmMassloadFileViewModel extends AdmAcmMassloadFile {
  disableSelection: boolean;
}

export interface AdmAcmMassloadFileTotals {
  adma: number;
  acma: number;
  total: number;
}

export enum AdmAcmMassloadFileStatus {
  Processed = 'PROCESSED',
  Rejected = 'REJECTED',
  Pending = 'PENDING',
  NotProcessed = 'NOT_PROCESSED',
  NotFound = 'NOT_FOUND'
}

export enum AdmAcmMassloadFileListOption {
  Active = 'active',
  NotProcessed = 'notProcessed'
}
