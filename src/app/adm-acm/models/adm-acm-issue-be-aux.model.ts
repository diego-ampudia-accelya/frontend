import { Agent } from '~app/master-data/models';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { LocalAndGlobalAirline } from '~app/master-data/models/airline/local-and-global-airline.model';
import { Gds } from '~app/master-data/models/gds.model';

export interface Contact {
  contactName: string;
  email: string;
  phoneFaxNumber: string;
}

export interface DTO {
  id: number;
}

export type AgentDto = DTO;
export type AirlineDto = DTO;
export type CurrencyDto = DTO;

export interface AcdmReason {
  id: number;
  description: string;
  reasonType: string;
  title: string;
  airlineId?: number;
  bspId?: number;
}

export interface BasicAgentInfo {
  agent: Partial<Agent>;
  agentVatNumber: string;
  agentAddress: AgentAddress;
  agentContact: Contact;
}

export interface BasicAirlineInfo {
  airline: Partial<LocalAndGlobalAirline>;
  airlineVatNumber: string;
  airlineAddress: AirlineAddress;
  airlineContact: Contact;
}

export interface BspTimeModel {
  id: number;
  localTime: Date;
}

export interface GdsForward {
  forwardDate: string;
  forwardReason: string;
  gds: Gds;
}
