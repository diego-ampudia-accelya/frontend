import { AdmAcmStatus, ConcernIndicator, SendToDpcType } from './adm-acm-issue-shared-aux.model';
import { PeriodOption } from '~app/shared/components';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { Category } from '~app/shared/models/dictionary/category.model';
import { GdsSummary } from '~app/shared/models/dictionary/gds-summary.model';

export interface AdmAcmFilter {
  bsp?: BspDto[] | BspDto;
  ticketDocumentNumber: string;
  airlineIataCode?: AirlineSummary[];
  agentId?: AgentSummary[];
  acdmStatus: AdmAcmStatus[] | string[];
  gdsForwardCode?: GdsSummary[];
  currencyId: Currency[];
  amountRange?: number[];
  concernsIndicator: ConcernIndicator[];
  dateOfIssue?: Date[];
  period?: PeriodOption[];
  reportingDate?: Date[];
  sentToDpc?: SendToDpcType[];
  hasInternalComment?: boolean;
  markStatus?: string | string[];
  primaryReason?: Category;
  reason?: Category[];
  disputeDate?: Date[];
}
