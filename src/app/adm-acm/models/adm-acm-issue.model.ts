import { Observable } from 'rxjs';

import { AgentDto, AirlineDto, Contact, CurrencyDto } from './adm-acm-issue-be-aux.model';
import {
  AdmAcmStatus,
  Calculation,
  ConcernIndicator,
  Reason,
  RelatedTicketDocument,
  SendToDpcType,
  TaxMiscellaneousFee
} from './adm-acm-issue-shared-aux.model';
import {
  AmountViewModel,
  BasicInformationViewModel,
  DetailViewModel,
  ForwardGdsViewModel,
  StatusHeaderViewModel,
  TaxViewModel
} from './adm-acm-issue-view-aux.model';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { AlertMessageType } from '~app/shared/enums';
import { Category } from '~app/shared/models';

/**
 * Interface that will be used to deal with BE (get and send data)
 */
export interface AdmAcmIssueBE {
  id?: number;
  acdmStatus?: AdmAcmStatus; //* Readonly for BE
  agent: AgentDto;
  agentAddress: AgentAddress;
  agentCalculations: Calculation;
  agentTotal: number;
  agentVatNumber?: string;
  attachmentIds?: string[];
  airline: AirlineDto;
  airlineAddress: AirlineAddress;
  airlineCalculations: Calculation;
  airlineContact: Contact;
  airlineIssued?: boolean;
  airlineTotal: number;
  airlineVatNumber?: string;
  categories?: Category[];
  concernsIndicator: ConcernIndicator;
  currency: CurrencyDto;
  dateOfIssue?: string; //* Readonly for BE
  dateOfIssueRelatedDocument?: string;
  deletionReason?: string;
  issueReason?: string;
  differenceCalculations: Calculation;
  disputeContact?: string;
  disputeTime?: string;
  gdsForwards?: ForwardGdsViewModel[];
  internalComment?: string;
  netReporting: boolean;
  period?: string;
  reasonForMemo?: string;
  reasonForMemoIssuanceCode?: string;
  reasons?: Reason[];
  regularized?: boolean;
  relatedTicketDocuments?: RelatedTicketDocument[];
  reportingDate?: string; //* Readonly for BE
  reportingPeriod?: string;
  sentToDpc?: SendToDpcType;
  statisticalCode: string;
  taxMiscellaneousFees?: TaxMiscellaneousFee[];
  taxOnCommissionType: string;
  ticketDocumentNumber?: string; //* Readonly for BE
  totalAmount: number;
  transactionCode: string;
  watching?: boolean;
}

/**
 * Interface that will be used in our FE components.
 */
export interface AdmAcmIssueViewModel {
  id?: number;
  attachmentIds?: string[];
  statusHeader: StatusHeaderViewModel;
  basicInformation: BasicInformationViewModel;
  details: DetailViewModel;

  amounts: AmountViewModel;
  taxes: TaxViewModel;
}

export enum AdmAcmActionEmitterType {
  netReporting = 'netReporting',
  currency = 'currency',
  agent = 'agent',
  acdmaFor = 'acdmaFor',
  totalAmount = 'totalAmount',
  totalTaxAmount = 'totalTaxAmount',
  status = 'status',
  gdsForwardReasons = 'gdsForwardReasons',
  reasons = 'reasons',
  airline = 'airline',
  bsp = 'bsp',
  hasToca = 'hasToca',
  tocaAmount = 'tocaAmount',
  tocaType = 'tocaType'
}

export interface AcdmAlertMessage {
  hidden: boolean | Observable<boolean>;
  message: string;
  type: AlertMessageType;
}

export enum TocaErrorsType {
  type = 'type',
  amount = 'amount'
}
