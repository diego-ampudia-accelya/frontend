import { AdmAcmStatus, MarkStatus, SendToDpcType, TransactionCode } from './adm-acm-issue-shared-aux.model';
import { TransactionRuleType } from './adm-acm-rule.model';
import { AirlineSummaryAux } from '~app/document/credit-card-documents/models/credit-card-document.model';
import { Agent } from '~app/master-data/models';
import { FooterButton } from '~app/shared/components';
import { Currency } from '~app/shared/models/currency.model';
import { ModalAction } from '~app/shared/models/modal-action.model';

export interface AdmAcmEnquiryDialogData {
  title: string;
  documentNumber: string;
  isSearching: boolean;
  isDocumentFound: boolean;
  footerButtonsType: (string | ModalAction)[];
  buttons?: ModalAction[];
  mainButtonType: FooterButton;
}

export interface AdmAcmDocumentSummary {
  id: string;
  ticketDocumentNumber: string;
  agent: Agent;
  airline: AirlineSummaryAux;
  currency: Currency;
  dateOfIssue: string;
  reportingDate: string;
  sentToDpc: SendToDpcType;
  markStatus: MarkStatus;
  acdmStatus: AdmAcmStatus;
  totalAmount: number;
  concernsIndicator: TransactionRuleType;
  transactionCode: TransactionCode;
  period: string;
}

export interface AdmAcmDocumentSearchForm {
  ticketDocumentNumber?: string;
}
