import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import { AdmAcmRule } from '../models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesService } from '../services/adm-acm-categorization-rules.service';

@Injectable()
export class AdmAcmRuleResolver implements Resolve<string> {
  constructor(private router: Router, private admAcmCategorizationRulesService: AdmAcmCategorizationRulesService) {}

  resolve(): Observable<any> {
    const acdmRuleRouter: AdmAcmRule = this.router.getCurrentNavigation().extras.state?.data.rule;
    let acdmRule: AdmAcmRule;

    if (acdmRuleRouter) {
      this.admAcmCategorizationRulesService.acdmRule = acdmRuleRouter;
      acdmRule = acdmRuleRouter;
    } else {
      acdmRule = this.admAcmCategorizationRulesService.acdmRule;
    }

    return of(acdmRule);
  }
}
