import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';

import { AdmAcmStatus, getDeleteQueryUrl } from '../models/adm-acm-issue-shared-aux.model';
import { AdmAcmDialogService } from '../services/adm-acm-dialog.service';
import { createAdmAcmService } from '../shared/helpers/adm-acm-general.helper';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { getMenuTabs } from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { TabService } from '~app/shared/services';

@Injectable()
export class AdmAcmItemResolver implements Resolve<any> {
  constructor(
    private http: HttpClient,
    private acdmDialogService: AdmAcmDialogService,
    private tabService: TabService,
    private store: Store<AppState>,
    private router: Router
  ) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<AdmAcmIssueBE> {
    const admAcmService = createAdmAcmService(this.http, activatedRouteSnapshot);
    const adcmType = activatedRouteSnapshot.data.admAcmType;

    return admAcmService.getOne(activatedRouteSnapshot.params['id']).pipe(
      switchMap(item => combineLatest([of(item), this.store.pipe(select(getMenuTabs), first())])),
      tap(([item, tabs]) => {
        // We check here if the ACDM document is deleted
        if (item.acdmStatus === AdmAcmStatus.deleted) {
          const tabToCl = tabs.find(tab => tab.label === activatedRouteSnapshot.data.tab.label);
          if (tabToCl) {
            this.tabService.closeTab(tabToCl);
          }
          const url = getDeleteQueryUrl(adcmType);
          this.router.navigateByUrl(url);
          this.acdmDialogService.openDialogNotFoundDialogComponent();
        }
      }),
      map(([item, _]) => item)
    );
  }
}
