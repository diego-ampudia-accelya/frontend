import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';

import { AdmAcmDocumentEnquiryResolver } from './adm-acm-document-enquiry.resolver';

describe('AdmAcmDocumentEnquiryResolver', () => {
  let resolver: AdmAcmDocumentEnquiryResolver;
  const routeSpy: SpyObject<ActivatedRouteSnapshot> = createSpyObject(ActivatedRouteSnapshot);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdmAcmDocumentEnquiryResolver, { provide: ActivatedRouteSnapshot, useValue: routeSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    });
    resolver = TestBed.inject(AdmAcmDocumentEnquiryResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeDefined();
  });

  it('should return null if route dont has documentNumber ', () => {
    routeSpy.params = { documentNumber: null };

    const result = resolver.resolve(routeSpy);

    expect(result).toBeNull();
  });

  it('should return value if route has documentNumber ', () => {
    routeSpy.params = { documentNumber: '123' };

    const result = resolver.resolve(routeSpy);

    expect(result).toBe('123');
  });
});
