import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { MenuItem } from 'primeng/api';
import { combineLatest, Observable, of } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { AdmAcmBspSelectionDialogComponent } from '../dialogs/adm-acm-bsp-selection-dialog/adm-acm-bsp-selection-dialog.component';
import { AcdmActionType, getUrlAcdmIndexed, urlCreate } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  getIssueAcdmPendingSupervisionPermission,
  getIssueAcdmPermission
} from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { getMenuTabs } from '~app/core/reducers';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-adm-acm-list',
  templateUrl: './adm-acm-list.component.html',
  styleUrls: ['./adm-acm-list.component.scss']
})
export class AdmAcmListComponent implements OnInit {
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  public loggedUser: User;

  public tabs: RoutedMenuItem[];

  public admAcmType: MasterDataType;

  public title: string;
  public issueBtnLbl: string;
  public issueSupervisionBtnLbl: string;

  public btnDesign = ButtonDesign;

  public showIssueBtn: boolean;
  public showIssuePendingBtn: boolean;
  public issueOptionActions: { options: MenuItem[]; mainOption: MenuItem };

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private menuBuilder: MenuBuilder,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private bspsDictionaryService: BspsDictionaryService
  ) {
    this.setConfig();
  }

  private setConfig(): void {
    const data = this.activatedRoute.snapshot.data;

    this.admAcmType = data.admAcmType;
    this.issueBtnLbl = `ADM_ACM.query.${this.admAcmType}_issueButton`;
    this.issueSupervisionBtnLbl = `ADM_ACM.query.acdm_supervisionIssueButton`;
    this.title = `ADM_ACM.query.${this.admAcmType}_title`;
  }

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.initializeTabs();
    this.initializeBtnOptions();
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => (this.loggedUser = loggedUser));
  }

  private initializeTabs(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }

  public navigateToIssue(): void {
    const url: string = urlCreate[this.admAcmType];
    this.handleIssueNavigation(AcdmActionType.issue, url);
  }

  public navigateToIssueUnderSupervision(): void {
    const url: string = getUrlAcdmIndexed(AcdmActionType.issuePendingSupervision, this.admAcmType);
    this.handleIssueNavigation(AcdmActionType.issuePendingSupervision, url);
  }

  private handleIssueNavigation(action: AcdmActionType, url: string): void {
    // Differentiation between issue and issue under supervision permissions
    const permissions: string[] =
      action === AcdmActionType.issue
        ? [getIssueAcdmPermission(this.admAcmType)]
        : [getIssueAcdmPendingSupervisionPermission(this.admAcmType)];

    combineLatest([
      this.store.pipe(select(getMenuTabs)),
      this.store.pipe(
        select(getUser),
        switchMap(user => this.bspsDictionaryService.getAllBspsByPermissions(user.bspPermissions, permissions))
      )
    ])
      .pipe(
        switchMap(([tabs, bsps]) => {
          const isIssueTabOpen = tabs.some(tab => tab.url === url);
          const dialogConfig: DialogConfig = {
            data: {
              title: this.translationService.translate(`ADM_ACM.bspSelection.dialog.title.${action}`, {
                acdmType: this.translationService.translate(`ADM_ACM.type.${this.admAcmType}`)
              }),
              footerButtonsType: [{ type: FooterButton.Request, isDisabled: true }],
              bsps
            }
          };

          // If issue tab is not opened and there are multiple BSPs, BSP selection dialog is opened
          // If not, `null` is returned in order to navigate to issue tab as usually
          return !isIssueTabOpen && bsps.length > 1
            ? this.dialogService.open(AdmAcmBspSelectionDialogComponent, dialogConfig)
            : of(null);
        }),
        first()
      )
      .subscribe(result => {
        if (result) {
          const { clickedBtn, contentComponentRef } = result;
          const bsp: BspDto = contentComponentRef.form.value.bsp;

          if (clickedBtn === FooterButton.Request) {
            // Navigating to issue tab with BSP id in query params
            this.router.navigate([url], { queryParams: { bspId: bsp.id } });
          }

          this.dialogService.close();
        } else {
          // Navigating to issue tab as usually
          this.router.navigate([url]);
        }
      });
  }

  private canUserIssueAcdm(): boolean {
    const issuePermission = getIssueAcdmPermission(this.admAcmType);

    return this.userHasPermission(issuePermission);
  }

  private canUserIssueAcdmUnderSupervision(): boolean {
    const issuePermission = getIssueAcdmPendingSupervisionPermission(this.admAcmType);

    return this.userHasPermission(issuePermission);
  }

  private userHasPermission(permission: string): boolean {
    return permission ? this.loggedUser?.permissions.includes(permission) : false;
  }

  private initializeBtnOptions(): void {
    this.showIssueBtn = this.canUserIssueAcdm();
    this.showIssuePendingBtn = this.canUserIssueAcdmUnderSupervision();

    if (this.showIssueBtn && this.showIssuePendingBtn) {
      this.showIssueBtn = false;
      this.showIssuePendingBtn = false;

      this.issueOptionActions = {
        mainOption: {
          label: this.translationService.translate(this.issueBtnLbl),
          command: () => this.navigateToIssue()
        },
        options: [
          {
            label: this.translationService.translate(this.issueSupervisionBtnLbl),
            command: () => this.navigateToIssueUnderSupervision()
          }
        ]
      };
    }
  }
}
