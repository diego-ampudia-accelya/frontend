import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { map } from 'rxjs/operators';

import { AdmAcmActiveListComponent } from '../adm-acm-active-list/adm-acm-active-list.component';
import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';
import { AdmAcmStatus } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { archivedKeys, archivedSelectors, State } from '~app/adm-acm/store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { AppState } from '~app/reducers';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus } from '~app/shared/models/mark-status.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService,
  TabService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { CategoriesDictionaryService } from '~app/shared/services/dictionary/categories-dictionary.service';

@Component({
  selector: 'bspl-adm-acm-archived-list',
  templateUrl: '../adm-acm-active-list/adm-acm-active-list.component.html',
  styleUrls: ['../adm-acm-active-list/adm-acm-active-list.component.scss'],
  providers: [
    AdmAcmFilterFormatter,
    AdmAcmService,
    AdmAcmDataService,
    {
      provide: AdmAcmConfigService,
      useFactory: createAdmAcmConfigService,
      deps: [AdmAcmDataService, AdmAcmDialogService, L10nTranslationService, HttpClient, Router, Store, TabService]
    }
  ]
})
export class AdmAcmArchivedListComponent extends AdmAcmActiveListComponent {
  public isArchivedSubTab = true;

  constructor(
    protected store: Store<AppState>,
    protected admAcmService: AdmAcmService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected notificationService: NotificationService,
    public displayFormatter: AdmAcmFilterFormatter,
    protected formConfig: AdmAcmConfigService,
    protected dialogService: DialogService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected fb: FormBuilder,
    protected agentDictionaryService: AgentDictionaryService,
    protected airlineDictionaryService: AirlineDictionaryService,
    protected currencyDictionaryService: CurrencyDictionaryService,
    protected categoriesDictionaryService: CategoriesDictionaryService,
    protected gdsDictionaryService: GdsDictionaryService,
    protected periodService: PeriodService,
    protected permissionsService: PermissionsService,
    protected globalErrorHandlerService: ErrorHandler,
    protected admAcmDialogService: AdmAcmDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(
      store,
      admAcmService,
      actions$,
      translationService,
      notificationService,
      displayFormatter,
      formConfig,
      dialogService,
      activatedRoute,
      router,
      fb,
      agentDictionaryService,
      airlineDictionaryService,
      currencyDictionaryService,
      categoriesDictionaryService,
      gdsDictionaryService,
      periodService,
      permissionsService,
      globalErrorHandlerService,
      admAcmDialogService,
      bspsDictionaryService
    );
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>,
    DefaultProjectorFn<ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>>
  > {
    return archivedSelectors[this.admAcmType];
  }

  protected getListKey(): string {
    return archivedKeys[this.admAcmType];
  }

  protected adaptSearchQuery(query: DataQuery<AdmAcmFilter>): DataQuery<AdmAcmFilter> {
    return {
      ...query,
      filterBy: {
        ...query.filterBy,
        // With "!" at the first status we are negating the following status for the API call
        acdmStatus: query.filterBy.acdmStatus || [`!${AdmAcmStatus.deleted}`, AdmAcmStatus.supervisionDeleted],
        markStatus: [`${MarkStatus.Archive}`]
      }
    };
  }

  public onGetActionList(acdm: AdmAcmIssueBE & { markStatus: string }): void {
    this.listActions$ = this.admAcmService.getAcdmActionList(acdm.id).pipe(
      map(actions => {
        let modifiedActions: any = actions;

        const hasLoadingAction = modifiedActions.some(action => action.action === GridTableActionType.Loading);

        if (!hasLoadingAction) {
          const watchingAction = acdm.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;
          const isIataUserType = this.userTypes?.IATA === this.userType;

          modifiedActions = [...modifiedActions, { action: GridTableActionType.MoveToActive, group: 'markStatus' }];

          if (!isIataUserType) {
            modifiedActions.push({ action: watchingAction, group: 'markStatus' });
          }
        }

        return this.groupActions(modifiedActions);
      })
    );
  }

  public onActionClick(event): void {
    const actionType: GridTableActionType = event.action.actionType;
    const actionCommand = this.formConfig.getActionCommand(
      actionType,
      event.row,
      this.dialogQueryFormSubmitEmitter,
      this.getListKey()
    );

    actionCommand();
  }
}
