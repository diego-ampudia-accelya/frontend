import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { AdmAcmArchivedListComponent } from './adm-acm-archived-list.component';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GlobalErrorHandlerService } from '~app/core/services';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService,
  TabService
} from '~app/shared/services';
import { CategoriesDictionaryService } from '~app/shared/services/dictionary/categories-dictionary.service';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('AdmAcmArchivedListComponent', () => {
  let component: AdmAcmArchivedListComponent;
  let fixture: ComponentFixture<AdmAcmArchivedListComponent>;

  const admAcmServiceSpy = createSpyObject(AdmAcmService);
  const admAcmConfigServiceSpy = createSpyObject(AdmAcmConfigService);

  const activatedRouteStub = {
    snapshot: {
      data: { admAcmType: MasterDataType.Acm }
    }
  };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: ['rAcm', 'rAcmIntCom'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          'MENU.ACDMS.QUERY.acm': {},
          activeTabId: 'MENU.ACDMS.QUERY.acm'
        }
      },
      viewListsInfo: {}
    },
    acdm: { 'acm-archived-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmArchivedListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({ initialState }),
        FormBuilder,
        TabService,
        PermissionsService,
        GlobalErrorHandlerService,
        AdmAcmDialogService,
        mockProvider(NotificationService),
        mockProvider(DialogService),
        mockProvider(ActivatedRoute),
        mockProvider(AgentDictionaryService),
        mockProvider(AirlineDictionaryService),
        mockProvider(CurrencyDictionaryService),
        mockProvider(CategoriesDictionaryService),
        mockProvider(GdsDictionaryService),
        mockProvider(PeriodService),
        mockProvider(ActivatedRoute, activatedRouteStub)
      ]
    })
      .overrideComponent(AdmAcmArchivedListComponent, {
        set: {
          providers: [
            AdmAcmFilterFormatter,
            AdmAcmDataService,
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: AdmAcmService, useValue: admAcmServiceSpy },
            { provide: AdmAcmConfigService, useValue: admAcmConfigServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    admAcmServiceSpy.getBspsList.and.returnValue(of({ records: [] }));
    admAcmServiceSpy.getAcdmMenuOptions.and.returnValue(of([]));

    // Initialize component
    fixture = TestBed.createComponent(AdmAcmArchivedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: include this test with maybe a ListSubtabs mock (`RangeError: Maximum call stack size exceeded`)
  xit('should create', () => {
    expect(component).toBeDefined();
  });
});
