import { ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of, throwError } from 'rxjs';

import { AdmAcmActiveListComponent } from './adm-acm-active-list.component';
import { AcdmOptionType, AdmAcmStatus, urlView } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService,
  TabService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { CategoriesDictionaryService } from '~app/shared/services/dictionary/categories-dictionary.service';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('AdmAcmActiveListComponent', () => {
  let component: AdmAcmActiveListComponent;
  let fixture: ComponentFixture<AdmAcmActiveListComponent>;
  let router: Router;

  const admAcmServiceSpy = createSpyObject(AdmAcmService);
  const admAcmConfigServiceSpy = createSpyObject(AdmAcmConfigService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const currencyDictionaryServiceSpy = createSpyObject(CurrencyDictionaryService);
  const categoriesDictionaryServiceSpy = createSpyObject(CategoriesDictionaryService);
  const gdsDictionaryServiceSpy = createSpyObject(GdsDictionaryService);
  const bspsDictionaryService = createSpyObject(BspsDictionaryService);
  const periodServiceSpy = createSpyObject(PeriodService);
  const admAcmDialogServiceSpy = createSpyObject(AdmAcmDialogService);

  const activatedRouteStub = {
    snapshot: {
      data: { admAcmType: MasterDataType.Adm }
    }
  };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: ['rAdm', 'rAdmIntCom', 'rLean'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          'MENU.ACDMS.QUERY.adm': {},
          activeTabId: 'MENU.ACDMS.QUERY.adm'
        }
      },
      viewListsInfo: {}
    },
    acdm: { 'adm-active-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmActiveListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromAuth.getUser, value: expectedUserDetails },
            { selector: fromAuth.getUserBsps, value: expectedUserDetails.bsps }
          ]
        }),
        FormBuilder,
        TabService,
        PermissionsService,
        ErrorHandler,
        { provide: AdmAcmDialogService, useValue: admAcmDialogServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionaryServiceSpy },
        { provide: CategoriesDictionaryService, useValue: categoriesDictionaryServiceSpy },
        { provide: GdsDictionaryService, useValue: gdsDictionaryServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryService },
        { provide: PeriodService, useValue: periodServiceSpy }
      ]
    })
      .overrideComponent(AdmAcmActiveListComponent, {
        set: {
          providers: [
            AdmAcmFilterFormatter,
            AdmAcmDataService,
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: AdmAcmService, useValue: admAcmServiceSpy },
            { provide: AdmAcmConfigService, useValue: admAcmConfigServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    admAcmServiceSpy.getBspsList.and.returnValue(of({ records: [] }));
    admAcmServiceSpy.getAcdmMenuOptions.and.returnValue(of([]));
    bspsDictionaryService.getAllBspsByPermissions.and.returnValue(of([]));

    // Initialize component
    fixture = TestBed.createComponent(AdmAcmActiveListComponent);
    component = fixture.componentInstance;
    router = TestBed.inject<any>(Router);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('initializeColumns', () => {
    it('No LEAN USER - should initialize columns without checkbox as AirlineUser', () => {
      component.isADM = false;
      component.hasViewPermission = false;
      component.isBspFilterMultiple = false;
      const columnsExpected: any[] = [
        'agent.bsp.isoCountryCode',
        'ticketDocumentNumber',
        'agent.iataCode',
        'acdmStatus',
        'currency.code',
        'totalAmount',
        'dateOfIssue',
        'period',
        'reportingDate'
      ];
      component['initializeColumns']();

      expect(component.columns.map(col => col.prop)).toEqual(columnsExpected);
    });

    it('LEAN USER - should initialize columns with checkbox as AirlineUser', () => {
      component.isADM = false;
      component.hasViewPermission = true;
      component.isBspFilterMultiple = true;

      const columnsExpected: any[] = [
        'isRowSelected',
        'agent.bsp.isoCountryCode',
        'ticketDocumentNumber',
        'agent.iataCode',
        'acdmStatus',
        'currency.code',
        'totalAmount',
        'dateOfIssue',
        'period',
        'reportingDate'
      ];
      component['initializeColumns']();

      expect(component.columns.map(col => col.prop)).toEqual(columnsExpected);
    });

    it('should initialize columns with dispute date column for ADMA as AirlineUser ', () => {
      component.isADM = true;
      component.hasViewPermission = false;
      component.isBspFilterMultiple = false;
      const columnsExpected: any[] = [
        'agent.bsp.isoCountryCode',
        'ticketDocumentNumber',
        'agent.iataCode',
        'acdmStatus',
        'currency.code',
        'totalAmount',
        'dateOfIssue',
        'disputeDate',
        'period',
        'reportingDate'
      ];
      component['initializeColumns']();

      expect(component.columns.map(col => col.prop)).toEqual(columnsExpected);
    });

    it('should initialize columns  as Agent ', () => {
      component.isADM = true;
      component.hasViewPermission = false;
      component.userType = UserType.AGENT;
      component.isBspFilterMultiple = false;
      const columnsExpected: any[] = [
        'agent.bsp.isoCountryCode',
        'ticketDocumentNumber',
        'airline.iataCode',
        'acdmStatus',
        'currency.code',
        'totalAmount',
        'dateOfIssue',
        'disputeDate',
        'period',
        'reportingDate'
      ];
      component['initializeColumns']();

      expect(component.columns.map(col => col.prop)).toEqual(columnsExpected);
    });
  });

  describe('updateCurrencyList', () => {
    it('should not updateCurrencyList if user is not multi country', () => {
      const updateCurrencyFilterListSpy = spyOn<any>(component, 'updateCurrencyFilterList').and.callThrough();
      const updateCurrencyListControlValueSpy = spyOn<any>(
        component,
        'updateCurrencyListControlValue'
      ).and.callThrough();
      component['isMultiCountryUser'] = false;
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));

      component['updateCurrencyList']();

      expect(updateCurrencyFilterListSpy).toHaveBeenCalled();
      expect(updateCurrencyListControlValueSpy).not.toHaveBeenCalled();
    });

    it('should updateCurrencyList if user is multi country', () => {
      const updateCurrencyFilterListSpy = spyOn<any>(component, 'updateCurrencyFilterList').and.callThrough();
      const updateCurrencyListControlValueSpy = spyOn<any>(
        component,
        'updateCurrencyListControlValue'
      ).and.callThrough();
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));
      component['isMultiCountryUser'] = true;

      component['updateCurrencyList']();

      expect(updateCurrencyFilterListSpy).toHaveBeenCalled();
      expect(updateCurrencyListControlValueSpy).toHaveBeenCalled();
    });

    it('should updateCurrencyFilterList', () => {
      component['bspSelectedIds'] = [2];
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));
      component['updateCurrencyFilterList']();

      expect(currencyDictionaryServiceSpy.getFilteredDropdownOptions).toHaveBeenCalledWith({ bspId: [2] });
    });

    it('should updateCurrencyListControlValue', () => {
      const currencyMock = [
        {
          value: { id: 1, code: 'EUR', decimals: 2 },
          label: 'EUR'
        },
        {
          value: { id: 2, code: 'USD', decimals: 2 },
          label: 'USD'
        }
      ];
      const selectedCurrency = [{ id: 1, code: 'EUR', decimals: 2 }];
      component['currencyControl'].patchValue(selectedCurrency);
      component.currencyList = currencyMock;
      component['bspSelectedIds'] = [1];

      component['updateCurrencyListControlValue']();

      expect(component['currencyControl'].value).toEqual(selectedCurrency);
    });
  });

  it('should updatePeriodControlDisability', () => {
    component.isBspFilterMultiple = true;
    component['bspSelectedIds'] = [];
    component.periodIsDisabled = true;
    component['periodControl'].setValue({ period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' });
    component['updatePeriodControlDisability']();

    expect(component['periodControl'].value).toEqual(null);
  });

  describe('setPeriodPickerYearsBack', () => {
    it('should setPeriodPickerYearsBack when user is LEAN', () => {
      component['hasLeanPermission'] = true;
      component['setPeriodPickerYearsBack']();

      expect(component['periodPickerYearsBack']).toEqual(component['LEAN_USERS_YEARS']);
    });

    it('should setPeriodPickerYearsBack when user is not LEAN', () => {
      component['hasLeanPermission'] = false;
      component['setPeriodPickerYearsBack']();

      expect(component['periodPickerYearsBack']).toEqual(component['NON_LEAN_USERS_YEARS']);
    });
  });

  describe('updateGdsForwardList', () => {
    it('should not updateGdsForwardList if user is not multi country', () => {
      const updateGdsFilterListSpy = spyOn<any>(component, 'updateGdsForwardList').and.callThrough();
      const updateGdsListControlValueSpy = spyOn<any>(component, 'updateGdsListControlValue').and.callThrough();
      component['isMultiCountryUser'] = false;
      gdsDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([]));

      component['updateGdsForwardList']();

      expect(updateGdsFilterListSpy).toHaveBeenCalled();
      expect(updateGdsListControlValueSpy).not.toHaveBeenCalled();
    });

    it('should updateGdsForwardList if user is multi country', () => {
      const updateGdsFilterListSpy = spyOn<any>(component, 'updateGdsForwardList').and.callThrough();
      const updateGdsListControlValueSpy = spyOn<any>(component, 'updateGdsListControlValue').and.callThrough();
      gdsDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([]));
      component['isMultiCountryUser'] = true;

      component['updateGdsForwardList']();

      expect(updateGdsFilterListSpy).toHaveBeenCalled();
      expect(updateGdsListControlValueSpy).toHaveBeenCalled();
    });

    it('should updateGdsFilterList', () => {
      component['bspSelectedIds'] = [2];
      gdsDictionaryServiceSpy.getDropdownOptionsForFiltering.and.returnValue(of([]));
      component['isGdsForwardVisible'] = true;
      component['updateGdsFilterList']();

      expect(gdsDictionaryServiceSpy.getDropdownOptionsForFiltering).toHaveBeenCalledWith([2]);
    });
  });

  it('should initialize AirlineFilterDropdown', fakeAsync(() => {
    const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
      {
        value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
        label: '001 / AIRLINE 001'
      },
      {
        value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
        label: '002 / AIRLINE 002'
      }
    ];
    airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));
    component['initializeAirlineFilterDropdown']();
    tick();

    let result;
    component.airlineCodesList$.subscribe(data => (result = data));
    expect(result).toEqual(mockAirlineDropdownOptions);
  }));

  it('should  initialize AgentFilterList', () => {
    const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
      {
        value: {
          id: '1',
          name: 'AGENT 1111111',
          code: '1111111',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        },
        label: '1111111 / AGENT 1111111'
      }
    ];
    agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));

    component['bspSelectedIds'] = [2];
    component['updateAgentFilterList']();
    let result;
    fixture.detectChanges();
    expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith({ bspId: [2] });

    component.agentCodesList$.subscribe(data => (result = data));
    expect(result).toEqual(mockAgentDropdownOptions);
  });

  describe('updateAgentList', () => {
    it('should update AgentListControlValue', fakeAsync(() => {
      const agentSelected = [
        {
          id: '2',
          name: 'AGENT 2222222',
          code: '2222222',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        }
      ];
      component['bspSelectedIds'] = [1];
      component['agentListControl'].patchValue(agentSelected);

      component['updateAgentListControlValue']();

      expect(component['agentListControl'].value).toEqual(agentSelected);
    }));

    it('should updateAgentList if user is multi country ', () => {
      const updateAgentFilterListSpy = spyOn<any>(component, 'updateAgentFilterList').and.callThrough();
      const updateAgentListControlValueSpy = spyOn<any>(component, 'updateAgentListControlValue').and.callThrough();
      component['isMultiCountryUser'] = true;
      component['updateAgentList']();

      expect(updateAgentFilterListSpy).toHaveBeenCalled();
      expect(updateAgentListControlValueSpy).toHaveBeenCalled();
    });

    it('should not updateAgentList if user is not multi country ', () => {
      const updateAgentFilterListSpy = spyOn<any>(component, 'updateAgentFilterList').and.callThrough();
      const updateAgentListControlValueSpy = spyOn<any>(component, 'updateAgentListControlValue').and.callThrough();

      component['updateAgentList']();

      expect(updateAgentFilterListSpy).toHaveBeenCalled();
      expect(updateAgentListControlValueSpy).not.toHaveBeenCalled();
    });
  });

  it('should update initializeAgentGroupFilterDropdown', fakeAsync(() => {
    const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
      {
        value: {
          id: '1',
          name: 'AGENT 1111111',
          code: '1111111',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        },
        label: '1111111 / AGENT 1111111'
      },
      {
        value: {
          id: '2',
          name: 'AGENT 2222222',
          code: '2222222',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        },
        label: '2222222 / AGENT 2222222'
      }
    ];
    component.agentCodesList$ = of([]);
    component['hasViewPermission'] = true;
    agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));

    component['initializeAgentGroupFilterDropdown']();
    tick();

    let result;

    component.agentCodesList$.subscribe(data => (result = data));

    expect(result).toEqual(mockAgentDropdownOptions);
  }));

  it('should update initializeAgentGroupFilterDropdown when hasViewPermission is false', fakeAsync(() => {
    component.agentCodesList$ = of([]);
    component['hasViewPermission'] = false;
    agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([]));

    component['initializeAgentGroupFilterDropdown']();
    tick();
    component.agentCodesList$.subscribe(res => expect(res).toEqual(null));
  }));

  it('should initializeStatusFilterDropdown', fakeAsync(() => {
    spyOn<any>(component, 'filterStatusByTypePermission').and.returnValue(true);
    component['initializeStatusFilterDropdown']();

    const expectedStatus = jasmine.arrayContaining<any>([
      { value: 'PENDING', label: 'ADM_ACM.status.PENDING' },
      { value: 'DISPUTED', label: 'ADM_ACM.status.DISPUTED' }
    ]);
    expect(component.statusList).toEqual(expectedStatus);
  }));

  it('should updatePeriodFilterOptions', fakeAsync(() => {
    const method = periodServiceSpy.getByBsp.and.callThrough();
    component['bspSelectedIds'] = [2];
    component['updatePeriodFilterOptions']();
    tick();

    expect(method).toHaveBeenCalledWith(2);
  }));

  it('should initializePrimaryReasonFilterDropdown', fakeAsync(() => {
    const method = categoriesDictionaryServiceSpy.getDropdownOptions.and.callThrough();
    component.isReasonsVisible = true;
    component['initializePrimaryReasonFilterDropdown']();
    tick();

    expect(method).toHaveBeenCalledWith();
  }));

  it('should filterStatusByTypePermission', () => {
    const result = component['filterStatusByTypePermission'](AdmAcmStatus.pendingAuthorization);

    expect(result).toBeFalsy();
  });

  describe('download', () => {
    it('should open download dialog correctly on download', () => {
      component.onDownload();

      expect(dialogServiceSpy.open).toHaveBeenCalled();
    });

    it('should setButtonMenuOptionGroup for read ', fakeAsync(() => {
      const d = {
        id: AcdmOptionType.Read,
        label: 'test'
      };
      expect(component['setButtonMenuOptionGroup'](d)).toEqual('markAs');
    }));

    it('should setButtonMenuOptionGroup for download', fakeAsync(() => {
      const d = {
        id: AcdmOptionType.DownloadAttachments,
        label: 'test'
      };
      expect(component['setButtonMenuOptionGroup'](d)).toEqual('download');
    }));

    it('should onDetailedDownload', fakeAsync(() => {
      const spy = spyOn<any>(component, 'download');
      component['onDetailedDownload']();

      expect(spy).toHaveBeenCalled();
    }));
  });

  describe('onFilterButtonClicked', () => {
    beforeEach(() => {});

    it('Should initialize for AGENT users', () => {
      const initializeAirlineFilterDropdownSpy = spyOn<any>(
        component,
        'initializeAirlineFilterDropdown'
      ).and.callThrough();

      spyOn<any>(component, 'updateCurrencyList');
      spyOn<any>(component, 'initializeStatusFilterDropdown');
      spyOn<any>(component, 'updateGdsFilterList');
      spyOn<any>(component, 'initializePrimaryReasonFilterDropdown');
      spyOn<any>(component, 'initializeBspCountrieslistener');

      component.userType = UserType.AGENT;
      component.onFilterButtonClicked(true);

      expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    });

    it('Should initialize for AIRLINE users', () => {
      const updateAgentListSpy = spyOn<any>(component, 'updateAgentList').and.callThrough();

      spyOn<any>(component, 'updateCurrencyList');
      spyOn<any>(component, 'initializeStatusFilterDropdown');
      spyOn<any>(component, 'updateGdsFilterList');
      spyOn<any>(component, 'initializePrimaryReasonFilterDropdown');
      spyOn<any>(component, 'initializeBspCountrieslistener');

      component.userType = UserType.AIRLINE;
      component.onFilterButtonClicked(true);

      expect(updateAgentListSpy).toHaveBeenCalled();
    });

    it('Should initialize for AGENT_GROUP users', () => {
      const initializeAgentGroupFilterDropdownSpy = spyOn<any>(
        component,
        'initializeAgentGroupFilterDropdown'
      ).and.callThrough();
      const initializeAirlineFilterDropdownSpy = spyOn<any>(
        component,
        'initializeAirlineFilterDropdown'
      ).and.callThrough();

      spyOn<any>(component, 'updateCurrencyList');
      spyOn<any>(component, 'initializeStatusFilterDropdown');
      spyOn<any>(component, 'updateGdsFilterList');
      spyOn<any>(component, 'initializePrimaryReasonFilterDropdown');
      spyOn<any>(component, 'initializeBspCountrieslistener');

      component.userType = UserType.AGENT_GROUP;
      component.onFilterButtonClicked(true);

      expect(initializeAgentGroupFilterDropdownSpy).toHaveBeenCalled();
      expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    });

    it('Should initialize for IATA users', () => {
      const updateAgentListSpy = spyOn<any>(component, 'updateAgentList').and.callThrough();
      const initializeAirlineFilterDropdownSpy = spyOn<any>(
        component,
        'initializeAirlineFilterDropdown'
      ).and.callThrough();

      spyOn<any>(component, 'updateCurrencyList');
      spyOn<any>(component, 'initializeStatusFilterDropdown');
      spyOn<any>(component, 'updateGdsFilterList');
      spyOn<any>(component, 'initializePrimaryReasonFilterDropdown');
      spyOn<any>(component, 'initializeBspCountrieslistener');

      component.userType = UserType.IATA;
      component.onFilterButtonClicked(true);

      expect(updateAgentListSpy).toHaveBeenCalled();
      expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    });
  });

  it('should initializeBspCountrieslistener', fakeAsync(() => {
    const updatePeriodFilterOptionsSpy = spyOn<any>(component, 'updatePeriodFilterOptions').and.callThrough();
    spyOn<any>(component, 'updateBspSelectedIds');
    spyOn<any>(component, 'updateAgentList');
    spyOn<any>(component, 'updateCurrencyList');
    spyOn<any>(component, 'updatePeriodControlDisability');
    const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

    component['initializeBspCountrieslistener']();
    component.searchform.get('bsp').patchValue(mockBsp);

    tick();
    expect(updatePeriodFilterOptionsSpy).toHaveBeenCalled();
  }));

  it('should navigate when on view is called.', fakeAsync(() => {
    spyOn(router, 'navigate');

    const url = urlView[component.admAcmType];
    const documentNumber = '123';
    component.onViewAdm(documentNumber);
    tick();

    expect(router.navigate).toHaveBeenCalledWith([url, documentNumber]);
  }));

  describe('updateBspSelectedIds', () => {
    it('should updateBspSelectedIds sets selected ids from bsps object', () => {
      const mockBsps = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

      component['updateBspSelectedIds'](mockBsps);

      expect(component['bspSelectedIds']).toEqual([2]);
    });

    it('should updateBspSelectedIds sets selected ids from bsps array', () => {
      const mockBsps = [{ id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }];

      component['updateBspSelectedIds'](mockBsps);

      expect(component['bspSelectedIds']).toEqual([2]);
    });

    it('should updateBspSelectedIds empty value sets when no ids selected  from bsps object', () => {
      component['updateBspSelectedIds'](null);

      expect(component['bspSelectedIds']).toEqual([]);
    });
  });

  describe('getSubReasonDropDown', () => {
    it('should getSubReasonDropDown when have some value', fakeAsync(() => {
      const mockReasonDropdownOptions = jasmine.arrayContaining<any>([
        {
          id: 48,
          subReason: "Doesn't recognize transaction"
        }
      ]);

      categoriesDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockReasonDropdownOptions));
      component['getSubReasonDropDown']({
        id: 21,
        primaryReason: 'Credit Card Chargebacks'
      });
      tick();

      let result;
      component.reasonList$.subscribe(data => (result = data));
      expect(result).toEqual(mockReasonDropdownOptions);
    }));

    it('should getSubReasonDropDown for empty value', fakeAsync(() => {
      categoriesDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([]));
      component['getSubReasonDropDown'](null);
      tick();
      component.reasonList$ = of([]);
      component['reasonControl'].patchValue('');
      let result;
      component.reasonList$.subscribe(data => (result = data));
      expect(result).toEqual([]);
    }));
  });

  it('should open refund internal comment dialog on image click', () => {
    const event = { event: null, row: { id: 1 } as AdmAcmIssueBE, column: null };

    component.onImageClick(event);

    expect(admAcmDialogServiceSpy.openInternalCommentDialog).toHaveBeenCalledWith({ id: 1 }, MasterDataType.Adm);
  });

  it('should onActionClick', () => {
    admAcmConfigServiceSpy.getActionCommand.and.returnValue(admAcmDialogServiceSpy.openActivityHistoryDialog);
    component.onActionClick({
      action: { actionType: GridTableActionType.Activity },
      row: { id: 1, transactionCode: 'TT' } as AdmAcmIssueBE
    });

    expect(admAcmConfigServiceSpy.getActionCommand).toHaveBeenCalled();
    expect(admAcmDialogServiceSpy.openActivityHistoryDialog).toHaveBeenCalled();
  });

  it('should onGetActionList', () => {
    const acdm = { id: 1, markStatus: 'test' } as AdmAcmIssueBE & { markStatus: string };
    admAcmServiceSpy.getAcdmActionList.and.returnValue(of([]));

    component.onGetActionList(acdm);

    expect(admAcmServiceSpy.getAcdmActionList).toHaveBeenCalledWith(1);
  });

  describe('setButtonMenuOptionCommand', () => {
    it('should setButtonMenuOptionCommand for read with isArchivedSubTab', fakeAsync(() => {
      component.isArchivedSubTab = true;
      const option = {
        id: AcdmOptionType.Read,
        label: 'test'
      };

      expect(component['setButtonMenuOptionCommand'](option)).toEqual(jasmine.any(Function));
    }));

    it('should setButtonMenuOptionCommand for read without isArchivedSubTab', fakeAsync(() => {
      component.isArchivedSubTab = false;
      const option = {
        id: AcdmOptionType.Read,
        label: 'test'
      };
      expect(component['setButtonMenuOptionCommand'](option)).toEqual(jasmine.any(Function));
    }));

    it('should setButtonMenuOptionCommand for unread', fakeAsync(() => {
      const option = {
        id: AcdmOptionType.Unread,
        label: 'test'
      };
      expect(component['setButtonMenuOptionCommand'](option)).toEqual(jasmine.any(Function));
    }));
  });

  it('`bulkDownloadAttachments` - should show a notification error when 404 occurs', () => {
    fixture.detectChanges();
    component.selectedRows$ = throwError({ status: 404 });
    component['bulkDownloadAttachments']();

    expect(notificationServiceSpy.showError).toHaveBeenCalled();
  });

  it('`bulkDownloadAttachments` - should handle error with a global behaviour when other than 404 occurs', () => {
    spyOn(component['globalErrorHandlerService'], 'handleError').and.callFake(() => {});

    fixture.detectChanges();
    component.selectedRows$ = throwError({ status: 401 });
    component['bulkDownloadAttachments']();

    expect(component['globalErrorHandlerService'].handleError).toHaveBeenCalled();
  });

  it('should initialize list actions on get action list', fakeAsync(() => {
    let actionList: { action: GridTableActionType; disabled?: boolean; group?: string }[];
    const mockActions = [{ action: GridTableActionType.InternalComment, group: 'comment' }];

    admAcmServiceSpy.getAcdmActionList.and.returnValue(of(mockActions));
    spyOn<any>(component, 'groupActions').and.callThrough();

    const modifiedActions = [
      ...mockActions,
      { action: GridTableActionType.MarkAsRead, group: 'markStatus' },
      { action: GridTableActionType.Archive, group: 'markStatus' },
      { action: GridTableActionType.StartWatching, group: 'markStatus' }
    ];

    component.onGetActionList({ id: 1 } as AdmAcmIssueBE & { markStatus: string });
    component.listActions$.subscribe(actions => (actionList = actions));

    tick();

    expect(admAcmServiceSpy.getAcdmActionList).toHaveBeenCalledWith(1);
    expect(component['groupActions']).toHaveBeenCalledWith(modifiedActions);
    expect(actionList).toEqual(modifiedActions);
  }));

  it('should getTranslatedButtonMenuOptionLabel if isArchivedSubTab ', fakeAsync(() => {
    component.isArchivedSubTab = true;
    const service = TestBed.inject(L10nTranslationService);
    const spyOnMethod = spyOn(service, 'translate').and.callThrough();

    component['getTranslatedButtonMenuOptionLabel']({ label: 'READ' });

    expect(spyOnMethod).toHaveBeenCalledWith('ADM_ACM.query.tableSelectionToolbar.buttonMenuOptionsArchive.READ');
  }));

  it('should getTranslatedButtonMenuOptionLabel if other than isArchivedSubTab', fakeAsync(() => {
    component.isArchivedSubTab = false;
    const service = TestBed.inject(L10nTranslationService);
    const spyOnMethod = spyOn(service, 'translate').and.callThrough();

    component['getTranslatedButtonMenuOptionLabel']({ label: 'READ' });

    expect(spyOnMethod).toHaveBeenCalledWith('ADM_ACM.query.tableSelectionToolbar.buttonMenuOptions.READ');
  }));

  describe('update gds', () => {
    const mockGdsList = [
      {
        value: {
          id: 1,
          gdsCode: '1',
          name: 'gds1',
          version: 2
        },
        label: 'gds1'
      },
      {
        value: {
          id: 2,
          gdsCode: '2',
          name: 'gds2',
          version: 2
        },
        label: 'gds2'
      }
    ];
    it('should update updateGdsListControlValue', fakeAsync(() => {
      const gdsSelected = [
        {
          id: 1,
          gdsCode: '1',
          name: 'gds1',
          version: 2
        }
      ];
      component['bspSelectedIds'] = [1];
      component.gdsForwardList = mockGdsList;
      component['gdsControl'].patchValue(gdsSelected);

      component['updateGdsListControlValue']();

      expect(component['gdsControl'].value).toEqual(gdsSelected);
    }));
    it('should update getGdsName if gdsCode is exists in gdsForwardList', () => {
      component.gdsForwardList = mockGdsList;

      const gds = {
        gdsCode: '2',
        gdsForwardDate: ''
      };

      expect(component.getGdsName(gds)).toEqual('gds2');
    });

    it('should update getGdsName if gdsCode is not exists in gdsForwardList ', () => {
      component.gdsForwardList = mockGdsList;
      const gds = {
        gdsCode: '3',
        gdsForwardDate: ''
      };

      expect(component.getGdsName(gds)).toEqual('3');
    });
  });

  it('should loggedUserIsOfType', () => {
    component.userType = UserType.AGENT;

    expect(component.loggedUserIsOfType(UserType.AGENT)).toBe(true);
  });
});
