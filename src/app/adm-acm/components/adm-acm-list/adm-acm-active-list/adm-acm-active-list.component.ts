import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { mapValues } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { SEARCH_FORM_DEFAULT_VALUE, SHARED } from '~app/adm-acm/components/adm-acm-list/adm-acm-list.constants';
import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';
import {
  AcdmActionType,
  AcdmOptionType,
  AdmAcmStatus,
  SendToDpcType,
  urlView
} from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { anyOptionGdsForwardValue } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import {
  getAcdmReadInternalCommentPermission,
  getBspFilterRequiredPermissions,
  getSupervisionAcdmPermissions,
  getViewAcdmPermission,
  getViewAcdntPermission,
  viewAdmForwardGdsPermission
} from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { activeKeys, activeSelectors, State } from '~app/adm-acm/store/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ButtonDesign, DialogService, DownloadFileComponent, FooterButton, PeriodOption } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { ServerError } from '~app/shared/errors';
import { FormUtil, toValueLabelObject, toValueLabelObjectBsp } from '~app/shared/helpers';
import { GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { Currency } from '~app/shared/models/currency.model';
import { GdsSummary } from '~app/shared/models/dictionary/gds-summary.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus, MarkStatusFilter } from '~app/shared/models/mark-status.model';
import { User, UserType } from '~app/shared/models/user.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService,
  TabService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { CategoriesDictionaryService } from '~app/shared/services/dictionary/categories-dictionary.service';

@Component({
  selector: 'bspl-adm-acm-active-list',
  templateUrl: './adm-acm-active-list.component.html',
  styleUrls: ['./adm-acm-active-list.component.scss'],
  providers: [
    AdmAcmFilterFormatter,
    AdmAcmService,
    AdmAcmDataService,
    {
      provide: AdmAcmConfigService,
      useFactory: createAdmAcmConfigService,
      deps: [AdmAcmDataService, AdmAcmDialogService, L10nTranslationService, HttpClient, Router, Store, TabService]
    }
  ]
})
export class AdmAcmActiveListComponent extends ListSubtabs<AdmAcmIssueBE, AdmAcmFilter> implements OnInit, OnDestroy {
  protected initialSortAttribute: keyof AdmAcmFilter = 'dateOfIssue';

  public admAcmType: MasterDataType;
  public userTypes = UserType;
  public btnDesign = ButtonDesign;

  public tableTitle: string;

  public loggedUser: User;
  public userType: UserType;

  public searchform: FormGroup;
  public isArchivedSubTab = false;
  public isReasonsVisible = false;
  public isDisputeDateVisible = false;
  public isGdsForwardVisible = false;
  public isInternalCommentVisible = false;

  public buttonMenuOptions$: Observable<ButtonMenuOption[]>;
  public listActions$: Observable<{ action: GridTableActionType; disabled?: boolean; group?: string }[]>;
  public columns: Array<GridColumn> = [];
  public downloadQuery: Partial<DataQuery>;

  public airlineCodesList$: Observable<DropdownOption[]>;
  public agentCodesList$: Observable<DropdownOption[]>;
  public bspCountriesList: DropdownOption<BspDto>[];
  public currencyList: DropdownOption<Currency>[];
  public rtdnTypeList$: Observable<DropdownOption[]>;
  public periodOptions$: Observable<PeriodOption[]>;
  public primaryReasonList$: Observable<DropdownOption[]> = of([]);
  public reasonList$: Observable<DropdownOption[]> = of([]);
  public statusList: DropdownOption[] = [];
  public sentToDpcList: DropdownOption[] = [];
  public markStatusList: DropdownOption[] = [];
  public gdsForwardList: DropdownOption<GdsSummary | Partial<GdsSummary>>[];
  public hasInternalCommentList: DropdownOption<boolean>[] = [];
  public periodIsDisabled = true;

  public anyFilteringOption = {
    label: this.translationService.translate('ADM_ACM.query.filters.gdsForwardAnyOption'),
    value: {
      id: anyOptionGdsForwardValue,
      gdsCode: anyOptionGdsForwardValue,
      name: this.translationService.translate('ADM_ACM.query.filters.gdsForwardAnyOption')
    }
  };

  public hasViewPermission = false;
  private hasReadInternalCommentPermission = false;

  protected dialogQueryFormSubmitEmitter = new Subject<AcdmActionType>();

  private viewPermission: string;
  private gdsForwardPermission: string;

  private reasonControl: AbstractControl;
  private primaryReasonControl: AbstractControl;
  private bspControl: AbstractControl;
  private agentListControl: AbstractControl;
  private periodControl: AbstractControl;
  private currencyControl: AbstractControl;
  private gdsControl: AbstractControl;

  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;

  private hasLeanPermission: boolean;
  private isMultiCountryUser: boolean;
  private bspSelectedIds: number[] = [];

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  public periodPickerYearsBack: number;
  public isADM: boolean;

  constructor(
    protected store: Store<AppState>,
    protected admAcmService: AdmAcmService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected notificationService: NotificationService,
    public displayFormatter: AdmAcmFilterFormatter,
    protected formConfig: AdmAcmConfigService,
    protected dialogService: DialogService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected fb: FormBuilder,
    protected agentDictionaryService: AgentDictionaryService,
    protected airlineDictionaryService: AirlineDictionaryService,
    protected currencyDictionaryService: CurrencyDictionaryService,
    protected categoriesDictionaryService: CategoriesDictionaryService,
    protected gdsDictionaryService: GdsDictionaryService,
    protected periodService: PeriodService,
    protected permissionsService: PermissionsService,
    protected globalErrorHandlerService: ErrorHandler,
    protected admAcmDialogService: AdmAcmDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(store, admAcmService, actions$, translationService, notificationService);
    this.setConfig();
  }

  private setConfig(): void {
    const { admAcmType } = this.activatedRoute.snapshot.data;

    this.tableTitle = `ADM_ACM.query.${admAcmType}_listTitle`;

    this.admAcmType = admAcmType;
    this.isADM = this.acdmIsOfType(MasterDataType.Adm);
    this.formConfig.admAcmType = admAcmType;

    this.viewPermission = this.formConfig.isAcdnt
      ? getViewAcdntPermission(admAcmType)
      : getViewAcdmPermission(admAcmType);
    this.gdsForwardPermission = viewAdmForwardGdsPermission;
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>,
    DefaultProjectorFn<ListSubtabsState<AdmAcmIssueBE, AdmAcmFilter>>
  > {
    return activeSelectors[this.admAcmType];
  }

  protected getListKey(): string {
    return activeKeys[this.admAcmType];
  }

  protected adaptSearchQuery(query: DataQuery<AdmAcmFilter>): DataQuery<AdmAcmFilter> {
    return {
      ...query,
      filterBy: {
        ...query.filterBy,
        // With "!" at the first status we are negating the following status for the API call
        acdmStatus: query.filterBy.acdmStatus || [`!${AdmAcmStatus.deleted}`, AdmAcmStatus.supervisionDeleted],
        markStatus: query.filterBy.markStatus || [`${MarkStatus.Read}, ${MarkStatus.Unread}`]
      }
    };
  }

  public ngOnInit(): void {
    this.setInitialFilterValues();

    // Initial loading view
    this.loading$ = of(true);
    this.initializeColumns();

    combineLatest([this.initializeLoggedUser$(), this.permissionsService.permissions$.pipe(first())])
      .pipe(
        tap(() => this.initializePermissions()),
        switchMap(() => this.initializeLeanBspFilter$()),
        tap(() => super.ngOnInit()),
        tap(() => this.setResolversData()),
        tap(() => this.initializeColumns()),
        tap(() => this.updatePeriodControlDisability()),
        tap(() => this.updatePeriodFilterOptions()),
        tap(() => this.setPeriodPickerYearsBack()),
        takeUntil(this.destroy$)
      )
      .subscribe();

    this.initializeFilterVisibility();
    this.initializeFilterListeners();
    this.populateFilterDropdowns();
    this.populateButtonMenuOptions();
  }

  private initializePermissions(): void {
    this.hasViewPermission = this.permissionsService.hasPermission(this.viewPermission);

    // Internal comment permissions
    this.hasReadInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmReadInternalCommentPermission(this.admAcmType)
    );

    //Lean permissions
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private setResolversData(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.downloadQuery = this.adaptSearchQuery(query)));
    this.dialogQueryFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.onQueryChanged());
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      switch (this.userType) {
        case UserType.AGENT:
          this.initializeAirlineFilterDropdown();
          break;
        case UserType.AIRLINE:
          this.updateAgentList();
          break;
        case UserType.AGENT_GROUP:
          this.initializeAgentGroupFilterDropdown();
          this.initializeAirlineFilterDropdown();
          break;
        default:
          this.initializeAirlineFilterDropdown();
          this.updateAgentList();
      }

      this.updateCurrencyList();
      this.updateGdsFilterList();
      this.initializeStatusFilterDropdown();
      this.initializePrimaryReasonFilterDropdown();

      // TODO: if filter is opened X times, this listener subscribes to BSP value changes X times
      // Do it on init with `skipWhile`
      this.initializeBspCountrieslistener();
    }
  }

  private initializeBspCountrieslistener(): void {
    this.bspControl.valueChanges
      .pipe(
        tap(bsps => this.updateBspSelectedIds(bsps)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.updateAgentList();
        this.updateCurrencyList();
        this.updateGdsForwardList();
        this.updatePeriodControlDisability();
        this.updatePeriodFilterOptions();
      });
  }

  private updateBspSelectedIds(bsps: Bsp | Bsp[]): void {
    if (bsps) {
      this.bspSelectedIds = Array.isArray(bsps) ? bsps.map(bsp => bsp.id) : [bsps.id];
    } else {
      this.bspSelectedIds = [];
    }
  }

  private updateAgentList(): void {
    this.updateAgentFilterList();

    // if LEAN user needs change Agent by BSP
    if (this.isMultiCountryUser) {
      this.updateAgentListControlValue();
    }
  }

  private updateAgentFilterList(): void {
    const param = { ...(this.bspSelectedIds.length && { bspId: this.bspSelectedIds }) };

    this.agentCodesList$ = this.agentDictionaryService.getDropdownOptions(param);
  }

  private updateAgentListControlValue(): void {
    const agentsSelected = this.agentListControl.value;
    if (this.bspSelectedIds.length && agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.bspSelectedIds.some(bspId => agent.bsp?.id === bspId));
      this.agentListControl.patchValue(agentsToPatch);
    }
  }

  private updateCurrencyList(): void {
    this.updateCurrencyFilterList();

    // if LEAN user needs change Currencies by BSP
    if (this.isMultiCountryUser) {
      this.updateCurrencyListControlValue();
    }
  }

  private updateCurrencyFilterList(): void {
    const param = { ...(this.bspSelectedIds.length && { bspId: this.bspSelectedIds }) };

    this.currencyDictionaryService
      .getFilteredDropdownOptions(param)
      .subscribe(currencyList => (this.currencyList = currencyList));
  }

  private updateCurrencyListControlValue(): void {
    const currenciesSelected = this.currencyControl.value;
    if (this.bspSelectedIds.length && currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyList.some(cur => cur.value.id === currency.id)
      );
      this.currencyControl.patchValue(currenciesToPatch);
    }
  }

  private updateGdsForwardList(): void {
    if (this.isMultiCountryUser) {
      this.updateGdsFilterList();
      this.updateGdsListControlValue();
    }
  }

  private updateGdsFilterList(): void {
    if (this.isGdsForwardVisible) {
      this.gdsDictionaryService
        .getDropdownOptionsForFiltering(this.bspSelectedIds)
        .subscribe(gdsOptions => (this.gdsForwardList = gdsOptions));
    }
  }

  private updateGdsListControlValue(): void {
    const gdsSelected = this.gdsControl.value;
    if (this.bspSelectedIds.length && gdsSelected?.length) {
      const gdsToPatch = gdsSelected.filter(gdsSelect =>
        this.gdsForwardList.some(gds => gds.value.id === gdsSelect.id)
      );
      this.gdsControl.patchValue(gdsToPatch);
    }
  }

  private updatePeriodControlDisability(): void {
    this.periodIsDisabled = this.isBspFilterMultiple && this.bspSelectedIds.length !== 1;
    if (this.periodIsDisabled) {
      this.periodControl.reset();
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private initializeAirlineFilterDropdown(): void {
    this.airlineCodesList$ = this.airlineDictionaryService.getDropdownOptions();
  }

  private initializeAgentGroupFilterDropdown(): void {
    this.agentCodesList$ = this.hasViewPermission
      ? this.agentDictionaryService.getDropdownOptions({ permission: this.viewPermission })
      : of(null);
  }

  private initializeStatusFilterDropdown(): void {
    this.statusList = Object.values(AdmAcmStatus)
      .filter(
        status =>
          status !== AdmAcmStatus.deleted &&
          status !== AdmAcmStatus.supervisionDeleted &&
          this.filterStatusByTypePermission(status)
      )
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.translationService.translate(`ADM_ACM.status.${label}`)
      }));
  }

  private updatePeriodFilterOptions(): void {
    if (this.bspSelectedIds.length === 1) {
      this.periodOptions$ = this.periodService.getByBsp(this.bspSelectedIds[0]);
    }
  }

  private initializePrimaryReasonFilterDropdown(): void {
    if (this.isReasonsVisible) {
      this.primaryReasonList$ = this.categoriesDictionaryService.getDropdownOptions();
    }
  }

  private filterStatusByTypePermission(status: AdmAcmStatus): boolean {
    let result = true;

    if (status === AdmAcmStatus.pendingAuthorization) {
      result =
        (this.acdmIsOfType(MasterDataType.Adm) || this.acdmIsOfType(MasterDataType.Acm)) &&
        this.permissionsService.hasPermission(getSupervisionAcdmPermissions());
    }

    return result;
  }

  private initializeColumns(): void {
    this.columns = [...SHARED.ACTIVE_COLUMNS];

    if (this.hasViewPermission) {
      this.columns = [this.getCheckboxColumn(), ...this.columns];
    }

    if (this.userType === UserType.AGENT) {
      this.columns = this.columns.filter(
        column => column.prop !== 'agent.bsp.name' && column.prop !== 'agent.iataCode'
      );
    }
    if (this.userType === UserType.AIRLINE) {
      this.columns = this.columns.filter(column => column.prop !== 'airline.iataCode');
    }
    if (!this.isADM) {
      this.columns = this.columns.filter(column => column.prop !== 'disputeDate');
    }

    this.initializeStatusBadgeInfo();

    this.setDocumentNumberColumn();

    this.setBspColumn();
  }

  private getCheckboxColumn(): GridColumn {
    return {
      prop: 'isRowSelected',
      maxWidth: 40,
      sortable: false,
      name: '',
      headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl'
    };
  }

  private setBspColumn(): void {
    const bspColumn = this.columns.find(column => column.prop === 'agent.bsp.isoCountryCode');

    if (bspColumn) {
      bspColumn.sortable = this.hasLeanPermission;
    }
  }

  private initializeStatusBadgeInfo(): void {
    const statusColumn = this.columns.find(col => col.prop === 'acdmStatus');
    if (statusColumn) {
      statusColumn.pipe = { transform: value => this.translationService.translate(`ADM_ACM.status.${value}`) };

      statusColumn.badgeInfo = {
        hidden: (value: AdmAcmIssueBE) => !value.sentToDpc,
        value: this.translationService.translate(`ADM_ACM.dpcBadgeInfo.label`),
        type: (value: AdmAcmIssueBE) => {
          switch (value.sentToDpc) {
            case SendToDpcType.pending:
              return BadgeInfoType.info;

            case SendToDpcType.sent:
              return BadgeInfoType.success;

            case SendToDpcType.n_a:
            default:
              return BadgeInfoType.regular;
          }
        },
        tooltipLabel: (value: AdmAcmIssueBE) =>
          value.sentToDpc
            ? this.translationService.translate(`ADM_ACM.dpcBadgeInfo.leyend.${value.sentToDpc.toUpperCase()}`, {
                type: this.translationService.translate(`ADM_ACM.type.${this.admAcmType}`)
              })
            : '',
        showIconType: true
      };
    }
  }

  private setDocumentNumberColumn(): void {
    const documentNumberColumn = this.columns.find(column => column.prop === 'ticketDocumentNumber');

    if (documentNumberColumn) {
      if (this.hasReadInternalCommentPermission) {
        documentNumberColumn.image = (acdm: AdmAcmIssueBE) =>
          acdm.internalComment
            ? {
                src: '/assets/images/utils/discuss.svg',
                tooltipLabel: this.translationService.translate(`ADM_ACM.query.internalCommentTooltip`)
              }
            : null;
      }

      documentNumberColumn.icon = (acdm: AdmAcmIssueBE) => ({
        class: 'material-icons-outlined',
        name: 'remove_red_eye',
        tooltip: this.translationService.translate(`watching`),
        hidden: !acdm.watching
      });
    }
  }

  private initializeLoggedUser$(): Observable<any> {
    return this.store.pipe(select(getUser), first()).pipe(
      tap(user => {
        this.loggedUser = user;
        this.userType = user.userType;
        this.isMultiCountryUser = user.bsps.length > 1;
      })
    );
  }

  public onGetActionList(acdm: AdmAcmIssueBE & { markStatus: string }): void {
    this.listActions$ = this.admAcmService.getAcdmActionList(acdm.id).pipe(
      map(actions => {
        let modifiedActions: any = actions;

        const hasLoadingAction = actions.some(action => action.action === GridTableActionType.Loading);

        if (!hasLoadingAction && this.hasViewPermission) {
          const readUnreadAction =
            acdm.markStatus === MarkStatus.Read ? GridTableActionType.MarkAsUnread : GridTableActionType.MarkAsRead;

          const watchingAction = acdm.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;
          const isIataUserType = this.userTypes?.IATA === this.userType;

          modifiedActions = [
            ...modifiedActions,
            { action: readUnreadAction, group: 'markStatus' },
            { action: GridTableActionType.Archive, group: 'markStatus' }
          ];

          if (!isIataUserType) {
            modifiedActions.push({ action: watchingAction, group: 'markStatus' });
          }
        }

        return this.groupActions(modifiedActions);
      })
    );
  }

  /** `protected` since it's used in adm-acm-archived-list (child) */
  protected groupActions(
    actions: { action: GridTableActionType; disabled?: boolean; group?: string }[]
  ): { action: GridTableActionType; disabled?: boolean; group?: string }[] {
    // For now, we just need to group `internal comment` action
    return actions.map(action =>
      action.action === GridTableActionType.InternalComment ? { ...action, group: 'comment' } : action
    );
  }

  private download(detailed?: boolean): void {
    const modalTitle = detailed
      ? `ADM_ACM.query.${this.admAcmType}_detailedDownloadTitle`
      : `ADM_ACM.query.${this.admAcmType}_downloadTitle`;

    const data = {
      title: this.translationService.translate(modalTitle),
      footerButtonsType: FooterButton.Download,
      downloadQuery: this.downloadQuery,
      totalElements: this.downloadQuery.paginateBy.totalElements,
      detailedDownload: detailed
    };

    this.dialogService.open(DownloadFileComponent, {
      data,
      apiService: this.admAcmService
    });
  }

  public onDownload(): void {
    this.download();
  }

  public onDetailedDownload(): void {
    this.download(true);
  }

  public onViewAdm(id): void {
    const url = urlView[this.admAcmType];

    this.router.navigate([url, id]);
  }

  public onActionClick({ action, row }): void {
    const actionType: GridTableActionType = action.actionType;
    const actionCommand = this.formConfig.getActionCommand(
      actionType,
      row,
      this.dialogQueryFormSubmitEmitter,
      this.getListKey()
    );

    actionCommand();
  }

  public onImageClick(event: { event: Event; row: AdmAcmIssueBE; column: GridColumn }): void {
    if (this.hasReadInternalCommentPermission) {
      const { row } = event;

      this.admAcmDialogService.openInternalCommentDialog(row, this.admAcmType);
    }
  }

  public loggedUserIsOfType(type): boolean {
    return type.toUpperCase() === this.userType.toUpperCase();
  }

  public getGdsName(gds: { gdsCode: string; gdsForwardDate: string }): string {
    const gdsFromDictionary = this.gdsForwardList?.find(gdsDictionary => gdsDictionary.value.gdsCode === gds.gdsCode);

    return gdsFromDictionary ? gdsFromDictionary.value.name : gds.gdsCode;
  }

  private acdmIsOfType(type: MasterDataType): boolean {
    return type.toUpperCase() === this.admAcmType.toUpperCase();
  }

  private initializeFilterVisibility(): void {
    this.isReasonsVisible = this.acdmIsOfType(MasterDataType.Adm) || this.acdmIsOfType(MasterDataType.Admq);
    this.isDisputeDateVisible = this.acdmIsOfType(MasterDataType.Adm) || this.acdmIsOfType(MasterDataType.Acm);
    this.isGdsForwardVisible =
      this.acdmIsOfType(MasterDataType.Adm) && this.permissionsService.hasPermission(this.gdsForwardPermission);
    this.isInternalCommentVisible = this.hasReadInternalCommentPermission;
  }

  private setInitialFilterValues(): void {
    this.searchform = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));

    this.reasonControl = FormUtil.get<AdmAcmFilter>(this.searchform, 'reason');
    this.primaryReasonControl = FormUtil.get<AdmAcmFilter>(this.searchform, 'primaryReason');
    this.bspControl = FormUtil.get<AdmAcmFilter>(this.searchform, 'bsp');
    this.agentListControl = FormUtil.get<AdmAcmFilter>(this.searchform, 'agentId');
    this.periodControl = FormUtil.get<AdmAcmFilter>(this.searchform, 'period');
    this.currencyControl = FormUtil.get<AdmAcmFilter>(this.searchform, 'currencyId');
    this.gdsControl = FormUtil.get<AdmAcmFilter>(this.searchform, 'gdsForwardCode');
  }

  private initializeFilterListeners(): void {
    if (this.isReasonsVisible) {
      this.primaryReasonControl.valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(value => this.getSubReasonDropDown(value));
    }
  }

  private getSubReasonDropDown(value): void {
    if (value) {
      this.reasonList$ = this.categoriesDictionaryService.getDropdownOptions(value.id);
    } else {
      this.reasonControl.patchValue('');
      this.reasonList$ = of([]);
    }
  }

  private populateFilterDropdowns(): void {
    this.rtdnTypeList$ = this.formConfig.getAdmaFor();

    this.sentToDpcList = Object.values(SendToDpcType)
      .filter(sentStatus => sentStatus !== SendToDpcType.na)
      .map(sentStatus => ({
        value: sentStatus,
        label: this.translationService.translate(`ADM_ACM.sentToDpcStatus.${sentStatus}`)
      }));

    if (this.isInternalCommentVisible) {
      this.hasInternalCommentList = [true, false].map(item => ({
        label: this.translationService.translate(`ADM_ACM.yesNoBooleanValue.${item}`),
        value: item
      }));
    }

    if (!this.isArchivedSubTab) {
      this.initializeMarkStatusDropdown();
    }
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, getBspFilterRequiredPermissions(this.admAcmType))
      .pipe(
        tap(bspList => {
          // Update BSP selected ids and predefined filters with the default BSP
          const onlyOneBsp = bspList.length === 1;

          if (bspList.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);

            const firstListBsp = bspList[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelectedIds = [filterValue.id];

            // Agent users have not predefined filters
            if (this.userType !== UserType.AGENT) {
              this.predefinedFilters = {
                ...this.predefinedFilters,
                bsp: filterValue
              };
            }
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspOptions => (this.isBspFilterMultiple = this.hasLeanPermission && bspOptions.length !== 1)),
        tap(bspOptions => (this.isBspFilterLocked = bspOptions.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  private initializeMarkStatusDropdown(): void {
    this.markStatusList = Object.values(MarkStatusFilter).map(markStatus => ({
      value: markStatus,
      label: this.getTranslatedMarkStatus(markStatus)
    }));
  }

  private getTranslatedMarkStatus(status: MarkStatusFilter): string {
    return this.translationService.translate(`ADM_ACM.markStatus.${status}`);
  }

  protected populateButtonMenuOptions(): void {
    this.buttonMenuOptions$ = this.admAcmService.getAcdmMenuOptions(this.isArchivedSubTab).pipe(
      map(options =>
        options.map(option => ({
          ...option,
          label: this.getTranslatedButtonMenuOptionLabel(option),
          group: this.setButtonMenuOptionGroup(option),
          command: this.setButtonMenuOptionCommand(option)
        }))
      )
    );
  }

  private getTranslatedButtonMenuOptionLabel(option: ButtonMenuOption): string {
    const pathTranslation = this.isArchivedSubTab
      ? `ADM_ACM.query.tableSelectionToolbar.buttonMenuOptionsArchive.${option.label}`
      : `ADM_ACM.query.tableSelectionToolbar.buttonMenuOptions.${option.label}`;

    return this.translationService.translate(pathTranslation);
  }

  private setButtonMenuOptionGroup(option: ButtonMenuOption): string {
    let group: string;

    switch (option.id) {
      case AcdmOptionType.Read:
      case AcdmOptionType.Unread:
      case AcdmOptionType.Archive:
        group = 'markAs';
        break;
      case AcdmOptionType.DownloadAttachments:
        group = 'download';
        break;
    }

    return group;
  }

  private setButtonMenuOptionCommand(option: ButtonMenuOption): () => void {
    let command: () => void;

    switch (option.id) {
      case AcdmOptionType.Read:
        if (this.isArchivedSubTab) {
          command = () => this.moveToActive();
        } else {
          command = () => this.setMarkStatus(MarkStatus.Read);
        }
        break;
      case AcdmOptionType.Unread:
        command = () => this.setMarkStatus(MarkStatus.Unread);
        break;
      case AcdmOptionType.Archive:
        command = () => this.markAsArchive();
        break;
      case AcdmOptionType.DownloadAttachments:
        command = () => this.bulkDownloadAttachments();
        break;
    }

    return command;
  }

  private bulkDownloadAttachments(): void {
    this.selectedRows$
      .pipe(
        first(),
        switchMap(rows => combineLatest([of(rows), this.admAcmService.bulkDownloadAttachments(rows)]))
      )
      .subscribe(
        ([rows, file]) => this.onDownloadSuccess(rows, file),
        error => this.onDownloadError(error)
      );
  }

  private onDownloadSuccess(rows: AdmAcmIssueBE[], file: { blob: Blob; fileName: string }): void {
    FileSaver.saveAs(file.blob, file.fileName);
    this.notificationService.showSuccess(
      this.translationService.translate('ADM_ACM.success_download_attachments', {
        items: rows.length,
        type: this.translationService.translate(`ADM_ACM.type.${this.admAcmType}`)
      })
    );
  }

  private onDownloadError(error: ServerError): void {
    if (error.status === 404) {
      this.notificationService.showError(this.translationService.translate('ADM_ACM.error_download_attachments'));
    } else {
      this.globalErrorHandlerService.handleError(error);
    }
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
