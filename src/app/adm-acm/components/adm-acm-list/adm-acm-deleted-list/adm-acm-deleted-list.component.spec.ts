import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { cold } from 'jasmine-marbles';
import { identity } from 'lodash';
import { of } from 'rxjs';

import { AdmAcmDeletedListComponent } from './adm-acm-deleted-list.component';
import { SEARCH_FORM_DEFAULT_VALUE } from '~app/adm-acm/components/adm-acm-list/adm-acm-list.constants';
import { AdmAcmDocumentSummary } from '~app/adm-acm/models/adm-acm-enquiry-document.model';
import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';
import { AdmAcmStatus, ConcernIndicator, TransactionCode } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmFilterFormatter } from '~app/adm-acm/services/adm-acm-filter-formatter.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { viewAcdntPermission } from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { deletedKeys, deletedSelectors } from '~app/adm-acm/store/reducers';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { AirlineSummary, DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { AgentDictionaryService, AirlineDictionaryService, CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('AdmAcmDeletedListComponent', () => {
  let component: AdmAcmDeletedListComponent;
  let fixture: ComponentFixture<AdmAcmDeletedListComponent>;

  const admAcmServiceSpy = createSpyObject(AdmAcmService);
  const admAcmConfigServiceSpy = createSpyObject(AdmAcmConfigService);
  const admAcmDialogServiceSpy = createSpyObject(AdmAcmDialogService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const currencyDictionaryServiceSpy = createSpyObject(CurrencyDictionaryService);
  const periodServiceSpy = createSpyObject(PeriodService);
  const bspsDictionaryService = createSpyObject(BspsDictionaryService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const activatedRouteStub = {
    snapshot: {
      data: { admAcmType: MasterDataType.Adm }
    }
  };

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    defaultIsoc: 'ES',
    permissions: ['rAdm', 'rAdmIntCom', 'uAdmIntCom', Permissions.lean],
    bspPermissions: [
      {
        bspId: 1,
        permissions: ['rAdm', 'rAdmIntCom', 'uAdmIntCom', Permissions.lean]
      },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          'MENU.ACDMS.QUERY.adm': {},
          activeTabId: 'MENU.ACDMS.QUERY.adm'
        }
      },
      viewListsInfo: {}
    },
    acdm: { 'adm-deleted-list': fromListSubtabs.initialState }
  };

  const storedQuery: DataQuery<AdmAcmFilter> = {
    ...defaultQuery
  };

  const bspsList: BspDto[] = [{ isoCountryCode: 'ES', name: 'Spain', id: 1 }];

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmDeletedListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule],
      providers: [
        FormBuilder,
        PermissionsService,
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromAuth.getUser, value: expectedUserDetails },
            { selector: fromAuth.getUserBsps, value: expectedUserDetails.bsps }
          ]
        }),
        provideMockActions(() => of()),
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: AdmAcmDialogService, useValue: admAcmDialogServiceSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionaryServiceSpy },
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryService },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    })
      .overrideComponent(AdmAcmDeletedListComponent, {
        set: {
          providers: [
            AdmAcmFilterFormatter,
            AdmAcmDataService,
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: AdmAcmService, useValue: admAcmServiceSpy },
            { provide: AdmAcmConfigService, useValue: admAcmConfigServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    bspsDictionaryService.getAllBspsByPermissions.and.returnValue(of(bspsList));
    translationServiceSpy.translate.and.callFake(identity);

    fixture = TestBed.createComponent(AdmAcmDeletedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should initialize userType user', () => {
    component.ngOnInit();
    expect(component['userType']).toEqual(expectedUserDetails.userType as UserType);
  });

  it('should initialize config data', () => {
    component.ngOnInit();

    expect(component.admAcmType).toBe(activatedRouteStub.snapshot.data.admAcmType);
    expect(component.isAcdmRequest).toBeFalsy();
  });

  it('should initialize loading', () => {
    component.ngOnInit();
    expect(component.loading$).toBeObservable(cold('a', { a: false }));
  });

  it('should initialize columns', () => {
    component.ngOnInit();
    expect(component.columns.length).toBeGreaterThan(0);
  });

  it('should return correct ListSelector', () => {
    const result = component['getListSelector']();

    expect(result).toBe(deletedSelectors[activatedRouteStub.snapshot.data.admAcmType]);
  });

  it('should return correct ListKey', () => {
    const result = component['getListKey']();

    expect(result).toBe(deletedKeys[activatedRouteStub.snapshot.data.admAcmType]);
  });

  it('should return adaptSearchQuery adapted with acdmStatus', () => {
    const result = component['adaptSearchQuery'](storedQuery);

    const expectedQuery: DataQuery<AdmAcmFilter> = {
      ...storedQuery,
      filterBy: {
        ...storedQuery.filterBy,
        acdmStatus: [AdmAcmStatus.deleted, AdmAcmStatus.supervisionDeleted]
      }
    };

    expect(result).toEqual(expectedQuery);
  });

  it('should return same query for adaptSearchQuery', () => {
    const query: DataQuery<AdmAcmFilter> = {
      ...storedQuery,
      filterBy: {
        ...storedQuery.filterBy,
        acdmStatus: [AdmAcmStatus.deleted, AdmAcmStatus.supervisionDeleted]
      }
    };
    const result = component['adaptSearchQuery'](query);

    expect(result).toEqual(query);
  });

  it('should initializePermissions with IATA user type', () => {
    component.isAcdmRequest = false;
    component.userType = UserType.IATA;
    component.hasEditInternalCommentPermission = false;

    component['initializePermissions']();

    expect(component.isActionListVisible).toBeTruthy();
    expect(component['hasLeanPermission']).toBeTruthy();
  });

  it('should initializePermissions when user type is different then IATA', () => {
    const service = TestBed.inject(PermissionsService);

    component.isAcdmRequest = true;
    component.userType = UserType.DPC;
    component.admAcmType = MasterDataType.GDS;
    spyOn(service, 'hasPermission').and.callThrough();

    component['initializePermissions']();
    expect(service.hasPermission).toHaveBeenCalled();
    expect(service.hasPermission).toHaveBeenCalled();
    expect(component.isActionListVisible).toBeFalsy();
  });

  it('should initializePermissions with LEAN permission', () => {
    component['initializePermissions']();

    expect(component['hasReadInternalCommentPermission']).toBeTruthy();
    expect(component['hasEditInternalCommentPermission']).toBeTruthy();
    expect(component.isActionListVisible).toBeTruthy();
    expect(component['hasLeanPermission']).toBeTruthy();
  });

  it('should setResolversData initialize correct observables', () => {
    const acdmBE: AdmAcmIssueBE = {
      id: 698300000444415,
      acdmStatus: AdmAcmStatus.deleted,
      agentTotal: 0,
      airlineTotal: 0,
      concernsIndicator: ConcernIndicator.issue,
      statisticalCode: 'ES',
      taxOnCommissionType: 'a',
      totalAmount: 0,
      agent: null,
      agentAddress: {
        street: null,
        city: null,
        state: null,
        country: null,
        postalCode: null,
        telephone: null
      },
      agentCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      airline: null,
      airlineAddress: {
        address1: null,
        address2: null,
        city: null,
        state: null,
        country: null,
        postalCode: null,
        telephone: null
      },
      airlineCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      airlineContact: null,
      attachmentIds: [],
      categories: [],
      currency: null,
      deletionReason: 'deletion Reason',
      issueReason: 'reason for memo',
      differenceCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      gdsForwards: [],
      netReporting: false,
      reasons: [],
      relatedTicketDocuments: [],
      taxMiscellaneousFees: [],
      ticketDocumentNumber: '0607512479 0',
      transactionCode: 'ADMA',
      internalComment: null
    };
    component.selectedItem$ = of(acdmBE);
    let deletionReason;
    let memoReason;

    component['setResolversData']();
    component.deletionReason$.subscribe(data => (deletionReason = data));
    component.memoReason$.subscribe(data => (memoReason = data));

    expect(deletionReason).toEqual(acdmBE.deletionReason);
    expect(memoReason).toEqual(acdmBE.issueReason);
  });

  it('should setResolversData initialize empty deletionReasion', () => {
    const acdmBE: AdmAcmIssueBE = {
      id: 698300000444415,
      acdmStatus: AdmAcmStatus.deleted,
      agentTotal: 0,
      airlineTotal: 0,
      concernsIndicator: ConcernIndicator.issue,
      statisticalCode: 'ES',
      taxOnCommissionType: 'a',
      totalAmount: 0,
      agent: null,
      agentAddress: {
        street: null,
        city: null,
        state: null,
        country: null,
        postalCode: null,
        telephone: null
      },
      agentCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      airline: null,
      airlineAddress: {
        address1: null,
        address2: null,
        city: null,
        state: null,
        country: null,
        postalCode: null,
        telephone: null
      },
      airlineCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      airlineContact: null,
      attachmentIds: [],
      categories: [],
      currency: null,
      issueReason: 'reason for memo',
      differenceCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      gdsForwards: [],
      netReporting: false,
      reasons: [],
      relatedTicketDocuments: [],
      taxMiscellaneousFees: [],
      ticketDocumentNumber: '0607512479 0',
      transactionCode: 'ADMA',
      internalComment: null
    };
    component.selectedItem$ = of(acdmBE);
    let deletionReason;
    let memoReason;

    component['setResolversData']();
    component.deletionReason$.subscribe(data => (deletionReason = data));
    component.memoReason$.subscribe(data => (memoReason = data));

    expect(deletionReason).toEqual('');
    expect(memoReason).toEqual(acdmBE.issueReason);
  });

  it('should setResolversData not set memoReason when isAcdmRequest is "false"', () => {
    const acdmBE: AdmAcmIssueBE = {
      id: 698300000444415,
      acdmStatus: AdmAcmStatus.deleted,
      agentTotal: 0,
      airlineTotal: 0,
      concernsIndicator: ConcernIndicator.issue,
      statisticalCode: 'ES',
      taxOnCommissionType: 'a',
      totalAmount: 0,
      agent: null,
      agentAddress: {
        street: null,
        city: null,
        state: null,
        country: null,
        postalCode: null,
        telephone: null
      },
      agentCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      airline: null,
      airlineAddress: {
        address1: null,
        address2: null,
        city: null,
        state: null,
        country: null,
        postalCode: null,
        telephone: null
      },
      airlineCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      airlineContact: null,
      attachmentIds: [],
      categories: [],
      currency: null,
      deletionReason: 'deletion Reason',
      issueReason: 'reason for memo',
      differenceCalculations: {
        fare: 0,
        tax: 0,
        commission: 0,
        supplementaryCommission: 0,
        taxOnCommission: 0,
        cancellationPenalty: 0,
        miscellaneousFee: 0
      },
      gdsForwards: [],
      netReporting: false,
      reasons: [],
      relatedTicketDocuments: [],
      taxMiscellaneousFees: [],
      ticketDocumentNumber: '0607512479 0',
      transactionCode: 'ADMA',
      internalComment: null
    };

    const mockDownloadQueryData: DataQuery<AdmAcmFilter> = {
      paginateBy: {
        page: 0,
        size: 20,
        totalElements: 114
      },
      sortBy: [
        {
          attribute: 'airline.iataCode',
          sortType: SortOrder.Asc
        }
      ],
      filterBy: {
        bsp: {
          id: 6983,
          isoCountryCode: 'ES',
          name: 'SPAIN'
        },
        acdmStatus: ['DELETED', 'SUPERVISION_DELETED']
      }
    };
    const onQueryChangedSpy = spyOn<any>(component, 'onQueryChanged').and.callThrough();
    const adaptSearchQuerySpy = spyOn<any>(component, 'adaptSearchQuery')
      .and.callThrough()
      .and.returnValue(mockDownloadQueryData);

    component.isAcdmRequest = true;

    component.selectedItem$ = of(acdmBE);
    let memoReason;

    component['setResolversData']();

    component.memoReason$.subscribe(data => (memoReason = data));

    expect(memoReason).toEqual('');
    expect(adaptSearchQuerySpy).toHaveBeenCalled();
    expect(onQueryChangedSpy).not.toHaveBeenCalled();
    expect(component.downloadQuery).toEqual(mockDownloadQueryData);
  });

  it('should initializeLeanBspFilter$ when user is AGENT', () => {
    component.userType = UserType.AGENT;
    component.predefinedFilters = null;
    let result;
    component['initializeLeanBspFilter$']().subscribe(data => (result = data));

    expect(result).not.toBeNull();
    expect(component.predefinedFilters).toBeNull();
  });

  it('should initializeLeanBspFilter$ initialize correct lists multiple BSPs', fakeAsync(() => {
    component['initializeLeanBspFilter$']();
    tick();

    expect(component.bspCountriesList.length).toEqual(bspsList.length);
    expect(component.isBspFilterMultiple).toBeFalsy();
    expect(component.isBspFilterLocked).toBeTruthy();
    expect(component['bspSelectedIds']).toEqual([1]);
  }));

  it('should initializeLeanBspFilter$ initialize correct lists with only one BSP', fakeAsync(() => {
    const bspList = [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }];
    bspsDictionaryService.getAllBspsByPermissions.and.returnValue(of(bspList));

    component['initializeLeanBspFilter$']();
    tick();

    expect(component.predefinedFilters.bsp).toEqual(bspsList[0]);
  }));

  it('should initializeLeanBspFilter$ initialize correct lists with default BSP', fakeAsync(() => {
    const bspList = [
      { id: 1, name: 'Spain', isoCountryCode: 'ES' },
      { id: 2, name: 'Malta', isoCountryCode: 'MT' }
    ];
    const expectedBsp = jasmine.objectContaining({ id: 1, name: 'Spain', isoCountryCode: 'ES' });
    bspsDictionaryService.getAllBspsByPermissions.and.returnValue(of(bspList));
    component.loggedUser.defaultIsoc = 'ES';
    component['hasLeanPermission'] = false;

    component['initializeLeanBspFilter$']();
    tick();

    expect(component.predefinedFilters.bsp).toEqual(expectedBsp);
    expect(component.loggedUser.defaultIsoc).toEqual('ES');
  }));

  it('should onFilterButtonClicked not execute any function', () => {
    const updateCurrencyListSpy = spyOn<any>(component, 'updateCurrencyList').and.callThrough();
    component.onFilterButtonClicked(false);

    expect(updateCurrencyListSpy).not.toHaveBeenCalled();
  });

  it('should onFilterButtonClicked initialize correctly as a AIRLINE', () => {
    const updateAgentListSpy = spyOn<any>(component, 'updateAgentList').and.callThrough();
    const initializeInternalCommentFilterDropdownSpy = spyOn<any>(
      component,
      'initializeInternalCommentFilterDropdown'
    ).and.callThrough();
    spyOn<any>(component, 'updateCurrencyList');
    spyOn<any>(component, 'initializeStatusFilterDropdown');
    spyOn<any>(component, 'initializeRtdnTypeFilterDropdown');
    spyOn<any>(component, 'initializeBspCountrieslistener');
    component['isInternalCommentVisible'] = true;

    component.onFilterButtonClicked(true);

    expect(updateAgentListSpy).toHaveBeenCalled();
    expect(initializeInternalCommentFilterDropdownSpy).toHaveBeenCalled();
  });

  it('should onFilterButtonClicked initialize correctly as a AGENT', () => {
    const initializeAirlineFilterDropdownSpy = spyOn<any>(
      component,
      'initializeAirlineFilterDropdown'
    ).and.callThrough();
    const initializeInternalCommentFilterDropdownSpy = spyOn<any>(
      component,
      'initializeInternalCommentFilterDropdown'
    ).and.callThrough();
    spyOn<any>(component, 'updateCurrencyList');
    spyOn<any>(component, 'initializeStatusFilterDropdown');
    spyOn<any>(component, 'initializeRtdnTypeFilterDropdown');
    spyOn<any>(component, 'initializeBspCountrieslistener');
    component['isInternalCommentVisible'] = true;
    component.userType = UserType.AGENT;

    component.onFilterButtonClicked(true);

    expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    expect(initializeInternalCommentFilterDropdownSpy).toHaveBeenCalled();
  });

  it('should onFilterButtonClicked initialize correctly as a AGENT_GROUP', () => {
    const initializeAirlineFilterDropdownSpy = spyOn<any>(
      component,
      'initializeAirlineFilterDropdown'
    ).and.callThrough();
    const initializeAgentGroupFilterDropdownSpy = spyOn<any>(
      component,
      'initializeAgentGroupFilterDropdown'
    ).and.callThrough();
    const initializeInternalCommentFilterDropdownSpy = spyOn<any>(
      component,
      'initializeInternalCommentFilterDropdown'
    ).and.callThrough();
    spyOn<any>(component, 'updateCurrencyList');
    spyOn<any>(component, 'initializeStatusFilterDropdown');
    spyOn<any>(component, 'initializeRtdnTypeFilterDropdown');
    spyOn<any>(component, 'initializeBspCountrieslistener');
    component['isInternalCommentVisible'] = true;
    component.userType = UserType.AGENT_GROUP;

    component.onFilterButtonClicked(true);

    expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    expect(initializeAgentGroupFilterDropdownSpy).toHaveBeenCalled();
    expect(initializeInternalCommentFilterDropdownSpy).toHaveBeenCalled();
  });

  it('should onFilterButtonClicked initialize correctly as a IATA', () => {
    const initializeAirlineFilterDropdownSpy = spyOn<any>(
      component,
      'initializeAirlineFilterDropdown'
    ).and.callThrough();
    const updateAgentFilterListSpy = spyOn<any>(component, 'updateAgentFilterList').and.callThrough();
    const initializeInternalCommentFilterDropdownSpy = spyOn<any>(
      component,
      'initializeInternalCommentFilterDropdown'
    ).and.callThrough();
    spyOn<any>(component, 'updateCurrencyList');
    spyOn<any>(component, 'initializeStatusFilterDropdown');
    spyOn<any>(component, 'initializeRtdnTypeFilterDropdown');
    spyOn<any>(component, 'initializeBspCountrieslistener');
    component['isInternalCommentVisible'] = true;
    component.userType = UserType.IATA;

    component.onFilterButtonClicked(true);

    expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
    expect(updateAgentFilterListSpy).toHaveBeenCalled();
    expect(initializeInternalCommentFilterDropdownSpy).toHaveBeenCalled();
  });

  it('should initializeBspCountrieslistener', fakeAsync(() => {
    const updatePeriodFilterOptionsSpy = spyOn<any>(component, 'updatePeriodFilterOptions').and.callThrough();
    const updateBspSelectedIdsSpy = spyOn<any>(component, 'updateBspSelectedIds').and.callThrough();
    const updateAgentListSpy = spyOn<any>(component, 'updateAgentList').and.callThrough();
    const updateCurrencyListSpy = spyOn<any>(component, 'updateCurrencyList').and.callThrough();
    const updatePeriodControlDisabilitySpy = spyOn<any>(component, 'updatePeriodControlDisability').and.callThrough();
    const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));

    component['initializeBspCountrieslistener']();
    component.searchform.get('bsp').patchValue(mockBsp);

    tick();
    expect(updatePeriodFilterOptionsSpy).toHaveBeenCalled();
    expect(updateBspSelectedIdsSpy).toHaveBeenCalled();
    expect(updateAgentListSpy).toHaveBeenCalled();
    expect(updateCurrencyListSpy).toHaveBeenCalled();
    expect(updatePeriodControlDisabilitySpy).toHaveBeenCalled();
  }));

  it('should updateBspSelectedIds sets selected ids from bsps array', () => {
    const mockBsps = [{ id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }];

    component['updateBspSelectedIds'](mockBsps);

    expect(component['bspSelectedIds']).toEqual([2]);
  });

  it('should updateBspSelectedIds sets selected ids from bsps object', () => {
    const mockBsps = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

    component['updateBspSelectedIds'](mockBsps);

    expect(component['bspSelectedIds']).toEqual([2]);
  });

  it('should updateBspSelectedIds sets any selected ids from empty list', () => {
    component['updateBspSelectedIds']([]);

    expect(component['bspSelectedIds']).toEqual([]);
  });

  it('should updateAgentList if user is multi country ', () => {
    const updateAgentFilterListSpy = spyOn<any>(component, 'updateAgentFilterList').and.callThrough();
    const updateAgentListControlValueSpy = spyOn<any>(component, 'updateAgentListControlValue').and.callThrough();

    component['updateAgentList']();

    expect(updateAgentFilterListSpy).toHaveBeenCalled();
    expect(updateAgentListControlValueSpy).toHaveBeenCalled();
  });

  it('should not updateAgentList if user is not multi country ', () => {
    const updateAgentFilterListSpy = spyOn<any>(component, 'updateAgentFilterList').and.callThrough();
    const updateAgentListControlValueSpy = spyOn<any>(component, 'updateAgentListControlValue').and.callThrough();
    component['isMultiCountryUser'] = false;
    component['updateAgentList']();

    expect(updateAgentFilterListSpy).toHaveBeenCalled();
    expect(updateAgentListControlValueSpy).not.toHaveBeenCalled();
  });

  it('should updateAgentFilterList', () => {
    component['bspSelectedIds'] = [2];
    component['updateAgentFilterList']();

    expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith({ bspId: [2] });
  });

  it('should updateAgentListControlValue', () => {
    const agentMock = {
      id: '1',
      name: 'AGENT 1111111',
      code: '1111111',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
    };

    expect(component['agentListControl'].value).toBeNull();

    component.searchform.get('agentId').patchValue(agentMock);
    component['bspSelectedIds'] = [1];
    component['updateAgentListControlValue']();

    expect(component['agentListControl'].value).toEqual(agentMock);
  });

  it('should updateAgentListControlValue not set agentListControl value', () => {
    const formPatchValueSpy = spyOn<any>(component['agentListControl'], 'patchValue');
    component['bspSelectedIds'] = [];
    component['updateAgentListControlValue']();

    expect(component['agentListControl'].value).toBeNull();
    expect(formPatchValueSpy).not.toHaveBeenCalled();
  });

  it('should updateAgentListControlValue', () => {
    const agentMock = {
      id: '1',
      name: 'AGENT 1111111',
      code: '1111111',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
    };
    expect(component['agentListControl'].value).toBeNull();

    component.searchform.get('agentId').patchValue(agentMock);
    component['bspSelectedIds'] = [1];
    component['updateAgentListControlValue']();

    expect(component['agentListControl'].value).toEqual(agentMock);
  });

  it('should not updateCurrencyList if user is not multi country', () => {
    const updateCurrencyFilterListSpy = spyOn<any>(component, 'updateCurrencyFilterList').and.callThrough();
    const updateCurrencyListControlValueSpy = spyOn<any>(component, 'updateCurrencyListControlValue').and.callThrough();
    component['isMultiCountryUser'] = false;
    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));

    component['updateCurrencyList']();

    expect(updateCurrencyFilterListSpy).toHaveBeenCalled();
    expect(updateCurrencyListControlValueSpy).not.toHaveBeenCalled();
  });

  it('should updateCurrencyList if user is multi country', () => {
    const updateCurrencyFilterListSpy = spyOn<any>(component, 'updateCurrencyFilterList').and.callThrough();
    const updateCurrencyListControlValueSpy = spyOn<any>(component, 'updateCurrencyListControlValue').and.callThrough();
    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));
    component['isMultiCountryUser'] = true;

    component['updateCurrencyList']();

    expect(updateCurrencyFilterListSpy).toHaveBeenCalled();
    expect(updateCurrencyListControlValueSpy).toHaveBeenCalled();
  });

  it('should updateCurrencyFilterList', () => {
    component['bspSelectedIds'] = [2];
    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));
    component['updateCurrencyFilterList']();

    expect(currencyDictionaryServiceSpy.getFilteredDropdownOptions).toHaveBeenCalledWith({ bspId: [2] });
  });

  it('should updateCurrencyListControlValue', () => {
    const currencyMock = { id: 1, code: 'EUR', decimals: 2 };
    component.searchform.get('currencyId').patchValue(currencyMock);
    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));
    component.currencyList = [{ label: currencyMock.code, value: currencyMock }];
    component['bspSelectedIds'] = [1];

    component['updateCurrencyFilterList']();

    expect(component['currencyControl'].value).toEqual(currencyMock);
  });

  it('should updateCurrencyListControlValue not ', () => {
    component['currencyControl'] = new FormControl();
    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));
    component['bspSelectedIds'] = [];

    component['updateCurrencyFilterList']();

    expect(component['currencyControl'].value).toBeNull();
  });

  it('should updatePeriodControlDisability', () => {
    component.isBspFilterMultiple = true;
    component['bspSelectedIds'] = [];
    component['periodControl'].setValue({ period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' });
    component['updatePeriodControlDisability']();

    expect(component['periodControl'].value).toEqual(null);
  });

  it('should updatePeriodControlDisability not reset period control', () => {
    const periodControlValue = { period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' };
    component.isBspFilterMultiple = false;
    component['bspSelectedIds'] = [1];
    component['periodControl'].setValue(periodControlValue);
    component['updatePeriodControlDisability']();

    expect(component['periodControl'].value).toEqual(periodControlValue);
  });

  it('should setPeriodPickerYearsBack when user is LEAN', () => {
    component['hasLeanPermission'] = true;
    component['setPeriodPickerYearsBack']();

    expect(component['periodPickerYearsBack']).toEqual(component['LEAN_USERS_YEARS']);
  });

  it('should setPeriodPickerYearsBack when user is not LEAN', () => {
    component['hasLeanPermission'] = false;
    component['setPeriodPickerYearsBack']();

    expect(component['periodPickerYearsBack']).toEqual(component['NON_LEAN_USERS_YEARS']);
  });

  it('should initializeAirlineFilterDropdown', fakeAsync(() => {
    airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));
    component['initializeAirlineFilterDropdown']();
    tick();
    let result;
    component.airlineCodesList$.subscribe(data => (result = data));
    expect(result).toEqual(mockAirlineDropdownOptions);
  }));

  it('should initializeAgentGroupFilterDropdown as a ADM', () => {
    component['initializeAgentGroupFilterDropdown']();

    expect(component.agentCodesList$).toBeNull();
  });

  it('should initializeAgentGroupFilterDropdown as a ADNT', () => {
    component['formConfig'].admAcmType = MasterDataType.Adnt;
    component.loggedUser.permissions = [...component.loggedUser.permissions, viewAcdntPermission.viewAdnt];
    component['initializeAgentGroupFilterDropdown']();

    expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
  });

  it('should initializeStatusFilterDropdown', () => {
    spyOn<any>(component, 'filterStatusByTypePermission').and.returnValue(true);
    component['initializeStatusFilterDropdown']();

    expect(component.statusList).toEqual([
      { value: 'DELETED', label: 'ADM_ACM.status.DELETED' },
      { value: 'SUPERVISION_DELETED', label: 'ADM_ACM.status.SUPERVISION_DELETED' }
    ]);
  });

  it('should initializeRtdnTypeFilterDropdown', fakeAsync(() => {
    const expectResult = [
      { value: 'ISSUE', label: 'ADM_ACM.acdmFor.ISSUE' },
      { value: 'REFUND', label: 'ADM_ACM.acdmFor.REFUND' },
      { value: 'EXCHANGE', label: 'ADM_ACM.acdmFor.EXCHANGE' },
      { value: 'EMD', label: 'ADM_ACM.acdmFor.EMD' }
    ];
    admAcmConfigServiceSpy.getAdmaFor.and.returnValue(of(expectResult));
    let result;

    component['initializeRtdnTypeFilterDropdown']();
    tick();
    component.rtdnTypeList$.subscribe(data => (result = data));

    expect(result).toEqual(expectResult);
  }));

  it('should updatePeriodFilterOptions', fakeAsync(() => {
    const method = periodServiceSpy.getByBsp.and.callThrough();
    component['bspSelectedIds'] = [2];
    component['updatePeriodFilterOptions']();
    tick();

    expect(method).toHaveBeenCalledWith(2);
  }));

  it('should initializeInternalCommentFilterDropdown', () => {
    component['initializeInternalCommentFilterDropdown']();

    expect(component.hasInternalCommentList).toEqual([
      { label: `ADM_ACM.yesNoBooleanValue.true`, value: true },
      { label: `ADM_ACM.yesNoBooleanValue.false`, value: false }
    ]);
  });

  it('should filterStatusByTypePermission with Adm type', () => {
    const acdmIsOfTypeSpy = spyOn<any>(component, 'acdmIsOfType').and.callThrough();
    const result = component['filterStatusByTypePermission'](AdmAcmStatus.supervisionDeleted);

    expect(result).toBeFalsy();
    expect(acdmIsOfTypeSpy).toHaveBeenCalledWith(MasterDataType.Adm);
  });

  it('should filterStatusByTypePermission with Acm type', () => {
    const acdmIsOfTypeSpy = spyOn<any>(component, 'acdmIsOfType').and.callThrough();
    component.admAcmType = MasterDataType.Acm;
    const result = component['filterStatusByTypePermission'](AdmAcmStatus.supervisionDeleted);

    expect(result).toBeFalsy();
    expect(acdmIsOfTypeSpy).toHaveBeenCalledWith(MasterDataType.Acm);
  });

  it('should filterStatusByTypePermission not set result property', () => {
    const acdmIsOfTypeSpy = spyOn<any>(component, 'acdmIsOfType').and.callThrough();
    const result = component['filterStatusByTypePermission'](AdmAcmStatus.approved);

    expect(result).toBeTruthy();
    expect(acdmIsOfTypeSpy).not.toHaveBeenCalled();
  });

  it('should initializeColumns call setDocumentNumberColumn when hasReadInternalCommentPermission is true', fakeAsync(() => {
    const setDocumentNumberColumnSpy = spyOn<any>(component, 'setDocumentNumberColumn').and.callThrough();
    const setBspColumnSpy = spyOn<any>(component, 'setBspColumn').and.callThrough();
    component.hasEditInternalCommentPermission = true;

    component['initializeColumns']();
    tick();

    expect(setDocumentNumberColumnSpy).toHaveBeenCalled();
    expect(setBspColumnSpy).toHaveBeenCalled();
    expect(translationServiceSpy.translate).toHaveBeenCalled();
  }));

  it('should set correct columns as AIRLINE', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'agent.bsp.isoCountryCode',
        name: 'ADM_ACM.query.tableColumns.isoc',
        draggable: false,
        resizeable: true,
        sortable: true,
        width: 55
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: true,
        width: 140,
        image: jasmine.anything()
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.agentCode`,
        prop: 'agent.iataCode',
        draggable: false,
        resizeable: true,
        width: 90
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.status`,
        prop: 'acdmStatus',
        draggable: false,
        badgeInfo: null,
        cellTemplate: jasmine.anything(),
        pipe: jasmine.anything(),
        width: 160
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: true,
        width: 70
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.issueDate`,
        prop: 'dateOfIssue',
        draggable: false,
        resizeable: true,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 70
      })
    ]);

    component['hasReadInternalCommentPermission'] = true;
    const setDocumentSpy = spyOn<any>(component, 'setDocumentNumberColumn').and.callThrough();

    component['initializeColumns']();
    tick();
    expect(component.columns).toEqual(expectedColumns);
    expect(setDocumentSpy).toHaveBeenCalled();
  }));

  it('should set correct columns as AGENT', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'agent.bsp.isoCountryCode',
        name: 'ADM_ACM.query.tableColumns.isoc',
        draggable: false,
        resizeable: true,
        sortable: true,
        width: 55
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: true,
        width: 140,
        image: jasmine.anything()
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.airlCode`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true,
        width: 90
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.status`,
        prop: 'acdmStatus',
        draggable: false,
        badgeInfo: null,
        cellTemplate: jasmine.anything(),
        pipe: jasmine.anything(),
        width: 160
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: true,
        width: 70
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.issueDate`,
        prop: 'dateOfIssue',
        draggable: false,
        resizeable: true,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        name: `ADM_ACM.query.tableColumns.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 70
      })
    ]);
    component.userType = MasterDataType.Agent;
    component['initializeColumns']();
    tick();
    expect(component.columns).toEqual(expectedColumns);
  }));

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should onRowToggle ', () => {
    const document = {
      id: '999999',
      transactionCode: TransactionCode['acm'],
      acdmStatus: AdmAcmStatus.deleted
    } as AdmAcmDocumentSummary;

    const onSelectedItemChangedSpy = spyOn<any>(component, 'onSelectedItemChanged').and.callThrough();

    const event = { type: 'row', value: document };
    component.onRowToggle(event);

    expect(onSelectedItemChangedSpy).toHaveBeenCalled();
  });

  it('should onRowToggle not call onSelectedItemChangedSpy method', () => {
    const onSelectedItemChangedSpy = spyOn<any>(component, 'onSelectedItemChanged').and.callThrough();
    component.onRowToggle({});

    expect(onSelectedItemChangedSpy).not.toHaveBeenCalled();
  });

  it('should onRowToggle not call onSelectedItemChangedSpy method when type is missing', () => {
    const document = {
      id: '999999',
      transactionCode: TransactionCode['acm'],
      acdmStatus: AdmAcmStatus.deleted
    } as AdmAcmDocumentSummary;

    const onSelectedItemChangedSpy = spyOn<any>(component, 'onSelectedItemChanged').and.callThrough();

    const event = { value: document };
    component.onRowToggle(event);

    expect(onSelectedItemChangedSpy).not.toHaveBeenCalled();
  });

  it('should open refund internal comment dialog on image click', () => {
    const event = { event: null, row: { id: 1 } as AdmAcmIssueBE, column: null };

    component.onImageClick(event);

    expect(admAcmDialogServiceSpy.openInternalCommentDialog).toHaveBeenCalledWith({ id: 1 }, MasterDataType.Adm);
  });

  it('should onGetActionList', () => {
    const acdm = { id: 1, markStatus: 'test' } as AdmAcmIssueBE & { markStatus: string };

    component.onGetActionList(acdm);
    expect(admAcmServiceSpy.getAcdmActionList).toHaveBeenCalledWith(1);
  });

  it('should initialize list actions onGetActionList called', fakeAsync(() => {
    let actionList: { action: GridTableActionType; disabled?: boolean; group?: string }[];
    const mockActions = [{ action: GridTableActionType.InternalComment, disabled: false }];

    admAcmServiceSpy.getAcdmActionList.and.returnValue(of(mockActions));

    component.onGetActionList({ id: 1, markStatus: 'test' } as AdmAcmIssueBE & { markStatus: string });
    component.listActions$.subscribe(actions => (actionList = actions));
    tick();

    expect(actionList).toEqual([{ action: GridTableActionType.InternalComment, disabled: false }]);
  }));

  it('should onActionClick', () => {
    // const spy = spyOn<any>(component['admAcmDialogService'], 'openActivityHistoryDialog').and.callThrough();
    admAcmConfigServiceSpy.getActionCommand.and.returnValue(admAcmDialogServiceSpy.openActivityHistoryDialog);
    component.onActionClick({
      action: { actionType: GridTableActionType.Activity },
      row: { id: 1, transactionCode: 'TT' } as AdmAcmIssueBE
    });

    expect(admAcmConfigServiceSpy.getActionCommand).toHaveBeenCalled();
    expect(admAcmDialogServiceSpy.openActivityHistoryDialog).toHaveBeenCalled();
  });

  it('should isAdmAcmRequest return true for ADMQ', () => {
    component.admAcmType = MasterDataType.Admq;
    const result = component['isAdmAcmRequest']();

    expect(result).toBeTruthy();
  });

  it('should isAdmAcmRequest return true for ACMQ', () => {
    component.admAcmType = MasterDataType.Acmq;

    const result = component['isAdmAcmRequest']();

    expect(result).toBeTruthy();
  });

  it('should isAcdmRequest return false for other type of ACDM', () => {
    const result = component['isAdmAcmRequest']();

    expect(result).toBeFalsy();
  });

  it('should setInitialFilterValues', () => {
    component['setInitialFilterValues']();

    expect(component.searchform.value).toEqual(SEARCH_FORM_DEFAULT_VALUE);
  });

  it('should initializeFilterVisibility', () => {
    component['hasReadInternalCommentPermission'] = true;
    component['initializeFilterVisibility']();

    expect(component['isInternalCommentVisible']).toBeTruthy();
  });
});
