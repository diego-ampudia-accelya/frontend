import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AdmAcmListComponent } from './adm-acm-list.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { MenuBuilder } from '~app/master-data/configuration';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { createAgentUser } from '~app/shared/mocks/agent-user';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { User, UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('AdmAcmListComponent', () => {
  let fixture: ComponentFixture<AdmAcmListComponent>;
  let component: AdmAcmListComponent;

  const menuBuilderMock = jasmine.createSpyObj<MenuBuilder>('MenuBuilder', {
    buildMenuItemsFrom: [
      {
        route: '/acdms/adm/query/deleted',
        title: 'ADM_ACM.query.tabs.deletedTitle',
        isAccessible: true
      }
    ]
  });

  const dialogServiceSpy = createSpyObject(DialogService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);

  const activatedRouteStub = {
    snapshot: {
      data: {
        tab: ROUTES.ADM_QUERY
      }
    }
  };

  const initialState = {
    auth: {}
  };

  let routerMock;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/deleted', './deleted')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './deleted'
    };

    TestBed.configureTestingModule({
      declarations: [AdmAcmListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        L10nTranslationService,
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: MenuBuilder, useValue: menuBuilderMock },
        provideMockStore({ selectors: [{ selector: fromAuth.isLoggedIn, value: true }], initialState }),
        { provide: Router, useValue: routerMock },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should be allowed issue ACDM according to user permissions', () => {
    component.admAcmType = MasterDataType.Adm;
    const users: User[] = [
      {
        ...createIataUser(),
        permissions: ['cAdm', 'cAcm']
      } as User,
      {
        ...createAgentUser(),
        permissions: []
      } as User,
      {
        ...createAirlineUser(),
        permissions: ['cAdm', 'cAcm']
      } as User
    ];

    users.forEach(user => {
      component.loggedUser = user;
      expect(component['canUserIssueAcdm']()).toBe(UserType.AGENT !== component.loggedUser.userType);
    });
  });
});
