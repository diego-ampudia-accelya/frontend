import { DatePipe } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { capitalize } from 'lodash';
import { MenuItem } from 'primeng/api';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, first, takeUntil } from 'rxjs/operators';

import { AcdmActionType, SendToDpcType, TransactionCode } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  AgentViewModel,
  CurrencyViewModel,
  SecondaryHeaderActionsViewModel,
  StatusHeaderViewModel,
  StatusViewModel
} from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmActionEmitterType, AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { roundCalculationValue } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { AppState } from '~app/reducers';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { WatcherDocType } from '~app/shared/models/watcher.model';
import { DocumentPdfExporter, NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-adm-acm-header-status',
  templateUrl: './adm-acm-header-status.component.html',
  styleUrls: ['./adm-acm-header-status.component.scss'],
  providers: [DatePipe]
})
export class AdmAcmHeaderStatusComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('actionsMenuContainer') actionsMenuContainer: ElementRef;
  @ViewChild('statusHeader') statusHeader: ElementRef;

  @Output() issueClick = new EventEmitter<AcdmActionType>();
  @Output() downloadClick = new EventEmitter();
  @Output() heightChange = new EventEmitter<number>();

  @Input() admAcmService: AdmAcmService;
  @Input() formData: AdmAcmDataService;
  @Input() formConfig: AdmAcmConfigService;
  @Input() currency: CurrencyViewModel;
  @Input() accordion: AccordionComponent;

  @Input() form: FormGroup;

  public btnDesign = ButtonDesign;

  public collapseAll = true;

  public statusHeaderFields: Array<keyof StatusHeaderViewModel>;

  public headerOptionActions: MenuItem[] = [];
  public showIssueBtn: boolean;
  public showIssuePendingBtn: boolean;

  public headerSecondaryActions: Array<SecondaryHeaderActionsViewModel>;
  public isSecondaryActionsMenuDisabled: boolean;
  public isPdfLoading: boolean;
  public showHeaderSecondaryActions: boolean;

  public badgeInfoType: BadgeInfoType;
  public badgeInfoLeyend: string;
  public badgeInfoField: keyof StatusHeaderViewModel;
  public amountField: keyof StatusHeaderViewModel = 'amount';
  public docNumberField: keyof StatusHeaderViewModel = 'ticketDocumentNumber';

  public get transactionCode(): TransactionCode {
    return this.formConfig.getTransactionCode();
  }
  public get admAcmType(): MasterDataType {
    return this.formConfig.admAcmType;
  }

  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)

  public watching: boolean;

  private _isHeaderOverlapped = false;
  public get isHeaderOverlapped(): boolean {
    return this._isHeaderOverlapped;
  }

  private subscriptions = [];
  private formFactory: FormUtil;
  private destroy$ = new Subject();
  private loggedUserType: UserType;

  private get loggedUserType$(): Observable<UserType> {
    return this.store.pipe(select(getUserType), first());
  }

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private datePipe: DatePipe,
    private cd: ChangeDetectorRef,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private notificationHeaderService: NotificationsHeaderService,
    private store: Store<AppState>,
    @Optional() public pdfExporter: DocumentPdfExporter
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.initializeLoggedUserType();
    this.setConfig();
    this.initializeIssueBtn();

    if (!this.form) {
      this.buildForm();
    }

    this.initializeChangesListener();
    this.isSecondaryActionsMenuDisabled = true;

    if (this.formConfig.isReadonlyMode) {
      this.initializeWatchingFlagAndActions();
    }
  }

  public ngAfterViewInit(): void {
    this.heightChange.emit(this.statusHeader.nativeElement.offsetHeight);
  }

  @HostListener('document:scroll')
  public onScroll(): void {
    this.updateHeaderOverlapState();
  }

  private initializeLoggedUserType(): void {
    this.loggedUserType$.pipe(takeUntil(this.destroy$)).subscribe(userType => {
      this.loggedUserType = userType;
    });
  }

  private setConfig() {
    this.statusHeaderFields = this.formConfig.getStatusHeaderFields();
    this.isAcdmd = this.admAcmType === MasterDataType.Admd || this.admAcmType === MasterDataType.Acmd;
  }

  private initializeIssueBtn() {
    if (this.formConfig.issuingPendingSupervision) {
      this.showIssuePendingBtn = true;
    } else {
      this.showIssueBtn = this.formConfig.isCreateMode || this.formConfig.isEditTicketMode;
    }
  }

  private buildForm() {
    const statusForm = this.formFactory.createGroup<StatusViewModel>({
      status: '',
      sentToDpc: ''
    });
    this.form = this.formFactory.createGroup<StatusHeaderViewModel>({
      ticketDocumentNumber: '',
      status: statusForm,
      bspName: '',
      agentCode: '',
      acdmaFor: '',
      issueDate: new Date(),
      amount: ''
    });

    this.formData.setSectionFormGroup('statusHeader', this.form);
  }

  public initializeHeaderOptionActions(updatedStatus?: StatusViewModel) {
    const itemRetrieved: AdmAcmIssueBE = this.activatedRoute.snapshot.data.item;
    if (updatedStatus) {
      itemRetrieved.acdmStatus = updatedStatus.status;
    }

    if (itemRetrieved) {
      this.admAcmService.getAcdmActionList(itemRetrieved.id).subscribe(actions => {
        this.headerOptionActions = this.formConfig.formatHeaderBtnOptions(actions, itemRetrieved);
        this.cd.detectChanges();
      });
    }
  }

  public toggleHeaderSecondaryActionsMenu(event: Event): void {
    if (event) {
      event.stopPropagation();
      this.showHeaderSecondaryActions = !this.showHeaderSecondaryActions;
    } else {
      this.showHeaderSecondaryActions = false;
    }
  }

  private initializeWatchingFlagAndActions(): void {
    const itemRetrieved: AdmAcmIssueBE = this.activatedRoute.snapshot.data.item;
    const bspId = (itemRetrieved.agent as Agent).bsp.id;

    this.notificationHeaderService.getWatcher(bspId, itemRetrieved.id, WatcherDocType.ACDM).subscribe(watcher => {
      this.watching = watcher.watching;
      this.initializeSecondaryHeaderActions();
    });
  }

  private initializeSecondaryHeaderActions(): void {
    const watcherAction = this.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;
    const watcherLabel = this.watching ? 'stopWatching' : 'startWatching';
    const isIataUserType = UserType?.IATA === this.loggedUserType;

    this.headerSecondaryActions = [
      {
        label: this.translationService.translate('ADM_ACM.statusHeader.secondaryActions.download'),
        action: GridTableActionType.Download
      }
    ];

    if (!isIataUserType) {
      this.headerSecondaryActions.push({
        label: this.translationService.translate(watcherLabel),
        action: watcherAction
      });
    }

    this.isSecondaryActionsMenuDisabled = false;
    this.showHeaderSecondaryActions = false;
  }

  public onActionClick(secondaryAction: SecondaryHeaderActionsViewModel) {
    const itemRetrieved: AdmAcmIssueBE = this.activatedRoute.snapshot.data.item;

    switch (secondaryAction.action) {
      case GridTableActionType.Download:
        this.downloadPdf();
        break;
      case GridTableActionType.StartWatching:
      case GridTableActionType.StopWatching:
        this.admAcmService
          .updateWatchers([itemRetrieved], secondaryAction.action === GridTableActionType.StartWatching)
          .subscribe(([{ watching }]) => this.onUpdateWatcherSuccess(watching));
        break;
    }

    this.showHeaderSecondaryActions = false;
  }

  @HostListener('document:click', ['$event'])
  public onDocumentClick(event: MouseEvent): void {
    const target = event.target;
    if (
      this.actionsMenuContainer &&
      this.actionsMenuContainer.nativeElement &&
      !this.actionsMenuContainer.nativeElement.contains(target)
    ) {
      this.toggleHeaderSecondaryActionsMenu(null);
    }
  }

  public async downloadPdf() {
    this.isPdfLoading = true;

    this.expandItems();
    const pdf = await this.pdfExporter.export();
    const document = this.activatedRoute.snapshot.data.item;

    const fileName = this.createFileNameByMasterDataType(document.ticketDocumentNumber);

    pdf.save(`${fileName}.pdf`);
    this.isPdfLoading = false;
    this.cd.detectChanges();
  }

  private onUpdateWatcherSuccess(watching: boolean): void {
    this.watching = watching;
    this.initializeSecondaryHeaderActions();
    this.notifyUpdateWatcherSuccess(watching);
    this.cd.markForCheck();
  }

  private notifyUpdateWatcherSuccess(watching: boolean): void {
    const translationKey = watching ? 'startWatchingDocument' : 'stopWatchingDocument';
    this.notificationService.showSuccess(this.translationService.translate(translationKey));
  }

  private createFileNameByMasterDataType(ticketDocumentNumber): string {
    let fileName = '';

    switch (this.admAcmType) {
      case MasterDataType.Acmq:
        fileName = `acmq_${ticketDocumentNumber}`;
        break;
      case MasterDataType.Admq:
        fileName = `admq_${ticketDocumentNumber}`;
        break;
      default:
        fileName = `${this.admAcmType}_${ticketDocumentNumber}`;
    }

    return fileName;
  }

  private expandItems(): void {
    this.accordion.expandItems();
    this.downloadClick.emit();
  }

  private initializeChangesListener() {
    this.subscriptions.push(
      this.formData.getSubject(AdmAcmActionEmitterType.bsp).subscribe(bsp => {
        this.updateBasicBspFields(bsp);
      }),

      this.formData.getSubject(AdmAcmActionEmitterType.agent).subscribe(agent => {
        this.updateBasicAgentFields(agent);
      }),

      this.formData.getSubject(AdmAcmActionEmitterType.totalAmount).subscribe(totalAmount => {
        this.updateAmountFields(totalAmount);
      }),

      this.formData.getSubject(AdmAcmActionEmitterType.status).subscribe(status => {
        this.updateStatusFields(status);
      }),

      this.formData
        .getForm()
        .statusChanges.pipe(distinctUntilChanged())
        .subscribe(() => this.cd.markForCheck())
    );

    if (!this.isAcdmd) {
      this.subscriptions.push(
        this.formData.getSubject(AdmAcmActionEmitterType.acdmaFor).subscribe(acdmaFor => {
          this.updateBasicAdmaFields(acdmaFor);
        })
      );
    }
  }

  private updateBasicBspFields(bsp: BspDto) {
    if (bsp) {
      this.form.patchValue({ bspName: `${capitalize(bsp.name)} (${bsp.isoCountryCode})` }, { emitEvent: false });

      if (this.formConfig.isCreateMode) {
        this.admAcmService.getBspTime(bsp.id).subscribe(bspTime => {
          FormUtil.get<StatusHeaderViewModel>(this.form, 'issueDate').setValue(bspTime.localTime, { emitEvent: false });
        });
      }
    }
  }

  private updateBasicAgentFields(agent: AgentViewModel) {
    if (agent) {
      this.form.patchValue({ agentCode: agent.iataCode }, { emitEvent: false });
    }
  }

  private updateBasicAdmaFields(acdmaFor) {
    this.form.patchValue({ acdmaFor: acdmaFor ? acdmaFor : '' }, { emitEvent: false });
  }

  private updateAmountFields(totalAmount) {
    this.form.patchValue({ amount: totalAmount }, { emitEvent: false });
  }

  private updateStatusFields(status: StatusViewModel) {
    this.form.patchValue({ status: status ? status : {} }, { emitEvent: false });
    this.initializeBadgeInfo();
    this.initializeHeaderOptionActions(status);
  }

  public onIssueClick() {
    this.issueClick.emit(AcdmActionType.issue);
  }

  public onIssuePendingClick() {
    this.issueClick.emit(AcdmActionType.issuePendingSupervision);
  }

  public isFieldInactive(field: keyof StatusHeaderViewModel): boolean {
    const fieldValue = this.form.get(field).value;

    return !fieldValue || (field === 'amount' && this.numberStringIsZero(fieldValue));
  }

  private numberStringIsZero(number: string): boolean {
    return parseFloat(number) === 0;
  }

  private initializeBadgeInfo() {
    this.badgeInfoField = 'status';
    const sentToDpc = this.getDpcStatusValue();

    this.badgeInfoLeyend = sentToDpc
      ? this.translationService.translate(`ADM_ACM.dpcBadgeInfo.leyend.${sentToDpc.toUpperCase()}`, {
          type: this.translationService.translate(`ADM_ACM.type.${this.admAcmType}`)
        })
      : '';

    switch (sentToDpc) {
      case SendToDpcType.pending:
        this.badgeInfoType = BadgeInfoType.info;
        break;
      case SendToDpcType.sent:
        this.badgeInfoType = BadgeInfoType.success;
        break;
      default:
        this.badgeInfoType = BadgeInfoType.regular;
        break;
    }
  }

  private getDpcStatusValue(): SendToDpcType {
    const statusValue: StatusViewModel = FormUtil.get<StatusHeaderViewModel>(this.form, 'status').value;

    return statusValue.sentToDpc;
  }

  public getFormattedField(field: keyof StatusHeaderViewModel): string {
    let fieldValue = this.form.get(field).value;

    switch (field) {
      case 'issueDate':
        if (fieldValue) {
          fieldValue = this.datePipe.transform(fieldValue, 'dd/MM/yyyy');
        }
        break;
      case 'bspName':
        fieldValue = fieldValue
          ? fieldValue.toUpperCase()
          : this.translationService.translate('ADM_ACM.statusHeader.notDefinedYet');
        break;
      case 'agentCode':
        fieldValue = fieldValue ? fieldValue : this.translationService.translate('ADM_ACM.statusHeader.notDefinedYet');
        break;
      case 'acdmaFor':
        fieldValue = fieldValue
          ? this.translationService.translate(`ADM_ACM.acdmFor.${fieldValue}`)
          : this.translationService.translate('ADM_ACM.statusHeader.notDefinedYet');
        break;
      case 'ticketDocumentNumber':
        fieldValue = fieldValue ? fieldValue : this.translationService.translate('ADM_ACM.statusHeader.notIssuedYet');
        break;
      case 'status':
        fieldValue = fieldValue.status
          ? this.translationService.translate(`ADM_ACM.status.${fieldValue.status}`)
          : this.translationService.translate('ADM_ACM.statusHeader.notIssuedYet');
        break;
      case 'amount':
        if (this.currency) {
          fieldValue = roundCalculationValue(fieldValue, this.currency.decimals);
        }
        break;
    }

    return fieldValue;
  }

  public showDropdownBtn(): boolean {
    return this.headerOptionActions && this.headerOptionActions.length > 0;
  }

  private updateHeaderOverlapState(): void {
    const headerTop: number = this.statusHeader.nativeElement.getBoundingClientRect().top;

    // Status header is not in the viewport and is therefore overlapped if it's top position is 0
    this._isHeaderOverlapped = !headerTop;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
    this.destroy$.next();
  }
}
