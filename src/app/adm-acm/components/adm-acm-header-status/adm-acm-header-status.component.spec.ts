import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { initialState, issue } from '../adm-acm/adm-acm-test.config';
import { AdmAcmHeaderStatusComponent } from './adm-acm-header-status.component';
import { AcdmActionType, SendToDpcType } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmActionEmitterType } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { createAdmAcmService } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { BspTimeModel } from '~app/refund/models/refund-be-aux.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { AccessType } from '~app/shared/enums';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService, TabService } from '~app/shared/services';

const activatedRouteStub = {
  snapshot: {
    data: {
      item: {}
    }
  }
};

describe('AdmAcmHeaderStatusComponent', () => {
  let component: AdmAcmHeaderStatusComponent;
  let fixture: ComponentFixture<AdmAcmHeaderStatusComponent>;
  let dataService: AdmAcmDataService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmHeaderStatusComponent],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        L10nTranslationModule.forRoot(l10nConfig),
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        provideMockStore({ initialState }),
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        AdmAcmDialogService,
        PermissionsService,
        L10nTranslationService,
        HttpClient,
        DialogService,
        FormBuilder,
        HttpTestingController,
        TabService,
        mockProvider(NotificationService),
        mockProvider(NotificationsHeaderService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmHeaderStatusComponent);
    component = fixture.componentInstance;
    component.admAcmService = createAdmAcmService(TestBed.inject(HttpClient), TestBed.inject(ActivatedRoute).snapshot);
    component.formData = new AdmAcmDataService(TestBed.inject(FormBuilder));
    component.formConfig = createAdmAcmConfigService(
      component.formData,
      TestBed.inject(AdmAcmDialogService),
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Router),
      TestBed.inject(Store),
      TestBed.inject(TabService)
    );
    dataService = component.formData;

    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update fields with agent info', () => {
    dataService.getSubject(AdmAcmActionEmitterType.agent).next(issue);
    const agentCode = component.form.get('agentCode');
    expect(agentCode.value).toBe(issue.iataCode);
  });

  it('should update fields with bsp info', () => {
    const mockBsp: BspDto = { id: 1, isoCountryCode: '1234', name: 'Name' };
    dataService.getSubject(AdmAcmActionEmitterType.bsp).next(mockBsp);

    const bspName = component.form.get('bspName');
    expect(bspName.value).toBe(`${mockBsp.name} (${mockBsp.isoCountryCode})`);
  });

  it('should update fields with admaFor info', () => {
    dataService.getSubject(AdmAcmActionEmitterType.acdmaFor).next('ISSUE');
    const admaFor = component.form.get('acdmaFor');
    expect(admaFor.value).toBe('ISSUE');
  });

  it('should emit issue ADM action on click', () => {
    spyOn(component.issueClick, 'emit').and.callThrough();
    component.onIssueClick();
    expect(component.issueClick.emit).toHaveBeenCalledWith(AcdmActionType.issue);
  });

  it('should emit issue ADM pending authorization action on click', () => {
    spyOn(component.issueClick, 'emit').and.callThrough();
    component.onIssuePendingClick();
    expect(component.issueClick.emit).toHaveBeenCalledWith(AcdmActionType.issuePendingSupervision);
  });

  describe('initializeBadgeInfo', () => {
    it('should set correct badgeInfoType Info and BadgeInfoLeyend Pending when initializeBadgeInfo is called', () => {
      const expectedBadgeInfoLeyend = 'ADM_ACM.dpcBadgeInfo.leyend.PENDING';

      const status = {
        sentToDpc: SendToDpcType.pending,
        status: 'REJECTED'
      };
      component.form.get('status').setValue(status);

      component['initializeBadgeInfo']();

      expect(component.badgeInfoLeyend).toEqual(expectedBadgeInfoLeyend);
      expect(component.badgeInfoType).toEqual(BadgeInfoType.info);
    });

    it('should set correct badgeInfoType success when initializeBadgeInfo is called', () => {
      const status = {
        sentToDpc: SendToDpcType.sent,
        status: 'REJECTED'
      };
      component.form.get('status').setValue(status);

      component['initializeBadgeInfo']();

      expect(component.badgeInfoType).toEqual(BadgeInfoType.success);
    });
  });

  it('should update variables when initializeSecondaryHeaderActions is called', () => {
    spyOn<any>(component, 'initializeSecondaryHeaderActions').and.callThrough();

    component['initializeSecondaryHeaderActions']();

    expect(component.headerSecondaryActions.length).toBeGreaterThan(0);
  });

  describe('toggleHeaderSecondaryActionsMenu', () => {
    it('should call correct functions when toggleHeaderSecondaryActionsMenu is called with event', () => {
      const event = new Event('onClick');

      spyOn(component, 'toggleHeaderSecondaryActionsMenu').and.callThrough();
      spyOn(event, 'stopPropagation').and.callThrough();

      component.toggleHeaderSecondaryActionsMenu(event);

      expect(event.stopPropagation).toHaveBeenCalled();
    });

    it('should set showHeaderSecondaryActions to false when toggleHeaderSecondaryActionsMenu is called without event', () => {
      spyOn(component, 'toggleHeaderSecondaryActionsMenu').and.callThrough();

      component.toggleHeaderSecondaryActionsMenu(undefined);

      expect(component.showHeaderSecondaryActions).toBeFalsy();
    });
  });

  // TODO Test fails in after all
  xit('should call downloadPdf when onActionClick is called', () => {
    spyOn(component, 'downloadPdf').and.callThrough();

    component.onActionClick(undefined);

    expect(component.downloadPdf).toHaveBeenCalled();
    expect(component.showHeaderSecondaryActions).toBeFalsy();
  });

  it('should set bsp value when updateBasicBspFields is called', () => {
    const bsp: Bsp = {
      id: 777,
      name: 'bspName',
      isoCountryCode: 'ES',
      effectiveFrom: '26/05/2022'
    };

    const bspTime: BspTimeModel = {
      id: 222,
      localTime: new Date('26/05/2022')
    };

    component.formConfig.accessType = AccessType.create;

    spyOn(component.admAcmService, 'getBspTime').and.returnValue(of(bspTime));

    component['updateBasicBspFields'](bsp);

    const bspControlValue = component.form.get('bspName').value;

    expect(bspControlValue).toEqual('Bspname (ES)');
    expect(component.admAcmService.getBspTime).toHaveBeenCalled();
  });

  it('should call specific functions if AccessType is read', fakeAsync(() => {
    component.formConfig.accessType = AccessType.read;

    const actions = [
      {
        action: GridTableActionType.Activity
      }
    ];
    spyOn(component.admAcmService, 'getAcdmActionList').and.returnValue(of(actions));

    spyOn<any>(component, 'initializeChangesListener');
    spyOn<any>(component, 'initializeWatchingFlagAndActions');

    component.ngOnInit();

    tick();

    expect(component['initializeChangesListener']).toHaveBeenCalled();
    expect(component['initializeWatchingFlagAndActions']).toHaveBeenCalled();
  }));

  describe('createFileNameByMasterDataType', () => {
    it('should createFileNameByMasterDataType for Acmq', () => {
      component.formConfig.admAcmType = MasterDataType.Acmq;

      expect(component['createFileNameByMasterDataType'](123)).toEqual('acmq_123');
    });

    it('should createFileNameByMasterDataType for Admq', () => {
      component.formConfig.admAcmType = MasterDataType.Admq;

      expect(component['createFileNameByMasterDataType'](123)).toEqual('admq_123');
    });

    it('should createFileNameByMasterDataType for other', () => {
      component.formConfig.admAcmType = MasterDataType.Agent;

      expect(component['createFileNameByMasterDataType'](123)).toEqual('Agent_123');
    });
  });

  it('should return true when numberStringIsZero is called and number is 0', () => {
    component['numberStringIsZero']('0');

    expect(component['numberStringIsZero']).toBeTruthy();
  });

  it('should format an amount field correctly', () => {
    component.currency = { code: 'EUR', decimals: 2 } as any;
    component.form.get('amount').setValue('1');

    expect(component.getFormattedField('amount')).toBe('1.00');
  });

  it('should updateHeaderOverlapState on scroll', () => {
    const updateHeaderOverlapStateSpy = spyOn<any>(component, 'updateHeaderOverlapState').and.callThrough();

    component.onScroll();

    expect(updateHeaderOverlapStateSpy).toHaveBeenCalled();
  });

  it('should emit height after view init', () => {
    spyOn(component.heightChange, 'emit');
    component.ngAfterViewInit();

    expect(component.heightChange.emit).toHaveBeenCalledWith(component.statusHeader.nativeElement.offsetHeight);
  });

  it('should initializeIssueBtn after view init', fakeAsync(() => {
    component.formConfig.issuingPendingSupervision = true;

    component['initializeIssueBtn']();
    tick();

    expect(component.showIssuePendingBtn).toBeTruthy();
  }));
});
