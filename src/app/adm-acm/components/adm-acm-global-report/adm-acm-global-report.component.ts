import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';

import { AdmGlobalRequestDialogComponent } from './adm-global-request-dialog/adm-global-request-dialog.component';
import { GlobalAdmReportFilter } from './model/global-adm-report-filter.model';
import { GlobalAdmReport } from './model/global-adm-report.model';
import { GlobalAdmReportFilterFormatter } from './services/global-adm-report-filter-formatter';
import { GlobalAdmReportService } from './services/global-adm-report.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-adm-acm-global-report',
  templateUrl: './adm-acm-global-report.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: GlobalAdmReportService }
  ]
})
export class AdmAcmGlobalReportComponent implements OnInit {
  public title = this.translationService.translate('MENU.ACDMS.QUERY.globalADMReport.title');
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public bspList$: Observable<DropdownOption<BspDto>[]>;
  public viewCalendarFormat = 'month';
  public customLabels = { create: 'MENU.ACDMS.QUERY.globalADMReport.buttonRequestReport.label' };

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private dialogTitle = this.translationService.translate('MENU.ACDMS.QUERY.globalADMReport.dialog.title');

  constructor(
    public displayFormatter: GlobalAdmReportFilterFormatter,
    public dataSource: QueryableDataSource<GlobalAdmReport>,

    private bspsDictionaryService: BspsDictionaryService,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private queryStorage: DefaultQueryStorage,
    private dialogService: DialogService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.populateFilterDropdowns();
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.loadData(this.storedQuery);
    this.initColumns();
  }

  public loadData(query: DataQuery<GlobalAdmReportFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public openGlobalAdmReport(): any {
    const dialogConfig: DialogConfig = {
      data: {
        title: this.dialogTitle,
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Request }]
      }
    };

    return this.dialogService.open(AdmGlobalRequestDialogComponent, dialogConfig);
  }

  private populateFilterDropdowns(): void {
    this.bspList$ = this.bspsDictionaryService.getAllBspDropdownOptions();
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<GlobalAdmReportFilter>({
      filename: [],
      requestMonth: [],
      bspIsos: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'filename',
        name: 'MENU.ACDMS.QUERY.globalADMReport.columns.filename'
      },
      {
        prop: 'requestMonth',
        name: 'MENU.ACDMS.QUERY.globalADMReport.columns.requestMonth'
      },
      {
        prop: 'bspIsos',
        name: 'MENU.ACDMS.QUERY.globalADMReport.columns.bspIsos',
        sortable: false,
        pipe: { transform: bspIsos => bspIsos.map(bsp => bsp).join(' ') }
      }
    ];
  }
}
