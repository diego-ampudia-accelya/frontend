import { HttpClient } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslatePipe, L10nTranslationService } from 'angular-l10n';
import { MockPipe } from 'ng-mocks/dist/lib/mock-pipe/mock-pipe';
import { MessageService } from 'primeng/api';
import { of } from 'rxjs';

import { GlobalAdmReport } from '../model/global-adm-report.model';
import { GlobalAdmReportService } from '../services/global-adm-report.service';
import { AdmGlobalRequestDialogComponent } from './adm-global-request-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('AdmGlobalRequestDialogComponent', () => {
  let component: AdmGlobalRequestDialogComponent;
  let fixture: ComponentFixture<AdmGlobalRequestDialogComponent>;
  let bspsDictionaryServiceSpy: SpyObject<BspsDictionaryService>;
  let globalAdmReportServiceSpy: SpyObject<GlobalAdmReportService>;
  let httpClientSpy: SpyObject<HttpClient>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let notificationServiceSpy: SpyObject<NotificationService>;

  let requestButton: ModalAction;

  const globalAdmReportMock: GlobalAdmReport[] = [
    {
      filename: 'testing-file1',
      requestMonth: '2017-02',
      bspIsos: ['ES', 'MT']
    },
    {
      filename: 'testing-file2',
      requestMonth: '2018-02',
      bspIsos: ['ES']
    }
  ];

  beforeEach(waitForAsync(() => {
    requestButton = { type: FooterButton.Request, isDisabled: true };
    bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
    globalAdmReportServiceSpy = createSpyObject(GlobalAdmReportService);
    httpClientSpy = createSpyObject(HttpClient);
    dialogServiceSpy = createSpyObject(DialogService);
    notificationServiceSpy = createSpyObject(NotificationService);

    TestBed.configureTestingModule({
      providers: [
        FormBuilder,
        mockProvider(DialogConfig, {
          data: { buttons: [requestButton] }
        }),
        mockProvider(L10nTranslationService),
        mockProvider(ReactiveSubject, { asObservable: of() }),
        mockProvider(MessageService),
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: GlobalAdmReportService, useValue: globalAdmReportServiceSpy },
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: DialogService, useValue: dialogServiceSpy }
      ],
      declarations: [AdmGlobalRequestDialogComponent, MockPipe(L10nTranslatePipe)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmGlobalRequestDialogComponent);
    component = fixture.componentInstance;

    component.config.data.buttons = [
      {
        isDisabled: true,
        type: FooterButton.Request
      } as ModalAction
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should populateFilterDropdowns method to  get All Bsp Dropdown Options', () => {
    component['populateFilterDropdowns']();

    expect(bspsDictionaryServiceSpy.getAllBspDropdownOptions).toHaveBeenCalled();
  });

  it('should call initializeFormListeners when onInit hook is called', fakeAsync(() => {
    spyOn<any>(component, 'initializeFormListeners');

    component.ngOnInit();
    tick();

    expect(component['initializeFormListeners']).toHaveBeenCalled();
  }));

  it('should request global Adm Report File when requestGlobalAdmReport is called', fakeAsync(() => {
    const globalAdmReportDataMock: PagedData<GlobalAdmReport> = {
      records: globalAdmReportMock,
      total: globalAdmReportMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };
    dialogServiceSpy.close.and.returnValue(of(FooterButton.Request));
    globalAdmReportServiceSpy.requestGlobalAdmReport.and.returnValue(of(globalAdmReportDataMock));

    component.form.get('bspIsos').setValue(['ES', 'MT']);
    component['requestGlobalAdmReport']();
    tick();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
    expect(globalAdmReportServiceSpy.requestGlobalAdmReport).toHaveBeenCalled();
  }));
});
