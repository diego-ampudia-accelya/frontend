export interface GlobalAdmReportRequest {
  requestMonth: string;
  bspIsos: string[];
}
