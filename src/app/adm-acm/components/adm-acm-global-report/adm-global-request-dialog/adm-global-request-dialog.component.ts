import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, take, takeUntil } from 'rxjs/operators';

import { GlobalAdmReport } from '../model/global-adm-report.model';
import { GlobalAdmReportService } from '../services/global-adm-report.service';
import { GlobalAdmReportRequest } from './model/global-adm-report-request.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { toShotIsoYearMonth } from '~app/shared/helpers/datesHelper';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-adm-global-request-dialog',
  templateUrl: './adm-global-request-dialog.component.html'
})
export class AdmGlobalRequestDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public viewCalendarFormat = 'month';
  public form: FormGroup;
  public bspList$: Observable<DropdownOption<BspDto>[]>;

  private requestMonthControl: FormControl;
  private bspIsosControl: FormControl;
  private requestButton: ModalAction;
  private destroy$ = new Subject();
  private formFactory: FormUtil;
  private requestButtonClick$ = this.reactiveSubject.asObservable.pipe(
    take(1),
    filter(action => action?.clickedBtn === FooterButton.Request)
  );
  private successMessageKey = 'MENU.ACDMS.QUERY.globalADMReport.dialog.successMessage';

  constructor(
    public config: DialogConfig,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private bspsDictionaryService: BspsDictionaryService,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    private globalAdmReportService: GlobalAdmReportService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.populateFilterDropdowns();
    this.form = this.buildForm();
    this.initializeFormListeners();
    this.initializeButtons();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.requestButton) {
        this.requestButton.isDisabled = status !== 'VALID';
      }
    });
  }

  private initializeButtons(): void {
    this.requestButton = this.config.data.buttons.find(button => button.type === FooterButton.Request);
    this.requestButton.isDisabled = true;
  }

  private initializeFormListeners(): void {
    this.requestButtonClick$.pipe(switchMap(() => this.requestGlobalAdmReport())).subscribe(response => {
      const fileNameRes = response.records[0].filename;
      this.notificationService.showSuccess(
        this.translationService.translate(this.successMessageKey, { fileName: fileNameRes })
      );
    });
  }

  private requestGlobalAdmReport(): Observable<PagedData<GlobalAdmReport>> {
    this.dialogService.close();

    const requestValues: GlobalAdmReportRequest = {
      requestMonth: toShotIsoYearMonth(this.requestMonthControl.value),
      bspIsos: this.bspIsosControl.value.map((bsp: { isoCountryCode: string }) => bsp.isoCountryCode)
    };

    return this.globalAdmReportService.requestGlobalAdmReport(requestValues);
  }

  private populateFilterDropdowns(): void {
    this.bspList$ = this.bspsDictionaryService.getAllBspDropdownOptions();
  }

  private buildForm(): FormGroup {
    this.requestMonthControl = new FormControl(null, Validators.required);
    this.bspIsosControl = new FormControl(null, Validators.required);

    return this.formFactory.createGroup<GlobalAdmReportRequest>({
      requestMonth: this.requestMonthControl,
      bspIsos: this.bspIsosControl
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
