import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AdmAcmGlobalReportComponent } from './adm-acm-global-report.component';
import { GlobalAdmReportFilterFormatter } from './services/global-adm-report-filter-formatter';
import { GlobalAdmReportService } from './services/global-adm-report.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

describe('AdmAcmGlobalReportComponent', () => {
  let component: AdmAcmGlobalReportComponent;
  let fixture: ComponentFixture<AdmAcmGlobalReportComponent>;
  const GlobalAdmReportServiceSpy: SpyObject<GlobalAdmReportService> = createSpyObject(GlobalAdmReportService);
  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const DialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { globalAdmReportQuery: { ...ROUTES.GLOBAL_ADM_REPORTS_QUERY, id: 'globalAdmReportQuery' } },
        activeTabId: 'globalAdmReportQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmGlobalReportComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        GlobalAdmReportFilterFormatter,
        FormBuilder
      ]
    })
      .overrideComponent(AdmAcmGlobalReportComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: GlobalAdmReportService, useValue: GlobalAdmReportServiceSpy },
            { provide: DialogService, useValue: DialogServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmGlobalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');
    component.ngOnInit();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should call open method in dialog service', () => {
    component.openGlobalAdmReport();

    expect(DialogServiceSpy.open).toHaveBeenCalled();
  });
});
