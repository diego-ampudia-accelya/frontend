import { BspDto } from '~app/shared/models/bsp.model';

export interface GlobalAdmReportFilter {
  filename: string;
  requestMonth: Date;
  bspIsos: BspDto[];
}
