export interface GlobalAdmReport {
  filename: string;
  requestMonth: string;
  bspIsos: string[];
}
