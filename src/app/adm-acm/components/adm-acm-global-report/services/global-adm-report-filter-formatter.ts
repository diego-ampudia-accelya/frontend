import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { GlobalAdmReportFilter } from './../model/global-adm-report-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { toShotIsoYearMonth } from '~app/shared/helpers/datesHelper';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };
@Injectable()
export class GlobalAdmReportFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: GlobalAdmReportFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<GlobalAdmReportFilter>> = {
      filename: filename => `${this.translate('filename')} - ${filename}`,
      requestMonth: requestMonth => `${this.translate('month')} - ${toShotIsoYearMonth(requestMonth)}`,
      bspIsos: bspIsos => `${this.translate('bsps')} - ${bspIsos.map(bsp => bsp.isoCountryCode).join(', ')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.ACDMS.QUERY.globalADMReport.filter.label.${key}`);
  }
}
