import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { GlobalAdmReportRequest } from '../adm-global-request-dialog/model/global-adm-report-request.model';
import { GlobalAdmReportFilter } from '../model/global-adm-report-filter.model';
import { GlobalAdmReport } from '../model/global-adm-report.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { toShotIsoYearMonth } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class GlobalAdmReportService implements Queryable<GlobalAdmReport> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/global-adm-report`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<GlobalAdmReportFilter>): Observable<PagedData<GlobalAdmReport>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<GlobalAdmReport>>(this.baseUrl + requestQuery.getQueryString());
  }

  public requestGlobalAdmReport(payload: GlobalAdmReportRequest): Observable<PagedData<GlobalAdmReport>> {
    return this.http.post<PagedData<GlobalAdmReport>>(this.baseUrl, payload);
  }

  private formatQuery(query: Partial<DataQuery<GlobalAdmReportFilter>>): RequestQuery<GlobalAdmReport> {
    const { requestMonth, bspIsos, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        requestMonth: requestMonth && toShotIsoYearMonth(requestMonth),
        bspIsos: bspIsos && bspIsos.map(bsp => bsp.isoCountryCode)
      }
    });
  }
}
