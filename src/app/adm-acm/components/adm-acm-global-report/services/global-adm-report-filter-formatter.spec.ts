import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { GlobalAdmReportFilter } from '../model/global-adm-report-filter.model';

import { GlobalAdmReportFilterFormatter } from './global-adm-report-filter-formatter';

const filters: GlobalAdmReportFilter = {
  filename: 'testingFile',
  requestMonth: new Date('2017-02'),
  bspIsos: [
    {
      id: 1,
      isoCountryCode: 'ES',
      name: 'Spain'
    },
    {
      id: 2,
      isoCountryCode: 'MT',
      name: 'Malta'
    }
  ]
};

describe('GlobalAdmReportFilterFormatter', () => {
  let formatter: GlobalAdmReportFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new GlobalAdmReportFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      { keys: ['filename'], label: 'MENU.ACDMS.QUERY.globalADMReport.filter.label.filename - testingFile' },
      { keys: ['requestMonth'], label: 'MENU.ACDMS.QUERY.globalADMReport.filter.label.month - 2017-02' },
      { keys: ['bspIsos'], label: 'MENU.ACDMS.QUERY.globalADMReport.filter.label.bsps - ES, MT' }
    ]);
  });
});
