import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { GlobalAdmReportRequest } from '../adm-global-request-dialog/model/global-adm-report-request.model';
import { GlobalAdmReport } from '../model/global-adm-report.model';
import { GlobalAdmReportService } from './global-adm-report.service';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('GlobalAdmReportService', () => {
  let service: GlobalAdmReportService;
  let httpClientSpy: SpyObject<HttpClient>;

  const globalAdmReportMock: GlobalAdmReport[] = [
    {
      filename: 'testing-file1',
      requestMonth: '2017-02',
      bspIsos: ['ES', 'MT']
    },
    {
      filename: 'testing-file2',
      requestMonth: '2018-02',
      bspIsos: ['ES']
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        GlobalAdmReportService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(GlobalAdmReportService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/file-management/global-adm-report');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let globalAdmReportData: PagedData<GlobalAdmReport>;

    const globalAdmReportDataMock: PagedData<GlobalAdmReport> = {
      records: globalAdmReportMock,
      total: globalAdmReportMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(globalAdmReportDataMock));
    service.find(query).subscribe(res => {
      globalAdmReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/file-management/global-adm-report?page=0&size=20');
    expect(globalAdmReportData).toEqual(globalAdmReportDataMock);
  }));

  it('should request a global adm report by http post method', fakeAsync(() => {
    let globalAdmReportData: PagedData<GlobalAdmReport>;
    const requestPayload: GlobalAdmReportRequest = {
      requestMonth: '2017-02',
      bspIsos: ['ES', 'MT']
    };

    const globalAdmReportDataMock: PagedData<GlobalAdmReport> = {
      records: globalAdmReportMock,
      total: globalAdmReportMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.post.and.returnValue(of(globalAdmReportDataMock));
    service.requestGlobalAdmReport(requestPayload).subscribe(res => {
      globalAdmReportData = res;
    });
    tick();

    expect(httpClientSpy.post).toHaveBeenCalledWith('/file-management/global-adm-report', requestPayload);
    expect(globalAdmReportData).toEqual(globalAdmReportDataMock);
  }));
});
