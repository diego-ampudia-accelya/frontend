import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { SHARED } from './adm-acm-policy-list.constants';
import { AdmAcmPolicyModel } from '~app/adm-acm/models/adm-acm-policy.model';
import { AdmAcmPolicyService } from '~app/adm-acm/services/adm-acm-policy.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import {
  DataQuery,
  DefaultDisplayFilterFormatter,
  DefaultQueryStorage,
  QueryableDataSource
} from '~app/shared/components/list-view';
import { toValueLabelObjectDictionary, toValueLabelObjectDictionaryDesignator } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { AirlineDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-adm-acm-policy-list',
  templateUrl: './adm-acm-policy-list.component.html',
  providers: [
    DefaultQueryStorage,
    DefaultDisplayFilterFormatter,
    AdmAcmPolicyService,
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [AdmAcmPolicyService]
    }
  ]
})
export class AdmAcmPolicyListComponent implements OnInit {
  public admAcmType: MasterDataType;
  public hasCreating = false;
  public query = { filterBy: {}, paginateBy: {}, sortBy: [] };
  public downloadQuery: Partial<DataQuery>;

  public tableTitle: string;
  public downloadModalTitle: string;
  public columns: Array<TableColumn> = [];
  public target = '_blank';

  public actions: { action: GridTableActionType; disabled?: (row: AdmAcmPolicyModel) => boolean }[] = [];
  public airlineCodesList$: Observable<DropdownOption[]>;
  public airlineDesignatorsList$: Observable<DropdownOption[]>;
  public searchForm: FormGroup;

  constructor(
    public dataSource: QueryableDataSource<AdmAcmPolicyModel>,
    public defaultDisplayFilterFormatter: DefaultDisplayFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    public admAcmPolicyService: AdmAcmPolicyService,
    private activatedRoute: ActivatedRoute,
    private airlineDictionaryService: AirlineDictionaryService,
    private formBuilder: FormBuilder,
    public translationService: L10nTranslationService,
    public dialogService: DialogService
  ) {
    this.setConfig();
  }

  private setConfig() {
    this.activatedRoute.data.pipe(first()).subscribe(data => {
      this.admAcmType = data.admAcmType;
      this.tableTitle = `ADM_ACM.policyQuery.${this.admAcmType}PolicyTitle`;
    });
  }

  ngOnInit() {
    this.setResolversData();
    this.initializeFilterForm();
  }

  public setResolversData(): void {
    this.setColumns();
    this.setQuery();
    this.loadData();
  }

  private setColumns() {
    this.columns = SHARED.COLUMNS;
  }

  public loadData(query = this.query): void {
    const dataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };
    this.downloadQuery = dataQuery;
    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public download(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('ADM_ACM.policyQuery.admPolicyDownloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery
      },
      apiService: this.admAcmPolicyService
    });
  }

  public setQuery(): void {
    const storedQuery = this.queryStorage.get();

    if (storedQuery && !isEmpty(storedQuery.filterBy)) {
      this.query = storedQuery;
    } else {
      this.query = {
        filterBy: {},
        sortBy: [],
        paginateBy: {}
      };
    }
  }

  public search(query: DataQuery<AdmAcmPolicyModel>): void {
    this.query = query;
    this.loadData(query);
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.initializeAirlineFilterDropdown();
    }
  }

  public initializeFilterForm(): void {
    this.searchForm = this.formBuilder.group({
      iataCode: null,
      localName: null,
      designator: null,
      url: null,
      comment: null,
      lastUpdate: null
    });
  }

  private initializeAirlineFilterDropdown() {
    this.airlineCodesList$ = this.airlineDictionaryService
      .getDropdownOptions()
      .pipe(map(airlinesSummary => toValueLabelObjectDictionary(airlinesSummary)));

    this.airlineDesignatorsList$ = this.airlineDictionaryService
      .getDropdownOptions()
      .pipe(map(airlinesSummary => toValueLabelObjectDictionaryDesignator(airlinesSummary)));
  }
}
