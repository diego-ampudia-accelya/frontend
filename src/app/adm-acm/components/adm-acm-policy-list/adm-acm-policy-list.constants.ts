const translationPrefix = 'ADM_ACM.policyQuery';

export const SHARED = {
  get COLUMNS() {
    return [
      {
        name: `${translationPrefix}.tableColumns.code`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.name`,
        prop: 'airline.localName',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.designator`,
        prop: 'airline.designator',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.link`,
        prop: 'url',
        resizeable: false,
        draggable: false,
        cellTemplate: 'commonLinkTargetCellTmpl'
      },
      {
        name: `${translationPrefix}.tableColumns.remarks`,
        prop: 'comment',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.lastUpdate`,
        prop: 'lastUpdate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl'
      }
    ];
  }
};
