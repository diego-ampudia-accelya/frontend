import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { initialState } from '../adm-acm/adm-acm-test.config';
import { AdmAcmPolicyListComponent } from './adm-acm-policy-list.component';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmPolicyService } from '~app/adm-acm/services/adm-acm-policy.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  DefaultDisplayFilterFormatter,
  DefaultQueryStorage,
  QueryableDataSource
} from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AirlineSummary, DropdownOption } from '~app/shared/models';
import { AirlineDictionaryService, AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

describe('AdmAcmPolicyListComponent', () => {
  let component: AdmAcmPolicyListComponent;
  let fixture: ComponentFixture<AdmAcmPolicyListComponent>;
  const queryStorageMock = createSpyObject(DefaultQueryStorage);
  const queryMock = { filterBy: {}, paginateBy: {}, sortBy: [] };
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const admAcmDialogServiceSpy = createSpyObject(AdmAcmDialogService);
  const AppConfigurationServiceSpy = createSpyObject(AppConfigurationService);
  const PermissionsServiceSpy = createSpyObject(PermissionsService);

  const dataQueryMock = {
    ...cloneDeep(defaultQuery),
    filterBy: {
      designator: ['A9', 'BB']
    },
    sortBy: []
  };

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    },
    {
      value: { id: 3, name: 'AIRLINE 003', code: '003', designator: 'L3' },
      label: '003 / AIRLINE 003'
    }
  ];

  const mockAirlineCodesList = [
    { value: '001', label: '001 / AIRLINE 001' },
    { value: '002', label: '002 / AIRLINE 002' },
    { value: '003', label: '003 / AIRLINE 003' }
  ];

  const mockAirlineDesignatorsList = [
    { value: 'L1', label: 'L1' },
    { value: 'L2', label: 'L2' },
    { value: 'L3', label: 'L3' }
  ];

  airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmPolicyListComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockStore({ initialState }),
        FormBuilder,
        { provide: PermissionsService, useValue: PermissionsServiceSpy },
        { provide: AppConfigurationService, useValue: AppConfigurationServiceSpy },
        { provide: AdmAcmDialogService, useValue: admAcmDialogServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    })
      .overrideComponent(AdmAcmPolicyListComponent, {
        set: {
          providers: [
            DefaultDisplayFilterFormatter,
            AdmAcmPolicyService,
            { provide: DefaultQueryStorage, useValue: queryStorageMock },
            {
              provide: QueryableDataSource,
              useValue: queryableDataSourceSpy,
              deps: [AdmAcmPolicyService]
            }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmPolicyListComponent);
    component = fixture.componentInstance;
    queryStorageMock.get.and.returnValue(queryMock);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');
    component.search(queryMock);
    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should initialize filter form', () => {
    component.searchForm = new FormGroup({});
    const initialFormState = {
      iataCode: null,
      localName: null,
      designator: null,
      url: null,
      comment: null,
      lastUpdate: null
    };
    component.initializeFilterForm();
    expect(component.searchForm.value).toEqual(initialFormState);
  });

  it('should not initialize airline filter dropdown when filters are closed', () => {
    const initFilterDropdownMethod = spyOn(component, 'initializeFilterForm');

    component.onFilterButtonClicked(false);
    expect(initFilterDropdownMethod).not.toHaveBeenCalled();
  });

  it('should initialize airline filter dropdown when button filter is clicked', () => {
    const initializeAirlineFilterDropdownSpy = spyOn<any>(component, 'initializeAirlineFilterDropdown');

    component.onFilterButtonClicked(true);
    expect(initializeAirlineFilterDropdownSpy).toHaveBeenCalled();
  });

  it('should set table columns', () => {
    const translationPrefix = 'ADM_ACM.policyQuery';
    const expectedColums = [
      {
        name: `${translationPrefix}.tableColumns.code`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.name`,
        prop: 'airline.localName',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.designator`,
        prop: 'airline.designator',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.link`,
        prop: 'url',
        resizeable: false,
        draggable: false,
        cellTemplate: 'commonLinkTargetCellTmpl'
      },
      {
        name: `${translationPrefix}.tableColumns.remarks`,
        prop: 'comment',
        draggable: false,
        resizeable: true
      },
      {
        name: `${translationPrefix}.tableColumns.lastUpdate`,
        prop: 'lastUpdate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl'
      }
    ];
    component['setColumns']();
    expect(component.columns).toEqual(expectedColums);
  });

  it('should set query variable', () => {
    component.setQuery();
    expect(component.query).toEqual(queryMock);
  });

  it('should set columns, set query and load data when setResolversData method is called', () => {
    const spyOnSetQuery = spyOn(component, 'setQuery');
    const spyOnLoadData = spyOn(component, 'loadData');
    const spyOnSetColumns = spyOn<any>(component, 'setColumns');

    component.setResolversData();
    expect(spyOnSetColumns).toHaveBeenCalled();
    expect(spyOnSetQuery).toHaveBeenCalled();
    expect(spyOnLoadData).toHaveBeenCalled();
  });

  it('should set data query and load it', fakeAsync(() => {
    component.loadData(dataQueryMock);

    tick();

    expect(component.downloadQuery).toEqual(dataQueryMock);
    expect(queryableDataSourceSpy.get).toHaveBeenCalled();
    expect(queryStorageMock.save).toHaveBeenCalled();
  }));

  it('should initialize airline filter dropdown returning observables data', fakeAsync(() => {
    let airlineCodeResult;
    let airlineDesignatorResult;
    let subscriptionFinalized: boolean;

    component['initializeAirlineFilterDropdown']();
    component.airlineCodesList$.pipe(finalize(() => (subscriptionFinalized = true))).subscribe(data => {
      airlineCodeResult = data;
    });
    component.airlineDesignatorsList$.pipe(finalize(() => (subscriptionFinalized = true))).subscribe(data => {
      airlineDesignatorResult = data;
    });

    expect(airlineCodeResult).toEqual(mockAirlineCodesList);
    expect(airlineDesignatorResult).toEqual(mockAirlineDesignatorsList);
    expect(subscriptionFinalized).toBe(true);
  }));
});
