import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslatePipe, L10nTranslationService } from 'angular-l10n';
import { MockDeclarations, MockPipe } from 'ng-mocks';
import { of } from 'rxjs';

import { admAcmServiceStub, admAcmTicketServiceStub, initialState, reason } from '../adm-acm/adm-acm-test.config';
import { AdmAcmDetailsComponent } from './adm-acm-details.component';
import { ReasonAction } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmActionEmitterType } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { InputComponent, SelectComponent, TextareaComponent, UserInputListComponent } from '~app/shared/components';
import { AlertMessageComponent } from '~app/shared/components/alert-message/alert-message.component';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { AccessType } from '~app/shared/enums';
import { TabService } from '~app/shared/services';

describe('AdmAcmDetailsComponent', () => {
  let component: AdmAcmDetailsComponent;
  let fixture: ComponentFixture<AdmAcmDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdmAcmDetailsComponent,
        MockPipe(L10nTranslatePipe),
        MockDeclarations(
          InputComponent,
          SelectComponent,
          TextareaComponent,
          DatepickerComponent,
          UserInputListComponent,
          AlertMessageComponent
        )
      ],
      imports: [
        CommonModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        HttpClientTestingModule
      ],
      providers: [
        provideMockStore({ initialState }),
        HttpTestingController,
        FormBuilder,
        mockProvider(HttpClient),
        mockProvider(L10nTranslationService),
        DialogService,
        AdmAcmDialogService,
        PermissionsService,
        AdmAcmDataService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmDetailsComponent);
    component = fixture.componentInstance;

    component.admAcmService = admAcmServiceStub;
    component.formTicket = admAcmTicketServiceStub;
    component.admAcmService.getReasons = jasmine.createSpy().and.returnValue(of([{ ...reason }]));
    component.admAcmService.getBspsList = jasmine.createSpy().and.returnValue(of());
    component.formData = new AdmAcmDataService(TestBed.inject(FormBuilder));
    component.formConfig = createSpyObject(AdmAcmConfigService);
    component.formConfig = createAdmAcmConfigService(
      component.formData,
      TestBed.inject(AdmAcmDialogService),
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Router),
      TestBed.inject(Store),
      TestBed.inject(TabService)
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should call the proper functions', () => {
      const initializeRelatedTicketDocumentsSpy = spyOn<any>(
        component,
        'initializeRelatedTicketDocuments'
      ).and.callThrough();
      const initializeDateOfIssueSpy = spyOn<any>(component, 'initializeDateOfIssue').and.callThrough();
      const buildFormSpy = spyOn<any>(component, 'buildForm').and.callThrough();
      const initializeRelatedDocUserInputListenerSpy = spyOn<any>(
        component,
        'initializeRelatedDocUserInputListener'
      ).and.callThrough();
      const initializeRelatedDocsListenerSpy = spyOn<any>(component, 'initializeRelatedDocsListener').and.callThrough();

      component.ngOnInit();

      expect(initializeRelatedTicketDocumentsSpy).toHaveBeenCalled();
      expect(initializeDateOfIssueSpy).toHaveBeenCalled();
      expect(buildFormSpy).toHaveBeenCalled();
      expect(initializeRelatedDocUserInputListenerSpy).toHaveBeenCalled();
      expect(initializeRelatedDocsListenerSpy).toHaveBeenCalled();
    });

    it('should set isAcdmd property to "true" when admAcmType is Admd', () => {
      component.formConfig.admAcmType = MasterDataType.Admd;
      component.ngOnInit();

      expect(component.isAcdmd).toBeTruthy();
    });

    it('should set isAcdmd property to "true" when admAcmType is Acmd', () => {
      component.formConfig.admAcmType = MasterDataType.Acmd;
      component.ngOnInit();

      expect(component.isAcdmd).toBeTruthy();
    });

    it('should set isAcdmd property to "false" when admAcmType is Agent', () => {
      const initializeAirlineListenerSpy = spyOn<any>(component, 'initializeAirlineListener').and.callThrough();
      const reasonForIssueChangeListenerSpy = spyOn<any>(component, 'reasonForIssueChangeListener').and.callThrough();
      component.formConfig.admAcmType = MasterDataType.Agent;
      component.ngOnInit();

      expect(component.isAcdmd).toBeFalsy();
      expect(initializeAirlineListenerSpy).toHaveBeenCalled();
      expect(reasonForIssueChangeListenerSpy).toHaveBeenCalled();
    });

    it('should set isRMICVisible property to "false" when admAcmType is Agent', () => {
      component.formConfig.admAcmType = MasterDataType.Agent;
      component.ngOnInit();

      expect(component.isRMICVisible).toBeTruthy();
    });

    it('should set isRMICVisible property to "false" when admAcmType is Acmd', () => {
      component.formConfig.admAcmType = MasterDataType.Acmd;
      component.ngOnInit();

      expect(component.isRMICVisible).toBeFalsy();
    });

    it('should call formData whenFormIsReady if formConfig isEditMode is true', fakeAsync(() => {
      const whenFormIsReadySpy = spyOn<any>(component.formData, 'whenFormIsReady').and.callThrough();
      component.formConfig.accessType = AccessType.edit;

      component.ngOnInit();

      tick();

      expect(component.formConfig.isEditTicketMode).toBeFalsy();
      expect(component.formConfig.isEditMode).toBeTruthy();
      expect(whenFormIsReadySpy).toHaveBeenCalled();
    }));

    it('should call formData whenFormIsReady if formConfig isEditTicketMode is true', fakeAsync(() => {
      const whenFormIsReadySpy = spyOn<any>(component.formData, 'whenFormIsReady').and.callThrough();
      component.formConfig.accessType = AccessType.editTicket;

      component.ngOnInit();

      tick();

      expect(component.formConfig.isEditTicketMode).toBeTruthy();
      expect(component.formConfig.isEditMode).toBeFalsy();
      expect(whenFormIsReadySpy).toHaveBeenCalled();
    }));

    it('should not call formData whenFormIsReady if formConfig isEditTicketMode is false', fakeAsync(() => {
      component.formConfig.accessType = AccessType.create;

      component.ngOnInit();

      tick();

      expect(component.formConfig.isEditTicketMode).toBeFalsy();
      expect(component.formConfig.isEditMode).toBeFalsy();
    }));
  });

  it('should getControlErrorStringFromBE return null when relatedTicketDocumentsControl is valid', fakeAsync(() => {
    component.ngOnInit();

    component.relatedTicketDocumentsControl.patchValue([
      {
        documentId: null,
        relatedTicketDocumentNumber: ''
      }
    ]);

    component.relatedTicketDocumentsControl.setValidators(Validators.required);

    tick();
    expect(component.getControlErrorStringFromBE()).toBeUndefined();
  }));

  it('should initialize relatedTicketDocuments', () => {
    const buildRelatedTicketDocumentGroupSpy = spyOn<any>(
      component,
      'buildRelatedTicketDocumentGroup'
    ).and.callThrough();
    expect(component.relatedTicketDocumentsControlUserInput).toBeUndefined();
    expect(component.relatedTicketDocumentsValidators).toBeUndefined();
    expect(component.relatedTicketDocumentsControl).toBeUndefined();

    component['initializeRelatedTicketDocuments']();

    expect(component.relatedTicketDocumentsControlUserInput).toBeTruthy();
    expect(component.relatedTicketDocumentsValidators).toBeTruthy();
    expect(component.relatedTicketDocumentsControl).toBeTruthy();
    expect(buildRelatedTicketDocumentGroupSpy).toHaveBeenCalled();
  });

  it('should create FormGroup object', () => {
    const createGroupSpy = spyOn<any>(component['formFactory'], 'createGroup').and.callThrough();
    expect(component['buildRelatedTicketDocumentGroup']()).toBeTruthy();
    expect(createGroupSpy).toHaveBeenCalled();
  });

  it('should create form', () => {
    const setSectionFormGroupSpy = spyOn<any>(component.formData, 'setSectionFormGroup').and.callThrough();

    expect(component.reasonForMemoControl).toBeUndefined();
    expect(component['gdsForwards']).toBeUndefined();
    expect(component.form).toBeUndefined();

    component['buildForm']();

    expect(component.reasonForMemoControl).toBeTruthy();
    expect(component['gdsForwards']).toBeTruthy();
    expect(component.form).toBeTruthy();
    expect(setSectionFormGroupSpy).toHaveBeenCalled();
  });

  it('should set maxDateOfIssue value when initializeDateOfIssue is called', fakeAsync(() => {
    const bspSubject = component.formData.getSubject(AdmAcmActionEmitterType.bsp);
    spyOn(bspSubject, 'next').and.callThrough();
    spyOn(component.formData, 'getIssueDate').and.returnValue(of(new Date('2019-08-03')));

    component['initializeDateOfIssue']();

    tick();

    expect(component.maxDateOfIssue).toEqual(new Date('2019-08-03'));
    expect(component.admAcmService.getBspTime).not.toHaveBeenCalled();
  }));

  it('should call getReasons when initializeReasonForIssueOptions is called', fakeAsync(() => {
    component.ngOnInit();
    component['initializeReasonForIssueOptions'](1, 1);

    tick();

    expect(component.admAcmService.getReasons).toHaveBeenCalledWith(1, 1, ReasonAction.issue);
    expect(component['issueReasons']).toEqual([reason]);
    expect(component.admAcmService.getReasonsDropDownOptions).toHaveBeenCalled();
  }));

  it('should set relatedTicketDocumentsControlUserInput when initializeRelatedTicketDocumentsForBEData is called', fakeAsync(() => {
    component.ngOnInit();
    tick();
    component.relatedTicketDocumentsControl.patchValue([
      {
        documentId: null,
        relatedTicketDocumentNumber: ''
      }
    ]);

    component['initializeRelatedTicketDocumentsForBEData']();

    expect(component.relatedTicketDocumentsControlUserInput).toBeDefined();
  }));

  it('should call relatedTicketDocumentsControl clear when modifyControlFromUserList is called', fakeAsync(() => {
    component.ngOnInit();
    tick();

    const relatedTicketDocumentsControlClearSpy = spyOn<any>(component.relatedTicketDocumentsControl, 'clear');
    const relatedTicketDocumentsControlPushSpy = spyOn<any>(component.relatedTicketDocumentsControl, 'push');

    component.relatedTicketDocumentsControl.patchValue([
      {
        documentId: null,
        relatedTicketDocumentNumber: ''
      }
    ]);

    component['modifyControlFromUserList'](['ticket1']);

    expect(relatedTicketDocumentsControlClearSpy).toHaveBeenCalled();
    expect(relatedTicketDocumentsControlPushSpy).toHaveBeenCalled();
  }));

  it('should get ticketId clear when modifyControlFromUserList is called', fakeAsync(() => {
    component.ngOnInit();
    component.formConfig.accessType = AccessType.editTicket;
    tick();

    component.relatedTicketDocumentsControl.patchValue([
      {
        documentId: null,
        relatedTicketDocumentNumber: ''
      }
    ]);

    component['modifyControlFromUserList'](['ticket1']);

    expect(component.formTicket.getTicketId).toHaveBeenCalled();
  }));
});
