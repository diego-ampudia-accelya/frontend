import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { combineLatest, EMPTY, Observable, of, Subject, Subscription } from 'rxjs';
import { map, skipWhile, switchMap, takeUntil } from 'rxjs/operators';

import { rmicPatternValidator } from './validators/rmic-pattern.validator';
import { rtdnPatternValidator } from './validators/rtdn-pattern.validator';
import { AcdmReason } from '~app/adm-acm/models/adm-acm-issue-be-aux.model';
import { ReasonAction, RelatedTicketDocument } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  DetailViewModel,
  ReasonDetailViewModel,
  RelatedDocumentViewModel
} from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmActionEmitterType } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import {
  transformRelatedDocumentTicketToBE,
  transformRelatedDocumentToBE
} from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AlertMessageType } from '~app/shared/enums/alert-message-type.enum';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Component({
  selector: 'bspl-adm-acm-details',
  templateUrl: './adm-acm-details.component.html',
  styleUrls: ['./adm-acm-details.component.scss'],
  providers: [AdmAcmService]
})
export class AdmAcmDetailsComponent implements OnInit, OnDestroy {
  @Output() showDocsWarning = new EventEmitter<boolean>();

  @Input() admAcmService: AdmAcmService;
  @Input() formData: AdmAcmDataService;
  @Input() formConfig: AdmAcmConfigService;
  @Input() formTicket: AdmAcmTicketService;

  public form: FormGroup;
  public relatedTicketDocumentsControl: FormArray;
  public relatedTicketDocumentsControlUserInput: FormArray;
  public dateOfIssueRelatedDocumentControl: FormControl;
  public reasonForMemoControl: FormControl;
  public relatedTicketDocumentsValidators: ValidatorFn[];

  public lineCharacterLimit = 45;
  public linesLimit = 20;

  public maxDateOfIssue = new Date();

  public reasonOptions: DropdownOption[] = [];

  public showWarningMsg = false;
  public docsMessageType = AlertMessageType.warning;

  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)
  public isRMICVisible: boolean;

  private gdsForwards: FormControl;
  private issueReasons: AcdmReason[];
  private subscriptions: Subscription[] = [];
  private destroy$ = new Subject();
  private formFactory: FormUtil;

  constructor(private cd: ChangeDetectorRef, public fb: FormBuilder) {
    this.formFactory = new FormUtil(this.fb);
  }

  ngOnInit() {
    this.isAcdmd =
      this.formConfig.admAcmType === MasterDataType.Admd || this.formConfig.admAcmType === MasterDataType.Acmd;
    this.isRMICVisible = this.formConfig.isRMICVisible();
    this.initializeRelatedTicketDocuments();
    this.initializeDateOfIssue();
    this.buildForm();
    this.initializeRelatedDocUserInputListener();
    this.initializeRelatedDocsListener();

    if (this.formConfig.isEditTicketMode || this.formConfig.isEditMode) {
      this.formData.whenFormIsReady().subscribe(() => {
        this.initializeRelatedTicketDocumentsForBEData();
      });
    }

    if (!this.isAcdmd) {
      this.initializeAirlineListener();
      this.reasonForIssueChangeListener();
    }
  }

  public getControlErrorStringFromBE(): string {
    let errorMsg;

    if (this.relatedTicketDocumentsControl.invalid) {
      const invalidControl = this.relatedTicketDocumentsControl.controls.find(group => group.invalid);
      errorMsg = FormUtil.get<RelatedTicketDocument>(invalidControl as FormGroup, 'relatedTicketDocumentNumber').errors
        .backendError?.message;
    }

    return errorMsg;
  }

  private initializeRelatedTicketDocuments() {
    this.relatedTicketDocumentsControlUserInput = this.fb.array([]);
    this.relatedTicketDocumentsValidators = [rtdnPatternValidator];
    this.relatedTicketDocumentsControl = this.fb.array([this.buildRelatedTicketDocumentGroup()]);
  }

  private buildRelatedTicketDocumentGroup(): FormGroup {
    return this.formFactory.createGroup<RelatedTicketDocument>({
      relatedTicketDocumentNumber: [null, this.relatedTicketDocumentsValidators],
      documentId: null
    });
  }

  private buildForm(): void {
    this.reasonForMemoControl = this.fb.control('');
    this.gdsForwards = this.fb.control([]);

    this.form = this.formFactory.createGroup<DetailViewModel>({
      disputeContact: '',
      disputeTime: '',
      gdsForwards: this.gdsForwards,
      reasons: this.fb.control([]),
      reasonsDetails: this.formFactory.createGroup<ReasonDetailViewModel>({
        reasonForIssue: null,
        reasonForMemo: this.reasonForMemoControl,
        reasonForMemoIssuanceCode: this.fb.control('', {
          validators: [rmicPatternValidator],
          updateOn: 'blur'
        })
      }),
      relatedDocuments: this.formFactory.createGroup<RelatedDocumentViewModel>({
        dateOfIssueRelatedDocument: this.dateOfIssueRelatedDocumentControl,
        relatedTicketDocuments: this.relatedTicketDocumentsControl
      })
    });

    this.formData.setSectionFormGroup('details', this.form);

    if (this.isAcdmd) {
      const reasonCtrl = FormUtil.get<DetailViewModel>(this.form, 'reasonsDetails') as FormGroup;
      FormUtil.get<ReasonDetailViewModel>(reasonCtrl, 'reasonForMemoIssuanceCode').disable();
      FormUtil.get<ReasonDetailViewModel>(reasonCtrl, 'reasonForMemo').setValidators(Validators.required);
    }
  }

  private initializeDateOfIssue() {
    this.subscriptions.push(
      combineLatest([this.formData.getSubject(AdmAcmActionEmitterType.bsp), this.formData.getIssueDate()])
        .pipe(
          switchMap(([bsp, issueDate]) => {
            let result$: Observable<Date> = EMPTY;

            if (issueDate) {
              result$ = of(new Date(issueDate));
            } else if (bsp) {
              result$ = this.admAcmService.getBspTime(bsp.id).pipe(map(bspTime => bspTime.localTime));
            }

            return result$;
          })
        )
        .subscribe(date => (this.maxDateOfIssue = date))
    );
  }

  private initializeAirlineListener() {
    this.subscriptions.push(
      combineLatest([
        this.formData.getSubject(AdmAcmActionEmitterType.airline),
        this.formData.getSubject(AdmAcmActionEmitterType.agent)
      ])
        .pipe(skipWhile(result => !result[0] || !result[1]?.bsp))
        .subscribe(result => {
          this.initializeReasonForIssueOptions(result[0].id, result[1].bsp.id);
        })
    );
  }

  private initializeReasonForIssueOptions(airlineId: number, bspId: number) {
    this.reasonOptions = [];
    this.admAcmService
      .getReasons(airlineId, bspId, ReasonAction.issue)
      //* takeUntil since this request can take long and user can navigate causing errors when detectChanges is called
      .pipe(takeUntil(this.destroy$))
      .subscribe(reasons => {
        this.issueReasons = reasons;
        this.reasonOptions = this.admAcmService.getReasonsDropDownOptions(reasons);
        this.cd.detectChanges();
      });
  }

  private reasonForIssueChangeListener() {
    const reasonDetailsGroup = FormUtil.get<DetailViewModel>(this.form, 'reasonsDetails') as FormGroup;
    const reasonForIssueCtrl = FormUtil.get<ReasonDetailViewModel>(reasonDetailsGroup, 'reasonForIssue');
    this.subscriptions.push(
      reasonForIssueCtrl.valueChanges.subscribe(reasonValue => {
        const selected = this.issueReasons.find(reasonSelected => reasonSelected.id === reasonValue);
        FormUtil.get<ReasonDetailViewModel>(reasonDetailsGroup, 'reasonForMemo').setValue(
          selected ? selected.description : null
        );
      })
    );
  }

  private initializeRelatedDocsListener() {
    const relatedDocumentsCtrl = FormUtil.get<DetailViewModel>(this.form, 'relatedDocuments') as FormGroup;
    this.subscriptions.push(
      relatedDocumentsCtrl.valueChanges.subscribe(data => {
        this.showWarningMsg = data.dateOfIssueRelatedDocument && !data.relatedTicketDocuments.length;
        this.showDocsWarning.emit(this.showWarningMsg);
      })
    );
  }

  private initializeRelatedTicketDocumentsForBEData() {
    this.relatedTicketDocumentsControl.value.forEach(docNumberControl =>
      this.relatedTicketDocumentsControlUserInput.push(this.fb.control(docNumberControl.relatedTicketDocumentNumber))
    );
  }

  private modifyControlFromUserList(tickets: string[]) {
    let relDocuments: RelatedTicketDocument[] = [];

    this.relatedTicketDocumentsControl.clear();

    if (this.formConfig.isEditTicketMode) {
      const ticketId = this.formTicket.getTicketId();
      relDocuments = transformRelatedDocumentTicketToBE(ticketId, tickets);
    } else {
      relDocuments = transformRelatedDocumentToBE(tickets);
    }

    relDocuments.forEach(relDoc => this.relatedTicketDocumentsControl.push(this.fb.group(relDoc)));
  }

  private initializeRelatedDocUserInputListener() {
    this.subscriptions.push(
      this.relatedTicketDocumentsControlUserInput.valueChanges.subscribe(data => {
        this.modifyControlFromUserList(data);
      })
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
