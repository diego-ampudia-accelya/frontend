import { AbstractControl } from '@angular/forms';

export function rtdnPatternValidator(control: AbstractControl): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  if (!(control.value instanceof Array)) {
    if (control.value && !control.value.match(/^[A-Z0-9]{3}\d{10}$/)) {
      result = {
        rtdnPattern: { key: 'ADM_ACM.details.error.rtdnPattern' }
      };
    }
  }

  return result;
}
