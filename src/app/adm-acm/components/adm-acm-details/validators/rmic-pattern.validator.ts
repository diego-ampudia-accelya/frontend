import { AbstractControl } from '@angular/forms';

export function rmicPatternValidator(control: AbstractControl): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  if (!control.value.match(/^[A-Z0-9 ./-]{0,5}$/i)) {
    result = {
      rmicPattern: { key: 'ADM_ACM.details.error.rmicPattern' }
    };
  }

  return result;
}
