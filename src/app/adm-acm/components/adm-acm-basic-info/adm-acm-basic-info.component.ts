import { ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10N_LOCALE } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { combineLatest, from, Observable, of } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';
import { BasicAgentInfo, BasicAirlineInfo } from '~app/adm-acm/models/adm-acm-issue-be-aux.model';
import { AdmAcmStatus, ConcernIndicator, TransactionCode } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  AdmSettingViewModel,
  AgentViewModel,
  AirlineViewModel,
  BasicInformationViewModel,
  ContactViewModel,
  CurrencyViewModel
} from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmActionEmitterType, TocaErrorsType } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { getUserDetails } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import {
  getAgentGroupAcdmqPermission,
  getIssueAcdmPendingSupervisionPermission,
  getIssueAllAcdmSpcdrAcdmdPermission,
  getUpdateAcdmPermissions
} from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { AcdmContactActions } from '~app/adm-acm/store/actions';
import { getAirlineContactData } from '~app/adm-acm/store/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { AccessType } from '~app/shared/enums/access-type.enum';
import {
  toValueLabelObjectAgentId,
  toValueLabelObjectAirlineId,
  toValueLabelObjectBsp,
  toValueLabelObjectDictionaryId
} from '~app/shared/helpers';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { AgentDictionaryQueryFilters, AgentSummary, AirlineSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { multiEmailValidator } from '~app/shared/validators/multi-email.validator';

@Component({
  selector: 'bspl-adm-acm-basic-info',
  templateUrl: './adm-acm-basic-info.component.html',
  styleUrls: ['./adm-acm-basic-info.component.scss'],
  providers: [AdmAcmService]
})
export class AdmAcmBasicInfoComponent implements OnInit, OnDestroy {
  @Output() agentChange = new EventEmitter<AgentViewModel>();
  @Output() currencyChange = new EventEmitter<CurrencyViewModel>();
  @Output() netReportingChange = new EventEmitter<boolean>();
  @Output() admaForChange = new EventEmitter<ConcernIndicator>();
  @Output() tocaTypeChange = new EventEmitter<string>();
  @Output() bspChange = new EventEmitter<BspDto>();

  @Input() admAcmService: AdmAcmService;
  @Input() formData: AdmAcmDataService;
  @Input() formConfig: AdmAcmConfigService;
  @Input() formTicket: AdmAcmTicketService;

  public objectKeys = Object.keys;
  public loggedUser: User;

  public ticketIssueMode; //* Indicates if we are accesing to issue ACDM from a document
  public isPendingAuthorization$; //* Indicates if ACDM is Pending Authorized

  public form: FormGroup;
  public admSettingsForm: FormGroup;
  public agentForm: FormGroup;
  public airlineForm: FormGroup;
  public airlineContactForm: FormGroup;
  private subscriptions = [];

  public currencyList: Array<CurrencyViewModel> = [];

  public airlineAddress = '';
  public showAgentDetails = false;
  public showAirlineDetails = false;

  public bspSelected: BspDto = null;
  public airlineSelected: AirlineViewModel;
  public agentSelected: AgentViewModel;

  public bspOptions: DropdownOption<BspDto>[];
  public agentOptions: DropdownOption[];
  public airlineOptions: DropdownOption[];
  public currencyOptions: DropdownOption[] = [];
  public statOptions: DropdownOption[] = [];
  public tocaTypeOptions: DropdownOption[] = [];
  public admaForOptions: DropdownOption[] = [];

  public isAirlineVatFieldVisible$: Observable<boolean>;
  public isAgentVatFieldVisible$: Observable<boolean>;
  public isNetReportingVisible$: Observable<boolean>;

  public get transactionCode(): TransactionCode {
    return this.formConfig.getTransactionCode();
  }
  public get admAcmType(): MasterDataType {
    return this.formConfig.admAcmType;
  }

  public get isAgentDropdownLocked(): boolean {
    return !this.bspSelected || this.agentOptions?.length === 1;
  }

  private formFactory: FormUtil;
  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private cd: ChangeDetectorRef,
    private agentDictionaryService: AgentDictionaryService,
    private airlineDictionaryService: AirlineDictionaryService,
    private bspsDictionaryService: BspsDictionaryService
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.initializeLoggedUser();
    this.setConfig();
    this.buildForm();

    this.initializeFieldsVisibility();

    this.isPendingAuthorization$ = this.formData
      .getStatus()
      .pipe(map(status => status === AdmAcmStatus.pendingAuthorization));

    switch (this.formConfig.accessType) {
      case AccessType.create:
        this.formData.isFormReady.next(false);
        this.initializeFormListeners();
        this.fetchInitialDataForm().subscribe(() => {
          this.emitSecondaryOutputs();
          // TODO This next(true) can be called when the form is not ready yet (not all sections initialized in formData).
          // TODO We should find another way of blocking the form or, at least, evaluate that all sections are included when indicating formReady true
          this.formData.isFormReady.next(true);
        });
        break;

      case AccessType.editTicket:
      case AccessType.edit:
        this.fetchInitialDataForm()
          .pipe(
            switchMap(() => this.formData.whenFormIsReady()),
            switchMap(() => this.onEditAccess()),
            switchMap(() => this.formData.whenFormIsReady())
          )
          .subscribe(() => {
            this.initializeFormListeners();
          });
        break;
    }
  }

  private setConfig() {
    this.ticketIssueMode = this.formConfig.isEditTicketMode;
    this.isAcdmd = this.formConfig.isAcdmd;
  }

  private buildForm() {
    this.initializeSectionForms();

    this.form = this.formFactory.createGroup<BasicInformationViewModel>({
      ticketDocumentNumber: ['N/A', []],
      reportingDate: ['N/A', []],
      bspName: [null, Validators.required],
      type: [this.transactionCode, []],
      admSettings: this.admSettingsForm,
      agent: this.agentForm,
      airline: this.airlineForm,
      airlineContact: this.airlineContactForm
    });

    this.formData.setSectionFormGroup('basicInformation', this.form);
  }

  private initializeContactDataStored() {
    this.formData
      .whenFormIsReady()
      .pipe(switchMap(() => this.store.pipe(select(getAirlineContactData))))
      .subscribe(data => {
        let contact: ContactViewModel;

        if (this.isAcdmd && this.loggedUser.userType === UserType.AGENT) {
          contact = this.agentSelected.contact as ContactViewModel;
        } else if (data && this.loggedUser.userType === UserType.AIRLINE) {
          contact = data;
        } else {
          contact = this.airlineSelected.contact as ContactViewModel;
        }

        this.airlineContactForm.patchValue(contact, { emitEvent: false });
      });
  }

  private fetchInitialDataForm() {
    return combineLatest([this.getBspsDropdownOptions$(), this.formData.getSubject(AdmAcmActionEmitterType.bsp)]).pipe(
      first(),
      tap(([options, selectedBsp]) => this.initializeBspsDropdown(options, selectedBsp))
    );
  }

  private initializeAirlineContactDataListener() {
    this.subscriptions.push(
      this.airlineContactForm.valueChanges.subscribe(value => {
        if (this.airlineContactForm.valid) {
          this.store.dispatch(new AcdmContactActions.UpdateAirlineContactData(value));
        }
      })
    );
  }

  private onEditAccess(): Observable<boolean> {
    //* We keep the original data from BE to restore it afterwards
    const originalData = this.form.value;

    //* While we do changes, the form is not ready
    this.formData.isFormReady.next(false);

    const bsp = FormUtil.get<AgentViewModel>(this.agentForm, 'bsp').value;
    const agentId = this.agentForm.get('id').value;

    return from(this.onBspChange$(bsp)).pipe(
      tap(() => {
        this.onAgentChange(agentId);
        //* We can restore previous data
        const previousData = { ...originalData, bspName: bsp };
        this.form.patchValue(previousData, { emitEvent: false });
      }),
      switchMap(() => (this.ticketIssueMode ? this.onEditTicketAccess$() : of(true))),
      tap(() => {
        //* We emit the secondary outputs (not agent/currency ones) to the outside world
        this.emitSecondaryOutputs();
        //* Now the form is ready again
        this.formData.isFormReady.next(true);
      })
    );
  }

  private onEditTicketAccess$(): Observable<any> {
    this.updateAcdmForOnTicketAccess();

    return this.updateNetReportingOnTicketAccess();
  }

  private updateAcdmForOnTicketAccess() {
    FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'concernsIndicator').patchValue(
      this.formTicket.getTicketConcernIndicator()
    );
  }

  private updateNetReportingOnTicketAccess(): Observable<any> {
    const netReportingControl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'netReporting');

    return this.formConfig.getAcdmSpam().pipe(
      tap(acdmSpam => {
        netReportingControl.patchValue(this.formTicket.getTicketNetReporting(acdmSpam));
      })
    );
  }

  private emitSecondaryOutputs() {
    const netReportingControl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'netReporting');
    const admaForControl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'concernsIndicator');

    this.onNetReportingChange(netReportingControl.value);
    this.admaForChange.emit(admaForControl.value);
  }

  private initializeFormListeners() {
    this.initializeAgentChangesListener();
    this.initializeAirlineChangesListener();
    this.initializeCurrencyChangesListener();

    if (this.loggedUser.userType === UserType.AIRLINE) {
      this.initializeAirlineContactDataListener();
    }

    if (!this.isAcdmd) {
      this.initializeTocaListener();
      this.initializeNetReportingChangesListener();
      this.initializeAdmForChangesListener();
    }
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private initializeLoggedUser() {
    this.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });
  }

  private getAgentsListObservable(bspId: number): Observable<DropdownOption<AgentSummary>[]> {
    let agentListResult;

    switch (this.loggedUser.userType) {
      case UserType.AGENT:
        agentListResult = this.isAcdmd ? of(null) : this.getAgentList(bspId);
        break;
      case UserType.AGENT_GROUP:
        agentListResult = this.getAgentFilterDropdownForAgentGroup(bspId);
        break;
      default:
        agentListResult = this.formConfig.isAcdmRequest ? of(null) : this.getAgentList(bspId);
    }

    return agentListResult;
  }

  private getAgentList(bspId) {
    const filters = this.getFilterByMasterDataType(bspId);

    return this.agentDictionaryService
      .getDropdownOptions(filters)
      .pipe(map(agents => toValueLabelObjectDictionaryId(agents)));
  }

  private getFilterByMasterDataType(bspId): AgentDictionaryQueryFilters {
    const queryfilters = { bspId };

    return this.formConfig.isAcdm || this.formConfig.isSpcdr ? { ...queryfilters, active: true } : queryfilters;
  }

  private getAgentFilterDropdownForAgentGroup(bspId): Observable<DropdownOption<any>[]> {
    const queryfilters = { bspId };
    const aGroupUserPermission = getAgentGroupAcdmqPermission(this.admAcmType);
    const aGroupUserHasPermission = this.loggedUser.permissions.some(perm => aGroupUserPermission === perm);

    if (aGroupUserHasPermission) {
      const filter = { ...queryfilters, permission: aGroupUserPermission };

      return this.agentDictionaryService
        .getDropdownOptions(filter)
        .pipe(map(agents => toValueLabelObjectDictionaryId(agents)));
    } else {
      return of(null);
    }
  }

  private getAirlinesListObservable(bspId: number): Observable<DropdownOption<number>[]> {
    // TODO Change endpoint called when acdmType === 'SPCR | SPDR'
    return this.loggedUser.userType !== UserType.AIRLINE
      ? this.airlineDictionaryService
          .getDropdownOptions(bspId)
          .pipe(map(airlines => toValueLabelObjectDictionaryId(airlines)))
      : of(null);
  }

  private getBspsDropdownOptions$(): Observable<DropdownOption<BspDto>[]> {
    let permissions: string[] = [];

    switch (this.formConfig.accessType) {
      case AccessType.create:
      case AccessType.editTicket:
        permissions = this.formConfig.issuingPendingSupervision
          ? [getIssueAcdmPendingSupervisionPermission(this.admAcmType)]
          : [getIssueAllAcdmSpcdrAcdmdPermission(this.admAcmType, this.loggedUser.userType)];
        break;

      case AccessType.edit:
        permissions = [getUpdateAcdmPermissions(this.admAcmType)];
        break;
    }

    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, permissions)
      .pipe(map(bsps => bsps.map(toValueLabelObjectBsp)));
  }

  private initializeAdmSettingsForm$(): Observable<any> {
    return combineLatest([
      this.formConfig.getAdmaFor(),
      this.formConfig.getTocaTypes(this.bspSelected.id),
      this.getCurrenciesFromBspObservable(this.bspSelected.id),
      this.formConfig.getStatOptions()
    ]).pipe(
      first(),
      tap(optionsRetrieved => {
        this.admaForOptions = optionsRetrieved[0];
        this.initializeToca(optionsRetrieved[1]);
        this.initializeCurrencies(optionsRetrieved[2] as CurrencyViewModel[]);
        this.initializeStat(optionsRetrieved[3]);
      })
    );
  }

  private initializeBspsDropdown(bsps: DropdownOption<BspDto>[], selectedBsp: BspDto) {
    this.bspOptions = bsps;

    const bsp = selectedBsp || bsps[0]?.value;

    if (bsp && this.formConfig.isCreateMode) {
      FormUtil.get<BasicInformationViewModel>(this.form, 'bspName').setValue(bsp);
      this.onBspChange$(bsp)?.subscribe();
    }
  }

  private initializeAgentsDropdown(agents: DropdownOption<AgentSummary>[]) {
    this.agentSelected = null;
    switch (this.loggedUser.userType) {
      case UserType.AGENT:
        if (this.isAcdmd || this.formConfig.isAcdmRequest || this.formConfig.isSpcdr) {
          this.setDefaultAgent();
        }
        break;
      case UserType.AIRLINE:
        if (this.isAcdmd) {
          this.setDefaultAirline();
        }
        this.agentOptions = agents;
        break;
      case UserType.AGENT_GROUP:
        if (agents?.length && this.formConfig.isAcdmRequest) {
          this.agentOptions = agents;
        }
        break;
      default:
        if (!this.formConfig.isAcdmRequest) {
          this.agentOptions = agents;
        } else {
          this.setDefaultAgent();
        }
    }
  }

  private initializeAirlinesDropdown(airlines: DropdownOption<AirlineSummary>[]) {
    this.airlineSelected = null;

    if (this.loggedUser.userType === UserType.AIRLINE) {
      const airlUser = this.loggedUser;
      const airlineUserId = airlUser.globalAirline.airlines.find(airline => airline.bsp.id === this.bspSelected.id)?.id;

      this.onAirlineChange(airlineUserId);
    } else {
      this.airlineOptions = airlines;
      this.setDefaultAirline();
    }
  }

  private initializeAgentChangesListener() {
    const subscription = this.agentForm.get('id').valueChanges.subscribe(agentId => {
      this.onAgentChange(agentId);
    });

    this.subscriptions.push(subscription);
  }

  private initializeAirlineChangesListener() {
    const subscription = FormUtil.get<AirlineViewModel>(this.airlineForm, 'id').valueChanges.subscribe(airlineId => {
      this.onAirlineChange(airlineId);
    });

    this.subscriptions.push(subscription);
  }

  private initializeCurrencyChangesListener() {
    const subscription = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'currency')
      .valueChanges.pipe(map(currency => currency.id))
      .subscribe(currencyId => {
        this.onCurrencyChange(currencyId);
      });

    this.subscriptions.push(subscription);
  }

  private initializeNetReportingChangesListener() {
    const netReportingControl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'netReporting');

    this.subscriptions.push(netReportingControl.valueChanges.subscribe(value => this.netReportingChange.emit(value)));
  }

  private initializeAdmForChangesListener() {
    const admaForControl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'concernsIndicator');

    this.subscriptions.push(
      admaForControl.valueChanges.subscribe(() => {
        this.admaForChange.emit(admaForControl.value);
      })
    );
  }

  private initializeTocaListener() {
    const tocaCtrl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'taxOnCommissionType');

    this.subscriptions.push(
      this.formConfig.validateToca().subscribe(res => {
        if (TocaErrorsType.type === res) {
          tocaCtrl.setErrors({ required: true });
        } else {
          tocaCtrl.setErrors(null);
        }
      }),
      tocaCtrl.valueChanges.subscribe(() => this.tocaTypeChange.emit(tocaCtrl.value))
    );
  }

  private getCurrenciesFromBspObservable(bspId: number): Observable<CurrencyViewModel[]> {
    return this.admAcmService.getCurrencies(bspId, true);
  }

  private async initializeCurrencies(currencies: CurrencyViewModel[]) {
    this.currencyList = currencies;
    this.currencyOptions = this.currencyList.map(currency => ({ value: currency.id, label: currency.code }));

    this.setDefaultCurrency();
  }

  private initializeToca(tocaTypes: any[] = []) {
    this.tocaTypeOptions = tocaTypes;

    if (this.formConfig.isEditMode) {
      this.formData.whenFormIsReady().subscribe(() => {
        const tocaValue = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'taxOnCommissionType').value;
        if (tocaValue && !tocaTypes.includes(tocaValue)) {
          this.tocaTypeOptions.push({ label: tocaValue, value: tocaValue });
        }

        this.formData.getSubject(AdmAcmActionEmitterType.hasToca).next(this.tocaTypeOptions.length > 0);
      });
    } else {
      this.formData.getSubject(AdmAcmActionEmitterType.hasToca).next(this.tocaTypeOptions.length > 0);
    }
  }

  private initializeStat(stats: string | { value: string; label: string; selected: boolean }[]) {
    this.statOptions = [];
    let defaultValue = stats;

    if (Array.isArray(stats)) {
      this.statOptions = stats;
      const defaultStat = stats.find(stat => stat.selected);
      defaultValue = defaultStat ? defaultStat.value : null;
    }

    const statControl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'statisticalCode');
    if (statControl.value === null) {
      statControl.setValue(defaultValue, { emitEvent: false });
    }
  }

  private initializeSectionForms(): void {
    this.admSettingsForm = this.formFactory.createGroup<AdmSettingViewModel>({
      concernsIndicator: ['', Validators.required],
      taxOnCommissionType: null,
      currency: this.formFactory.createGroup<CurrencyViewModel>({
        id: ['', Validators.required],
        code: '',
        decimals: '',
        isDefault: ''
      }),
      statisticalCode: [null, [Validators.pattern(/^( {0,3}|([IDid][ A-Za-z0-9]{0,2})?)$/)]],
      netReporting: false
    });

    this.agentForm = this.formFactory.createGroup<AgentViewModel>({
      id: ['', [Validators.required]],
      vatNumber: ['', [Validators.maxLength(15)]],
      iataCode: '',
      name: '',
      bsp: {
        id: '',
        isoCountryCode: '',
        name: ''
      },
      address: {
        street: '',
        city: '',
        country: '',
        postalCode: ''
      },
      acdmVatNumber: ['', [Validators.maxLength(15)]]
    });

    this.airlineForm = this.formFactory.createGroup<AirlineViewModel>({
      vatNumber: ['', [Validators.maxLength(15)]],
      iataCode: '',
      id: ['', [Validators.required]],
      localName: '',
      globalName: '',
      bsp: {
        id: '',
        isoCountryCode: '',
        name: ''
      },
      contact: {
        email: '',
        firstName: '',
        lastName: '',
        telephone: ''
      },
      address: {
        address1: '',
        address2: '',
        city: '',
        country: '',
        postalCode: '',
        state: ''
      },
      acdmVatNumber: ['', [Validators.maxLength(15)]]
    });

    this.airlineContactForm = this.formFactory.createGroup<ContactViewModel>({
      contactName: ['', [Validators.required, Validators.maxLength(49)]],
      phoneFaxNumber: ['', [Validators.required, Validators.maxLength(30)]],
      email: this.fb.control('', {
        validators: [Validators.required, multiEmailValidator]
      })
    });

    if (this.isAcdmd) {
      this.adaptFormAcdmd();
    }
  }

  private adaptFormAcdmd() {
    FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'concernsIndicator').disable();
    FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'netReporting').disable();
    FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'taxOnCommissionType').disable();
  }

  private onBspChange$(bsp: BspDto): Observable<any> {
    this.bspSelected = bsp;
    this.bspChange.emit(this.bspSelected);

    return this.updateInfoBspFields$(bsp.id);
  }

  private onAgentChange(agentId: number) {
    //* An agent has been selected
    if (agentId) {
      this.subscriptions.push(
        this.getUserAgentInfo(agentId).subscribe(info => {
          this.agentSelected = null;
          if (info) {
            this.agentSelected = {
              ...(info.agent as AgentViewModel),
              acdmVatNumber: info.agentVatNumber,
              address: info.agentAddress,
              contact: info.agentContact
            };

            if (this.loggedUser.userType === UserType.AGENT && this.formConfig.isCreateMode) {
              this.agentOptions = [toValueLabelObjectAgentId(this.agentSelected)];
            }

            this.agentForm.reset({ ...this.agentSelected }, { emitEvent: false });

            if (this.isAcdmd) {
              this.initializeContactDataStored();
            }

            this.agentChange.emit(this.agentSelected);

            this.cd.markForCheck();
          }
        })
      );
    }
  }

  private getUserAgentInfo(agentId: number): Observable<BasicAgentInfo> {
    if (this.isAcdmd && this.loggedUser.userType === UserType.AGENT) {
      return this.admAcmService.getBspAgentInfo(agentId);
    }

    return this.admAcmService.getAgentInfo(agentId);
  }

  private onAirlineChange(airlineId: number): void {
    this.airlineSelected = null;
    if (airlineId) {
      this.subscriptions.push(
        this.getUserAirlineInfo(airlineId).subscribe(info => {
          if (info) {
            this.airlineSelected = {
              ...(info.airline as AirlineViewModel),
              address: info.airlineAddress,
              contact: info.airlineContact,
              acdmVatNumber: info.airlineVatNumber
            };

            if (this.loggedUser.userType === UserType.AIRLINE) {
              this.airlineOptions = [toValueLabelObjectAirlineId(this.airlineSelected)];
            }

            this.airlineForm.reset({ ...this.airlineSelected }, { emitEvent: false });

            this.initializeContactDataStored();

            this.formData.getSubject(AdmAcmActionEmitterType.airline).next(this.airlineSelected);
            this.cd.markForCheck();
          }
        })
      );
    }
  }

  private getUserAirlineInfo(airlineId: number): Observable<BasicAirlineInfo> {
    if (this.isAcdmd && this.loggedUser.userType === UserType.AIRLINE) {
      return this.admAcmService.getBspAirlineInfo(airlineId);
    }

    return this.admAcmService.getAirlineInfo(airlineId);
  }

  private onNetReportingChange(value: boolean) {
    this.netReportingChange.emit(value);
  }

  private onCurrencyChange(id: number) {
    const currencySelected: CurrencyViewModel = cloneDeep(this.currencyList.find(curr => curr.id === id)) || null;
    const currencyForm = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'currency');

    currencyForm.reset(currencySelected ? currencySelected : {}, { emitEvent: false });

    currencyForm.get('id').updateValueAndValidity({ onlySelf: true });

    this.currencyChange.emit(currencySelected);
  }

  private updateInfoBspFields$(bspId: number) {
    this.agentForm.reset({}, { onlySelf: true });
    this.airlineForm.reset({}, { onlySelf: true });
    this.admSettingsForm.reset({}, { onlySelf: true });

    return combineLatest([this.initializeDropdownsFromBsp$(bspId), this.initializeAdmSettingsForm$()]);
  }

  /**
   * Initializes Agent & Airline dropdowns from BSP
   */
  private initializeDropdownsFromBsp$(bspId: number): Observable<any> {
    return combineLatest([this.getAgentsListObservable(bspId), this.getAirlinesListObservable(bspId)]).pipe(
      first(),
      map(pagedDataArray => pagedDataArray.map(pagedData => (pagedData ? pagedData : []))),
      tap(records => {
        this.initializeAgentsDropdown(records[0] as DropdownOption<AgentSummary>[]);
        this.initializeAirlinesDropdown(records[1] as DropdownOption<AirlineSummary>[]);
      })
    );
  }

  private setDefaultCurrency() {
    if (!this.ticketIssueMode) {
      this.currencyList.forEach(currency => {
        if (this.currencyList.length === 1 || currency.isDefault) {
          this.onCurrencyChange(currency.id);
        }
      });
    }

    if (this.ticketIssueMode) {
      const ticketCurrencyCode = this.formTicket.getTicketCurrency().code;
      const ticketCurrency = this.currencyList.find(currency => currency.code === ticketCurrencyCode);
      this.onCurrencyChange(ticketCurrency.id);
    }
  }

  private setDefaultAirline() {
    //* Airline pre-selected from airlineList if there is only one in the BSP selected
    if (this.airlineOptions && this.airlineOptions.length === 1) {
      FormUtil.get<AirlineViewModel>(this.airlineForm, 'id').setValue(this.airlineOptions[0].value);
      this.cd.markForCheck();
    }
  }

  private setDefaultAgent() {
    if (this.loggedUser.userType === UserType.AGENT) {
      const agent = this.loggedUser.agent;

      this.onAgentChange(agent.id);
    }
  }

  public toggleAirlineDetails() {
    this.showAirlineDetails = !this.showAirlineDetails;
  }

  public toggleAgentDetails() {
    this.showAgentDetails = !this.showAgentDetails;
  }

  public getUserSelectedDetails(address: AgentAddress | AirlineAddress) {
    return getUserDetails(address);
  }

  public areContactFieldsLocked(): boolean {
    return !this.bspSelected || this.admAcmType === MasterDataType.Admq || this.admAcmType === MasterDataType.Acmq;
  }

  private initializeFieldsVisibility() {
    this.initializeVatFieldsVisibility();
    this.initializeNetReportingVisibility();
  }

  private initializeVatFieldsVisibility() {
    this.isAirlineVatFieldVisible$ = of(false);
    this.isAgentVatFieldVisible$ = of(false);

    this.formData.whenFormIsReady().subscribe(() => {
      const airlineAcdmVatNumberCtrl = FormUtil.get<AirlineViewModel>(this.airlineForm, 'acdmVatNumber');
      const agentAcdmVatNumberCtrl = FormUtil.get<AgentViewModel>(this.agentForm, 'acdmVatNumber');

      this.isAirlineVatFieldVisible$ = this.formConfig
        .isAirlineVatFieldVisible(airlineAcdmVatNumberCtrl.value)
        .pipe(tap(fieldVisible => this.toggleControlDisable(airlineAcdmVatNumberCtrl, !fieldVisible)));
      this.isAgentVatFieldVisible$ = this.formConfig
        .isAgentVatFieldVisible(agentAcdmVatNumberCtrl.value)
        .pipe(tap(fieldVisible => this.toggleControlDisable(agentAcdmVatNumberCtrl, !fieldVisible)));
    });
  }

  private initializeNetReportingVisibility() {
    this.isNetReportingVisible$ = of(false);

    this.formData.whenFormIsReady().subscribe(() => {
      const netReportingCtrl = FormUtil.get<AdmSettingViewModel>(this.admSettingsForm, 'netReporting');

      this.isNetReportingVisible$ = this.formConfig.isNetReportingVisible(netReportingCtrl.value);
    });
  }

  private toggleControlDisable(control: AbstractControl, isDisabled: boolean) {
    if (isDisabled) {
      control.disable();
    } else {
      control.enable();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
