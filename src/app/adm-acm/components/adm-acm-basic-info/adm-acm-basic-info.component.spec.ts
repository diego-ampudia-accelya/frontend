import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { admAcmServiceStub, initialState } from '../adm-acm/adm-acm-test.config';
import { AdmAcmBasicInfoComponent } from './adm-acm-basic-info.component';
import { AdmAcmStatus } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmActionEmitterType } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { commonAcdmPermission } from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { getAirlineContactData } from '~app/adm-acm/store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Airline } from '~app/master-data/models';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import {
  AgentViewModel,
  AirlineViewModel,
  BasicInformationViewModel,
  ContactViewModel
} from '~app/refund/models/refund-view-aux.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { CheckboxComponent, InputComponent, SelectComponent } from '~app/shared/components';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { FormUtil } from '~app/shared/helpers';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { AirlineUser, User, UserType } from '~app/shared/models/user.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  AppConfigurationService,
  TabService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('AdmAcmBasicInfoComponent', () => {
  let component: AdmAcmBasicInfoComponent;
  let fixture: ComponentFixture<AdmAcmBasicInfoComponent>;
  let mockStore: MockStore<AppState>;

  const contact: ContactViewModel = { contactName: 'name', email: 'email@email.com', phoneFaxNumber: '1231231' };

  const currenciesMock = [
    { code: 'EUR', decimals: 2, id: 1, isDefault: true },
    { code: 'JOD', decimals: 3, id: 2, isDefault: false }
  ];

  const bspMock: BspDto = {
    id: 1,
    name: 'bspName',
    isoCountryCode: 'ES'
  };

  const userAirline: User = {
    ...createAirlineUser(),
    userType: UserType.AIRLINE
  };

  const userAgentGroup: User = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AGENT_GROUP
  };

  const userIata: User = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.IATA
  };

  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const admAcmTicketServiceSpy = createSpyObject(AdmAcmTicketService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmBasicInfoComponent, InputComponent, SelectComponent, CheckboxComponent],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getAirlineContactData, value: contact }]
        }),
        DialogService,
        AdmAcmDialogService,
        PermissionsService,
        L10nTranslationService,
        HttpClient,
        FormBuilder,
        HttpTestingController,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        { provide: AdmAcmTicketService, useValue: admAcmTicketServiceSpy },
        TabService,
        AdmAcmDataService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmBasicInfoComponent);
    component = fixture.componentInstance;

    mockStore = TestBed.inject<any>(Store);

    component.admAcmService = admAcmServiceStub;
    component.admAcmService.getBspsList = jasmine.createSpy().and.returnValue(of());
    component.formData = new AdmAcmDataService(TestBed.inject(FormBuilder));
    component.formConfig = createAdmAcmConfigService(
      component.formData,
      TestBed.inject(AdmAcmDialogService),
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Router),
      TestBed.inject(Store),
      TestBed.inject(TabService)
    );

    component.formConfig.accessType = AccessType.create;
    component.formConfig.admAcmType = MasterDataType.Adm;
    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(of([]));

    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should propagate currency change', done => {
    component['initializeCurrencies'](currenciesMock); //* Initializes currencyList calling getCurrencies
    component.admSettingsForm.get('currency.id').enable({ emitEvent: false }); //* Enabling id control (which is disabled while agent is not selected)

    spyOn(component.currencyChange, 'emit').and.callThrough();
    component.admSettingsForm.get('currency').setValue(currenciesMock[1]); //* Simulating click

    fixture.whenStable().then(() => {
      expect(component.currencyChange.emit).toHaveBeenCalledWith(currenciesMock[1]);
      done();
    });
  });

  it('should set the default currency properly', done => {
    spyOn<any>(component, 'onCurrencyChange');
    component['initializeCurrencies'](currenciesMock); //* Initializes currencyList calling getCurrencies

    fixture.whenStable().then(() => {
      expect(component['onCurrencyChange']).toHaveBeenCalledWith(currenciesMock[0].id);
      done();
    });
  });

  describe('isAgentDropdownLocked', () => {
    it('should isAgentDropdownLocked return true when theres is no bspSelected', fakeAsync(() => {
      component.bspSelected = null;

      expect(component.isAgentDropdownLocked).toBeTruthy();
    }));

    it('should isAgentDropdownLocked return true when there is only 1 agentOption', fakeAsync(() => {
      const agentOptionsMock: DropdownOption<any>[] = [
        { value: { id: '1', name: 'AGENT 78200010', code: '78200010' }, label: 'AGENT 78200010' }
      ];

      component.bspSelected = bspMock;
      component.agentOptions = agentOptionsMock;

      expect(component.isAgentDropdownLocked).toBeTruthy();
    }));

    it('should isAgentDropdownLocked return false when bspSelected is not null', fakeAsync(() => {
      component.bspSelected = bspMock;

      expect(component.isAgentDropdownLocked).toBeFalsy();
    }));
  });

  describe('isPendingAuthorization$', () => {
    it('should isPendingAuthorization$ be true if status is pendingAuthorization', fakeAsync(() => {
      spyOn<any>(component.formData, 'getStatus').and.returnValue(of(AdmAcmStatus.pendingAuthorization));

      component.ngOnInit();

      let isPendingAuthorization: boolean;
      component.isPendingAuthorization$.subscribe(value => (isPendingAuthorization = value));
      tick();

      expect(isPendingAuthorization).toBeTruthy();
    }));
  });

  it('should call emitSecondaryOutputs if AccessType is create', fakeAsync(() => {
    component.formConfig.accessType = AccessType.create;
    spyOn<any>(component, 'emitSecondaryOutputs');
    spyOn<any>(component, 'fetchInitialDataForm').and.returnValue(of({}));

    component.ngOnInit();
    tick();

    expect(component['emitSecondaryOutputs']).toHaveBeenCalled();
  }));

  it('should call initializeFormListeners if AccessType is edit', fakeAsync(() => {
    component.formConfig.accessType = AccessType.edit;
    spyOn<any>(component, 'initializeFormListeners');
    spyOn<any>(component, 'fetchInitialDataForm').and.returnValue(of({}));
    spyOn<any>(component.formData, 'whenFormIsReady').and.returnValue(of(true));
    spyOn<any>(component, 'onEditAccess').and.returnValue(of(true));

    component.ngOnInit();

    tick();

    expect(component['initializeFormListeners']).toHaveBeenCalled();
  }));

  describe('onEditAccess', () => {
    it('should be call onBspChange$ when is bsp', fakeAsync(() => {
      const mockBsp = { id: 2, isoCountryCode: 'MT', name: 'MALTA' };

      FormUtil.get<AgentViewModel>(component.agentForm, 'bsp').setValue(mockBsp);

      spyOn<any>(component, 'onBspChange$').and.returnValue(of());

      component['onEditAccess']();

      expect(component['onBspChange$']).toHaveBeenCalledWith(mockBsp);
    }));

    it('should be change agent when is called onAgentChange with agentId', fakeAsync(() => {
      spyOn<any>(component, 'onBspChange$').and.returnValue(of({}));
      spyOn<any>(component, 'onAgentChange');

      FormUtil.get<AgentViewModel>(component.agentForm, 'id').setValue(10);

      component['onEditAccess']().subscribe();
      tick();

      expect(component['onAgentChange']).toHaveBeenCalledWith(10);
    }));

    it('should be called onEditTicketAccess$ when ticketIssueMode is not null', fakeAsync(() => {
      spyOn<any>(component, 'onBspChange$').and.returnValue(of({}));
      spyOn<any>(component, 'onEditTicketAccess$').and.returnValue(of({}));

      component.ticketIssueMode = true;

      component['onEditAccess']().subscribe();
      tick();

      expect(component['onEditTicketAccess$']).toHaveBeenCalled();
    }));

    it('should update Acdm For On TicketAccess', fakeAsync(() => {
      spyOn<any>(component, 'updateAcdmForOnTicketAccess');

      component['onEditTicketAccess$']().subscribe();
      tick();

      expect(component['updateAcdmForOnTicketAccess']).toHaveBeenCalled();
    }));

    //TODO analize if this is a random test
    xit('should emit bsp selected when onBspChange$ is called', fakeAsync(() => {
      const mockBsp = { id: 2, isoCountryCode: 'MT', name: 'MALTA' };
      spyOn<any>(component, 'getAgentList').and.returnValue(of({}));

      const bspChangeSpyEmitter = spyOn(component.bspChange, 'emit');
      component['onBspChange$'](mockBsp).subscribe(() => {});
      tick();

      expect(component.bspSelected).toBe(mockBsp);
      expect(bspChangeSpyEmitter).toHaveBeenCalled();
    }));
  });

  describe('initializeContactDataStored', () => {
    it('should set agent type to the contact when isAcdmd and user type is agent', fakeAsync(() => {
      const userAgent: User = {
        ...createAirlineUser(),
        agent: {
          id: 1,
          name: 'test',
          iataCode: '007',
          effectiveFrom: '2020-01-01',
          bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
        },
        userType: UserType.AGENT
      };

      const agentSelected: AgentViewModel = {
        id: 2,
        name: 'AGENT 2222222',
        iataCode: '2222222',
        vatNumber: '1111111',
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
        address: {
          street: null,
          city: null,
          state: null,
          country: null,
          postalCode: null,
          telephone: null
        },
        contact
      };

      mockStore.overrideSelector(getAirlineContactData, contact);
      component.loggedUser = userAgent;
      component.formData.isFormReady.next(true);
      component.isAcdmd = true;
      component.agentSelected = agentSelected;

      component['initializeContactDataStored']();
      tick();

      mockStore.select(getAirlineContactData).subscribe(data => expect(data).toEqual(contact));
    }));

    it('should set agent type to the contact when isAcdmd and user type is not agent or airline', fakeAsync(() => {
      const airlineSelected: AirlineViewModel = {
        id: 1111,
        localName: 'AGENT 2222222',
        globalName: 'AGENT 2222222',
        iataCode: '2222222',
        vatNumber: '1111111',
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
        address: {
          city: null,
          state: null,
          country: null,
          postalCode: null,
          telephone: null
        },
        contact
      };

      mockStore.overrideSelector(getAirlineContactData, contact);
      component.loggedUser = userAgentGroup;
      component.formData.isFormReady.next(true);
      component.airlineSelected = airlineSelected;

      component['initializeContactDataStored']();
      tick();

      mockStore.select(getAirlineContactData).subscribe(data => {
        expect(data).toEqual(component.airlineSelected.contact);
      });
    }));
  });

  it('should initialize Bsps Dropdown when fetch Initial Data Form is called', fakeAsync(() => {
    spyOn<any>(component, 'getBspsDropdownOptions$').and.returnValue(of({}));
    spyOn<any>(component, 'initializeBspsDropdown').and.callThrough();

    component['fetchInitialDataForm']().subscribe();
    tick();

    expect(component['initializeBspsDropdown']).toHaveBeenCalled();
  }));

  it('should dispatch update action when initializeAirlineContactDataListener and form is valid', done => {
    component['initializeAirlineContactDataListener']();
    component.airlineContactForm.patchValue(contact);

    spyOn(mockStore, 'dispatch');
    component.airlineContactForm.get('contactName').setValue('test contact'); //* Simulating change

    fixture.whenStable().then(() => {
      expect(mockStore.dispatch).toHaveBeenCalled();
      done();
    });
  });

  it('should not dispatch update action when initializeAirlineContactDataListener and form is not valid', done => {
    component['initializeAirlineContactDataListener']();

    spyOn(mockStore, 'dispatch');
    component.airlineContactForm.get('contactName').setValue('test contact'); //* Simulating change

    fixture.whenStable().then(() => {
      expect(mockStore.dispatch).not.toHaveBeenCalled();
      done();
    });
  });

  describe('getAgentsListObservable', () => {
    const userAgent: User = {
      ...createAirlineUser(),
      agent: {
        id: 1,
        name: 'test',
        iataCode: '007',
        effectiveFrom: '2020-01-01',
        bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
      },
      userType: UserType.AGENT
    };

    it('should be called getAgentList method if user type is agent and isAcdmd is false', fakeAsync(() => {
      spyOn<any>(component, 'getAgentList').and.returnValue(of({}));

      component.isAcdmd = false;
      component.loggedUser = userAgent;

      component['getAgentsListObservable'](bspMock.id);
      expect(component['getAgentList']).toHaveBeenCalled();
    }));

    it('should not be called getAgentList method if user type is agent and isAcdmd is true', fakeAsync(() => {
      spyOn<any>(component, 'getAgentList').and.returnValue(of({}));

      component.isAcdmd = true;
      component.loggedUser = userAgent;

      component['getAgentsListObservable'](bspMock.id);
      expect(component['getAgentList']).not.toHaveBeenCalled();
    }));

    it('should be called getAgentFilterDropdownForAgentGroup if user type is agent group', fakeAsync(() => {
      spyOn<any>(component, 'getAgentFilterDropdownForAgentGroup').and.returnValue(of({}));

      component.loggedUser = userAgentGroup;

      component['getAgentsListObservable'](bspMock.id);
      expect(component['getAgentFilterDropdownForAgentGroup']).toHaveBeenCalled();
    }));

    it('should be called getAgentList if user type is not agent or agent group and isAcdmRequest is true', fakeAsync(() => {
      spyOn<any>(component, 'getAgentList').and.returnValue(of({}));

      component.loggedUser = userAirline;
      component.formConfig.admAcmType = MasterDataType.Acmq;

      component['getAgentsListObservable'](bspMock.id);

      expect(component['getAgentList']).not.toHaveBeenCalled();
    }));

    it('should be called getAgentList if user type is not agent or agent group and isAcdmRequest is false', fakeAsync(() => {
      spyOn<any>(component, 'getAgentList').and.returnValue(of({}));

      component.formConfig.admAcmType = MasterDataType.Acm;
      component.loggedUser = userAirline;

      component['getAgentsListObservable'](bspMock.id);

      expect(component['getAgentList']).toHaveBeenCalled();
    }));
  });

  it('should call getFilterByMasterDataType on getAgentList', () => {
    spyOn<any>(component, 'getFilterByMasterDataType').and.returnValue(of({}));
    spyOn<any>(component, 'getAgentList').and.callThrough();

    const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
      {
        value: {
          id: '1',
          name: 'AGENT 1111111',
          code: '1111111',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        },
        label: '1111111 / AGENT 1111111'
      }
    ];

    agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));

    component['getAgentList'](bspMock.id).subscribe();

    fixture.detectChanges();

    expect(component['getFilterByMasterDataType']).toHaveBeenCalledWith(bspMock.id);
  });

  describe('getFilterByMasterDataType', () => {
    it('should call getFilterByMasterDataType and return active true when isAcdm', () => {
      const queryFilters = { bspId: bspMock.id };
      const result = component['getFilterByMasterDataType'](bspMock.id);

      expect(result).toEqual({ ...queryFilters, active: true });
    });

    it('should call getFilterByMasterDataType and return active true when isSpcdr', () => {
      component.formConfig.admAcmType = MasterDataType.Spdr;
      const queryFilters = { bspId: bspMock.id };
      const result = component['getFilterByMasterDataType'](bspMock.id);

      expect(result).toEqual({ ...queryFilters, active: true });
    });

    it('should call getFilterByMasterDataType and return queryfilters when is not isAcdm or isSpcdr', () => {
      component.formConfig.admAcmType = MasterDataType.Acnt;

      const queryFilters = { bspId: bspMock.id };
      const result = component['getFilterByMasterDataType'](bspMock.id);

      expect(result).toEqual(queryFilters);
    });
  });

  describe('getAgentFilterDropdownForAgentGroup', () => {
    it('should call getDropdownOptions with the correct filter if admAcmType is cAdmq', () => {
      const userAgent: User = {
        ...createAirlineUser(),
        agent: {
          id: 1,
          name: 'test',
          iataCode: '007',
          effectiveFrom: '2020-01-01',
          bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
        },
        permissions: ['rAdm', 'rAdmIntCom', 'cAdmq'],
        userType: UserType.AGENT
      };

      const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
        {
          value: {
            id: '1',
            name: 'AGENT 1111111',
            code: '1111111',
            bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
          },
          label: '1111111 / AGENT 1111111'
        }
      ];

      component.formConfig.admAcmType = MasterDataType.Admq;
      component.loggedUser = userAgent;

      const queryFilters = { bspId: bspMock.id };
      const filter = { ...queryFilters, permission: commonAcdmPermission.admqIssue };

      agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));

      component['getAgentFilterDropdownForAgentGroup'](bspMock.id).subscribe();

      fixture.detectChanges();

      expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith(filter);
    });

    it('should not be called getDropdownOptions if admAcmType is not cAdmq', fakeAsync(() => {
      const userAgent: User = {
        ...createAirlineUser(),
        agent: {
          id: 1,
          name: 'test',
          iataCode: '007',
          effectiveFrom: '2020-01-01',
          bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
        },
        permissions: [],
        userType: UserType.AGENT
      };

      component.formConfig.admAcmType = null;
      component.loggedUser = userAgent;

      spyOn<any>(component, 'getAgentFilterDropdownForAgentGroup').and.callThrough();

      let result;
      component['getAgentFilterDropdownForAgentGroup'](bspMock.id).subscribe(data => (result = data));
      tick();

      expect(result).toBeNull();
    }));
  });

  describe('getAirlinesListObservable', () => {
    it('should be called getDropdownOptions with bsp id if is not an airline user', fakeAsync(() => {
      const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
        {
          value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
          label: '001 / AIRLINE 001'
        },
        {
          value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
          label: '002 / AIRLINE 002'
        }
      ];

      const userAgent: User = {
        ...createAirlineUser(),
        agent: {
          id: 1,
          name: 'test',
          iataCode: '007',
          effectiveFrom: '2020-01-01',
          bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
        },
        permissions: ['rAdm', 'rAdmIntCom', 'cAdmq'],
        userType: UserType.AGENT
      };

      airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));

      component.loggedUser = userAgent;

      component['getAirlinesListObservable'](bspMock.id).subscribe();
      tick();

      expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith(bspMock.id);
    }));

    it('should not be called getDropdownOptions if is an airline user', fakeAsync(() => {
      spyOn<any>(component, 'getAirlinesListObservable').and.callThrough();

      component.loggedUser = userAirline;

      let result;
      component['getAirlinesListObservable'](bspMock.id).subscribe(data => (result = data));
      tick();

      expect(result).toBeNull();
    }));
  });

  describe('initializeBspsDropdown', () => {
    it('should call onBspChange$ when receive a bsp selected', () => {
      const selectedBsp: BspDto = {
        id: 1,
        name: 'SPAIN',
        isoCountryCode: 'ES'
      };

      spyOn<any>(component, 'onBspChange$');

      component.formConfig.accessType = AccessType.create;
      FormUtil.get<BasicInformationViewModel>(component.form, 'bspName').patchValue(selectedBsp);

      component['initializeBspsDropdown']([], selectedBsp);

      expect(component['onBspChange$']).toHaveBeenCalled();
    });

    it('should call onBspChange$ when receive a bsp dropdown list', () => {
      const bspsMock: DropdownOption<BspDto>[] = [
        {
          value: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
          label: 'ES - SPAIN'
        }
      ];

      spyOn<any>(component, 'onBspChange$');

      component.formConfig.accessType = AccessType.create;
      FormUtil.get<BasicInformationViewModel>(component.form, 'bspName').patchValue(bspsMock[0].value);

      component['initializeBspsDropdown'](bspsMock, null);

      expect(component['onBspChange$']).toHaveBeenCalledWith(bspsMock[0].value);
    });
  });

  describe('initializeAgentsDropdown', () => {
    const userAgent: User = {
      ...createAirlineUser(),
      agent: {
        id: 1,
        name: 'test',
        iataCode: '007',
        effectiveFrom: '2020-01-01',
        bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
      },
      permissions: ['rAdm', 'rAdmIntCom', 'cAdmq'],
      userType: UserType.AGENT
    };

    const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
      {
        value: {
          id: '1',
          name: 'AGENT 1111111',
          code: '1111111',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        },
        label: '1111111 / AGENT 1111111'
      }
    ];

    it('should call setDefaultAgent when conditions are met', () => {
      spyOn<any>(component, 'setDefaultAgent');

      component.loggedUser = userAgent;
      component.formConfig.admAcmType = MasterDataType.Acmq;
      component.formConfig.admAcmType = MasterDataType.Spcr;

      component['initializeAgentsDropdown'](mockAgentDropdownOptions);

      expect(component['setDefaultAgent']).toHaveBeenCalled();
    });

    it('should not call setDefaultAgent when conditions are not met', () => {
      spyOn<any>(component, 'setDefaultAgent');

      component.loggedUser = userAgent;
      component['initializeAgentsDropdown'](mockAgentDropdownOptions);

      expect(component['setDefaultAgent']).not.toHaveBeenCalled();
    });

    it('should call setDefaultAgent when the user is airline and isAcdmd', () => {
      spyOn<any>(component, 'setDefaultAirline');

      component.loggedUser = userAirline;
      component.isAcdmd = true;

      component['initializeAgentsDropdown'](mockAgentDropdownOptions);

      expect(component['setDefaultAirline']).toHaveBeenCalled();
      expect(component.agentOptions).toEqual(mockAgentDropdownOptions);
    });

    it('should not call setDefaultAgent when the user is airline and not isAcdmd', () => {
      spyOn<any>(component, 'setDefaultAirline');

      component.loggedUser = userAirline;
      component.isAcdmd = false;

      component['initializeAgentsDropdown'](mockAgentDropdownOptions);

      expect(component['setDefaultAirline']).not.toHaveBeenCalled();
    });

    it('should set agentOptions if user is agentGroup and conditions are met', () => {
      component.loggedUser = userAgentGroup;
      component.formConfig.admAcmType = MasterDataType.Acmq;

      component['initializeAgentsDropdown'](mockAgentDropdownOptions);

      expect(component.agentOptions).toEqual(mockAgentDropdownOptions);
    });

    it('should not set agentOptions if user is agentGroup and conditions are not met', () => {
      component.loggedUser = userAgentGroup;
      component.formConfig.admAcmType = MasterDataType.Acmq;
      component.agentOptions = mockAgentDropdownOptions;

      const agents = null;

      component['initializeAgentsDropdown'](agents);

      expect(component.agentOptions).toEqual(mockAgentDropdownOptions);
    });

    it('should set agentOptions when user is not agent, airline or agentGroup and not isAcdmRequest', () => {
      component.loggedUser = userIata;
      component.formConfig.admAcmType = MasterDataType.Adm;
      component.agentOptions = mockAgentDropdownOptions;

      component['initializeAgentsDropdown'](mockAgentDropdownOptions);

      expect(component.agentOptions).toEqual(mockAgentDropdownOptions);
    });

    it('should call setDefaultAgent when user is not agent, airline or agentGroup and isAcdmRequest', () => {
      spyOn<any>(component, 'setDefaultAgent');

      component.loggedUser = userIata;
      component.formConfig.admAcmType = MasterDataType.Acmq;
      component.agentOptions = mockAgentDropdownOptions;

      component['initializeAgentsDropdown'](mockAgentDropdownOptions);

      expect(component['setDefaultAgent']).toHaveBeenCalled();
    });
  });

  describe('initializeAirlinesDropdown', () => {
    const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
      {
        value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
        label: '001 / AIRLINE 001'
      },
      {
        value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
        label: '002 / AIRLINE 002'
      }
    ];

    const airlineUser: AirlineUser = {
      id: 10126,
      email: 'airline1@example.com',
      firstName: 'firstName',
      lastName: 'lastName',
      userType: UserType.AIRLINE,
      globalAirline: {
        id: 12,
        globalName: 'Test Airline',
        iataCode: '626',
        logo: '',
        airlines: [{ id: 1, localName: 'Lufthansa', bsp: { id: 1, name: 'USA' } as Bsp } as Airline],
        version: 1
      },
      permissions: []
    };

    it('should be called onAirlineChange if user is airline ', () => {
      spyOn<any>(component, 'onAirlineChange');

      component.loggedUser = airlineUser;
      component.bspSelected = bspMock;

      component['initializeAirlinesDropdown'](mockAirlineDropdownOptions);

      expect(component['onAirlineChange']).toHaveBeenCalled();
    });

    it('should be called onAirlineChange with undefined if user is airline and airlineUserId is undefined', () => {
      const bsp: BspDto = {
        id: 2,
        name: 'bspName',
        isoCountryCode: 'ES'
      };

      spyOn<any>(component, 'onAirlineChange');

      component.loggedUser = userAirline;
      component.bspSelected = bsp;

      component['initializeAirlinesDropdown'](mockAirlineDropdownOptions);

      expect(component['onAirlineChange']).toHaveBeenCalledWith(undefined);
    });

    it('should be called setDefaultAirline if user is not airline', () => {
      spyOn<any>(component, 'setDefaultAirline');

      component.loggedUser = userIata;
      component.bspSelected = bspMock;

      component['initializeAirlinesDropdown'](mockAirlineDropdownOptions);

      expect(component['setDefaultAirline']).toHaveBeenCalled();
    });
  });

  describe('getBspsDropdownOptions$', () => {
    it('should subscribe to getBspsDropdownOptions and get All Bsps By Permissions', () => {
      component.formConfig.accessType = AccessType.editTicket;
      component.formConfig.issuingPendingSupervision = true;

      component['getBspsDropdownOptions$']().subscribe();

      expect(bspsDictionaryServiceSpy.getAllBspsByPermissions).toHaveBeenCalled();
    });
  });

  it('should get user agent info from admAcm service when getUserAgentInfo is called', () => {
    const userAgent: User = {
      ...createAirlineUser(),
      agent: {
        id: 1,
        name: 'test',
        iataCode: '007',
        effectiveFrom: '2020-01-01',
        bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
      },
      userType: UserType.AGENT
    };
    component.loggedUser = userAgent;
    component.isAcdmd = true;
    component['getUserAgentInfo'](component.loggedUser.id);

    expect(admAcmServiceStub.getBspAgentInfo).toHaveBeenCalled();
  });

  it('should get User Airline Info when getUserAirlineInfo is called', () => {
    component.loggedUser = userAirline;
    component.isAcdmd = true;
    component['getUserAirlineInfo'](component.loggedUser.id);

    expect(admAcmServiceStub.getBspAirlineInfo).toHaveBeenCalled();
  });

  it('should toggle agent and airline details', () => {
    component.showAirlineDetails = false;
    component.showAgentDetails = false;

    component.toggleAirlineDetails();
    component.toggleAgentDetails();

    expect(component.showAirlineDetails).toBeTruthy();
    expect(component.showAgentDetails).toBeTruthy();
  });

  it('should set currrency when currency length list is one', () => {
    component.ticketIssueMode = false;
    component.currencyList = [
      {
        id: 656568,
        code: 'AAD',
        decimals: 0,
        isDefault: false
      }
    ];
    const onCurrencyChangeSpy = spyOn<any>(component, 'onCurrencyChange');
    component['setDefaultCurrency']();

    expect(onCurrencyChangeSpy).toHaveBeenCalledWith(component.currencyList[0].id);
  });

  it('should get user agent info when Acdmd is false', () => {
    component.isAcdmd = false;
    component['getUserAgentInfo'](userAgentGroup.id);

    expect(admAcmServiceStub.getAgentInfo).toHaveBeenCalledWith(userAgentGroup.id);
  });

  it('should get airline info when Acdmd is false', () => {
    component.loggedUser = userAirline;
    component.isAcdmd = false;
    component['getUserAirlineInfo'](component.loggedUser.id);

    expect(admAcmServiceStub.getAirlineInfo).toHaveBeenCalled();
  });

  it('should initialize adapt Forms Acdmd when acdmd is true tick', fakeAsync(() => {
    component.isAcdmd = true;
    spyOn<any>(component, 'adaptFormAcdmd');
    component['initializeSectionForms']();

    tick();

    expect(component['adaptFormAcdmd']).toHaveBeenCalled();
  }));

  it('should set default agent when user type is an agent', () => {
    spyOn<any>(component, 'onAgentChange');
    const userAgent: User = {
      ...createAirlineUser(),
      agent: {
        id: 1,
        name: 'test',
        iataCode: '007',
        effectiveFrom: '2020-01-01',
        bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
      },
      userType: UserType.AGENT
    };
    component.loggedUser = userAgent;
    component['setDefaultAgent']();

    expect(component['onAgentChange']).toHaveBeenCalledWith(userAgent.agent.id);
  });

  it('should not set default agent when user type is diferent of agent one', () => {
    spyOn<any>(component, 'onAgentChange');
    component.loggedUser.userType = UserType.AGENT_GROUP;
    component['setDefaultAgent']();

    expect(component['onAgentChange']).not.toHaveBeenCalled();
  });

  it('should initialize stat when options as an array is passed as params', () => {
    const statsParam = [
      { value: 'I', label: '', selected: false },
      { value: 'J', label: '', selected: false }
    ];
    component['initializeStat'](statsParam);

    expect(component.statOptions).toBe(statsParam);
  });

  it('should get user agent info on agent change passing agent id without initialize contact Data Stored', fakeAsync(() => {
    const agentId = 69830000002;
    component.formConfig.accessType = AccessType.create;
    component.loggedUser.userType = UserType.AGENT;
    component.isAcdmd = false;
    spyOn<any>(component, 'getUserAgentInfo').and.returnValue(
      of({
        agent: {
          id: 69830000002,
          iataCode: '0000002',
          bsp: {
            id: 6983,
            version: 0,
            isoCountryCode: 'ES',
            name: 'SPAIN'
          },
          name: 'NOMBRE 2'
        },
        agentVatNumber: null,
        bsplinkUser: false,
        agentAddress: {
          street: 'UNO',
          city: 'SDAS ASDFASF',
          state: 'KAKA',
          country: 'ASDF',
          postalCode: '28002',
          telephone: null
        },
        agentContact: {
          contactName: null,
          phoneFaxNumber: null,
          email: null
        }
      })
    );
    component['onAgentChange'](agentId);
    tick();

    expect(component.agentSelected).toBeDefined();
  }));

  it('should get airline info on airline change passing airline id to onAirlineChange method and initialize contact Data Stored', fakeAsync(() => {
    const airlineId = 6983484849;
    component.loggedUser.userType = UserType.AIRLINE;
    spyOn<any>(component, 'getUserAirlineInfo').and.returnValue(
      of({
        airline: {
          id: 6983484849,
          iataCode: '001',
          bsp: {
            id: 6983,
            version: 0,
            isoCountryCode: 'ES',
            name: 'SPAIN'
          },
          localName: 'AIRLINE NAME 001',
          designator: 'A9'
        },
        airlineVatNumber: null,
        bsplinkUser: true,
        airlineAddress: {
          address1: 'CUMHURIYET CAD. SIBEL APT.',
          address2: '',
          city: 'ASDAS ISTANBUL',
          state: null,
          country: 'TURKEY',
          postalCode: '1111',
          telephone: '0'
        },
        airlineContact: {
          contactName: 'NAME 001',
          phoneFaxNumber: '0',
          email: 'nines.garcia@accelya.com'
        }
      })
    );
    component['onAirlineChange'](airlineId);
    tick();

    expect(component.airlineSelected).toBeDefined();
  }));

  it('should get Subject of form data when initialize toca', fakeAsync(() => {
    const airlineSubject = component.formData.getSubject(AdmAcmActionEmitterType.hasToca);
    spyOn(airlineSubject, 'next').and.callThrough();
    const tocaTypes = [
      {
        label: '123456',
        value: '123456'
      },
      {
        label: 'TCTP',
        value: 'TCTP'
      },
      {
        label: 'VATES',
        value: 'VATES'
      }
    ];
    component['initializeToca'](tocaTypes);
    tick();

    expect(airlineSubject.next).toHaveBeenCalled();
  }));

  it('should called toggleControlDisable', () => {
    const airlineAcdmVatNumberCtrl = FormUtil.get<AirlineViewModel>(component.airlineForm, 'vatNumber');
    spyOn<any>(component, 'toggleControlDisable');
    component['toggleControlDisable'](airlineAcdmVatNumberCtrl, true);

    expect(component['toggleControlDisable']).toHaveBeenCalled();
  });
});
