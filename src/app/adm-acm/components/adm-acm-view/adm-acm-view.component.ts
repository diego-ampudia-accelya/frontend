import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import FileSaver from 'file-saver';
import { Observable, of, Subject } from 'rxjs';
import { first, skip, skipWhile, takeUntil, tap } from 'rxjs/operators';

import { Calculation, Reason, ReasonAction, TransactionCode } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  AgentViewModel,
  CurrencyViewModel,
  DetailViewModel,
  ForwardGdsViewModel,
  StatusHeaderViewModel,
  StatusViewModel
} from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AcdmAlertMessage, AdmAcmActionEmitterType, AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import {
  getBspNameFromAgent,
  getUserDetails,
  roundCalculationValue
} from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import {
  getAcdmCreateInternalCommentPermission,
  getAcdmReadInternalCommentPermission
} from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Agent } from '~app/master-data/models';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ButtonDesign } from '~app/shared/components';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { AlertMessageType } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { appConfiguration, DocumentPdfExporter, TabService } from '~app/shared/services';

@Component({
  selector: 'bspl-adm-acm-view',
  templateUrl: './adm-acm-view.component.html',
  styleUrls: ['./adm-acm-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    AdmAcmService,
    AdmAcmDataService,
    {
      provide: AdmAcmConfigService,
      useFactory: createAdmAcmConfigService,
      deps: [AdmAcmDataService, AdmAcmDialogService, L10nTranslationService, HttpClient, Router, Store, TabService]
    },
    DocumentPdfExporter
  ]
})
export class AdmAcmViewComponent implements OnInit, OnDestroy {
  public itemRetrieved: AdmAcmIssueBE;

  public btnDesign = ButtonDesign;
  public currency: CurrencyViewModel;

  public activities: Activity[] = [];
  public commentAdded$: Subject<any>;
  public isActivityCommentVisible = false;
  public fileUploadPath: string;

  public airlineDetailsVisible = false;
  public agentDetailsVisible = false;
  public agentDetails;
  public airlineDetails;

  public objectKeys = Object.keys;
  public buttonDesign = ButtonDesign;

  public amountAlertMessages: Array<AcdmAlertMessage> = [];
  public statisticalCodeLabel: string;
  public relatedTicketDocumentsControl: FormArray;
  public calculationFields: Array<keyof Calculation>;
  public calculationFieldsVisibility: { [key in keyof Calculation]?: Observable<boolean> } = {};
  public taxTotal: number;
  public headerFormGroup: FormGroup;

  public reasonActions = ReasonAction;
  public reasons: Reason[];
  public gdsForwardsReasons: ForwardGdsViewModel[];
  public reasonsAndGdsForwardReasons: (Reason | ForwardGdsViewModel)[];
  public issueReason: Reason;
  public pdfRelatedDocumentString: string;

  public get transactionCode(): TransactionCode {
    return this.formConfig.getTransactionCode();
  }
  public get admAcmType(): MasterDataType {
    return this.formConfig.admAcmType;
  }

  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)
  public isAcdnt = false; //* Indicates if it is an ACDNT

  public isAirlineVatFieldVisible$: Observable<boolean>;
  public isAgentVatFieldVisible$: Observable<boolean>;

  public isRMICVisible: boolean;
  public isInternalCommentButtonVisible: boolean;

  public headerHeight: number;

  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public formData: AdmAcmDataService,
    public formConfig: AdmAcmConfigService,
    public admAcmService: AdmAcmService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private element: ElementRef,
    private pdfExporter: DocumentPdfExporter,
    private permissionsService: PermissionsService,
    private admAcmDialogService: AdmAcmDialogService
  ) {
    this.initializeRouteData();
  }

  public ngOnInit(): void {
    this.initializeUserDetails();
    this.initializeStat();
    this.initializeReasons();
    this.initializeReasonsListener();
    this.initializeRelatedTicketDocuments();
    this.initializeCalculationFields();
    this.initializeTaxTotal();
    this.initializeHeaderFormGroup();
    this.initializeAlertMessages();
    this.initializeActivityPanel();
    this.initializeVatFieldsVisibility();
    this.initializeRMICVisibility();
    this.initializeInternalCommentVisibility();

    this.emitDataValues();
    this.initializeStatusListener();

    this.formData.isFormReady.next(true);
    this.pdfExporter.attach(this.element);
  }

  public onDownloadClick() {
    this.airlineDetailsVisible = true;
    this.agentDetailsVisible = true;
    this.cd.detectChanges();
  }

  public onDownloadFile(fileUrl: string) {
    this.admAcmService.downloadFile(fileUrl).subscribe(file => {
      FileSaver.saveAs(file.blob, file.fileName);
    });
  }

  public onSendComment(message: { reason: string }) {
    this.commentAdded$ = new Subject<any>();
    this.admAcmService
      .addComment(message, this.itemRetrieved.id)
      .pipe(tap(() => this.initializeActivities()))
      .subscribe(this.commentAdded$);
  }

  public onInternalComment(): void {
    this.admAcmDialogService.openInternalCommentDialog(this.itemRetrieved, this.admAcmType);
  }

  public onHeaderHeightChange(height: number) {
    this.headerHeight = height;
  }

  public round(value: number): string | number {
    const currency = this.itemRetrieved.currency as Currency;

    return roundCalculationValue(value, currency.decimals);
  }

  public getBspNameFromAgent(agent: AgentViewModel): string {
    return getBspNameFromAgent(agent);
  }

  public amountHasInfoAlerts(): boolean {
    return this.amountAlertMessages.some(msg => !msg.hidden);
  }

  public toggleAirlineDetails(): void {
    this.airlineDetailsVisible = !this.airlineDetailsVisible;
  }

  public toggleAgentDetails(): void {
    this.agentDetailsVisible = !this.agentDetailsVisible;
  }

  private initializeRouteData() {
    const data = this.activatedRoute.snapshot.data;
    this.itemRetrieved = data.item;
    this.formConfig.accessType = data.access;
    this.formConfig.admAcmType = data.admAcmType;
    this.formConfig.airlineIssued = this.itemRetrieved ? this.itemRetrieved.airlineIssued : null;
    this.isAcdmd = this.formConfig.isAcdmd;
    this.isAcdnt = this.formConfig.isAcdnt;
  }

  private initializeStat() {
    const statCode = this.formConfig.getDefaultStats().find(stat => stat.value === this.itemRetrieved.statisticalCode);

    this.statisticalCodeLabel = statCode ? statCode.label : this.itemRetrieved.statisticalCode.toUpperCase();
  }

  private initializeReasons() {
    this.issueReason = {
      id: null,
      action: ReasonAction.issue,
      reason: this.itemRetrieved.reasonForMemo?.match(/.{1,45}/g)?.join('\n') // line break after 45 characters
    };

    this.reasons = this.itemRetrieved.reasons || [];
    this.gdsForwardsReasons = this.itemRetrieved.gdsForwards || [];

    const allReasons: (Reason | ForwardGdsViewModel)[] = [...this.reasons, ...this.gdsForwardsReasons];
    this.reasonsAndGdsForwardReasons = this.sortReasonsByDate(allReasons);
  }

  private sortReasonsByDate(reasons: (Reason | ForwardGdsViewModel)[]): (Reason | ForwardGdsViewModel)[] {
    return reasons.sort((a, b) => {
      const dateA = a['date'] || a['forwardDate'];
      const dateB = b['date'] || b['forwardDate'];

      return new Date(dateA).getTime() - new Date(dateB).getTime();
    });
  }

  private initializeRelatedTicketDocuments() {
    this.relatedTicketDocumentsControl = new FormArray([]);
    const pdfRelatedDocument = [];

    if (this.itemRetrieved.relatedTicketDocuments) {
      this.itemRetrieved.relatedTicketDocuments.forEach(doc => {
        this.relatedTicketDocumentsControl.push(new FormControl(doc.relatedTicketDocumentNumber));
        pdfRelatedDocument.push(doc.relatedTicketDocumentNumber);
      });

      this.pdfRelatedDocumentString = pdfRelatedDocument.join(' ');
    }
  }

  private initializeCalculationFields() {
    const calculationReference = this.itemRetrieved.differenceCalculations;

    this.calculationFields = this.formConfig.getAmountCalculationFields().filter(key => {
      const fieldIsNotNull = calculationReference[key] !== null && Object.keys(calculationReference).includes(key);
      const fieldIsFare = key === 'fare';

      return this.isAcdmd ? fieldIsFare : fieldIsNotNull;
    });

    this.calculationFields.forEach(
      field =>
        (this.calculationFieldsVisibility = {
          ...this.calculationFieldsVisibility,
          [field]: this.isAmountFieldVisible(field)
        })
    );
  }

  private isAmountFieldVisible(field: keyof Calculation): Observable<boolean> {
    let visible = of(true);

    if (field === 'miscellaneousFee' || field === 'cancellationPenalty') {
      visible = this.formConfig.isConcernsRefundFieldVisible(
        field,
        this.itemRetrieved.agentCalculations[field],
        this.itemRetrieved.airlineCalculations[field],
        this.itemRetrieved.differenceCalculations[field]
      );
    }

    return visible;
  }

  private initializeTaxTotal() {
    const total = this.itemRetrieved.taxMiscellaneousFees.reduce(
      (prev, current) => prev + current.taxDifferenceAmount,
      0
    );
    this.taxTotal = this.round(total) as number;
  }

  private initializeHeaderFormGroup() {
    const statusForm = new FormUtil(this.fb).createGroup<StatusViewModel>({
      status: this.itemRetrieved.acdmStatus,
      sentToDpc: this.itemRetrieved.sentToDpc
    });
    this.headerFormGroup = new FormUtil(this.fb).createGroup<StatusHeaderViewModel>({
      ticketDocumentNumber: this.itemRetrieved.ticketDocumentNumber,
      status: statusForm,
      bspName: getBspNameFromAgent(this.itemRetrieved.agent as AgentViewModel),
      agentCode: (this.itemRetrieved.agent as Agent).iataCode,
      acdmaFor: this.itemRetrieved.concernsIndicator,
      issueDate: this.itemRetrieved.dateOfIssue,
      amount: this.itemRetrieved.totalAmount
    });
  }

  private initializeActivityPanel() {
    this.fileUploadPath = `${appConfiguration.baseUploadPath}/acdm-management/files`;
    this.initializeActivities();
  }

  private initializeActivities() {
    this.admAcmService
      .getActivities(this.itemRetrieved.id)
      .pipe(first())
      .subscribe(activities => {
        this.activities = activities;
        this.cd.markForCheck();
      });
  }

  private emitDataValues() {
    const { id, isoCountryCode, name } = (this.itemRetrieved.agent as Agent).bsp;

    this.formData.getSubject(AdmAcmActionEmitterType.bsp).next({ id, isoCountryCode, name } as BspDto);
    this.formData
      .getSubject(AdmAcmActionEmitterType.status)
      .next({ status: this.itemRetrieved.acdmStatus, sentToDpc: this.itemRetrieved.sentToDpc });
    this.formData.getSubject(AdmAcmActionEmitterType.acdmaFor).next(this.itemRetrieved.concernsIndicator);
    this.formData.getSubject(AdmAcmActionEmitterType.totalAmount).next(this.itemRetrieved.totalAmount);
    this.formData.getSubject(AdmAcmActionEmitterType.airline).next(this.itemRetrieved.airline);
  }

  private initializeReasonsListener() {
    this.formData
      .getSubject(AdmAcmActionEmitterType.reasons)
      .pipe(
        skipWhile(reasonsData => !reasonsData),
        takeUntil(this.destroy$)
      )
      .subscribe(({ disputeContact, reasons }: Partial<DetailViewModel>) => {
        this.itemRetrieved.disputeContact = disputeContact;
        this.reasons = reasons;

        this.cd.markForCheck();
      });

    this.formData
      .getSubject(AdmAcmActionEmitterType.gdsForwardReasons)
      .pipe(
        skipWhile(reasonsData => !reasonsData),
        takeUntil(this.destroy$)
      )
      .subscribe((reasonsData: ForwardGdsViewModel) => {
        this.gdsForwardsReasons = [...this.gdsForwardsReasons, reasonsData];
        this.cd.markForCheck();
      });
  }

  private initializeStatusListener() {
    this.formData
      .getSubject(AdmAcmActionEmitterType.status)
      .pipe(skip(1)) //* Skipping initial status value to avoid double query to /history
      .subscribe(() => this.initializeActivities());
  }

  private initializeUserDetails() {
    this.agentDetails = getUserDetails(this.itemRetrieved.agentAddress);
    this.airlineDetails = getUserDetails(this.itemRetrieved.airlineAddress);
  }

  private initializeAlertMessages() {
    this.amountAlertMessages = [
      {
        hidden: !this.itemRetrieved.regularized,
        message: 'ADM_ACM.amounts.amount.regularizedMessage',
        type: AlertMessageType.info
      }
    ];
  }

  private initializeVatFieldsVisibility(): void {
    this.isAirlineVatFieldVisible$ = of(false);
    this.isAgentVatFieldVisible$ = of(false);

    this.formData.whenFormIsReady().subscribe(() => {
      this.isAirlineVatFieldVisible$ = this.formConfig.isAirlineVatFieldVisible(this.itemRetrieved.airlineVatNumber);
      this.isAgentVatFieldVisible$ = this.formConfig.isAgentVatFieldVisible(this.itemRetrieved.agentVatNumber);
    });
  }

  private initializeRMICVisibility(): void {
    this.isRMICVisible = this.formConfig.isRMICVisible() && !!this.itemRetrieved.reasonForMemoIssuanceCode;
  }

  private initializeInternalCommentVisibility(): void {
    const hasReadInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmReadInternalCommentPermission(this.formConfig.admAcmType)
    );
    const hasCreateInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmCreateInternalCommentPermission(this.formConfig.admAcmType)
    );

    this.isInternalCommentButtonVisible =
      hasReadInternalCommentPermission && (!!this.itemRetrieved.internalComment || hasCreateInternalCommentPermission);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
