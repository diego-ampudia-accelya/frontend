import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { of } from 'rxjs';

import { AdmAcmViewComponent } from './adm-acm-view.component';
import {
  Calculation,
  Reason,
  ReasonAction,
  RelatedTicketDocument
} from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AcdmAlertMessage } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Activity, ActivityType } from '~app/pbd/shared/pbd.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { AlertMessageType } from '~app/shared/enums';
import { AirlineUser, UserType } from '~app/shared/models/user.model';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';
import { AppConfigurationService, DocumentPdfExporter, TabService } from '~app/shared/services';

const expectedUserDetails: AirlineUser = {
  id: 10126,
  email: 'airline1@example.com',
  firstName: 'firstName',
  lastName: 'lastName',
  userType: UserType.AIRLINE,
  globalAirline: {
    id: 12,
    globalName: 'Test Airline',
    iataCode: '626',
    logo: '',
    airlines: [],
    version: 1
  },
  permissions: []
};

const initialState = {
  auth: {
    user: expectedUserDetails
  }
};

const acdmInstanceMock = {
  statisticalCode: 'I',
  agentCalculations: {
    commission: 0,
    fare: 20,
    supplementaryCommission: 4,
    tax: 0,
    taxOnCommission: 0,
    cancellationPenalty: null,
    miscellaneousFee: null
  },
  airlineCalculations: {
    commission: 3,
    fare: 45,
    supplementaryCommission: 0,
    tax: 0,
    taxOnCommission: 0,
    cancellationPenalty: null,
    miscellaneousFee: null
  },
  differenceCalculations: {
    commission: 3,
    fare: 25,
    supplementaryCommission: -4,
    tax: 0,
    taxOnCommission: 0,
    cancellationPenalty: null,
    miscellaneousFee: null
  },
  taxMiscellaneousFees: [
    {
      agentAmount: 1,
      airlineAmount: 2,
      taxDifferenceAmount: 3,
      type: ''
    },
    {
      agentAmount: 1,
      airlineAmount: 2,
      taxDifferenceAmount: 5,
      type: ''
    }
  ],
  currency: {},
  agent: { bsp: {} },
  airline: {},
  reasonForMemoIssuanceCode: ''
};

const activatedRouteStub = {
  snapshot: {
    data: { item: acdmInstanceMock }
  }
};

const amountAlertMessagesMock: Array<AcdmAlertMessage> = [
  {
    hidden: true,
    message: 'ADM_ACM.amounts.amount.regularizedMessage',
    type: AlertMessageType.info
  }
];

const issueReasonMock: Reason = {
  id: null,
  action: ReasonAction.issue,
  reason: undefined
};

describe('AdmAcmViewComponent', () => {
  let component: AdmAcmViewComponent;
  let fixture: ComponentFixture<AdmAcmViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmViewComponent, DateTimeFormatPipe, EmptyPipe],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        L10nTranslationModule.forRoot(l10nConfig),
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        provideMockStore({ initialState }),
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        AdmAcmDialogService,
        PermissionsService,
        L10nTranslationService,
        HttpClient,
        DialogService,
        FormBuilder,
        DocumentPdfExporter,
        { provide: AppConfigurationService, useValue: { features: { downloadDocumentAsPdf: true } } },
        provideMockStore({ initialState }),
        TabService
      ],

      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmViewComponent);
    component = fixture.componentInstance;
    component.formData = new AdmAcmDataService(TestBed.inject(FormBuilder));
    component.formConfig = createAdmAcmConfigService(
      component.formData,
      TestBed.inject(AdmAcmDialogService),
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Router),
      TestBed.inject(Store),
      TestBed.inject(TabService)
    );
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display amount considering agent and airline calculations', () => {
    const calculationFieldsResult: Array<keyof Calculation> = [
      'fare',
      'tax',
      'commission',
      'taxOnCommission',
      'supplementaryCommission'
    ];

    expect(component.calculationFields).toEqual(calculationFieldsResult);
  });

  it('should Initialize Alert Messages callThrough', () => {
    spyOn<any>(component, 'initializeAlertMessages').and.callThrough();
    expect(component.amountAlertMessages).toEqual(amountAlertMessagesMock);
  });

  it('should initialize issueReason, reasons and gdsForwardsReasons variables when initializeReasons method is called', () => {
    component.itemRetrieved.reasonForMemo = undefined;

    (component as any).initializeReasons();

    expect(component.issueReason).toEqual(issueReasonMock);
    expect(component.reasons).toEqual([]);
    expect(component.gdsForwardsReasons).toEqual([]);
  });

  it('should initialize issueReason and cut the reason to 45 characters each line', () => {
    component.itemRetrieved.reasonForMemo = `123456789 123456789 123456789 123456789 12345123456789 123456789 123456789 123456789 12345123456789 123456789 123456789 123456789 12345`;

    (component as any).initializeReasons();
    const rows = component.issueReason.reason.split('\n');

    expect(rows.length).toBe(3);
    expect(rows[0].length).toBe(45);
    expect(rows[1].length).toBe(45);
    expect(rows[2].length).toBe(45);
  });

  it('should check if amount field passed by parameter is visible when isAmountFieldVisible method is called', fakeAsync(() => {
    let isVisible: boolean;

    component['isAmountFieldVisible']('fare').subscribe(visible => {
      isVisible = visible;
    });

    tick();

    expect(isVisible).toBe(true);
  }));

  it('should set header height to the passing param value', () => {
    component.onHeaderHeightChange(20);
    expect(component.headerHeight).toEqual(20);
  });

  it('should toggle airline and agent details', () => {
    component.airlineDetailsVisible = false;
    component.toggleAirlineDetails();
    expect(component.airlineDetailsVisible).toBe(true);

    component.agentDetailsVisible = true;
    component.toggleAgentDetails();
    expect(component.agentDetailsVisible).toBe(false);
  });

  it('should initialize tax total reducing tax miscellaneous fees', () => {
    const totalTaxReduced = acdmInstanceMock.taxMiscellaneousFees.reduce(
      (prev, current) => prev + current.taxDifferenceAmount,
      0
    );
    component['initializeTaxTotal']();
    expect(component.taxTotal).toBe(totalTaxReduced);
  });

  it('should set airlineDetailsVisible and agentDetailsVisible to true when download is clicked', () => {
    component.onDownloadClick();
    expect(component.airlineDetailsVisible).toBe(true);
    expect(component.agentDetailsVisible).toBe(true);
  });

  it('should call service to download a file', fakeAsync(() => {
    const mockFile = {
      blob: 'blob',
      fileName: 'fileName'
    };

    const downloadFileSpy = spyOn(component.admAcmService, 'downloadFile').and.returnValue(of(mockFile));
    const fileSaverSpy = spyOn(FileSaver, 'saveAs');
    component.onDownloadFile('url');
    tick();

    expect(downloadFileSpy).toHaveBeenCalled();
    expect(fileSaverSpy).toHaveBeenCalledWith(mockFile.blob, mockFile.fileName);
  }));

  it('should initializeStat', fakeAsync(() => {
    const activitiesMock: Activity[] = [
      {
        pbdId: 1,
        type: ActivityType.Commented
      },
      {
        pbdId: 1,
        type: ActivityType.Commented
      }
    ];
    const getActivitiesSpy = spyOn(component.admAcmService, 'getActivities').and.returnValue(of(activitiesMock));
    component['initializeActivities']();
    tick();

    expect(getActivitiesSpy).toHaveBeenCalled();
  }));

  it('should check if isAmountFieldVisible method is called when the param passed by parameter is miscellaneousFee', () => {
    const isConcernsRefundFieldVisibleSpy = spyOn(component.formConfig, 'isConcernsRefundFieldVisible').and.returnValue(
      false
    );
    component['isAmountFieldVisible']('miscellaneousFee');

    expect(isConcernsRefundFieldVisibleSpy).toHaveBeenCalled();
  });

  it('should call openInternalCommentDialog when adding internal comment', () => {
    const openInternalCommentDialogSpy = spyOn(component['admAcmDialogService'], 'openInternalCommentDialog');

    component.onInternalComment();
    expect(openInternalCommentDialogSpy).toHaveBeenCalled();
  });

  it('should send comment with a reason passed by parameter and initialize activities', fakeAsync(() => {
    const addCommentSpy = spyOn(component['admAcmService'], 'addComment').and.returnValue(of([]));
    const initializeActivitiesSpy = spyOn<any>(component, 'initializeActivities');

    component.onSendComment({ reason: 'testing' });
    tick();

    expect(addCommentSpy).toHaveBeenCalled();
    expect(initializeActivitiesSpy).toHaveBeenCalled();
  }));

  it('should initialize related ticket documents when calling initializeRelatedTicketDocuments method', () => {
    const relatedTicketsMock: RelatedTicketDocument[] = [];
    component.itemRetrieved.relatedTicketDocuments = relatedTicketsMock;

    component['initializeRelatedTicketDocuments']();
    expect(component.pdfRelatedDocumentString).toBe('');
  });
});
