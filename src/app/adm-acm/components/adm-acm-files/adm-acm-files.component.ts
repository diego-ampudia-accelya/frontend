import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';

@Component({
  selector: 'bspl-adm-acm-files',
  templateUrl: './adm-acm-files.component.html',
  styleUrls: ['./adm-acm-files.component.scss']
})
export class AdmAcmFilesComponent implements OnInit {
  public tabs: RoutedMenuItem[];

  constructor(private activatedRoute: ActivatedRoute, private menuBuilder: MenuBuilder) {}

  public ngOnInit(): void {
    this.initializeTabs();
  }

  private initializeTabs(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }
}
