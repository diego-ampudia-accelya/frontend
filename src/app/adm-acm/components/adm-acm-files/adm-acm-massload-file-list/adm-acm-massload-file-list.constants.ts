import { AdmAcmMassloadFileStatus, AdmAcmMassloadFileTotals } from '~app/adm-acm/models/adm-acm-massload-file.model';
import { GridColumn } from '~app/shared/models';

export const ACTIVE_COLUMNS: Array<GridColumn> = [
  {
    name: 'ADM_ACM.files.massload.columns.filename',
    prop: 'filename',
    flexGrow: 3
  },
  {
    name: 'ADM_ACM.files.massload.columns.uploadDate',
    prop: 'uploadDate',
    flexGrow: 2
  },
  {
    name: 'ADM_ACM.files.massload.columns.userName',
    prop: 'userName',
    flexGrow: 2
  },
  {
    name: 'ADM_ACM.files.massload.columns.email',
    prop: 'email',
    flexGrow: 3
  },
  {
    name: 'ADM_ACM.files.massload.columns.adma',
    prop: 'adma',
    flexGrow: 1
  },
  {
    name: 'ADM_ACM.files.massload.columns.acma',
    prop: 'acma',
    flexGrow: 1
  },
  {
    name: 'ADM_ACM.files.massload.columns.total',
    prop: 'total',
    flexGrow: 1
  },
  {
    name: 'ADM_ACM.files.massload.columns.status',
    prop: 'status',
    flexGrow: 2,
    cellTemplate: 'badgeInfoCellTmpl'
  }
];

export const NOT_PROCESSED_COLUMNS: Array<GridColumn> = [
  {
    name: 'ADM_ACM.files.massload.columns.filename',
    prop: 'filename',
    flexGrow: 2
  },
  {
    name: 'ADM_ACM.files.massload.columns.cancellationDate',
    prop: 'requestDate',
    flexGrow: 1.5
  },
  {
    name: 'ADM_ACM.files.massload.columns.cancelBy',
    prop: 'userName',
    flexGrow: 1.5
  },
  {
    name: 'ADM_ACM.files.massload.columns.email',
    prop: 'email',
    flexGrow: 2
  },
  {
    name: 'ADM_ACM.files.massload.columns.status',
    prop: 'status',
    flexGrow: 1,
    cellTemplate: 'badgeInfoCellTmpl'
  }
];

export const ACTIVE_STATUSES: AdmAcmMassloadFileStatus[] = [
  AdmAcmMassloadFileStatus.Pending,
  AdmAcmMassloadFileStatus.Processed,
  AdmAcmMassloadFileStatus.Rejected
];

export const NOT_PROCESSED_STATUSES: AdmAcmMassloadFileStatus[] = [
  AdmAcmMassloadFileStatus.NotProcessed,
  AdmAcmMassloadFileStatus.NotFound
];

export const EMPTY_ADM_ACM_MASSLOAD_FILE_TOTALS: AdmAcmMassloadFileTotals = {
  adma: null,
  acma: null,
  total: null
};
