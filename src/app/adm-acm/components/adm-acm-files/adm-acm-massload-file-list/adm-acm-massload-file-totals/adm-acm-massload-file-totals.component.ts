import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { AdmAcmMassloadFileTotals } from '~app/adm-acm/models/adm-acm-massload-file.model';
import { SimpleTableColumn } from '~app/shared/components';

@Component({
  selector: 'bspl-adm-acm-massload-file-totals',
  templateUrl: './adm-acm-massload-file-totals.component.html',
  styleUrls: ['./adm-acm-massload-file-totals.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdmAcmMassloadFileTotalsComponent implements OnInit {
  @Input() public totals: AdmAcmMassloadFileTotals;

  public columns: SimpleTableColumn<AdmAcmMassloadFileTotals>[];

  public ngOnInit(): void {
    this.columns = this.getColumns();
  }

  private getColumns(): SimpleTableColumn<AdmAcmMassloadFileTotals>[] {
    return [
      { header: 'ADM_ACM.files.massload.totalsColumns.adma', field: 'adma' },
      { header: 'ADM_ACM.files.massload.totalsColumns.acma', field: 'acma' },
      { header: 'ADM_ACM.files.massload.totalsColumns.total', field: 'total', customStyle: 'font-weight-bold' }
    ];
  }
}
