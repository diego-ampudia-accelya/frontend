import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil, tap } from 'rxjs/operators';

import { AdmAcmMassloadFileService } from '~app/adm-acm/services/adm-acm-massload-file.service';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ServerError } from '~app/shared/errors';
import { ResponseErrorMessageBE } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-stop-processing-dialog',
  templateUrl: './stop-processing-dialog.component.html',
  styleUrls: ['./stop-processing-dialog.component.scss']
})
export class StopProcessingDialogComponent implements OnInit, OnDestroy {
  public form: FormArray;

  public buttonDesign = ButtonDesign;

  private get initialFormGroup(): FormGroup {
    return this.formBuilder.group({
      filename: ['', Validators.required]
    });
  }

  private actionEmitter: Subject<any>;

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private dataService: AdmAcmMassloadFileService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  public ngOnInit(): void {
    this.form = this.buildForm();
    this.actionEmitter = this.config.data.actionEmitter;

    this.initializeFormListener();
    this.initializeDialogListener();
  }

  public removeFilenameControlLine(index: number): void {
    if (this.form.length > 1) {
      this.form.removeAt(index);
    } else {
      this.form.reset({ emitEvent: false });
    }
  }

  public addFilenameControlLine(): void {
    this.form.push(this.initialFormGroup);
  }

  private buildForm(): FormArray {
    return this.formBuilder.array([this.initialFormGroup]);
  }

  private initializeFormListener(): void {
    this.form.statusChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(status => this.setIsStopProcessing(status === 'INVALID'));
  }

  private setIsStopProcessing(disabled: boolean): void {
    const stopProcessingButton: ModalAction = this.config.data.buttons.find(
      (btn: ModalAction) => btn.type === FooterButton.StopProcessing
    );
    stopProcessingButton.isDisabled = disabled;
  }

  private initializeDialogListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        switchMap(({ clickedBtn }) =>
          clickedBtn === FooterButton.StopProcessing ? this.stopProcessing() : of(clickedBtn)
        ),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private stopProcessing(): Observable<any> {
    const values: Array<{ filename: string }> = this.form.value;

    // Disabling stop processing button: on error the button will remain disabled and on success the dialog will be closed
    this.setIsStopProcessing(true);

    return this.dataService.stopProcessing(values).pipe(
      tap(() => {
        this.notificationService.showSuccess(this.buildSuccessTranslation(values));
        this.dialogService.close();
        this.actionEmitter.next();
      }),
      catchError((err: ServerError) => {
        this.setControlErrors(err.error.messages);

        return of(err);
      })
    );
  }

  private buildSuccessTranslation(values: Array<{ filename: string }>): string {
    let message: string;
    let params: any;

    if (values.length > 1) {
      message = 'ADM_ACM.files.massload.dialogs.stopProcessing.successMessage.severalFiles';
      params = { length: values.length };
    } else {
      message = 'ADM_ACM.files.massload.dialogs.stopProcessing.successMessage.singleFile';
      params = { fileName: values[0].filename };
    }

    return this.translationService.translate(message, params);
  }

  private setControlErrors(errorMessages: ResponseErrorMessageBE[]): void {
    const indexes = errorMessages.reduce((acc: number[], message) => {
      const { messageParams } = message;
      const param = messageParams && messageParams[0];

      return param ? [...acc, +param.value.substring(1, param.value.indexOf(']'))] : acc;
    }, []);

    indexes.forEach(index => {
      this.form.controls[index]
        .get('filename')
        .setErrors(
          { file_wrong_name: { key: 'ADM_ACM.files.massload.dialogs.stopProcessing.field.error' } },
          { emitEvent: false }
        );
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
