import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AdmAcmMassloadFileViewModel } from '~app/adm-acm/models/adm-acm-massload-file.model';
import { ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable()
export class StopProcessingConfirmationDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(files: AdmAcmMassloadFileViewModel[]): ChangesDialogConfig {
    return {
      data: {
        title: 'ADM_ACM.files.massload.dialogs.stopProcessingConfirmation.title',
        footerButtonsType: [{ type: FooterButton.StopProcessing }]
      },
      changes: this.formatChanges(files),
      message: this.translation.translate('ADM_ACM.files.massload.dialogs.stopProcessingConfirmation.description'),
      usePillsForChanges: true
    };
  }

  private formatChanges(files: AdmAcmMassloadFileViewModel[]): ChangeModel[] {
    return files.map(file => ({
      group: '',
      name: file.filename,
      value: ''
    }));
  }
}
