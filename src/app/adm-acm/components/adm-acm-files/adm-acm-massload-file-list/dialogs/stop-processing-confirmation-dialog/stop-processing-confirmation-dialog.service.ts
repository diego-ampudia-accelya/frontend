import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';

import { StopProcessingConfirmationDialogConfigService } from './stop-processing-confirmation-dialog-config.service';
import { AdmAcmMassloadFileViewModel } from '~app/adm-acm/models/adm-acm-massload-file.model';
import { AdmAcmMassloadFileService } from '~app/adm-acm/services/adm-acm-massload-file.service';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class StopProcessingConfirmationDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: StopProcessingConfirmationDialogConfigService,
    private dataService: AdmAcmMassloadFileService,
    private notificationService: NotificationService
  ) {}

  public open(files: AdmAcmMassloadFileViewModel[]): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(files);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap((result: FooterButton) =>
        result === FooterButton.StopProcessing ? this.stopProcessing(files, dialogConfig) : of(result)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private stopProcessing(
    files: AdmAcmMassloadFileViewModel[],
    dialogConfig: ChangesDialogConfig
  ): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      dialogConfig.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      const filenames: Array<{ filename: string }> = files.map(file => ({ filename: file.filename }));

      return this.dataService.stopProcessing(filenames).pipe(
        tap(() =>
          this.notificationService.showSuccess(
            'ADM_ACM.files.massload.dialogs.stopProcessingConfirmation.successMessage'
          )
        ),
        mapTo(FooterButton.StopProcessing),
        finalize(() => setLoading(false))
      );
    });
  }
}
