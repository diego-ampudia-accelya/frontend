import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { Subject } from 'rxjs';
import { first, switchMap, takeUntil, tap } from 'rxjs/operators';

import {
  ACTIVE_COLUMNS,
  ACTIVE_STATUSES,
  EMPTY_ADM_ACM_MASSLOAD_FILE_TOTALS,
  NOT_PROCESSED_COLUMNS,
  NOT_PROCESSED_STATUSES
} from './adm-acm-massload-file-list.constants';
import { StopProcessingConfirmationDialogConfigService } from './dialogs/stop-processing-confirmation-dialog/stop-processing-confirmation-dialog-config.service';
import { StopProcessingConfirmationDialogService } from './dialogs/stop-processing-confirmation-dialog/stop-processing-confirmation-dialog.service';
import { StopProcessingDialogComponent } from './dialogs/stop-processing-dialog/stop-processing-dialog.component';
import { AdmAcmMassloadFileFilter } from '~app/adm-acm/models/adm-acm-massload-file-filter.model';
import {
  AdmAcmMassloadFileListOption,
  AdmAcmMassloadFileStatus,
  AdmAcmMassloadFileTotals,
  AdmAcmMassloadFileViewModel
} from '~app/adm-acm/models/adm-acm-massload-file.model';
import { AdmAcmMassloadFileFormatter } from '~app/adm-acm/services/adm-acm-massload-file-formatter.service';
import { AdmAcmMassloadFileService } from '~app/adm-acm/services/adm-acm-massload-file.service';
import { acdmFilesPermissions } from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { AcdmMassloadFiles } from '~app/adm-acm/store/actions';
import { massloadFileKey, massloadFileListOption, massloadFileSelector, State } from '~app/adm-acm/store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import {
  ButtonDesign,
  DialogConfig,
  DialogService,
  FloatingPanelComponent,
  FooterButton
} from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SelectMode } from '~app/shared/enums';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-adm-acm-massload-file-list',
  templateUrl: './adm-acm-massload-file-list.component.html',
  styleUrls: ['./adm-acm-massload-file-list.component.scss'],
  providers: [
    AdmAcmMassloadFileFormatter,
    StopProcessingConfirmationDialogService,
    StopProcessingConfirmationDialogConfigService
  ]
})
export class AdmAcmMassloadFileListComponent
  extends ListSubtabs<AdmAcmMassloadFileViewModel, AdmAcmMassloadFileFilter>
  implements OnInit
{
  @ViewChild(FloatingPanelComponent, { static: true }) public totalsPanel: FloatingPanelComponent;

  public hasCreatePermission: boolean;

  public columns: Array<GridColumn>;
  public searchForm: FormGroup;
  public actions: Array<{ action: GridTableActionType; disabled?: boolean }>;

  public totalItemsMessage: string;
  public noticeMessage: string;

  public selectMode = SelectMode;
  public buttonDesign = ButtonDesign;
  public admAcmMassloadFileListOption = AdmAcmMassloadFileListOption;

  public selectedListOption: AdmAcmMassloadFileListOption;
  public listOptions: DropdownOption<AdmAcmMassloadFileListOption>[] = Object.values(AdmAcmMassloadFileListOption).map(
    option => ({
      value: option,
      label: this.translationService.translate(`ADM_ACM.files.massload.listOptions.${option}`)
    })
  );

  public statusDropdownOptions: DropdownOption<AdmAcmMassloadFileStatus>[];

  public totals: AdmAcmMassloadFileTotals = EMPTY_ADM_ACM_MASSLOAD_FILE_TOTALS;
  public isTotalLoading: boolean;

  private formFactory: FormUtil;

  private dialogSubmitEmitter = new Subject<any>();

  constructor(
    public displayFormatter: AdmAcmMassloadFileFormatter,
    protected store: Store<AppState>,
    protected dataService: AdmAcmMassloadFileService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private permissionsService: PermissionsService,
    private dialogService: DialogService,
    private stopProcessingConfirmationDialogService: StopProcessingConfirmationDialogService,
    private dateTimeFormatPipe: DateTimeFormatPipe
  ) {
    super(store, dataService, actions$, translationService);

    this.formFactory = new FormUtil(this.formBuilder);

    // On massload file list option change, data service endpoint url is changed
    this.store.pipe(select(massloadFileListOption)).subscribe(option => {
      this.dataService.changeEndpointUrl(option);
      this.selectedListOption = option;
    });
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<AdmAcmMassloadFileViewModel, AdmAcmMassloadFileFilter>,
    DefaultProjectorFn<ListSubtabsState<AdmAcmMassloadFileViewModel, AdmAcmMassloadFileFilter>>
  > {
    return massloadFileSelector;
  }

  protected getListKey(): string {
    return massloadFileKey;
  }

  public ngOnInit(): void {
    super.ngOnInit();

    this.initializePermissions();
    this.initializeListView();
    this.initializeTotals();

    // Reset query on successful dialog submit
    this.dialogSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.onQueryChanged());
  }

  public onListOptionSelected(option: AdmAcmMassloadFileListOption): void {
    this.store.dispatch(AcdmMassloadFiles.changeListOption({ option }));
    this.selectedListOption = option;

    // Columns, search form and dropdown options change on different option selected
    this.initializeListView();

    // Data service url endpoint is updated on list option change, so we send new request to BE with default query
    this.onQueryChanged(defaultQuery);
  }

  public onBulkStopProcessing(): void {
    this.selectedRows$.pipe(first()).subscribe(rows => this.confirmStopProcessing(rows));
  }

  public onStopProcessing(): void {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'ADM_ACM.files.massload.dialogs.stopProcessing.title',
        footerButtonsType: [{ type: FooterButton.StopProcessing, isDisabled: true }],
        actionEmitter: this.dialogSubmitEmitter
      }
    };

    this.dialogService.open(StopProcessingDialogComponent, dialogConfig);
  }

  public onActionMenuClick(file: AdmAcmMassloadFileViewModel): void {
    if (this.hasCreatePermission && file.status === AdmAcmMassloadFileStatus.Pending) {
      this.actions = [{ action: GridTableActionType.StopProcessing }];
    } else if (file.evaluationFileId) {
      this.actions = [{ action: GridTableActionType.DownloadEvaluationFile }];
    } else {
      this.actions = [{ action: GridTableActionType.DownloadEvaluationFile, disabled: true }];
    }
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: AdmAcmMassloadFileViewModel }): void {
    switch (action.actionType) {
      case GridTableActionType.DownloadEvaluationFile:
        this.dataService.downloadEvaluationFile(row.evaluationFileId).subscribe(response => {
          FileSaver.saveAs(response.blob, response.fileName);
          this.notificationService.showSuccess(
            this.translationService.translate('ADM_ACM.files.massload.successDownloadMessage', {
              fileName: response.fileName
            })
          );
        });

        break;
      case GridTableActionType.StopProcessing:
        this.confirmStopProcessing([row]);

        break;
    }
  }

  public onTotalPanelOpened(): void {
    this.query$
      .pipe(
        first(),
        tap(() => (this.isTotalLoading = true)),
        switchMap(query => this.dataService.findTotals(query)),
        tap(() => (this.isTotalLoading = false))
      )
      .subscribe(totals => (this.totals = totals));
  }

  public onTotalPanelClosed(): void {
    this.totals = EMPTY_ADM_ACM_MASSLOAD_FILE_TOTALS;
  }

  private getTranslatedStatus(status: AdmAcmMassloadFileStatus): string {
    return this.translationService.translate(`ADM_ACM.files.massload.status.${status.toLowerCase()}`);
  }

  private initializePermissions(): void {
    this.hasCreatePermission = this.permissionsService.hasPermission(acdmFilesPermissions.createEcCnx);
  }

  private initializeListView(): void {
    this.noticeMessage =
      this.selectedListOption === AdmAcmMassloadFileListOption.Active ? 'ADM_ACM.files.massload.alertMessage' : null;

    this.buildColumns();
    this.buildForm();

    this.initializeDropdownOptions();
  }

  private initializeTotals(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => {
      const queryTotalItems = query.paginateBy.totalElements;

      // Update query total items on query change
      this.totalItemsMessage = this.translationService.translate('ADM_ACM.files.massload.totalItemsMessage', {
        total: queryTotalItems
      });

      // Close totals panel on query change
      this.totalsPanel.close();
    });
  }

  private buildColumns(): void {
    this.columns =
      this.selectedListOption === AdmAcmMassloadFileListOption.NotProcessed ? NOT_PROCESSED_COLUMNS : ACTIVE_COLUMNS;

    this.columns.find(column => column.prop === 'uploadDate' || column.prop === 'requestDate').pipe =
      this.dateTimeFormatPipe;

    this.setCheckboxColumn();
    this.setStatusColumn();
  }

  private buildForm(): void {
    if (this.searchForm) {
      this.searchForm.reset();
    } else {
      this.searchForm = this.formFactory.createGroup<AdmAcmMassloadFileFilter>({
        filename: [],
        uploadDate: [],
        requestDate: [],
        userName: [],
        email: [],
        status: []
      });
    }
  }

  private initializeDropdownOptions(): void {
    const statuses =
      this.selectedListOption === AdmAcmMassloadFileListOption.NotProcessed ? NOT_PROCESSED_STATUSES : ACTIVE_STATUSES;

    this.statusDropdownOptions = statuses.map(status => ({
      value: status,
      label: this.getTranslatedStatus(status)
    }));
  }

  private setCheckboxColumn(): void {
    if (this.hasCreatePermission && this.selectedListOption === AdmAcmMassloadFileListOption.Active) {
      this.columns = [
        {
          name: '',
          prop: 'isRowSelected',
          maxWidth: 40,
          flexGrow: 1,
          sortable: false,
          headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
          cellTemplate: 'checkboxWithDisableRowCellTmpl'
        },
        ...this.columns
      ];
    }
  }

  private setStatusColumn(): void {
    const statusColumn = this.columns.find(col => col.prop === 'status');

    statusColumn.pipe = { transform: status => this.getTranslatedStatus(status) };
    statusColumn.badgeInfo = {
      hidden: (value: AdmAcmMassloadFileViewModel) => !value,
      type: (value: AdmAcmMassloadFileViewModel) => this.getBadgeInfoType(value.status),
      tooltipLabel: (value: AdmAcmMassloadFileViewModel) => this.getBadgeInfoTooltip(value.status),
      showIconType: true
    };
  }

  private getBadgeInfoType(status: AdmAcmMassloadFileStatus): BadgeInfoType {
    let type: BadgeInfoType;

    switch (status) {
      case AdmAcmMassloadFileStatus.Pending:
        type = BadgeInfoType.info;
        break;
      case AdmAcmMassloadFileStatus.Processed:
        type = BadgeInfoType.success;
        break;
      case AdmAcmMassloadFileStatus.Rejected:
        type = BadgeInfoType.regular;
        break;
      case AdmAcmMassloadFileStatus.NotProcessed:
        type = BadgeInfoType.error;
        break;
      case AdmAcmMassloadFileStatus.NotFound:
      default:
        type = BadgeInfoType.warning;
        break;
    }

    return type;
  }

  private getBadgeInfoTooltip(status: AdmAcmMassloadFileStatus): string {
    return `${this.translationService.translate('ADM_ACM.files.massload.columns.status')}: ${this.getTranslatedStatus(
      status
    )}`;
  }

  private confirmStopProcessing(files: AdmAcmMassloadFileViewModel[]): void {
    this.stopProcessingConfirmationDialogService.open(files).subscribe(() => this.onQueryChanged());
  }
}
