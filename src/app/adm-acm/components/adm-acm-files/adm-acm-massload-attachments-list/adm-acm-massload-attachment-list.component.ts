import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { takeUntil } from 'rxjs/operators';

import { getViewUrlFromTransactionCode } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmMassloadAttachmentFilter } from '~app/adm-acm/models/adm-acm-massload-attachment-filter.model';
import { AdmAcmMassloadAttachment } from '~app/adm-acm/models/adm-acm-massload-attachment.model';
import { AdmAcmMassloadFileStatus } from '~app/adm-acm/models/adm-acm-massload-file.model';
import { AdmAcmMassloadAttachmentFormatter } from '~app/adm-acm/services/adm-acm-massload-attachment-formatter.service';
import { AdmAcmMassloadAttachmentService } from '~app/adm-acm/services/adm-acm-massload-attachment.service';
import { massloadAttachmentKey, massloadAttachmentSelector, State } from '~app/adm-acm/store/reducers';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';

@Component({
  selector: 'bspl-adm-acm-massload-attachment-list',
  templateUrl: './adm-acm-massload-attachment-list.component.html',
  styleUrls: ['./adm-acm-massload-attachment-list.component.scss'],
  providers: [AdmAcmMassloadAttachmentService, AdmAcmMassloadAttachmentFormatter]
})
export class AdmAcmMassloadAttachmentListComponent
  extends ListSubtabs<AdmAcmMassloadAttachment, AdmAcmMassloadAttachmentFilter>
  implements OnInit
{
  public listItems: string;
  public columns: Array<GridColumn>;
  public searchForm: FormGroup;

  public statusDropdownOptions: DropdownOption<AdmAcmMassloadFileStatus>[] = Object.values(
    AdmAcmMassloadFileStatus
  ).map(status => ({
    value: status,
    label: this.translationService.translate(`ADM_ACM.files.massload.status.${status.toLowerCase()}`)
  }));

  private formFactory: FormUtil;

  constructor(
    public displayFormatter: AdmAcmMassloadAttachmentFormatter,
    protected store: Store<AppState>,
    protected dataService: AdmAcmMassloadAttachmentService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected router: Router,
    private formBuilder: FormBuilder
  ) {
    super(store, dataService, actions$, translationService);

    this.formFactory = new FormUtil(this.formBuilder);
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<AdmAcmMassloadAttachment, AdmAcmMassloadAttachmentFilter>,
    DefaultProjectorFn<ListSubtabsState<AdmAcmMassloadAttachment, AdmAcmMassloadAttachmentFilter>>
  > {
    return massloadAttachmentSelector;
  }

  protected getListKey(): string {
    return massloadAttachmentKey;
  }

  public ngOnInit(): void {
    super.ngOnInit();

    this.columns = this.buildColumns();
    this.searchForm = this.buildForm();

    this.initializeListTitle();
  }

  public onViewDocument(row: AdmAcmMassloadAttachment) {
    const url = getViewUrlFromTransactionCode(row.documentTrnc);

    this.router.navigate([url, row.documentId]);
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: 'ADM_ACM.files.massloadAttachments.columns.filename',
        prop: 'filename',
        flexGrow: 3
      },
      {
        name: 'ADM_ACM.files.massloadAttachments.columns.documentNumber',
        prop: 'documentNumber',
        flexGrow: 2,
        cellTemplate: 'commonLinkFromObjCellTmpl'
      },
      {
        name: 'ADM_ACM.files.massloadAttachments.columns.uploadDate',
        prop: 'uploadDate',
        flexGrow: 2,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY hh:mm:ss') }
      },
      {
        name: 'ADM_ACM.files.massloadAttachments.columns.userName',
        prop: 'userName',
        flexGrow: 2
      },
      {
        name: 'ADM_ACM.files.massloadAttachments.columns.email',
        prop: 'email',
        flexGrow: 3
      }
    ];
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<AdmAcmMassloadAttachmentFilter>({
      filename: [],
      documentNumber: [],
      uploadDate: [],
      userName: [],
      email: []
    });
  }

  private initializeListTitle() {
    this.queryTotalItems$.pipe(takeUntil(this.destroy$)).subscribe(queryTotalItems => {
      this.listItems = this.translationService.translate('ADM_ACM.files.massloadAttachments.titleItems', {
        total: queryTotalItems
      });
    });
  }
}
