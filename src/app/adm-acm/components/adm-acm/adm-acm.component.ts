import { HttpClient } from '@angular/common/http';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ErrorHandler,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, filter, first, map, retry, switchMap, take, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import {
  AcdmActionType,
  AdmAcmStatus,
  ConcernIndicator,
  urlView
} from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AgentViewModel, CurrencyViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmActionEmitterType, AdmAcmIssueBE, AdmAcmIssueViewModel } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { createAdmAcmConfigService, createAdmAcmTicketService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { adapterViewFunctionsMapper, fieldMapperAdmAcm } from '~app/adm-acm/shared/helpers/view-be-mapper.config';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { DocumentService } from '~app/document/services/document.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ROUTES } from '~app/shared/constants/routes';
import { AccessType } from '~app/shared/enums';
import { ServerError } from '~app/shared/errors';
import { FormUtil } from '~app/shared/helpers';
import { buildViewFromBEObject } from '~app/shared/helpers/form-helpers/mapping/mapper.helper';
import { BspDto } from '~app/shared/models/bsp.model';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { appConfiguration } from '~app/shared/services';
import { NotificationService } from '~app/shared/services/notification.service';
import { TabService } from '~app/shared/services/tab.service';

@Component({
  selector: 'bspl-adm-acm',
  templateUrl: './adm-acm.component.html',
  styleUrls: ['./adm-acm.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    AdmAcmService,
    AdmAcmDataService,
    {
      provide: AdmAcmConfigService,
      useFactory: createAdmAcmConfigService,
      deps: [AdmAcmDataService, AdmAcmDialogService, L10nTranslationService, HttpClient, Router, Store, TabService]
    },
    {
      provide: AdmAcmTicketService,
      useFactory: createAdmAcmTicketService,
      deps: [AdmAcmDataService, DocumentService, Store]
    }
  ]
})
export class AdmAcmComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(AccordionComponent, { static: true }) accordion: AccordionComponent;
  @ViewChildren(AccordionItemComponent) accordionSections: QueryList<AccordionItemComponent>;

  public isFormLoading = true;
  public admAcmActionEmitterType = AdmAcmActionEmitterType;

  public btnDesign = ButtonDesign;
  public currency: CurrencyViewModel;
  public showSectionsError = false;
  public showDocWarning: boolean;

  public sectionsMapper: { [key in keyof Partial<AdmAcmIssueViewModel>]: string } = {
    amounts: 'amountsTaxes',
    taxes: 'amountsTaxes',
    basicInformation: 'basicInformation',
    details: 'details'
  };

  public activities: Activity[] = [];
  public commentAdded$: Subject<any>;
  public isActivityCommentVisible = false;
  public fileUploadPath: string;

  public showAcdmSpam$: Observable<boolean>;

  public headerHeight: number;

  public get isAcdmRegularized(): boolean {
    return this.itemRetrieved?.regularized;
  }

  public isAmountSectionWarning = false;

  private itemRetrieved: AdmAcmIssueBE;
  private documentIdRetrieved: string;
  private documentIdsRetrieved: string[];

  private destroy$ = new Subject();

  constructor(
    public formData: AdmAcmDataService,
    public formConfig: AdmAcmConfigService,
    public formTicket: AdmAcmTicketService,
    public admAcmService: AdmAcmService,
    private store: Store<AppState>,
    private tabService: TabService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private activatedRoute: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private dialog: DialogService,
    private reactiveSubject: ReactiveSubject,
    private acdmDialogService: AdmAcmDialogService,
    private globalErrorHandlerService: ErrorHandler
  ) {
    this.initializeRouteData();
    this.initializeRouteParams();
  }

  public ngOnInit(): void {
    this.initializeActivityPanel();
    this.initializeFormLoadingListener();
    this.initializeDialogSaveActionListener();

    this.initializeSpamVisibility();
  }

  public ngAfterViewInit(): void {
    this.updateForm();
  }

  public onCurrencyChange(value: CurrencyViewModel): void {
    this.currency = value;
    //* For now amounts will be reseted only if bsp changes
    // this.onAmountsReset();
  }

  public onChange(
    value: boolean | ConcernIndicator | string | BspDto | AgentViewModel,
    emitterType: AdmAcmActionEmitterType
  ): void {
    this.formData.getSubject(emitterType).next(value);
  }

  public onIssueClick(action: AcdmActionType): void {
    if (this.formData.isFormValid()) {
      const actionCommand = this.formConfig.getActionCommand(action, this.formData.getDataForSubmit());
      actionCommand();
    } else {
      this.handlePendingFields();
    }

    //* Error icons are shown in accordion after trying to issue for the first time
    this.showSectionsError = true;
  }

  public onShowDocsWarning(value: boolean): void {
    this.showDocWarning = value;
  }

  public onShowTaxWarning(value: boolean): void {
    this.isAmountSectionWarning = value;
  }

  public onDownloadFile(fileUrl: string): void {
    this.admAcmService.downloadFile(fileUrl).subscribe(file => {
      FileSaver.saveAs(file.blob, file.fileName);
    });
  }

  public onSendComment(message: { reason: string }): void {
    this.commentAdded$ = new Subject<any>();

    this.admAcmService
      .addComment(message, this.itemRetrieved.id)
      .pipe(tap(() => this.initializeActivities()))
      .subscribe(this.commentAdded$);
  }

  public onHeaderHeightChange(height: number): void {
    this.headerHeight = height;
  }

  public navigateToList(): void {
    const queryUrl = this.formConfig.admAcmType === MasterDataType.Adm ? ROUTES.ADM_QUERY.url : ROUTES.ACM_QUERY.url;

    this.tabService.closeCurrentTabAndNavigate(queryUrl);
  }

  private initializeFormLoadingListener(): void {
    this.formData.isFormReady
      .pipe(
        map(ready => !ready),
        takeUntil(this.destroy$)
      )
      .subscribe(loading => {
        this.isFormLoading = loading;
      });
  }

  private initializeRouteData(): void {
    this.activatedRoute.data.pipe(take(1)).subscribe(data => {
      this.itemRetrieved = data.item;
      this.formConfig.accessType = data.access;
      this.formConfig.admAcmType = data.admAcmType;
      this.formConfig.issuingPendingSupervision = data.issuingPendingSupervision;
      this.formConfig.airlineIssued = this.itemRetrieved ? this.itemRetrieved.airlineIssued : null;
    });
  }

  private initializeRouteParams(): void {
    this.activatedRoute.queryParams
      .pipe(first(), withLatestFrom(this.store.pipe(select(getUserBsps))))
      .subscribe(([queryParams, userBsps]) => {
        if (queryParams.documentId || queryParams.documentIds) {
          this.formConfig.accessType = AccessType.editTicket;
          this.documentIdRetrieved = queryParams.documentId;
          this.documentIdsRetrieved = queryParams.documentIds;
        }
        if (queryParams.bspId) {
          const bsp = userBsps.find(userBsp => userBsp.id === +queryParams.bspId);
          const { id, isoCountryCode, name } = bsp;
          this.onChange({ id, isoCountryCode, name } as BspDto, AdmAcmActionEmitterType.bsp);
        }
      });
  }

  private updateForm(): void {
    if (this.itemRetrieved) {
      buildViewFromBEObject<AdmAcmIssueBE, AdmAcmIssueViewModel>(
        this.itemRetrieved,
        this.formData.getForm(),
        this.formConfig.isReadonlyMode,
        fieldMapperAdmAcm,
        adapterViewFunctionsMapper
      );

      this.formData.isFormReady.next(true);
      this.cd.detectChanges();
    } else if (this.documentIdRetrieved) {
      this.formTicket.updateSummaryForm(this.documentIdRetrieved);
    } else if (this.documentIdsRetrieved) {
      this.formTicket.updateMultipopulationSummaryForm();
    } else {
      this.formData.isFormReady.next(true);
    }
  }

  private initializeActivityPanel(): void {
    this.fileUploadPath = `${appConfiguration.baseUploadPath}/acdm-management/files`;
    this.initializeActivities();
    this.initializeActivityCommentVisibility();
  }

  private initializeActivityCommentVisibility(): void {
    this.isActivityCommentVisible = !this.formConfig.isCreateMode;
  }

  private initializeActivities(): void {
    if (!this.formConfig.isCreateMode && this.itemRetrieved) {
      this.admAcmService
        .getActivities(this.itemRetrieved.id)
        .pipe(first())
        .subscribe(activities => {
          this.activities = activities;
          this.cd.markForCheck();
        });
    }
  }

  private initializeDialogSaveActionListener(): void {
    this.formConfig.dialogActionEmitter
      .pipe(
        filter(
          actionType =>
            actionType === AcdmActionType.issue ||
            actionType === AcdmActionType.supervise ||
            actionType === AcdmActionType.issuePendingSupervision
        ),
        switchMap(actionType => {
          let payloadToBE = this.formData.getDataForSubmit();

          if (actionType === AcdmActionType.supervise) {
            payloadToBE = { ...payloadToBE, acdmStatus: AdmAcmStatus.pending };
          }

          if (actionType === AcdmActionType.issuePendingSupervision) {
            payloadToBE = { ...payloadToBE, acdmStatus: AdmAcmStatus.pendingAuthorization };
          }

          return this.admAcmService.save(payloadToBE);
        }),
        catchError(error => {
          this.handleErrorsOnSubmit(error);

          return throwError(error);
        }),
        retry(),
        takeUntil(this.destroy$)
      )
      .subscribe(result => this.handleSuccessOnSubmit(result));

    this.initalizeDialogCancelActionListener();
  }

  private initalizeDialogCancelActionListener(): void {
    //* Issue dialog modifies form and changes are not propagated correctly unless using this
    this.reactiveSubject.asObservable
      .pipe(
        filter(event => event.clickedBtn === FooterButton.Cancel),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.cd.detectChanges());
  }

  private initializeSpamVisibility(): void {
    this.showAcdmSpam$ = this.formConfig
      .getAcdmSpam()
      .pipe(map(acdmSpam => this.formConfig.isEditTicketMode && !acdmSpam));
  }

  private handlePendingFields(): void {
    FormUtil.showControlState(this.formData.getForm());
    this.scrollToFirstInvalidSection();
    this.notificationService.showError(
      this.translationService.translate('ADM_ACM.error_submit', {
        type: this.translationService.translate(`ADM_ACM.type.${this.formConfig.admAcmType}`)
      })
    );
  }

  private handleSuccessOnSubmit(acdm: AdmAcmIssueBE): void {
    this.dialog.close();
    if (acdm) {
      const msgKey = this.formConfig.issuingPendingSupervision
        ? 'ADM_ACM.success_submit_pending'
        : 'ADM_ACM.success_submit';

      const msg = this.translationService.translate(msgKey, {
        number: acdm.ticketDocumentNumber,
        type: this.translationService.translate(`ADM_ACM.type.${this.formConfig.admAcmType}`)
      });

      this.notificationService.showSuccess(msg);
      this.navigateToAcdm(acdm.id);
    }
  }

  private handleErrorsOnSubmit(error: ServerError): void {
    this.dialog.close();

    const errorToManage: ResponseErrorBE = error.error;

    if (errorToManage?.errorCode === 400) {
      //* We process errors on Bad Request. Errors that refer to some form param
      this.formData.processErrorsOnSubmit(errorToManage);
      this.notificationService.showError(
        this.translationService.translate('ADM_ACM.error_submit', {
          type: this.translationService.translate(`ADM_ACM.type.${this.formConfig.admAcmType}`)
        })
      );

      const hasErrorsWithoutParams = errorToManage.messages.some(
        errorMessage => errorMessage.messageParams.length === 0
      );

      //* Errors on Bad Request that do not refer to any form field
      if (hasErrorsWithoutParams) {
        this.acdmDialogService
          .openErrorDialog(errorToManage, this.formConfig.admAcmType)
          .pipe(takeUntil(this.destroy$))
          .subscribe();
      } else {
        this.scrollToFirstInvalidSection();
      }
    } else {
      //* Other errors that must be handled generically
      this.globalErrorHandlerService.handleError(error);
    }
  }

  private navigateToAcdm(id: number): void {
    const queryUrl = urlView[this.formConfig.admAcmType];

    const url = `${queryUrl}/${id}`;

    this.tabService.closeCurrentTabAndNavigate(url);
  }

  private scrollToFirstInvalidSection(): void {
    const sectionWithError = this.accordionSections.find(section => {
      const id = section.nativeElement.id;

      return Object.keys(this.sectionsMapper).some(
        (formMapped: keyof Partial<AdmAcmIssueViewModel>) =>
          this.sectionsMapper[formMapped] === id && this.formData.getSectionFormGroup(formMapped).invalid
      );
    });

    if (sectionWithError) {
      this.accordion.goToItem(sectionWithError);
    }
  }

  public isSectionInvalid(sectionId: string): boolean {
    const formSection = Object.keys(this.sectionsMapper).find(id => this.sectionsMapper[id] === sectionId);

    return (
      this.formData.isFormReady.value &&
      this.formData.getSectionFormGroup(formSection as keyof AdmAcmIssueViewModel)?.invalid
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
