import { ReasonAction } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { Bsp } from '~app/shared/models/bsp.model';
import { AirlineUser, UserType } from '~app/shared/models/user.model';

export const admAcmServiceStub = jasmine.createSpyObj<AdmAcmService>('AdmAcmService', [
  'getBspsList',
  'getAgentDropDownOptions',
  'getBspDropDownOptions',
  'updateAcdmStatus',
  'getCurrencies',
  'getBspTime',
  'getBspAgentInfo',
  'getBspAirlineInfo',
  'getAirlineInfo',
  'getAgentInfo',
  'getReasonsDropDownOptions',
  'getReasons'
]);

export const admAcmTicketServiceStub = jasmine.createSpyObj<AdmAcmTicketService>('AdmAcmTicketService', [
  'getTicketId'
]);

export const expectedUserDetails: AirlineUser = {
  id: 10126,
  email: 'airline1@example.com',
  firstName: 'firstName',
  lastName: 'lastName',
  userType: UserType.AIRLINE,
  globalAirline: {
    id: 12,
    globalName: 'Test Airline',
    iataCode: '626',
    logo: '',
    airlines: [],
    version: 1
  },
  permissions: []
};

export const initialState = {
  auth: {
    user: expectedUserDetails
  },
  router: null,
  core: {
    menu: {
      tabs: {}
    }
  },
  acdm: {}
};

export const issue = {
  active: true,
  iataCode: '12345675',
  id: 1,
  address: {
    street: 'Gran Vía 1',
    city: 'Madrid',
    state: null,
    country: 'Spain',
    postalCode: '28013'
  } as AirlineAddress,
  name: 'Agent 1',
  vatNumber: 'AGE001',
  bsp: {
    id: 1,
    isoCountryCode: 'ES',
    name: 'Spain'
  } as Bsp
};

export const reason = {
  id: 1,
  description: 'description',
  reasonType: ReasonAction.issue,
  title: 'title'
};
