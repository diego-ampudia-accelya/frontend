import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ErrorHandler } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { MockComponents, MockProviders } from 'ng-mocks';
import { BehaviorSubject, of, Subject, throwError } from 'rxjs';
import { take } from 'rxjs/operators';

import { AdmAcmAmountTaxComponent } from '../adm-acm-amount-tax/adm-acm-amount-tax.component';
import { AdmAcmAmountComponent } from '../adm-acm-amount/adm-acm-amount.component';
import { AdmAcmBasicInfoComponent } from '../adm-acm-basic-info/adm-acm-basic-info.component';
import { AdmAcmDetailsComponent } from '../adm-acm-details/adm-acm-details.component';
import { AdmAcmHeaderStatusComponent } from '../adm-acm-header-status/adm-acm-header-status.component';
import { AdmAcmComponent } from './adm-acm.component';
import { AcdmActionType, AdmAcmStatus, urlView } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmActionEmitterType } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService, FooterButton, SpinnerComponent } from '~app/shared/components';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { ActivityPanelComponent } from '~app/shared/components/activity-panel/activity-panel.component';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ROUTES } from '~app/shared/constants';
import { AccessType } from '~app/shared/enums';
import { appConfiguration, NotificationService, TabService } from '~app/shared/services';

const usersBsps = [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }];

const initialState = {
  auth: {
    user: {
      bsps: usersBsps
    }
  }
};

describe('AdmAcmComponent', () => {
  let component: AdmAcmComponent;
  let fixture: ComponentFixture<AdmAcmComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdmAcmComponent,
        MockComponents(
          AdmAcmHeaderStatusComponent,
          SpinnerComponent,
          AccordionComponent,
          AdmAcmBasicInfoComponent,
          AdmAcmDetailsComponent,
          AdmAcmAmountComponent,
          AdmAcmAmountTaxComponent,
          ActivityPanelComponent,
          AccordionItemComponent
        )
      ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule
      ],
      providers: [
        provideMockStore({ initialState, selectors: [{ selector: getUserBsps, value: usersBsps }] }),
        {
          provide: AdmAcmDataService,
          useValue: {
            isFormReady: new BehaviorSubject(false),
            getSubject: () => new BehaviorSubject('status'),
            getForm: () => new FormGroup({}),
            getSectionFormGroup: () => new FormGroup({}),
            isFormValid: () => true,
            processErrorsOnSubmit: () => {}
          }
        },
        {
          provide: AdmAcmService,
          useValue: {
            downloadFile: () => of({}),
            save: () => of({}),
            addComment: () => of({}),
            getActivities: () => of({})
          }
        },
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({}),
            queryParams: of({ bspId: '1' })
          }
        },
        {
          provide: ReactiveSubject,
          useValue: {
            asObservable: of({})
          }
        },
        { provide: L10nTranslationService, useValue: { translate: () => '' } },
        MockProviders(AdmAcmTicketService, TabService, NotificationService, DialogService, ErrorHandler)
      ],
      schemas: []
    })
      .overrideComponent(AdmAcmComponent, {
        set: {
          providers: [
            {
              provide: AdmAcmDialogService,
              useValue: {
                openErrorDialog: () => of({})
              }
            },
            {
              provide: AdmAcmConfigService,
              useValue: {
                isEditTicketMode: false,
                admAcmType: MasterDataType.RefundApp,
                issuingPendingSupervision: false,
                isReadonlyMode: false,
                getActionCommand: () => {},
                isCreateMode: false,
                dialogActionEmitter: new BehaviorSubject(AcdmActionType.activity),
                getAcdmSpam: () => of(false)
              }
            }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmComponent);
    component = fixture.componentInstance;
    spyOn(component, 'ngAfterViewInit');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set true to isLoading', () => {
    component.isFormLoading = false;

    (component as any).initializeFormLoadingListener();
    component.formData.isFormReady.next(false);

    expect(component.isFormLoading).toBe(true);
  });

  describe('initializeRouteData', () => {
    beforeEach(() => {
      (component as any).activatedRoute.data = of({
        item: { id: 5, airlineIssued: false },
        access: AccessType.create,
        admAcmType: MasterDataType.AGENT_TA,
        issuingPendingSupervision: false
      });
    });

    it('should set itemRetrieved', () => {
      (component as any).itemRetrieved = null;

      (component as any).initializeRouteData();

      expect((component as any).itemRetrieved).toEqual({ id: 5, airlineIssued: false });
    });

    it('should set accessType', () => {
      component.formConfig.accessType = null;

      (component as any).initializeRouteData();

      expect(component.formConfig.accessType).toBe(AccessType.create);
    });

    it('should set admAcmType', () => {
      component.formConfig.admAcmType = null;

      (component as any).initializeRouteData();

      expect(component.formConfig.admAcmType).toBe(MasterDataType.AGENT_TA);
    });

    it('should set issuingPendingSupervision', () => {
      component.formConfig.issuingPendingSupervision = true;

      (component as any).initializeRouteData();

      expect(component.formConfig.issuingPendingSupervision).toBe(false);
    });

    it('should set airlineIssued', () => {
      component.formConfig.airlineIssued = true;

      (component as any).initializeRouteData();

      expect(component.formConfig.airlineIssued).toBe(false);
    });
  });

  describe('initializeRouteParams', () => {
    beforeEach(() => {
      (component as any).activatedRoute.queryParams = of({
        documentId: 'doc-id',
        bspId: '1'
      });
    });

    it('should set accessType', () => {
      component.formConfig.accessType = null;

      (component as any).initializeRouteParams();

      expect(component.formConfig.accessType).toBe(AccessType.editTicket);
    });

    it('should set accessType', () => {
      (component as any).documentIdRetrieved = null;

      (component as any).initializeRouteParams();

      expect((component as any).documentIdRetrieved).toBe('doc-id');
    });

    it('should call getSubject() when bspId exists in queryParams', () => {
      component.formData.getSubject = jasmine.createSpy().and.returnValue(new BehaviorSubject({}));

      (component as any).initializeRouteParams();

      expect(component.formData.getSubject).toHaveBeenCalledWith(AdmAcmActionEmitterType.bsp);
    });

    it('should set subject', fakeAsync(() => {
      const subject = new BehaviorSubject({});
      component.formData.getSubject = jasmine.createSpy().and.returnValue(subject);

      (component as any).initializeRouteParams();
      tick();

      expect(subject.value).toEqual({ id: 1, isoCountryCode: 'ES', name: 'SPAIN' });
    }));
  });

  describe('updateForm', () => {
    it('should call getForm as parameter when itemRetrieved exists', () => {
      component.formData.getForm = jasmine.createSpy().and.callThrough();
      (component as any).itemRetrieved = {};

      (component as any).updateForm();

      expect(component.formData.getForm).toHaveBeenCalled();
    });

    it('should call updateSummaryForm when documentIdRetrieved exists', () => {
      component.formTicket.updateSummaryForm = jasmine.createSpy().and.callThrough();
      (component as any).itemRetrieved = null;
      (component as any).documentIdRetrieved = 'test-id';

      (component as any).updateForm();

      expect(component.formTicket.updateSummaryForm).toHaveBeenCalledWith('test-id');
    });

    it('should set isFormReady to true when itemRetrieved and documentIdRetrieved are not exist', () => {
      component.formData.isFormReady.next(false);
      (component as any).itemRetrieved = null;
      (component as any).documentIdRetrieved = '';

      (component as any).updateForm();

      expect(component.formData.isFormReady.value).toBe(true);
    });
  });

  it('should set new value for currency', () => {
    (component as any).currency = {};
    const value = {
      code: 'code',
      decimals: 10,
      id: 1,
      isDefault: true
    };

    component.onCurrencyChange(value);

    expect((component as any).currency).toEqual(value);
  });

  describe('onChange', () => {
    it('should call getSubject', () => {
      component.formData.getSubject = jasmine.createSpy().and.returnValue(new Subject());

      component.onChange(true, AdmAcmActionEmitterType.netReporting);

      expect(component.formData.getSubject).toHaveBeenCalledWith(AdmAcmActionEmitterType.netReporting);
    });

    it('should next value to corresponding subject', () => {
      const s = new BehaviorSubject('test');
      component.formData.getSubject = jasmine.createSpy().and.returnValue(s);

      component.onChange('test-2', AdmAcmActionEmitterType.netReporting);

      expect(s.value).toBe('test-2');
    });
  });

  describe('onIssueClick', () => {
    it('should call action command when form is valid', () => {
      const func = jasmine.createSpy();
      component.formConfig.getActionCommand = jasmine.createSpy().and.returnValue(func);
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });

      component.onIssueClick(AcdmActionType.activity);

      expect(component.formConfig.getActionCommand).toHaveBeenCalledWith(AcdmActionType.activity, { id: 1 });
    });

    it('should call handlePendingFields when form is not valid', () => {
      component.formData.isFormValid = jasmine.createSpy().and.returnValue(false);
      (component as any).handlePendingFields = jasmine.createSpy();

      component.onIssueClick(AcdmActionType.activity);

      expect((component as any).handlePendingFields).toHaveBeenCalled();
    });

    it('should set showSectionsError to true', () => {
      component.showSectionsError = false;
      component.formData.isFormValid = jasmine.createSpy().and.returnValue(false);
      (component as any).handlePendingFields = jasmine.createSpy();

      component.onIssueClick(AcdmActionType.activity);

      expect(component.showSectionsError).toBe(true);
    });
  });

  it('should set value to showDocWarning', () => {
    component.showDocWarning = false;

    component.onShowDocsWarning(true);

    expect(component.showDocWarning).toBe(true);
  });

  it('should set value to isAmountSectionWarning', () => {
    (component as any).isAmountSectionWarning = false;

    component.onShowTaxWarning(true);

    expect((component as any).isAmountSectionWarning).toBe(true);
  });

  it('should call downloadFile with file url', () => {
    spyOn(FileSaver, 'saveAs');
    (component as any).admAcmService.downloadFile = jasmine.createSpy().and.returnValue(of({}));

    component.onDownloadFile('file-url');

    expect((component as any).admAcmService.downloadFile).toHaveBeenCalledWith('file-url');
  });

  describe('onSendComment', () => {
    it('should call addComment', fakeAsync(() => {
      (component as any).admAcmService.addComment = jasmine.createSpy().and.returnValue(of({}));
      (component as any).itemRetrieved = { id: 5 };

      component.onSendComment({ reason: 'reason' });
      tick();

      expect((component as any).admAcmService.addComment).toHaveBeenCalledWith({ reason: 'reason' }, 5);
    }));

    it('should call initializeActivities', fakeAsync(() => {
      (component as any).admAcmService.addComment = jasmine.createSpy().and.returnValue(of({}));
      (component as any).initializeActivities = jasmine.createSpy();
      (component as any).itemRetrieved = { id: 5 };

      component.onSendComment({ reason: 'reason' });
      tick();

      expect((component as any).initializeActivities).toHaveBeenCalled();
    }));
  });

  it('should set headerHeight', () => {
    component.headerHeight = 0;

    component.onHeaderHeightChange(10);

    expect(component.headerHeight).toBe(10);
  });

  describe('navigateToList', () => {
    it('should call closeCurrentTabAndNavigate with ADM query url', () => {
      (component as any).tabService.closeCurrentTabAndNavigate = jasmine.createSpy();
      component.formConfig.admAcmType = MasterDataType.Adm;

      component.navigateToList();

      expect((component as any).tabService.closeCurrentTabAndNavigate).toHaveBeenCalledWith(ROUTES.ADM_QUERY.url);
    });

    it('should call closeCurrentTabAndNavigate with ACM query url', () => {
      (component as any).tabService.closeCurrentTabAndNavigate = jasmine.createSpy();
      component.formConfig.admAcmType = MasterDataType.AGENT_TA;

      component.navigateToList();

      expect((component as any).tabService.closeCurrentTabAndNavigate).toHaveBeenCalledWith(ROUTES.ACM_QUERY.url);
    });
  });

  describe('initializeActivityPanel', () => {
    it('should set fileUploadPath', () => {
      component.fileUploadPath = '';

      (component as any).initializeActivityPanel();

      expect(component.fileUploadPath).toBe(`${appConfiguration.baseUploadPath}/acdm-management/files`);
    });

    it('should call initializeActivities', () => {
      (component as any).initializeActivities = jasmine.createSpy();

      (component as any).initializeActivityPanel();

      expect((component as any).initializeActivities).toHaveBeenCalled();
    });

    it('should call initializeActivityCommentVisibility', () => {
      (component as any).initializeActivityCommentVisibility = jasmine.createSpy();

      (component as any).initializeActivityPanel();

      expect((component as any).initializeActivityCommentVisibility).toHaveBeenCalled();
    });
  });

  it('should set isActivityCommentVisible', () => {
    component.isActivityCommentVisible = false;
    (component as any).formConfig.isCreateMode = false;

    (component as any).initializeActivityCommentVisibility();

    expect(component.isActivityCommentVisible).toBe(true);
  });

  describe('initializeActivities', () => {
    it('should NOT call getActivities when isCreateMode is true', () => {
      (component as any).admAcmService.getActivities = jasmine.createSpy().and.returnValue(of([]));
      (component as any).formConfig.isCreateMode = true;

      (component as any).initializeActivities();

      expect((component as any).admAcmService.getActivities).not.toHaveBeenCalled();
    });

    it('should NOT call getActivities when itemRetrieved does NOT exist', () => {
      (component as any).admAcmService.getActivities = jasmine.createSpy().and.returnValue(of([]));
      (component as any).formConfig.isCreateMode = false;
      (component as any).itemRetrieved = null;

      (component as any).initializeActivities();

      expect((component as any).admAcmService.getActivities).not.toHaveBeenCalled();
    });

    it('should call getActivities when isCreateMode is false and itemRetrieved exists', () => {
      (component as any).admAcmService.getActivities = jasmine.createSpy().and.returnValue(of([]));
      (component as any).formConfig.isCreateMode = false;
      (component as any).itemRetrieved = { id: 5 };

      (component as any).initializeActivities();

      expect((component as any).admAcmService.getActivities).toHaveBeenCalledWith(5);
    });

    it('should set activities', fakeAsync(() => {
      (component as any).admAcmService.getActivities = jasmine
        .createSpy()
        .and.returnValue(of([{ type: 'activity-type', creationDate: '01/01/2022' }]));
      (component as any).formConfig.isCreateMode = false;
      (component as any).itemRetrieved = { id: 5 };
      component.activities = [];

      (component as any).initializeActivities();
      tick();

      expect(component.activities).toEqual([{ type: 'activity-type', creationDate: '01/01/2022' }] as any);
    }));
  });

  describe('initializeDialogSaveActionListener', () => {
    it('should NOT call getDataForSubmit when emitted value was filtered', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.activity);
      tick();

      expect(component.formData.getDataForSubmit).not.toHaveBeenCalled();
    }));

    it('should call getDataForSubmit when emitted value was NOT filtered', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });
      component.admAcmService.save = jasmine.createSpy().and.returnValue(of({ id: 1, agent: {} }));

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.issue);
      tick();

      expect(component.formData.getDataForSubmit).toHaveBeenCalled();
    }));

    it('should call admAcmService.save without special acdmStatus', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });
      component.admAcmService.save = jasmine.createSpy().and.returnValue(of({ id: 1, agent: {} }));

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.issue);
      tick();

      expect(component.admAcmService.save).toHaveBeenCalledWith({ id: 1 });
    }));

    it('should call admAcmService.save with pendind acdmStatus', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });
      component.admAcmService.save = jasmine.createSpy().and.returnValue(of({ id: 1, agent: {} }));

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.supervise);
      tick();

      expect(component.admAcmService.save).toHaveBeenCalledWith({ id: 1, acdmStatus: AdmAcmStatus.pending });
    }));

    it('should call admAcmService.save with pendingAuthorization acdmStatus', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });
      component.admAcmService.save = jasmine.createSpy().and.returnValue(of({ id: 1, agent: {} }));

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.issuePendingSupervision);
      tick();

      expect(component.admAcmService.save).toHaveBeenCalledWith({
        id: 1,
        acdmStatus: AdmAcmStatus.pendingAuthorization
      });
    }));

    it('should call handleSuccessOnSubmit with save result', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });
      component.admAcmService.save = jasmine.createSpy().and.returnValue(of({ id: 1, agent: {} }));
      (component as any).handleSuccessOnSubmit = jasmine.createSpy();

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.issue);
      tick();

      expect((component as any).handleSuccessOnSubmit).toHaveBeenCalledWith({ id: 1, agent: {} });
    }));

    it('should NOT call handleSuccessOnSubmit when error occured', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });
      component.admAcmService.save = jasmine.createSpy().and.returnValue(throwError({}));
      (component as any).handleSuccessOnSubmit = jasmine.createSpy();

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.issue);
      tick();

      expect((component as any).handleSuccessOnSubmit).not.toHaveBeenCalled();
    }));

    it('should call handleErrorsOnSubmit with error when error occured', fakeAsync(() => {
      component.formData.getDataForSubmit = jasmine.createSpy().and.returnValue({ id: 1 });
      component.admAcmService.save = jasmine.createSpy().and.returnValue(throwError({ code: 500 }));
      (component as any).handleErrorsOnSubmit = jasmine.createSpy();

      (component as any).initializeDialogSaveActionListener();
      component.formConfig.dialogActionEmitter.next(AcdmActionType.issue);
      tick();

      expect((component as any).handleErrorsOnSubmit).toHaveBeenCalledWith({ code: 500 });
    }));

    it('should call initalizeDialogCancelActionListener', () => {
      (component as any).initalizeDialogCancelActionListener = jasmine.createSpy();

      (component as any).initializeDialogSaveActionListener();

      expect((component as any).initalizeDialogCancelActionListener).toHaveBeenCalled();
    });
  });

  describe('initalizeDialogCancelActionListener', () => {
    it('should call detectChanges when cancel click', fakeAsync(() => {
      (component as any).cd.detectChanges = jasmine.createSpy();
      (component as any).reactiveSubject.asObservable = new BehaviorSubject({ clickedBtn: FooterButton.Add });

      (component as any).initalizeDialogCancelActionListener();
      (component as any).reactiveSubject.asObservable.next({ clickedBtn: FooterButton.Cancel });
      tick();

      expect((component as any).cd.detectChanges).toHaveBeenCalled();
    }));

    it('should NOT call detectChanges when NOT cancel click', fakeAsync(() => {
      (component as any).cd.detectChanges = jasmine.createSpy();
      (component as any).reactiveSubject.asObservable = new BehaviorSubject({ clickedBtn: FooterButton.Add });

      (component as any).initalizeDialogCancelActionListener();
      (component as any).reactiveSubject.asObservable.next({ clickedBtn: FooterButton.Accept });
      tick();

      expect((component as any).cd.detectChanges).not.toHaveBeenCalled();
    }));
  });

  describe('initializeSpamVisibility', () => {
    it('should call getAcdmSpam', fakeAsync(() => {
      component.formConfig.getAcdmSpam = jasmine.createSpy().and.returnValue(of(false));

      (component as any).initializeSpamVisibility();
      component.showAcdmSpam$.pipe(take(1)).subscribe();
      tick();

      expect(component.formConfig.getAcdmSpam).toHaveBeenCalled();
    }));

    it('should return false to show acdm spam when isEditTicketMode is false', fakeAsync(() => {
      component.formConfig.getAcdmSpam = jasmine.createSpy().and.returnValue(of(false));
      (component as any).formConfig.isEditTicketMode = false;
      let res;

      (component as any).initializeSpamVisibility();
      component.showAcdmSpam$.pipe(take(1)).subscribe(data => (res = data));
      tick();

      expect(res).toBe(false);
    }));

    it('should return false to show acdm spam when acdmSpam is true', fakeAsync(() => {
      component.formConfig.getAcdmSpam = jasmine.createSpy().and.returnValue(of(true));
      (component as any).formConfig.isEditTicketMode = true;
      let res;

      (component as any).initializeSpamVisibility();
      component.showAcdmSpam$.pipe(take(1)).subscribe(data => (res = data));
      tick();

      expect(res).toBe(false);
    }));
  });

  describe('handlePendingFields', () => {
    beforeEach(() => {
      (component as any).scrollToFirstInvalidSection = jasmine.createSpy();
    });

    it('should call scrollToFirstInvalidSection', () => {
      (component as any).handlePendingFields();

      expect((component as any).scrollToFirstInvalidSection).toHaveBeenCalled();
    });

    it('should call showError', () => {
      (component as any).notificationService.showError = jasmine.createSpy();

      (component as any).handlePendingFields();

      expect((component as any).notificationService.showError).toHaveBeenCalled();
    });
  });

  describe('handleSuccessOnSubmit', () => {
    it('should close dialog', () => {
      (component as any).dialog.close = jasmine.createSpy();

      (component as any).handleSuccessOnSubmit(null);

      expect((component as any).dialog.close).toHaveBeenCalled();
    });

    it('should call translate for type', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      component.formConfig.admAcmType = MasterDataType.Acm;

      (component as any).handleSuccessOnSubmit({ id: 5, ticketDocumentNumber: 10 });

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.first().args).toEqual(['ADM_ACM.type.acm']);
    });

    it('should call translate for message with success submit pendind', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      component.formConfig.admAcmType = MasterDataType.Acm;
      component.formConfig.issuingPendingSupervision = true;

      (component as any).handleSuccessOnSubmit({ id: 5, ticketDocumentNumber: 10 });

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.mostRecent().args).toEqual([
        'ADM_ACM.success_submit_pending',
        {
          number: 10,
          type: 'test'
        }
      ]);
    });

    it('should call translate for message with success submit', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      component.formConfig.admAcmType = MasterDataType.Acm;
      component.formConfig.issuingPendingSupervision = false;

      (component as any).handleSuccessOnSubmit({ id: 5, ticketDocumentNumber: 10 });

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.mostRecent().args).toEqual([
        'ADM_ACM.success_submit',
        {
          number: 10,
          type: 'test'
        }
      ]);
    });

    it('should call showSuccess when acdm param is not empty', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      (component as any).notificationService.showSuccess = jasmine.createSpy();

      (component as any).handleSuccessOnSubmit({ id: 5, ticketDocumentNumber: 10 });

      expect((component as any).notificationService.showSuccess).toHaveBeenCalledWith('test');
    });

    it('should call navigateToAcdm when acdm param is not empty', () => {
      (component as any).navigateToAcdm = jasmine.createSpy();

      (component as any).handleSuccessOnSubmit({ id: 5, ticketDocumentNumber: 10 });

      expect((component as any).navigateToAcdm).toHaveBeenCalledWith(5);
    });

    it('should NOT call navigateToAcdm when acdm is empty', () => {
      (component as any).navigateToAcdm = jasmine.createSpy();

      (component as any).handleSuccessOnSubmit(null);

      expect((component as any).navigateToAcdm).not.toHaveBeenCalled();
    });
  });

  describe('handleErrorsOnSubmit', () => {
    beforeEach(() => {
      (component as any).scrollToFirstInvalidSection = jasmine.createSpy();
    });

    it('should close dialog', () => {
      (component as any).dialog.close = jasmine.createSpy();

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 500 } });

      expect((component as any).dialog.close).toHaveBeenCalled();
    });

    it('should handle error as global', () => {
      (component as any).globalErrorHandlerService.handleError = jasmine.createSpy();

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 500 } });

      expect((component as any).globalErrorHandlerService.handleError).toHaveBeenCalledWith({
        error: { errorCode: 500 }
      });
    });

    it('should NOT handle error as global', () => {
      (component as any).globalErrorHandlerService.handleError = jasmine.createSpy();

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 400, messages: [] } });

      expect((component as any).globalErrorHandlerService.handleError).not.toHaveBeenCalled();
    });

    it('should call translate for type', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      component.formConfig.admAcmType = MasterDataType.Acm;

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 400, messages: [] } });

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.first().args).toEqual(['ADM_ACM.type.acm']);
    });

    it('should call translate for message with error submit', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      component.formConfig.admAcmType = MasterDataType.Acm;

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 400, messages: [] } });

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.mostRecent().args).toEqual([
        'ADM_ACM.error_submit',
        {
          type: 'test'
        }
      ]);
    });

    it('should call showError when error code is 400', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      (component as any).notificationService.showError = jasmine.createSpy();

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 400, messages: [] } });

      expect((component as any).notificationService.showError).toHaveBeenCalledWith('test');
    });

    it('should call openErrorDialog when error without params exists', fakeAsync(() => {
      (component as any).acdmDialogService.openErrorDialog = jasmine.createSpy().and.returnValue(of({}));
      (component as any).formConfig.admAcmType = MasterDataType.Acm;

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 400, messages: [{ messageParams: [] }] } });
      tick();

      expect((component as any).acdmDialogService.openErrorDialog).toHaveBeenCalledWith(
        {
          errorCode: 400,
          messages: [{ messageParams: [] }]
        },
        MasterDataType.Acm
      );
    }));

    it('should NOT call openErrorDialog when error without params doesnt exist', fakeAsync(() => {
      (component as any).acdmDialogService.openErrorDialog = jasmine.createSpy().and.returnValue(of({}));
      (component as any).formConfig.admAcmType = MasterDataType.Acm;

      (component as any).handleErrorsOnSubmit({ error: { errorCode: 400, messages: [{ messageParams: ['msg'] }] } });
      tick();

      expect((component as any).acdmDialogService.openErrorDialog).not.toHaveBeenCalled();
    }));
  });

  it('should call closeCurrentTabAndNavigate with corresponding url', () => {
    (component as any).tabService.closeCurrentTabAndNavigate = jasmine.createSpy();
    component.formConfig.admAcmType = MasterDataType.Adm;
    const url = `${urlView.adm}/1`;

    (component as any).navigateToAcdm(1);

    expect((component as any).tabService.closeCurrentTabAndNavigate).toHaveBeenCalledWith(url);
  });

  describe('scrollToFirstInvalidSection', () => {
    it('should NOT call goToItem when no sections with error', () => {
      (component as any).accordion = { goToItem: jasmine.createSpy() };
      component.sectionsMapper = {
        test: '1',
        test2: '2'
      } as any;
      component.accordionSections = [{ nativeElement: { id: '1' } }] as any;
      component.formData.getSectionFormGroup = jasmine.createSpy().and.returnValues({ invalid: false });

      (component as any).scrollToFirstInvalidSection();

      expect((component as any).accordion.goToItem).not.toHaveBeenCalled();
    });

    it('should call goToItem when one of sections has error', () => {
      component.sectionsMapper = {
        test: '1',
        test2: '2'
      } as any;
      (component as any).accordion = { goToItem: jasmine.createSpy() };
      component.accordionSections = [{ nativeElement: { id: '1' } }] as any;
      component.formData.getSectionFormGroup = jasmine.createSpy().and.returnValue({ invalid: true });

      (component as any).scrollToFirstInvalidSection();

      expect((component as any).accordion.goToItem).toHaveBeenCalled();
    });
  });

  describe('isSectionInvalid', () => {
    it('should return false when isFormReady is false', () => {
      component.formData.isFormReady.next(false);
      component.formData.getSectionFormGroup = jasmine.createSpy().and.returnValue({ invalid: true });

      const res = component.isSectionInvalid('details');

      expect(res).toBe(false);
    });

    it('should return false when invalid is false', () => {
      component.formData.isFormReady.next(true);
      component.formData.getSectionFormGroup = jasmine.createSpy().and.returnValue({ invalid: false });

      const res = component.isSectionInvalid('details');

      expect(res).toBe(false);
    });

    it('should return true when isFormReady is true and invalid is true', () => {
      component.formData.isFormReady.next(true);
      component.formData.getSectionFormGroup = jasmine.createSpy().and.returnValue({ invalid: true });

      const res = component.isSectionInvalid('details');

      expect(res).toBe(true);
    });
  });
});
