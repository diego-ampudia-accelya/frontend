import { AbstractControl, FormArray, ValidatorFn } from '@angular/forms';

import { TaxMiscellaneousFee } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';

/**
 * Validates if a tax amount form is valid together with its fields. Conditions are:
 * ```
 * a line is partially filled, `ISO Tax Code control` is required.
 * a line is empty and it is NOT the last line, `ISO Tax Code control` is required
 * a line is empty but it is the last line, `ISO Tax Code control` is NOT required 
 ```
 *
 * @param isDifferenceFieldReadOnly difference field is readonly
 * @returns error object if any field is required and was not filled
 */
export function taxRequiredValidator(isDifferenceFieldReadOnly: boolean): ValidatorFn {
  return (formArray: FormArray): { [key: string]: boolean } => {
    let error = null;

    if (formArray.value) {
      const amountsToCheck: Array<keyof TaxMiscellaneousFee> = isDifferenceFieldReadOnly
        ? ['agentAmount', 'airlineAmount']
        : ['taxDifferenceAmount'];
      const emptyRequiredControls: AbstractControl[] = [];

      formArray.controls.forEach((lineGroup, index, controls) => {
        const typeControl = lineGroup.get('type');
        const isAnyAmountFilled = amountsToCheck.some(key => lineGroup.get(key).value);

        if (!typeControl.value && ((controls.length !== 1 && index !== controls.length - 1) || isAnyAmountFilled)) {
          typeControl.setErrors({ required: true });
        } else if (typeControl.getError('required')) {
          typeControl.setErrors(null);
        }
      });

      if (emptyRequiredControls.length) {
        error = {
          error: 'required'
        };
      }
    }

    return error;
  };
}
