import { AbstractControl } from '@angular/forms';

import { GLOBALS } from '~app/shared/constants/globals';

export function taxIsoCodePatternValidator(
  control: AbstractControl
): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  if (control.value && !control.value.match(GLOBALS.PATTERNS.ACDMS.TAX)) {
    result = {
      taxIsoCodePattern: { key: 'ADM_ACM.amounts.error.taxIsoCodePattern' }
    };
  }

  return result;
}
