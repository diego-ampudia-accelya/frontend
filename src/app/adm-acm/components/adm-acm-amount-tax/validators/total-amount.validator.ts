import { AbstractControl, ValidatorFn } from '@angular/forms';

import { greaterValidator } from '~app/shared/validators/greater.validator';

/**
 * Validates if totalAmount is greater than zero when Amounts Form is dirty.
 *
 * @returns error object if totalAmount is equal or less than zero and Amounts Form is dirty
 */
export function totalAmountValidator(control: AbstractControl): { [key: string]: ValidatorFn } | null {
  let error = null;

  if (control.parent && control.parent.touched) {
    error = greaterValidator(0)(control);
  }

  return error;
}
