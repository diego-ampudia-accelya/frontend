import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { initialState } from '../adm-acm/adm-acm-test.config';
import { AdmAcmAmountTaxComponent } from './adm-acm-amount-tax.component';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService, InputComponent } from '~app/shared/components';
import { TabService } from '~app/shared/services';

describe('AdmAcmAmountTaxComponent', () => {
  let component: AdmAcmAmountTaxComponent;
  let fixture: ComponentFixture<AdmAcmAmountTaxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmAmountTaxComponent, InputComponent],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        L10nTranslationModule.forRoot(l10nConfig),
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        provideMockStore({ initialState }),
        DialogService,
        AdmAcmDialogService,
        PermissionsService,
        L10nTranslationService,
        HttpClient,
        FormBuilder,
        HttpTestingController,
        TabService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmAmountTaxComponent);
    component = fixture.componentInstance;
    component.formData = new AdmAcmDataService(TestBed.inject(FormBuilder));
    component.formConfig = createAdmAcmConfigService(
      component.formData,
      TestBed.inject(AdmAcmDialogService),
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Router),
      TestBed.inject(Store),
      TestBed.inject(TabService)
    );
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the difference correctly', done => {
    component.isDifferenceFieldReadOnly = true;
    component['reverseOrderFactor'] = -1;
    const firstRow = component.form.at(0);
    const agentControl = firstRow.get('agentAmount');
    const airlineControl = firstRow.get('airlineAmount');
    const differenceControl = firstRow.get('taxDifferenceAmount');

    agentControl.setValue('3.035', { emitEvent: false });
    airlineControl.setValue('5.06');

    fixture.whenStable().then(() => {
      expect(differenceControl.value).toBe('2.02');
      done();
    });
  });

  it('should remove consecutive empty lines', done => {
    spyOn(component, 'removeTaxRows').and.callThrough();
    spyOn(component, 'onClear');

    component.form.controls.push(
      component['initialFormGroup'],
      component['initialFormGroup'],
      component['initialFormGroup']
    );

    component['reduceTableLines']();

    fixture.whenStable().then(() => {
      expect(component.form.length).toBe(1);
      expect(component.removeTaxRows).toHaveBeenCalled();
      expect(component.onClear).toHaveBeenCalledTimes(0);
      done();
    });
  });

  it('should clear a single-row-form if removeRow is clicked', () => {
    spyOn(component, 'onClear');

    component.removeTaxRows(0);
    expect(component.onClear).toHaveBeenCalled();
  });

  it('should add a line only if the form is valid', () => {
    component.isDifferenceFieldReadOnly = false;

    fixture.detectChanges();

    expect(component.disableAddBtn()).toBe(true);

    component.form.at(0).get('type').setValue('AA');

    component.addTaxRow();
    expect(component.form.length).toBe(2);
  });

  it('should remove a line if there is more than one', () => {
    component.form.controls.push(component['initialFormGroup']);
    expect(component.form.length).toBe(2);
    component.removeTaxRows(1);
    expect(component.form.length).toBe(1);
  });
});
