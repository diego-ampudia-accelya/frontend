import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren
} from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

import { taxIsoCodePatternValidator } from './validators/tax-iso-code-pattern.validator';
import { taxRequiredValidator } from './validators/tax-required.validator';
import { TaxMiscellaneousFee } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { CurrencyViewModel, TaxViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { roundCalculationValue } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { InputComponent } from '~app/shared/components/input/input.component';
import { AlertMessageType } from '~app/shared/enums';
import { NumberHelper } from '~app/shared/helpers';
import { FormList } from '~app/shared/helpers/form-helpers/list/form-list.helper';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { maxLengthRoundedValidator } from '~app/shared/validators/max-length-rounded.validator';
import { positiveNumberValidator } from '~app/shared/validators/positive-number.validator';

@Component({
  selector: 'bspl-adm-acm-amount-tax',
  templateUrl: './adm-acm-amount-tax.component.html',
  styleUrls: ['./adm-acm-amount-tax.component.scss']
})
export class AdmAcmAmountTaxComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChildren('taxCodeType') taxCodeTypes: QueryList<InputComponent>;
  @Output() totalTaxAmountChange = new EventEmitter<string>();
  @Output() showTaxRepeatedWarning = new EventEmitter<boolean>();

  @Input() formData: AdmAcmDataService;
  @Input() formConfig: AdmAcmConfigService;
  @Input() currency: CurrencyViewModel;

  public form: FormArray;

  public btnDesign = ButtonDesign;

  public fields: Array<keyof TaxMiscellaneousFee>;
  public isDifferenceFieldReadOnly: boolean;
  public amountPlaceholder = '';
  public differenceTotal = '0';

  public decimalsPattern: RegExp;

  public showTaxRepeatedMsg = false;
  public taxesRepeatedMessageType = AlertMessageType.warning;

  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)

  private reverseOrderFactor: 1 | -1;
  private defaultAmountValidator: ValidatorFn[];

  private formFactory: FormUtil;
  private destroy$ = new Subject();

  constructor(private fb: FormBuilder) {
    this.formFactory = new FormUtil(this.fb);
  }

  private get decimals(): number {
    return this.currency ? this.currency.decimals : 2;
  }

  public ngOnInit() {
    this.setConfig();
    this.buildForm();

    if (!this.isAcdmd) {
      this.initializeChangesListener();

      this.differenceTotal = this.round(this.differenceTotal);
      this.amountPlaceholder = Number(0).toFixed(this.decimals);
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.currency && !changes.currency.firstChange) {
      this.updateControls();
    }
  }

  private initializeChangesListener() {
    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      this.updateControls();
      this.reduceTableLines();
      this.validateDuplicatedTaxes(value);
    });
  }

  private setConfig(): void {
    this.fields = this.formConfig.getTaxAmountCalculationFields();
    const agentField: keyof TaxMiscellaneousFee = 'agentAmount';
    const airlineField: keyof TaxMiscellaneousFee = 'airlineAmount';

    this.isDifferenceFieldReadOnly = this.fields.includes(agentField) && this.fields.includes(airlineField);
    this.reverseOrderFactor = this.formConfig.reverseOrderFactor;
    this.decimalsPattern = this.formConfig.getDecimalsPattern();
    this.isAcdmd =
      this.formConfig.admAcmType === MasterDataType.Admd || this.formConfig.admAcmType === MasterDataType.Acmd;
  }

  private buildForm(): void {
    this.defaultAmountValidator = [
      positiveNumberValidator,
      Validators.pattern(this.decimalsPattern),
      maxLengthRoundedValidator(this.decimals, NumberHelper.getMonetaryAmountMaxLength(this.decimals))
    ];

    this.form = this.fb.array([this.initialFormGroup], {
      validators: [taxRequiredValidator(this.isDifferenceFieldReadOnly)],
      updateOn: 'blur'
    });

    this.formData.setSectionFormGroup(
      'taxes',
      this.formFactory.createGroup<TaxViewModel>({
        taxMiscellaneousFees: this.form
      })
    );
  }

  private updateControls() {
    this.amountPlaceholder = Number(0).toFixed(this.decimals);

    let diff = 0;

    this.form.controls.forEach(group => {
      const difference = FormUtil.get<TaxMiscellaneousFee>(group as FormGroup, 'taxDifferenceAmount');
      const agent = FormUtil.get<TaxMiscellaneousFee>(group as FormGroup, 'agentAmount');
      const airline = FormUtil.get<TaxMiscellaneousFee>(group as FormGroup, 'airlineAmount');
      const type = FormUtil.get<TaxMiscellaneousFee>(group as FormGroup, 'type');

      FormUtil.regularizeCalculationControls(difference, agent, airline);

      if (type.value && type.valid) {
        type.setValue(type.value.toUpperCase(), { onlySelf: true });
      }

      let differenceAmount;
      if (this.isDifferenceFieldReadOnly) {
        if (agent.value && agent.valid) {
          agent.setValue(this.round(agent.value), { onlySelf: true });
        }
        if (airline.value && airline.valid) {
          airline.setValue(this.round(airline.value), { onlySelf: true });
        }

        differenceAmount = this.getDifference(agent, airline);
      } else {
        differenceAmount = difference.value && difference.valid ? this.round(difference.value) : 0;
      }

      if ((difference.value && difference.valid) || this.isDifferenceFieldReadOnly) {
        difference.setValue(this.round(Math.abs(differenceAmount)), {
          onlySelf: true
        });
      }

      diff += parseFloat(differenceAmount);
    });

    const newTotal = this.round(diff);

    if (this.differenceTotal !== newTotal) {
      this.totalTaxAmountChange.emit(newTotal);
    }

    this.differenceTotal = this.round(diff);
  }

  private reduceTableLines() {
    let isPreviousGroupEmpty = false;
    const indexesToRemove = [];

    this.form.controls.forEach((lineGroup, index) => {
      const isCurrentGroupEmpty = Object.values((lineGroup as FormGroup).controls).every(control => !control.value);

      if (isCurrentGroupEmpty && isPreviousGroupEmpty) {
        indexesToRemove.push(index - 1);
      }

      isPreviousGroupEmpty = isCurrentGroupEmpty;
    });

    if (indexesToRemove.length) {
      this.removeTaxRows(...indexesToRemove);
    }
  }

  private validateDuplicatedTaxes(value: Array<TaxMiscellaneousFee>) {
    this.showTaxRepeatedMsg = value.some(
      (tax: TaxMiscellaneousFee, i: number, taxList: TaxMiscellaneousFee[]) =>
        !!tax.type &&
        taxList.some(
          (t: TaxMiscellaneousFee, j: number) => i !== j && !!t.type && t.type.toUpperCase() === tax.type.toUpperCase()
        )
    );
    this.showTaxRepeatedWarning.emit(this.showTaxRepeatedMsg);
  }

  public addTaxRow() {
    if (this.form.valid) {
      this.focusTaxTypeWhenInserted();
      this.form.push(this.initialFormGroup);
    }
  }

  private focusTaxTypeWhenInserted() {
    this.taxCodeTypes.changes.pipe(take(1)).subscribe(() => this.taxCodeTypes.last.focusElement());
  }

  public onClear() {
    while (this.form.length > 1) {
      this.form.removeAt(0);
    }

    this.form.reset({ emitEvent: false });
  }

  public removeTaxRows(...indexes: Array<number>) {
    if (this.form.length > 1) {
      while (indexes.length > 0 && this.form.length > 1) {
        this.form.removeAt(indexes.pop());
      }
    } else if (indexes.length === 1) {
      this.onClear();
    }
  }

  public disableAddBtn(): boolean {
    return FormList.isAddLineDisabled(this.form);
  }

  /**
   * Get empty taxes form group
   *
   * Initial form group to push into FormArray (initialization and add new rows)
   */
  private get initialFormGroup() {
    return this.formFactory.createGroup<TaxMiscellaneousFee>({
      type: ['', [taxIsoCodePatternValidator]],
      agentAmount: ['', this.defaultAmountValidator],
      airlineAmount: ['', this.defaultAmountValidator],
      taxDifferenceAmount: ['', this.defaultAmountValidator]
    });
  }

  private getDifference(agentControl: AbstractControl, airlineControl: AbstractControl): string {
    return this.round((agentControl.value - airlineControl.value) * this.reverseOrderFactor);
  }

  private round(value): string {
    return roundCalculationValue(value, this.decimals);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
