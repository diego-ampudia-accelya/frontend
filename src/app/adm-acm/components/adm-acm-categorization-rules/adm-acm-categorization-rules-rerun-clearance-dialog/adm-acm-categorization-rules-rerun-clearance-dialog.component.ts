import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';

import { CATEGORIZATION_RULES_RERUN_CONSTANTS } from './adm-acm-categorization-rules-rerun-clearance-dialog.constants';
import { AllOption } from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmRule, AdmAcmRulesRerunClearanceDialogViewModel } from '~app/adm-acm/models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { lastDayOfMonthToShortIsoDate, toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { AirlineSummary } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { AirlineDictionaryService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-adm-acm-categorization-rules-rerun-clearance-dialog',
  templateUrl: './adm-acm-categorization-rules-rerun-clearance-dialog.component.html',
  styleUrls: ['./adm-acm-categorization-rules-rerun-clearance-dialog.component.scss']
})
export class AdmAcmCategorizationRulesRerunClearanceDialogComponent implements AfterViewInit, OnInit, OnDestroy {
  public form: FormGroup;

  public userBspOptions: DropdownOption<BspDto>[];
  public airlineDropdownOptions: DropdownOption<AirlineSummary>[];

  public infoText: string;

  public isBspOptionsLocked = false;
  public isAirlineOptionsLocked = false;

  public allBspFilteringOption = {
    value: CATEGORIZATION_RULES_RERUN_CONSTANTS.ALL_OPTIONS_BSP_VALUE,
    label: this.getAllOptionTranslation()
  };

  private formUtil: FormUtil;
  private airlineControl: AbstractControl;

  private reRunButton: ModalAction;
  private clearanceButton: ModalAction;

  private destroy$ = new Subject();

  private isClearanceMode = this.config.data.isClearanceMode;

  private item: AdmAcmRule = this.config.data.item;

  private isOneAirlineSelected = false;

  private reRunButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Rerun),
    takeUntil(this.destroy$)
  );

  private clearanceButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Clearance),
    takeUntil(this.destroy$)
  );

  constructor(
    public formBuilder: FormBuilder,
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public translationService: L10nTranslationService,
    public notificationService: NotificationService,
    private bspsDictionaryService: BspsDictionaryService,
    private admAcmCategorizationRulesService: AdmAcmCategorizationRulesService,
    private airlineDictionaryService: AirlineDictionaryService
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.createForm();
    this.initializeInfoText();
    this.initializeFormFeatures();
    this.initializeDropdownsOptions();
    this.onBspChange().subscribe();
    this.initializeButtons();
    this.initializeButtonsListeners();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.reRunButton) {
        this.reRunButton.isDisabled = status !== 'VALID';
      }
      if (this.clearanceButton) {
        this.clearanceButton.isDisabled = status !== 'VALID';
      }
    });
  }

  public onBspChange(): Observable<any> {
    return FormUtil.get<AdmAcmRulesRerunClearanceDialogViewModel>(this.form, 'bsp').valueChanges.pipe(
      tap(bsp => {
        if (bsp?.length) {
          this.modifyAirlineDropDownByBsp(bsp);
        }

        if (!this.isOneAirlineSelected) {
          this.airlineControl.reset();
        }
      }),
      takeUntil(this.destroy$)
    );
  }

  private initializeInfoText(): void {
    const clearanceMessage = 'ADM_ACM.categorizationRules.dialog.clearanceMessage';
    const rerunMessage = 'ADM_ACM.categorizationRules.dialog.rerunMessage';
    const message = this.isClearanceMode ? clearanceMessage : rerunMessage;

    this.infoText = this.translationService.translate(message);
  }

  private initializeFormFeatures(): void {
    this.airlineControl = FormUtil.get<AdmAcmRulesRerunClearanceDialogViewModel>(this.form, 'airline');
  }

  private initializeButtons(): void {
    this.reRunButton = this.config.data.buttons.find(button => button.type === FooterButton.Rerun);
    if (this.reRunButton) {
      this.reRunButton.isDisabled = true;
    }

    this.clearanceButton = this.config.data.buttons.find(button => button.type === FooterButton.Clearance);
    if (this.clearanceButton) {
      this.clearanceButton.isDisabled = true;
    }
  }

  private initializeButtonsListeners(): void {
    this.reRunButtonListener();
    this.clearanceButtonlistener();
  }

  private reRunButtonListener(): void {
    this.reRunButtonClick$.pipe(switchMap(() => this.requestRerun())).subscribe(() => {
      const translateMessage = 'ADM_ACM.categorizationReasons.dialog.rerunSuccessMessage';

      this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      this.dialogService.close();
    });
  }

  private clearanceButtonlistener(): void {
    this.clearanceButtonClick$.pipe(switchMap(() => this.requestRerun())).subscribe(() => {
      const translateMessage = 'ADM_ACM.categorizationReasons.dialog.clearanceSuccessMessage';

      this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      this.dialogService.close();
    });
  }

  private requestRerun(): Observable<AdmAcmRule> {
    const issueDateControl = FormUtil.get<AdmAcmRulesRerunClearanceDialogViewModel>(this.form, 'issueDate');
    const bspControlValue = FormUtil.get<AdmAcmRulesRerunClearanceDialogViewModel>(this.form, 'bsp').value;
    const initialDate = issueDateControl.value && issueDateControl.value[0];
    const finalDate = issueDateControl.value && issueDateControl.value[1];
    const airlineCodeValue = this.airlineControl.value?.code;
    const isAllSelected = bspControlValue.some(bsp => bsp.isoCountryCode === AllOption.VALUE);

    const itemToRequest = {
      active: !this.isClearanceMode,
      airlineCode: airlineCodeValue !== AllOption.VALUE ? airlineCodeValue : null,
      bspIds: isAllSelected ? null : bspControlValue.map(bsp => bsp.id),
      fromIssueDate: initialDate && toShortIsoDate(initialDate),
      toIssueDate: finalDate && lastDayOfMonthToShortIsoDate(finalDate)
    };

    return this.admAcmCategorizationRulesService.rerun(itemToRequest, this.item.id);
  }

  private modifyAirlineDropDownByBsp(selectedBsp: Bsp[]) {
    let bspId: number;

    if (selectedBsp.length === 1 && selectedBsp[0].isoCountryCode !== AllOption.VALUE) {
      bspId = selectedBsp[0].id;
    }

    this.airlineDictionaryService
      .getDropdownOptions(bspId)
      .pipe(takeUntil(this.destroy$))
      .subscribe(airlines => (this.airlineDropdownOptions = this.addAllOptionToAirlines(airlines)));
  }

  private createForm(): void {
    this.form = this.formUtil.createGroup<AdmAcmRulesRerunClearanceDialogViewModel>({
      bsp: [null, Validators.required],
      airline: [null, Validators.required],
      issueDate: []
    });
  }

  private initializeDropdownsOptions(): void {
    combineLatest([
      this.bspsDictionaryService.getAllBspDropdownOptions(),
      this.airlineDictionaryService.getDropdownOptions()
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(options => {
        const itemBsps = this.item.isoCountryCodes;
        const airlinesSelected = this.item.airlineCode;

        this.userBspOptions = options[0];
        this.airlineDropdownOptions = this.addAllOptionToAirlines(options[1]);

        if (itemBsps) {
          this.setDropdownsWhenNoAllBsps();
        }

        if (airlinesSelected) {
          this.setDropdownsWhenNoAllAirlines();
        }
      });
  }

  private setDropdownsWhenNoAllBsps(): void {
    const itemBsps = this.item.isoCountryCodes;

    this.userBspOptions = this.userBspOptions.filter(bsp => itemBsps.includes(bsp.value.isoCountryCode));
  }

  private setDropdownsWhenNoAllAirlines(): void {
    const airlinesSelected = this.item.airlineCode;

    if (airlinesSelected !== AllOption.VALUE.toUpperCase()) {
      const airlineToPatch = this.airlineDropdownOptions.filter(airline => airline.value.code === airlinesSelected);
      const airlineControl = FormUtil.get<AdmAcmRulesRerunClearanceDialogViewModel>(this.form, 'airline');

      airlineControl.patchValue(airlineToPatch[0]?.value);
      this.isAirlineOptionsLocked = true;
      this.isOneAirlineSelected = true;
    }
  }

  private addAllOptionToAirlines(
    airlines: DropdownOption<AirlineSummary>[]
  ): Partial<DropdownOption<AirlineSummary>[]> {
    return [
      {
        value: { id: 0, name: AllOption.LABEL, code: AllOption.VALUE, designator: AllOption.LABEL },
        label: this.getAllOptionTranslation()
      },
      ...airlines
    ];
  }

  private getAllOptionTranslation(): string {
    return this.translationService.translate(`dropdown.${AllOption.LABEL}`);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
