import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { combineLatest, of, Subject } from 'rxjs';
import { AdmAcmRulesRerunClearanceDialogViewModel, AllOption } from '~app/adm-acm/models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { AirlineSummary, DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { AirlineDictionaryService, AppConfigurationService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { TranslatePipeMock } from '~app/test';
import { AdmAcmCategorizationRulesRerunClearanceDialogComponent } from './adm-acm-categorization-rules-rerun-clearance-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Deactivate,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.RERUN',
        buttonDesign: 'tertiary',
        type: 'rerun'
      },
      {
        title: 'BUTTON.DEFAULT.CLEARANCE',
        buttonDesign: 'primary',
        type: 'clearance',
        isDisabled: true
      }
    ],
    item: {
      id: '001',
      primaryReason: 'primary Reason',
      primaryReasonId: '003',
      subReason: 'sub Reason',
      active: true,
      isoCountryCodes: 'code',
      airlineCode: ['code1', 'code2']
    }
  }
} as DialogConfig;

const mockAirlineOptions: DropdownOption<AirlineSummary>[] = [
  {
    value: {
      id: 1,
      name: 'AIRLINE 001',
      code: '001',
      designator: 'L1'
    },
    label: '001 / AIRLINE 001'
  },
  {
    value: {
      id: 2,
      name: 'AIRLINE 002',
      code: '002',
      designator: 'L2'
    },
    label: '002 / AIRLINE 002'
  }
];

const userBspOptions: DropdownOption<Bsp>[] = [
  {
    value: {
      id: 1,
      name: 'ES',
      isoCountryCode: 'ES'
    },
    label: 'ES'
  }
];

describe('AdmAcmCategorizationRulesRerunClearanceDialogComponent', () => {
  let component: AdmAcmCategorizationRulesRerunClearanceDialogComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationRulesRerunClearanceDialogComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;
  const admAcmCategorizationRulesServiceSpy = createSpyObject(AdmAcmCategorizationRulesService);
  const AppConfigurationServiceSpy = createSpyObject(AppConfigurationService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);

    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationRulesRerunClearanceDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        FormBuilder,
        ReactiveSubject,
        {
          provide: BspsDictionaryService,
          useValue: bspsDictionaryServiceSpy
        },
        {
          provide: AirlineDictionaryService,
          useValue: airlineDictionaryServiceSpy
        },
        HttpClient,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: AdmAcmCategorizationRulesService, useValue: admAcmCategorizationRulesServiceSpy },
        { provide: AppConfigurationService, useValue: AppConfigurationServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationRulesRerunClearanceDialogComponent);
    airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineOptions));
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should set item and call rerun method from admAcmCategorizationRulesService when requestRerun method is called', fakeAsync(() => {
    const issueDateControl = FormUtil.get<AdmAcmRulesRerunClearanceDialogViewModel>(component.form, 'issueDate');
    const bspControlValue = FormUtil.get<AdmAcmRulesRerunClearanceDialogViewModel>(component.form, 'bsp');

    const airlineValue = {
      id: 0,
      name: 'name',
      code: 'code',
      designator: 'designator'
    };

    const bspAllValue = [
      {
        value: { id: 0, isoCountryCode: AllOption.VALUE, name: AllOption.LABEL },
        label: 'dropdown.all'
      }
    ];

    const initialDate = new Date('2023-02-28');
    const finalDate = new Date('2023-05-31');

    issueDateControl.setValue([initialDate, finalDate]);
    component['airlineControl'].setValue(airlineValue);
    bspControlValue.setValue(bspAllValue);

    component['requestRerun']();
    tick();

    expect(admAcmCategorizationRulesServiceSpy.rerun).toHaveBeenCalled();
  }));

  describe('initializeButtons', () => {
    let reRunButton;
    let clearanceButton;

    beforeEach(() => {
      reRunButton = { type: FooterButton.Rerun, isDisabled: false };
      clearanceButton = { type: FooterButton.Clearance, isDisabled: false };

      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [reRunButton, clearanceButton]
        }
      };
    });

    it('should set is disabled to reRunButton', () => {
      component['initializeButtons']();

      expect(reRunButton.isDisabled).toBeTruthy();
    });

    it('should set is disabled to clearanceButton', () => {
      component['initializeButtons']();

      expect(clearanceButton.isDisabled).toBeTruthy();
    });

    it('should not set is disabled to clearanceButton', () => {
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [reRunButton]
        }
      };

      component['initializeButtons']();

      expect(clearanceButton.isDisabled).toBeFalsy();
    });
  });

  describe('reRunButtonListener', () => {
    beforeEach(() => {
      component['requestRerun'] = jasmine.createSpy().and.returnValue(of({}));
      (component as any).reRunButtonClick$ = new Subject();
    });

    it('should call close dialog', fakeAsync(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['reRunButtonListener']();
      (component as any).reRunButtonClick$.next();
      tick();

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  describe('clearanceButtonlistener', () => {
    beforeEach(() => {
      component['requestRerun'] = jasmine.createSpy().and.returnValue(of({}));
      component['clearanceButtonClick$'] = new Subject();
    });

    it('should call close dialog', fakeAsync(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['clearanceButtonlistener']();
      (component as any).clearanceButtonClick$.next();
      tick();

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  it('should get Iata Options', fakeAsync(() => {
    const setDropdownsWhenNoAllAirlinesSpy = spyOn<any>(component, 'setDropdownsWhenNoAllAirlines');

    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue(of(userBspOptions));

    const servicesCombined$ = combineLatest([
      component['bspsDictionaryService'].getAllBspDropdownOptions,
      component['airlineDictionaryService'].getDropdownOptions
    ]);

    component['initializeDropdownsOptions']();

    tick();
    fixture.detectChanges();
    servicesCombined$.subscribe();
    expect(setDropdownsWhenNoAllAirlinesSpy).toHaveBeenCalled();
  }));
});
