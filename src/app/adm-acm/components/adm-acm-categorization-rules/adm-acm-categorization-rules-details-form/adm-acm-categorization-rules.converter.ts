import {
  AcmAdmRuleDetail,
  AcmAdmRuleDetailBE,
  AllOption,
  AnyOption,
  PrimaryReasonRule,
  SubReasonRule
} from '~app/adm-acm/models/adm-acm-rule.model';
import { DropdownOption } from '~app/shared/models';

export const acmAdmRuleConverter = (data: AcmAdmRuleDetail): AcmAdmRuleDetail => ({
  ...data,
  concernsIndicator: data.concernsIndicator ? data.concernsIndicator : AnyOption.VALUE
});

export const acmAdmRuleConverterToBE = (data: AcmAdmRuleDetail): Partial<AcmAdmRuleDetailBE> => ({
  ...data,
  airlineCode: data.airlineCode?.code === AllOption.VALUE ? null : data.airlineCode?.code,
  isoCountryCodes: data.isoCountryCodes?.some(bsp => bsp.isoCountryCode === AllOption.VALUE)
    ? null
    : data.isoCountryCodes?.map(bsp => bsp.isoCountryCode),
  concernsIndicator: data.concernsIndicator === AnyOption.VALUE ? null : data.concernsIndicator
});

export function toValueLabelObjectPrimaryReason(value: PrimaryReasonRule[]): DropdownOption<PrimaryReasonRule>[] {
  return value.map(item => ({ value: item, label: item.primaryReason }));
}

export function toValueLabelPrimaryReasonId(value: PrimaryReasonRule[]): DropdownOption<any>[] {
  return value.map(item => ({ value: item.id, label: item.primaryReason }));
}

export function toValueLabelObjectSubReason(value: SubReasonRule[]): DropdownOption<SubReasonRule>[] {
  return value.map(item => ({ value: item, label: item.subReason }));
}

export function toValueLabelSubReasonId(value: SubReasonRule[]): DropdownOption<any>[] {
  return value.map(item => ({ value: item.id, label: item.subReason }));
}
