import { AfterViewInit, Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { isEqual, isNil, omit, omitBy } from 'lodash';
import { combineLatest, Observable, Subject } from 'rxjs';
import { first, map, take, takeUntil } from 'rxjs/operators';
import {
  AcmAdmRuleChanges,
  AcmAdmRuleDetail,
  AllOption,
  AnyOption,
  PositionRuleType,
  RmicType,
  StatusRuleType,
  TransactionRuleType
} from '~app/adm-acm/models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { acdmCategorizationRules } from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuTabActions } from '~app/core/actions';
import { getTabById } from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { Permissions, ROUTES } from '~app/shared/constants';
import { ServerError } from '~app/shared/errors';
import { FormUtil, ObjectHelper, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AirlineSummary, DropdownOption, ResponseErrorBE } from '~app/shared/models';
import { User, UserType } from '~app/shared/models/user.model';
import { NotificationService, TabService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';
import { AdmAcmCategorizationRulesApplyChangesDialogComponent } from '../../dialogs/adm-acm-categorization-rules-apply-changes-dialog/adm-acm-categorization-rules-apply-changes-dialog.component';
import { CATEGORIZATION_RULES_CONSTANTS } from '../adm-acm-categorization-rules-list/adm-acm-categorization-rules.constant';

@Component({
  selector: 'bspl-adm-acm-categorization-rules-details-form',
  templateUrl: 'adm-acm-categorization-rules-details-form.component.html',
  styleUrls: ['adm-acm-categorization-rules-details-form.component.scss']
})
export class AdmAcmCategorizationRulesDetailsFormComponent implements OnInit, AfterViewInit, OnDestroy {
  public isLoading = true;
  public title = 'MENU.ACDMS.QUERY.acdmRuleDetail';
  public subTitle = 'ADM_ACM.categorizationRuleDetail.subTitle';
  public characteristicsTitle = 'ADM_ACM.categorizationRuleDetail.characteristicsLabel';
  public conditionsTitle = 'ADM_ACM.categorizationRuleDetail.conditionsLabel';
  public categoryTitle = 'ADM_ACM.categorizationRuleDetail.categoryLabel';
  public statusLabel = 'ADM_ACM.categorizationRules.filters.status.options.ACTIVE';

  public airlineOptions: DropdownOption[] = [];
  public bspOptions: DropdownOption[] = [];
  public transactionOptions: DropdownOption[] = [];
  public reasonForMemoFieldOptions: DropdownOption[] = [];
  public positionOptions: DropdownOption[] = [];
  public primaryReasonOptions: DropdownOption[] = [];
  public subReasonOptions: DropdownOption[] = [];
  public buttonDesign = ButtonDesign;
  public form: FormGroup;

  public allBspFilteringOption = {
    value: CATEGORIZATION_RULES_CONSTANTS.ALL_OPTIONS_BSP_VALUE,
    label: this.getAllOptionTranslation()
  };

  public statusCheckBox = true;
  public isEditMode = false;
  public isBspOptionsLocked = false;
  public isApplyChangesDisabled = true;
  public isAirlineLocked = true;
  private isIataUser = false;

  private item: Partial<AcmAdmRuleDetail>;
  private changesToApply: Partial<AcmAdmRuleChanges>;

  private formFactory: FormUtil;

  private hasLeanPermission: boolean;

  private dialogQueryFormSubmitEmitter = new Subject();
  private destroy$ = new Subject();

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private get primaryReason(): FormControl {
    return FormUtil.get<AcmAdmRuleDetail>(this.form, 'primaryReasonId') as FormControl;
  }

  private get rmicControl(): FormControl {
    return FormUtil.get<AcmAdmRuleDetail>(this.form, 'reasonForMemoField') as FormControl;
  }

  private get patternControl(): FormControl {
    return FormUtil.get<AcmAdmRuleDetail>(this.form, 'pattern') as FormControl;
  }

  private get subReason(): FormControl {
    return FormUtil.get<AcmAdmRuleDetail>(this.form, 'subReasonId') as FormControl;
  }

  private get allOptionAirline() {
    return {
      value: CATEGORIZATION_RULES_CONSTANTS.ALL_OPTIONS_AIRLINE_VALUE,
      label: this.getAllOptionTranslation()
    };
  }

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translationService: L10nTranslationService,
    private admAcmCategorizationRulesService: AdmAcmCategorizationRulesService,
    private notificationService: NotificationService,
    private tabService: TabService,
    private permissionsService: PermissionsService,
    private bspsDictionaryService: BspsDictionaryService,
    private globalAirlineDictionaryService: GlobalAirlineDictionaryService,
    private globalErrorHandlerService: ErrorHandler,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.form = this.buildForm();
    this.initializePermissions();
    this.initializeDropDowns();
    this.initializePrimaryReasonListener();
    this.initializeRmicListener();
    this.closeCurrentTabOrShowErrors();
  }

  public applyChanges(): void {
    this.getReasonSubReasonChangesLabels();
    const dialogConfig: DialogConfig = {
      data: {
        title: this.translationService.translate('ADM_ACM.categorizationRules.dialog.dialogTitle'),
        footerButtonsType: { type: FooterButton.Apply },
        actionEmitter: this.dialogQueryFormSubmitEmitter
      },
      ruleId: this.item.id,
      changes: this.changesToApply,
      itemToSend: this.formatItemToSend()
    };

    this.dialogService.open(AdmAcmCategorizationRulesApplyChangesDialogComponent, dialogConfig);
  }

  public addItem(): void {
    if (this.form.invalid) {
      FormUtil.showControlState(this.form);
    } else {
      this.admAcmCategorizationRulesService.createRule(this.form.value).subscribe(
        () => {
          this.notificationService.showSuccess(
            this.translationService.translate('ADM_ACM.categorizationRuleDetail.successMessage')
          );
          this.tabService.closeCurrentTab();
        },
        error => {
          this.handleErrorsOnSubmit(error);
        }
      );
    }
  }

  public ngAfterViewInit(): void {
    this.formStatusChangeListener();
  }

  public discard(): void {
    //to raise the unsaved changes dialog
    const route = this.isEditMode
      ? ROUTES.ACDM_CATEGORIZATION_RULES_EDIT.tabLabel
      : ROUTES.ACDM_CATEGORIZATION_RULES_CREATE.tabLabel;
    this.store
      .pipe(select(getTabById, { id: route }), take(1))
      .subscribe(tab => this.store.dispatch(new MenuTabActions.Close(tab, true)));
  }

  public onStatusCheckBoxChange(event: boolean): void {
    const keys = Object.keys(StatusRuleType);
    let result = keys.filter(status => status !== StatusRuleType.ACTIVE.toUpperCase())[0];
    if (event) {
      result = StatusRuleType.ACTIVE;
    }
    this.form.get('status').patchValue(result.toUpperCase());
  }

  public processErrorsOnSubmit(errorResponse: ResponseErrorBE): void {
    errorResponse.messages?.forEach(item => {
      const messageText = item.message;

      if (item.messageParams.length && item.messageParams[0].name === 'fieldName') {
        const fieldPath = item.messageParams[0].value;
        const viewModelControl = FormUtil.get<AcmAdmRuleDetail>(
          this.form,
          fieldPath as keyof AcmAdmRuleDetail
        ) as FormControl;

        if (viewModelControl) {
          FormUtil.showControlState(viewModelControl);
          viewModelControl.markAsDirty();
          viewModelControl.setErrors({
            backendError: { message: messageText }
          });
        }
      }
    });
  }

  private closeCurrentTabOrShowErrors() {
    this.dialogQueryFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(error => {
      if (error) {
        this.handleErrorsOnSubmit(error as ServerError);
      } else {
        this.tabService.closeCurrentTab();
      }
    });
  }

  private initializePrimaryReasonListener() {
    this.primaryReason.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((primaryReasonId: number) => {
      this.subReason.reset();
      this.initializeSubReasonFilterDropdown(primaryReasonId);
    });
  }

  private initializeRmicListener() {
    this.rmicControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.patternControl.reset();

      if (this.isEditMode) {
        const validators =
          this.rmicControl.value === RmicType.RMIN
            ? [Validators.required]
            : [Validators.required, Validators.maxLength(5)];
        this.patternControl.setValidators(validators);
      }
    });
  }

  private formatItemToSend(): any {
    const { airlineCode, isoCountryCodes, concernsIndicator, ...rest } = this.form.value;

    return {
      ...rest,
      airlineCode: airlineCode?.code !== AllOption.VALUE ? airlineCode?.code : null,
      isoCountryCodes:
        isoCountryCodes && isoCountryCodes[0]?.isoCountryCode !== AllOption.VALUE
          ? isoCountryCodes.map(isoCountryCode => isoCountryCode.isoCountryCode)
          : null,
      concernsIndicator: concernsIndicator !== AnyOption.VALUE ? concernsIndicator : null
    };
  }

  private getReasonSubReasonChangesLabels(): void {
    const containReasonChanges = Object.keys(this.changesToApply).some(
      key => key === 'primaryReasonId' || key === 'subReasonId'
    );

    if (containReasonChanges) {
      const primaryReason = this.primaryReasonOptions.find(
        option => option.value === this.changesToApply.primaryReasonId
      );
      const subReason = this.subReasonOptions.find(option => option.value === this.changesToApply.subReasonId);

      const changesReasonsToConfig = omitBy(
        {
          primaryReasonId: primaryReason?.label,
          subReasonId: subReason?.label
        },
        isNil
      );

      this.changesToApply = { ...this.changesToApply, ...changesReasonsToConfig };
    }
  }

  private formStatusChangeListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.isEditMode) {
        const isFormInvalid = status !== 'VALID';
        const itemOmited = omit(this.item, ['id', 'name', 'primaryReason', 'readonly', 'reasonStatus', 'subReason']);

        const hasNoChangesInForm = isEqual(this.form.value, itemOmited);
        this.isApplyChangesDisabled = isFormInvalid || hasNoChangesInForm;
        this.changesToApply = ObjectHelper.getDifferenceBetweenTwoObjects(this.form.value, itemOmited);
      }
    });
  }

  private initializeRouteData(): void {
    const row = this.activatedRoute.snapshot.data.acdmRule;
    this.isEditMode = !!row;

    if (this.isEditMode) {
      this.statusCheckBox = row?.status === StatusRuleType.ACTIVE.toUpperCase();
      this.item = {
        ...row,
        airlineCode: row.airlineCode ? this.getAirlineFromOptions(row.airlineCode) : this.airlineOptions[0].value,
        isoCountryCodes: row.isoCountryCodes
          ? this.getbspFromOptions(row.isoCountryCodes)
          : [this.allBspFilteringOption.value],
        concernsIndicator: row.concernsIndicator ? row.concernsIndicator : AnyOption.VALUE
      };

      this.initializeFormData(this.item);
      this.initializeSubReasonFilterDropdown(this.item.primaryReasonId);
    } else {
      this.isAirlineLocked = !this.isIataUser;
      const value: Partial<AcmAdmRuleDetail> = {
        status: StatusRuleType.ACTIVE.toUpperCase() as StatusRuleType
      };
      this.initializeFormData(value);
    }

    this.isLoading = false;
  }

  private getAirlineFromOptions(code: string): any {
    return this.airlineOptions.find(option => option.value.code === code).value;
  }

  private getbspFromOptions(isoCountryCodes: string[]): any {
    return this.bspOptions
      .filter(element => isoCountryCodes.includes(element.value.isoCountryCode))
      .map(option => option.value);
  }

  private initializeSubReasonFilterDropdown(primaryReasonId: number) {
    if (primaryReasonId) {
      this.admAcmCategorizationRulesService
        .getSubReasonDropDownValueId(primaryReasonId, true)
        .pipe(takeUntil(this.destroy$))
        .subscribe(options => (this.subReasonOptions = options));
    } else {
      this.subReasonOptions = [];
      this.subReason.reset();
    }
  }

  private initializeFormData(value: Partial<AcmAdmRuleDetail>): void {
    this.form.patchValue(value, { emitEvent: false });
  }

  private buildForm() {
    return this.formFactory.createGroup<AcmAdmRuleDetail>({
      airlineCode: [null, Validators.required],
      status: [null, Validators.required],
      isoCountryCodes: [null, Validators.required],
      concernsIndicator: [null, Validators.required],
      reasonForMemoField: [null, Validators.required],
      position: [null, Validators.required],
      primaryReasonId: [null, Validators.required],
      subReasonId: [null, Validators.required],
      pattern: [null, Validators.required]
    });
  }

  private getAirlineOptions(loggedUser: User): void {
    const airlineControl = FormUtil.get<AcmAdmRuleDetail>(this.form, 'airlineCode');

    this.airlineOptions = [loggedUser].map(airline => ({
      value: {
        id: airline.id,
        name: airline.lastName,
        code: airline.iataCode,
        designator: null
      },
      label: `${airline.iataCode} / ${airline.lastName}`
    }));

    airlineControl.patchValue(this.airlineOptions[0].value);
  }

  private getIataOptions(): void {
    combineLatest([
      this.bspsDictionaryService.getAllBspDropdownOptions(),
      this.globalAirlineDictionaryService.getDropdownOptions()
    ])
      .pipe(first())
      .subscribe(options => {
        const mappedAirline: DropdownOption<AirlineSummary>[] = options[1].map(airline => ({
          value: {
            id: airline.value.id,
            name: airline.value.globalName,
            code: airline.value.iataCode,
            designator: null
          },
          label: `${airline.value.iataCode} / ${airline.value.globalName}`
        }));

        this.bspOptions = options[0];
        this.airlineOptions = [this.allOptionAirline, ...mappedAirline];

        this.initializeRouteData();
      });
  }

  private initializeDropDowns(): void {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      if (loggedUser.userType === UserType.IATA) {
        this.isIataUser = true;
        this.getIataOptions();
      } else {
        this.getAirlineOptions(loggedUser);
        this.getAirlineBspOptions(loggedUser);
      }
    });

    this.transactionOptions.push(
      ...Object.keys(TransactionRuleType).map((value: string) => ({
        value,
        label: this.translationService.translate(`ADM_ACM.categorizationRules.filters.transaction.options.${value}`)
      }))
    );

    this.reasonForMemoFieldOptions = Object.keys(RmicType).map(value => ({ value, label: value }));

    this.positionOptions = Object.keys(PositionRuleType).map(value => ({
      value,
      label: this.translationService.translate(`ADM_ACM.categorizationRules.filters.position.options.${value}`)
    }));

    this.admAcmCategorizationRulesService
      .getPrimaryReasonDropDownValueId(true)
      .pipe(takeUntil(this.destroy$))
      .subscribe(options => (this.primaryReasonOptions = options));
  }

  private getAirlineBspOptions(loggedUser: User): void {
    if (this.hasLeanPermission) {
      this.getLeanBspOptions(loggedUser, [acdmCategorizationRules.updateCatRule, Permissions.lean]);
    } else {
      const bspToPatch = loggedUser.bsps.map(bsp => ({
        id: bsp.id,
        isoCountryCode: bsp.isoCountryCode,
        name: bsp.name
      }));

      this.bspOptions = bspToPatch.map(value => ({ label: value.isoCountryCode, value }));

      this.initializeRouteData();

      const bspControl = FormUtil.get<AcmAdmRuleDetail>(this.form, 'isoCountryCodes');
      bspControl.patchValue(bspToPatch);
      this.isBspOptionsLocked = true;
      this.isLoading = false;
    }
  }

  private getLeanBspOptions(loggedUser: User, permissions): void {
    this.bspsDictionaryService
      .getAllBspsByPermissions(loggedUser.bspPermissions, permissions)
      .pipe(
        takeUntil(this.destroy$),
        map(bsps => bsps.map(toValueLabelObjectBsp))
      )
      .subscribe(options => {
        this.bspOptions = options;
        this.initializeRouteData();
      });
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private handleErrorsOnSubmit(error: ServerError): void {
    const errorToManage: ResponseErrorBE = error.error;

    if (errorToManage && errorToManage.errorCode === 400) {
      //* We process errors on Bad Request. Errors that refer to some form param
      this.processErrorsOnSubmit(errorToManage);
      this.notificationService.showError(
        this.translationService.translate('ADM_ACM.categorizationRuleDetail.errorSubmit')
      );

      const errorsWithoutParams = errorToManage.messages.filter(
        errorMessage => errorMessage.messageParams.length === 0
      );

      //* Errors on Bad Request that do not refer to any form field
      if (errorsWithoutParams.length) {
        // shown by toast service
        this.globalErrorHandlerService.handleError(error);
      }
    } else {
      //* Other errors that must be handled generically
      this.globalErrorHandlerService.handleError(error);
    }
  }

  private getAllOptionTranslation() {
    return this.translationService.translate(`dropdown.${AllOption.LABEL}`);
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
