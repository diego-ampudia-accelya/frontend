import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { combineLatest, of } from 'rxjs';

import { AdmAcmCategorizationRulesDetailsFormComponent } from './adm-acm-categorization-rules-details-form.component';
import { AdmAcmCategorizationRulesFormatter } from '~app/adm-acm/services/adm-acm-categorization-rules-formatter.service';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { User, UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, NotificationService, TabService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';
import { DialogService } from '~app/shared/components';
import {
  AcmAdmRuleChanges,
  AcmAdmRuleDetail,
  AdmAcmRule,
  PositionRuleType,
  PrimaryReasonRule,
  RmicType,
  StatusRuleType,
  SubReasonRule,
  TransactionRuleType
} from '~app/adm-acm/models/adm-acm-rule.model';
import { AirlineSummary, DropdownOption, ResponseErrorBE } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { FormUtil } from '~app/shared/helpers';
import { createAirlineUser } from '~app/shared/mocks/airline-user';

describe('AdmAcmCategorizationRulesDetailsFormComponent', () => {
  let component: AdmAcmCategorizationRulesDetailsFormComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationRulesDetailsFormComponent>;

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  const tabServiceSpy = createSpyObject(TabService);
  const queryStorageMock = createSpyObject(DefaultQueryStorage);
  const queryMock = { filterBy: {}, paginateBy: {}, sortBy: [] };
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const appConfigurationServiceSpy = createSpyObject(AppConfigurationService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const globalAirlineDictionaryServiceSpy = createSpyObject(GlobalAirlineDictionaryService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const dialogServiceSpy = createSpyObject(DialogService);

  const expectedUserDetails: User = {
    id: 10126,
    email: 'iata@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.IATA,
    permissions: ['rAdmCatRule'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const airlineUser = createAirlineUser();

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  const airlineCode: AirlineSummary = {
    id: 1,
    name: 'AIRLINE 001',
    code: '001',
    designator: 'L1'
  };

  const isoCountryCodes: BspDto[] = [
    {
      id: 1,
      name: 'bspName',
      isoCountryCode: 'ES'
    }
  ];

  const item: AcmAdmRuleDetail = {
    id: 22,
    airlineCode,
    status: StatusRuleType.ACTIVE,
    isoCountryCodes,
    position: PositionRuleType.STARTS_WITH,
    primaryReasonId: 123,
    subReasonId: 321,
    pattern: 'Start'
  };

  const changesToApply: Partial<AcmAdmRuleChanges> = {
    primaryReasonId: 'primaryReason 001',
    subReasonId: 'subReason 001'
  };

  const admAcmRulesPrimaryReason: PrimaryReasonRule[] = [
    {
      id: 22,
      primaryReason: 'primaryReason 001',
      status: 'active'
    },
    {
      id: 23,
      primaryReason: 'primaryReason 002',
      status: 'acive'
    }
  ];

  const admAcmSubReasonRule: SubReasonRule[] = [
    {
      id: 22,
      subReason: 'subReason 001',
      status: 'active'
    },
    {
      id: 23,
      subReason: 'subReason 002',
      status: 'acive'
    }
  ];

  const optionsPrimaryReason: DropdownOption<PrimaryReasonRule>[] = [
    {
      value: admAcmRulesPrimaryReason[0],
      label: admAcmRulesPrimaryReason[0].primaryReason
    },
    {
      value: admAcmRulesPrimaryReason[1],
      label: admAcmRulesPrimaryReason[1].primaryReason
    }
  ];

  const optionsSubReason: DropdownOption<SubReasonRule>[] = [
    {
      value: admAcmSubReasonRule[0],
      label: admAcmSubReasonRule[0].subReason
    },
    {
      value: admAcmSubReasonRule[1],
      label: admAcmSubReasonRule[1].subReason
    }
  ];

  const validFormValue = {
    airlineCode: {
      id: 400,
      name: 'My airline',
      code: '001',
      designator: 'designator'
    },
    status: StatusRuleType.ACTIVE,
    isoCountryCodes: [
      {
        id: 23,
        isoCountryCode: 'isoCountryCode'
      }
    ],
    concernsIndicator: TransactionRuleType.ISSUE,
    reasonForMemoField: RmicType.RMIC,
    position: PositionRuleType.CONTAINS,
    primaryReasonId: 22,
    subReasonId: 4,
    pattern: 'init'
  };

  const acdmRule: AdmAcmRule = {
    id: 21,
    status: StatusRuleType.ACTIVE,
    airlineCode: '001',
    isoCountryCodes: ['ES'],
    concernsIndicator: TransactionRuleType.ISSUE,
    primaryReasonId: 54,
    pattern: 'REF'
  };

  const mockAirlineOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: {
        id: 1,
        name: 'AIRLINE 001',
        code: '001',
        designator: 'L1'
      },
      label: '001 / AIRLINE 001'
    },
    {
      value: {
        id: 2,
        name: 'AIRLINE 002',
        code: '002',
        designator: 'L2'
      },
      label: '002 / AIRLINE 002'
    }
  ];

  const userBspOptions: DropdownOption<Bsp>[] = [
    {
      value: {
        id: 1,
        name: 'ES',
        isoCountryCode: 'ES'
      },
      label: 'ES'
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationRulesDetailsFormComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: fromAuth.getUser, value: expectedUserDetails }]
        }),
        FormBuilder,
        HttpClient,
        AdmAcmCategorizationRulesService,
        PermissionsService,
        { provide: TabService, useValue: tabServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: AppConfigurationService, useValue: appConfigurationServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        { provide: GlobalAirlineDictionaryService, useValue: globalAirlineDictionaryServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    })
      .overrideComponent(AdmAcmCategorizationRulesDetailsFormComponent, {
        set: {
          providers: [
            AdmAcmCategorizationRulesFormatter,
            AdmAcmCategorizationRulesService,
            PermissionsService,
            { provide: DefaultQueryStorage, useValue: queryStorageMock },
            {
              provide: QueryableDataSource,
              useValue: queryableDataSourceSpy,
              deps: [AdmAcmCategorizationRulesService]
            }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationRulesDetailsFormComponent);
    component = fixture.componentInstance;
    queryStorageMock.get.and.returnValue(queryMock);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call proper methods when applyChanges is called', () => {
    const getReasonSubReasonChangesLabelsSpy = spyOn<any>(component, 'getReasonSubReasonChangesLabels');

    component['item'] = item;
    component['changesToApply'] = changesToApply;
    component.applyChanges();

    expect(getReasonSubReasonChangesLabelsSpy).toHaveBeenCalled();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should show control state when form is invalid', () => {
    spyOn(FormUtil, 'showControlState');

    component.addItem();

    expect(FormUtil.showControlState).toHaveBeenCalledWith(component.form);
  });

  it('should call proper methods when form is valid', () => {
    component.form.setValue(validFormValue);

    spyOn<any>(component['admAcmCategorizationRulesService'], 'createRule').and.returnValue(of(null));

    component.addItem();

    expect(component['admAcmCategorizationRulesService'].createRule).toHaveBeenCalledWith(validFormValue);
  });

  it('should format item to send', () => {
    component.form.setValue(validFormValue);

    const expectedItem = {
      ...validFormValue,
      airlineCode: '001',
      isoCountryCodes: ['isoCountryCode'],
      concernsIndicator: TransactionRuleType.ISSUE
    };

    const formatItemToSendSpy = (component as any).formatItemToSend();

    expect(formatItemToSendSpy).toEqual(expectedItem);
  });

  it('should get reasons and subReasons labels when getReasonSubReasonChangesLabels is called, reasons changed', () => {
    const expectedChangesToApply = {
      primaryReasonId: optionsPrimaryReason[0].label,
      subReasonId: optionsSubReason[0].label
    };

    component.primaryReasonOptions = optionsPrimaryReason;
    component.subReasonOptions = optionsSubReason;
    component['changesToApply'] = changesToApply;
    component['getReasonSubReasonChangesLabels']();

    expect(component['changesToApply']).toEqual(expectedChangesToApply);
  });

  it('should not get reasons and subReasons labels when getReasonSubReasonChangesLabels is called, reasons not changed', () => {
    const otherChangesToApply = {
      pattern: 'RPat'
    };

    component.primaryReasonOptions = optionsPrimaryReason;
    component.subReasonOptions = optionsSubReason;
    component['changesToApply'] = otherChangesToApply;
    component['getReasonSubReasonChangesLabels']();

    expect(component['changesToApply']).toEqual(otherChangesToApply);
  });

  it('should set proper status value when onStatusCheckBoxChange it is called with true param', () => {
    component.onStatusCheckBoxChange(true);
    const formStatus = component.form.get('status').value;

    expect(formStatus).toEqual('ACTIVE');
  });

  it('should set proper status value when onStatusCheckBoxChange it is called with false param', () => {
    component.onStatusCheckBoxChange(false);
    const formStatus = component.form.get('status').value;

    expect(formStatus).toEqual('NON_ACTIVE');
  });

  it('should set form field errors when a server validation error is sent with a field param', () => {
    const primaryReasonIdControl = component.form.get('primaryReasonId');

    const error = {
      errorCode: 400,
      messages: [
        {
          message: 'Wrong primaryReasonId',
          messageParams: [{ name: 'fieldName', value: 'primaryReasonId' }]
        }
      ]
    } as ResponseErrorBE;

    spyOn(FormUtil, 'showControlState');

    component.processErrorsOnSubmit(error);

    expect(FormUtil.showControlState).toHaveBeenCalledWith(primaryReasonIdControl);

    expect(component.form.get('primaryReasonId').hasError('backendError')).toBeTruthy();
  });

  it('should not set form field errors when a server validation error is sent with another param', () => {
    const error = {
      errorCode: 400,
      messages: [
        {
          message: 'Wrong primaryReasonId',
          messageParams: [{ name: 'anotherError', value: 'anotherValue' }]
        }
      ]
    } as ResponseErrorBE;

    spyOn(FormUtil, 'showControlState');

    component.processErrorsOnSubmit(error);

    expect(FormUtil.showControlState).not.toHaveBeenCalled();

    expect(component.form.get('primaryReasonId').hasError('backendError')).toBeFalsy();
  });

  it('should call proper methods when initializeRouteData is called, edit mode', () => {
    component['activatedRoute'].snapshot.data.acdmRule = acdmRule;
    const initializeFormDataSpy = spyOn<any>(component, 'initializeFormData');
    const initializeSubReasonFilterDropdownSpy = spyOn<any>(component, 'initializeSubReasonFilterDropdown');

    const expectedItem: Partial<AcmAdmRuleDetail> = {
      id: 21,
      status: StatusRuleType.ACTIVE,
      airlineCode: {
        id: 1,
        name: 'AIRLINE 001',
        code: '001',
        designator: 'L1'
      },
      isoCountryCodes: [{ id: 1, isoCountryCode: 'ES', name: 'ES' }],
      concernsIndicator: TransactionRuleType.ISSUE,
      primaryReasonId: 54,
      pattern: 'REF'
    };
    component.airlineOptions = mockAirlineOptions;
    component.bspOptions = userBspOptions;

    component['initializeRouteData']();

    expect(component.isEditMode).toBeTruthy();
    expect(component['item']).toEqual(expectedItem);
    expect(initializeFormDataSpy).toHaveBeenCalledWith(component['item']);
    expect(initializeSubReasonFilterDropdownSpy).toHaveBeenCalledWith(component['item'].primaryReasonId);
  });

  it('should call proper methods when initializeRouteData is called, not edit mode', () => {
    component['activatedRoute'].snapshot.data = {};
    const expectedItem = {
      status: StatusRuleType.ACTIVE.toUpperCase()
    };

    const initializeFormDataSpy = spyOn<any>(component, 'initializeFormData');

    component['initializeRouteData']();

    expect(component.isEditMode).toBeFalsy();
    expect(component['item']).toEqual(undefined);
    expect(initializeFormDataSpy).toHaveBeenCalledWith(expectedItem);
  });

  it('should initialize SubReasonFilterDropdown', fakeAsync(() => {
    const getSubReasonDropDownValueIdSpy = spyOn<any>(
      component['admAcmCategorizationRulesService'],
      'getSubReasonDropDownValueId'
    ).and.returnValue(of(optionsSubReason));

    component['initializeSubReasonFilterDropdown'](22);
    tick();
    expect(getSubReasonDropDownValueIdSpy).toHaveBeenCalledWith(22, true);
  }));

  it('should not initialize SubReasonFilterDropdown', fakeAsync(() => {
    const resetSubReasonSpy = spyOn<any>(component['subReason'], 'reset');

    component['initializeSubReasonFilterDropdown'](null);
    tick();
    expect(component.subReasonOptions).toEqual([]);
    expect(resetSubReasonSpy).toHaveBeenCalled();
  }));

  it('should initialize Form Data', () => {
    const patchValueSpy = spyOn<any>(component.form, 'patchValue');

    component['initializeFormData'](item);
    expect(patchValueSpy).toHaveBeenCalled();
  });

  it('should get Airline Options', () => {
    const airlineControlTest = FormUtil.get<AcmAdmRuleDetail>(component.form, 'airlineCode');

    const option = [
      {
        value: {
          id: 10024,
          name: 'Scot',
          code: '623',
          designator: null
        },
        label: '623 / Scot'
      }
    ];

    component['getAirlineOptions'](airlineUser);
    expect(airlineControlTest.value).toEqual(option[0].value);
  });

  it('should get Airline Bsp Options when user has lean Permission', () => {
    component['hasLeanPermission'] = true;
    const getLeanBspOptionsSpy = spyOn<any>(component, 'getLeanBspOptions');

    component['getAirlineBspOptions'](expectedUserDetails);
    expect(getLeanBspOptionsSpy).toHaveBeenCalled();
  });

  it('should get Airline Bsp Options when user has not lean Permission', () => {
    component['hasLeanPermission'] = false;
    const expectedBspOptions = [
      {
        value: {
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN'
        },
        label: 'ES'
      }
    ];
    const initializeRouteDataSpy = spyOn<any>(component, 'initializeRouteData');

    component['getAirlineBspOptions'](expectedUserDetails);
    expect(initializeRouteDataSpy).toHaveBeenCalled();
    expect(component.bspOptions).toEqual(expectedBspOptions);
  });

  it('should get Lean Bsp Options', fakeAsync(() => {
    const initializeRouteDataSpy = spyOn<any>(component, 'initializeRouteData');
    const expectedBspOptions = [
      {
        value: {
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN'
        },
        label: 'ES'
      }
    ];

    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(of(expectedBspOptions));

    component['getLeanBspOptions'](expectedUserDetails, 'erer');

    tick();
    component['bspsDictionaryService']
      .getAllBspsByPermissions([{ bspId: 123, permissions: ['erer'] }], ['erer'])
      .subscribe();

    expect(initializeRouteDataSpy).toHaveBeenCalled();
  }));

  it('should get Iata Options', fakeAsync(() => {
    const initializeRouteDataSpy = spyOn<any>(component, 'initializeRouteData');

    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue(of(userBspOptions));
    globalAirlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineOptions));

    const servicesCombined$ = combineLatest([
      component['bspsDictionaryService'].getAllBspDropdownOptions,
      component['globalAirlineDictionaryService'].getDropdownOptions
    ]);

    component['getIataOptions']();

    tick();
    fixture.detectChanges();
    servicesCombined$.subscribe();
    expect(initializeRouteDataSpy).toHaveBeenCalled();
  }));

  it('should handle Errors On Submit when error code 400', () => {
    (component as any).handleErrorsOnSubmit({ error: { errorCode: 400, messages: [{ messageParams: ['msg'] }] } });

    expect(notificationServiceSpy.showError).toHaveBeenCalled();
  });
});
