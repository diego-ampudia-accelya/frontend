import {
  AcmAdmRuleDetail,
  AllOption,
  AnyOption,
  PositionRuleType,
  PrimaryReasonRule,
  RmicType,
  StatusRuleType,
  SubReasonRule,
  TransactionRuleType
} from '~app/adm-acm/models/adm-acm-rule.model';
import { DropdownOption } from '~app/shared/models';
import {
  acmAdmRuleConverter,
  acmAdmRuleConverterToBE,
  toValueLabelObjectPrimaryReason,
  toValueLabelObjectSubReason,
  toValueLabelPrimaryReasonId,
  toValueLabelSubReasonId
} from './adm-acm-categorization-rules.converter';

describe('toValueLabelRulesObjectPrimaryReason converter', () => {
  const admAcmRulesPrimaryReason: PrimaryReasonRule[] = [
    {
      id: 100,
      primaryReason: 'primaryReason 100',
      status: 'status100'
    },
    {
      id: 101,
      primaryReason: 'primaryReason 101',
      status: 'status101'
    }
  ];

  const expectedOptionsRulesPrimaryReason: DropdownOption<PrimaryReasonRule>[] = [
    {
      value: admAcmRulesPrimaryReason[0],
      label: admAcmRulesPrimaryReason[0].primaryReason
    },
    {
      value: admAcmRulesPrimaryReason[1],
      label: admAcmRulesPrimaryReason[1].primaryReason
    }
  ];

  const expectedOptionsRulesPrimaryReasonId: DropdownOption<any>[] = [
    {
      value: admAcmRulesPrimaryReason[0].id,
      label: admAcmRulesPrimaryReason[0].primaryReason
    },
    {
      value: admAcmRulesPrimaryReason[1].id,
      label: admAcmRulesPrimaryReason[1].primaryReason
    }
  ];

  const admAcmRulesSubReason: SubReasonRule[] = [
    {
      id: 100,
      subReason: 'subReason 100',
      status: 'status100'
    },
    {
      id: 101,
      subReason: 'subReason 101',
      status: 'status101'
    }
  ];

  const expectedOptionsRulesSubReason: DropdownOption<SubReasonRule>[] = [
    {
      value: admAcmRulesSubReason[0],
      label: admAcmRulesSubReason[0].subReason
    },
    {
      value: admAcmRulesSubReason[1],
      label: admAcmRulesSubReason[1].subReason
    }
  ];

  const expectedOptionsRulesSubReasonId: DropdownOption<any>[] = [
    {
      value: admAcmRulesSubReason[0].id,
      label: admAcmRulesSubReason[0].subReason
    },
    {
      value: admAcmRulesSubReason[1].id,
      label: admAcmRulesSubReason[1].subReason
    }
  ];

  const acdmRuleDetail: AcmAdmRuleDetail = {
    airlineCode: {
      id: 9,
      name: 'Airline',
      code: 'code',
      designator: 'designator'
    },
    status: StatusRuleType.ACTIVE,
    isoCountryCodes: [
      {
        id: 1,
        isoCountryCode: AllOption.VALUE,
        name: AllOption.VALUE
      }
    ],
    concernsIndicator: null,
    position: PositionRuleType.STARTS_WITH,
    primaryReasonId: 123,
    subReasonId: 234,
    pattern: 'Begin'
  };

  const acdmRuleDetailExpected: AcmAdmRuleDetail = {
    airlineCode: {
      id: 9,
      name: 'Airline',
      code: 'code',
      designator: 'designator'
    },
    status: StatusRuleType.ACTIVE,
    isoCountryCodes: [
      {
        id: 1,
        isoCountryCode: AllOption.VALUE,
        name: AllOption.VALUE
      }
    ],
    concernsIndicator: AnyOption.VALUE,
    position: PositionRuleType.STARTS_WITH,
    primaryReasonId: 123,
    subReasonId: 234,
    pattern: 'Begin'
  };

  const acdmRuleDetailBEExpected = {
    airlineCode: 'code',
    status: StatusRuleType.ACTIVE,
    isoCountryCodes: null,
    concernsIndicator: null,
    reasonForMemoField: RmicType.RMIC,
    position: PositionRuleType.STARTS_WITH,
    primaryReasonId: 123,
    subReasonId: 234,
    pattern: 'Begin'
  };

  it('should convert admAcm Rule Primary Reason', () => {
    expect(toValueLabelObjectPrimaryReason(admAcmRulesPrimaryReason)).toEqual(expectedOptionsRulesPrimaryReason);
  });

  it('should convert admAcm Rule Primary Reason Id', () => {
    expect(toValueLabelPrimaryReasonId(admAcmRulesPrimaryReason)).toEqual(expectedOptionsRulesPrimaryReasonId);
  });

  it('should convert admAcm Rule Sub Reason', () => {
    expect(toValueLabelObjectSubReason(admAcmRulesSubReason)).toEqual(expectedOptionsRulesSubReason);
  });

  it('should convert admAcm Rule Sub Reason Id', () => {
    expect(toValueLabelSubReasonId(admAcmRulesSubReason)).toEqual(expectedOptionsRulesSubReasonId);
  });

  it('should convert admAcm Rule', () => {
    expect(acmAdmRuleConverter(acdmRuleDetail)).toEqual(acdmRuleDetailExpected);
  });
  it('should convert admAcm Rule when concernsIndicator ', () => {
    const rule = { ...acdmRuleDetail, concernsIndicator: TransactionRuleType.ISSUE };
    const ruleExpected = {
      ...acdmRuleDetailExpected,
      concernsIndicator: TransactionRuleType.ISSUE
    };
    expect(acmAdmRuleConverter(rule)).toEqual(ruleExpected);
  });
  it('should convert admAcm Rule BE', () => {
    const ruleDetail = { ...acdmRuleDetail, reasonForMemoField: RmicType.RMIC };
    expect(acmAdmRuleConverterToBE(ruleDetail)).toEqual(acdmRuleDetailBEExpected);
  });

  it('should convert admAcm Rule BE withot airline code', () => {
    const ruleDetail = { ...acdmRuleDetail, reasonForMemoField: RmicType.RMIC, airlineCode: null };
    expect(acmAdmRuleConverterToBE(ruleDetail)).toEqual({ ...acdmRuleDetailBEExpected, airlineCode: undefined });
  });
});
