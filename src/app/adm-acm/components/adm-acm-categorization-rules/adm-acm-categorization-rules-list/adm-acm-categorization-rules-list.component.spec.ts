import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AdmAcmCategorizationRulesListComponent } from './adm-acm-categorization-rules-list.component';
import { AdmAcmCategorizationRulesFormatter } from '~app/adm-acm/services/adm-acm-categorization-rules-formatter.service';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { User, UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';
import {
  AdmAcmRule,
  AdmAcmRuleFilter,
  ReasonStatusRuleType,
  StatusRuleType,
  TransactionRuleType
} from '~app/adm-acm/models/adm-acm-rule.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { AirlineSummary, DropdownOption, GlobalAirlineSummary, GridColumnMultipleLinkClick } from '~app/shared/models';
import { FormUtil } from '~app/shared/helpers';
import { AppState } from '~app/reducers';
import { Store } from '@ngrx/store';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';

describe('AdmAcmCategorizationRulesListComponent', () => {
  let component: AdmAcmCategorizationRulesListComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationRulesListComponent>;
  let mockStore: MockStore<AppState>;

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  const dialogServiceSpy = createSpyObject(DialogService);
  const queryStorageMock = createSpyObject(DefaultQueryStorage);
  const queryMock = { filterBy: {}, paginateBy: {}, sortBy: [] };
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const AppConfigurationServiceSpy = createSpyObject(AppConfigurationService);
  const routerSpy = createSpyObject(Router);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const globalAirlineDictionaryServiceSpy = createSpyObject(GlobalAirlineDictionaryService);

  const expectedUserDetails: User = {
    id: 1,
    email: 'iata@example.com',
    firstName: 'firstName',
    lastName: 'AIRLINE 001',
    iataCode: '001',
    userType: UserType.IATA,
    permissions: ['rAdmCatRule', 'uAdmCatRule', 'cAdmCatRule'],
    bspPermissions: [{ bspId: 123, permissions: ['rAdmCatRule'] }],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const acdmRule: AdmAcmRule = {
    id: 21,
    airlineCode: '001',
    isoCountryCodes: ['ES'],
    concernsIndicator: TransactionRuleType.ISSUE,
    primaryReasonId: 54,
    pattern: 'REF'
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  const row = {
    status: StatusRuleType.ACTIVE,
    readonly: false
  };

  const action: GridTableAction = {
    actionType: GridTableActionType.View,
    methodName: 'methodName'
  };

  const mockAllOption = {
    value: {
      id: 0,
      name: 'All',
      code: 'ALL',
      designator: 'All'
    },
    label: null
  };

  const expectedBspOptions = [
    {
      value: {
        id: 1,
        isoCountryCode: 'ES',
        name: 'SPAIN'
      },
      label: 'ES'
    }
  ];

  const mockAirline: DropdownOption<AirlineSummary> = {
    value: {
      id: 1,
      name: 'AIRLINE 001',
      code: '001',
      designator: null
    },
    label: '001 / AIRLINE 001'
  };

  const mockDictionaryAirlineOptions: DropdownOption<GlobalAirlineSummary>[] = [
    {
      value: {
        id: 1,
        globalName: 'AIRLINE 001',
        iataCode: '001'
      },
      label: '001 / AIRLINE 001'
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationRulesListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: fromAuth.getUser, value: expectedUserDetails }]
        }),
        FormBuilder,
        HttpClient,
        AdmAcmCategorizationRulesService,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: AppConfigurationService, useValue: AppConfigurationServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        { provide: GlobalAirlineDictionaryService, useValue: globalAirlineDictionaryServiceSpy }
      ]
    })
      .overrideComponent(AdmAcmCategorizationRulesListComponent, {
        set: {
          providers: [
            AdmAcmCategorizationRulesFormatter,
            AdmAcmCategorizationRulesService,
            PermissionsService,
            { provide: DefaultQueryStorage, useValue: queryStorageMock },
            {
              provide: QueryableDataSource,
              useValue: queryableDataSourceSpy,
              deps: [AdmAcmCategorizationRulesService]
            }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationRulesListComponent);
    component = fixture.componentInstance;
    queryStorageMock.get.and.returnValue(queryMock);

    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue(of(expectedBspOptions));
    globalAirlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockDictionaryAirlineOptions));
    mockStore = TestBed.inject<any>(Store);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');

    component.ngOnInit();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should navigate correctly on Create Rule', () => {
    component.onCreateRule();

    expect(routerSpy.navigate).toHaveBeenCalledWith(['/acdms/categorization-rules/rule-detail']);
  });

  it('should initialize filter dropdown when button filter is clicked', () => {
    const initializeFiltersDropdownSpy = spyOn<any>(component, 'initializeFilterDropdowns');

    component.onFilterButtonClicked(true);
    expect(initializeFiltersDropdownSpy).toHaveBeenCalled();
  });

  it('should not initialize filter dropdown when button filter is not clicked', () => {
    const initializeFiltersDropdownSpy = spyOn<any>(component, 'initializeFilterDropdowns');

    component.onFilterButtonClicked(false);
    expect(initializeFiltersDropdownSpy).not.toHaveBeenCalled();
  });

  it('should set proper table actions when onActionMenuClick', () => {
    const permissionsServiceSpy = spyOn<any>(component['permissionsService'], 'hasPermission').and.returnValue(true);

    const expectedTableActions = [
      { action: GridTableActionType.View },
      { action: GridTableActionType.Edit, disabled: false },
      { action: GridTableActionType.RerunADM, disabled: true },
      { action: GridTableActionType.ClearanceADM, disabled: false }
    ];

    component.onActionMenuClick(row);
    expect(permissionsServiceSpy).toHaveBeenCalled();
    expect(component.tableActions).toEqual(expectedTableActions);
  });

  it('should call proper method when onActionClick with view action', () => {
    const navigateToRuleViewSpy = spyOn<any>(component, 'navigateToRuleView');

    component.onActionClick({ action, row });

    expect(navigateToRuleViewSpy).toHaveBeenCalled();
  });

  it('should call proper method when onActionClick with edit action', () => {
    const navigateToRuleEditSpy = spyOn<any>(component, 'navigateToRuleEdit');

    component.onActionClick({ action: { ...action, actionType: GridTableActionType.Edit }, row });

    expect(navigateToRuleEditSpy).toHaveBeenCalled();
  });

  it('should call proper method when onActionClick with RerunADM action', () => {
    const openRerunClearanceDialogSpy = spyOn<any>(component, 'openRerunClearanceDialog');

    component.onActionClick({ action: { ...action, actionType: GridTableActionType.RerunADM }, row });

    expect(openRerunClearanceDialogSpy).toHaveBeenCalled();
  });

  it('should call proper method when onActionClick with ClearanceADM action', () => {
    const openRerunClearanceDialogSpy = spyOn<any>(component, 'openRerunClearanceDialog');

    component.onActionClick({ action: { ...action, actionType: GridTableActionType.ClearanceADM }, row });

    expect(openRerunClearanceDialogSpy).toHaveBeenCalled();
  });

  it('should call navigateToRuleView when onRowLinkClick', () => {
    const event: GridColumnMultipleLinkClick<AdmAcmRule> = {
      row,
      propertyName: 'name',
      propertyValue: row
    };
    const navigateToRuleViewSpy = spyOn<any>(component, 'navigateToRuleView');

    component.onRowLinkClick(event);

    expect(navigateToRuleViewSpy).toHaveBeenCalled();
  });

  it('should not call navigateToRuleView when onRowLinkClick without property name', () => {
    const event: GridColumnMultipleLinkClick<AdmAcmRule> = {
      row,
      propertyName: 'other',
      propertyValue: row
    };
    const navigateToRuleViewSpy = spyOn<any>(component, 'navigateToRuleView');

    component.onRowLinkClick(event);

    expect(navigateToRuleViewSpy).not.toHaveBeenCalled();
  });

  it('should open Rerun-Clearance Dialog', () => {
    const config: DialogConfig = {
      data: null
    };

    component['openRerunClearanceDialog'](config);

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should initialize SubReason Filter Dropdown when primaryReasonId', () => {
    const subReasonControlEnableSpy = spyOn<any>(component['subReasonFilter'], 'enable');

    component['initializeSubReasonFilterDropdown'](754);

    expect(subReasonControlEnableSpy).toHaveBeenCalled();
  });

  it('should initialize SubReason Filter Dropdown when primaryReasonId', () => {
    const subReasonControlDisableSpy = spyOn<any>(component['subReasonFilter'], 'disable');

    component['initializeSubReasonFilterDropdown']();

    expect(subReasonControlDisableSpy).toHaveBeenCalled();
  });

  it('should navigate correctly on navigateToRuleEdit', () => {
    component['navigateToRuleEdit'](row);

    expect(routerSpy.navigate).toHaveBeenCalled();
  });

  it('should navigate correctly on navigateToRuleView', () => {
    component['navigateToRuleView'](row);

    expect(routerSpy.navigate).toHaveBeenCalled();
  });

  it('should get Bsp Options', () => {
    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(of(expectedBspOptions));

    component['getBspOptions'](expectedUserDetails, 'rAdmCatRule');

    expect(bspsDictionaryServiceSpy.getAllBspsByPermissions).toHaveBeenCalled();
  });

  it('should get Iata Bsp Options', () => {
    component['getIataBspOptions']();

    expect(bspsDictionaryServiceSpy.getAllBspDropdownOptions).toHaveBeenCalled();
  });

  it('should get Iata Airline Options', () => {
    const expectedAirlineOptions: DropdownOption<AirlineSummary>[] = [mockAllOption, mockAirline];

    globalAirlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockDictionaryAirlineOptions));
    component.airlineOptions = [];

    component['getIataAirlineOptions']();

    expect(component.airlineOptions).toEqual(expectedAirlineOptions);
  });

  it('should get Airline Options', () => {
    const expectedAirlineOptions: DropdownOption<AirlineSummary>[] = [mockAllOption, mockAirline];

    component.airlineOptions = [];

    component['getAirlineOptions'](expectedUserDetails);

    expect(component.airlineOptions).toEqual(expectedAirlineOptions);
  });

  it('should get Airline Bsp Options with lean permission', () => {
    const getBspOptionsSpy = spyOn<any>(component, 'getBspOptions');
    component['hasLeanPermission'] = true;

    component['getAirlineBspOptions'](expectedUserDetails);

    expect(getBspOptionsSpy).toHaveBeenCalledWith(expectedUserDetails, ['rAdmCatRule', 'rLean']);
  });

  it('should get Airline Bsp Options without lean permission', () => {
    const getBspOptionsSpy = spyOn<any>(component, 'getBspOptions');
    component['hasLeanPermission'] = false;

    component['getAirlineBspOptions'](expectedUserDetails);

    expect(getBspOptionsSpy).toHaveBeenCalledWith(expectedUserDetails, ['rAdmCatRule']);
  });

  it('should initialize Filter Dropdowns for Iata User', fakeAsync(() => {
    const getIataAirlineOptionsSpy = spyOn<any>(component, 'getIataAirlineOptions');
    const initializeSubReasonFilterDropdownSpy = spyOn<any>(component, 'initializeSubReasonFilterDropdown');

    const primaryReasonControl = FormUtil.get<AdmAcmRuleFilter>(component.searchForm, 'primaryReason');

    primaryReasonControl.patchValue('primaryReason');

    component['initializeFilterDropdowns']();
    tick();

    expect(getIataAirlineOptionsSpy).toHaveBeenCalled();
    expect(initializeSubReasonFilterDropdownSpy).toHaveBeenCalled();
  }));

  it('should initialize Filter Dropdowns for Airline User', fakeAsync(() => {
    mockStore.overrideSelector(fromAuth.getUser, createAirlineUser());
    mockStore.refreshState();

    const getAirlineBspOptionsSpy = spyOn<any>(component, 'getAirlineBspOptions');

    component['initializeFilterDropdowns']();

    tick();

    expect(getAirlineBspOptionsSpy).toHaveBeenCalled();
  }));

  it('should get Tooltip Status empty', () => {
    const toolTipStatus = component['getTooltipStatus'](acdmRule);

    expect(toolTipStatus).toEqual('');
  });

  it('should get Tooltip Status label', () => {
    const admRuleReasonStatus = {
      ...acdmRule,
      status: StatusRuleType.ACTIVE.toUpperCase(),
      reasonStatus: ReasonStatusRuleType.DISABLED.toUpperCase()
    };

    const translateSpy = spyOn<any>(component, 'translate');

    component['getTooltipStatus'](admRuleReasonStatus as AdmAcmRule);

    expect(translateSpy).toHaveBeenCalled();
  });

  it('should get BadgeInfoType success', () => {
    const admRuleBadgeInfo = {
      ...acdmRule,
      status: StatusRuleType.ACTIVE.toUpperCase(),
      reasonStatus: ReasonStatusRuleType.ENABLED.toUpperCase()
    };

    const badgeInfoType = component['getBadgeInfoType'](admRuleBadgeInfo as AdmAcmRule);

    expect(badgeInfoType).toEqual(BadgeInfoType.success);
  });

  it('should get BadgeInfoType regular', () => {
    const admRuleBadgeInfo = {
      ...acdmRule,
      status: StatusRuleType.ACTIVE.toUpperCase(),
      reasonStatus: ReasonStatusRuleType.DISABLED.toUpperCase()
    };

    const badgeInfoType = component['getBadgeInfoType'](admRuleBadgeInfo as AdmAcmRule);

    expect(badgeInfoType).toEqual(BadgeInfoType.regular);
  });

  it('should translate', () => {
    component['translate']('key');

    expect(translationServiceSpy.translate).toHaveBeenCalledWith('ADM_ACM.categorizationRules.key');
  });
});
