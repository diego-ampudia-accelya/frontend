import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, of, Subject } from 'rxjs';
import { first, map, takeUntil } from 'rxjs/operators';

import { AdmAcmCategorizationRulesRerunClearanceDialogComponent } from '../adm-acm-categorization-rules-rerun-clearance-dialog/adm-acm-categorization-rules-rerun-clearance-dialog.component';
import { CATEGORIZATION_RULES_CONSTANTS } from './adm-acm-categorization-rules.constant';
import {
  AdmAcmRule,
  AdmAcmRuleFilter,
  AllOption,
  PositionRuleType,
  PrimaryReasonRule,
  ReasonStatusRuleType,
  RmicType,
  StatusRuleType,
  TransactionRuleType
} from '~app/adm-acm/models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesFormatter } from '~app/adm-acm/services/adm-acm-categorization-rules-formatter.service';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { acdmCategorizationRules } from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogConfig, DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { ROUTES } from '~app/shared/constants/routes';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AirlineSummary, DropdownOption, GridColumn, GridColumnMultipleLinkClick } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';

@Component({
  selector: 'bspl-adm-acm-categorization-rules-list',
  templateUrl: 'adm-acm-categorization-rules-list.component.html',
  styleUrls: ['adm-acm-categorization-rules-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    AdmAcmCategorizationRulesService,
    AdmAcmCategorizationRulesFormatter,
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [AdmAcmCategorizationRulesService] }
  ]
})
export class AdmAcmCategorizationRulesListComponent implements OnInit, OnDestroy {
  public title = 'ADM_ACM.categorizationRules.title';
  public columns: GridColumn[] = [];
  public totalItems$ = this.dataSource.appliedQuery$.pipe(map(query => query.paginateBy.totalElements));
  public searchForm: FormGroup;
  public bspOptions$: Observable<DropdownOption<BspDto>[]>;
  public airlineOptions: DropdownOption<AirlineSummary>[] = [];
  public transactionOptions: DropdownOption[] = [];
  public reasonForMemoFieldOptions: DropdownOption[] = [];
  public statusOptions: DropdownOption[] = [];
  public positionOptions: DropdownOption[] = [];
  public primaryReasonOptions$: Observable<DropdownOption[]>;
  public subReasonOptions$: Observable<DropdownOption[]>;
  public loading$: Observable<boolean> = this.dataSource.loading$;

  public tableActions: Array<{ action: GridTableActionType; disabled?: boolean }> = [];
  public buttonDesign = ButtonDesign;

  public allBspFilteringOption = {
    value: CATEGORIZATION_RULES_CONSTANTS.ALL_OPTIONS_BSP_VALUE,
    label: this.getAllOptionTranslation()
  };

  public hasCreatePermission: boolean;
  private hasLeanPermission: boolean;

  private formFactory: FormUtil;

  private get primaryReasonFilter(): FormControl {
    return FormUtil.get<AdmAcmRuleFilter>(this.searchForm, 'primaryReason') as FormControl;
  }

  private get subReasonFilter(): FormControl {
    return FormUtil.get<AdmAcmRuleFilter>(this.searchForm, 'subReason') as FormControl;
  }

  private get allOptionAirline() {
    return {
      value: CATEGORIZATION_RULES_CONSTANTS.ALL_OPTIONS_AIRLINE_VALUE,
      label: this.getAllOptionTranslation()
    };
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public filterFormatter: AdmAcmCategorizationRulesFormatter,
    public dataSource: QueryableDataSource<AdmAcmCategorizationRulesListComponent>,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private queryStorage: DefaultQueryStorage,
    private admAcmCategorizationRulesService: AdmAcmCategorizationRulesService,
    private formBuilder: FormBuilder,
    private bspsDictionaryService: BspsDictionaryService,
    private globalAirlineDictionaryService: GlobalAirlineDictionaryService,
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.columns = this.buildColumns();
    this.searchForm = this.buildSearchForm();
    this.initializePermissions();

    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  public loadData(query?: DataQuery): void {
    query = query || defaultQuery;

    const dataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);

    this.primaryReasonFilter.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((primaryReason: PrimaryReasonRule) => {
        this.subReasonFilter.reset();
        this.initializeSubReasonFilterDropdown(primaryReason?.id);
      });
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.admAcmCategorizationRulesService
    });
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      this.initializeFilterDropdowns();
    }
  }

  public onActionMenuClick(row: AdmAcmRule): void {
    const hasUpdatePermission = this.permissionsService.hasPermission(acdmCategorizationRules.updateCatRule);
    const hasRerunPermission = this.permissionsService.hasPermission(acdmCategorizationRules.createCatRuleRerun);
    const isReadonlyRow = row.readonly;
    const isActiveFile = row.status === StatusRuleType.ACTIVE.toUpperCase();

    this.tableActions = [
      { action: GridTableActionType.View },
      { action: GridTableActionType.Edit, disabled: !hasUpdatePermission || isReadonlyRow },
      { action: GridTableActionType.RerunADM, disabled: !hasRerunPermission || !isActiveFile },
      { action: GridTableActionType.ClearanceADM, disabled: !hasRerunPermission || isActiveFile }
    ];
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: AdmAcmRule }): void {
    switch (action.actionType) {
      case GridTableActionType.View:
        this.navigateToRuleView(row);
        break;
      case GridTableActionType.Edit:
        this.navigateToRuleEdit(row);
        break;
      case GridTableActionType.RerunADM:
        this.openRerunClearanceDialog(this.getConfigRerunDialog(row));
        break;
      case GridTableActionType.ClearanceADM:
        this.openRerunClearanceDialog(this.getConfigClearanceDialog(row));
        break;
    }
  }

  public onRowLinkClick(event: GridColumnMultipleLinkClick<AdmAcmRule>): void {
    // To detect when airline column is clicked
    if (event.propertyName === 'name') {
      this.navigateToRuleView(event.row);
    }
  }

  private openRerunClearanceDialog(config: DialogConfig) {
    this.dialogService.open(AdmAcmCategorizationRulesRerunClearanceDialogComponent, config);
  }

  private getConfigRerunDialog(item: AdmAcmRule): DialogConfig {
    return {
      data: {
        item,
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.rerunTitle'),
        footerButtonsType: FooterButton.Rerun,
        isClearanceMode: false
      }
    };
  }

  private getConfigClearanceDialog(item: AdmAcmRule): DialogConfig {
    return {
      data: {
        item,
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.clearanceTitle'),
        footerButtonsType: FooterButton.Clearance,
        isClearanceMode: true
      }
    };
  }

  private initializeSubReasonFilterDropdown(primaryReasonId?: number) {
    if (primaryReasonId) {
      this.subReasonOptions$ = this.admAcmCategorizationRulesService.getSubReasonDropDownValueObject(primaryReasonId);
      this.subReasonFilter.enable();
    } else {
      this.subReasonOptions$ = of([]);
      this.subReasonFilter.reset();
      this.subReasonFilter.disable();
    }
  }

  private navigateToRuleEdit(rule: AdmAcmRule): void {
    this.router.navigate([ROUTES.ACDM_CATEGORIZATION_RULES_EDIT.url, rule.id], { state: { data: { rule } } });
  }

  private navigateToRuleView(rule: AdmAcmRule): void {
    // We can pass some data across routing and avoid resend a request to retrieve data using history.state
    this.router.navigate([ROUTES.ACDM_CATEGORIZATION_RULES_VIEW.url, rule.id], { state: { data: { rule } } });
  }

  private getBspOptions(loggedUser: User, permissions): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService.getAllBspsByPermissions(loggedUser.bspPermissions, permissions).pipe(
      takeUntil(this.destroy$),
      map(bsps => bsps.map(toValueLabelObjectBsp))
    );
  }

  private getIataBspOptions(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService.getAllBspDropdownOptions().pipe(takeUntil(this.destroy$));
  }

  private getIataAirlineOptions(): void {
    this.globalAirlineDictionaryService
      .getDropdownOptions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(airlines => {
        const mappedAirline: DropdownOption<AirlineSummary>[] = airlines.map(airline => ({
          value: {
            id: airline.value.id,
            name: airline.value.globalName,
            code: airline.value.iataCode,
            designator: null
          },
          label: `${airline.value.iataCode} / ${airline.value.globalName}`
        }));

        this.airlineOptions = [this.allOptionAirline, ...mappedAirline];
      });
  }

  private getAirlineOptions(loggedUser: User) {
    const baseAirlineOptions: DropdownOption<AirlineSummary>[] = [loggedUser].map(airline => ({
      value: {
        id: airline.id,
        name: airline.lastName,
        code: airline.iataCode,
        designator: null
      },
      label: `${airline.iataCode} / ${airline.lastName}`
    }));

    this.airlineOptions = [this.allOptionAirline, ...baseAirlineOptions];
  }

  private getAirlineBspOptions(loggedUser: User) {
    if (this.hasLeanPermission) {
      this.bspOptions$ = this.getBspOptions(loggedUser, [acdmCategorizationRules.readCatRule, Permissions.lean]);
    } else {
      this.bspOptions$ = this.getBspOptions(loggedUser, [acdmCategorizationRules.readCatRule]);
    }
  }

  private initializeFilterDropdowns() {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      if (loggedUser.userType === UserType.IATA) {
        this.bspOptions$ = this.getIataBspOptions();
        this.getIataAirlineOptions();
      } else {
        this.getAirlineOptions(loggedUser);
        this.getAirlineBspOptions(loggedUser);
      }
    });

    this.transactionOptions = [AllOption.VALUE, ...Object.keys(TransactionRuleType)].map((value: string) => ({
      value,
      label: this.translationService.translate(`ADM_ACM.categorizationRules.filters.transaction.options.${value}`)
    }));

    this.reasonForMemoFieldOptions = Object.keys(RmicType).map(value => ({ value, label: value }));

    this.primaryReasonOptions$ = this.admAcmCategorizationRulesService.getPrimaryReasonDropDownValueObject();

    this.statusOptions = Object.keys(StatusRuleType).map((value: string) => ({
      value,
      label: this.translationService.translate(`ADM_ACM.categorizationRules.filters.status.options.${value}`)
    }));

    this.positionOptions = Object.keys(PositionRuleType).map((value: string) => ({
      value,
      label: this.translationService.translate(`ADM_ACM.categorizationRules.filters.position.options.${value}`)
    }));

    if (this.primaryReasonFilter.value) {
      this.initializeSubReasonFilterDropdown(this.primaryReasonFilter.value?.id);
    }
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<AdmAcmRuleFilter>({
      name: [],
      bsps: [],
      airlineCode: [],
      concernsIndicator: [],
      reasonForMemoField: [],
      pattern: [],
      primaryReason: [],
      subReason: [],
      status: [],
      position: []
    });
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'name',
        name: 'ADM_ACM.categorizationRules.columns.name',
        cellTemplate: 'multipleLinkCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'isoCountryCodes',
        name: 'ADM_ACM.categorizationRules.columns.bsp',
        pipe: {
          transform: value => (value ? value.join(' ') : AllOption.LABEL)
        },
        cellClass: this.isDeactivated.bind(this),
        flexGrow: 1
      },
      {
        prop: 'airlineCode',
        name: 'ADM_ACM.categorizationRules.columns.airline',
        pipe: {
          transform: value => (value ? value : AllOption.LABEL)
        },
        cellClass: this.isDeactivated.bind(this),
        flexGrow: 1
      },
      {
        prop: 'concernsIndicator',
        name: 'ADM_ACM.categorizationRules.columns.transaction',
        pipe: {
          transform: value => (value ? this.columnTransform(value, TransactionRuleType) : AllOption.LABEL)
        },
        cellClass: this.isDeactivated.bind(this),
        flexGrow: 1.2
      },
      {
        prop: 'reasonForMemoField',
        name: 'ADM_ACM.categorizationRules.columns.reason',
        cellClass: this.isDeactivated.bind(this),
        flexGrow: 1.2
      },
      {
        prop: 'position',
        name: 'ADM_ACM.categorizationRules.columns.position',
        pipe: {
          transform: value => (value ? this.columnTransform(value, PositionRuleType) : AllOption.LABEL)
        },
        cellClass: this.isDeactivated.bind(this),
        flexGrow: 1
      },
      {
        prop: 'pattern',
        name: 'ADM_ACM.categorizationRules.columns.pattern',
        flexGrow: 3,
        cellClass: this.isDeactivated.bind(this)
      },
      {
        prop: 'primaryReason',
        name: 'ADM_ACM.categorizationRules.columns.primaryReason',
        flexGrow: 1,
        cellClass: this.isDeactivated.bind(this)
      },
      {
        prop: 'subReason',
        name: 'ADM_ACM.categorizationRules.columns.subReason',
        flexGrow: 2,
        cellClass: this.isDeactivated.bind(this),
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        badgeInfo: {
          showIconType: false,
          value: this.columnTransform(ReasonStatusRuleType.DISABLED, ReasonStatusRuleType, true),
          type: () => BadgeInfoType.error,
          hidden: (value: AdmAcmRule) => value.reasonStatus === ReasonStatusRuleType.ENABLED.toUpperCase()
        }
      },
      {
        prop: 'status',
        pipe: {
          transform: value => this.columnTransform(value, StatusRuleType, true)
        },
        name: 'ADM_ACM.categorizationRules.columns.status',
        flexGrow: 1,
        cellTemplate: 'badgeInfoCellTmpl',
        badgeInfo: {
          type: (value: AdmAcmRule) => this.getBadgeInfoType(value),
          tooltipLabel: (value: AdmAcmRule) => this.getTooltipStatus(value),
          hidden: (value: AdmAcmRule) => !value,
          showIconType: false
        }
      }
    ];
  }

  private getTooltipStatus(value: AdmAcmRule) {
    return value.status === StatusRuleType.ACTIVE.toUpperCase() &&
      value.reasonStatus === ReasonStatusRuleType.DISABLED.toUpperCase()
      ? this.translate(`labels.${StatusRuleType.ACTIVE}${ReasonStatusRuleType.DISABLED}`)
      : '';
  }

  private getBadgeInfoType(value: AdmAcmRule): BadgeInfoType {
    return value.status === StatusRuleType.ACTIVE.toUpperCase() &&
      value.reasonStatus === ReasonStatusRuleType.ENABLED.toUpperCase()
      ? BadgeInfoType.success
      : BadgeInfoType.regular;
  }

  private columnTransform(
    value: string,
    enumType:
      | typeof TransactionRuleType
      | typeof PositionRuleType
      | typeof StatusRuleType
      | typeof ReasonStatusRuleType,
    translation: boolean = false
  ) {
    if (Object.keys(enumType).includes(value)) {
      return translation ? this.translate(`labels.${enumType[value]}`) : enumType[value];
    }

    return value;
  }

  public onCreateRule(): void {
    this.router.navigate([ROUTES.ACDM_CATEGORIZATION_RULES_CREATE.url]);
  }

  private translate(key: string) {
    return this.translationService.translate(`ADM_ACM.categorizationRules.${key}`);
  }

  private getAllOptionTranslation() {
    return this.translationService.translate(`dropdown.${AllOption.LABEL}`);
  }

  private isDeactivated({ row }: { row: AdmAcmRule }): { [key: string]: boolean } {
    return {
      'disabled-row': row.reasonStatus === ReasonStatusRuleType.DISABLED.toUpperCase()
    };
  }

  private initializePermissions(): void {
    this.hasCreatePermission = this.permissionsService.hasPermission(acdmCategorizationRules.createCatRule);
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
