import { AllOption } from '~app/adm-acm/models/adm-acm-rule.model';

export const CATEGORIZATION_RULES_CONSTANTS = {
  ALL_OPTIONS_AIRLINE_VALUE: {
    id: 0,
    code: AllOption.VALUE,
    name: AllOption.LABEL,
    designator: AllOption.LABEL
  },
  ALL_OPTIONS_BSP_VALUE: {
    id: 0,
    isoCountryCode: AllOption.VALUE,
    name: AllOption.LABEL
  }
};
