import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';

import { L10N_LOCALE, L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { l10nConfig } from '~app/shared/base/conf/l10n.config';

import { ROUTES } from '~app/shared/constants/routes';

import { AdmAcmCategorizationRulesViewComponent } from './adm-acm-categorization-rules-view.component';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { AdmAcmRule, PositionRuleType, TransactionRuleType } from '~app/adm-acm/models/adm-acm-rule.model';
import { of } from 'rxjs';

describe('AdmAcmCategorizationRulesViewComponent', () => {
  let fixture: ComponentFixture<AdmAcmCategorizationRulesViewComponent>;
  let component: AdmAcmCategorizationRulesViewComponent;

  const admAcmCategorizationRulesServiceSpy = createSpyObject(AdmAcmCategorizationRulesService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const activatedRouteStub = {
    snapshot: {
      data: {
        tab: ROUTES.ACDM_CATEGORIZATION_RULES_VIEW
      }
    }
  };

  const acdmRule: AdmAcmRule = {
    id: 21,
    airlineCode: '001',
    isoCountryCodes: ['ES'],
    concernsIndicator: TransactionRuleType.ISSUE,
    primaryReasonId: 54,
    pattern: 'REF',
    position: PositionRuleType.STARTS_WITH
  };

  beforeEach(waitForAsync(() => {
    window.history.pushState({ data: { rule: acdmRule } }, '', '');

    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationRulesViewComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: AdmAcmCategorizationRulesService, useValue: admAcmCategorizationRulesServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationRulesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize Route Data without history state data', () => {
    window.history.pushState({ data: null }, '', '');
    admAcmCategorizationRulesServiceSpy.getRuleById.and.returnValue(of({}));

    new AdmAcmCategorizationRulesViewComponent(
      TestBed.inject(L10N_LOCALE),
      activatedRouteStub as any,
      admAcmCategorizationRulesServiceSpy,
      translationServiceSpy
    );

    expect(admAcmCategorizationRulesServiceSpy.getRuleById).toHaveBeenCalled();
  });
});
