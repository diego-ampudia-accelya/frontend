import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';

import { AdmAcmRule, AnyOption } from '~app/adm-acm/models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { ButtonDesign } from '~app/shared/components';

@Component({
  selector: 'bspl-adm-acm-categorization-rules-view',
  templateUrl: 'adm-acm-categorization-rules-view.component.html',
  styleUrls: ['adm-acm-categorization-rules-view.component.scss']
})
export class AdmAcmCategorizationRulesViewComponent {
  public title = 'ADM_ACM.categorizationRuleDetail.view.title';
  public subTitle = 'ADM_ACM.categorizationRuleDetail.subTitle';
  public characteristicsTitle = 'ADM_ACM.categorizationRuleDetail.characteristicsLabel';
  public conditionsTitle = 'ADM_ACM.categorizationRuleDetail.conditionsLabel';
  public categoryTitle = 'ADM_ACM.categorizationRuleDetail.categoryLabel';

  public buttonDesign = ButtonDesign;
  public ruleSelected: AdmAcmRule;

  public isLoading = true;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public activatedRoute: ActivatedRoute,
    public admAcmCategorizationRulesService: AdmAcmCategorizationRulesService,
    public translationService: L10nTranslationService
  ) {
    this.initializeRouteData();
  }

  private initializeRouteData() {
    if (history.state.data) {
      this.ruleSelected = this.getFormattedRule(history.state.data.rule);
      this.isLoading = false;
    } else {
      this.admAcmCategorizationRulesService.getRuleById(this.activatedRoute.snapshot.params?.id).subscribe(rule => {
        this.ruleSelected = this.getFormattedRule(rule);
        this.isLoading = false;
      });
    }
  }

  private getFormattedRule(rule: AdmAcmRule) {
    return {
      ...rule,
      position: rule.position
        ? this.translationService.translate(`ADM_ACM.categorizationRules.filters.position.options.${rule.position}`)
        : rule.position,
      concernsIndicator: rule.concernsIndicator
        ? this.translationService.translate(
            `ADM_ACM.categorizationRules.filters.transaction.options.${rule.concernsIndicator}`
          )
        : this.translationService.translate(
            `ADM_ACM.categorizationRules.filters.transaction.options.${AnyOption.VALUE}`
          )
    };
  }
}
