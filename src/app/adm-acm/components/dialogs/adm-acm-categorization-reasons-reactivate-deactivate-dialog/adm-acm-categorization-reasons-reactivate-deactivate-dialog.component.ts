import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';

import { CATEGORIZATION_REASON_DEACTIVATE_DIALOG } from './adm-acm-categorization-reasons-reactivate-deactivate-dialog.constants';
import {
  AdmAcmReasons,
  AdmAcmReasonsDeactivateReactivateDialogViewModel
} from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-adm-acm-categorization-reasons-reactivate-deactivate-dialog',
  templateUrl: './adm-acm-categorization-reasons-reactivate-deactivate-dialog.component.html',
  styleUrls: ['./adm-acm-categorization-reasons-reactivate-deactivate-dialog.component.scss']
})
export class AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent
  implements AfterViewInit, OnInit, OnDestroy
{
  public form: FormGroup;
  public radioButtonOptions: { value: boolean; label: string }[];
  public isDeactivateMode: boolean;

  private deactivationTypeControl: AbstractControl;
  private primaryReasonControl: AbstractControl;
  private subReasonControl: AbstractControl;

  private item: AdmAcmReasons;

  private formFactory: FormUtil;
  private deactivateButton: ModalAction;
  private reactivateButton: ModalAction;

  private actionEmitter = new Subject();
  private destroy$ = new Subject();

  private deactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Deactivate),
    takeUntil(this.destroy$)
  );

  private reactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Reactivate),
    takeUntil(this.destroy$)
  );

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public notificationService: NotificationService,
    public translationService: L10nTranslationService,
    private admAcmCategorizationReasonsService: AdmAcmCategorizationReasonsService,
    private fb: FormBuilder
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.initializeConfigFeatures();
    this.buildForm();
    this.initializeFormFeatures();
    this.initializeRadioButtonValue();

    this.initializeButtons();
    this.initializeButtonsListeners();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.deactivateButton) {
        this.deactivateButton.isDisabled = status !== 'VALID';
      }
    });
  }

  private initializeButtons(): void {
    this.deactivateButton = this.config.data.buttons.find(button => button.type === FooterButton.Deactivate);
    if (this.deactivateButton) {
      this.deactivateButton.isDisabled = true;
    }

    this.reactivateButton = this.config.data.buttons.find(button => button.type === FooterButton.Reactivate);
    if (this.reactivateButton) {
      this.reactivateButton.isDisabled = false;
    }
  }

  private initializeFormFeatures() {
    this.deactivationTypeControl = FormUtil.get<AdmAcmReasonsDeactivateReactivateDialogViewModel>(
      this.form,
      'deactivationType'
    );
    this.primaryReasonControl = FormUtil.get<AdmAcmReasonsDeactivateReactivateDialogViewModel>(
      this.form,
      'primaryReason'
    );
    this.subReasonControl = FormUtil.get<AdmAcmReasonsDeactivateReactivateDialogViewModel>(this.form, 'subReason');

    this.primaryReasonControl.patchValue(this.item.primaryReason);
    this.subReasonControl.patchValue(this.item.subReason);
  }

  private initializeConfigFeatures(): void {
    this.item = this.config.data.item;
    this.isDeactivateMode = this.config.data.isDeactivateMode;
    this.actionEmitter = this.config.data.actionEmitter;
  }

  private initializeRadioButtonValue(): void {
    this.radioButtonOptions = CATEGORIZATION_REASON_DEACTIVATE_DIALOG.RADIO_BUTTON_OPTIONS;
  }

  private initializeButtonsListeners(): void {
    this.deactivateButtonListener();
    this.reactivateButtonlistener();
  }

  private deactivateButtonListener() {
    this.deactivateButtonClick$
      .pipe(
        switchMap(() => this.requestDeactivate()),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.actionEmitter.next();
        this.dialogService.close();
        const translateMessage = 'ADM_ACM.categorizationReasons.dialog.deactivateSuccessMessage';

        this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      });
  }

  private reactivateButtonlistener() {
    this.reactivateButtonClick$
      .pipe(
        switchMap(() => this.requestReactivate()),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.actionEmitter.next();
        this.dialogService.close();
        const translateMessage = 'ADM_ACM.categorizationReasons.dialog.reactivateSuccessMessage';

        this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      });
  }

  private requestDeactivate(): Observable<AdmAcmReasons> {
    const deactivateTypeValue = this.deactivationTypeControl.value;
    const reasonToDeactivate: AdmAcmReasons = {
      active: false
    };

    return !deactivateTypeValue
      ? this.admAcmCategorizationReasonsService.updateSubReason(
          {
            ...reasonToDeactivate,
            primaryReasonId: this.item.primaryReasonId,
            primaryReason: this.item.primaryReason,
            subReason: this.item.subReason
          },
          this.item.id
        )
      : this.admAcmCategorizationReasonsService.rerun(reasonToDeactivate, this.item.id);
  }

  private requestReactivate(): Observable<AdmAcmReasons> {
    const reasonToDeactivate: AdmAcmReasons = {
      primaryReasonId: this.item.primaryReasonId,
      primaryReason: this.item.primaryReason,
      subReason: this.item.subReason,
      active: true
    };

    return this.admAcmCategorizationReasonsService.updateSubReason(reasonToDeactivate, this.item.id);
  }

  private buildForm(): void {
    this.form = this.formFactory.createGroup<AdmAcmReasonsDeactivateReactivateDialogViewModel>({
      deactivationType: [null, Validators.required],
      primaryReason: [],
      subReason: []
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
