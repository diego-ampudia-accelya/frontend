export const CATEGORIZATION_REASON_DEACTIVATE_DIALOG = {
  RADIO_BUTTON_OPTIONS: [
    {
      value: false,
      label: 'Categorization for existing categorized ADMs will be left as it is.'
    },
    {
      value: true,
      label: 'Clear categorization for existing categorized ADMs.'
    }
  ]
};
