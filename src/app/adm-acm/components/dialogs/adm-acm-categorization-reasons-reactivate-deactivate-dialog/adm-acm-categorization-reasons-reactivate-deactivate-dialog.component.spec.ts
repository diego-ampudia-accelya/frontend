import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent } from './adm-acm-categorization-reasons-reactivate-deactivate-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Deactivate,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel'
      },
      {
        title: 'BUTTON.DEFAULT.DEACTIVATE',
        buttonDesign: 'primary',
        type: 'deactivate',
        isDisabled: true
      }
    ],
    item: {
      primaryReason: 'primary Reason',
      primaryReasonId: 'id',
      subReason: 'sub Reason',
      active: true
    }
  }
} as DialogConfig;

describe('AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent', () => {
  let component: AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let admAcmCategorizationReasonsServiceSpy: SpyObject<AdmAcmCategorizationReasonsService>;
  const notificationServiceSpy = createSpyObject(NotificationService);

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    admAcmCategorizationReasonsServiceSpy = createSpyObject(AdmAcmCategorizationReasonsService);

    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        mockProvider(L10nTranslationService),
        FormBuilder,
        ReactiveSubject,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: AdmAcmCategorizationReasonsService, useValue: admAcmCategorizationReasonsServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should call correct functions on Init', () => {
    const initializeConfigFeaturesSpy = spyOn<any>(component, 'initializeConfigFeatures');
    const buildFormSpy = spyOn<any>(component, 'buildForm');
    const initializeFormFeaturesSpy = spyOn<any>(component, 'initializeFormFeatures');
    const initializeRadioButtonValueSpy = spyOn<any>(component, 'initializeRadioButtonValue');
    const initializeButtonsSpy = spyOn<any>(component, 'initializeButtons');
    const initializeButtonsListenersSpy = spyOn<any>(component, 'initializeButtonsListeners');

    component.ngOnInit();

    expect(initializeConfigFeaturesSpy).toHaveBeenCalled();
    expect(buildFormSpy).toHaveBeenCalled();
    expect(initializeFormFeaturesSpy).toHaveBeenCalled();
    expect(initializeRadioButtonValueSpy).toHaveBeenCalled();
    expect(initializeButtonsSpy).toHaveBeenCalled();
    expect(initializeButtonsListenersSpy).toHaveBeenCalled();
  });

  it('should call notificationService when deactivateButtonListener function is called', fakeAsync(() => {
    spyOn<any>(component['deactivateButtonClick$'], 'pipe').and.returnValue(of({}));

    component['actionEmitter'] = new Subject();

    component['deactivateButtonListener']();
    component['deactivateButtonClick$'].pipe(take(1)).subscribe();

    tick();

    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
    expect(dialogServiceSpy.close).toHaveBeenCalled();
  }));

  it('should call notificationService when reactivateButtonlistener function is called', fakeAsync(() => {
    spyOn<any>(component['reactivateButtonClick$'], 'pipe').and.returnValue(of({}));

    component['actionEmitter'] = new Subject();

    component['reactivateButtonlistener']();
    component['reactivateButtonClick$'].pipe(take(1)).subscribe();

    tick();

    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
    expect(dialogServiceSpy.close).toHaveBeenCalled();
  }));

  it('should set deactivateButton isDisabled to true if form is not valid', () => {
    spyOn<any>(component.form.statusChanges, 'pipe').and.returnValue(of(false));

    component.ngAfterViewInit();
    component.form.statusChanges.pipe(take(1)).subscribe();

    expect(component['deactivateButton'].isDisabled).toBe(true);
  });

  it('should set deactivateButton isDisabled to false if form is valid', () => {
    spyOn<any>(component.form.statusChanges, 'pipe').and.returnValue(of('VALID'));

    component.ngAfterViewInit();
    component.form.statusChanges.pipe(take(1)).subscribe();

    expect(component['deactivateButton'].isDisabled).toBe(false);
  });

  it('should set reactivateButton isDisabled when initializeButtons it is called', () => {
    const buttons: ModalAction[] = [
      {
        type: FooterButton.Reactivate,
        isDisabled: true
      },
      {
        type: FooterButton.Deactivate,
        isDisabled: true
      }
    ];

    component.config.data.buttons = buttons;
    component['initializeButtons']();

    expect(component['reactivateButton'].isDisabled).toBe(false);
  });

  it('should call proper service method when requestDeactivate is called and typeControl is false', () => {
    component['deactivationTypeControl'].setValue(false);

    component['requestDeactivate']();

    expect(admAcmCategorizationReasonsServiceSpy.updateSubReason).toHaveBeenCalled();
  });

  it('should call proper service method when requestDeactivate is called and typeControl is true', () => {
    component['deactivationTypeControl'].setValue(true);

    component['requestDeactivate']();

    expect(admAcmCategorizationReasonsServiceSpy.rerun).toHaveBeenCalled();
  });

  it('should call service updateSubReason when requestReactivate is called', () => {
    component['requestReactivate']();

    expect(admAcmCategorizationReasonsServiceSpy.updateSubReason).toHaveBeenCalled();
  });
});
