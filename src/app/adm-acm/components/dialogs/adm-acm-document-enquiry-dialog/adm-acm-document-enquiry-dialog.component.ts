import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { AdmAcmEnquiryDialogData } from '~app/adm-acm/models/adm-acm-enquiry-document.model';
import { documentConfig } from '~app/document/services/document.config';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { AlertMessageType } from '~app/shared/enums';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-adm-acm-document-enquiry-dialog',
  templateUrl: './adm-acm-document-enquiry-dialog.component.html',
  styleUrls: ['./adm-acm-document-enquiry-dialog.component.scss']
})
export class AdmAcmDocumentEnquiryDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public isSearching: boolean;
  public readonly requiredLength = documentConfig.requiredMaxLength;
  public messageKey = {
    noResultsNotification: 'ADM_ACM.documentEnquiry.dialog.noResultsNotifier',
    notReadableNotification: 'ADM_ACM.documentEnquiry.dialog.notReadableNotification',
    tooltip: 'ADM_ACM.documentEnquiry.dialog.tooltipMessage',
    emptyString: ''
  };
  public inlineMessageType = AlertMessageType;
  public form: FormGroup;

  private readonly documentNumberRegex = new RegExp(documentConfig.ticketNumberRegEpx);

  private destroy$ = new Subject();

  constructor(private formBuilder: FormBuilder, private config: DialogConfig, private cd: ChangeDetectorRef) {}

  public get dialogData(): AdmAcmEnquiryDialogData {
    return this.config.data as AdmAcmEnquiryDialogData;
  }

  public ngOnInit(): void {
    this.createForm();
    this.formStatusOnChange().subscribe();
  }

  public ngAfterViewInit(): void {
    this.dialogData.isDocumentFound = true;
    if (this.dialogData.documentNumber) {
      const initialValue = this.dialogData.documentNumber.substring(0, documentConfig.requiredMaxLength);
      this.form.controls.documentNumber.setValue(initialValue);
      this.form.controls.documentNumber.markAsTouched();
    }

    // It's necessary to avoid error ExpressionChangedAfterItHasBeenCheckedError
    this.cd.detectChanges();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public setIsSearching(state: boolean): void {
    this.isSearching = state;
    const searchButton = this.dialogData.buttons.find(button => button.type === FooterButton.Search);
    searchButton.isLoading = state;
  }

  private createForm(): void {
    this.form = this.formBuilder.group(
      {
        documentNumber: new FormControl('', [Validators.pattern(this.documentNumberRegex), Validators.required])
      },
      { updateOn: 'change' }
    );
  }

  private formStatusOnChange(): Observable<any> {
    return this.form.statusChanges.pipe(
      tap(() => {
        const searchButton = this.getFormButton(FooterButton.Search);
        this.dialogData.isDocumentFound = true;
        if (searchButton) {
          searchButton.isDisabled = this.form.invalid;
          searchButton.tooltipText = this.form.invalid ? this.messageKey.tooltip : this.messageKey.emptyString;
        }
      }),
      takeUntil(this.destroy$)
    );
  }

  private getFormButton(name: string): ModalAction {
    return this.dialogData.buttons.find(button => button.type === name);
  }
}
