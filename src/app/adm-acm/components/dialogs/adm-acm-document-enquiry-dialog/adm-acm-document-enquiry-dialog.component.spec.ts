import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { TranslatePipeMock } from '~app/test';
import { AdmAcmDocumentEnquiryDialogComponent } from './adm-acm-document-enquiry-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    isDocumentFound: false,
    documentNumber: '123456789',
    footerButtonsType: FooterButton.Search,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel'
      },
      {
        title: 'BUTTON.DEFAULT.SEARCH',
        buttonDesign: 'primary',
        type: 'search',
        isDisabled: true,
        tooltipText: 'ADM_ACM.documentEnquiry.dialog.tooltipMessage'
      }
    ]
  }
} as DialogConfig;

describe('AdmAcmDocumentEnquiryDialog', () => {
  let component: AdmAcmDocumentEnquiryDialogComponent;
  let fixture: ComponentFixture<AdmAcmDocumentEnquiryDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmDocumentEnquiryDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [FormBuilder, { provide: DialogConfig, useValue: mockConfig }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmDocumentEnquiryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('createForm', () => {
    it('should set form', fakeAsync(() => {
      component.form = null;

      component['createForm']();

      expect(component.form).not.toBeNull('');
    }));
  });

  describe('formStatusOnChange', () => {
    it('should have active action button when document is valid', () => {
      component.form.setValue({ documentNumber: '161' });
      const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);

      expect(searchButton.isDisabled).toBe(false);
      expect(component.dialogData.isDocumentFound).toBe(true);
    });

    it('should have inactive action button when document is invalid', () => {
      component.form.setValue({ documentNumber: 'abc' });
      const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);

      expect(searchButton.isDisabled).toBe(true);
      expect(component.dialogData.isDocumentFound).toBe(true);
    });
  });

  describe('setIsSearching', () => {
    it('should have inactive action button in search mode', () => {
      component.setIsSearching(true);

      const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);
      expect(searchButton.isLoading).toBeTruthy();
      expect(component.isSearching).toBeTruthy();
    });

    it('should have active action button when is not in search mode', () => {
      component.form.setValue({ documentNumber: '314' });
      component.setIsSearching(false);

      const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);
      expect(searchButton.isLoading).toBeFalsy();
      expect(component.isSearching).toBeFalsy();
    });
  });
});
