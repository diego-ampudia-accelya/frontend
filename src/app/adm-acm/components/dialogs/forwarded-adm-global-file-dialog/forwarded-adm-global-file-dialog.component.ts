import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ForwardedAdmGlobalFileFilters } from '~app/adm-acm/models/forwarded-adm-global-file.model';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { GlobalAirlineSummary } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { NotificationService } from '~app/shared/services';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';

@Component({
  selector: 'bspl-forwarded-adm-global-file-dialog',
  templateUrl: './forwarded-adm-global-file-dialog.component.html',
  styleUrls: ['./forwarded-adm-global-file-dialog.component.scss'],
  providers: [AdmAcmService]
})
export class ForwardedAdmGlobalFileDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public airlineDropdownOptions$: Observable<DropdownOption<GlobalAirlineSummary>[]>;

  private formUtil: FormUtil;
  private destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private admAcmService: AdmAcmService,
    private notificationService: NotificationService,
    private globalAirlinesService: GlobalAirlineDictionaryService
  ) {
    this.config.data.title = 'MENU.ACDMS.QUERY.forwardedAdmGlobalFile';
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.getAirlines();
    this.createForm();
    this.onButtonClicked().subscribe();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private onButtonClicked(): Observable<any> {
    return this.reactiveSubject.asObservable.pipe(
      tap(action => {
        if (action.clickedBtn === FooterButton.Accept) {
          const airline = FormUtil.get<ForwardedAdmGlobalFileFilters>(this.form, 'airline').value;
          const date = FormUtil.get<ForwardedAdmGlobalFileFilters>(this.form, 'date').value;

          const filters = {
            airlineCode: airline && airline.iataCode,
            forwardDateFrom: date && toShortIsoDate(date[0]),
            forwardDateTo: date && toShortIsoDate(date[1] ? date[1] : date[0])
          };

          this.admAcmService.downloadForwardedAdmGlobalFile(filters).subscribe(() => {
            const successMessage = this.translationService.translate('TIP.globalFiles.successMessage');
            this.notificationService.showSuccess(successMessage);
          });

          this.dialogService.close();
        }
      })
    );
  }

  private createForm(): void {
    this.form = this.formUtil.createGroup<ForwardedAdmGlobalFileFilters>({
      airline: [null, Validators.required],
      date: []
    });
  }

  private getAirlines(): void {
    this.airlineDropdownOptions$ = this.globalAirlinesService.getDropdownOptions();
  }
}
