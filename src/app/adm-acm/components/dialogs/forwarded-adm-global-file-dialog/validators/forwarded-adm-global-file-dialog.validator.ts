import { FormGroup, ValidatorFn } from '@angular/forms';

export function billingAnalysisDialogValidator(): ValidatorFn {
  return (group: FormGroup): { [key: string]: any } => {
    const agentControl = group.controls['agent'];
    const agentGroupControl = group.controls['agentGroup'];

    if (!agentControl.value && !agentGroupControl.value) {
      agentControl.setErrors({ mustSelectAgentOrGroup: true });
    } else {
      agentControl.setErrors(null);
    }

    return null;
  };
}
