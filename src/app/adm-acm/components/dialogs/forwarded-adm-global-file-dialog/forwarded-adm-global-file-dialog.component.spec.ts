import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockProvider } from 'ng-mocks';
import { of } from 'rxjs';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';
import { ForwardedAdmGlobalFileDialogComponent } from './forwarded-adm-global-file-dialog.component';

describe('ForwardedAdmGlobalFileDialogComponent', () => {
  let component: ForwardedAdmGlobalFileDialogComponent;
  let fixture: ComponentFixture<ForwardedAdmGlobalFileDialogComponent>;

  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const globalAirlineDictionaryService = createSpyObject(GlobalAirlineDictionaryService);
  const admAcmServiceSpy = createSpyObject(AdmAcmService);

  const mockConfig = {
    data: {
      title: 'title',
      footerButtonsType: FooterButton.Accept,
      buttons: [
        {
          title: 'test',
          buttonDesign: ButtonDesign.Primary,
          type: FooterButton.Accept,
          isDisabled: false
        } as ModalAction
      ]
    }
  } as DialogConfig;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ForwardedAdmGlobalFileDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        mockProvider(DialogService),
        FormBuilder,
        ReactiveSubject,
        MockProvider(NotificationService),
        { provide: GlobalAirlineDictionaryService, useValue: globalAirlineDictionaryService },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    })
      .overrideComponent(ForwardedAdmGlobalFileDialogComponent, {
        set: {
          providers: [{ provide: AdmAcmService, useValue: admAcmServiceSpy }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForwardedAdmGlobalFileDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should call downloadForwardedAdmGlobalFile when accept button is clicked', fakeAsync(() => {
    admAcmServiceSpy.downloadForwardedAdmGlobalFile.and.returnValue(of({}));

    component['reactiveSubject'].emit({ clickedBtn: FooterButton.Accept });

    component['onButtonClicked']();

    // Perform tick to resolve the observable subscription
    tick();

    expect(admAcmServiceSpy.downloadForwardedAdmGlobalFile).toHaveBeenCalled();
  }));

  describe('createForm', () => {
    it('should set form', () => {
      component.form = null;

      component['createForm']();

      expect(component.form.value).toEqual({ airline: null, date: null });
    });
  });

  describe('getAirlines', () => {
    beforeEach(() => {
      component['globalAirlinesService'].getDropdownOptions = jasmine
        .createSpy()
        .and.returnValue(of([{ id: 1, iataCode: '11', globalName: 'airline-name' }] as any));
    });

    it('should call getAirlineDropdownOptions', () => {
      component['getAirlines']();

      expect(component['globalAirlinesService'].getDropdownOptions).toHaveBeenCalled();
    });

    it('should set airlineDropdownOptions', fakeAsync(() => {
      component.airlineDropdownOptions$ = of(null);
      let res;

      component['getAirlines']();
      component.airlineDropdownOptions$.subscribe(r => (res = r));
      tick();

      expect(res).toEqual([{ id: 1, iataCode: '11', globalName: 'airline-name' }] as any);
    }));
  });
});
