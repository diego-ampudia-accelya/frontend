import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { createAdmAcmService } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { ResponseErrorBE } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService, TabService } from '~app/shared/services';

@Component({
  selector: 'bspl-adm-acm-internal-comment-dialog',
  templateUrl: './adm-acm-internal-comment-dialog.component.html',
  styleUrls: ['./adm-acm-internal-comment-dialog.component.scss']
})
export class AdmAcmInternalCommentDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public isReadonlyMode: boolean;

  private acdmType: MasterDataType;
  private acdm: AdmAcmIssueBE;

  private acdmService: AdmAcmService;

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private reactiveSubject: ReactiveSubject,
    private http: HttpClient,
    private dialog: DialogService,
    private globalErrorHandlerService: ErrorHandler,
    private tabService: TabService
  ) {
    this.initializeDialogData();

    this.acdmService = createAdmAcmService(this.http, { data: { admAcmType: this.acdmType } } as any);
  }

  public ngOnInit(): void {
    this.buildForm();
    this.initializeButtonsListener();

    if (!this.isReadonlyMode) {
      this.initializeFormListener();
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeDialogData(): void {
    this.acdmType = this.config.data.acdmType;
    this.acdm = this.config.data.acdm;
    this.isReadonlyMode = this.config.data.isReadonly;

    this.config.data.title = this.translationService.translate('ADM_ACM.internalComment.dialog.title');
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      internalComment: [this.acdm.internalComment, [Validators.required, Validators.maxLength(250)]]
    });
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const postButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Post
      );
      postButton.isDisabled = status === 'INVALID';
    });
  }

  private initializeButtonsListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        tap(() => this.validateControls()),
        first(({ clickedBtn }) => clickedBtn !== FooterButton.Post || this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Post:
          case FooterButton.Delete:
            this.onSubmit(clickedBtn);
            break;
          case FooterButton.Cancel:
          case FooterButton.Done:
            this.dialog.close();
            break;
        }
      });
  }

  private validateControls(): void {
    if (this.form.invalid) {
      FormUtil.showControlState(this.form);
    }
  }

  private onSubmit(clickedBtn: FooterButton): void {
    this.setLoading(true);

    if (clickedBtn === FooterButton.Delete) {
      this.acdmService.deleteInternalComment(this.acdm.id).subscribe(
        () => this.onDeleteSuccess(),
        response => this.onDeleteError(response)
      );
    } else if (clickedBtn === FooterButton.Post) {
      if (this.acdm.internalComment) {
        this.acdmService.editInternalComment(this.acdm.id, this.form.value).subscribe(
          () => this.onEditSuccess(),
          response => this.onPostError(response)
        );
      } else {
        this.acdmService.addInternalComment(this.acdm.id, this.form.value).subscribe(
          () => this.onAddSuccess(),
          response => this.onPostError(response)
        );
      }
    }
  }

  private setLoading(value: boolean): void {
    this.config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = value));
  }

  private onDeleteSuccess(): void {
    this.onSuccess('ADM_ACM.internalComment.dialog.successMessage.delete');
  }

  private onDeleteError(response: ResponseErrorBE): void {
    this.globalErrorHandlerService.handleError(response);
    this.dialog.close();
  }

  private onEditSuccess(): void {
    this.onSuccess('ADM_ACM.internalComment.dialog.successMessage.edit');
  }

  private onAddSuccess(): void {
    this.onSuccess('ADM_ACM.internalComment.dialog.successMessage.add');
  }

  private onSuccess(key: string): void {
    const message = this.translationService.translate(key);

    this.notificationService.showSuccess(message);
    this.tabService.refreshCurrentTab();
    this.dialog.close();
  }

  private onPostError(response: ResponseErrorBE): void {
    this.setLoading(false);

    // We subscribe again to dialog action button so errors can be amended and form resent
    this.initializeButtonsListener();

    this.globalErrorHandlerService.handleError(response);
  }
}
