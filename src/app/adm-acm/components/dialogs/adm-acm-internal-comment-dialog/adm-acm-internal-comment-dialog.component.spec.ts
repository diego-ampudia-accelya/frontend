import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService, TabService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { AdmAcmInternalCommentDialogComponent } from './adm-acm-internal-comment-dialog.component';

describe('AdmAcmInternalCommentDialogComponent', () => {
  let component: AdmAcmInternalCommentDialogComponent;
  let fixture: ComponentFixture<AdmAcmInternalCommentDialogComponent>;

  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const errorHandlerServiceSpy = createSpyObject(ErrorHandler);
  const tabServiceSpy = createSpyObject(TabService);
  const dialogServiceSpy = createSpyObject(DialogService);

  const mockConfig = {
    data: {
      title: 'title',
      acdmType: MasterDataType.Acm,
      acdm: { internalComment: 'test-comment', id: 5 },
      isReadonly: false,
      footerButtonsType: FooterButton.Post,
      buttons: [
        {
          title: 'test',
          buttonDesign: ButtonDesign.Primary,
          type: FooterButton.Post,
          isDisabled: false
        } as ModalAction
      ]
    }
  } as DialogConfig;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmInternalCommentDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule],
      providers: [
        FormBuilder,
        { provide: ReactiveSubject, asObservable: of({}) },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: ErrorHandler, useValue: errorHandlerServiceSpy },
        { provide: TabService, useValue: tabServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmInternalCommentDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should NOT call initializeFormListener when it is readonly mode', () => {
    component['initializeFormListener'] = jasmine.createSpy();
    component.isReadonlyMode = true;

    component.ngOnInit();

    expect(component['initializeFormListener']).not.toHaveBeenCalled();
  });

  it('should call initializeFormListener when it is NOT readonly mode', () => {
    component['initializeFormListener'] = jasmine.createSpy();
    component.isReadonlyMode = false;

    component.ngOnInit();

    expect(component['initializeFormListener']).toHaveBeenCalled();
  });

  describe('initializeDialogData', () => {
    beforeEach(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-title');
    });

    it('should set acdmType', () => {
      component['acdmType'] = null;
      component['acdm'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          acdmType: MasterDataType.Acm
        }
      };

      component['initializeDialogData']();

      expect(component['acdmType']).toBe(MasterDataType.Acm);
    });

    it('should set acdm', () => {
      component['acdm'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          acdm: { internalComment: 'test-comment', id: 5 }
        }
      };

      component['initializeDialogData']();

      expect(component['acdm']).toEqual({ internalComment: 'test-comment', id: 5 } as any);
    });

    it('should set isReadonlyMode', () => {
      component['isReadonlyMode'] = null;
      component['acdmType'] = null;
      component['acdm'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          isReadonlyMode: false
        }
      };

      component['initializeDialogData']();

      expect(component['isReadonlyMode']).toBe(false);
    });

    it('should call translate', () => {
      component['initializeDialogData']();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith('ADM_ACM.internalComment.dialog.title');
    });

    it('should set title for config data', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('translated-title');
      component['config'] = cloneDeep(mockConfig);

      expect(component['config'].data.title).not.toBe('translated-title');

      component['initializeDialogData']();

      expect(component['config'].data.title).toBe('translated-title');
    });
  });

  describe('buildForm', () => {
    it('should set form', () => {
      component.form = null;

      component['buildForm']();

      expect(component.form).toBeTruthy();
      expect(component.form.controls.internalComment).toBeTruthy();
    });
  });

  describe('initializeFormListener', () => {
    it('should set isDisabled for Post button as false when form status is valid', fakeAsync(() => {
      const postButton = { type: FooterButton.Post, isDisabled: true };
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [postButton]
        }
      };
      component['buildForm']();
      component.form.controls.internalComment.setValue(null);

      component['initializeFormListener']();
      component.form.controls.internalComment.setValue('test');
      tick();

      expect(postButton.isDisabled).toBe(false);
    }));

    it('should set isDisabled for Post button as true when form status is invalid', fakeAsync(() => {
      const postButton = { type: FooterButton.Post, isDisabled: false };
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [postButton]
        }
      };
      component['buildForm']();
      component.form.controls.internalComment.setValue('test');

      component['initializeFormListener']();
      component.form.controls.internalComment.setValue(null);
      tick();

      expect(postButton.isDisabled).toBe(true);
    }));
  });

  describe('initializeButtonsListener', () => {
    beforeEach(() => {
      (component as any).reactiveSubject.asObservable = new BehaviorSubject({});
      (component as any).validateControls = jasmine.createSpy();
      (component as any).onSubmit = jasmine.createSpy();

      component['buildForm']();
      component.form.setValue({ internalComment: 'test' });
    });

    it('should call validateControls()', fakeAsync(() => {
      (component['reactiveSubject'].asObservable as any).next({ clickedBtn: FooterButton.Post });
      component['initializeButtonsListener']();
      tick();

      expect((component as any).validateControls).toHaveBeenCalled();
    }));

    it('should call onSubmit when clicked button is Post', fakeAsync(() => {
      (component['reactiveSubject'].asObservable as any).next({ clickedBtn: FooterButton.Post });
      component['initializeButtonsListener']();
      tick(2);

      expect((component as any).onSubmit).toHaveBeenCalled();
    }));

    it('should call onSubmit when clicked button is Delete', fakeAsync(() => {
      (component['reactiveSubject'].asObservable as any).next({ clickedBtn: FooterButton.Delete });
      component['initializeButtonsListener']();
      tick(2);

      expect((component as any).onSubmit).toHaveBeenCalled();
    }));

    it('should close dialog when clicked button is Cancel', fakeAsync(() => {
      (component['reactiveSubject'].asObservable as any).next({ clickedBtn: FooterButton.Cancel });
      component['initializeButtonsListener']();
      tick(2);

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should close dialog when clicked button is Done', fakeAsync(() => {
      (component['reactiveSubject'].asObservable as any).next({ clickedBtn: FooterButton.Done });
      component['initializeButtonsListener']();
      tick(2);

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  describe('onSubmit', () => {
    beforeEach(() => {
      component['setLoading'] = jasmine.createSpy();
    });

    it('should call setLoading with true', () => {
      component['onSubmit'](FooterButton.Delete);

      expect(component['setLoading']).toHaveBeenCalledWith(true);
    });

    // Delete
    it('should call deleteInternalComment with acdm id when clicked button is Delete', fakeAsync(() => {
      component['acdmService'].deleteInternalComment = jasmine.createSpy().and.returnValue(of({}));
      component['onDeleteSuccess'] = jasmine.createSpy();

      component['onSubmit'](FooterButton.Delete);
      tick();

      expect(component['acdmService'].deleteInternalComment).toHaveBeenCalledWith(5);
    }));

    it('should call onDeleteSuccess when success delete when clicked button is Delete', fakeAsync(() => {
      component['acdmService'].deleteInternalComment = jasmine.createSpy().and.returnValue(of({}));
      component['onDeleteSuccess'] = jasmine.createSpy();

      component['onSubmit'](FooterButton.Delete);
      tick();

      expect(component['onDeleteSuccess']).toHaveBeenCalled();
    }));

    it('should call onDeleteError when failed delete when clicked button is Delete', fakeAsync(() => {
      component['acdmService'].deleteInternalComment = jasmine.createSpy().and.returnValue(throwError({ code: 500 }));
      component['onDeleteError'] = jasmine.createSpy();

      component['onSubmit'](FooterButton.Delete);
      tick();

      expect(component['onDeleteError']).toHaveBeenCalledWith({ code: 500 });
    }));

    // Post
    describe('Post - internalComment exists', () => {
      beforeEach(() => {
        component['buildForm']();
        component.form.setValue({ internalComment: 'test-comment' });
        component['acdm'].internalComment = 'test-comment';
      });

      it('should call editInternalComment with acdm id when clicked button is Post', fakeAsync(() => {
        component['acdmService'].editInternalComment = jasmine.createSpy().and.returnValue(of({}));
        component['onDeleteSuccess'] = jasmine.createSpy();

        component['onSubmit'](FooterButton.Post);
        tick();

        expect(component['acdmService'].editInternalComment).toHaveBeenCalledWith(5, {
          internalComment: 'test-comment'
        });
      }));

      it('should call onEditSuccess when success delete when clicked button is Post', fakeAsync(() => {
        component['acdmService'].editInternalComment = jasmine.createSpy().and.returnValue(of({}));
        component['onEditSuccess'] = jasmine.createSpy();

        component['onSubmit'](FooterButton.Post);
        tick();

        expect(component['onEditSuccess']).toHaveBeenCalled();
      }));

      it('should call onPostError when failed delete when clicked button is Post', fakeAsync(() => {
        component['acdmService'].editInternalComment = jasmine.createSpy().and.returnValue(throwError({ code: 500 }));
        component['onPostError'] = jasmine.createSpy();

        component['onSubmit'](FooterButton.Post);
        tick();

        expect(component['onPostError']).toHaveBeenCalledWith({ code: 500 });
      }));
    });

    describe('Post - internalComment doesnt exist', () => {
      beforeEach(() => {
        component['buildForm']();
        component.form.setValue({ internalComment: null });
        component['acdm'].internalComment = null;
      });

      it('should call addInternalComment with acdm id when clicked button is Post', fakeAsync(() => {
        component['acdmService'].addInternalComment = jasmine.createSpy().and.returnValue(of({}));
        component['onDeleteSuccess'] = jasmine.createSpy();

        component['onSubmit'](FooterButton.Post);
        tick();

        expect(component['acdmService'].addInternalComment).toHaveBeenCalledWith(5, {
          internalComment: null
        });
      }));

      it('should call onAddSuccess when success delete when clicked button is Post', fakeAsync(() => {
        component['acdmService'].addInternalComment = jasmine.createSpy().and.returnValue(of({}));
        component['onAddSuccess'] = jasmine.createSpy();

        component['onSubmit'](FooterButton.Post);
        tick();

        expect(component['onAddSuccess']).toHaveBeenCalled();
      }));

      it('should call onPostError when failed delete when clicked button is Post', fakeAsync(() => {
        component['acdmService'].addInternalComment = jasmine.createSpy().and.returnValue(throwError({ code: 500 }));
        component['onPostError'] = jasmine.createSpy();

        component['onSubmit'](FooterButton.Post);
        tick();

        expect(component['onPostError']).toHaveBeenCalledWith({ code: 500 });
      }));
    });
  });

  describe('setLoading', () => {
    it('should set isDisabled as true to all buttons', () => {
      const postButton = { type: FooterButton.Post, isDisabled: false };
      const cancelButton = { type: FooterButton.Cancel, isDisabled: false };
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [postButton, cancelButton]
        }
      };

      component['setLoading'](true);

      expect(postButton.isDisabled).toBe(true);
      expect(cancelButton.isDisabled).toBe(true);
    });

    it('should set isDisabled as false to all buttons', () => {
      const postButton = { type: FooterButton.Post, isDisabled: true };
      const cancelButton = { type: FooterButton.Cancel, isDisabled: true };
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [postButton, cancelButton]
        }
      };

      component['setLoading'](false);

      expect(postButton.isDisabled).toBe(false);
      expect(cancelButton.isDisabled).toBe(false);
    });
  });

  describe('onDeleteSuccess', () => {
    it('should call translate', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['onDeleteSuccess']();

      expect(component['translationService'].translate).toHaveBeenCalledWith(
        'ADM_ACM.internalComment.dialog.successMessage.delete'
      );
    });

    it('should call showSuccess with translated message', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['onDeleteSuccess']();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith('test-message');
    });

    it('should call refreshCurrentTab', () => {
      component['onDeleteSuccess']();

      expect(tabServiceSpy.refreshCurrentTab).toHaveBeenCalled();
    });

    it('should call dialog close', () => {
      component['onDeleteSuccess']();

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    });
  });

  describe('onDeleteError', () => {
    it('should call handleError', () => {
      component['onDeleteError']({ errorCode: 500 } as any);

      expect(errorHandlerServiceSpy.handleError).toHaveBeenCalledWith({ errorCode: 500 });
    });

    it('should call dialog close', () => {
      component['onDeleteError']({ errorCode: 500 } as any);

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    });
  });

  describe('onEditSuccess', () => {
    it('should call translate', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['onEditSuccess']();

      expect(component['translationService'].translate).toHaveBeenCalledWith(
        'ADM_ACM.internalComment.dialog.successMessage.edit'
      );
    });

    it('should call showSuccess with translated message', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['onEditSuccess']();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith('test-message');
    });

    it('should call refreshCurrentTab', () => {
      component['onEditSuccess']();

      expect(tabServiceSpy.refreshCurrentTab).toHaveBeenCalled();
    });

    it('should call dialog close', () => {
      component['onEditSuccess']();

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    });
  });

  describe('onAddSuccess', () => {
    it('should call translate', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['onAddSuccess']();

      expect(component['translationService'].translate).toHaveBeenCalledWith(
        'ADM_ACM.internalComment.dialog.successMessage.add'
      );
    });

    it('should call showSuccess with translated message', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['onAddSuccess']();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith('test-message');
    });

    it('should call refreshCurrentTab', () => {
      component['onAddSuccess']();

      expect(tabServiceSpy.refreshCurrentTab).toHaveBeenCalled();
    });

    it('should call dialog close', () => {
      component['onAddSuccess']();

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    });
  });

  describe('onPostError', () => {
    beforeEach(() => {
      component['setLoading'] = jasmine.createSpy();
      component['initializeButtonsListener'] = jasmine.createSpy();
    });

    it('should call setLoading with false', () => {
      component['onPostError']({ errorCode: 400 } as any);

      expect(component['setLoading']).toHaveBeenCalledWith(false);
    });

    it('should call initializeButtonsListener', () => {
      component['onPostError']({ errorCode: 400 } as any);

      expect(component['initializeButtonsListener']).toHaveBeenCalled();
    });

    it('should call handleError', () => {
      component['onPostError']({ errorCode: 400 } as any);

      expect(errorHandlerServiceSpy.handleError).toHaveBeenCalledWith({ errorCode: 400 });
    });
  });
});
