export const CATEGORIZATION_REASON_DIALOG = {
  RADIO_BUTTON_OPTIONS: [
    {
      value: false,
      label: 'Use existing primary reason'
    },
    {
      value: true,
      label: 'Create new primary reason'
    }
  ]
};
