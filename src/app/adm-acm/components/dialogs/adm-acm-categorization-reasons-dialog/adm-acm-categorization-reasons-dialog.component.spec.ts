import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of, Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AdmAcmReasons, AdmAcmReasonsPrimaryReason } from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { toValueLabelObjectPrimaryReason } from '../../adm-acm-categorization-reasons/adm-acm-categorization-reason.converter';
import { AdmAcmCategorizationReasonsDialogComponent } from './adm-acm-categorization-reasons-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Save,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel'
      },
      {
        title: 'BUTTON.DEFAULT.SAVE',
        buttonDesign: 'primary',
        type: 'save'
      }
    ]
  }
} as DialogConfig;

const item: AdmAcmReasons = {
  id: 22,
  primaryReason: 'primaryReason',
  primaryReasonId: '1234',
  subReason: 'subR',
  active: true
};

const primaryReasons: AdmAcmReasonsPrimaryReason[] = [
  {
    id: '001',
    primaryReason: 'primaryReason 001'
  },
  {
    id: '1234',
    primaryReason: 'primaryReason 1234'
  }
];

describe('AdmAcmCategorizationReasonsDialog', () => {
  let component: AdmAcmCategorizationReasonsDialogComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationReasonsDialogComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let admAcmCategorizationReasonsServiceSpy: SpyObject<AdmAcmCategorizationReasonsService>;
  const notificationServiceSpy = createSpyObject(NotificationService);

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    admAcmCategorizationReasonsServiceSpy = createSpyObject(AdmAcmCategorizationReasonsService);

    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationReasonsDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        mockProvider(L10nTranslationService),
        FormBuilder,
        ReactiveSubject,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: AdmAcmCategorizationReasonsService, useValue: admAcmCategorizationReasonsServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationReasonsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize control validators with radioButtonGroup true', () => {
    const newPrimaryReasonControl = component['newPrimaryReasonControl'];
    component['initializeFormListeners']();

    component['reasonTypeControl'].setValue(true);
    newPrimaryReasonControl.setValue('');

    expect(newPrimaryReasonControl.valid).toBeFalsy();
  });

  it('should initialize control validators with radioButtonGroup false', () => {
    const existingPrimaryReasonControl = component['existingPrimaryReasonControl'];
    component['initializeFormListeners']();

    component['reasonTypeControl'].setValue(false);
    existingPrimaryReasonControl.setValue('');

    expect(existingPrimaryReasonControl.valid).toBeFalsy();
  });

  it('should call createNew on requestReasonCreation and createNewReason to true', () => {
    component.createNewReason = true;

    admAcmCategorizationReasonsServiceSpy.createNew.and.returnValue(of(item));

    component['requestReasonCreation']().pipe(take(1)).subscribe();

    fixture.detectChanges();

    expect(admAcmCategorizationReasonsServiceSpy.createNew).toHaveBeenCalled();
  });

  it('should call update on requestReasonUpdate and createNewReason to true', () => {
    component.createNewReason = true;
    component['item'] = item;

    admAcmCategorizationReasonsServiceSpy.update.and.returnValue(of(item));

    component['requestReasonUpdate']().subscribe();

    fixture.detectChanges();

    expect(admAcmCategorizationReasonsServiceSpy.update).toHaveBeenCalled();
  });

  it('should call upateSaveButtonEditMode when is Edit Mode', fakeAsync(() => {
    const subReasonControlControl = component['subReasonControl'];
    const upateSaveButtonEditModeSpy = spyOn<any>(component, 'upateSaveButtonEditMode');

    component['isEditMode'] = true;

    component.ngAfterViewInit();

    tick();

    subReasonControlControl.setValue(null);
    expect(upateSaveButtonEditModeSpy).toHaveBeenCalled();
  }));

  it('should set saveButton disabled when form is not valid', () => {
    component['item'] = item;
    component['existingPrimaryReasonControl'].setValue(primaryReasons[0]);
    component['upateSaveButtonEditMode'](false);

    expect(component['saveButton'].isDisabled).toBeTruthy();
  });

  it('should set properly controls values when initializeExistingPrimaryFeatures is called', fakeAsync(() => {
    const subReasonControlControl = component['subReasonControl'];

    component['item'] = item;

    component['isEditMode'] = true;

    component['primaryReasonOptions$'] = of(primaryReasons).pipe(map(toValueLabelObjectPrimaryReason));

    component['initializeExistingPrimaryFeatures']();

    tick();

    expect(subReasonControlControl.value).toEqual(item.subReason);
    expect(component['existingPrimaryReasonControl'].value).toEqual(primaryReasons[1]);
  }));

  it('should call notificationService when initializeButtonListeners function is called', fakeAsync(() => {
    spyOn<any>(component['saveButtonClick$'], 'pipe').and.returnValue(of({}));

    component['actionEmitter'] = new Subject();
    component['isEditMode'] = true;

    component['initializeButtonListeners']();
    component['saveButtonClick$'].subscribe();

    tick();

    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
  }));
});
