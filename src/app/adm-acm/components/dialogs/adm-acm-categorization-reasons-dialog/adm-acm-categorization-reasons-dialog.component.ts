import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import {
  AdmAcmReasons,
  AdmAcmReasonsCreateDialog,
  AdmAcmReasonsCreateDialogViewModel
} from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { RadioButton } from '~app/shared/models/radio-button.model';
import { NotificationService } from '~app/shared/services/notification.service';
import { CATEGORIZATION_REASON_DIALOG } from './adm-acm-categorization-reasons-dialog.component.constants';

@Component({
  selector: 'bspl-adm-acm-categorization-reasons-dialog',
  templateUrl: './adm-acm-categorization-reasons-dialog.component.html',
  styleUrls: ['./adm-acm-categorization-reasons-dialog.component.scss']
})
export class AdmAcmCategorizationReasonsDialogComponent implements AfterViewInit, OnInit, OnDestroy {
  public form: FormGroup;
  public radioButtonOptions: RadioButton[];
  public primaryReasonOptions$: Observable<DropdownOption[]>;
  public createNewReason = false;

  private reasonTypeControl: AbstractControl;
  private newPrimaryReasonControl: AbstractControl;
  private existingPrimaryReasonControl: AbstractControl;
  private subReasonControl: AbstractControl;

  private item: AdmAcmReasons;

  private isEditMode: boolean;

  private formFactory: FormUtil;
  private saveButton: ModalAction;

  private actionEmitter = new Subject();
  private destroy$ = new Subject();

  private saveButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Save),
    takeUntil(this.destroy$)
  );

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public notificationService: NotificationService,
    public translationService: L10nTranslationService,
    private admAcmCategorizationReasonsService: AdmAcmCategorizationReasonsService,
    private fb: FormBuilder
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.buildForm();
    this.initializeFormFeatures();
    this.initializeConfigFeatures();
    this.initializeRadioButtonValue();
    this.initializeDropdown();

    this.initializeButtons();
    this.initializeButtonListeners();

    this.initializeExistingPrimaryFeatures();

    this.initializeFormListeners();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const isFormValid = status === 'VALID';

      if (!this.isEditMode) {
        this.upateSaveButtonNotEditMode(isFormValid);
      } else {
        this.upateSaveButtonEditMode(isFormValid);
      }
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private upateSaveButtonNotEditMode(isFormValid: boolean): void {
    if (this.saveButton) {
      this.saveButton.isDisabled = !isFormValid;
    }
  }

  private upateSaveButtonEditMode(isFormValid: boolean): void {
    const isSubReasonChanged = this.subReasonControl.value !== this.item.subReason;
    const isPrimaryReasonChanged = this.existingPrimaryReasonControl.value?.primaryReason !== this.item.primaryReason;

    if (this.saveButton) {
      this.saveButton.isDisabled = !isFormValid || (!isSubReasonChanged && !isPrimaryReasonChanged);
    }
  }

  private initializeButtons(): void {
    this.saveButton = this.config.data.buttons.find(button => button.type === FooterButton.Save);
    if (this.saveButton) {
      this.saveButton.isDisabled = true;
    }
  }

  private initializeFormFeatures(): void {
    this.subReasonControl = FormUtil.get<AdmAcmReasonsCreateDialogViewModel>(this.form, 'subReason');
    this.existingPrimaryReasonControl = FormUtil.get<AdmAcmReasonsCreateDialogViewModel>(
      this.form,
      'existingPrimaryReason'
    );
    this.reasonTypeControl = FormUtil.get<AdmAcmReasonsCreateDialogViewModel>(this.form, 'reasonType');
    this.newPrimaryReasonControl = FormUtil.get<AdmAcmReasonsCreateDialogViewModel>(this.form, 'newPrimaryReason');
  }

  private initializeConfigFeatures(): void {
    this.item = this.config.data.item;
    this.isEditMode = !!this.item;
    this.actionEmitter = this.config.data.actionEmitter;
  }

  private initializeDropdown(): void {
    this.primaryReasonOptions$ = this.admAcmCategorizationReasonsService.getPrimaryReasonDropDownValueObject();
  }

  private initializeRadioButtonValue(): void {
    this.radioButtonOptions = CATEGORIZATION_REASON_DIALOG.RADIO_BUTTON_OPTIONS;

    const formControlValue = this.isEditMode ? this.radioButtonOptions[0].value : this.radioButtonOptions[1].value;

    this.reasonTypeControl.patchValue(formControlValue);
    this.createNewReason = formControlValue;
  }

  private initializeExistingPrimaryFeatures(): void {
    if (this.isEditMode) {
      this.existingPrimaryReasonControl.setValidators([Validators.required]);
      this.existingPrimaryReasonControl.updateValueAndValidity();

      this.primaryReasonOptions$.pipe(takeUntil(this.destroy$)).subscribe(primaryReasonOptions => {
        const subReason = this.item.subReason;
        this.subReasonControl.patchValue(subReason);

        const existingReasonToPatch = primaryReasonOptions.find(
          option => option.value.id === this.item.primaryReasonId
        );
        this.existingPrimaryReasonControl.patchValue(existingReasonToPatch.value);
      });
    }
  }

  private initializeFormListeners(): void {
    this.reasonTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(isNewPrimary => {
      this.createNewReason = isNewPrimary;

      if (isNewPrimary) {
        this.newPrimaryReasonControl.setValidators([Validators.required]);
        this.newPrimaryReasonControl.updateValueAndValidity();
        this.existingPrimaryReasonControl.clearValidators();
        this.existingPrimaryReasonControl.reset();
      } else {
        this.initializeExistingPrimaryFeatures();
        this.existingPrimaryReasonControl.setValidators([Validators.required]);
        this.existingPrimaryReasonControl.updateValueAndValidity();
        this.newPrimaryReasonControl.clearValidators();
        this.newPrimaryReasonControl.reset();
      }
    });
  }

  private initializeButtonListeners(): void {
    this.saveButtonClick$
      .pipe(
        switchMap(() => (this.isEditMode ? this.requestReasonUpdate() : this.requestReasonCreation())),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.actionEmitter.next();
        this.dialogService.close();
        const translateMessage = this.isEditMode
          ? 'ADM_ACM.categorizationReasons.dialog.updateMessage'
          : 'ADM_ACM.categorizationReasons.dialog.successMessage';

        this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      });
  }

  private requestReasonCreation(): Observable<AdmAcmReasons> {
    const existingPrimaryReasonValue = this.existingPrimaryReasonControl.value;
    const reasonToCreate: AdmAcmReasonsCreateDialog = {
      subReason: this.subReasonControl.value
    };

    return this.createNewReason
      ? this.admAcmCategorizationReasonsService.createNew({
          ...reasonToCreate,
          primaryReason: this.newPrimaryReasonControl.value
        })
      : this.admAcmCategorizationReasonsService.createFromExisting(reasonToCreate, existingPrimaryReasonValue.id);
  }

  private requestReasonUpdate(): Observable<AdmAcmReasons> {
    const existingPrimaryReasonControlValue = this.existingPrimaryReasonControl.value;
    const primaryReason = this.createNewReason
      ? this.newPrimaryReasonControl.value
      : existingPrimaryReasonControlValue.primaryReason;

    const subReason = this.subReasonControl.value;
    const active = this.item.active;

    const reasonToUpdate = {
      subReason,
      active
    };

    return this.createNewReason
      ? this.admAcmCategorizationReasonsService.update({ ...reasonToUpdate, primaryReason }, this.item.id)
      : this.admAcmCategorizationReasonsService.updateSubReason(
          { ...reasonToUpdate, primaryReasonId: existingPrimaryReasonControlValue?.id },
          this.item.id
        );
  }

  private buildForm(): void {
    this.form = this.formFactory.createGroup<AdmAcmReasonsCreateDialogViewModel>({
      reasonType: [],
      newPrimaryReason: ['', { updateOn: 'change' }],
      existingPrimaryReason: [],
      subReason: ['', { validators: [Validators.required], updateOn: 'change' }]
    });
  }
}
