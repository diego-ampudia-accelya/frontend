import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AcdmReason, GdsForward } from '~app/adm-acm/models/adm-acm-issue-be-aux.model';
import {
  AcdmActionType,
  AcdmNumberingMode,
  AdmAcmStatus,
  getDeleteQueryUrl,
  Reason,
  ReasonAction,
  urlList,
  urlView
} from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  BasicInformationViewModel,
  DetailViewModel,
  DisputeViewModel,
  ForwardGdsViewModel,
  GdsViewModel,
  ReasonDetailViewModel,
  StatusViewModel
} from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import {
  AcdmAlertMessage,
  AdmAcmActionEmitterType,
  AdmAcmIssueBE,
  AdmAcmIssueViewModel
} from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import {
  createAdmAcmService,
  fromGridTableActionToAcdmAction
} from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { Agent } from '~app/master-data/models';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { AlertMessageType } from '~app/shared/enums';
import { ServerError } from '~app/shared/errors';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { DropdownOption, ResponseErrorBE } from '~app/shared/models';
import { appConfiguration, NotificationService, TabService } from '~app/shared/services';
import { blankStringValidator } from '~app/shared/validators/blank-string.validator';

@Component({
  selector: 'bspl-adm-acm-confirmation-dialog',
  templateUrl: './adm-acm-confirmation-dialog.component.html',
  styleUrls: ['./adm-acm-confirmation-dialog.component.scss'],
  providers: [DatePipe]
})
export class AdmAcmConfirmationDialogComponent implements OnInit, OnDestroy {
  @ViewChild(UploadComponent) uploadComponentRef: UploadComponent;

  public admAcmType: MasterDataType;
  public actionType: AcdmActionType;
  public actionTypeEnum = AcdmActionType;
  public isAcdmNumberEditable = false; //* Indicates if user can introduce ACDM number manually

  public acdmNumber: string;
  public issueDate: string;
  public amount: string;
  public currency;
  public regularizedMsg: AcdmAlertMessage;
  public forwardGdsMessage: AcdmAlertMessage = null;

  public reasonRows = 20;
  public reasonLines = 45;

  public isRMICVisible: boolean;

  public uploadMode = UploadModes.Preview;
  public uploadPath: string;

  public dialogForm: FormGroup;

  public gdsOptions: DropdownOption<number>[];
  public reasonOptions: DropdownOption[];

  private isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)
  private keysRoot: string;
  private dialogInjectedData: DialogConfig<any>;

  private issueReasons: AcdmReason[];
  private acdmItem: AdmAcmIssueBE;
  private isUploadingInProgress = false;

  private actionEmitter: Subject<AcdmActionType>;

  private admAcmService: AdmAcmService;
  private formData: AdmAcmDataService;

  private gdsList: GdsViewModel[];
  private gdsSelected: GdsViewModel;

  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private dialog: DialogService,
    private config: DialogConfig,
    private translationService: L10nTranslationService,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private http: HttpClient,
    private reactiveSubject: ReactiveSubject,
    private notificationService: NotificationService,
    private tabService: TabService,
    private globalErrorHandlerService: ErrorHandler
  ) {
    this.initializeDialogData();
    this.admAcmService = createAdmAcmService(this.http, { data: { admAcmType: this.admAcmType } } as any);
  }

  private get isAcdmRequest(): boolean {
    return this.admAcmType === MasterDataType.Admq || this.admAcmType === MasterDataType.Acmq;
  }

  public ngOnInit(): void {
    this.uploadPath = `${appConfiguration.baseUploadPath}/acdm-management/files`;
    this.initializeDialogForm();
    this.initializeIssueClickListener();
    this.initializeRegularizedMessage();
    this.initializeRMICVisibility();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public getTranslationKey(key: string): string {
    return `${this.keysRoot}.${key}`;
  }

  public onUploadIsInProgress(value: boolean): void {
    this.isUploadingInProgress = value;
  }

  private initializeRegularizedMessage(): void {
    const showRegularizedMsg$: Observable<boolean> = this.dialogInjectedData.showRegularizedMsg$ || of(false);

    this.regularizedMsg = {
      hidden: showRegularizedMsg$.pipe(map(show => !show)),
      message: this.getTranslationKey('regularizedMsg'),
      type: AlertMessageType.info
    };
  }

  private initializeDialogData(): void {
    this.dialogInjectedData = this.config.data;
    this.formData = this.dialogInjectedData.formData;
    this.admAcmType = this.dialogInjectedData.dataType;
    this.actionType =
      fromGridTableActionToAcdmAction(this.dialogInjectedData.actionType) || this.dialogInjectedData.actionType;
    this.acdmItem = this.dialogInjectedData.rowTableContent;
    this.isAcdmd = this.admAcmType === MasterDataType.Admd || this.admAcmType === MasterDataType.Acmd;
    this.keysRoot = `ADM_ACM.${this.getActionTypeForTranslation(this.actionType)}.dialog`;
    this.dialogInjectedData.title = this.translationService.translate(`${this.keysRoot}.title`, {
      type: this.translationService.translate(`ADM_ACM.type.${this.admAcmType}`)
    });

    this.actionEmitter = this.dialogInjectedData.actionEmitter;

    const date = this.acdmItem.dateOfIssue;
    this.issueDate = this.datePipe.transform(date, 'dd/MM/yyyy');

    this.acdmNumber = this.acdmItem.ticketDocumentNumber || null;
    this.currency = this.acdmItem.currency;

    this.normalizeAmount();
  }

  private initializeDialogForm(): void {
    switch (this.actionType) {
      case AcdmActionType.supervise:
      case AcdmActionType.issue:
      case AcdmActionType.issuePendingSupervision:
        this.buildIssueForm();
        this.initializeIssueFormListeners();
        this.initializeReasonOptions(ReasonAction.issue);
        break;

      case AcdmActionType.dispute:
        this.buildDisputeForm();
        this.initializeDisputeFormListeners();
        this.initializeReasonOptions(ReasonAction.dispute);
        break;

      case AcdmActionType.approve:
        this.buildApproveDisputeForm();
        break;

      case AcdmActionType.reject:
      case AcdmActionType.editRejectACDM:
        this.buildRejectDisputeForm();
        break;

      case AcdmActionType.forwardGds:
        this.reasonRows = 10;
        this.buildForwardGdsForm();
        this.initializeGdsFormListener();
        this.admAcmService
          .getOne(this.acdmItem.id)
          .pipe(first())
          .subscribe(item => {
            this.acdmItem = item;

            this.initializeGdsOptions();
          });

        break;

      case AcdmActionType.deactivate:
        this.reasonRows = 5;
        this.buildDeactivateForm();
        break;

      case AcdmActionType.reactivate:
      case AcdmActionType.editReactivateACDM:
        this.buildReactivateForm();
        break;

      case AcdmActionType.rejectRequest:
      case AcdmActionType.authorizeRequest:
        this.reasonRows = 5;
        this.buildApproveRejectRequestForm();
        break;

      case AcdmActionType.delete:
        this.buildDeleteForm();
        break;

      default:
        throw new Error('Action not included');
    }
  }

  private normalizeAmount(): void {
    if (this.currency) {
      const decimals = this.currency.decimals;
      this.amount = NumberHelper.roundString(this.acdmItem.totalAmount, decimals);
    }
  }

  private initializeReasonOptions(reasonAction: ReasonAction): void {
    this.reasonOptions = [];
    if (this.acdmItem && !this.isAcdmd) {
      const airlineId = this.acdmItem.airline.id;
      const bspId = (this.acdmItem.agent as Agent).bsp.id;

      this.admAcmService.getReasons(airlineId, bspId, reasonAction).subscribe(reasons => {
        this.issueReasons = reasons;
        this.reasonOptions = this.admAcmService.getReasonsDropDownOptions(reasons);
      });
    }
  }

  private initializeRMICVisibility(): void {
    this.isRMICVisible = !this.isAcdmd && !this.isAcdmRequest;
  }

  private buildIssueForm(): void {
    const detailsGroup = FormUtil.get<AdmAcmIssueViewModel>(this.formData.getForm(), 'details') as FormGroup;

    if (detailsGroup) {
      const originalReasonGroup = FormUtil.get<DetailViewModel>(detailsGroup, 'reasonsDetails') as FormGroup;

      const originalReasonForIssue = FormUtil.get<ReasonDetailViewModel>(
        originalReasonGroup,
        'reasonForIssue'
      ) as FormControl;
      const originalReasonForMemo = FormUtil.get<ReasonDetailViewModel>(
        originalReasonGroup,
        'reasonForMemo'
      ) as FormControl;
      const originalReasonForMemoIssuanceCode = FormUtil.get<ReasonDetailViewModel>(
        originalReasonGroup,
        'reasonForMemoIssuanceCode'
      ) as FormControl;

      this.dialogForm = this.fb.group({
        reasonForIssue: new FormControl(originalReasonForIssue.value),
        reasonForMemo: new FormControl(originalReasonForMemo.value, { updateOn: 'blur' }),
        reasonForMemoIssuanceCode: new FormControl(originalReasonForMemoIssuanceCode.value, {
          validators: [originalReasonForMemoIssuanceCode.validator],
          updateOn: originalReasonForMemoIssuanceCode.updateOn
        }),
        attachmentIds: new FormControl([])
      });

      this.initializeIssueDocumentNumber();
    }
  }

  private initializeIssueDocumentNumber(): void {
    this.dialogInjectedData.numberingConf$
      .pipe(
        switchMap((conf: { mode: AcdmNumberingMode; prefix: string; length: number }) => {
          this.isAcdmNumberEditable =
            conf.mode === AcdmNumberingMode.manual && this.actionType !== AcdmActionType.supervise;
          if (this.isAcdmNumberEditable) {
            this.dialogForm.addControl(
              'ticketDocumentNumber',
              new FormControl('', [Validators.required, Validators.maxLength(10)])
            );

            return this.admAcmService.getDefaultTicketDocNumber(this.acdmItem.airline.id);
          }

          return of(null);
        })
      )
      .subscribe((res: number) => {
        if (res) {
          this.dialogForm.get('ticketDocumentNumber').setValue(res);
        }
      });
  }

  private buildDisputeForm(): void {
    this.dialogForm = this.fb.group({
      status: [AdmAcmStatus.disputed, []],
      disputeReason: null,
      reason: ['', [Validators.required]],
      disputeContact: ['', [Validators.required, Validators.maxLength(70)]],
      attachmentIds: new FormControl([])
    });
  }

  private buildApproveDisputeForm(): void {
    this.dialogForm = this.fb.group({
      status: AdmAcmStatus.approved,
      reason: ''
    });
  }

  private buildRejectDisputeForm(): void {
    this.dialogForm = this.fb.group({
      status: AdmAcmStatus.rejected,
      reason: ['', Validators.required]
    });

    if (this.actionType === AcdmActionType.reject) {
      this.dialogForm.addControl('attachmentIds', new FormControl([]));
    }
  }

  private buildForwardGdsForm(): void {
    const gdsForm = this.fb.group({
      id: [null, [Validators.required]],
      gdsCode: [null, [Validators.required]],
      name: [null, [Validators.required]]
    });

    this.dialogForm = this.fb.group({
      forwardDate: null,
      forwardReason: [null, [Validators.required, blankStringValidator]],
      gds: gdsForm,
      attachmentIds: new FormControl([])
    });

    this.updateDateControl();
  }

  private buildDeactivateForm(): void {
    this.dialogForm = this.fb.group({
      status: AdmAcmStatus.deactivated,
      reason: null
    });
  }

  private buildReactivateForm(): void {
    this.dialogForm = this.fb.group({
      status: AdmAcmStatus.pending
    });
  }

  private buildApproveRejectRequestForm(): void {
    const nextAcdmStatus =
      this.actionType === AcdmActionType.authorizeRequest ? AdmAcmStatus.authorizeRequest : AdmAcmStatus.rejectRequest;

    this.dialogForm = this.fb.group({
      status: nextAcdmStatus
    });

    if (this.actionType === AcdmActionType.rejectRequest) {
      this.dialogForm.addControl('reason', new FormControl([], [Validators.required]));
    }
  }

  private buildDeleteForm(): void {
    this.dialogForm = this.fb.group({
      status: AdmAcmStatus.deleted,
      reason: [null, [Validators.required]]
    });
  }

  private initializeIssueFormListeners(): void {
    const detailsGroup = FormUtil.get<AdmAcmIssueViewModel>(this.formData.getForm(), 'details') as FormGroup;

    const originalReasonGroup = FormUtil.get<DetailViewModel>(detailsGroup, 'reasonsDetails') as FormGroup;

    const reasonForIssueCtrl = FormUtil.get<ReasonDetailViewModel>(this.dialogForm, 'reasonForIssue');
    const reasonForMemoCtrl = FormUtil.get<ReasonDetailViewModel>(this.dialogForm, 'reasonForMemo');
    const reasonIssuanceCodeCtrl = FormUtil.get<ReasonDetailViewModel>(this.dialogForm, 'reasonForMemoIssuanceCode');

    reasonForIssueCtrl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      const selected = this.issueReasons.find(reason => reason.id === value);
      FormUtil.get<ReasonDetailViewModel>(originalReasonGroup, 'reasonForIssue').setValue(value);
      reasonForMemoCtrl.setValue(selected ? selected.description : null);
    });

    reasonForMemoCtrl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      FormUtil.get<ReasonDetailViewModel>(originalReasonGroup, 'reasonForMemo').setValue(value);
    });

    reasonIssuanceCodeCtrl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      FormUtil.get<ReasonDetailViewModel>(originalReasonGroup, 'reasonForMemoIssuanceCode').setValue(value);
    });
  }

  private initializeDisputeFormListeners(): void {
    const disputeReasonCtrl = FormUtil.get<DisputeViewModel>(this.dialogForm, 'disputeReason');
    const reasonCtrl = FormUtil.get<DisputeViewModel>(this.dialogForm, 'reason');

    disputeReasonCtrl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      const selected = this.reasonOptions.find(reason => reason.value === value);
      reasonCtrl.setValue(selected.label);
    });
  }

  private initializeIssueClickListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        tap(() => this.validateControls()),
        skipWhile(
          event => event.clickedBtn === FooterButton.Cancel || this.isUploadingInProgress || this.dialogForm.invalid
        ),
        tap(() => (this.isUploadingInProgress = true)),
        first(),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.handleFooterButtonSubscription();
      });
  }

  private handleFooterButtonSubscription(): void {
    const fileshasErrors = this.uploadComponentRef ? this.uploadComponentRef.hasFilesWithErrors : false;
    if (!this.dialogForm.invalid && !fileshasErrors) {
      this.onSendFiles();
    } else {
      this.handleDialogSubmission();
    }
  }

  private initializeGdsFormListener(): void {
    const gdsForm = FormUtil.get<ForwardGdsViewModel>(this.dialogForm, 'gds') as FormGroup;

    FormUtil.get<GdsViewModel>(gdsForm, 'id')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(gdsId => this.updateGds(gdsId));
  }

  private getActionTypeForTranslation(actionType: AcdmActionType): AcdmActionType {
    let actionTypeForTranslation = actionType;

    if (actionTypeForTranslation === AcdmActionType.editReactivateACDM) {
      actionTypeForTranslation = AcdmActionType.reactivate;
    }

    if (actionTypeForTranslation === AcdmActionType.editRejectACDM) {
      actionTypeForTranslation = AcdmActionType.reject;
    }

    if (actionTypeForTranslation === AcdmActionType.supervise) {
      actionTypeForTranslation = AcdmActionType.issue;
    }

    return actionTypeForTranslation;
  }

  private validateControls() {
    if (this.dialogForm.invalid) {
      FormUtil.showControlState(this.dialogForm);
    }
  }

  private onSendFiles(): void {
    if (this.uploadComponentRef && this.uploadComponentRef.hasFilesInPreviewMode) {
      this.uploadFiles();
    } else {
      this.handleDialogSubmission();
    }
  }

  private handleDialogSubmission(): void {
    let payloadToBE: any;
    switch (this.actionType) {
      case AcdmActionType.issue:
      case AcdmActionType.supervise:
      case AcdmActionType.issuePendingSupervision:
        this.formData.getForm().patchValue(this.dialogForm.value);
        if (this.actionType === AcdmActionType.issue || this.actionType === AcdmActionType.issuePendingSupervision) {
          this.handleTicketDocNumberSubmission();
        }

        this.actionEmitter.next(this.actionType);
        break;

      case AcdmActionType.editReactivateACDM:
      case AcdmActionType.editRejectACDM: {
        const acdmStatus = FormUtil.get<DisputeViewModel>(this.dialogForm, 'status').value;
        payloadToBE = { acdmStatus };

        const reasonCtrl = FormUtil.get<DisputeViewModel>(this.dialogForm, 'reason');
        if (reasonCtrl?.value) {
          const reasons: Array<Omit<Reason, 'id'>> = [
            { action: ReasonAction.disputeRejected, reason: reasonCtrl.value }
          ];
          payloadToBE = { ...payloadToBE, reasons };
        }

        this.admAcmService.save({ ...this.acdmItem, ...payloadToBE }).subscribe(
          response => {
            this.onStatusChangeSuccess(response);
            this.navigateView();
          },
          response => this.onStatusChangeError(response)
        );
        break;
      }
      case AcdmActionType.delete:
      case AcdmActionType.authorizeRequest:
      case AcdmActionType.rejectRequest:
      case AcdmActionType.reactivate:
      case AcdmActionType.deactivate:
      case AcdmActionType.approve:
      case AcdmActionType.reject:
      case AcdmActionType.dispute:
        this.admAcmService
          .updateAcdmStatus(
            this.acdmItem.id,
            FormUtil.get<DisputeViewModel>(this.dialogForm, 'status').value,
            this.dialogForm.value
          )
          .subscribe(
            response => {
              if (this.actionType !== AcdmActionType.delete) {
                this.onStatusChangeSuccess(response);
                if (this.actionType === AcdmActionType.dispute) {
                  this.propagateDisputeReason(response);
                }
              } else {
                this.onDeleteSuccess();
              }
            },
            response => {
              this.onStatusChangeError(response);
            }
          );
        break;

      case AcdmActionType.forwardGds:
        this.admAcmService.forwardGdsAdm(this.acdmItem.id, this.dialogForm.value).subscribe(
          response => {
            this.onForwardToGdsSuccess(response);
          },
          response => {
            this.onStatusChangeError(response.error);
          }
        );
        break;

      default:
        break;
    }
  }

  private onStatusChangeSuccess(updatedAcdm: AdmAcmIssueBE): void {
    this.showSuccessNotification();
    this.dialog.close();
    this.updateStatus({ status: updatedAcdm.acdmStatus, sentToDpc: updatedAcdm.sentToDpc });
  }

  private onForwardToGdsSuccess(gdsForward: GdsForward): void {
    this.showSuccessNotification();
    this.dialog.close();
    // TODO Review refreshCurrentTab to keep notifications displayed and change this line by this.tabService.refreshCurrentTab()
    this.updateStatus({ status: this.acdmItem.acdmStatus, sentToDpc: this.acdmItem.sentToDpc });
    this.updateForwardReasons(gdsForward);
  }

  private onDeleteSuccess(): void {
    this.showSuccessNotification();
    this.dialog.close();
    this.navigateDeleteQuery();
  }

  private navigateDeleteQuery(): void {
    const deleteQueryUrl = getDeleteQueryUrl(this.admAcmType);

    this.isQueryView().subscribe(isQueryView =>
      isQueryView ? this.emitActionToReloadQuery() : this.tabService.closeCurrentTabAndNavigate(deleteQueryUrl)
    );
  }

  private navigateView(): void {
    const url = `${urlView[this.admAcmType]}/${this.acdmItem.id}`;
    this.tabService.closeCurrentTabAndNavigate(url);
  }

  private showSuccessNotification(): void {
    const successKey = this.getTranslationKey('notification.success');
    const message = this.translationService.translate(successKey, {
      type: this.admAcmType.toUpperCase(),
      docNumber: this.acdmNumber,
      gdsCode: this.gdsSelected?.gdsCode,
      gdsName: this.gdsSelected?.name
    });
    this.notificationService.showSuccess(message);
  }

  private onStatusChangeError(response: ServerError | ResponseErrorBE): void {
    this.isUploadingInProgress = false;
    //* We subscribe again to dialog action button so errors can be amended and form resent
    this.initializeIssueClickListener();

    if ((response as ResponseErrorBE).errorCode === 400) {
      const errorKey = this.getTranslationKey('notification.error');
      const message = this.translationService.translate(errorKey, {
        type: this.admAcmType.toUpperCase(),
        docNumber: this.acdmNumber
      });
      this.notificationService.showError(this.translationService.translate(message));
      this.markFieldsWithErrors(response as ResponseErrorBE);
    } else if ((response as ServerError).status === 403) {
      this.dialog.close();
      this.notificationService.showError(this.translationService.translate('ADM_ACM.not-found-document-message'));
      this.formData.getSubject(AdmAcmActionEmitterType.status).next(AdmAcmStatus.deleted);
    } else {
      this.globalErrorHandlerService.handleError(response);
    }
  }

  private markFieldsWithErrors(response: ResponseErrorBE): void {
    if (response.messages?.length) {
      response.messages.forEach(messageObj => {
        const { message, messageParams } = messageObj;

        if (message && messageParams?.length) {
          const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
          const invalidControl = this.dialogForm.get(controlName);

          if (invalidControl) {
            invalidControl.setErrors({
              backendError: { message }
            });
          }
        }
      });
    }
  }

  private updateStatus(acdmStatus: StatusViewModel): void {
    this.isQueryView().subscribe(isQueryView =>
      isQueryView
        ? this.emitActionToReloadQuery()
        : this.formData.getSubject(AdmAcmActionEmitterType.status).next(acdmStatus)
    );
  }

  private updateForwardReasons(gdsForward: GdsForward): void {
    this.formData.getSubject(AdmAcmActionEmitterType.gdsForwardReasons).next(gdsForward as ForwardGdsViewModel);
  }

  private emitActionToReloadQuery(): void {
    this.actionEmitter.next();
  }

  private isQueryView(): Observable<boolean> {
    return this.tabService.isCurrentTabOnUrl(urlList[this.admAcmType]);
  }

  private propagateDisputeReason(updatedAcdm: AdmAcmIssueBE): void {
    const reasonsData: Partial<DetailViewModel> = {
      reasons: updatedAcdm.reasons,
      disputeContact: updatedAcdm.disputeContact,
      disputeTime: updatedAcdm.disputeTime
    };

    this.formData.getSubject(AdmAcmActionEmitterType.reasons).next(reasonsData);
  }

  private uploadFiles(): void {
    this.uploadComponentRef
      .uploadFiles()
      .pipe(takeUntil(this.destroy$))
      .subscribe(allValidFilesAreUploaded =>
        allValidFilesAreUploaded ? this.handleDialogSubmission() : this.initializeIssueClickListener()
      );
  }

  private handleTicketDocNumberSubmission(): void {
    const basicInfoGroup = FormUtil.get<AdmAcmIssueViewModel>(this.formData.getForm(), 'basicInformation') as FormGroup;
    const ticketDocNumberCtrl = FormUtil.get<BasicInformationViewModel>(basicInfoGroup, 'ticketDocumentNumber');
    if (this.isAcdmNumberEditable) {
      const ticketDocNumberValue = this.dialogForm.get('ticketDocumentNumber').value;
      ticketDocNumberCtrl.setValue(ticketDocNumberValue);
    } else {
      ticketDocNumberCtrl.setValue(null);
    }
  }

  private initializeGdsOptions(): void {
    this.gdsOptions = [];
    const bspId = (this.acdmItem.agent as Agent).bsp.id;
    this.admAcmService.getGdssBsp(bspId, this.acdmItem.id).subscribe((res: GdsViewModel[]) => {
      this.gdsList = res;
      this.gdsOptions = this.getGdsOptions(this.gdsList);
      this.updateGdsControl();
      this.initializeForwardMessage();
      this.disableForwardBtn();
    });
  }

  private getGdsOptions(list: GdsViewModel[]): DropdownOption<number>[] {
    let forwardGdsList = list;
    if (this.acdmItem.gdsForwards) {
      forwardGdsList = list.filter(
        item => !this.acdmItem.gdsForwards.map(forwarded => forwarded.gds.id).includes(item.id)
      );
    }

    return this.admAcmService.getGdsOptionDropdown(forwardGdsList);
  }

  private updateGdsControl(): void {
    const gdsForm = FormUtil.get<ForwardGdsViewModel>(this.dialogForm, 'gds');
    if (!this.gdsOptions.length) {
      gdsForm.disable();
    } else {
      gdsForm.enable();
    }
    if (this.gdsOptions.length === 1) {
      FormUtil.get<GdsViewModel>(gdsForm as FormGroup, 'id').setValue(this.gdsOptions[0].value);
    }
  }

  private initializeForwardMessage(): void {
    this.forwardGdsMessage = {
      hidden: this.gdsOptions.length > 0,
      message: 'ADM_ACM.forwardGds.dialog.noOptionsMessage',
      type: AlertMessageType.info
    };
  }

  private updateDateControl(): void {
    const bspId = (this.acdmItem.agent as Agent).bsp.id;
    this.admAcmService.getBspTime(bspId).subscribe(bspTime => {
      FormUtil.get<ForwardGdsViewModel>(this.dialogForm, 'forwardDate').setValue(bspTime.localTime, {
        emitEvent: false
      });
    });
  }

  private updateGds(gdsId: number): void {
    this.gdsSelected = cloneDeep(this.gdsList.find(gds => gds.id === gdsId));

    if (this.gdsSelected) {
      const gdsForm = FormUtil.get<ForwardGdsViewModel>(this.dialogForm, 'gds') as FormGroup;

      FormUtil.get<GdsViewModel>(gdsForm, 'gdsCode').setValue(this.gdsSelected.gdsCode);
      FormUtil.get<GdsViewModel>(gdsForm, 'name').setValue(this.gdsSelected.name);
    }
  }

  private disableForwardBtn(): void {
    const forwardBtn = this.config.data.buttons.find(button => button.type === FooterButton.ForwardGds);
    if (forwardBtn) {
      forwardBtn.isDisabled = !this.gdsOptions.length;
    }
  }
}
