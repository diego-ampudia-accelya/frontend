import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { MockProvider } from 'ng-mocks';
import { BehaviorSubject, NEVER, of, Subject } from 'rxjs';
import { AcdmActionType, AcdmNumberingMode, ReasonAction } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AlertMessageType } from '~app/shared/enums';
import { NumberHelper } from '~app/shared/helpers';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService, TabService } from '~app/shared/services';
import { AdmAcmConfirmationDialogComponent } from './adm-acm-confirmation-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    formData: {},
    admAcmType: MasterDataType.Acm,
    actionType: AcdmActionType.approve,
    hasCancelButton: true,
    footerButtonsType: FooterButton.Issue,
    actionEmitter: new Subject(),
    rowTableContent: {
      dateOfIssue: '09/08/2019',
      ticketDocumentNumber: '2791565094071',
      currency: {
        code: 'EUR',
        decimals: 2,
        id: 10022
      },
      amounts: {
        totalAmount: 1000
      },
      details: {
        reasonDetails: {}
      }
    },
    activatedRoute: { data: of({}), params: of({}) },
    dialogFormSubmitEmitter: NEVER
  }
};

describe('AdmAcmConfirmationDialogComponent', () => {
  let component: AdmAcmConfirmationDialogComponent;
  let fixture: ComponentFixture<AdmAcmConfirmationDialogComponent>;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmConfirmationDialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: DialogService, useValue: { close: jasmine.createSpy() } },
        { provide: ReactiveSubject, useValue: { asObservable: of({}) } },
        FormBuilder,
        MockProvider(TabService),
        DatePipe,
        { provide: ErrorHandler, useValue: { handleError: () => {} } },
        { provide: NotificationService, useValue: { showError: jasmine.createSpy(), showSuccess: jasmine.createSpy() } }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmConfirmationDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getTranslationKey', () => {
    it('should return translation key', () => {
      component['keysRoot'] = 'root-key';
      const res = component.getTranslationKey('test');

      expect(res).toBe('root-key.test');
    });
  });

  describe('onUploadIsInProgress', () => {
    it('should set isUploadingInProgress as true', () => {
      component['isUploadingInProgress'] = false;

      component.onUploadIsInProgress(true);

      expect(component['isUploadingInProgress']).toBe(true);
    });
  });

  describe('initializeRegularizedMessage', () => {
    beforeEach(() => {
      component.getTranslationKey = jasmine.createSpy().and.returnValue('translated-msg');
    });

    it('should call getTranslationKey', () => {
      component['initializeRegularizedMessage']();

      expect(component.getTranslationKey).toHaveBeenCalledWith('regularizedMsg');
    });

    it('should set regularizedMsg', () => {
      component.regularizedMsg = null;

      component['initializeRegularizedMessage']();

      expect(component.regularizedMsg).not.toBeNull();
      expect(component.regularizedMsg.message).toBe('translated-msg');
      expect(component.regularizedMsg.type).toBe(AlertMessageType.info);
    });
  });

  describe('initializeDialogData', () => {
    beforeEach(() => {
      component['normalizeAmount'] = jasmine.createSpy();
      component['getActionTypeForTranslation'] = jasmine.createSpy().and.returnValue('issue');
    });

    it('should set dialogInjectedData', () => {
      component['dialogInjectedData'] = null;

      component['initializeDialogData']();

      expect(component['dialogInjectedData']).not.toBeNull();
      expect(component['dialogInjectedData']).toEqual(mockConfig.data as any);
    });

    it('should set formData', () => {
      component['formData'] = null;

      component['initializeDialogData']();

      expect(component['formData']).not.toBeNull();
    });

    it('should set admAcmType', () => {
      component['admAcmType'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          dataType: MasterDataType.Acm
        }
      };

      component['initializeDialogData']();

      expect(component['admAcmType']).not.toBeNull();
      expect(component['admAcmType']).toBe(MasterDataType.Acm);
    });

    it('should set actionType', () => {
      component['actionType'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          actionType: GridTableActionType.Details
        }
      };

      component['initializeDialogData']();

      expect(component['actionType']).not.toBeNull();
      expect(component['actionType']).toBe(AcdmActionType.details);
    });

    it('should set acdmItem', () => {
      component['actionType'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          rowTableContent: {
            id: 5
          }
        }
      };

      component['initializeDialogData']();

      expect(component['acdmItem']).not.toBeNull();
      expect(component['acdmItem']).toEqual({ id: 5 } as any);
    });

    it('should set isAcdmd', () => {
      component['actionType'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          dataType: MasterDataType.Admd
        }
      };

      component['initializeDialogData']();

      expect(component['isAcdmd']).not.toBeNull();
      expect(component['isAcdmd']).toBe(true);
    });

    it('should call getActionTypeForTranslation', () => {
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          actionType: GridTableActionType.Details
        }
      };

      component['initializeDialogData']();

      expect(component['getActionTypeForTranslation']).toHaveBeenCalledWith(AcdmActionType.details);
    });

    it('should set keysRoot', () => {
      component['actionType'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          dataType: MasterDataType.Admd
        }
      };

      component['initializeDialogData']();

      expect(component['keysRoot']).not.toBeNull();
      expect(component['keysRoot']).toBe('ADM_ACM.issue.dialog');
    });

    it('should set title for config data', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('translated-title');
      component['config'] = cloneDeep(mockConfig);

      expect(component['config'].data.title).not.toBe('translated-title');

      component['initializeDialogData']();

      expect(component['config'].data.title).toBe('translated-title');
    });

    it('should set actionEmitter', () => {
      component['actionEmitter'] = null;

      component['initializeDialogData']();

      expect(component['actionEmitter']).not.toBeNull();
    });

    it('should set issueDate', () => {
      component['issueDate'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          rowTableContent: {
            dateOfIssue: '2020-20-08'
          }
        }
      };
      const expectedValue = component['datePipe'].transform('2020-20-08', 'dd/MM/yyyy');

      component['initializeDialogData']();

      expect(component['issueDate']).not.toBeNull();
      expect(component['issueDate']).toBe(expectedValue);
    });

    it('should set acdmNumber', () => {
      component['issueDate'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          rowTableContent: {
            ticketDocumentNumber: 'doc-number'
          }
        }
      };

      component['initializeDialogData']();

      expect(component['acdmNumber']).not.toBeNull();
      expect(component['acdmNumber']).toBe('doc-number');
    });

    it('should set currency', () => {
      component['issueDate'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          rowTableContent: {
            currency: { decimals: 2 }
          }
        }
      };

      component['initializeDialogData']();

      expect(component['currency']).not.toBeNull();
      expect(component['currency']).toEqual({ decimals: 2 });
    });

    it('should call normalizeAmount', () => {
      component['initializeDialogData']();

      expect(component['normalizeAmount']).toHaveBeenCalled();
    });
  });

  describe('initializeDialogForm', () => {
    describe('supervise, issue, issuePendingSupervision', () => {
      beforeEach(() => {
        component['buildIssueForm'] = jasmine.createSpy();
        component['initializeIssueFormListeners'] = jasmine.createSpy();
        component['initializeReasonOptions'] = jasmine.createSpy();
      });

      it('should call buildIssueForm, initializeIssueFormListeners, initializeReasonOptions when actionType is supervise', () => {
        component.actionType = AcdmActionType.supervise;

        component['initializeDialogForm']();

        expect(component['buildIssueForm']).toHaveBeenCalled();
        expect(component['initializeIssueFormListeners']).toHaveBeenCalled();
        expect(component['initializeReasonOptions']).toHaveBeenCalledWith(ReasonAction.issue);
      });

      it('should call buildIssueForm, initializeIssueFormListeners, initializeReasonOptions when actionType is issue', () => {
        component.actionType = AcdmActionType.issue;

        component['initializeDialogForm']();

        expect(component['buildIssueForm']).toHaveBeenCalled();
        expect(component['initializeIssueFormListeners']).toHaveBeenCalled();
        expect(component['initializeReasonOptions']).toHaveBeenCalledWith(ReasonAction.issue);
      });

      it('should call buildIssueForm, initializeIssueFormListeners, initializeReasonOptions when actionType is issuePendingSupervision', () => {
        component.actionType = AcdmActionType.issuePendingSupervision;

        component['initializeDialogForm']();

        expect(component['buildIssueForm']).toHaveBeenCalled();
        expect(component['initializeIssueFormListeners']).toHaveBeenCalled();
        expect(component['initializeReasonOptions']).toHaveBeenCalledWith(ReasonAction.issue);
      });
    });

    it('should call buildDisputeForm, initializeDisputeFormListeners, initializeReasonOptions when actionType is dispute', () => {
      component['buildDisputeForm'] = jasmine.createSpy();
      component['initializeDisputeFormListeners'] = jasmine.createSpy();
      component['initializeReasonOptions'] = jasmine.createSpy();
      component.actionType = AcdmActionType.dispute;

      component['initializeDialogForm']();

      expect(component['buildDisputeForm']).toHaveBeenCalled();
      expect(component['initializeDisputeFormListeners']).toHaveBeenCalled();
      expect(component['initializeReasonOptions']).toHaveBeenCalledWith(ReasonAction.dispute);
    });

    it('should call buildApproveDisputeForm when actionType is approve', () => {
      component['buildApproveDisputeForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.approve;

      component['initializeDialogForm']();

      expect(component['buildApproveDisputeForm']).toHaveBeenCalled();
    });

    it('should call buildRejectDisputeForm when actionType is reject', () => {
      component['buildRejectDisputeForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.reject;

      component['initializeDialogForm']();

      expect(component['buildRejectDisputeForm']).toHaveBeenCalled();
    });

    it('should call buildRejectDisputeForm when actionType is editRejectACDM', () => {
      component['buildRejectDisputeForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.editRejectACDM;

      component['initializeDialogForm']();

      expect(component['buildRejectDisputeForm']).toHaveBeenCalled();
    });

    describe('forwardGds', () => {
      beforeEach(() => {
        component.actionType = AcdmActionType.forwardGds;
        component['buildForwardGdsForm'] = jasmine.createSpy();
        component['initializeGdsFormListener'] = jasmine.createSpy();
        component['admAcmService'].getOne = jasmine.createSpy().and.returnValue(of({ id: 7 }));
        component['initializeGdsOptions'] = jasmine.createSpy();
      });

      it('should set reasonRows to 10 when actionType is forwardGds', () => {
        component.reasonRows = 0;

        component['initializeDialogForm']();

        expect(component.reasonRows).toBe(10);
      });

      it('should call buildForwardGdsForm, initializeGdsFormListener when actionType is forwardGds', () => {
        component['initializeDialogForm']();

        expect(component['buildForwardGdsForm']).toHaveBeenCalled();
        expect(component['initializeGdsFormListener']).toHaveBeenCalled();
      });

      it('should call getOne when actionType is forwardGds', fakeAsync(() => {
        component['acdmItem'] = { id: 4 } as any;

        component['initializeDialogForm']();
        tick();

        expect(component['admAcmService'].getOne).toHaveBeenCalledWith(4);
      }));

      it('should set acdmItem when actionType is forwardGds', fakeAsync(() => {
        component['acdmItem'] = { id: 4 } as any;

        component['initializeDialogForm']();
        tick();

        expect(component['acdmItem']).toEqual({ id: 7 } as any);
      }));

      it('should call initializeGdsOptions when actionType is forwardGds', fakeAsync(() => {
        component['acdmItem'] = { id: 4 } as any;

        component['initializeDialogForm']();
        tick();

        expect(component['initializeGdsOptions']).toHaveBeenCalled();
      }));
    });

    it('should set reasonRows to 5 when actionType is deactivate', () => {
      component['buildDeactivateForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.deactivate;
      component.reasonRows = 0;

      component['initializeDialogForm']();

      expect(component.reasonRows).toBe(5);
    });

    it('should call buildDeactivateForm when actionType is deactivate', () => {
      component['buildDeactivateForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.deactivate;

      component['initializeDialogForm']();

      expect(component['buildDeactivateForm']).toHaveBeenCalled();
    });

    it('should call buildReactivateForm when actionType is reactivate', () => {
      component['buildReactivateForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.reactivate;

      component['initializeDialogForm']();

      expect(component['buildReactivateForm']).toHaveBeenCalled();
    });

    it('should call buildReactivateForm when actionType is editReactivateACDM', () => {
      component['buildReactivateForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.editReactivateACDM;

      component['initializeDialogForm']();

      expect(component['buildReactivateForm']).toHaveBeenCalled();
    });

    it('should set reasonRows to 5 when actionType is rejectRequest', () => {
      component['buildApproveRejectRequestForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.rejectRequest;
      component.reasonRows = 0;

      component['initializeDialogForm']();

      expect(component.reasonRows).toBe(5);
    });

    it('should call buildApproveRejectRequestForm when actionType is rejectRequest', () => {
      component['buildApproveRejectRequestForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.rejectRequest;

      component['initializeDialogForm']();

      expect(component['buildApproveRejectRequestForm']).toHaveBeenCalled();
    });

    it('should set reasonRows to 5 when actionType is authorizeRequest', () => {
      component['buildApproveRejectRequestForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.authorizeRequest;
      component.reasonRows = 0;

      component['initializeDialogForm']();

      expect(component.reasonRows).toBe(5);
    });

    it('should call buildApproveRejectRequestForm when actionType is authorizeRequest', () => {
      component['buildApproveRejectRequestForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.authorizeRequest;

      component['initializeDialogForm']();

      expect(component['buildApproveRejectRequestForm']).toHaveBeenCalled();
    });

    it('should call buildDeleteForm when actionType is delete', () => {
      component['buildDeleteForm'] = jasmine.createSpy();
      component.actionType = AcdmActionType.delete;

      component['initializeDialogForm']();

      expect(component['buildDeleteForm']).toHaveBeenCalled();
    });
  });

  describe('normalizeAmount', () => {
    it('should set amount when currency exists', () => {
      component.currency = { decimals: 2 };
      component['acdmItem'] = { totalAmount: 5 } as any;
      const expectedRes = NumberHelper.roundString(5, 2);
      component.amount = '';

      component['normalizeAmount']();

      expect(component.amount).toBe(expectedRes);
    });
  });

  describe('initializeReasonOptions', () => {
    beforeEach(() => {
      component['admAcmService'].getReasons = jasmine
        .createSpy()
        .and.returnValue(of([{ id: 1, description: 'test-description', title: 'test-title' }]));
      component['admAcmService'].getReasonsDropDownOptions = jasmine
        .createSpy()
        .and.returnValue([
          { value: { id: 1, description: 'test-description', title: 'test-title' }, label: 'test-label' }
        ]);
      component['acdmItem'] = {
        airline: { id: 3 },
        agent: { bsp: { id: 1111 } }
      } as any;
    });

    it('should call getReasons', fakeAsync(() => {
      component['initializeReasonOptions'](ReasonAction.issue);
      tick();

      expect(component['admAcmService'].getReasons).toHaveBeenCalledWith(3, 1111, ReasonAction.issue);
    }));

    it('should set issueReasons', fakeAsync(() => {
      component['issueReasons'] = [];

      component['initializeReasonOptions'](ReasonAction.issue);
      tick();

      expect(component['issueReasons']).toEqual([
        { id: 1, description: 'test-description', title: 'test-title' }
      ] as any);
    }));

    it('should call getReasonsDropDownOptions', fakeAsync(() => {
      component['initializeReasonOptions'](ReasonAction.issue);
      tick();

      expect(component['admAcmService'].getReasonsDropDownOptions).toHaveBeenCalledWith([
        { id: 1, description: 'test-description', title: 'test-title' }
      ]);
    }));

    it('should set reasonOptions', fakeAsync(() => {
      component['reasonOptions'] = [];

      component['initializeReasonOptions'](ReasonAction.issue);
      tick();

      expect(component['reasonOptions']).toEqual([
        { value: { id: 1, description: 'test-description', title: 'test-title' }, label: 'test-label' }
      ] as any);
    }));
  });

  describe('initializeRMICVisibility', () => {
    it('should set initializeRMICVisibility as true when isAcdmd and isAcdmRequest are false', () => {
      component['isAcdmd'] = false;
      component.admAcmType = MasterDataType.Acm;

      component['initializeRMICVisibility']();

      expect(component.isRMICVisible).toBe(true);
    });

    it('should set initializeRMICVisibility as false when isAcdmd is true and isAcdmRequest is false', () => {
      component['isAcdmd'] = true;
      component.admAcmType = MasterDataType.Acm;

      component['initializeRMICVisibility']();

      expect(component.isRMICVisible).toBe(false);
    });

    it('should set initializeRMICVisibility as false when isAcdmd is false and isAcdmRequest is true', () => {
      component['isAcdmd'] = false;
      component.admAcmType = MasterDataType.Acmq;

      component['initializeRMICVisibility']();

      expect(component.isRMICVisible).toBe(false);
    });

    it('should set initializeRMICVisibility as false when isAcdmd and isAcdmRequest are true', () => {
      component['isAcdmd'] = true;
      component.admAcmType = MasterDataType.Acmq;

      component['initializeRMICVisibility']();

      expect(component.isRMICVisible).toBe(false);
    });
  });

  xdescribe('buildIssueForm', () => {
    beforeEach(() => {
      component['initializeIssueDocumentNumber'] = jasmine.createSpy();
      component['formData'].getForm = jasmine
        .createSpy()
        .and.returnValue(new FormGroup({ details: new FormGroup({ reasonsDetails: null }) }));
    });

    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildIssueForm']();

      expect(component.dialogForm).not.toBeNull();
    });

    it('should call initializeIssueDocumentNumber', () => {
      component['buildIssueForm']();

      expect(component['initializeIssueDocumentNumber']).toHaveBeenCalled();
    });
  });

  describe('initializeIssueDocumentNumber', () => {
    beforeEach(() => {
      component['dialogInjectedData'].numberingConf$ = new BehaviorSubject({ mode: AcdmNumberingMode.manual });
      component.actionType = AcdmActionType.activity;
      component.dialogForm = new FormGroup({});
      component['admAcmService'].getDefaultTicketDocNumber = jasmine.createSpy().and.returnValue(of(88));

      component['acdmItem'] = {
        airline: {
          id: 7
        }
      } as any;
    });

    it('should set isAcdmNumberEditable', fakeAsync(() => {
      component.isAcdmNumberEditable = null;

      component['initializeIssueDocumentNumber']();
      tick();

      expect(component.isAcdmNumberEditable).toBe(true);
    }));

    it('should add ticketDocumentNumber', fakeAsync(() => {
      expect(component.dialogForm.controls.ticketDocumentNumber).toBeFalsy();

      component['initializeIssueDocumentNumber']();
      tick();

      expect(component.dialogForm.controls.ticketDocumentNumber).toBeTruthy();
    }));

    it('should call getDefaultTicketDocNumber', fakeAsync(() => {
      component['initializeIssueDocumentNumber']();
      tick();

      expect(component['admAcmService'].getDefaultTicketDocNumber).toHaveBeenCalledWith(7);
    }));

    it('should set value to ticketDocumentNumber', fakeAsync(() => {
      component['initializeIssueDocumentNumber']();
      tick();

      expect(component.dialogForm.controls.ticketDocumentNumber.value).toBe(88);
    }));
  });

  describe('buildDisputeForm', () => {
    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildDisputeForm']();

      expect(component.dialogForm).not.toBeNull();
    });
  });

  describe('buildApproveDisputeForm', () => {
    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildApproveDisputeForm']();

      expect(component.dialogForm).not.toBeNull();
    });
  });

  describe('buildRejectDisputeForm', () => {
    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildRejectDisputeForm']();

      expect(component.dialogForm).not.toBeNull();
    });

    it('should set attachmentIds control to dialogForm', () => {
      component.actionType = AcdmActionType.reject;
      expect(component.dialogForm.controls.attachmentIds).toBeFalsy();

      component['buildRejectDisputeForm']();

      expect(component.dialogForm.controls.attachmentIds).toBeTruthy();
    });
  });

  describe('buildForwardGdsForm', () => {
    beforeEach(() => {
      component['updateDateControl'] = jasmine.createSpy();
    });

    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildForwardGdsForm']();

      expect(component.dialogForm).not.toBeNull();
    });

    it('should call updateDateControl', () => {
      component['buildForwardGdsForm']();

      expect(component['updateDateControl']).toHaveBeenCalled();
    });
  });

  describe('buildDeactivateForm', () => {
    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildDeactivateForm']();

      expect(component.dialogForm).not.toBeNull();
    });
  });

  describe('buildReactivateForm', () => {
    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildReactivateForm']();

      expect(component.dialogForm).not.toBeNull();
    });
  });

  describe('buildApproveRejectRequestForm', () => {
    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildApproveRejectRequestForm']();

      expect(component.dialogForm).not.toBeNull();
    });

    it('should set reason control to dialogForm', () => {
      component.actionType = AcdmActionType.rejectRequest;
      component.dialogForm = null;

      component['buildApproveRejectRequestForm']();

      expect(component.dialogForm.controls.reason).toBeTruthy();
    });
  });

  describe('buildDeleteForm', () => {
    it('should set dialogForm', () => {
      component.dialogForm = null;

      component['buildDeleteForm']();

      expect(component.dialogForm).not.toBeNull();
    });
  });
});
