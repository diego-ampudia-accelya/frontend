import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, of } from 'rxjs';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { TranslatePipeMock } from '~app/test';
import { AdmAcmNotFoundDialogComponent } from './adm-acm-not-found-dialog.component';

describe('AdmAcmNotFoundDialogComponent', () => {
  let component: AdmAcmNotFoundDialogComponent;
  let fixture: ComponentFixture<AdmAcmNotFoundDialogComponent>;

  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const mockConfig = {
    data: {
      title: 'title',
      footerButtonsType: FooterButton.Accept,
      buttons: [
        {
          title: 'test',
          buttonDesign: ButtonDesign.Primary,
          type: FooterButton.Accept,
          isDisabled: false
        } as ModalAction
      ]
    }
  } as DialogConfig;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmNotFoundDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [],
      providers: [
        FormBuilder,
        { provide: ReactiveSubject, asObservable: of({}) },
        { provide: DialogService, useValue: { close: jasmine.createSpy() } },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmNotFoundDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('initializeDialogData', () => {
    beforeEach(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-title');
    });

    it('should set dialogInjectedData', () => {
      component['dialogInjectedData'] = null;

      component['initializeDialogData']();

      expect(component['dialogInjectedData']).toEqual(mockConfig.data);
    });

    it('should call translate', () => {
      component['initializeDialogData']();

      expect(component['translationService'].translate).toHaveBeenCalledWith('ADM_ACM.not-found-document-label');
    });

    it('should set title for dialogInjectedData', () => {
      component['initializeDialogData']();

      expect(component['dialogInjectedData'].title).toEqual('test-title');
    });
  });

  describe('initializeButtonClickListener', () => {
    it('should call close', fakeAsync(() => {
      (component as any).reactiveSubject.asObservable = new BehaviorSubject(1);

      component['initializeButtonClickListener']();
      (component['reactiveSubject'].asObservable as any).next();
      tick(2);

      expect(component['dialog'].close).toHaveBeenCalled();
    }));
  });
});
