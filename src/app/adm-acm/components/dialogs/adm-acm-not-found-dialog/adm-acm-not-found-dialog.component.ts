import { Component, OnDestroy, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';

@Component({
  selector: 'bspl-adm-acm-not-found-dialog',
  templateUrl: './adm-acm-not-found-dialog.component.html',
  styleUrls: ['./adm-acm-not-found-dialog.component.scss']
})
export class AdmAcmNotFoundDialogComponent implements OnInit, OnDestroy {
  private dialogInjectedData;
  private destroy$ = new Subject();

  constructor(
    private dialog: DialogService,
    private config: DialogConfig,
    private translationService: L10nTranslationService,
    private reactiveSubject: ReactiveSubject
  ) {
    this.initializeDialogData();
  }

  public ngOnInit(): void {
    this.initializeButtonClickListener();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeDialogData(): void {
    this.dialogInjectedData = this.config.data;
    this.dialogInjectedData.title = this.translationService.translate('ADM_ACM.not-found-document-label');
  }

  private initializeButtonClickListener(): void {
    this.reactiveSubject.asObservable.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.dialog.close();
    });
  }
}
