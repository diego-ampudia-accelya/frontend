import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthState } from '~app/auth/reducers/auth.reducer';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { User } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { AdmAcmBspSelectionDialogComponent } from './adm-acm-bsp-selection-dialog.component';

const dialogConfig: DialogConfig = {
  data: {
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ],
    buttons: [{ type: FooterButton.Request }]
  }
};

describe('AdmAcmBspSelectionDialogComponent', () => {
  let component: AdmAcmBspSelectionDialogComponent;
  let fixture: ComponentFixture<AdmAcmBspSelectionDialogComponent>;
  const notificationServiceSpy = createSpyObject(NotificationService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const initialState: { auth: AuthState } = {
    auth: {
      user: {} as User
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmBspSelectionDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        L10nTranslationModule.forRoot(l10nConfig),
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: DialogConfig,
          useValue: dialogConfig
        },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        provideMockStore({ initialState }),
        ReactiveSubject,
        L10nTranslationService,
        FormBuilder
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmBspSelectionDialogComponent);
    component = fixture.componentInstance;
    const formBuilder: FormBuilder = new FormBuilder();
    component.form = formBuilder.group({
      bsp: [null, Validators.required]
    });
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should build a form calling buildForm method', () => {
    const formExpected = { bsp: null };

    (component as any).buildForm();

    expect(component.form.value).toEqual(formExpected);
  });

  it('should call buildForm and initializeFormListener methods when ngOnInit hook is called', () => {
    const buildFormSpy = spyOn<any>(component, 'buildForm');
    const initializeFormListenerSpy = spyOn<any>(component, 'initializeFormListener');
    component.ngOnInit();

    expect(buildFormSpy).toHaveBeenCalled();
    expect(initializeFormListenerSpy).toHaveBeenCalled();
  });

  it('should set isDisabled property value of requestButton when form statusChangues listener changed', fakeAsync(() => {
    spyOn(component.form.statusChanges, 'pipe').and.returnValue(of('INVALID'));
    const requestButton: ModalAction = {
      type: 'button',
      isDisabled: true
    };
    component['initializeFormListener']();

    component.form.statusChanges.pipe(take(1)).subscribe(status => {
      tick();
      const expectedStatus = status === 'INVALID';
      expect(requestButton.isDisabled).toBe(expectedStatus);
    });
  }));

  describe('initializeDialogData', () => {
    it('should set bspDropdownOptions', () => {
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          bsps: [{ isoCountryCode: 'ES', id: 1, name: 'Spain' }]
        }
      };
      const expectedRes = [{ value: { isoCountryCode: 'ES', id: 1, name: 'Spain' }, label: 'ES - Spain' }];
      component.bspDropdownOptions = [];

      component['initializeDialogData']();

      expect(component.bspDropdownOptions).toEqual(expectedRes);
    });
  });

  describe('buildForm', () => {
    it('should set form', () => {
      component.form = null;

      component['buildForm']();

      expect(component.form.value).toEqual({ bsp: null });
    });
  });

  describe('initializeFormListener', () => {
    it('should set isDisable to request button', () => {
      const requestButton = { isDisabled: true, type: FooterButton.Request };
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [requestButton]
        }
      };
      component['buildForm']();
      component['initializeFormListener']();

      component.form.setValue({ bsp: { isoCountryCode: 'ES', id: 1, name: 'Spain' } });
      expect(requestButton.isDisabled).toBe(false);

      component.form.setValue({ bsp: null });
      expect(requestButton.isDisabled).toBe(true);
    });
  });
});
