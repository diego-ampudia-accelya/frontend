import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import FileSaver from 'file-saver';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { createAdmAcmService } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DialogConfig } from '~app/shared/components';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';

@Component({
  selector: 'bspl-adm-acm-activity-history-dialog',
  templateUrl: './adm-acm-activity-history-dialog.component.html',
  styleUrls: ['./adm-acm-activity-history-dialog.component.scss']
})
export class AdmAcmActivityHistoryDialogComponent implements OnInit, OnDestroy {
  public activities$: Observable<Activity[]>;
  public acdmService: AdmAcmService;

  private _loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public isLoading$: Observable<boolean> = this._loadingSubject.asObservable();

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private dialog: DialogService,
    private reactiveSubject: ReactiveSubject,
    private http: HttpClient
  ) {
    this.initializeDialogData();
  }

  public ngOnInit(): void {
    this.initializeButtonClickListener();
    this.initializeActivities();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public onDownloadFile(fileUrl: string): void {
    this.acdmService.downloadFile(fileUrl).subscribe(file => {
      FileSaver.saveAs(file.blob, file.fileName);
    });
  }

  private initializeDialogData(): void {
    const admAcmType: MasterDataType = this.config.data.acdmType;
    this.acdmService = createAdmAcmService(this.http, { data: { admAcmType } } as any);
  }

  private initializeActivities(): void {
    const acdm: AdmAcmIssueBE = this.config.data.acdm;
    this.activities$ = this.acdmService.getActivities(acdm.id).pipe(finalize(() => this._loadingSubject.next(false)));
  }

  private initializeButtonClickListener(): void {
    this.reactiveSubject.asObservable.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.dialog.close();
    });
  }
}
