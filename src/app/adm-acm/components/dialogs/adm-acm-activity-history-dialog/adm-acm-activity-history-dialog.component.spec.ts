import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { first, take } from 'rxjs/operators';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AdmAcmActivityHistoryDialogComponent } from './adm-acm-activity-history-dialog.component';

const mockConfig = {
  data: {
    acdm: {},
    footerButtonsType: FooterButton.Close,
    rowTableContent: {},
    hasCancelButton: false,
    isClosable: false,
    dataType: MasterDataType.Adm
  }
};

describe('AdmAcmActivityHistoryDialogComponent', () => {
  let component: AdmAcmActivityHistoryDialogComponent;
  let fixture: ComponentFixture<AdmAcmActivityHistoryDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdmAcmActivityHistoryDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: DialogService, useValue: { close: jasmine.createSpy() } },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: ReactiveSubject, useValue: { asObservable: of({}) } },
        HttpClient
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmActivityHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onDownloadFile', () => {
    it('should call service to download file', fakeAsync(() => {
      const downloadFileSpy = spyOn(component.acdmService, 'downloadFile').and.returnValue(of(null)).and.callThrough();
      component.onDownloadFile('url');
      tick();

      expect(downloadFileSpy).toHaveBeenCalled();
    }));
  });

  describe('initializeDialogData', () => {
    it('should set acdmService', () => {
      (component as any).config = {
        ...(component as any).config,
        data: {
          ...(component as any).config.data,
          acdmType: MasterDataType.Acm
        }
      };
      component.acdmService = null;

      component['initializeDialogData']();

      expect(component.acdmService).not.toBeNull();
    });
  });

  describe('initializeActivities', () => {
    it('should call service to get activity', fakeAsync(() => {
      const activitySpy = spyOn(component.acdmService, 'getActivities').and.returnValue(of(null)).and.callThrough();
      component['initializeActivities']();
      tick();

      expect(activitySpy).toHaveBeenCalled();
    }));

    it('should set false to _loadingSubject', fakeAsync(() => {
      spyOn(component.acdmService, 'getActivities').and.returnValue(of(null));
      let res;
      component['_loadingSubject'].next(true);

      component['initializeActivities']();
      component.isLoading$.pipe(take(2)).subscribe(r => (res = r));
      component.activities$.pipe(first()).subscribe();
      tick(5);

      expect(res).toBe(false);
    }));
  });

  describe('initializeButtonClickListener', () => {
    it('should call close dialog function', fakeAsync(() => {
      component['dialog'].close = jasmine.createSpy();

      component['initializeButtonClickListener']();
      tick();

      expect(component['dialog'].close).toHaveBeenCalled();
    }));
  });
});
