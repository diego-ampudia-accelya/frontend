import { Component, OnDestroy, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ResponseErrorBE, ResponseErrorMessageBE } from '~app/shared/models';

@Component({
  selector: 'bspl-adm-acm-error-dialog',
  templateUrl: './adm-acm-error-dialog.component.html',
  styleUrls: ['./adm-acm-error-dialog.component.scss']
})
export class AdmAcmErrorDialogComponent implements OnInit, OnDestroy {
  public errorMessages: ResponseErrorMessageBE[];

  private dialogInjectedData;
  private admAcmType: MasterDataType;

  private destroy$ = new Subject();

  constructor(
    private dialog: DialogService,
    private config: DialogConfig,
    private translationService: L10nTranslationService,
    private reactiveSubject: ReactiveSubject
  ) {
    this.initializeDialogData();
  }

  public ngOnInit(): void {
    this.initializeButtonClickListener();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeDialogData(): void {
    this.dialogInjectedData = this.config.data;
    this.admAcmType = this.dialogInjectedData.dataType;

    const translatedType = this.translationService.translate(`ADM_ACM.type.${this.admAcmType}`);
    this.dialogInjectedData.title = this.translationService.translate('ADM_ACM.error.dialog.title', {
      type: translatedType
    });

    const rowData: ResponseErrorBE = this.dialogInjectedData.rowTableContent;
    this.errorMessages = rowData.messages;
  }

  private initializeButtonClickListener(): void {
    this.reactiveSubject.asObservable.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.dialog.close();
    });
  }
}
