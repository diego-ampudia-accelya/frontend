import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject } from 'rxjs';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AdmAcmErrorDialogComponent } from './adm-acm-error-dialog.component';

const mockConfig = {
  data: {
    footerButtonsType: FooterButton.Ok,
    rowTableContent: { messages: [{ messageCode: 403, message: 'Forbidden' }] },
    hasCancelButton: false,
    isClosable: false,
    dataType: MasterDataType.Adm
  }
};

describe('AdmAcmErrorDialogComponent', () => {
  let component: AdmAcmErrorDialogComponent;
  let fixture: ComponentFixture<AdmAcmErrorDialogComponent>;
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const dialogServiceSpy = createSpyObject(DialogService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmErrorDialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        },
        { provide: DialogService, useCValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmErrorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('initializeDialogData', () => {
    beforeEach(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-title');
    });

    it('should set dialogInjectedData', () => {
      component['dialogInjectedData'] = null;

      component['initializeDialogData']();

      expect(component['dialogInjectedData']).toEqual(mockConfig.data);
    });

    it('should set admAcmType', () => {
      component['admAcmType'] = null;

      component['initializeDialogData']();

      expect(component['admAcmType']).toEqual(MasterDataType.Adm);
    });

    it('should call translate', () => {
      component['initializeDialogData']();

      expect(translationServiceSpy.translate).toHaveBeenCalledTimes(2);
      expect(translationServiceSpy.translate.calls.first().args).toEqual(['ADM_ACM.type.adm']);
      expect(translationServiceSpy.translate.calls.mostRecent().args).toEqual([
        'ADM_ACM.error.dialog.title',
        { type: 'test-title' }
      ]);
    });

    it('should set title for dialogInjectedData', () => {
      component['dialogInjectedData'] = null;

      component['initializeDialogData']();

      expect(component['dialogInjectedData'].title).toEqual('test-title');
    });

    it('should set admAcmType', () => {
      component.errorMessages = [];

      component['initializeDialogData']();

      expect(component.errorMessages).toEqual([{ messageCode: 403, message: 'Forbidden' }] as any);
    });
  });

  describe('initializeButtonClickListener', () => {
    it('should call close', fakeAsync(() => {
      component['dialog'].close = jasmine.createSpy();
      (component as any).reactiveSubject.asObservable = new BehaviorSubject(1);

      component['initializeButtonClickListener']();
      (component['reactiveSubject'].asObservable as any).next();
      tick(2);

      expect(component['dialog'].close).toHaveBeenCalled();
    }));
  });
});
