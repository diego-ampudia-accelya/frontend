import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import {
  AcmAdmRuleDetail,
  AcmAdmRuleDetailApplyChangeViewModel,
  AdmAcmRule,
  StatusRuleType
} from '~app/adm-acm/models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services/notification.service';
import { CATEGORIZATION_RULE_DEACTIVATE_OPTIONS } from './adm-acm-categorization-rules-apply-changes-dialog.constants';

@Component({
  selector: 'bspl-adm-acm-categorization-rules-apply-changes-dialog',
  templateUrl: './adm-acm-categorization-rules-apply-changes-dialog.component.html',
  styleUrls: ['./adm-acm-categorization-rules-apply-changes-dialog.component.scss']
})
export class AdmAcmCategorizationRulesApplyChangesDialogComponent implements AfterViewInit, OnInit, OnDestroy {
  public form: FormGroup;
  public radioButtonOptions: { value: boolean; label: string }[];
  public isDeactivateMode: boolean;
  public pills: { value: string; tooltip: string }[];

  private deactivationTypeControl: AbstractControl;
  private changes: Partial<AcmAdmRuleDetail>;
  private itemToSend: AcmAdmRuleDetail;
  private ruleId: number;

  private formFactory: FormUtil;
  private applyChangeButton: ModalAction;

  private actionEmitter = new Subject();
  private destroy$ = new Subject();

  private applyChangesButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Apply),
    takeUntil(this.destroy$)
  );

  constructor(
    private config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private admAcmCategorizationRulesService: AdmAcmCategorizationRulesService,
    private fb: FormBuilder
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.buildForm();
    this.initializeFormFeatures();
    this.initializeConfigFeatures();
    this.initializeRadioButtonValue();

    this.initializeButtons();
    this.deactivateButtonListener();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.applyChangeButton) {
        this.applyChangeButton.isDisabled = status !== 'VALID';
      }
    });
  }

  private initializeButtons(): void {
    this.applyChangeButton = this.config.data.buttons.find(button => button.type === FooterButton.Apply);
    if (this.applyChangeButton) {
      this.applyChangeButton.isDisabled = this.isDeactivateMode;
    }
  }

  private initializeFormFeatures(): void {
    this.deactivationTypeControl = FormUtil.get<AcmAdmRuleDetailApplyChangeViewModel>(this.form, 'deactivationType');
  }

  private initializeConfigFeatures(): void {
    this.actionEmitter = this.config.data.actionEmitter;
    this.changes = this.config.changes;
    this.itemToSend = this.config.itemToSend;
    this.pills = this.formatPill();
    this.ruleId = this.config.ruleId;

    this.isDeactivateMode = this.config.changes.status === Object.keys(StatusRuleType)[1];
    if (this.isDeactivateMode) {
      this.deactivationTypeControl.setValidators(Validators.required);
    }
  }

  private formatPill(): { value: string; tooltip: string }[] {
    const keys: string[] = Object.keys(this.changes);

    return keys.map(key => {
      const keyTranslated = this.translationService.translate(`ADM_ACM.categorizationRuleDetail.pills.${key}`);
      const value = `${keyTranslated}:  ${this.formatPillValue(key)}`;

      return {
        value,
        tooltip: value
      };
    });
  }

  private formatPillValue(key: string): string {
    let value = this.changes[key];

    switch (key as keyof AcmAdmRuleDetail) {
      case 'airlineCode':
        value = `${this.changes[key].code} / ${this.changes[key].name}`;
        break;
      case 'isoCountryCodes':
        value = this.changes[key].map(bsp => bsp.isoCountryCode).join(', ');
        break;
      case 'status':
        value = this.translationService.translate(
          `ADM_ACM.categorizationRules.filters.status.options.${this.changes[key]}`
        );
        break;
    }

    return value;
  }

  private initializeRadioButtonValue(): void {
    this.radioButtonOptions = CATEGORIZATION_RULE_DEACTIVATE_OPTIONS.RADIO_BUTTON_OPTIONS;
  }

  private deactivateButtonListener(): void {
    this.applyChangesButtonClick$
      .pipe(
        switchMap(() => this.requestUpdate()),
        takeUntil(this.destroy$)
      )
      .subscribe(
        () => {
          this.dialogService.close();
          this.actionEmitter.next();
          const translateMessage = 'ADM_ACM.categorizationRuleDetail.successEditMessage';

          this.notificationService.showSuccess(this.translationService.translate(translateMessage));
        },
        error => {
          this.actionEmitter.next(error);
          this.dialogService.close();
        }
      );
  }

  private requestUpdate(): Observable<AdmAcmRule> {
    const deactivateTypeValue = this.deactivationTypeControl.value;

    return !this.isDeactivateMode || !deactivateTypeValue
      ? this.admAcmCategorizationRulesService.update(this.itemToSend, this.ruleId)
      : this.admAcmCategorizationRulesService.rerun({ active: false }, this.ruleId);
  }

  private buildForm(): void {
    this.form = this.formFactory.createGroup<AcmAdmRuleDetailApplyChangeViewModel>({
      deactivationType: []
    });
  }
}
