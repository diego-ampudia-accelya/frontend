import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormControl } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, of, Subject, throwError } from 'rxjs';
import { take } from 'rxjs/operators';
import { AdmAcmCategorizationRulesService } from '~app/adm-acm/services/adm-acm-categorization-rules.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { AdmAcmCategorizationRulesApplyChangesDialogComponent } from './adm-acm-categorization-rules-apply-changes-dialog.component';
import { CATEGORIZATION_RULE_DEACTIVATE_OPTIONS } from './adm-acm-categorization-rules-apply-changes-dialog.constants';

const mockConfig = {
  data: {
    title: 'title',
    actionEmitter: new Subject(),
    footerButtonsType: FooterButton.Deactivate,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.RERUN',
        buttonDesign: 'tertiary',
        type: 'apply'
      }
    ]
  },
  changes: { name: 'new', status: 'ACTIVE' },
  itemToSend: {
    id: '001',
    primaryReason: 'primary Reason',
    primaryReasonId: '003',
    subReason: 'sub Reason',
    active: true
  },
  ruleId: 5
} as DialogConfig;

describe('AdmAcmCategorizationRulesApplyChangesDialogComponent', () => {
  let component: AdmAcmCategorizationRulesApplyChangesDialogComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationRulesApplyChangesDialogComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let admAcmCategorizationRulesServiceSpy: SpyObject<AdmAcmCategorizationRulesService>;
  const notificationServiceSpy = createSpyObject(NotificationService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    admAcmCategorizationRulesServiceSpy = createSpyObject(AdmAcmCategorizationRulesService);

    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationRulesApplyChangesDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        FormBuilder,
        ReactiveSubject,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: AdmAcmCategorizationRulesService, useValue: admAcmCategorizationRulesServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationRulesApplyChangesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('initializeButtons', () => {
    it('should set isDisable for apply button to true when isDeactivateMode is true', () => {
      const requestButton = { isDisabled: false, type: FooterButton.Apply };
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [requestButton]
        }
      };
      component.isDeactivateMode = true;

      component['initializeButtons']();

      expect(requestButton.isDisabled).toBe(true);
    });

    it('should set isDisable for apply button to false when isDeactivateMode is false', () => {
      const requestButton = { isDisabled: true, type: FooterButton.Apply };
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [requestButton]
        }
      };
      component.isDeactivateMode = false;

      component['initializeButtons']();

      expect(requestButton.isDisabled).toBe(false);
    });
  });

  describe('initializeFormFeatures', () => {
    it('should set deactivation type control', () => {
      component['deactivationTypeControl'] = null;

      component['initializeFormFeatures']();

      expect(component['deactivationTypeControl']).toEqual(component.form.controls.deactivationType);
    });
  });

  describe('initializeConfigFeatures', () => {
    it('should set actionEmitter', () => {
      const emitter = new Subject();
      component['actionEmitter'] = null;
      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          actionEmitter: emitter
        }
      };

      component['initializeConfigFeatures']();

      expect(component['actionEmitter']).toEqual(emitter);
    });

    it('should set changes', () => {
      const changes = [{ test: 1 }];
      component['changes'] = null;
      component['config'] = {
        ...component['config'],
        changes
      };

      component['initializeConfigFeatures']();

      expect(component['changes']).toEqual([{ test: 1 }] as any);
    });

    it('should set itemToSend', () => {
      const itemToSend = { id: 1 };
      component['itemToSend'] = null;
      component['config'] = {
        ...component['config'],
        itemToSend
      };

      component['initializeConfigFeatures']();

      expect(component['itemToSend']).toEqual({ id: 1 } as any);
    });

    it('should set pills', () => {
      component['pills'] = null;
      component['formatPill'] = jasmine.createSpy().and.returnValue([{ value: 'test-value', tooltip: 'test-tooltip' }]);

      component['initializeConfigFeatures']();

      expect(component['pills']).toEqual([{ value: 'test-value', tooltip: 'test-tooltip' }]);
    });

    it('should set ruleId', () => {
      component['ruleId'] = null;
      component['config'] = {
        ...component['config'],
        ruleId: 5
      };

      component['initializeConfigFeatures']();

      expect(component['ruleId']).toBe(5);
    });

    it('should set isDeactivateMode to true', () => {
      component['isDeactivateMode'] = null;
      component['config'] = {
        ...component['config'],
        changes: {
          status: 'NON_ACTIVE'
        }
      };

      component['initializeConfigFeatures']();

      expect(component['isDeactivateMode']).toBe(true);
    });

    it('should set isDeactivateMode to false', () => {
      component['isDeactivateMode'] = null;
      component['config'] = {
        ...component['config'],
        changes: {
          status: 'ACTIVE'
        }
      };

      component['initializeConfigFeatures']();

      expect(component['isDeactivateMode']).toBe(false);
    });
  });

  describe('formatPill', () => {
    beforeEach(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-key');
      component['formatPillValue'] = jasmine.createSpy().and.returnValue('test-value');
    });

    it('should return mapped changes', () => {
      component['changes'] = { id: 1 };
      const expectedRes = [{ value: 'test-key:  test-value', tooltip: 'test-key:  test-value' }];

      const res = component['formatPill']();

      expect(res).toEqual(expectedRes);
    });

    it('should call translate', () => {
      component['changes'] = { id: 1 };

      component['formatPill']();

      expect(component['translationService'].translate).toHaveBeenCalledWith(
        'ADM_ACM.categorizationRuleDetail.pills.id'
      );
    });
  });

  describe('formatPillValue', () => {
    beforeEach(() => {
      component['changes'] = {
        airlineCode: { name: 'test-airline', code: '000' },
        isoCountryCodes: [{ isoCountryCode: '1111' }, { isoCountryCode: '2222' }],
        status: 'test-status',
        primaryReasonId: '55'
      } as any;
    });

    it('should return value with airline code and name', () => {
      const res = component['formatPillValue']('airlineCode');

      expect(res).toBe('000 / test-airline');
    });

    it('should return value with bsps codes', () => {
      const res = component['formatPillValue']('isoCountryCodes');

      expect(res).toBe('1111, 2222');
    });

    it('should return value with translated status', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-value');

      const res = component['formatPillValue']('status');

      expect(res).toBe('test-value');
    });

    it('should call translate status', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-value');

      component['formatPillValue']('status');

      expect(component['translationService'].translate).toHaveBeenCalledWith(
        'ADM_ACM.categorizationRules.filters.status.options.test-status'
      );
    });

    it('should return value with primaryReasonId', () => {
      const res = component['formatPillValue']('primaryReasonId');

      expect(res).toBe('55');
    });
  });

  describe('initializeRadioButtonValue', () => {
    it('should set radioButtonOptions', () => {
      component.radioButtonOptions = null;

      component['initializeRadioButtonValue']();

      expect(component.radioButtonOptions).toEqual(CATEGORIZATION_RULE_DEACTIVATE_OPTIONS.RADIO_BUTTON_OPTIONS);
    });
  });

  describe('deactivateButtonListener', () => {
    beforeEach(() => {
      component['applyChangesButtonClick$'] = new BehaviorSubject(1);
    });

    describe('success', () => {
      const subject = new BehaviorSubject(1);
      beforeEach(() => {
        component['requestUpdate'] = jasmine.createSpy().and.returnValue(subject);
      });

      it('should call requestUpdate', fakeAsync(() => {
        component['deactivateButtonListener']();
        tick();

        expect(component['requestUpdate']).toHaveBeenCalled();
      }));

      it('should call dialog close', fakeAsync(() => {
        component['dialogService'].close = jasmine.createSpy();

        component['deactivateButtonListener']();
        tick();

        expect(component['dialogService'].close).toHaveBeenCalled();
      }));

      it('should call action emit', fakeAsync(() => {
        component['actionEmitter'].next = jasmine.createSpy();

        component['deactivateButtonListener']();
        tick();

        expect(component['actionEmitter'].next).toHaveBeenCalled();
      }));

      it('should call translate', fakeAsync(() => {
        component['translationService'].translate = jasmine.createSpy().and.returnValue('success');

        component['deactivateButtonListener']();
        tick();

        expect(component['translationService'].translate).toHaveBeenCalledWith(
          'ADM_ACM.categorizationRuleDetail.successEditMessage'
        );
      }));

      it('should call show success', fakeAsync(() => {
        component['notificationService'].showSuccess = jasmine.createSpy();
        component['translationService'].translate = jasmine.createSpy().and.returnValue('success');

        component['deactivateButtonListener']();
        tick();

        expect(component['notificationService'].showSuccess).toHaveBeenCalledWith('success');
      }));
    });

    describe('error', () => {
      beforeEach(() => {
        component['requestUpdate'] = jasmine.createSpy().and.returnValue(throwError({ code: 500 }));
        component['handleErrorsOnSubmit'] = jasmine.createSpy();
      });

      it('should call action emit', fakeAsync(() => {
        component['actionEmitter'].next = jasmine.createSpy();

        component['deactivateButtonListener']();
        tick();

        expect(component['actionEmitter'].next).toHaveBeenCalled();
      }));

      it('should call dialog close', fakeAsync(() => {
        component['dialogService'].close = jasmine.createSpy();

        component['deactivateButtonListener']();
        tick();

        expect(component['dialogService'].close).toHaveBeenCalled();
      }));
    });
  });

  describe('requestUpdate', () => {
    beforeEach(() => {
      component['admAcmCategorizationRulesService'].update = jasmine
        .createSpy()
        .and.returnValue(of({ id: 1, name: 'test-1' }));
      component['admAcmCategorizationRulesService'].rerun = jasmine
        .createSpy()
        .and.returnValue(of({ id: 2, name: 'test-2' }));
      component['deactivationTypeControl'] = new FormControl();
    });

    it('should call update when it is NOT deactivation', fakeAsync(() => {
      component.isDeactivateMode = false;
      component['itemToSend'] = { name: 'test-1' } as any;
      component['ruleId'] = 1;

      component['requestUpdate']().pipe(take(1)).subscribe();
      tick();

      expect(component['admAcmCategorizationRulesService'].update).toHaveBeenCalledWith({ name: 'test-1' }, 1);
    }));

    it('should return updated when it is NOT deactivation', fakeAsync(() => {
      component.isDeactivateMode = false;
      let res;

      component['requestUpdate']()
        .pipe(take(1))
        .subscribe(r => (res = r));
      tick();

      expect(res).toEqual({ id: 1, name: 'test-1' });
    }));

    it('should call update when it is deactivation and NO deactivateTypeValue', fakeAsync(() => {
      component.isDeactivateMode = true;
      component['deactivationTypeControl'].setValue(null);
      component['itemToSend'] = { name: 'test-1' } as any;
      component['ruleId'] = 1;

      component['requestUpdate']().pipe(take(1)).subscribe();
      tick();

      expect(component['admAcmCategorizationRulesService'].update).toHaveBeenCalledWith({ name: 'test-1' }, 1);
    }));

    it('should return updated when it is deactivation and NO deactivateTypeValue', fakeAsync(() => {
      component.isDeactivateMode = true;
      component['deactivationTypeControl'].setValue(null);
      let res;

      component['requestUpdate']()
        .pipe(take(1))
        .subscribe(r => (res = r));
      tick();

      expect(res).toEqual({ id: 1, name: 'test-1' });
    }));

    it('should call rerun when it is deactivation and deactivateTypeValue exists', fakeAsync(() => {
      component.isDeactivateMode = true;
      component['deactivationTypeControl'].setValue({ deactivationType: true });
      component['itemToSend'] = { name: 'test-1' } as any;
      component['ruleId'] = 1;

      component['requestUpdate']().pipe(take(1)).subscribe();
      tick();

      expect(component['admAcmCategorizationRulesService'].rerun).toHaveBeenCalledWith({ active: false }, 1);
    }));

    it('should return rerun value when it is deactivation and deactivateTypeValue exists', fakeAsync(() => {
      component.isDeactivateMode = true;
      component['deactivationTypeControl'].setValue({ deactivationType: true });
      let res;

      component['requestUpdate']()
        .pipe(take(1))
        .subscribe(r => (res = r));
      tick();

      expect(res).toEqual({ id: 2, name: 'test-2' });
    }));
  });

  describe('buildForm', () => {
    it('shoul set form', () => {
      component['form'] = null;

      component['buildForm']();

      expect(component['form'].value).toEqual({ deactivationType: null });
    });
  });
});
