import { AllOption } from '~app/adm-acm/models/adm-acm-reason.model';

export const CATEGORIZATION_REASONS_RERUN_CONSTANTS = {
  ALL_OPTIONS_BSP_VALUE: {
    id: 0,
    isoCountryCode: AllOption.VALUE,
    name: AllOption.LABEL
  }
};
