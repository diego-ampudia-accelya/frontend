import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import {
  AdmAcmReasons,
  AdmAcmReasonsRerunClearanceDialogViewModel,
  AllOption
} from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { lastDayOfMonthToShortIsoDate, toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { AirlineSummary } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { AirlineDictionaryService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { CATEGORIZATION_REASONS_RERUN_CONSTANTS } from './adm-acm-categorization-reasons-rerun-clearance-dialog.constants';

@Component({
  selector: 'bspl-adm-acm-categorization-reasons-rerun-clearance-dialog',
  templateUrl: './adm-acm-categorization-reasons-rerun-clearance-dialog.component.html',
  styleUrls: ['./adm-acm-categorization-reasons-rerun-clearance-dialog.component.scss']
})
export class AdmAcmCategorizationReasonsRerunClearanceDialogComponent implements AfterViewInit, OnInit, OnDestroy {
  public form: FormGroup;

  public userBspOptions$: Observable<DropdownOption<BspDto>[]>;
  public airlineDropdownOptions: DropdownOption<AirlineSummary>[];

  public infoText: string;

  public allBspFilteringOption = {
    value: CATEGORIZATION_REASONS_RERUN_CONSTANTS.ALL_OPTIONS_BSP_VALUE,
    label: this.getAllOptionTranslation()
  };

  private formUtil: FormUtil;
  private airlineControl: AbstractControl;

  private reRunButton: ModalAction;
  private clearanceButton: ModalAction;

  private destroy$ = new Subject();

  private isClearanceMode = this.config.data.isClearanceMode;

  private reRunButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Rerun),
    takeUntil(this.destroy$)
  );

  private clearanceButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Clearance),
    takeUntil(this.destroy$)
  );

  constructor(
    private formBuilder: FormBuilder,
    private config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private bspsDictionaryService: BspsDictionaryService,
    private admAcmCategorizationReasonsService: AdmAcmCategorizationReasonsService,
    private airlineDictionaryService: AirlineDictionaryService,
    private notificationService: NotificationService
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.createForm();
    this.initializeInfoText();
    this.initializeFormFeatures();
    this.initializeUserBspOptions();
    this.setAirlineOptions();
    this.onBspChange().subscribe();
    this.initializeButtons();
    this.initializeButtonsListeners();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.reRunButton) {
        this.reRunButton.isDisabled = status !== 'VALID';
      }
      if (this.clearanceButton) {
        this.clearanceButton.isDisabled = status !== 'VALID';
      }
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private onBspChange(): Observable<any> {
    return FormUtil.get<AdmAcmReasonsRerunClearanceDialogViewModel>(this.form, 'bsp').valueChanges.pipe(
      tap(bsp => {
        if (bsp?.length) {
          this.modifyAirlineDropDownByBsp(bsp);
        }

        this.airlineControl.reset();
      }),
      takeUntil(this.destroy$)
    );
  }

  private initializeInfoText(): void {
    const clearanceMessage = 'ADM_ACM.categorizationReasons.dialog.clearanceMessage';
    const rerunMessage = 'ADM_ACM.categorizationReasons.dialog.rerunMessage';
    const message = this.isClearanceMode ? clearanceMessage : rerunMessage;

    this.infoText = this.translationService.translate(message);
  }

  private initializeFormFeatures(): void {
    this.airlineControl = FormUtil.get<AdmAcmReasonsRerunClearanceDialogViewModel>(this.form, 'airline');
  }

  private initializeButtons(): void {
    this.reRunButton = this.config.data.buttons.find(button => button.type === FooterButton.Rerun);
    if (this.reRunButton) {
      this.reRunButton.isDisabled = true;
    }

    this.clearanceButton = this.config.data.buttons.find(button => button.type === FooterButton.Clearance);
    if (this.clearanceButton) {
      this.clearanceButton.isDisabled = true;
    }
  }

  private initializeButtonsListeners(): void {
    this.reRunButtonListener();
    this.clearanceButtonlistener();
  }

  private reRunButtonListener(): void {
    this.reRunButtonClick$.pipe(switchMap(() => this.requestRerun())).subscribe(() => {
      const translateMessage = 'ADM_ACM.categorizationReasons.dialog.rerunSuccessMessage';

      this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      this.dialogService.close();
    });
  }

  private clearanceButtonlistener(): void {
    this.clearanceButtonClick$.pipe(switchMap(() => this.requestRerun())).subscribe(() => {
      const translateMessage = 'ADM_ACM.categorizationReasons.dialog.clearanceSuccessMessage';

      this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      this.dialogService.close();
    });
  }

  private requestRerun(): Observable<AdmAcmReasons> {
    const issueDateControl = FormUtil.get<AdmAcmReasonsRerunClearanceDialogViewModel>(this.form, 'issueDate');
    const bspControlValue = FormUtil.get<AdmAcmReasonsRerunClearanceDialogViewModel>(this.form, 'bsp').value;
    const initialDate = issueDateControl.value && issueDateControl.value[0];
    const finalDate = issueDateControl.value && issueDateControl.value[1];
    const airlineCodeValue = this.airlineControl.value?.code;
    const isAllSelected = bspControlValue.some(bsp => bsp.isoCountryCode === AllOption.VALUE);
    const itemId = this.config.data.item.id;

    const itemToRequest = {
      active: !this.isClearanceMode,
      airlineCode: airlineCodeValue !== AllOption.VALUE ? airlineCodeValue : null,
      bspIds: isAllSelected ? null : bspControlValue.map(bsp => bsp.id),
      fromIssueDate: initialDate && toShortIsoDate(initialDate),
      toIssueDate: finalDate && lastDayOfMonthToShortIsoDate(finalDate)
    };

    return this.admAcmCategorizationReasonsService.rerun(itemToRequest, itemId);
  }

  private modifyAirlineDropDownByBsp(selectedBsp: Bsp[]): void {
    let bspId: number;

    if (selectedBsp.length === 1 && selectedBsp[0].isoCountryCode !== AllOption.VALUE) {
      bspId = selectedBsp[0].id;
    }

    this.setAirlineOptions(bspId);
  }

  private createForm(): void {
    this.form = this.formUtil.createGroup<AdmAcmReasonsRerunClearanceDialogViewModel>({
      bsp: [null, Validators.required],
      airline: [null, Validators.required],
      issueDate: []
    });
  }

  private initializeUserBspOptions(): void {
    this.userBspOptions$ = this.bspsDictionaryService.getAllBspDropdownOptions().pipe(takeUntil(this.destroy$));
  }

  private setAirlineOptions(bspId?: number): void {
    this.airlineDictionaryService
      .getDropdownOptions(bspId)
      .pipe(takeUntil(this.destroy$))
      .subscribe(airlines => (this.airlineDropdownOptions = this.addAllOptionToAirlines(airlines)));
  }

  private addAllOptionToAirlines(
    airlines: DropdownOption<AirlineSummary>[]
  ): Partial<DropdownOption<AirlineSummary>[]> {
    return [
      {
        value: { id: 0, name: AllOption.LABEL, code: AllOption.VALUE, designator: AllOption.LABEL },
        label: this.getAllOptionTranslation()
      },
      ...airlines
    ];
  }

  private getAllOptionTranslation(): string {
    return this.translationService.translate(`dropdown.${AllOption.LABEL}`);
  }
}
