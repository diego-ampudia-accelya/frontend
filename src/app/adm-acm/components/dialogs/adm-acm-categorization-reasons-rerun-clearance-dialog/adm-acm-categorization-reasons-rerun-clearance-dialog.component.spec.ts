import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AdmAcmReasonsRerunClearanceDialogViewModel, AllOption } from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { AirlineDictionaryService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { TranslatePipeMock } from '~app/test';
import { AdmAcmCategorizationReasonsRerunClearanceDialogComponent } from './adm-acm-categorization-reasons-rerun-clearance-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Deactivate,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.RERUN',
        buttonDesign: 'tertiary',
        type: 'rerun'
      },
      {
        title: 'BUTTON.DEFAULT.CLEARANCE',
        buttonDesign: 'primary',
        type: 'clearance',
        isDisabled: true
      }
    ],
    item: {
      id: '001',
      primaryReason: 'primary Reason',
      primaryReasonId: '003',
      subReason: 'sub Reason',
      active: true
    }
  }
} as DialogConfig;

describe('AdmAcmCategorizationReasonsRerunClearanceDialogComponent', () => {
  let component: AdmAcmCategorizationReasonsRerunClearanceDialogComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationReasonsRerunClearanceDialogComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let admAcmCategorizationReasonsServiceSpy: SpyObject<AdmAcmCategorizationReasonsService>;
  const notificationServiceSpy = createSpyObject(NotificationService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    admAcmCategorizationReasonsServiceSpy = createSpyObject(AdmAcmCategorizationReasonsService);

    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationReasonsRerunClearanceDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        FormBuilder,
        ReactiveSubject,
        {
          provide: BspsDictionaryService,
          useValue: { getAllBspDropdownOptions: jasmine.createSpy().and.returnValue(of([])) }
        },
        {
          provide: AirlineDictionaryService,
          useValue: { getDropdownOptions: jasmine.createSpy().and.returnValue(of([])) }
        },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: AdmAcmCategorizationReasonsService, useValue: admAcmCategorizationReasonsServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationReasonsRerunClearanceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('onBspChange', () => {
    beforeEach(() => {
      component['modifyAirlineDropDownByBsp'] = jasmine.createSpy();
      component['createForm']();
      component['initializeFormFeatures']();
    });

    it('should call modifyAirlineDropDownByBsp with bsps', fakeAsync(() => {
      component['onBspChange']().pipe(take(1)).subscribe();
      component.form.controls.bsp.setValue([{ id: 1, isoCountryCode: 'ES', name: 'Spain' }]);
      tick();

      expect(component['modifyAirlineDropDownByBsp']).toHaveBeenCalledWith([
        { id: 1, isoCountryCode: 'ES', name: 'Spain' }
      ]);
    }));

    it('should NOT call modifyAirlineDropDownByBsp when bsps are empty', fakeAsync(() => {
      component.form.controls.bsp.setValue([{ id: 1, isoCountryCode: 'ES', name: 'Spain' }]);

      component['onBspChange']().pipe(take(1)).subscribe();
      component.form.controls.bsp.setValue([]);
      tick();

      expect(component['modifyAirlineDropDownByBsp']).not.toHaveBeenCalled();
    }));

    it('should reset airline control', fakeAsync(() => {
      component['airlineControl'].setValue({
        id: 2,
        name: 'test-airline',
        code: 'test-code',
        designator: 'test-designator'
      });
      expect(component['airlineControl'].value).not.toBeNull();

      component['onBspChange']().pipe(take(1)).subscribe();
      component.form.controls.bsp.setValue([{ id: 1, isoCountryCode: 'ES', name: 'Spain' }]);
      tick();

      expect(component['airlineControl'].value).toBeNull();
    }));
  });

  describe('initializeInfoText', () => {
    it('should call translate with clearance message', () => {
      component['isClearanceMode'] = true;

      component['initializeInfoText']();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(
        'ADM_ACM.categorizationReasons.dialog.clearanceMessage'
      );
    });

    it('should call translate with rerun message', () => {
      component['isClearanceMode'] = false;

      component['initializeInfoText']();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith('ADM_ACM.categorizationReasons.dialog.rerunMessage');
    });

    it('should set info text as a result of translation', () => {
      component['isClearanceMode'] = false;
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-info-text');
      component.infoText = '';

      component['initializeInfoText']();

      expect(component.infoText).toBe('test-info-text');
    });
  });

  describe('initializeFormFeatures', () => {
    it('should set airline control', () => {
      component['airlineControl'] = null;

      component['initializeFormFeatures']();

      expect(component['airlineControl']).toEqual(component.form.controls.airline);
    });
  });

  describe('initializeButtons', () => {
    let reRunButton;
    let clearanceButton;

    beforeEach(() => {
      reRunButton = { type: FooterButton.Rerun, isDisabled: false };
      clearanceButton = { type: FooterButton.Clearance, isDisabled: false };

      component['config'] = {
        ...component['config'],
        data: {
          ...component['config'].data,
          buttons: [reRunButton, clearanceButton]
        }
      };
    });

    it('should set is disabled to reRunButton', () => {
      component['initializeButtons']();

      expect(reRunButton.isDisabled).toBe(true);
    });

    it('should set is disabled to clearanceButton', () => {
      component['initializeButtons']();

      expect(clearanceButton.isDisabled).toBe(true);
    });
  });

  describe('initializeButtonsListeners', () => {
    beforeEach(() => {
      component['reRunButtonListener'] = jasmine.createSpy();
      component['clearanceButtonlistener'] = jasmine.createSpy();
    });

    it('should call reRunButtonListener', () => {
      component['initializeButtonsListeners']();

      expect(component['reRunButtonListener']).toHaveBeenCalled();
    });

    it('should call clearanceButtonlistener', () => {
      component['initializeButtonsListeners']();

      expect(component['clearanceButtonlistener']).toHaveBeenCalled();
    });
  });

  describe('reRunButtonListener', () => {
    beforeEach(() => {
      component['requestRerun'] = jasmine.createSpy().and.returnValue(of({}));
      (component as any).reRunButtonClick$ = new Subject();
    });

    it('should call requestRerun', fakeAsync(() => {
      component['reRunButtonListener']();
      (component as any).reRunButtonClick$.next();
      tick();

      expect(component['requestRerun']).toHaveBeenCalled();
    }));

    it('should call translate with rerun success message', fakeAsync(() => {
      component['reRunButtonListener']();
      (component as any).reRunButtonClick$.next();
      tick();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(
        'ADM_ACM.categorizationReasons.dialog.rerunSuccessMessage'
      );
    }));

    it('should call translate with rerun success message', fakeAsync(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['reRunButtonListener']();
      (component as any).reRunButtonClick$.next();
      tick();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith('test-message');
    }));

    it('should call close dialog', fakeAsync(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['reRunButtonListener']();
      (component as any).reRunButtonClick$.next();
      tick();

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  describe('clearanceButtonlistener', () => {
    beforeEach(() => {
      component['requestRerun'] = jasmine.createSpy().and.returnValue(of({}));
      component['clearanceButtonClick$'] = new Subject();
    });

    it('should call requestRerun', fakeAsync(() => {
      component['clearanceButtonlistener']();
      (component as any).clearanceButtonClick$.next();
      tick();

      expect(component['requestRerun']).toHaveBeenCalled();
    }));

    it('should call translate with rerun success message', fakeAsync(() => {
      component['clearanceButtonlistener']();
      (component as any).clearanceButtonClick$.next();
      tick();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(
        'ADM_ACM.categorizationReasons.dialog.clearanceSuccessMessage'
      );
    }));

    it('should call translate with rerun success message', fakeAsync(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['clearanceButtonlistener']();
      (component as any).clearanceButtonClick$.next();
      tick();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith('test-message');
    }));

    it('should call close dialog', fakeAsync(() => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('test-message');

      component['clearanceButtonlistener']();
      (component as any).clearanceButtonClick$.next();
      tick();

      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  describe('requestRerun', () => {
    it('should set item and call service when requestRerun is called', () => {
      const issueDateControl = FormUtil.get<AdmAcmReasonsRerunClearanceDialogViewModel>(component.form, 'issueDate');
      const bspControlValue = FormUtil.get<AdmAcmReasonsRerunClearanceDialogViewModel>(component.form, 'bsp');

      const airlineValue = {
        id: 0,
        name: AllOption.LABEL,
        code: AllOption.VALUE,
        designator: AllOption.LABEL
      };

      const bspAllValue = [
        {
          value: { id: 0, isoCountryCode: AllOption.VALUE, name: AllOption.LABEL },
          label: 'dropdown.all'
        }
      ];

      const initialDate = new Date('2023-02-28');
      const finalDate = new Date('2023-05-31');

      const itemToRequest = {
        active: true,
        airlineCode: undefined,
        bspIds: [undefined],
        fromIssueDate: '2023-02-28',
        toIssueDate: '2023-05-31'
      };

      issueDateControl.setValue([initialDate, finalDate]);
      component['airlineControl'].setValue(airlineValue);
      bspControlValue.setValue(bspAllValue);

      component['requestRerun']();

      expect(admAcmCategorizationReasonsServiceSpy.rerun).toHaveBeenCalledWith(itemToRequest, '001');
    });
  });

  describe('modifyAirlineDropDownByBsp', () => {
    beforeEach(() => {
      component['addAllOptionToAirlines'] = jasmine.createSpy().and.returnValue([
        {
          value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
          label: 'test-label'
        },
        {
          value: { id: 0, name: AllOption.LABEL, code: AllOption.VALUE, designator: AllOption.LABEL },
          label: AllOption.LABEL
        }
      ]);
    });

    it('should call get airline dropdown options with undefined when there are >1 selected bsp', fakeAsync(() => {
      component['modifyAirlineDropDownByBsp']([
        { id: 1, isoCountryCode: 'ES', name: 'Spain' },
        { id: 2, isoCountryCode: 'CH', name: 'China' }
      ]);
      tick();

      expect(component['airlineDictionaryService'].getDropdownOptions).toHaveBeenCalledWith(undefined);
    }));

    it('should call get airline dropdown options with all bsps are selected', fakeAsync(() => {
      component['modifyAirlineDropDownByBsp']([{ id: 0, name: AllOption.LABEL, isoCountryCode: AllOption.VALUE }]);
      tick();

      expect(component['airlineDictionaryService'].getDropdownOptions).toHaveBeenCalledWith(undefined);
    }));

    it('should call get airline dropdown options with ES bsp id', fakeAsync(() => {
      component['modifyAirlineDropDownByBsp']([{ id: 1, isoCountryCode: 'ES', name: 'Spain' }]);
      tick();

      expect(component['airlineDictionaryService'].getDropdownOptions).toHaveBeenCalledWith(1);
    }));

    it('should call addAllOptionToAirlines with airlines', fakeAsync(() => {
      component['airlineDictionaryService'].getDropdownOptions = jasmine.createSpy().and.returnValue(
        of([
          {
            value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
            label: 'test-label'
          }
        ])
      );

      component['modifyAirlineDropDownByBsp']([{ id: 1, isoCountryCode: 'ES', name: 'Spain' }]);
      tick();

      expect(component['addAllOptionToAirlines']).toHaveBeenCalledWith([
        {
          value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
          label: 'test-label'
        }
      ]);
    }));

    it('should set airlineDropdownOptions', fakeAsync(() => {
      component.airlineDropdownOptions = [];

      component['modifyAirlineDropDownByBsp']([{ id: 1, isoCountryCode: 'ES', name: 'Spain' }]);
      tick();

      expect(component.airlineDropdownOptions).toEqual([
        {
          value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
          label: 'test-label'
        },
        {
          value: { id: 0, name: AllOption.LABEL, code: AllOption.VALUE, designator: AllOption.LABEL },
          label: AllOption.LABEL
        }
      ]);
    }));
  });

  describe('createForm', () => {
    it('should set form', () => {
      component.form = null;

      component['createForm']();

      expect(component.form).not.toBeNull();
      expect(component.form.value).toEqual({ bsp: null, airline: null, issueDate: null });
    });
  });

  describe('initializeUserBspOptions', () => {
    it('should set userBspOptions', () => {
      component.userBspOptions$ = null;

      component['initializeUserBspOptions']();

      expect(component.userBspOptions$).not.toBeNull();
    });
  });

  describe('setAirlineOptions', () => {
    beforeEach(() => {
      component['addAllOptionToAirlines'] = jasmine.createSpy().and.returnValue([
        {
          value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
          label: 'test-label'
        },
        {
          value: { id: 0, name: AllOption.LABEL, code: AllOption.VALUE, designator: AllOption.LABEL },
          label: AllOption.LABEL
        }
      ]);
    });

    it('should call get airline dropdown options', fakeAsync(() => {
      component['setAirlineOptions']();
      tick();

      expect(component['airlineDictionaryService'].getDropdownOptions).toHaveBeenCalled();
    }));

    it('should call addAllOptionToAirlines with airlines', fakeAsync(() => {
      component['airlineDictionaryService'].getDropdownOptions = jasmine.createSpy().and.returnValue(
        of([
          {
            value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
            label: 'test-label'
          }
        ])
      );

      component['setAirlineOptions']();
      tick();

      expect(component['addAllOptionToAirlines']).toHaveBeenCalledWith([
        {
          value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
          label: 'test-label'
        }
      ]);
    }));

    it('should set airlineDropdownOptions', fakeAsync(() => {
      component.airlineDropdownOptions = [];

      component['setAirlineOptions']();
      tick();

      expect(component.airlineDropdownOptions).toEqual([
        {
          value: { id: 33, name: 'test-name', code: 'test-code', designator: 'test-designator' },
          label: 'test-label'
        },
        {
          value: { id: 0, name: AllOption.LABEL, code: AllOption.VALUE, designator: AllOption.LABEL },
          label: AllOption.LABEL
        }
      ]);
    }));
  });

  describe('addAllOptionToAirlines', () => {
    beforeEach(() => {
      component['getAllOptionTranslation'] = jasmine.createSpy().and.returnValue('all-label');
    });

    it('should call getAllOptionTranslation', () => {
      component['addAllOptionToAirlines']([
        { value: { id: 1, code: 'ES', name: 'Spain', designator: 'ES' }, label: 'ES' },
        { value: { id: 2, code: 'CH', name: 'China', designator: 'CH' }, label: 'CH' }
      ]);

      expect(component['getAllOptionTranslation']).toHaveBeenCalled();
    });

    it('should return result with all option', () => {
      const res = component['addAllOptionToAirlines']([
        { value: { id: 1, code: 'ES', name: 'Spain', designator: 'ES' }, label: 'ES' },
        { value: { id: 2, code: 'CH', name: 'China', designator: 'CH' }, label: 'CH' }
      ]);

      expect(res).toEqual([
        {
          value: { id: 0, name: AllOption.LABEL, code: AllOption.VALUE, designator: AllOption.LABEL },
          label: 'all-label'
        },
        { value: { id: 1, code: 'ES', name: 'Spain', designator: 'ES' }, label: 'ES' },
        { value: { id: 2, code: 'CH', name: 'China', designator: 'CH' }, label: 'CH' }
      ]);
    });
  });

  describe('getAllOptionTranslation', () => {
    it('should call translate', () => {
      component['getAllOptionTranslation']();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(`dropdown.${AllOption.LABEL}`);
    });

    it('should return translated result', () => {
      component['translationService'].translate = jasmine.createSpy().and.returnValue('all-label');

      const res = component['getAllOptionTranslation']();

      expect(res).toBe('all-label');
    });
  });
});
