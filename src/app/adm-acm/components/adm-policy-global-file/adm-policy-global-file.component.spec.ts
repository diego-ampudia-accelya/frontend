import { ErrorHandler } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of, throwError } from 'rxjs';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { AdmPolicyGlobalFileComponent } from './adm-policy-global-file.component';
import { AdmPolicyGlobalFileService } from './services/adm-policy-global-file.service';

describe('AdmPolicyGlobalFileComponent', () => {
  let component: AdmPolicyGlobalFileComponent;
  let fixture: ComponentFixture<AdmPolicyGlobalFileComponent>;
  const notificationServiceSpy: SpyObject<NotificationService> = createSpyObject(NotificationService);
  const globalErrorHandlerServiceSpy: SpyObject<ErrorHandler> = createSpyObject(ErrorHandler);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const admPolicyGlobalFileServiceSpy: SpyObject<AdmPolicyGlobalFileService> =
    createSpyObject(AdmPolicyGlobalFileService);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdmPolicyGlobalFileComponent, TranslatePipeMock],
      providers: [
        { provide: DialogConfig, useValue: { data: { buttons: [{ type: FooterButton.Accept }] } } },
        { provide: ReactiveSubject, useValue: { asObservable: of({ clickedBtn: FooterButton.Accept }) } },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: L10nTranslationService, useValue: { translate: () => {} } },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: ErrorHandler, useValue: globalErrorHandlerServiceSpy },
        { provide: AdmPolicyGlobalFileService, useValue: admPolicyGlobalFileServiceSpy }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmPolicyGlobalFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call requestAdmGlobalPolicyFile when initializeButtonListeners function is called', fakeAsync(() => {
    spyOn<any>(component['acceptButtonClick$'], 'pipe').and.returnValue(of({}));

    component['initializeButtonListeners']();
    tick();

    expect(admPolicyGlobalFileServiceSpy.requestAdmGlobalPolicyFile).toHaveBeenCalled();
  }));

  it('should call showSuccess when request was success', fakeAsync(() => {
    spyOn<any>(component['acceptButtonClick$'], 'pipe').and.returnValue(of({}));

    component['initializeButtonListeners']();
    tick();

    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
  }));

  it('should call handleError when error was received', fakeAsync(() => {
    spyOn<any>(component['acceptButtonClick$'], 'pipe').and.returnValue(throwError({ status: 401 }));

    component['initializeButtonListeners']();
    tick();

    expect(globalErrorHandlerServiceSpy.handleError).toHaveBeenCalled();
  }));

  it('should call dialod close when subscription was finished with success', fakeAsync(() => {
    spyOn<any>(component['acceptButtonClick$'], 'pipe').and.returnValue(of({}));

    component['initializeButtonListeners']();
    tick();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
  }));

  it('should call dialod close when subscription was finished with error', fakeAsync(() => {
    spyOn<any>(component['acceptButtonClick$'], 'pipe').and.returnValue(throwError({ status: 401 }));

    component['initializeButtonListeners']();
    tick();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
  }));
});
