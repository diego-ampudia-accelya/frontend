import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class AdmPolicyGlobalFileService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/global-adm-policy-file`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public requestAdmGlobalPolicyFile(): Observable<any> {
    return this.http.post(this.baseUrl, {});
  }
}
