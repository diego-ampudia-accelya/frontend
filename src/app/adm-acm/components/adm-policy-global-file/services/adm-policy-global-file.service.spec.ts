import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { AppConfigurationService } from '~app/shared/services';
import { AdmPolicyGlobalFileService } from './adm-policy-global-file.service';

describe('AdmPolicyGlobalFileService', () => {
  let service: AdmPolicyGlobalFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AdmPolicyGlobalFileService,
        { provide: AppConfigurationService, useValue: { baseApiPath: 'test-api' } }
      ]
    });
    service = TestBed.inject(AdmPolicyGlobalFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('shouel call pist with base url', () => {
    (service as any).http.post = jasmine.createSpy().and.returnValue(of({}));

    service.requestAdmGlobalPolicyFile();

    expect((service as any).http.post).toHaveBeenCalledWith('test-api/file-management/global-adm-policy-file', {});
  });
});
