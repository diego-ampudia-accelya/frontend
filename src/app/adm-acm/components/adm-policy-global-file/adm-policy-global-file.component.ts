import { Component, ErrorHandler, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';
import { AdmPolicyGlobalFileService } from './services/adm-policy-global-file.service';

@Component({
  selector: 'bspl-adm-policy-global-file',
  templateUrl: './adm-policy-global-file.component.html',
  styleUrls: ['./adm-policy-global-file.component.scss']
})
export class AdmPolicyGlobalFileComponent implements OnInit {
  private acceptButton: ModalAction = this.config.data.buttons.find(button => button.type === FooterButton.Accept);

  private destroy$ = new Subject();
  private acceptButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Accept),
    takeUntil(this.destroy$)
  );

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private globalErrorHandlerService: ErrorHandler,
    private admPolicyGlobalFileService: AdmPolicyGlobalFileService
  ) {}

  public ngOnInit(): void {
    this.initializeButtonListeners();
  }

  private initializeButtonListeners(): void {
    this.acceptButtonClick$
      .pipe(
        tap(() => (this.acceptButton.isLoading = true)),
        switchMap(() => this.admPolicyGlobalFileService.requestAdmGlobalPolicyFile()),
        takeUntil(this.destroy$)
      )
      .subscribe(
        () => {
          this.notificationService.showSuccess(
            this.translationService.translate('ADM_ACM.policyGlobalFile.successMessage')
          );
          this.dialogService.close();
        },
        error => {
          this.globalErrorHandlerService.handleError(error);
          this.dialogService.close();
        }
      );
  }
}
