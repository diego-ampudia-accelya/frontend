import { DecimalPipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AdmAcmEnquiryDocumentsListComponent } from './adm-acm-enquiry-documents-list.component';
import { AdmAcmDocumentSearchForm, AdmAcmDocumentSummary } from '~app/adm-acm/models/adm-acm-enquiry-document.model';
import { AcdmType, AdmAcmStatus, TransactionCode } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmDocumentEnquiryService } from '~app/adm-acm/services/adm-acm-document-enquiry.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import {
  DataQuery,
  DefaultDisplayFilterFormatter,
  DefaultQueryStorage,
  QueryableDataSource
} from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('ScuSummaryComponent', () => {
  let component: AdmAcmEnquiryDocumentsListComponent;
  let fixture: ComponentFixture<AdmAcmEnquiryDocumentsListComponent>;
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const admAcmDocumentEnquiryServiceSpy = createSpyObject(AdmAcmDocumentEnquiryService);
  let mockStore: MockStore<AppState>;
  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [
      Permissions.readAcdmSupervisionDocument,
      Permissions.readAcdmNotSupervisedDocument,
      Permissions.readAdjustmentDocuments
    ],
    bspPermissions: [
      {
        bspId: 1,
        permissions: [
          Permissions.readAcdmSupervisionDocument,
          Permissions.readAcdmNotSupervisedDocument,
          Permissions.readAdjustmentDocuments
        ]
      },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const activatedRouteStub = {
    snapshot: {
      data: { documentNumber: '123123' }
    }
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    core: {
      menu: {
        tabs: { dashboard: { ...ROUTES.DASHBOARD, id: 'dashboard' } },
        activeTabId: 'dashboard'
      },
      viewListsInfo: {}
    }
  };

  const storedQuery: DataQuery<AdmAcmDocumentSearchForm> = {
    ...defaultQuery
  };

  queryableDataSourceSpy.hasData$ = of(true);
  queryableDataSourceSpy.appliedQuery$ = of(storedQuery);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmEnquiryDocumentsListComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [RouterTestingModule, L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        FormBuilder,
        DecimalPipe,
        DefaultDisplayFilterFormatter,
        provideMockStore({ initialState, selectors: [{ selector: getUser, value: expectedUserDetails }] }),
        mockProvider(L10nTranslationService),
        { provide: PeriodPipe, useClass: PeriodPipeMock },
        { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
        { provide: AdmAcmDocumentEnquiryService, useValue: admAcmDocumentEnquiryServiceSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    })
      .overrideComponent(AdmAcmEnquiryDocumentsListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            DecimalPipe,
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    admAcmDocumentEnquiryServiceSpy.getDocumentDeletionReasonById.and.returnValue(of('Error transaction'));

    fixture = TestBed.createComponent(AdmAcmEnquiryDocumentsListComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject<any>(Store);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should initialize documentNumber predefined filter', () => {
    component.ngOnInit();
    expect(component.predefinedFilters).toEqual({
      ticketDocumentNumber: activatedRouteStub.snapshot.data.documentNumber
    });
  });

  it('should set correct columns on init as an AGENT', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'agent.bsp.isoCountryCode',
        name: 'ADM_ACM.documentEnquiry.query.columns.bsp',
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'ticketDocumentNumber',
        name: 'ADM_ACM.documentEnquiry.query.columns.documentNumber',
        flexGrow: 1,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        name: 'ADM_ACM.documentEnquiry.query.columns.agentCode',
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'acdmStatus',
        name: 'ADM_ACM.documentEnquiry.query.columns.status',
        draggable: false,
        badgeInfo: jasmine.anything(),
        flexGrow: 2,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'currency.code',
        name: 'ADM_ACM.documentEnquiry.query.columns.currency',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'totalAmount',
        name: 'ADM_ACM.documentEnquiry.query.columns.amount',
        draggable: false,
        resizeable: true,
        flexGrow: 2,
        cellClass: 'text-right',
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'dateOfIssue',
        name: 'ADM_ACM.documentEnquiry.query.columns.issueDate',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'disputeDate',
        name: 'ADM_ACM.documentEnquiry.query.columns.disputeDate',
        draggable: false,
        resizeable: true,
        flexGrow: 1,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'period',
        name: 'ADM_ACM.documentEnquiry.query.columns.period',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'reportingDate',
        name: 'ADM_ACM.documentEnquiry.query.columns.repDate',
        draggable: false,
        resizeable: true,
        flexGrow: 1,
        cellTemplate: jasmine.anything()
      })
    ]);

    component['initializeColumns']();
    tick();
    expect(component.columns).toEqual(expectedColumns);
  }));

  it('should set correct columns on init as an AIRLINE', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'agent.bsp.isoCountryCode',
        name: 'ADM_ACM.documentEnquiry.query.columns.bsp',
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'ticketDocumentNumber',
        name: 'ADM_ACM.documentEnquiry.query.columns.documentNumber',
        flexGrow: 1,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'airline.iataCode',
        name: 'ADM_ACM.documentEnquiry.query.columns.airlineCode',
        draggable: false,
        resizeable: true,
        flexGrow: 2
      }),
      jasmine.objectContaining({
        prop: 'acdmStatus',
        name: 'ADM_ACM.documentEnquiry.query.columns.status',
        draggable: false,
        badgeInfo: jasmine.anything(),
        flexGrow: 2,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'currency.code',
        name: 'ADM_ACM.documentEnquiry.query.columns.currency',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'totalAmount',
        name: 'ADM_ACM.documentEnquiry.query.columns.amount',
        draggable: false,
        resizeable: true,
        flexGrow: 2,
        cellClass: 'text-right',
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'dateOfIssue',
        name: 'ADM_ACM.documentEnquiry.query.columns.issueDate',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'disputeDate',
        name: 'ADM_ACM.documentEnquiry.query.columns.disputeDate',
        draggable: false,
        resizeable: true,
        flexGrow: 1,
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        prop: 'period',
        name: 'ADM_ACM.documentEnquiry.query.columns.period',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'reportingDate',
        name: 'ADM_ACM.documentEnquiry.query.columns.repDate',
        draggable: false,
        resizeable: true,
        flexGrow: 1,
        cellTemplate: jasmine.anything()
      })
    ]);
    const userAgent = { ...expectedUserDetails, userType: UserType.AGENT } as User;
    mockStore.overrideSelector(getUser, userAgent);

    component.ngOnInit();
    component['initializeColumns']();
    tick();
    expect(component.columns).toEqual(expectedColumns);
  }));

  it('should add predefined filters to load data query', fakeAsync(() => {
    const expectedQuery = {
      filterBy: jasmine.objectContaining({
        ...storedQuery.filterBy,
        ticketDocumentNumber: activatedRouteStub.snapshot.data.documentNumber
      }),
      paginateBy: storedQuery.paginateBy,
      sortBy: storedQuery.sortBy
    };

    component.ngOnInit();
    tick();

    expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
    expect(queryStorageSpy.save).toHaveBeenCalledWith(expectedQuery);
  }));

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should return that autorizeRequest is an ACDM Request Document', () => {
    const result = component['isAcdmRequestDocument'](AdmAcmStatus.authorizeRequest);

    expect(result).toBeTruthy();
  });

  it('should return that pendingRequest is an ACDM Request Document', () => {
    const result = component['isAcdmRequestDocument'](AdmAcmStatus.pendingRequest);

    expect(result).toBeTruthy();
  });

  it('should return that rejectRequest is an ACDM Request Document', () => {
    const result = component['isAcdmRequestDocument'](AdmAcmStatus.rejectRequest);

    expect(result).toBeTruthy();
  });

  it('should return ACM type of a document', () => {
    const document = {
      transactionCode: TransactionCode.acm
    } as AdmAcmDocumentSummary;

    const result = component['getAcdmType'](document);

    expect(result).toBe(AcdmType[document.transactionCode]);
  });

  it('should return Adm-Request type of a document', () => {
    const document = {
      transactionCode: TransactionCode['adm-request']
    } as AdmAcmDocumentSummary;

    const result = component['getAcdmType'](document);

    expect(result).toBe(AcdmType[document.transactionCode]);
  });

  it('should return Acm-Request type of a document', () => {
    const document = {
      transactionCode: TransactionCode['acm-request']
    } as AdmAcmDocumentSummary;

    const result = component['getAcdmType'](document);

    expect(result).toBe(AcdmType[document.transactionCode]);
  });

  it('should return if document with deleted status is a deleted document', () => {
    const document = {
      transactionCode: TransactionCode['acm-request'],
      acdmStatus: AdmAcmStatus.deleted
    } as AdmAcmDocumentSummary;

    const result = component['isDeletedDocument'](document);

    expect(result).toBeTruthy();
  });

  it('should return if document with supervisionDeleted status is a deleted document', () => {
    const document = {
      transactionCode: TransactionCode['acm-request'],
      acdmStatus: AdmAcmStatus.supervisionDeleted
    } as AdmAcmDocumentSummary;

    const result = component['isDeletedDocument'](document);

    expect(result).toBeTruthy();
  });

  it('should navigate to document view', () => {
    const document = {
      id: '999999',
      transactionCode: TransactionCode['acm-request'],
      acdmStatus: AdmAcmStatus.approved
    } as AdmAcmDocumentSummary;

    const column = { prop: 'ticketDocumentNumber' };
    const event = new Event('onClick');

    routerSpy.navigate.and.callThrough();

    component.onRowLinkClick(event, document, column);

    expect(routerSpy.navigate).toHaveBeenCalledWith(['/acdms/acm/view', document.id]);
  });

  it('should not navigate to document view if user click in other column', () => {
    const document = {
      id: '999999',
      transactionCode: TransactionCode['acm-request'],
      acdmStatus: AdmAcmStatus.approved
    } as AdmAcmDocumentSummary;

    const column = { prop: 'status' };
    const event = new Event('onClick');

    routerSpy.navigate.calls.reset();
    routerSpy.navigate.and.callThrough();

    component.onRowLinkClick(event, document, column);

    expect(routerSpy.navigate).not.toHaveBeenCalled();
  });

  it('should onRowToggle load deletion Reason', () => {
    const document = {
      id: '999999',
      transactionCode: TransactionCode['acm-request'],
      acdmStatus: AdmAcmStatus.deleted
    } as AdmAcmDocumentSummary;

    const event = { type: 'row', value: document };
    component.onRowToggle(event);

    expect(admAcmDocumentEnquiryServiceSpy.getDocumentDeletionReasonById).toHaveBeenCalled();
  });
});
