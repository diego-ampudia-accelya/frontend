import { Component, Inject, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { finalize, first, takeUntil } from 'rxjs/operators';

import { AdmAcmDocumentSearchForm, AdmAcmDocumentSummary } from '~app/adm-acm/models/adm-acm-enquiry-document.model';
import {
  AcdmType,
  AdmAcmStatus,
  SendToDpcType,
  TransactionCode,
  urlView
} from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmDocumentEnquiryService } from '~app/adm-acm/services/adm-acm-document-enquiry.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import {
  DataQuery,
  DefaultDisplayFilterFormatter,
  DefaultQueryStorage,
  QueryableDataSource
} from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { GridColumn } from '~app/shared/models';
import { User, UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-adm-acm-enquiry-documents-list',
  templateUrl: 'adm-acm-enquiry-documents-list.component.html',
  providers: [
    DefaultQueryStorage,
    DefaultDisplayFilterFormatter,
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [AdmAcmDocumentEnquiryService] }
  ]
})
export class AdmAcmEnquiryDocumentsListComponent implements OnInit, OnDestroy {
  @ViewChild('multipleLinkByAcdmStatusCellTmpl', { static: true }) multipleLinkByAcdmStatusCellTmpl: TemplateRef<any>;
  public title = 'ADM_ACM.documentEnquiry.query.title';
  public columns: GridColumn[] = [];
  public predefinedFilters: AdmAcmDocumentSearchForm;

  private $destroy = new Subject();
  private _loadingExpandableRowSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public loading$: Observable<boolean> = this.dataSource.loading$;
  public loadingExpandableRow$: Observable<boolean> = this._loadingExpandableRowSubject.asObservable();

  public deletionReason$: Observable<string>;

  public loggedUser: User;
  public userType: UserType;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public dataSource: QueryableDataSource<AdmAcmDocumentSummary>,
    public filterFormatter: DefaultDisplayFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private store: Store<AppState>,
    private translationService: L10nTranslationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private admAcmDocumentService: AdmAcmDocumentEnquiryService,
    protected dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.initializePredefinedFilters();
    this.initializeLoggedUser();
    this.initializeColumns();

    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  public loadData(query?: DataQuery): void {
    query = query || defaultQuery;

    const dataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy,
        ...this.predefinedFilters
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private download(detailed?: boolean) {
    const data = {
      title: this.translationService.translate('common.download'),
      footerButtonsType: FooterButton.Download,
      downloadQuery: this.queryStorage.get(),
      detailedDownload: detailed
    };

    this.dialogService.open(DownloadFileComponent, {
      data,
      apiService: this.admAcmDocumentService
    });
  }

  public onDownload(): void {
    this.download();
  }

  public onDetailedDownload(): void {
    this.download(true);
  }

  public onRowToggle(event): void {
    const row: AdmAcmDocumentSummary = event?.value;
    if (event?.type === 'row' && row && this.isDeletedDocument(row)) {
      this.setDeletionReason(row);
    }
  }

  private setDeletionReason(row: AdmAcmDocumentSummary): void {
    this._loadingExpandableRowSubject.next(true);
    const acdmId: number = +row.id;
    const acdmType = this.getAcdmType(row);
    this.deletionReason$ = this.admAcmDocumentService
      .getDocumentDeletionReasonById(acdmId, acdmType)
      .pipe(finalize(() => this._loadingExpandableRowSubject.next(false)));
  }

  private isDeletedDocument(document: AdmAcmDocumentSummary) {
    return [AdmAcmStatus.deleted, AdmAcmStatus.supervisionDeleted].includes(document.acdmStatus);
  }

  public onRowLinkClick(event: Event, row, column): void {
    event.stopPropagation();

    // To detect when documentNumber column is clicked
    if (column.prop === 'ticketDocumentNumber') {
      const acdmType = this.getAcdmType(row);
      const url = urlView[acdmType];

      this.router.navigate([url, row.id]);
    }
  }

  private getAcdmType(document: AdmAcmDocumentSummary) {
    let result = AcdmType[document.transactionCode];

    /*
      If transactionCode from document is ACMA or ADMA, we need make a difference between ACDMA or acdm-request 
      To distinguish ADM-REQUEST from ADMA, we can check if status is ENDING_REQUEST, REJECTED_REQUEST or AUTHORIZED_REQUEST
    */
    if (document.transactionCode === TransactionCode.adm && this.isAcdmRequestDocument(document.acdmStatus)) {
      result = AcdmType['ADMQ'];
    }

    if (document.transactionCode === TransactionCode.acm && this.isAcdmRequestDocument(document.acdmStatus)) {
      result = AcdmType['ACMQ'];
    }

    return result;
  }

  private isAcdmRequestDocument(status: AdmAcmStatus): boolean {
    return [AdmAcmStatus.pendingRequest, AdmAcmStatus.authorizeRequest, AdmAcmStatus.rejectRequest].includes(status);
  }

  private initializePredefinedFilters(): void {
    const ticketDocumentNumber = this.activatedRoute.snapshot.data.documentNumber;
    this.predefinedFilters = {
      ticketDocumentNumber
    };
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.pipe(takeUntil(this.$destroy)).subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.userType = loggedUser.userType;
    });
  }

  private initializeColumns(): void {
    this.columns = this.buildColumns();

    if (this.userType === UserType.AGENT) {
      this.columns = this.columns.filter(
        column => column.prop !== 'agent.bsp.name' && column.prop !== 'agent.iataCode'
      );
    }
    if (this.userType === UserType.AIRLINE) {
      this.columns = this.columns.filter(column => column.prop !== 'airline.iataCode');
    }

    this.initializeStatusBadgeInfo();
  }

  private initializeStatusBadgeInfo(): void {
    const statusColumn = this.columns.find(col => col.prop === 'acdmStatus');
    if (statusColumn) {
      statusColumn.pipe = { transform: value => this.translationService.translate(`ADM_ACM.status.${value}`) };

      statusColumn.badgeInfo = {
        hidden: (value: AdmAcmDocumentSummary) => !value.sentToDpc,
        value: this.translationService.translate(`ADM_ACM.dpcBadgeInfo.label`),
        type: (value: AdmAcmDocumentSummary) => {
          switch (value.sentToDpc) {
            case SendToDpcType.pending:
              return BadgeInfoType.info;

            case SendToDpcType.sent:
              return BadgeInfoType.success;

            case SendToDpcType.n_a:
            default:
              return BadgeInfoType.regular;
          }
        },
        tooltipLabel: (value: AdmAcmDocumentSummary) =>
          value.sentToDpc
            ? this.translationService.translate(`ADM_ACM.dpcBadgeInfo.leyend.${value.sentToDpc.toUpperCase()}`, {
                type: this.translationService.translate(`ADM_ACM.type.${this.getAcdmType(value)}`)
              })
            : '',
        showIconType: true
      };
    }
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'agent.bsp.isoCountryCode',
        name: 'ADM_ACM.documentEnquiry.query.columns.bsp',
        flexGrow: 1
      },
      {
        prop: 'ticketDocumentNumber',
        name: 'ADM_ACM.documentEnquiry.query.columns.documentNumber',
        cellTemplate: this.multipleLinkByAcdmStatusCellTmpl,
        flexGrow: 1
      },
      {
        prop: 'agent.iataCode',
        name: 'ADM_ACM.documentEnquiry.query.columns.agentCode',
        flexGrow: 1
      },
      {
        prop: 'airline.iataCode',
        name: 'ADM_ACM.documentEnquiry.query.columns.airlineCode',
        draggable: false,
        resizeable: true,
        flexGrow: 2
      },
      {
        prop: 'acdmStatus',
        name: 'ADM_ACM.documentEnquiry.query.columns.status',
        draggable: false,
        badgeInfo: null,
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        flexGrow: 2
      },
      {
        prop: 'currency.code',
        name: 'ADM_ACM.documentEnquiry.query.columns.currency',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      },
      {
        prop: 'totalAmount',
        name: 'ADM_ACM.documentEnquiry.query.columns.amount',
        draggable: false,
        resizeable: true,
        flexGrow: 2,
        cellClass: 'text-right',
        cellTemplate: 'amountCellTmpl'
      },
      {
        prop: 'dateOfIssue',
        name: 'ADM_ACM.documentEnquiry.query.columns.issueDate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'disputeDate',
        name: 'ADM_ACM.documentEnquiry.query.columns.disputeDate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        flexGrow: 1
      },
      {
        prop: 'period',
        name: 'ADM_ACM.documentEnquiry.query.columns.period',
        draggable: false,
        resizeable: true,
        flexGrow: 1
      },
      {
        prop: 'reportingDate',
        name: 'ADM_ACM.documentEnquiry.query.columns.repDate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        flexGrow: 1
      }
    ];
  }

  public ngOnDestroy(): void {
    this.$destroy.next();
  }
}
