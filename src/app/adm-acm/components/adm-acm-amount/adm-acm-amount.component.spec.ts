import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { initialState } from '../adm-acm/adm-acm-test.config';
import { AdmAcmAmountComponent } from '~app/adm-acm/components/adm-acm-amount/adm-acm-amount.component';
import { AmountViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { createAdmAcmConfigService } from '~app/adm-acm/shared/helpers/adm-acm.factory';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { TabService } from '~app/shared/services';

describe('AmountAdmAcmComponent', () => {
  let component: AdmAcmAmountComponent;
  let fixture: ComponentFixture<AdmAcmAmountComponent>;

  type Mock<T> = { [P in keyof T]?: Mock<T[P]> | string };

  const mockData: Mock<AmountViewModel> = {
    agentCalculations: {
      commission: '5',
      fare: '10',
      supplementaryCommission: '1',
      tax: '2',
      taxOnCommission: '3',
      cancellationPenalty: null,
      miscellaneousFee: null
    },
    airlineCalculations: {
      commission: '5',
      fare: '10',
      supplementaryCommission: '1',
      tax: '2',
      taxOnCommission: '3',
      cancellationPenalty: null,
      miscellaneousFee: null
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmAmountComponent],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        L10nTranslationModule.forRoot(l10nConfig),
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        provideMockStore({ initialState }),
        DialogService,
        AdmAcmDialogService,
        PermissionsService,
        L10nTranslationService,
        HttpClient,
        FormBuilder,
        HttpTestingController,
        TabService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmAmountComponent);
    component = fixture.componentInstance;
    component.currency = { id: 1, code: 'EUR', decimals: 2, isDefault: true };
    component.formData = new AdmAcmDataService(TestBed.inject(FormBuilder));
    component.formConfig = createAdmAcmConfigService(
      component.formData,
      TestBed.inject(AdmAcmDialogService),
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Router),
      TestBed.inject(Store),
      TestBed.inject(TabService)
    );
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should round correctly', done => {
    const agentFare = component.form.get('agentCalculations.fare');

    agentFare.setValue('5.0356789');

    fixture.whenStable().then(() => {
      expect(agentFare.value).toBe('5.04');
      done();
    });
  });

  it('should calculate a difference', done => {
    const agentFare = component.form.get('agentCalculations.fare');
    const airlineFare = component.form.get('airlineCalculations.fare');
    const differenceFare = component.form.get('differenceCalculations.fare');

    component['reverseOrderFactor'] = -1;

    agentFare.setValue('1.15', { emitEvent: false });
    airlineFare.setValue('4.30');

    fixture.whenStable().then(() => {
      expect(differenceFare.value).toBe('3.15');
      done();
    });
  });

  it('should calculate totals', done => {
    const agentTotal = component.form.get('agentTotal');
    const airlineTotal = component.form.get('airlineTotal');
    const totalAmount = component.form.get('totalAmount');

    component['reverseOrderFactor'] = -1;
    component['concernsReverseOrderFactor'] = -1;

    component.form.get('agentCalculations.fare').setValue('1', { emitEvent: false });
    component.form.get('airlineCalculations.fare').setValue('4', { emitEvent: false });

    component.form.get('agentCalculations.commission').setValue('20', { emitEvent: false });
    component.form.get('airlineCalculations.commission').setValue('8');

    fixture.whenStable().then(() => {
      expect(agentTotal.value).toBe('-19.00');
      expect(airlineTotal.value).toBe('-4.00');
      expect(totalAmount.value).toBe('15.00');
      done();
    });
  });

  it('should propagate a totalAmount change', done => {
    const emitter = component.totalAmountChange;
    spyOn(emitter, 'emit');

    component.form.patchValue(mockData);

    fixture.whenStable().then(() => {
      expect(emitter.emit).toHaveBeenCalledWith('0.00');
      done();
    });
  });
});
