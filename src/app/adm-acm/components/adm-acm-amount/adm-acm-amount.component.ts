import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { combineLatest, merge, Observable, of, Subject, Subscription } from 'rxjs';
import { distinctUntilChanged, skipWhile, switchMap, takeUntil } from 'rxjs/operators';

import { totalAmountValidator } from '../adm-acm-amount-tax/validators/total-amount.validator';
import { Calculation, ConcernIndicator } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  AmountCalculationFieldConfig,
  AmountViewModel,
  CurrencyViewModel
} from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AcdmAlertMessage, AdmAcmActionEmitterType, TocaErrorsType } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { roundCalculationValue } from '~app/adm-acm/shared/helpers/adm-acm-general.helper';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AlertMessageType } from '~app/shared/enums/alert-message-type.enum';
import { NumberHelper } from '~app/shared/helpers';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { maxLengthRoundedValidator } from '~app/shared/validators/max-length-rounded.validator';
import { positiveNumberValidator } from '~app/shared/validators/positive-number.validator';

@Component({
  selector: 'bspl-adm-acm-amount',
  templateUrl: './adm-acm-amount.component.html',
  styleUrls: ['./adm-acm-amount.component.scss']
})
export class AdmAcmAmountComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @Input() currency: CurrencyViewModel;
  @Input() formData: AdmAcmDataService;
  @Input() formConfig: AdmAcmConfigService;
  @Input() formTicket: AdmAcmTicketService;

  @Output() totalAmountChange = new EventEmitter<number>();
  @Output() tocaAmountChange = new EventEmitter<number>();

  public amountAlertMessages: Array<AcdmAlertMessage> = [];

  public ticketIssueMode; //* Indicates if we are accesing to issue ACDM from a document
  public isAcdmd = false; //* Indicates if it is an ACDMD (BSP Adjustment)

  public form: FormGroup;
  public amountPlaceholder = '';

  public calculationFields: Array<keyof Calculation>;
  public calculationFieldsVisibility: { [key in keyof Calculation]?: Observable<boolean> } = {};
  private decimalsPattern: RegExp;
  private calculationConfig: AmountCalculationFieldConfig;
  private reverseOrderFactor: 1 | -1;
  private concernsReverseOrderFactor: 1 | -1; //* Used to change coin sign when acdmFor value is REFUND

  private fieldsConfigChange: Subject<any> = new Subject();
  private subscriptions: Array<Subscription> = [];

  private formFactory: FormUtil;

  constructor(private fb: FormBuilder, private cd: ChangeDetectorRef) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.setConfig();
    this.buildForm();

    this.initializeChangesListener();

    this.initializeConcernsIndicatorListener();
    this.initializeTocaTypeListener();

    this.initializeCalculationFieldsVisibility();

    if (this.ticketIssueMode) {
      this.formData.whenFormIsReady().subscribe(() => {
        this.populateAgentTicketAmounts();
      });
    }

    if (this.formConfig.isEditMode) {
      this.updateAmountsFieldsVisibilityOnEdit();
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.currency && !changes.currency.firstChange) {
      this.updateForm();
    }
  }

  public ngAfterViewInit(): void {
    this.updateForm();
  }

  public getTotalControl(field: keyof AmountViewModel & ('agentTotal' | 'airlineTotal' | 'totalAmount')): FormControl {
    return this.form.get(field) as FormControl;
  }

  public getAcdmcFareGroupName(): keyof AmountViewModel {
    if (this.formConfig.admAcmType === MasterDataType.Admd) {
      return 'airlineCalculations';
    } else {
      return 'agentCalculations';
    }
  }

  private get decimals(): number {
    return this.currency ? this.currency.decimals : 2;
  }

  private setConfig() {
    this.ticketIssueMode = this.formConfig.isEditTicketMode;
    this.isAcdmd =
      this.formConfig.admAcmType === MasterDataType.Admd || this.formConfig.admAcmType === MasterDataType.Acmd;

    this.calculationFields = this.formConfig.getAmountCalculationFields();
    this.subscriptions.push(
      this.formConfig.getAmountConfigurationCalculationFields().subscribe(value => {
        const isInitialConfig = typeof this.calculationConfig === 'undefined';
        this.calculationConfig = value;

        //* InitialConfig is coming from BehavSubj initial value. No need to update the form in this case
        if (!isInitialConfig) {
          this.formData.whenFormIsReady().subscribe(() => {
            this.updateForm();
          });
        }

        this.fieldsConfigChange.next();
        this.subscriptions.push(
          //* Every time a field visibility is changed, we call detectChanges to show correctly the updated form
          merge(...Object.values(this.calculationConfig).map(fieldConfig => fieldConfig.visible))
            .pipe(
              skipWhile(() => !this.formData.isFormReady.value || !this.form),
              takeUntil(this.fieldsConfigChange)
            )
            .subscribe(() => {
              this.cd.detectChanges();
            })
        );
      })
    );
    this.reverseOrderFactor = this.formConfig.reverseOrderFactor;
    this.decimalsPattern = this.formConfig.getDecimalsPattern();

    this.initializeAlertMessages();
  }

  private buildForm() {
    const maxLength = NumberHelper.getMonetaryAmountMaxLength(this.decimals);

    const agentCalculations = this.fb.group({}, { updateOn: 'blur' });
    const airlineCalculations = this.fb.group({}, { updateOn: 'blur' });
    const differenceCalculations = this.fb.group({});
    const agentTotal = this.fb.control('');
    const airlineTotal = this.fb.control('');
    const totalAmount = this.fb.control('', [totalAmountValidator]);

    this.calculationFields.forEach(field => {
      agentCalculations.addControl(
        field,
        this.fb.control('', [
          positiveNumberValidator,
          Validators.pattern(this.decimalsPattern),
          maxLengthRoundedValidator(this.decimals, maxLength)
        ])
      );

      airlineCalculations.addControl(
        field,
        this.fb.control('', [
          positiveNumberValidator,
          Validators.pattern(this.decimalsPattern),
          maxLengthRoundedValidator(this.decimals, maxLength)
        ])
      );

      differenceCalculations.addControl(field, this.fb.control(''));
    });

    this.form = this.formFactory.createGroup<AmountViewModel>({
      agentCalculations,
      airlineCalculations,
      differenceCalculations,
      agentTotal,
      airlineTotal,
      totalAmount
    });

    this.formData.setSectionFormGroup('amounts', this.form);
  }

  private initializeChangesListener() {
    this.subscriptions.push(
      this.form.valueChanges.subscribe(() => this.updateForm()),

      this.formData
        .getSubject(AdmAcmActionEmitterType.totalTaxAmount)
        .pipe(skipWhile(() => this.form.valid))
        .subscribe(() => this.getCalculationControl('differenceCalculations', 'tax').updateValueAndValidity())
    );
  }

  private initializeConcernsIndicatorListener() {
    this.subscriptions.push(
      this.formData
        .getSubject(AdmAcmActionEmitterType.acdmaFor)
        .pipe(distinctUntilChanged())
        .subscribe((concern: ConcernIndicator) => {
          this.concernsReverseOrderFactor =
            concern === ConcernIndicator.refund ? ((-1 * this.reverseOrderFactor) as 1 | -1) : this.reverseOrderFactor;
          this.setTotals();
        })
    );
  }

  private initializeTocaTypeListener() {
    const agentTocaAmountCtrl = this.getCalculationControl('agentCalculations', 'taxOnCommission');
    const airlineTocaAmountCtrl = this.getCalculationControl('airlineCalculations', 'taxOnCommission');
    const diffTocaAmountCtrl = this.getCalculationControl('differenceCalculations', 'taxOnCommission');

    this.subscriptions.push(
      this.formConfig.validateToca().subscribe(res => {
        if (TocaErrorsType.amount === res && !+diffTocaAmountCtrl.value) {
          diffTocaAmountCtrl.setErrors({ toca_difference: true });
        } else {
          diffTocaAmountCtrl.setErrors(null);
        }
      }),
      combineLatest([agentTocaAmountCtrl.valueChanges, airlineTocaAmountCtrl.valueChanges]).subscribe(() => {
        this.tocaAmountChange.emit(+this.getDifference(agentTocaAmountCtrl, airlineTocaAmountCtrl));
      })
    );
  }

  private initializeCalculationFieldsVisibility() {
    this.calculationFields.forEach(
      field =>
        (this.calculationFieldsVisibility = {
          ...this.calculationFieldsVisibility,
          [field]: this.calculationConfig[field].visible
        })
    );
  }

  private initializeAlertMessages() {
    this.amountAlertMessages = [
      {
        hidden: of(true),
        message: 'ADM_ACM.amounts.amount.spamMessage',
        type: AlertMessageType.info
      }
    ];
  }

  private populateAgentTicketAmounts() {
    const agentFormGroup = FormUtil.get<AmountViewModel>(this.form, 'agentCalculations');

    this.subscriptions.push(
      this.formConfig.bspConfig
        .pipe(switchMap(bspConfig => this.formTicket.getAgentTicketCalculations(bspConfig)))
        .subscribe(agentTicketCalculations => {
          agentFormGroup.patchValue(agentTicketCalculations);
          this.updateHideSpamMsg();
        })
    );
  }

  private updateHideSpamMsg() {
    this.amountAlertMessages.find(msg => msg.message === 'ADM_ACM.amounts.amount.spamMessage').hidden =
      this.formConfig.getAcdmSpam();
  }

  private getCalculationControl(
    type: keyof AmountViewModel & ('agentCalculations' | 'airlineCalculations' | 'differenceCalculations'),
    field: keyof Calculation
  ): FormControl {
    return this.form.get(type).get(field) as FormControl;
  }

  private updateForm() {
    this.updateControls();
    this.setTotals();
  }

  /**
   * Updates controls
   *
   * Sets amount differences in controls and also correct decimals in inputs
   */
  private updateControls() {
    this.amountPlaceholder = Number(0).toFixed(this.decimals);

    this.calculationFields.forEach(field => {
      const agentControl = this.getCalculationControl('agentCalculations', field);
      const airlineControl = this.getCalculationControl('airlineCalculations', field);
      const differenceControl = this.getCalculationControl('differenceCalculations', field);

      FormUtil.regularizeCalculationControls(agentControl, airlineControl, differenceControl);

      if (agentControl.value && agentControl.valid) {
        agentControl.setValue(this.round(agentControl.value), {
          emitEvent: false
        });
      }

      if (airlineControl.value && airlineControl.valid) {
        airlineControl.setValue(this.round(airlineControl.value), {
          emitEvent: false
        });
      }

      const difference =
        agentControl.valid && airlineControl.valid ? this.getDifference(agentControl, airlineControl) : this.round(0);

      differenceControl.setValue(difference, {
        emitEvent: false
      });
    });
  }

  private setTotals() {
    const totals = this.calculationFields
      .map(field => {
        const agentControl = this.getCalculationControl('agentCalculations', field);
        const airlineControl = this.getCalculationControl('airlineCalculations', field);

        const fieldTotalConfig = this.calculationConfig[field].sign;

        return [
          fieldTotalConfig *
            parseFloat(agentControl.enabled && agentControl.valid && agentControl.value ? agentControl.value : 0),
          fieldTotalConfig *
            parseFloat(
              airlineControl.enabled && airlineControl.valid && airlineControl.value ? airlineControl.value : 0
            )
        ];
      })
      .reduce((total, current) => [total[0] + current[0], total[1] + current[1]]);

    this.getTotalControl('agentTotal').setValue(this.round(totals[0]), { emitEvent: false });
    this.getTotalControl('airlineTotal').setValue(this.round(totals[1]), { emitEvent: false });
    this.getTotalControl('totalAmount').setValue(
      this.round((totals[0] - totals[1]) * this.concernsReverseOrderFactor),
      {
        emitEvent: false
      }
    );

    this.totalAmountChange.emit(this.getTotalControl('totalAmount').value);
  }

  private getDifference(agentControl: AbstractControl, airlineControl: AbstractControl): string {
    return this.round(Math.abs(agentControl.value - airlineControl.value));
  }

  private round(value): string {
    return roundCalculationValue(value, this.decimals);
  }

  private updateAmountsFieldsVisibilityOnEdit() {
    this.formData.whenFormIsReady().subscribe(() => {
      this.updateAmountFieldVisibility('miscellaneousFee');
      this.updateAmountFieldVisibility('cancellationPenalty');
    });
  }

  private updateAmountFieldVisibility(field: keyof Calculation & ('miscellaneousFee' | 'cancellationPenalty')) {
    const agentMiscFeeValue = this.getCalculationControl('agentCalculations', field).value;
    const airlineMiscFeeValue = this.getCalculationControl('airlineCalculations', field).value;
    const differenceMiscFeeValue = this.getCalculationControl('differenceCalculations', field).value;
    const miscFeeVisibility = this.formConfig.isConcernsRefundFieldVisible(
      field,
      agentMiscFeeValue,
      airlineMiscFeeValue,
      differenceMiscFeeValue
    );
    this.formConfig.updateVisiblityCalculationField(field, miscFeeVisibility);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subcription => subcription.unsubscribe());
  }
}
