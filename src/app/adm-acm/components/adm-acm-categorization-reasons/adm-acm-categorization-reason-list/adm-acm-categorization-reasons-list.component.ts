import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { AdmAcmCategorizationReasonsDialogComponent } from '../../dialogs/adm-acm-categorization-reasons-dialog/adm-acm-categorization-reasons-dialog.component';
import { AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent } from '../../dialogs/adm-acm-categorization-reasons-reactivate-deactivate-dialog/adm-acm-categorization-reasons-reactivate-deactivate-dialog.component';
import { AdmAcmCategorizationReasonsRerunClearanceDialogComponent } from '../../dialogs/adm-acm-categorization-reasons-rerun-clearance-dialog/adm-acm-categorization-reasons-rerun-clearance-dialog.component';
import { AdmAcmReasons, AdmAcmReasonsFilter, StatusReasonsType } from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsFormatter } from '~app/adm-acm/services/adm-acm-categorization-reasons-formatter.service';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DialogConfig, DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-adm-acm-categorization-reasons-list',
  templateUrl: 'adm-acm-categorization-reasons-list.component.html',
  styleUrls: ['adm-acm-categorization-reasons-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    AdmAcmCategorizationReasonsService,
    AdmAcmCategorizationReasonsFormatter,
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [AdmAcmCategorizationReasonsService] }
  ]
})
export class AdmAcmCategorizationReasonsListComponent implements OnInit, OnDestroy {
  public title = 'ADM_ACM.categorizationReasons.title';
  public customLabels = { create: 'ADM_ACM.categorizationReasons.createButtonLabel' };

  public columns: GridColumn[] = [];
  public totalItems$ = this.dataSource.appliedQuery$.pipe(map(query => query.paginateBy.totalElements));
  public searchForm: FormGroup;

  public statusOptions: DropdownOption[] = [];
  public primaryReasonOptions$: Observable<DropdownOption[]>;

  public loading$: Observable<boolean> = this.dataSource.loading$;
  public canCreateReason = this.permissionsService.hasPermission(Permissions.createCategorizationReason);

  public actions: Array<{ action: GridTableActionType; disabled: boolean }>;

  private formFactory: FormUtil;

  private dialogQueryFormSubmitEmitter = new Subject();
  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public filterFormatter: AdmAcmCategorizationReasonsFormatter,
    public dataSource: QueryableDataSource<AdmAcmCategorizationReasonsListComponent>,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private queryStorage: DefaultQueryStorage,
    private admAcmCategorizationReasonsService: AdmAcmCategorizationReasonsService,
    private formBuilder: FormBuilder,
    private permissionsService: PermissionsService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.columns = this.buildColumns();
    this.searchForm = this.buildSearchForm();

    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);

    this.dialogQueryFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.loadData());
  }

  public loadData(query?: DataQuery): void {
    query = query || defaultQuery;

    const dataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.admAcmCategorizationReasonsService
    });
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      this.initializeFilterDropdowns();
    }
  }

  public createReason(): Observable<any> {
    const dialogConfig: DialogConfig = {
      data: {
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.title'),
        footerButtonsType: FooterButton.Save,
        actionEmitter: this.dialogQueryFormSubmitEmitter
      }
    };

    return this.dialogService.open(AdmAcmCategorizationReasonsDialogComponent, dialogConfig);
  }

  public onActionMenuClick(file: AdmAcmReasons): void {
    const hasUpdatePermission = this.permissionsService.hasPermission(Permissions.updateCategorizationReason);
    const isActiveFile = file.active;

    this.actions = [
      { action: GridTableActionType.Edit, disabled: !hasUpdatePermission },
      { action: GridTableActionType.DeactivateADMReason, disabled: !hasUpdatePermission || !isActiveFile },
      { action: GridTableActionType.ReactivateADMReason, disabled: !hasUpdatePermission || isActiveFile },
      { action: GridTableActionType.RerunADM, disabled: !hasUpdatePermission || !isActiveFile },
      { action: GridTableActionType.ClearanceADM, disabled: !hasUpdatePermission || isActiveFile }
    ];
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: AdmAcmReasons }): void {
    switch (action.actionType) {
      case GridTableActionType.Edit:
        this.openEditDialog(this.getConfigEditDialog(row));
        break;
      case GridTableActionType.DeactivateADMReason:
        this.openDeactivateReactivateDialog(this.getConfigDeactivateDialog(row));
        break;
      case GridTableActionType.ReactivateADMReason:
        this.openDeactivateReactivateDialog(this.getConfigReactivateDialog(row));
        break;
      case GridTableActionType.RerunADM:
        this.openRerunClearanceDialog(this.getConfigRerunDialog(row));
        break;
      case GridTableActionType.ClearanceADM:
        this.openRerunClearanceDialog(this.getConfigClearanceDialog(row));
        break;
    }
  }

  private openEditDialog(config: DialogConfig) {
    this.dialogService.open(AdmAcmCategorizationReasonsDialogComponent, config);
  }

  private openDeactivateReactivateDialog(config: DialogConfig) {
    this.dialogService.open(AdmAcmCategorizationReasonsReactivateDeactivateDialogComponent, config);
  }

  private openRerunClearanceDialog(config: DialogConfig) {
    this.dialogService.open(AdmAcmCategorizationReasonsRerunClearanceDialogComponent, config);
  }

  private getConfigEditDialog(item: AdmAcmReasons): DialogConfig {
    return {
      data: {
        actionEmitter: this.dialogQueryFormSubmitEmitter,
        item,
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.updateTitle'),
        footerButtonsType: FooterButton.Save
      }
    };
  }

  private getConfigDeactivateDialog(item: AdmAcmReasons): DialogConfig {
    return {
      data: {
        actionEmitter: this.dialogQueryFormSubmitEmitter,
        item,
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.deactivateTitle'),
        footerButtonsType: FooterButton.Deactivate,
        isDeactivateMode: true
      }
    };
  }

  private getConfigReactivateDialog(item: AdmAcmReasons): DialogConfig {
    return {
      data: {
        actionEmitter: this.dialogQueryFormSubmitEmitter,
        item,
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.reactivateTitle'),
        footerButtonsType: FooterButton.Reactivate,
        isDeactivateMode: false
      }
    };
  }

  private getConfigRerunDialog(item: AdmAcmReasons): DialogConfig {
    return {
      data: {
        item,
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.rerunTitle'),
        footerButtonsType: FooterButton.Rerun,
        isClearanceMode: false
      }
    };
  }

  private getConfigClearanceDialog(item: AdmAcmReasons): DialogConfig {
    return {
      data: {
        item,
        title: this.translationService.translate('ADM_ACM.categorizationReasons.dialog.clearanceTitle'),
        footerButtonsType: FooterButton.Clearance,
        isClearanceMode: true
      }
    };
  }

  private initializeFilterDropdowns(): void {
    this.primaryReasonOptions$ = this.admAcmCategorizationReasonsService.getPrimaryReasonDropDownValueObject();

    this.statusOptions = [true, false].map(value => ({
      value,
      label: this.filterFormatter.formatActive(value)
    }));
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<AdmAcmReasonsFilter>({
      primaryReason: [],
      subReason: [],
      active: []
    });
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'primaryReason',
        name: 'ADM_ACM.categorizationReasons.columns.primaryReason',
        flexGrow: 2
      },
      {
        prop: 'subReason',
        name: 'ADM_ACM.categorizationReasons.columns.subReason',
        flexGrow: 2
      },
      {
        prop: 'active',
        pipe: {
          transform: value => this.filterFormatter.formatActive(value)
        },
        name: 'ADM_ACM.categorizationReasons.columns.status',
        flexGrow: 1,
        cellTemplate: 'badgeInfoCellTmpl',
        badgeInfo: {
          type: (value: AdmAcmReasons) => this.getBadgeInfoType(value),
          tooltipLabel: (value: AdmAcmReasons) => this.filterFormatter.formatActive(value.active),
          hidden: (value: AdmAcmReasons) => !value,
          showIconType: false
        }
      }
    ];
  }

  private getBadgeInfoType(value: AdmAcmReasons): BadgeInfoType {
    const formattedActive = this.filterFormatter.formatActive(value.active)?.toLowerCase();

    return formattedActive === StatusReasonsType.active ? BadgeInfoType.success : BadgeInfoType.regular;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
