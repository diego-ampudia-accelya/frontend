import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AdmAcmCategorizationReasonsListComponent } from './adm-acm-categorization-reasons-list.component';
import { AdmAcmReasons } from '~app/adm-acm/models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsFormatter } from '~app/adm-acm/services/adm-acm-categorization-reasons-formatter.service';
import { AdmAcmCategorizationReasonsService } from '~app/adm-acm/services/adm-acm-categorization-reasons.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService, FooterButton } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';

describe('AdmAcmCategorizationReasonsListComponent', () => {
  let component: AdmAcmCategorizationReasonsListComponent;
  let fixture: ComponentFixture<AdmAcmCategorizationReasonsListComponent>;

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  const dialogServiceSpy = createSpyObject(DialogService);
  const queryStorageMock = createSpyObject(DefaultQueryStorage);
  const queryMock = { filterBy: {}, paginateBy: {}, sortBy: [] };
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const AppConfigurationServiceSpy = createSpyObject(AppConfigurationService);

  const expectedUserDetails = {
    id: 10126,
    email: 'iata@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.IATA,
    permissions: ['rAdmCatRsn', 'uAdmCatRsn'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  const row: AdmAcmReasons = {
    active: true
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmAcmCategorizationReasonsListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockStore({
          initialState
        }),
        FormBuilder,
        HttpClient,
        AdmAcmCategorizationReasonsService,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: AppConfigurationService, useValue: AppConfigurationServiceSpy }
      ]
    })
      .overrideComponent(AdmAcmCategorizationReasonsListComponent, {
        set: {
          providers: [
            AdmAcmCategorizationReasonsFormatter,
            AdmAcmCategorizationReasonsService,
            PermissionsService,
            { provide: DefaultQueryStorage, useValue: queryStorageMock },
            {
              provide: QueryableDataSource,
              useValue: queryableDataSourceSpy,
              deps: [AdmAcmCategorizationReasonsService]
            }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmCategorizationReasonsListComponent);
    component = fixture.componentInstance;
    queryStorageMock.get.and.returnValue(queryMock);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');

    component.ngOnInit();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should open create dialog correctly on createReason', () => {
    component.createReason();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should initialize filter dropdown when button filter is clicked', () => {
    const initializeFiltersDropdownSpy = spyOn<any>(component, 'initializeFilterDropdowns');

    component.onFilterButtonClicked(true);
    expect(initializeFiltersDropdownSpy).toHaveBeenCalled();
  });

  it('should call getPrimaryReasonDropDownValueObject when initializeFilterDropdowns is called', () => {
    const initializeFiltersDropdownSpy = spyOn<any>(
      component['admAcmCategorizationReasonsService'],
      'getPrimaryReasonDropDownValueObject'
    );

    component['initializeFilterDropdowns']();

    expect(initializeFiltersDropdownSpy).toHaveBeenCalled();
  });

  it('should call correct functions when buildColumns is called', () => {
    const formatParams: AdmAcmReasons = {
      id: 1,
      primaryReason: 'reason',
      subReason: 'subReason',
      active: true
    };
    const formatActiveSpy = spyOn<any>(component['filterFormatter'], 'formatActive');

    component['getBadgeInfoType'](formatParams);

    expect(formatActiveSpy).toHaveBeenCalled();
  });

  it('should call loadData when dialogQueryFormSubmitEmitter emit', () => {
    const loadDataSpy = spyOn(component, 'loadData');

    component.ngOnInit();
    component['dialogQueryFormSubmitEmitter'].next();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should set actions when onActionMenuClick is called', () => {
    const expectedActions = [
      { action: GridTableActionType.Edit, disabled: false },
      { action: GridTableActionType.DeactivateADMReason, disabled: false },
      { action: GridTableActionType.ReactivateADMReason, disabled: true },
      { action: GridTableActionType.RerunADM, disabled: false },
      { action: GridTableActionType.ClearanceADM, disabled: true }
    ];

    const param: AdmAcmReasons = {
      active: true
    };

    component.onActionMenuClick(param);

    expect(component.actions).toEqual(expectedActions);
  });

  it('should call open edit dialog when onActionClick is called', () => {
    const openEditDialogSpy = spyOn<any>(component, 'openEditDialog');
    const action = {
      actionType: GridTableActionType.Edit,
      methodName: 'name'
    };

    const param = { action, row };

    component.onActionClick(param);

    expect(openEditDialogSpy).toHaveBeenCalled();
  });

  it('should call open Deactivate dialog when onActionClick is called', () => {
    const openDeactivateReactivateDialogSpy = spyOn<any>(component, 'openDeactivateReactivateDialog');
    const action = {
      actionType: GridTableActionType.DeactivateADMReason,
      methodName: 'name'
    };

    const param = { action, row };

    component.onActionClick(param);

    expect(openDeactivateReactivateDialogSpy).toHaveBeenCalled();
  });

  it('should call open Reactivate dialog when onActionClick is called', () => {
    const openDeactivateReactivateSpy = spyOn<any>(component, 'openDeactivateReactivateDialog');
    const action = {
      actionType: GridTableActionType.ReactivateADMReason,
      methodName: 'name'
    };

    const param = { action, row };

    component.onActionClick(param);

    expect(openDeactivateReactivateSpy).toHaveBeenCalled();
  });

  it('should call open Reactivate dialog when onActionClick is called', () => {
    const openRerunClearanceDialogSpy = spyOn<any>(component, 'openRerunClearanceDialog');
    const action = {
      actionType: GridTableActionType.RerunADM,
      methodName: 'name'
    };

    const param = { action, row };

    component.onActionClick(param);

    expect(openRerunClearanceDialogSpy).toHaveBeenCalled();
  });

  it('should call open Reactivate dialog when onActionClick is called', () => {
    const openRerunClearanceDialogSpy = spyOn<any>(component, 'openRerunClearanceDialog');
    const action = {
      actionType: GridTableActionType.ClearanceADM,
      methodName: 'name'
    };

    const param = { action, row };

    component.onActionClick(param);

    expect(openRerunClearanceDialogSpy).toHaveBeenCalled();
  });

  it('should open Edit dialog when openEditDialog is called', () => {
    const dialogConfig = {
      data: {
        footerButtonsType: FooterButton.Save
      }
    };

    component['openEditDialog'](dialogConfig);

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should open Deactivate dialog when openDeactivateReactivateDialog is called', () => {
    const dialogConfig = {
      data: {
        footerButtonsType: FooterButton.Deactivate
      }
    };

    component['openDeactivateReactivateDialog'](dialogConfig);

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should open Rerun dialog when openEditDialog is called', () => {
    const dialogConfig = {
      data: {
        footerButtonsType: FooterButton.Rerun
      }
    };

    component['openRerunClearanceDialog'](dialogConfig);

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
