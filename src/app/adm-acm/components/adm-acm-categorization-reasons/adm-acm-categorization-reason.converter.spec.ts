import { toValueLabelObjectPrimaryReason } from './adm-acm-categorization-reason.converter';
import { AdmAcmReasonsPrimaryReason } from '~app/adm-acm/models/adm-acm-reason.model';
import { DropdownOption } from '~app/shared/models';

describe('toValueLabelObjectPrimaryReason converter', () => {
  const admAcmReasonsPrimaryReason: AdmAcmReasonsPrimaryReason[] = [
    {
      id: '001',
      primaryReason: 'primaryReason 001'
    },
    {
      id: '002',
      primaryReason: 'primaryReason 002'
    }
  ];

  const expectedOptionsPrimaryReason: DropdownOption<AdmAcmReasonsPrimaryReason>[] = [
    {
      value: admAcmReasonsPrimaryReason[0],
      label: admAcmReasonsPrimaryReason[0].primaryReason
    },
    {
      value: admAcmReasonsPrimaryReason[1],
      label: admAcmReasonsPrimaryReason[1].primaryReason
    }
  ];

  it('should convert admAcm Primary Reason', () => {
    expect(toValueLabelObjectPrimaryReason(admAcmReasonsPrimaryReason)).toEqual(expectedOptionsPrimaryReason);
  });
});
