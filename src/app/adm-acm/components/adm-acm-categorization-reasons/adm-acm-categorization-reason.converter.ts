import { AdmAcmReasonsPrimaryReason } from '~app/adm-acm/models/adm-acm-reason.model';
import { DropdownOption } from '~app/shared/models';

export function toValueLabelObjectPrimaryReason(
  value: AdmAcmReasonsPrimaryReason[]
): DropdownOption<AdmAcmReasonsPrimaryReason>[] {
  return value.map(item => ({ value: item, label: item.primaryReason }));
}
