import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { AdmAcmConfigService } from '~app/adm-acm/services/adm-acm-config.service';
import { AdmAcmDataService } from '~app/adm-acm/services/adm-acm-data.service';
import { AdmAcmDialogService } from '~app/adm-acm/services/adm-acm-dialog.service';
import { AdmAcmTicketService } from '~app/adm-acm/services/adm-acm-ticket.service';
import { DocumentService } from '~app/document/services/document.service';
import { AppState } from '~app/reducers';
import { TabService } from '~app/shared/services';

export function createAdmAcmConfigService(
  formData: AdmAcmDataService,
  acdmDialogService: AdmAcmDialogService,
  translationService: L10nTranslationService,
  http: HttpClient,
  router: Router,
  store: Store<AppState>,
  tabService: TabService
): AdmAcmConfigService {
  return new AdmAcmConfigService(formData, acdmDialogService, translationService, http, router, store, tabService);
}

export function createAdmAcmTicketService(
  formData: AdmAcmDataService,
  docService: DocumentService,
  store: Store<AppState>
) {
  return new AdmAcmTicketService(formData, docService, store);
}
