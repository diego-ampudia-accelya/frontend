import { AdmAcmIssueBE, AdmAcmIssueViewModel } from '~app/adm-acm/models/adm-acm-issue.model';
import { SummaryTicket } from '~app/document/models/summary-ticket.model';
import {
  getDefaultCalculationFieldValueBE,
  uppercaseAdapter
} from '~app/shared/helpers/form-helpers/adapter/view-be-adapter.helper';
import { AdapterMapper, FieldMapper } from '~app/shared/helpers/form-helpers/mapping/types';
import {
  calculationBEAdapter,
  calculationViewAdapter,
  DocumentNumbersAdapter,
  fullDocumentNumberAdapter,
  idTicketViewAdapter,
  netReportingBEAdapter,
  taxFeeBEAdapter,
  taxFeeTicketViewAdapter,
  taxFeeViewAdapter
} from './view-be-adapter.helper';

export const fieldMapperAdmAcm: FieldMapper<AdmAcmIssueBE, AdmAcmIssueViewModel> = {
  id: null,
  acdmStatus: { statusHeader: { status: { status: null } } },
  agent: { basicInformation: { agent: null } },
  agentAddress: { basicInformation: { agent: { address: null } } },
  agentCalculations: { amounts: { agentCalculations: null } },
  agentTotal: { amounts: { agentTotal: null } },
  agentVatNumber: { basicInformation: { agent: { acdmVatNumber: null } } },
  airline: { basicInformation: { airline: null } },
  airlineAddress: { basicInformation: { airline: { address: null } } },
  airlineCalculations: { amounts: { airlineCalculations: null } },
  airlineContact: { basicInformation: { airlineContact: null } },
  airlineTotal: { amounts: { airlineTotal: null } },
  airlineVatNumber: { basicInformation: { airline: { acdmVatNumber: null } } },
  attachmentIds: { attachmentIds: null },
  concernsIndicator: { basicInformation: { admSettings: { concernsIndicator: null } } },
  currency: { basicInformation: { admSettings: { currency: null } } },
  dateOfIssue: { statusHeader: { issueDate: null } },
  dateOfIssueRelatedDocument: { details: { relatedDocuments: { dateOfIssueRelatedDocument: null } } },
  differenceCalculations: { amounts: { differenceCalculations: null } },
  disputeContact: { details: { disputeContact: null } },
  disputeTime: { details: { disputeTime: null } },
  gdsForwards: { details: { gdsForwards: null } },
  netReporting: { basicInformation: { admSettings: { netReporting: null } } },
  reasonForMemo: { details: { reasonsDetails: { reasonForMemo: null } } },
  reasonForMemoIssuanceCode: { details: { reasonsDetails: { reasonForMemoIssuanceCode: null } } },
  reasons: { details: { reasons: null } },
  relatedTicketDocuments: { details: { relatedDocuments: { relatedTicketDocuments: null } } },
  reportingDate: { basicInformation: { reportingDate: null } },
  sentToDpc: { statusHeader: { status: { sentToDpc: null } } },
  statisticalCode: { basicInformation: { admSettings: { statisticalCode: null } } },
  taxMiscellaneousFees: { taxes: { taxMiscellaneousFees: null } },
  taxOnCommissionType: { basicInformation: { admSettings: { taxOnCommissionType: null } } },
  ticketDocumentNumber: [
    { basicInformation: { ticketDocumentNumber: null } },
    { statusHeader: { ticketDocumentNumber: null } }
  ],
  totalAmount: { amounts: { totalAmount: null } },
  transactionCode: { basicInformation: { type: null } }
};

export const adapterBEFunctionsMapper: AdapterMapper<AdmAcmIssueBE> = {
  agentCalculations: calculationBEAdapter,
  agentTotal: getDefaultCalculationFieldValueBE,
  airlineCalculations: calculationBEAdapter,
  airlineTotal: getDefaultCalculationFieldValueBE,
  differenceCalculations: calculationBEAdapter,
  netReporting: netReportingBEAdapter,
  reasonForMemoIssuanceCode: uppercaseAdapter,
  statisticalCode: uppercaseAdapter,
  taxMiscellaneousFees: taxFeeBEAdapter,
  totalAmount: getDefaultCalculationFieldValueBE,
  default: value => (value === '' ? null : value)
};

export const adapterViewFunctionsMapper: AdapterMapper<AdmAcmIssueBE> = {
  agentCalculations: calculationViewAdapter,
  airlineCalculations: calculationViewAdapter,
  taxMiscellaneousFees: taxFeeViewAdapter
};

export const fieldMapperAdmAcmTicket: FieldMapper<Partial<SummaryTicket>, AdmAcmIssueViewModel> = {
  id: null,
  bsp: [{ basicInformation: { agent: { bsp: null } } }, { basicInformation: { bspName: null } }],
  agent: { basicInformation: { agent: null } },
  airline: { basicInformation: { airline: null } },
  currency: { basicInformation: { admSettings: { currency: null } } },
  dateOfIssue: { details: { relatedDocuments: { dateOfIssueRelatedDocument: null } } },
  fullDocumentNumber: { details: { relatedDocuments: { relatedTicketDocuments: null } } },
  statisticalCode: { basicInformation: { admSettings: { statisticalCode: null } } },
  taxes: { taxes: { taxMiscellaneousFees: null } },
  documentNumbers: [{ details: { relatedDocuments: { relatedTicketDocuments: null } } }]
};

export const adapterViewTicketFunctionsMapper: AdapterMapper<SummaryTicket> = {
  taxes: taxFeeTicketViewAdapter,
  id: idTicketViewAdapter,
  fullDocumentNumber: fullDocumentNumberAdapter,
  documentNumbers: DocumentNumbersAdapter
};
