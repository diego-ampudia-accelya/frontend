/* eslint-disable @typescript-eslint/ban-types */
import {
  Calculation,
  RelatedTicketDocument,
  TaxMiscellaneousFee,
  TaxMiscellaneousTicketFee,
  TicketTaxType
} from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import {
  getDefaultCalculationFieldValueBE,
  getDefaultCalculationFieldValueView,
  uppercaseAdapter
} from '~app/shared/helpers/form-helpers/adapter/view-be-adapter.helper';

export function calculationBEAdapter(calculations: Calculation): Calculation {
  return calculationAdapter(calculations, getDefaultCalculationFieldValueBE);
}

export function calculationViewAdapter(calculations: Calculation): Calculation {
  return calculationAdapter(calculations, getDefaultCalculationFieldValueView);
}

function calculationAdapter(calculations: Calculation, adapter: Function): Calculation {
  Object.keys(calculations).forEach(field => {
    calculations[field] = adapter(calculations[field]);
  });

  return calculations;
}

export function netReportingBEAdapter(netReporting: boolean): boolean {
  return Boolean(netReporting);
}

export function taxFeeBEAdapter(taxFee: TaxMiscellaneousFee[]): TaxMiscellaneousFee[] {
  return taxFeeAdapter(taxFee, getDefaultCalculationFieldValueBE);
}

export function taxFeeViewAdapter(taxFee: TaxMiscellaneousFee[]): TaxMiscellaneousFee[] {
  return taxFeeAdapter(taxFee, getDefaultCalculationFieldValueView);
}

function taxFeeAdapter(taxFee: TaxMiscellaneousFee[], adapter: Function): TaxMiscellaneousFee[] {
  return (
    taxFee
      //* We remove empty lines from payload
      .filter(line => line.type)
      //* We set default values for calculations
      .map(line => {
        line.type = uppercaseAdapter(line.type);
        line.agentAmount = adapter(line.agentAmount);
        line.airlineAmount = adapter(line.airlineAmount);
        line.taxDifferenceAmount = adapter(line.taxDifferenceAmount);

        return line;
      })
  );
}

export function taxFeeTicketViewAdapter(taxFee: TaxMiscellaneousTicketFee[]): TaxMiscellaneousFee[] {
  return taxFeeTicketAdapter(taxFee, getDefaultCalculationFieldValueView);
}

export function fullDocumentNumberAdapter(documents: any): RelatedTicketDocument[] {
  return [{ relatedTicketDocumentNumber: documents, documentId: null }];
}

export function DocumentNumbersAdapter(documents: any): RelatedTicketDocument[] {
  const documentNumbers: RelatedTicketDocument[] = [];

  documents.forEach(document => {
    const documentNumber = { relatedTicketDocumentNumber: document, documentId: null };
    documentNumbers.push(documentNumber);
  });

  return documentNumbers;
}

// We update ACDM form id to null, id retrieved from ticket is refered to document id
export function idTicketViewAdapter(): any {
  return null;
}

function taxFeeTicketAdapter(taxFee: TaxMiscellaneousTicketFee[], adapter: Function): TaxMiscellaneousFee[] {
  return (
    taxFee
      //* We remove empty lines from payload
      .filter(line => line.type && line.type !== TicketTaxType.CP && line.type !== TicketTaxType.MF)
      //* We set default values for calculations
      .map(line => ({
        type: uppercaseAdapter(line.type),
        agentAmount: adapter(line.amount),
        airlineAmount: adapter(0),
        taxDifferenceAmount: adapter(line.amount)
      }))
  );
}
