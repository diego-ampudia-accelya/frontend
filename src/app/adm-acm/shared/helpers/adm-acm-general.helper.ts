import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot } from '@angular/router';
import { capitalize } from 'lodash';

import { AcdmActionType, RelatedTicketDocument } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AgentViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NumberHelper } from '~app/shared/helpers/numberHelper';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MenuTab, MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';

export function roundCalculationValue(value, decimals: number): string {
  const rounded = NumberHelper.roundString(value, decimals);

  return isNaN(rounded as any) ? value : rounded;
}

export function createAdmAcmService(http: HttpClient, activatedRouteSnapshot: ActivatedRouteSnapshot) {
  return new AdmAcmService(http, null, activatedRouteSnapshot);
}

export function getAdmFamily(): Array<MasterDataType> {
  return [MasterDataType.Adm, MasterDataType.Spdr, MasterDataType.Admq, MasterDataType.Admd];
}

export function getUserDetails(address: AgentAddress | AirlineAddress) {
  let details = {};

  if (address) {
    const { address1, address2, street, city, country, postalCode, telephone } = address as AgentAddress &
      AirlineAddress;

    details = {
      telephone,
      address: !address2 ? address1 : `${address1} ${address2}`,
      street,
      city,
      country,
      postalCode
    };
  }

  return details;
}

export function getBspNameFromAgent(agent: AgentViewModel): string {
  return `${capitalize(agent.bsp.name)} (${agent.bsp.isoCountryCode})`;
}

export function fromGridTableActionToAcdmAction(gridAction: GridTableActionType): AcdmActionType {
  const mapper: { [key in GridTableActionType]?: AcdmActionType } = {
    [GridTableActionType.Details]: AcdmActionType.details,
    [GridTableActionType.DisputeACDM]: AcdmActionType.dispute,
    [GridTableActionType.RejectACDM]: AcdmActionType.reject,
    [GridTableActionType.ApproveACDM]: AcdmActionType.approve,
    [GridTableActionType.DeactivateACDM]: AcdmActionType.deactivate,
    [GridTableActionType.ReactivateACDM]: AcdmActionType.reactivate,
    [GridTableActionType.RejectRequest]: AcdmActionType.rejectRequest,
    [GridTableActionType.AuthorizeRequest]: AcdmActionType.authorizeRequest,
    [GridTableActionType.ForwardGdsADM]: AcdmActionType.forwardGds,
    [GridTableActionType.Delete]: AcdmActionType.delete,
    [GridTableActionType.EditReactivateACDM]: AcdmActionType.editReactivateACDM,
    [GridTableActionType.EditRejectACDM]: AcdmActionType.editRejectACDM,
    [GridTableActionType.Supervise]: AcdmActionType.supervise,
    [GridTableActionType.Archive]: AcdmActionType.archive,
    [GridTableActionType.MoveToActive]: AcdmActionType.moveToActive,
    [GridTableActionType.MarkAsRead]: AcdmActionType.markAsRead,
    [GridTableActionType.MarkAsUnread]: AcdmActionType.markAsUnread,
    [GridTableActionType.StartWatching]: AcdmActionType.startWatching,
    [GridTableActionType.StopWatching]: AcdmActionType.stopWatching
  };

  return mapper[gridAction];
}

export function transformRelatedDocumentTicketToBE(
  ticketId: string,
  docNumberArray: string[] = []
): RelatedTicketDocument[] {
  const ticketDocArray = transformRelatedDocumentToBE(docNumberArray);

  if (ticketDocArray.length) {
    ticketDocArray[0].documentId = ticketId;
  }

  return ticketDocArray;
}

export function transformRelatedDocumentToBE(docNumberArray: string[]): RelatedTicketDocument[] {
  return docNumberArray.map(doc => ({ documentId: null, relatedTicketDocumentNumber: doc }));
}

export function getACDMTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;

  return { label: tab.tabLabel as string, params: { admAcmNumber: routeData.item.ticketDocumentNumber } };
}

export const anyOptionGdsForwardValue = 'not_blank';
