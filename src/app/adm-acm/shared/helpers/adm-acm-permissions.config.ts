import {
  getAcdmdCreationAgentPermission,
  getAcdmdCreationAirlPermission
} from '~app/bsp-adjustment/shared/helpers/acdmd-permissions.config';
import {
  getSpcdrCreationAgentPermission,
  getSpcdrCreationAirlPermission
} from '~app/bsp-adjustment/shared/helpers/spcdr-permissions.config';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { UserType } from '~app/shared/models/user.model';

export const commonAcdmPermission = {
  admIssue: 'cAdm',
  acmIssue: 'cAcm',
  admqIssue: 'cAdmq',
  acmqIssue: 'cAcmq'
};

export const issuePendingSupervisionPermission = {
  admSupIssue: 'cAdmSupv',
  acmSupIssue: 'cAcmSupv'
};

export const viewAcdmPermission = { admView: 'rAdm', acmView: 'rAcm', admqView: 'rAdmq', acmqView: 'rAcmq' };
export const agentAcdmPermission = {
  forwardGdsAdm: 'uAdmGdsFwd',
  viewAdmPolicies: 'rAdmPol'
};

export const admPoliciesPermissions = {
  createAdmPolicies: 'cAdmPolCfg',
  readAdmPolicies: 'rAdmPolCfg',
  updateAdmPolicies: 'uAdmPolCfg',
  deleteAdmPolicies: 'dAdmPolCfg'
};

export const updateAcdmStatusPermission = {
  admUpdateStatus: 'uAdmState',
  acmUpdateStatus: 'uAcmState',
  admqUpdateStatus: 'uAdmqState',
  acmqUpdateStatus: 'uAcmqState'
};

export const disputeAcdmPermission = {
  disputeAdm: 'uAdmDisp',
  disputeAcm: 'uAcmDisp'
};

export const viewAcdntPermission = {
  viewAdnt: 'rAdnt',
  viewAcnt: 'rAcnt'
};

export const supervisionAcdmPermission = {
  supervisionAdm: 'rAdmSupv',
  supervisionAcm: 'rAcmSupv'
};

export const supervisionDeletedAcdmPermission = {
  supervisionDeletedAdm: 'rAdmSupvDel',
  supervisionDeletedAcm: 'rAcmSupvDel'
};

export const updateAcdmSupervisionPermission = {
  supervisionUpdateAdm: 'uAdmSupvAuth',
  supervisionUpdateAcm: 'uAcmSupvAuth'
};

export const viewAdmForwardGdsPermission = 'rAdmGds';

export const createAdmGlobalPolicyFile = 'cGlobAdmPolFile';

export const acdmFilesPermissions = {
  readEv: 'rEv',
  readEcCnx: 'rEcCnx',
  createEcCnx: 'cEcCnx'
};

export const acdmCategorizationRules = {
  readCatRule: 'rAdmCatRule',
  createCatRule: 'cAdmCatRule',
  updateCatRule: 'uAdmCatRule',
  createCatRuleRerun: 'cAdmCatRerun'
};

export const readSpdcr = {
  readSpdr: 'rSpdr',
  readSpcr: 'rSpcr'
};

export const readAdcmd = {
  readAdmd: 'rAdmd',
  readAcmd: 'rAcmd'
};

export function getViewAcdmPermission(admAcmType: MasterDataType): string {
  switch (admAcmType) {
    case MasterDataType.Adm:
      return viewAcdmPermission.admView;
    case MasterDataType.Admq:
      return viewAcdmPermission.admqView;
    case MasterDataType.Acm:
      return viewAcdmPermission.acmView;
    case MasterDataType.Acmq:
      return viewAcdmPermission.acmqView;
  }
}

export function getIssueAllAcdmSpcdrAcdmdPermission(admAcmType: MasterDataType, userType: UserType): string {
  switch (admAcmType) {
    case MasterDataType.Adm:
    case MasterDataType.Admq:
    case MasterDataType.Acm:
    case MasterDataType.Acmq:
      return getIssueAcdmPermission(admAcmType);
    case MasterDataType.Acmd:
    case MasterDataType.Admd:
      return userType === UserType.AIRLINE
        ? getAcdmdCreationAirlPermission(admAcmType)
        : getAcdmdCreationAgentPermission(admAcmType);
    case MasterDataType.Spdr:
    case MasterDataType.Spcr:
      return userType === UserType.AIRLINE
        ? getSpcdrCreationAirlPermission(admAcmType)
        : getSpcdrCreationAgentPermission(admAcmType);
  }
}

export function getIssueAcdmPermission(admAcmType: MasterDataType): string {
  switch (admAcmType) {
    case MasterDataType.Adm:
      return commonAcdmPermission.admIssue;
    case MasterDataType.Admq:
      return commonAcdmPermission.admqIssue;
    case MasterDataType.Acm:
      return commonAcdmPermission.acmIssue;
    case MasterDataType.Acmq:
      return commonAcdmPermission.acmqIssue;
  }
}

export function getIssueAcdmPendingSupervisionPermission(admAcmType: MasterDataType): string {
  switch (admAcmType) {
    case MasterDataType.Adm:
      return issuePendingSupervisionPermission.admSupIssue;
    case MasterDataType.Acm:
      return issuePendingSupervisionPermission.acmSupIssue;
    default:
      return '';
  }
}

export function getUpdateAcdmPermissions(admAcmType: MasterDataType): string {
  switch (admAcmType) {
    case MasterDataType.Adm:
      return updateAcdmStatusPermission.admUpdateStatus;
    case MasterDataType.Admq:
      return updateAcdmStatusPermission.admqUpdateStatus;
    case MasterDataType.Acm:
      return updateAcdmStatusPermission.acmUpdateStatus;
    case MasterDataType.Acmq:
      return updateAcdmStatusPermission.acmqUpdateStatus;
  }
}

export function getViewAdmPolicies(): string {
  return agentAcdmPermission.viewAdmPolicies;
}

export function getViewAcdntPermission(admAcmType: MasterDataType): string {
  return admAcmType === MasterDataType.Acnt ? viewAcdntPermission.viewAcnt : viewAcdntPermission.viewAdnt;
}

export function getSupervisionAcdmPermissions(): Array<string> {
  return [supervisionAcdmPermission.supervisionAdm, supervisionAcdmPermission.supervisionAcm];
}

export function getAgentGroupAcdmqPermission(admAcmType: MasterDataType): string {
  return admAcmType === MasterDataType.Admq ? commonAcdmPermission.admqIssue : commonAcdmPermission.acmqIssue;
}

export function getSupervisionDeletedAcdmPermissions(): Array<string> {
  return [
    supervisionDeletedAcdmPermission.supervisionDeletedAdm,
    supervisionDeletedAcdmPermission.supervisionDeletedAcm
  ];
}

export function getAdmQueryPermissions(): Array<string> {
  return [
    viewAcdmPermission.admView,
    commonAcdmPermission.admIssue,
    supervisionAcdmPermission.supervisionAdm,
    issuePendingSupervisionPermission.admSupIssue,
    updateAcdmSupervisionPermission.supervisionUpdateAdm
  ];
}

export function getAcmQueryPermissions(): Array<string> {
  return [
    viewAcdmPermission.acmView,
    commonAcdmPermission.acmIssue,
    supervisionAcdmPermission.supervisionAcm,
    issuePendingSupervisionPermission.acmSupIssue,
    updateAcdmSupervisionPermission.supervisionUpdateAcm
  ];
}

export function getAcdmFilesPermissions(): Array<string> {
  return [acdmFilesPermissions.readEv, acdmFilesPermissions.readEcCnx];
}

export function getAcdmCategorizationRules(): Array<string> {
  return [acdmCategorizationRules.readCatRule, acdmCategorizationRules.createCatRule];
}

export function getUpdateAcdmCategorizationRules(): Array<string> {
  return [acdmCategorizationRules.updateCatRule];
}

export function getAcdmReadInternalCommentPermission(acdmType: MasterDataType): string {
  return acdmReadInternalCommentPermission[acdmType];
}

export function getAcdmCreateInternalCommentPermission(acdmType: MasterDataType): string {
  return acdmCreateInternalCommentPermission[acdmType];
}

export function getAcdmUpdateInternalCommentPermission(acdmType: MasterDataType): string {
  return acdmUpdateInternalCommentPermission[acdmType];
}

export function getAcdmDeleteInternalCommentPermission(acdmType: MasterDataType): string {
  return acdmDeleteInternalCommentPermission[acdmType];
}

export function getAcdmEditInternalCommentPermission(acdmType: MasterDataType): string[] {
  return [
    getAcdmCreateInternalCommentPermission(acdmType),
    getAcdmUpdateInternalCommentPermission(acdmType),
    getAcdmDeleteInternalCommentPermission(acdmType)
  ];
}

export function getBspFilterRequiredPermissions(adcmType: MasterDataType): string[] {
  const requiredPermissions = {
    [MasterDataType.Adm]: [supervisionAcdmPermission.supervisionAdm, viewAcdmPermission.admView],
    [MasterDataType.Acm]: [supervisionAcdmPermission.supervisionAcm, viewAcdmPermission.acmView],
    [MasterDataType.Adnt]: [viewAcdntPermission.viewAdnt],
    [MasterDataType.Acnt]: [viewAcdntPermission.viewAcnt],
    [MasterDataType.Admq]: [viewAcdmPermission.admqView],
    [MasterDataType.Acmq]: [viewAcdmPermission.acmqView],
    [MasterDataType.Spdr]: [readSpdcr.readSpdr],
    [MasterDataType.Spcr]: [readSpdcr.readSpcr],
    [MasterDataType.Admd]: [readAdcmd.readAdmd],
    [MasterDataType.Acmd]: [readAdcmd.readAcmd]
  };

  return requiredPermissions[adcmType];
}

const acdmReadInternalCommentPermission = {
  [MasterDataType.Adm]: 'rAdmIntCom',
  [MasterDataType.Acm]: 'rAcmIntCom',
  [MasterDataType.Admd]: 'rAdmdIntCom',
  [MasterDataType.Acmd]: 'rAcmdIntCom',
  [MasterDataType.Admq]: 'rAdmqIntCom',
  [MasterDataType.Acmq]: 'rAcmqIntCom',
  [MasterDataType.Adnt]: 'rAdntIntCom',
  [MasterDataType.Acnt]: 'rAcntIntCom',
  [MasterDataType.Spdr]: 'rSpdrIntCom',
  [MasterDataType.Spcr]: 'rSpcrIntCom'
};

const acdmCreateInternalCommentPermission = {
  [MasterDataType.Adm]: 'cAdmIntCom',
  [MasterDataType.Acm]: 'cAcmIntCom',
  [MasterDataType.Admd]: 'cAdmdIntCom',
  [MasterDataType.Acmd]: 'cAcmdIntCom',
  [MasterDataType.Admq]: 'cAdmqIntCom',
  [MasterDataType.Acmq]: 'cAcmqIntCom',
  [MasterDataType.Adnt]: 'cAdntIntCom',
  [MasterDataType.Acnt]: 'cAcntIntCom',
  [MasterDataType.Spdr]: 'cSpdrIntCom',
  [MasterDataType.Spcr]: 'cSpcrIntCom'
};

const acdmUpdateInternalCommentPermission = {
  [MasterDataType.Adm]: 'uAdmIntCom',
  [MasterDataType.Acm]: 'uAcmIntCom',
  [MasterDataType.Admd]: 'uAdmdIntCom',
  [MasterDataType.Acmd]: 'uAcmdIntCom',
  [MasterDataType.Admq]: 'uAdmqIntCom',
  [MasterDataType.Acmq]: 'uAcmqIntCom',
  [MasterDataType.Adnt]: 'uAdntIntCom',
  [MasterDataType.Acnt]: 'uAcntIntCom',
  [MasterDataType.Spdr]: 'uSpdrIntCom',
  [MasterDataType.Spcr]: 'uSpcrIntCom'
};

const acdmDeleteInternalCommentPermission = {
  [MasterDataType.Adm]: 'dAdmIntCom',
  [MasterDataType.Acm]: 'dAcmIntCom',
  [MasterDataType.Admd]: 'dAdmdIntCom',
  [MasterDataType.Acmd]: 'dAcmdIntCom',
  [MasterDataType.Admq]: 'dAdmqIntCom',
  [MasterDataType.Acmq]: 'dAcmqIntCom',
  [MasterDataType.Adnt]: 'dAdntIntCom',
  [MasterDataType.Acnt]: 'dAcntIntCom',
  [MasterDataType.Spdr]: 'dSpdrIntCom',
  [MasterDataType.Spcr]: 'dSpcrIntCom'
};
