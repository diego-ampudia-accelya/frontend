/* eslint-disable @typescript-eslint/no-shadow */
import { Calculation, ConcernIndicator, TransactionCode } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';

/* eslint-disable no-shadow */
/* eslint-disable no-bitwise */

function acdmDifference(acdm: AdmAcmIssueBE) {
  const airlineCalcs = acdm.airlineCalculations;
  const agentCalcs = acdm.agentCalculations;
  const d: Partial<Calculation> = {};

  d.commission = airlineCalcs.commission - agentCalcs.commission;
  d.fare = airlineCalcs.fare - agentCalcs.fare;
  d.supplementaryCommission = airlineCalcs.supplementaryCommission - agentCalcs.supplementaryCommission;
  d.tax = airlineCalcs.tax - agentCalcs.tax;
  d.taxOnCommission = airlineCalcs.taxOnCommission - agentCalcs.taxOnCommission;
  if (airlineCalcs.cancellationPenalty != null && agentCalcs.cancellationPenalty != null) {
    d.cancellationPenalty = airlineCalcs.cancellationPenalty - agentCalcs.cancellationPenalty;
  } else {
    d.cancellationPenalty = null;
  }
  if (airlineCalcs.miscellaneousFee != null && agentCalcs.miscellaneousFee != null) {
    d.miscellaneousFee = airlineCalcs.miscellaneousFee - agentCalcs.miscellaneousFee;
  } else {
    d.miscellaneousFee = null;
  }

  return d;
}

// Not used code for now
/*
function acdmAbsDifference(acdm: AdmAcmIssueBE) {
  var calcs = acdmDifference(acdm);
  calcs.commission = Math.abs(calcs.commission);
  calcs.fare = Math.abs(calcs.fare);
  calcs.supplementaryCommission = Math.abs(calcs.supplementaryCommission);
  calcs.tax = Math.abs(calcs.tax);
  calcs.taxOnCommission = Math.abs(calcs.taxOnCommission);
  if (calcs.cancellationPenalty != null) {
    calcs.cancellationPenalty = Math.abs(calcs.cancellationPenalty);
  } else {
    calcs.cancellationPenalty = null;
  }
  if (calcs.miscellaneousFee) {
    calcs.miscellaneousFee = Math.abs(calcs.miscellaneousFee);
  } else {
    calcs.miscellaneousFee = null;
  }

  return calcs;
}

function acdmTotal(acdm, isPositiveVat) {
  var result =
    acdmCalculationsTotal(acdm.airlineCalculations, isPositiveVat) -
    acdmCalculationsTotal(acdm.agentCalculations, isPositiveVat);
  if ((acdm.concernsIndicator == 'REFUND') ^ ('ACMA' == acdm.transactionCode)) {
    result *= -1;
  }

  return result;
}

function acdmCalculationsTotal(calculations, isPositiveVat) {
  var result = calculations.fare + calculations.tax - calculations.commission - calculations.supplementaryCommission;
  if (isPositiveVat) {
    result = result + calculations.taxOnCommission;
  } else {
    result = result - calculations.taxOnCommission;
  }
  if (calculations.cancellationPenalty != null) {
    result = result - calculations.cancellationPenalty;
  }
  if (calculations.miscellaneousFee != null) {
    result = result - calculations.miscellaneousFee;
  }

  return result;
}
*/

export function isRegularized(acdm: AdmAcmIssueBE, isPositiveVat: boolean): boolean {
  function signum(acdm) {
    function sgn(x) {
      if (x === undefined || x === null || x === 0) {
        return 0;
      }
      if (x < 0) {
        return -1;
      }

      return 1;
    }

    const calcs = acdmDifference(acdm);
    const s: any = {};
    s.fare = sgn(calcs.fare);
    s.tax = sgn(calcs.tax);
    s.commission = sgn(calcs.commission);
    s.supplementaryCommission = sgn(calcs.supplementaryCommission);
    s.totalCommission = sgn(calcs.commission + calcs.supplementaryCommission);
    s.taxOnCommission = sgn(calcs.taxOnCommission);

    if (calcs.cancellationPenalty != null) {
      s.cancellationPenalty = sgn(calcs.cancellationPenalty);
    } else {
      s.cancellationPenalty = 0;
    }
    if (calcs.miscellaneousFee != null) {
      s.miscellaneousFee = sgn(calcs.miscellaneousFee);
    } else {
      s.miscellaneousFee = 0;
    }

    return s;
  }

  function negate(s) {
    s.fare *= -1;
    s.tax *= -1;
    s.commission *= -1;
    s.supplementaryCommission *= -1;
    s.totalCommission *= -1;
    s.taxOnCommission *= -1;
    s.cancellationPenalty *= -1;
    s.miscellaneousFee *= -1;
  }

  function regularizedSignum(s, isPositiveVat, isPosNeg): boolean {
    function regularizedCommissionsWithNonZeroTdam(s, isPositiveVat, isPosNeg): boolean {
      if (s.supplementaryCommission < 0 || (s.taxOnCommission < 0 && s.commission >= 0)) {
        return true;
      }
      if (isPosNeg) {
        return (
          (!isPositiveVat && s.commission < 0 && s.taxOnCommission > 0) ||
          (isPositiveVat && s.commission < 0 && s.taxOnCommission < 0)
        );
      }

      return s.commission < 0 || s.taxOnCommission < 0;
    }
    function regularizedCommissionsWithZeroTdam(s, isPositiveVat) {
      return (
        s.commission > 0 ||
        s.supplementaryCommission > 0 ||
        (s.taxOnCommission > 0 && s.totalCommission !== 0) ||
        (isPositiveVat && s.commission === 0 && s.supplementaryCommission === 0 && s.taxOnCommission < 0) ||
        (!isPositiveVat && s.commission === 0 && s.supplementaryCommission === 0 && s.taxOnCommission > 0)
      );
    }
    if (s.fare < 0 || s.tax < 0 || s.cancellationPenalty > 0 || s.miscellaneousFee > 0) {
      return true;
    }
    if (s.fare === 0 && s.tax === 0 && s.cancellationPenalty === 0 && s.miscellaneousFee === 0) {
      return regularizedCommissionsWithZeroTdam(s, isPositiveVat);
    }

    return regularizedCommissionsWithNonZeroTdam(s, isPositiveVat, isPosNeg);
  }

  const isPosNeg = acdm.transactionCode === TransactionCode.adm || acdm.transactionCode === TransactionCode.acm;
  const signumValue = signum(acdm);

  const isConcernsRefund = acdm.concernsIndicator === ConcernIndicator.refund ? 1 : 0;
  const isTransactionCodeAcm = TransactionCode.acm === acdm.transactionCode ? 1 : 0;

  if (isConcernsRefund ^ isTransactionCodeAcm) {
    negate(signumValue);
  }

  return regularizedSignum(signumValue, isPositiveVat, isPosNeg);
}
