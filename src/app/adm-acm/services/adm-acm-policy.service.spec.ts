import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { AdmAcmPolicyModel } from '../models/adm-acm-policy.model';
import { AdmAcmPolicyService } from './adm-acm-policy.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';

describe('AdmAcmPolicyService', () => {
  let service: AdmAcmPolicyService;

  const httpClientSpy = createSpyObject(HttpClient);
  const activatedRouteStub = { snapshot: { data: { admAcmType: MasterDataType.Adm } } };

  const policyListMock: AdmAcmPolicyModel[] = [
    {
      id: 6983484849,
      url: 'https://accelya.com/agencyreference/es-ES/#/home',
      email: '',
      comment: '',
      lastUpdate: '2017-01-29',
      bsp: { id: 1234, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: {
        id: 12334574,
        iataCode: '111',
        bsp: { version: 0 },
        localName: 'Airline Name 111',
        designator: 'L8',
        address: null,
        contact: null
      }
    },
    {
      id: 6983484954,
      url: 'https://www.accelya.com/web/en-US/content/agency/bookticket/default.aspx',
      email: '',
      comment: '',
      lastUpdate: '2018-06-02',
      bsp: { id: 1234, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: {
        id: 12334574,
        iataCode: '111',
        bsp: { version: 0 },
        localName: 'Airline Name 111',
        designator: 'L8',
        address: null,
        contact: null
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AdmAcmPolicyService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(AdmAcmPolicyService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should set correct HTTP request base url', () => {
    expect(service.baseUrl).toBe('/acdm-management/adm-policies');
  });

  it('should call HTTP method with proper url and return correct policy list', fakeAsync(() => {
    let policyData: PagedData<AdmAcmPolicyModel>;

    const policyDataMock: PagedData<AdmAcmPolicyModel> = {
      records: policyListMock,
      total: policyListMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    const query: DataQuery = {
      filterBy: { iataCode: ['111'] },
      sortBy: [{ attribute: 'lastUpdate', sortType: SortOrder.Asc }],
      paginateBy: { page: 0, size: 20 }
    };

    httpClientSpy.get.and.returnValue(of(policyDataMock));

    service.find(query).subscribe(results => (policyData = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/acdm-management/adm-policies?page=0&size=20&sort=lastUpdate,ASC&iataCode=111'
    );
    expect(policyData).toEqual(policyDataMock);
  }));

  it('should format filter dates properly', () => {
    const queryFilter = {
      iataCode: ['111'],
      lastUpdate: new Date('2017-04-10')
    };

    expect(service['formatDateFilter'](queryFilter)).toEqual({ ...queryFilter, lastUpdate: '2017-04-10' } as any);
  });

  it('should call HTTP method with proper url and return correct downloaded file', fakeAsync(() => {
    let policyDownload: { blob: Blob; fileName: string };

    const fileName = 'Adm-policy-query-20200811153638.txt';
    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({
        'Some-Header': 'some header value',
        'Content-Disposition': `filename="${fileName}"`
      })
    });

    const query: DataQuery = {
      filterBy: { iataCode: ['111'] },
      sortBy: [{ attribute: 'lastUpdate', sortType: SortOrder.Asc }],
      paginateBy: { page: 0, size: 20 }
    };

    const policyDownloadMock: { blob: Blob; fileName: string } = {
      blob: new Blob([null], { type: 'application/zip' }),
      fileName
    };

    httpClientSpy.get.and.returnValue(of(httpResponse));

    service.download(query, DownloadFormat.TXT).subscribe(result => (policyDownload = result));

    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/acdm-management/adm-policies/download?page=0&size=20&sort=lastUpdate,ASC&iataCode=111&exportAs=txt',
      jasmine.anything()
    );
    expect(policyDownload).toEqual(policyDownloadMock);
  }));
});
