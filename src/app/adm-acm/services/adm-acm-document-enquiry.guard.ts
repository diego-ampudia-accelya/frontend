import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, Observable, of } from 'rxjs';
import { finalize, first, switchMap, tap } from 'rxjs/operators';

import { AdmAcmDocumentEnquiryDialogComponent } from '../components/dialogs/adm-acm-document-enquiry-dialog/adm-acm-document-enquiry-dialog.component';
import { AdmAcmDocumentSummary, AdmAcmEnquiryDialogData } from '../models/adm-acm-enquiry-document.model';
import { AdmAcmDocumentEnquiryService } from './adm-acm-document-enquiry.service';
import { DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants';
import { PagedData } from '~app/shared/models/paged-data.model';

@Injectable()
export class AdmAcmDocumentEnquiryGuard implements CanActivate {
  constructor(
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private router: Router,
    private admAcmDocumentEnquiryService: AdmAcmDocumentEnquiryService
  ) {}

  public canActivate(next: ActivatedRouteSnapshot): Observable<boolean> {
    return this.openDialogObservable().pipe(
      switchMap(
        ({
          clickedBtn,
          contentComponentRef
        }: {
          clickedBtn: FooterButton;
          contentComponentRef: AdmAcmDocumentEnquiryDialogComponent;
        }) => {
          if (clickedBtn === FooterButton.Search) {
            return this.checkDocumentExistence(contentComponentRef).pipe(
              tap(isDocumentFound => {
                if (isDocumentFound) {
                  // Retrieve documentNumber from Dialog Form
                  const { documentNumber } = contentComponentRef.form.value;

                  next.data = { ...next.data, documentNumber };

                  this.dialogService.close();
                  this.router.onSameUrlNavigation = 'reload';
                  this.router.routeReuseStrategy.shouldReuseRoute = () => false; // It is necessary to reload the tab when a tab is already open.
                  this.router.navigateByUrl(`${ROUTES.ACDM_DOCUMENT_ENQUIRY_LIST.url}/${documentNumber}`);

                  return of(true);
                }
              })
            );
          }

          if (clickedBtn === FooterButton.Cancel) {
            return of(false);
          }

          return EMPTY;
        }
      ),
      first()
    );
  }

  private openDialogObservable(): Observable<any> {
    const title = this.translationService.translate('ADM_ACM.documentEnquiry.dialog.title');

    return this.dialogService.open(AdmAcmDocumentEnquiryDialogComponent, {
      data: {
        title,
        isSearching: false,
        mainButtonType: FooterButton.Search,
        footerButtonsType: [
          { type: FooterButton.Search, isDisabled: true, tooltipText: 'ADM_ACM.documentEnquiry.dialog.tooltipMessage' }
        ]
      } as AdmAcmEnquiryDialogData
    });
  }

  private checkDocumentExistence(contentComponentRef: AdmAcmDocumentEnquiryDialogComponent): Observable<boolean> {
    contentComponentRef.setIsSearching(true);

    // Retrieve documentNumber from Dialog Form
    const { documentNumber } = contentComponentRef.form.value;

    return this.admAcmDocumentEnquiryService.getBy(documentNumber).pipe(
      switchMap((documents: PagedData<AdmAcmDocumentSummary>) => {
        contentComponentRef.dialogData.isDocumentFound = true;
        if (!documents?.records?.length) {
          contentComponentRef.dialogData.isDocumentFound = false;
        }

        return contentComponentRef.dialogData.isDocumentFound
          ? of(contentComponentRef.dialogData.isDocumentFound)
          : EMPTY;
      }),
      finalize(() => contentComponentRef.setIsSearching(false))
    );
  }
}
