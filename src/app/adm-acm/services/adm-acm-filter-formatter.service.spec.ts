import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AdmAcmFilter } from '../models/adm-acm-filter.model';
import { AdmAcmStatus, ConcernIndicator, SendToDpcType } from '../models/adm-acm-issue-shared-aux.model';
import { AdmAcmFilterFormatter } from './adm-acm-filter-formatter.service';
import { PeriodOption } from '~app/shared/components';
import { AppliedFilter } from '~app/shared/components/list-view';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

describe('AdmAcmFilterFormatterService', () => {
  let formatter: AdmAcmFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const periodPipeSpy = createSpyObject(PeriodPipe);

  const requiredAdmAcmFilter: AdmAcmFilter = {
    ticketDocumentNumber: '0006220565',
    acdmStatus: [AdmAcmStatus.pending, AdmAcmStatus.disputed],
    currencyId: [
      { id: 1, code: 'EUR', decimals: 3 },
      { id: 2, code: 'GBP', decimals: 2 }
    ],
    concernsIndicator: [ConcernIndicator.issue, ConcernIndicator.refund]
  };

  const requiredFilterResult: AppliedFilter[] = [
    {
      keys: ['ticketDocumentNumber'],
      label: 'ADM_ACM.query.filters.labels.docNumber - 0006220565'
    },
    {
      keys: ['acdmStatus'],
      label: 'ADM_ACM.query.filters.labels.status - ADM_ACM.status.PENDING, ADM_ACM.status.DISPUTED'
    },
    {
      keys: ['currencyId'],
      label: 'ADM_ACM.query.filters.labels.currency - EUR, GBP'
    },
    {
      keys: ['concernsIndicator'],
      label: 'ADM_ACM.query.filters.labels.rtdnType - ADM_ACM.acdmFor.ISSUE, ADM_ACM.acdmFor.REFUND'
    }
  ];

  const mockPeriodPipeResult = 'MOCK PERIODS';

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    periodPipeSpy.transform.and.callFake(() => mockPeriodPipeResult);

    formatter = new AdmAcmFilterFormatter(translationServiceSpy, periodPipeSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format required fields', () => {
    expect(formatter.format(requiredAdmAcmFilter)).toEqual(requiredFilterResult);
  });

  it('should format specified fields if required filters are missing', () => {
    const filter: any = {
      ticketDocumentNumber: '0006220565',
      sentToDpc: [SendToDpcType.sent]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['ticketDocumentNumber'],
        label: 'ADM_ACM.query.filters.labels.docNumber - 0006220565'
      },
      {
        keys: ['sentToDpc'],
        label: 'ADM_ACM.query.filters.labels.sentToDpc - ADM_ACM.sentToDpcStatus.SENT'
      }
    ]);
  });

  it('should format required fields and airlines if they exist', () => {
    const filter: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      airlineIataCode: [
        { id: 1, name: 'AIRLINE NAME 220', code: '220', designator: 'L8' },
        { id: 2, name: 'AIRLINE NAME 005', code: '005', designator: 'L5' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['airlineIataCode'],
        label: 'ADM_ACM.query.filters.labels.airlineTag - 220, 005'
      }
    ]);
  });

  it('should format required fields and agents if they exist', () => {
    const filter: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      agentId: [
        { id: '1', name: 'AGENT NAME 7820010', code: '7820010' },
        { id: '2', name: 'AGENT NAME 3406700', code: '3406700' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['agentId'],
        label: 'ADM_ACM.query.filters.labels.agentTag - 7820010, 3406700'
      }
    ]);
  });

  it('should format required fields and BSPs if they exist', () => {
    const filter: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      bsp: [
        { id: 1, isoCountryCode: 'ES', name: 'SPAIN' },
        { id: 2, isoCountryCode: 'IL', name: 'ISRAEL' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['bsp'],
        label: 'ADM_ACM.query.filters.labels.countryTag - SPAIN (ES), ISRAEL (IL)'
      }
    ]);
  });

  it('should format required fields and sentToDpc status if they exist', () => {
    const filter: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      sentToDpc: [SendToDpcType.pending]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['sentToDpc'],
        label: 'ADM_ACM.query.filters.labels.sentToDpc - ADM_ACM.sentToDpcStatus.PENDING'
      }
    ]);
  });

  it('should format required fields and hasInternalComment if they exist', () => {
    const filter: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      hasInternalComment: true
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['hasInternalComment'],
        label: `ADM_ACM.query.filters.labels.internalComment - ADM_ACM.yesNoBooleanValue.true`
      }
    ]);
  });

  it('should format required fields and date of issue if it exists with or without range', () => {
    // Date range has been specified
    const filterWithRangeDate: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      dateOfIssue: [new Date('2020/07/14'), new Date('2020/07/21')]
    };
    // Unique date has been specified
    const filterWithUniqueDate: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      dateOfIssue: [new Date('2020/07/14')]
    };

    expect(formatter.format(filterWithRangeDate)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['dateOfIssue'],
        label: 'ADM_ACM.query.filters.labels.issueDate - 14/07/2020 - 21/07/2020'
      }
    ]);

    expect(formatter.format(filterWithUniqueDate)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['dateOfIssue'],
        label: 'ADM_ACM.query.filters.labels.issueDate - 14/07/2020'
      }
    ]);
  });

  it('should format required fields and  period if it exists', () => {
    const periods: PeriodOption[] = [
      { period: '2020011', dateFrom: '01/01/2020', dateTo: '01/07/2020' },
      { period: '2020014', dateFrom: '01/24/2020', dateTo: '01/31/2020' }
    ];

    const filter: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      period: periods
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      { keys: ['period'], label: mockPeriodPipeResult }
    ]);
  });

  it('should format required fields and reporting date if it exists with or without range', () => {
    // Date range has been specified
    const filterWithRangeDate: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      reportingDate: [new Date('2020/07/14'), new Date('2020/07/21')]
    };
    // Unique date has been specified
    const filterWithUniqueDate: AdmAcmFilter = {
      ...requiredAdmAcmFilter,
      reportingDate: [new Date('2020/07/14')]
    };

    expect(formatter.format(filterWithRangeDate)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['reportingDate'],
        label: 'ADM_ACM.query.filters.labels.repDate - 14/07/2020 - 21/07/2020'
      }
    ]);

    expect(formatter.format(filterWithUniqueDate)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['reportingDate'],
        label: 'ADM_ACM.query.filters.labels.repDate - 14/07/2020'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: AdmAcmFilter = {
      ticketDocumentNumber: '',
      acdmStatus: [],
      currencyId: [],
      concernsIndicator: null
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      ticketDocumentNumber: '0006220565',
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['ticketDocumentNumber'],
        label: 'ADM_ACM.query.filters.labels.docNumber - 0006220565'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
