import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AdmAcmMassloadAttachmentFilter } from '../models/adm-acm-massload-attachment-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AdmAcmMassloadAttachmentFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: Partial<AdmAcmMassloadAttachmentFilter>): AppliedFilter[] {
    const filterMappers: FilterMappers<AdmAcmMassloadAttachmentFilter> = {
      filename: filename => `${this.translate('filename')} - ${filename}`,
      documentNumber: documentNumber => `${this.translate('documentNumber')} - ${documentNumber}`,
      uploadDate: date => `${this.translate('uploadDate')} - ${rangeDateFilterTagMapper(date)}`,
      userName: userName => `${this.translate('userName')} - ${userName}`,
      email: email => `${this.translate('email')} - ${email}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`ADM_ACM.files.massloadAttachments.filters.${key}.label`);
  }
}
