import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { MenuItem } from 'primeng/api';
import { BehaviorSubject, combineLatest, Observable, of, ReplaySubject, Subject } from 'rxjs';
import { catchError, filter, first, map, switchMap, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

import {
  AcdmActionType,
  AcdmNumberingMode,
  AirlineConfigParamName,
  Calculation,
  ConcernIndicator,
  getUrlAcdmIndexed,
  TaxMiscellaneousFee,
  TransactionCode,
  urlList,
  urlView
} from '../models/adm-acm-issue-shared-aux.model';
import {
  AdmAcmAirlineConfig,
  AdmAcmBspConfig,
  AirlineViewModel,
  AmountCalculationFieldConfig,
  StatusHeaderViewModel,
  TocaTypeViewModel
} from '../models/adm-acm-issue-view-aux.model';
import { AdmAcmActionEmitterType, AdmAcmIssueBE, TocaErrorsType } from '../models/adm-acm-issue.model';
import { fromGridTableActionToAcdmAction, getAdmFamily } from '../shared/helpers/adm-acm-general.helper';
import { isRegularized } from '../shared/helpers/adm-acm-regularized.helper';
import { AdmAcmDataService } from './adm-acm-data.service';
import { AdmAcmDialogService } from './adm-acm-dialog.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabsActions } from '~app/shared/base/list-subtabs/actions';
import { GLOBALS } from '~app/shared/constants/globals';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus } from '~app/shared/models/mark-status.model';
import { TabService } from '~app/shared/services';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable()
export class AdmAcmConfigService implements OnDestroy {
  public accessType: AccessType;
  public airlineIssued: boolean;

  private _admAcmType: MasterDataType;
  public get admAcmType(): MasterDataType {
    return this._admAcmType;
  }
  public set admAcmType(admAcmType: MasterDataType) {
    if (admAcmType !== this._admAcmType) {
      this._admAcmType = admAcmType;
      this.updateTypeConfig();
    }
  }

  /**
   * Gets reverse order factor
   *
   * @returns `1` when order is Agent - Airline (Acm's), `-1` when order is Airline - Agent (Adm's)
   */
  public get reverseOrderFactor(): 1 | -1 {
    return getAdmFamily().includes(this.admAcmType) ? -1 : 1;
  }

  public get isAcdm(): boolean {
    return this.admAcmType === MasterDataType.Adm || this.admAcmType === MasterDataType.Acm;
  }

  public get isAcdmd(): boolean {
    return this.admAcmType === MasterDataType.Admd || this.admAcmType === MasterDataType.Acmd;
  }

  public get isAcdmRequest(): boolean {
    return this.admAcmType === MasterDataType.Admq || this.admAcmType === MasterDataType.Acmq;
  }

  public get isAcdnt(): boolean {
    return this.admAcmType === MasterDataType.Adnt || this.admAcmType === MasterDataType.Acnt;
  }

  public get isSpcdr(): boolean {
    return this.admAcmType === MasterDataType.Spdr || this.admAcmType === MasterDataType.Spcr;
  }

  public get isReadonlyMode(): boolean {
    return this.accessType === AccessType.read;
  }

  public get isCreateMode(): boolean {
    return this.accessType === AccessType.create;
  }

  public get isEditMode(): boolean {
    return this.accessType === AccessType.edit;
  }

  public get isEditTicketMode(): boolean {
    return this.accessType === AccessType.editTicket;
  }

  public issuingPendingSupervision: boolean;

  public bspConfig: ReplaySubject<AdmAcmBspConfig>;
  public airlineConfig: ReplaySubject<AdmAcmAirlineConfig>;

  public dialogActionEmitter = new Subject<AcdmActionType>();

  private initialAmountConfigFields: AmountCalculationFieldConfig;

  //* BehaviourSubject because we want always a default value to be provided to components
  private amountConfigFieldsSubj: BehaviorSubject<AmountCalculationFieldConfig>;

  private destroy$ = new Subject();

  private latencyDaysSubj: ReplaySubject<number>;

  private handlingFeeSubj: ReplaySubject<boolean>;
  private penaltyChargeSubj: ReplaySubject<boolean>;

  constructor(
    private formData: AdmAcmDataService,
    private acdmDialogService: AdmAcmDialogService,
    public translationService: L10nTranslationService,
    private http: HttpClient,
    private router: Router,
    private store: Store<AppState>,
    private tabService: TabService
  ) {
    this.initializeDataSubjects();
    this.initializeAmountConfig();
    this.initializeBspConfigListener();

    //* ACDM airline configurations do not apply to ACDMD types for now
    if (!this.isAcdmd) {
      this.initializeAirlineConfigListener();
    }
  }

  public getAmountCalculationFields(): Array<keyof Calculation> {
    return (
      [
        'fare',
        'tax',
        'commission',
        'taxOnCommission',
        'supplementaryCommission',
        'cancellationPenalty',
        'miscellaneousFee'
      ] as Array<keyof Calculation>
    ).sort(this.sortCalculationFields());
  }

  public updateTypeConfig() {
    if (this.isAcdmd) {
      this.amountConfigFieldsSubj.pipe(first()).subscribe(config => {
        Object.keys(config).forEach(field => {
          config[field].visible = of((field as keyof Calculation) === 'fare');
        });

        this.amountConfigFieldsSubj.next(config);
      });
    }
  }

  public getAmountConfigurationCalculationFields(): Observable<AmountCalculationFieldConfig> {
    return this.amountConfigFieldsSubj.asObservable();
  }

  public updateVisiblityCalculationField(field: keyof Calculation, visibility: Observable<boolean>) {
    const config = this.amountConfigFieldsSubj.value;
    config[field].visible = visibility;

    this.amountConfigFieldsSubj.next(config);
  }

  // TODO Default config. Will be changed in future. Update testing function then
  public getTaxAmountCalculationFields(): Array<keyof TaxMiscellaneousFee> {
    return ['type', 'taxDifferenceAmount'];
  }

  public getDecimalsPattern(): RegExp {
    return GLOBALS.PATTERNS.DECIMALS_PATTERN;
  }

  public getTocaTypes(bspsId): Observable<Array<{ value: string; label: string }>> {
    return this.getAcdmToca().pipe(switchMap(res => (res ? this.getTOCATypesDropdown(bspsId) : of([]))));
  }

  public getAdmaFor(): Observable<Array<{ value: string; label: string }>> {
    // TODO Add new value in future
    if (!this.isAcdmd) {
      return of([
        { value: 'ISSUE', label: this.translationService.translate('ADM_ACM.acdmFor.ISSUE') },
        { value: 'REFUND', label: this.translationService.translate('ADM_ACM.acdmFor.REFUND') },
        { value: 'EXCHANGE', label: this.translationService.translate('ADM_ACM.acdmFor.EXCHANGE') },
        { value: 'EMD', label: this.translationService.translate('ADM_ACM.acdmFor.EMD') }
      ]);
    } else {
      return of(null);
    }
  }

  public getTransactionCode(): TransactionCode {
    return TransactionCode[this.admAcmType];
  }

  public getStatOptions(): Observable<Array<DropdownOption & { selected: boolean }> | string> {
    const defaultStats = this.getDefaultStats();

    return this.bspConfig.pipe(
      map(config => {
        let resultStat;

        if (config.freeStat) {
          //* If freeStat, defaultStat will be returned to populate the input
          resultStat = config.defaultStat;
        } else {
          //* If not freeStat, we set the selected property on every option before returning the stats
          defaultStats.forEach(stat => (stat.selected = config.defaultStat === stat.value));
          resultStat = defaultStats;
        }

        return resultStat;
      })
    );
  }

  public getDefaultStats(): Array<DropdownOption & { selected: boolean }> {
    return [
      {
        value: 'I',
        label: this.translationService.translate('ADM_ACM.basicInfo.acdmProp.statValues.international'),
        selected: false
      },
      {
        value: 'D',
        label: this.translationService.translate('ADM_ACM.basicInfo.acdmProp.statValues.domestic'),
        selected: false
      }
    ];
  }

  public getStatusHeaderFields(): Array<keyof StatusHeaderViewModel> {
    const statusHeaderFields: Array<keyof StatusHeaderViewModel> = [
      'ticketDocumentNumber',
      'status',
      'bspName',
      'agentCode',
      'acdmaFor',
      'issueDate',
      'amount'
    ];

    return this.isAcdmd ? statusHeaderFields.filter(field => field !== 'acdmaFor') : statusHeaderFields;
  }

  public getLatencyDays(): Observable<number> {
    return this.latencyDaysSubj.asObservable();
  }

  public getAcdmSpam(): Observable<boolean> {
    return this.bspConfig.pipe(
      first(),
      map(config => config.acdmSpam)
    );
  }

  public getAcdmToca(): Observable<boolean> {
    return this.bspConfig.pipe(
      first(),
      map(config => config.acdmToca)
    );
  }

  public getPositiveVat(): Observable<boolean> {
    return this.bspConfig.pipe(map(config => config.positiveVat));
  }

  public formatHeaderBtnOptions(
    actions: { action: GridTableActionType; disabled?: boolean }[],
    acdm: AdmAcmIssueBE
  ): ButtonMenuOption[] & MenuItem[] {
    return actions
      .filter(action => this.isHeaderOptionFiltered(action.action))
      .map(action => ({
        label: this.translationService.translate(action.action.toString()),
        isDisabled: action.disabled,
        disabled: action.disabled,
        command: this.getActionCommand(action.action, acdm)
      }));
  }

  private isHeaderOptionFiltered(action: GridTableActionType): boolean {
    let optionIncluded;

    switch (action) {
      case GridTableActionType.Details:
      case GridTableActionType.InternalComment:
        optionIncluded = false;
        break;
      case GridTableActionType.ReactivateACDM:
      case GridTableActionType.RejectACDM:
        optionIncluded = !this.isEditMode;
        break;
      default:
        optionIncluded = true;
    }

    return optionIncluded;
  }

  public getActionCommand(
    action: GridTableActionType | AcdmActionType,
    acdmItem: AdmAcmIssueBE,
    actionEmitter: Subject<AcdmActionType> = this.dialogActionEmitter,
    listKey = ''
  ): () => void {
    const dialogActionType =
      fromGridTableActionToAcdmAction(action as GridTableActionType) || (action as AcdmActionType);
    let actionCommand: () => void;

    switch (dialogActionType) {
      case AcdmActionType.details:
        actionCommand = () => {
          this.router.navigate([urlView[this.admAcmType], acdmItem.id]);
        };
        break;

      case AcdmActionType.issue:
      case AcdmActionType.issuePendingSupervision:
      case AcdmActionType.supervise:
        actionCommand = () => {
          const urlToNavigate = getUrlAcdmIndexed(dialogActionType, this.admAcmType);
          //* If tab opened is Details and Supervise is clicked, the tab must be closed
          const shouldCloseCurrentTab$: Observable<boolean> =
            dialogActionType === AcdmActionType.supervise ? this.isDetailView() : of(false);

          this.isQueryView()
            .pipe(withLatestFrom(shouldCloseCurrentTab$))
            .subscribe(([shouldNavigate, shouldCloseCurrentTab]) => {
              if (shouldCloseCurrentTab) {
                this.tabService.closeCurrentTabAndNavigate(`${urlToNavigate}/${acdmItem.id}`);
              } else if (shouldNavigate) {
                this.router.navigate([urlToNavigate, acdmItem.id]);
              } else {
                const acdmItemUpdated = this.formData.getDataForSubmit();
                const showRegularizedMsg$ = this.getPositiveVat().pipe(
                  map(posVat => isRegularized(acdmItemUpdated, posVat))
                );
                const numberingConf$ = this.getNumberingConfiguration();

                this.acdmDialogService.openDialog(dialogActionType, acdmItemUpdated, this.admAcmType, this.formData, {
                  actionEmitter,
                  showRegularizedMsg$,
                  numberingConf$
                });
              }
            });
        };
        break;

      case AcdmActionType.editReactivateACDM:
      case AcdmActionType.editRejectACDM:
        actionCommand = () => {
          const urlToNavigate = getUrlAcdmIndexed(dialogActionType, this.admAcmType);
          const shouldCloseCurrentTab$: Observable<boolean> = this.isDetailView();

          this.isQueryView()
            .pipe(withLatestFrom(shouldCloseCurrentTab$))
            .subscribe(([shouldNavigate, shouldCloseCurrentTab]) => {
              if (shouldCloseCurrentTab) {
                this.tabService.closeCurrentTabAndNavigate(`${urlToNavigate}/${acdmItem.id}`);
              } else if (shouldNavigate) {
                this.router.navigate([urlToNavigate, acdmItem.id]);
              } else {
                const acdmItemUpdated = this.formData.getDataForSubmit();
                this.acdmDialogService.openDialog(dialogActionType, acdmItemUpdated, this.admAcmType, this.formData, {
                  actionEmitter
                });
              }
            });
        };
        break;

      case AcdmActionType.dispute:
      case AcdmActionType.reject:
      case AcdmActionType.approve:
      case AcdmActionType.deactivate:
      case AcdmActionType.reactivate:
      case AcdmActionType.rejectRequest:
      case AcdmActionType.authorizeRequest:
      case AcdmActionType.forwardGds:
      case AcdmActionType.delete:
        actionCommand = () => {
          this.acdmDialogService.openDialog(dialogActionType, acdmItem, this.admAcmType, this.formData, {
            actionEmitter
          });
        };
        break;

      case AcdmActionType.archive:
        actionCommand = () => {
          this.store.dispatch(
            ListSubtabsActions.markArchiveMoveToActive(listKey)({
              data: [acdmItem],
              markStatus: MarkStatus.Archive
            })
          );
        };
        break;

      case AcdmActionType.moveToActive:
        actionCommand = () => {
          this.store.dispatch(
            ListSubtabsActions.markArchiveMoveToActive(listKey)({
              data: [acdmItem],
              markStatus: MarkStatus.Read
            })
          );
        };
        break;

      case AcdmActionType.markAsRead:
        actionCommand = () => {
          this.store.dispatch(
            ListSubtabsActions.markReadUnreadStatus(listKey)({
              data: [acdmItem],
              markStatus: MarkStatus.Read
            })
          );
        };
        break;

      case AcdmActionType.markAsUnread:
        actionCommand = () => {
          this.store.dispatch(
            ListSubtabsActions.markReadUnreadStatus(listKey)({
              data: [acdmItem],
              markStatus: MarkStatus.Unread
            })
          );
        };
        break;

      case AcdmActionType.internalComment:
        actionCommand = () => {
          this.acdmDialogService.openInternalCommentDialog(acdmItem, this.admAcmType);
        };

        break;
      case AcdmActionType.activity:
        actionCommand = () => {
          this.acdmDialogService.openActivityHistoryDialog(acdmItem, this.admAcmType);
        };

        break;

      case AcdmActionType.startWatching:
        actionCommand = () => {
          this.store.dispatch(
            ListSubtabsActions.updateWatcher(listKey)({
              data: [acdmItem],
              watching: true
            })
          );
        };
        break;

      case AcdmActionType.stopWatching:
        actionCommand = () => {
          this.store.dispatch(
            ListSubtabsActions.updateWatcher(listKey)({
              data: [acdmItem],
              watching: false
            })
          );
        };
        break;

      default:
        actionCommand = () => {
          // Default action command case empty
        };
    }

    return actionCommand;
  }

  public getBspConf(bspId: number): Observable<AdmAcmBspConfig> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}/acdm-configuration`;

    return this.http.get(baseUrl).pipe(
      map((pagedData: AdmAcmBspConfig[]) => pagedData[0]),
      catchError(() => of(null))
    );
  }

  public getAirlConfig(airlineId: number): Observable<AdmAcmAirlineConfig> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/airlines/${airlineId}/configuration`;

    return this.http.get(baseUrl).pipe(
      map((pagedData: AdmAcmAirlineConfig) => pagedData),
      catchError(() => of(null))
    );
  }

  public getTOCATypesDropdown(bspId: number): Observable<Array<{ label: string; value: string }>> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}/acdm-tctp-configuration`;

    if (!this.isAcdmd) {
      return this.http.get<TocaTypeViewModel[]>(baseUrl).pipe(
        map((tocaList: Array<TocaTypeViewModel>) => tocaList.map(toc => ({ label: toc.type, value: toc.type }))),
        catchError(() =>
          // TODO Handle backend errors
          []
        )
      );
    } else {
      return of([]);
    }
  }

  public isAgentVatFieldVisible(agentVatValue: string): Observable<boolean> {
    return this.areVatFieldsVisible(agentVatValue, null).pipe(map(vatFields => vatFields[0]));
  }

  public isAirlineVatFieldVisible(airlVatValue: string): Observable<boolean> {
    return this.areVatFieldsVisible(null, airlVatValue).pipe(map(vatFields => vatFields[1]));
  }

  public isNetReportingVisible(netReportingValue: boolean): Observable<boolean> {
    return this.getAcdmSpam().pipe(
      map(acdmSpam => {
        let visible = acdmSpam;

        if (this.isReadonlyMode || this.isEditMode) {
          visible = visible || netReportingValue;
        }

        return visible;
      })
    );
  }

  public isRMICVisible(): boolean {
    const isHidden = this.isAcdmRequest || (this.isAcdnt && this.accessType === AccessType.read) || this.isAcdmd;

    return !isHidden;
  }

  public getNumberingConfiguration(): Observable<{ mode: AcdmNumberingMode; prefix: string; length: number }> {
    return this.bspConfig.pipe(
      map(config => {
        const numberingConfiguration = {
          mode: this.getNumberingMode(config.acdmCaptureMode),
          prefix: null,
          length: null
        };

        if (!this.isAcdmRequest && numberingConfiguration.mode === AcdmNumberingMode.manual) {
          numberingConfiguration.prefix = getAdmFamily().includes(this.admAcmType)
            ? config.admNumberPrefix
            : config.acmNumberPrefix;
          numberingConfiguration.length = getAdmFamily().includes(this.admAcmType)
            ? config.admNumberLength
            : config.acmNumberLength;
        }

        return numberingConfiguration;
      })
    );
  }

  private isQueryView(): Observable<boolean> {
    return this.tabService.isCurrentTabOnUrl(urlList[this.admAcmType]);
  }

  private isDetailView(): Observable<boolean> {
    return this.tabService.isCurrentTabOnUrl(getUrlAcdmIndexed(AcdmActionType.details, this.admAcmType));
  }

  private getNumberingMode(confNumberMode: string): AcdmNumberingMode {
    if (this.isAcdmRequest || confNumberMode === '8' || confNumberMode === '9') {
      return AcdmNumberingMode.automatic;
    }

    return AcdmNumberingMode.manual;
  }

  private areVatFieldsVisible(agentVatNumber?: string, airlineVatNumber?: string): Observable<Array<boolean>> {
    return this.bspConfig.pipe(
      map(config => {
        let isAgentVatFieldVisible = config.agentVatNumber;
        let isAirlineVatFieldVisible = config.airlineVatnumber;

        if (this.isReadonlyMode || this.isEditMode) {
          isAgentVatFieldVisible = config.agentVatNumber || !!agentVatNumber;
          isAirlineVatFieldVisible = config.airlineVatnumber || !!airlineVatNumber;
        }

        return [isAgentVatFieldVisible, isAirlineVatFieldVisible];
      })
    );
  }

  private initializeDataSubjects(): void {
    this.latencyDaysSubj = new ReplaySubject<number>(1);

    this.bspConfig = new ReplaySubject<AdmAcmBspConfig>(1);
    this.airlineConfig = new ReplaySubject<AdmAcmAirlineConfig>(1);

    this.handlingFeeSubj = new ReplaySubject<boolean>(1);
    this.penaltyChargeSubj = new ReplaySubject<boolean>(1);
  }

  private initializeAmountConfig() {
    this.initialAmountConfigFields = {
      fare: { sign: 1, visible: of(true) },
      tax: { sign: 1, visible: of(true) },
      commission: { sign: -1, visible: of(true) },
      taxOnCommission: {
        sign: 1,
        visible: this.formData.getSubject(AdmAcmActionEmitterType.hasToca).asObservable()
      },
      supplementaryCommission: {
        sign: -1,
        visible: this.formData.getSubject(AdmAcmActionEmitterType.netReporting).asObservable()
      },
      cancellationPenalty: {
        sign: -1,
        visible: this.isConcernsRefundFieldVisible('cancellationPenalty')
      },
      miscellaneousFee: {
        sign: -1,
        visible: this.isConcernsRefundFieldVisible('miscellaneousFee')
      }
    };

    this.amountConfigFieldsSubj = new BehaviorSubject<AmountCalculationFieldConfig>(this.initialAmountConfigFields);
  }

  private initializeBspConfigListener(): void {
    this.formData
      .getSubject(AdmAcmActionEmitterType.bsp)
      .pipe(
        filter<BspDto>(bsp => !!bsp),
        switchMap(bsp => this.getBspConf(bsp.id)),
        tap(bspConfig => {
          if (bspConfig) {
            this.updateAmountConfigFields(bspConfig.positiveVat);
            this.updateBspLatencyDays(bspConfig);
          } else {
            // TODO If bsp configuration is not available ACDM could not be submited.
            this.setDefaultBspConfigValues();
          }
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(bspConfig => {
        if (bspConfig) {
          this.bspConfig.next(bspConfig);
        }
      });
  }

  private initializeAirlineConfigListener() {
    this.formData
      .getSubject(AdmAcmActionEmitterType.airline)
      .pipe(
        filter<AirlineViewModel>(airl => !!airl),
        switchMap(airline => this.getAirlConfig(airline.id)),
        tap(airlConfig => {
          if (airlConfig) {
            this.updateHandlingFee(airlConfig);
            this.updatePenaltyCharge(airlConfig);
          }
        }),

        takeUntil(this.destroy$)
      )
      .subscribe(airlConfig => {
        this.airlineConfig.next(airlConfig);
      });
  }

  private updateHandlingFee(config: AdmAcmAirlineConfig) {
    if (config.parameters) {
      const feeValue = config.parameters.find(param => param.name === AirlineConfigParamName.handlingFee).value;

      if (feeValue) {
        this.handlingFeeSubj.next(feeValue === 'true');
      }
    }
  }

  private updatePenaltyCharge(config: AdmAcmAirlineConfig) {
    if (config.parameters) {
      const penaltyChargeValue = config.parameters.find(
        param => param.name === AirlineConfigParamName.penaltyCharge
      ).value;

      if (penaltyChargeValue) {
        this.penaltyChargeSubj.next(penaltyChargeValue === 'true');
      }
    }
  }

  private setDefaultBspConfigValues() {
    const config = this.amountConfigFieldsSubj.value;
    const defaultLatencyDays = 15;

    config.taxOnCommission.sign = 1;
    this.amountConfigFieldsSubj.next(config);
    this.latencyDaysSubj.next(defaultLatencyDays);
  }

  private updateAmountConfigFields(positiveVat: boolean) {
    const config = this.amountConfigFieldsSubj.value;
    const taxOnCommissionPreviousSign = config.taxOnCommission.sign;

    config.taxOnCommission.sign = positiveVat ? 1 : -1;

    if (taxOnCommissionPreviousSign !== config.taxOnCommission.sign) {
      this.amountConfigFieldsSubj.next(config);
    }
  }

  private updateBspLatencyDays(bspConfig: AdmAcmBspConfig) {
    const latencyDays = this.admAcmType === MasterDataType.Adm ? bspConfig.admLatencyDays : bspConfig.acmLatencyDays;
    this.latencyDaysSubj.next(latencyDays);
  }

  public resetData(): void {
    this.initializeDataSubjects();
  }

  public isConcernsRefundFieldVisible(
    field: (keyof Calculation & 'miscellaneousFee') | 'cancellationPenalty',
    ...amounts: number[]
  ): Observable<boolean> {
    const fieldSubject = field === 'miscellaneousFee' ? this.handlingFeeSubj : this.penaltyChargeSubj;
    let isConcernRefund: boolean;

    return this.isConcernRefund().pipe(
      tap(isCR => (isConcernRefund = isCR)),
      switchMap(() => fieldSubject),
      map(configValue => {
        let visible = isConcernRefund && configValue;

        if (isConcernRefund && (this.isEditMode || this.isReadonlyMode)) {
          visible = visible || this.isSomeValueGreaterThanZero(...amounts);
        }

        return visible;
      })
    );
  }

  private isSomeValueGreaterThanZero(...amounts: number[]): boolean {
    return amounts.some(amount => amount > 0);
  }

  private isConcernRefund(): Observable<boolean> {
    return this.formData
      .getSubject(AdmAcmActionEmitterType.acdmaFor)
      .pipe(map(concerns => concerns === ConcernIndicator.refund));
  }

  // TODO Move to async validator together with component methods logic
  public validateToca(): Observable<any> {
    return combineLatest([
      this.formData.getSubject(AdmAcmActionEmitterType.tocaType),
      this.formData.getSubject(AdmAcmActionEmitterType.tocaAmount)
    ]).pipe(
      map(retrieved => {
        // TOCA type selected

        if (retrieved[0] && !retrieved[1]) {
          return TocaErrorsType.amount;
        }

        if (retrieved[0] && retrieved[1] && Number(retrieved[1]) !== 0) {
          return null;
        }

        if (retrieved[0] && retrieved[1] && Number(retrieved[1]) === 0) {
          return TocaErrorsType.amount;
        }

        // No TOCA type selected

        if (!retrieved[0] && retrieved[1] && Number(retrieved[1]) !== 0) {
          return TocaErrorsType.type;
        }

        if (!retrieved[0] && retrieved[1] && Number(retrieved[1]) === 0) {
          return null;
        }

        if (!retrieved[0] && !retrieved[1]) {
          return null;
        }
      })
    );
  }

  private sortCalculationFields() {
    const orderedFields: Array<keyof Calculation> = [
      'fare',
      'tax',
      'commission',
      'taxOnCommission',
      'supplementaryCommission',
      'cancellationPenalty',
      'miscellaneousFee'
    ];

    return (a: keyof Calculation, b: keyof Calculation) => orderedFields.indexOf(a) - orderedFields.indexOf(b);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
