import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AdmAcmReasonsFilter, StatusReasonsType } from '../models/adm-acm-reason.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AdmAcmCategorizationReasonsFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: AdmAcmReasonsFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<AdmAcmReasonsFilter> = {
      primaryReason: primaryReason => `${this.translateLabel('primaryReason')} - ${primaryReason.primaryReason}`,
      subReason: subReason => `${this.translateLabel('subReason')} - ${subReason}`,
      active: active => `${this.translateLabel('status')} - ${this.formatActive(active)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translateLabel(key: string): string {
    return this.translationService.translate(`ADM_ACM.categorizationReasons.filters.${key}.label`);
  }

  public formatActive(active: boolean): string {
    return this.translateActiveOptions(active ? StatusReasonsType.active : StatusReasonsType.inactive);
  }

  private translateActiveOptions(key: string): string {
    return this.translationService.translate(`ADM_ACM.categorizationReasons.filters.status.options.${key}`);
  }
}
