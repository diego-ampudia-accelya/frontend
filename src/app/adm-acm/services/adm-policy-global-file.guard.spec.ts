import { TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { DialogService, FooterButton } from '~app/shared/components';
import { AdmPolicyGlobalFileGuard } from './adm-policy-global-file.guard';

describe('AdmPolicyGlobalFileGuard', () => {
  let guard: AdmPolicyGlobalFileGuard;
  const dialogServiceSpy = createSpyObject(DialogService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdmPolicyGlobalFileGuard, { provide: DialogService, useValue: dialogServiceSpy }]
    });
    guard = TestBed.inject(AdmPolicyGlobalFileGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should open dialog', () => {
    guard.canActivate();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
    expect(dialogServiceSpy.open.calls.first().args[1]).toEqual({
      data: {
        title: 'ADM_ACM.policyGlobalFile.title',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Accept }],
        buttons: [{ type: FooterButton.Accept }]
      }
    });
  });
});
