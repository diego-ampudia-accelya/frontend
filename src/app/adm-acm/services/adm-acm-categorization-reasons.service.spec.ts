import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { AdmAcmReasonsFilter } from '../models/adm-acm-reason.model';
import { AdmAcmCategorizationReasonsService } from './adm-acm-categorization-reasons.service';
import { DataQuery } from '~app/shared/components/list-view';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

const filterByQuery: AdmAcmReasonsFilter = {
  primaryReason: {
    id: '1',
    primaryReason: 'primary'
  },
  subReason: 'subReason',
  active: true
};

describe('AdmAcmCategorizationReasonsService', () => {
  let admAcmCategorizationReasonsService: AdmAcmCategorizationReasonsService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of(httpResponse));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        AdmAcmCategorizationReasonsService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ]
    });

    admAcmCategorizationReasonsService = TestBed.inject(AdmAcmCategorizationReasonsService);
  });

  it('should create', () => {
    expect(admAcmCategorizationReasonsService).toBeDefined();
  });

  it('should send proper find request', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    const expectedUrl = '/acdm-management/categories?page=0&size=20&primaryReasonId=1&subReason=subReason&active=true';

    admAcmCategorizationReasonsService.find(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl =
      '/acdm-management/categories/download?primaryReasonId=1&subReason=subReason&active=true&downloadType=txt';

    admAcmCategorizationReasonsService.download(query, exportOptions).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));

  it('should send proper find request', fakeAsync(() => {
    const expectedUrl = '/acdm-management/categories/primary-reasons';

    admAcmCategorizationReasonsService.getPrimaryReasonDropDownValueObject().subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));
});
