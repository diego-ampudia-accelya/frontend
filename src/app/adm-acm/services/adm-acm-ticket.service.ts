import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { FormOfPaymentType } from '~app/document/enums';
import { SummaryTicket } from '~app/document/models/summary-ticket.model';
import { DocumentService } from '~app/document/services/document.service';
import { AppState } from '~app/reducers';
import * as fromBillingAnalysis from '~app/sales/billing-analysis/reducers';
import { buildViewFromBEObject } from '~app/shared/helpers';
import {
  Calculation,
  ConcernIndicator,
  TicketNetReportingIndicator,
  TicketTaxType,
  TicketTransactionCode
} from '../models/adm-acm-issue-shared-aux.model';
import { AdmAcmBspConfig, CurrencyViewModel } from '../models/adm-acm-issue-view-aux.model';
import { AdmAcmIssueViewModel } from '../models/adm-acm-issue.model';
import { adapterViewTicketFunctionsMapper, fieldMapperAdmAcmTicket } from '../shared/helpers/view-be-mapper.config';
import { AdmAcmDataService } from './adm-acm-data.service';

@Injectable({
  providedIn: 'root'
})
export class AdmAcmTicketService {
  private ticketSummary: SummaryTicket;

  constructor(
    private formData: AdmAcmDataService,
    private docService: DocumentService,
    private appStore: Store<AppState>
  ) {}

  public updateSummaryForm(documentId: string) {
    this.formData.isFormReady.next(false);

    this.docService
      .getDocumentSummary(documentId)
      .pipe(
        take(1),
        map((summary: SummaryTicket) => this.removeNegativesSigns(summary))
      )
      .subscribe((summary: SummaryTicket) => {
        this.ticketSummary = summary;

        buildViewFromBEObject<Partial<SummaryTicket>, AdmAcmIssueViewModel>(
          this.ticketSummary,
          this.formData.getForm(),
          false,
          fieldMapperAdmAcmTicket,
          adapterViewTicketFunctionsMapper
        );

        this.formData.isFormReady.next(true);
      });
  }

  public updateMultipopulationSummaryForm() {
    this.formData.isFormReady.next(false);

    this.appStore
      .pipe(
        select(fromBillingAnalysis.getMultipopulationDocumentsDetails),
        take(1),
        map((summary: SummaryTicket) => this.removeNegativesSigns(summary))
      )
      .subscribe((summary: SummaryTicket) => {
        this.ticketSummary = summary;

        buildViewFromBEObject<Partial<SummaryTicket>, AdmAcmIssueViewModel>(
          this.ticketSummary,
          this.formData.getForm(),
          false,
          fieldMapperAdmAcmTicket,
          adapterViewTicketFunctionsMapper
        );

        this.formData.isFormReady.next(true);
      });
  }

  public getTicketConcernIndicator(): ConcernIndicator {
    if (
      this.ticketSummary.transactionCode === TicketTransactionCode.RFND ||
      this.ticketSummary.transactionCode === TicketTransactionCode.RFNC
    ) {
      return ConcernIndicator.refund;
    }
    if (this.ticketSummary.formOfPayment?.find(payment => payment.type === FormOfPaymentType.EX)) {
      return ConcernIndicator.exchange;
    }

    if (
      this.ticketSummary.transactionCode === TicketTransactionCode.EMDA ||
      this.ticketSummary.transactionCode === TicketTransactionCode.EMDS
    ) {
      return ConcernIndicator.emd;
    }

    return ConcernIndicator.issue;
  }

  public getTicketCurrency(): CurrencyViewModel {
    return this.ticketSummary.currency;
  }

  public getTicketId(): string {
    return this.ticketSummary.id;
  }

  public getTicketNetReporting(acdmSpam: boolean): boolean {
    return (
      acdmSpam &&
      (this.ticketSummary.netReportingIndicator === TicketNetReportingIndicator.NR ||
        this.ticketSummary.sumSupplementaryAmount > 0)
    );
  }

  private removeNegativesSigns(summary: SummaryTicket): SummaryTicket {
    const { taxes, formOfPayment, ticketAmount, totalPaymentAmount } = summary;

    const taxesUnsigned = taxes.map(tax => ({ ...tax, amount: Math.abs(tax.amount) }));

    const formOfPaymentUnsigned = formOfPayment?.map(formOfPay => ({
      ...formOfPay,
      amount: Math.abs(formOfPay.amount)
    }));

    const result: SummaryTicket = {
      ...summary,
      taxes: taxesUnsigned,
      formOfPayment: formOfPaymentUnsigned,
      ticketAmount: Math.abs(ticketAmount),
      totalPaymentAmount: Math.abs(totalPaymentAmount)
    };

    return result;
  }

  private getAgentFareAmount(): number {
    let agentFareAmount = this.ticketSummary.ticketAmount;
    if (
      this.ticketSummary.transactionCode === TicketTransactionCode.RFND ||
      this.ticketSummary.transactionCode === TicketTransactionCode.RFNC
    ) {
      this.ticketSummary.taxes.forEach(tax => {
        agentFareAmount += tax.type !== TicketTaxType.CP && tax.type !== TicketTaxType.MF ? -tax.amount : tax.amount;
      });
    } else {
      this.ticketSummary.taxes.forEach(tax => {
        agentFareAmount -= tax.amount;
      });
    }

    return agentFareAmount;
  }

  private getAgentTaxAmount(): number {
    let agentTaxAmount = 0;
    if (
      this.ticketSummary.transactionCode === TicketTransactionCode.RFND ||
      this.ticketSummary.transactionCode === TicketTransactionCode.RFNC
    ) {
      this.ticketSummary.taxes
        .filter(tax => tax.type !== TicketTaxType.CP && tax.type !== TicketTaxType.MF)
        .forEach(tax => {
          agentTaxAmount += tax.amount;
        });
    } else {
      this.ticketSummary.taxes.forEach(tax => {
        agentTaxAmount += tax.amount;
      });
    }

    return agentTaxAmount;
  }

  private getAgentCommissionAmount(acdmSpam: boolean): number {
    return acdmSpam
      ? Math.abs(this.ticketSummary.sumCommissionAmount)
      : Math.abs(this.ticketSummary.sumCommissionAmount) + Math.abs(this.ticketSummary.sumSupplementaryAmount);
  }

  private getAgentSuplCommissionAmount(acdmSpam): number {
    return acdmSpam ? Math.abs(this.ticketSummary.sumSupplementaryAmount) : 0;
  }

  private getAgentTaxOnCommissionAmount(acdmToca): number {
    return acdmToca ? this.ticketSummary.vatGstAmount : 0;
  }

  private getAgentCPAmount(): number {
    let agentCPAmount = 0;
    if (
      this.ticketSummary.transactionCode === TicketTransactionCode.RFND ||
      this.ticketSummary.transactionCode === TicketTransactionCode.RFNC
    ) {
      this.ticketSummary.taxes
        .filter(tax => tax.type === TicketTaxType.CP)
        .forEach(tax => {
          agentCPAmount += tax.amount;
        });
    }

    return agentCPAmount;
  }

  private getAgentMFAmount(): number {
    let agentMFAmount = 0;
    if (
      this.ticketSummary.transactionCode === TicketTransactionCode.RFND ||
      this.ticketSummary.transactionCode === TicketTransactionCode.RFNC
    ) {
      this.ticketSummary.taxes
        .filter(tax => tax.type === TicketTaxType.MF)
        .forEach(tax => {
          agentMFAmount += tax.amount;
        });
    }

    return agentMFAmount;
  }

  public getAgentTicketCalculations(bspConfig: AdmAcmBspConfig): Observable<Calculation> {
    const agentCalculations: Calculation = {
      fare: this.getAgentFareAmount(),
      tax: this.getAgentTaxAmount(),
      commission: this.getAgentCommissionAmount(bspConfig.acdmSpam),
      taxOnCommission: this.getAgentTaxOnCommissionAmount(bspConfig.acdmToca),
      supplementaryCommission: this.getAgentSuplCommissionAmount(bspConfig.acdmSpam),
      cancellationPenalty: this.getAgentCPAmount(),
      miscellaneousFee: this.getAgentMFAmount()
    };

    return of(agentCalculations);
  }
}
