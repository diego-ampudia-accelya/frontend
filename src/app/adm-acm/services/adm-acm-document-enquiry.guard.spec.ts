import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AdmAcmDocumentEnquiryDialogComponent } from '../components/dialogs/adm-acm-document-enquiry-dialog/adm-acm-document-enquiry-dialog.component';
import { AdmAcmDocumentSummary } from '../models/adm-acm-enquiry-document.model';
import { AdmAcmStatus, MarkStatus, SendToDpcType, TransactionCode } from '../models/adm-acm-issue-shared-aux.model';
import { TransactionRuleType } from '../models/adm-acm-rule.model';

import { AdmAcmDocumentEnquiryGuard } from './adm-acm-document-enquiry.guard';
import { AdmAcmDocumentEnquiryService } from './adm-acm-document-enquiry.service';
import { PagedData } from '~app/shared/models/paged-data.model';
import { ROUTES } from '~app/shared/constants';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

const dialogConfig: DialogConfig = {
  data: {
    title: null,
    isSearching: false,
    mainButtonType: 'search',
    footerButtonsType: [
      { type: FooterButton.Search, isDisabled: true, tooltipText: 'ADM_ACM.documentEnquiry.dialog.tooltipMessage' }
    ]
  }
};

const pagedDataMock: PagedData<AdmAcmDocumentSummary> = {
  records: [],
  pageSize: 0,
  total: 0,
  totalPages: 0,
  pageNumber: 1
};

const documentMock: AdmAcmDocumentSummary = {
  id: '123123',
  ticketDocumentNumber: '1231231232 1',
  agent: {
    id: 465456,
    iataCode: '7550010',
    name: 'NOMBRE 1',
    effectiveFrom: '2000-01-01',
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01'
    }
  },
  airline: {
    id: 6983485553,
    iataCode: '075',
    localName: 'Nombre 45'
  },
  currency: {
    id: 698582,
    code: 'EUR',
    decimals: 2
  },
  dateOfIssue: '2022-01-31',
  reportingDate: '2022-02-07',
  sentToDpc: SendToDpcType.pending,
  markStatus: MarkStatus.pending,
  acdmStatus: AdmAcmStatus.pending,
  totalAmount: 100,
  concernsIndicator: TransactionRuleType.ISSUE,
  transactionCode: TransactionCode.acm,
  period: '2022021'
};

describe('AdmAcmDocumentEnquiryGuard', () => {
  let guard: AdmAcmDocumentEnquiryGuard;
  let dialogServiceSpy: SpyObject<DialogService>;
  let routerSpy: SpyObject<Router>;
  const routerSnapshotSpy: SpyObject<ActivatedRouteSnapshot> = createSpyObject(ActivatedRouteSnapshot);
  let documentServiceSpy: SpyObject<AdmAcmDocumentEnquiryService>;
  let dialogComponentSpy: SpyObject<AdmAcmDocumentEnquiryDialogComponent>;

  beforeEach(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    routerSpy = createSpyObject(Router);
    documentServiceSpy = createSpyObject(AdmAcmDocumentEnquiryService);
    dialogComponentSpy = createSpyObject(AdmAcmDocumentEnquiryDialogComponent);

    dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));

    TestBed.configureTestingModule({
      providers: [
        AdmAcmDocumentEnquiryGuard,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: ActivatedRouteSnapshot, useValue: routerSnapshotSpy },
        { provide: AdmAcmDocumentEnquiryService, useValue: documentServiceSpy },
        mockProvider(L10nTranslationService)
      ]
    });

    guard = TestBed.inject(AdmAcmDocumentEnquiryGuard);
  });

  it('should create', () => {
    expect(guard).toBeDefined();
  });

  it('should open dialog', () => {
    const openDialogObservableSpy = spyOn<any>(guard, 'openDialogObservable').and.callThrough();

    guard.canActivate(routerSnapshotSpy).subscribe();

    expect(openDialogObservableSpy).toHaveBeenCalled();
    expect(dialogServiceSpy.open).toHaveBeenCalledWith(AdmAcmDocumentEnquiryDialogComponent, dialogConfig);
  });

  it('should navigate when document number is not found', fakeAsync(() => {
    const documentResponse = {
      ...pagedDataMock,
      records: []
    };
    spyOn<any>(guard, 'openDialogObservable').and.returnValue(
      of({ clickedBtn: FooterButton.Search, contentComponentRef: dialogComponentSpy })
    );
    spyOn<any>(guard, 'checkDocumentExistence').and.returnValue(of(false));
    documentServiceSpy.getBy.and.returnValue(of(documentResponse));
    routerSpy.navigateByUrl.and.callThrough();
    dialogComponentSpy.form = new FormGroup({ documentNumber: new FormControl(123123) });

    guard.canActivate(routerSnapshotSpy).subscribe();

    tick();

    expect(routerSpy.navigateByUrl).not.toHaveBeenCalled();
  }));

  it('should navigate when document number is found', fakeAsync(() => {
    const documentResponse = {
      ...pagedDataMock,
      records: [documentMock]
    };
    spyOn<any>(guard, 'openDialogObservable').and.returnValue(
      of({ clickedBtn: FooterButton.Search, contentComponentRef: dialogComponentSpy })
    );
    spyOn<any>(guard, 'checkDocumentExistence').and.returnValue(of(true));
    documentServiceSpy.getBy.and.returnValue(of(documentResponse));
    routerSpy.navigateByUrl.and.callThrough();
    dialogComponentSpy.form = new FormGroup({ documentNumber: new FormControl(123123) });
    routerSpy.routeReuseStrategy = {
      shouldReuseRoute() {
        return true;
      },
      store: null,
      shouldAttach: null,
      shouldDetach: null,
      retrieve: null
    };
    let result;

    guard.canActivate(routerSnapshotSpy).subscribe(data => (result = data));

    tick();

    expect(result).toBeTruthy();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith(`${ROUTES.ACDM_DOCUMENT_ENQUIRY_LIST.url}/123123`);
  }));

  it('should not pass guard if dialog return cancel event', fakeAsync(() => {
    spyOn<any>(guard, 'openDialogObservable').and.returnValue(
      of({ clickedBtn: FooterButton.Cancel, contentComponentRef: null })
    );

    let result;

    guard.canActivate(routerSnapshotSpy).subscribe(data => (result = data));

    tick();

    expect(result).toBeFalsy();
  }));
});
