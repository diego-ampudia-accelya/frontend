import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { AdmAcmMassloadFileFilter } from '../models/adm-acm-massload-file-filter.model';
import {
  AdmAcmMassloadFile,
  AdmAcmMassloadFileListOption,
  AdmAcmMassloadFileStatus,
  AdmAcmMassloadFileTotals,
  AdmAcmMassloadFileViewModel
} from '../models/adm-acm-massload-file.model';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class AdmAcmMassloadFileService implements Queryable<AdmAcmMassloadFileViewModel> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/acdm-management`;
  private url: string;

  private bspId: number;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {
    this.store
      .pipe(select(fromAuth.getUserBsps), first())
      .subscribe(bsps => (this.bspId = bsps && bsps[0] ? bsps[0].id : null));
  }

  public changeEndpointUrl(option: AdmAcmMassloadFileListOption): void {
    if (option === AdmAcmMassloadFileListOption.NotProcessed) {
      this.url = `${this.baseUrl}/massload-file-cancellations`;
    } else {
      this.url = `${this.baseUrl}/massload-files`;
    }
  }

  public find(query: DataQuery<AdmAcmMassloadFileFilter>): Observable<PagedData<AdmAcmMassloadFileViewModel>> {
    const requestQuery = this.formatQuery(query);

    return this.http
      .get<PagedData<AdmAcmMassloadFile>>(this.url + requestQuery.getQueryString())
      .pipe(map(this.toViewModel));
  }

  public findTotals(query: DataQuery<AdmAcmMassloadFileFilter>): Observable<AdmAcmMassloadFileTotals> {
    const url = `${this.url}/totals`;
    const requestQuery = this.formatQuery(query);

    return this.http.get<AdmAcmMassloadFileTotals>(url + requestQuery.getQueryString());
  }

  public stopProcessing(filenames: Array<{ filename: string }>): Observable<any> {
    const postUrl = `${this.baseUrl}/massload-file-cancellations`;

    return this.http.post(postUrl, filenames);
  }

  public downloadEvaluationFile(id: string): Observable<{ blob: Blob; fileName: string }> {
    const downloadUrl = `${this.baseUrl}/massload-evaluation-files/${id}`;

    return this.http
      .get<HttpResponse<ArrayBuffer>>(downloadUrl, downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<AdmAcmMassloadFileFilter>): RequestQuery {
    const { uploadDate, requestDate, ...filterBy } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromUploadDate: uploadDate && toShortIsoDate(uploadDate[0]),
        toUploadDate: uploadDate && toShortIsoDate(uploadDate[1] ? uploadDate[1] : uploadDate[0]),
        fromRequestDate: requestDate && toShortIsoDate(requestDate[0]),
        toRequestDate: requestDate && toShortIsoDate(requestDate[1] ? requestDate[1] : requestDate[0]),
        bspId: this.bspId
      }
    });
  }

  private toViewModel(data: PagedData<AdmAcmMassloadFile>): PagedData<AdmAcmMassloadFileViewModel> {
    return {
      ...data,
      records: data.records.map(file => ({
        ...file,
        disableSelection: file.status !== AdmAcmMassloadFileStatus.Pending
      }))
    };
  }
}
