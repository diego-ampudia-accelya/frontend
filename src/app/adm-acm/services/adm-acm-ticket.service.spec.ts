import { fakeAsync, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { FormOfPaymentType } from '~app/document/enums';
import { SummaryTicket } from '~app/document/models/summary-ticket.model';
import { DocumentService } from '~app/document/services/document.service';
import { StatisticalCode } from '~app/sales/shared/models';
import { FormUtil } from '~app/shared/helpers';
import {
  Calculation,
  ConcernIndicator,
  TicketNetReportingIndicator,
  TicketTaxType,
  TicketTransactionCode
} from '../models/adm-acm-issue-shared-aux.model';
import {
  AmountViewModel,
  BasicInformationViewModel,
  DetailViewModel,
  StatusHeaderViewModel,
  TaxViewModel
} from '../models/adm-acm-issue-view-aux.model';
import { AdmAcmIssueViewModel } from '../models/adm-acm-issue.model';
import { AdmAcmDataService } from './adm-acm-data.service';
import { AdmAcmTicketService } from './adm-acm-ticket.service';

describe('AdmAcmTicketService', () => {
  let service: AdmAcmTicketService;
  const documentServiceSpy: SpyObject<DocumentService> = createSpyObject(DocumentService);

  const mockSummaryTicket: SummaryTicket = {
    id: '1234567890-2025-SUxfTUlTWUlOREhPVF8yMDIwMDIxNS56aXA=',
    bsp: { id: 1234, isoCountryCode: 'ES', name: 'SPAIN' },
    airline: {
      id: 1234987654,
      localName: 'Airline',
      vatNumber: '560010779',
      globalAirline: { id: 987654, iataCode: '001', globalName: 'Airlines, Inc.' },
      bsp: { id: 1234, isoCountryCode: 'ES', name: 'SPAIN' }
    },
    agent: { id: 12341234567, iataCode: '1234567', name: 'Agent', vatNumber: '557611977' },
    currency: null,
    documentNumber: '1234567890',
    fullDocumentNumber: '0011234567890',
    transactionCode: TicketTransactionCode.TKTT,
    dateOfIssue: '2020-02-15',
    couponUseIndicator: 'FSFF',
    passengerName: 'XXXXXXXXXXXXX',
    ticketAmount: 998.61,
    taxes: [
      { type: TicketTaxType.CP, amount: 12.14 },
      { type: 'IL', amount: 27.26 }
    ],
    totalPaymentAmount: 998.61,
    sumCommissionAmount: -43.97,
    sumSupplementaryAmount: 0.9,
    vatGstAmount: -0.11,
    formOfPayment: [{ type: FormOfPaymentType.CA, amount: 998.61 }],
    statisticalCode: StatisticalCode.International,
    conjunctionTickets: [
      { documentNumber: '1234567891', fullDocumentNumber: '0011234567891', couponUseIndicator: 'SFVV' }
    ]
  };

  const loadTicketSummary = (ticket: SummaryTicket): void => {
    documentServiceSpy.getDocumentSummary.and.returnValue(of(ticket));
    service.updateSummaryForm(ticket.id);
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AdmAcmTicketService,
        AdmAcmDataService,
        FormBuilder,
        { provide: DocumentService, useValue: documentServiceSpy },
        provideMockStore({})
      ]
    });
  });

  beforeEach(done => {
    service = TestBed.inject(AdmAcmTicketService);

    service['formData'].whenFormIsReady().subscribe(done);

    const builder = TestBed.inject(FormBuilder);
    const formUtil = new FormUtil(builder);

    const statusHeader = formUtil.createGroup<StatusHeaderViewModel>({
      ticketDocumentNumber: null,
      status: null,
      bspName: null,
      agentCode: null,
      issueDate: null,
      amount: null
    });

    const basicInformation = formUtil.createGroup<BasicInformationViewModel>({
      ticketDocumentNumber: null,
      reportingDate: null,
      bspName: null,
      type: null,
      airline: null,
      agent: null,
      airlineContact: null,
      admSettings: null
    });

    const details = formUtil.createGroup<DetailViewModel>({
      disputeContact: null,
      disputeTime: null,
      gdsForwards: null,
      reasons: null,
      reasonsDetails: null,
      relatedDocuments: null
    });

    const amounts = formUtil.createGroup<AmountViewModel>({
      agentCalculations: null,
      airlineCalculations: null,
      differenceCalculations: null,
      agentTotal: null,
      airlineTotal: null,
      totalAmount: null
    });

    const taxes = formUtil.createGroup<TaxViewModel>({
      taxMiscellaneousFees: null
    });

    service['formData']['form'] = formUtil.createGroup<AdmAcmIssueViewModel>({
      statusHeader,
      basicInformation,
      details,
      amounts,
      taxes
    });

    service['formData'].isFormReady.next(true);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should update summary form properly', () => {
    documentServiceSpy.getDocumentSummary.and.returnValue(of(mockSummaryTicket));
    service.updateSummaryForm(mockSummaryTicket.id);

    expect(service['ticketSummary']).toEqual(mockSummaryTicket);
  });

  describe('getTicketConcernIndicator()', () => {
    it('should return refund concern indicator', () => {
      loadTicketSummary({
        ...mockSummaryTicket,
        transactionCode: TicketTransactionCode.RFNC
      });
      expect(service.getTicketConcernIndicator()).toEqual(ConcernIndicator.refund);
    });

    it('should return exchange concern indicator', () => {
      loadTicketSummary({
        ...mockSummaryTicket,
        formOfPayment: [...mockSummaryTicket.formOfPayment, { type: FormOfPaymentType.EX, amount: 900.89 }]
      });
      expect(service.getTicketConcernIndicator()).toEqual(ConcernIndicator.exchange);
    });

    it('should return emd concern indicator', () => {
      loadTicketSummary({
        ...mockSummaryTicket,
        transactionCode: TicketTransactionCode.EMDA
      });
      expect(service.getTicketConcernIndicator()).toEqual(ConcernIndicator.emd);
    });

    it('should return issue concern indicator', () => {
      loadTicketSummary(mockSummaryTicket);
      expect(service.getTicketConcernIndicator()).toEqual(ConcernIndicator.issue);
    });
  });

  it('should return ticket currency properly', () => {
    loadTicketSummary(mockSummaryTicket);
    expect(service.getTicketCurrency()).toBeNull();
  });

  it('should return ticket id properly', () => {
    loadTicketSummary(mockSummaryTicket);
    expect(service.getTicketId()).toEqual('1234567890-2025-SUxfTUlTWUlOREhPVF8yMDIwMDIxNS56aXA=');
  });

  it('should return net reporting properly', () => {
    loadTicketSummary({
      ...mockSummaryTicket,
      netReportingIndicator: TicketNetReportingIndicator.NR
    });

    expect(service.getTicketNetReporting(false)).toBeFalsy();
    expect(service.getTicketNetReporting(true)).toBeTruthy();
  });

  it('should return agent fare amount properly', () => {
    loadTicketSummary({
      ...mockSummaryTicket,
      transactionCode: TicketTransactionCode.RFND
    });
    expect(service['getAgentFareAmount']()).toEqual(998.61 + (12.14 - 27.26));

    service['ticketSummary'].transactionCode = TicketTransactionCode.TKTT;
    expect(service['getAgentFareAmount']()).toEqual(998.61 - (12.14 + 27.26));
  });

  it('should return agent tax amount properly', () => {
    loadTicketSummary({
      ...mockSummaryTicket,
      transactionCode: TicketTransactionCode.RFNC
    });
    expect(service['getAgentTaxAmount']()).toEqual(27.26);

    service['ticketSummary'].transactionCode = TicketTransactionCode.TKTT;
    expect(service['getAgentTaxAmount']()).toEqual(12.14 + 27.26);
  });

  it('should return agent commission amount properly', () => {
    loadTicketSummary(mockSummaryTicket);

    expect(service['getAgentCommissionAmount'](true)).toEqual(Math.abs(-43.97));
    expect(service['getAgentCommissionAmount'](false)).toEqual(Math.abs(-43.97) + Math.abs(0.9));
  });

  it('should return agent suppl commission amount properly', () => {
    loadTicketSummary(mockSummaryTicket);

    expect(service['getAgentSuplCommissionAmount'](true)).toEqual(Math.abs(0.9));
    expect(service['getAgentSuplCommissionAmount'](false)).toEqual(0);
  });

  it('should return agent tax on commission amount properly', () => {
    loadTicketSummary(mockSummaryTicket);

    expect(service['getAgentTaxOnCommissionAmount'](true)).toEqual(-0.11);
    expect(service['getAgentTaxOnCommissionAmount'](false)).toEqual(0);
  });

  it('should return agent CP amount properly', () => {
    loadTicketSummary({
      ...mockSummaryTicket,
      transactionCode: TicketTransactionCode.RFND
    });
    expect(service['getAgentCPAmount']()).toEqual(12.14);

    loadTicketSummary({
      ...mockSummaryTicket,
      transactionCode: TicketTransactionCode.TKTT
    });
    expect(service['getAgentCPAmount']()).toEqual(0);
  });

  it('should return agent MF amount properly', () => {
    loadTicketSummary({
      ...mockSummaryTicket,
      transactionCode: TicketTransactionCode.RFNC,
      taxes: [...mockSummaryTicket.taxes, { type: TicketTaxType.MF, amount: 9.8 }]
    });
    expect(service['getAgentMFAmount']()).toEqual(9.8);

    loadTicketSummary({
      ...mockSummaryTicket,
      transactionCode: TicketTransactionCode.TKTT,
      taxes: [...mockSummaryTicket.taxes, { type: TicketTaxType.MF, amount: 9.8 }]
    });
    expect(service['getAgentMFAmount']()).toEqual(0);
  });

  it('should return correct agent ticket calculations', fakeAsync(() => {
    loadTicketSummary(mockSummaryTicket);

    let agentTicketCalculation: Calculation;
    service
      .getAgentTicketCalculations({ acdmSpam: true, acdmToca: false } as any)
      .subscribe(calc => (agentTicketCalculation = calc));

    expect(agentTicketCalculation).toEqual({
      fare: 998.61 - (12.14 + 27.26),
      tax: 12.14 + 27.26,
      commission: Math.abs(-43.97),
      taxOnCommission: 0,
      supplementaryCommission: Math.abs(0.9),
      cancellationPenalty: 0,
      miscellaneousFee: 0
    });
  }));
});
