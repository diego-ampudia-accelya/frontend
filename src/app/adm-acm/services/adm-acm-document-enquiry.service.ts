import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AdmAcmDocumentSearchForm, AdmAcmDocumentSummary } from '../models/adm-acm-enquiry-document.model';
import { AcdmType } from '../models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '../models/adm-acm-issue.model';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import { getPluralPath } from '~app/shared/utils/api-utils';

@Injectable()
export class AdmAcmDocumentEnquiryService {
  private baseManagementUrl = `${this.appConfiguration.baseApiPath}/acdm-management`;
  private baseUrl = `${this.baseManagementUrl}/acdms`;

  constructor(public http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public getBy(ticketDocumentNumber: string): Observable<PagedData<AdmAcmDocumentSummary>> {
    const defaultDataQuery = cloneDeep(defaultQuery);
    defaultDataQuery.filterBy = { ticketDocumentNumber };

    return this.find(defaultDataQuery);
  }

  @Cacheable()
  find(query: DataQuery<AdmAcmDocumentSearchForm>): Observable<PagedData<AdmAcmDocumentSummary>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<AdmAcmDocumentSummary>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: DataQuery<AdmAcmDocumentSearchForm>): RequestQuery {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...query.filterBy
      }
    });
  }

  @Cacheable()
  public getDocumentDeletionReasonById(id: number, acdmType: AcdmType): Observable<string> {
    const pluralType = getPluralPath(acdmType);

    return this.http
      .get<AdmAcmIssueBE>(`${this.baseManagementUrl}/${pluralType}/${id}`)
      .pipe(map(document => document.deletionReason || ''));
  }

  public download(
    query: DataQuery<AdmAcmDocumentSearchForm>,
    downloadFormat: DownloadFormat,
    _downloadBody: any = {},
    detailedDownload?: boolean
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = {
      ...requestQuery.filterBy,
      exportAs: downloadFormat.toUpperCase(),
      ...(detailedDownload && { exportDetail: true })
    };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(
        this.baseUrl + requestQuery.getQueryString({ omitPaging: true }),
        downloadRequestOptions
      )
      .pipe(map(formatDownloadResponse));
  }
}
