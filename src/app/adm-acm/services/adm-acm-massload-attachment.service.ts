import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { AdmAcmMassloadAttachmentFilter } from '../models/adm-acm-massload-attachment-filter.model';
import { AdmAcmMassloadAttachment } from '../models/adm-acm-massload-attachment.model';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class AdmAcmMassloadAttachmentService implements Queryable<AdmAcmMassloadAttachment> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/acdm-management/attached-files`;

  private bspId: number;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {
    this.store
      .pipe(select(fromAuth.getUserBsps), first())
      .subscribe(bsps => (this.bspId = bsps && bsps[0] ? bsps[0].id : null));
  }

  public find(query: DataQuery<AdmAcmMassloadAttachmentFilter>): Observable<PagedData<AdmAcmMassloadAttachment>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<AdmAcmMassloadAttachment>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: DataQuery<AdmAcmMassloadAttachmentFilter>): RequestQuery {
    const { uploadDate, ...filterBy } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromUploadDate: uploadDate && toShortIsoDate(uploadDate[0]),
        toUploadDate: uploadDate && toShortIsoDate(uploadDate[1] ? uploadDate[1] : uploadDate[0]),
        bspId: this.bspId
      }
    });
  }
}
