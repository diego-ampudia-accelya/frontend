import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import {
  acmAdmRuleConverterToBE,
  toValueLabelObjectPrimaryReason,
  toValueLabelObjectSubReason,
  toValueLabelPrimaryReasonId,
  toValueLabelSubReasonId
} from '../components/adm-acm-categorization-rules/adm-acm-categorization-rules-details-form/adm-acm-categorization-rules.converter';
import {
  AcmAdmRuleDetail,
  AcmAdmRuleFilterBE,
  AdmAcmRule,
  AdmAcmRuleFilter,
  PrimaryReasonRule,
  ReasonStatusRuleType,
  SubReasonRule
} from '../models/adm-acm-rule.model';

@Injectable()
export class AdmAcmCategorizationRulesService implements Queryable<AdmAcmRule> {
  public get acdmRule(): AdmAcmRule {
    return this._acdmRule;
  }

  public set acdmRule(acdmRule: AdmAcmRule) {
    this._acdmRule = acdmRule;
  }

  private baseUrl = `${this.appConfiguration.baseApiPath}/acdm-management/adm-rules`;

  private _acdmRule: AdmAcmRule;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public getRuleById(ruleId: number): Observable<AdmAcmRule> {
    const url = `${this.baseUrl}/${ruleId}`;

    return this.http.get<AdmAcmRule>(url);
  }

  @Cacheable()
  private getSubReasonRules(reasonId: number, active: boolean): Observable<SubReasonRule[]> {
    const url = `${this.baseUrl}/primary-reasons/${reasonId}/sub-reasons`;
    const params = { ...(active !== undefined && { status: ReasonStatusRuleType.ENABLED.toUpperCase() }) };

    return this.http.get<SubReasonRule[]>(url, { params });
  }

  @Cacheable()
  private getPrimaryReasonRules(active: boolean): Observable<PrimaryReasonRule[]> {
    const url = `${this.baseUrl}/primary-reasons`;
    const params = { ...(active !== undefined && { status: ReasonStatusRuleType.ENABLED.toUpperCase() }) };

    return this.http.get<PrimaryReasonRule[]>(url, { params });
  }

  public find(query: DataQuery<AdmAcmRuleFilter>): Observable<PagedData<AdmAcmRule>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<AdmAcmRule>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<AdmAcmRuleFilter>,
    format: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}/download`;
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, downloadType: format };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(url + requestQuery.getQueryString({ omitPaging: true }), downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  public getPrimaryReasonDropDownValueObject(active?: boolean): Observable<DropdownOption[]> {
    return this.getPrimaryReasonRules(active).pipe(
      map(toValueLabelObjectPrimaryReason),
      catchError(() => of([]))
    );
  }

  public getPrimaryReasonDropDownValueId(active?: boolean): Observable<DropdownOption[]> {
    return this.getPrimaryReasonRules(active).pipe(
      map(toValueLabelPrimaryReasonId),
      catchError(() => of([]))
    );
  }

  public getSubReasonDropDownValueObject(reasonId: number, active?: boolean): Observable<DropdownOption[]> {
    return this.getSubReasonRules(reasonId, active).pipe(
      map(toValueLabelObjectSubReason),
      catchError(() => of([]))
    );
  }

  public getSubReasonDropDownValueId(reasonId: number, active?: boolean): Observable<DropdownOption[]> {
    return this.getSubReasonRules(reasonId, active).pipe(
      map(toValueLabelSubReasonId),
      catchError(() => of([]))
    );
  }

  public update(item: AcmAdmRuleDetail, id: number): Observable<any> {
    const url = `${this.baseUrl}/${id}`;

    return this.http.put<AdmAcmRule>(url, item);
  }

  public rerun(item: { active: boolean }, id: number): Observable<AdmAcmRule> {
    const url = `${this.baseUrl}/${id}/rerun`;

    return this.http.post<AdmAcmRule>(url, item);
  }

  public createRule(rule: AcmAdmRuleDetail): Observable<any> {
    return this.http.post(this.baseUrl, acmAdmRuleConverterToBE(rule));
  }

  private formatQuery(query: DataQuery<AdmAcmRuleFilter>): RequestQuery<AcmAdmRuleFilterBE> {
    const { bsps, primaryReason, subReason, airlineCode, ...rest } = query.filterBy;

    return RequestQuery.fromDataQuery<AcmAdmRuleFilterBE>({
      ...query,
      filterBy: {
        ...rest,
        airlineCode: airlineCode && airlineCode.code,
        isoCountryCodes: bsps && bsps.map(bsp => bsp.isoCountryCode),
        primaryReasonId: primaryReason && primaryReason.id,
        subReasonId: subReason && subReason.map(value => value.id)
      }
    });
  }
}
