import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { toValueLabelObjectPrimaryReason } from '../components/adm-acm-categorization-reasons/adm-acm-categorization-reason.converter';
import {
  AdmAcmReasons,
  AdmAcmReasonsCreateDialog,
  AdmAcmReasonsFilter,
  AdmAcmReasonsPrimaryReason
} from '../models/adm-acm-reason.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class AdmAcmCategorizationReasonsService implements Queryable<AdmAcmReasons> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/acdm-management/categories`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<AdmAcmReasonsFilter>): Observable<PagedData<AdmAcmReasons>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<AdmAcmReasons>>(this.baseUrl + requestQuery.getQueryString());
  }

  public createNew(item: AdmAcmReasonsCreateDialog): Observable<AdmAcmReasons> {
    return this.http.post<AdmAcmReasonsCreateDialog>(this.baseUrl, item);
  }

  public createFromExisting(item: AdmAcmReasonsCreateDialog, id: string): Observable<AdmAcmReasons> {
    const url = `${this.baseUrl}/primary-reasons/${id}/sub-reasons`;

    return this.http.post<AdmAcmReasonsCreateDialog>(url, item);
  }

  public update(item: AdmAcmReasons, id: number): Observable<AdmAcmReasons> {
    const url = `${this.baseUrl}/${id}`;

    return this.http.put<AdmAcmReasonsCreateDialog>(url, item);
  }

  public updateSubReason(item: AdmAcmReasons, id: number): Observable<AdmAcmReasons> {
    const url = `${this.baseUrl}/primary-reasons/sub-reasons/${id}`;

    return this.http.put<AdmAcmReasonsCreateDialog>(url, item);
  }

  public rerun(item: AdmAcmReasons, id: number): Observable<AdmAcmReasons> {
    const url = `${this.baseUrl}/${id}/rerun`;

    return this.http.post<AdmAcmReasons>(url, item);
  }

  public download(
    query: DataQuery<AdmAcmReasonsFilter>,
    format: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}/download`;
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, downloadType: format };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(url + requestQuery.getQueryString({ omitPaging: true }), downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  public getPrimaryReasonDropDownValueObject(): Observable<DropdownOption[]> {
    return this.getPrimaryReasons().pipe(
      map(toValueLabelObjectPrimaryReason),
      catchError(() => of([]))
    );
  }

  private formatQuery(query: Partial<DataQuery<AdmAcmReasonsFilter>>): RequestQuery<AdmAcmReasons> {
    const { primaryReason, ...rest } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        primaryReasonId: primaryReason && primaryReason.id,
        ...rest
      }
    });
  }

  private getPrimaryReasons(): Observable<AdmAcmReasonsPrimaryReason[]> {
    const url = `${this.baseUrl}/primary-reasons`;

    return this.http.get<AdmAcmReasonsPrimaryReason[]>(url);
  }
}
