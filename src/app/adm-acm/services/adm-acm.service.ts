import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, first, map, startWith, switchMap, tap } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models';
import { Period } from '~app/master-data/periods/shared/period.models';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { AppState } from '~app/reducers';
import { PeriodUtils } from '~app/shared/components';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { DataQuery } from '~app/shared/components/list-view';
import { RequestMethod } from '~app/shared/enums/request-method.enum';
import { contactInfoCacheBuster$ } from '~app/shared/helpers/cacheable-helper';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { NumberHelper } from '~app/shared/helpers/numberHelper';
import { BspDto } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus, MarkStatusUpdateModel } from '~app/shared/models/mark-status.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { WatcherDocType, WatcherModel } from '~app/shared/models/watcher.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { DynamicEndpointClass } from '~app/shared/services/endpoints.config';
import { buildEndpointPath } from '~app/shared/services/endpoints.helper';
import { AdmAcmFilter } from '../models/adm-acm-filter.model';
import {
  AcdmReason,
  BasicAgentInfo,
  BasicAirlineInfo,
  BspTimeModel,
  GdsForward
} from '../models/adm-acm-issue-be-aux.model';
import { AcdmOptionType, AdmAcmStatus, BEAcdmActionType, ReasonAction } from '../models/adm-acm-issue-shared-aux.model';
import { CurrencyViewModel, GdsViewModel } from '../models/adm-acm-issue-view-aux.model';
import { AdmAcmIssueBE } from '../models/adm-acm-issue.model';

@Injectable()
export class AdmAcmService extends ApiService<AdmAcmIssueBE> {
  private periods: Period[];

  constructor(
    http: HttpClient,
    private activatedRoute: ActivatedRoute,
    @Optional() private activatedRouteSnapshot?: ActivatedRouteSnapshot,
    @Optional() private notificationHeaderService?: NotificationsHeaderService,
    @Optional() private store?: Store<AppState>,
    @Optional() private periodService?: PeriodService
  ) {
    super(http, '');

    const routeSnapshotData = this.activatedRouteSnapshot
      ? this.activatedRouteSnapshot.data
      : this.activatedRoute.snapshot.data;

    this.baseUrl = buildEndpointPath(this.getEndpointClass(routeSnapshotData.admAcmType), routeSnapshotData);

    if (this.store && this.periodService) {
      this.store
        .pipe(
          select(getUserBsps),
          switchMap(bsps => (bsps.length ? this.periodService.getByBsp(bsps[0].id) : of([]))),
          first()
        )
        .subscribe(periods => (this.periods = periods));
    }
  }

  @Cacheable({ cacheBusterObserver: contactInfoCacheBuster$ })
  public getAirlineInfo(airlineId: number): Observable<BasicAirlineInfo> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/airline-info/${airlineId}`;

    return this.http.get<BasicAirlineInfo>(baseUrl);
  }

  @Cacheable({ cacheBusterObserver: contactInfoCacheBuster$ })
  public getAgentInfo(agentId: number): Observable<BasicAgentInfo> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/agent-info/${agentId}`;

    return this.http.get<BasicAgentInfo>(baseUrl);
  }

  public setServiceName(serviceName: string) {
    this.serviceName = serviceName;
  }

  public getBspsList(active?: boolean): Observable<PagedData<BspDto>> {
    const params = { ...(active && { active }) };

    return this.getAllIssues('bsps', params);
  }

  public getAll(requestQuery: RequestQuery): Observable<PagedData<AdmAcmIssueBE>> {
    return super.getAll(requestQuery).pipe(tap(result => result.records.forEach(record => this.dataAdapter(record))));
  }

  public find(dataQuery: DataQuery): Observable<PagedData<AdmAcmIssueBE>> {
    const reqQuery = this.formatQuery(dataQuery);

    return this.getAll(reqQuery);
  }

  public getBspAgentInfo(agentId: number): Observable<BasicAgentInfo> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsp-agent-info/${agentId}`;

    return this.http.get<BasicAgentInfo>(baseUrl);
  }

  public getBspAirlineInfo(airlineId: number): Observable<BasicAirlineInfo> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsp-airline-info/${airlineId}`;

    return this.http.get<BasicAirlineInfo>(baseUrl);
  }

  public getBspDropDownOptions(list: BspDto[]): DropdownOption[] {
    return list.map(item => ({
      value: item.id,
      label: `${item.name} (${item.isoCountryCode})`
    }));
  }

  public getAgentDropDownOptions(list): DropdownOption[] {
    return list.map(item => ({
      value: item.id,
      label: `${item.iataCode} / ${item.name}`
    }));
  }

  public getAirlineDropDownOptions(list): DropdownOption[] {
    return list.map(item => ({
      value: item.id,
      label: `${item.iataCode} / ${item.localName}`
    }));
  }

  public getReasonsDropDownOptions(list: AcdmReason[]): DropdownOption[] {
    return list.map(item => ({
      value: item.id,
      label: `${item.title}`
    }));
  }

  public getReasons(airlineId: number, bspId: number, type: ReasonAction): Observable<AcdmReason[]> {
    const baseAirlUrl = `${appConfiguration.baseApiPath}/acdm-management/airlines/${airlineId}/acdm-reason-configuration`;
    const baseBspUrl = `${appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}/acdm-reason-configuration`;

    const paramQuery = {
      reasonType: type
    };

    return this.http
      .get<AcdmReason[]>(baseAirlUrl, { params: paramQuery })
      .pipe(
        switchMap(airlReasons =>
          this.http
            .get<AcdmReason[]>(baseBspUrl, { params: paramQuery })
            .pipe(map(bspReasons => this.populateReasonsId(airlReasons.concat(bspReasons))))
        )
      );
  }

  public getCurrencies(bspId, activeParam): Observable<CurrencyViewModel[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}/currencies`;
    const paramQuery = {
      active: activeParam
    };

    return this.http.get(baseUrl, { params: paramQuery }).pipe(map((info: any) => info));
  }

  public getCurrencyById(id): Observable<CurrencyViewModel> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/currencies/${id}`;

    return this.http.get<CurrencyViewModel>(baseUrl).pipe(map((pagedData: CurrencyViewModel) => pagedData));
  }

  public getGdssBsp(bspId: number, admId: number): Observable<GdsViewModel[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}/gdss`;
    const paramQuery = {
      admId: admId.toString()
    };

    return this.http.get<GdsViewModel[]>(baseUrl, { params: paramQuery });
  }

  public getBspTime(bspId: number): Observable<BspTimeModel> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}/time`;

    return this.http.get<BspTimeModel>(baseUrl).pipe(map(time => ({ ...time, localTime: new Date(time.localTime) })));
  }

  public getGdsOptionDropdown(list: GdsViewModel[]): DropdownOption<number>[] {
    return list.map(item => ({
      value: item.id,
      label: `${item.gdsCode} / ${item.name}`
    }));
  }

  public downloadForwardedAdmGlobalFile(requestPayload: any): Observable<any> {
    const baseUrl = `${appConfiguration.baseApiPath}/file-management/forwarded-adms-global-file`;

    return this.http.post(baseUrl, requestPayload);
  }

  public download(requestQueryBody, downloadFormat: string, _downloadBody: any = {}, detailedDownload?: boolean) {
    const query = this.formatQuery(requestQueryBody).getQueryString();
    const urlPath = detailedDownload
      ? `?exportAs=${downloadFormat.toUpperCase()}&exportDetail=true&${query}`
      : `?exportAs=${downloadFormat.toUpperCase()}&${query}`;

    return super.download({}, urlPath, RequestMethod.Get);
  }

  public updateAcdmStatus(id: number, status: AdmAcmStatus, requestBody?: any): Observable<AdmAcmIssueBE> {
    const baseUrl = `${this.baseUrl}/${id}/status`;

    if (!requestBody) {
      requestBody = { status };
    }

    return this.http.put<AdmAcmIssueBE>(baseUrl, requestBody);
  }

  public forwardGdsAdm(id: number, requestBody: any): Observable<GdsForward> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/adms/${id}/gdsforwards`;

    return this.http.post<GdsForward>(baseUrl, requestBody);
  }

  public setMarkStatus(items: AdmAcmIssueBE[], markStatus: MarkStatus): Observable<MarkStatusUpdateModel[]> {
    const url = `${this.baseUrl}/mark-status`;
    const payload = items.map(item => ({
      id: item.id,
      markStatus
    }));

    return this.http.post<MarkStatusUpdateModel[]>(url, payload);
  }

  public updateWatchers(items: AdmAcmIssueBE[], watching: boolean): Observable<WatcherModel[]> {
    return forkJoin(
      items.map(item =>
        this.notificationHeaderService.updateWatcher(
          {
            documentId: item.id,
            documentType: WatcherDocType.ACDM,
            watching
          },
          (item.agent as Agent).bsp.id
        )
      )
    );
  }

  public getBspAirlines(): Observable<DropdownOption[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/bsp-airlines`;

    return this.http.get<PagedData<any>>(baseUrl).pipe(
      map(items =>
        items.records.map(item => ({
          value: item.iataCode,
          label: item.iataCode
        }))
      )
    );
  }

  private getAllIssues(type, paramQuery): Observable<PagedData<any>> {
    const baseUrl = `${appConfiguration.baseApiPath}/acdm-management/${type}`;

    return this.http.get<PagedData<any>>(baseUrl, { params: paramQuery });
  }

  private formatQuery(query: DataQuery<AdmAcmFilter>): RequestQuery {
    const {
      bsp,
      airlineIataCode,
      agentId,
      currencyId,
      amountRange,
      dateOfIssue,
      period,
      reportingDate,
      disputeDate,
      gdsForwardCode,
      primaryReason,
      reason,
      ...filterBy
    } = query.filterBy;

    let bsps: BspDto[];
    if (bsp) {
      bsps = !Array.isArray(bsp) ? [bsp] : bsp;
    }

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        bspId: bsps && bsps.map(({ id }) => id),
        airlineIataCode: airlineIataCode && airlineIataCode.map(item => item.code),
        agentId: agentId && agentId.map(item => item.id),
        currencyId: currencyId && currencyId.map(item => item.id),
        fromTotalAmount: amountRange && amountRange[0],
        toTotalAmount: amountRange && amountRange[1],
        gdsForwardCode: gdsForwardCode && gdsForwardCode.map(gds => gds.gdsCode),
        fromDateOfIssue: dateOfIssue && toShortIsoDate(dateOfIssue[0]),
        toDateOfIssue: dateOfIssue && toShortIsoDate(dateOfIssue[1] ? dateOfIssue[1] : dateOfIssue[0]),
        period:
          period &&
          PeriodUtils.getPeriodsBetweenDates(this.periods, period[0].dateFrom, period[period.length - 1].dateTo).map(
            p => p.period
          ),
        fromReportingDate: reportingDate && toShortIsoDate(reportingDate[0]),
        toReportingDate: reportingDate && toShortIsoDate(reportingDate[1] ? reportingDate[1] : reportingDate[0]),
        fromDisputeDate: disputeDate && toShortIsoDate(disputeDate[0]),
        toDisputeDate: disputeDate && toShortIsoDate(disputeDate[1] ? disputeDate[1] : disputeDate[0]),
        primaryReason: primaryReason && primaryReason.id,
        reason: reason && reason.map(item => item.id)
      }
    });
  }

  public getActivities(acdmId: number): Observable<Activity[]> {
    const requestUrl = `${this.baseUrl}/${acdmId}/history`;

    return this.http.get<Activity[]>(requestUrl).pipe(
      map(activities =>
        activities
          // TODO This filter is to avoid errors with previous BE. Remove when transition to new approach is completed
          .filter(activity => !(activity as any).userId)
          .map(activity => {
            activity.creationDate = new Date(activity.creationDate);

            return activity;
          })
      )
    );
  }

  public getDefaultTicketDocNumber(airlineId: number): Observable<number> {
    const baseUrl = `${this.baseUrl}/ticket-document-number/next-number`;
    const paramQuery = {
      airlineId: airlineId.toString()
    };

    return this.http.get<{ ticketDocumentNumber: number }>(baseUrl, { params: paramQuery }).pipe(
      map((response: { ticketDocumentNumber: number }) => response.ticketDocumentNumber),
      catchError(() => of(null))
    );
  }

  public getAcdmActionList(acdmId: number): Observable<{ action: GridTableActionType; disabled?: boolean }[]> {
    const baseUrl = `${this.baseUrl}/${acdmId}/actions`;

    return this.http.get<{ id: BEAcdmActionType; enabled: boolean }[]>(baseUrl).pipe(
      startWith([{ id: BEAcdmActionType.loading, enabled: false }]),
      map(actionList =>
        actionList
          .filter(item => this.fromBEActionToGridTableAction(item.id))
          .map(item => ({
            action: this.fromBEActionToGridTableAction(item.id),
            disabled: !item.enabled
          }))
      )
    );
  }

  public getAcdmMenuOptions(isArchivedSubTab?: boolean): Observable<ButtonMenuOption[]> {
    const params = {
      ...(isArchivedSubTab && { markStatus: '' + MarkStatus.Archive })
    };

    const baseUrl = `${this.baseUrl}/actions`;

    return this.http.get<{ id: AcdmOptionType; enabled: boolean }[]>(baseUrl, { params }).pipe(
      startWith([{ id: AcdmOptionType.Loading, enabled: false }]),
      map(options => {
        const acdmOptionTypeValues = Object.values(AcdmOptionType);

        return options
          .filter(option => acdmOptionTypeValues.includes(option.id))
          .map(({ id, enabled }) => ({
            id,
            label: id,
            isDisabled: !enabled
          }));
      })
    );
  }

  public downloadFile(resourceUrl) {
    return super.downloadFile(resourceUrl);
  }

  public addComment(requestBody: { reason: string }, id: number): Observable<any> {
    const baseUrl = `${this.baseUrl}/${id}/comments`;

    return this.http.post(baseUrl, requestBody);
  }

  public addInternalComment(id: number, body: { internalComment: string }): Observable<any> {
    const baseUrl = `${this.baseUrl}/${id}/internal-comment`;

    return this.http.post(baseUrl, body);
  }

  public editInternalComment(id: number, body: { internalComment: string }): Observable<any> {
    const baseUrl = `${this.baseUrl}/${id}/internal-comment`;

    return this.http.put(baseUrl, body);
  }

  public deleteInternalComment(id: number): Observable<any> {
    const baseUrl = `${this.baseUrl}/${id}/internal-comment`;

    return this.http.delete(baseUrl);
  }

  public bulkDownloadAttachments(items: AdmAcmIssueBE[]): Observable<{ blob: Blob; fileName: string }> {
    const urlPath = `/download-attachments`;
    const ids = items.map(item => item.id);

    return super.download({ ids }, urlPath);
  }

  //* Needed to map BE values to GridTableActionType ones
  private fromBEActionToGridTableAction(actionBE: BEAcdmActionType): GridTableActionType {
    const mapper: { [key in BEAcdmActionType]?: GridTableActionType } = {
      [BEAcdmActionType.loading]: GridTableActionType.Loading,
      [BEAcdmActionType.edit]: GridTableActionType.Edit,
      [BEAcdmActionType.dispute]: GridTableActionType.DisputeACDM,
      [BEAcdmActionType.details]: GridTableActionType.Details,
      [BEAcdmActionType.forwardGds]: GridTableActionType.ForwardGdsADM,
      [BEAcdmActionType.deactivate]: GridTableActionType.DeactivateACDM,
      [BEAcdmActionType.reactivate]: GridTableActionType.ReactivateACDM,
      [BEAcdmActionType.approve]: GridTableActionType.ApproveACDM,
      [BEAcdmActionType.reject]: GridTableActionType.RejectACDM,
      [BEAcdmActionType.authorizeRequest]: GridTableActionType.AuthorizeRequest,
      [BEAcdmActionType.rejectRequest]: GridTableActionType.RejectRequest,
      [BEAcdmActionType.delete]: GridTableActionType.Delete,
      [BEAcdmActionType.editReactivate]: GridTableActionType.EditReactivateACDM,
      [BEAcdmActionType.editReject]: GridTableActionType.EditRejectACDM,
      [BEAcdmActionType.supervise]: GridTableActionType.Supervise,
      [BEAcdmActionType.internalComment]: GridTableActionType.InternalComment,
      [BEAcdmActionType.activity]: GridTableActionType.Activity
      //* Uncomment actions mapper when logic is developed
      //[BEAcdmActionType.authorize]: GridTableActionType.AuthorizeSPCDR,
      //[BEAcdmActionType.disputePostBilling]: GridTableActionType.DisputePostBillingACDM,
      //[BEAcdmActionType.deletePostBilling]: GridTableActionType.DeletePostBillingACDM
    };

    return mapper[actionBE];
  }

  private dataAdapter(data): void {
    data.totalAmount = NumberHelper.roundString(data.totalAmount, data.currency.decimals) as any;
  }

  private populateReasonsId(reasons: AcdmReason[]): AcdmReason[] {
    reasons.forEach((reason, index) => {
      reason.id = index;
    });

    return reasons;
  }

  private getEndpointClass(acdmType: MasterDataType): DynamicEndpointClass {
    let endpointClass: DynamicEndpointClass;

    switch (acdmType) {
      case MasterDataType.Admq:
        endpointClass = DynamicEndpointClass.AdmRequestService;
        break;

      case MasterDataType.Acmq:
        endpointClass = DynamicEndpointClass.AcmRequestService;
        break;

      default:
        endpointClass = DynamicEndpointClass.AdmAcmService;
    }

    return endpointClass;
  }
}
