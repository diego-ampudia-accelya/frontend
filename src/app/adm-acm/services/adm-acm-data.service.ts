import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdmAcmStatus, ConcernIndicator } from '../models/adm-acm-issue-shared-aux.model';
import {
  AgentViewModel,
  AirlineViewModel,
  BasicInformationViewModel,
  CurrencyViewModel,
  DetailViewModel,
  StatusHeaderViewModel,
  StatusViewModel
} from '../models/adm-acm-issue-view-aux.model';
import { AdmAcmActionEmitterType, AdmAcmIssueBE, AdmAcmIssueViewModel } from '../models/adm-acm-issue.model';
import { adapterBEFunctionsMapper, fieldMapperAdmAcm } from '../shared/helpers/view-be-mapper.config';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { BspDto } from '~app/shared/models/bsp.model';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { FormDataService } from '~app/shared/services/form-data.service';

@Injectable()
export class AdmAcmDataService extends FormDataService<AdmAcmIssueBE, AdmAcmIssueViewModel> {
  protected get initialForm() {
    return { id: null, attachmentIds: [] };
  }
  protected get initialSubjects(): { [key: string]: Subject<any> } {
    return {
      netReporting: new BehaviorSubject<boolean>(true),
      currency: new BehaviorSubject<CurrencyViewModel>(null),
      agent: new BehaviorSubject<AgentViewModel>(null),
      acdmaFor: new BehaviorSubject<ConcernIndicator>(null),
      totalAmount: new BehaviorSubject<string>(null),
      totalTaxAmount: new BehaviorSubject<string>(null),
      status: new BehaviorSubject<StatusViewModel>(null),
      reasons: new BehaviorSubject<Partial<DetailViewModel>>(null),
      gdsForwardReasons: new Subject<Partial<DetailViewModel>>(),
      airline: new BehaviorSubject<AirlineViewModel>(null),
      bsp: new BehaviorSubject<BspDto>(null),
      hasToca: new BehaviorSubject<boolean>(false),
      tocaAmount: new BehaviorSubject<string>(null),
      tocaType: new BehaviorSubject<string>(null)
    };
  }

  constructor(protected fb: FormBuilder) {
    super(fb);
  }

  public getSubject(name: AdmAcmActionEmitterType) {
    return super.getSubject(name);
  }

  public getIssueDate(): Observable<string> {
    return this.whenFormIsReady().pipe(
      map(() => FormUtil.get<StatusHeaderViewModel>(this.getSectionFormGroup('statusHeader'), 'issueDate').value)
    );
  }

  public getReportingDate(): Observable<string> {
    return this.whenFormIsReady().pipe(
      map(
        () =>
          FormUtil.get<BasicInformationViewModel>(this.getSectionFormGroup('basicInformation'), 'reportingDate').value
      )
    );
  }

  public getStatus(): Observable<AdmAcmStatus> {
    return this.whenFormIsReady().pipe(
      map(() => {
        const statusHeader = FormUtil.get<StatusHeaderViewModel>(this.getSectionFormGroup('statusHeader'), 'status');

        return FormUtil.get<StatusViewModel>(statusHeader as FormGroup, 'status').value;
      })
    );
  }

  public getDataForSubmit(): AdmAcmIssueBE {
    return super.getDataForSubmit(fieldMapperAdmAcm, adapterBEFunctionsMapper);
  }

  public processErrorsOnSubmit(errorResponse: ResponseErrorBE): void {
    super.processErrorsOnSubmit(errorResponse, fieldMapperAdmAcm);
  }
}
