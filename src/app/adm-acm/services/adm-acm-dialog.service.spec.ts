import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';

import { AcdmActionType } from '../models/adm-acm-issue-shared-aux.model';
import { AdmAcmDialogService } from './adm-acm-dialog.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DialogService } from '~app/shared/components';

describe('AdmAcmDialogService', () => {
  let service: AdmAcmDialogService;

  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(AdmAcmDialogService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should open dialog properly', fakeAsync(() => {
    service.openDialog(AcdmActionType.issue, null, MasterDataType.Adm, null);

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  }));

  it('should open error dialog properly', () => {
    service.openErrorDialog(null, MasterDataType.Acm);

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
