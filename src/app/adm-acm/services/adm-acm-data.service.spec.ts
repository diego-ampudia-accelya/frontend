import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormArray, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';

import { initialState } from '../components/adm-acm/adm-acm-test.config';
import {
  AdmAcmStatus,
  Calculation,
  SendToDpcType,
  TaxMiscellaneousFee
} from '../models/adm-acm-issue-shared-aux.model';
import {
  AmountViewModel,
  BasicInformationViewModel,
  StatusHeaderViewModel,
  StatusViewModel,
  TaxViewModel
} from '../models/adm-acm-issue-view-aux.model';
import { AdmAcmIssueBE, AdmAcmIssueViewModel } from '../models/adm-acm-issue.model';
import { adapterBEFunctionsMapper, fieldMapperAdmAcm } from '../shared/helpers/view-be-mapper.config';

import { AdmAcmDataService } from './adm-acm-data.service';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { generateBEObject } from '~app/shared/helpers';

describe('AdmAcmDataService', () => {
  let service: AdmAcmDataService;
  let builder: FormBuilder;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, FormsModule, ReactiveFormsModule],
      providers: [AdmAcmDataService, FormBuilder, provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(done => {
    service = TestBed.inject(AdmAcmDataService);
    builder = TestBed.inject(FormBuilder);

    service.whenFormIsReady().subscribe(done);

    const formUtil = new FormUtil(builder);

    const status = formUtil.createGroup<StatusViewModel>({
      status: AdmAcmStatus.pending,
      sentToDpc: SendToDpcType.pending
    });

    const statusHeader = formUtil.createGroup<StatusHeaderViewModel>({
      ticketDocumentNumber: null,
      status,
      bspName: null,
      agentCode: null,
      issueDate: '2019-08-03',
      amount: null
    });

    const basicInformation = formUtil.createGroup<BasicInformationViewModel>({
      ticketDocumentNumber: null,
      reportingDate: '2019-08-18',
      bspName: null,
      type: null,
      airline: null,
      agent: null,
      airlineContact: null,
      admSettings: null
    });

    const taxes = formUtil.createGroup<TaxViewModel>({
      taxMiscellaneousFees: formUtil.builder.array([
        formUtil.createGroup<TaxMiscellaneousFee>({
          agentAmount: null,
          airlineAmount: null,
          taxDifferenceAmount: null,
          type: null
        })
      ])
    });

    const calculation = formUtil.createGroup<Calculation>({
      cancellationPenalty: 0,
      commission: 0,
      fare: 0,
      miscellaneousFee: 0,
      supplementaryCommission: 0,
      tax: 0,
      taxOnCommission: 0
    });

    const amounts = formUtil.createGroup<AmountViewModel>({
      agentCalculations: calculation,
      airlineCalculations: calculation,
      differenceCalculations: calculation,
      agentTotal: 0,
      airlineTotal: 0,
      totalAmount: 0
    });

    service['form'] = formUtil.createGroup<AdmAcmIssueViewModel>({
      id: null,
      statusHeader,
      basicInformation,
      details: null,
      amounts,
      taxes
    });

    service.isFormReady.next(true);
  }, 1000);

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should get correct form issue date', () => {
    let formIssueDate: string;
    service.getIssueDate().subscribe(issueDate => (formIssueDate = issueDate));

    expect(formIssueDate).toBe('2019-08-03');
  });

  it('should get correct form reporting date', () => {
    let formReportingDate: string;
    service.getReportingDate().subscribe(reportingDate => (formReportingDate = reportingDate));

    expect(formReportingDate).toBe('2019-08-18');
  });

  it('should get correct form status', () => {
    let formStatus: string;
    service.getStatus().subscribe(status => (formStatus = status));

    expect(formStatus).toBe(AdmAcmStatus.pending);
  });

  it('should getDataForSubmit', () => {
    //* Data for submit should be retrieved with the correct data
    //* To check this, we set a mock Calculation and expect it to be the same when building the BE object
    const mockCalculation: Calculation = {
      commission: 1,
      fare: 2,
      supplementaryCommission: 3,
      tax: 4,
      taxOnCommission: 5,
      cancellationPenalty: 0,
      miscellaneousFee: 0
    };

    const agentViewCalculations = FormUtil.get<AmountViewModel>(
      service.getSectionFormGroup('amounts'),
      'agentCalculations'
    );

    agentViewCalculations.patchValue(mockCalculation, { emitEvent: false });

    spyOn(service, 'getDataForSubmit').and.returnValue(
      generateBEObject<Partial<AdmAcmIssueBE>, Partial<AdmAcmIssueViewModel>>(
        service.getForm(),
        { agentCalculations: fieldMapperAdmAcm.agentCalculations },
        adapterBEFunctionsMapper
      )
    );

    const agentBECalculations = service.getDataForSubmit().agentCalculations;

    expect(Object.keys(agentBECalculations).every(key => agentBECalculations[key] === mockCalculation[key])).toBe(true);
  });

  // TODO: check why it doesn't work correctly
  xit('should processErrorsOnSubmit', () => {
    const errorBEMock: ResponseErrorBE = {
      timestamp: '1555947364364',
      errorMessage: 'Validation error',
      errorCode: 400,
      messages: [
        {
          message: "'CP' tax is not permitted for documents concerning issues.",
          messageCode: null,
          messageParams: [{ name: 'fieldName', value: 'taxes.taxMiscellaneousFees[0].type' }]
        }
      ]
    };

    const typeControl = FormUtil.get<TaxMiscellaneousFee>(
      (service.getSectionFormGroup('taxes').get('taxMiscellaneousFees') as FormArray).controls[0] as FormGroup,
      'type'
    );

    expect(typeControl.valid).toBe(true);

    service.processErrorsOnSubmit(errorBEMock);

    expect(typeControl.valid && typeControl.pristine).toBe(false);
  });
});
