import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { of } from 'rxjs';
import { finalize, switchMap } from 'rxjs/operators';

import * as fromAuth from '../../auth/selectors/auth.selectors';
import {
  AcdmActionType,
  AcdmNumberingMode,
  AirlineConfigParamName,
  Calculation,
  TransactionCode
} from '../models/adm-acm-issue-shared-aux.model';
import {
  AdmAcmAirlineConfig,
  AdmAcmBspConfig,
  AirlineConfigParameters,
  AirlineViewModel,
  AmountCalculationFieldConfig,
  AmountViewModel,
  BasicInformationViewModel,
  DetailViewModel,
  StatusHeaderViewModel,
  TaxViewModel,
  TocaTypeViewModel
} from '../models/adm-acm-issue-view-aux.model';
import {
  AdmAcmActionEmitterType,
  AdmAcmIssueBE,
  AdmAcmIssueViewModel,
  TocaErrorsType
} from '../models/adm-acm-issue.model';
import * as regularizedHelper from '../shared/helpers/adm-acm-regularized.helper';
import { AdmAcmConfigService } from './adm-acm-config.service';
import { AdmAcmDataService } from './adm-acm-data.service';
import { AdmAcmDialogService } from './adm-acm-dialog.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AccessType } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { TabService } from '~app/shared/services';

describe('AdmAcmConfigService', () => {
  let service: AdmAcmConfigService;

  const httpClientSpy = createSpyObject(HttpClient);
  const translationSpy = createSpyObject(L10nTranslationService);
  const routerSpy = createSpyObject(Router);
  const acdmDialogServiceSpy = createSpyObject(AdmAcmDialogService);
  const tabServiceSpy = createSpyObject(TabService);

  const mockAcdmBspConfig: AdmAcmBspConfig = {
    positiveVat: false,
    admLatencyDays: 15,
    acmLatencyDays: 0,
    admGdsForward: false,
    acdmSpam: true,
    freeStat: false,
    defaultStat: 'I',
    acdmToca: false,
    airlineVatnumber: false,
    agentVatNumber: true,
    acdmCaptureMode: '9',
    acmNumberPrefix: '8000',
    admNumberPrefix: '6000',
    acmNumberLength: 10,
    admNumberLength: 11
  };

  const mockAcdmAirlineConfig: AdmAcmAirlineConfig = {
    descriptorId: 778450150481000,
    parameters: [],
    scopeId: '7784505048',
    scopeType: 'airlines',
    service: 'acdm-management'
  };

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      providers: [
        AdmAcmConfigService,
        AdmAcmDataService,
        FormBuilder,
        provideMockStore({ selectors: [{ selector: fromAuth.getUser, value: createAirlineUser() }] }),
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: L10nTranslationService, useValue: translationSpy },
        { provide: Router, useValue: routerSpy },
        { provide: AdmAcmDialogService, useValue: acdmDialogServiceSpy },
        { provide: TabService, useValue: tabServiceSpy }
      ]
    });
  });

  beforeEach(done => {
    service = TestBed.inject(AdmAcmConfigService);

    service['formData'].whenFormIsReady().subscribe(done);

    const builder = TestBed.inject(FormBuilder);
    const formUtil = new FormUtil(builder);

    const statusHeader = formUtil.createGroup<StatusHeaderViewModel>({
      ticketDocumentNumber: null,
      status: null,
      bspName: null,
      agentCode: null,
      issueDate: null,
      amount: null
    });

    const basicInformation = formUtil.createGroup<BasicInformationViewModel>({
      ticketDocumentNumber: null,
      reportingDate: null,
      bspName: null,
      type: null,
      airline: null,
      agent: null,
      airlineContact: null,
      admSettings: null
    });

    const details = formUtil.createGroup<DetailViewModel>({
      disputeContact: null,
      disputeTime: null,
      gdsForwards: null,
      reasons: null,
      reasonsDetails: null,
      relatedDocuments: null
    });

    const amounts = formUtil.createGroup<AmountViewModel>({
      agentCalculations: null,
      airlineCalculations: null,
      differenceCalculations: null,
      agentTotal: null,
      airlineTotal: null,
      totalAmount: null
    });

    const taxes = formUtil.createGroup<TaxViewModel>({
      taxMiscellaneousFees: null
    });

    service['formData']['form'] = formUtil.createGroup<AdmAcmIssueViewModel>({
      statusHeader,
      basicInformation,
      details,
      amounts,
      taxes
    });

    service['formData'].isFormReady.next(true);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should set acdmType correctly and, when its different, updating type config', () => {
    spyOn(service, 'updateTypeConfig');
    service['_admAcmType'] = MasterDataType.Acm;

    service.admAcmType = MasterDataType.Adm;
    service.admAcmType = MasterDataType.Adm;

    expect(service.admAcmType).toBe(MasterDataType.Adm);
    expect(service.updateTypeConfig).toHaveBeenCalledTimes(1);
  });

  it('should update type config setting all visibility properties correctly when ACDMD is set', fakeAsync(() => {
    let amountFieldsConfig: AmountCalculationFieldConfig;
    let fareFieldVisibility: boolean;
    let taxOnCommissionFieldVisibility: boolean;
    let miscellaneousFeeFieldVisibility: boolean;

    spyOn(service, 'isConcernsRefundFieldVisible').and.returnValue(of(true));
    service['formData'].getSubject(AdmAcmActionEmitterType.hasToca).next(true);
    service.admAcmType = MasterDataType.Acmd;

    // We test 3 critical properties of the amount fields configuration object

    service.getAmountConfigurationCalculationFields().subscribe(conf => (amountFieldsConfig = conf));
    amountFieldsConfig.fare.visible.subscribe(visible => (fareFieldVisibility = visible));
    amountFieldsConfig.taxOnCommission.visible.subscribe(visible => (taxOnCommissionFieldVisibility = visible));
    amountFieldsConfig.miscellaneousFee.visible.subscribe(visible => (miscellaneousFeeFieldVisibility = visible));

    // Sign properties expects
    expect(amountFieldsConfig.fare.sign).toBe(1);
    expect(amountFieldsConfig.supplementaryCommission.sign).toBe(-1);
    expect(amountFieldsConfig.cancellationPenalty.sign).toBe(-1);

    // Visible properties expects
    expect(fareFieldVisibility).toBe(true);
    expect(taxOnCommissionFieldVisibility).toBe(false);
    expect(miscellaneousFeeFieldVisibility).toBe(false);
  }));

  it('should return reverse order factor depending on ACDM type family', () => {
    service.admAcmType = MasterDataType.Admd;
    expect(service.reverseOrderFactor).toBe(-1);

    service.admAcmType = MasterDataType.Acmq;
    expect(service.reverseOrderFactor).toBe(1);
  });

  it('should return calculation fields correctly ordered', () => {
    const calculationFields: Array<keyof Calculation> = [
      'fare',
      'tax',
      'commission',
      'taxOnCommission',
      'supplementaryCommission',
      'cancellationPenalty',
      'miscellaneousFee'
    ];

    expect(service.getAmountCalculationFields()).toEqual(calculationFields);
  });

  it('should update visibility of a calculation field', fakeAsync(() => {
    let commissionFieldVisible: boolean;

    // Commission field visibility is already initialize true by 'initializeAmountConfig' method
    service.updateVisiblityCalculationField('commission', of(false));

    service
      .getAmountConfigurationCalculationFields()
      .pipe(switchMap(conf => conf.commission.visible))
      .subscribe(visible => (commissionFieldVisible = visible));

    expect(commissionFieldVisible).toBe(false);
  }));

  it('should return toca types depending on bsp config', fakeAsync(() => {
    let tocaTypes: Array<{ value: string; label: string }>;
    const mockTOCATypesDropdown: Array<{ label: string; value: string }> = [
      { label: 'TOCA10', value: 'TOCA10' },
      { label: 'TOCA20', value: 'TOCA20' }
    ];

    spyOn(service, 'getTOCATypesDropdown').and.returnValue(of(mockTOCATypesDropdown));

    // ACDM TOCA: false
    service['bspConfig'].next(mockAcdmBspConfig);
    service.getTocaTypes(jasmine.any(Number)).subscribe(types => (tocaTypes = types));
    expect(tocaTypes).toEqual([]);

    // ACDM TOCA: true
    service['bspConfig'].next({ ...mockAcdmBspConfig, acdmToca: true });
    service.getTocaTypes(jasmine.any(Number)).subscribe(types => (tocaTypes = types));
    expect(tocaTypes).toEqual(mockTOCATypesDropdown);
  }));

  it('should return adma for correctly depending on acdm type', fakeAsync(() => {
    let admaFor: Array<{ value: string; label: string }>;

    service.admAcmType = MasterDataType.Adm;
    service.getAdmaFor().subscribe(res => (admaFor = res));
    expect(admaFor).toEqual([
      { value: 'ISSUE', label: 'ADM_ACM.acdmFor.ISSUE' },
      { value: 'REFUND', label: 'ADM_ACM.acdmFor.REFUND' },
      { value: 'EXCHANGE', label: 'ADM_ACM.acdmFor.EXCHANGE' },
      { value: 'EMD', label: 'ADM_ACM.acdmFor.EMD' }
    ]);

    service.admAcmType = MasterDataType.Admd;
    service.getAdmaFor().subscribe(res => (admaFor = res));
    expect(admaFor).toBeNull();
  }));

  it('should return correct transaction code', () => {
    service.admAcmType = MasterDataType.Admq;
    expect(service.getTransactionCode()).toBe(TransactionCode['adm-request']);
  });

  describe('get stat options depending on user and acdm type', () => {
    let statOptions: Array<DropdownOption & { selected: boolean }> | string;

    beforeEach(() => {
      service.admAcmType = MasterDataType.Adm;
      service['bspConfig'].next(mockAcdmBspConfig);
    });

    it('should return selected default stat options on AIRLINE user, ADM type and FREE STAT disabled', fakeAsync(() => {
      service.getStatOptions().subscribe(options => (statOptions = options));

      expect(statOptions).toEqual([
        {
          value: 'I',
          label: 'ADM_ACM.basicInfo.acdmProp.statValues.international',
          selected: true
        },
        {
          value: 'D',
          label: 'ADM_ACM.basicInfo.acdmProp.statValues.domestic',
          selected: false
        }
      ]);
    }));

    it('should return bsp default stat on AIRLINE user, ADM type and FREE STAT enabled', fakeAsync(() => {
      service['bspConfig'].next({ ...mockAcdmBspConfig, freeStat: true });
      service.getStatOptions().subscribe(options => (statOptions = options));

      expect(statOptions).toBe('I');
    }));
  });

  it('should return status header fields depending on ACDM type', () => {
    service.admAcmType = MasterDataType.Admd;
    expect(service.getStatusHeaderFields()).toEqual([
      'ticketDocumentNumber',
      'status',
      'bspName',
      'agentCode',
      'issueDate',
      'amount'
    ]);

    service.admAcmType = MasterDataType.Acm;
    expect(service.getStatusHeaderFields()).toEqual([
      'ticketDocumentNumber',
      'status',
      'bspName',
      'agentCode',
      'acdmaFor',
      'issueDate',
      'amount'
    ]);
  });

  it('should return bsp acdm spam and finish with the first emission', fakeAsync(() => {
    let acdmSpam: boolean;
    let subscriptionFinalized: boolean;

    service['bspConfig'].next(mockAcdmBspConfig);
    service
      .getAcdmSpam()
      .pipe(finalize(() => (subscriptionFinalized = true)))
      .subscribe(res => (acdmSpam = res));

    expect(acdmSpam).toBe(true);
    expect(subscriptionFinalized).toBe(true);
  }));

  it('should return bsp acdm TOCA and finish with the first emission', fakeAsync(() => {
    let acdmToca: boolean;
    let subscriptionFinalized: boolean;

    service['bspConfig'].next(mockAcdmBspConfig);
    service
      .getAcdmToca()
      .pipe(finalize(() => (subscriptionFinalized = true)))
      .subscribe(res => (acdmToca = res));

    expect(acdmToca).toBe(false);
    expect(subscriptionFinalized).toBe(true);
  }));

  it('should return bsp positive vat', fakeAsync(() => {
    let positiveVat: boolean;

    service['bspConfig'].next(mockAcdmBspConfig);
    service.getPositiveVat().subscribe(res => (positiveVat = res));

    expect(positiveVat).toBe(false);
  }));

  it('should format header button options correctly depending on actions type', () => {
    const mockAcdmIssue: AdmAcmIssueBE = { id: 1 } as AdmAcmIssueBE;
    const mockActions: { action: GridTableActionType; disabled?: boolean }[] = [
      { action: GridTableActionType.Details },
      { action: GridTableActionType.InternalComment },
      { action: GridTableActionType.ReactivateACDM, disabled: false },
      { action: GridTableActionType.DeactivateACDM, disabled: false },
      { action: GridTableActionType.ApproveACDM, disabled: true }
    ];

    const spy = spyOn(service, 'getActionCommand').and.callThrough();
    service.accessType = AccessType.edit;

    expect(service.formatHeaderBtnOptions(mockActions, mockAcdmIssue)).toEqual([
      { label: 'deactivateAcdm', isDisabled: false, disabled: false, command: jasmine.any(Function) },
      { label: 'approveDispute', isDisabled: true, disabled: true, command: jasmine.any(Function) }
    ]);
    expect(spy.calls.allArgs()).toEqual([
      [GridTableActionType.DeactivateACDM, mockAcdmIssue],
      [GridTableActionType.ApproveACDM, mockAcdmIssue]
    ]);
  });

  // TODO: waiting for a separated service implementation for this function (adm-acm-actions.service)
  describe('get action command depending on ACDM action type', () => {
    const mockAcdmItem: any = { id: 1 };
    let actionCommand: () => void;

    beforeEach(() => {
      service.admAcmType = MasterDataType.Adm;
      service['bspConfig'].next(mockAcdmBspConfig);
    });

    it('should navigate to the proper url on DETAILS action', () => {
      actionCommand = service.getActionCommand(GridTableActionType.Details, mockAcdmItem);
      actionCommand();

      expect(routerSpy.navigate).toHaveBeenCalledWith(['/acdms/adm/view', 1]);
    });

    // TODO: find a way to spy an individual exported function or mock store
    xit('should navigate to the proper url if possible on any SUPERVISION action', () => {
      spyOn(tabServiceSpy, 'isCurrentTabOnUrl').and.returnValue(of(false));
      actionCommand = service.getActionCommand(GridTableActionType.Supervise, mockAcdmItem);
      actionCommand();

      expect(routerSpy.navigate).toHaveBeenCalledWith(['/acdms/adm-supervision', 1]);
    });

    // TODO: find a way to spy an individual exported function and to compare these observables in params
    xit('should open dialog correctly if it is not possible to navigate on ISSUE/SUPERVISION action', () => {
      const regularizedHelperSpy = spyOn(regularizedHelper, 'isRegularized').and.returnValue(true);

      spyOn(tabServiceSpy, 'isCurrentTabOnUrl').and.returnValue(of(true));
      spyOn(service['formData'], 'getDataForSubmit').and.returnValue({ ...mockAcdmItem, totalAmount: 10 });

      actionCommand = service.getActionCommand(AcdmActionType.issue, mockAcdmItem);
      actionCommand();

      expect(regularizedHelperSpy).toHaveBeenCalledWith([{ ...mockAcdmItem, totalAmount: 10 }, false]);
      expect(acdmDialogServiceSpy.openDialog).toHaveBeenCalledWith([
        AcdmActionType.issue,
        { ...mockAcdmItem, totalAmount: 10 },
        MasterDataType.Adm,
        service['formData'],
        {
          actionEmitter: service['dialogActionEmitter'],
          showRegularizedMsg$: of(true),
          // With this bsp conf, numbering conf is: mode: auto, prefix: null and length: null
          numberingConf$: of({ mode: AcdmNumberingMode.automatic, prefix: null, length: null })
        }
      ]);
    });

    // TODO: find a way to spy an individual exported function or mock store
    xit('should navigate to the proper url if possible on any EDIT action', () => {
      spyOn(tabServiceSpy, 'isCurrentTabOnUrl').and.returnValue(of(false));
      actionCommand = service.getActionCommand(AcdmActionType.editRejectACDM, mockAcdmItem);
      actionCommand();

      expect(routerSpy.navigate).toHaveBeenCalledWith(['/acdms/adm-edit-reactivate', 1]);
    });

    // TODO: find a way to spy an individual exported function and to compare the subject in params
    xit('should open dialog correctly if it is not possible to navigate on EDIT action', () => {
      spyOn(tabServiceSpy, 'isCurrentTabOnUrl').and.returnValue(of(true));
      spyOn(service['formData'], 'getDataForSubmit').and.returnValue({ ...mockAcdmItem, totalAmount: 10 });

      actionCommand = service.getActionCommand(GridTableActionType.EditReactivateACDM, mockAcdmItem);
      actionCommand();

      expect(acdmDialogServiceSpy.openDialog).toHaveBeenCalledWith([
        AcdmActionType.editReactivateACDM,
        { ...mockAcdmItem, totalAmount: 10 },
        MasterDataType.Adm,
        service['formData'],
        { actionEmitter: service['dialogActionEmitter'] }
      ]);
    });

    // TODO: find a way to compare the subject in params
    xit('should just open dialog correctly on any other action declared in AcdmActionType model', () => {
      // Forward GDS action
      actionCommand = service.getActionCommand(GridTableActionType.ForwardGdsADM, mockAcdmItem);
      actionCommand();

      expect(acdmDialogServiceSpy.openDialog).toHaveBeenCalledWith([
        AcdmActionType.forwardGds,
        mockAcdmItem,
        MasterDataType.Adm,
        service['formData'],
        { actionEmitter: service['dialogActionEmitter'] }
      ]);

      // Delete action
      actionCommand = service.getActionCommand(AcdmActionType.delete, mockAcdmItem);
      actionCommand();

      expect(acdmDialogServiceSpy.openDialog).toHaveBeenCalledWith([
        AcdmActionType.delete,
        mockAcdmItem,
        MasterDataType.Adm,
        service['formData'],
        { actionEmitter: service['dialogActionEmitter'] }
      ]);
    });

    it('should do anything on any action not declared in AcdmActionType model', () => {
      acdmDialogServiceSpy.openDialog.calls.reset();
      routerSpy.navigate.calls.reset();

      actionCommand = service.getActionCommand(GridTableActionType.Archive, mockAcdmItem);
      actionCommand();

      expect(acdmDialogServiceSpy.openDialog).toHaveBeenCalledTimes(0);
      expect(routerSpy.navigate).toHaveBeenCalledTimes(0);
    });
  });

  it('should call with proper url and return ACDM bsp configuration', fakeAsync(() => {
    let acdmBspConfig: AdmAcmBspConfig;

    httpClientSpy.get.and.returnValue(of([mockAcdmBspConfig]));
    service.getBspConf(1).subscribe(conf => (acdmBspConfig = conf));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsps/1/acdm-configuration');
    expect(acdmBspConfig).toEqual(mockAcdmBspConfig);
  }));

  it('should call with proper url and return ACDM airline configuration', fakeAsync(() => {
    let acdmAirlineConfig: AdmAcmAirlineConfig;

    httpClientSpy.get.and.returnValue(of(mockAcdmAirlineConfig));
    service.getAirlConfig(7784505048).subscribe(conf => (acdmAirlineConfig = conf));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/airlines/7784505048/configuration');
    expect(acdmAirlineConfig).toEqual(mockAcdmAirlineConfig);
  }));

  describe('get TOCA types dropdown depending on ACDM type', () => {
    let tocaTypeDropdown: Array<{ label: string; value: string }>;
    const mockTocaTypes: TocaTypeViewModel[] = [
      {
        bspAcdmConfigId: 1,
        bspId: 1,
        description: 'Tax on Commission 10',
        id: 2,
        sequenceNumber: 2,
        type: 'TOCA10'
      }
    ];

    it('should return an empty dropdown on ACDMD type', fakeAsync(() => {
      httpClientSpy.get.and.returnValue(of(mockTocaTypes));
      httpClientSpy.get.calls.reset();

      service.admAcmType = MasterDataType.Acmd;
      service.getTOCATypesDropdown(1).subscribe(list => (tocaTypeDropdown = list));

      expect(tocaTypeDropdown).toEqual([]);
    }));

    it('should call with proper url and return correct dropdown on not ACDMD type', fakeAsync(() => {
      httpClientSpy.get.and.returnValue(of(mockTocaTypes));
      service.admAcmType = MasterDataType.Adm;
      service.getTOCATypesDropdown(1).subscribe(list => (tocaTypeDropdown = list));

      expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsps/1/acdm-tctp-configuration');
      expect(tocaTypeDropdown).toEqual([{ label: 'TOCA10', value: 'TOCA10' }]);
    }));
  });

  it('should return agent vat field visibility correctly', fakeAsync(() => {
    let agentVatFieldVisibility: boolean;
    const vatFieldsVisibility: Array<boolean> = [false, true];

    spyOn<any>(service, 'areVatFieldsVisible').and.returnValue(of(vatFieldsVisibility));
    service.isAgentVatFieldVisible('vat_number 005').subscribe(visible => (agentVatFieldVisibility = visible));

    expect(agentVatFieldVisibility).toBe(false);
  }));

  it('should return airline vat field visibility correctly', fakeAsync(() => {
    let airlineVatFieldVisibility: boolean;
    const vatFieldsVisibility: Array<boolean> = [false, true];

    spyOn<any>(service, 'areVatFieldsVisible').and.returnValue(of(vatFieldsVisibility));
    service.isAirlineVatFieldVisible(null).subscribe(visible => (airlineVatFieldVisibility = visible));

    expect(airlineVatFieldVisibility).toBe(true);
  }));

  describe('get net reporting visibility depending on acdm spam and access type', () => {
    let netReportingVisibility: boolean;

    it('should be TRUE when acdm spam enabled on create/editTicket mode and whatever NR value', fakeAsync(() => {
      service['bspConfig'].next(mockAcdmBspConfig);

      service.accessType = AccessType.create;
      service.isNetReportingVisible(true).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(true);

      service.accessType = AccessType.editTicket;
      service.isNetReportingVisible(false).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(true);
    }));

    it('should be FALSE when acdm spam disabled on create/editTicket mode and whatever NR value', fakeAsync(() => {
      service['bspConfig'].next({ ...mockAcdmBspConfig, acdmSpam: false });

      service.accessType = AccessType.create;
      service.isNetReportingVisible(false).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(false);

      service.accessType = AccessType.editTicket;
      service.isNetReportingVisible(true).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(false);
    }));

    it('should be TRUE when acdm spam enabled on read/edit mode and whatever NR value', fakeAsync(() => {
      service['bspConfig'].next(mockAcdmBspConfig);

      service.accessType = AccessType.read;
      service.isNetReportingVisible(false).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(true);

      service.accessType = AccessType.edit;
      service.isNetReportingVisible(true).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(true);
    }));

    it('should return correct visibility depending on NR param value when acdm spam disabled on read/edit mode', fakeAsync(() => {
      service['bspConfig'].next({ ...mockAcdmBspConfig, acdmSpam: false });

      service.accessType = AccessType.read;
      service.isNetReportingVisible(false).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(false);

      service.accessType = AccessType.edit;
      service.isNetReportingVisible(true).subscribe(visible => (netReportingVisibility = visible));
      expect(netReportingVisibility).toBe(true);
    }));
  });

  describe('get RMIC visibility depending on acdm and access type', () => {
    it('should be FALSE on ACDM REQUEST', () => {
      service['_admAcmType'] = MasterDataType.Admq;
      expect(service.isRMICVisible()).toBe(false);
    });

    it('should be FALSE on ACDNT and READ mode', () => {
      service['_admAcmType'] = MasterDataType.Acnt;
      service.accessType = AccessType.read;
      expect(service.isRMICVisible()).toBe(false);
    });

    it('should be TRUE on ACDNT and no READ mode', () => {
      service['_admAcmType'] = MasterDataType.Adnt;
      service.accessType = AccessType.create;
      expect(service.isRMICVisible()).toBe(true);
    });

    it('should be FALSE on ACDMD', () => {
      service['_admAcmType'] = MasterDataType.Acmd;
      expect(service.isRMICVisible()).toBe(false);
    });

    it('should be TRUE on any other ACDM type', () => {
      service['_admAcmType'] = MasterDataType.Adm;
      expect(service.isRMICVisible()).toBe(true);
    });
  });

  describe('get numbering configuration depending on BSP configuration, ACDM type and numbering mode', () => {
    let numberingConfiguration;

    beforeEach(() => service['bspConfig'].next(mockAcdmBspConfig));

    it('should return correct configuration without prefix or length on ACDMQ type', fakeAsync(() => {
      service.admAcmType = MasterDataType.Acmq;
      service['bspConfig'].next(mockAcdmBspConfig);
      service.getNumberingConfiguration().subscribe(conf => (numberingConfiguration = conf));

      expect(numberingConfiguration).toEqual({
        mode: AcdmNumberingMode.automatic,
        prefix: null,
        length: null
      });
    }));

    it('should return correct configuration without prefix or length on AUTO numbering', fakeAsync(() => {
      service.admAcmType = MasterDataType.Adm;
      service['bspConfig'].next(mockAcdmBspConfig);
      service.getNumberingConfiguration().subscribe(conf => (numberingConfiguration = conf));

      expect(numberingConfiguration).toEqual({
        mode: AcdmNumberingMode.automatic,
        prefix: null,
        length: null
      });
    }));

    it('should return correct configuration on not ACDMQ type and MANUAL numbering', fakeAsync(() => {
      service['bspConfig'].next({ ...mockAcdmBspConfig, acdmCaptureMode: '7' });

      // ADM family type
      service.admAcmType = MasterDataType.Adm;
      service.getNumberingConfiguration().subscribe(conf => (numberingConfiguration = conf));

      expect(numberingConfiguration).toEqual({
        mode: AcdmNumberingMode.manual,
        prefix: '6000',
        length: 11
      });

      // ACM family type
      service.admAcmType = MasterDataType.Acm;
      service.getNumberingConfiguration().subscribe(conf => (numberingConfiguration = conf));

      expect(numberingConfiguration).toEqual({
        mode: AcdmNumberingMode.manual,
        prefix: '8000',
        length: 10
      });
    }));
  });

  describe('get numbering mode depending on ACDM type', () => {
    it('should return AUTO mode on ACDMQ type', () => {
      service.admAcmType = MasterDataType.Admq;
      expect(service['getNumberingMode'](null)).toBe(AcdmNumberingMode.automatic);
    });

    it('should return AUTO mode on 8 or 9 configuration number mode', () => {
      service.admAcmType = MasterDataType.Acmd;
      expect(service['getNumberingMode']('8')).toBe(AcdmNumberingMode.automatic);
      expect(service['getNumberingMode']('9')).toBe(AcdmNumberingMode.automatic);
    });

    it('should return MANUAL mode on not ACDMQ type nor 8 nor 9 configuration number mode', () => {
      service.admAcmType = MasterDataType.Acmd;
      expect(service['getNumberingMode']('7')).toBe(AcdmNumberingMode.manual);
    });
  });

  describe('get vat fields visibility according to bsp configuration and access mode', () => {
    let vatFieldsVisibility: Array<boolean>;

    beforeEach(() => service['bspConfig'].next(mockAcdmBspConfig));

    it('should return bsp config vat fields visibility on create/editTicket mode', fakeAsync(() => {
      service.accessType = AccessType.create;
      service['areVatFieldsVisible']('53242412').subscribe(visible => (vatFieldsVisibility = visible));
      expect(vatFieldsVisibility).toEqual([true, false]);

      service.accessType = AccessType.editTicket;
      service['areVatFieldsVisible']('6234141').subscribe(visible => (vatFieldsVisibility = visible));
      expect(vatFieldsVisibility).toEqual([true, false]);
    }));

    it('should return correct visibility on read/edit mode', fakeAsync(() => {
      service.accessType = AccessType.read;
      service['areVatFieldsVisible'](null, '2342432').subscribe(visible => (vatFieldsVisibility = visible));
      expect(vatFieldsVisibility).toEqual([true, true]);

      service.accessType = AccessType.edit;
      service['areVatFieldsVisible']('12312321').subscribe(visible => (vatFieldsVisibility = visible));
      expect(vatFieldsVisibility).toEqual([true, false]);
    }));
  });

  describe('should initialize data subjects to be replayed just 1 time', () => {
    let count = 0;

    beforeEach(() => (count = 0));

    it('latency days subject', fakeAsync(() => {
      service['latencyDaysSubj'].next();
      service['latencyDaysSubj'].next();
      service['latencyDaysSubj'].subscribe(() => count++);

      expect(count).toBe(1);
    }));

    it('bsp configuration subject', fakeAsync(() => {
      service['bspConfig'].next();
      service['bspConfig'].next();
      service['bspConfig'].subscribe(() => count++);

      expect(count).toBe(1);
    }));

    it('airline configuration subject', fakeAsync(() => {
      service['airlineConfig'].next();
      service['airlineConfig'].next();
      service['airlineConfig'].subscribe(() => count++);

      expect(count).toBe(1);
    }));

    it('handling fee subject', fakeAsync(() => {
      service['handlingFeeSubj'].next();
      service['handlingFeeSubj'].next();
      service['handlingFeeSubj'].subscribe(() => count++);

      expect(count).toBe(1);
    }));

    it('penalty charge subject', fakeAsync(() => {
      service['penaltyChargeSubj'].next();
      service['penaltyChargeSubj'].next();
      service['penaltyChargeSubj'].subscribe(() => count++);

      expect(count).toBe(1);
    }));
  });

  it('should initialize amount configuration fields subject', fakeAsync(() => {
    let amountConfig: AmountCalculationFieldConfig;
    service['amountConfigFieldsSubj'].subscribe(value => (amountConfig = value));
    expect(amountConfig).toBeTruthy();
  }));

  describe('initialize bsp configuration listener method and private auxiliary methods', () => {
    // Initialize method tests

    describe('should initialize bsp configuration listener', () => {
      const mockBsp: BspDto = { id: 1, isoCountryCode: 'ES', name: 'SPAIN' };

      it('should not emit any bsp configuration when no bsp has found in form', fakeAsync(() => {
        let acdmBspConfig: AdmAcmBspConfig;

        service['formData'].getSubject(AdmAcmActionEmitterType.bsp).next(null);
        service.bspConfig.subscribe(conf => (acdmBspConfig = conf));

        expect(acdmBspConfig).toBeUndefined();
      }));

      it('should NOT emit null bsp configuration and should call right methods', fakeAsync(() => {
        let acdmBspConfig: AdmAcmBspConfig;

        spyOn(service, 'getBspConf').and.returnValue(of(null));
        spyOn<any>(service, 'setDefaultBspConfigValues').and.callThrough();

        service['formData'].getSubject(AdmAcmActionEmitterType.bsp).next(mockBsp);
        tick();

        service.bspConfig.subscribe(conf => (acdmBspConfig = conf));

        expect(service.getBspConf).toHaveBeenCalledWith(1);
        expect(service['setDefaultBspConfigValues']).toHaveBeenCalled();
        expect(acdmBspConfig).toBeUndefined();
      }));

      it('should emit correct bsp configuration and call right methods', fakeAsync(() => {
        let acdmBspConfig: AdmAcmBspConfig;

        spyOn(service, 'getBspConf').and.returnValue(of(mockAcdmBspConfig));
        spyOn<any>(service, 'updateAmountConfigFields').and.callThrough();
        spyOn<any>(service, 'updateBspLatencyDays').and.callThrough();

        service['formData'].getSubject(AdmAcmActionEmitterType.bsp).next(mockBsp);
        tick();

        service.bspConfig.subscribe(conf => (acdmBspConfig = conf));

        expect(service.getBspConf).toHaveBeenCalledWith(1);
        expect(service['updateAmountConfigFields']).toHaveBeenCalledWith(false);
        expect(service['updateBspLatencyDays']).toHaveBeenCalledWith(mockAcdmBspConfig);
        expect(acdmBspConfig).toEqual(mockAcdmBspConfig);
      }));
    });

    // Private auxiliary methods tests

    it('should set default bsp configuration values', fakeAsync(() => {
      let amountConfig: AmountCalculationFieldConfig;
      let latencyDays: number;

      service['setDefaultBspConfigValues']();
      service['amountConfigFieldsSubj'].subscribe(config => (amountConfig = config));
      service['latencyDaysSubj'].subscribe(days => (latencyDays = days));

      expect(amountConfig.taxOnCommission.sign).toBe(1);
      expect(latencyDays).toBe(15);
    }));

    it('should update amount configuration fields', fakeAsync(() => {
      let amountConfig: AmountCalculationFieldConfig;

      spyOn(service['amountConfigFieldsSubj'], 'next').and.callThrough();

      service['updateAmountConfigFields'](true);
      service['amountConfigFieldsSubj'].subscribe(config => (amountConfig = config));
      expect(amountConfig.taxOnCommission.sign).toBe(1);
      expect(service['amountConfigFieldsSubj'].next).toHaveBeenCalledTimes(0);

      service['updateAmountConfigFields'](false);
      service['amountConfigFieldsSubj'].subscribe(config => (amountConfig = config));
      expect(amountConfig.taxOnCommission.sign).toBe(-1);
      expect(service['amountConfigFieldsSubj'].next).toHaveBeenCalled();
    }));

    it('should update bsp latency days depending on ACDM type', fakeAsync(() => {
      let latencyDays: number;

      service['_admAcmType'] = MasterDataType.Adm;
      service['updateBspLatencyDays'](mockAcdmBspConfig);
      service['latencyDaysSubj'].subscribe(days => (latencyDays = days));
      expect(latencyDays).toBe(15);

      service['_admAcmType'] = MasterDataType.Admd;
      service['updateBspLatencyDays'](mockAcdmBspConfig);
      service['latencyDaysSubj'].subscribe(days => (latencyDays = days));
      expect(latencyDays).toBe(0);
    }));
  });

  describe('initialize airline configuration listener method and private auxiliary methods', () => {
    // Initialize method tests

    describe('should initialize airline configuration listener', () => {
      const mockAirlineView: AirlineViewModel = {
        id: 1,
        iataCode: '060',
        localName: 'Airline 1',
        globalName: 'AIRLINE1',
        vatNumber: '2354321432141',
        bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' },
        contact: { contactName: 'Phillips', email: 'phillips@airline1.com', phoneFaxNumber: '23423432' },
        address: null
      };

      it('should not emit any airline configuration when no airline has found in form', fakeAsync(() => {
        let acdmAirlineConfig: AdmAcmAirlineConfig;

        service['formData'].getSubject(AdmAcmActionEmitterType.airline).next(null);
        service.airlineConfig.subscribe(conf => (acdmAirlineConfig = conf));

        expect(acdmAirlineConfig).toBeUndefined();
      }));

      it('should emit null airline configuration and call right methods', fakeAsync(() => {
        let acdmAirlineConfig: AdmAcmAirlineConfig;

        spyOn(service, 'getAirlConfig').and.returnValue(of(null));

        service['formData'].getSubject(AdmAcmActionEmitterType.airline).next(mockAirlineView);
        service.airlineConfig.subscribe(conf => (acdmAirlineConfig = conf));

        expect(service.getAirlConfig).toHaveBeenCalledWith(1);
        expect(acdmAirlineConfig).toBeNull();
      }));

      it('should emit correct airline configuration and call right methods', fakeAsync(() => {
        let acdmAirlineConfig: AdmAcmAirlineConfig;

        spyOn(service, 'getAirlConfig').and.returnValue(of(mockAcdmAirlineConfig));
        spyOn<any>(service, 'updateHandlingFee').and.callFake(() => {});
        spyOn<any>(service, 'updatePenaltyCharge').and.callFake(() => {});

        service['formData'].getSubject(AdmAcmActionEmitterType.airline).next(mockAirlineView);
        tick();

        service.airlineConfig.subscribe(conf => (acdmAirlineConfig = conf));

        expect(service.getAirlConfig).toHaveBeenCalledWith(1);
        expect(service['updateHandlingFee']).toHaveBeenCalledWith(mockAcdmAirlineConfig);
        expect(service['updatePenaltyCharge']).toHaveBeenCalledWith(mockAcdmAirlineConfig);
        expect(acdmAirlineConfig).toEqual(mockAcdmAirlineConfig);
      }));
    });

    // Private auxiliary methods tests

    describe('should update handling fee', () => {
      it('should emit the correct handling fee parameter value', fakeAsync(() => {
        let handlingFee: boolean;

        service['updateHandlingFee']({
          ...mockAcdmAirlineConfig,
          parameters: [{ name: AirlineConfigParamName.handlingFee, value: 'true' } as AirlineConfigParameters]
        });
        service['handlingFeeSubj'].subscribe(hf => (handlingFee = hf));
        expect(handlingFee).toBe(true);

        service['updateHandlingFee']({
          ...mockAcdmAirlineConfig,
          parameters: [{ name: AirlineConfigParamName.handlingFee, value: 'false' } as AirlineConfigParameters]
        });
        service['handlingFeeSubj'].subscribe(hf => (handlingFee = hf));
        expect(handlingFee).toBe(false);
      }));
    });

    describe('should update penalty charge', () => {
      it('should emit the correct penalty charge parameter value', fakeAsync(() => {
        let penaltyCharge: boolean;

        service['updatePenaltyCharge']({
          ...mockAcdmAirlineConfig,
          parameters: [{ name: AirlineConfigParamName.penaltyCharge, value: 'true' } as AirlineConfigParameters]
        });
        service['penaltyChargeSubj'].subscribe(pc => (penaltyCharge = pc));
        expect(penaltyCharge).toBe(true);

        service['updatePenaltyCharge']({
          ...mockAcdmAirlineConfig,
          parameters: [{ name: AirlineConfigParamName.penaltyCharge, value: 'false' } as AirlineConfigParameters]
        });
        service['penaltyChargeSubj'].subscribe(pc => (penaltyCharge = pc));
        expect(penaltyCharge).toBe(false);
      }));
    });
  });

  it('should reset all data subjects', fakeAsync(() => {
    let acdmBspConfig: AdmAcmBspConfig;
    let handlingFee: boolean;

    service.bspConfig.next(mockAcdmBspConfig);
    service['handlingFeeSubj'].next(true);
    service.resetData();
    service.bspConfig.subscribe(conf => (acdmBspConfig = conf));
    service['handlingFeeSubj'].subscribe(hf => (handlingFee = hf));

    expect(acdmBspConfig).toBeUndefined();
    expect(handlingFee).toBeUndefined();
  }));

  describe('get TOCA validation depending on TOCA type and amount from form data', () => {
    let tocaErrorType: TocaErrorsType;

    it('should return amount TOCA error on TOCA type selected and no TOCA amount introduced', fakeAsync(() => {
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaType).next('TOCA1');
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaAmount).next(null);
      service.validateToca().subscribe(error => (tocaErrorType = error));

      expect(tocaErrorType).toBe(TocaErrorsType.amount);
    }));

    it('should not return TOCA error on TOCA type selected and TOCA amount different to 0', fakeAsync(() => {
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaType).next('TOCA1');
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaAmount).next(3);
      service.validateToca().subscribe(error => (tocaErrorType = error));

      expect(tocaErrorType).toBeNull();
    }));

    it('should return amount TOCA error on TOCA type selected and TOCA amount equal to 0', fakeAsync(() => {
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaType).next('TOCA1');
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaAmount).next(0);
      service.validateToca().subscribe(error => (tocaErrorType = error));

      expect(tocaErrorType).toBe(TocaErrorsType.amount);
    }));

    it('should return type TOCA error on no TOCA type selected and TOCA amount different to 0', fakeAsync(() => {
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaType).next(null);
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaAmount).next(1);
      service.validateToca().subscribe(error => (tocaErrorType = error));

      expect(tocaErrorType).toBe(TocaErrorsType.type);
    }));

    it('should not return TOCA error on no TOCA type selected and TOCA amount equal to 0', fakeAsync(() => {
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaType).next(null);
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaAmount).next(0);
      service.validateToca().subscribe(error => (tocaErrorType = error));

      expect(tocaErrorType).toBeNull();
    }));

    it('should not return TOCA error on no TOCA type selected and no TOCA amount introduced', fakeAsync(() => {
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaType).next(null);
      service['formData'].getSubject(AdmAcmActionEmitterType.tocaAmount).next(null);
      service.validateToca().subscribe(error => (tocaErrorType = error));

      expect(tocaErrorType).toBeNull();
    }));
  });
});
