import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AdmAcmActivityHistoryDialogComponent } from '../components/dialogs/adm-acm-activity-history-dialog/adm-acm-activity-history-dialog.component';
import { AdmAcmInternalCommentDialogComponent } from '../components/dialogs/adm-acm-internal-comment-dialog/adm-acm-internal-comment-dialog.component';
import { AcdmActionType } from '../models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '../models/adm-acm-issue.model';
import {
  getAcdmCreateInternalCommentPermission,
  getAcdmDeleteInternalCommentPermission,
  getAcdmUpdateInternalCommentPermission
} from '../shared/helpers/adm-acm-permissions.config';
import { AdmAcmDataService } from './adm-acm-data.service';
import { AdmAcmConfirmationDialogComponent } from '~app/adm-acm/components/dialogs/adm-acm-confirmation-dialog/adm-acm-confirmation-dialog.component';
import { AdmAcmErrorDialogComponent } from '~app/adm-acm/components/dialogs/adm-acm-error-dialog/adm-acm-error-dialog.component';
import { AdmAcmNotFoundDialogComponent } from '~app/adm-acm/components/dialogs/adm-acm-not-found-dialog/adm-acm-not-found-dialog.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ResponseErrorBE } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Injectable({ providedIn: 'root' })
export class AdmAcmDialogService {
  private footerButtons: { [key in AcdmActionType]?: FooterButton } = {
    [AcdmActionType.approve]: FooterButton.Approve,
    [AcdmActionType.authorizeRequest]: FooterButton.Authorize,
    [AcdmActionType.deactivate]: FooterButton.Deactivate,
    [AcdmActionType.dispute]: FooterButton.Dispute,
    [AcdmActionType.forwardGds]: FooterButton.ForwardGds,
    [AcdmActionType.issue]: FooterButton.Issue,
    [AcdmActionType.issuePendingSupervision]: FooterButton.Issue,
    [AcdmActionType.supervise]: FooterButton.Issue,
    [AcdmActionType.reactivate]: FooterButton.Reactivate,
    [AcdmActionType.reject]: FooterButton.Reject,
    [AcdmActionType.rejectRequest]: FooterButton.Reject,
    [AcdmActionType.delete]: FooterButton.Delete,
    [AcdmActionType.editReactivateACDM]: FooterButton.Reactivate,
    [AcdmActionType.editRejectACDM]: FooterButton.Reject
  };

  private hasCreateInternalCommentPermission: boolean;
  private hasUpdateInternalCommentPermission: boolean;
  private hasDeleteInternalCommentPermission: boolean;

  constructor(private dialog: DialogService, private permissionsService: PermissionsService) {}

  public openDialog(
    acdmAction: AcdmActionType,
    acdmItem: AdmAcmIssueBE,
    dataType: MasterDataType,
    formData: AdmAcmDataService,
    extraConfig?: any
  ): Observable<any> {
    return this.dialog.open(AdmAcmConfirmationDialogComponent, {
      data: {
        footerButtonsType: this.footerButtons[acdmAction],
        hasCancelButton: true,
        isClosable: true,
        rowTableContent: acdmItem,
        actionType: acdmAction,
        dataType,
        formData,
        ...extraConfig
      }
    });
  }

  public openErrorDialog(errorToManage: ResponseErrorBE, dataType: MasterDataType): Observable<any> {
    return this.dialog.open(AdmAcmErrorDialogComponent, {
      data: {
        footerButtonsType: FooterButton.Ok,
        rowTableContent: errorToManage,
        hasCancelButton: false,
        isClosable: false,
        dataType
      }
    });
  }

  public openDialogNotFoundDialogComponent(): Observable<any> {
    return this.dialog.open(AdmAcmNotFoundDialogComponent, {
      data: {
        footerButtonsType: FooterButton.Ok,
        hasCancelButton: false,
        isClosable: false
      }
    });
  }

  public openInternalCommentDialog(acdm: AdmAcmIssueBE, acdmType: MasterDataType): void {
    // Permissions
    this.hasCreateInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmCreateInternalCommentPermission(acdmType)
    );
    this.hasUpdateInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmUpdateInternalCommentPermission(acdmType)
    );
    this.hasDeleteInternalCommentPermission = this.permissionsService.hasPermission(
      getAcdmDeleteInternalCommentPermission(acdmType)
    );

    // Dialog configuration
    const dialogConfig: DialogConfig = {
      data: {
        acdm,
        acdmType,
        hasCancelButton: false,
        isReadonly: this.isInternalCommentDialogReadonly(acdm),
        footerButtonsType: this.buildInternalCommentDialogButtons(acdm, acdmType)
      }
    };

    this.dialog.open(AdmAcmInternalCommentDialogComponent, dialogConfig);
  }

  public openActivityHistoryDialog(acdm: AdmAcmIssueBE, acdmType: MasterDataType): void {
    const dialogConfig: DialogConfig = {
      data: {
        acdm,
        acdmType,
        hasCancelButton: false,
        footerButtonsType: { type: FooterButton.Close, buttonDesign: ButtonDesign.Primary },
        title: `${acdm.transactionCode} ${acdm.ticketDocumentNumber}`
      }
    };

    this.dialog.open(AdmAcmActivityHistoryDialogComponent, dialogConfig);
  }

  private isInternalCommentDialogReadonly(acdm: AdmAcmIssueBE): boolean {
    let isReadonly = true;

    if (
      (!acdm.internalComment && this.hasCreateInternalCommentPermission) ||
      (acdm.internalComment && this.hasUpdateInternalCommentPermission)
    ) {
      // User wants to create an internal comment and has the right permission for it
      isReadonly = false;
    }

    return isReadonly;
  }

  private buildInternalCommentDialogButtons(acdm: AdmAcmIssueBE, acdmType: MasterDataType): ModalAction[] {
    const deleteButton: ModalAction = {
      type: FooterButton.Delete,
      isDisabled: !acdm.internalComment,
      buttonDesign: ButtonDesign.Secondary
    };
    const postButton: ModalAction = { type: FooterButton.Post, isDisabled: true };
    const cancelButton = { type: FooterButton.Cancel, buttonDesign: ButtonDesign.Tertiary };
    const doneButton = { type: FooterButton.Done, buttonDesign: ButtonDesign.Tertiary };

    const buttons = [
      ...this.insertIf(this.hasDeleteInternalCommentPermission, deleteButton),
      ...this.insertIf(this.hasCreateInternalCommentPermission && !acdm.internalComment, postButton),
      ...this.insertIf(this.hasUpdateInternalCommentPermission && !!acdm.internalComment, postButton)
    ];

    return buttons.length ? [cancelButton, ...buttons] : [doneButton];
  }

  private insertIf(condition: boolean, button: ModalAction): ModalAction[] {
    return condition ? [button] : [];
  }
}
