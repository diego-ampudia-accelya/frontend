import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AdmPolicyGlobalFileComponent } from '../components/adm-policy-global-file/adm-policy-global-file.component';

@Injectable()
export class AdmPolicyGlobalFileGuard implements CanActivate {
  constructor(private dialogService: DialogService) {}

  public canActivate(): Observable<boolean> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'ADM_ACM.policyGlobalFile.title',
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Accept }],
        buttons: [{ type: FooterButton.Accept }]
      }
    };

    return this.dialogService.open(AdmPolicyGlobalFileComponent, dialogConfig);
  }
}
