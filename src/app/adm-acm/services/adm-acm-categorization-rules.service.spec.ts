import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { DataQuery } from '~app/shared/components/list-view';
import { AirlineSummary, DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';
import {
  AcmAdmRuleDetail,
  AdmAcmRule,
  AdmAcmRuleFilter,
  PositionRuleType,
  StatusRuleType
} from '../models/adm-acm-rule.model';
import { AdmAcmCategorizationRulesService } from './adm-acm-categorization-rules.service';
import { BspDto } from '~app/shared/models/bsp.model';

const admAcmRuleFilter: AdmAcmRuleFilter = {
  primaryReason: {
    id: 1,
    primaryReason: 'primary',
    status: 'active'
  },
  subReason: [
    {
      id: 2,
      subReason: 'subReason',
      status: 'active'
    }
  ],
  status: StatusRuleType.ACTIVE
};

const airlineCode: AirlineSummary = {
  id: 1,
  name: 'AIRLINE 001',
  code: '001',
  designator: 'L1'
};

const isoCountryCodes: BspDto[] = [
  {
    id: 1,
    name: 'bspName',
    isoCountryCode: 'ES'
  }
];

const ruleDetail: AcmAdmRuleDetail = {
  id: 1,
  airlineCode,
  status: StatusRuleType.ACTIVE,
  isoCountryCodes,
  position: PositionRuleType.STARTS_WITH,
  primaryReasonId: 1,
  subReasonId: 22,
  pattern: 'Start'
};

const rule: AdmAcmRule = {
  id: 1,
  airlineCode: 'code',
  status: StatusRuleType.ACTIVE,
  isoCountryCodes: [],
  position: PositionRuleType.STARTS_WITH,
  primaryReasonId: 1,
  subReasonId: 22,
  pattern: 'Start'
};

describe('AdmAcmCategorizationRulesService', () => {
  let admAcmCategorizationRulesService: AdmAcmCategorizationRulesService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of(httpResponse));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        AdmAcmCategorizationRulesService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ]
    });

    admAcmCategorizationRulesService = TestBed.inject(AdmAcmCategorizationRulesService);
  });

  it('should create', () => {
    expect(admAcmCategorizationRulesService).toBeDefined();
  });

  it('should get acdmRule', () => {
    admAcmCategorizationRulesService['_acdmRule'] = null;
    admAcmCategorizationRulesService.acdmRule = rule;

    expect(admAcmCategorizationRulesService.acdmRule).toEqual(rule);
  });

  it('should send proper find request', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: admAcmRuleFilter,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    const expectedUrl = '/acdm-management/adm-rules?page=0&size=20&status=Active&primaryReasonId=1&subReasonId=2';

    admAcmCategorizationRulesService.find(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: admAcmRuleFilter,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl =
      '/acdm-management/adm-rules/download?status=Active&primaryReasonId=1&subReasonId=2&downloadType=txt';

    admAcmCategorizationRulesService.download(query, exportOptions).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));

  it('should call proper url when getRuleById', fakeAsync(() => {
    const expectedUrl = '/acdm-management/adm-rules/1';

    admAcmCategorizationRulesService.getRuleById(1).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should call proper url when getSubReasonRules', fakeAsync(() => {
    const expectedUrl = '/acdm-management/adm-rules/primary-reasons/1/sub-reasons';

    admAcmCategorizationRulesService['getSubReasonRules'](1, true).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
      params: {
        status: 'ENABLED'
      }
    });
  }));

  it('should call proper url when getPrimaryReasonRules', fakeAsync(() => {
    const expectedUrl = '/acdm-management/adm-rules/primary-reasons';

    admAcmCategorizationRulesService['getPrimaryReasonRules'](true).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl, {
      params: {
        status: 'ENABLED'
      }
    });
  }));

  it('should call proper url when update', fakeAsync(() => {
    const expectedUrl = '/acdm-management/adm-rules/10';

    admAcmCategorizationRulesService.update(ruleDetail, 10);

    expect(httpSpy.put).toHaveBeenCalledWith(expectedUrl, ruleDetail);
  }));

  it('should call proper url when rerun', fakeAsync(() => {
    const expectedUrl = '/acdm-management/adm-rules/10/rerun';

    admAcmCategorizationRulesService.rerun({ active: true }, 10);

    expect(httpSpy.post).toHaveBeenCalledWith(expectedUrl, { active: true });
  }));

  it('should call proper url when createRule', fakeAsync(() => {
    const expectedUrl = '/acdm-management/adm-rules';
    const rulrdetailConverted = {
      id: 1,
      airlineCode: '001',
      status: 'Active',
      isoCountryCodes: ['ES'],
      position: 'Starts with',
      primaryReasonId: 1,
      subReasonId: 22,
      pattern: 'Start',
      concernsIndicator: undefined
    };

    admAcmCategorizationRulesService.createRule(ruleDetail);

    expect(httpSpy.post).toHaveBeenCalledWith(expectedUrl, rulrdetailConverted);
  }));

  it('should call getPrimaryReasonRules when getPrimaryReasonDropDownValueId is called', fakeAsync(() => {
    const getPrimaryReasonRulesSpy = spyOn<any>(admAcmCategorizationRulesService, 'getPrimaryReasonRules');
    getPrimaryReasonRulesSpy.and.returnValue(of([]));

    admAcmCategorizationRulesService.getPrimaryReasonDropDownValueId(true);

    expect(getPrimaryReasonRulesSpy).toHaveBeenCalled();
  }));

  it('should call getSubReasonRules when getSubReasonDropDownValueId is called', fakeAsync(() => {
    const getSubReasonRulesSpy = spyOn<any>(admAcmCategorizationRulesService, 'getSubReasonRules');
    getSubReasonRulesSpy.and.returnValue(of([]));

    admAcmCategorizationRulesService.getSubReasonDropDownValueId(10, true);
    tick();

    expect(getSubReasonRulesSpy).toHaveBeenCalled();
  }));
});
