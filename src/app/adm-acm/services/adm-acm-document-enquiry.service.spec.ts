import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { of } from 'rxjs';

import { AdmAcmDocumentSearchForm, AdmAcmDocumentSummary } from '../models/adm-acm-enquiry-document.model';
import {
  AcdmType,
  AdmAcmStatus,
  MarkStatus,
  SendToDpcType,
  TransactionCode
} from '../models/adm-acm-issue-shared-aux.model';
import { AdmAcmIssueBE } from '../models/adm-acm-issue.model';
import { TransactionRuleType } from '../models/adm-acm-rule.model';
import { AdmAcmDocumentEnquiryService } from './adm-acm-document-enquiry.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { downloadRequestOptions } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

const pagedDataMock: PagedData<AdmAcmDocumentSummary> = {
  records: [],
  pageSize: 0,
  total: 0,
  totalPages: 0,
  pageNumber: 1
};

const documentMock: AdmAcmDocumentSummary = {
  id: '123123',
  ticketDocumentNumber: '1231231232 1',
  agent: {
    id: 465456,
    iataCode: '7550010',
    name: 'NOMBRE 1',
    effectiveFrom: '2000-01-01',
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01'
    }
  },
  airline: {
    id: 6983485553,
    iataCode: '075',
    localName: 'Nombre 45'
  },
  currency: {
    id: 698582,
    code: 'EUR',
    decimals: 2
  },
  dateOfIssue: '2022-01-31',
  reportingDate: '2022-02-07',
  sentToDpc: SendToDpcType.pending,
  markStatus: MarkStatus.pending,
  acdmStatus: AdmAcmStatus.pending,
  totalAmount: 100,
  concernsIndicator: TransactionRuleType.ISSUE,
  transactionCode: TransactionCode.acm,
  period: '2022021'
};

describe('AdmAcmDocumentEnquiryService', () => {
  let service: AdmAcmDocumentEnquiryService;
  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        AdmAcmDocumentEnquiryService,
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        { provide: HttpClient, useValue: httpClientSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(AdmAcmDocumentEnquiryService);
    httpClientSpy.get.calls.reset();
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should return Document by ticketDocumentNumber', fakeAsync(() => {
    const documentResponse = {
      ...pagedDataMock,
      records: [documentMock]
    };

    httpClientSpy.get.and.returnValue(of(documentResponse));
    let result;

    service.getBy('1231231232').subscribe(data => (result = data));
    tick();

    expect(result).toBe(documentResponse);
    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/acdm-management/acdms?page=0&size=20&ticketDocumentNumber=1231231232'
    );
  }));

  it('should return Document deletionReason by documentId', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(of({ id: 123123, deletionReason: 'Transaction Error' } as AdmAcmIssueBE));
    let result;

    service.getDocumentDeletionReasonById(123123, AcdmType.ACMA).subscribe(data => (result = data));
    tick();

    expect(result).toBe('Transaction Error');
    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/acms/123123');
  }));

  it('should download file ', fakeAsync(() => {
    const body: ArrayBuffer = new ArrayBuffer(1133825);
    const headers: Map<string, string> = new Map();
    headers.set('Content-Disposition', 'attachment;filename="File_Name_testing_123123.txt"');
    const response = {
      headers: {
        get: key => headers.get(key)
      },
      status: 200,
      statusText: 'OK',
      url: 'test',
      ok: true,
      type: 4,
      body
    };

    const query = {
      ...defaultQuery,
      filterBy: { ...defaultQuery.filterBy, ticketDocumentNumber: '123123' }
    } as DataQuery<AdmAcmDocumentSearchForm>;

    httpClientSpy.get.and.returnValue(of(response));

    let fileDownloaded: any;
    service.download(query, DownloadFormat.TXT, {}).subscribe(file => (fileDownloaded = file));
    tick();

    expect(fileDownloaded.fileName).toEqual('File_Name_testing_123123.txt');
    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/acdm-management/acdms?ticketDocumentNumber=123123&exportAs=TXT',
      downloadRequestOptions
    );
  }));

  it('should detailed download file ', fakeAsync(() => {
    const body: ArrayBuffer = new ArrayBuffer(1133825);
    const headers: Map<string, string> = new Map();
    headers.set('Content-Disposition', 'attachment;filename="File_Name_testing_123123.txt"');
    const response = {
      headers: {
        get: key => headers.get(key)
      },
      status: 200,
      statusText: 'OK',
      url: 'test',
      ok: true,
      type: 4,
      body
    };

    const query = {
      ...defaultQuery,
      filterBy: { ...defaultQuery.filterBy, ticketDocumentNumber: '123123' }
    } as DataQuery<AdmAcmDocumentSearchForm>;

    httpClientSpy.get.and.returnValue(of(response));

    let fileDownloaded: any;
    service.download(query, DownloadFormat.TXT, {}, true).subscribe(file => (fileDownloaded = file));
    tick();

    expect(fileDownloaded.fileName).toEqual('File_Name_testing_123123.txt');
    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/acdm-management/acdms?ticketDocumentNumber=123123&exportAs=TXT&exportDetail=true',
      downloadRequestOptions
    );
  }));
});
