import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import {
  AdmAcmRuleFilter,
  PositionRuleType,
  RmicType,
  StatusRuleType,
  TransactionRuleType
} from '../models/adm-acm-rule.model';

import { AdmAcmCategorizationRulesFormatter } from './adm-acm-categorization-rules-formatter.service';

describe('AdmAcmCategorizationRulesFormatter', () => {
  let formatter: AdmAcmCategorizationRulesFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new AdmAcmCategorizationRulesFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format primaryReason if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      primaryReason: {
        id: 1,
        primaryReason: 'primary',
        status: 'active'
      }
    };
    expect(formatter.format(filter)).toEqual([
      {
        keys: ['primaryReason'],
        label: 'ADM_ACM.categorizationRules.filters.primaryReason.label - primary'
      }
    ]);
  });

  it('should format name if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      name: 'Name'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['name'],
        label: 'ADM_ACM.categorizationRules.filters.name.label - Name'
      }
    ]);
  });

  it('should format status if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      status: StatusRuleType.ACTIVE
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['status'],
        label:
          'ADM_ACM.categorizationRules.filters.status.label - ADM_ACM.categorizationRules.filters.status.options.Active'
      }
    ]);
  });

  it('should format required fields and BSPs if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      bsps: [
        { id: 1, isoCountryCode: 'ES', name: 'SPAIN' },
        { id: 2, isoCountryCode: 'IL', name: 'ISRAEL' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['bsps'],
        label: 'ADM_ACM.categorizationRules.filters.bsp.label - SPAIN, ISRAEL'
      }
    ]);
  });

  it('should format airlineCode if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      airlineCode: {
        id: 102,
        name: 'Qantas',
        code: '081',
        designator: 'QA'
      }
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airlineCode'],
        label: 'ADM_ACM.categorizationRules.filters.airline.label - 081'
      }
    ]);
  });

  it('should format concernsIndicator if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      concernsIndicator: TransactionRuleType.ISSUE
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['concernsIndicator'],
        label: 'ADM_ACM.categorizationRules.filters.transaction.label - Issue'
      }
    ]);
  });

  it('should format pattern if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      pattern: 'pattern'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['pattern'],
        label: 'ADM_ACM.categorizationRules.filters.pattern.label - pattern'
      }
    ]);
  });

  it('should format subReason if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      subReason: [
        {
          id: 1,
          subReason: 'sub1',
          status: 'active'
        },
        {
          id: 2,
          subReason: 'sub2',
          status: 'active'
        }
      ]
    };
    expect(formatter.format(filter)).toEqual([
      {
        keys: ['subReason'],
        label: 'ADM_ACM.categorizationRules.filters.subReason.label - 1, 2'
      }
    ]);
  });

  it('should format reasonForMemoField if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      reasonForMemoField: RmicType.RMIC
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['reasonForMemoField'],
        label: 'ADM_ACM.categorizationRules.filters.reason.label - RMIC'
      }
    ]);
  });

  it('should format position if they exist', () => {
    const filter: AdmAcmRuleFilter = {
      position: PositionRuleType.CONTAINS
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['position'],
        label:
          'ADM_ACM.categorizationRules.filters.position.label - ADM_ACM.categorizationRules.filters.position.options.Contains'
      }
    ]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      name: 'Name',
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['name'],
        label: 'ADM_ACM.categorizationRules.filters.name.label - Name'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
