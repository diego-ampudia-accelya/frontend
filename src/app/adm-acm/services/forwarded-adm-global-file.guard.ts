import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { UserType } from '~app/shared/models/user.model';
import { ForwardedAdmGlobalFileDialogComponent } from '../components/dialogs/forwarded-adm-global-file-dialog/forwarded-adm-global-file-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ForwardedAdmGlobalFileGuard implements CanActivate {
  private user$ = this.store.pipe(select(getUser));

  constructor(private dialogService: DialogService, private store: Store<AppState>) {}

  canActivate(): Observable<boolean> {
    return this.user$.pipe(
      first(),
      tap(user => {
        if (user.userType === UserType.GDS) {
          this.dialogService.open(ForwardedAdmGlobalFileDialogComponent, {
            data: {
              footerButtonsType: FooterButton.Accept,
              mainButtonType: FooterButton.Accept
            }
          });
        }
      }),
      mapTo(false)
    );
  }
}
