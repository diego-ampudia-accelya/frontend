import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';

import { AdmAcmRuleFilter } from '../models/adm-acm-rule.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AdmAcmCategorizationRulesFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: AdmAcmRuleFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<AdmAcmRuleFilter> = {
      status: status => `${this.translateLabel('status')} - ${this.translateOption('status', status)}`,
      bsps: bsps => `${this.translateLabel('bsp')} - ${bsps.map(value => value.name).join(', ')}`,
      airlineCode: airlineCode => `${this.translateLabel('airline')} - ${capitalize(airlineCode.code)}`,
      name: name => `${this.translateLabel('name')} - ${name}`,
      concernsIndicator: transactions => `${this.translateLabel('transaction')} - ${transactions}`,
      pattern: pattern => `${this.translateLabel('pattern')} - ${pattern}`,
      primaryReason: primaryReason => `${this.translateLabel('primaryReason')} - ${primaryReason.primaryReason}`,
      subReason: subReasons => `${this.translateLabel('subReason')} - ${subReasons.map(value => value.id).join(', ')}`,
      reasonForMemoField: reason => `${this.translateLabel('reason')} - ${reason}`,
      position: position => `${this.translateLabel('position')} - ${this.translateOption('position', position)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translateLabel(key: string): string {
    return this.translationService.translate(`ADM_ACM.categorizationRules.filters.${key}.label`);
  }

  private translateOption(key: string, value: string): string {
    return this.translationService.translate(`ADM_ACM.categorizationRules.filters.${key}.options.${value}`);
  }
}
