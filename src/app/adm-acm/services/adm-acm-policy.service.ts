import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { AdmAcmPolicyModel } from '../models/adm-acm-policy.model';
import { DataQuery } from '~app/shared/components/list-view';
import { RequestMethod } from '~app/shared/enums';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';
import { DynamicEndpointClass } from '~app/shared/services/endpoints.config';
import { buildEndpointPath } from '~app/shared/services/endpoints.helper';

@Injectable()
export class AdmAcmPolicyService extends ApiService<AdmAcmPolicyModel> {
  constructor(
    http: HttpClient,
    private activatedRoute: ActivatedRoute,
    @Optional() private activatedRouteSnapshot: ActivatedRouteSnapshot
  ) {
    super(http, 'admAcmPolicy');
    const routeSnapshotData = this.activatedRouteSnapshot
      ? this.activatedRouteSnapshot.data
      : this.activatedRoute.snapshot.data;
    this.baseUrl = buildEndpointPath(DynamicEndpointClass.AdmAcmPolicyService, routeSnapshotData);
  }

  public find(dataQuery: DataQuery): Observable<PagedData<AdmAcmPolicyModel>> {
    const modifiedDataQuery = {
      ...dataQuery,
      filterBy: this.formatDateFilter(dataQuery.filterBy)
    };

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const reqQuery = RequestQuery.fromDataQuery(modifiedDataQuery);

    return super.getAll(reqQuery);
  }

  private formatDateFilter(queryfilters: any): RequestQuery {
    const datesFields = 'lastUpdate';

    let queryFiltersModified = { ...queryfilters };

    if (Object.keys(queryfilters).includes(datesFields)) {
      const dateString = toShortIsoDate(queryfilters[datesFields]);
      queryFiltersModified = { ...queryfilters, lastUpdate: dateString };
    }

    return queryFiltersModified;
  }

  public download(query: DataQuery, downloadFormat: string): Observable<{ blob: Blob; fileName: string }> {
    const modifiedDataQuery = {
      ...query,
      filterBy: this.formatDateFilter(query.filterBy)
    };

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery(modifiedDataQuery);

    const filterBy = requestQuery.getQueryString();

    return super.download(requestQuery, `/download${filterBy}&exportAs=${downloadFormat}`, RequestMethod.Get);
  }
}
