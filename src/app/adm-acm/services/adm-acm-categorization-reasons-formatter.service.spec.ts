import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AdmAcmReasonsFilter } from '../models/adm-acm-reason.model';

import { AdmAcmCategorizationReasonsFormatter } from './adm-acm-categorization-reasons-formatter.service';

describe('AdmAcmCategorizationReasonsFormatter', () => {
  let formatter: AdmAcmCategorizationReasonsFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new AdmAcmCategorizationReasonsFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format primaryReason if they exist', () => {
    const filter: AdmAcmReasonsFilter = {
      primaryReason: {
        id: '1',
        primaryReason: 'primary'
      }
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['primaryReason'],
        label: 'ADM_ACM.categorizationReasons.filters.primaryReason.label - primary'
      }
    ]);
  });

  it('should format subReason if they exist', () => {
    const filter: AdmAcmReasonsFilter = {
      subReason: 'subReason'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['subReason'],
        label: 'ADM_ACM.categorizationReasons.filters.subReason.label - subReason'
      }
    ]);
  });

  it('should format active if they exist', () => {
    const filter: AdmAcmReasonsFilter = {
      active: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['active'],
        label:
          'ADM_ACM.categorizationReasons.filters.status.label - ADM_ACM.categorizationReasons.filters.status.options.active'
      }
    ]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      subReason: 'SubReason',
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['subReason'],
        label: 'ADM_ACM.categorizationReasons.filters.subReason.label - SubReason'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
