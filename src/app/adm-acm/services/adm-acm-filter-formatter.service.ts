import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AdmAcmFilter } from '../models/adm-acm-filter.model';
import { anyOptionGdsForwardValue } from '../shared/helpers/adm-acm-general.helper';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { rangeDateFilterTagMapper, rangeFilterTagMapper } from '~app/shared/helpers';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AdmAcmFilterFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService, private periodPipe: PeriodPipe) {}

  public format(filter: AdmAcmFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<AdmAcmFilter>> = {
      ticketDocumentNumber: docNumber => `${this.translate('docNumber')} - ${docNumber}`,
      airlineIataCode: airlines => `${this.translate('airlineTag')} - ${airlines.map(airl => airl.code).join(', ')}`,
      agentId: agents => `${this.translate('agentTag')} - ${agents.map(agt => agt.code).join(', ')}`,
      bsp: bsps => {
        const bspsArray = !Array.isArray(bsps) ? [bsps] : bsps;

        return `${this.translate('countryTag')} - ${bspsArray
          .map(bsp => `${bsp.name} (${bsp.isoCountryCode})`)
          .join(', ')}`;
      },
      acdmStatus: (statusList: string[]) =>
        `${this.translate('status')} - ${statusList
          .map(status => this.translationService.translate(`ADM_ACM.status.${status}`))
          .join(', ')}`,
      gdsForwardCode: gdsForwardCodes =>
        `${this.translate('gdsForward')} - ${gdsForwardCodes
          .map(gds => `${this.translateGdsCode(gds.gdsCode)}`)
          .join(', ')}`,
      sentToDpc: toDpcList =>
        `${this.translate('sentToDpc')} - ${toDpcList
          .map(sent => this.translationService.translate(`ADM_ACM.sentToDpcStatus.${sent}`))
          .join(', ')}`,
      hasInternalComment: hasInternalComment =>
        `${this.translate('internalComment')} - ${this.translationService.translate(
          `ADM_ACM.yesNoBooleanValue.${hasInternalComment}`
        )}`,
      concernsIndicator: concernsList =>
        `${this.translate('rtdnType')} - ${concernsList
          .map(concern => this.translationService.translate(`ADM_ACM.acdmFor.${concern}`))
          .join(', ')}`,
      currencyId: currencies => `${this.translate('currency')} - ${currencies.map(curr => curr.code).join(', ')}`,
      dateOfIssue: date => `${this.translate('issueDate')} - ${rangeDateFilterTagMapper(date)}`,
      period: periods => this.periodPipe.transform(periods),
      reportingDate: date => `${this.translate('repDate')} - ${rangeDateFilterTagMapper(date)}`,
      disputeDate: date => `${this.translate('disputeDate')} - ${rangeDateFilterTagMapper(date)}`,
      primaryReason: primaryReason => `${this.translate('primaryReason')} - ${primaryReason?.primaryReason}`,
      reason: reason => `${this.translate('reason')} - ${reason.map(reas => reas?.subReason).join(', ')}`,
      markStatus: (markStatus: string) =>
        `${this.translate('markStatus')} - ${this.translationService.translate(`ADM_ACM.markStatus.${markStatus}`)}`,
      amountRange: amounts => `${this.translate('amountRange')}: ${rangeFilterTagMapper(amounts)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({ keys: [item.key], label: item.mapper(item.value) }));
  }

  private translate(key: string): string {
    return this.translationService.translate('ADM_ACM.query.filters.labels.' + key);
  }

  private translateGdsCode(code: string): string {
    return code === anyOptionGdsForwardValue ? this.translationService.translate('dropdown.any') : code;
  }
}
