import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { of } from 'rxjs';

import { AdmAcmFilter } from '../models/adm-acm-filter.model';
import { BasicAgentInfo, BasicAirlineInfo } from '../models/adm-acm-issue-be-aux.model';
import { AdmAcmStatus, ReasonAction } from '../models/adm-acm-issue-shared-aux.model';
import { CurrencyViewModel } from '../models/adm-acm-issue-view-aux.model';
import { AdmAcmService } from './adm-acm.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';

const activatedRouteStub = {
  snapshot: {
    data: {
      item: {},
      admAcmType: MasterDataType.Adm
    }
  }
};

describe('AdmAcmService', () => {
  const httpClientSpy = createSpyObject(HttpClient);
  let service: AdmAcmService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        AdmAcmService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        mockProvider(NotificationsHeaderService)
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(AdmAcmService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should call with proper url and return bsp list', waitForAsync(() => {
    let bspList: PagedData<BspDto>;
    const bspListMock: PagedData<BspDto> = {
      records: [{ id: 1, isoCountryCode: 'ES', name: 'Spain' }],
      pageNumber: 0,
      pageSize: 1,
      total: 1,
      totalPages: 1
    };
    httpClientSpy.get.and.returnValue(of(bspListMock));

    service.getBspsList(true).subscribe(results => (bspList = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsps', { params: { active: true } });
    expect(bspList).toBe(bspListMock);
  }));

  it('should call with proper url and return agent info', waitForAsync(() => {
    let agentInfo: BasicAgentInfo;
    const agentInfoMock: BasicAgentInfo = {
      agent: { id: 1, name: 'agent test', bsp: null, effectiveFrom: '2020/06/29', iataCode: '1234567' },
      agentContact: { contactName: 'contact test', email: 'email.agent@test.com', phoneFaxNumber: '1111111' },
      agentAddress: null,
      agentVatNumber: '1234567'
    };
    httpClientSpy.get.and.returnValue(of(agentInfoMock));

    service.getAgentInfo(1).subscribe(results => (agentInfo = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/agent-info/1');
    expect(agentInfo).toBe(agentInfoMock);
  }));

  it('should call with proper url and return airline info', waitForAsync(() => {
    let airlineInfo: BasicAirlineInfo;
    const airlineInfoMock: BasicAirlineInfo = {
      airline: {
        id: 1,
        bsp: null,
        iataCode: '123',
        active: true,
        address: null,
        contact: null,
        designator: 'DES',
        globalName: 'Airline test',
        localName: 'Airline test local',
        vatNumber: '1234567'
      },
      airlineAddress: null,
      airlineContact: {
        contactName: 'Airline Contact',
        email: 'email.airline@test.com',
        phoneFaxNumber: '123422321'
      },
      airlineVatNumber: '1234567'
    };
    httpClientSpy.get.and.returnValue(of(airlineInfoMock));

    service.getAirlineInfo(1).subscribe(results => (airlineInfo = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/airline-info/1');
    expect(airlineInfo).toEqual(airlineInfoMock);
  }));

  it('should call with proper url and return bsp agent info', waitForAsync(() => {
    let agentInfo: BasicAgentInfo;
    const agentInfoMock: BasicAgentInfo = {
      agent: { id: 1, name: 'agent test', bsp: null, effectiveFrom: '2020/06/29', iataCode: '1234567' },
      agentContact: { contactName: 'contact test', email: 'email.agent@test.com', phoneFaxNumber: '1111111' },
      agentAddress: null,
      agentVatNumber: '1234567'
    };
    httpClientSpy.get.and.returnValue(of(agentInfoMock));

    service.getBspAgentInfo(1).subscribe(results => (agentInfo = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsp-agent-info/1');
    expect(agentInfo).toBe(agentInfoMock);
  }));

  it('should call with proper url and return bsp airline info', waitForAsync(() => {
    let airlineInfo: BasicAirlineInfo;
    const airlineInfoMock: BasicAirlineInfo = {
      airline: {
        id: 1,
        bsp: null,
        iataCode: '123',
        active: true,
        address: null,
        contact: null,
        designator: 'DES',
        globalName: 'Airline test',
        localName: 'Airline test local',
        vatNumber: '1234567'
      },
      airlineAddress: null,
      airlineContact: {
        contactName: 'Airline Contact',
        email: 'email.airline@test.com',
        phoneFaxNumber: '123422321'
      },
      airlineVatNumber: '1234567'
    };
    httpClientSpy.get.and.returnValue(of(airlineInfoMock));

    service.getAirlineInfo(1).subscribe(results => (airlineInfo = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/airline-info/1');
    expect(airlineInfo).toEqual(airlineInfoMock);
  }));

  it('should call with proper url and return currencies', waitForAsync(() => {
    let currencyList: CurrencyViewModel[];
    const currencyListMock: CurrencyViewModel[] = [{ id: 1, code: 'EUR', decimals: 2, isDefault: true }];
    httpClientSpy.get.and.returnValue(of(currencyListMock));

    service.getCurrencies(1, true).subscribe(results => (currencyList = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsps/1/currencies', {
      params: { active: true }
    });
    expect(currencyList).toBe(currencyListMock);
  }));

  it('should call with proper url and return a currency', waitForAsync(() => {
    let currency: CurrencyViewModel;
    const currencyMock: CurrencyViewModel = { id: 1, code: 'EUR', decimals: 2, isDefault: true };
    httpClientSpy.get.and.returnValue(of(currencyMock));

    service.getCurrencyById(1).subscribe(results => (currency = results));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/currencies/1');
    expect(currency).toBe(currencyMock);
  }));

  it('should call with proper urls to retrieve reasons', waitForAsync(() => {
    httpClientSpy.get.and.returnValue(of([]));

    service.getReasons(1, 1, ReasonAction.dispute).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/airlines/1/acdm-reason-configuration', {
      params: { reasonType: ReasonAction.dispute }
    });

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsps/1/acdm-reason-configuration', {
      params: { reasonType: ReasonAction.dispute }
    });
  }));

  it('should call with proper url to retrieve gds from bsp', waitForAsync(() => {
    httpClientSpy.get.and.returnValue(of([]));

    service.getGdssBsp(1, 1).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsps/1/gdss', { params: { admId: '1' } });
  }));

  it('should call with proper url to retrieve time from bsp', waitForAsync(() => {
    httpClientSpy.get.and.returnValue(of());

    service.getBspTime(1).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/bsps/1/time');
  }));

  it('should call with proper url to forward gds', waitForAsync(() => {
    httpClientSpy.post.and.returnValue(of());

    service.forwardGdsAdm(1, { bodyMocked: true }).subscribe();

    expect(httpClientSpy.post).toHaveBeenCalledWith('/acdm-management/adms/1/gdsforwards', { bodyMocked: true });
  }));

  it('should call with proper url to update status', waitForAsync(() => {
    httpClientSpy.put.and.returnValue(of());

    service.updateAcdmStatus(1, AdmAcmStatus.approved).subscribe();

    expect(httpClientSpy.put).toHaveBeenCalledWith('/acdm-management/adms/1/status', {
      status: AdmAcmStatus.approved
    });
  }));

  it('should call with proper url to retrieve activities', waitForAsync(() => {
    httpClientSpy.get.and.returnValue(of());

    service.getActivities(1).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/adms/1/history');
  }));

  it('should call with proper url to retrieve next ticket number', waitForAsync(() => {
    httpClientSpy.get.and.returnValue(of());

    service.getDefaultTicketDocNumber(1).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/adms/ticket-document-number/next-number', {
      params: { airlineId: '1' }
    });
  }));

  it('should call with proper url to retrieve actions list', waitForAsync(() => {
    httpClientSpy.get.and.returnValue(of());

    service.getAcdmActionList(1).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/acdm-management/adms/1/actions');
  }));

  it('should call with proper url to add comment', waitForAsync(() => {
    const reason = { reason: 'test' };
    httpClientSpy.post.and.returnValue(of());

    service.addComment(reason, 1).subscribe();

    expect(httpClientSpy.post).toHaveBeenCalledWith('/acdm-management/adms/1/comments', reason);
  }));

  it('should call with proper url with detailed download', waitForAsync(() => {
    const fileName = 'filename=list-to-Download.txt';
    let adcmQueryDownload: { blob: Blob; fileName: string };

    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({
        'Some-Header': 'some header',
        'Content-Disposition': `filename="${fileName}"`
      })
    });

    const query: DataQuery<AdmAcmFilter> = {
      filterBy: {
        ticketDocumentNumber: '1234'
      },
      sortBy: [{ attribute: 'dateOfIssue', sortType: SortOrder.Desc }],
      paginateBy: { page: 0, size: 20 }
    };

    const acdmQueryDownloadMock: { blob: Blob; fileName: string } = {
      blob: new Blob([null], { type: 'application/zip' }),
      fileName
    };

    httpClientSpy.get.and.returnValue(of(httpResponse));

    service.download(query, DownloadFormat.TXT, {}, true).subscribe(result => (adcmQueryDownload = result));

    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/acdm-management/adms?exportAs=TXT&exportDetail=true&?page=0&size=20&sort=dateOfIssue,DESC&ticketDocumentNumber=1234',
      jasmine.anything()
    );
    expect(adcmQueryDownload).toEqual(acdmQueryDownloadMock);
  }));
});
