/* eslint-disable @typescript-eslint/no-shadow */
import { Action } from '@ngrx/store';

import { ContactViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';

export enum Types {
  UpdateAirlineContactData = '[Acdm] Update Contact Data',
  ResetAirlineContactData = '[Acdm] Reset Contact Data'
}

export class UpdateAirlineContactData implements Action {
  readonly type = Types.UpdateAirlineContactData;
  constructor(public payload: ContactViewModel) {}
}

export class ResetAirlineContactData implements Action {
  readonly type = Types.ResetAirlineContactData;
}

export type Actions = UpdateAirlineContactData | ResetAirlineContactData;
