import { createAction, props } from '@ngrx/store';

import { AdmAcmMassloadFileListOption } from '~app/adm-acm/models/adm-acm-massload-file.model';

export const changeListOption = createAction(
  '[ACDM Massload Files] Change List Option',
  props<{ option: AdmAcmMassloadFileListOption }>()
);
