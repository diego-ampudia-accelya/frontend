import { Injectable } from '@angular/core';
import { createEffect } from '@ngrx/effects';
import { map } from 'rxjs/operators';

import { AcdmContactActions } from '../actions';
import { contactInfoCacheBuster$ } from '~app/shared/helpers/cacheable-helper';

@Injectable()
export class AdmAcmContactEffects {
  public resetContactInfo$ = createEffect(() =>
    contactInfoCacheBuster$.pipe(map(() => new AcdmContactActions.ResetAirlineContactData()))
  );
}
