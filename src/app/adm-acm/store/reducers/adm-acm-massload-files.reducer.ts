import { createReducer, on } from '@ngrx/store';

import { AcdmMassloadFiles } from '../actions';
import { AdmAcmMassloadFileListOption } from '~app/adm-acm/models/adm-acm-massload-file.model';

export const initialState: AdmAcmMassloadFileListOption = AdmAcmMassloadFileListOption.Active;

export const reducer = createReducer(
  initialState,
  on(AcdmMassloadFiles.changeListOption, (state, { option }) => (option ? option : state))
);
