import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromAcdmContact from './adm-acm-contact.reducer';
import * as fromAcdmMassloadFiles from './adm-acm-massload-files.reducer';
import { AdmAcmFilter } from '~app/adm-acm/models/adm-acm-filter.model';
import { ContactViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { AdmAcmIssueBE } from '~app/adm-acm/models/adm-acm-issue.model';
import { AdmAcmMassloadAttachmentFilter } from '~app/adm-acm/models/adm-acm-massload-attachment-filter.model';
import { AdmAcmMassloadAttachment } from '~app/adm-acm/models/adm-acm-massload-attachment.model';
import { AdmAcmMassloadFileFilter } from '~app/adm-acm/models/adm-acm-massload-file-filter.model';
import {
  AdmAcmMassloadFileListOption,
  AdmAcmMassloadFileViewModel
} from '~app/adm-acm/models/adm-acm-massload-file.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';

export const acdmFeatureKey = 'acdm';

//* Massload file tab key
export const massloadFileKey = 'massload-file-list';

//* Massload attachment tab key
export const massloadAttachmentKey = 'massload-attachment-list';

//* Active tab keys
const activeTabId = 'active';
export const activeKeys = {
  // ACDM keys
  [MasterDataType.Adm]: fromListSubtabs.getKey(MasterDataType.Adm, activeTabId),
  [MasterDataType.Acm]: fromListSubtabs.getKey(MasterDataType.Acm, activeTabId),
  [MasterDataType.Adnt]: fromListSubtabs.getKey(MasterDataType.Adnt, activeTabId),
  [MasterDataType.Acnt]: fromListSubtabs.getKey(MasterDataType.Acnt, activeTabId),
  [MasterDataType.Admq]: fromListSubtabs.getKey(MasterDataType.Admq, activeTabId),
  [MasterDataType.Acmq]: fromListSubtabs.getKey(MasterDataType.Acmq, activeTabId),
  // BSP Adjustment keys
  [MasterDataType.Admd]: fromListSubtabs.getKey(MasterDataType.Admd, activeTabId),
  [MasterDataType.Acmd]: fromListSubtabs.getKey(MasterDataType.Acmd, activeTabId),
  [MasterDataType.Spdr]: fromListSubtabs.getKey(MasterDataType.Spdr, activeTabId),
  [MasterDataType.Spcr]: fromListSubtabs.getKey(MasterDataType.Spcr, activeTabId)
};

//* Deleted tab keys
const deletedTabId = 'deleted';
export const deletedKeys = {
  // ACDM keys
  [MasterDataType.Adm]: fromListSubtabs.getKey(MasterDataType.Adm, deletedTabId),
  [MasterDataType.Acm]: fromListSubtabs.getKey(MasterDataType.Acm, deletedTabId),
  [MasterDataType.Adnt]: fromListSubtabs.getKey(MasterDataType.Adnt, deletedTabId),
  [MasterDataType.Acnt]: fromListSubtabs.getKey(MasterDataType.Acnt, deletedTabId),
  [MasterDataType.Admq]: fromListSubtabs.getKey(MasterDataType.Admq, deletedTabId),
  [MasterDataType.Acmq]: fromListSubtabs.getKey(MasterDataType.Acmq, deletedTabId),
  // BSP Adjustment keys
  [MasterDataType.Admd]: fromListSubtabs.getKey(MasterDataType.Admd, deletedTabId),
  [MasterDataType.Acmd]: fromListSubtabs.getKey(MasterDataType.Acmd, deletedTabId),
  [MasterDataType.Spdr]: fromListSubtabs.getKey(MasterDataType.Spdr, deletedTabId),
  [MasterDataType.Spcr]: fromListSubtabs.getKey(MasterDataType.Spcr, deletedTabId)
};

//* Archived tab keys
const archivedTabId = 'archive';
export const archivedKeys = {
  // ACDM keys
  [MasterDataType.Adm]: fromListSubtabs.getKey(MasterDataType.Adm, archivedTabId),
  [MasterDataType.Acm]: fromListSubtabs.getKey(MasterDataType.Acm, archivedTabId),
  [MasterDataType.Adnt]: fromListSubtabs.getKey(MasterDataType.Adnt, archivedTabId),
  [MasterDataType.Acnt]: fromListSubtabs.getKey(MasterDataType.Acnt, archivedTabId),
  [MasterDataType.Admq]: fromListSubtabs.getKey(MasterDataType.Admq, archivedTabId),
  [MasterDataType.Acmq]: fromListSubtabs.getKey(MasterDataType.Acmq, archivedTabId),
  // BSP Adjustment keys
  [MasterDataType.Admd]: fromListSubtabs.getKey(MasterDataType.Admd, archivedTabId),
  [MasterDataType.Acmd]: fromListSubtabs.getKey(MasterDataType.Acmd, archivedTabId),
  [MasterDataType.Spdr]: fromListSubtabs.getKey(MasterDataType.Spdr, archivedTabId),
  [MasterDataType.Spcr]: fromListSubtabs.getKey(MasterDataType.Spcr, archivedTabId)
};

export interface AcdmState {
  // ACDM airline contact data state
  airlineContactData: ContactViewModel;

  // ACDM massload files state
  massloadFileListOption: AdmAcmMassloadFileListOption;
  [massloadFileKey]: fromListSubtabs.ListSubtabsState<AdmAcmMassloadFileViewModel, AdmAcmMassloadFileFilter>;
  [massloadAttachmentKey]: fromListSubtabs.ListSubtabsState<AdmAcmMassloadAttachment, AdmAcmMassloadAttachmentFilter>;
  [key: string]: any;
}

export interface State extends AppState {
  [acdmFeatureKey]: AcdmState;
}

export function reducers(state: AcdmState | undefined, action: Action) {
  return combineReducers({
    // ACDM airline contact data reducer
    airlineContactData: fromAcdmContact.reducer,

    // ACDM massload files reducers
    massloadFileListOption: fromAcdmMassloadFiles.reducer,
    [massloadFileKey]: fromListSubtabs.reducer<AdmAcmMassloadFileViewModel, AdmAcmMassloadFileFilter>(massloadFileKey),
    [massloadAttachmentKey]: fromListSubtabs.reducer<AdmAcmMassloadAttachment, AdmAcmMassloadAttachmentFilter>(
      massloadAttachmentKey
    ),

    // ACDM active reducers
    [activeKeys[MasterDataType.Adm]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Adm]
    ),
    [activeKeys[MasterDataType.Acm]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Acm]
    ),
    [activeKeys[MasterDataType.Adnt]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Adnt]
    ),
    [activeKeys[MasterDataType.Acnt]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Acnt]
    ),
    [activeKeys[MasterDataType.Admq]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Admq]
    ),
    [activeKeys[MasterDataType.Acmq]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Acmq]
    ),
    // BSP Adjustment active reducers
    [activeKeys[MasterDataType.Admd]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Admd]
    ),
    [activeKeys[MasterDataType.Acmd]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Acmd]
    ),
    [activeKeys[MasterDataType.Spdr]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Spdr]
    ),
    [activeKeys[MasterDataType.Spcr]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      activeKeys[MasterDataType.Spcr]
    ),
    // ACDM deleted reducers
    [deletedKeys[MasterDataType.Adm]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Adm]
    ),
    [deletedKeys[MasterDataType.Acm]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Acm]
    ),
    [deletedKeys[MasterDataType.Adnt]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Adnt]
    ),
    [deletedKeys[MasterDataType.Acnt]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Acnt]
    ),
    [deletedKeys[MasterDataType.Admq]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Admq]
    ),
    [deletedKeys[MasterDataType.Acmq]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Acmq]
    ),
    // BSP Adjustment deleted reducers
    [deletedKeys[MasterDataType.Admd]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Admd]
    ),
    [deletedKeys[MasterDataType.Acmd]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Acmd]
    ),
    [deletedKeys[MasterDataType.Spdr]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Spdr]
    ),
    [deletedKeys[MasterDataType.Spcr]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      deletedKeys[MasterDataType.Spcr]
    ),
    // ACDM archived reducers
    [archivedKeys[MasterDataType.Adm]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Adm]
    ),
    [archivedKeys[MasterDataType.Acm]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Acm]
    ),
    [archivedKeys[MasterDataType.Adnt]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Adnt]
    ),
    [archivedKeys[MasterDataType.Acnt]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Acnt]
    ),
    [archivedKeys[MasterDataType.Admq]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Admq]
    ),
    [archivedKeys[MasterDataType.Acmq]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Acmq]
    ),
    // BSP Adjustment archived reducers
    [archivedKeys[MasterDataType.Admd]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Admd]
    ),
    [archivedKeys[MasterDataType.Acmd]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Acmd]
    ),
    [archivedKeys[MasterDataType.Spdr]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Spdr]
    ),
    [archivedKeys[MasterDataType.Spcr]]: fromListSubtabs.reducer<AdmAcmIssueBE, AdmAcmFilter>(
      archivedKeys[MasterDataType.Spcr]
    )
  })(state, action);
}

export const getAcdmState = createFeatureSelector<State, AcdmState>(acdmFeatureKey);

export const getAirlineContactData = createSelector(getAcdmState, state => state.airlineContactData);

export const massloadFileListOption = createSelector(getAcdmState, state => state.massloadFileListOption);
export const massloadFileSelector = createSelector(getAcdmState, state => state[massloadFileKey]);
export const massloadAttachmentSelector = createSelector(getAcdmState, state => state[massloadAttachmentKey]);

export const activeSelectors = {
  // ACDM selectors
  [MasterDataType.Adm]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Adm]]),
  [MasterDataType.Acm]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Acm]]),
  [MasterDataType.Adnt]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Adnt]]),
  [MasterDataType.Acnt]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Acnt]]),
  [MasterDataType.Admq]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Admq]]),
  [MasterDataType.Acmq]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Acmq]]),
  // BSP Adjustment selectors
  [MasterDataType.Admd]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Admd]]),
  [MasterDataType.Acmd]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Acmd]]),
  [MasterDataType.Spdr]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Spdr]]),
  [MasterDataType.Spcr]: createSelector(getAcdmState, state => state[activeKeys[MasterDataType.Spcr]])
};

export const deletedSelectors = {
  // ACDM selectors
  [MasterDataType.Adm]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Adm]]),
  [MasterDataType.Acm]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Acm]]),
  [MasterDataType.Adnt]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Adnt]]),
  [MasterDataType.Acnt]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Acnt]]),
  [MasterDataType.Admq]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Admq]]),
  [MasterDataType.Acmq]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Acmq]]),
  // BSP Adjustment selectors
  [MasterDataType.Admd]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Admd]]),
  [MasterDataType.Acmd]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Acmd]]),
  [MasterDataType.Spdr]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Spdr]]),
  [MasterDataType.Spcr]: createSelector(getAcdmState, state => state[deletedKeys[MasterDataType.Spcr]])
};

export const archivedSelectors = {
  // ACDM selectors
  [MasterDataType.Adm]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Adm]]),
  [MasterDataType.Acm]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Acm]]),
  [MasterDataType.Adnt]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Adnt]]),
  [MasterDataType.Acnt]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Acnt]]),
  [MasterDataType.Admq]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Admq]]),
  [MasterDataType.Acmq]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Acmq]]),
  // BSP Adjustment selectors
  [MasterDataType.Admd]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Admd]]),
  [MasterDataType.Acmd]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Acmd]]),
  [MasterDataType.Spdr]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Spdr]]),
  [MasterDataType.Spcr]: createSelector(getAcdmState, state => state[archivedKeys[MasterDataType.Spcr]])
};
