import { AcdmContactActions } from '../actions';
import { ContactViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';

const initialState: ContactViewModel = null;

export function reducer(state = initialState, action: AcdmContactActions.Actions): ContactViewModel {
  if (action.type === AcdmContactActions.Types.UpdateAirlineContactData) {
    return {
      ...action.payload
    };
  }

  if (action.type === AcdmContactActions.Types.ResetAirlineContactData) {
    return initialState;
  }

  return state;
}
