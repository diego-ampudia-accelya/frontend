import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { EffectsModule } from '@ngrx/effects';
import { NavigationActionTiming, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { L10nLoader, L10nTranslationModule } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { AuthService } from './auth/services/auth.service';
import { PermissionsService } from './auth/services/permissions.service';
import { CoreModule } from './core/core.module';
import { AppInitializerService, initApp } from './core/services/app-initializer.service';
import { httpInterceptorProviders } from './shared/interceptors/http-interceptor-providers';
import { appConfiguration, AppConfigurationService } from './shared/services/app-configuration.service';
import { SharedModule } from './shared/shared.module';
import { TabRouteReuseStrategy } from './shared/utils';
import { environment } from '~env/environment';

import { AppRoutingModule } from '~app/app-routing.module';
import { AppComponent } from '~app/app.component';
import { AuthModule } from '~app/auth/auth.module';
import { metaReducers, reducers } from '~app/reducers';
import { HttpTranslationLoader, initL10n, l10nConfig } from '~app/shared/base/conf/l10n.config';
import { CustomRouterSateSerializer } from '~app/shared/utils/custom-router-state-serializer';
import { TrainingModuleImport } from '~app/training/module.import';
import { UserFeedbackModule } from '~app/user-feedback/user-feedback.module';

export function tokenGetter() {
  return '';
}

// TODO: Check how it should be configured.
const jwtModuleOptions: JwtModuleOptions = {
  config: { tokenGetter }
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    ...TrainingModuleImport,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    SharedModule,
    L10nTranslationModule.forRoot(l10nConfig, {
      translationLoader: HttpTranslationLoader
    }),
    AuthModule,
    CoreModule,
    UserFeedbackModule,
    JwtModule.forRoot(jwtModuleOptions),
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictActionImmutability: !environment.production,
        strictStateImmutability: !environment.production
      }
    }),
    EffectsModule.forRoot([]),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomRouterSateSerializer,
      navigationActionTiming: NavigationActionTiming.PostActivation, // https://github.com/ngrx/platform/issues/816
      stateKey: 'router'
    }),
    // TODO set proper case for StoreDevtoolsModule
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [
    httpInterceptorProviders,
    AuthService,
    PermissionsService,
    MessageService,
    {
      provide: APP_INITIALIZER,
      useFactory: initL10n,
      deps: [L10nLoader],
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initApp,
      deps: [AppInitializerService],
      multi: true
    },
    {
      provide: RouteReuseStrategy,
      useClass: TabRouteReuseStrategy
    },
    {
      provide: AppConfigurationService,
      useValue: appConfiguration
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
