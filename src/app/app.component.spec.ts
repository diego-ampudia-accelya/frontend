import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { NavigationEnd, NavigationError, ResolveEnd, ResolveStart, Router, RouterEvent } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';
import { Subject } from 'rxjs';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const routerEvents = new Subject<RouterEvent>();
  let router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [],
      providers: [
        mockProvider(Router, {
          events: routerEvents.asObservable()
        })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('can create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to ./ if there is no lastSuccessfulNavigation', () => {
    routerEvents.next(new NavigationError(1, null, null));

    expect(router.navigate).toHaveBeenCalledWith(['./']);
  });

  describe('isRouteLoading$', () => {
    it('should be `true` when ResolveStart is detected', fakeAsync(() => {
      let isRouteLoading: boolean;
      component.isRouteLoading$.subscribe(result => (isRouteLoading = result));

      routerEvents.next(new ResolveStart(1, null, null, null));

      expect(isRouteLoading).toBe(true);
    }));

    it('should be `false` when ResolveEnd is detected', fakeAsync(() => {
      let isRouteLoading: boolean;
      component.isRouteLoading$.subscribe(result => (isRouteLoading = result));

      routerEvents.next(new ResolveEnd(1, null, null, null));

      expect(isRouteLoading).toBe(false);
    }));

    it('should be `false` when NavigationError is detected', fakeAsync(() => {
      let isRouteLoading: boolean;
      component.isRouteLoading$.subscribe(result => (isRouteLoading = result));

      routerEvents.next(new NavigationError(1, null, null));

      expect(isRouteLoading).toBe(false);
    }));

    it('should be `undefined` when NavigationEnd is detected', fakeAsync(() => {
      let isRouteLoading = true;
      component.isRouteLoading$.subscribe(result => (isRouteLoading = result));

      routerEvents.next(new NavigationEnd(1, null, null));

      expect(isRouteLoading).toBeFalsy();
    }));
  });
});
