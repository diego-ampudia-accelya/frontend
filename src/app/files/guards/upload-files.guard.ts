import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';

import { FileUploadDialogConfig } from '../model';
import { FileUploadDialogComponent } from '~app/files/file-upload-dialog/file-upload-dialog.component';
import { EV_FILE_PATTERN } from '~app/files/file-upload-dialog/filename-pattern';
import { DialogService, FooterButton } from '~app/shared/components';
import { FileValidators } from '~app/shared/components/upload/file-validators';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class UploadFilesGuard implements CanActivate {
  constructor(
    private fileUploadDialogService: DialogService,
    private translationService: L10nTranslationService,
    private appConfig: AppConfigurationService
  ) {}

  canActivate(): Observable<boolean> {
    return this.fileUploadDialogService.open<FileUploadDialogConfig>(FileUploadDialogComponent, {
      data: {
        title: 'MENU.files.uploadFiles.title',
        footerButtonsType: [{ type: FooterButton.Upload, isDisabled: true }],
        uploadingTitle: 'MENU.files.uploadFiles.uploading',
        uploadedTitle: 'MENU.files.uploadFiles.uploaded',
        uploadPath: `${this.appConfig.baseUploadPath}/file-management/files`,
        validationRules: [
          FileValidators.maxFileSize(),
          FileValidators.single(file => EV_FILE_PATTERN.test(file.name), 'upload.errors.singleAcdmFile')
        ],
        helpText: this.translationService.translate('upload.notification', {
          pattern: '«CCddRecipient_date_freetext»'
        })
      }
    });
  }
}
