import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { FileUploadDialogConfig } from '../model';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { FileUploadDialogComponent } from '~app/files/file-upload-dialog/file-upload-dialog.component';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { FileValidators } from '~app/shared/components/upload/file-validators';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({ providedIn: 'root' })
export class UploadCommunicationsGuard implements CanActivate {
  constructor(
    private fileUploadDialogService: DialogService,
    private appConfig: AppConfigurationService,
    private store: Store<AppState>
  ) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(getUserBsps),
      first(),
      switchMap(userBsps =>
        this.fileUploadDialogService.open<FileUploadDialogConfig>(FileUploadDialogComponent, {
          data: {
            title: 'MENU.files.uploadCommunications.title',
            uploadingTitle: 'MENU.files.uploadCommunications.uploading',
            uploadedTitle: 'MENU.files.uploadCommunications.uploaded',
            footerButtonsType: [{ type: FooterButton.Upload, isDisabled: true }],
            uploadPath: `${this.appConfig.baseUploadPath}/file-management/communications`,
            helpText: 'MENU.files.uploadCommunications.helpText',
            bsp: userBsps[0],
            requireAdditionalInfo: true,
            validationRules: [
              // Maximum 2 MB
              FileValidators.maxFileSize(2 * Math.pow(2, 20))
            ]
          }
        })
      )
    );
  }
}
