import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash/fp';

import { SpecialFilesFilter, SpecialFilesOptionForTranslate } from '../model/special-files-filter.model';
import { SpecialFile, SpecialFilesAccount } from '../model/special-files.model';
import { YesNoOptions } from '../model/yes-no.enum';
import { SpecialFilesFilterFormatter } from './services/special-files-filter-formatter';
import { SpecialFilesService } from './services/special-files.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { UserTypeForFilter } from '~app/shared/models/user-type-for-filter.model';

@Component({
  selector: 'bspl-special-files',
  templateUrl: './special-files.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: SpecialFilesService }
  ]
})
export class SpecialFilesComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];

  public userTypesOptions = this.getEnumOptions(UserTypeForFilter, 'userType');
  public accountOptions = this.getEnumOptions(SpecialFilesAccount, 'account');
  public fileReadOptions = this.getEnumOptions(YesNoOptions, 'fileRead');

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;

  constructor(
    public displayFormatter: SpecialFilesFilterFormatter,
    public dataSource: QueryableDataSource<SpecialFile>,
    private queryStorage: DefaultQueryStorage,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private permissionsService: PermissionsService,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private specialFilesService: SpecialFilesService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.checkPermissionAndLoadData();

    this.initColumns();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.files.specialFiles.downloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.specialFilesService
    });
  }

  public loadData(query: DataQuery<SpecialFilesFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private checkPermissionAndLoadData(): void {
    if (this.permissionsService.hasPermission(Permissions.readSpecialFiles)) {
      this.loadData(this.storedQuery);
    }
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<SpecialFilesFilter>({
      userType: [],
      account: [],
      organisation: [],
      filename: [],
      fileUploadTime: [],
      emailSubmittedAddress: [],
      emailDateTime: [],
      fileRead: [],
      readTimestamp: [],
      readUser: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'userType',
        name: 'MENU.files.specialFiles.columns.userType',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'account',
        name: 'MENU.files.specialFiles.columns.account',
        width: 120,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'organisation',
        name: 'MENU.files.specialFiles.columns.organisation',
        width: 200,
        draggable: false,
        resizeable: true
      },
      {
        prop: 'filename',
        name: 'MENU.files.specialFiles.columns.filename',
        width: 250,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'fileUploadTime',
        name: 'MENU.files.specialFiles.columns.fileUploadTime',
        width: 180,
        resizeable: true,
        draggable: false,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        }
      },
      {
        prop: 'emailSubmittedAddress',
        name: 'MENU.files.specialFiles.columns.emailSubmittedAddress',
        width: 250,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'emailDateTime',
        name: 'MENU.files.specialFiles.columns.emailDateTime',
        width: 180,
        resizeable: true,
        draggable: false,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        }
      },
      {
        prop: 'fileRead',
        name: 'MENU.files.specialFiles.columns.fileRead',
        width: 80,
        resizeable: true,
        draggable: false,
        pipe: {
          transform: value => this.formatFileRead(value)
        }
      },
      {
        prop: 'readTimestamp',
        name: 'MENU.files.specialFiles.columns.readTimestamp',
        width: 180,
        resizeable: true,
        draggable: false,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        }
      },
      {
        prop: 'readUser',
        name: 'MENU.files.specialFiles.columns.readUser',
        width: 200,
        resizeable: true,
        draggable: false
      }
    ];
  }

  private formatFileRead(fileRead: boolean): string {
    const fileReadKeys = fileRead ? 'Yes' : 'No';

    return this.displayFormatter.translate(`fileRead.options.${fileReadKeys}`);
  }

  private getEnumOptions(options: any, filterName: string): DropdownOption<SpecialFilesOptionForTranslate>[] {
    return Object.keys(options).map((key: string) => ({
      value: { translationKey: `${filterName}.options.${key}`, value: options[key] },
      label: this.displayFormatter.translate(`${filterName}.options.${key}`)
    }));
  }
}
