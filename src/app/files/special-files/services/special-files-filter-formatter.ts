import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SpecialFilesFilter } from '~app/files/model/special-files-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { joinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class SpecialFilesFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: SpecialFilesFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<SpecialFilesFilter>> = {
      userType: userTypes =>
        `${this.translate('userType.title')} - ${joinMapper(
          userTypes.map(userType => this.translate(userType.translationKey))
        )}`,
      account: accounts =>
        `${this.translate('account.title')} - ${joinMapper(
          accounts.map(account => this.translate(account.translationKey))
        )}`,
      organisation: organisation => `${this.translate('organisation')} - ${organisation}`,
      filename: filename => `${this.translate('filename')} - ${filename}`,
      fileUploadTime: fileUploadTime =>
        `${this.translate('fileUploadTime')} - ${rangeDateFilterTagMapper(fileUploadTime)}`,
      emailSubmittedAddress: emailSubmittedAddress =>
        `${this.translate('emailSubmittedAddress')} - ${emailSubmittedAddress}`,
      emailDateTime: emailDateTime => `${this.translate('emailDateTime')} - ${rangeDateFilterTagMapper(emailDateTime)}`,
      fileRead: fileRead => `${this.translate('fileRead.title')} - ${this.translate(fileRead.translationKey)}`,
      readTimestamp: readTimestamp => `${this.translate('readTimestamp')} - ${rangeDateFilterTagMapper(readTimestamp)}`,
      readUser: readUser => `${this.translate('readUser')} - ${readUser}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public translate(key: string): string {
    return this.translation.translate(`MENU.files.specialFiles.filters.${key}`);
  }
}
