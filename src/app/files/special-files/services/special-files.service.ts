import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SpecialFilesFilter, SpecialFilesFilterBE } from '~app/files/model/special-files-filter.model';
import { SpecialFile } from '~app/files/model/special-files.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class SpecialFilesService implements Queryable<SpecialFile> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/special-files`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<SpecialFilesFilter>): Observable<PagedData<SpecialFile>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<SpecialFile>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<SpecialFilesFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<SpecialFilesFilter>>): RequestQuery<SpecialFilesFilterBE> {
    const { fileUploadTime, emailDateTime, readTimestamp, userType, account, fileRead, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromFileUploadTime: fileUploadTime?.[0] && toShortIsoDate(fileUploadTime[0]),
        toFileUploadTime: fileUploadTime?.[1] && toShortIsoDate(fileUploadTime[1]),
        fromEmailDateTime: emailDateTime?.[0] && toShortIsoDate(emailDateTime[0]),
        toEmailDateTime: emailDateTime?.[1] && toShortIsoDate(emailDateTime[1]),
        fromReadTimestamp: readTimestamp?.[0] && toShortIsoDate(readTimestamp[0]),
        toReadTimestamp: readTimestamp?.[1] && toShortIsoDate(readTimestamp[1]),
        userType: userType && userType.map(v => v.value),
        account: account && account.map(v => v.value),
        fileRead: fileRead?.value
      }
    });
  }
}
