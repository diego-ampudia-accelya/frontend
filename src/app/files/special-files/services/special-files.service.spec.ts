import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import moment from 'moment-mini';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { SpecialFilesService } from './special-files.service';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

const query = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery;

describe('SpecialFilesService', () => {
  let service: SpecialFilesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SpecialFilesService,
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: 'test'
          }
        }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SpecialFilesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('find', () => {
    let formattedQuery;

    beforeEach(() => {
      formattedQuery = RequestQuery.fromDataQuery({ filterBy: {} });
      (service as any).formatQuery = jasmine.createSpy().and.returnValue(formattedQuery);
      (service as any).http.get = jasmine.createSpy().and.returnValue(of({ records: [] }));
    });

    it('should call formatQuery', fakeAsync(() => {
      service.find(query).pipe(take(1)).subscribe();
      tick();

      expect((service as any).formatQuery).toHaveBeenCalledWith(query);
    }));

    it('should call http get with corresponding query', fakeAsync(() => {
      (service as any).baseUrl = 'base-url';
      const param = `base-url${formattedQuery.getQueryString()}`;

      service.find(query).pipe(take(1)).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(param);
    }));
  });

  describe('download', () => {
    let formattedQuery;

    beforeEach(() => {
      formattedQuery = RequestQuery.fromDataQuery({ filterBy: {} });
      (service as any).formatQuery = jasmine.createSpy().and.returnValue(formattedQuery);
      (service as any).http.get = jasmine.createSpy().and.returnValue(of({ records: [] }));
    });

    it('should call formatQuery', fakeAsync(() => {
      service.download(query, DownloadFormat.TXT).pipe(take(1)).subscribe();
      tick();

      expect((service as any).formatQuery).toHaveBeenCalledWith(query);
    }));

    it('should call http get with download options', fakeAsync(() => {
      (service as any).baseUrl = 'base-url';
      formattedQuery.filterBy = { exportAs: DownloadFormat.TXT };
      const param = `base-url/download${formattedQuery.getQueryString({ omitPaging: true })}`;

      service.download(query, DownloadFormat.TXT).pipe(take(1)).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(param, downloadRequestOptions);
    }));
  });

  it('formatQuery - should return mapped query', () => {
    const param = {
      filterBy: {
        fileUploadTime: [moment('20220520'), null],
        userType: [{ value: 'user1' }, { value: 'user2' }],
        account: [{ value: 'acc1' }, { value: 'acc2' }],
        fileRead: { value: 'fileRead-value' }
      }
    };
    const expectedRes = RequestQuery.fromDataQuery({
      filterBy: {
        fromFileUploadTime: '2022-05-20',
        toFileUploadTime: null,
        fromEmailDateTime: undefined,
        toEmailDateTime: undefined,
        fromReadTimestamp: undefined,
        toReadTimestamp: undefined,
        userType: ['user1', 'user2'],
        account: ['acc1', 'acc2'],
        fileRead: 'fileRead-value'
      }
    });

    const res = (service as any).formatQuery(param);

    expect(res).toEqual(expectedRes);
  });
});
