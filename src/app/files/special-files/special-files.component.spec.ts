import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslatePipe, L10nTranslationService } from 'angular-l10n';
import { MockComponents, MockProviders } from 'ng-mocks';
import { of } from 'rxjs';

import { SpecialFilesFilterFormatter } from './services/special-files-filter-formatter';
import { SpecialFilesService } from './services/special-files.service';
import { SpecialFilesComponent } from './special-files.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  DialogService,
  FooterButton,
  GridTableComponent,
  InputComponent,
  SelectComponent
} from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import {
  DataQuery,
  DefaultQueryStorage,
  ListViewComponent,
  QueryableDataSource
} from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';

const query = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery;

const query$ = of(query);

describe('SpecialFilesComponent', () => {
  let component: SpecialFilesComponent;
  let fixture: ComponentFixture<SpecialFilesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SpecialFilesComponent,
        MockComponents(ListViewComponent, DatepickerComponent, SelectComponent, GridTableComponent, InputComponent),
        L10nTranslatePipe
      ],
      providers: [
        MockProviders(
          SpecialFilesFilterFormatter,
          QueryableDataSource,
          DefaultQueryStorage,
          PermissionsService,
          L10nTranslationService,
          DialogService,
          SpecialFilesService
        ),
        FormBuilder,
        DatePipe
      ],
      imports: [ReactiveFormsModule]
    })
      .overrideComponent(SpecialFilesComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: { hasData$: of(true), appliedQuery$: query$, get: () => {} } },
            { provide: DefaultQueryStorage, useValue: createSpyObject(DefaultQueryStorage) }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onDownload', () => {
    beforeEach(() => {
      (component as any).dialogService.open = jasmine.createSpy();
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      (component as any).queryStorage.get = jasmine.createSpy().and.returnValue({ title: 'test-title' });
    });

    it('should call translate', () => {
      component.onDownload();

      expect((component as any).translationService.translate).toHaveBeenCalledWith(
        'MENU.files.specialFiles.downloadTitle'
      );
    });

    it('should call open', () => {
      component.onDownload();

      expect((component as any).dialogService.open).toHaveBeenCalled();
    });

    it('should call open with corresponding data', () => {
      component.onDownload();
      const args = (component as any).dialogService.open.calls.mostRecent().args;

      expect(args[1].data).toEqual({
        title: 'test',
        footerButtonsType: FooterButton.Download,
        downloadQuery: { title: 'test-title' }
      });
    });
  });

  describe('loadData', () => {
    beforeEach(() => {
      component.dataSource.get = jasmine.createSpy();
      (component as any).queryStorage.save = jasmine.createSpy();
    });

    it('should call set and save with query from parameters', () => {
      component.loadData(query);

      expect(component.dataSource.get).toHaveBeenCalledWith(query);
      expect((component as any).queryStorage.save).toHaveBeenCalledWith(query);
    });

    it('should call set and save with default query', () => {
      component.loadData(null);

      expect(component.dataSource.get).toHaveBeenCalledWith(defaultQuery);
      expect((component as any).queryStorage.save).toHaveBeenCalledWith(defaultQuery);
    });
  });

  describe('checkPermissionAndLoadData', () => {
    beforeEach(() => {
      component.loadData = jasmine.createSpy();
      (component as any).permissionsService.hasPermission = jasmine.createSpy().and.returnValue(false);
    });

    it('should call hasPermission', () => {
      (component as any).checkPermissionAndLoadData();

      expect((component as any).permissionsService.hasPermission).toHaveBeenCalledWith(Permissions.readSpecialFiles);
    });

    it('should call loadData when user has permission', () => {
      (component as any).permissionsService.hasPermission = jasmine.createSpy().and.returnValue(true);
      (component as any).storedQuery = query;

      (component as any).checkPermissionAndLoadData();

      expect(component.loadData).toHaveBeenCalledWith(query);
    });

    it('should call loadData when user has no permissions', () => {
      (component as any).checkPermissionAndLoadData();

      expect(component.loadData).not.toHaveBeenCalled();
    });
  });

  it('buildForm - should create form', () => {
    const formExpected = {
      userType: null,
      account: null,
      organisation: null,
      filename: null,
      fileUploadTime: null,
      emailSubmittedAddress: null,
      emailDateTime: null,
      fileRead: null,
      readTimestamp: null,
      readUser: null
    };

    const form = (component as any).buildForm();

    expect(form.value).toEqual(formExpected);
  });

  describe('formatFileRead', () => {
    beforeEach(() => {
      (component as any).displayFormatter.translate = jasmine.createSpy().and.returnValue('test-1');
    });

    it('should call translate with yes', () => {
      (component as any).formatFileRead(true);

      expect((component as any).displayFormatter.translate).toHaveBeenCalledWith('fileRead.options.Yes');
    });

    it('should call translate with no', () => {
      (component as any).formatFileRead(false);

      expect((component as any).displayFormatter.translate).toHaveBeenCalledWith('fileRead.options.No');
    });

    it('should return value from translate', () => {
      const res = (component as any).formatFileRead(false);

      expect(res).toBe('test-1');
    });
  });

  describe('getEnumOptions', () => {
    let options;

    beforeEach(() => {
      (component as any).displayFormatter.translate = jasmine.createSpy().and.returnValue('testq');
      options = {
        user1: 'user_1',
        user2: 'user_2'
      };
    });

    it('should return mapped options', () => {
      const expectedOptions = [
        { value: { translationKey: 'userName.options.user1', value: 'user_1' }, label: 'testq' },
        { value: { translationKey: 'userName.options.user2', value: 'user_2' }, label: 'testq' }
      ];

      const res = (component as any).getEnumOptions(options, 'userName');

      expect(res).toEqual(expectedOptions);
    });

    it('should call translate 2 times', () => {
      (component as any).getEnumOptions(options, 'userName');

      expect((component as any).displayFormatter.translate).toHaveBeenCalledTimes(2);
    });
  });
});
