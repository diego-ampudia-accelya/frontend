import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { castArray } from 'lodash';

import { FileStatus } from '../model';
import { FileInfoFilter } from '../model/files.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { openRangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class ReportsDisplayFilterFormatter {
  private fileStatusMap = new Map([
    [FileStatus.NotDownloaded, `filesStatus.${FileStatus.NotDownloaded}`],
    [FileStatus.Downloaded, `filesStatus.${FileStatus.Downloaded}`]
  ]);

  public ignoredFilters: Array<keyof FileInfoFilter> = [];

  constructor(private translationService: L10nTranslationService) {}

  public format(filter: FileInfoFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMapper<FileInfoFilter>> = {
      type: values =>
        `${this.translate('fileType')} - ${castArray(values)
          .map(value => value.description)
          .join(', ')}`,
      uploadDate: dates => `${this.translate('uploadDate')} - ${openRangeDateFilterTagMapper(dates)}`,
      status: values => `${this.translate('status')} - ${this.formatStatus(values)}`,
      name: value => `${this.translate('fileName')} - ${value}`,
      bspId: bsp => `${this.translate('columns.bsp')} - ${bsp.name} (${bsp.isoCountryCode})`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value !== null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({ keys: [item.key], label: item.mapper(item.value) }));
  }

  private formatStatus(status: FileStatus[]): string {
    return status.map(value => this.translate(this.fileStatusMap.get(value))).join(', ');
  }

  private translate(key: string): string {
    return this.translationService.translate('MENU.files.reports.' + key);
  }
}
