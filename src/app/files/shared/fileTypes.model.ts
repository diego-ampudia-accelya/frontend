export enum FileTypeDescriptor {
  HOTErrorsFile = 'en',
  BSPlinkRetFile = 'eb',
  WEBlinkRETFile = 'ea'
}

export enum FileTypeDescription {
  HOTErrorsFile = 'Hot Errors File',
  BSPlinkRetFile = 'BSPlink RET file',
  WEBlinkRETFile = 'WebLink output RET file'
}
