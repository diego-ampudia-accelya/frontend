import { TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { FileStatus } from '../model';
import { FileInfoFilter } from '../model/files.model';

import { ReportsDisplayFilterFormatter } from './reports-display-filter-formatter';
import { AppliedFilter } from '~app/shared/components/list-view';

describe('ReportsDisplayFilterFormatter', () => {
  let formatter: ReportsDisplayFilterFormatter;
  let translationServiceSpy: SpyObject<L10nTranslationService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReportsDisplayFilterFormatter, mockProvider(L10nTranslationService)]
    });

    translationServiceSpy = TestBed.inject<any>(L10nTranslationService);
    translationServiceSpy.translate.and.callFake(key => key);

    formatter = new ReportsDisplayFilterFormatter(translationServiceSpy);
  });

  it('should create', () => {
    expect(formatter).toBeTruthy();
  });

  it('should format `name` properly', () => {
    const filter: FileInfoFilter = {
      name: 'fileName',
      status: null,
      uploadDate: null,
      type: null,
      bspId: null
    };
    const expected: AppliedFilter[] = [
      {
        keys: ['name'],
        label: 'MENU.files.reports.fileName - fileName'
      }
    ];

    const actual = formatter.format(filter);

    expect(actual).toEqual(expected);
  });

  it('should format `type` properly when there are multiple values', () => {
    const filter: FileInfoFilter = {
      name: null,
      status: null,
      uploadDate: null,
      bspId: null,
      type: [
        {
          descriptor: 'eu',
          description: 'Master Tables'
        },
        {
          descriptor: 'ed',
          description: 'Mass Loading files'
        }
      ]
    };
    const expected: AppliedFilter[] = [
      {
        label: 'MENU.files.reports.fileType - Master Tables, Mass Loading files',
        keys: ['type']
      }
    ];

    const actual = formatter.format(filter);

    expect(actual['label']).toEqual(expected['label']);
  });

  it('should format `type` properly when there is a single value', () => {
    const filter: FileInfoFilter = {
      name: null,
      status: null,
      uploadDate: null,
      type: [
        {
          descriptor: 'eu',
          description: 'Master Tables'
        }
      ],
      bspId: null
    };
    const expected: AppliedFilter[] = [
      {
        label: 'MENU.files.reports.fileType - Master Tables',
        keys: ['type']
      }
    ];

    const actual = formatter.format(filter);

    expect(actual['label']).toEqual(expected['label']);
  });

  it('should format `uploadDate` properly', () => {
    const filter: FileInfoFilter = {
      name: null,
      status: null,
      type: null,
      uploadDate: [new Date(2020, 6, 21), null],
      bspId: null
    };
    const expected: AppliedFilter[] = [
      {
        label: 'MENU.files.reports.uploadDate - 21/07/2020 - *',
        keys: ['uploadDate']
      }
    ];

    const actual = formatter.format(filter);

    expect(actual).toEqual(expected);
  });

  it('should format `status` properly', () => {
    const filter: FileInfoFilter = {
      name: null,
      type: null,
      uploadDate: null,
      status: [FileStatus.Downloaded],
      bspId: null
    };
    const expected: AppliedFilter[] = [
      {
        keys: ['status'],
        label: 'MENU.files.reports.status - MENU.files.reports.filesStatus.D'
      }
    ];

    const actual = formatter.format(filter);

    expect(actual).toEqual(expected);
  });
});
