import moment from 'moment-mini';

import { FileType } from '../model';
import { GLOBALS } from '~app/shared/constants/globals';

export const hasFileType = (itemFileType: FileType, filterValues: Array<FileType>) =>
  filterValues.some(filterValue => filterValue.descriptor === itemFileType.descriptor);

export const hasFileTypeDescription = (itemFileType: FileType, filterValues: Array<FileType>) =>
  filterValues.some(filterValue => filterValue.description === itemFileType.description);

export const hasNameContaining = (itemFileName: string, filterValue: string) =>
  itemFileName.toLowerCase().includes(filterValue.toLowerCase());

export const hasUploadDateInRange = (itemUploadDate: string, [uploadDateFrom, uploadDateTo]: Date[]) => {
  const from = uploadDateFrom || GLOBALS.MIN_DATE;
  const to = uploadDateTo || GLOBALS.MAX_DATE;

  return moment(itemUploadDate).isBetween(from, to, 'date', '[]');
};
