import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { sortBy, uniqBy } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { FileType } from '../model';
import { DropdownOption } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({ providedIn: 'root' })
export class FileTypesService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management`;

  constructor(private appConfiguration: AppConfigurationService, private http: HttpClient) {}

  private formatFileTypes(fileTypes: FileType[]) {
    const types = uniqBy(
      fileTypes.map(type => ({
        value: type,
        label: type.description
      })),
      'label'
    );

    return sortBy(types, 'label');
  }

  @Cacheable()
  public getFileTypeOption(bspId: string): Observable<DropdownOption[]> {
    const url = `${this.baseUrl}/file-types`;

    return this.http
      .get<FileType[]>(url, { params: { bspId } })
      .pipe(map(fileTypes => this.formatFileTypes(fileTypes)));
  }

  @Cacheable()
  public getAgentReportFileTypeOption(bspId: string): Observable<DropdownOption[]> {
    const url = `${this.baseUrl}/agent-report-types`;

    return this.http.get<FileType[]>(url, { params: { bspId } }).pipe(
      map(fileTypes => {
        const types = fileTypes.map(({ descriptor: id, description }) => ({
          // Id is required by bspl-select to determine selected items
          value: { id, descriptor: id, description: description || id },
          label: description
        }));

        return sortBy(types, 'label');
      })
    );
  }

  @Cacheable()
  public getMassloadFileTypeOption(bspId: string): Observable<DropdownOption<FileType>[]> {
    const url = `${this.baseUrl}/massload-file-types`;

    return this.http
      .get<FileType[]>(url, { params: { bspId } })
      .pipe(map(fileTypes => this.formatFileTypes(fileTypes)));
  }

  @Cacheable()
  public getXmlFileTypeOption(bspId: string): Observable<DropdownOption[]> {
    const url = `${this.baseUrl}/xml-file-types`;

    return this.http.get<FileType[]>(url, { params: { bspId } }).pipe(
      map(fileTypes => {
        const types = fileTypes.map(type => ({
          value: type,
          label: type.description
        }));

        return sortBy(types, 'label');
      })
    );
  }
}
