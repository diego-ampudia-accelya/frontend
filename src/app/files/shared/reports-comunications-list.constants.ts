export const FILE_LIST = {
  SEARCH_FORM_DEFAULT_VALUE: {
    bspId: null,
    name: null,
    type: null,
    uploadDate: '',
    status: null
  }
};
