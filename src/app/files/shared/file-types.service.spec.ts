import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { FileType } from '../model';
import { FileTypesService } from './file-types.service';
import { DropdownOption } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

describe('FileTypesService', () => {
  let service: FileTypesService;
  let httpMock: SpyObject<HttpClient>;

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        FileTypesService,
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, {
          baseApiPath: 'api'
        })
      ]
    })
  );

  beforeEach(() => {
    service = TestBed.inject(FileTypesService);
    httpMock = TestBed.inject<any>(HttpClient);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('getFileTypeOption - should return file type options sorted', fakeAsync(() => {
    const fileTypes: FileType[] = [
      { descriptor: '11', description: 'AGENT COMMUNICATIONS' },
      { descriptor: '12', description: 'IATA Important Information' },
      { descriptor: '13', description: 'ERN Input files' },
      { descriptor: 'ac', description: 'ac' }
    ];

    const expectedFileTypes: DropdownOption<FileType>[] = [
      {
        label: 'AGENT COMMUNICATIONS',
        value: { descriptor: '11', description: 'AGENT COMMUNICATIONS' }
      },
      {
        label: 'ERN Input files',
        value: { descriptor: '13', description: 'ERN Input files' }
      },
      {
        label: 'IATA Important Information',
        value: { descriptor: '12', description: 'IATA Important Information' }
      },
      {
        label: 'ac',
        value: { descriptor: 'ac', description: 'ac' }
      }
    ];

    httpMock.get.and.returnValue(of(fileTypes));

    service.getFileTypeOption('6983').subscribe(fileTypeOptions => {
      expect(fileTypeOptions).toEqual(expectedFileTypes);
    });
  }));

  it('getAgentReportFileTypeOption - should return file type options sorted', fakeAsync(() => {
    const fileTypes: FileType[] = [
      { descriptor: '11', description: 'AGENT COMMUNICATIONS' },
      { descriptor: '12', description: 'IATA Important Information' },
      { descriptor: '13', description: 'ERN Input files' },
      { descriptor: 'ac', description: 'ac' }
    ];

    const expectedFileTypes: DropdownOption<FileType>[] = [
      {
        label: 'AGENT COMMUNICATIONS',
        value: { id: '11', descriptor: '11', description: 'AGENT COMMUNICATIONS' }
      },
      {
        label: 'ERN Input files',
        value: { id: '13', descriptor: '13', description: 'ERN Input files' }
      },
      {
        label: 'IATA Important Information',
        value: { id: '12', descriptor: '12', description: 'IATA Important Information' }
      },
      {
        label: 'ac',
        value: { id: 'ac', descriptor: 'ac', description: 'ac' }
      }
    ];

    httpMock.get.and.returnValue(of(fileTypes));

    service.getAgentReportFileTypeOption('6983').subscribe(fileTypeOptions => {
      expect(fileTypeOptions).toEqual(expectedFileTypes);
    });
  }));

  it('getMassloadFileTypeOption - should return file type options sorted', fakeAsync(() => {
    const fileTypes: FileType[] = [
      { descriptor: '11', description: 'AGENT COMMUNICATIONS' },
      { descriptor: '12', description: 'IATA Important Information' },
      { descriptor: '13', description: 'ERN Input files' },
      { descriptor: 'ac', description: 'ac' }
    ];

    const expectedFileTypes: DropdownOption<FileType>[] = [
      {
        label: 'AGENT COMMUNICATIONS',
        value: { descriptor: '11', description: 'AGENT COMMUNICATIONS' }
      },
      {
        label: 'ERN Input files',
        value: { descriptor: '13', description: 'ERN Input files' }
      },
      {
        label: 'IATA Important Information',
        value: { descriptor: '12', description: 'IATA Important Information' }
      },
      {
        label: 'ac',
        value: { descriptor: 'ac', description: 'ac' }
      }
    ];

    httpMock.get.and.returnValue(of(fileTypes));

    service.getMassloadFileTypeOption('6983').subscribe(fileTypeOptions => {
      expect(fileTypeOptions).toEqual(expectedFileTypes);
    });
  }));

  it('getXmlFileTypeOption - should return file type options sorted', fakeAsync(() => {
    const fileTypes: FileType[] = [
      { descriptor: '11', description: 'AGENT COMMUNICATIONS' },
      { descriptor: '12', description: 'IATA Important Information' },
      { descriptor: '13', description: 'ERN Input files' },
      { descriptor: 'ac', description: 'ac' }
    ];

    const expectedFileTypes: DropdownOption<FileType>[] = [
      {
        label: 'AGENT COMMUNICATIONS',
        value: { descriptor: '11', description: 'AGENT COMMUNICATIONS' }
      },
      {
        label: 'ERN Input files',
        value: { descriptor: '13', description: 'ERN Input files' }
      },
      {
        label: 'IATA Important Information',
        value: { descriptor: '12', description: 'IATA Important Information' }
      },
      {
        label: 'ac',
        value: { descriptor: 'ac', description: 'ac' }
      }
    ];

    httpMock.get.and.returnValue(of(fileTypes));

    service.getXmlFileTypeOption('6983').subscribe(fileTypeOptions => {
      expect(fileTypeOptions).toEqual(expectedFileTypes);
    });
  }));
});
