import { DeletedFilesStatus } from '.';

export interface DeletedFile {
  id: string;
  name: string;
  actionDate: Date;
  user: string;
  email: string;
  status: DeletedFilesStatus;
}

export interface DeletedFileByName {
  name: 'string';
}
