import { FileDescriptorSummary } from '~app/shared/models/dictionary/file-descriptor-summary';

export interface NotDownloadedFilesFilter {
  sendDate: Date[];
  receiverTypes: any[];
  fileTypes: FileDescriptorSummary[];
}
export interface NotDownloadedFilesFilterBE {
  receiverTypeId?: number[];
  receiverType?: string[];
  descriptorId?: number;
  descriptor?: string[];
  fromSendDate: string;
  toSendDate: string;
}
