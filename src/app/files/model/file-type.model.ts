export interface FileType {
  id?: string;
  descriptor?: string;
  description: string;
}
