import { FileValidator } from '~app/shared/components/upload/file-validators';

export interface MassDeleteDialogConfig {
  uploadingTitle?: string;
  uploadedTitle?: string;
  uploadPath?: string;
  validationRules?: FileValidator[];
  secondaryTittle?: string;
}
