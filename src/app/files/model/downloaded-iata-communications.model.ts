export interface DownloadedIataCommunication {
  name: string;
  downloadDate: string;
  receiverTypeId: number;
  receiverType: string;
  receiverUserId: string;
  receiverName: string;
}
