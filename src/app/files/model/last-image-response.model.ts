export interface LastImageResponse {
  adhocFile: boolean;
  requestDate: string;
}
