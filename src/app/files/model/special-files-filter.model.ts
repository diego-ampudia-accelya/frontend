export interface SpecialFilesOptionForTranslate {
  translationKey: string;
  value: string;
}

export interface SpecialFilesFilter {
  userType: SpecialFilesOptionForTranslate[];
  account: SpecialFilesOptionForTranslate[];
  organisation: string;
  filename: string;
  fileUploadTime: Date[];
  emailSubmittedAddress: string;
  emailDateTime: Date[];
  fileRead: SpecialFilesOptionForTranslate;
  readTimestamp: Date[];
  readUser: string;
}

export interface SpecialFilesFilterBE {
  userType: string[];
  account: string[];
  organisation: string;
  filename: string;
  fromFileUploadTime: string;
  toFileUploadTime: string;
  emailSubmittedAddress: string;
  fromEmailDateTime: string;
  toEmailDateTime: string;
  fileRead: string;
  fromReadTimestamp: string;
  toReadTimestamp: string;
  readUser: string;
}
