export enum SectionType {
  Inbox = 'inbox',
  Archive = 'archive'
}
