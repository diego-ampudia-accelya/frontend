import { FileType } from './file-type.model';
import { BspDto } from '~app/shared/models/bsp.model';

export interface AgentReportsFilter {
  bsp: BspDto;
  type: FileType;
  uploadDate?: Date[];
  name?: string;
}
