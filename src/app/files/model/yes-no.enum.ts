export enum YesNoOptions {
  Yes = 'true',
  No = 'false'
}
