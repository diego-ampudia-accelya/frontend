export enum FileStatus {
  Downloaded = 'D',
  NotDownloaded = 'N',
  Archived = 'A'
}
