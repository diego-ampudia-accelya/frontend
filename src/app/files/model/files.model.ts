import { FileStatus } from './file-status.enum';
import { FileType } from './file-type.model';
import { BspDto } from '~app/shared/models/bsp.model';

export interface FileInfo {
  id?: string;
  name: string;
  type: FileType;
  uploadDate: string;
  size?: number;
  status?: FileStatus;
  isoCountryCode?: string;
  userType?: string;
}

export interface FileInfoViewListModel extends FileInfo {
  isSelected?: boolean;
}
export interface FileInfoFilter {
  bspId: BspDto;
  name: string;
  type: FileType[];
  uploadDate: Date[];
  status: FileStatus[];
}
