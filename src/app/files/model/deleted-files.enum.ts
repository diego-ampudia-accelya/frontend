export enum DeletedFilesStatus {
  InProcess = 'IN_PROCESS',
  Deleted = 'DELETED',
  NotDeleted = 'NOT_DELETED'
}
