import { FileValidator } from '~app/shared/components/upload/file-validators';
import { Bsp } from '~app/shared/models/bsp.model';

export interface FileUploadDialogConfig {
  uploadingTitle?: string;
  uploadedTitle?: string;
  uploadPath?: string;
  validationRules?: FileValidator[];
  helpText?: string;
  bsp?: Bsp;
  requireAdditionalInfo?: boolean;
}
