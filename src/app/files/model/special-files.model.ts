export enum SpecialFilesAccount {
  userIndividualFiles = 'USER_INDIVIDUAL_FILES',
  airlineCommunications = 'AIRLINE_COMMUNICATIONS',
  agentCommunications = 'AGENT_COMMUNICATIONS',
  filesForAllUsers = 'FILES_FOR_ALL_USERS'
}

export interface SpecialFile {
  userTypes: string;
  account: string;
  organisation: string;
  filename: string;
  fileUploadTime: string; // Date
  emailSubmittedAddress: string;
  emailDateTime: string; // Date
  fileRead: boolean;
  readTimestamp: string; // Date
  readUser: string;
}
