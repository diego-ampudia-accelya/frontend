import { SenderTypeOption } from '../uploaded-files/model/sender-type';
import { FileDescriptorSummary } from '~app/shared/models/dictionary/file-descriptor-summary';

export interface UploadedFilesFilter {
  sendingDate: Date[];
  type: FileDescriptorSummary[];
  senderType: SenderTypeOption[];
}
export interface UploadedFilesFilterBE {
  senderTypeId?: number[];
  senderType?: string[];
  descriptorId?: number[];
  descriptor?: string[];
  fromSendingDate: string;
  toSendingDate: string;
}
