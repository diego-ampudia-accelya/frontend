export interface DownloadedIataCommunicationOptionForTranslate {
  translationKey: string;
  value: string;
}

export enum ReceiverTypes {
  airline = 'Airline',
  agent = 'Agent',
  all = 'ALL'
}

export interface DownloadedIataCommunicationFilter {
  downloadDate: Date[];
  receiverTypes: DownloadedIataCommunicationOptionForTranslate[];
}

export interface DownloadedIataCommunicationFilterBE {
  fromDownloadDate: string;
  toDownloadDate: string;
  receiverType: string[];
}
