import { Subject } from 'rxjs';

import { DeletedFileByName } from './deleted-files.model';
import { FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';

export interface DeletedFilesDialogData {
  title: string;
  isDeleting: boolean;
  footerButtonsType: (string | ModalAction)[];
  actionEmitter: Subject<any>;
  buttons?: ModalAction[];
  mainButtonType: FooterButton;
  files: DeletedFileByName[];
}
