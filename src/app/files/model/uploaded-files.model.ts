import { FileType } from './file-type.model';

export interface UploadedFile {
  id: string;
  name: string;
  type: FileType;
  size: number;
  sendingNumber: number;
  senderType: string;
  senderUserId: string;
  sendingDate: Date;
  fileDate: Date;
}
