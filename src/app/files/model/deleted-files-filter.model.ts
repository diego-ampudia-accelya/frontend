import { DeletedFilesStatus } from '.';

export interface DeletedFilesFilter {
  name?: string;
  actionDate?: Date[];
  user?: string;
  email?: string;
  status?: DeletedFilesStatus[];
}
