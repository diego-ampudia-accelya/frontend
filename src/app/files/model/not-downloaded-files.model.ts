import { FileType } from './file-type.model';

export interface NotDownloadedFile {
  id: string;
  name: string;
  type: FileType;
  size: number;
}
