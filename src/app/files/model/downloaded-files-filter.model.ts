import { ReceiverTypeOption } from '../downloaded-files/model/receiver-type';
import { FileDescriptorSummary } from '~app/shared/models/dictionary/file-descriptor-summary';

export interface DownloadedFilesFilter {
  downloadDate: Date[];
  fileTypes: FileDescriptorSummary[];
  receiverTypes: ReceiverTypeOption[];
}
export interface DownloadedFilesFilterBE {
  receiverTypeId: number;
  receiverType: string[];
  descriptorId: number;
  descriptor: string[];
  fromDownloadDate: string;
  toDownloadDate: string;
}
