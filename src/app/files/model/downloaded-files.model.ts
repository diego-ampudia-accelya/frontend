export interface DownloadedFile {
  name: string;
  type: string;
  descriptorId: number;
  descriptor: string;
  downloadDate: Date[];
  receiverTypeId: number;
  receiverType: string;
  receiverUserId: string;
}
