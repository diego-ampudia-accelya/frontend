export interface FopAgentStatusRequest {
  gdsCode: string;
  agentCode: string;
  isoCountryCode: string;
}
