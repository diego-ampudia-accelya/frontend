import { FileInfo, FileStatus } from '../model';

export const files: FileInfo[] = [
  {
    id: '13',
    name: 'ESaz3441_20200107_20200107_Airline_Daily.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '14',
    name: 'ESaz3441_20200108_20200108_Airline_Daily.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-25',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '15',
    name: 'ESaz3441_20200113_200101_Airline_Extended_Transaction_File.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-28',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '16',
    name: 'ESaz3441_20200113_20200101_Airline_Billing.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-29',
    size: 3330000,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '17',
    name: 'ESaz3441_20200113_20200101_Airline_TaxOnCommission.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '18',
    name: 'ESaz3441_20200122_200102_Airline_Extended_Transaction_File.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '19',
    name: 'ESaz3441_20200122_20200102_Airline_Billing.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-28',
    size: 3100000,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '20',
    name: 'ESaz3441_20200122_20200102_Airline_TaxOnCommission.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '21',
    name: 'ESd13441_20200122_AirlineDefaultSummary_AMM_BSP.txt',
    type: {
      descriptor: 'd1',
      description: 'Airline Default Summary'
    },
    uploadDate: '2020-02-29',
    size: 1100000,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '22',
    name: 'ESd13441_20200123_AirlineDefaultSummary_MAD_BSP.txt',
    type: {
      descriptor: 'd1',
      description: 'Airline Default Summary'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '23',
    name: 'ESd13441_20200123_AirlineDefaultSummary_MIA_BSP.txt',
    type: {
      descriptor: 'd1',
      description: 'Airline Default Summary'
    },
    uploadDate: '2020-02-27',
    size: 3555444,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '24',
    name: 'ESei3441_20200114_000',
    type: {
      descriptor: 'ei',
      description: 'Mass Upload v2.0 evaluation file'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '25',
    name: 'ESei3441_20200114_001',
    type: {
      descriptor: 'ei',
      description: 'Mass Upload v2.0 evaluation file'
    },
    uploadDate: '2020-02-26',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '26',
    name: 'ESi33441_20191231_LOCAL_TIP_MONTHLY.txt',
    type: {
      descriptor: 'i3',
      description: 'TIP reports'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '27',
    name: 'ESmk3441_20200107_ES.HOT.D.200107.001.344.txt',
    type: {
      descriptor: 'mk',
      description: 'Unmasked CC# file(s)'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '28',
    name: 'ESmk3441_20200108_ES.HOT.D.200108.001.344.txt',
    type: {
      descriptor: 'mk',
      description: 'Unmasked CC# file(s)'
    },
    uploadDate: '2020-02-29',
    size: 3856475,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '29',
    name: 'ESrs3441_20200107_SettlementDetails_OperationalSettlement_20200107104939.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 6656485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '30',
    name: 'ESrs3441_20200108_SettlementDetails_OperationalSettlement_20200108105931.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 8859499,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '31',
    name: 'ESrs3441_20200109_SettlementDetails_OperationalSettlement_20200109043849.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 9856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '32',
    name: 'ESaz3441_20200112_20200112_Airline_Daily.txt',
    type: {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    },
    uploadDate: '2020-02-29',
    size: 896325,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '33',
    name: 'ESei3441_20200125_002',
    type: {
      descriptor: 'ei',
      description: 'Mass Upload v2.0 evaluation file'
    },
    uploadDate: '2020-02-29',
    size: 777485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '34',
    name: 'ESrs3441_20200114_SettlementDetails_OperationalSettlement_20200114010900.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 5856485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '36',
    name: 'ESei3441_20200125_002',
    type: {
      descriptor: 'ei',
      description: 'Mass Upload v2.0 evaluation file'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '37',
    name: 'ESrs3441_20200114_SettlementDetails_OperationalSettlement_20200114010900.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '38',
    name: 'ESei3441_20200125_002',
    type: {
      descriptor: 'ei',
      description: 'Mass Upload v2.0 evaluation file'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '39',
    name: 'ESrs3441_20200114_SettlementDetails_OperationalSettlement_20200114010900.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '40',
    name: 'ESei3441_20200125_002',
    type: {
      descriptor: 'ei',
      description: 'Mass Upload v2.0 evaluation file'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '41',
    name: 'ESrs3441_20200114_SettlementDetails_OperationalSettlement_20200114010900.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '42',
    name: 'ESei3441_20200125_002',
    type: {
      descriptor: 'ei',
      description: 'Mass Upload v2.0 evaluation file'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '43',
    name: 'ESrs3441_20200114_SettlementDetails_OperationalSettlement_20200114010900.csv',
    type: {
      descriptor: 'rs',
      description: 'R&S Reports'
    },
    uploadDate: '2020-02-29',
    size: 856485,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '44',
    name: 'ES12ALL_20200109_Welcome, Vistara to BSP Spain.txt',
    type: {
      descriptor: '12',
      description: 'IATA Important Information'
    },
    uploadDate: '2020-03-05',
    size: 785412,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '45',
    name: 'ESuu0000_20200212_BSP ES Suspension of Air Italy (IG-191).txt',
    type: {
      descriptor: 'uu',
      description: 'Urgent Information from the BSP'
    },
    uploadDate: '2020-03-05',
    size: 589632,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '46',
    name: 'ESmi0000_20200224_Management_Information_Report_20200224.txt',
    type: {
      descriptor: 'mi',
      description: 'Daily sales monitoring report'
    },
    uploadDate: '2020-03-05',
    size: 789123,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '47',
    name: 'ESmi0000_20200210_Management_Information_Report_20200210.pdf',
    type: {
      descriptor: 'mi',
      description: 'Daily sales monitoring report'
    },
    uploadDate: '2020-03-05',
    size: 616161,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '48',
    name: 'ESuu0000_20200201_BSP Information update.txt',
    type: {
      descriptor: 'uu',
      description: 'Urgent Information from the BSP'
    },
    uploadDate: '2020-03-05',
    size: 838551,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '49',
    name: 'ES12ALL_20200116_BSP ES - RET819 Card type IIN not accepted Modified Airline.txt',
    type: {
      descriptor: '12',
      description: 'IATA Important Information'
    },
    uploadDate: '2020-03-05',
    size: 856555,
    status: FileStatus.NotDownloaded,
    isoCountryCode: '6983'
  },
  {
    id: '50',
    name: 'ES12ALL_20191230_Calendar Change Notification.txt',
    type: {
      descriptor: '12',
      description: 'IATA Important Information'
    },
    uploadDate: '2020-03-05',
    size: 756485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '51',
    name: 'ESuu0000_20200128_Suspension of Andes Lineas Aereas (OY 650)_BSP Spain.txt',
    type: {
      descriptor: 'uu',
      description: 'Urgent Information from the BSP'
    },
    uploadDate: '2020-03-05',
    size: 512085,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  },
  {
    id: '52',
    name: 'ESmi0000_20200113_Management_Information_Report_20200113.txt',
    type: {
      descriptor: 'mi',
      description: 'Daily sales monitoring report'
    },
    uploadDate: '2020-03-05',
    size: 100485,
    status: FileStatus.Archived,
    isoCountryCode: '6983'
  }
];

export const fileTypes = [
  {
    descriptor: 'az',
    description: 'IBSPs Billing Output'
  },
  {
    descriptor: 'd1',
    description: 'Airline Default Summary'
  },
  {
    descriptor: 'ei',
    description: 'Mass Upload v2.0 evaluation file'
  },
  {
    descriptor: 'i3',
    description: 'TIP reports'
  },
  {
    descriptor: 'mk',
    description: 'Unmasked CC# file(s)'
  },
  {
    descriptor: 'rs',
    description: 'R&S Reports'
  },
  {
    descriptor: 'ai',
    description: 'TEST FILES'
  },
  {
    descriptor: 'eh',
    description: 'ACM/ADM mass download v2.0 file'
  },
  {
    descriptor: 'xx',
    description: 'IATA invoice'
  },
  {
    descriptor: 'nl',
    description: 'Newsletter'
  },
  {
    descriptor: 'pg',
    description: 'PBD download zipped file'
  },
  {
    descriptor: '12',
    description: 'IATA Important Information'
  },
  {
    descriptor: 'uu',
    description: 'Urgent Information from the BSP'
  },
  {
    descriptor: 'mi',
    description: 'Daily sales monitoring report'
  },
  {
    descriptor: 'xxx',
    description: 'Invoice from IATA'
  },
  {
    descriptor: 'xxxx',
    description: 'SALES BALANCING TAPE (4031)'
  }
];
