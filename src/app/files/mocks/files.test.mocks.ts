import * as fromAgentReports from '~app/files/reducers/agent-reports.reducer';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';

export const initialState = {
  auth: {
    user: createIataUser()
  },
  router: null,
  files: {
    reports: {
      selectedBsp: {
        id: 54
      }
    },
    agentReports: fromAgentReports.initialState
  },
  core: {
    menu: {
      tabs: { dashboard: { ...ROUTES.DASHBOARD, id: 'dashboard' } },
      activeTabId: 'dashboard'
    },
    viewListsInfo: {}
  }
};
