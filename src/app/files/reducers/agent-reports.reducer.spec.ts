import { AgentReportsActions } from '../actions';
import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import { FileInfoViewListModel } from '../model/files.model';

import * as fromAgentReports from './agent-reports.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('Agent Reports Reducer', () => {
  it('should update state on changeDefaultFilter', () => {
    const defaultFilter: AgentReportsFilter = {
      type: { descriptor: 'ac', description: 'ac' },
      bsp: { id: 1 }
    };
    const action = AgentReportsActions.changeDefaultFilter({ filter: defaultFilter });
    const oldState = fromAgentReports.initialState;

    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.defaultFilter).toEqual(defaultFilter);
    expect(newState.query).toEqual(null);
    expect(newState.canProceed).toBe(true);
  });

  it('should set loading to `true` on search', () => {
    const action = AgentReportsActions.search({ query: defaultQuery });

    const newState = fromAgentReports.reducer(fromAgentReports.initialState, action);

    expect(newState.loading).toBeTruthy();
  });

  it('should update state on search', () => {
    const action = AgentReportsActions.search({ query: defaultQuery });

    const newState = fromAgentReports.reducer(fromAgentReports.initialState, action);

    expect(newState.query).toEqual(defaultQuery);
    expect(newState.canProceed).toBe(null);
  });

  it('should set canProceed to false on cancelDefaultFilterDialog', () => {
    const action = AgentReportsActions.cancelDefaultFilterDialog();

    const newState = fromAgentReports.reducer(fromAgentReports.initialState, action);

    expect(newState.canProceed).toBe(false);
  });

  it('should set canProceed to null on openDefaultFilterDialog', () => {
    const action = AgentReportsActions.openDefaultFilterDialog();

    const newState = fromAgentReports.reducer(fromAgentReports.initialState, action);

    expect(newState.canProceed).toBe(null);
  });

  it('should update state on searchSuccess', () => {
    const action = AgentReportsActions.searchSuccess({
      result: {
        records: [],
        pageNumber: 0,
        pageSize: 10,
        total: 10,
        totalPages: 1
      }
    });
    const oldState = {
      ...fromAgentReports.initialState,
      loading: true
    };

    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.loading).toBeFalsy();
    expect(newState.query.paginateBy).toEqual({ page: 0, size: 10, totalElements: 10 });
    expect(newState.data).toEqual([]);
  });

  it('should update state on searchFailure', () => {
    const action = AgentReportsActions.searchError();
    const oldState = {
      ...fromAgentReports.initialState,
      loading: true
    };

    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.loading).toBeFalsy();
    expect(newState.data).toEqual([]);
  });

  it('should set data on selectionChange', () => {
    const action = AgentReportsActions.selectionChange({ data: [{} as FileInfoViewListModel] });
    const oldState = { ...fromAgentReports.initialState };
    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.data).toEqual([{}]);
  });

  it('should clear data isSelected values on clearSelection', () => {
    const action = AgentReportsActions.clearSelection();
    const oldState = { ...fromAgentReports.initialState, data: [{ isSelected: true } as FileInfoViewListModel] };
    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.data).toEqual([{ isSelected: false }]);
  });

  it('should set loading to `true` on download', () => {
    const action = AgentReportsActions.download({ file: null });
    const oldState = fromAgentReports.initialState;

    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.loading).toBeTruthy();
  });

  it('should set loading to `false` on downloadSuccess', () => {
    const action = AgentReportsActions.downloadSuccess();
    const oldState = {
      ...fromAgentReports.initialState,
      loading: true
    };

    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.loading).toBeFalsy();
  });

  it('should set loading to `false` on downloadError', () => {
    const action = AgentReportsActions.downloadError();
    const oldState = {
      ...fromAgentReports.initialState,
      loading: true
    };

    const newState = fromAgentReports.reducer(oldState, action);

    expect(newState.loading).toBeFalsy();
  });

  describe('getSelectedData', () => {
    it('should return empty array, when data is null', () => {
      const state = { ...fromAgentReports.initialState, data: null };
      const selectedData = fromAgentReports.getSelectedData(state);

      expect(selectedData).toEqual([]);
    });

    it('should return selected data items', () => {
      const data = [{}, { isSelected: true }, { isSelected: false }] as FileInfoViewListModel[];
      const state = { ...fromAgentReports.initialState, data };
      const selectedData = fromAgentReports.getSelectedData(state);

      expect(selectedData.length).toBe(1);
    });
  });
});
