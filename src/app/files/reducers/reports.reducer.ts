import { createReducer, on } from '@ngrx/store';

import { ReportsActions } from '../actions';
import { BspDto } from '~app/shared/models/bsp.model';

export interface ReportsState {
  selectedBsp: BspDto;
}

const initialState: ReportsState = {
  selectedBsp: null
};

export const reducer = createReducer(
  initialState,
  on(ReportsActions.selectBsp, (state, { selectedBsp }) => ({
    ...state,
    selectedBsp
  }))
);
