import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import * as fromFiles from './index';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';

describe('File Reducers', () => {
  describe('getAgentReportDefaultQuery', () => {});

  describe('getAgentReportsQuery', () => {
    const defaultQuery = fromFiles.getAgentReportDefaultQuery.projector(null);

    it('should return default query when no query is stored', () => {
      expect(fromFiles.getAgentReportsQuery.projector({}, defaultQuery)).toEqual(defaultQuery);
    });

    it('should apply sorting defaults when no sorting is specified in query', () => {
      const storedQuery: DataQuery<AgentReportsFilter> = {
        sortBy: [],
        filterBy: { name: 'test' },
        paginateBy: { page: 1, size: 20, totalElements: 30 }
      };

      expect(fromFiles.getAgentReportsQuery.projector({ query: storedQuery }, defaultQuery)).toEqual({
        sortBy: [{ attribute: 'uploadDate', sortType: SortOrder.Desc }],
        filterBy: { name: 'test' },
        paginateBy: { page: 1, size: 20, totalElements: 30 }
      });
    });

    it('should preserve sorting when it is specified in query', () => {
      const storedQuery: DataQuery<AgentReportsFilter> = {
        sortBy: [{ attribute: 'name', sortType: SortOrder.Asc }],
        filterBy: { name: 'test' },
        paginateBy: { page: 0, size: 20, totalElements: 10 }
      };

      expect(fromFiles.getAgentReportsQuery.projector({ query: storedQuery }, defaultQuery)).toEqual(storedQuery);
    });
  });
});
