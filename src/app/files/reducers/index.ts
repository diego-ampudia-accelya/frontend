import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
import { merge } from 'lodash';

import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import * as fromAgentReports from './agent-reports.reducer';
import * as fromReports from './reports.reducer';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';

export interface FilesState {
  reports: fromReports.ReportsState;
  agentReports: fromAgentReports.AgentReportsState;
}

export interface State extends AppState {
  files: FilesState;
}

export function reducers(state: FilesState | undefined, action: Action) {
  return combineReducers({
    reports: fromReports.reducer,
    agentReports: fromAgentReports.reducer
  })(state, action);
}

export const getFilesState = createFeatureSelector<FilesState>('files');

export const getReports = createSelector(getFilesState, state => state?.reports);

export const getSelectedBsp = createSelector(getReports, state => state?.selectedBsp);

export const getAgentReports = createSelector(getFilesState, state => state?.agentReports);

export const getAgentReportsLoading = createSelector(getAgentReports, fromAgentReports.getLoading);

export const getAgentReportsData = createSelector(getAgentReports, fromAgentReports.getData);

export const getAgentReportsSelectedData = createSelector(getAgentReports, fromAgentReports.getSelectedData);

export const getAgentReportsSelectedDataCount = createSelector(getAgentReports, fromAgentReports.getSelectedDataCount);

export const getAgentReportDefaultFilter = createSelector(getAgentReports, fromAgentReports.getDefaultFilter);

export const canProceedToAgentReports = createSelector(getAgentReports, fromAgentReports.canProceed);

export const getAgentReportDefaultQuery = createSelector(
  getAgentReportDefaultFilter,
  (defaultFilter): DataQuery<AgentReportsFilter> => ({
    sortBy: [{ attribute: 'uploadDate', sortType: SortOrder.Desc }],
    filterBy: defaultFilter,
    paginateBy: { page: 0, size: 20, totalElements: 0 }
  })
);

export const getAgentReportsQuery = createSelector(
  [getAgentReports, getAgentReportDefaultQuery],
  (state, defaultDataQuery): DataQuery<AgentReportsFilter> => merge({}, defaultDataQuery, state.query)
);
