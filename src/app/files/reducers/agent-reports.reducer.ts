import { createReducer, on } from '@ngrx/store';

import { AgentReportsActions } from '~app/files/actions';
import { AgentReportsFilter } from '~app/files/model/agent-reports-filter.model';
import { FileInfoViewListModel } from '~app/files/model/files.model';
import { DataQuery } from '~app/shared/components/list-view';

export interface AgentReportsState {
  defaultFilter: AgentReportsFilter;
  query: DataQuery<AgentReportsFilter>;
  data: FileInfoViewListModel[];
  loading: boolean;
  canProceed: boolean | null;
}

export const initialState: AgentReportsState = {
  defaultFilter: null,
  query: null,
  data: null,
  loading: false,
  canProceed: null
};

export const reducer = createReducer(
  initialState,
  on(AgentReportsActions.changeDefaultFilter, (state, { filter }) => ({
    ...state,
    defaultFilter: filter,
    query: null,
    canProceed: true
  })),
  on(AgentReportsActions.openDefaultFilterDialog, state => ({
    ...state,
    canProceed: null
  })),
  on(AgentReportsActions.cancelDefaultFilterDialog, state => ({
    ...state,
    canProceed: false
  })),
  on(AgentReportsActions.search, (state, { query }) => ({
    ...state,
    query: query || state.query,
    loading: true,
    canProceed: null
  })),
  on(AgentReportsActions.searchSuccess, (state, { result }) => ({
    ...state,
    query: {
      ...state.query,
      paginateBy: {
        page: result.pageNumber,
        size: result.pageSize,
        totalElements: result.total
      }
    },
    data: result.records || [],
    loading: false
  })),
  on(AgentReportsActions.selectionChange, (state, { data }) => ({
    ...state,
    data
  })),
  on(AgentReportsActions.clearSelection, state => ({
    ...state,
    data: state.data.map(item => ({ ...item, isSelected: false }))
  })),
  on(AgentReportsActions.activate, AgentReportsActions.searchError, state => ({
    ...state,
    data: [],
    loading: false
  })),
  on(AgentReportsActions.download, AgentReportsActions.bulkDownload, state => ({ ...state, loading: true })),
  on(
    AgentReportsActions.downloadSuccess,
    AgentReportsActions.downloadError,
    AgentReportsActions.bulkDownloadSuccess,
    AgentReportsActions.bulkDownloadError,
    state => ({ ...state, loading: false })
  )
);

export const getLoading = (state: AgentReportsState) => state.loading;
export const getData = (state: AgentReportsState) => state.data;
export const getSelectedData = (state: AgentReportsState) => state.data?.filter(item => Boolean(item.isSelected)) ?? [];
export const getSelectedDataCount = (state: AgentReportsState) => getSelectedData(state).length;
export const getQuery = (state: AgentReportsState) => state.query;
export const getDefaultFilter = (state: AgentReportsState) => state.defaultFilter;
export const canProceed = (state: AgentReportsState) => state.canProceed;
