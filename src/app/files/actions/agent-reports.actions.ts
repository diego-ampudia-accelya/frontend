import { createAction, props } from '@ngrx/store';

import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import { FileInfo, FileInfoViewListModel } from '../model/files.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const openDefaultFilterDialog = createAction('[Agent Reports] Open Default Filter Dialog');
export const cancelDefaultFilterDialog = createAction('[Agent Reports] Cancel Default Filter Dialog');

export const activate = createAction('[Agent Reports] Activate');

export const changeDefaultFilter = createAction(
  '[Agent Reports] Change Default Filter',
  props<{ filter: AgentReportsFilter }>()
);

export const search = createAction(
  '[Agent Reports] Search',
  props<{ query?: DataQuery<AgentReportsFilter>; refreshCache?: boolean }>()
);
export const searchSuccess = createAction('[Agent Reports] Search Success', props<{ result: PagedData<FileInfo> }>());
export const searchError = createAction('[Agent Reports] Search Error');

export const selectionChange = createAction(
  '[Agent Reports] Selection Change',
  props<{ data: FileInfoViewListModel[] }>()
);
export const clearSelection = createAction('[Agent Reports] Clear Selection');

export const download = createAction('[Agent Reports] Download', props<{ file: FileInfo }>());
export const downloadSuccess = createAction('[Agent Reports] Download Success');
export const downloadError = createAction('[Agent Reports] Download Error');

export const bulkDownload = createAction('[Agent Reports] Bulk Download');
export const bulkDownloadSuccess = createAction('[Agent Reports] Bulk Download Success');
export const bulkDownloadError = createAction('[Agent Reports] Bulk Download Error');
