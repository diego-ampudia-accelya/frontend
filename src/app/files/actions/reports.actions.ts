import { createAction, props } from '@ngrx/store';

import { BspDto } from '~app/shared/models/bsp.model';

export const selectBsp = createAction('[Reports] Select BSP', props<{ selectedBsp: BspDto }>());
