import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import FileSaver from 'file-saver';
import { defer, Observable } from 'rxjs';
import { filter, first, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AgentReportsActions } from '../actions';
import { AgentReportsService } from '../agent-reports/agent-reports.service';
import { DefaultFiltersDialogComponent } from '../agent-reports/default-filters-dialog/default-filters-dialog.component';
import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import * as fromAgentReports from '../reducers';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { ROUTES } from '~app/shared/constants/routes';
import { DialogService, FooterButton } from '~app/shared/components';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class AgentReportsEffects {
  search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AgentReportsActions.search),
      withLatestFrom(this.store.pipe(select(fromAgentReports.getAgentReportsQuery))),
      switchMap(([{ refreshCache }, query]) => this.agentReportsService.find(query, { refreshCache })),
      map(result => AgentReportsActions.searchSuccess({ result })),
      rethrowError(() => AgentReportsActions.searchError())
    )
  );

  download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AgentReportsActions.download),
      switchMap(({ file }) => this.agentReportsService.download(file.id)),
      tap(({ blob, fileName }) => FileSaver.saveAs(blob, fileName)),
      map(() => AgentReportsActions.downloadSuccess()),
      rethrowError(() => AgentReportsActions.downloadError())
    )
  );

  bulkDownload$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AgentReportsActions.bulkDownload),
      withLatestFrom(this.store.pipe(select(fromAgentReports.getAgentReportsSelectedData))),
      switchMap(([_, files]) => this.agentReportsService.bulkDownload(files)),
      tap(({ blob, fileName }) => FileSaver.saveAs(blob, fileName)),
      map(() => AgentReportsActions.bulkDownloadSuccess()),
      rethrowError(() => AgentReportsActions.bulkDownloadError())
    )
  );

  openDefaultFiltersDialog$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AgentReportsActions.openDefaultFilterDialog),
      switchMap(() => this.promptForDefaultFilter()),
      map(defaultFilter =>
        defaultFilter
          ? AgentReportsActions.changeDefaultFilter({ filter: defaultFilter })
          : AgentReportsActions.cancelDefaultFilterDialog()
      )
    )
  );

  changeDefaultFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AgentReportsActions.changeDefaultFilter),
      filter(() => this.router.isActive(ROUTES.AGENT_REPORTS.url, true)),
      map(() => AgentReportsActions.search({ refreshCache: true }))
    )
  );

  constructor(
    private store: Store<any>,
    private actions$: Actions,
    private agentReportsService: AgentReportsService,
    private dialogService: DialogService,
    private router: Router
  ) {}

  private promptForDefaultFilter(): Observable<AgentReportsFilter> {
    const bsp$ = this.store.pipe(
      select(fromAuth.getUserBsps),
      map(([bsp]) => bsp)
    );

    return defer(() =>
      this.dialogService.open(DefaultFiltersDialogComponent, {
        data: {
          title: 'MENU.files.agentReports.parameters.title',
          footerButtonsType: [{ type: FooterButton.Search }],
          hasCancelButton: true,
          isClosable: true
        }
      })
    ).pipe(
      first(),
      tap(() => this.dialogService.close()),
      withLatestFrom(bsp$),
      map(([{ clickedBtn, contentComponentRef }, bsp]) =>
        clickedBtn === FooterButton.Search ? { ...contentComponentRef.form.value, bsp } : null
      )
    );
  }
}
