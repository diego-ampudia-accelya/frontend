import { fakeAsync, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import FileSaver from 'file-saver';
import { take } from 'lodash';
import { Observable, of, throwError } from 'rxjs';

import { AgentReportsActions } from '../actions';
import { AgentReportsService } from '../agent-reports/agent-reports.service';
import { files } from '../mocks/files.mock';
import { FileType } from '../model';
import { FileInfo } from '../model/files.model';
import * as fromFiles from '../reducers';

import { AgentReportsEffects } from './agent-reports.effects';
import { PagedData } from '~app/shared/models/paged-data.model';
import { Bsp } from '~app/shared/models/bsp.model';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DialogService, FooterButton } from '~app/shared/components';
import { initialState } from '~app/files/mocks/files.test.mocks';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

describe('AgentReportsEffects', () => {
  let effects: AgentReportsEffects;
  let actions$: Observable<Action>;
  let mockStore: MockStore<any>;
  let reportsServiceSpy: SpyObject<AgentReportsService>;
  let routerSpy: SpyObject<Router>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let testFiles: FileInfo[];

  const usa = { id: 1 } as Bsp;
  const acFileType: FileType = { descriptor: 'ac', description: '' };

  beforeEach(() => {
    testFiles = take(files, 2);

    TestBed.configureTestingModule({
      providers: [
        AgentReportsEffects,
        mockProvider(AgentReportsService),
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromFiles.getAgentReportsQuery, value: null },
            { selector: fromFiles.getAgentReportsSelectedData, value: testFiles },
            { selector: fromAuth.getUserBsps, value: [usa] }
          ]
        }),
        provideMockActions(() => actions$),
        mockProvider(DialogService),
        mockProvider(Router)
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.inject(AgentReportsEffects);
    mockStore = TestBed.inject<any>(Store);
    reportsServiceSpy = TestBed.inject<any>(AgentReportsService);
    routerSpy = TestBed.inject<any>(Router);
    dialogServiceSpy = TestBed.inject<any>(DialogService);
  });

  afterEach(() => {
    actions$ = null;
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('search$', () => {
    it('should dispatch searchSuccess when a search result is received', fakeAsync(() => {
      actions$ = of(AgentReportsActions.search({ query: defaultQuery, refreshCache: true }));
      mockStore.overrideSelector(fromFiles.getAgentReportsQuery, defaultQuery);

      const result: PagedData<FileInfo> = {
        records: testFiles,
        pageSize: 20,
        pageNumber: 0,
        total: files.length,
        totalPages: 0
      };
      reportsServiceSpy.find.and.returnValue(of(result));

      let dispatchedAction: Action;
      effects.search$.subscribe(action => (dispatchedAction = action));

      expect(reportsServiceSpy.find).toHaveBeenCalledWith(defaultQuery, { refreshCache: true });
      expect(dispatchedAction).toEqual(AgentReportsActions.searchSuccess({ result }));
    }));

    it('should dispatch searchError and throw when an error is received', fakeAsync(() => {
      reportsServiceSpy.find.and.returnValue(throwError(new Error()));
      mockStore.overrideSelector(fromFiles.getAgentReportsQuery, defaultQuery);
      actions$ = of(AgentReportsActions.search({ query: defaultQuery }));

      let dispatchedAction: Action;
      let actualError: Error;
      effects.search$.subscribe(
        action => (dispatchedAction = action),
        error => (actualError = error)
      );

      expect(dispatchedAction).toEqual(AgentReportsActions.searchError());
      expect(actualError).toBeDefined();
    }));
  });

  describe('download$', () => {
    let fileSaverSpy: jasmine.Spy;

    beforeEach(() => {
      fileSaverSpy = spyOn(FileSaver, 'saveAs');
    });

    it('should dispatch downloadSuccess when download completes', fakeAsync(() => {
      reportsServiceSpy.download.and.returnValue(of({ blob: new Blob(), fileName: 'test.txt' }));
      const fileToDownload = { id: '1' } as FileInfo;
      const downloadAction = AgentReportsActions.download({ file: fileToDownload });
      actions$ = of(downloadAction);

      let dispatchedAction: Action;
      effects.download$.subscribe(action => (dispatchedAction = action));

      expect(reportsServiceSpy.download).toHaveBeenCalledWith(fileToDownload.id);
      expect(fileSaverSpy).toHaveBeenCalledWith(jasmine.any(Blob), 'test.txt');
      expect(dispatchedAction).toEqual(AgentReportsActions.downloadSuccess());
    }));

    it('should dispatch downloadError and throw when an error is received', fakeAsync(() => {
      reportsServiceSpy.download.and.returnValue(throwError(new Error()));
      const fileToDownload = { id: '1' } as FileInfo;
      const downloadAction = AgentReportsActions.download({ file: fileToDownload });
      actions$ = of(downloadAction);

      let dispatchedAction: Action;
      let actualError: Error;
      effects.download$.subscribe(
        action => (dispatchedAction = action),
        error => (actualError = error)
      );

      expect(dispatchedAction).toEqual(AgentReportsActions.downloadError());
      expect(actualError).toBeDefined();
    }));
  });

  describe('bulkDownload$', () => {
    let fileSaverSpy: jasmine.Spy;

    beforeEach(() => {
      fileSaverSpy = spyOn(FileSaver, 'saveAs');
    });

    it('should dispatch bulkDownloadSuccess when bulkDownload completes', fakeAsync(() => {
      reportsServiceSpy.bulkDownload.and.returnValue(of({ blob: new Blob(), fileName: 'test.txt' }));
      const bulkDownloadAction = AgentReportsActions.bulkDownload();
      actions$ = of(bulkDownloadAction);

      let dispatchedAction: Action;
      effects.bulkDownload$.subscribe(action => (dispatchedAction = action));

      expect(reportsServiceSpy.bulkDownload).toHaveBeenCalledWith(testFiles);
      expect(fileSaverSpy).toHaveBeenCalledWith(jasmine.any(Blob), 'test.txt');
      expect(dispatchedAction).toEqual(AgentReportsActions.bulkDownloadSuccess());
    }));

    it('should dispatch bulkDownloadError and throw when an error is received', fakeAsync(() => {
      reportsServiceSpy.download.and.returnValue(throwError(new Error()));
      const bulkDownloadAction = AgentReportsActions.bulkDownload();
      actions$ = of(bulkDownloadAction);

      let dispatchedAction: Action;
      let actualError: Error;
      effects.bulkDownload$.subscribe(
        action => (dispatchedAction = action),
        error => (actualError = error)
      );

      expect(dispatchedAction).toEqual(AgentReportsActions.bulkDownloadError());
      expect(actualError).toBeDefined();
    }));
  });

  describe('changeDefaultFilter', () => {
    it('should dispatch search action when Agent Reports is the active route', fakeAsync(() => {
      const action = AgentReportsActions.changeDefaultFilter({ filter: null });
      actions$ = of(action);
      routerSpy.isActive.and.returnValue(true);

      let dispatchedAction: Action;
      effects.changeDefaultFilter$.subscribe(result => (dispatchedAction = result));

      expect(dispatchedAction).toEqual(AgentReportsActions.search({ refreshCache: true }));
    }));

    it('should dispatch no action when Agent Reports is not the active route', fakeAsync(() => {
      const action = AgentReportsActions.changeDefaultFilter({ filter: null });
      actions$ = of(action);
      routerSpy.isActive.and.returnValue(false);

      let dispatchedAction: Action;
      effects.changeDefaultFilter$.subscribe(result => (dispatchedAction = result));

      expect(dispatchedAction).not.toBeDefined();
    }));
  });

  describe('openDefaultFilterDialog', () => {
    beforeEach(() => {
      const openAction = AgentReportsActions.openDefaultFilterDialog();
      actions$ = of(openAction);
    });

    it('should dispatch changeDefaultFilter action when the search button is clicked', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(
        of({
          clickedBtn: FooterButton.Search,
          contentComponentRef: { form: { value: { type: acFileType } } }
        })
      );

      let dispatchedAction: Action;
      effects.openDefaultFiltersDialog$.subscribe(result => (dispatchedAction = result));

      expect(dispatchedAction).toEqual(
        AgentReportsActions.changeDefaultFilter({ filter: { bsp: usa, type: acFileType } })
      );
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should dispatch cancelDefaultFilterDialog action when the cancel button is clicked', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));

      let dispatchedAction: Action;
      effects.openDefaultFiltersDialog$.subscribe(result => (dispatchedAction = result));

      expect(dispatchedAction).toEqual(AgentReportsActions.cancelDefaultFilterDialog());
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });
});
