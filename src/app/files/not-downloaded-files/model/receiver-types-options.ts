import { ReceiverTypeOption } from './receiver-type';
import { DropdownOption } from '~app/shared/models';

export enum ERecieverType {
  Airline = 'Airline',
  Agent = 'Agent',
  IATA = 'IATA',
  DPC = 'DPC',
  EARS = 'EARS',
  GDS = 'GDS',
  ThirdParty = 'Third Party',
  AgentGroup = 'Agent Group'
}

export const RECEIVER_TYPES_OPTIONS: DropdownOption<ReceiverTypeOption>[] = [
  {
    value: { receiverType: ERecieverType.Airline, receiverTypeId: 0 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.Airline}`
  },
  {
    value: { receiverType: ERecieverType.Agent, receiverTypeId: 1 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.Agent}`
  },
  {
    value: { receiverType: ERecieverType.IATA, receiverTypeId: 2 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.IATA}`
  },
  {
    value: { receiverType: ERecieverType.DPC, receiverTypeId: 3 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.DPC}`
  },
  {
    value: { receiverType: ERecieverType.EARS, receiverTypeId: 4 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.EARS}`
  },
  {
    value: { receiverType: ERecieverType.GDS, receiverTypeId: 5 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.GDS}`
  },
  {
    value: { receiverType: ERecieverType.ThirdParty, receiverTypeId: 7 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.ThirdParty.replace(/ /g, '')}`
  },
  {
    value: { receiverType: ERecieverType.AgentGroup, receiverTypeId: 8 },
    label: `MENU.files.notDownloadedFiles.receiverType.${ERecieverType.AgentGroup.replace(/ /g, '')}`
  }
];
