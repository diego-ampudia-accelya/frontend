export interface ReceiverTypeOption {
  receiverType: string;
  receiverTypeId: number;
}
