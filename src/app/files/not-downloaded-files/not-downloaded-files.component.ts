import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import moment from 'moment-mini';
import { Observable } from 'rxjs';

import { NotDownloadedFilesFilter } from '../model/not-downloaded-files-filter.model';
import { NotDownloadedFile } from '../model/not-downloaded-files.model';
import { sameMonthValidator } from '../uploaded-files/validators/same-month.validator';
import { ReceiverTypeOption } from './model/receiver-type';
import { RECEIVER_TYPES_OPTIONS } from './model/receiver-types-options';
import { NotDownloadedFilesFilterFormatter } from './services/not-downloaded-files-formatter';
import { NotDownloadedFilesService } from './services/not-downloaded-files.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { FileDescriptorDictionaryService } from '~app/shared/services/dictionary/file-descriptor-dictionary.service';

@Component({
  selector: 'bspl-not-downloaded-files',
  templateUrl: './not-downloaded-files.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: NotDownloadedFilesService }
  ]
})
export class NotDownloadedFilesComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public fileTypesOptions$: Observable<DropdownOption[]>;

  public maxDate = moment().endOf('day').toDate();
  public minDate = moment(this.maxDate).subtract(2, 'years').toDate();

  public receiverTypesOptions: DropdownOption<ReceiverTypeOption>[] = RECEIVER_TYPES_OPTIONS.map(
    ({ value, label }) => ({
      value,
      label: this.translationService.translate(label)
    })
  );

  public predefinedFilters: {
    sendDate: Date[];
  };

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private sendedDateInitialValue = [moment().startOf('month').toDate(), this.maxDate];

  constructor(
    public displayFormatter: NotDownloadedFilesFilterFormatter,
    public dataSource: QueryableDataSource<NotDownloadedFile>,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private queryStorage: DefaultQueryStorage,
    private permissionsService: PermissionsService,
    private fileDescriptorDictionary: FileDescriptorDictionaryService,
    private notDownloadedFilesService: NotDownloadedFilesService,
    private translationService: L10nTranslationService,
    private dialogService: DialogService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.initializeFileTypeDropdown();
    this.checkPermissionAndLoadData();

    this.initColumns();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.files.notDownloadedFiles.notDownloadedFilesDownloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.notDownloadedFilesService
    });
  }

  public loadData(query: DataQuery<NotDownloadedFilesFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private checkPermissionAndLoadData(): void {
    const hasReadNotDownloadedFilesPermissions = this.permissionsService.hasPermission(
      Permissions.readNotDownloadedFiles
    );
    if (hasReadNotDownloadedFilesPermissions) {
      this.initializeDataQuery();
    }
  }

  private initializeDataQuery(): void {
    this.setPredefinedFilters();
    this.loadData(this.storedQuery);
  }

  private initializeFileTypeDropdown(): void {
    this.fileTypesOptions$ = this.fileDescriptorDictionary.getDropdownOptions();
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<NotDownloadedFilesFilter>({
      sendDate: [null, sameMonthValidator()],
      fileTypes: [],
      receiverTypes: []
    });
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      sendDate: this.sendedDateInitialValue
    };
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'name',
        name: 'MENU.files.notDownloadedFiles.columns.fileName',
        flexGrow: 4
      },
      {
        prop: 'type',
        name: 'MENU.files.notDownloadedFiles.columns.fileType',
        resizeable: true,
        flexGrow: 4
      },
      {
        prop: 'sendDate',
        name: 'MENU.files.notDownloadedFiles.columns.sendDate',
        flexGrow: 1,
        resizeable: true,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy')
        }
      },
      {
        prop: 'receiverType',
        name: 'MENU.files.notDownloadedFiles.columns.receiverType',
        flexGrow: 1
      },
      {
        prop: 'receiverUserId',
        name: 'MENU.files.notDownloadedFiles.columns.receiverUserId',
        flexGrow: 1
      }
    ];
  }
}
