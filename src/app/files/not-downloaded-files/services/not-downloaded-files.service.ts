import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { NotDownloadedFile, NotDownloadedFilesFilter, NotDownloadedFilesFilterBE } from '~app/files/model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class NotDownloadedFilesService implements Queryable<NotDownloadedFile> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/not-downloaded-files`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<NotDownloadedFilesFilter>): Observable<PagedData<NotDownloadedFile>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<NotDownloadedFile>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<NotDownloadedFilesFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<NotDownloadedFilesFilter>>): RequestQuery<NotDownloadedFilesFilterBE> {
    const { sendDate, receiverTypes, fileTypes, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromSendDate: sendDate && toShortIsoDate(sendDate[0]),
        toSendDate: sendDate && toShortIsoDate(sendDate[1]),
        receiverType: receiverTypes && receiverTypes.map(s => s.receiverType),
        descriptor: fileTypes && fileTypes.map(t => t.descriptor)
      }
    });
  }
}
