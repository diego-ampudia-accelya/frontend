import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { NotDownloadedFilesFilter } from '~app/files/model/not-downloaded-files-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class NotDownloadedFilesFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: NotDownloadedFilesFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<NotDownloadedFilesFilter>> = {
      sendDate: sendDateRange => `${this.translate('sendDateRange')} - ${rangeDateFilterTagMapper(sendDateRange)}`,
      fileTypes: type => `${this.translate('fileType')} - ${mapJoinMapper(type, 'description')}`,
      receiverTypes: receiverType =>
        `${this.translate('receiverType')} - ${mapJoinMapper(receiverType, 'receiverType')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.files.notDownloadedFiles.filters.${key}`);
  }
}
