import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AgentReportsComponent } from './agent-reports/agent-reports.component';
import { AgentReportsService } from './agent-reports/agent-reports.service';
import { DefaultFiltersDialogComponent } from './agent-reports/default-filters-dialog/default-filters-dialog.component';
import { DeletedFilesDialogComponent } from './deleted-files/deleted-files-dialog/deleted-files-dialog.component';
import { DeletedFilesComponent } from './deleted-files/deleted-files.component';
import { MassDeleteDialogComponent } from './deleted-files/mass-delete-dialog/mass-delete-dialog.component';
import { DeletedFilesDialogService } from './deleted-files/services/deleted-files-dialog.service';
import { DeletedFilesFilterFormatter } from './deleted-files/services/deleted-files-filter-formatter';
import { DeletedFilesService } from './deleted-files/services/deleted-files.service';
import { DownloadedFilesComponent } from './downloaded-files/downloaded-files.component';
import { DownloadedFilesFilterFormatter } from './downloaded-files/services/downloaded-files-filter-formatter';
import { DownloadedFilesService } from './downloaded-files/services/downloaded-files.service';
import { DownloadedIataCommunicationsComponent } from './downloaded-iata-communications/downloaded-iata-communications.component';
import { DownloadedIataCommunicationsFilterFormatter } from './downloaded-iata-communications/services/downloaded-iata-communications-filter-formatter';
import { DownloadedIataCommunicationsService } from './downloaded-iata-communications/services/downloaded-iata-communications.service';
import { AgentReportsEffects } from './effects/agent-reports.effects';
import { EvaluationFilesComponent } from './evaluation-files/evaluation-files.component';
import { FilesRoutingModule } from './files-routing.module';
import { FopAgentStatusRequestDialogComponent } from './fop-agent-status-request/fop-agent-status-request-dialog.component';
import { FopAgentStatusRequestGuard } from './fop-agent-status-request/guards/fop-agent-status-request.guard';
import { FopAgentStatusRequestDialogService } from './fop-agent-status-request/services/fop-agent-status-request-dialog.service';
import { FopAgentStatusService } from './fop-agent-status-request/services/fop-agent-status.service';
import { GlobalTaImageRequestDialogComponent } from './global-ta/global-ta-image-request-dialog/global-ta-image-request-dialog.component';
import { GlobalTaStatusDialogComponent } from './global-ta/global-ta-status-dialog/global-ta-status-dialog.component';
import { GlobalTaComponent } from './global-ta/global-ta.component';
import { GlobalNonTaImageRequestDialogService } from './global-ta/services/global-non-ta-image-request-dialog.service';
import { GlobalTaImageRequestDialogService } from './global-ta/services/global-ta-image-request-dialog.service';
import { GlobalTaService } from './global-ta/services/global-ta.service';
import { UploadFilesGuard } from './guards/upload-files.guard';
import { NotDownloadedFilesComponent } from './not-downloaded-files/not-downloaded-files.component';
import { NotDownloadedFilesFilterFormatter } from './not-downloaded-files/services/not-downloaded-files-formatter';
import { NotDownloadedFilesService } from './not-downloaded-files/services/not-downloaded-files.service';
import { ProcessedHotsModule } from './processed-hots/processed-hots.module';
import { ReportsListComponent } from './reports/reports-list/reports-list.component';
import { ReportsComponent } from './reports/reports.component';
import { ReportsDisplayFilterFormatter } from './shared/reports-display-filter-formatter';
import { SpecialFilesFilterFormatter } from './special-files/services/special-files-filter-formatter';
import { SpecialFilesService } from './special-files/services/special-files.service';
import { SpecialFilesComponent } from './special-files/special-files.component';
import { UploadedFilesFilterFormatter } from './uploaded-files/services/uploaded-files-filter-formatter';
import { UploadedFilesService } from './uploaded-files/services/uploaded-files.service';
import { UploadedFilesComponent } from './uploaded-files/uploaded-files.component';
import { SharedModule } from '~app/shared/shared.module';
import * as fromFiles from '~app/files/reducers/';
import { FileUploadDialogComponent } from '~app/files/file-upload-dialog/file-upload-dialog.component';

@NgModule({
  declarations: [
    ReportsComponent,
    DeletedFilesComponent,
    ReportsListComponent,
    FileUploadDialogComponent,
    MassDeleteDialogComponent,
    DeletedFilesDialogComponent,
    AgentReportsComponent,
    DefaultFiltersDialogComponent,
    EvaluationFilesComponent,
    GlobalTaComponent,
    GlobalTaImageRequestDialogComponent,
    GlobalTaStatusDialogComponent,
    FopAgentStatusRequestDialogComponent,
    UploadedFilesComponent,
    DownloadedFilesComponent,
    NotDownloadedFilesComponent,
    SpecialFilesComponent,
    DownloadedIataCommunicationsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FilesRoutingModule,
    StoreModule.forFeature('files', fromFiles.reducers),
    EffectsModule.forFeature([AgentReportsEffects]),
    ProcessedHotsModule
  ],
  providers: [
    UploadFilesGuard,
    AgentReportsService,
    ReportsDisplayFilterFormatter,
    DeletedFilesService,
    DeletedFilesFilterFormatter,
    DeletedFilesDialogService,
    GlobalTaImageRequestDialogService,
    GlobalNonTaImageRequestDialogService,
    GlobalTaService,
    FopAgentStatusRequestGuard,
    FopAgentStatusService,
    FopAgentStatusRequestDialogService,
    UploadedFilesFilterFormatter,
    UploadedFilesService,
    DownloadedFilesFilterFormatter,
    DownloadedFilesService,
    NotDownloadedFilesFilterFormatter,
    NotDownloadedFilesService,
    SpecialFilesFilterFormatter,
    SpecialFilesService,
    DownloadedIataCommunicationsFilterFormatter,
    DownloadedIataCommunicationsService
  ]
})
export class FilesModule {}
