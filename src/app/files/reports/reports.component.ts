import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
@Component({
  selector: 'bspl-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  public tabs: RoutedMenuItem[];

  constructor(private activatedRoute: ActivatedRoute, private menuBuilder: MenuBuilder) {}

  ngOnInit() {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }
}
