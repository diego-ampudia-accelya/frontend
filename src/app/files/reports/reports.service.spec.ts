import { HttpClient } from '@angular/common/http';
import { discardPeriodicTasks, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { files } from '../mocks/files.mock';
import { FileStatus, FileType } from '../model';

import { ReportFile } from './reports-list.model';
import { ReportsService } from './reports.service';
import { AppConfigurationService } from '~app/shared/services';
import { DataQuery } from '~app/shared/components/list-view';
import { initialState } from '~app/files/mocks/files.test.mocks';

describe('ReportsService', () => {
  let service: ReportsService;
  let httpMock: SpyObject<HttpClient>;
  const selectedBsp = '6983';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ReportsService,
        provideMockStore({ initialState }),
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, {
          baseApiPath: 'base-url'
        })
      ]
    });

    service = TestBed.inject(ReportsService);
    httpMock = TestBed.inject<any>(HttpClient);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should load all mock files for user with defaultQuery + bsp and sectionType', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files.filter(file => file.status !== FileStatus.Archived)));
    const dataQuery: DataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        isArchive: false,
        bspId: selectedBsp
      }
    };
    const filesForIataUser = files.filter(
      file => file.isoCountryCode === selectedBsp && file.status !== FileStatus.Archived
    );

    tick(1000);

    service.find(dataQuery).subscribe(filesList => {
      expect(filesList.records).toEqual(filesForIataUser as ReportFile[]);
    });

    discardPeriodicTasks();
  }));

  it('should filter files by file type', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files.filter(file => file.status !== FileStatus.Archived)));
    const azFileType: FileType = {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    };
    const dataQuery: DataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        type: [azFileType],
        isArchive: false,
        bspId: selectedBsp
      }
    };
    let expectedFiles = files.filter(
      file => file.isoCountryCode === selectedBsp && file.status !== FileStatus.Archived
    );
    expectedFiles = expectedFiles.filter(file => file.type.descriptor === azFileType.descriptor);

    tick(1000);

    service.find(dataQuery).subscribe(filesList => {
      expect(filesList.records).toEqual(expectedFiles as ReportFile[]);
    });

    discardPeriodicTasks();
  }));

  it('should filter files by file name', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files.filter(file => file.status !== FileStatus.Archived)));
    const fileName = 'ESaz';
    const dataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        name: fileName,
        isArchive: false,
        bspId: selectedBsp
      }
    };
    let expectedFiles = files.filter(
      file => file.isoCountryCode === selectedBsp && file.status !== FileStatus.Archived
    );
    expectedFiles = expectedFiles.filter(file => file.name.toLowerCase().includes(fileName.toLowerCase()));

    tick(1000);

    service.find(dataQuery).subscribe(filesList => {
      expect(filesList.records.length).toEqual(expectedFiles.length);
    });
    discardPeriodicTasks();
  }));

  it('should change status of the file when archive method is called', () => {
    httpMock.put.and.returnValue(of({}));
    const url = 'base-url/file-management/files/1';

    service.archive('1');

    expect(httpMock.put).toHaveBeenCalledWith(url, { id: '1', status: FileStatus.Archived });
  });

  it('should change downloaded file status', fakeAsync(() => {
    const fileInfo: ReportFile = {
      id: '13',
      name: 'ESaz3441_20200107_20200107_Airline_Daily.txt',
      type: {
        descriptor: 'az',
        description: 'IBSPs Billing Output'
      },
      uploadDate: '2020-02-29',
      size: 856485,
      status: FileStatus.NotDownloaded,
      isoCountryCode: '6983',
      userType: 'airline'
    };
    const body: ArrayBuffer = new ArrayBuffer(1133825);
    const headers: Map<string, string> = new Map();
    headers.set('Content-Disposition', 'attachment;filename="ESxxALL_20200806_testing99999.txt"');
    const response = {
      headers: {
        get: key => headers.get(key)
      },
      status: 200,
      statusText: 'OK',
      url: 'test',
      ok: true,
      type: 4,
      body
    };
    service['data'] = [fileInfo];

    httpMock.get.and.returnValue(of(response));

    let fileDownloaded: any;
    service.download('13').subscribe(file => (fileDownloaded = file));
    tick();

    expect(fileDownloaded.fileName).toEqual('ESxxALL_20200806_testing99999.txt');
    expect(service['data'][0].status).toBe(FileStatus.Downloaded);
  }));
});
