import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { delay, map, tap } from 'rxjs/operators';

import { FileStatus } from '../model';
import { hasFileType, hasNameContaining, hasUploadDateInRange } from '../shared/filter.utils';
import { ReportFile, ReportFileId } from './reports-list.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ClientGridTableHelper, downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class ReportsService implements Queryable<ReportFile> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management`;
  private clientGridTableHelper = new ClientGridTableHelper<ReportFile>();
  // We need to store the data per component instance so we can modify it when downloading/archiving file.
  // This is needed in order not to send a new request each time a file is downloaded.
  private data: ReportFile[];

  public dataList$: Observable<Array<ReportFile>>;
  protected dataSubject = new ReplaySubject<Array<ReportFile>>(1);

  private customFilterFor = {
    type: hasFileType,
    name: hasNameContaining,
    uploadDate: hasUploadDateInRange
  };

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {
    this.dataList$ = this.dataSubject.asObservable();
  }

  public find(dataQuery: DataQuery): Observable<PagedData<ReportFile>> {
    const { isArchive, bspId, ...filterBy } = dataQuery.filterBy;
    dataQuery.sortBy = formatSortBy(dataQuery.sortBy, [{ from: 'type', to: 'type.description' }]);

    dataQuery.filterBy = {
      ...filterBy
    };

    const params = {
      ...(bspId !== undefined && { bspId: bspId.id }),
      ...(isArchive !== undefined && { archived: isArchive.toString() })
    };

    return this.getFiles(params).pipe(
      tap(result => {
        this.data = result;
        this.dataSubject.next(this.data);
      }),
      map(f => {
        // TODO: add BE filter model as generic type to `fromDataQuery` factory method
        const reqQuery = RequestQuery.fromDataQuery(dataQuery);

        return this.clientGridTableHelper.query(f, reqQuery, undefined, this.customFilterFor);
      })
    );
  }

  public archive(id: string) {
    const url = `${this.baseUrl}/files/${id}`;

    return this.http.put(url, { id, status: FileStatus.Archived }).pipe(
      tap(() => {
        const fileIndex = this.data.findIndex(f => f.id === id);
        this.data.splice(fileIndex, 1);
      })
    );
  }

  public archiveMultiple(filesId: ReportFileId[]): Observable<any> {
    const url = `${this.baseUrl}/archived-files`;

    return this.http.post(url, filesId);
  }

  public download(id: string): Observable<any> {
    const url = `${this.baseUrl}/files/${id}`;

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(
      map(formatDownloadResponse),
      tap(() => {
        const file = this.data.find(f => f.id === id);
        if (file.status === FileStatus.NotDownloaded) {
          file.status = FileStatus.Downloaded;
        }
      })
    );
  }

  /* 
    In this component We need the whole data first time user access to this component, 
    and then we'll filter depend on filter or sorting, but this filters is done it in local, doesn't in the API
  */

  private getFiles(params: { bspId: string; archived: string }): Observable<ReportFile[]> {
    const url = `${this.baseUrl}/files`;

    return this.http.get<ReportFile[]>(url, { params }).pipe(delay(300));
  }
}
