import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { of } from 'rxjs';

import { ReportFile } from '../reports-list.model';
import { ReportsService } from '../reports.service';
import { ReportsListComponent } from './reports-list.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { TranslatePipeMock } from '~app/test';

describe('ReportsListComponent', () => {
  let component: ReportsListComponent;
  let fixture: ComponentFixture<ReportsListComponent>;
  const translationMock = jasmine.createSpyObj<L10nTranslationService>('L10nTranslationService', ['translate']);
  const permissionServiceMock = jasmine.createSpyObj<PermissionsService>('PermissionsService', [
    'hasPermission',
    'hasUserType'
  ]);
  permissionServiceMock.hasPermission.and.returnValue(true);
  const appConfigServiceMock = createSpyObject(AppConfigurationService);
  const bytesFormatPipeMock = createSpyObject(BytesFormatPipe);
  const reportsServiceMock = createSpyObject(ReportsService);
  reportsServiceMock.find.and.returnValue(of());
  reportsServiceMock.download.and.returnValue(of());
  reportsServiceMock.archive.and.returnValue(of());
  reportsServiceMock.dataList$ = of([]);

  const file = {
    id: 'test-id',
    name: 'test-name'
  } as ReportFile;

  const activatedRouteStub = {
    snapshot: {
      routeConfig: {
        children: {}
      },
      data: {
        tableTitle: 'Inbox',
        type: 'inbox',
        tab: ROUTES.FILES
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ReportsListComponent, TranslatePipeMock, HasPermissionPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({ initialState }),
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        FormBuilder,
        NotificationService,
        { provide: L10nTranslationService, useValue: translationMock },
        { provide: PermissionsService, useValue: permissionServiceMock },
        { provide: AppConfigurationService, useValue: appConfigServiceMock },
        { provide: BytesFormatPipe, useValue: bytesFormatPipeMock },
        mockProvider(LocalBspTimeService),
        mockProvider(MessageService)
      ]
    })
      .overrideComponent(ReportsListComponent, {
        add: {
          providers: [{ provide: ReportsService, useValue: reportsServiceMock }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set title of the grid table for received files', () => {
    component.ngOnInit();
    expect(component.title).toEqual('Inbox');
  });

  it('should table actions for this sub-section', () => {
    component.ngOnInit();

    expect(component.tableActions).toEqual([
      {
        action: GridTableActionType.Download,
        permissions: [Permissions.readFile, Permissions.readCommunicationsFile]
      },
      {
        action: GridTableActionType.Archive,
        permissions: [Permissions.archiveFile]
      }
    ]);
  });

  it('should init columns for the table when component is loaded', () => {
    component.ngOnInit();

    const expectedColumns: any[] = [
      jasmine.objectContaining({
        prop: 'isRowSelected',
        name: '',
        maxWidth: 40
      }),
      jasmine.objectContaining({
        prop: 'isoCountryCode',
        name: 'MENU.files.reports.columns.bsp',
        width: 100
      }),
      jasmine.objectContaining({
        prop: 'name',
        name: 'MENU.files.reports.columns.fileName',
        resizeable: true,
        width: 500
      }),
      jasmine.objectContaining({
        prop: 'type',
        name: 'MENU.files.reports.columns.fileType',
        resizeable: true,
        width: 300
      }),
      jasmine.objectContaining({
        prop: 'uploadDate',
        name: 'MENU.files.reports.columns.uploadDate',
        resizeable: true,
        width: 100
      }),
      jasmine.objectContaining({
        prop: 'size',
        name: 'MENU.files.reports.columns.size',
        resizeable: true,
        width: 100
      }),
      jasmine.objectContaining({
        prop: 'status',
        name: 'MENU.files.reports.columns.status',
        resizeable: true,
        width: 150
      })
    ];

    expect(component.columns).toEqual(expectedColumns);
  });

  describe('saveFile', () => {
    it('should display loading indication while the operation is pending', fakeAsync(() => {
      const isLoadingValues = [];
      component.isLoading$.subscribe(isLoading => isLoadingValues.push(isLoading));

      component.saveFile(file);
      tick();

      expect(isLoadingValues).toEqual([false, true, false]);
    }));

    it('should use the file id to get the file from service', fakeAsync(() => {
      component.saveFile(file);
      tick();

      expect(reportsServiceMock.download).toHaveBeenCalledWith(file.id);
    }));

    // TODO: Review if it is necessary and fix it
    xit('should reload data', fakeAsync(() => {
      const loadDataSpy = spyOn<any>(component, 'reloadData').and.callThrough();
      component.saveFile(file);
      tick();

      expect(reportsServiceMock.download).toHaveBeenCalledWith(file.id);
      expect(loadDataSpy).toHaveBeenCalled();
    }));
  });

  describe('archiveFile', () => {
    it('should display loading indication while the operation is pending', fakeAsync(() => {
      const isLoadingValues = [];
      component.isLoading$.subscribe(isLoading => isLoadingValues.push(isLoading));

      component.archiveFile(file);
      tick();

      expect(isLoadingValues).toEqual([false, true, false]);
    }));

    it('should invoke the service with proper parameters', fakeAsync(() => {
      component.archiveFile(file);
      tick();

      expect(reportsServiceMock.archive).toHaveBeenCalledWith(file.id);
    }));

    // TODO: Review if it is necessary and fix it
    xit('should reload data', fakeAsync(() => {
      component.archiveFile(file);
      tick();
      expect(reportsServiceMock.archive).toHaveBeenCalledWith(file.id);
      expect(reportsServiceMock.find).toHaveBeenCalled();
    }));
  });
});
