import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { isEmpty } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { finalize, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { FileStatus, FileType } from '../../model';
import { ReportsDisplayFilterFormatter } from '../../shared/reports-display-filter-formatter';
import { ReportFile } from '../reports-list.model';
import { ReportsService } from '../reports.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { FileInfoFilter } from '~app/files/model/files.model';
import { SectionType } from '~app/files/model/section-type.enum';
import { FileTypesService } from '~app/files/shared/file-types.service';
import { AppState } from '~app/reducers';
import {
  DataQuery,
  DefaultQueryStorage,
  QUERYABLE,
  QueryableDataSource,
  QUERY_STORAGE_ID_SUFFIX
} from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { SortOrder } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

export function setSectionTypeSuffix(activatedRoute: ActivatedRoute) {
  return activatedRoute.snapshot.data.type;
}

@Component({
  selector: 'bspl-reports-list',
  templateUrl: './reports-list.component.html',
  styleUrls: ['./reports-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    ReportsDisplayFilterFormatter,
    DatePipe,
    ReportsService,
    { provide: QUERYABLE, useExisting: ReportsService },
    {
      provide: QUERY_STORAGE_ID_SUFFIX,
      deps: [ActivatedRoute],
      useFactory: setSectionTypeSuffix
    },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [ReportsService]
    }
  ]
})
export class ReportsListComponent implements OnInit, OnDestroy {
  @ViewChild('filesLinkCellTmpl', { static: true }) private filesLinkCellTmpl: TemplateRef<any>;

  public persistenceDataFiles: ReportFile[] = [];
  public predefinedFilters: Partial<FileInfoFilter>;
  public loggedUser: User;

  public title: string;
  public query = { filterBy: {}, paginateBy: {}, sortBy: [] };
  public tableActions = [];
  public columns: GridColumn[];
  public fileType = FileStatus;
  public searchForm: FormGroup;
  public bspControl: FormControl;
  public fileTypeListControl: FormControl;
  public statusOptions: DropdownOption[] = [
    {
      value: FileStatus.Downloaded,
      label: this.translationService.translate(`MENU.files.reports.filesStatus.${FileStatus.Downloaded}`)
    },
    {
      value: FileStatus.NotDownloaded,
      label: this.translationService.translate(`MENU.files.reports.filesStatus.${FileStatus.NotDownloaded}`)
    }
  ];
  public fileTypeOptions$: Observable<DropdownOption<FileType>[]>;
  public isArchive = false;
  public maxDate = this.localBspTimeService.getDate();
  public permissions = Permissions;
  public isActionPending$ = new BehaviorSubject<boolean>(false);
  public selectedRowsCount$: Observable<number>;
  public isLoading$ = combineLatest([this.dataSource.loading$, this.isActionPending$]).pipe(
    map(([isDataLoading, isDownloading]) => isDataLoading || isDownloading)
  );

  public isBspFilterLocked: boolean;
  public bspCountriesList: DropdownOption<BspDto>[];
  public bspSelected: BspDto;
  private filesSelected: ReportFile[];

  private type;
  private readPermissionsFile = [Permissions.readFile, Permissions.readCommunicationsFile];
  private archivePermissionsFile = [Permissions.archiveFile];
  private actionsByTab = {
    inbox: [
      {
        action: GridTableActionType.Download,
        permissions: this.readPermissionsFile
      },
      {
        action: GridTableActionType.Archive,
        permissions: this.archivePermissionsFile
      }
    ],
    archive: [
      {
        action: GridTableActionType.Download,
        permissions: this.readPermissionsFile
      }
    ]
  };

  private get checkboxColumn(): GridColumn {
    return {
      prop: 'isRowSelected',
      maxWidth: 40,
      sortable: false,
      name: '',
      headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl'
    };
  }

  private destroy$ = new Subject<any>();

  public loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  constructor(
    public dataSource: QueryableDataSource<ReportFile>,
    public formatFilter: ReportsDisplayFilterFormatter,
    private activatedRoute: ActivatedRoute,
    private queryStorage: DefaultQueryStorage,
    private reportsService: ReportsService,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private permissionsService: PermissionsService,
    private bytesFormatPipe: BytesFormatPipe,
    private localBspTimeService: LocalBspTimeService,
    private bspsDictionaryService: BspsDictionaryService,
    private fileTypeService: FileTypesService
  ) {}

  public ngOnInit(): void {
    this.title = this.activatedRoute.snapshot.data.tableTitle;
    this.type = this.activatedRoute.snapshot.data.type;

    this.searchForm = this.buildSearchForm();
    this.tableActions = this.getTableActions();
    this.bspControl = FormUtil.get<FileInfoFilter>(this.searchForm, 'bspId') as FormControl;
    this.fileTypeListControl = FormUtil.get<FileInfoFilter>(this.searchForm, 'type') as FormControl;
    this.initColumns();

    this.initializeLoggedUserAndQuery();
  }

  public initializeLoggedUserAndQuery() {
    this.loggedUser$()
      .pipe(
        tap(user => (this.loggedUser = user)),
        switchMap(_ => this.initializeLeanBspFilter$()),
        tap(() => this.initializeSearchQuery()),
        takeUntil(this.destroy$)
      )
      .subscribe(_ => {
        this.initializeChangeListener();
      });
  }

  public initializeChangeListener(): void {
    if (!this.isBspFilterLocked) {
      // If the BSP selector changes, we need filter data to load all fileTypes in FileType filter field on the BSPs selected
      this.bspControl.valueChanges
        .pipe(
          tap(bsp => (this.bspSelected = bsp)),
          takeUntil(this.destroy$)
        )
        .subscribe(() => {
          this.updateFileTypeList();
          this.fileTypeListControl.reset();
        });
    }
  }

  public loadData(query?: DataQuery): void {
    query = query || defaultQuery;
    this.isArchive = this.type !== SectionType.Inbox;

    if (isEmpty(query.sortBy)) {
      const sortBy = [{ attribute: 'uploadDate', sortType: SortOrder.Desc }];
      query = { ...query, sortBy };
    }

    const dataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy,
        isArchive: this.isArchive
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public executeAction({ action, row }: { event: Event; action: GridTableAction; row: ReportFile }) {
    if (action.actionType === GridTableActionType.Archive) {
      this.archiveFile(row);
    }

    if (action.actionType === GridTableActionType.Download) {
      this.saveFile(row);
    }
  }

  public saveFile(file: ReportFile) {
    const { id, name } = file;
    this.applyLoadingIndication(this.reportsService.download(id)).subscribe(fileInfo => {
      FileSaver.saveAs(fileInfo.blob, name);
      this.reloadData();
    });
  }

  public archiveFile(file: ReportFile) {
    this.applyLoadingIndication(this.reportsService.archive(file.id)).subscribe(() => this.reloadData());
  }

  public onMultipleArchiveClick() {
    const filesId = this.filesSelected.map(file => ({ id: file.id }));

    this.reportsService.archiveMultiple(filesId).subscribe(() => {
      this.clearSelectedRows();
      this.notificationService.showSuccess(
        this.translationService.translate('MENU.files.reports.multipleArchive.successMessage', {
          numberOfFiles: this.filesSelected.length
        })
      );
    });
  }

  public onRowsSelectionChange(files: ReportFile[]) {
    this.filesSelected = files.filter(file => file.isRowSelected);
    this.selectedRowsCount$ = of(this.filesSelected.length);
  }

  public clearSelectedRows() {
    this.selectedRowsCount$ = of(null);
    this.reloadData();
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.updateFileTypeList();
    }
  }

  private initializeSearchQuery(): void {
    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  private updateFileTypeList(): void {
    if (this.bspSelected) {
      this.fileTypeOptions$ = this.fileTypeService.getFileTypeOption(this.bspSelected.id.toString());
    }
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, this.readPermissionsFile)
      .pipe(
        tap(bspList => {
          if (bspList.length) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);
            const firstListBsp = bspList[0];
            const filterValue = bspList.length === 1 ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelected = filterValue;

            this.predefinedFilters = {
              ...this.predefinedFilters,
              bspId: filterValue
            };
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspList => (this.isBspFilterLocked = bspList.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  private reloadData(): void {
    this.loadData(this.queryStorage.get());
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'isoCountryCode',
        name: 'MENU.files.reports.columns.bsp',
        width: 100
      },
      {
        prop: 'name',
        name: 'MENU.files.reports.columns.fileName',
        cellTemplate: this.filesLinkCellTmpl,
        resizeable: true,
        width: 500
      },
      {
        prop: 'type',
        name: 'MENU.files.reports.columns.fileType',
        resizeable: true,
        pipe: {
          transform: value => value.description
        },
        width: 300
      },
      {
        prop: 'uploadDate',
        name: 'MENU.files.reports.columns.uploadDate',
        resizeable: true,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy')
        },
        width: 100
      },
      {
        prop: 'size',
        name: 'MENU.files.reports.columns.size',
        resizeable: true,
        pipe: {
          transform: value => this.bytesFormatPipe.transform(value)
        },
        width: 100
      },
      {
        prop: 'status',
        name: 'MENU.files.reports.columns.status',
        pipe: {
          transform: value => this.translationService.translate(`MENU.files.reports.filesStatus.${value}`)
        },
        resizeable: true,
        width: 150
      }
    ];

    if (this.type === SectionType.Inbox && this.permissionsService.hasPermission(this.archivePermissionsFile)) {
      this.columns = [this.checkboxColumn, ...this.columns];
    }
  }

  private getTableActions(): Array<any> {
    return this.actionsByTab[this.type].filter(gridAction =>
      this.permissionsService.hasPermission(gridAction.permissions)
    );
  }

  private applyLoadingIndication<T>(source: Observable<T>): Observable<T> {
    this.isActionPending$.next(true);

    return source.pipe(finalize(() => this.isActionPending$.next(false)));
  }

  private buildSearchForm(): FormGroup {
    return this.formBuilder.group({
      bspId: [],
      name: [''],
      type: [],
      uploadDate: '',
      status: []
    });
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
