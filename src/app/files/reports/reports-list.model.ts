import { FileInfo } from '../model';

export interface ReportFile extends FileInfo {
  isRowSelected?: boolean;
}

export interface ReportFileId {
  id: string;
}
