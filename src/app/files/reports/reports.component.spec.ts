import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { ReportsComponent } from './reports.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { MenuBuilder } from '~app/master-data/configuration';
import { ROUTES } from '~app/shared/constants/routes';
import { TranslatePipeMock } from '~app/test';

const tab = {
  allowedUserTypes: ['iata', 'agent', 'airline'],
  route: '/files/reports',
  title: 'menu.files.reports.tabLabel',
  active: true,
  url: './reports'
};

const permissionsService = createSpyObject(PermissionsService);

class MenuBuilderMock {
  buildMenuItemsFrom(route) {
    return [tab];
  }
}

describe('ReportsComponent', () => {
  let component: ReportsComponent;
  let fixture: ComponentFixture<ReportsComponent>;
  const translationServiceSpy = jasmine.createSpyObj('L10nTranslationService', ['translate']);

  const activatedRouteStub = {
    snapshot: {
      data: {
        tableTitle: 'Inbox',
        type: 'inbox',
        tab: ROUTES.REPORTS
      }
    }
  };

  let routerMock;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/archive', './archive')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './archive'
    };

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ReportsComponent, TranslatePipeMock, HasPermissionPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: PermissionsService, useValue: permissionsService },
        provideMockStore({ initialState }),
        { provide: MenuBuilder, useClass: MenuBuilderMock },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: Router, useValue: routerMock }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
