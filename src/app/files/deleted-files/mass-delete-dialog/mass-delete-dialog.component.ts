import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { MassDeleteDialogConfig, UploadDialogAction } from '~app/files/model';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { FileValidator } from '~app/shared/components/upload/file-validators';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { AlertMessageType } from '~app/shared/enums';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-mass-delete-dialog',
  templateUrl: './mass-delete-dialog.component.html',
  styleUrls: ['./mass-delete-dialog.component.scss']
})
export class MassDeleteDialogComponent implements OnInit, OnDestroy {
  @ViewChild(UploadComponent) uploadComponentRef: UploadComponent;
  public canAddFiles = true;
  public files: File[] = [];
  public validationRules: FileValidator[] = [];
  public mode = UploadModes.Initial;
  public secondaryTittle: string;
  public uploadModes = UploadModes;
  public alertMessageType = AlertMessageType;

  private actionEmitter: Subject<any>;
  private filesTotal: number;
  private atLeastOneFileIsUploaded: boolean;
  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig<MassDeleteDialogConfig>,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.onActionExecuted().subscribe();
    const config = this.config.data;
    this.files = config.files;
    this.validationRules = config.validationRules ?? [];
    this.secondaryTittle = config.secondaryTittle;
    this.actionEmitter = config.actionEmitter;
  }

  public addFiles(files: File[]) {
    if (this.dialogService.isDialogOpen()) {
      const fileUploadDialogRef = this.dialogService.getDynamicInstance();
      if (fileUploadDialogRef instanceof MassDeleteDialogComponent) {
        fileUploadDialogRef.files = files;
      }
    }
  }

  public onUploadStarted(): void {
    this.canAddFiles = false;
    this.config.data.isClosable = false;
    this.setButtonLoading(FooterButton.Done, true);
  }

  public onUploadFinished(): void {
    this.config.data.isClosable = true;
    this.setButtonLoading(FooterButton.Done, false);
    this.clearFiles();
  }

  public onResetInitialMode() {
    this.config.data.buttons = [
      {
        title: this.translationService.translate('MENU.files.modalCancel'),
        type: FooterButton.Cancel,
        buttonDesign: ButtonDesign.Tertiary
      },
      {
        title: this.translationService.translate('MENU.files.deletedFiles.massDelete.buttonLabel'),
        isDisabled: true,
        type: FooterButton.MassDelete,
        buttonDesign: ButtonDesign.Primary
      }
    ];

    this.reactiveSubject.emit({
      reset: UploadDialogAction.Reset
    });

    this.canAddFiles = true;
  }

  public onFilesChange(files: FileUpload[]) {
    this.config.data.files = files;
    this.setButtonDisabled(!this.isValid());
  }

  public onUploadFinishedResult(result) {
    let total = 0;

    if (Array.isArray(result)) {
      result.forEach(res => {
        total += res.responseData ? res.responseData.length : 0;
      });
    }
    this.filesTotal = total;
  }

  private onActionExecuted() {
    return this.reactiveSubject.asObservable.pipe(
      takeUntil(this.destroy$),
      tap(action => {
        if (action.clickedBtn === FooterButton.Cancel) {
          this.clearFilesAndCloseDialog();
        }

        if (action.clickedBtn === FooterButton.Done) {
          this.clearFilesAndCloseDialog();
          if (this.atLeastOneFileIsUploaded) {
            this.actionEmitter.next();
            this.showSuccessNotification();
          }
        }

        if (action.clickedBtn === FooterButton.MassDelete) {
          this.config.data.buttons = [
            {
              title: this.translationService.translate('MENU.files.modalDone'),
              isDisabled: false,
              type: FooterButton.Done,
              buttonDesign: ButtonDesign.Tertiary
            }
          ];

          this.uploadComponentRef.uploadFiles().subscribe(res => {
            this.atLeastOneFileIsUploaded = res;
            if (!this.atLeastOneFileIsUploaded) {
              this.showErrorNotification();
            }
          });
        }
      })
    );
  }

  private clearFilesAndCloseDialog() {
    this.clearFiles();
    this.dialogService.close();
  }

  private showErrorNotification() {
    this.notificationService.showError(
      this.translationService.translate('MENU.files.deletedFiles.itemsToDeleteErrorMessage')
    );
  }

  private showSuccessNotification() {
    const message = this.translationService.translate('MENU.files.deletedFiles.itemsToDeleteSuccessMessage', {
      items: this.filesTotal
    });
    this.notificationService.showSuccess(message);
  }

  private clearFiles() {
    this.config.data.files = [];
  }

  private setButtonDisabled(isDisabled: boolean) {
    const buttonType = FooterButton.MassDelete;
    const actionButton = this.getButton(buttonType);
    if (actionButton) {
      actionButton.isDisabled = isDisabled;
    }
  }

  private setButtonLoading(buttonType: FooterButton, isLoading: boolean) {
    const actionButton = this.getButton(buttonType);
    if (actionButton) {
      actionButton.isLoading = isLoading;
    }
  }

  private getButton(buttonType: FooterButton): any {
    return this.config.data.buttons.find(button => button.type === buttonType);
  }

  private isValid(): boolean {
    return this.config.data.files?.some((file: FileUpload) => file.isValid);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
