import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEqual } from 'lodash';

import { MassDeleteDialogComponent } from './mass-delete-dialog.component';
import { MassDeleteDialogConfig } from '~app/files/model/mass-delete-dialog-config';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('MassDeleteDialogComponent', () => {
  let fixture: ComponentFixture<MassDeleteDialogComponent>;
  let component: MassDeleteDialogComponent;

  const mockConfig: DialogConfig<MassDeleteDialogConfig> = {
    data: {
      title: 'title',
      bsp: {
        id: 5
      },
      uploadingTitle: 'Uploading',
      uploadedTitle: 'Uploaded',
      footerButtonsType: FooterButton.Search,
      files: [new File([''], 'filename', { type: 'text/json' })]
    }
  } as DialogConfig;

  const mockedData = {
    isClosable: true,
    buttons: [
      {
        title: 'Done',
        isDisabled: false,
        type: FooterButton.Done,
        buttonDesign: ButtonDesign.Tertiary
      }
    ]
  };

  const dialogServiceSpy = createSpyObject(DialogService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MassDeleteDialogComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        ReactiveSubject,
        { provide: DialogConfig, useValue: cloneDeep(mockConfig) },
        { provide: DialogService, useValue: dialogServiceSpy },
        mockProvider(L10nTranslationService),
        mockProvider(NotificationService)
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MassDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.config.data.buttons = [
      {
        isDisabled: true,
        type: FooterButton.MassDelete,
        buttonDesign: ButtonDesign.Tertiary
      } as ModalAction
    ];
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should set files to component, when config data is not empty', () => {
    expect(isEqual(component.files, mockConfig.data.files)).toBe(true);
  });

  it('should call specific methods, when new files are added', () => {
    const dynamicInstance = createSpyObject(DialogService);
    dialogServiceSpy.isDialogOpen.and.returnValue(true);
    dialogServiceSpy.getDynamicInstance.and.returnValue(dynamicInstance);

    const filesMock = [new File([''], 'mockedFile', { type: 'text/json' })];
    component.addFiles(filesMock);

    expect(dialogServiceSpy.getDynamicInstance).toHaveBeenCalled();
    expect(dialogServiceSpy.isDialogOpen).toHaveBeenCalled();
  });

  it('should show loading state of Done button and block closing the dialog when upload is started', () => {
    component.config.data = { ...component.config.data, ...cloneDeep(mockedData) };
    component.onUploadStarted();

    const instanceData = component.config.data;
    const doneButton = instanceData.buttons.find(b => b.type === FooterButton.Done);

    expect(doneButton.isLoading).toBe(true);
    expect(instanceData.isClosable).toBe(false);
  });

  it('should hide loading state of Done button and enable closing the dialog, when upload is finished', () => {
    const configData = cloneDeep(mockedData);
    configData.isClosable = false;

    component.config.data = {
      ...component.config.data,
      ...configData,
      files: [new File([''], 'filename', { type: 'text/json' })]
    };
    component.onUploadFinished();

    const instanceData = component.config.data;
    const doneButton = instanceData.buttons.find(b => b.type === FooterButton.Done);

    expect(doneButton.isLoading).toBe(false);
    expect(instanceData.isClosable).toBe(true);
    expect(component.config.data.files).toEqual([]);
  });

  it('should reset buttons, when onResetInitialMode is called.', () => {
    component.ngOnInit();
    const buttonsAfterReset = [
      {
        type: FooterButton.Cancel,
        buttonDesign: ButtonDesign.Tertiary
      },
      {
        isDisabled: true,
        type: FooterButton.Upload,
        buttonDesign: ButtonDesign.Primary
      }
    ];

    component.config.data.buttons = [
      {
        type: FooterButton.Done,
        isDisabled: false,
        buttonDesign: ButtonDesign.Primary
      }
    ];

    component.onResetInitialMode();

    expect(isEqual(component.config.data.buttons[0].type, buttonsAfterReset[0].type)).toBe(true);
  });

  it('should enable Mass Delete button, when some file is valid.', () => {
    const files = [{ isValid: true }, { isValid: false }] as FileUpload[];
    component.config.data.buttons = [
      {
        title: 'Mass Delete',
        type: FooterButton.MassDelete,
        isDisabled: true
      }
    ];

    component.onFilesChange(files);
    const uploadButton = component.config.data.buttons.find(b => b.type === FooterButton.MassDelete);
    expect(uploadButton.isDisabled).toBe(false);
  });

  it('should disable Mass Delete button, when all files are invalid.', () => {
    const files = [{ isValid: false }, { isValid: false }] as FileUpload[];
    component.config.data.buttons = [
      {
        title: 'Mass Delete',
        type: FooterButton.MassDelete,
        isDisabled: true
      }
    ];

    component.onFilesChange(files);
    const massDeleteButton = component.config.data.buttons.find(b => b.type === FooterButton.MassDelete);
    expect(massDeleteButton.isDisabled).toBe(true);
  });
});
