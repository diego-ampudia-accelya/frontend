import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of, Subject } from 'rxjs';
import { catchError, filter, finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { DeletedFilesService } from '../services/deleted-files.service';
import { DeletedFilesDialogData } from '~app/files/model';
import { DeletedFileByName } from '~app/files/model/deleted-files.model';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-deleted-files-dialog',
  templateUrl: './deleted-files-dialog.component.html',
  styleUrls: ['./deleted-files-dialog.component.scss']
})
export class DeletedFilesDialogComponent implements OnInit, OnDestroy {
  public form: FormArray;
  public isDeleting = false;
  public btnDesign = ButtonDesign;
  private actionEmitter: Subject<any>;

  public messageKey = {
    tooltip: this.translationService.translate('MENU.files.deletedFiles.dialog.tooltipMessage'),
    emptyString: ''
  };

  private formUtil: FormUtil;
  private destroy$ = new Subject();

  constructor(
    private translationService: L10nTranslationService,
    private fb: FormBuilder,
    public reactiveSubject: ReactiveSubject,
    private deletedFilesService: DeletedFilesService,
    public config: DialogConfig,
    private dialogService: DialogService,
    private notificationService: NotificationService
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public get dialogData(): DeletedFilesDialogData {
    return this.config.data as DeletedFilesDialogData;
  }

  private get initialFormGroup() {
    return this.formUtil.createGroup<DeletedFileByName>({
      name: ['', [Validators.required]]
    });
  }

  public ngOnInit(): void {
    this.buildForm();
    this.formStatusOnChange().subscribe();
    this.getDeleteFilesModalObservable().subscribe();
    this.actionEmitter = this.dialogData.actionEmitter;
  }

  public setIsDeleting(state: boolean) {
    this.isDeleting = state;
    const searchButton = this.config.data.buttons.find(button => button.type === FooterButton.DeleteFile);
    searchButton.isLoading = state;
  }

  public addFileToDeleteLine() {
    if (this.form.valid) {
      this.form.push(this.initialFormGroup);
    }
  }

  public removeTaxRows(...indexes: Array<number>) {
    if (this.form.length > 1) {
      while (indexes.length > 0 && this.form.length > 1) {
        this.form.removeAt(indexes.pop());
      }
    } else if (indexes.length === 1) {
      this.form.reset({ emitEvent: false });
    }
  }

  private buildForm(): void {
    this.form = this.fb.array([this.initialFormGroup], {
      updateOn: 'blur'
    });
  }

  private formStatusOnChange(): Observable<any> {
    return this.form.statusChanges.pipe(
      tap(() => {
        const deleteButton = this.getFormButton(FooterButton.DeleteFile);
        if (deleteButton) {
          deleteButton.isDisabled = this.form.invalid;
          deleteButton.tooltipText = this.form.invalid ? this.messageKey.tooltip : this.messageKey.emptyString;
        }
      }),
      takeUntil(this.destroy$)
    );
  }

  private getFormButton(name: string): ModalAction {
    return this.dialogData.buttons.find(button => button.type === name);
  }

  private getDeleteFilesModalObservable(): Observable<any> {
    return this.reactiveSubject.asObservable.pipe(
      switchMap(action => {
        let deleteFilesResult$ = of(null);

        if (action.clickedBtn === FooterButton.DeleteFile) {
          deleteFilesResult$ = this.delete();
        }

        return deleteFilesResult$;
      }),
      filter(result => !!result),
      tap(result => {
        if (result.error) {
          this.setControlsErrors(result.error.messages);
        }
      }),
      takeUntil(this.destroy$)
    );
  }

  private delete(): Observable<string> {
    this.setIsDeleting(true);
    const fileToPost = this.form.controls.map(control => control.value);

    return this.deletedFilesService.save(fileToPost).pipe(
      tap(() => {
        this.showSuccessNotification(this.form.controls.length);
        this.dialogService.close();
        this.actionEmitter.next();
      }),
      catchError(error => of(error)),
      finalize(() => this.setIsDeleting(false))
    );
  }

  private showSuccessNotification(itemsToDeleteTotal: number) {
    const message = this.translationService.translate('MENU.files.deletedFiles.itemsToDeleteSuccessMessage', {
      items: itemsToDeleteTotal
    });
    this.notificationService.showSuccess(message);
  }

  private setControlsErrors(errorMessage) {
    const indexOfErrors = this.getIndexOfErrorFile(errorMessage);

    indexOfErrors.forEach(ind => {
      this.form.controls[ind].get('name').setErrors({ file_wrong_name: true });
    });
  }

  private getIndexOfErrorFile(errorMessage): number[] {
    const result = [];
    errorMessage.forEach(messageObj => {
      const { messageParams } = messageObj;
      result.push(
        messageParams.map(param => {
          const value = param.value.substring(1, param.value.indexOf(']'));

          return parseInt(value, 10);
        })
      );
    });

    return result;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
