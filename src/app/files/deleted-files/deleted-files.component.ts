import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEmpty } from 'lodash';
import moment from 'moment-mini';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { DeletedFile, DeletedFilesFilter, DeletedFilesStatus } from '../model';
import { DeletedFilesDialogService } from './services/deleted-files-dialog.service';
import { DeletedFilesFilterFormatter } from './services/deleted-files-filter-formatter';
import { DeletedFilesService } from './services/deleted-files.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { SortOrder } from '~app/shared/enums';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil, toValueLabelObject } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';

@Component({
  selector: 'bspl-deleted-files',
  templateUrl: './deleted-files.component.html',
  styleUrls: ['./deleted-files.component.scss'],
  providers: [DefaultQueryStorage, QueryableDataSource, { provide: QUERYABLE, useExisting: DeletedFilesService }]
})
export class DeletedFilesComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public ButtonDesign = ButtonDesign;

  public columns: Array<GridColumn>;
  public statusOptions: DropdownOption[] = [];
  public totalItemsMessage: string;
  public hasCreateDeletedFilesPermission = false;
  private destroy$ = new Subject();
  private formFactory: FormUtil;
  protected dialogQueryFormSubmitEmitter = new Subject<any>();

  constructor(
    public displayFormatter: DeletedFilesFilterFormatter,
    public dataSource: QueryableDataSource<DeletedFile>,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private queryStorage: DefaultQueryStorage,
    private permissionsService: PermissionsService,
    private deletedFilesDialogService: DeletedFilesDialogService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.columns = this.buildColumns();

    this.hasCreateDeletedFilesPermission = this.permissionsService.hasPermission(Permissions.createDeletedFiles);

    this.searchForm = this.buildForm();
    this.initializeStatusDropdown();

    const storedQuery = this.queryStorage.get();
    this.checkPermissionAndLoadData(storedQuery);

    this.initializeTotalItems();

    this.dialogQueryFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.checkPermissionAndLoadData());
  }

  private checkPermissionAndLoadData(storedQuery?: DataQuery) {
    const hasReadDeleteFilesPermissions = this.permissionsService.hasPermission(Permissions.readDeletedFiles);
    if (hasReadDeleteFilesPermissions) {
      this.loadData(storedQuery);
    }
  }

  public loadData(query?: DataQuery): void {
    query = query || cloneDeep(defaultQuery);
    let dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    if (isEmpty(dataQuery.sortBy)) {
      const attribute: keyof DeletedFile = 'actionDate';
      const sortBy = [{ attribute, sortType: SortOrder.Desc }];
      dataQuery = { ...dataQuery, sortBy };
    }

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public openDeleteFileDialog() {
    this.deletedFilesDialogService.openDeleteFileDialog(this.dialogQueryFormSubmitEmitter);
  }

  public openMassDeleteFileDialog() {
    this.deletedFilesDialogService.openMassDeleteFileDialog(this.dialogQueryFormSubmitEmitter);
  }

  private initializeStatusDropdown() {
    this.statusOptions = Object.values(DeletedFilesStatus)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.getTranslatedStatus(label)
      }));
  }

  private getTranslatedStatus(status: DeletedFilesStatus): string {
    return this.translationService.translate(`MENU.files.deletedFiles.status.${status}`);
  }

  private initializeTotalItems() {
    this.dataSource.appliedQuery$
      .pipe(
        takeUntil(this.destroy$),
        map(query => query.paginateBy.totalElements)
      )
      .subscribe(total => {
        this.totalItemsMessage = this.translationService.translate('MENU.files.deletedFiles.totalItemsMessage', {
          total
        });
      });
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<DeletedFilesFilter>({
      name: [],
      actionDate: [],
      user: [],
      email: [],
      status: []
    });
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'name',
        name: 'MENU.files.deletedFiles.columns.fileName',
        flexGrow: 4
      },
      {
        prop: 'user',
        name: 'MENU.files.deletedFiles.columns.user',
        flexGrow: 2
      },
      {
        prop: 'email',
        name: 'MENU.files.deletedFiles.columns.email',
        flexGrow: 4
      },
      {
        prop: 'actionDate',
        name: 'MENU.files.deletedFiles.columns.actionDate',
        pipe: { transform: date => moment(date).format('DD/MM/YYYY HH:mm:SS') },
        flexGrow: 1
      },
      {
        prop: 'status',
        name: 'MENU.files.deletedFiles.columns.status',
        pipe: { transform: status => this.getTranslatedStatus(status) },
        cellTemplate: 'badgeInfoCellTmpl',
        badgeInfo: {
          hidden: (value: DeletedFile) => !value,
          type: (value: DeletedFile) => this.getBadgeInfoType(value.status),
          tooltipLabel: (value: DeletedFile) => this.getBadgeInfoTooltip(value.status),
          showIconType: true
        },
        flexGrow: 1
      }
    ];
  }

  private getBadgeInfoType(status: DeletedFilesStatus): BadgeInfoType {
    let type: BadgeInfoType;

    switch (status) {
      case DeletedFilesStatus.InProcess:
        type = BadgeInfoType.info;
        break;
      case DeletedFilesStatus.Deleted:
        type = BadgeInfoType.success;
        break;
      case DeletedFilesStatus.NotDeleted:
      default:
        type = BadgeInfoType.regular;
        break;
    }

    return type;
  }

  private getBadgeInfoTooltip(status: DeletedFilesStatus): string {
    return `${this.translationService.translate('MENU.files.deletedFiles.columns.status')}: ${this.getTranslatedStatus(
      status
    )}`;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
