import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';

import { DeletedFilesDialogComponent } from '../deleted-files-dialog/deleted-files-dialog.component';
import { MassDeleteDialogComponent } from '../mass-delete-dialog/mass-delete-dialog.component';
import { DeletedFilesDialogData, MassDeleteDialogConfig } from '~app/files/model';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { FileValidators } from '~app/shared/components/upload/file-validators';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class DeletedFilesDialogService {
  constructor(
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private appConfig: AppConfigurationService
  ) {}

  public openDeleteFileDialog(actionEmitter: Subject<any>): Observable<any> {
    const primaryButtonTitle = this.translationService.translate('MENU.files.deletedFiles.button.label');
    const dialogTitle = this.translationService.translate('MENU.files.deletedFiles.dialog.title');
    const dialogConfig: DialogConfig = {
      data: {
        title: dialogTitle,
        isDeleting: false,
        files: [],
        actionEmitter,
        mainButtonType: FooterButton.DeleteFile,
        footerButtonsType: [
          {
            type: FooterButton.DeleteFile,
            isDisabled: true,
            title: primaryButtonTitle,
            buttonDesign: ButtonDesign.Primary
          }
        ]
      } as DeletedFilesDialogData
    };

    return this.dialogService.open(DeletedFilesDialogComponent, dialogConfig);
  }

  public openMassDeleteFileDialog(actionEmitter: Subject<any>): Observable<any> {
    const title = this.translationService.translate('MENU.files.deletedFiles.massDelete.dialog.title');
    const secondaryTittle = this.translationService.translate(
      'MENU.files.deletedFiles.massDelete.dialog.secondaryTittle'
    );
    const uploadingTitle = this.translationService.translate('MENU.files.uploadFiles.uploading');
    const uploadedTitle = this.translationService.translate('MENU.files.uploadFiles.uploaded');
    const dialogConfig: DialogConfig = {
      data: {
        title,
        actionEmitter,
        footerButtonsType: [{ type: FooterButton.MassDelete, isDisabled: true }],
        uploadingTitle,
        uploadedTitle,
        secondaryTittle,
        uploadPath: `${this.appConfig.baseUploadPath}/file-management/deleted-files/upload`,
        validationRules: [FileValidators.maxFileSize()]
      } as MassDeleteDialogConfig
    };

    return this.dialogService.open(MassDeleteDialogComponent, dialogConfig);
  }
}
