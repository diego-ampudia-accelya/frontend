import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { DeletedFilesFilter, DeletedFilesStatus } from '~app/files/model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class DeletedFilesFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: DeletedFilesFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<DeletedFilesFilter>> = {
      name: name => `${this.translate('name')} - ${name}`,
      actionDate: actionDate => `${this.translate('actionDate')} - ${rangeDateFilterTagMapper(actionDate)}`,
      user: user => `${this.translate('user')} - ${user}`,
      email: email => `${this.translate('email')} - ${email}`,
      status: status => `${this.translate('status')} - ${this.statusMapper(status)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate('MENU.files.deletedFiles.filters.' + key);
  }

  private statusMapper(statuses: DeletedFilesStatus[]): string {
    return statuses.map(status => this.translation.translate(`MENU.files.deletedFiles.status.${status}`)).join(', ');
  }
}
