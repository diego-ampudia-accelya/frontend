import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DeletedFile, DeletedFilesFilter } from '~app/files/model';
import { DeletedFileByName } from '~app/files/model/deleted-files.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable()
export class DeletedFilesService implements Queryable<DeletedFile> {
  private baseUrl: string;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {
    this.baseUrl = `${this.appConfiguration.baseApiPath}/file-management/deleted-files`;
  }

  public find(query?: DataQuery<DeletedFilesFilter>): Observable<PagedData<DeletedFile>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<DeletedFile>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: DataQuery<DeletedFilesFilter>): RequestQuery {
    const { actionDate, ...filterBy } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromActionDate: actionDate && toShortIsoDate(actionDate[0]),
        toActionDate: actionDate && toShortIsoDate(actionDate[1] ? actionDate[1] : actionDate[0])
      }
    });
  }

  public save(files: DeletedFileByName[]) {
    const url = `${this.baseUrl}`;

    return this.http.post<DeletedFile[]>(url, files);
  }
}
