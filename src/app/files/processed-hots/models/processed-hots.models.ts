import { Observable } from 'rxjs';

import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export interface ProcessedHotsEntryFilters {
  name: string;
  endProcessingTime: Date[];
}

export interface LastProcessedDateAndPeriod {
  lastDateOfIssue: string;
  period: string;
}

export interface ProcessedHotsEntry extends LastProcessedDateAndPeriod {
  data: PagedData<ProcessedHotsRecord>;
}

export interface ProcessedHotsRecord {
  name: string;
  endProcessingTime: string;
}

export interface PeriodQueryable<T> {
  find(query: DataQuery): Observable<ProcessedHotsEntry>;
}
