import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProcessedHotsComponent } from './processed-hots.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: ProcessedHotsComponent,
    data: {
      tab: ROUTES.PROCESSED_HOTS
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessedHotsRoutingModule {}
