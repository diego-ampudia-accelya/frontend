import { createFeatureSelector, createSelector } from '@ngrx/store';

import { ProcessedHotsState, processedHotsStateFeatureKey } from '../store/processed-hots.state';

export const selectProcessedHotsStateFeature = createFeatureSelector<ProcessedHotsState>(processedHotsStateFeatureKey);

export const selectProcessedHots = createSelector(selectProcessedHotsStateFeature, state => state.data);
export const selectProcessedHotsQuery = createSelector(selectProcessedHotsStateFeature, state => state.query);
export const selectSearchProcessedHotsLoading = createSelector(
  selectProcessedHotsStateFeature,
  state => state.loading.searchProcessedHots
);
export const selectLastDateAvailable = createSelector(
  selectProcessedHotsStateFeature,
  state => state.lastDateAndPeriod
);

export const selectHasData = createSelector(selectProcessedHotsStateFeature, state =>
  Boolean(state.data && state.data.length)
);
