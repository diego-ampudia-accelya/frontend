import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/processed-hots.actions';
import { initialState } from '../store/processed-hots.state';

export const processedHotsReducer = createReducer(
  initialState,
  on(fromActions.searchProcessedHotsRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchProcessedHots: true
    },
    error: {
      ...state.error,
      searchProcessedHots: null
    }
  })),
  on(fromActions.searchProcessedHotsFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchProcessedHots: false
    },
    error: {
      ...state.error,
      searchProcessedHots: error
    }
  })),
  on(fromActions.searchProcessedHotsSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.data.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.data.pageNumber,
        size: pagedData.data.pageSize,
        totalElements: pagedData.data.total
      }
    },
    lastDateAndPeriod: {
      lastDateOfIssue: pagedData.lastDateOfIssue,
      period: pagedData.period
    },
    loading: {
      ...state.loading,
      searchProcessedHots: false
    },
    error: {
      ...state.error,
      searchProcessedHots: null
    }
  }))
);
