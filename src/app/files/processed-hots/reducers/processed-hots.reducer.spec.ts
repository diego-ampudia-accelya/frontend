import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/processed-hots.actions';
import * as fromState from '../store/processed-hots.state';
import * as fromStub from '../stubs/processed-hots.stubs';

import * as fromReducer from './processed-hots.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('processedHotsReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.processedHotsReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchProcessedHotsRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchProcessedHotsRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.processedHotsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchProcessedHots: true
        },
        error: {
          ...fromState.initialState.error,
          searchProcessedHots: null
        }
      });
    });
  });

  describe('searchProcessedHotsFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchProcessedHotsFailure({ payload: { error } });
      const result = fromReducer.processedHotsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchProcessedHots: false
        },
        error: {
          ...fromState.initialState.error,
          searchProcessedHots: error
        }
      });
    });
  });

  describe('searchProcessedHotsSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchProcessedHotsSuccess({
        payload: { pagedData: fromStub.processedHotsPagedData }
      });
      const result = fromReducer.processedHotsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.processedHotsPagedData.data.records,
        lastDateAndPeriod: {
          lastDateOfIssue: fromStub.processedHotsPagedData.lastDateOfIssue,
          period: fromStub.processedHotsPagedData.period
        },
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.processedHotsPagedData.data.pageNumber,
            size: fromStub.processedHotsPagedData.data.pageSize,
            totalElements: fromStub.processedHotsPagedData.data.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchProcessedHots: false
        },
        error: {
          ...fromState.initialState.error,
          searchProcessedHots: null
        }
      });
    });
  });
});
