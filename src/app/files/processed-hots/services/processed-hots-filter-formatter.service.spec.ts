import { inject, TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { ProcessedHotsFilterFormatterService } from './processed-hots-filter-formatter.service';

describe('ProcessedHotsFilterFormatterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProcessedHotsFilterFormatterService,
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        }
      ]
    });
  });

  it('should be created', inject(
    [ProcessedHotsFilterFormatterService],
    (service: ProcessedHotsFilterFormatterService) => {
      expect(service).toBeTruthy();
    }
  ));
});
