import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { clone } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PeriodQueryable, ProcessedHotsEntry, ProcessedHotsEntryFilters } from '../models/processed-hots.models';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class ProcessedHotsService implements PeriodQueryable<ProcessedHotsEntry> {
  private baseUrl = `${this.appConfiguration.baseApiPath}`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<ProcessedHotsEntryFilters>): Observable<ProcessedHotsEntry> {
    const requestQuery = this.buildRequestQuery(query);

    return this.http.get<ProcessedHotsEntry>(
      this.baseUrl + `/monitoring-service/hot-files/query` + requestQuery.getQueryString()
    );
  }

  public download(
    query: DataQuery<ProcessedHotsEntryFilters>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.buildRequestQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private buildRequestQuery(query: DataQuery<ProcessedHotsEntryFilters>) {
    const _query = JSON.parse(JSON.stringify(query));

    if (_query.filterBy.endProcessingTime?.length) {
      const [endProcessingDateAfter, endProcessingDateBefore] = clone(_query.filterBy.endProcessingTime);
      delete _query.filterBy.endProcessingTime;

      _query.filterBy['endProcessingDateAfter'] = toShortIsoDate(endProcessingDateAfter);
      _query.filterBy['endProcessingDateBefore'] = toShortIsoDate(endProcessingDateBefore || endProcessingDateAfter);
    }

    return RequestQuery.fromDataQuery(_query);
  }
}
