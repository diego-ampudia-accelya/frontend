import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ProcessedHotsEntryFilters } from '../models/processed-hots.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type ProcessedHotsMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class ProcessedHotsFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: ProcessedHotsEntryFilters) {
    const filterMappers: Partial<ProcessedHotsMapper<ProcessedHotsEntryFilters>> = {
      name: fileName => `${this.translate('fileName')} - ${fileName}`,
      endProcessingTime: processDateTime =>
        `${this.translate('processDateTime')} - ${rangeDateFilterTagMapper(processDateTime)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`MENU.files.processedHots.filters.${key}.label`);
  }
}
