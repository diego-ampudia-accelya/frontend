import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { ProcessedHotsFacadeService } from '../store/processed-hots-store-facade.service';

import { ProcessedHotsService } from './processed-hots.service';
import { AppConfigurationService } from '~app/shared/services';

describe('ProcessedHotsService', () => {
  let service: ProcessedHotsService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ProcessedHotsService,
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: ''
          }
        },
        {
          provide: ProcessedHotsFacadeService,
          useValue: {
            actions: {
              searchProcessedHots: () => {},
              downloadProcessedHots: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null)
            },
            loading: {
              searchProcessedHots$: of(null)
            }
          }
        }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(ProcessedHotsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
