import { HttpErrorResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/processed-hots.actions';
import * as fromSelectors from '../selectors/processed-hots.selectors';
import { ProcessedHotsService } from '../services/processed-hots.service';
import * as fromState from '../store/processed-hots.state';
import * as fromStub from '../stubs/processed-hots.stubs';

import { ProcessedHotsEffects } from './processed-hots.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

describe('ProcessedHotsEffects', () => {
  let actions$: Observable<any>;
  let effects: ProcessedHotsEffects;
  let service: ProcessedHotsService;
  let dialogService: DialogService;

  const initialState = {
    [fromState.processedHotsStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProcessedHotsEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectProcessedHotsQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: ProcessedHotsService,
          useValue: {
            find: () => {},
            download: () => {}
          }
        },
        {
          provide: DialogService,
          useValue: {
            open: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<ProcessedHotsEffects>(ProcessedHotsEffects);
    service = TestBed.inject<ProcessedHotsService>(ProcessedHotsService);
    dialogService = TestBed.inject<DialogService>(DialogService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchProcessedHotsEffect', () => {
    it('should return searchProcessedHotsSuccess with paged data', () => {
      const action = fromActions.searchProcessedHotsRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchProcessedHotsSuccess({
        payload: {
          pagedData: fromStub.processedHotsPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.processedHotsPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchProcessedHotsEffect$).toBeObservable(expected);
    });

    it('should return searchProcessedHotsRequest when processed hots service find fails', () => {
      const action = fromActions.searchProcessedHotsRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchProcessedHotsFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchProcessedHotsEffect$).toBeObservable(expected);
    });
  });

  describe('downloadProcessedHotsRequest', () => {
    it('should open download dialog when downloadProcessedHotsRequest is called', fakeAsync(() => {
      const action = fromActions.downloadProcessedHotsRequest();

      spyOn(dialogService, 'open');

      actions$ = new BehaviorSubject<unknown>(action);

      effects.downloadProcessedHotsEffect$.subscribe();

      tick();

      expect(dialogService.open).toHaveBeenCalledWith(DownloadFileComponent, {
        data: {
          title: 'BUTTON.DEFAULT.DOWNLOAD',
          footerButtonsType: FooterButton.Download,
          downloadQuery: {
            ...defaultQuery,
            filterBy: {
              ...defaultQuery?.filterBy
            }
          }
        },
        apiService: service
      });
    }));
  });
});
