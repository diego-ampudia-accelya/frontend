import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as fromActions from '../actions/processed-hots.actions';
import * as fromSelectors from '../selectors/processed-hots.selectors';
import { ProcessedHotsService } from '../services/processed-hots.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

@Injectable()
export class ProcessedHotsEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly processedHotsService: ProcessedHotsService,
    private readonly store: Store<unknown>,
    private readonly dialogService: DialogService
  ) {}

  public searchProcessedHotsEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchProcessedHotsRequest>>(fromActions.searchProcessedHotsRequest),
      switchMap(({ payload: { query } }) =>
        this.processedHotsService.find(query).pipe(
          map(data =>
            fromActions.searchProcessedHotsSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchProcessedHotsFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public downloadProcessedHotsEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.downloadProcessedHotsRequest>>(fromActions.downloadProcessedHotsRequest),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectProcessedHotsQuery))),
        tap(([_, query]) => {
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.processedHotsService
          });
        })
      ),
    { dispatch: false }
  );
}
