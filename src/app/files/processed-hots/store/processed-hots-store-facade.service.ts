import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/processed-hots.actions';
import { ProcessedHotsEntryFilters } from '../models/processed-hots.models';
import * as fromSelectors from '../selectors/processed-hots.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class ProcessedHotsFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchProcessedHots: (query?: DataQuery<ProcessedHotsEntryFilters>) =>
        this.store.dispatch(fromActions.searchProcessedHotsRequest({ payload: { query } })),
      downloadProcessedHots: () => this.store.dispatch(fromActions.downloadProcessedHotsRequest())
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectProcessedHotsQuery),
      data$: this.store.select(fromSelectors.selectProcessedHots),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser),
      lastDateAvailable$: this.store.select(fromSelectors.selectLastDateAvailable)
    };
  }

  public get loading() {
    return {
      searchProcessedHots: this.store.select(fromSelectors.selectSearchProcessedHotsLoading)
    };
  }
}
