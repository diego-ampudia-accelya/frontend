import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/processed-hots.actions';
import * as fromSelectors from '../selectors/processed-hots.selectors';

import { ProcessedHotsFacadeService } from './processed-hots-store-facade.service';
import { initialState, processedHotsStateFeatureKey } from './processed-hots.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('ProcessedHotsFacadeService', () => {
  let service: ProcessedHotsFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProcessedHotsFacadeService,
        provideMockStore({
          initialState: {
            [processedHotsStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(ProcessedHotsFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('should dispatch a new searchProcessedHotsRequest action when searchProcessedHots is called', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchProcessedHots(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchProcessedHotsRequest({ payload: { query: defaultQuery } })
      );
    });

    it('should dispatch a new downloadProcessedHotsRequest action when downloadProcessedHots is called', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.downloadProcessedHots();

      expect(store.dispatch).toHaveBeenCalledWith(fromActions.downloadProcessedHotsRequest());
    });
  });

  describe('selectors', () => {
    it('should select query with query$ ', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectProcessedHotsQuery);
    });

    it('should select data with data$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectProcessedHots);
    });

    it('should select hasData with hasData$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('should invoke selectSearchProcessedHotsLoading with searchProcessedHots$', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchProcessedHots; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchProcessedHotsLoading);
    });
  });
});
