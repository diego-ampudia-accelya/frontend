import { HttpErrorResponse } from '@angular/common/http';

import {
  LastProcessedDateAndPeriod,
  ProcessedHotsEntryFilters,
  ProcessedHotsRecord
} from '../models/processed-hots.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const processedHotsStateFeatureKey = 'processedHotsStateFeatureKey';

export interface ProcessedHotsState {
  query: DataQuery<ProcessedHotsEntryFilters>;
  data: ProcessedHotsRecord[];
  lastDateAndPeriod: LastProcessedDateAndPeriod;
  loading: {
    searchProcessedHots: boolean;
  };
  error: {
    searchProcessedHots: HttpErrorResponse | null;
  };
}

export const initialState: ProcessedHotsState = {
  query: defaultQuery,
  data: [],
  lastDateAndPeriod: {
    lastDateOfIssue: '',
    period: ''
  },
  loading: {
    searchProcessedHots: false
  },
  error: {
    searchProcessedHots: null
  }
};
