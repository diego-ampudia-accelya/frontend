import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { combineLatest, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { PROCESSED_HOTS } from '../../constants/processed-hots.constants';
import {
  LastProcessedDateAndPeriod,
  ProcessedHotsEntryFilters,
  ProcessedHotsRecord
} from '../../models/processed-hots.models';
import { ProcessedHotsFilterFormatterService } from '../../services/processed-hots-filter-formatter.service';
import { ProcessedHotsFacadeService } from '../../store/processed-hots-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

@Component({
  selector: 'bspl-processed-hots-list',
  templateUrl: './processed-hots-list.component.html'
})
export class ProcessedHotsListComponent implements OnInit {
  public columns: TableColumn[] = PROCESSED_HOTS.COLUMNS;
  public hasAllBspsPermission: boolean;
  public userBspDropdownOptions$: Observable<DropdownOption<BspDto>[]>;
  public userDefaultBsp$: Observable<Bsp>;
  public data$: Observable<ProcessedHotsRecord[]> = this.processedHotsFacadeService.selectors.data$;
  public query$: Observable<DataQuery<ProcessedHotsEntryFilters>> = this.processedHotsFacadeService.selectors.query$;
  public loading$: Observable<boolean> = this.processedHotsFacadeService.loading.searchProcessedHots;
  public hasData$: Observable<boolean> = this.processedHotsFacadeService.selectors.hasData$;
  public lastDateAvailable$: Observable<LastProcessedDateAndPeriod> =
    this.processedHotsFacadeService.selectors.lastDateAvailable$;
  public filterFormGroup: FormGroup;

  constructor(
    public readonly processedHotsFilterFormatterService: ProcessedHotsFilterFormatterService,
    private readonly permissionService: PermissionsService,
    private readonly translationService: L10nTranslationService,
    private readonly processedHotsFacadeService: ProcessedHotsFacadeService,
    private readonly periodPipe: PeriodPipe
  ) {}

  public get headerData$(): Observable<string> {
    return this.lastDateAvailable$.pipe(
      map(
        data =>
          `${this.translationService.translate('MENU.files.processedHots.lastDateText')}: ${moment(
            data.lastDateOfIssue
          ).format('DD/MM/YYYY')} (${this.periodPipe.transform(data.period)})`
      )
    );
  }

  public get isBspFilterDisabled$(): Observable<boolean> {
    return this.userBspDropdownOptions$.pipe(map(userBspDropdownOptions => Boolean(userBspDropdownOptions?.length)));
  }

  public ngOnInit(): void {
    this.initializePermissions();
    this.initializeFormGroup();
    this.initializeBspAndDropdownOptions();
  }

  public onQueryChanged(query: DataQuery<ProcessedHotsEntryFilters>): void {
    this.processedHotsFacadeService.actions.searchProcessedHots(query);
  }

  public onDownload(): void {
    this.processedHotsFacadeService.actions.downloadProcessedHots();
  }

  private initializeFormGroup(): void {
    this.filterFormGroup = new FormGroup({
      name: new FormControl(null, []),
      endProcessingTime: new FormControl(null, [])
    });
  }

  private initializeBspAndDropdownOptions(): void {
    this.userBspDropdownOptions$ = this.processedHotsFacadeService.selectors.userBsps$.pipe(
      map(bspList => bspList.map(bsp => toValueLabelObjectBsp(bsp)))
    );

    this.userDefaultBsp$ = combineLatest([
      this.processedHotsFacadeService.selectors.userBsps$,
      this.processedHotsFacadeService.selectors.user$
    ]).pipe(map(([bspList, user]) => this.getDefaultBsp(bspList, user)));

    this.userDefaultBsp$.pipe(first()).subscribe(bsp =>
      this.processedHotsFacadeService.actions.searchProcessedHots({
        ...defaultQuery,
        filterBy: {
          ...defaultQuery.filterBy
        },
        sortBy: [{ attribute: 'endProcessingTime', sortType: SortOrder.Desc }]
      })
    );
  }

  private initializePermissions(): void {
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private getDefaultBsp(bspList: Bsp[], user: User): Bsp {
    return bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
  }
}
