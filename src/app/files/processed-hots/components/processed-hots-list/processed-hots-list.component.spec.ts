import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { ProcessedHotsFilterFormatterService } from '../../services/processed-hots-filter-formatter.service';
import { ProcessedHotsFacadeService } from '../../store/processed-hots-store-facade.service';
import * as fromStub from '../../stubs/processed-hots.stubs';
import { ProcessedHotsListComponent } from './processed-hots-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { ListViewComponent } from '~app/shared/components/list-view';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('ProcessedHotsListComponent', () => {
  let component: ProcessedHotsListComponent;
  let fixture: ComponentFixture<ProcessedHotsListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule)],
      declarations: [
        ProcessedHotsListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: ProcessedHotsFacadeService,
          useValue: {
            actions: {
              searchProcessedHots: () => {},
              downloadProcessedHots: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null),
              userBsps$: of(fromStub.userBsps),
              user$: of(fromStub.user),
              lastDateAvailable$: of(fromStub.lastDateAvailable)
            },
            loading: {
              searchProcessedHots$: of(null)
            }
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        }
      ]
    })
      .overrideComponent(ProcessedHotsListComponent, {
        set: {
          providers: [
            {
              provide: ProcessedHotsFilterFormatterService,
              useValue: {
                format: () => {}
              }
            },
            { provide: PeriodPipe, useClass: PeriodPipeMock }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessedHotsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
