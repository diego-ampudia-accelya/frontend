import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import { ProcessedHotsEntry, ProcessedHotsEntryFilters } from '../models/processed-hots.models';
import { DataQuery } from '~app/shared/components/list-view';

export const searchProcessedHotsRequest = createAction(
  '[Processed Hots]Search Processed Hots Request',
  props<{ payload: { query?: DataQuery<ProcessedHotsEntryFilters> } }>()
);

export const searchProcessedHotsSuccess = createAction(
  '[Processed Hots]Search Processed Hots Success',
  props<{ payload: { pagedData: ProcessedHotsEntry } }>()
);

export const searchProcessedHotsFailure = createAction(
  '[Processed Hots]Search Processed Hots Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const downloadProcessedHotsRequest = createAction('[Processed Hots]Download Processed Hots Request');
