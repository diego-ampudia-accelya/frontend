import moment from 'moment-mini';

import { GridColumn } from '~app/shared/models';

export const PROCESSED_HOTS = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'name',
        name: 'MENU.files.processedHots.columns.fileName',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'endProcessingTime',
        name: 'MENU.files.processedHots.columns.processDateTime',
        resizeable: false,
        draggable: false,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY HH:mm:ss') }
      }
    ];
  }
};
