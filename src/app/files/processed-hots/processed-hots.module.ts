import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { ProcessedHotsListComponent } from './components/processed-hots-list/processed-hots-list.component';
import { ProcessedHotsEffects } from './effects/processed-hots.effects';
import { ProcessedHotsRoutingModule } from './processed-hots-routing.module';
import { ProcessedHotsComponent } from './processed-hots.component';
import { processedHotsReducer } from './reducers/processed-hots.reducer';
import { ProcessedHotsFilterFormatterService } from './services/processed-hots-filter-formatter.service';
import { ProcessedHotsService } from './services/processed-hots.service';
import { ProcessedHotsFacadeService } from './store/processed-hots-store-facade.service';
import { processedHotsStateFeatureKey } from './store/processed-hots.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ProcessedHotsRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(processedHotsStateFeatureKey, processedHotsReducer),
    EffectsModule.forFeature([ProcessedHotsEffects])
  ],
  declarations: [ProcessedHotsComponent, ProcessedHotsListComponent],
  providers: [ProcessedHotsService, ProcessedHotsFacadeService, ProcessedHotsFilterFormatterService]
})
export class ProcessedHotsModule {}
