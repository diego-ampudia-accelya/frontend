export interface SenderTypeOption {
  senderType: string;
  senderTypeId: number;
}
