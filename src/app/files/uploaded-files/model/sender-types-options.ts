import { SenderTypeOption } from './sender-type';
import { DropdownOption } from '~app/shared/models';

export const SENDER_TYPES_OPTIONS: DropdownOption<SenderTypeOption>[] = [
  { value: { senderType: 'Airline', senderTypeId: 0 }, label: 'Airline' },
  { value: { senderType: 'Agent', senderTypeId: 1 }, label: 'Agent' },
  { value: { senderType: 'IATA', senderTypeId: 2 }, label: 'IATA' },
  { value: { senderType: 'DPC', senderTypeId: 3 }, label: 'DPC' },
  { value: { senderType: 'EARS', senderTypeId: 4 }, label: 'EARS' },
  { value: { senderType: 'GDS', senderTypeId: 5 }, label: 'GDS' },
  { value: { senderType: 'Third Party', senderTypeId: 7 }, label: 'Third Party' },
  { value: { senderType: 'Agent Group', senderTypeId: 8 }, label: 'Agent Group' }
];
