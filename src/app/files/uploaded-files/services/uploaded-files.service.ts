import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UploadedFilesFilter, UploadedFilesFilterBE } from '~app/files/model/uploaded-files-filter.model';
import { UploadedFile } from '~app/files/model/uploaded-files.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class UploadedFilesService implements Queryable<UploadedFile> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/uploaded-files`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<UploadedFilesFilter>): Observable<PagedData<UploadedFile>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<UploadedFile>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery<UploadedFilesFilter>>): RequestQuery<UploadedFilesFilterBE> {
    const { sendingDate, senderType, type, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromSendingDate: sendingDate && toShortIsoDate(sendingDate[0]),
        toSendingDate: sendingDate && toShortIsoDate(sendingDate[1]),
        senderType: senderType && senderType.map(s => s.senderType),
        descriptor: type && type.map(t => t.descriptor)
      }
    });
  }

  public download(
    query: DataQuery<UploadedFilesFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
