import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { UploadedFilesFilter } from '~app/files/model/uploaded-files-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class UploadedFilesFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: UploadedFilesFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<UploadedFilesFilter>> = {
      sendingDate: sendingDate => `${this.translate('dateRange')} - ${rangeDateFilterTagMapper(sendingDate)}`,
      type: type => `${this.translate('fileType')} - ${mapJoinMapper(type, 'description')}`,
      senderType: senderType => `${this.translate('senderType')} - ${mapJoinMapper(senderType, 'senderType')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.files.uploadedFiles.filters.${key}`);
  }
}
