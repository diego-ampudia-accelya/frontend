import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import moment from 'moment-mini';
import { Observable, Subject } from 'rxjs';

import { UploadedFilesFilter } from '../model/uploaded-files-filter.model';
import { UploadedFile } from '../model/uploaded-files.model';
import { SENDER_TYPES_OPTIONS } from './model/sender-types-options';
import { UploadedFilesFilterFormatter } from './services/uploaded-files-filter-formatter';
import { UploadedFilesService } from './services/uploaded-files.service';
import { sameMonthValidator } from './validators/same-month.validator';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { FileDescriptorDictionaryService } from '~app/shared/services/dictionary/file-descriptor-dictionary.service';

@Component({
  selector: 'bspl-uploaded-files',
  templateUrl: './uploaded-files.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: UploadedFilesService }
  ]
})
export class UploadedFilesComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public senderTypesOptions: DropdownOption[] = SENDER_TYPES_OPTIONS;
  public fileTypesOptions$: Observable<DropdownOption[]>;

  public maxDate = moment().endOf('day').toDate();
  public minDate = moment(this.maxDate).subtract(2, 'years').toDate();

  public predefinedFilters: {
    sendingDate: Date[];
  };

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private sendindDateInitialValue = [moment().startOf('month').toDate(), this.maxDate];

  private destroy$ = new Subject<any>();

  constructor(
    public displayFormatter: UploadedFilesFilterFormatter,
    public dataSource: QueryableDataSource<UploadedFile>,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private bytesFormatPipe: BytesFormatPipe,
    private queryStorage: DefaultQueryStorage,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private permissionsService: PermissionsService,
    private uploadedFilesService: UploadedFilesService,
    private fileDescriptorDictionary: FileDescriptorDictionaryService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.initializeFileTypeDropdown();
    this.checkPermissionAndLoadData();

    this.initColumns();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.files.uploadedFiles.uploadedFilesDownloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.uploadedFilesService
    });
  }

  public loadData(query: DataQuery<UploadedFilesFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private checkPermissionAndLoadData(): void {
    const hasReadUploadedFilesPermissions = this.permissionsService.hasPermission(Permissions.readUploadedFiles);
    if (hasReadUploadedFilesPermissions) {
      this.initializeDataQuery();
    }
  }

  private initializeDataQuery(): void {
    this.setPredefinedFilters();
    this.loadData(this.storedQuery);
  }
  private initializeFileTypeDropdown(): void {
    this.fileTypesOptions$ = this.fileDescriptorDictionary.getDropdownOptions();
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      sendingDate: this.sendindDateInitialValue
    };
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<UploadedFilesFilter>({
      sendingDate: [null, sameMonthValidator()],
      type: [],
      senderType: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'name',
        name: 'MENU.files.uploadedFiles.columns.fileName',
        flexGrow: 4
      },
      {
        prop: 'type',
        name: 'MENU.files.uploadedFiles.columns.fileType',
        resizeable: true,
        flexGrow: 4
      },
      {
        prop: 'size',
        name: 'MENU.files.uploadedFiles.columns.fileSize',
        pipe: {
          transform: value => this.bytesFormatPipe.transform(value)
        },
        flexGrow: 1
      },
      {
        prop: 'sendingNumber',
        name: 'MENU.files.uploadedFiles.columns.sendingNumber',
        flexGrow: 1
      },
      {
        prop: 'senderType',
        name: 'MENU.files.uploadedFiles.columns.senderType',
        flexGrow: 1
      },
      {
        prop: 'senderUserId',
        name: 'MENU.files.uploadedFiles.columns.senderUserId',
        flexGrow: 1
      },
      {
        prop: 'sendingDate',
        name: 'MENU.files.uploadedFiles.columns.sendingDate',
        flexGrow: 1,
        resizeable: true,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        }
      },
      {
        prop: 'filenameDate',
        name: 'MENU.files.uploadedFiles.columns.fileDate',
        resizeable: true,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy')
        },
        flexGrow: 1
      }
    ];
  }
}
