import { FormControl } from '@angular/forms';
import moment from 'moment-mini';

import { sameMonthValidator } from './same-month.validator';

describe('sameMonthValidator', () => {
  it('should return no errors when both dates have been set and have the same month', () => {
    const value = [moment('11/12/2021').toDate(), moment('11/29/2021').toDate()];
    const result = new FormControl(value, [sameMonthValidator()]);

    expect(result.errors).toBeNull();
  });

  it('should return error when second date not set', () => {
    const value = [moment('12/20/2021').toDate(), null];
    const expectedError = {
      bothDatesExist: {
        message: 'MENU.files.uploadedFiles.errors.bothDatesExist'
      }
    };
    const result = new FormControl(value, [sameMonthValidator()]);

    expect(result.errors).toEqual(expectedError);
  });

  it('should return error when dates have not the same month', () => {
    const value = [moment('11/20/2021').toDate(), moment('12/15/2021').toDate()];
    const expectedError = {
      sameMonth: {
        message: 'MENU.files.uploadedFiles.errors.sameMonth'
      }
    };
    const result = new FormControl(value, [sameMonthValidator()]);

    expect(result.errors).toEqual(expectedError);
  });
});
