import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

import { PeriodUtils } from '~app/shared/components';

export function sameMonthValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (control.value) {
      const startDate = control.value[0];
      const endDate = control.value[1];

      if (!startDate || !endDate) {
        return {
          bothDatesExist: {
            message: 'MENU.files.uploadedFiles.errors.bothDatesExist'
          }
        };
      }

      const startMonth = PeriodUtils.getMonthFrom(startDate.toString());
      const endMonth = PeriodUtils.getMonthFrom(endDate.toString());
      const startYear = PeriodUtils.getYear(startDate.toString());
      const endYear = PeriodUtils.getYear(endDate.toString());
      if (startMonth !== endMonth || startYear !== endYear) {
        return {
          sameMonth: {
            message: 'MENU.files.uploadedFiles.errors.sameMonth'
          }
        };
      }
    }

    return null;
  };
}
