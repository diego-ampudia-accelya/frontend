import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { AgentReportsComponent } from './agent-reports/agent-reports.component';
import { AgentReportsGuard } from './agent-reports/agent-reports.guard';
import { DefaultFiltersGuard } from './agent-reports/default-filters.guard';
import { DeletedFilesComponent } from './deleted-files/deleted-files.component';
import { DownloadedFilesComponent } from './downloaded-files/downloaded-files.component';
import { DownloadedIataCommunicationsComponent } from './downloaded-iata-communications/downloaded-iata-communications.component';
import { EvaluationFilesComponent } from './evaluation-files/evaluation-files.component';
import { FopAgentStatusRequestGuard } from './fop-agent-status-request/guards/fop-agent-status-request.guard';
import { GlobalTaComponent } from './global-ta/global-ta.component';
import { UploadCommunicationsGuard } from './guards/upload-communications.guard';
import { UploadFilesGuard } from './guards/upload-files.guard';
import { NotDownloadedFilesComponent } from './not-downloaded-files/not-downloaded-files.component';
import { ReportsListComponent } from './reports/reports-list/reports-list.component';
import { ReportsComponent } from './reports/reports.component';
import { SpecialFilesComponent } from './special-files/special-files.component';
import { UploadedFilesComponent } from './uploaded-files/uploaded-files.component';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';

const reportsChildren: Route[] = [
  {
    path: 'inbox',
    component: ReportsListComponent,
    data: {
      title: 'MENU.files.reports.inbox.title',
      tableTitle: 'MENU.files.reports.inbox.tableTitle',
      type: 'inbox',
      requiredPermissions: [Permissions.readFile, Permissions.readCommunicationsFile]
    }
  },
  {
    path: 'archive',
    component: ReportsListComponent,
    data: {
      title: 'MENU.files.reports.archive.title',
      tableTitle: 'MENU.files.reports.archive.tableTitle',
      type: 'archive',
      requiredPermissions: [Permissions.readFile, Permissions.readCommunicationsFile]
    }
  },
  { path: '', redirectTo: 'inbox', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: ROUTES.REPORTS.path,
    component: ReportsComponent,
    children: reportsChildren,
    data: {
      tab: ROUTES.REPORTS
    }
  },
  {
    path: ROUTES.EVALUATION_FILES.path,
    component: EvaluationFilesComponent,
    data: {
      tab: ROUTES.EVALUATION_FILES
    }
  },
  {
    path: ROUTES.GLOBAL_TA_FILES.path,
    component: GlobalTaComponent,
    data: {
      tab: ROUTES.GLOBAL_TA_FILES
    }
  },
  {
    path: ROUTES.FOP_AGENT_STATUS_REQUEST.path,
    canActivate: [FopAgentStatusRequestGuard]
  },
  {
    path: ROUTES.UPLOAD_FILES.path,
    canActivate: [UploadFilesGuard]
  },
  {
    path: ROUTES.DELETED_FILES.path,
    component: DeletedFilesComponent,
    data: {
      tab: ROUTES.DELETED_FILES
    }
  },
  {
    path: ROUTES.UPLOAD_COMMUNICATIONS.path,
    canActivate: [UploadCommunicationsGuard]
  },
  {
    path: ROUTES.AGENT_REPORTS_PARAMETERS.path,
    canActivate: [DefaultFiltersGuard]
  },
  {
    path: ROUTES.AGENT_REPORTS.path,
    component: AgentReportsComponent,
    canActivate: [AgentReportsGuard],
    data: {
      tab: ROUTES.AGENT_REPORTS
    }
  },
  {
    path: ROUTES.UPLOADED_FILES.path,
    component: UploadedFilesComponent,
    data: {
      tab: ROUTES.UPLOADED_FILES
    }
  },
  {
    path: ROUTES.DOWNLOADED_FILES.path,
    component: DownloadedFilesComponent,
    data: {
      tab: ROUTES.DOWNLOADED_FILES
    }
  },
  {
    path: ROUTES.NOT_DOWNLOADED_FILES.path,
    component: NotDownloadedFilesComponent,
    data: {
      tab: ROUTES.NOT_DOWNLOADED_FILES
    }
  },
  {
    path: ROUTES.SPECIAL_FILES.path,
    component: SpecialFilesComponent,
    data: {
      tab: ROUTES.SPECIAL_FILES
    }
  },
  {
    path: ROUTES.DOWNLOADED_IATA_COMMUNICATIONS.path,
    component: DownloadedIataCommunicationsComponent,
    data: {
      tab: ROUTES.DOWNLOADED_IATA_COMMUNICATIONS
    }
  },
  {
    path: ROUTES.PROCESSED_HOTS.path,
    loadChildren: () => import('./processed-hots/processed-hots.module').then(m => m.ProcessedHotsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilesRoutingModule {}
