import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEqual } from 'lodash';
import moment from 'moment-mini';
import { of } from 'rxjs';

import { FileUploadDialogConfig } from '../model';
import { FileUploadDialogComponent } from './file-upload-dialog.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { TranslatePipeMock } from '~app/test';

describe('FileUploadDialogComponent', () => {
  let fixture: ComponentFixture<FileUploadDialogComponent>;
  let component: FileUploadDialogComponent;
  let localBspTimeServiceMock: SpyObject<LocalBspTimeService>;
  const today = moment('2020-01-01').toDate();
  const tomorrow = moment('2020-01-02').toDate();

  const mockConfig: DialogConfig<FileUploadDialogConfig> = {
    data: {
      title: 'title',
      bsp: {
        id: 5
      },
      uploadingTitle: 'Uploading',
      uploadedTitle: 'Uploaded',
      footerButtonsType: FooterButton.Search,
      files: [new File([''], 'filename', { type: 'text/json' })]
    }
  } as DialogConfig;

  const mockedData = {
    isClosable: true,
    buttons: [
      {
        title: 'Done',
        isDisabled: false,
        type: FooterButton.Done,
        buttonDesign: ButtonDesign.Tertiary
      }
    ]
  };

  const dialogServiceSpy = createSpyObject(DialogService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadDialogComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        ReactiveSubject,
        { provide: DialogConfig, useValue: cloneDeep(mockConfig) },
        { provide: DialogService, useValue: dialogServiceSpy },
        mockProvider(LocalBspTimeService),
        mockProvider(L10nTranslationService)
      ]
    }).compileComponents();

    localBspTimeServiceMock = TestBed.inject<any>(LocalBspTimeService);
    localBspTimeServiceMock.getLocalBspTime.and.returnValue(of({ id: 1234, localTime: today }));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.config.data.buttons = [
      {
        title: 'Upload',
        isDisabled: true,
        type: FooterButton.Upload,
        buttonDesign: ButtonDesign.Tertiary
      } as ModalAction
    ];
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should set files to component, when config data is not empty', () => {
    expect(isEqual(component.files, mockConfig.data.files)).toBe(true);
  });

  it('should call specific methods, when new files are added', () => {
    const dynamicInstance = createSpyObject(DialogService);
    dialogServiceSpy.isDialogOpen.and.returnValue(true);
    dialogServiceSpy.getDynamicInstance.and.returnValue(dynamicInstance);

    const filesMock = [new File([''], 'mockedFile', { type: 'text/json' })];
    component.addFiles(filesMock);

    expect(dialogServiceSpy.getDynamicInstance).toHaveBeenCalled();
    expect(dialogServiceSpy.isDialogOpen).toHaveBeenCalled();
  });

  it('should show loading state of Done button and block closing the dialog when upload is started', () => {
    component.config.data = { ...component.config.data, ...cloneDeep(mockedData) };
    component.onUploadStarted();

    const instanceData = component.config.data;
    const doneButton = instanceData.buttons.find(b => b.type === FooterButton.Done);

    expect(doneButton.isLoading).toBe(true);
    expect(instanceData.isClosable).toBe(false);
    expect(instanceData.title).toBe('Uploading');
  });

  it('should hide loading state of Done button and enable closing the dialog, when upload is finished', () => {
    const configData = cloneDeep(mockedData);
    configData.isClosable = false;

    component.config.data = {
      ...component.config.data,
      ...configData,
      files: [new File([''], 'filename', { type: 'text/json' })]
    };
    component.onUploadFinished();

    const instanceData = component.config.data;
    const doneButton = instanceData.buttons.find(b => b.type === FooterButton.Done);

    expect(doneButton.isLoading).toBe(false);
    expect(instanceData.isClosable).toBe(true);
    expect(instanceData.title).toBe('Uploaded');
    expect(component.config.data.files).toEqual([]);
  });

  it('should reset buttons, when onResetInitialMode is called.', () => {
    const buttonsAfterReset = [
      {
        type: FooterButton.Cancel,
        buttonDesign: ButtonDesign.Tertiary
      },
      {
        isDisabled: true,
        type: FooterButton.Upload,
        buttonDesign: ButtonDesign.Primary
      }
    ];

    component.config.data.buttons = [
      {
        type: FooterButton.Done,
        isDisabled: false,
        buttonDesign: ButtonDesign.Primary
      }
    ];

    component.onResetInitialMode();

    expect(isEqual(component.config.data.buttons[0].type, buttonsAfterReset[0].type)).toBe(true);
  });

  it('should enable upload button, when some file is valid.', () => {
    const files = [{ isValid: true }, { isValid: false }] as FileUpload[];
    component.config.data.buttons = [
      {
        title: 'Upload',
        type: FooterButton.Upload,
        isDisabled: true
      }
    ];

    component.onFilesChange(files);
    const uploadButton = component.config.data.buttons.find(b => b.type === FooterButton.Upload);
    expect(uploadButton.isDisabled).toBe(false);
  });

  it('should disable upload button, when all files are invalid.', () => {
    const files = [{ isValid: false }, { isValid: false }] as FileUpload[];
    component.config.data.buttons = [
      {
        title: 'Upload',
        type: FooterButton.Upload,
        isDisabled: true
      }
    ];

    component.onFilesChange(files);
    const uploadButton = component.config.data.buttons.find(b => b.type === FooterButton.Upload);
    expect(uploadButton.isDisabled).toBe(true);
  });

  describe('when additional info is required', () => {
    beforeEach(() => {
      component.config.data.requireAdditionalInfo = true;
      component.ngOnInit();
    });

    it('should initialize expiry date range', fakeAsync(() => {
      const min = moment('2020-01-02').toDate();
      const max = moment('2021-01-01').toDate();

      component.expiryDateRange$.subscribe(expiryDate => {
        expect(expiryDate.min).toEqual(min);
        expect(expiryDate.max).toEqual(max);
      });
    }));

    it('should disable upload button when expiry date is not set and no file is selected', () => {
      const uploadButton = component.config.data.buttons.find(button => button.type === FooterButton.Upload);

      expect(uploadButton.isDisabled).toBe(true);
    });

    it('should enable upload button when expiry date is provided and a file is selected', () => {
      const files = [{ isValid: true }, { isValid: false }] as FileUpload[];

      component.onFilesChange(files);
      component.additionalInfoForm.setValue({ expiryDate: tomorrow });

      const uploadButton = component.config.data.buttons.find(button => button.type === FooterButton.Upload);
      expect(uploadButton.isDisabled).toBe(false);
    });

    it('should update additionalInfo when expiry date is set', () => {
      component.additionalInfoForm.setValue({ expiryDate: tomorrow });

      expect(component.additionalInfo).toEqual({ expiryDate: '2020-01-02' });
    });

    it('should clear additionalInfo when expiry date is cleared', () => {
      component.additionalInfoForm.setValue({ expiryDate: tomorrow });
      component.additionalInfoForm.setValue({ expiryDate: null });

      expect(component.additionalInfo).toEqual({});
    });
  });
});
