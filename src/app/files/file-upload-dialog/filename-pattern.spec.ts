import { FILE_NAME_PATTERN } from './filename-pattern';

describe('filename pattern', () => {
  it('should not pass verification when a file name is wrong', () => {
    const invalidFileNames = [
      'eUel2203_20190605_B_01.220.zip',
      'EUEl2203_20190605_B_01.220.zip',
      'EUeL2203_20190605_B_01.220.zip',
      'EUel2207_20190605_B_01.220.zip',
      'EUeL999A_20190605_B_01.220.zip',
      'EUelA996_20190605_B_01.220.zip',
      'EUelA99__20190605_B_01.220.zip',
      'AUelCRSaFF1_20190605_B_01.220.zip',
      'AUelCRS_FF1_20190605_B_01.220.zip',
      'AUelCRSFFF11_20190605_B_01.220.zip',
      'AUelGRPaz_Z090_20190605_B_01.220.zip',
      'AUelGRPazAZ0901_20190605_B_01.220.zip',
      'AUelGRPazAZ 090_20190605_B_01.220.zip',
      'AUelBsP_20190605_B_01.220.zip',
      'AUelDpC_20190605_B_01.220.zip',
      'AUelbsp_20190605_B_01.220.zip',
      'AUelBSP1_20190605_B_01.220.zip',
      'AUelBSPz_20190605_B_01.220.zip',
      'AUelBSP__20190605_B_01.220.zip',
      'AUelTTTazAZ5678901_20190605_B_01.220.zip',
      'AUelTTTazAZ5_20190605_B_01.220.zip',
      'AUelTTT_azAZ5_20190605_B_01.220.zip',
      'AUelTTTazAZ567890_30000101_B_01.220.zip',
      'AUelTTTazAZ567890_29991301_B_01.220.zip',
      'AUelTTTazAZ567890_29991232_B_01.220.zip',
      'AUelTTTazAZ567890_301232_B_01.220.zip',
      'AUelTTTazAZ567890_190032_B_01.220.zip',
      'AUelTTTazAZ567890_190100_B_01.220.zip',
      'AUel2203_20190605_B"_01.220.zip',
      'AUel2203_20190605_B*_01.220.zip',
      'AUel2203_20190605_B/_01.220.zip',
      'AUel2203_20190605_B:_01.220.zip',
      'AUel2203_20190605_B<_01.220.zip',
      'AUel2203_20190605_B>_01.220.zip',
      'AUel2203_20190605_B?_01.220.zip',
      'AUel2203_20190605_B_01\\.220.zip',
      'AUel2203_20190605_B|_01.220.zip',
      'ABC220.zip',
      'AUelGPRazAZ09_20190605_B_01.220.zip'
    ];

    invalidFileNames.forEach(fileName => {
      expect(FILE_NAME_PATTERN.test(fileName)).toBe(false, fileName);
    });
  });

  it('should pass verification when a file name is correct', () => {
    const correctFileNames = [
      'AUel2203_20190605_B_01.220.zip',
      'AZaz2203_20190605_B_01.220.zip',
      'AU662203_20190605_B_01.220.zip',
      'AUz62203_20190605_B_01.220.zip',
      'GRel9996_20190605_B_01.220.zip',
      'GRel99933130_20190605_B_01.220.zip',
      'AUelCRS0996_20190605_B_01.220.zip',
      'AUelCRSFFF1_20190605_B_01.220.zip',
      'AUelGRPazAZ09_20190605_B_01.220.zip',
      'AUelGRPazAZ090_20190605_B_01.220.zip',
      'AUelBSP_20190605_B_01.220.zip',
      'AUelALL_20190605_B_01.220.zip',
      'AUelDPC_20190605_B_01.220.zip',
      'AUelTTTazAZ09_20190605_B_01.220.zip',
      'AUelTTTazAZ567890_20190605_B_01.220.zip',
      'AUelTTTazAZ567890_29991231_B_01.220.zip',
      'AUelTTTazAZ567890_29990101_B_01.220.zip',
      'AUel2203_20190605_B_01.220.zip',
      'AUel2203_20190605_~_01.220.zip',
      'AUel2203_20190605_}_01.220.zip',
      'AUel2203_20190605_{_01.220.zip',
      'AUel2203_20190605_`_01.220.zip',
      'AUel2203_20190605___01.220.zip',
      'AUel2203_20190605_^_01.220.zip',
      'AUel2203_20190605_]_01.220.zip',
      'AUel2203_20190605_[_01.220.zip',
      'AUel2203_20190605_@_01.220.zip',
      'AUel2203_20190605_=_01.220.zip',
      'AUel2203_20190605_;_01.220.zip',
      'AUel2203_20190605_._01.220.zip',
      'AUel2203_20190605_-_01.220.zip',
      'AUel2203_20190605_,_01.220.zip',
      'AUel2203_20190605_+_01.220.zip',
      'AUel2203_20190605_)_01.220.zip',
      'AUel2203_20190605_(_01.220.zip',
      "AUel2203_20190605_'_01.220.zip",
      'AUel2203_20190605_&_01.220.zip',
      'AUel2203_20190605_%_01.220.zip',
      'AUel2203_20190605_$_01.220.zip',
      'AUel2203_20190605_#_01.220.zip',
      'AUel2203_20190605_!_01.220.zip',
      'AUel2203_20190605_ _01.220.zip',
      'AUel2203_20190605_0123456789_01.220.zip',
      'AUel2203_20190605_AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz_01.220.zip'
    ];

    correctFileNames.forEach(fileName => {
      expect(FILE_NAME_PATTERN.test(fileName)).toBe(true, fileName);
    });
  });
});
