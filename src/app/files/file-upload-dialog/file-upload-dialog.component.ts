import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Dictionary } from 'lodash';
import moment from 'moment-mini';
import { defer, Observable, of } from 'rxjs';
import { catchError, finalize, map, shareReplay, tap } from 'rxjs/operators';

import { FileUploadDialogConfig, UploadDialogAction } from '../model';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { FileValidator } from '~app/shared/components/upload/file-validators';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { GLOBALS } from '~app/shared/constants/globals';
import { AlertMessageType } from '~app/shared/enums';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-file-upload-dialog',
  templateUrl: './file-upload-dialog.component.html',
  styleUrls: ['./file-upload-dialog.component.scss']
})
export class FileUploadDialogComponent implements OnInit {
  @ViewChild(UploadComponent) uploadComponentRef: UploadComponent;
  public canAddFiles = true;
  public files: File[] = [];
  public helpText: string;
  public validationRules: FileValidator[] = [];
  public mode = UploadModes.Initial;
  public isExpiryDateLocked: boolean;

  public additionalInfoForm: FormGroup;
  public additionalInfo: Dictionary<string>;
  public expiryDateRange$: Observable<{ min: Date; max: Date }>;
  public uploadModes = UploadModes;
  public alertMessageType = AlertMessageType;

  constructor(
    public config: DialogConfig<FileUploadDialogConfig>,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private formBuilder: FormBuilder,
    private localBspTimeService: LocalBspTimeService,
    private translationService: L10nTranslationService
  ) {}

  ngOnInit() {
    this.onActionExecuted().subscribe();
    const config = this.config.data;
    this.files = config.files;
    this.validationRules = config.validationRules ?? [];
    this.helpText = config.helpText;

    if (config.requireAdditionalInfo) {
      this.initializeAdditionalInfoForm();
    }
    this.expiryDateRange$ = this.loadExpiryDateRange();
  }

  public addFiles(files: File[]) {
    if (this.dialogService.isDialogOpen()) {
      const fileUploadDialogRef = this.dialogService.getDynamicInstance();
      if (fileUploadDialogRef instanceof FileUploadDialogComponent) {
        fileUploadDialogRef.files = files;
      }
    }
  }

  public onUploadStarted(): void {
    this.canAddFiles = false;
    this.config.data.isClosable = false;
    this.config.data.title = this.config.data.uploadingTitle;
    this.setButtonLoading(FooterButton.Done, true);
  }

  public onUploadFinished(): void {
    this.config.data.isClosable = true;
    this.config.data.title = this.config.data.uploadedTitle;
    this.setButtonLoading(FooterButton.Done, false);
    this.clearFiles();
  }

  public onResetInitialMode() {
    this.config.data.buttons = [
      {
        title: this.translationService.translate('MENU.files.modalCancel'),
        type: FooterButton.Cancel,
        buttonDesign: ButtonDesign.Tertiary
      },
      {
        title: this.translationService.translate('MENU.files.upload.title'),
        isDisabled: true,
        type: FooterButton.Upload,
        buttonDesign: ButtonDesign.Primary
      }
    ];

    this.reactiveSubject.emit({
      reset: UploadDialogAction.Reset
    });

    this.canAddFiles = true;
  }

  public onFilesChange(files: FileUpload[]) {
    this.config.data.files = files;
    this.setButtonDisabled(FooterButton.Upload, !this.isValid());
  }

  private onActionExecuted() {
    return this.reactiveSubject.asObservable.pipe(
      tap(action => {
        if (action.clickedBtn === FooterButton.Cancel || action.clickedBtn === FooterButton.Done) {
          this.clearFiles();
          this.dialogService.close();
        }

        if (action.clickedBtn === FooterButton.Upload) {
          this.config.data.buttons = [
            {
              title: this.translationService.translate('MENU.files.modalDone'),
              isDisabled: false,
              type: FooterButton.Done,
              buttonDesign: ButtonDesign.Tertiary
            }
          ];

          this.uploadComponentRef.uploadFiles();
        }
      })
    );
  }

  private clearFiles() {
    this.config.data.files = [];
  }

  private setButtonDisabled(buttonType: FooterButton, isDisabled: boolean) {
    const actionButton = this.getButton(buttonType);
    if (actionButton) {
      actionButton.isDisabled = isDisabled;
    }
  }

  private setButtonLoading(buttonType: FooterButton, isLoading: boolean) {
    const actionButton = this.getButton(buttonType);
    if (actionButton) {
      actionButton.isLoading = isLoading;
    }
  }

  private getButton(buttonType: FooterButton): any {
    return this.config.data.buttons.find(button => button.type === buttonType);
  }

  private isValid(): boolean {
    const hasValidFiles = this.config.data.files?.some((file: FileUpload) => file.isValid);
    const isFormValid = this.additionalInfoForm?.valid ?? true;

    return hasValidFiles && isFormValid;
  }

  private initializeAdditionalInfoForm(): void {
    this.additionalInfoForm = this.formBuilder.group({
      expiryDate: [null, Validators.required]
    });

    this.additionalInfoForm.valueChanges.subscribe(() => {
      this.setButtonDisabled(FooterButton.Upload, !this.isValid());
      this.additionalInfo = this.serializeAdditionalInfo(this.additionalInfoForm.value);
    });
  }

  private serializeAdditionalInfo({ expiryDate }): Dictionary<string> {
    let requestData: Dictionary<string> = {};
    if (expiryDate) {
      const formattedDate = moment(expiryDate).startOf('day').format(GLOBALS.DATE_FORMAT.SHORT_ISO);
      requestData = { expiryDate: formattedDate };
    }

    return requestData;
  }

  private loadExpiryDateRange(): Observable<{ min: Date; max: Date }> {
    return defer(() => {
      this.setButtonLoading(FooterButton.Upload, true);
      this.isExpiryDateLocked = true;

      return this.localBspTimeService.getLocalBspTime(this.config.data.bsp.id);
    }).pipe(
      map(localBspTime => ({
        min: moment(localBspTime.localTime).startOf('day').add(1, 'day').toDate(),
        max: moment(localBspTime.localTime).startOf('day').add(1, 'year').toDate()
      })),
      finalize(() => {
        this.isExpiryDateLocked = false;
        this.setButtonLoading(FooterButton.Upload, false);
      }),
      catchError(() => of(null)),
      shareReplay(1)
    );
  }
}
