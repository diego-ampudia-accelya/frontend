const airline = '\\d{3}[0-6]{1}';
const agent = '\\d{7}[0-6]{1}';
const gds = 'CRS[A-Z0-9]{4}';
const agentGroup = 'GRP[a-zA-Z0-9]{6,7}';
const thirdParty = 'TTT[a-zA-Z0-9]{6,10}';

const recepient = `DPC|BSP|ALL|${airline}|${agent}|${gds}|${agentGroup}|${thirdParty}`;
const bsp = '[A-Z0-9]{2}';
const fileType = '[a-z0-9]{2}';
const date = '[12](\\d{1}|\\d{3})(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01])';
const freeText = '(?!.*[:/<>?\\\\|"*]+)[\\w\\W]+';

export const FILE_NAME_PATTERN = new RegExp(`^(${bsp})(${fileType})(${recepient})_(${date})_(${freeText})$`);

export const EV_FILE_PATTERN = new RegExp(`^${bsp}ev.*$`);
