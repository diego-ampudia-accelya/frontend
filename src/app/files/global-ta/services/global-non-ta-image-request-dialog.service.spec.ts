import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of, throwError } from 'rxjs';

import { GlobalTaImageRequestDialogComponent } from '../global-ta-image-request-dialog/global-ta-image-request-dialog.component';

import { GlobalNonTaImageRequestDialogService } from './global-non-ta-image-request-dialog.service';
import { GlobalTaService } from './global-ta.service';
import { NotificationService } from '~app/shared/services';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

describe('GlobalNonTaImageRequestDialogService', () => {
  let service: GlobalNonTaImageRequestDialogService;

  const dialogServiceSpy = createSpyObject(DialogService);
  const dataServiceSpy = createSpyObject(GlobalTaService);
  const notificationServiceSpy = createSpyObject(NotificationService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GlobalNonTaImageRequestDialogService,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: GlobalTaService, useValue: dataServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(GlobalNonTaImageRequestDialogService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('open', () => {
    beforeEach(() => {
      dialogServiceSpy.open.calls.reset();
      dialogServiceSpy.close.calls.reset();
    });

    it('should open and close request dialog properly with image request NON TA action', fakeAsync(() => {
      const dialogConfig: DialogConfig = {
        data: {
          title: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.title',
          description: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.description',
          footerButtonsType: [{ type: FooterButton.Request }],
          isNonTaRequest: true
        }
      };

      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Request }));
      spyOn<any>(service, 'sendRequest').and.returnValue(of(FooterButton.Request));

      let result: FooterButton;
      service.open().subscribe(button => (result = button));
      tick();

      expect(result).toEqual(FooterButton.Request);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(GlobalTaImageRequestDialogComponent, dialogConfig);
      expect(service['sendRequest']).toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close dialog correctly with CANCEL action', fakeAsync(() => {
      const dialogConfig: DialogConfig = {
        data: {
          title: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.title',
          description: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.description',
          footerButtonsType: [{ type: FooterButton.Request }],
          isNonTaRequest: true
        }
      };

      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));
      spyOn<any>(service, 'sendRequest').and.callThrough();

      let result: FooterButton;
      service.open().subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Cancel);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(GlobalTaImageRequestDialogComponent, dialogConfig);
      expect(service['sendRequest']).not.toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close dialog correctly even with an ERROR from request method', fakeAsync(() => {
      const dialogConfig: DialogConfig = {
        data: {
          title: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.title',
          description: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.description',
          footerButtonsType: [{ type: FooterButton.Request }],
          isNonTaRequest: true
        }
      };

      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Request }));
      spyOn<any>(service, 'sendRequest').and.returnValue(throwError(new Error()));

      let error: Error;
      service.open().subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(GlobalTaImageRequestDialogComponent, dialogConfig);
      expect(service['sendRequest']).toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  it('should `sendRequest` calls service method with proper params NEXTGLOBAL selection', fakeAsync(() => {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.title',
        description: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.description',
        buttons: [{ type: FooterButton.Request, isDisabled: true }],
        isNonTaRequest: true
      }
    };

    const mockComponent: any = {
      form: {
        value: {
          requestType: 'false'
        }
      }
    };

    const paramExpected = {
      deleteFile: false,
      globalTaGdsFile: true
    };

    dataServiceSpy.sendNonTaImageRequest.and.returnValue(of());

    service['sendRequest'](dialogConfig, mockComponent).subscribe();
    tick();

    expect(dataServiceSpy.sendNonTaImageRequest).toHaveBeenCalledWith(paramExpected);
  }));

  it('should `sendRequest` calls service method with proper params DELETEFILE selection', fakeAsync(() => {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.title',
        description: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.description',
        buttons: [{ type: FooterButton.Request, isDisabled: true }],
        isNonTaRequest: true
      }
    };

    const mockComponent: any = {
      form: {
        value: {
          requestType: 'true'
        }
      }
    };

    const paramExpected = {
      deleteFile: true,
      globalTaGdsFile: false
    };

    dataServiceSpy.sendNonTaImageRequest.and.returnValue(of());

    service['sendRequest'](dialogConfig, mockComponent).subscribe();
    tick();

    expect(dataServiceSpy.sendNonTaImageRequest).toHaveBeenCalledWith(paramExpected);
  }));
});
