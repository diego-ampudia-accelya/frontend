import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { of, throwError } from 'rxjs';

import { GlobalTaService } from './global-ta.service';
import { files } from '~app/files/mocks/files.mock';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { FileType } from '~app/files/model';
import { DataQuery } from '~app/shared/components/list-view';
import { AppConfigurationService } from '~app/shared/services';

describe('GlobalTaService', () => {
  let service: GlobalTaService;
  let httpMock: SpyObject<HttpClient>;
  const selectedBsp = { id: '6983', isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GlobalTaService,
        provideMockStore({ initialState }),
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, {
          baseApiPath: 'base-url'
        })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    service = TestBed.inject(GlobalTaService);
    httpMock = TestBed.inject<any>(HttpClient);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should load all mock files for user with defaultQuery + bsp', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files));
    const dataQuery: DataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        bsp: selectedBsp
      }
    };
    const filesExpected = files.filter(file => file.isoCountryCode === selectedBsp.id);
    let result;
    service.find(dataQuery).subscribe(filesList => {
      result = filesList.records;
    });
    tick();
    expect(result).toEqual(filesExpected);
  }));

  it('should filter files by file type', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files));
    const azFileType: FileType = {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    };
    const dataQuery: DataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        type: [azFileType],
        bsp: selectedBsp
      }
    };
    const expectedFiles = files.filter(
      file => file.isoCountryCode === selectedBsp.id && file.type.descriptor === azFileType.descriptor
    );

    let result;
    service.find(dataQuery).subscribe(filesList => {
      result = filesList.records;
    });
    tick();
    expect(result).toEqual(expectedFiles);
  }));

  it('should download file ', fakeAsync(() => {
    const body: ArrayBuffer = new ArrayBuffer(1133825);
    const headers: Map<string, string> = new Map();
    headers.set('Content-Disposition', 'attachment;filename="ESxxALL_20200806_testing99999.txt"');
    const response = {
      headers: {
        get: key => headers.get(key)
      },
      status: 200,
      statusText: 'OK',
      url: 'test',
      ok: true,
      type: 4,
      body
    };

    httpMock.get.and.returnValue(of(response));

    let fileDownloaded: any;
    service.download('13').subscribe(file => (fileDownloaded = file));
    tick();

    expect(fileDownloaded.fileName).toEqual('ESxxALL_20200806_testing99999.txt');
  }));

  it('should sendTaImagesRequest', () => {
    const paramExpected = {
      adhocFile: true,
      includeDeleteAction: true
    };

    service.sendTaImageRequest(true, true);
    expect(httpMock.post).toHaveBeenCalledWith(service['baseUrl_ta_image_request'], paramExpected);
  });

  it('should sendNonTaImagesRequest', () => {
    const param = {
      deleteFile: true,
      globalTaGdsFile: false
    };

    service.sendNonTaImageRequest(param);
    expect(httpMock.post).toHaveBeenCalledWith(service['baseUrl_non_ta_image_request'], param);
  });
  it('should sendNonTaImagesRequest', () => {
    const param = {
      deleteFile: true,
      globalTaGdsFile: false
    };

    service.sendNonTaImageRequest(param);
    expect(httpMock.post).toHaveBeenCalledWith(service['baseUrl_non_ta_image_request'], param);
  });

  it('should sendImageStatusRequest as Airline', () => {
    const expectedParam = {
      airline_code: '111'
    };

    service.sendImageStatusRequest(true, '111');
    expect(httpMock.post).toHaveBeenCalledWith(service['baseUrl_image_status_airline'], expectedParam);
  });

  it('should sendImageStatusRequest as Agent', () => {
    const expectedParam = {
      agent_code: '111'
    };

    service.sendImageStatusRequest(false, '111');
    expect(httpMock.post).toHaveBeenCalledWith(service['baseUrl_image_status_agent'], expectedParam);
  });

  it('should getLastGlobalImageRequest', () => {
    httpMock.get.and.returnValue(of({ adhocFile: false, requestDate: null }));
    service.getLastGlobalImageRequest();
    expect(httpMock.get).toHaveBeenCalledWith(service['baseUrl_ta_image_request'] + '/last');
  });

  it('should getLastGlobalImageRequest raise an error', fakeAsync(() => {
    httpMock.get.and.returnValue(throwError(new Error()));
    let error: Error;
    service.getLastGlobalImageRequest().subscribe(
      _ => {},
      err => (error = err)
    );
    tick();
    expect(error).toBeDefined();
  }));
});
