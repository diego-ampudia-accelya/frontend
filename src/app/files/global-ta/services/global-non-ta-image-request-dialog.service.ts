import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { GlobalTaImageRequestDialogComponent } from '../global-ta-image-request-dialog/global-ta-image-request-dialog.component';
import {
  ImageNonTaRequest,
  ImageNonTaRequestType
} from '../global-ta-image-request-dialog/global-ta-image-request-dialog.constants';
import { GlobalTaService } from '../services/global-ta.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class GlobalNonTaImageRequestDialogService {
  constructor(
    private dialogService: DialogService,
    private dataService: GlobalTaService,
    private notificationService: NotificationService
  ) {}

  public open(): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.title',
        description: 'MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.description',
        footerButtonsType: [{ type: FooterButton.Request }],
        isNonTaRequest: true
      }
    };

    return this.dialogService.open(GlobalTaImageRequestDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Request ? this.sendRequest(dialogConfig, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private sendRequest(config: DialogConfig, component: GlobalTaImageRequestDialogComponent): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    const deleteActionFileTypeValue = component.form.value.requestType === 'true';
    const selection = deleteActionFileTypeValue ? ImageNonTaRequestType.DELETEFILE : ImageNonTaRequestType.NEXTGLOBAL;

    const data: ImageNonTaRequest = {
      deleteFile: deleteActionFileTypeValue,
      globalTaGdsFile: !deleteActionFileTypeValue
    };

    return defer(() => {
      setLoading(true);

      return this.dataService.sendNonTaImageRequest(data).pipe(
        tap(() =>
          this.notificationService.showSuccess(
            `MENU.files.globalTa.dialog.dialogStatus.nonTaRequest.successMessage.${selection}`
          )
        ),
        mapTo(FooterButton.Request),
        finalize(() => setLoading(false))
      );
    });
  }
}
