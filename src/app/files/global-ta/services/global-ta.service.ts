import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, finalize, map, tap } from 'rxjs/operators';

import { ImageNonTaRequest } from '../global-ta-image-request-dialog/global-ta-image-request-dialog.constants';
import { FileInfo } from '~app/files/model';
import { LastImageResponse } from '~app/files/model/last-image-response.model';
import { hasFileTypeDescription, hasNameContaining, hasUploadDateInRange } from '~app/files/shared/filter.utils';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ServerError } from '~app/shared/errors';
import { ClientGridTableHelper, downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class GlobalTaService implements Queryable<FileInfo> {
  public loading$: Observable<boolean>;

  private baseUrl_xml_files = `${this.appConfiguration.baseApiPath}/file-management/xml-files`;
  private baseUrl_ta_image_request = `${this.appConfiguration.baseApiPath}/file-management/files/global-ta-requests`;
  private baseUrl_non_ta_image_request = `${this.appConfiguration.baseApiPath}/file-management/non-ta-image-request`;
  private baseUrl_image_status_airline = `${this.appConfiguration.baseApiPath}/file-management/gds-ta-status-airline`;
  private baseUrl_image_status_agent = `${this.appConfiguration.baseApiPath}/file-management/gds-ta-status-agent`;
  private clientGridTableHelper = new ClientGridTableHelper<FileInfo>();

  private loadingSubject = new BehaviorSubject(false);

  // We need to store the data per component instance so we can modify it when downloading/archiving file.
  // This is needed in order not to send a new request each time a file is downloaded.
  private data: FileInfo[];

  private customFilterFor = {
    type: hasFileTypeDescription,
    name: hasNameContaining,
    uploadDate: hasUploadDateInRange
  };

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {
    this.loading$ = this.loadingSubject.asObservable();
  }

  public find(dataQuery: DataQuery): Observable<PagedData<FileInfo>> {
    const { bsp, ...filterBy } = dataQuery.filterBy;
    dataQuery.sortBy = formatSortBy(dataQuery.sortBy, [{ from: 'type', to: 'type.description' }]);
    dataQuery.filterBy = filterBy;

    return this.getFiles(bsp.id).pipe(
      map((files: FileInfo[]) => {
        // TODO: add BE filter model as generic type to `fromDataQuery` factory method
        const reqQuery = RequestQuery.fromDataQuery(dataQuery);

        return this.clientGridTableHelper.query(files, reqQuery, undefined, this.customFilterFor);
      })
    );
  }

  private getFiles(bspId: string): Observable<FileInfo[]> {
    if (this.data) {
      return of(this.data);
    }

    return this.http.get<FileInfo[]>(this.baseUrl_xml_files, { params: { bspId: bspId?.toString() } }).pipe(
      tap(files => {
        this.data = files;
      })
    );
  }

  public download(id: string): Observable<any> {
    const url = `${this.baseUrl_xml_files}/${id}`;
    this.loadingSubject.next(true);

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(
      map(formatDownloadResponse),
      finalize(() => this.loadingSubject.next(false))
    );
  }

  public sendTaImageRequest(isAdhocRequestType: boolean, includeDeleteAction: boolean): Observable<any> {
    return this.http.post(this.baseUrl_ta_image_request, { adhocFile: isAdhocRequestType, includeDeleteAction });
  }

  public sendNonTaImageRequest(data: ImageNonTaRequest): Observable<any> {
    return this.http.post(this.baseUrl_non_ta_image_request, data);
  }

  public sendImageStatusRequest(isAirlineRequest: boolean, code: string): Observable<any> {
    const url = isAirlineRequest ? this.baseUrl_image_status_airline : this.baseUrl_image_status_agent;
    const params = {
      ...(isAirlineRequest && code && { airline_code: code }),
      ...(!isAirlineRequest && code && { agent_code: code })
    };

    return this.http.post(url, { ...params });
  }

  public getLastGlobalImageRequest(): Observable<LastImageResponse> {
    const url = `${this.baseUrl_ta_image_request}/last`;

    return this.http.get<LastImageResponse>(url).pipe(
      catchError((error: ServerError) => {
        if (error.status === 404) {
          return of(null);
        }

        return throwError(error);
      })
    );
  }
}
