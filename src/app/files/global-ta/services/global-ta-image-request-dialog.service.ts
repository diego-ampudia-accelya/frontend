import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { GlobalTaImageRequestDialogComponent } from '../global-ta-image-request-dialog/global-ta-image-request-dialog.component';
import { ImageRequestType } from '../global-ta-image-request-dialog/global-ta-image-request-dialog.constants';
import { GlobalTaService } from '../services/global-ta.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class GlobalTaImageRequestDialogService {
  constructor(
    private dialogService: DialogService,
    private dataService: GlobalTaService,
    private notificationService: NotificationService
  ) {}

  public open(): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.title',
        description: 'MENU.files.globalTa.dialog.description',
        footerButtonsType: [{ type: FooterButton.Request }],
        isNonTaRequest: false
      }
    };

    return this.dialogService.open(GlobalTaImageRequestDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Request ? this.sendRequest(dialogConfig, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private sendRequest(config: DialogConfig, component: GlobalTaImageRequestDialogComponent): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    const isAdhocRequestType = component.form.value.requestType === 'true';
    const includeDeleteAction = component.form.value.delActionCheckboxControl;

    const selection = isAdhocRequestType ? ImageRequestType.ADHOC : ImageRequestType.NEXTGLOBAL;

    return defer(() => {
      setLoading(true);

      return this.dataService.sendTaImageRequest(isAdhocRequestType, includeDeleteAction).pipe(
        tap(() => this.notificationService.showSuccess(`MENU.files.globalTa.dialog.successMessage.${selection}`)),
        mapTo(FooterButton.Request),
        finalize(() => setLoading(false))
      );
    });
  }
}
