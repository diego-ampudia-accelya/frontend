import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { first } from 'rxjs/operators';

import { IMAGE_NON_TA_REQUEST_FORM, IMAGE_REQUEST_FORM } from './global-ta-image-request-dialog.constants';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DialogConfig } from '~app/shared/components';
import { UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-image-request-dialog',
  templateUrl: './global-ta-image-request-dialog.component.html',
  styleUrls: ['./global-ta-image-request-dialog.component.scss']
})
export class GlobalTaImageRequestDialogComponent implements OnInit {
  public form: FormGroup;

  public radioButtonOptions: { value: string; label: string }[];

  public radioButtonGroup: FormControl;

  public delActionCheckboxControl: FormControl;

  public isUserTypeAirline: boolean;
  public isNonTaRequest: boolean;

  constructor(
    public config: DialogConfig,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {}

  public ngOnInit(): void {
    this.isNonTaRequest = this.config.data.isNonTaRequest;
    this.radioButtonOptions = this.initializeRadioButtons();
    this.initializeUserType();
    this.form = this.buildForm();
  }

  private initializeRadioButtons() {
    const radioButtonOptions = this.isNonTaRequest
      ? IMAGE_NON_TA_REQUEST_FORM.RADIO_BUTTON_OPTIONS
      : IMAGE_REQUEST_FORM.RADIO_BUTTON_OPTIONS;

    return Object.values(radioButtonOptions).map(selection => ({
      value: selection.value,
      label: this.translationService.translate(`MENU.files.globalTa.dialog.radioButtonOptions.${selection.label}`)
    }));
  }

  private initializeUserType() {
    this.store.pipe(select(getUserType), first()).subscribe(userType => {
      this.isUserTypeAirline = userType === UserType.AIRLINE;
    });
  }

  private buildForm(): FormGroup {
    this.delActionCheckboxControl = this.formBuilder.control(false);
    this.radioButtonGroup = new FormControl(this.radioButtonOptions[1].value);

    return this.formBuilder.group({
      requestType: this.radioButtonGroup,
      delActionCheckboxControl: this.delActionCheckboxControl
    });
  }
}
