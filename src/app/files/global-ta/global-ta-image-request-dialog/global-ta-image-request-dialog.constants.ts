export const IMAGE_REQUEST_FORM = {
  RADIO_BUTTON_OPTIONS: [
    {
      value: 'true',
      label: 'adhoc'
    },
    {
      value: 'false',
      label: 'nextGlobal'
    }
  ]
};

export const IMAGE_NON_TA_REQUEST_FORM = {
  RADIO_BUTTON_OPTIONS: [
    {
      value: 'true',
      label: 'deleteFile'
    },
    {
      value: 'false',
      label: 'nextGlobal'
    }
  ]
};

export enum ImageRequestType {
  ADHOC = 'adhoc',
  NEXTGLOBAL = 'nextGlobal'
}

export enum ImageNonTaRequestType {
  DELETEFILE = 'deleteFile',
  NEXTGLOBAL = 'nextGlobal'
}

export interface ImageNonTaRequest {
  deleteFile: boolean;
  globalTaGdsFile: boolean;
}
