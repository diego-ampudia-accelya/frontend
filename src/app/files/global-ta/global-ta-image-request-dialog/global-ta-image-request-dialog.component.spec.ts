import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { GlobalTaImageRequestDialogComponent } from './global-ta-image-request-dialog.component';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { Permissions } from '~app/shared/constants';
import { User, UserType } from '~app/shared/models/user.model';

const expectedUserDetails: Partial<User> = {
  id: 10126,
  email: 'airline1@example.com',
  firstName: 'firstName',
  lastName: 'lastName',
  userType: UserType.AIRLINE,
  permissions: [Permissions.lean],
  bspPermissions: [
    { bspId: 1, permissions: [Permissions.lean] },
    { bspId: 2, permissions: [] }
  ],
  bsps: [
    { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
    { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
  ]
};

const initialState = {
  auth: { user: expectedUserDetails },
  core: {
    menu: {
      tabs: {}
    }
  },
  refund: {}
};

const mockConfig = {
  data: {
    title: 'title',
    description: 'description',
    footerButtonsType: FooterButton.Request,
    isNonTaRequest: false
  }
};

describe('GlobalTAImageRequestDialogComponent', () => {
  let component: GlobalTaImageRequestDialogComponent;
  let fixture: ComponentFixture<GlobalTaImageRequestDialogComponent>;
  let mockStore: MockStore;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GlobalTaImageRequestDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: getUserType,
              value: expectedUserDetails.userType
            }
          ]
        }),
        FormBuilder,
        { provide: DialogConfig, useValue: mockConfig },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalTaImageRequestDialogComponent);
    component = fixture.componentInstance;
    translationServiceSpy.translate.and.callFake(identity);
    mockStore = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize component when is a TA Request as Airline', fakeAsync(() => {
    const formExpected = {
      requestType: 'false',
      delActionCheckboxControl: false
    };

    component.ngOnInit();
    tick();
    expect(component.isNonTaRequest).toBeFalsy();
    expect(component.form.value).toEqual(formExpected);
    expect(component.isUserTypeAirline).toBeTruthy();
  }));

  it('should initialize component when is a TA Request as GDS', fakeAsync(() => {
    mockStore.overrideSelector(getUserType, UserType.GDS);
    const formExpected = {
      requestType: 'false',
      delActionCheckboxControl: false
    };
    component.ngOnInit();
    tick();
    expect(component.isNonTaRequest).toBeFalsy();
    expect(component.form.value).toEqual(formExpected);
    expect(component.isUserTypeAirline).toBeFalsy();
  }));

  it('should initialize component when is a NON TA Request as GDS', fakeAsync(() => {
    component.config = {
      data: {
        ...mockConfig.data,
        isNonTaRequest: true
      }
    };
    const formExpected = {
      requestType: 'false',
      delActionCheckboxControl: false
    };
    component.ngOnInit();
    tick();
    expect(component.isNonTaRequest).toBeTruthy();
    expect(component.form.value).toEqual(formExpected);
    expect(component.isUserTypeAirline).toBeTruthy();
  }));
});
