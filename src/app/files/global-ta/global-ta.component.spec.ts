import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { FileType } from '../model';
import { FileTypesService } from '../shared/file-types.service';
import { ReportsDisplayFilterFormatter } from '../shared/reports-display-filter-formatter';
import { GlobalTaStatusDialogComponent } from './global-ta-status-dialog/global-ta-status-dialog.component';
import { GlobalTaComponent } from './global-ta.component';
import { GlobalNonTaImageRequestDialogService } from './services/global-non-ta-image-request-dialog.service';
import { GlobalTaImageRequestDialogService } from './services/global-ta-image-request-dialog.service';
import { GlobalTaService } from './services/global-ta.service';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ViewType } from '~app/master-data/ticketing-authority';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService, FooterButton } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { DropdownOption } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  AppConfigurationService,
  NotificationService
} from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { TranslatePipeMock } from '~app/test';

describe('GlobalTaComponent', () => {
  let fixture: ComponentFixture<GlobalTaComponent>;
  let component: GlobalTaComponent;

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [
      Permissions.readGlobalTicketingAuthority,
      Permissions.createNonTicketingAuthority,
      Permissions.createNonTicketingAuthority,
      Permissions.lean
    ],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readGlobalTicketingAuthority] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    }
  };

  const expectedFileTypes: DropdownOption<FileType>[] = [
    {
      label: 'AGENT COMMUNICATIONS',
      value: { descriptor: '11', description: 'AGENT COMMUNICATIONS' }
    },
    {
      label: 'ERN Input files',
      value: { descriptor: '13', description: 'ERN Input files' }
    },
    {
      label: 'IATA Important Information',
      value: { descriptor: '12', description: 'IATA Important Information' }
    },
    {
      label: 'ac',
      value: { descriptor: 'ac', description: 'ac' }
    }
  ];

  const dataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const fileTypesServiceSpy = createSpyObject(FileTypesService);
  const globalTaServiceSpy = createSpyObject(GlobalTaService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const globalTaImageRequestDialogServiceSpy = createSpyObject(GlobalTaImageRequestDialogService);
  const globalNonTaImageRequestDialogServiceSpy = createSpyObject(GlobalNonTaImageRequestDialogService);
  const dialogServiceSpy = createSpyObject(DialogService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig)],
      declarations: [GlobalTaComponent, TranslatePipeMock, HasPermissionPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getUser, value: expectedUserDetails }]
        }),
        PermissionsService,
        ReportsDisplayFilterFormatter,
        BytesFormatPipe,
        FormBuilder,
        AppConfigurationService,
        LocalBspTimeService,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: FileTypesService, useValue: fileTypesServiceSpy },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: GlobalTaImageRequestDialogService, useValue: globalTaImageRequestDialogServiceSpy },
        { provide: GlobalNonTaImageRequestDialogService, useValue: globalNonTaImageRequestDialogServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    })
      .overrideComponent(GlobalTaComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: dataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: GlobalTaService, useValue: globalTaServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalTaComponent);
    component = fixture.componentInstance;
    globalTaServiceSpy.getLastGlobalImageRequest.and.returnValue(of({ adhocFile: true, requestDate: 'test' }));
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should set correct columns on init as an AGENT', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'name',
        name: 'MENU.files.reports.columns.fileName',
        cellTemplate: jasmine.anything(),
        flexGrow: 4
      }),
      jasmine.objectContaining({
        prop: 'type',
        name: 'MENU.files.reports.columns.fileType',
        pipe: jasmine.anything(),
        flexGrow: 3
      }),
      jasmine.objectContaining({
        prop: 'uploadDate',
        name: 'MENU.files.reports.columns.uploadDate',
        pipe: jasmine.anything(),
        flexGrow: 2
      }),
      jasmine.objectContaining({
        prop: 'size',
        name: 'MENU.files.reports.columns.size',
        pipe: jasmine.anything(),
        flexGrow: 1
      })
    ]);

    component.ngOnInit();
    tick();
    expect(component.columns).toEqual(expectedColumns);
  }));

  it('should buildAction', () => {
    component.ngOnInit();

    expect(component.tableActions).toEqual([{ action: GridTableActionType.Download }]);
  });

  it('should getLastGlobalImageRequest', () => {
    component.ngOnInit();

    expect(component.lastImageRequestText).toEqual('MENU.files.globalTa.subtitle.adhoc');
  });

  it('should buttonOptionActions', () => {
    const buttonOptionActionsExpected = {
      mainOption: {
        label: 'MENU.files.globalTa.button.addTaImage',
        command: jasmine.anything()
      },
      options: [
        {
          label: 'MENU.files.globalTa.button.requestNonTaGds',
          command: jasmine.anything(),
          visible: false
        },
        {
          label: 'MENU.files.globalTa.button.requestTaStatusAirline',
          command: jasmine.anything(),
          visible: false
        },
        {
          label: 'MENU.files.globalTa.button.requestTaStatusAgent',
          command: jasmine.anything(),
          visible: false
        }
      ]
    };
    component.ngOnInit();

    expect(component.buttonOptionActions).toEqual(buttonOptionActionsExpected);
  });

  it('should onFilterButtonClicked', fakeAsync(() => {
    fileTypesServiceSpy.getXmlFileTypeOption.and.returnValue(of(expectedFileTypes));
    component.onFilterButtonClicked(true);
    tick();
    let result;
    component.fileTypeOptions.subscribe(data => (result = data));
    expect(result).toEqual(expectedFileTypes);
  }));

  it('should onRowLinkClick', () => {
    globalTaServiceSpy.download.and.returnValue(of({ blob: 'data', name: 'fileName' }));
    component.onRowLinkClick({ id: '1', name: 'test', type: { id: '2', description: 'ac' }, uploadDate: '01/01/2022' });

    expect(globalTaServiceSpy.download).toHaveBeenCalled();
    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
  });

  it('should onActionClick', () => {
    globalTaServiceSpy.download.and.returnValue(of({ blob: 'data', name: 'fileName' }));
    component.onActionClick({
      action: { actionType: GridTableActionType.Download, methodName: 'test' },
      event: null,
      row: { id: '1', name: 'test', type: { id: '2', description: 'ac' }, uploadDate: '01/01/2022' }
    });

    expect(globalTaServiceSpy.download).toHaveBeenCalled();
    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
  });

  it('should onGlobalImageRequest', () => {
    globalTaImageRequestDialogServiceSpy.open.and.returnValue(of());
    component.onGlobalImageRequest();

    expect(globalTaImageRequestDialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should onNonTaImageRequest', () => {
    globalNonTaImageRequestDialogServiceSpy.open.and.returnValue(of());
    component.onNonTaImageRequest();

    expect(globalNonTaImageRequestDialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should onTaStatusAirlineRequest', () => {
    dialogServiceSpy.open.and.returnValue(of());
    const dialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.dialogStatus.airline.title',
        footerButtonsType: [{ type: FooterButton.Request, isDisabled: true }]
      },
      requestType: ViewType.Airline,
      currentBsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' },
      apiService: jasmine.anything()
    };
    component.onTaStatusAirlineRequest();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(GlobalTaStatusDialogComponent, dialogConfig);
  });

  it('should onTaStatusAgentRequest', () => {
    dialogServiceSpy.open.and.returnValue(of());
    const dialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.dialogStatus.agent.title',
        footerButtonsType: [{ type: FooterButton.Request, isDisabled: true }]
      },
      requestType: ViewType.Agent,
      currentBsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' },
      apiService: jasmine.anything()
    };
    component.onTaStatusAgentRequest();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(GlobalTaStatusDialogComponent, dialogConfig);
  });
});
