import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import FileSaver from 'file-saver';
import { isEmpty } from 'lodash';
import { MenuItem } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';

import { FileInfo } from '../model';
import { LastImageResponse } from '../model/last-image-response.model';
import { FileTypesService } from '../shared/file-types.service';
import { ReportsDisplayFilterFormatter } from '../shared/reports-display-filter-formatter';
import { ImageRequestType } from './global-ta-image-request-dialog/global-ta-image-request-dialog.constants';
import { GlobalTaStatusDialogComponent } from './global-ta-status-dialog/global-ta-status-dialog.component';
import { GlobalNonTaImageRequestDialogService } from './services/global-non-ta-image-request-dialog.service';
import { GlobalTaImageRequestDialogService } from './services/global-ta-image-request-dialog.service';
import { GlobalTaService } from './services/global-ta.service';
import { getUser, getUserBsps } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ViewType } from '~app/master-data/ticketing-authority';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { AgentDictionaryService, AirlineDictionaryService, NotificationService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-global-ta',
  templateUrl: 'global-ta.component.html',
  styleUrls: ['./global-ta.component.scss'],
  providers: [
    DefaultQueryStorage,
    DatePipe,
    GlobalTaService,
    { provide: QUERYABLE, useExisting: GlobalTaService },
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [GlobalTaService] }
  ]
})
export class GlobalTaComponent implements OnInit {
  private formFactory: FormUtil;
  private getCurrentUserBsp$: Observable<BspDto> = this.store.pipe(
    select(getUserBsps),
    first(),
    map(bsps => bsps && bsps[0]),
    tap(bsp => (this.currentUserBsp = bsp))
  );

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  public loggedUser: User;
  public isGdsUserType: boolean;

  public ButtonDesign = ButtonDesign;
  public title = 'MENU.files.globalTa.title';
  public columns: GridColumn[] = [];
  public currentUserBsp: BspDto;
  public searchForm: FormGroup;
  public totalItems$ = this.dataSource.appliedQuery$.pipe(map(query => query.paginateBy.totalElements));
  public fileTypeOptions: Observable<DropdownOption[]>;
  public maxDate = this.localBspTimeService.getDate();
  public tableActions = [];

  public hasReadGlobalFilesPermission = this.permissionsService.hasPermission(Permissions.readGlobalTicketingAuthority);
  public readLastGlobalTicketingAuthority = this.permissionsService.hasPermission(
    Permissions.readLastGlobalTicketingAuthority
  );
  public hasCreateGlobalRequestPermission = this.permissionsService.hasPermission(
    Permissions.createGlobalTicketingAuthority
  );
  public hasCreateNonTicketingAuthority = this.permissionsService.hasPermission(
    Permissions.createNonTicketingAuthority
  );
  public hasCreateTicketingAuthorityStatus = this.permissionsService.hasPermission(
    Permissions.createNonTicketingAuthority
  );
  public isButtonTaImageAvailable =
    this.hasCreateGlobalRequestPermission ||
    this.hasCreateNonTicketingAuthority ||
    this.hasCreateTicketingAuthorityStatus;

  public buttonOptionActions: { options: MenuItem[]; mainOption: MenuItem } = {
    mainOption: {
      label: this.translationService.translate('MENU.files.globalTa.button.addTaImage'),
      command: () => this.onGlobalImageRequest()
    },
    options: [
      {
        label: this.translationService.translate('MENU.files.globalTa.button.requestNonTaGds'),
        command: () => this.onNonTaImageRequest(),
        visible: !this.hasCreateNonTicketingAuthority
      },
      {
        label: this.translationService.translate('MENU.files.globalTa.button.requestTaStatusAirline'),
        command: () => this.onTaStatusAirlineRequest(),
        visible: !this.hasCreateTicketingAuthorityStatus
      },
      {
        label: this.translationService.translate('MENU.files.globalTa.button.requestTaStatusAgent'),
        command: () => this.onTaStatusAgentRequest(),
        visible: !this.hasCreateTicketingAuthorityStatus
      }
    ]
  };

  public lastImageRequest: LastImageResponse;
  public lastImageRequestText: string;

  public isLoading$: Observable<boolean> = combineLatest([
    this.dataSource.loading$,
    this.globalTaService.loading$
  ]).pipe(map(([isDataLoading, isDownloading]) => isDataLoading || isDownloading));

  constructor(
    public dataSource: QueryableDataSource<GlobalTaComponent>,
    public filterFormatter: ReportsDisplayFilterFormatter,
    private datePipe: DatePipe,
    private bytesFormatPipe: BytesFormatPipe,
    private queryStorage: DefaultQueryStorage,
    private store: Store<AppState>,
    private globalTaService: GlobalTaService,
    private formBuilder: FormBuilder,
    private localBspTimeService: LocalBspTimeService,
    private fileTypeService: FileTypesService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private permissionsService: PermissionsService,
    private globalTaImageRequestDialogService: GlobalTaImageRequestDialogService,
    private globalNonTaImageRequestDialogService: GlobalNonTaImageRequestDialogService,
    private dialogService: DialogService,
    private airlineDictionaryService: AirlineDictionaryService,
    private agentDictionaryService: AgentDictionaryService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
    this.initializeLoggedUser();
  }

  ngOnInit(): void {
    this.columns = this.buildColumns();
    this.searchForm = this.buildSearchForm();
    this.tableActions = this.buildActions();
    this.getLastGlobalImageRequest();

    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  private initializeLoggedUser() {
    this.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.isGdsUserType = loggedUser.userType === UserType.GDS;
    });
  }

  public loadData(query?: DataQuery): void {
    query = query || defaultQuery;

    if (isEmpty(query.sortBy)) {
      const sortBy = [{ attribute: 'uploadDate', sortType: SortOrder.Desc }];
      query = { ...query, sortBy };
    }

    this.getCurrentUserBsp$.subscribe(bsp => {
      const dataQuery = {
        ...query,
        filterBy: {
          ...query.filterBy,
          bsp
        }
      };

      this.dataSource.get(dataQuery);
      this.queryStorage.save(dataQuery);
    });
  }

  private getLastGlobalImageRequest() {
    this.globalTaService.getLastGlobalImageRequest().subscribe(response => {
      if (response) {
        this.lastImageRequest = response;
        this.lastImageRequestText = response.adhocFile
          ? `MENU.files.globalTa.subtitle.${ImageRequestType.ADHOC}`
          : `MENU.files.globalTa.subtitle.${ImageRequestType.NEXTGLOBAL}`;
      }
    });
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.fileTypeOptions = this.fileTypeService.getXmlFileTypeOption(this.currentUserBsp.id.toString());
    }
  }

  public onRowLinkClick(row: FileInfo) {
    this.saveFile(row);
  }

  public onActionClick({ action, row }: { event: Event; action: GridTableAction; row: FileInfo }) {
    if (action.actionType === GridTableActionType.Download) {
      this.saveFile(row);
    }
  }

  private buildActions(): Array<{ action: GridTableActionType; disabled?: boolean }> {
    return [{ action: GridTableActionType.Download }];
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'name',
        name: 'MENU.files.reports.columns.fileName',
        cellTemplate: 'commonLinkFromObjCellTmpl',
        flexGrow: 4
      },
      {
        prop: 'type',
        name: 'MENU.files.reports.columns.fileType',
        pipe: {
          transform: value => value.description
        },
        flexGrow: 3
      },
      {
        prop: 'uploadDate',
        name: 'MENU.files.reports.columns.uploadDate',
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        },
        flexGrow: 2
      },
      {
        prop: 'size',
        name: 'MENU.files.reports.columns.size',
        pipe: {
          transform: value => this.bytesFormatPipe.transform(value)
        },
        flexGrow: 1
      }
    ];
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<FileInfo>({
      name: [],
      type: [],
      uploadDate: []
    });
  }

  public saveFile(fileInfo: FileInfo) {
    this.globalTaService.download(fileInfo.id).subscribe(file => {
      FileSaver.saveAs(file.blob, fileInfo.name);
      this.notificationService.showSuccess(
        this.translationService.translate('MENU.files.globalTa.successfullDownload', { fileName: fileInfo.name })
      );
    });
  }

  //* Open dialog to request new global image
  public onGlobalImageRequest(): void {
    this.globalTaImageRequestDialogService.open().subscribe();
  }

  //* Open dialog to request new non-TA image
  public onNonTaImageRequest(): void {
    this.globalNonTaImageRequestDialogService.open().subscribe();
  }

  public onTaStatusAirlineRequest(): void {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.dialogStatus.airline.title',
        footerButtonsType: [{ type: FooterButton.Request, isDisabled: true }]
      },
      requestType: ViewType.Airline,
      currentBsp: this.currentUserBsp,
      apiService: this.airlineDictionaryService
    };

    this.dialogService.open(GlobalTaStatusDialogComponent, dialogConfig);
  }

  public onTaStatusAgentRequest(): void {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.files.globalTa.dialog.dialogStatus.agent.title',
        footerButtonsType: [{ type: FooterButton.Request, isDisabled: true }]
      },
      requestType: ViewType.Agent,
      currentBsp: this.currentUserBsp,
      apiService: this.agentDictionaryService
    };

    this.dialogService.open(GlobalTaStatusDialogComponent, dialogConfig);
  }
}
