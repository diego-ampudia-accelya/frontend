import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, defer, Observable, Subject } from 'rxjs';
import { filter, finalize, first, takeUntil, tap } from 'rxjs/operators';

import { GlobalTaService } from '../services/global-ta.service';
import { ViewType } from '~app/master-data/ticketing-authority';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AgentDictionaryQueryFilters, AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-global-ta-status-dialog.component',
  templateUrl: './global-ta-status-dialog.component.html',
  styleUrls: ['./global-ta-status-dialog.component.scss']
})
export class GlobalTaStatusDialogComponent implements OnInit, OnDestroy {
  public isFormReady$: Observable<boolean>;
  public descriptionLabel: string;
  public fieldLabel: string;
  public placeholderLabel: string;

  public form: FormGroup;

  public fieldControl: FormControl;
  public isAirlineRequestType: boolean;
  public currentUserBsp: BspDto;

  public dropdownOptions$: Observable<DropdownOption<AirlineSummary | AgentSummary>[]>;

  private _isFormReadySubject = new BehaviorSubject<boolean>(false);

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    private dialog: DialogService,
    private globalTaService: GlobalTaService,
    private notificationService: NotificationService,
    private cd: ChangeDetectorRef
  ) {
    this.isFormReady$ = this._isFormReadySubject.asObservable();
  }

  public ngOnInit(): void {
    this.isAirlineRequestType = this.config.requestType === ViewType.Airline;
    this.currentUserBsp = this.config.currentBsp;
    this.descriptionLabel = `MENU.files.globalTa.dialog.dialogStatus.${this.config.requestType}.description`;
    this.fieldLabel = `MENU.files.globalTa.dialog.dialogStatus.${this.config.requestType}.form.label`;
    this.placeholderLabel = `MENU.files.globalTa.dialog.dialogStatus.${this.config.requestType}.form.placeholder`;
    this.buildForm();

    this.initializeDropdownOptions();
    this.initializeButtonsListener();
    this.initializeFormListener();
  }

  private buildForm(): void {
    this.fieldControl = this.formBuilder.control(null, Validators.required);

    this.form = this.formBuilder.group({
      field: this.fieldControl
    });
  }

  private initializeDropdownOptions(): void {
    const param = this.isAirlineRequestType
      ? this.currentUserBsp?.id
      : ({ bspId: this.currentUserBsp?.id } as AgentDictionaryQueryFilters);
    //  dropdown options
    this.dropdownOptions$ = this.config.apiService.getDropdownOptions(param).pipe(
      first(),
      tap(_ => {
        this._isFormReadySubject.next(true);
        this.cd.detectChanges();
      })
    );
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const requestButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Request
      );

      requestButton.isDisabled = status === 'INVALID';
    });
  }

  private initializeButtonsListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        filter(({ clickedBtn }) => clickedBtn !== FooterButton.Request || this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Request:
            this.sendImageStatus(this.isAirlineRequestType, this.fieldControl.value?.code).subscribe();
            break;
          case FooterButton.Cancel:
            this.dialog.close();
            break;
        }
      });
  }

  private sendImageStatus(isAirlineRequest: boolean, code: string) {
    const setLoading = (loading: boolean) =>
      this.config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.globalTaService.sendImageStatusRequest(isAirlineRequest, code).pipe(
        tap(() => this.notificationService.showSuccess(`MENU.files.globalTa.dialog.dialogStatus.successMessage`)),
        finalize(() => {
          setLoading(false);
          this.dialog.close();
        })
      );
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
