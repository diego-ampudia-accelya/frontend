import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import moment from 'moment-mini';
import { Observable } from 'rxjs';

import { DownloadedFilesFilter } from '../model/downloaded-files-filter.model';
import { DownloadedFile } from '../model/downloaded-files.model';
import { sameMonthValidator } from '../uploaded-files/validators/same-month.validator';
import { ReceiverTypeOption } from './model/receiver-type';
import { RECEIVER_TYPES_OPTIONS } from './model/receiver-types-options';
import { DownloadedFilesFilterFormatter } from './services/downloaded-files-filter-formatter';
import { DownloadedFilesService } from './services/downloaded-files.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { FileDescriptorDictionaryService } from '~app/shared/services/dictionary/file-descriptor-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-downloaded-files',
  templateUrl: './downloaded-files.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: DownloadedFilesService }
  ]
})
export class DownloadedFilesComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public fileTypesOptions$: Observable<DropdownOption[]>;
  public receiverTypesOptions$: Observable<DropdownOption[]>;
  public userBspId: number;
  public receiverTypesOptions: DropdownOption<ReceiverTypeOption>[] = Object.values(RECEIVER_TYPES_OPTIONS).map(
    option => ({
      value: option.value,
      label: this.translationService.translate(`${option.label}`)
    })
  );

  public maxDate = moment().endOf('day').toDate();
  public minDate = moment(this.maxDate).subtract(2, 'years').toDate();

  public predefinedFilters: {
    downloadDate: Date[];
  };

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private downloadedDateInitialValue: Date[];

  constructor(
    public displayFormatter: DownloadedFilesFilterFormatter,
    public dataSource: QueryableDataSource<DownloadedFile>,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private queryStorage: DefaultQueryStorage,
    private permissionsService: PermissionsService,
    private fileDescriptorDictionary: FileDescriptorDictionaryService,
    private downloadedFilesService: DownloadedFilesService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private localBspTimeService: LocalBspTimeService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.initializeLoggedUserFeatures();
    this.initializeFileTypeDropdown();

    this.initColumns();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.files.downloadedFiles.downloadedFilesDownloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.downloadedFilesService
    });
  }

  public loadData(query: DataQuery<DownloadedFilesFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private initializeLoggedUserFeatures() {
    const localBspDate = this.localBspTimeService.getDate();
    const firtDayOfMonthBspTime = new Date(localBspDate.getFullYear(), localBspDate.getMonth(), 1);
    this.downloadedDateInitialValue = [firtDayOfMonthBspTime, localBspDate];
    this.checkPermissionAndLoadData();
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      downloadDate: this.downloadedDateInitialValue
    };
  }

  private checkPermissionAndLoadData(): void {
    const hasReadDownloadedFilesPermissions = this.permissionsService.hasPermission(Permissions.readDownloadedFiles);
    if (hasReadDownloadedFilesPermissions) {
      this.initializeDataQuery();
    }
  }

  private initializeDataQuery(): void {
    this.setPredefinedFilters();
    this.loadData(this.storedQuery);
  }

  private initializeFileTypeDropdown(): void {
    this.fileTypesOptions$ = this.fileDescriptorDictionary.getDropdownOptions();
    this.receiverTypesOptions$ = this.fileDescriptorDictionary.getDropdownOptions();
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<DownloadedFilesFilter>({
      downloadDate: ['', sameMonthValidator()],
      fileTypes: [],
      receiverTypes: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'name',
        name: 'MENU.files.downloadedFiles.columns.fileName',
        flexGrow: 4
      },
      {
        prop: 'type',
        name: 'MENU.files.downloadedFiles.columns.fileType',
        resizeable: true,
        flexGrow: 4
      },
      {
        prop: 'downloadDate',
        name: 'MENU.files.downloadedFiles.columns.downloadDate',
        flexGrow: 1,
        resizeable: true,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        }
      },
      {
        prop: 'receiverType',
        name: 'MENU.files.downloadedFiles.columns.receiverType',
        flexGrow: 1
      },
      {
        prop: 'receiverUserId',
        name: 'MENU.files.downloadedFiles.columns.receiverUserId',
        flexGrow: 1
      }
    ];
  }
}
