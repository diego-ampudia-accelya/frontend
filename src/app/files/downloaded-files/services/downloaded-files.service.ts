import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DownloadedFilesFilter, DownloadedFilesFilterBE } from '~app/files/model/downloaded-files-filter.model';
import { DownloadedFile } from '~app/files/model/downloaded-files.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class DownloadedFilesService implements Queryable<DownloadedFile> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/downloaded-files`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<DownloadedFilesFilter>): Observable<PagedData<DownloadedFile>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<DownloadedFile>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery<DownloadedFilesFilter>>): RequestQuery<DownloadedFilesFilterBE> {
    const { downloadDate, receiverTypes, fileTypes, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromDownloadDate: downloadDate && toShortIsoDate(downloadDate[0]),
        toDownloadDate: downloadDate && toShortIsoDate(downloadDate[1]),
        receiverType: receiverTypes && receiverTypes.map(r => r.receiverType),
        descriptor: fileTypes && fileTypes.map(f => f.descriptor)
      }
    });
  }

  public download(
    query: DataQuery<DownloadedFilesFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
