import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { DownloadedFilesFilter } from '~app/files/model/downloaded-files-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class DownloadedFilesFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: DownloadedFilesFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<DownloadedFilesFilter>> = {
      downloadDate: downloadDate => `${this.translate('downloadDate')} - ${rangeDateFilterTagMapper(downloadDate)}`,
      fileTypes: type => `${this.translate('fileTypes')} - ${mapJoinMapper(type, 'description')}`,
      receiverTypes: receiverType =>
        `${this.translate('receiverTypes')} - ${mapJoinMapper(receiverType, 'receiverType')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.files.downloadedFiles.filters.${key}`);
  }
}
