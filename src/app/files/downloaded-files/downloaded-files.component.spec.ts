import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { DownloadedFilesComponent } from './downloaded-files.component';
import { DownloadedFilesFilterFormatter } from './services/downloaded-files-filter-formatter';
import { DownloadedFilesService } from './services/downloaded-files.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

describe('DownloadedFilesComponent', () => {
  let component: DownloadedFilesComponent;
  let fixture: ComponentFixture<DownloadedFilesComponent>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const downloadedFilesServiceSpy: SpyObject<DownloadedFilesService> = createSpyObject(DownloadedFilesService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { downloadedFilesQuery: { ...ROUTES.BSP_CURRENCY_ASSIGNATION, id: 'downloadedFilesQuery' } },
        activeTabId: 'downloadedFilesQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DownloadedFilesComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        DownloadedFilesFilterFormatter,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy },
        FormBuilder
      ]
    })
      .overrideComponent(DownloadedFilesComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: DownloadedFilesService, useValue: downloadedFilesServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadedFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search initialize logged user features', () => {
    const initializeLoggedUserFeaturesSpy = spyOn<any>(component, 'initializeLoggedUserFeatures');
    permissionsServiceSpy.hasPermission.and.returnValue(true);

    component.ngOnInit();

    expect(initializeLoggedUserFeaturesSpy).toHaveBeenCalled();
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
