import { ReceiverTypeOption } from './receiver-type';
import { DropdownOption } from '~app/shared/models';

export const RECEIVER_TYPES_OPTIONS: DropdownOption<ReceiverTypeOption>[] = [
  {
    value: { receiverType: 'Airline', receiverTypeId: 0 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.airline'
  },
  {
    value: { receiverType: 'Agent', receiverTypeId: 1 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.agent'
  },
  {
    value: { receiverType: 'IATA', receiverTypeId: 2 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.iata'
  },
  {
    value: { receiverType: 'DPC', receiverTypeId: 3 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.dpc'
  },
  {
    value: { receiverType: 'EARS', receiverTypeId: 4 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.ears'
  },
  {
    value: { receiverType: 'GDS', receiverTypeId: 5 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.gds'
  },
  {
    value: { receiverType: 'Third Party', receiverTypeId: 7 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.thirdParty'
  },
  {
    value: { receiverType: 'Agent Group', receiverTypeId: 8 },
    label: 'MENU.files.downloadedFiles.filters.receiverTypesDropdownLabels.agentGroup'
  }
];
