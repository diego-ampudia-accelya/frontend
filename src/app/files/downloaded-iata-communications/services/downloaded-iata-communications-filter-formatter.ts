import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { DownloadedIataCommunicationFilter } from '~app/files/model/downloaded-iata-communications-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { joinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class DownloadedIataCommunicationsFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: DownloadedIataCommunicationFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<DownloadedIataCommunicationFilter>> = {
      downloadDate: downloadDate => `${this.translate('downloadDate')} - ${rangeDateFilterTagMapper(downloadDate)}`,
      receiverTypes: receiverTypes =>
        `${this.translate('receiverTypes.title')} - ${joinMapper(
          receiverTypes.map(receiverType => this.translate(receiverType.translationKey))
        )}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public translate(key: string): string {
    return this.translation.translate(`MENU.files.downloadedIataCommunications.filters.${key}`);
  }
}
