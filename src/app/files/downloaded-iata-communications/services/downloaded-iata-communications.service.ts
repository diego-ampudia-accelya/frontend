import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  DownloadedIataCommunicationFilter,
  DownloadedIataCommunicationFilterBE
} from '~app/files/model/downloaded-iata-communications-filter.model';
import { DownloadedIataCommunication } from '~app/files/model/downloaded-iata-communications.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class DownloadedIataCommunicationsService implements Queryable<DownloadedIataCommunication> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/downloaded-communications`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(
    query?: DataQuery<DownloadedIataCommunicationFilter>
  ): Observable<PagedData<DownloadedIataCommunication>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<DownloadedIataCommunication>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<DownloadedIataCommunicationFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(
    query: Partial<DataQuery<DownloadedIataCommunicationFilter>>
  ): RequestQuery<DownloadedIataCommunicationFilterBE> {
    const { downloadDate, receiverTypes, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromDownloadDate: downloadDate?.[0] && toShortIsoDate(downloadDate[0]),
        toDownloadDate: downloadDate?.[1] && toShortIsoDate(downloadDate[1]),
        receiverType: receiverTypes && receiverTypes.map(v => v.value)
      }
    });
  }
}
