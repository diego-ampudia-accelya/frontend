import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash/fp';
import moment from 'moment-mini';

import {
  DownloadedIataCommunicationFilter,
  DownloadedIataCommunicationOptionForTranslate,
  ReceiverTypes
} from '../model/downloaded-iata-communications-filter.model';
import { SpecialFile } from '../model/special-files.model';
import { sameMonthValidator } from '../uploaded-files/validators/same-month.validator';
import { DownloadedIataCommunicationsFilterFormatter } from './services/downloaded-iata-communications-filter-formatter';
import { DownloadedIataCommunicationsService } from './services/downloaded-iata-communications.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-downloaded-iata-communications',
  templateUrl: './downloaded-iata-communications.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: DownloadedIataCommunicationsService }
  ]
})
export class DownloadedIataCommunicationsComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];

  public receiverTypesOptions = this.getEnumOptions(ReceiverTypes, 'receiverTypes');
  public maxDate = moment().endOf('day').toDate();
  public minDate = moment(this.maxDate).subtract(2, 'years').toDate();
  public predefinedFilters: {
    downloadDate: Date[];
  };

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private downloadedDateInitialValue: Date[];

  constructor(
    public displayFormatter: DownloadedIataCommunicationsFilterFormatter,
    public dataSource: QueryableDataSource<SpecialFile>,
    private queryStorage: DefaultQueryStorage,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private permissionsService: PermissionsService,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private downloadedIataCommunicationsService: DownloadedIataCommunicationsService,
    private localBspTimeService: LocalBspTimeService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.initializeLoggedUserFeatures();

    this.initColumns();
  }

  private initializeLoggedUserFeatures() {
    const localBspDate = this.localBspTimeService.getDate();
    const firtDayOfMonthBspTime = new Date(localBspDate.getFullYear(), localBspDate.getMonth(), 1);
    this.downloadedDateInitialValue = [firtDayOfMonthBspTime, localBspDate];
    this.checkPermissionAndLoadData();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.files.downloadedIataCommunications.downloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.downloadedIataCommunicationsService
    });
  }

  public loadData(query: DataQuery<DownloadedIataCommunicationFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      downloadDate: this.downloadedDateInitialValue
    };
  }

  private checkPermissionAndLoadData(): void {
    if (this.permissionsService.hasPermission(Permissions.readDownloadedIataCommunications)) {
      this.setPredefinedFilters();
      this.loadData(this.storedQuery);
    }
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<DownloadedIataCommunicationFilter>({
      downloadDate: ['', sameMonthValidator()],
      receiverTypes: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'name',
        name: 'MENU.files.downloadedIataCommunications.columns.fileName',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'downloadDate',
        name: 'MENU.files.downloadedIataCommunications.columns.downloadDate',
        width: 140,
        resizeable: true,
        draggable: false,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        }
      },
      {
        prop: 'receiverType',
        name: 'MENU.files.downloadedIataCommunications.columns.receiverType',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'receiverUserId',
        name: 'MENU.files.downloadedIataCommunications.columns.receiverUserId',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'receiverName',
        name: 'MENU.files.downloadedIataCommunications.columns.receiverName',
        width: 140,
        resizeable: true,
        draggable: false
      }
    ];
  }

  private getEnumOptions(
    options: any,
    filterName: string
  ): DropdownOption<DownloadedIataCommunicationOptionForTranslate>[] {
    return Object.keys(options).map((key: string) => ({
      value: { translationKey: `${filterName}.options.${key}`, value: options[key] },
      label: this.displayFormatter.translate(`${filterName}.options.${key}`)
    }));
  }
}
