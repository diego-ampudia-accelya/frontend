import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { DownloadedIataCommunicationsComponent } from './downloaded-iata-communications.component';
import { DownloadedIataCommunicationsFilterFormatter } from './services/downloaded-iata-communications-filter-formatter';
import { DownloadedIataCommunicationsService } from './services/downloaded-iata-communications.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

describe('DownloadedIataCommunicationsComponent', () => {
  let component: DownloadedIataCommunicationsComponent;
  let fixture: ComponentFixture<DownloadedIataCommunicationsComponent>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const downloadedIataCommunicationsServiceSpy: SpyObject<DownloadedIataCommunicationsService> = createSpyObject(
    DownloadedIataCommunicationsService
  );

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {
          downloadedIataCommunicationsQuery: {
            ...ROUTES.BSP_CURRENCY_ASSIGNATION,
            id: 'downloadedIataCommunicationsQuery'
          }
        },
        activeTabId: 'downloadedIataCommunicationsQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DownloadedIataCommunicationsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        DownloadedIataCommunicationsFilterFormatter,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy },
        FormBuilder
      ]
    })
      .overrideComponent(DownloadedIataCommunicationsComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: DownloadedIataCommunicationsService, useValue: downloadedIataCommunicationsServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadedIataCommunicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');
    permissionsServiceSpy.hasPermission.and.returnValue(true);

    component.ngOnInit();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
