import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockPipe } from 'ng-mocks/dist/lib/mock-pipe/mock-pipe';
import { of } from 'rxjs';

import { DefaultFiltersDialogComponent } from './default-filters-dialog.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { FileTypesService } from '~app/files/shared/file-types.service';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { Bsp } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';

describe('DefaultFiltersDialogComponent ', () => {
  let component: DefaultFiltersDialogComponent;
  let fixture: ComponentFixture<DefaultFiltersDialogComponent>;
  let fileTypesServiceSpy: SpyObject<FileTypesService>;

  let searchButton: ModalAction;
  const spain = { id: 1 } as Bsp;

  beforeEach(waitForAsync(() => {
    searchButton = { type: FooterButton.Search };
    TestBed.configureTestingModule({
      declarations: [DefaultFiltersDialogComponent, MockPipe(L10nTranslatePipe)],
      providers: [
        FormBuilder,
        mockProvider(DialogConfig, {
          data: { buttons: [searchButton] }
        }),
        provideMockStore({
          initialState,
          selectors: [{ selector: fromAuth.getUserBsps, value: [spain] }]
        }),
        mockProvider(FileTypesService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fileTypesServiceSpy = TestBed.inject<any>(FileTypesService);
    fileTypesServiceSpy.getAgentReportFileTypeOption.and.returnValue(of([]));

    fixture = TestBed.createComponent(DefaultFiltersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initially disable the Search button', () => {
    expect(searchButton.isDisabled).toBe(true);
  });

  it('should enable the Search button when a File Type is selected', () => {
    component.form.setValue({ type: { descriptor: 'ac' } });

    expect(searchButton.isDisabled).toBeFalsy();
  });

  it('should load file types for the first BSP', fakeAsync(() => {
    component.fileTypes$.subscribe();
    tick();

    expect(fileTypesServiceSpy.getAgentReportFileTypeOption).toHaveBeenCalledWith(spain.id.toString());
  }));
});
