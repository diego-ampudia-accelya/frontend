import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { map, startWith, switchMap } from 'rxjs/operators';

import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { FileTypesService } from '~app/files/shared/file-types.service';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-default-filters-dialog',
  templateUrl: './default-filters-dialog.component.html',
  styleUrls: ['./default-filters-dialog.component.scss']
})
export class DefaultFiltersDialogComponent implements OnInit {
  public form = this.formBuilder.group({
    type: [null, [Validators.required]]
  });

  public fileTypes$ = this.store.pipe(
    select(fromAuth.getUserBsps),
    map(([bsp]) => bsp),
    switchMap(bsp => this.fileTypesService.getAgentReportFileTypeOption(bsp.id.toString()))
  );

  private get searchButton(): ModalAction {
    return this.config.data.buttons.find((button: ModalAction) => button.type === FooterButton.Search);
  }

  constructor(
    public config: DialogConfig,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private fileTypesService: FileTypesService
  ) {}

  ngOnInit(): void {
    this.form.statusChanges.pipe(startWith({})).subscribe(() => (this.searchButton.isDisabled = !this.form.valid));
  }
}
