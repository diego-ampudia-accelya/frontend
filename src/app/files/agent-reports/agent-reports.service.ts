import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { get } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import { FileType } from '../model/file-type.model';
import { FileInfo } from '../model/files.model';
import { hasNameContaining, hasUploadDateInRange } from '../shared/filter.utils';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ServerError } from '~app/shared/errors';
import { ClientGridTableHelper, downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class AgentReportsService implements Queryable<FileInfo> {
  private static cacheCleared$ = new Subject();

  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/agent-reports`;
  private clientGridTableHelper = new ClientGridTableHelper<FileInfo>();

  private customFilterFor = {
    name: hasNameContaining,
    uploadDate: hasUploadDateInRange
  };

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private translationService: L10nTranslationService
  ) {}

  public find(
    dataQuery: DataQuery<AgentReportsFilter>,
    options?: { refreshCache?: boolean }
  ): Observable<PagedData<FileInfo>> {
    const { type, bsp, ...filterBy } = dataQuery.filterBy;
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const reqQuery = RequestQuery.fromDataQuery({ ...dataQuery, filterBy });

    if (options?.refreshCache) {
      AgentReportsService.cacheCleared$.next();
    }

    return this.getFiles(bsp.id, type).pipe(
      map(files => this.clientGridTableHelper.query(files, reqQuery, undefined, this.customFilterFor))
    );
  }

  public download(id: string): Observable<{ blob: Blob; fileName: string }> {
    return this.http
      .get<HttpResponse<ArrayBuffer>>(`${this.baseUrl}/${id}`, downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  public bulkDownload(files: FileInfo[]): Observable<{ blob: Blob; fileName: string }> {
    return this.http
      .post<HttpResponse<ArrayBuffer>>(
        `${this.baseUrl}/download`,
        files.map(file => file.id),
        downloadRequestOptions
      )
      .pipe(
        map(formatDownloadResponse),
        catchError((error: ServerError) => {
          throw this.normalizeBulkDownloadError(error, files);
        })
      );
  }

  @Cacheable({
    cacheHasher: ([bspId, type]) => `${bspId}_${type.descriptor}`,
    cacheBusterObserver: AgentReportsService.cacheCleared$
  })
  private getFiles(bspId: number, type: FileType): Observable<FileInfo[]> {
    return this.http.get<FileInfo[]>(this.baseUrl, {
      params: {
        bspId: bspId.toString(),
        fileType: type.descriptor
      }
    });
  }

  private normalizeBulkDownloadError(error: ServerError, files: FileInfo[]): ServerError {
    try {
      const fileNames = this.getErrorFileNames(error, files);

      if (fileNames) {
        const errorMessage = this.translationService.translate(
          'MENU.files.agentReports.errors.filesCouldNotBeDownloaded'
        );

        error.error.messages = [
          {
            message: `${errorMessage}<br /><span class="secondary-message">${fileNames}</span>`,
            messageCode: '',
            messageParams: []
          }
        ];
      }

      return error;
    } catch {
      return error;
    }
  }

  private getErrorFileNames(error: ServerError, files: FileInfo[]): string {
    const fileNames = files.map(file => file.name);

    return error.error.messages
      .reduce((acc, { messageParams }) => {
        messageParams.forEach(({ name, value }) => {
          const fileName = get(fileNames, value);

          if (name === 'fieldName' && fileName) {
            acc.push(fileName);
          }
        });

        return acc;
      }, [])
      .join(', ');
  }
}
