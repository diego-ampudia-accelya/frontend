import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, withLatestFrom } from 'rxjs/operators';

import { AgentReportsActions } from '../actions';
import * as fromFiles from '../reducers';
import * as fromCore from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';

@Injectable({ providedIn: 'root' })
export class DefaultFiltersGuard implements CanActivate {
  constructor(private store: Store<AppState>, private router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UrlTree> {
    this.store.dispatch(AgentReportsActions.openDefaultFilterDialog());

    const previousUrl$ = this.store.pipe(
      select(fromCore.getActiveTab),
      map(tab => tab?.url || ROUTES.DASHBOARD.url)
    );

    return this.store.pipe(
      select(fromFiles.canProceedToAgentReports),
      first(canProceed => canProceed != null),
      withLatestFrom(previousUrl$),
      map(([canProceed, previousUrl]) => (canProceed ? ROUTES.AGENT_REPORTS.url : previousUrl)),
      map(url => this.router.createUrlTree([url]))
    );
  }
}
