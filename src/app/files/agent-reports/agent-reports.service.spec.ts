import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { take } from 'lodash';
import { of, throwError } from 'rxjs';

import { files } from '../mocks/files.mock';
import { FileInfo, FileType } from '../model';
import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import { AgentReportsService } from './agent-reports.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ServerError } from '~app/shared/errors';
import { ResponseErrorBE } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

const testError: ResponseErrorBE = {
  timestamp: null,
  errorCode: 400,
  errorMajorCode: '998',
  errorMessage: 'Validation error',
  messages: []
};

describe('AgentReportsService', () => {
  let service: AgentReportsService;
  let http: SpyObject<HttpClient>;

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        AgentReportsService,
        mockProvider(HttpClient),
        mockProvider(L10nTranslationService, {
          translate: (key: string) => key
        }),
        mockProvider(AppConfigurationService, { baseApiPath: 'api' })
      ]
    })
  );

  beforeEach(() => {
    service = TestBed.inject(AgentReportsService);
    http = TestBed.inject<any>(HttpClient);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    const spain = { id: 6983 };
    const fileType: FileType = { descriptor: 'az', description: 'az' };

    beforeEach(() => {
      http.get.and.returnValue(of(files));
    });

    it('should filter by name', fakeAsync(() => {
      const expectedResult = files.find(f => (f.id = '16'));
      const query: DataQuery<AgentReportsFilter> = {
        ...defaultQuery,
        filterBy: { type: fileType, bsp: spain, name: expectedResult.name }
      };

      let actualResult: PagedData<FileInfo>;
      service.find(query, { refreshCache: true }).subscribe(result => (actualResult = result));

      expect(actualResult.records).toEqual([expectedResult]);
      expect(actualResult.total).toBe(1);
      expect(actualResult.pageSize).toBe(20);
      expect(actualResult.pageNumber).toBe(0);
    }));

    it('should filter by upload date', fakeAsync(() => {
      const query: DataQuery<AgentReportsFilter> = {
        ...defaultQuery,
        filterBy: {
          type: fileType,
          bsp: spain,
          uploadDate: [new Date('2020-02-28'), new Date('2020-02-28')]
        }
      };

      let actualResult: PagedData<FileInfo>;
      service.find(query, { refreshCache: true }).subscribe(result => (actualResult = result));

      expect(actualResult.records.length).toBe(2);
      expect(actualResult.total).toBe(2);
      expect(actualResult.pageSize).toBe(20);
      expect(actualResult.pageNumber).toBe(0);
    }));

    it('should cache response based on BSP and File Type', fakeAsync(() => {
      const query: DataQuery<AgentReportsFilter> = {
        ...defaultQuery,
        filterBy: {
          type: fileType,
          bsp: spain
        }
      };

      service.find(query, { refreshCache: true }).subscribe();
      service.find(query).subscribe();
      service.find(query, { refreshCache: true }).subscribe();

      expect(http.get).toHaveBeenCalledTimes(2);
    }));

    it('should send a request based on BSP and File Type', fakeAsync(() => {
      const query: DataQuery<AgentReportsFilter> = {
        ...defaultQuery,
        filterBy: {
          type: fileType,
          bsp: spain
        }
      };

      service.find(query, { refreshCache: true }).subscribe();

      expect(http.get).toHaveBeenCalledWith('api/file-management/agent-reports', {
        params: {
          bspId: spain.id.toString(),
          fileType: fileType.descriptor
        }
      });
    }));
  });

  describe('download', () => {
    beforeEach(() => {
      http.get.and.returnValue(
        of(
          new HttpResponse({
            body: new ArrayBuffer(0),
            headers: new HttpHeaders({ 'content-disposition': 'attachment;filename=test.txt' })
          })
        )
      );
    });

    it('should send a download request', fakeAsync(() => {
      service.download('123').subscribe();

      expect(http.get).toHaveBeenCalledWith('api/file-management/agent-reports/123', jasmine.anything());
    }));

    it('should return the file content and name', fakeAsync(() => {
      let actualResult: { blob: Blob; fileName: string };

      service.download('123').subscribe(result => (actualResult = result));

      expect(actualResult.blob).toBeDefined();
      expect(actualResult.fileName).toBe('test.txt');
    }));
  });

  describe('bulkDownload', () => {
    beforeEach(() => {
      http.post.and.returnValue(
        of(
          new HttpResponse({
            body: new ArrayBuffer(0),
            headers: new HttpHeaders({ 'content-disposition': 'attachment;filename=test.txt' })
          })
        )
      );
    });

    it('should send a download request', fakeAsync(() => {
      const testFiles = take(files, 1);
      service.bulkDownload(testFiles).subscribe();

      expect(http.post).toHaveBeenCalledWith(
        'api/file-management/agent-reports/download',
        [testFiles[0].id],
        jasmine.anything()
      );
    }));

    it('should return the file content and name', fakeAsync(() => {
      let actualResult: { blob: Blob; fileName: string };

      service.bulkDownload(take(files, 1)).subscribe(result => (actualResult = result));

      expect(actualResult.blob).toBeDefined();
      expect(actualResult.fileName).toBe('test.txt');
    }));

    it('should throw normalized error, when messageParams has name="fieldName" and value="[index]"', fakeAsync(() => {
      const testFiles = take(files, 2);
      const serverError = new ServerError({
        ...testError,
        messages: [
          {
            message: '',
            messageCode: '',
            messageParams: [{ name: 'fieldName', value: '[0]' }]
          },
          {
            message: '',
            messageCode: '',
            messageParams: [{ name: 'fieldName', value: '[1]' }]
          }
        ]
      });
      let expectedError: ServerError;
      http.post.andReturn(throwError(serverError));
      service.bulkDownload(testFiles).subscribe(
        () => {},
        (error: ServerError) => (expectedError = error)
      );

      expect(expectedError.error.messages.length).toBe(1);
      expect(expectedError.error.messages[0].message).toBe(
        `MENU.files.agentReports.errors.filesCouldNotBeDownloaded<br /><span class="secondary-message">${files[0].name}, ${files[1].name}</span>`
      );
    }));

    it('should throw same error, when messageParams do not match name="fieldName" and value="[index]"', fakeAsync(() => {
      const testFiles = take(files, 2);
      const serverError = new ServerError({
        ...testError,
        messages: [
          {
            message: '',
            messageCode: '',
            messageParams: [{ name: 'test', value: '[0]' }]
          },
          {
            message: '',
            messageCode: '',
            messageParams: [{ name: 'fieldName', value: '[10]' }]
          }
        ]
      });
      let expectedError: ServerError;
      http.post.andReturn(throwError(serverError));
      service.bulkDownload(testFiles).subscribe(
        () => {},
        (error: ServerError) => (expectedError = error)
      );

      expect(expectedError.error.messages.length).toBe(2);
    }));

    it('should throw same error, when there is error thrown on normalizing', fakeAsync(() => {
      const testFiles = take(files, 2);
      const serverError = new ServerError({
        ...testError,
        messages: [
          {
            message: '',
            messageCode: '',
            messageParams: null
          },
          {
            message: '',
            messageCode: '',
            messageParams: [{ name: 'fieldName', value: '[0]' }]
          }
        ]
      });
      let expectedError: ServerError;
      http.post.andReturn(throwError(serverError));
      service.bulkDownload(testFiles).subscribe(
        () => {},
        (error: ServerError) => (expectedError = error)
      );

      expect(expectedError.error.messages.length).toBe(2);
    }));
  });
});
