import { fakeAsync, TestBed } from '@angular/core/testing';
import { Router, UrlTree } from '@angular/router';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import * as fromFiles from '../reducers';
import { AgentReportsGuard } from './agent-reports.guard';
import { AgentReportsActions } from '~app/files/actions';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { ROUTES } from '~app/shared/constants/routes';

describe('AgentReportsGuard', () => {
  let guard: AgentReportsGuard;
  let mockStore: MockStore;
  let routerSpy: SpyObject<Router>;

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        AgentReportsGuard,
        mockProvider(Router),
        provideMockStore({
          initialState,
          selectors: [{ selector: fromFiles.getAgentReportDefaultFilter, value: null }]
        })
      ]
    })
  );

  beforeEach(() => {
    guard = TestBed.inject(AgentReportsGuard);
    mockStore = TestBed.inject<any>(Store);
    routerSpy = TestBed.inject<any>(Router);
    routerSpy.createUrlTree.and.returnValue(new UrlTree());
  });

  it('should create', () => {
    expect(guard).toBeDefined();
  });

  it('canActivate should dispatch AgentReportsActions.activate', fakeAsync(() => {
    spyOn(mockStore, 'dispatch').and.callThrough();

    guard.canActivate(null, null);

    expect(mockStore.dispatch).toHaveBeenCalledWith(AgentReportsActions.activate());
  }));

  it('should return true when default filter exists', fakeAsync(() => {
    mockStore.overrideSelector(fromFiles.getAgentReportDefaultFilter, {} as AgentReportsFilter);

    guard.canActivate(null, null).subscribe(canActivate => {
      expect(canActivate).toBe(true);
    });
  }));

  it('should redirect to Agent Reports Parameters dialog when no default filter exists', fakeAsync(() => {
    guard.canActivate(null, null).subscribe(redirectUrl => {
      expect(redirectUrl).toEqual(jasmine.any(UrlTree));
      expect(routerSpy.createUrlTree).toHaveBeenCalledWith([ROUTES.AGENT_REPORTS_PARAMETERS.url]);
    });
  }));
});
