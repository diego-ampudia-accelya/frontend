import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { map, switchMap } from 'rxjs/operators';

import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import { FileInfo, FileInfoViewListModel } from '../model/files.model';
import * as fromAgentReports from '../reducers';
import { FileTypesService } from '../shared/file-types.service';
import { ReportsDisplayFilterFormatter } from '../shared/reports-display-filter-formatter';
import { AgentReportsActions } from '~app/files/actions';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';

@Component({
  selector: 'bspl-agent-reports',
  templateUrl: './agent-reports.component.html',
  styleUrls: ['./agent-reports.component.scss']
})
export class AgentReportsComponent implements OnInit {
  @ViewChild('filesLinkCellTmpl', { static: true }) private filesLinkCellTmpl: TemplateRef<any>;

  public query$ = this.store.pipe(select(fromAgentReports.getAgentReportsQuery));
  public data$ = this.store.pipe(select(fromAgentReports.getAgentReportsData));
  public selectedRowsCount$ = this.store.pipe(select(fromAgentReports.getAgentReportsSelectedDataCount));
  public isLoading$ = this.store.pipe(select(fromAgentReports.getAgentReportsLoading));
  public predefinedFilters$ = this.store.pipe(
    select(fromAgentReports.getAgentReportDefaultQuery),
    map(({ filterBy }) => filterBy)
  );

  public tableActions = [{ action: GridTableActionType.Download }];
  public columns: TableColumn[];
  public searchForm: FormGroup;
  public fileTypeOptions$ = this.predefinedFilters$.pipe(
    switchMap(({ bsp }) => this.fileTypeService.getAgentReportFileTypeOption(bsp.id.toString()))
  );
  public maxDate = new Date();

  constructor(
    public formatFilter: ReportsDisplayFilterFormatter,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private bytesFormatPipe: BytesFormatPipe,
    private fileTypeService: FileTypesService
  ) {}

  public ngOnInit(): void {
    this.initColumns();
    this.searchForm = this.buildSearchForm();
    this.store.dispatch(AgentReportsActions.search({ refreshCache: true }));
  }

  public loadData(query?: DataQuery<AgentReportsFilter>): void {
    this.store.dispatch(AgentReportsActions.search({ query }));
  }

  public executeAction({ action, row }: { event: Event; action: GridTableAction; row: FileInfo }) {
    if (action.actionType === GridTableActionType.Download) {
      this.saveFile(row);
    }
  }

  public saveFile(file: FileInfo) {
    this.store.dispatch(AgentReportsActions.download({ file }));
  }

  public onCheckboxChange(data: FileInfoViewListModel[]) {
    this.store.dispatch(AgentReportsActions.selectionChange({ data }));
  }

  public clearSelectedRows() {
    this.store.dispatch(AgentReportsActions.clearSelection());
  }

  public onBulkDownloadClick() {
    this.store.dispatch(AgentReportsActions.bulkDownload());
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'isSelected',
        maxWidth: 40,
        sortable: false,
        name: '',
        headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
        cellTemplate: 'defaultLabeledCheckboxCellTmpl'
      },
      {
        prop: 'name',
        name: 'MENU.files.reports.columns.fileName',
        cellTemplate: this.filesLinkCellTmpl,
        resizeable: true,
        width: 500
      },
      {
        prop: 'type.description',
        name: 'MENU.files.reports.columns.fileType',
        resizeable: true,
        width: 300
      },
      {
        prop: 'uploadDate',
        name: 'MENU.files.reports.columns.uploadDate',
        resizeable: true,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy')
        },
        width: 100
      },
      {
        prop: 'size',
        name: 'MENU.files.reports.columns.size',
        resizeable: true,
        pipe: {
          transform: value => this.bytesFormatPipe.transform(value)
        },
        width: 100
      }
    ];
  }

  private buildSearchForm(): FormGroup {
    return this.formBuilder.group({
      name: [],
      type: [],
      uploadDate: []
    });
  }
}
