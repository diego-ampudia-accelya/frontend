import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';
import { of } from 'rxjs';

import { AgentReportsFilter } from '../model/agent-reports-filter.model';
import * as fromFiles from '../reducers';
import { FileTypesService } from '../shared/file-types.service';
import { ReportsDisplayFilterFormatter } from '../shared/reports-display-filter-formatter';
import { AgentReportsComponent } from './agent-reports.component';
import { AgentReportsActions } from '~app/files/actions';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';

describe('AgentReportsComponent', () => {
  let component: AgentReportsComponent;
  let fixture: ComponentFixture<AgentReportsComponent>;
  let fileTypeServiceSpy: SpyObject<FileTypesService>;
  let mockStore: MockStore;
  const dataQuery: DataQuery<AgentReportsFilter> = {
    ...defaultQuery,
    filterBy: { bsp: { id: 1 }, type: { id: 'ac', descriptor: 'ac', description: 'Test' } }
  };

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      declarations: [AgentReportsComponent, MockPipe(L10nTranslatePipe)],
      providers: [
        FormBuilder,
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromFiles.getAgentReportDefaultQuery,
              value: dataQuery
            },
            {
              selector: fromFiles.getAgentReportsQuery,
              value: dataQuery
            },
            { selector: fromFiles.getAgentReportsLoading, value: false },
            { selector: fromFiles.getAgentReportsData, value: [] }
          ]
        }),
        mockProvider(ReportsDisplayFilterFormatter),
        mockProvider(DatePipe),
        mockProvider(FileTypesService),
        mockProvider(BytesFormatPipe)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
  );

  beforeEach(() => {
    fileTypeServiceSpy = TestBed.inject<any>(FileTypesService);
    fileTypeServiceSpy.getAgentReportFileTypeOption.and.returnValue(of([]));

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(AgentReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    expect(component).toBeDefined();
  }));

  it('loadData should call dispatch with AgentReportsActions.search', () => {
    component.loadData(null);

    expect(mockStore.dispatch).toHaveBeenCalledWith(AgentReportsActions.search({ query: null }));
  });

  describe('executeAction', () => {
    it('should call dispatch with AgentReportsActions.download, if action is Download', () => {
      component.executeAction({
        event: null,
        action: {
          actionType: GridTableActionType.Download
        } as GridTableAction,
        row: null
      });

      expect(mockStore.dispatch).toHaveBeenCalledWith(AgentReportsActions.download({ file: null }));
    });

    it('should not call dispatch with AgentReportsActions.download, if action is not Download', () => {
      component.executeAction({
        event: null,
        action: {} as GridTableAction,
        row: null
      });

      expect(mockStore.dispatch).not.toHaveBeenCalledWith(AgentReportsActions.download({ file: null }));
    });
  });

  it('saveFile should call dispatch with AgentReportsActions.download', () => {
    component.saveFile(null);

    expect(mockStore.dispatch).toHaveBeenCalledWith(AgentReportsActions.download({ file: null }));
  });

  it('onBulkDownloadClick should call dispatch with AgentReportsActions.bulkDownload', () => {
    component.onBulkDownloadClick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(AgentReportsActions.bulkDownload());
  });

  it('onCheckboxChange should call dispatch with AgentReportsActions.selectionChange', () => {
    component.onCheckboxChange([]);

    expect(mockStore.dispatch).toHaveBeenCalledWith(AgentReportsActions.selectionChange({ data: [] }));
  });

  it('clearSelectedRows should call dispatch with AgentReportsActions.clearSelection', () => {
    component.clearSelectedRows();

    expect(mockStore.dispatch).toHaveBeenCalledWith(AgentReportsActions.clearSelection());
  });
});
