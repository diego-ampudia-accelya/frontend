import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AgentReportsActions } from '~app/files/actions';
import * as fromFiles from '~app/files/reducers';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';

@Injectable({ providedIn: 'root' })
export class AgentReportsGuard implements CanActivate {
  constructor(private store: Store<AppState>, private router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UrlTree | true> {
    this.store.dispatch(AgentReportsActions.activate());

    return this.store.pipe(
      select(fromFiles.getAgentReportDefaultFilter),
      map(defaultFilter => defaultFilter != null),
      map(hasDefaultFilter => hasDefaultFilter || this.router.createUrlTree([ROUTES.AGENT_REPORTS_PARAMETERS.url]))
    );
  }
}
