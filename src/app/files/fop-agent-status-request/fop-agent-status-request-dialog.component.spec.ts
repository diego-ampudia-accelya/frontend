import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { FopAgentStatusSelection } from '../model';
import { FopAgentStatusRequestDialogComponent } from './fop-agent-status-request-dialog.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { AgentDictionaryService } from '~app/shared/services';

describe('FopAgentStatusRequestDialogComponent', () => {
  let fixture: ComponentFixture<FopAgentStatusRequestDialogComponent>;
  let component: FopAgentStatusRequestDialogComponent;

  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);

  const initialState = {
    auth: {
      user: {
        defaultIsoc: 'ES',
        bsps: [
          { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
          { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
        ]
      }
    }
  };

  const mockConfig: DialogConfig = {
    data: {
      title: 'MENU.files.fopAgentStatusRequest.title',
      buttons: [{ type: FooterButton.Cancel }, { type: FooterButton.Request, isDisabled: true }]
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FopAgentStatusRequestDialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        provideMockStore({ initialState }),
        { provide: DialogConfig, useValue: mockConfig },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FopAgentStatusRequestDialogComponent);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('should initialize agent dropdown options', fakeAsync(() => {
    let agents: DropdownOption<AgentSummary>[];

    agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([]));

    fixture.detectChanges();
    component.agentDropdownOptions$.subscribe(options => (agents = options));

    expect(agents).toEqual([]);
    expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith({ bspId: 1 });
  }));

  it('should initialize form listener correctly', fakeAsync(() => {
    const value = { agentSelection: FopAgentStatusSelection.All };

    fixture.detectChanges();
    component.form.patchValue(value);
    tick();

    expect(component['config'].data.buttons[1].isDisabled).toBe(false);
  }));

  it('should enable agent control on `Single` agent selection', fakeAsync(() => {
    const value = { agentSelection: FopAgentStatusSelection.Single };

    fixture.detectChanges();
    component.form.patchValue(value);
    tick();

    expect(component.form.get('agent').disabled).toBe(false);
  }));
});
