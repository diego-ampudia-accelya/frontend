import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';

import { FopAgentStatusSelection } from '../model';
import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { RadioButton } from '~app/shared/models/radio-button.model';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-fop-agent-status-request-dialog',
  templateUrl: './fop-agent-status-request-dialog.component.html',
  styleUrls: ['./fop-agent-status-request-dialog.component.scss']
})
export class FopAgentStatusRequestDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public agentSelectionRadioButtons: RadioButton[] = Object.values(FopAgentStatusSelection).map(selection => ({
    value: selection,
    label: this.translationService.translate(`MENU.files.fopAgentStatusRequest.agentSelection.${selection}`)
  }));

  private agentSelectionControl: FormControl = this.formBuilder.control([null, Validators.required]);
  public isSingleAgentSelection$: Observable<boolean> = this.agentSelectionControl.valueChanges.pipe(
    map(value => value && value === FopAgentStatusSelection.Single)
  );

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private agentDictionaryService: AgentDictionaryService,
    private store: Store<AppState>
  ) {}

  public ngOnInit(): void {
    this.initializeForm();

    this.initializeDropdownOptions();
    this.initializeListeners();
  }

  private initializeForm(): void {
    this.form = this.formBuilder.group({
      agentSelection: this.agentSelectionControl,
      agent: [{ value: null, disabled: true }, Validators.required]
    });
  }

  private initializeDropdownOptions(): void {
    this.agentDropdownOptions$ = this.store.pipe(
      select(getUserDefaultBsp),
      switchMap(defaultBsp => this.agentDictionaryService.getDropdownOptions({ bspId: defaultBsp.id }))
    );
  }

  private initializeListeners(): void {
    this.initializeFormListener();
    this.initializeAgentSelectionListener();
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const requestButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Request
      );
      requestButton.isDisabled = status === 'INVALID';
    });
  }

  private initializeAgentSelectionListener(): void {
    const agentControl = this.form.get('agent');

    this.isSingleAgentSelection$.pipe(takeUntil(this.destroy$)).subscribe(isSingleAgentSelection => {
      if (isSingleAgentSelection) {
        agentControl.enable();
      } else {
        agentControl.reset();
        agentControl.disable();
      }
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
