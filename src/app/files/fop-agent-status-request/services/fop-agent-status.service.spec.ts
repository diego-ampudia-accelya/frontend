import { HttpClient } from '@angular/common/http';
import { fakeAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { FopAgentStatusService } from './fop-agent-status.service';
import { AppConfigurationService } from '~app/shared/services';

describe('FopAgentStatusService', () => {
  let service: FopAgentStatusService;
  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(() => {
    service = new FopAgentStatusService(httpClientSpy, { baseApiPath: '' } as AppConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should call base endpoint with the proper body', fakeAsync(() => {
    httpClientSpy.post.and.returnValue(of());

    service.request('gds', 'agent', 'ES').subscribe();

    expect(httpClientSpy.post).toHaveBeenCalledWith(jasmine.stringMatching('/file-management/gds-fop-agents-status'), {
      gdsCode: 'gds',
      agentCode: 'agent',
      isoCountryCode: 'ES'
    });
  }));
});
