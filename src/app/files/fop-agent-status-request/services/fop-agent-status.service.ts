import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { FopAgentStatusRequest } from '~app/files/model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class FopAgentStatusService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/gds-fop-agents-status`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  /**
   * Request for including an agent’s FOP status or all agents' statuses in the new Global FOP File.
   *
   * @param gdsCode The GDS code of the user
   * @param agentCode The IATA code of the agent for which I request the FOP statuses inclusion in the next global FOP file
   * @param isoCountryCode The country code of my BSP
   */
  public request(gdsCode: string, agentCode: string, isoCountryCode: string): Observable<FopAgentStatusRequest> {
    const body = { gdsCode, agentCode, isoCountryCode };

    return this.http.post<FopAgentStatusRequest>(this.baseUrl, body);
  }
}
