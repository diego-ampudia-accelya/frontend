import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { of, throwError } from 'rxjs';

import { FopAgentStatusRequestDialogComponent } from '../fop-agent-status-request-dialog.component';

import { FopAgentStatusRequestDialogService } from './fop-agent-status-request-dialog.service';
import { FopAgentStatusService } from './fop-agent-status.service';
import { NotificationService } from '~app/shared/services';
import { DialogService, FooterButton } from '~app/shared/components';

describe('FopAgentStatusRequestDialogService', () => {
  let service: FopAgentStatusRequestDialogService;

  const dialogServiceSpy = createSpyObject(DialogService);
  const dataServiceSpy = createSpyObject(FopAgentStatusService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const initialState = {
    auth: {
      user: {
        gds: { gdsCode: '14' },
        defaultIsoc: 'ES'
      }
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FopAgentStatusRequestDialogService,
        provideMockStore({ initialState }),
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: FopAgentStatusService, useValue: dataServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(FopAgentStatusRequestDialogService);
    translationServiceSpy.translate.and.callFake(identity);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('open', () => {
    beforeEach(() => {
      dialogServiceSpy.open.calls.reset();
      dialogServiceSpy.close.calls.reset();
    });

    it('should open and close FOP agent status request dialog properly with REQUEST action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Request }));
      spyOn<any>(service, 'request').and.returnValue(of(FooterButton.Request));

      let result: FooterButton;
      service.open().subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Request);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(FopAgentStatusRequestDialogComponent, jasmine.anything());
      expect(service['request']).toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close FOP agent status request dialog correctly with CANCEL action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));
      spyOn<any>(service, 'request').and.callThrough();

      let result: FooterButton;
      service.open().subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Cancel);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(FopAgentStatusRequestDialogComponent, jasmine.anything());
      expect(service['request']).not.toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close FOP agent status request dialog correctly even with an ERROR from request method', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Request }));
      spyOn<any>(service, 'request').and.returnValue(throwError(new Error()));

      let error: Error;
      service.open().subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(FopAgentStatusRequestDialogComponent, jasmine.anything());
      expect(service['request']).toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  describe('request', () => {
    const mockConfig = {
      data: {
        title: 'MENU.files.fopAgentStatusRequest.title',
        buttons: [{ type: FooterButton.Request, isDisabled: true }]
      }
    };

    beforeEach(() => {
      dataServiceSpy.request.calls.reset();
      notificationServiceSpy.showSuccess.calls.reset();
    });

    it('should call `request` service method with proper stored and form params', fakeAsync(() => {
      const mockComponent: any = {
        form: {
          value: {
            agent: { code: 'agentCode01' }
          }
        }
      };

      dataServiceSpy.request.and.returnValue(of());

      service['request'](mockConfig, mockComponent).subscribe();
      tick();

      expect(dataServiceSpy.request).toHaveBeenCalledWith('14', 'agentCode01', 'ES'); // Stored params
    }));

    it('should call `request` service method with proper stored params and no form agents', fakeAsync(() => {
      const mockComponent: any = {
        form: {
          value: {}
        }
      };

      dataServiceSpy.request.and.returnValue(of());

      service['request'](mockConfig, mockComponent).subscribe();
      tick();

      expect(dataServiceSpy.request).toHaveBeenCalledWith('14', '', 'ES'); // Stored params
    }));

    it('should notify sucessfully with all agents selection', fakeAsync(() => {
      const mockComponent: any = {
        form: {
          value: {}
        }
      };

      // No agent code returned by BE
      dataServiceSpy.request.and.returnValue(of({ agentCode: '' }));
      notificationServiceSpy.showSuccess.and.callThrough();

      service['request'](mockConfig, mockComponent).subscribe();
      tick();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith(
        'MENU.files.fopAgentStatusRequest.success.allAgentsMessage'
      );
    }));

    it('should notify sucessfully with single agent selection', fakeAsync(() => {
      const mockComponent: any = {
        form: {
          value: {
            agent: { code: 'agentCode01' }
          }
        }
      };

      // An agent code returned by BE
      dataServiceSpy.request.and.returnValue(of({ agentCode: 'agentCode01' }));
      notificationServiceSpy.showSuccess.and.callThrough();

      service['request'](mockConfig, mockComponent).subscribe();
      tick();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith(
        'MENU.files.fopAgentStatusRequest.success.singleAgentMessage'
      );
    }));

    it('should NOT notify successfully on `request` error response from service', fakeAsync(() => {
      const mockComponent: any = {
        form: {
          value: {
            agent: { code: 'agentCode01' }
          }
        }
      };

      dataServiceSpy.request.and.returnValue(throwError(new Error()));
      notificationServiceSpy.showSuccess.and.callThrough();

      service['request'](mockConfig, mockComponent).subscribe(
        _ => {},
        _ => {}
      );
      tick();

      expect(notificationServiceSpy.showSuccess).not.toHaveBeenCalled();
    }));
  });
});
