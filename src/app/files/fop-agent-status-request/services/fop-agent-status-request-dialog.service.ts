import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { FopAgentStatusRequestDialogComponent } from '../fop-agent-status-request-dialog.component';
import { FopAgentStatusService } from './fop-agent-status.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { GdsUser } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class FopAgentStatusRequestDialogService {
  private user$ = this.store.pipe(select(getUser), first());

  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private dataService: FopAgentStatusService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  public open(): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.files.fopAgentStatusRequest.title',
        footerButtonsType: [{ type: FooterButton.Request, isDisabled: true }]
      }
    };

    return this.dialogService.open(FopAgentStatusRequestDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Request ? this.request(dialogConfig, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private request(config: DialogConfig, component: FopAgentStatusRequestDialogComponent): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      const { agent } = component.form.value;

      return this.user$.pipe(
        switchMap(user => this.dataService.request((user as GdsUser).gds.gdsCode, agent?.code || '', user.defaultIsoc)),
        tap(request => this.notificationService.showSuccess(this.getSuccessMessage(request.agentCode))),
        mapTo(FooterButton.Request),
        finalize(() => setLoading(false))
      );
    });
  }

  private getSuccessMessage(agentCode: string): string {
    return agentCode
      ? this.translationService.translate('MENU.files.fopAgentStatusRequest.success.singleAgentMessage', { agentCode })
      : 'MENU.files.fopAgentStatusRequest.success.allAgentsMessage';
  }
}
