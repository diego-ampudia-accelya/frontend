import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { mapTo } from 'rxjs/operators';

import { FopAgentStatusRequestDialogService } from '../services/fop-agent-status-request-dialog.service';

@Injectable()
export class FopAgentStatusRequestGuard implements CanActivate {
  constructor(private fopAgentStatusRequestDialogService: FopAgentStatusRequestDialogService) {}

  // Only available for GDS users
  canActivate(): Observable<boolean> {
    return this.fopAgentStatusRequestDialogService.open().pipe(mapTo(false));
  }
}
