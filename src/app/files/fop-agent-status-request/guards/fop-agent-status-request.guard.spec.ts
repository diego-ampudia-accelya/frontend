import { fakeAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { FopAgentStatusRequestDialogService } from '../services/fop-agent-status-request-dialog.service';

import { FopAgentStatusRequestGuard } from './fop-agent-status-request.guard';
import { FooterButton } from '~app/shared/components';

describe('FopAgentStatusRequestGuard', () => {
  let guard: FopAgentStatusRequestGuard;
  const fopAgentStatusRequestDialogServiceSpy = createSpyObject(FopAgentStatusRequestDialogService);

  beforeEach(() => {
    guard = new FopAgentStatusRequestGuard(fopAgentStatusRequestDialogServiceSpy);
  });

  it('should be created', () => {
    expect(guard).toBeDefined();
  });

  it('should map dialog response to `false`', fakeAsync(() => {
    let result: boolean;

    fopAgentStatusRequestDialogServiceSpy.open.and.returnValue(of(FooterButton.Request));

    guard.canActivate().subscribe(response => (result = response));

    expect(fopAgentStatusRequestDialogServiceSpy.open).toHaveBeenCalled();
    expect(result).toBe(false);
  }));
});
