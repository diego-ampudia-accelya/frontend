import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { finalize, map, tap } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { FileInfo } from '~app/files/model';
import { hasFileType, hasNameContaining, hasUploadDateInRange } from '~app/files/shared/filter.utils';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ClientGridTableHelper, downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class EvaluationFilesService implements Queryable<FileInfo> {
  public loading$: Observable<boolean>;

  private loadingSubject = new BehaviorSubject(false);
  protected dataSubject = new ReplaySubject<Array<FileInfo>>(1);

  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/massload-files`;
  private clientGridTableHelper = new ClientGridTableHelper<FileInfo>();

  private customFilterFor = {
    type: hasFileType,
    name: hasNameContaining,
    uploadDate: hasUploadDateInRange
  };

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {
    this.loading$ = this.loadingSubject.asObservable();
  }

  public find(dataQuery: DataQuery): Observable<PagedData<FileInfo>> {
    const { bspId, ...filterBy } = dataQuery.filterBy;
    dataQuery.sortBy = formatSortBy(dataQuery.sortBy, [{ from: 'type', to: 'type.description' }]);
    dataQuery.filterBy = filterBy;

    return this.getFiles(bspId.id).pipe(
      tap(result => {
        this.dataSubject.next(result);
      }),
      map((files: FileInfo[]) => {
        // TODO: add BE filter model as generic type to `fromDataQuery` factory method
        const reqQuery = RequestQuery.fromDataQuery(dataQuery);

        return this.clientGridTableHelper.query(files, reqQuery, undefined, this.customFilterFor);
      })
    );
  }

  @Cacheable()
  private getFiles(bspId: string): Observable<FileInfo[]> {
    return this.http.get<FileInfo[]>(this.baseUrl, { params: { bspId: bspId?.toString() } });
  }

  public download(id: string): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}/${id}`;
    this.loadingSubject.next(true);

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(
      map(formatDownloadResponse),
      finalize(() => this.loadingSubject.next(false))
    );
  }
}
