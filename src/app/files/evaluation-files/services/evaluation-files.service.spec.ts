import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { EvaluationFilesService } from './evaluation-files.service';
import { files } from '~app/files/mocks/files.mock';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { FileType } from '~app/files/model';
import { DataQuery } from '~app/shared/components/list-view';
import { AppConfigurationService } from '~app/shared/services';

describe('EvalutionFileService', () => {
  let service: EvaluationFilesService;
  let httpMock: SpyObject<HttpClient>;
  const selectedBsp = '6983';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EvaluationFilesService,
        provideMockStore({ initialState }),
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, {
          baseApiPath: 'base-url'
        })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    service = TestBed.inject(EvaluationFilesService);
    httpMock = TestBed.inject<any>(HttpClient);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should load all mock files for user with defaultQuery + bsp and sectionType', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files));
    const dataQuery: DataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        bspId: selectedBsp
      }
    };
    const filesForIataUser = files.filter(file => file.isoCountryCode === selectedBsp);

    tick();

    service.find(dataQuery).subscribe(filesList => {
      expect(filesList.records).toEqual(filesForIataUser);
    });
  }));

  it('should filter files by file type', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files));
    const azFileType: FileType = {
      descriptor: 'az',
      description: 'IBSPs Billing Output'
    };
    const dataQuery: DataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        type: [azFileType],
        bspId: selectedBsp
      }
    };
    const expectedFiles = files.filter(
      file => file.isoCountryCode === selectedBsp && file.type.descriptor === azFileType.descriptor
    );

    tick();

    service.find(dataQuery).subscribe(filesList => {
      expect(filesList.records).toEqual(expectedFiles);
    });
  }));

  it('should filter files by file name', fakeAsync(() => {
    httpMock.get.and.returnValue(of(files));
    const fileName = 'ESaz';
    const dataQuery = {
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999,
        totalElements: 0
      },
      filterBy: {
        name: fileName,
        bspId: selectedBsp
      }
    };
    const expectedFiles = files.filter(
      file => file.isoCountryCode === selectedBsp && file.name.toLowerCase().includes(fileName.toLowerCase())
    );

    let filesResult = null;
    service.find(dataQuery).subscribe(filesList => (filesResult = filesList));

    tick();

    expect(filesResult.records.length).toEqual(expectedFiles.length);
  }));

  it('should change downloaded file status', fakeAsync(() => {
    const body: ArrayBuffer = new ArrayBuffer(1133825);
    const headers: Map<string, string> = new Map();
    headers.set('Content-Disposition', 'attachment;filename="ESxxALL_20200806_testing99999.txt"');
    const response = {
      headers: {
        get: key => headers.get(key)
      },
      status: 200,
      statusText: 'OK',
      url: 'test',
      ok: true,
      type: 4,
      body
    };

    httpMock.get.and.returnValue(of(response));

    let fileDownloaded: any;
    service.download('13').subscribe(file => (fileDownloaded = file));
    tick();

    expect(fileDownloaded.fileName).toEqual('ESxxALL_20200806_testing99999.txt');
  }));
});
