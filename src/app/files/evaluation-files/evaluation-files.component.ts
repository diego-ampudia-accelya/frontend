import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import FileSaver from 'file-saver';
import { isEmpty } from 'lodash';
import { combineLatest, Observable, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { FileInfo, FileType } from '../model';
import { FileInfoFilter } from '../model/files.model';
import { FileTypesService } from '../shared/file-types.service';
import { ReportsDisplayFilterFormatter } from '../shared/reports-display-filter-formatter';
import { EvaluationFilesService } from './services/evaluation-files.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-evaluation-files',
  templateUrl: './evaluation-files.component.html',
  styleUrls: ['./evaluation-files.component.scss'],
  providers: [
    DefaultQueryStorage,
    DatePipe,
    EvaluationFilesService,
    { provide: QUERYABLE, useExisting: EvaluationFilesService },
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [EvaluationFilesService] }
  ]
})
export class EvaluationFilesComponent implements OnInit {
  private formFactory: FormUtil;

  public persistenceDataFiles: FileInfo[] = [];
  public predefinedFilters: Partial<FileInfoFilter>;
  public loggedUser: User;

  public title = 'MENU.files.evaluation.title';
  public columns: GridColumn[] = [];
  public searchForm: FormGroup;
  public bspControl: FormControl;
  public fileTypeListControl: FormControl;
  public tableActions = [];
  public maxDate = this.localBspTimeService.getDate();
  public fileTypeOptions$: Observable<DropdownOption<FileType>[]>;

  public isLoading$: Observable<boolean> = combineLatest([
    this.dataSource.loading$,
    this.evaluationService.loading$
  ]).pipe(map(([isDataLoading, isDownloading]) => isDataLoading || isDownloading));

  public totalItems$ = this.dataSource?.appliedQuery$.pipe(map(query => query.paginateBy.totalElements));

  private readPermissionsFile = [Permissions.readEvaluationDailyDownloads];

  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;
  public bspCountriesList: DropdownOption<BspDto>[];
  public bspSelected: BspDto;

  private destroy$ = new Subject<any>();

  public loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  constructor(
    public dataSource: QueryableDataSource<EvaluationFilesComponent>,
    public filterFormatter: ReportsDisplayFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private bytesFormatPipe: BytesFormatPipe,
    private formBuilder: FormBuilder,
    private localBspTimeService: LocalBspTimeService,
    private fileTypeService: FileTypesService,
    private evaluationService: EvaluationFilesService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private bspsDictionaryService: BspsDictionaryService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  ngOnInit(): void {
    this.columns = this.buildColumns();
    this.searchForm = this.buildSearchForm();
    this.tableActions = this.buildActions();
    this.bspControl = FormUtil.get<FileInfoFilter>(this.searchForm, 'bspId') as FormControl;
    this.fileTypeListControl = FormUtil.get<FileInfoFilter>(this.searchForm, 'type') as FormControl;

    this.initializeLoggedUserAndQuery();
  }

  public initializeLoggedUserAndQuery() {
    this.loggedUser$()
      .pipe(
        tap(user => (this.loggedUser = user)),
        switchMap(_ => this.initializeLeanBspFilter$()),
        tap(() => this.initializeSearchQuery()),
        takeUntil(this.destroy$)
      )
      .subscribe(_ => {
        this.initializeChangeListener();
      });
  }

  public initializeChangeListener(): void {
    if (!this.isBspFilterLocked) {
      // If the BSP selector changes, we need filter data to load all fileTypes in FileType filter field on the BSPs selected
      this.bspControl.valueChanges
        .pipe(
          tap(bsp => (this.bspSelected = bsp)),
          takeUntil(this.destroy$)
        )
        .subscribe(() => {
          this.updateFileTypeList();
          this.fileTypeListControl.reset();
        });
    }
  }

  private updateFileTypeList(): void {
    if (this.bspSelected) {
      this.fileTypeOptions$ = this.fileTypeService.getMassloadFileTypeOption(this.bspSelected.id.toString());
    }
  }

  public loadData(query?: DataQuery): void {
    query = query || defaultQuery;

    if (isEmpty(query.sortBy)) {
      const sortBy = [{ attribute: 'uploadDate', sortType: SortOrder.Desc }];
      query = { ...query, sortBy };
    }

    const dataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private initializeSearchQuery(): void {
    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, this.readPermissionsFile)
      .pipe(
        tap(bspList => {
          if (bspList.length) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);
            const firstListBsp = bspList[0];
            const filterValue = bspList.length === 1 ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelected = filterValue;

            this.predefinedFilters = {
              ...this.predefinedFilters,
              bspId: filterValue
            };
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspList => (this.isBspFilterMultiple = bspList.length > 1)),
        tap(bspList => (this.isBspFilterLocked = bspList.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  public saveFile(fileInfo: FileInfo) {
    this.evaluationService.download(fileInfo.id).subscribe(file => {
      FileSaver.saveAs(file.blob, fileInfo.name);
      this.notificationService.showSuccess(
        this.translationService.translate('MENU.files.evaluation.successfullDownload', { fileName: fileInfo.name })
      );
    });
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.updateFileTypeList();
    }
  }

  public onRowLinkClick(row: FileInfo) {
    this.saveFile(row);
  }

  public onActionClick({ action, row }: { event: Event; action: GridTableAction; row: FileInfo }) {
    if (action.actionType === GridTableActionType.Download) {
      this.saveFile(row);
    }
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<Partial<FileInfoFilter>>({
      bspId: [],
      name: [],
      type: [],
      uploadDate: []
    });
  }

  private buildActions(): Array<{ action: GridTableActionType; disabled?: boolean }> {
    return [{ action: GridTableActionType.Download }];
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'isoCountryCode',
        name: 'MENU.files.reports.columns.bsp',
        flexGrow: 1
      },
      {
        prop: 'name',
        name: 'MENU.files.reports.columns.fileName',
        cellTemplate: 'commonLinkFromObjCellTmpl',
        flexGrow: 4
      },
      {
        prop: 'type',
        name: 'MENU.files.reports.columns.fileType',
        pipe: {
          transform: value => value.description
        },
        flexGrow: 4
      },
      {
        prop: 'uploadDate',
        name: 'MENU.files.reports.columns.uploadDate',
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy')
        },
        flexGrow: 1
      },
      {
        prop: 'size',
        name: 'MENU.files.reports.columns.size',
        pipe: {
          transform: value => this.bytesFormatPipe.transform(value)
        },
        flexGrow: 1
      }
    ];
  }
}
