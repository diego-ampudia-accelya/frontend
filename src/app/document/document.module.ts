import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AcdmDetailsComponent } from './components/acdm/acdm-details/acdm-details.component';
import { AcdmComponent } from './components/acdm/acdm.component';
import { CanxDetailsComponent } from './components/canx/canx-details/canx-details.component';
import { CanxComponent } from './components/canx/canx.component';
import { ConjunctionsComponent } from './components/common/conjunctions/conjunctions.component';
import { EasyPayInfoComponent } from './components/common/easy-pay-info/easy-pay-info.component';
import { ExchangeInfoComponent } from './components/common/exchange-info/exchange-info.component';
import { NetRemitInfoComponent } from './components/common/net-remit-info/net-remit-info.component';
import { PaymentCardInfoComponent } from './components/common/payment-card-info/payment-card-info.component';
import { TaxComponent } from './components/common/tax/tax.component';
import { CreditCardDialogComponent } from './components/credit-card-dialog/credit-card-dialog.component';
import { DocumentHeaderComponent } from './components/document-header/document-header.component';
import { DocumentNotFoundComponent } from './components/document-not-found/document-not-found.component';
import { DocumentViewComponent } from './components/document-view/document-view.component';
import { EmdAdditionalInformationComponent } from './components/emd/emd-additional-information/emd-additional-information.component';
import { EmdDetailsComponent } from './components/emd/emd-details/emd-details.component';
import { EmdComponent } from './components/emd/emd.component';
import { EnquiryDialogComponent } from './components/enquiry-dialog/enquiry-dialog.component';
import { RefundDetailsComponent } from './components/refund/refund-details/refund-details.component';
import { RefundComponent } from './components/refund/refund.component';
import { CodeNamePairPipe } from './components/related-documents/code-name-pair.pipe';
import { CouponPipe } from './components/related-documents/coupon.pipe';
import { RelatedDocumentsComponent } from './components/related-documents/related-documents.component';
import { SelectionDialogComponent } from './components/selection-dialog/selection-dialog.component';
import { TasfDetailsComponent } from './components/tasf/tasf-details/tasf-details.component';
import { TasfComponent } from './components/tasf/tasf.component';
import { AdditionalInformationComponent } from './components/ticket/additional-information/additional-information.component';
import { TaxTransformPipe } from './components/ticket/ticket-details/tax-transform.pipe';
import { TicketComponent } from './components/ticket/ticket.component';
import { DocumentRoutingModule } from './document-routing.module';
import { DocumentCreditCardGuard } from './services/document-credit-card.guard';
import { DocumentCreditCardResolver } from './services/document-credit-card.resolver';
import { DocumentEnquiryGuard } from './services/document-enquiry.guard';
import { DocumentNotFoundResolver } from './services/document-not-found.resolver';
import { DocumentViewGuard } from './services/document-view.guard';
import { DocumentResolver } from './services/document.resolver';
import { StockApplicationRequestViewComponent } from './stock-request/stock-application-request-view/stock-application-request-view.component';
import { StockApplicationRequestService } from './stock-request/stock-application-request/data/stock-application-request.service';
import { StockApplicationRequestComponent } from './stock-request/stock-application-request/stock-application-request.component';
import { SharedModule } from '~app/shared/shared.module';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { TicketDetailsComponent } from '~app/document/components/ticket/ticket-details/ticket-details.component';
import { GlobalErrorHandlerService } from '~app/core/services';

@NgModule({
  declarations: [
    TicketComponent,
    EnquiryDialogComponent,
    CreditCardDialogComponent,
    DocumentHeaderComponent,
    TicketDetailsComponent,
    AdditionalInformationComponent,
    TaxTransformPipe,
    RelatedDocumentsComponent,
    CodeNamePairPipe,
    CouponPipe,
    DocumentViewComponent,
    AcdmComponent,
    AcdmDetailsComponent,
    SelectionDialogComponent,
    RefundComponent,
    RefundDetailsComponent,
    EmdComponent,
    EmdDetailsComponent,
    EmdAdditionalInformationComponent,
    TasfComponent,
    TasfDetailsComponent,
    CanxComponent,
    CanxDetailsComponent,
    TaxComponent,
    EasyPayInfoComponent,
    PaymentCardInfoComponent,
    NetRemitInfoComponent,
    ExchangeInfoComponent,
    ConjunctionsComponent,
    DocumentNotFoundComponent,
    StockApplicationRequestComponent,
    StockApplicationRequestViewComponent
  ],
  imports: [RouterModule, CommonModule, SharedModule, DocumentRoutingModule],
  providers: [
    DocumentResolver,
    DocumentNotFoundResolver,
    DocumentCreditCardResolver,
    DocumentEnquiryGuard,
    DocumentCreditCardGuard,
    DocumentViewGuard,
    DialogConfig,
    ReactiveSubject,
    DatePipe,
    DecimalPipe,
    CodeNamePairPipe,
    CouponPipe,
    AbsoluteDecimalPipe,
    StockApplicationRequestService,
    GlobalErrorHandlerService
  ]
})
export class DocumentModule {}
