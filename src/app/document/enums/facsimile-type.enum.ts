export enum FacsimileType {
  ADM = 'ADM',
  ACM = 'ACM',
  TKTT = 'TKTT',
  RFND = 'RFND',
  EMD = 'EMD',
  TASF = 'TASF',
  VOID = 'VOID'
}
