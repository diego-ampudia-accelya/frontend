export enum FormOfPaymentType {
  EX = 'EX',
  CC = 'CC',
  CA = 'CA',
  MSCA = 'MSCA',
  MSCC = 'MSCC',
  EP = 'EP',
  VD = 'VD'
}
