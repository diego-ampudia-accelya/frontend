import {
  ApplicationRef,
  Component,
  ContentChild,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  SimpleChanges
} from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { DocumentBase } from '../../models';
import { AutoPopulationIssueAcdm } from '~app/adm-acm/models/auto-population-issue-acdm';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { IssueAction } from '~app/document/models/issue-action.model';
import { AccordionItemSelectionComponent } from '~app/shared/components/accordion/accordion-item-selection/accordion-item-selection.component';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { Permissions } from '~app/shared/constants/permissions';
import { LabeledValue } from '~app/shared/models/labeled-value.model';
import { appConfiguration, DocumentPdfExporter } from '~app/shared/services';

@Component({
  selector: 'bspl-document-header',
  templateUrl: './document-header.component.html',
  styleUrls: ['./document-header.component.scss']
})
export class DocumentHeaderComponent implements OnChanges, OnDestroy, OnInit {
  @Input() public document: DocumentBase;
  @Input() public hasIssueBtn = false;
  @Input() public issueActions: IssueAction[] = [];

  public buttonDesign = ButtonDesign;
  public permissions = Permissions;
  public statusFigures: LabeledValue<any>[];

  public issueButtonLabel = 'document.ticket.issueButton.label';

  private destroy$ = new Subject();

  public logo: string;
  public isDownloading = false;

  @ContentChild(AccordionItemSelectionComponent)
  private accordionSelector: AccordionItemSelectionComponent;

  constructor(
    private router: Router,
    private permissionsService: PermissionsService,
    private applicationRef: ApplicationRef,
    @Optional() public pdfExporter: DocumentPdfExporter
  ) {}

  ngOnInit(): void {
    this.issueActions = this.getAccessibleItems(this.issueActions);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.document) {
      this.statusFigures = this.getStatusFigures(this.document);
      this.logo = appConfiguration.getAirlineLogoUrl(this.document.transaction.airlineCode);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  public async download() {
    try {
      this.isDownloading = true;
      this.expandItems();
      const pdf = await this.pdfExporter.export();
      const documentNumber =
        this.document.conjunctionDocumentNumber || this.document.documentIdentification.documentNumber;

      const fileName = [
        this.document.bsp.isoCountryCode,
        this.document.documentIdentification.transactionCode,
        documentNumber
      ].join('_');

      pdf.save(`${fileName}.pdf`);
    } finally {
      this.isDownloading = false;
    }
  }

  public redirectToIssue(buttonOption: IssueAction): void {
    this.router.navigate([buttonOption.redirectUrl], {
      queryParams: this.getRedirectQueryParams(this.document)
    });
  }

  private expandItems(): void {
    this.accordionSelector.accordion.expandItems();
    this.applicationRef.tick();
  }

  private getAccessibleItems(issueActions: IssueAction[]): IssueAction[] {
    return issueActions.filter(
      action =>
        this.permissionsService.hasUserType(action.allowedUserTypes) &&
        this.permissionsService.hasPermission(action.permissionKey, this.document.bsp.id)
    );
  }

  private getRedirectQueryParams(document: DocumentBase): AutoPopulationIssueAcdm {
    return {
      documentId: document.id
    };
  }

  private getStatusFigures(document: DocumentBase): LabeledValue<any>[] {
    const baseKey = 'document.ticket';
    const { documentNumber, transactionCode } = document.documentIdentification;
    const statusValues = {
      documentNumber: `${document.conjunctionDocumentNumber || documentNumber} ${
        document.documentIdentification.checkDigit
      }`,
      description: transactionCode,
      owner: document.airline.localName
    };

    return Object.entries(statusValues).map(([key, value]) => ({
      label: `${baseKey}.${key}`,
      value
    }));
  }
}
