import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { ticket } from '../../mocks/ticket.mock';
import { DocumentHeaderComponent } from './document-header.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { IssueAction } from '~app/document/models/issue-action.model';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { TranslatePipeMock } from '~app/test';

describe('DocumentHeaderComponent', () => {
  let component: DocumentHeaderComponent;
  let fixture: ComponentFixture<DocumentHeaderComponent>;
  let debugElement: DebugElement;

  const initialState = {
    auth: {
      user: createAirlineUser()
    }
  };

  const routerStub = jasmine.createSpyObj<Router>('Router', ['navigate']);
  const translationServiceSpy = jasmine.createSpyObj('L10nTranslationService', ['translate']);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  translationServiceSpy.translate.and.returnValue('translated');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentHeaderComponent, TranslatePipeMock, HasPermissionPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({ initialState }),
        HasPermissionPipe,
        { provide: Router, useValue: routerStub },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentHeaderComponent);
    component = fixture.componentInstance;
    component.document = {} as any;
    component.document.transaction = {} as any;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display N/A when ticket description, owner or number are empty', () => {
    const figures = getFigures();

    expect(figures.every(figure => figure === 'N/A')).toBeTruthy();
  });

  it('should return empty logo when airlineCode is not specified', () => {
    expect(component.logo).toBeFalsy();
  });

  it('should navigate on redirectToIssue', fakeAsync(() => {
    const issueAction: IssueAction = { label: 'label', permissionKey: 'key', redirectUrl: 'localhost' };
    component.document = ticket;

    component.redirectToIssue(issueAction);

    expect(routerStub.navigate).toHaveBeenCalledWith([issueAction.redirectUrl], {
      queryParams: {
        documentId: ticket.id
      }
    });
  }));

  it('should not returns issue options, when user has NOT permission', () => {
    component.issueActions = [{ label: 'label', permissionKey: 'key', redirectUrl: 'localhost' }];
    permissionsServiceSpy.hasPermission.and.returnValue(false);
    permissionsServiceSpy.hasUserType.and.returnValue(true);
    component.document = ticket;
    component.ngOnInit();

    expect(component.issueActions.length).toBe(0);
  });

  it('should returns issue options, when user has permission', () => {
    component.issueActions = [{ label: 'label', permissionKey: 'key', redirectUrl: 'localhost' }];
    permissionsServiceSpy.hasPermission.and.returnValue(true);
    permissionsServiceSpy.hasUserType.and.returnValue(true);
    component.document = ticket;
    component.ngOnInit();

    expect(component.issueActions.length).toBe(1);
  });

  function getFigures(): string[] {
    return debugElement.queryAll(By.css('.figure > .figure-value')).map(element => element.nativeElement.textContent);
  }
});
