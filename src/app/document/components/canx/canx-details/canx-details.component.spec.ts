/* eslint-disable @typescript-eslint/no-unused-vars */
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { cloneDeep } from 'lodash';

import { CanxDetailsComponent } from './canx-details.component';
import { canx } from '~app/document/mocks/canx.mock';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { TranslatePipeMock } from '~app/test';

describe('CanxDetailsComponent', () => {
  let component: CanxDetailsComponent;
  let fixture: ComponentFixture<CanxDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CanxDetailsComponent, TranslatePipeMock, AbsoluteDecimalPipe],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanxDetailsComponent);
    component = fixture.componentInstance;
    component.canx = cloneDeep(canx);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set Net remit field to N if NetReportingIndicator is not equal to NR', () => {
    component.ngOnInit();

    expect(component.netRemit).toEqual('N');
  });

  it('should set Net remit field to Y if NetReportingIndicator is NR', () => {
    component.canx.transaction.netReportingIndicator = 'NR';
    component.ngOnInit();

    expect(component.netRemit).toEqual('Y');
  });
});
