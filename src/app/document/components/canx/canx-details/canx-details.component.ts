import { Component, Input, OnInit } from '@angular/core';

import { CANX } from '~app/document/models/canx.model';

const NET_REMIT = 'NR';

@Component({
  selector: 'bspl-canx-details',
  templateUrl: './canx-details.component.html',
  styleUrls: ['./canx-details.component.scss']
})
export class CanxDetailsComponent implements OnInit {
  @Input() public canx: CANX;

  public netRemit: string;

  public ngOnInit(): void {
    this.netRemit = this.canx.transaction.netReportingIndicator === NET_REMIT ? 'Y' : 'N';
  }
}
