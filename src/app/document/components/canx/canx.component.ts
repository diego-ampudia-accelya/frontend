import { Component, Input } from '@angular/core';

import { RelatedDocument } from '~app/document/models';
import { CANX } from '~app/document/models/canx.model';

@Component({
  selector: 'bspl-canx',
  templateUrl: './canx.component.html',
  styleUrls: ['./canx.component.scss']
})
export class CanxComponent {
  @Input() public canx: CANX;
  @Input() public relatedDocuments: RelatedDocument[];
  @Input() public isLoadingRelatedDocuments = true;
}
