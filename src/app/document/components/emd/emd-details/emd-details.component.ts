import { Component, Input, OnInit } from '@angular/core';

import { FormOfPaymentType } from '~app/document/enums';
import { Tax } from '~app/document/models';
import { EMD } from '~app/document/models/emd.model';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

@Component({
  selector: 'bspl-emd-details',
  templateUrl: './emd-details.component.html',
  styleUrls: ['./emd-details.component.scss']
})
export class EmdDetailsComponent implements OnInit {
  @Input() emd: EMD;

  public flights = new Array(4).fill({});
  public formOfPaymentType = FormOfPaymentType;
  public numberPipePrecision = '1.2-2';
  public readonly lastAvailableFlightIndex = 3;

  constructor(private absoluteDecimalPipe: AbsoluteDecimalPipe) {}

  ngOnInit() {
    const precision = this.emd.numberOfDecimals;
    this.numberPipePrecision = precision != null ? `1.${precision}-${precision}` : this.numberPipePrecision;
    this.flights = this.flights.map((_, index) => this.emd.itineraryData[index]);
  }

  public transformTax(tax: Tax): string {
    if (tax) {
      const currency = tax.currency || this.emd.currencyType;
      const amount = this.absoluteDecimalPipe.transform(tax.amount, this.numberPipePrecision);

      return `${currency} ${amount} ${tax.type || ''}`;
    }

    return '';
  }
}
