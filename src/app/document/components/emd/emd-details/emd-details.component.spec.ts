import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { cloneDeep } from 'lodash';

import { emd } from '../../../mocks/emd.mock';
import { EmdDetailsComponent } from './emd-details.component';
import { Tax } from '~app/document/models/';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

describe('EmdaDetailsComponent', () => {
  let component: EmdDetailsComponent;
  let fixture: ComponentFixture<EmdDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EmdDetailsComponent, AbsoluteDecimalPipe],
      providers: [AbsoluteDecimalPipe],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmdDetailsComponent);
    component = fixture.componentInstance;
    component.emd = cloneDeep(emd);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should transform mockedTax when transformTax is called', () => {
    const mockedTax: Tax = {
      amount: 4,
      type: 'mockedType',
      currency: 'mockedCurrency'
    };

    const transformedTax = component.transformTax(mockedTax);
    expect(transformedTax).toBe(`${mockedTax.currency} ${mockedTax.amount}.00 ${mockedTax.type}`);
  });
});
