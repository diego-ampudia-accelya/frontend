import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { IssueActions } from '~app/document/constants/issue-actions';
import { RelatedDocument } from '~app/document/models';
import { EMD } from '~app/document/models/emd.model';
import { IssueAction } from '~app/document/models/issue-action.model';
import { DocumentService } from '~app/document/services/document.service';

@Component({
  selector: 'bspl-emd',
  templateUrl: './emd.component.html',
  styleUrls: ['./emd.component.scss']
})
export class EmdComponent implements OnInit {
  @Input() emd: EMD;
  @Input() public relatedDocuments: RelatedDocument[];
  @Input() public isLoadingRelatedDocuments = true;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() open = new EventEmitter<string>();

  public issueActions: IssueAction[] = [IssueActions.ADM, IssueActions.ACM, IssueActions.RA, IssueActions.RN];

  constructor(private documentService: DocumentService) {}

  ngOnInit() {
    this.emd.netRemitInformation = this.documentService.getNetRemitInformation(this.emd);
    this.emd.conjunctionTicketNumbers = this.getConjunctionTicketNumbers(this.emd);
  }

  private getConjunctionTicketNumbers(emd: EMD): string[] {
    let conjunctions: string[] = [];
    if (emd.conjunctionTickets && emd.conjunctionTickets.length > 0) {
      conjunctions = [
        emd.documentIdentification.documentNumber,
        ...emd.conjunctionTickets.map(conjunction => conjunction.documentIdentification.documentNumber)
      ];
    }

    return conjunctions;
  }
}
