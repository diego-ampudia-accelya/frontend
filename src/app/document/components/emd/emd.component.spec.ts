import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';

import { emd } from '../../mocks/emd.mock';
import { ticket } from '../../mocks/ticket.mock';
import { EmdComponent } from './emd.component';
import { NetRemit } from '~app/document/models/additional-information/net-remit.model';
import { DocumentService } from '~app/document/services/document.service';
import { TranslatePipeMock } from '~app/test';

describe('EmdComponent', () => {
  let component: EmdComponent;
  let fixture: ComponentFixture<EmdComponent>;
  const documentServiceMock = createSpyObject(DocumentService);
  documentServiceMock.getNetRemitInformation.and.returnValue([]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EmdComponent, TranslatePipeMock],
      providers: [{ provide: DocumentService, useValue: documentServiceMock }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmdComponent);
    component = fixture.componentInstance;
    component.emd = emd;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set netRemitInformation when reporting indicator is NR (net remit)', () => {
    const commercialAgreementReference = '351';
    component.emd.transaction.netReportingIndicator = 'NR';
    component.emd.transaction.commercialAgreementReference = commercialAgreementReference;

    expect(component.emd.netRemitInformation).toEqual([]);

    documentServiceMock.getNetRemitInformation.and.returnValue([
      { documentNumber: '3096060992', deal: '351', incentive: 125 }
    ]);

    component.ngOnInit();

    expect(component.emd.netRemitInformation).not.toEqual([]);
    expect(component.emd.netRemitInformation.length).toBeGreaterThan(0);

    const netRemitList: NetRemit[] = component.emd.netRemitInformation;
    netRemitList.forEach(nr => {
      expect(nr.documentNumber).toBe(component.emd.documentIdentification.documentNumber);
      expect(nr.deal).toBe(commercialAgreementReference);
      expect(nr.incentive).toBeDefined();
    });
  });

  it('should set conjunctionTicketNumbers when emda contains conjunctionTickets', () => {
    expect(component.emd.conjunctionTicketNumbers).toEqual([]);
    component.emd.conjunctionTickets = [cloneDeep(ticket)];
    component.ngOnInit();

    const conjunctionTicketNumber = component.emd.conjunctionTicketNumbers.find(
      ctn => ctn === ticket.documentIdentification.documentNumber
    );
    expect(conjunctionTicketNumber).toBe(ticket.documentIdentification.documentNumber);
  });
});
