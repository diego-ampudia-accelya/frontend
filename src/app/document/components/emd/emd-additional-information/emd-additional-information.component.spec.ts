import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';

import { emd } from '../../../mocks/emd.mock';
import { EmdAdditionalInformationComponent } from './emd-additional-information.component';
import { DocumentService } from '~app/document/services/document.service';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { TranslatePipeMock } from '~app/test';

describe('EmdAdditionalInformationComponent', () => {
  let component: EmdAdditionalInformationComponent;
  let fixture: ComponentFixture<EmdAdditionalInformationComponent>;
  const documentServiceMock = createSpyObject(DocumentService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EmdAdditionalInformationComponent, TranslatePipeMock, AbsoluteDecimalPipe],
      providers: [{ provide: DocumentService, useValue: documentServiceMock }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmdAdditionalInformationComponent);
    component = fixture.componentInstance;
    component.emd = cloneDeep(emd);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should conjunction ticket numbers from ticket to object on changes', () => {
    component.emd = { ...emd, conjunctionTicketNumbers: ['2323423445', '6876876852'] };

    component.ngOnChanges({
      emd: new SimpleChange(null, emd, false)
    });

    const expected = [
      {
        documentNumber: '2323423445'
      },
      {
        documentNumber: '6876876852'
      }
    ];

    expect(component.conjunctions).toEqual(expected);
  });
});
