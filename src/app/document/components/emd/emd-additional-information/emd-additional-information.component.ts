import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { isEmpty, throttle } from 'lodash';

import { MiscellaneousCreditPaymentInformation } from '~app/document/models/additional-information/payment-information.model';
import { EMD } from '~app/document/models/emd.model';
import { DocumentService } from '~app/document/services/document.service';
import { SimpleTableColumn } from '~app/shared/components';

@Component({
  selector: 'bspl-emd-additional-information',
  templateUrl: './emd-additional-information.component.html',
  styleUrls: ['./emd-additional-information.component.scss']
})
export class EmdAdditionalInformationComponent implements OnInit, OnChanges {
  @Input() emd: EMD;
  @Output() documentOpened = new EventEmitter();

  public conjunctions: { documentNumber: string }[];

  public netRemitColumns;
  public conjunctionColumns: SimpleTableColumn<{ documentNumber: string }>[];
  public hasPaymentCardInformation;
  public hasAdditionalPaymentInformation;
  public hasNoPaymentCardSectionBorder;

  public openDocument = throttle(documentNumber => this.documentOpened.emit(documentNumber), 1000, { trailing: false });

  constructor(private documentService: DocumentService) {}

  public ngOnInit() {
    const paymentInformation = this.documentService.getPaymentInformation(
      this.emd
    ) as MiscellaneousCreditPaymentInformation;
    this.emd = { ...this.emd, ...paymentInformation };

    this.setSectionBorderChecks();
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.emd) {
      const { conjunctionTicketNumbers: conjunctionTickets = [] } = this.emd;
      this.conjunctions = conjunctionTickets.map(documentNumber => ({ documentNumber }));
    }
  }

  private setSectionBorderChecks() {
    this.hasPaymentCardInformation = !isEmpty(this.emd.paymentCardInformation);

    this.hasAdditionalPaymentInformation = !(
      isEmpty(this.emd.easyPayInformation) &&
      isEmpty(this.emd.netRemitInformation) &&
      isEmpty(this.emd.exchangeInformation) &&
      isEmpty(this.emd.conjunctionTicketNumbers)
    );

    this.hasNoPaymentCardSectionBorder =
      (this.hasPaymentCardInformation && !this.hasAdditionalPaymentInformation) || !this.hasPaymentCardInformation;
  }
}
