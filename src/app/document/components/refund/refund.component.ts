import { Component, EventEmitter, Input, Output } from '@angular/core';

import { IssueActions } from '~app/document/constants/issue-actions';
import { Refund, RelatedDocument } from '~app/document/models';
import { IssueAction } from '~app/document/models/issue-action.model';

@Component({
  selector: 'bspl-refund-document',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss']
})
export class RefundComponent {
  @Input() refund: Refund;
  @Input() public relatedDocuments: RelatedDocument[];
  @Input() public isLoadingRelatedDocuments = true;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() open = new EventEmitter<string>();

  public issueActions: IssueAction[] = [IssueActions.ACM, IssueActions.ADM];
}
