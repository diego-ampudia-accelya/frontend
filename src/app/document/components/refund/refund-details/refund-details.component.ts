import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { isEmpty } from 'lodash';

import { CouponPipe } from '../../related-documents/coupon.pipe';
import { withHeadersBasedOnFields } from '~app/document/helpers/section-config.helper';
import { PaymentCardInformation, Refund } from '~app/document/models';
import { RelatedDocumentInformation } from '~app/document/models/related-document-information.model';
import { DocumentService } from '~app/document/services/document.service';
import { SimpleTableColumn } from '~app/shared/components';

const NET_REMIT = 'NR';

@Component({
  selector: 'bspl-refund-details',
  templateUrl: './refund-details.component.html',
  styleUrls: ['./refund-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RefundDetailsComponent implements OnInit {
  @ViewChild('relatedDocumentsTemplate', { static: true }) relatedDocumentsTemplate: TemplateRef<any>;
  @ViewChild('paymentCardAmountTemplate', { static: true }) paymentCardAmountTemplate: TemplateRef<any>;

  @Input() refund: Refund;

  @Output() documentOpened = new EventEmitter<string>();

  public hasRelatedDocumentInformation = false;
  public netRemit: string;
  public hasPaymentCardInformation;

  public transactionRelatedDocuments: SimpleTableColumn<RelatedDocumentInformation>[];

  constructor(private documentService: DocumentService) {}

  public ngOnInit(): void {
    const paymentInformation = this.documentService.getPaymentInformation(this.refund);
    this.refund = {
      ...this.refund,
      paymentCardInformation:
        paymentInformation && (paymentInformation.paymentCardInformation as PaymentCardInformation[])
    };

    this.hasRelatedDocumentInformation = !isEmpty(this.refund.relatedDocumentInformation);

    this.netRemit = this.refund.transaction.netReportingIndicator === NET_REMIT ? 'Y' : 'N';
    this.transactionRelatedDocuments = withHeadersBasedOnFields(
      [
        { field: 'relatedDocument', cellTemplate: this.relatedDocumentsTemplate },
        { field: 'airlineCode' },
        {
          field: 'couponNumberIdentifier',
          pipe: CouponPipe
        }
      ],
      'document.refund.transactionRelatedDocumentNumbers'
    );
    this.hasPaymentCardInformation = !isEmpty(this.refund.paymentCardInformation);
  }
}
