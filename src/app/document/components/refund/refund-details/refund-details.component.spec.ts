import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { refund } from '../../../mocks/refund.mock';
import { RefundDetailsComponent } from './refund-details.component';
import { DocumentService } from '~app/document/services/document.service';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { TranslatePipeMock } from '~app/test';

describe('RefundDetailsComponent', () => {
  let component: RefundDetailsComponent;
  let fixture: ComponentFixture<RefundDetailsComponent>;
  const documentServiceMock = createSpyObject(DocumentService);
  documentServiceMock.getPaymentInformation.and.returnValue(
    of({ paymentCardInformation: [], easyPayInformation: [], exchangeInformation: [] })
  );

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundDetailsComponent, TranslatePipeMock, AbsoluteDecimalPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: DocumentService, useValue: documentServiceMock }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundDetailsComponent);
    component = fixture.componentInstance;
    component.refund = refund;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
