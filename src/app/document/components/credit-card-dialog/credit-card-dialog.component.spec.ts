import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockPipe } from 'ng-mocks/dist/lib/mock-pipe/mock-pipe';
import { of } from 'rxjs';

import { CreditCardDialogComponent, EOption } from './credit-card-dialog.component';
import { DocumentService } from '~app/document/services/document.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

describe('CreditCardDialogComponent', () => {
  let component: CreditCardDialogComponent;
  let fixture: ComponentFixture<CreditCardDialogComponent>;

  let searchButton: ModalAction;

  beforeEach(waitForAsync(() => {
    searchButton = { type: FooterButton.Search, isDisabled: true };

    TestBed.configureTestingModule({
      providers: [
        FormBuilder,
        mockProvider(DialogConfig, {
          data: { buttons: [searchButton] }
        }),
        mockProvider(ReactiveSubject, { asObservable: of() }),
        mockProvider(DialogService),
        mockProvider(L10nTranslationService),
        mockProvider(DocumentService)
      ],
      declarations: [CreditCardDialogComponent, MockPipe(L10nTranslatePipe)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable the Search button when credit card number is not empty', () => {
    component.selected = EOption.SearchByPaymentCard;
    component.form.setValue({ creditCardNumber: '1234567890123456' });

    expect(searchButton.isDisabled).toBeFalsy();
  });

  it('should disable the Search button when credit card number is empty', () => {
    component.selected = EOption.SearchByPaymentCard;

    component.form.setValue({ creditCardNumber: '' });

    expect(searchButton.isDisabled).toBeTruthy();
  });
});
