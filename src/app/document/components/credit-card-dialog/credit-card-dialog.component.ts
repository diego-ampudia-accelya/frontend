import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { CreditCardDialogData } from '~app/document/models/credit-card-dialog-data.model';
import { DocumentService } from '~app/document/services/document.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AlertMessageType } from '~app/shared/enums';
import { ModalAction } from '~app/shared/models/modal-action.model';

export enum EOption {
  SearchByPaymentCard = 0x01,
  DocumentsQuery = 0x02
}

@Component({
  selector: 'bspl-credit-card-dialog',
  templateUrl: './credit-card-dialog.component.html',
  styleUrls: ['./credit-card-dialog.component.scss']
})
export class CreditCardDialogComponent implements OnInit, OnDestroy {
  public creditCardNumber: string;
  public isSearching: boolean;
  public messageKey = {
    tooltip: 'document.creditCardDialog.tooltipMessage',
    emptyString: ''
  };
  public inlineMessageType = AlertMessageType;
  public hasNoResults = false;
  public form: FormGroup;

  public get dialogData(): CreditCardDialogData {
    return this.config.data as CreditCardDialogData;
  }

  private destroy$ = new Subject();

  public options = [
    { label: 'Search by Payment Card', value: EOption.SearchByPaymentCard },
    { label: 'Documents Query', value: EOption.DocumentsQuery }
  ];

  public selected: EOption = EOption.DocumentsQuery;

  public readonly EOption = EOption;

  constructor(
    public formBuilder: FormBuilder,
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public translationService: L10nTranslationService,
    public documentService: DocumentService
  ) {}

  public ngOnInit(): void {
    this.createForm();
    this.formStatusOnChange().subscribe();
    this.selected = EOption.SearchByPaymentCard;
  }

  private formStatusOnChange(): Observable<any> {
    return this.form.statusChanges.pipe(
      tap(() => this.handleSelectedChange(this.selected)),
      takeUntil(this.destroy$)
    );
  }

  public handleSelectedChange(option: EOption) {
    const searchButton = this.getFormButton(FooterButton.Search);
    if (searchButton) {
      if (option === EOption.DocumentsQuery) {
        searchButton.isDisabled = false;
        searchButton.tooltipText = undefined;
      } else {
        searchButton.isDisabled = this.form.invalid;
        searchButton.tooltipText = this.form.invalid ? this.messageKey.tooltip : undefined;
      }
    }
  }

  public setIsSearching(state: boolean) {
    this.isSearching = state;
    const searchButton = this.config.data.buttons.find(button => button.type === FooterButton.Search);
    searchButton.isLoading = state;
  }

  private createForm() {
    this.form = this.formBuilder.group(
      {
        creditCardNumber: new FormControl('', [Validators.required])
      },
      { updateOn: 'change' }
    );
  }

  private getFormButton(name: string): ModalAction {
    return this.dialogData.buttons.find(button => button.type === name);
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
