import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, createSpyObject, Spectator } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { SelectionDialogComponent } from './selection-dialog.component';
import { DocumentSummary } from '~app/document/models/';
import { ButtonDesign, DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { TranslatePipeMock } from '~app/test';

describe('SelectionDialogComponent', () => {
  let spectator: Spectator<SelectionDialogComponent>;
  const document: DocumentSummary = {
    id: '3684802151-11-c291cmNlLnppcA==',
    documentNumber: '3684802151',
    transactionCode: 'TKTT',
    dateOfIssue: '2019-01-01',
    agentCode: '0230023',
    airlineCode: '220',
    readableByUser: true,
    isoCountryCode: 'ES'
  };
  const actionButtonType = FooterButton.ViewDocument;

  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const mockConfig = {
    data: {
      title: 'title',
      isDocumentFound: false,
      footerButtonsType: [
        {
          buttonDesign: ButtonDesign.Tertiary,
          title: 'BUTTON.DEFAULT.CANCEL',
          type: FooterButton.Cancel
        },
        {
          buttonDesign: ButtonDesign.Primary,
          title: 'BUTTON.DEFAULT.VIEWDOCUMENT',
          type: actionButtonType,
          isDisabled: true
        }
      ] as ModalAction[]
    }
  };

  const routerStub = jasmine.createSpyObj<Router>('Router', ['navigate']);

  const createComponent = createComponentFactory({
    component: SelectionDialogComponent,
    imports: [RouterTestingModule],
    declarations: [DateTimeFormatPipe, TranslatePipeMock],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [
      FormBuilder,
      DialogConfig,
      ReactiveSubject,
      { provide: DialogConfig, useValue: mockConfig },
      { provide: L10nTranslationService, useValue: translationServiceSpy },
      { provide: Router, useValue: routerStub }
    ]
  });

  beforeEach(() => {
    spectator = createComponent();
    spectator.component.config.data.buttons = [
      {
        buttonDesign: ButtonDesign.Tertiary,
        title: 'BUTTON.DEFAULT.CANCEL',
        type: FooterButton.Cancel
      },
      {
        buttonDesign: ButtonDesign.Primary,
        title: 'BUTTON.DEFAULT.VIEWDOCUMENT',
        type: actionButtonType,
        isDisabled: true
      }
    ];
  });

  it('should create component', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should set document when selectCard is called with that document', () => {
    spectator.component.selectCard(document);
    expect(spectator.component.selectedDocument).toBe(document);
  });

  it('should action button be enabled when selectCard is called', () => {
    expect(getButtonByType(actionButtonType).isDisabled).toBe(true);
    spectator.component.selectCard(document);
    expect(getButtonByType(actionButtonType).isDisabled).toBe(false);
  });

  it('should navigate to document view when action button is clicked', () => {
    spyOn(spectator.component.changedItem, 'emit');
    spectator.component.selectCard(document);
    expect(spectator.component.changedItem.emit).toHaveBeenCalledWith(document);
  });

  function getButtonByType(buttonType: string): ModalAction {
    return spectator.component.config.data.buttons.find(actionButton => actionButton.type === buttonType);
  }
});
