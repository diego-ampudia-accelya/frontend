import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { DocumentSummary } from '~app/document/models';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-selection-dialog',
  templateUrl: './selection-dialog.component.html',
  styleUrls: ['./selection-dialog.component.scss']
})
export class SelectionDialogComponent implements OnInit {
  @Input() documents: DocumentSummary[] = [];
  @Output() changedItem = new EventEmitter();

  public selectedDocument: DocumentSummary;
  private actionButtonType = FooterButton.ViewDocument;
  private buttons = [
    {
      buttonDesign: ButtonDesign.Tertiary,
      title: 'BUTTON.DEFAULT.CANCEL',
      type: FooterButton.Cancel
    },
    {
      buttonDesign: ButtonDesign.Primary,
      title: 'BUTTON.DEFAULT.VIEWDOCUMENT',
      type: this.actionButtonType,
      isDisabled: true
    }
  ] as ModalAction[];

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService
  ) {}

  public ngOnInit(): void {
    this.config.data.buttons = this.buttons;
  }

  public selectCard(document: DocumentSummary): void {
    this.selectedDocument = document;
    const button = this.config.data.buttons.find(actionButton => actionButton.type === this.actionButtonType);
    button.isDisabled = false;
    this.changedItem.emit(document);
  }
}
