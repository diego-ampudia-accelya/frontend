import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { IssueActions } from '~app/document/constants/issue-actions';
import { FacsimileType } from '~app/document/enums';
import { ACDM, RelatedDocument } from '~app/document/models';
import { IssueAction } from '~app/document/models/issue-action.model';

@Component({
  selector: 'bspl-acdm-document',
  templateUrl: './acdm.component.html',
  styleUrls: ['./acdm.component.scss']
})
export class AcdmComponent implements OnInit {
  @Input() acdm: ACDM;
  @Input() facsimileType: FacsimileType.ACM | FacsimileType.ADM;
  @Input() public relatedDocuments: RelatedDocument[];
  @Input() public isLoadingRelatedDocuments = true;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() open = new EventEmitter<string>();

  public isAcm: boolean;
  public issueActions: IssueAction[] = [IssueActions.ACM, IssueActions.ADM];

  public ngOnInit(): void {
    this.isAcm = this.facsimileType === FacsimileType.ACM;
  }
}
