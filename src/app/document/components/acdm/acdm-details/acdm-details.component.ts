import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { isEmpty } from 'lodash';

import { withHeadersBasedOnFields } from '~app/document/helpers/section-config.helper';
import { ACDM } from '~app/document/models';
import { SimpleTableColumn } from '~app/shared/components';

const NET_REMIT = 'NR';

@Component({
  selector: 'bspl-acdm-details',
  templateUrl: './acdm-details.component.html',
  styleUrls: ['./acdm-details.component.scss']
})
export class AcdmDetailsComponent implements OnInit {
  @ViewChild('relatedDocumentsTemplate', { static: true }) relatedDocumentsTemplate: TemplateRef<any>;

  @Input() acdm: ACDM;
  @Input() isAcm: boolean;

  @Output() documentOpened = new EventEmitter<string>();

  public hasRelatedDocumentInformation = false;
  public netRemit: string;
  public transactionRelatedDocuments: SimpleTableColumn<{ relatedDocument: string }>[];

  ngOnInit() {
    this.hasRelatedDocumentInformation = !isEmpty(this.acdm.relatedDocumentInformation);

    this.transactionRelatedDocuments = withHeadersBasedOnFields(
      [{ field: 'relatedDocument', cellTemplate: this.relatedDocumentsTemplate }],
      'document.acdm.transactionRelatedDocumentNumbers'
    );

    this.netRemit = this.acdm.transaction.netReportingIndicator === NET_REMIT ? 'Y' : 'N';
  }
}
