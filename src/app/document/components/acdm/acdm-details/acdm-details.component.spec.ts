import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { cloneDeep } from 'lodash';

import { adm } from '../../../mocks/adm.mock';
import { AcdmDetailsComponent } from './acdm-details.component';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { TranslatePipeMock } from '~app/test';

describe('AcdmDetailsComponent', () => {
  let component: AcdmDetailsComponent;
  let fixture: ComponentFixture<AcdmDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AcdmDetailsComponent, TranslatePipeMock, AbsoluteDecimalPipe],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcdmDetailsComponent);
    component = fixture.componentInstance;
    component.acdm = cloneDeep(adm);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set Net remit field to N if NetReportingIndicator is not equal to NR', () => {
    component.ngOnInit();

    expect(component.netRemit).toEqual('N');
  });

  it('should set Net remit field to Y if NetReportingIndicator is NR', () => {
    component.acdm.transaction.netReportingIndicator = 'NR';
    component.ngOnInit();

    expect(component.netRemit).toEqual('Y');
  });

  it('should set hasRelatedDocumentInformation to true if there are related documents', () => {
    component.ngOnInit();

    expect(component.hasRelatedDocumentInformation).toBe(true);
  });

  it('should set hasRelatedDocumentInformation to false if there are no related documents', () => {
    component.acdm.relatedDocumentInformation = [];
    component.ngOnInit();

    expect(component.hasRelatedDocumentInformation).toBe(false);
  });
});
