import { Pipe, PipeTransform } from '@angular/core';

import { Tax } from '~app/document/models';

@Pipe({ name: 'taxTransform' })
export class TaxTransformPipe implements PipeTransform {
  transform(tax?: Tax): string {
    if (tax) {
      return `${tax.currency} ${tax.amount.toFixed(2)} ${tax.type || ''}`;
    }

    return '';
  }
}
