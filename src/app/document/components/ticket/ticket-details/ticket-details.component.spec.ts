import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ticket } from '../../../mocks/ticket.mock';
import { TicketDetailsComponent } from './ticket-details.component';
import { TaxTransformPipe } from '~app/document/components/ticket/ticket-details/tax-transform.pipe';
import { Tax } from '~app/document/models';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { TranslatePipeMock } from '~app/test';

describe('TicketDetailsComponent', () => {
  let component: TicketDetailsComponent;
  let fixture: ComponentFixture<TicketDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TicketDetailsComponent, TranslatePipeMock, TaxTransformPipe],
      providers: [AbsoluteDecimalPipe],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketDetailsComponent);
    component = fixture.componentInstance;

    component.ticket = ticket;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should transform tax with currency, amount and type', () => {
    const tax: Tax = {
      amount: 2,
      currency: 'EUR',
      type: 'XT'
    };
    expect(component.transformTax(tax)).toBe('EUR 2.00 XT');
  });

  it('should transform tax only with amount and type and to get general currency from ticket', () => {
    const tax: Tax = {
      amount: 2,
      type: 'XT'
    };

    expect(component.transformTax(tax)).toBe('CHF 2.00 XT');
  });
});
