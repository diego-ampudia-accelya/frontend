import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { FormOfPaymentType } from '../../../enums';
import { generateFlightData } from '~app/document/helpers/ticket-details.helper';
import { Tax, Ticket } from '~app/document/models';
import { ItineraryData } from '~app/document/models/itinerary-data.model';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

@Component({
  selector: 'bspl-ticket-details',
  templateUrl: './ticket-details.component.html',
  styleUrls: ['./ticket-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TicketDetailsComponent implements OnInit {
  @Input() ticket: Ticket;
  @Input() isCNJTicket: boolean;

  public readonly lastAvailableFlightIndex = 3;
  public flights: Array<ItineraryData>;
  public formOfPaymentType = FormOfPaymentType;
  public numberPipePrecision = '1.2-2';

  constructor(private absoluteDecimalPipe: AbsoluteDecimalPipe) {}

  public ngOnInit(): void {
    const precision = this.ticket.numberOfDecimals;
    this.numberPipePrecision = precision != null ? `1.${precision}-${precision}` : this.numberPipePrecision;

    this.flights = generateFlightData(this.ticket);
  }

  public transformTax(tax: Tax): string {
    if (tax) {
      const currency = tax.currency || this.ticket.currencyType;
      const amount = this.absoluteDecimalPipe.transform(tax.amount, this.numberPipePrecision);

      return `${currency} ${amount} ${tax.type || ''}`;
    }

    return '';
  }
}
