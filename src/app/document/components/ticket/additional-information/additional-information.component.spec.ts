import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { createComponentFactory, createSpyObject, Spectator } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { MockComponent } from 'ng-mocks';

import { ticket } from '../../../mocks/ticket.mock';
import { AdditionalInformationComponent } from './additional-information.component';
import { Ticket } from '~app/document/models';
import { DocumentService } from '~app/document/services/document.service';
import { SimpleTableComponent } from '~app/shared/components';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { TranslatePipeMock } from '~app/test';

describe('AdditionalInformationComponent', () => {
  let spectator: Spectator<AdditionalInformationComponent>;
  const documentServiceMock = createSpyObject(DocumentService);

  const createComponent = createComponentFactory({
    component: AdditionalInformationComponent,
    declarations: [TranslatePipeMock, MockComponent(SimpleTableComponent), AbsoluteDecimalPipe],
    providers: [{ provide: DocumentService, useValue: documentServiceMock }],
    schemas: [NO_ERRORS_SCHEMA]
  });

  beforeEach(() => {
    spectator = createComponent({ props: { ticket } });
  });

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });

  it('should hide all optional sections when information is not available', () => {
    const ticketWithoutAdditionalInfo: Ticket = {
      ...cloneDeep(ticket),
      paymentCardInformation: [],
      netRemitInformation: [],
      exchangeInformation: [],
      easyPayInformation: [],
      conjunctionTicketNumbers: []
    };

    spectator.setInput('ticket', ticketWithoutAdditionalInfo);

    expect(spectator.queryAll('bspl-simple-table')).toHaveLength(0);
  });

  it('should conjunction ticket numbers from ticket to object on changes', () => {
    spectator.component.ticket = { ...ticket, conjunctionTicketNumbers: ['2323423445', '6876876852'] };

    spectator.component.ngOnChanges({
      ticket: new SimpleChange(null, ticket, false)
    });

    const expected = [
      {
        documentNumber: '2323423445'
      },
      {
        documentNumber: '6876876852'
      }
    ];

    expect(spectator.component.conjunctions).toEqual(expected);
  });
});
