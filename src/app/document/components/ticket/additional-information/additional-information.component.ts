import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { isEmpty, throttle } from 'lodash';

import { Ticket } from '~app/document/models';
import { CardPaymentInformation } from '~app/document/models/additional-information/payment-information.model';
import { DocumentService } from '~app/document/services/document.service';

@Component({
  selector: 'bspl-additional-information',
  templateUrl: './additional-information.component.html',
  styleUrls: ['./additional-information.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdditionalInformationComponent implements OnInit, OnChanges {
  @Input() ticket: Ticket;
  @Output() documentOpened = new EventEmitter();

  @ViewChild('conjunctionTicketTemplate', { static: true }) conjunctionTicketTemplate: TemplateRef<any>;

  public conjunctions: { documentNumber: string }[];

  public netRemitColumns;
  public hasPaymentCardInformation;
  public hasAdditionalPaymentInformation;
  public hasNoPaymentCardSectionBorder;

  public openDocument = throttle(documentNumber => this.documentOpened.emit(documentNumber), 1000, { trailing: false });

  constructor(private documentService: DocumentService) {}

  public ngOnInit() {
    const paymentInformation = this.documentService.getPaymentInformation(this.ticket) as CardPaymentInformation;
    this.ticket = { ...this.ticket, ...paymentInformation };

    this.setSectionBorderChecks();
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.ticket) {
      const { conjunctionTicketNumbers: conjunctionTickets = [] } = this.ticket;
      this.conjunctions = conjunctionTickets.map(documentNumber => ({ documentNumber }));
    }
  }

  private setSectionBorderChecks() {
    this.hasPaymentCardInformation = !isEmpty(this.ticket.paymentCardInformation);

    this.hasAdditionalPaymentInformation = !(
      isEmpty(this.ticket.easyPayInformation) &&
      isEmpty(this.ticket.netRemitInformation) &&
      isEmpty(this.ticket.exchangeInformation) &&
      isEmpty(this.ticket.conjunctionTicketNumbers)
    );

    this.hasNoPaymentCardSectionBorder =
      (this.hasPaymentCardInformation && !this.hasAdditionalPaymentInformation) || !this.hasPaymentCardInformation;
  }
}
