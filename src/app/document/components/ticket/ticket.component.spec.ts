import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';

import { ticket } from '../../mocks/ticket.mock';
import { TicketComponent } from './ticket.component';
import { DocumentService } from '~app/document/services/document.service';
import { TranslatePipeMock } from '~app/test';

describe('TicketComponent', () => {
  let component: TicketComponent;
  let fixture: ComponentFixture<TicketComponent>;
  const documentServiceMock = createSpyObject(DocumentService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TicketComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: DocumentService, useValue: documentServiceMock }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketComponent);
    component = fixture.componentInstance;
    component.ticket = ticket;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
