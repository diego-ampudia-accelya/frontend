import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { IssueActions } from '~app/document/constants/issue-actions';
import { RelatedDocument, Ticket } from '~app/document/models';
import { IssueAction } from '~app/document/models/issue-action.model';
import { DocumentService } from '~app/document/services/document.service';

@Component({
  selector: 'bspl-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  @Input() public ticket: Ticket;
  @Input() public relatedDocuments: RelatedDocument[];
  @Input() public isLoadingRelatedDocuments = true;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() open = new EventEmitter();

  public issueActions: IssueAction[] = [IssueActions.ACM, IssueActions.ADM, IssueActions.RA, IssueActions.RN];
  public isCNJTicket: boolean;

  constructor(private documentService: DocumentService) {}

  ngOnInit(): void {
    this.ticket.netRemitInformation = this.documentService.getNetRemitInformation(this.ticket);
    this.ticket.conjunctionTicketNumbers = this.getConjunctionTicketNumbers(this.ticket);
    this.isCNJTicket = !!this.ticket.conjunctionDocumentNumber;
  }

  private getConjunctionTicketNumbers(ticket: Ticket): string[] {
    let conjunctions: string[] = [];
    if (ticket.conjunctionTickets && ticket.conjunctionTickets.length > 0) {
      conjunctions = [
        ticket.documentIdentification.documentNumber,
        ...ticket.conjunctionTickets.map(conjunction => conjunction.documentIdentification.documentNumber)
      ];
    }

    return conjunctions;
  }
}
