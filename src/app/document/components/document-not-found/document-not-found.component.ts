import { Component, Input } from '@angular/core';

import { RelatedDocument } from '~app/document/models';
import { AlertMessageType } from '~app/shared/enums';

@Component({
  selector: 'bspl-document-not-found',
  templateUrl: './document-not-found.component.html',
  styleUrls: ['./document-not-found.component.scss']
})
export class DocumentNotFoundComponent {
  @Input() public documentNumber: string;
  @Input() public relatedDocuments: RelatedDocument[];
  @Input() public isLoadingRelatedDocuments = true;

  public alertMessageType = AlertMessageType;
}
