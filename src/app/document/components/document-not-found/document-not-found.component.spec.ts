import { DocumentNotFoundComponent } from './document-not-found.component';

describe('DocumentNotFoundComponent', () => {
  let component: DocumentNotFoundComponent;

  beforeEach(() => {
    component = new DocumentNotFoundComponent();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
