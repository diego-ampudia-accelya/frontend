import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { isEmpty, throttle } from 'lodash';

import { CardPaymentInformation } from '~app/document/models/additional-information/payment-information.model';
import { TASF } from '~app/document/models/tasf.model';
import { DocumentService } from '~app/document/services/document.service';

@Component({
  selector: 'bspl-tasf-details',
  templateUrl: './tasf-details.component.html',
  styleUrls: ['./tasf-details.component.scss']
})
export class TasfDetailsComponent implements OnInit {
  @Input() tasf: TASF;
  @Output() documentOpened = new EventEmitter();

  public netRemitColumns;

  public hasPaymentCardInformation;
  public hasAdditionalPaymentInformation;
  public hasNoPaymentCardSectionBorder;

  public openDocument = throttle(documentNumber => this.documentOpened.emit(documentNumber), 1000, { trailing: false });

  constructor(private documentService: DocumentService) {}

  ngOnInit() {
    const paymentInformation = this.documentService.getPaymentInformation(this.tasf) as CardPaymentInformation;
    this.tasf = { ...this.tasf, ...paymentInformation };
    this.tasf.netRemit = this.documentService.getNetRemitInformation(this.tasf);

    this.setSectionBorderChecks();
  }

  private setSectionBorderChecks() {
    this.hasPaymentCardInformation = !isEmpty(this.tasf.paymentCardInformation);

    this.hasAdditionalPaymentInformation = !(
      isEmpty(this.tasf.easyPayInformation) &&
      isEmpty(this.tasf.netRemit) &&
      isEmpty(this.tasf.exchangeInformation)
    );

    this.hasNoPaymentCardSectionBorder =
      (this.hasPaymentCardInformation && !this.hasAdditionalPaymentInformation) || !this.hasPaymentCardInformation;
  }
}
