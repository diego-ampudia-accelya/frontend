import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';

import { TasfDetailsComponent } from './tasf-details.component';
import { tasf } from '~app/document/mocks/tasf.mock';
import { DocumentService } from '~app/document/services/document.service';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';
import { TranslatePipeMock } from '~app/test';

describe('TasfDetailsComponent', () => {
  let component: TasfDetailsComponent;
  let fixture: ComponentFixture<TasfDetailsComponent>;
  const documentServiceMock = createSpyObject(DocumentService);
  documentServiceMock.getPaymentInformation.and.returnValue({
    paymentCardInformation: [],
    easyPayInformation: [],
    exchangeInformation: []
  });
  documentServiceMock.getNetRemitInformation.and.returnValue([]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TasfDetailsComponent, TranslatePipeMock, AbsoluteDecimalPipe],
      providers: [{ provide: DocumentService, useValue: documentServiceMock }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasfDetailsComponent);
    component = fixture.componentInstance;
    component.tasf = tasf;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
