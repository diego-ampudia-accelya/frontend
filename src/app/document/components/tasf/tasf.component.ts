import { Component, EventEmitter, Input, Output } from '@angular/core';

import { IssueActions } from '~app/document/constants/issue-actions';
import { RelatedDocument } from '~app/document/models';
import { IssueAction } from '~app/document/models/issue-action.model';
import { TASF } from '~app/document/models/tasf.model';

@Component({
  selector: 'bspl-tasf',
  templateUrl: './tasf.component.html',
  styleUrls: ['./tasf.component.scss']
})
export class TasfComponent {
  @Input() tasf: TASF;
  @Input() public relatedDocuments: RelatedDocument[];
  @Input() public isLoadingRelatedDocuments = true;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() open = new EventEmitter<string>();

  public issueActions: IssueAction[] = [IssueActions.RN, IssueActions.RA];
}
