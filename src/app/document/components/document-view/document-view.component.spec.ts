import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { ticket } from '../../mocks/ticket.mock';
import { DocumentViewComponent } from './document-view.component';
import { ROUTES } from '~app/shared/constants/routes';
import { AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('DocumentViewComponent', () => {
  let component: DocumentViewComponent;
  let fixture: ComponentFixture<DocumentViewComponent>;
  let mockActivatedRoute: Partial<ActivatedRoute>;
  let router: Router;
  const translationMock = jasmine.createSpyObj<L10nTranslationService>('L10nTranslationService', ['translate']);

  beforeEach(waitForAsync(() => {
    mockActivatedRoute = {
      data: of({ document: ticket }),
      params: of()
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [DocumentViewComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: L10nTranslationService, useValue: translationMock },
        { provide: AppConfigurationService, useValue: { features: { downloadDocumentAsPdf: true } } }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.inject<any>(Router);
    fixture = TestBed.createComponent(DocumentViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to document enquiry when openDocument is called.', fakeAsync(() => {
    spyOn(router, 'navigate');

    const documentId = ticket.id;
    const documentNumber = '7890123456';
    component.openDocument(documentNumber);
    tick();

    expect(router.navigate).toHaveBeenCalledWith([ROUTES.DOCUMENT_ENQUIRY.url, documentNumber], {
      queryParams: { documentToExclude: documentId }
    });
  }));
});
