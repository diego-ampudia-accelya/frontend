import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { finalize, first, map, switchMap, tap } from 'rxjs/operators';

import { DocumentTransactionCode, FacsimileType } from '~app/document/enums';
import { DocumentBase, RelatedDocument } from '~app/document/models';
import { DocumentService } from '~app/document/services/document.service';
import { ROUTES } from '~app/shared/constants/routes';
import { DocumentPdfExporter } from '~app/shared/services';

@Component({
  selector: 'bspl-document-view',
  templateUrl: './document-view.component.html',
  styleUrls: ['./document-view.component.scss'],
  providers: [DocumentPdfExporter]
})
export class DocumentViewComponent implements OnInit, OnDestroy {
  public document$: Observable<DocumentBase> = this.activatedRoute.data.pipe(map(data => data.document));
  public relatedDocuments$: Observable<RelatedDocument[]>;

  public documentNumberNotFound$: Observable<string> = this.activatedRoute.params.pipe(
    map(params => params.documentNumber)
  );

  public documentType$: Observable<string> = this.document$.pipe(
    map(document => document && this.documentTransactionCodeMap[document.documentIdentification.transactionCode])
  );

  public facsimileType = FacsimileType;
  public isLoadingRelatedDocuments = true;

  private documentTransactionCodeMap = {
    [DocumentTransactionCode.ADMA]: this.facsimileType.ADM,
    [DocumentTransactionCode.ADMD]: this.facsimileType.ADM,
    [DocumentTransactionCode.SPDR]: this.facsimileType.ADM,
    [DocumentTransactionCode.ADNT]: this.facsimileType.ADM,
    [DocumentTransactionCode.TKTT]: this.facsimileType.TKTT,
    [DocumentTransactionCode.ACMA]: this.facsimileType.ACM,
    [DocumentTransactionCode.SPCR]: this.facsimileType.ACM,
    [DocumentTransactionCode.ACMD]: this.facsimileType.ACM,
    [DocumentTransactionCode.ACNT]: this.facsimileType.ACM,
    [DocumentTransactionCode.RFND]: this.facsimileType.RFND,
    [DocumentTransactionCode.RFNC]: this.facsimileType.RFND,
    [DocumentTransactionCode.EMDA]: this.facsimileType.EMD,
    [DocumentTransactionCode.EMDS]: this.facsimileType.EMD,
    [DocumentTransactionCode.TASF]: this.facsimileType.TASF,
    [DocumentTransactionCode.CANX]: this.facsimileType.VOID,
    [DocumentTransactionCode.TKTT_CNJ]: this.facsimileType.TKTT,
    [DocumentTransactionCode.EMDA_CNJ]: this.facsimileType.EMD,
    [DocumentTransactionCode.EMDS_CNJ]: this.facsimileType.EMD
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private documentService: DocumentService,
    private router: Router,
    private element: ElementRef,
    private pdfExporter: DocumentPdfExporter
  ) {}

  public ngOnInit(): void {
    this.relatedDocuments$ = combineLatest([this.document$, this.documentNumberNotFound$]).pipe(
      tap(() => (this.isLoadingRelatedDocuments = true)),
      switchMap(([document, documentNumberNotFound]) => {
        const documentNumber = document ? document.documentIdentification.documentNumber : documentNumberNotFound;
        const bspId = document?.bsp ? document.bsp.id : null;
        const documentId = document?.id;

        return this.documentService
          .getRelatedDocuments(documentNumber, bspId, documentId)
          .pipe(finalize(() => (this.isLoadingRelatedDocuments = false)));
      })
    );

    this.pdfExporter.attach(this.element);
  }

  public ngOnDestroy(): void {
    this.pdfExporter.detach();
  }

  public openDocument(documentNumber: string) {
    this.document$
      .pipe(
        first(),
        tap(document =>
          this.router.navigate([ROUTES.DOCUMENT_ENQUIRY.url, documentNumber], {
            queryParams: {
              documentToExclude: document.id
            }
          })
        )
      )
      .subscribe();
  }
}
