import { Component, Input, OnInit } from '@angular/core';

import { withHeadersBasedOnFields } from '~app/document/helpers/section-config.helper';
import { EasyPayInformation } from '~app/document/models';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

@Component({
  selector: 'bspl-easy-pay-info',
  templateUrl: './easy-pay-info.component.html'
})
export class EasyPayInfoComponent implements OnInit {
  @Input() data: EasyPayInformation[];
  @Input() decimalPrecision;

  public easyPayColumns;

  ngOnInit() {
    this.easyPayColumns = withHeadersBasedOnFields(
      [
        { field: 'entity' },
        { field: 'maskedAccountNumber' },
        {
          field: 'amount',
          customStyle: 'text-right',
          pipe: AbsoluteDecimalPipe,
          pipeArgs: [this.decimalPrecision]
        }
      ],
      'document.sections.easyPay'
    );
  }
}
