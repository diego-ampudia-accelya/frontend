import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';

import { SimpleTableColumn } from '~app/shared/components';

@Component({
  selector: 'bspl-conjunctions',
  templateUrl: './conjunctions.component.html'
})
export class ConjunctionsComponent implements OnInit {
  @Input() currentDocumentNumber: string;
  @Input() data: { documentNumber: string }[];

  @Output() openDocument = new EventEmitter<string>();

  @ViewChild('conjunctionTicketTemplate', { static: true }) conjunctionTicketTemplate: TemplateRef<any>;

  public columns: SimpleTableColumn<{ documentNumber: string }>[];

  ngOnInit() {
    this.columns = [
      {
        header: 'document.sections.conjunctions.documentNumber',
        field: 'documentNumber',
        cellTemplate: this.conjunctionTicketTemplate
      }
    ];
  }
}
