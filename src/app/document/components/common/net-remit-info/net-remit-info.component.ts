import { Component, Input, OnInit } from '@angular/core';

import { withHeadersBasedOnFields } from '~app/document/helpers/section-config.helper';
import { NetRemit } from '~app/document/models';
import { SimpleTableColumn } from '~app/shared/components';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

@Component({
  selector: 'bspl-net-remit-info',
  templateUrl: './net-remit-info.component.html'
})
export class NetRemitInfoComponent implements OnInit {
  @Input() data: NetRemit[];
  @Input() decimalPrecision;

  public columns: SimpleTableColumn<NetRemit>[];

  public ngOnInit() {
    this.columns = withHeadersBasedOnFields(
      [
        { field: 'documentNumber' },
        { field: 'deal' },
        { field: 'incentive', customStyle: 'text-right', pipe: AbsoluteDecimalPipe, pipeArgs: [this.decimalPrecision] }
      ],
      'document.sections.netRemit'
    );
  }
}
