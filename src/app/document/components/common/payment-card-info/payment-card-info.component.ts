import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';

import { withHeadersBasedOnFields } from '~app/document/helpers/section-config.helper';
import { EasyPayInformation, PaymentCardInformation } from '~app/document/models';
import { MiscellaneousCreditInformation } from '~app/document/models/additional-information/miscellanious-credit.model';
import { SimpleTableColumn } from '~app/shared/components';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

@Component({
  selector: 'bspl-payment-card-info',
  templateUrl: './payment-card-info.component.html'
})
export class PaymentCardInfoComponent implements OnInit {
  @Input() data: PaymentCardInformation[] | MiscellaneousCreditInformation[] | EasyPayInformation[];
  @Input() decimalPrecision = '1.2-2';

  public translationBase = 'document.sections.paymentCard';
  public columns: SimpleTableColumn<PaymentCardInformation>[] = [];

  private columnConfig: SimpleTableColumn[] = [
    { field: 'entity' },
    { field: 'maskedAccountNumber' },
    { field: 'approvalCode' },
    {
      field: 'expiryDate',
      pipe: DatePipe,
      pipeArgs: ['MM/yy']
    },
    { field: 'invoiceNumber' },
    {
      field: 'amount',
      customStyle: 'text-right',
      pipe: AbsoluteDecimalPipe,
      pipeArgs: [this.decimalPrecision]
    }
  ];

  ngOnInit() {
    this.columns = withHeadersBasedOnFields(this.columnConfig, this.translationBase);
  }
}
