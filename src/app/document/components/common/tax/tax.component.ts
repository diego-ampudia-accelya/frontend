import { Component, Input } from '@angular/core';

import { Tax } from '~app/document/models';

@Component({
  selector: 'bspl-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.scss']
})
export class TaxComponent {
  @Input() taxes: Tax[] = [];
  @Input() totalTaxes: number;
  @Input() decimalPrecision = 2;
}
