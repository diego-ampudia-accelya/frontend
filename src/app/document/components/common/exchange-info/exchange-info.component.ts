import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';

import { withHeadersBasedOnFields } from '~app/document/helpers/section-config.helper';
import { ExchangeInformation } from '~app/document/models';
import { SimpleTableColumn } from '~app/shared/components';

@Component({
  selector: 'bspl-exchange-info',
  templateUrl: './exchange-info.component.html'
})
export class ExchangeInfoComponent implements OnInit {
  @Input() data: ExchangeInformation[];

  @Output() openDocument = new EventEmitter();

  @ViewChild('exchangeTicketTemplate', { static: true }) exchangeTicketTemplate: TemplateRef<any>;

  public columns: SimpleTableColumn<ExchangeInformation>[];

  public ngOnInit() {
    this.columns = withHeadersBasedOnFields(
      [
        { field: 'documentNumber', cellTemplate: this.exchangeTicketTemplate },
        { field: 'airlineCode' },
        { field: 'coupons' }
      ],
      'document.sections.exchangeInfo'
    );
  }
}
