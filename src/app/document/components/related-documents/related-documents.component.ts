import { DatePipe } from '@angular/common';
import { Component, Input, OnChanges, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { groupBy } from 'lodash';

import { CodeNamePairPipe } from './code-name-pair.pipe';
import { CouponPipe } from './coupon.pipe';
import { DocumentGroup } from './document-group.model';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { NfeDocumentType, NfeStatusType, RelatedDocument, RelatedDocumentCategory } from '~app/document/models';
import { RelatedDocumentNavigationService } from '~app/document/services/related-document-navigation.service';
import { SimpleTableColumn } from '~app/shared/components';
import { Permissions } from '~app/shared/constants';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

const baseTranslationKey = 'document.ticket.relatedDocuments';

@Component({
  selector: 'bspl-related-documents',
  templateUrl: './related-documents.component.html',
  styleUrls: ['./related-documents.component.scss']
})
export class RelatedDocumentsComponent implements OnChanges {
  @Input() documents: Array<RelatedDocument>;

  public documentsByCategory: Array<DocumentGroup>;

  @ViewChild('documentNumberTemplate', { static: true })
  private documentNumberTemplate: TemplateRef<any>;

  @ViewChild('bspLinkDocumentsTitleTemplate', { static: true })
  private bspLinkDocumentsTitleTemplate: TemplateRef<any>;

  @ViewChild('statusTemplate', { static: true })
  private statusTemplate: TemplateRef<any>;

  @ViewChild('currencyTemplate', { static: true })
  private currencyTemplate: TemplateRef<any>;

  @ViewChild('amountTemplate', { static: true })
  private amountTemplate: TemplateRef<any>;

  constructor(
    private relatedDocumentNavigationService: RelatedDocumentNavigationService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.documents) {
      const groupedDocuments = groupBy(this.documents, 'category');
      const configurations = this.getConfigurationsByCategory();
      this.documentsByCategory = configurations.map(configuration => ({
        ...configuration,
        items: groupedDocuments[configuration.category]
      }));
    }
  }

  public openDocument(document: RelatedDocument): Promise<boolean> {
    return this.relatedDocumentNavigationService.open(document);
  }

  public generateNfeStatusTranslationKey(document: RelatedDocument, status: NfeStatusType): string {
    const basePath = baseTranslationKey + '.nfeStatuses.';
    const statusTranslationKeysMap: { [key in NfeStatusType]: string } = {
      [NfeStatusType.Pending]: this.isRefundType(document.type) ? 'pending' : 'issued',
      [NfeStatusType.Disputed]: 'disputed',
      [NfeStatusType.DisputeApproved]: 'disputeApproved',
      [NfeStatusType.DisputeRejected]: 'disputeRejected',
      [NfeStatusType.Deactivated]: 'deactivated',
      [NfeStatusType.Deleted]: 'deleted',
      [NfeStatusType.PendingRequest]: 'pendingRequest',
      [NfeStatusType.Authorized]: 'authorized',
      [NfeStatusType.Rejected]: 'rejected',
      [NfeStatusType.AuthorizedRequest]: 'authorizedRequest',
      [NfeStatusType.RejectedRequest]: 'rejectedRequest',
      [NfeStatusType.BillingDisputed]: 'billingDisputed',
      [NfeStatusType.PendingAuthorization]: 'pendingAuthorization',
      [NfeStatusType.Issued]: 'issued',
      [NfeStatusType.UnderInvestigation]: 'underInvestigation',
      [NfeStatusType.SentToAirline]: 'sentToAirline'
    };

    const translationPath = statusTranslationKeysMap[status] ? basePath + statusTranslationKeysMap[status] : null;

    return translationPath || status;
  }

  private getConfigurationsByCategory(): Array<DocumentGroup> {
    const columns = this.getColumns();
    const canViewRejectedDocuments = this.permissionsService.hasPermission(Permissions.readDocumentDetailsRejected);

    let configurations: Array<DocumentGroup> = [
      {
        category: RelatedDocumentCategory.Nfe,
        titleTemplate: this.bspLinkDocumentsTitleTemplate,
        columns: [
          columns.documentNumberActive,
          columns.type,
          columns.airline,
          columns.agent,
          columns.status,
          columns.issueDate,
          columns.billingPeriod,
          columns.currency,
          columns.amountPaid
        ],
        noDataMessage: `${baseTranslationKey}.messages.noNfeDocumentsFound`
      },
      {
        category: RelatedDocumentCategory.Billed,
        title: `${baseTranslationKey}.billedDocuments`,
        columns: [
          columns.documentNumberActive,
          columns.type,
          columns.airline,
          columns.agent,
          columns.issueDate,
          columns.billingPeriod,
          columns.coupons,
          columns.currency,
          columns.amountPaid
        ],
        noDataMessage: `${baseTranslationKey}.messages.noBilledDocumentsFound`
      }
    ];

    if (canViewRejectedDocuments) {
      configurations = [
        ...configurations,
        {
          category: RelatedDocumentCategory.Rejected,
          title: `${baseTranslationKey}.rejectedDocuments`,
          columns: [
            columns.documentNumber,
            columns.type,
            columns.airline,
            columns.agent,
            columns.gds,
            columns.errorLevel,
            columns.reasonForRejection,
            columns.rejectionDate,
            columns.period
          ],
          noDataMessage: `${baseTranslationKey}.messages.noRejectedDocumentsFound`
        }
      ];
    }

    return configurations;
  }

  private getColumns(): { [key: string]: SimpleTableColumn<RelatedDocument> } {
    const dateFormat = 'dd/MM/yyyy';

    return {
      documentNumberActive: {
        field: 'documentNumber',
        header: `${baseTranslationKey}.documentNumber`,
        cellTemplate: this.documentNumberTemplate
      },
      type: { field: 'type', header: `${baseTranslationKey}.documentType` },
      airline: { field: 'airline', header: `${baseTranslationKey}.airline`, pipe: CodeNamePairPipe },
      agent: { field: 'agent', header: `${baseTranslationKey}.agent`, pipe: CodeNamePairPipe },
      status: { field: 'status', header: `${baseTranslationKey}.status`, cellTemplate: this.statusTemplate },
      issueDate: {
        field: 'issueDate',
        header: `${baseTranslationKey}.issueDate`,
        pipe: DatePipe,
        pipeArgs: [dateFormat]
      },
      billingPeriod: { field: 'billingPeriod', header: `${baseTranslationKey}.billingPeriod`, pipe: PeriodPipe },
      period: { field: 'billingPeriod', header: `${baseTranslationKey}.period`, pipe: PeriodPipe },
      currency: { field: 'currency', header: `${baseTranslationKey}.currency`, cellTemplate: this.currencyTemplate },
      amountPaid: {
        field: 'amountPaid',
        header: `${baseTranslationKey}.amountPaid`,
        customStyle: 'text-right',
        cellTemplate: this.amountTemplate
      },
      coupons: { field: 'coupons', header: `${baseTranslationKey}.coupons`, pipe: CouponPipe },
      documentNumber: { field: 'documentNumber', header: `${baseTranslationKey}.documentNumber` },
      gds: { field: 'gds', header: `${baseTranslationKey}.gds`, pipe: CodeNamePairPipe },
      errorLevel: { field: 'errorLevel', header: `${baseTranslationKey}.errorLevel` },
      reasonForRejection: { field: 'reasonForRejection', header: `${baseTranslationKey}.rejectionReason` },
      rejectionDate: {
        field: 'rejectionDate',
        header: `${baseTranslationKey}.rejectionDate`,
        pipe: DatePipe,
        pipeArgs: [dateFormat]
      }
    };
  }

  private isRefundType(type: string): boolean {
    return type === NfeDocumentType.RA || type === NfeDocumentType.RN;
  }
}
