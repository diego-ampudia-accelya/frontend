import { TemplateRef } from '@angular/core';

import { SimpleTableColumn } from '~app/shared/components';

export interface DocumentGroup {
  category: string;
  title?: string;
  titleTemplate?: TemplateRef<any>;
  columns: SimpleTableColumn<any>[];
  items?: any[];
  noDataMessage: string;
}
