import { CodeNamePairPipe } from './code-name-pair.pipe';
import { CodeNamePair } from '~app/document/models';

describe('CodeNamePairPipe', () => {
  let pipe: CodeNamePairPipe;

  beforeEach(() => {
    pipe = new CodeNamePairPipe();
  });

  it('should return null when no value is provided', () => {
    const model: CodeNamePair = null;

    expect(pipe.transform(model)).toBe(null);
  });

  it('should return code and name separated with a dash when both values are available', () => {
    const model: CodeNamePair = { code: '016', name: 'Lufthansa' };

    expect(pipe.transform(model)).toBe('016 / Lufthansa');
  });

  it('should return dash instead of code when it is missing', () => {
    const model: CodeNamePair = { code: null, name: 'Lufthansa' };

    expect(pipe.transform(model)).toBe('— / Lufthansa');
  });

  it('should return dash instead of name when it is missing', () => {
    const model: CodeNamePair = { code: '016', name: null };

    expect(pipe.transform(model)).toBe('016 / —');
  });
});
