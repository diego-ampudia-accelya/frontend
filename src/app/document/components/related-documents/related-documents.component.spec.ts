import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { MockComponent } from 'ng-mocks';

import { RelatedDocumentsComponent } from './related-documents.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import relatedDocuments from '~app/document/mocks/related-documents.json';
import { RelatedDocumentNavigationService } from '~app/document/services/related-document-navigation.service';
import { SimpleTableComponent } from '~app/shared/components';
import { TranslatePipeMock } from '~app/test';

describe('RelatedDocumentsComponent', () => {
  let component: RelatedDocumentsComponent;
  let fixture: ComponentFixture<RelatedDocumentsComponent>;

  const navigationServiceSpy = createSpyObject(RelatedDocumentNavigationService);
  const permissionsServiceSpy = createSpyObject(PermissionsService);

  beforeEach(waitForAsync(() => {
    navigationServiceSpy.open.and.returnValue(Promise.resolve(true));

    TestBed.configureTestingModule({
      declarations: [RelatedDocumentsComponent, MockComponent(SimpleTableComponent), TranslatePipeMock],
      providers: [
        { provide: RelatedDocumentNavigationService, useValue: navigationServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('group documents by category', () => {
    it('should display rejected documents if user has permissions', () => {
      permissionsServiceSpy.hasPermission.and.returnValue(true);

      component.documents = relatedDocuments as any[];
      component.ngOnChanges({ documents: new SimpleChange(null, relatedDocuments, false) });

      const { documentsByCategory } = component;

      expect(documentsByCategory.length).toEqual(3);
      expect(documentsByCategory[0].items.length).toEqual(4);
      expect(documentsByCategory[1].items.length).toEqual(2);
      expect(documentsByCategory[2].items.length).toEqual(1);
    });

    it('should NOT display rejected documents if user has NOT the right permissions', () => {
      permissionsServiceSpy.hasPermission.and.returnValue(false);

      component.documents = relatedDocuments as any[];
      component.ngOnChanges({ documents: new SimpleChange(null, relatedDocuments, false) });

      const { documentsByCategory } = component;

      expect(documentsByCategory.length).toEqual(2);
      expect(documentsByCategory[0].items.length).toEqual(4);
      expect(documentsByCategory[1].items.length).toEqual(2);
    });
  });
});
