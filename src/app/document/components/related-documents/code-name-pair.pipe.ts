import { Pipe, PipeTransform } from '@angular/core';
import { defaultTo } from 'lodash';

import { CodeNamePair } from '~app/document/models';
import { GLOBALS } from '~app/shared/constants/globals';

@Pipe({
  name: 'codeNamePair',
  pure: true
})
export class CodeNamePairPipe implements PipeTransform {
  transform(model: CodeNamePair) {
    if (!model) {
      return null;
    }

    const normalizedModel = {
      code: defaultTo(model.code, GLOBALS.LONG_DASH),
      name: defaultTo(model.name, GLOBALS.LONG_DASH)
    };

    return `${normalizedModel.code} / ${normalizedModel.name}`;
  }
}
