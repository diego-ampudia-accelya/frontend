import { CouponPipe } from './coupon.pipe';

describe('CouponPipe', () => {
  let couponPipe: CouponPipe;

  beforeEach(() => {
    couponPipe = new CouponPipe();
  });

  it('should format coupons when all digits are non-zero', () => {
    const coupon = '1234';

    expect(couponPipe.transform(coupon)).toEqual('1234');
  });

  it('should format coupons when the last digits is zero', () => {
    const coupon = '1230';

    expect(couponPipe.transform(coupon)).toEqual('123 -');
  });

  it('should format coupons when the first digits is zero', () => {
    const coupon = '0234';

    expect(couponPipe.transform(coupon)).toEqual('- 234');
  });

  it('should format coupons when a digits in the middle is zero', () => {
    const coupon = '1004';

    expect(couponPipe.transform(coupon)).toEqual('1 - - 4');
  });

  it('should format coupons when all digits are zero', () => {
    const coupon = '0000';

    expect(couponPipe.transform(coupon)).toEqual('- - - -');
  });

  it('should return `null` when coupon is falsy', () => {
    expect(couponPipe.transform(null)).toEqual(null);
  });
});
