import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coupon',
  pure: true
})
export class CouponPipe implements PipeTransform {
  transform(coupons: string) {
    let formattedCoupons = null;
    if (coupons) {
      formattedCoupons = coupons
        // replace zeroes with dashes
        .replace(/0/g, ' - ')
        // remove double spaces between dashes
        .replace(/ {2}/g, ' ')
        // remove leading and traling spaces
        .trim();
    }

    return formattedCoupons;
  }
}
