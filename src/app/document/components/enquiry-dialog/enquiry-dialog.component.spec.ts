import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { EnquiryDialogComponent } from './enquiry-dialog.component';
import { DocumentSummary } from '~app/document/models';
import { ButtonDesign, DialogConfig, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { TranslatePipeMock } from '~app/test';

describe('EnquiryDialogComponent', () => {
  let component: EnquiryDialogComponent;
  let fixture: ComponentFixture<EnquiryDialogComponent>;

  const mockConfig = {
    data: {
      title: 'title',
      isDocumentFound: false,
      footerButtonsType: [{ type: FooterButton.Search, isDisabled: true }]
    }
  } as DialogConfig;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EnquiryDialogComponent, TranslatePipeMock],
      providers: [FormBuilder, { provide: DialogConfig, useValue: mockConfig }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryDialogComponent);
    component = fixture.componentInstance;

    component['config'].data.buttons = [
      {
        title: 'Search',
        buttonDesign: ButtonDesign.Primary,
        type: FooterButton.Search,
        isDisabled: false
      } as ModalAction
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should be initialized with empty value', () => {
    component.ngOnInit();
    expect(component.form.controls.documentNumber.value).toEqual('');
  });

  it('should have active action button when document is valid', () => {
    component.form.setValue({ documentNumber: '161' });
    const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);

    expect(searchButton.isDisabled).toBe(false);
  });

  it('should have inactive action button when document is invalid', () => {
    component.form.setValue({ documentNumber: 'abc' });
    const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);

    expect(searchButton.isDisabled).toBe(true);
  });

  it('should support initial number', () => {
    const validNumber = '123';
    component.dialogData.initialDocumentNumber = validNumber;
    component.ngAfterViewInit();

    const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);
    expect(searchButton.isDisabled).toBeFalsy();
    expect(component.form.controls.documentNumber.value).toEqual(validNumber);
  });

  it('should have inactive action button in search mode', () => {
    component.setIsSearching(true);

    const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);
    expect(searchButton.isLoading).toBeTruthy();
    expect(component.isSearching).toBeTruthy();
  });

  it('should have active action button when is not in search mode', () => {
    component.form.setValue({ documentNumber: '314' });
    component.setIsSearching(false);

    const searchButton = component.dialogData.buttons.find(button => button.type === FooterButton.Search);
    expect(searchButton.isLoading).toBeFalsy();
    expect(component.isSearching).toBeFalsy();
  });

  it('should set document when setSelectedDocument is called', () => {
    component.setSelectedDocument({
      id: '1',
      documentNumber: '1234567890',
      readableByUser: true
    } as DocumentSummary);
    expect(component.selectedDocument).toEqual({
      id: '1',
      documentNumber: '1234567890',
      readableByUser: true
    } as DocumentSummary);
  });
});
