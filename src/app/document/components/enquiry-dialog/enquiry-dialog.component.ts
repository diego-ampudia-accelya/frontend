import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DocumentSummary } from '~app/document/models';
import { EnquiryDialogData } from '~app/document/models/enquiry-dialog-data.model';
import { documentConfig } from '~app/document/services/document.config';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { AlertMessageType } from '~app/shared/enums';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-enquiry-dialog',
  templateUrl: './enquiry-dialog.component.html',
  styleUrls: ['./enquiry-dialog.component.scss']
})
export class EnquiryDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public selectedDocument: DocumentSummary;

  public readonly requiredLength = documentConfig.requiredMaxLength;
  public readonly documentNumberRegex = new RegExp(documentConfig.ticketNumberRegEpx);

  public inlineMessageType = AlertMessageType;
  public messageKey = {
    notReadableNotification: 'document.enquiryDialog.notReadableNotification',
    tooltip: 'document.enquiryDialog.tooltipMessage',
    emptyString: ''
  };

  public form: FormGroup;
  public isSearching: boolean;

  public get dialogData(): EnquiryDialogData {
    return this.config.data as EnquiryDialogData;
  }

  public get hasMultipleDocuments(): boolean {
    return this.dialogData.documents.length > 0;
  }

  private destroy$ = new Subject();

  constructor(private formBuilder: FormBuilder, private config: DialogConfig, private cd: ChangeDetectorRef) {}

  public ngOnInit(): void {
    if (!this.dialogData.documents) {
      this.dialogData.documents = [];
    }

    this.form = this.buildForm();
    this.initializeFormStatusListener();
  }

  public ngAfterViewInit(): void {
    if (this.dialogData.initialDocumentNumber) {
      const initialValue = this.dialogData.initialDocumentNumber.substring(0, documentConfig.requiredMaxLength);
      this.form.controls.documentNumber.setValue(initialValue);
      this.form.controls.documentNumber.markAsTouched();
    }

    this.cd.detectChanges();
  }

  public setIsSearching(state: boolean): void {
    this.isSearching = state;

    const searchButton = this.dialogData.buttons.find(button => button.type === FooterButton.Search);
    searchButton.isLoading = state;
  }

  public setSelectedDocument(document: DocumentSummary): void {
    this.selectedDocument = document;
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group(
      {
        documentNumber: new FormControl('', [Validators.pattern(this.documentNumberRegex), Validators.required])
      },
      { updateOn: 'change' }
    );
  }

  private initializeFormStatusListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      const searchButton = this.getFormButton(FooterButton.Search);

      if (searchButton) {
        searchButton.isDisabled = this.form.invalid;
        searchButton.tooltipText = this.form.invalid ? this.messageKey.tooltip : this.messageKey.emptyString;
      }
    });
  }

  private getFormButton(name: string): ModalAction {
    return this.dialogData.buttons.find(button => button.type === name);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
