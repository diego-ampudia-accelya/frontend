import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of, Subject } from 'rxjs';
import { catchError, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { GlobalErrorHandlerService } from '~app/core/services';
import { Agent } from '~app/master-data/models';
import { ButtonDesign } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GdsSummary } from '~app/shared/models';
import { User, UserType } from '~app/shared/models/user.model';
import { NotificationService, TabService } from '~app/shared/services';
import { CREATE_STOCK_APPLICATION_FORM } from '../models/create-stock-applicaton-form-options';
import { StockRequestRMBResponse } from '../models/stock-application-request';
import { StockRequest } from '../models/stock-request.model';
import { StockApplicationRequestService } from './data/stock-application-request.service';
import { requiredAndMultipleOf10PatternValidator } from './validators/required-and-multiple-of-10-pattern.validator';

@Component({
  selector: 'bspl-stock-application-request',
  templateUrl: './stock-application-request.component.html',
  styleUrls: ['./stock-application-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockApplicationRequestComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public agentOptions$: Observable<DropdownOption<AgentSummary>[]> = this.stockApplicationRequestService.agentList$;
  public gdsOptions$: Observable<DropdownOption<GdsSummary | Partial<GdsSummary>>[]> =
    this.stockApplicationRequestService.gdsList$;

  public isAgentDropdownLocked$ = this.stockApplicationRequestService.loggedUser$.pipe(
    map((user: User) => {
      if (user.userType === UserType.AGENT) {
        this.form.get('agent').reset(user.agent);

        return true;
      }

      return false;
    })
  );

  public readonlyInfo: StockRequest = null;
  public buttonDesign = ButtonDesign;
  public isCreateButtonLoading: boolean;
  public isFormSubmitted = false;
  public isTicketsAmountLoading = false;
  public isTicketsAmountWarningShowing = false;

  public maxAvailableTickets: number;

  public documentTypeOptions = CREATE_STOCK_APPLICATION_FORM.DOCUMENT_TYPE_OPTIONS;
  public statisticalCodeOptions = CREATE_STOCK_APPLICATION_FORM.STATISTICAL_CODE_OPTIONS;

  get availableAmountPlaceholder(): number | ' ' {
    return this.isTicketsAmountLoading || this.maxAvailableTickets === null
      ? ''
      : this.translationService.translate('document.stockRequest.request.placeholders.rmb');
  }

  private destroy$ = new Subject();

  constructor(
    private stockApplicationRequestService: StockApplicationRequestService,
    private formBuilder: FormBuilder,
    private tabService: TabService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private cd: ChangeDetectorRef,
    private globalErrorHandlerService: GlobalErrorHandlerService
  ) {}

  public ngOnInit(): void {
    this.initializeForm();
    this.updateAvaliableATicketAmounts();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public discardChanges(): void {
    this.tabService.closeCurrentTabAndNavigate(ROUTES.STOCK_REQUEST_LIST.url);
  }

  public createRequest(): void {
    if (this.form.invalid) {
      FormUtil.showControlState(this.form);

      return;
    }

    this.isCreateButtonLoading = true;
    this.stockApplicationRequestService
      .create(this.form.value)
      .pipe(
        catchError(error => {
          if (error?.error?.messages[0]?.messageCode === '410') {
            this.notificationService.showError(
              this.translationService.translate(
                'document.stockRequest.request.errors.domesticAgentsInternationalRequest'
              )
            );
          } else {
            this.notificationService.showError(
              this.translationService.translate('document.stockRequest.request.messages.errorSubmitMessage')
            );
          }

          return of(null);
        }),
        tap((data: StockRequest) => {
          if (data) {
            this.readonlyInfo = data;

            const successMessage = this.translationService.translate(
              'document.stockRequest.request.messages.successMessage'
            );
            this.notificationService.showSuccess(successMessage);
            this.isFormSubmitted = true;

            return;
          }
        }),
        finalize(() => {
          this.isCreateButtonLoading = false;
          this.cd.markForCheck();
        })
      )
      .subscribe();
  }

  private initializeForm(): void {
    this.form = this.formBuilder.group({
      agent: [null, Validators.required],
      gds: [null, Validators.required],
      documentType: [this.documentTypeOptions[0].value, Validators.required],
      statisticalCode: [null, Validators.required],
      applicationQuantity: [null, requiredAndMultipleOf10PatternValidator(() => this.maxAvailableTickets)]
    });
  }

  private updateAvaliableATicketAmounts(): void {
    this.form
      .get('agent')
      .valueChanges.pipe(
        tap(() => {
          this.isTicketsAmountLoading = true;
          this.maxAvailableTickets = null;
        }),
        switchMap((value: AgentSummary | Agent) =>
          this.stockApplicationRequestService
            .getAvailableAmount((value as AgentSummary).code || (value as Agent).iataCode)
            .pipe(
              catchError(error => {
                this.isTicketsAmountWarningShowing = true;
                this.globalErrorHandlerService.handleError(error);

                return of(null);
              })
            )
        ),
        tap((data: StockRequestRMBResponse) => {
          if (data) {
            this.maxAvailableTickets = data.availableAmount >= 0 ? data.availableAmount : 0;
          }
        }),
        tap(() => {
          this.form.get('applicationQuantity').updateValueAndValidity();
          this.isTicketsAmountLoading = false;
          this.cd.markForCheck();
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
}
