const IATA_DIGITS_COUNT = 7;

export function getCodeWithCheckDigit(iataNumber: string): string {
  if (iataNumber.length > IATA_DIGITS_COUNT || !+iataNumber) {
    return iataNumber;
  }

  const checkDigit = +iataNumber % IATA_DIGITS_COUNT;

  return `${iataNumber}${checkDigit}`;
}
