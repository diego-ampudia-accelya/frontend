import { FormControl } from '@angular/forms';

import { requiredAndMultipleOf10PatternValidator } from './required-and-multiple-of-10-pattern.validator';

describe('requiredAndMultipleOf10PatternValidator', () => {
  let availableAmount;
  const availableAmountFunc = () => availableAmount;

  it('should return error with empty available amount field when availableAmount is undefined', () => {
    availableAmount = undefined;
    const result = new FormControl(null, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toEqual({
      maxValueExist: { key: 'document.stockRequest.request.errors.maxValueExist' }
    });
  });

  it('should return error with empty available amount field when availableAmount is null', () => {
    availableAmount = null;
    const result = new FormControl(null, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toEqual({
      maxValueExist: { key: 'document.stockRequest.request.errors.maxValueExist' }
    });
  });

  it('should return required error when availableAmount is NOT null and value is null', () => {
    availableAmount = 5;
    const result = new FormControl(null, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toEqual({
      required: { key: 'FORM_CONTROL_VALIDATORS.required' }
    });
  });

  it('should return lowerValue error when value higher than availableAmount', () => {
    availableAmount = 100;
    const result = new FormControl(200, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toEqual({
      lowerValue: {
        key: 'document.stockRequest.request.errors.lowerValue',
        maxValue: '100'
      }
    });
  });

  it('should return multiple10Pattern error when value is NOT multiple of 10', () => {
    availableAmount = 200;
    const result = new FormControl(101, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toEqual({
      multiple10Pattern: { key: 'document.stockRequest.request.errors.multipleOf10Value' }
    });
  });

  it('should return no errors when value lower than availableAmount and is multiple of 10', () => {
    availableAmount = 200;
    const result = new FormControl(100, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toBeNull();
  });

  it('should return no errors when availableAmount is 0 and value is null', () => {
    availableAmount = 0;
    const result = new FormControl(null, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toBeNull();
  });

  it('should return lowerValue error when availableAmount is 0 and value higher than availableAmount', () => {
    availableAmount = 0;
    const result = new FormControl(10, [requiredAndMultipleOf10PatternValidator(availableAmountFunc)]);

    expect(result.errors).toEqual({
      lowerValue: {
        key: 'document.stockRequest.request.errors.lowerValue',
        maxValue: '0'
      }
    });
  });
});
