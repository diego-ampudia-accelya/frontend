import { AbstractControl, ValidatorFn } from '@angular/forms';

import { GLOBALS } from '~app/shared/constants/globals';

// param is a function to get the actual value
export function requiredAndMultipleOf10PatternValidator(maxAvailableTicketsFunc: () => number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: { [key: string]: string } } | null => {
    const maxAvailableTickets = maxAvailableTicketsFunc();
    if (!isDefined(maxAvailableTickets)) {
      return {
        maxValueExist: { key: 'document.stockRequest.request.errors.maxValueExist' }
      };
    }

    // field can be blank when maxAvailableTickets is 0
    if (!control.value && maxAvailableTickets !== 0) {
      return {
        required: { key: 'FORM_CONTROL_VALIDATORS.required' }
      };
    }

    if (control.value) {
      if (control.value > maxAvailableTickets) {
        return {
          lowerValue: {
            key: 'document.stockRequest.request.errors.lowerValue',
            maxValue: maxAvailableTickets.toString()
          }
        };
      }

      if (!control.value.toString().match(GLOBALS.PATTERNS.MULTIPLE_OF_10)) {
        return {
          multiple10Pattern: { key: 'document.stockRequest.request.errors.multipleOf10Value' }
        };
      }
    }

    return null;
  };
}

function isDefined(value: number | undefined | null): boolean {
  return value !== undefined && value !== null;
}
