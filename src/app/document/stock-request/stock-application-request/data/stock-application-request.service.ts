import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { iif, Observable, of } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { AgentSummary, DropdownOption, GdsSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AppConfigurationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import {
  StockApplicationRequest,
  StockApplicationRequestForm,
  StockRequestRMBResponse,
  TRAVELSKY_GDS_CODE
} from '../../models/stock-application-request';
import { StockRequest } from '../../models/stock-request.model';
import { getCodeWithCheckDigit } from '../utils/add-check-digit';

@Injectable()
export class StockApplicationRequestService {
  public loggedUser$: Observable<User> = this.store.pipe(select(getUser), first());

  private bspsIds$: Observable<number[]> = this.loggedUser$.pipe(
    switchMap((user: User) =>
      this.bspsDictionaryService.getAllBspsByPermissions(user.bspPermissions, [
        Permissions.createStockApplicationRequest
      ])
    ),
    map((bspList: BspDto[]) => bspList.map(({ id }: BspDto) => id))
  );

  private agentListForAgentGroup$: Observable<DropdownOption<AgentSummary>[]> = this.bspsIds$.pipe(
    switchMap((bspsIds: number[]) =>
      this.agentDictionaryService.getDropdownOptions({
        bspId: bspsIds,
        permission: Permissions.createStockApplicationRequest
      })
    )
  );

  public agentList$: Observable<DropdownOption<AgentSummary>[]> = this.loggedUser$.pipe(
    switchMap(({ userType, agent }: { userType: UserType; agent?: Agent }) =>
      iif(
        () => userType === UserType.AGENT,
        of([{ label: agent?.iataCode, value: agent }] as DropdownOption[]),
        this.agentListForAgentGroup$
      )
    )
  );

  public gdsList$: Observable<DropdownOption<GdsSummary | Partial<GdsSummary>>[]> = this.bspsIds$.pipe(
    switchMap((bspsIds: number[]) => this.gdsDictionaryService.getDropdownOptions(bspsIds)),
    map((gdsList: DropdownOption<GdsSummary | Partial<GdsSummary>>[]) => {
      let travelSkyElement;

      // It's neccessary to put 'Travelsky' first, if it exists
      gdsList = gdsList.filter((gds: DropdownOption<GdsSummary | Partial<GdsSummary>>) => {
        if (gds.value.gdsCode === TRAVELSKY_GDS_CODE) {
          travelSkyElement = gds;

          return false;
        }

        return true;
      });

      if (travelSkyElement) {
        gdsList.unshift(travelSkyElement);
      }

      return gdsList;
    })
  );

  private baseUrl = `${this.appConfiguration.baseApiPath}/asd-stock-request/stock-request`;

  constructor(
    private store: Store<AppState>,
    private bspsDictionaryService: BspsDictionaryService,
    private agentDictionaryService: AgentDictionaryService,
    private gdsDictionaryService: GdsDictionaryService,
    private http: HttpClient,
    private appConfiguration: AppConfigurationService
  ) {}

  public create(stockRequest: StockApplicationRequestForm): Observable<StockRequest> {
    const request: StockApplicationRequest = {
      gdsCode: stockRequest.gds?.gdsCode,
      agentIataCode: (stockRequest.agent as Agent).iataCode || (stockRequest.agent as AgentSummary).code,
      documentType: stockRequest.documentType,
      statisticalCode: stockRequest.statisticalCode,
      requestedQty: stockRequest.applicationQuantity
    };

    return this.http.post<StockRequest>(this.baseUrl, request);
  }

  public getAvailableAmount(iataCode: string): Observable<StockRequestRMBResponse> {
    const params = { agentIataCode: getCodeWithCheckDigit(iataCode) };

    return this.http.get<StockRequestRMBResponse>(`${this.baseUrl}/available-amount`, { params });
  }
}
