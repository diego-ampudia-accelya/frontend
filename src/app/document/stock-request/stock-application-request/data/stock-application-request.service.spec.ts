import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockProviders } from 'ng-mocks';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { DocumentType, StatisticalCode } from '../../models/stock-request.model';
import { StockApplicationRequestService } from './stock-application-request.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { Permissions } from '~app/shared/constants';
import { AgentDictionaryService, AppConfigurationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

const initialState = {
  auth: {
    user: {
      bspPermissions: [1, 2],
      agent: { iataCode: '111', id: 99 },
      userType: 'agent'
    }
  }
};

describe('StockApplicationRequestService', () => {
  let service: StockApplicationRequestService;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        StockApplicationRequestService,
        provideMockStore({ initialState }),
        MockProviders(BspsDictionaryService, AgentDictionaryService, GdsDictionaryService),
        { provide: AppConfigurationService, useValue: { baseApiPath: 'test-api' } }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(StockApplicationRequestService);
    mockStore = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('bspsIds$', () => {
    it('should call getAllBspsByPermissions with user bsp permissions', fakeAsync(() => {
      spyOn((service as any).bspsDictionaryService, 'getAllBspsByPermissions').and.returnValue(
        of([{ id: 3 }, { id: 4 }])
      );

      (service as any).bspsIds$.pipe(take(1)).subscribe(() => {
        expect((service as any).bspsDictionaryService.getAllBspsByPermissions).toHaveBeenCalledWith(
          [1, 2],
          [Permissions.createStockApplicationRequest]
        );
      });
    }));

    it('should map bsps to ids', fakeAsync(() => {
      spyOn((service as any).bspsDictionaryService, 'getAllBspsByPermissions').and.returnValue(
        of([{ id: 5 }, { id: 6 }])
      );

      (service as any).bspsIds$.pipe(take(1)).subscribe(ids => {
        expect(ids).toEqual([5, 6]);
      });
    }));
  });

  it('agentListForAgentGroup$ - should call getDropdownOptions with filters', fakeAsync(() => {
    spyOn((service as any).bspsDictionaryService, 'getAllBspsByPermissions').and.returnValue(
      of([{ id: 5, isoCountryCode: 'EN' }])
    );
    spyOn((service as any).agentDictionaryService, 'getDropdownOptions').and.returnValue(
      of([
        {
          bspId: [5],
          permission: Permissions.createStockApplicationRequest
        }
      ])
    );

    (service as any).agentListForAgentGroup$.pipe(take(1)).subscribe(() => {});
    tick();

    expect((service as any).agentDictionaryService.getDropdownOptions).toHaveBeenCalledWith({
      bspId: [5],
      permission: Permissions.createStockApplicationRequest
    });
  }));

  describe('agentList$', () => {
    it('should return agent when user type is Agent', fakeAsync(() => {
      let res;
      service.agentList$.pipe(take(1)).subscribe(value => {
        res = value;
      });
      tick();

      expect(res).toEqual([{ label: '111', value: { iataCode: '111', id: 99 } }]);
    }));

    it('should return agents list when user type is Agent Group', fakeAsync(() => {
      mockStore.setState({
        ...initialState,
        auth: {
          user: {
            userType: 'agent_group'
          }
        }
      });
      (service as any).agentListForAgentGroup$ = of([10, 11]);
      tick();

      let res;
      service.agentList$.pipe(take(1)).subscribe(value => {
        res = value;
      });
      tick();

      expect(res).toEqual([10, 11]);
    }));
  });

  describe('gdsList$', () => {
    it('should call getDropdownOptions with bsp ids', fakeAsync(() => {
      spyOn((service as any).bspsDictionaryService, 'getAllBspsByPermissions').and.returnValue(
        of([{ id: 5, isoCountryCode: 'EN' }])
      );
      spyOn((service as any).gdsDictionaryService, 'getDropdownOptions').and.returnValue(of([]));

      service.gdsList$.pipe(take(1)).subscribe(() => {});
      tick();

      expect((service as any).gdsDictionaryService.getDropdownOptions).toHaveBeenCalledWith([5]);
    }));

    it('should return list with Travelsky first', fakeAsync(() => {
      spyOn((service as any).bspsDictionaryService, 'getAllBspsByPermissions').and.returnValue(
        of([{ id: 5, isoCountryCode: 'EN' }])
      );
      spyOn((service as any).gdsDictionaryService, 'getDropdownOptions').and.returnValue(
        of([
          {
            value: {
              gdsCode: 'ABAC',
              name: 'Abacus'
            }
          },
          {
            value: {
              gdsCode: 'MINS',
              name: 'Travelsky'
            }
          }
        ])
      );

      let res;
      service.gdsList$.pipe(take(1)).subscribe(value => {
        res = value;
      });
      tick();

      expect(res).toEqual([
        {
          value: {
            gdsCode: 'MINS',
            name: 'Travelsky'
          }
        },
        {
          value: {
            gdsCode: 'ABAC',
            name: 'Abacus'
          }
        }
      ]);
    }));

    it('should return the same list without Travelsky', fakeAsync(() => {
      spyOn((service as any).bspsDictionaryService, 'getAllBspsByPermissions').and.returnValue(
        of([{ id: 5, isoCountryCode: 'EN' }])
      );
      spyOn((service as any).gdsDictionaryService, 'getDropdownOptions').and.returnValue(
        of([
          {
            value: {
              gdsCode: 'ABAC',
              name: 'Abacus'
            }
          }
        ])
      );

      let res;
      service.gdsList$.pipe(take(1)).subscribe(value => {
        res = value;
      });
      tick();

      expect(res).toEqual([
        {
          value: {
            gdsCode: 'ABAC',
            name: 'Abacus'
          }
        }
      ]);
    }));
  });

  it('create - should call http post with base url and mapped request', () => {
    const param = {
      gds: {
        gdsCode: 'gdsCode'
      },
      agent: {
        iataCode: '123',
        code: '123'
      },
      documentType: DocumentType.eTicket,
      statisticalCode: StatisticalCode.domestic,
      applicationQuantity: 10
    } as any;
    const mappedParam = {
      gdsCode: 'gdsCode',
      agentIataCode: '123',
      documentType: 'E-Ticket',
      statisticalCode: 'Domestic',
      requestedQty: 10
    };
    const path = 'test-api/asd-stock-request/stock-request';
    spyOn((service as any).http, 'post').and.returnValue(of({}));

    service.create(param);

    expect((service as any).http.post).toHaveBeenCalledWith(path, mappedParam);
  });

  it('getAvailableAmount - should call http post with base url and agent code with check digit as param', () => {
    const path = 'test-api/asd-stock-request/stock-request/available-amount';
    spyOn((service as any).http, 'get').and.returnValue(of({}));

    service.getAvailableAmount('0807301');

    expect((service as any).http.get).toHaveBeenCalledWith(path, { params: { agentIataCode: '08073015' } });
  });
});
