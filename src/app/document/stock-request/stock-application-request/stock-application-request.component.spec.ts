import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { MockDeclarations, MockProviders } from 'ng-mocks';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { GlobalErrorHandlerService } from '~app/core/services';
import {
  ButtonComponent,
  InputComponent,
  PropertyComponent,
  PropertyListComponent,
  RadioButtonGroupComponent,
  SelectComponent
} from '~app/shared/components';
import { AlertMessageComponent } from '~app/shared/components/alert-message/alert-message.component';
import { StepInputComponent } from '~app/shared/components/step-input/step-input.component';
import { ROUTES } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { NotificationService, TabService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { StockApplicationRequestService } from './data/stock-application-request.service';
import { StockApplicationRequestComponent } from './stock-application-request.component';

const MockStockApplicationRequestService = {
  create: () => of({}),
  getAvailableAmount: () => of({}),
  agentList$: new BehaviorSubject({}),
  gdsList$: new BehaviorSubject({}),
  loggedUser$: new BehaviorSubject({})
};

const formValidValue = {
  agent: {},
  gds: {},
  documentType: 'EMD',
  statisticalCode: 'Domestic',
  applicationQuantity: 10
};

describe('StockApplicationRequestComponent', () => {
  let component: StockApplicationRequestComponent;
  let fixture: ComponentFixture<StockApplicationRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        StockApplicationRequestComponent,
        TranslatePipeMock,
        MockDeclarations(
          ButtonComponent,
          SelectComponent,
          RadioButtonGroupComponent,
          StepInputComponent,
          PropertyListComponent,
          PropertyComponent,
          AlertMessageComponent,
          InputComponent
        )
      ],
      providers: [
        FormBuilder,
        MockProviders(L10nTranslationService, NotificationService, TabService, GlobalErrorHandlerService),
        { provide: StockApplicationRequestService, useValue: MockStockApplicationRequestService }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockApplicationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('discardChanges - should close current tab and navigate to the list', () => {
    spyOn((component as any).tabService, 'closeCurrentTabAndNavigate');

    component.discardChanges();

    expect((component as any).tabService.closeCurrentTabAndNavigate).toHaveBeenCalledWith(
      ROUTES.STOCK_REQUEST_LIST.url
    );
  });

  describe('isAgentDropdownLocked$', () => {
    beforeEach(() => {
      component.form.get('agent').setValue('test value');
    });

    it('should return true and reset agent control when user is Agent', fakeAsync(() => {
      (component as any).stockApplicationRequestService.loggedUser$.next({
        userType: 'agent',
        agent: 'test agent'
      });

      let res;
      component.isAgentDropdownLocked$.subscribe(value => (res = value));
      tick();

      expect(res).toBe(true);
      expect(component.form.get('agent').value).toBe('test agent');
    }));

    it('should return false and NOT reset agent control when user is NOT Agent', fakeAsync(() => {
      (component as any).stockApplicationRequestService.loggedUser$.next({
        userType: 'agent_group',
        agent: 'test agent'
      });

      let res;
      component.isAgentDropdownLocked$.subscribe(value => (res = value));
      tick();

      expect(res).toBe(false);
      expect(component.form.get('agent').value).toBe('test value');
    }));
  });

  describe('createRequest', () => {
    it('should show control state when form is invalid', () => {
      spyOn(FormUtil, 'showControlState');

      component.createRequest();

      expect(FormUtil.showControlState).toHaveBeenCalledWith(component.form);
    });

    describe('when form is valid', () => {
      beforeEach(() => {
        component.form.setValue(formValidValue);
        component.maxAvailableTickets = 100;
        component.form.get('applicationQuantity').updateValueAndValidity();
      });

      it('should NOT show control state', () => {
        spyOn(FormUtil, 'showControlState');

        component.createRequest();

        expect(FormUtil.showControlState).not.toHaveBeenCalled();
      });

      it('should call create request with form value', () => {
        spyOn((component as any).stockApplicationRequestService, 'create').and.returnValue(of(null));

        component.createRequest();

        expect((component as any).stockApplicationRequestService.create).toHaveBeenCalledWith(formValidValue);
      });

      describe('when returned data is NOT empty', () => {
        it('should set readonlyInfo', () => {
          spyOn((component as any).stockApplicationRequestService, 'create').and.returnValue(of({ id: 5 }));
          component.readonlyInfo = null;

          component.createRequest();

          expect(component.readonlyInfo).toEqual({ id: 5 });
        });

        it('should call translate for success message', fakeAsync(() => {
          spyOn((component as any).translationService, 'translate');

          component.createRequest();
          tick();

          expect((component as any).translationService.translate).toHaveBeenCalledWith(
            'document.stockRequest.request.messages.successMessage'
          );
        }));

        it('should call show success norification with success message', fakeAsync(() => {
          spyOn((component as any).translationService, 'translate').and.returnValue('success message');
          spyOn((component as any).notificationService, 'showSuccess');

          component.createRequest();
          tick();

          expect((component as any).notificationService.showSuccess).toHaveBeenCalledWith('success message');
        }));

        it('should set isFormSubmitted to true', fakeAsync(() => {
          component.isFormSubmitted = false;

          component.createRequest();
          tick();

          expect(component.isFormSubmitted).toBeTruthy();
        }));
      });

      describe('when error was returned', () => {
        beforeEach(() => {
          const mockError = {
            error: {
              messages: [
                {
                  messageCode: '410'
                }
              ]
            }
          };
          spyOn((component as any).stockApplicationRequestService, 'create').and.returnValue(throwError(mockError));
        });

        it('should NOT set readonlyInfo', () => {
          component.readonlyInfo = null;

          component.createRequest();

          expect(component.readonlyInfo).toBe(null);
        });

        it('should NOT set isFormSubmitted to true', () => {
          component.isFormSubmitted = false;

          component.createRequest();

          expect(component.isFormSubmitted).toBeFalsy();
        });

        it('should call translate for error message', fakeAsync(() => {
          spyOn((component as any).translationService, 'translate');

          component.createRequest();
          tick();

          expect((component as any).translationService.translate).toHaveBeenCalledWith(
            'document.stockRequest.request.errors.domesticAgentsInternationalRequest'
          );
        }));

        it('should call show error notification with error message', fakeAsync(() => {
          spyOn((component as any).translationService, 'translate').and.returnValue('error message');
          spyOn((component as any).notificationService, 'showError');

          component.createRequest();
          tick();

          expect((component as any).notificationService.showError).toHaveBeenCalledWith('error message');
        }));
      });
    });
  });

  it('initializeForm - should initialize form', () => {
    component.form = null;

    (component as any).initializeForm();

    expect(component.form.get('agent')).toBeDefined();
    expect(component.form.get('gds')).toBeDefined();
    expect(component.form.get('documentType')).toBeDefined();
    expect(component.form.get('statisticalCode')).toBeDefined();
    expect(component.form.get('applicationQuantity')).toBeDefined();
  });

  describe('updateAvaliableATicketAmounts', () => {
    let agentControl;

    beforeEach(() => {
      (component as any).initializeForm();
      agentControl = component.form.get('agent');
      (component as any).updateAvaliableATicketAmounts();
    });

    it('should call get RMB with agent iata code for Agent user', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(of(null));

      agentControl.setValue({ code: 'agent-code' });
      tick();

      expect((component as any).stockApplicationRequestService.getAvailableAmount).toHaveBeenCalledWith('agent-code');
    }));

    it('should call get RMB with agent iata code for Agent Group user', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(of(null));

      agentControl.setValue({ iataCode: 'agent-code-2' });
      tick();

      expect((component as any).stockApplicationRequestService.getAvailableAmount).toHaveBeenCalledWith('agent-code-2');
    }));

    it('should set maxAvailableTickets to null when response is null', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(of(null));
      component.maxAvailableTickets = 100;

      agentControl.setValue({ iataCode: 'agent-code' });
      tick();

      expect(component.maxAvailableTickets).toBeNull();
    }));

    it('should NOT set isTicketsAmountWarningShowing to false when response is null', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(of(null));
      component.isTicketsAmountWarningShowing = true;

      agentControl.setValue({ iataCode: 'agent-code' });
      tick();

      expect(component.isTicketsAmountWarningShowing).toBe(true);
    }));

    it('should update maxAvailableTickets when response is NOT null', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(
        of({ availableAmount: 200 })
      );
      component.maxAvailableTickets = 100;

      agentControl.setValue({ iataCode: 'agent-code' });
      tick();

      expect(component.maxAvailableTickets).toBe(200);
    }));

    it('should update maxAvailableTickets when response is less than 0', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(
        of({ availableAmount: -200 })
      );
      component.maxAvailableTickets = 100;

      agentControl.setValue({ iataCode: 'agent-code' });
      tick();

      expect(component.maxAvailableTickets).toBe(0);
    }));

    it('should set maxAvailableTickets to null when response is error', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(throwError(''));
      component.maxAvailableTickets = 100;

      agentControl.setValue({ iataCode: 'agent-code' });
      tick();

      expect(component.maxAvailableTickets).toBeNull();
    }));

    it('should set isTicketsAmountWarningShowing to true when response is error', fakeAsync(() => {
      spyOn((component as any).stockApplicationRequestService, 'getAvailableAmount').and.returnValue(throwError(''));
      component.isTicketsAmountWarningShowing = false;

      agentControl.setValue({ iataCode: 'agent-code' });
      tick();

      expect(component.isTicketsAmountWarningShowing).toBe(true);
    }));
  });
});
