import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockProviders } from 'ng-mocks';

import { StockRequestFilterFormatter } from './data/stock-request-formatter';
import { StockRequestService } from './data/stock-request.service';
import { StockRequestListComponent } from './stock-request-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { QueryableDataSourceStub } from '~app/master-data/periods/shared/periods-mocks';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('StockRequestListComponent', () => {
  let component: StockRequestListComponent;
  let fixture: ComponentFixture<StockRequestListComponent>;
  let router: Router;

  const initialState = {
    auth: {
      user: {
        userType: UserType.AGENT
      }
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StockRequestListComponent, TranslatePipeMock],
      providers: [
        FormBuilder,
        provideMockStore({ initialState }),
        MockProviders(
          StockRequestFilterFormatter,
          DefaultQueryStorage,
          L10nTranslationService,
          StockRequestService,
          PermissionsService,
          AgentDictionaryService
        )
      ],
      imports: [RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(StockRequestListComponent, {
        set: {
          providers: [{ provide: QueryableDataSource, useClass: QueryableDataSourceStub }]
        }
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockRequestListComponent);
    component = fixture.componentInstance;

    router = TestBed.inject<any>(Router);
    spyOn(router, 'navigate');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
