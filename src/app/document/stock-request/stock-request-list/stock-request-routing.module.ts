import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StockRequestListComponent } from './stock-request-list.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: StockRequestListComponent,
    data: { tab: ROUTES.STOCK_REQUEST_LIST }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class StockRequestRoutingModule {}
