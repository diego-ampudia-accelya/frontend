import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { StockRequestService } from './data/stock-request.service';
import { StockRequestListComponent } from './stock-request-list.component';
import { StockRequestRoutingModule } from './stock-request-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, StockRequestRoutingModule],
  declarations: [StockRequestListComponent],
  providers: [StockRequestService]
})
export class StockRequestModule {}
