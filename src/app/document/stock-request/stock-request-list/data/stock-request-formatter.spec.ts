import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { DocumentType, StatisticalCode, StockRequestFilters } from '../../models/stock-request.model';

import { StockRequestFilterFormatter } from './stock-request-formatter';

describe('StockRequestFilterFormatter', () => {
  let formatter: StockRequestFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new StockRequestFilterFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format documentType if they exist', () => {
    const filter: StockRequestFilters = {
      documentType: DocumentType.eTicket
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['documentType'],
        label: 'document.stockRequest.filters.documentType.label - E-Ticket'
      }
    ]);
  });

  it('should format statisticalCode if they exist', () => {
    const filter: StockRequestFilters = {
      statisticalCode: StatisticalCode.domestic
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['statisticalCode'],
        label: 'document.stockRequest.filters.statisticalCode.label - Domestic'
      }
    ]);
  });

  it('should format request application date if they exist with or without range', () => {
    // Date range has been specified
    const filterWithRequestDateRange: StockRequestFilters = {
      applicationDateRange: [new Date('2022/07/14'), new Date('2022/07/28')]
    };
    // Unique date has been specified
    const filterWithUniqueRequestDate: StockRequestFilters = {
      applicationDateRange: [new Date('2022/07/14')]
    };

    expect(formatter.format(filterWithRequestDateRange)).toEqual([
      {
        keys: ['applicationDateRange'],
        label: 'document.stockRequest.filters.applicationDate.label - 14/07/2022 - 28/07/2022'
      }
    ]);

    expect(formatter.format(filterWithUniqueRequestDate)).toEqual([
      {
        keys: ['applicationDateRange'],
        label: 'document.stockRequest.filters.applicationDate.label - 14/07/2022'
      }
    ]);
  });

  it('should format applicationQuantityRange if they exist', () => {
    const filter: StockRequestFilters = {
      applicationQuantityRange: [2, 3]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['applicationQuantityRange'],
        label: 'document.stockRequest.filters.applicationQuantity.label: 2 - 3'
      }
    ]);
  });

  it('should format request review date if they exist with or without range', () => {
    // Date range has been specified
    const filterWithRequestDateRange: StockRequestFilters = {
      reviewDateRange: [new Date('2022/07/14'), new Date('2022/07/28')]
    };
    // Unique date has been specified
    const filterWithUniqueRequestDate: StockRequestFilters = {
      reviewDateRange: [new Date('2022/07/14')]
    };

    expect(formatter.format(filterWithRequestDateRange)).toEqual([
      {
        keys: ['reviewDateRange'],
        label: 'document.stockRequest.filters.reviewDate.label - 14/07/2022 - 28/07/2022'
      }
    ]);

    expect(formatter.format(filterWithUniqueRequestDate)).toEqual([
      {
        keys: ['reviewDateRange'],
        label: 'document.stockRequest.filters.reviewDate.label - 14/07/2022'
      }
    ]);
  });

  it('should format approvedQuantityRange if they exist', () => {
    const filter: StockRequestFilters = {
      approvedQuantityRange: [2, 3]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['approvedQuantityRange'],
        label: 'document.stockRequest.filters.approvedQuantity.label: 2 - 3'
      }
    ]);
  });

  it('should format reviewComment if they exist', () => {
    const filter: StockRequestFilters = {
      reviewComment: 'Comment about payment'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['reviewComment'],
        label: 'document.stockRequest.filters.reviewComment.label - Comment about payment'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: StockRequestFilters = {
      documentType: null,
      statisticalCode: null
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
