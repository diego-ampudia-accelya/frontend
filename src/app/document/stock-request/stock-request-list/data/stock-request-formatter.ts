import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { StockRequestFilters } from '../../models/stock-request.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper, rangeFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class StockRequestFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: StockRequestFilters): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<StockRequestFilters>> = {
      agentCode: agent => `${this.translate('agentCode')} - ${agent.code}`,
      documentType: documentType => `${this.translate('documentType')} - ${documentType}`,
      statisticalCode: statisticalCode => `${this.translate('statisticalCode')} - ${statisticalCode}`,
      applicationDateRange: applicationDateRange =>
        `${this.translate('applicationDate')} - ${rangeDateFilterTagMapper(applicationDateRange)}`,
      applicationQuantityRange: applicationQuantityRange =>
        `${this.translate('applicationQuantity')}: ${rangeFilterTagMapper(applicationQuantityRange)}`,
      reviewDateRange: reviewDateRange =>
        `${this.translate('reviewDate')} - ${rangeDateFilterTagMapper(reviewDateRange)}`,
      approvedQuantityRange: approvedQuantityRange =>
        `${this.translate('approvedQuantity')}: ${rangeFilterTagMapper(approvedQuantityRange)}`,
      reviewComment: reviewComment => `${this.translate('reviewComment')} - ${reviewComment}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`document.stockRequest.filters.${key}.label`);
  }
}
