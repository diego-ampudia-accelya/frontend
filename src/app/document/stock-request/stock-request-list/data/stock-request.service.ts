import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { StockRequest, StockRequestFilters } from '../../models/stock-request.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class StockRequestService implements Queryable<StockRequest> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/asd-stock-request/stock-request`;
  private sortMapper = [
    {
      from: 'reviewDate',
      to: 'approveDate'
    },
    {
      from: 'applicationQuantity',
      to: 'applyTktQty'
    },
    {
      from: 'approvedQuantity',
      to: 'applyTktResultQty'
    },
    {
      from: 'reviewComment',
      to: 'reason'
    },
    {
      from: 'applicationDate',
      to: 'applyDate'
    },
    {
      from: 'agent.iataCode',
      to: 'iataNumber'
    }
  ];

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<StockRequestFilters>): Observable<PagedData<StockRequest>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<StockRequest>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<StockRequestFilters>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions.toUpperCase() };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<StockRequestFilters>): RequestQuery {
    const {
      agentCode,
      applicationDateRange,
      applicationQuantityRange,
      reviewDateRange,
      approvedQuantityRange,
      ...rest
    } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      sortBy: query.sortBy && formatSortBy(query.sortBy, this.sortMapper),
      filterBy: {
        iataNumber: agentCode && agentCode.code,
        requestDateFrom: applicationDateRange && toShortIsoDate(applicationDateRange[0]),
        requestDateTo:
          applicationDateRange &&
          toShortIsoDate(applicationDateRange[1] ? applicationDateRange[1] : applicationDateRange[0]),
        applicationQuantityFrom: applicationQuantityRange && applicationQuantityRange[0],
        applicationQuantityTo: applicationQuantityRange && applicationQuantityRange[1],
        reviewDateFrom: reviewDateRange && toShortIsoDate(reviewDateRange[0]),
        reviewDateTo: reviewDateRange && toShortIsoDate(reviewDateRange[1] ? reviewDateRange[1] : reviewDateRange[0]),
        approvedQuantityFrom: approvedQuantityRange && approvedQuantityRange[0],
        approvedQuantityTo: approvedQuantityRange && approvedQuantityRange[1],
        ...rest
      }
    });
  }
}
