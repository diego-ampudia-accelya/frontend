import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { DocumentType, StatisticalCode, StockRequestFilters } from '../models/stock-request.model';
import { StockRequestFilterFormatter } from './data/stock-request-formatter';
import { StockRequestService } from './data/stock-request.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { FormUtil, toValueLabelObject } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-stock-request-list',
  templateUrl: './stock-request-list.component.html',
  providers: [
    StockRequestFilterFormatter,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: StockRequestService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [StockRequestService]
    }
  ]
})
export class StockRequestListComponent implements OnInit, OnDestroy {
  public customLabels = { create: 'document.stockRequest.requestButton' };
  public columns: GridColumn[] = [];
  public searchForm: FormGroup;

  public documentTypeOptions: DropdownOption[] = [];
  public statisticalCodeOptions: DropdownOption[] = [];
  public canRequestStock = this.permissionService.hasPermission(Permissions.createStockApplicationRequest);

  public agentOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public isAgentUser: boolean;
  public isIataUser: boolean;

  private agentDropdownParam = null;
  private formFactory: FormUtil;
  private destroy$ = new Subject();

  constructor(
    public displayFilterFormatter: StockRequestFilterFormatter,
    public dataSource: QueryableDataSource<StockRequestFilters>,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private queryStorage: DefaultQueryStorage,
    private router: Router,
    private permissionService: PermissionsService,
    private store: Store<AppState>,
    private agentDictionaryService: AgentDictionaryService,
    private dialogService: DialogService,
    private stockRequestService: StockRequestService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeLoggedUserType();
    this.initializeDropdowns();
    this.columns = this.buildColumns();
    this.searchForm = this.buildSearchForm();
    const storedQuery = this.queryStorage.get();
    this.loadData(storedQuery);
  }

  public loadData(query?: DataQuery) {
    query = query || cloneDeep(defaultQuery);
    this.dataSource.get(query);
    this.queryStorage.save(query);
  }

  public onDownload() {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('document.stockRequest.downloadDialogTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.stockRequestService
    });
  }

  public createStockRequest() {
    this.router.navigateByUrl(ROUTES.STOCK_APPLICATION_REQUEST.url);
  }

  private initializeLoggedUserType(): void {
    this.store.pipe(select(getUser), first(), takeUntil(this.destroy$)).subscribe(({ userType, id }: User) => {
      this.isAgentUser = userType === UserType.AGENT;
      this.isIataUser = userType === UserType.IATA;

      if (userType === UserType.AGENT_GROUP) {
        this.agentDropdownParam = {
          agentGroupId: id
        };
      }
    });
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<StockRequestFilters>({
      agentCode: [],
      documentType: [],
      statisticalCode: [],
      applicationDateRange: [],
      applicationQuantityRange: [],
      reviewDateRange: [],
      approvedQuantityRange: [],
      reviewComment: []
    });
  }

  private buildColumns(): GridColumn[] {
    return [
      {
        prop: 'bsp.isoCountryCode',
        name: 'document.stockRequest.columns.bsp',
        flexGrow: 1
      },
      {
        prop: 'agent.iataCode',
        name: 'document.stockRequest.columns.agentCode',
        flexGrow: 1,
        hidden: this.isAgentUser
      },
      {
        prop: 'documentType',
        name: 'document.stockRequest.columns.documentType',
        flexGrow: 1
      },
      {
        prop: 'statisticalCode',
        name: 'document.stockRequest.columns.statisticalCode',
        flexGrow: 1
      },
      {
        prop: 'applicationDate',
        name: 'document.stockRequest.columns.applicationDate',
        flexGrow: 1
      },
      {
        prop: 'applicationQuantity',
        name: 'document.stockRequest.columns.applicationQuantity',
        flexGrow: 1
      },
      {
        prop: 'approveDate',
        name: 'document.stockRequest.columns.reviewDate',
        flexGrow: 1
      },
      {
        prop: 'approvedQuantity',
        name: 'document.stockRequest.columns.approvedQuantity',
        flexGrow: 1
      },
      {
        prop: 'reviewComment',
        name: 'document.stockRequest.columns.reviewComment',
        flexGrow: 1
      }
    ];
  }

  private initializeDropdowns(): void {
    this.documentTypeOptions = Object.values(DocumentType)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.getTranslatedDocumentType(label)
      }));

    this.statisticalCodeOptions = Object.values(StatisticalCode)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.getTranslatedStatisticalCode(label)
      }));

    if (!this.isAgentUser) {
      this.agentOptions$ = this.agentDictionaryService.getDropdownOptions(this.agentDropdownParam);
    }
  }

  private getTranslatedDocumentType(documentType: DocumentType): string {
    return this.translationService.translate(
      `document.stockRequest.filters.documentType.options.${documentType.toLowerCase()}`
    );
  }

  private getTranslatedStatisticalCode(statisticalCode: StatisticalCode): string {
    return this.translationService.translate(
      `document.stockRequest.filters.statisticalCode.options.${statisticalCode.toLowerCase()}`
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
