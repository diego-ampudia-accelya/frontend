import { DocumentType, StatisticalCode } from './stock-request.model';
import { Agent } from '~app/master-data/models';
import { Gds } from '~app/master-data/models/gds.model';
import { AgentSummary } from '~app/shared/models';

export interface StockApplicationRequestForm {
  agent: Agent | AgentSummary;
  gds: Gds;
  documentType: DocumentType;
  statisticalCode: StatisticalCode;
  applicationQuantity: number;
}

export interface StockApplicationRequest {
  gdsCode: string;
  agentIataCode: string;
  documentType: string;
  statisticalCode: string;
  requestedQty: number;
}

export interface StockRequestRMBResponse {
  errors?: ResultError[];
  eventId?: string;
  iataNumber: string;
  availableAmount: number;
  message?: string;
  status?: string;
}

interface ResultError {
  error: string;
  errorCode: string;
  name: string;
}

export const TRAVELSKY_GDS_CODE = 'MINS';
