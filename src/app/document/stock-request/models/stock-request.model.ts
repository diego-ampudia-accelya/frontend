import { Agent } from '~app/master-data/models';
import { Gds } from '~app/master-data/models/gds.model';
import { AgentSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';

export interface StockRequest {
  id?: number;
  bsp?: BspDto;
  channel?: string;
  agentCode?: string;
  documentType?: DocumentType;
  statisticalCode?: StatisticalCode;
  applicationDate?: string;
  applicationQuantity?: number;
  reviewDate?: string;
  approvedQuantity?: number;
  reviewComment?: string;
  agent?: Agent;
  gds?: Gds;
}

export interface StockRequestFilters {
  id?: number;
  agentCode?: AgentSummary;
  documentType?: DocumentType;
  statisticalCode?: StatisticalCode;
  applicationDateRange?: Date[];
  applicationQuantityRange?: number[];
  reviewDateRange?: Date[];
  approvedQuantityRange?: number[];
  reviewComment?: string;
}

export interface StockRequestFiltersBE {
  id?: number;
  documentType?: DocumentType;
  statisticalCode?: StatisticalCode;
  applicationDateFrom?: string;
  applicationDateTo?: string;
  applicationQuantityFrom?: number;
  applicationQuantityTo?: number;
  reviewDateFrom?: string;
  reviewDateTo?: string;
  approvedQuantityFrom?: number;
  approvedQuantityTo?: number;
  reviewComment?: string;
}

export enum DocumentType {
  eTicket = 'E-Ticket',
  emd = 'EMD'
}

export enum StatisticalCode {
  domestic = 'Domestic',
  international = 'International'
}
