import { DocumentType, StatisticalCode } from './stock-request.model';

export const CREATE_STOCK_APPLICATION_FORM = {
  DOCUMENT_TYPE_OPTIONS: [
    {
      value: DocumentType.eTicket,
      label: DocumentType.eTicket
    },
    {
      value: DocumentType.emd,
      label: DocumentType.emd
    }
  ],
  STATISTICAL_CODE_OPTIONS: [
    {
      value: StatisticalCode.domestic,
      label: StatisticalCode.domestic
    },
    {
      value: StatisticalCode.international,
      label: StatisticalCode.international
    }
  ]
};
