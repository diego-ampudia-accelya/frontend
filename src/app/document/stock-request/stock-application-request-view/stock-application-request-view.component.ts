import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { StockRequest } from '../models/stock-request.model';

@Component({
  selector: 'bspl-stock-application-request-view',
  templateUrl: './stock-application-request-view.component.html',
  styleUrls: ['./stock-application-request-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockApplicationRequestViewComponent {
  @Input() data: StockRequest = {};
}
