import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockApplicationRequestViewComponent } from './stock-application-request-view.component';
import { TranslatePipeMock } from '~app/test';

describe('StockApplicationRequestViewComponent', () => {
  let component: StockApplicationRequestViewComponent;
  let fixture: ComponentFixture<StockApplicationRequestViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StockApplicationRequestViewComponent, TranslatePipeMock]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockApplicationRequestViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
