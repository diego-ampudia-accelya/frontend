import { FormOfPaymentType } from '../enums';
import { CANX } from '../models/canx.model';

export const canx: CANX = {
  id: '0123456789-88-CANX8hfTUlTWUlOREhPVF9yCAN6MTExNS56aXA==',
  type: 'CANX',
  isoCountryCode: 'CH',
  accountInformation: {
    agentCommissionAmount: 0.0,
    agentCommissionPercent: 0.0,
    balancePayableAirline: 10.93,
    cancellationFees: 0.0,
    cancellationFeesCommission: 0.0,
    fareAdjustmentAmount: 0.0,
    netFareAmount: 10.93,
    onlineBillingStatementForAgent: 10.93,
    vatGstAmount: 0.0
  },
  agent: {
    iataCode: '0230010',
    id: 15535,
    name: 'GBT AUSTRALIA PTY',
    vatNumber: '19191989'
  },
  airline: {
    globalAirline: {
      globalName: 'VIRGIN AUSTRALIA',
      iataCode: '220',
      id: 11124,
      logo: 'virginAustraliaLogo'
    },
    id: 0,
    localName: 'VIRGIN AUSTRALIA',
    vatNumber: 'vatNumber'
  },
  bsp: {
    id: 10084,
    isoCountryCode: 'CH',
    name: 'Switzerland'
  },
  auditCouponAgentCode: '8120317-2',
  billingAnalysisPeriodCode: '2019062',
  cashAmount: 0,
  easyPayAmount: 0,
  creditAmount: 0,
  captureDate: '2019-11-16',
  settlementPeriod: '2019112',
  commissions: [
    {
      amountPaidByCustomer: 0.0,
      commissionAmount: 0.0,
      commissionRate: 0.0,
      commissionType: 'XLP',
      effectiveCommissionAmount: 0.0,
      effectiveCommissionRate: 0.0,
      routingIndicator: 'R',
      statisticalCode: 'I',
      supplementaryAmount: 0.0,
      supplementaryRate: 0.0,
      supplementaryType: 'ST'
    }
  ],
  currencyType: 'CHF',
  numberOfDecimals: 2,
  documentIdentification: {
    agentCode: '0230023',
    checkDigit: '4',
    dateOfIssue: '2019-01-01',
    documentNumber: '0123456789',
    fullDocumentNumber: 'string',
    transactionCode: 'CANX',
    timeOfIssue: '0000'
  },
  formOfPayments: {
    totalPaymentAmount: 0,
    allPayments: [
      {
        type: FormOfPaymentType.VD,
        description: 'VOID',
        amount: 0
      }
    ]
  },
  gds: {
    id: 11758,
    gdsCode: 'EARS',
    name: 'BSPlink'
  },
  stdAmounts: {
    commissionableAmount: 10.93,
    netFareAmount: 0.0,
    ticketAmount: 10.93,
    taxes: [],
    summarizedTaxes: [],
    totalTaxes: 0.0
  },
  transaction: { airlineCode: '047', reportingSystemIdentifier: 'EARS' }
};
