import { FormOfPaymentType } from '../enums';
import { ACDM } from '../models';

export const adm: ACDM = {
  id: '0588053833-6035-Q0hfTUlTWUlOREhPVF8yMDE5MTExNS56aXA=',
  type: 'ACDM',
  bsp: { id: 10084, isoCountryCode: 'CH', name: 'Switzerland' },
  airline: {
    localName: 'TAP Portugal',
    vatNumber: 'vat#forTAPPortugal',
    globalAirline: { id: 10039, iataCode: '047', globalName: 'TAP Portugal', logo: 'LogoTAPPortugal' }
  },
  agent: { iataCode: '8120670' },
  gds: { gdsCode: 'EARS', name: 'EARS', id: 0 },
  isoCountryCode: 'CH',
  currencyType: 'CHF',
  numberOfDecimals: 2,
  auditCouponAgentCode: '8120670-5',
  cashAmount: 10.93,
  creditAmount: 0.0,
  easyPayAmount: 0.0,
  transaction: { airlineCode: '047', reportingSystemIdentifier: 'EARS' },
  documentIdentification: {
    dateOfIssue: '2019-10-25',
    documentNumber: '0588053833',
    checkDigit: '3',
    agentCode: '8120670',
    transactionCode: 'ADMA',
    timeOfIssue: '0000'
  },
  stdAmounts: {
    commissionableAmount: 10.93,
    netFareAmount: 0.0,
    ticketAmount: 10.93,
    taxes: [],
    summarizedTaxes: [],
    totalTaxes: 0.0
  },
  formOfPayments: {
    totalPaymentAmount: 10.93,
    allPayments: [{ type: FormOfPaymentType.CA, description: 'CASH', amount: 10.93 }]
  },
  accountInformation: {
    vatGstAmount: 0.0,
    agentCommissionPercent: 0.0,
    agentCommissionAmount: 0.0,
    fareAdjustmentAmount: 0.0,
    cancellationFees: 0.0,
    cancellationFeesCommission: 0.0,
    onlineBillingStatementForAgent: 10.93,
    balancePayableAirline: 10.93,
    netFareAmount: 10.93
  },
  commissions: [
    {
      statisticalCode: 'I',
      commissionRate: 0,
      commissionAmount: 0.0,
      supplementaryRate: 0,
      supplementaryAmount: 0.0,
      effectiveCommissionRate: 0,
      effectiveCommissionAmount: 0.0,
      amountPaidByCustomer: 0.0
    }
  ],
  captureDate: '2019-11-16',
  settlementPeriod: '2019112',
  relatedDocumentInformation: [
    {
      remittancePeriodEndingDate: '2019-11-15',
      airlineCode: '047',
      relatedDocument: '1105546175',
      memoIssuanceCode: 'CHUR',
      dateOfIssue: '2019-08-31'
    }
  ]
};
