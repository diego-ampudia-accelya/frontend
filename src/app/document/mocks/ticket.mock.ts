import { FormOfPaymentType } from '../enums';
import { Ticket } from '../models';

export const ticket: Ticket = {
  id: '5504141100-6-Q0hfTUlTWUlOREhPVF8yMDE5MTEzMC56aXA=',
  type: 'TICKET',
  bsp: { id: 10084, isoCountryCode: 'CH', name: 'Switzerland' },
  airline: {
    id: 10040,
    localName: 'TAP Portugal',
    vatNumber: 'vat#forTAPPortugal',
    globalAirline: { id: 10039, iataCode: '047', globalName: 'TAP Portugal', logo: 'LogoTAPPortugal' }
  },
  agent: { iataCode: '8120012' },
  gds: { id: 10024, gdsCode: 'AGTD', name: 'AMADEUS' },
  isoCountryCode: 'CH',
  currencyType: 'CHF',
  numberOfDecimals: 2,
  auditCouponAgentCode: '8120012-5',
  cashAmount: 0.0,
  creditAmount: 0.0,
  easyPayAmount: 0.0,
  transaction: { airlineCode: '076', reportingSystemIdentifier: 'AGTD' },
  documentIdentification: {
    dateOfIssue: '2019-11-30',
    documentNumber: '5504141100',
    checkDigit: '6',
    couponUseIndicator: 'FFVV',
    agentCode: '8120012',
    transactionCode: 'TKTT',
    originCityCodes: 'GVAGVA',
    pnrReference: 'RUBW4W/1A',
    timeOfIssue: '0000'
  },
  stdAmounts: {
    commissionableAmount: 0.0,
    netFareAmount: 0.0,
    ticketAmount: 0.15,
    taxes: [{ type: 'LB', amount: 0.15 }],
    summarizedTaxes: [{ type: 'LB', amount: 0.15 }],
    totalTaxes: 0.15
  },
  formOfPayments: {
    totalPaymentAmount: 0.15,
    allPayments: [
      {
        type: FormOfPaymentType.EX,
        description: 'EXCHANGED',
        accountNumber: '0765504058741512',
        maskedAccountNumber: '076550XXXXXXXX',
        amount: 0.0,
        documentNumber: '5504058741',
        airlineCode: '076',
        coupons: 'FFVV'
      },
      {
        type: FormOfPaymentType.CC,
        description: 'CREDIT',
        entity: 'TP',
        maskedAccountNumber: '192072XXXXXXXX',
        amount: 0.15,
        approvalCode: '189J',
        expiryDate: '2022-10-31',
        invoiceNumber: 'TP076191201324'
      },
      { type: FormOfPaymentType.CA, description: 'CASH', amount: 0.0 }
    ]
  },
  formOfPaymentAdditionalInformation: [
    { paymentSequenceNumber: 1, paymentInformation: 'CC/5//TP/XXXXXXXXXXX5517/1022 CC/3/0.15/TP/XXXXXXX' },
    { paymentSequenceNumber: 2, paymentInformation: 'XXXX5517/1022/189J/S' }
  ],
  accountInformation: {
    vatGstAmount: 0.0,
    agentCommissionPercent: 0.0,
    agentCommissionAmount: 0.0,
    fareAdjustmentAmount: 0.0,
    cancellationFees: 0.0,
    cancellationFeesCommission: 0.0,
    onlineBillingStatementForAgent: 0.0,
    balancePayableAirline: 0.15,
    netFareAmount: 0.0
  },
  commissions: [
    {
      statisticalCode: 'I',
      commissionRate: 0,
      commissionAmount: 0.0,
      supplementaryRate: 0,
      supplementaryAmount: 0.0,
      effectiveCommissionRate: 0,
      effectiveCommissionAmount: 0.0,
      amountPaidByCustomer: 0.0,
      routingIndicator: 'I'
    }
  ],
  issueInformationForSales: [
    {
      originalIssueDocumentNumber: '0763494408729',
      originalIssueCityCode: 'LUG',
      originalIssueDate: '2019-04-10',
      iataNumber: '81200125',
      endorsementsRestrictions: 'CHF50.00 NONREF - VALID ON MEA ONLY NON-ENDO'
    }
  ],
  itineraryData: [
    {
      segmentIdentifier: 1,
      stopoverCode: 'O',
      notValidBeforeDate: '02DEC',
      notValidAfterDate: '02DEC',
      originAirportCode: 'GVA',
      destinationAirportCode: 'BEY',
      carrier: 'ME',
      flightNumber: '0214',
      reservationBookingDesignator: 'L',
      flightDepartureTime: '1145',
      flightBookingStatus: 'OK',
      baggageAllowance: '1PC',
      fareTicketDesignator: 'LLRCH'
    },
    {
      segmentIdentifier: 2,
      stopoverCode: 'O',
      notValidBeforeDate: '09DEC',
      notValidAfterDate: '09DEC',
      originAirportCode: 'BEY',
      destinationAirportCode: 'GVA',
      carrier: 'ME',
      flightNumber: '0213',
      reservationBookingDesignator: 'L',
      flightDepartureTime: '0725',
      flightBookingStatus: 'OK',
      baggageAllowance: '1PC',
      fareTicketDesignator: 'LLRCH'
    }
  ],
  documentAmounts: {
    fare: { currency: 'CHF', amount: '   400.00' },
    total: { currency: 'CHF', amount: '    0.15A' },
    servicingAirlineProviderIdentifier: '7906',
    fareCalculationModeIndicator: '0',
    bookingAgentIdentification: '9999WS',
    fareCalculationPricingIndicator: '0'
  },
  passengerInformation: { name: 'XXXXXXXXXXXXXXX XX', dateOfBirth: '2066-02-08' },
  conjunctionTickets: [],
  billingAnalysisPeriodCode: '2019114',
  commissionPercent: 0.0,
  originalIssue: ['0765504058741'],
  fareCalculationArea:
    'EDI EI DUB36.68ZUK26GDS EI X/CHI Q156.39AA DFW3961.95J26PUBRT UA X/CHI EI DUB Q156.39 1743.49D26PUBRT EI EDI104.61NUK26GDS NUC6159.51END ROE0.736064 XF ORD4.5',
  additionalCardInformation: [],
  remittanceArea: {
    sumCash: 0.0,
    sumCredit: 0.0,
    sumEasyPay: 0.0,
    commissionPercent: 0.0,
    sumTaxAmount: 0.15
  }
};
