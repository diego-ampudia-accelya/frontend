import { FormOfPaymentType } from '../enums';
import { Refund } from '../models';

export const refund: Refund = {
  id: '3494501556-169-Q0hlbDE1NzNfMjAxOTA0MzBfMTkwNDMwX0JfMDEuemlw',
  type: 'REFUND',
  bsp: { id: 10084, isoCountryCode: 'CH', name: 'Switzerland' },
  airline: {
    id: 10040,
    localName: 'TAP Portugal',
    vatNumber: 'vat#forTAPPortugal',
    globalAirline: { id: 10039, iataCode: '047', globalName: 'TAP Portugal', logo: 'LogoTAPPortugal' }
  },
  agent: { id: 10252, iataCode: '8121157', name: 'Agency 8121157', vatNumber: '19191989' },
  gds: { id: 10024, gdsCode: 'AGTD', name: 'AMADEUS' },
  isoCountryCode: 'CH',
  currencyType: 'CHF',
  numberOfDecimals: 2,
  auditCouponAgentCode: '8121157-2',
  cashAmount: 0.0,
  creditAmount: 0.0,
  easyPayAmount: 0.0,
  transaction: { airlineCode: '157', reportingSystemIdentifier: 'AGTD', dataInputStatusIndicator: 'F' },
  documentIdentification: {
    dateOfIssue: '2019-04-29',
    documentNumber: '3494501556',
    checkDigit: '0',
    agentCode: '8121157',
    transactionCode: 'RFND',
    timeOfIssue: '0000'
  },
  stdAmounts: {
    commissionableAmount: -4732.0,
    netFareAmount: 0.0,
    ticketAmount: -5334.4,
    taxes: [
      { type: 'YQ', amount: -534.0 },
      { type: 'YR', amount: -8.2 }
    ],
    summarizedTaxes: [
      { type: 'YQ', amount: -534.0 },
      { type: 'YR', amount: -8.2 },
      { type: 'XT', amount: -60.2 }
    ],
    totalTaxes: -602.4
  },
  formOfPayments: {
    totalPaymentAmount: -5334.4,
    allPayments: [{ type: FormOfPaymentType.CA, description: 'CASH', amount: -5334.4 }]
  },
  accountInformation: {
    vatGstAmount: 0.0,
    agentCommissionPercent: 0.0,
    agentCommissionAmount: 0.0,
    fareAdjustmentAmount: 0.0,
    cancellationFees: 0.0,
    cancellationFeesCommission: 0.0,
    onlineBillingStatementForAgent: -5334.4,
    balancePayableAirline: -5334.4,
    netFareAmount: -4732.0
  },
  commissions: [
    {
      statisticalCode: 'I',
      commissionRate: 0,
      commissionAmount: 0.0,
      supplementaryRate: 0,
      supplementaryAmount: 0.0,
      effectiveCommissionRate: 0,
      effectiveCommissionAmount: 0.0,
      amountPaidByCustomer: 0.0,
      routingIndicator: 'I'
    }
  ],
  captureDate: '2019-04-30',
  settlementPeriod: '2019044',
  relatedDocumentInformation: [
    {
      remittancePeriodEndingDate: '2019-04-30',
      airlineCode: '157',
      relatedDocument: '3494501556',
      couponNumberIdentifier: '1200',
      dateOfIssue: '2019-04-23'
    }
  ]
};
