import { FormOfPaymentType } from '../enums';
import { TASF } from '../models/tasf.model';

export const tasf: TASF = {
  id: '5069573724-11-c291cmNlLnppcA==',
  type: 'TASF',
  bsp: {
    id: 10000,
    isoCountryCode: 'CH',
    name: 'Switzerland'
  },
  airline: {
    id: 10001,
    localName: 'Lufthansa',
    vatNumber: 'LufthansaVatNumber',
    globalAirline: {
      id: 10002,
      iataCode: '220',
      globalName: 'Lufthansa',
      logo: 'LufthansaLogo'
    }
  },
  agent: {
    id: 10003,
    iataCode: '0230023',
    name: 'CONSOLIDATED TRAVEL',
    vatNumber: 'vatNumber'
  },
  gds: {
    id: 10006,
    gdsCode: 'AGTD',
    name: 'AMADEUS'
  },
  isoCountryCode: 'CH',
  billingAnalysisPeriodCode: '2019011',
  currencyType: 'CHF',
  numberOfDecimals: 2,
  auditCouponAgentCode: '0230023-3',
  cashAmount: 0.0,
  creditAmount: 52.95,
  easyPayAmount: 50.6,
  transaction: {
    airlineCode: '220',
    reportingSystemIdentifier: 'AGTD'
  },
  documentIdentification: {
    dateOfIssue: '2019-01-01',
    documentNumber: '5069573724',
    checkDigit: '4',
    agentCode: '0230023',
    reasonIssuanceCode: 'A',
    transactionCode: 'TASF',
    originCityCodes: 'ZRHZRH',
    pnrReference: 'WFQZG4/1G'
  },
  stdAmounts: {
    commissionableAmount: 103.55,
    netFareAmount: 0.0,
    ticketAmount: 123.45,
    taxes: [
      {
        type: 'CC',
        amount: 12.0
      },
      {
        type: 'EP',
        amount: 10.0
      },
      {
        type: 'CA',
        amount: 11.3
      }
    ],
    summarizedTaxes: [
      {
        type: 'CC',
        amount: 12.0
      },
      {
        type: 'EP',
        amount: 10.0
      },
      {
        type: 'CA',
        amount: 11.3
      }
    ],
    totalTaxes: 33.3
  },
  formOfPayments: {
    totalPaymentAmount: 147.45,
    allPayments: [
      {
        type: FormOfPaymentType.EX,
        description: 'EXCHANGED',
        accountNumber: '22034945983971  34',
        maskedAccountNumber: '220349XXXXXXXX',
        amount: 0.0,
        documentNumber: '3494598397',
        airlineCode: '220',
        coupons: 'VVFF'
      },
      {
        type: FormOfPaymentType.CC,
        description: 'CREDIT',
        entity: 'AX',
        maskedAccountNumber: '376061XXXXXXXX',
        amount: 55.0,
        approvalCode: '190774',
        expiryDate: '2023-11-30',
        invoiceNumber: 'AX795190613728'
      },
      {
        type: FormOfPaymentType.EP,
        description: 'EASY PAY',
        entity: 'EP',
        maskedAccountNumber: '288061XXXXXXXX',
        amount: 50.6
      },
      {
        type: FormOfPaymentType.CA,
        description: 'CASH',
        amount: 41.85
      }
    ]
  },
  accountInformation: {
    vatGstAmount: -0.1,
    agentCommissionPercent: 6.8,
    agentCommissionAmount: 18.26,
    fareAdjustmentAmount: 3.21,
    cancellationFees: 0.0,
    cancellationFeesCommission: 0.0,
    onlineBillingStatementForAgent: 148.81,
    balancePayableAirline: 165.61,
    netFareAmount: 127.1
  },
  commissions: [
    {
      statisticalCode: 'I',
      commissionRate: 120,
      commissionAmount: 12.4,
      supplementaryRate: 220,
      supplementaryAmount: 3.21,
      effectiveCommissionRate: 1,
      effectiveCommissionAmount: 23.55,
      amountPaidByCustomer: 45.69
    },
    {
      statisticalCode: 'I',
      commissionRate: 120,
      commissionAmount: 1.2,
      supplementaryRate: 220,
      supplementaryAmount: 1.45,
      effectiveCommissionRate: 1,
      effectiveCommissionAmount: 4.32,
      amountPaidByCustomer: 23.41
    }
  ]
};
