import { FormOfPaymentType } from '../enums';
import { EMD } from '../models/emd.model';

export const emd: EMD = {
  id: '3096060992-10298-Q0hfTUlTWUlOREhPVF8yMDE5MTExNC56aXA=',
  type: 'EMD',
  bsp: { id: 10084, isoCountryCode: 'CH', name: 'Switzerland' },
  airline: {
    localName: 'TAP Portugal',
    vatNumber: 'vat#forTAPPortugal',
    globalAirline: { id: 10039, iataCode: '047', globalName: 'TAP Portugal', logo: 'LogoTAPPortugal' }
  },
  agent: { id: 10244, iataCode: '8121114', name: 'Agency 8121114', vatNumber: '19191989' },
  gds: { id: 10036, gdsCode: 'SABR', name: 'SABRE' },
  isoCountryCode: 'CH',
  currencyType: 'CHF',
  numberOfDecimals: 2,
  auditCouponAgentCode: '8121114-1',
  cashAmount: 30.7,
  creditAmount: 0.0,
  easyPayAmount: 0.0,
  transaction: { airlineCode: '047', reportingSystemIdentifier: 'SABR' },
  documentIdentification: {
    dateOfIssue: '2019-11-14',
    documentNumber: '3096060992',
    checkDigit: '2',
    couponUseIndicator: 'FFVV',
    agentCode: '8121114',
    reasonIssuanceCode: 'A',
    transactionCode: 'EMDA',
    pnrReference: 'RUIPME/AA',
    timeOfIssue: '0000'
  },
  stdAmounts: {
    commissionableAmount: 30.7,
    netFareAmount: 0.0,
    ticketAmount: 30.7,
    taxes: [],
    summarizedTaxes: [],
    totalTaxes: 0.0
  },
  formOfPayments: {
    totalPaymentAmount: 30.7,
    allPayments: [{ type: FormOfPaymentType.CA, description: 'CASH', amount: 30.7 }]
  },
  formOfPaymentAdditionalInformation: [{ paymentSequenceNumber: 1, paymentInformation: 'CASH' }],
  accountInformation: {
    vatGstAmount: 0.0,
    agentCommissionPercent: 0.0,
    agentCommissionAmount: 0.0,
    fareAdjustmentAmount: 0.0,
    cancellationFees: 0.0,
    cancellationFeesCommission: 0.0,
    onlineBillingStatementForAgent: 30.7,
    balancePayableAirline: 30.7,
    netFareAmount: 30.7
  },
  commissions: [
    {
      statisticalCode: 'I',
      commissionRate: 0,
      commissionAmount: 0.0,
      supplementaryRate: 0,
      supplementaryAmount: 0.0,
      effectiveCommissionRate: 0,
      effectiveCommissionAmount: 0.0,
      amountPaidByCustomer: 0.0
    }
  ],
  issueInformationForSales: [{ iataNumber: '0', endorsementsRestrictions: 'VALID FOR EXCHANGE ONLY' }],
  itineraryData: [
    { segmentIdentifier: 1, originAirportCode: 'ZRH', destinationAirportCode: 'OPO', carrier: 'TP' },
    { segmentIdentifier: 2, originAirportCode: 'OPO', destinationAirportCode: 'ZRH', carrier: 'TP' }
  ],
  documentAmounts: {
    fare: { currency: 'CHF', amount: '    30.70' },
    total: { currency: 'CHF', amount: '    30.70' },
    servicingAirlineProviderIdentifier: '0011',
    fareCalculationModeIndicator: '0',
    fareCalculationPricingIndicator: '0'
  },
  passengerInformation: { name: 'XXXXXXXXXXX XXX' },
  conjunctionTickets: [],
  emdCouponDetails: [
    {
      couponNumber: 1,
      relatedDocument: '6006595856',
      reasonForIssuanceCode: '0B5'
    },
    {
      couponNumber: 2,
      relatedDocument: '6006595856',
      reasonForIssuanceCode: '0B5'
    }
  ],
  emdCouponRemarks: [
    { couponNumber: 1, remarks: 'PRE RESERVED SEAT ASSIGNMENT' },
    { couponNumber: 2, remarks: 'PRE RESERVED SEAT ASSIGNMENT' }
  ]
};
