import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DocumentViewComponent } from './components/document-view/document-view.component';
import { DocumentCreditCardGuard } from './services/document-credit-card.guard';
import { DocumentCreditCardResolver } from './services/document-credit-card.resolver';
import { DocumentEnquiryGuard } from './services/document-enquiry.guard';
import { DocumentNotFoundResolver } from './services/document-not-found.resolver';
import { DocumentViewGuard } from './services/document-view.guard';
import { DocumentResolver } from './services/document.resolver';
import { StockApplicationRequestComponent } from './stock-request/stock-application-request/stock-application-request.component';
import { MenuTab, MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';

export function getDocumentEnquiryTabLabel(tabConfig: any): string {
  const documentIdentification = tabConfig.document.documentIdentification;
  const documentNumber = tabConfig.document.conjunctionDocumentNumber || documentIdentification.documentNumber;

  return `${documentIdentification.transactionCode} - ${documentNumber}`;
}

export function getDocumentViewNotFoundTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;
  const documentNumber: string = routeData.documentNumber;

  return { label: tab.label as string, params: { documentNumber } };
}

const routes: Routes = [
  {
    path: `${ROUTES.DOCUMENT_ENQUIRY.path}`,
    canActivate: [DocumentEnquiryGuard],
    data: {
      requiredPermissions: Permissions.readDocumentDetails
    }
  },
  {
    path: `${ROUTES.DOCUMENT_ENQUIRY.path}/:documentNumber`,
    canActivate: [DocumentEnquiryGuard],
    data: {
      requiredPermissions: Permissions.readDocumentDetails
    }
  },
  {
    path: ROUTES.REJECTED_DOCUMENTS.path,
    loadChildren: () => import('./rejected-documents/rejected-documents.module').then(m => m.RejectedDocumentModule)
  },
  {
    path: `${ROUTES.SEARCH_BY_PAYMENT_CARD.path}`,
    canActivate: [DocumentCreditCardGuard]
  },
  {
    path: `${ROUTES.CREDIT_CARD_DOCUMENTS.path}/query`,
    loadChildren: () =>
      import('./bsp-credit-card-documents/bsp-credit-card-documents.module').then(m => m.BspCreditCardDocumentsModule)
  },
  {
    path: `${ROUTES.CREDIT_CARD_DOCUMENTS.path}/list`,
    resolve: {
      creditCard: DocumentCreditCardResolver
    },
    runGuardsAndResolvers: 'always',
    loadChildren: () =>
      import('./credit-card-documents/credit-card-documents.module').then(m => m.CreditCardDocumentsModule)
  },
  {
    path: ROUTES.NET_REMIT_DOCUMENTS.path,
    loadChildren: () => import('./net-remit-documents/net-remit-documents.module').then(m => m.NetRemitDocumentsModule)
  },
  {
    path: ROUTES.DOCUMENT_VIEW_NOT_FOUND.path,
    component: DocumentViewComponent,
    resolve: { documentNumber: DocumentNotFoundResolver },
    data: {
      tab: {
        ...ROUTES.DOCUMENT_VIEW_NOT_FOUND,
        getTabLabel: getDocumentViewNotFoundTabLabel
      }
    }
  },
  {
    path: `${ROUTES.DOCUMENT_VIEW.path}/:documentId`,
    component: DocumentViewComponent,
    canActivate: [DocumentViewGuard],
    resolve: {
      document: DocumentResolver
    },
    data: {
      tab: {
        ...ROUTES.DOCUMENT_VIEW,
        getTabLabel: getDocumentEnquiryTabLabel
      }
    }
  },
  {
    path: ROUTES.STOCK_REQUEST_LIST.path,
    loadChildren: () =>
      import('./stock-request/stock-request-list/stock-request.module').then(m => m.StockRequestModule)
  },
  {
    path: ROUTES.STOCK_APPLICATION_REQUEST.path,
    component: StockApplicationRequestComponent,
    data: {
      tab: ROUTES.STOCK_APPLICATION_REQUEST
    }
  },
  {
    path: ROUTES.DOCUMENTS_SEARCH.path,
    data: {
      removeSelectedCreditCard: true
    },
    loadChildren: () =>
      import('./credit-card-documents/credit-card-documents.module').then(m => m.CreditCardDocumentsModule)
  },
  { path: '**', redirectTo: ROUTES.DOCUMENT_ENQUIRY.url }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRoutingModule {}
