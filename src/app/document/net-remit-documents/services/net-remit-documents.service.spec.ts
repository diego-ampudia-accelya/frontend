import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { NetRemitDocumentFilter } from '../models/net-remit-document-filter.model';
import {
  NetRemitDocumentType,
  NetReportingCalculationType,
  NetReportingMethodIndicator
} from '../models/net-remit-document.model';
import { NetRemitDocumentsService } from './net-remit-documents.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

describe('NetRemitDocumentsService', () => {
  let service: NetRemitDocumentsService;
  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy },
        {
          provide: AppConfigurationService,
          useValue: createSpyObject(AppConfigurationService, {
            baseApiPath: ''
          })
        }
      ]
    });
  });

  beforeEach(() => {
    httpClientSpy.get.and.returnValue(of());
    service = TestBed.inject(NetRemitDocumentsService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    beforeEach(() => httpClientSpy.get.calls.reset());

    it('should call net remit documents endpoint', fakeAsync(() => {
      service.find(defaultQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/globalsearch/hot-documents/net-remits'));
    }));

    it('should include pagination parameters in request', fakeAsync(() => {
      service.find(defaultQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('page=0&size=20'));
    }));

    it('should include BSP filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: {
          bsp: [
            { id: 1, name: 'Spain', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
            { id: 2, name: 'Malta', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('isoCountryCode=ES,MT'));
    }));

    it('should include period range as billing analysis ending date parameters in request', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: {
          periodRange: [
            { period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' },
            { period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('billingAnalysisEndingDateFrom=2020-02-01&billingAnalysisEndingDateTo=2020-02-29')
      );
    }));

    it('should include month period as period year month parameters in request', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: {
          monthPeriod: [
            { period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' },
            { period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('periodYearMonth=202002'));
    }));

    it('should include AGENT filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: {
          agent: [
            { id: '1', code: '001', name: 'Agent 1' },
            { id: '2', code: '002', name: 'Agent 2' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agentCode=001,002'));
    }));

    it('should include AIRLINE filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: {
          airline: [
            { id: 1, code: '001', name: 'Airline 1', designator: 'DC' },
            { id: 2, code: '002', name: 'Airline 2', designator: 'AC' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('airlineCode=001,002'));
    }));

    it('should include CURRENCY filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: {
          currency: [
            { id: 1, code: 'EUR', decimals: 2 },
            { id: 2, code: 'USD', decimals: 2 }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('currencyCode=EUR,USD'));
    }));

    it('should include TRANSACTION CODE filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { transactionCode: [NetRemitDocumentType.TKTT, NetRemitDocumentType.RFND] }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('transactionCode=TKTT,RFND'));
    }));

    it('should include TOUR CODE filter in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { tourCode: 'ZZZZI3' }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('tourCode=ZZZZI3'));
    }));

    it('should include FARE TICKET DESIGNATOR filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { fareTicketDesignator: 'TLITP' }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('fareTicketDesignator=TLITP'));
    }));

    it('should include COMMERCIAL AGREEMENT filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { commercialAgreementReference: 'ZZZZI3' }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('commercialAgreementReference=ZZZZI3'));
    }));

    it('should include NR METHOD INDICATOR filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { netReportingMethodIndicator: [NetReportingMethodIndicator.Blank, NetReportingMethodIndicator.Four] }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('netReportingMethodIndicator=blank,4'));
    }));

    it('should include NR CALCULATION TYPE filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { netReportingCalculationType: [NetReportingCalculationType.Blank, NetReportingCalculationType.D] }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('netReportingCalculationType=blank,D'));
    }));

    it('should include ORIGIN AIRPORT filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { originAirport: 'AGP' }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('originAirportCode=AGP'));
    }));

    it('should include DESTINATION AIRPORT filters in request when specified', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: { destinationAirport: 'MLA' }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('destinationAirportCode=MLA'));
    }));

    it('should map ORIGIN AIRPORT sort criteria', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        sortBy: [{ attribute: 'originAirport', sortType: SortOrder.Asc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('sort=originAirportCode,ASC'));
    }));

    it('should map DESTINATION AIRPORT sort criteria', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        sortBy: [{ attribute: 'destinationAirport', sortType: SortOrder.Desc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('sort=destinationAirportCode,DESC'));
    }));
  });

  describe('download', () => {
    beforeEach(() => httpClientSpy.get.calls.reset());

    it('should call net remit documents endpoint with correct options', fakeAsync(() => {
      service.download(defaultQuery, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/globalsearch/hot-documents/net-remits'), {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));

    it('should include actual query in request', fakeAsync(() => {
      const query: DataQuery<NetRemitDocumentFilter> = {
        ...defaultQuery,
        filterBy: {
          bsp: [
            { id: 1, name: 'Spain', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
            { id: 2, name: 'Malta', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
          ]
        }
      };

      service.download(query, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('page=0&size=20&isoCountryCode=ES,MT'),
        jasmine.anything()
      );
    }));

    it('should call endpoint with specified download format', fakeAsync(() => {
      service.download(defaultQuery, DownloadFormat.TXT).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('exportAs=TXT'), jasmine.anything());
    }));
  });
});
