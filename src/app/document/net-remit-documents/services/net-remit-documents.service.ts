import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import moment from 'moment-mini';
import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { NetRemitDocumentFilter } from '../models/net-remit-document-filter.model';
import {
  NetRemitDocument,
  NetReportingCalculationType,
  NetReportingMethodIndicator
} from '../models/net-remit-document.model';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import { toPeriodEntityData } from '~app/shared/utils/entity-period-mapper.utils';

@Injectable({
  providedIn: 'root'
})
export class NetRemitDocumentsService implements Queryable<NetRemitDocument> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/globalsearch/hot-documents/net-remits`;

  private sortMapper = [
    { from: 'originAirport', to: 'originAirportCode' },
    { from: 'destinationAirport', to: 'destinationAirportCode' },
    { from: 'period', to: 'billingAnalysisEndingDate' }
  ];

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private periodDictionary: PeriodService
  ) {}

  public find(query: DataQuery<NetRemitDocumentFilter>): Observable<PagedData<NetRemitDocument>> {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery({
      ...query,
      filterBy: this.formatFilter(query.filterBy),
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });

    return this.http.get<PagedData<NetRemitDocument>>(this.baseUrl + requestQuery.getQueryString()).pipe(
      switchMap(data =>
        combineLatest([
          of(data),
          data.records.length ? this.periodDictionary.getByBsp(data.records[0].bsp.id) : of(null)
        ])
      ),
      map(([data, periods]) => (periods ? toPeriodEntityData<NetRemitDocument>(data, periods) : data))
    );
  }

  public download(
    query: DataQuery<NetRemitDocumentFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery({
      ...query,
      filterBy: this.formatFilter(query.filterBy),
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat.toUpperCase() };
    const url = `${this.baseUrl}` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatFilter(filter: Partial<NetRemitDocumentFilter>) {
    const {
      bsp,
      periodRange,
      monthPeriod,
      agent,
      airline,
      currency,
      transactionCode,
      tourCode,
      fareTicketDesignator,
      commercialAgreementReference,
      netReportingMethodIndicator,
      netReportingCalculationType,
      originAirport,
      destinationAirport
    } = filter;

    return {
      isoCountryCode: bsp && (!Array.isArray(bsp) ? [bsp] : bsp).map(({ isoCountryCode }) => isoCountryCode),
      billingAnalysisEndingDateFrom: periodRange && this.formatPeriod(periodRange[0].dateFrom),
      billingAnalysisEndingDateTo: periodRange && this.formatPeriod(periodRange[1].dateTo),
      periodYearMonth: monthPeriod && this.formatMonthPeriod(monthPeriod[0].period),
      agentCode: agent && agent.map(item => item.code),
      airlineCode: airline && airline.map(item => item.code),
      currencyCode: currency && currency.map(item => item.code),
      transactionCode,
      tourCode,
      fareTicketDesignator,
      commercialAgreementReference,
      netReportingMethodIndicator:
        netReportingMethodIndicator &&
        netReportingMethodIndicator.map(item =>
          item === NetReportingMethodIndicator.Blank ? item.toLowerCase() : item
        ),
      netReportingCalculationType:
        netReportingCalculationType &&
        netReportingCalculationType.map(item =>
          item === NetReportingCalculationType.Blank ? item.toLowerCase() : item
        ),
      originAirportCode: originAirport,
      destinationAirportCode: destinationAirport
    };
  }

  private formatPeriod(date: string): string {
    return date ? moment(date, 'MM/DD/YYYY').format('YYYY-MM-DD') : null;
  }

  /**
   * Format month period, removing last specific period digit
   *
   * @param period with format YYYYMMP
   * @returns `periodYearMonth` with format YYYYMM
   */
  private formatMonthPeriod(period: string): string {
    return period.substring(0, period.length - 1);
  }
}
