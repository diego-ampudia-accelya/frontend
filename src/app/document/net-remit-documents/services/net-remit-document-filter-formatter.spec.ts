import { DatePipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { NetRemitDocumentFilter } from '../models/net-remit-document-filter.model';
import {
  NetRemitDocumentType,
  NetReportingCalculationType,
  NetReportingMethodIndicator
} from '../models/net-remit-document.model';

import { NetRemitDocumentFilterFormatter } from './net-remit-document-filter-formatter';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

describe('NetRemitDocumentFilterFormatter', () => {
  let formatter: NetRemitDocumentFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NetRemitDocumentFilterFormatter,
        PeriodPipe,
        DatePipe,
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    });

    translationServiceSpy.translate.and.callFake(identity);
    formatter = TestBed.inject(NetRemitDocumentFilterFormatter);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format BSPs if they exist', () => {
    const filter: NetRemitDocumentFilter = {
      bsp: [
        { id: 1, name: 'Spain', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
        { id: 2, name: 'Malta', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['bsp'],
        label: 'document.netRemit.filters.bsp.label - Spain (ES), Malta (MT)'
      }
    ]);
  });

  it('should format period range if it exists', () => {
    const filter: NetRemitDocumentFilter = {
      periodRange: [
        { period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' },
        { period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['periodRange'],
        label: 'P1 Feb 2020 - P2 Feb 2020'
      }
    ]);
  });

  it('should format month period if it exists', () => {
    const filter: NetRemitDocumentFilter = {
      monthPeriod: [
        { period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' },
        { period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['monthPeriod'],
        label: 'Feb 2020'
      }
    ]);
  });

  it('should format airlines if they exist', () => {
    const filter: NetRemitDocumentFilter = {
      airline: [
        { id: 1, code: '001', name: 'Airline 1', designator: 'DC' },
        { id: 2, code: '002', name: 'Airline 2', designator: 'AC' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airline'],
        label: '001 / Airline 1, 002 / Airline 2'
      }
    ]);
  });

  it('should format agents if they exist', () => {
    const filter: NetRemitDocumentFilter = {
      agent: [
        { id: '1', code: '001', name: 'Agent 1' },
        { id: '2', code: '002', name: 'Agent 2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agent'],
        label: '001 / Agent 1, 002 / Agent 2'
      }
    ]);
  });

  it('should format currencies if they exist', () => {
    const filter: NetRemitDocumentFilter = {
      currency: [
        { id: 1, code: 'EUR', decimals: 2 },
        { id: 2, code: 'USD', decimals: 2 }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['currency'],
        label: 'document.netRemit.filters.currency.label - EUR, USD'
      }
    ]);
  });

  it('should format transaction codes if they exist', () => {
    const filter: NetRemitDocumentFilter = {
      transactionCode: [NetRemitDocumentType.TKTT, NetRemitDocumentType.RFND]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['transactionCode'],
        label: 'document.netRemit.filters.transactionCode.label - TKTT, RFND'
      }
    ]);
  });

  it('should format tour code if it exists', () => {
    const filter: NetRemitDocumentFilter = {
      tourCode: 'ZZZZI3'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['tourCode'],
        label: 'document.netRemit.filters.tourCode.label - ZZZZI3'
      }
    ]);
  });

  it('should format fare ticket designator if it exists', () => {
    const filter: NetRemitDocumentFilter = {
      fareTicketDesignator: 'TLITP'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['fareTicketDesignator'],
        label: 'document.netRemit.filters.fareTicketDesignator.label - TLITP'
      }
    ]);
  });

  it('should format commercial agreement if it exists', () => {
    const filter: NetRemitDocumentFilter = {
      commercialAgreementReference: 'ZZZZI3'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['commercialAgreementReference'],
        label: 'document.netRemit.filters.commercialAgreementReference.label - ZZZZI3'
      }
    ]);
  });

  it('should format NR method indicators if they exist', () => {
    const filter: NetRemitDocumentFilter = {
      netReportingMethodIndicator: [NetReportingMethodIndicator.Blank, NetReportingMethodIndicator.Four]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['netReportingMethodIndicator'],
        label: 'document.netRemit.filters.netReportingMethodIndicator.label - Blank, 4'
      }
    ]);
  });

  it('should format NR calculation types if they exist', () => {
    const filter: NetRemitDocumentFilter = {
      netReportingCalculationType: [NetReportingCalculationType.Blank, NetReportingCalculationType.D]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['netReportingCalculationType'],
        label: 'document.netRemit.filters.netReportingCalculationType.label - Blank, D'
      }
    ]);
  });

  it('should format origin airport if it exists', () => {
    const filter: NetRemitDocumentFilter = {
      originAirport: 'AGP'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['originAirport'],
        label: 'document.netRemit.filters.originAirport.label - AGP'
      }
    ]);
  });

  it('should format destination airport if it exists', () => {
    const filter: NetRemitDocumentFilter = {
      destinationAirport: 'MLA'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['destinationAirport'],
        label: 'document.netRemit.filters.destinationAirport.label - MLA'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: NetRemitDocumentFilter = {
      periodRange: null,
      agent: [],
      commercialAgreementReference: ''
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
