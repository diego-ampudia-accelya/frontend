import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { NetRemitDocumentFilter } from '../models/net-remit-document-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { agentFilterTagMapper, airlineFilterTagMapper, bspFilterTagMapper } from '~app/shared/helpers';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

type DisplayFilterConfig<T> = {
  [P in keyof T]?: {
    label?: string;
    format?(value: T[P]): string | string[];
  };
};

@Injectable()
export class NetRemitDocumentFilterFormatter implements FilterFormatter {
  private configuration: DisplayFilterConfig<NetRemitDocumentFilter> = {
    bsp: {
      label: 'document.netRemit.filters.bsp.label',
      format: bspFilterTagMapper
    },
    periodRange: { format: range => this.periodPipe.transform(range) },
    monthPeriod: { format: periods => this.periodPipe.transform(periods, true) },
    agent: { format: agentFilterTagMapper },
    airline: { format: airlineFilterTagMapper },
    currency: {
      label: 'document.netRemit.filters.currency.label',
      format: currencies => currencies.map(curr => curr.code)
    },
    transactionCode: { label: 'document.netRemit.filters.transactionCode.label' },
    tourCode: { label: 'document.netRemit.filters.tourCode.label' },
    fareTicketDesignator: { label: 'document.netRemit.filters.fareTicketDesignator.label' },
    commercialAgreementReference: { label: 'document.netRemit.filters.commercialAgreementReference.label' },
    netReportingMethodIndicator: { label: 'document.netRemit.filters.netReportingMethodIndicator.label' },
    netReportingCalculationType: { label: 'document.netRemit.filters.netReportingCalculationType.label' },
    originAirport: { label: 'document.netRemit.filters.originAirport.label' },
    destinationAirport: { label: 'document.netRemit.filters.destinationAirport.label' }
  };

  constructor(private periodPipe: PeriodPipe, private translation: L10nTranslationService) {}

  public format(filters: NetRemitDocumentFilter): AppliedFilter[] {
    return Object.entries(this.configuration)
      .filter(([filterKey]) => filters && !isEmpty(filters[filterKey]))
      .map(([filterKey, config]) => {
        const formatValue = config.format || identity;
        let formattedValue = formatValue(filters[filterKey]);
        if (Array.isArray(formattedValue)) {
          formattedValue = formattedValue.join(', ');
        }

        let label = formattedValue;
        if (config.label) {
          label = `${this.translation.translate(config.label)} - ${formattedValue}`;
        }

        return { label, keys: [filterKey] };
      });
  }
}
