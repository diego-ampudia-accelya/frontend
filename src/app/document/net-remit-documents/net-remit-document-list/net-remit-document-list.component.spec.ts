import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { NetRemitDocumentFilter } from '../models/net-remit-document-filter.model';
import { NetRemitDocument } from '../models/net-remit-document.model';
import { NetRemitDocumentFilterFormatter } from '../services/net-remit-document-filter-formatter';
import { NetRemitDocumentsService } from '../services/net-remit-documents.service';
import { NetRemitDocumentListComponent } from './net-remit-document-list.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService, PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  AppConfigurationService,
  CurrencyDictionaryService
} from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('NetRemitDocumentListComponent', () => {
  let component: NetRemitDocumentListComponent;
  let fixture: ComponentFixture<NetRemitDocumentListComponent>;

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const periodServiceSpy = createSpyObject(PeriodService);
  const airlineDictionarySpy = createSpyObject(AirlineDictionaryService);
  const agentDictionarySpy = createSpyObject(AgentDictionaryService);
  const currencyDictionarySpy = createSpyObject(CurrencyDictionaryService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [Permissions.lean, Permissions.readNetRemitDocuments],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readNetRemitDocuments] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    }
  ];

  const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
    {
      value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
      label: '1111111 / AGENT 1111111'
    },
    {
      value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
      label: '2222222 / AGENT 2222222'
    }
  ];

  const mockCurrencyDropdownOptions: DropdownOption<Currency>[] = [
    {
      value: { id: 1, code: 'EUR', decimals: 2 },
      label: 'EUR'
    },
    {
      value: { id: 1, code: 'USD', decimals: 2 },
      label: 'USD'
    }
  ];

  const mockPeriods: Period[] = [
    { id: 1, period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020', isoCountryCode: 'ES' },
    { id: 2, period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020', isoCountryCode: 'ES' }
  ];

  airlineDictionarySpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));
  agentDictionarySpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));
  currencyDictionarySpy.getFilteredDropdownOptions.and.returnValue(of(mockCurrencyDropdownOptions));
  periodServiceSpy.getByBsp.and.returnValue(of(mockPeriods));

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NetRemitDocumentListComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        NetRemitDocumentFilterFormatter,
        NetRemitDocumentsService,
        FormBuilder,
        L10nTranslationService,
        AppConfigurationService,
        PermissionsService,
        PeriodPipe,
        DatePipe,
        provideMockStore({
          selectors: [{ selector: getUser, value: expectedUserDetails }]
        }),
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionarySpy },
        { provide: AgentDictionaryService, useValue: agentDictionarySpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionarySpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    })
      .overrideComponent(NetRemitDocumentListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetRemitDocumentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should set correct columns on init as an AIRLINE', () => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'airline.iataCode',
        hidden: true
      }),
      jasmine.objectContaining({
        prop: 'airline.localName',
        sortable: false,
        hidden: true
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        hidden: false
      }),
      jasmine.objectContaining({
        prop: 'agent.name',
        sortable: false,
        hidden: false
      })
    ]);

    component.columns = [];
    component.ngOnInit();

    expect(component.columns).toEqual(expectedColumns);
  });

  it('should set correct columns on init as an AGENT', () => {
    component['loggedUser'] = {
      ...expectedUserDetails,
      userType: UserType.AGENT
    } as User;

    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'airline.iataCode',
        hidden: false
      }),
      jasmine.objectContaining({
        prop: 'airline.localName',
        sortable: false,
        hidden: false
      }),
      jasmine.objectContaining({
        prop: 'agent.iataCode',
        hidden: true
      }),
      jasmine.objectContaining({
        prop: 'agent.name',
        sortable: false,
        hidden: true
      })
    ]);

    component.columns = [];
    component.ngOnInit();

    expect(component.columns).toEqual(expectedColumns);
  });

  it('should initialize BSP listener if user has LEAN permission', fakeAsync(() => {
    const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

    component.ngOnInit();
    component.searchForm.get('bsp').patchValue(mockBsp);
    tick();

    expect(component.selectedBsps).toEqual([mockBsp]);
  }));

  describe('filter dropdowns initialization', () => {
    beforeEach(() => {
      periodServiceSpy.getByBsp.calls.reset();
      currencyDictionarySpy.getFilteredDropdownOptions.calls.reset();
      agentDictionarySpy.getDropdownOptions.calls.reset();
      airlineDictionarySpy.getDropdownOptions.calls.reset();
    });

    it('should initialize PERIOD options dropdown', fakeAsync(() => {
      const expectedPeriodOptions: any = [
        jasmine.objectContaining({ period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' }),
        jasmine.objectContaining({ period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' })
      ];

      component.ngOnInit();
      tick();

      expect(periodServiceSpy.getByBsp).toHaveBeenCalledWith(1);
      expect(component.periodOptions).toEqual(expectedPeriodOptions);
    }));

    it('should initialize CURRENCY dropdown with selected BSPs', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(currencyDictionarySpy.getFilteredDropdownOptions).toHaveBeenCalledWith({ bspId: [1] });
      expect(component.currencyDropdownOptions).toEqual(mockCurrencyDropdownOptions);
    }));

    it('should initialize AGENT dropdown when user is Airline', fakeAsync(() => {
      let agentDropdownOptions: DropdownOption<AgentSummary>[];

      component.ngOnInit();
      tick();

      component.agentDropdownOptions$.subscribe(options => (agentDropdownOptions = options));

      expect(agentDictionarySpy.getDropdownOptions).toHaveBeenCalledWith({ bspId: [1] });
      expect(agentDropdownOptions).toEqual(mockAgentDropdownOptions);
    }));

    it('should NOT initialize AGENT dropdown when user is Agent', fakeAsync(() => {
      component['loggedUser'] = {
        ...expectedUserDetails,
        userType: UserType.AGENT
      } as User;

      component.ngOnInit();
      tick();

      expect(agentDictionarySpy.getDropdownOptions).not.toHaveBeenCalled();
      expect(component.agentDropdownOptions$).toBeUndefined();
    }));

    it('should initialize AIRLINE dropdown when user is Agent', fakeAsync(() => {
      component['loggedUser'] = {
        ...expectedUserDetails,
        userType: UserType.AGENT
      } as User;

      let airlineDropdownOptions: DropdownOption<AirlineSummary>[];

      component.ngOnInit();
      tick();

      component.airlineDropdownOptions$.subscribe(options => (airlineDropdownOptions = options));

      expect(airlineDictionarySpy.getDropdownOptions).toHaveBeenCalledWith(1);
      expect(airlineDropdownOptions).toEqual(mockAirlineDropdownOptions);
    }));

    it('should NOT initialize AIRLINE dropdown when user is Airline', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(airlineDictionarySpy.getDropdownOptions).not.toHaveBeenCalled();
      expect(component.airlineDropdownOptions$).toBeUndefined();
    }));
  });

  describe('set predefined filters', () => {
    it('should set predefined filters if user has not LEAN permission', fakeAsync(() => {
      const mockCurrentPeriod: PeriodOption = { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' };

      component['hasLeanPermission'] = false;
      spyOn(PeriodUtils, 'findCurrentPeriod').and.returnValue(mockCurrentPeriod);

      component.ngOnInit();
      tick();

      expect(component.predefinedFilters).toEqual({
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
        periodRange: [mockCurrentPeriod, mockCurrentPeriod]
      });
    }));

    it('should NOT set predefined filters if user has LEAN permission', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(component.predefinedFilters).toBeUndefined();
    }));
  });

  describe('load data', () => {
    const mockCurrentPeriod: PeriodOption = { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' };
    const storedQuery: DataQuery<NetRemitDocumentFilter> = {
      ...defaultQuery,
      filterBy: { tourCode: 'ZZZZI3' }
    };

    beforeEach(() => {
      spyOn(PeriodUtils, 'findCurrentPeriod').and.returnValue(mockCurrentPeriod);
      queryStorageSpy.get.and.returnValue(storedQuery);
    });

    it('should add predefined filters to search query if user has not LEAN permission', fakeAsync(() => {
      component['hasLeanPermission'] = false;
      component.ngOnInit();
      tick();

      const expectedQuery = {
        filterBy: jasmine.objectContaining({
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
          periodRange: [mockCurrentPeriod, mockCurrentPeriod],
          ...storedQuery.filterBy
        }),
        paginateBy: storedQuery.paginateBy,
        sortBy: storedQuery.sortBy
      };

      expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
      expect(queryStorageSpy.save).toHaveBeenCalledWith(expectedQuery);
    }));

    it('should add `periodRange` to query filter if user has LEAN permission and there is one filtered BSP', fakeAsync(() => {
      component.ngOnInit();
      tick();

      const expectedQuery = {
        filterBy: jasmine.objectContaining({
          ...storedQuery.filterBy,
          bsp: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }],
          periodRange: [mockCurrentPeriod, mockCurrentPeriod]
        }),
        paginateBy: storedQuery.paginateBy,
        sortBy: storedQuery.sortBy
      };

      expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
      expect(queryStorageSpy.save).toHaveBeenCalledWith(expectedQuery);
    }));
  });

  it('should open download dialog correctly on download', () => {
    const query: DataQuery<NetRemitDocumentFilter> = {
      ...defaultQuery,
      filterBy: { fareTicketDesignator: 'TLITP' }
    };

    queryStorageSpy.get.and.returnValue(query);
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(
      jasmine.anything(),
      jasmine.objectContaining({ data: jasmine.objectContaining({ downloadQuery: query }) })
    );
  });

  it('should navigate to the specified document opening it', () => {
    const mockDocument: Partial<NetRemitDocument> = { documentNumber: '12345' };

    component.onOpenDocument(mockDocument as NetRemitDocument);

    expect(routerSpy.navigate).toHaveBeenCalledWith([ROUTES.DOCUMENT_ENQUIRY.url, '12345']);
  });
});
