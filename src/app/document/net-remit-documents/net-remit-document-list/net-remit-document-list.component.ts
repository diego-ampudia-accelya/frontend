import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { first, skipWhile, takeUntil } from 'rxjs/operators';

import { NetRemitDocumentFilter } from '../models/net-remit-document-filter.model';
import {
  NetRemitDocument,
  NetRemitDocumentType,
  NetReportingCalculationType,
  NetReportingMethodIndicator
} from '../models/net-remit-document.model';
import { NetRemitDocumentFilterFormatter } from '../services/net-remit-document-filter-formatter';
import { NetRemitDocumentsService } from '../services/net-remit-documents.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  DialogService,
  DownloadFileComponent,
  FooterButton,
  PeriodOption,
  PeriodUtils
} from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { ROUTES } from '~app/shared/constants/routes';
import { SelectMode } from '~app/shared/enums';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService, CurrencyDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-net-remit-document-list',
  templateUrl: './net-remit-document-list.component.html',
  styleUrls: ['./net-remit-document-list.component.scss'],
  providers: [DefaultQueryStorage, QueryableDataSource, { provide: QUERYABLE, useExisting: NetRemitDocumentsService }]
})
export class NetRemitDocumentListComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public columns: Array<GridColumn>;

  public selectedBsps: Bsp[] = [];
  public buttonDesign = ButtonDesign;
  public selectMode = SelectMode;
  public userTypes = UserType;

  public predefinedFilters: NetRemitDocumentFilter;

  public airlineDropdownOptions$: Observable<DropdownOption<AirlineSummary>[]>;
  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;

  public userBspOptions: DropdownOption<Bsp>[] = [];
  public periodOptions: PeriodOption[];
  public currencyDropdownOptions: DropdownOption<Currency>[];
  public transactionCodeDropdownOptions: DropdownOption<NetRemitDocumentType>[] = ArrayHelper.toDropdownOptions(
    Object.values(NetRemitDocumentType)
  );
  public netReportingMethodIndicatorDropdownsOptions: DropdownOption<NetReportingMethodIndicator>[] =
    ArrayHelper.toDropdownOptions(Object.values(NetReportingMethodIndicator));
  public netReportingCalculationTypeDropdownOptions: DropdownOption<NetReportingCalculationType>[] =
    ArrayHelper.toDropdownOptions(Object.values(NetReportingCalculationType));

  public isMonthPeriodPicker: boolean;
  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  public periodMonthPickerYearsBack: number;

  private loggedUser: User;
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private get currentPeriodValue(): PeriodOption[] {
    const currentPeriod = PeriodUtils.findCurrentPeriod(this.periodOptions || []);

    return currentPeriod ? [currentPeriod, currentPeriod] : null;
  }

  private hasLeanPermission: boolean;

  private formFactory: FormUtil;

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<NetRemitDocument>,
    public displayFilterFormatter: NetRemitDocumentFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private periodDictionary: PeriodService,
    private airlineDictionary: AirlineDictionaryService,
    private agentDictionary: AgentDictionaryService,
    private currencyDictionary: CurrencyDictionaryService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private netRemitDocumentsService: NetRemitDocumentsService,
    private permissionsService: PermissionsService,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);

    this.initializeLoggedUser();
    this.initializePermissions();
  }

  public async ngOnInit(): Promise<void> {
    this.searchForm = this.buildSearchForm();
    this.columns = this.buildColumns();

    this.initializeBspFeatures();
    this.setPeriodPickerYearsBack();

    // Load initial data. Use stored Data Query if available
    const storedQuery = this.queryStorage.get();

    await this.populateFilterDropdowns();

    this.setPredefinedFilters();
    this.loadData(storedQuery);
  }

  public loadData(query?: DataQuery): void {
    query = query || cloneDeep(defaultQuery);

    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...(!this.hasLeanPermission && this.predefinedFilters),
        ...this.handleQueryFilter(query.filterBy)
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public loggedUserIsOfType(type: UserType): boolean {
    return type.toUpperCase() === this.loggedUser.userType.toUpperCase();
  }

  public onDownload(): void {
    const requestQuery = this.queryStorage.get();

    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: requestQuery
      },
      apiService: this.netRemitDocumentsService
    });
  }

  public onOpenDocument(document: NetRemitDocument): void {
    this.router.navigate([ROUTES.DOCUMENT_ENQUIRY.url, document.documentNumber]);
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => (this.loggedUser = loggedUser));
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeBspFeatures(): void {
    if (this.hasLeanPermission) {
      this.initializeBspListener();
    }

    // Initialize BSP filter dropdown
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    this.initializeSelectedBsps();

    // Initialize BSP related flags
    this.isBspFilterLocked = !this.hasLeanPermission;
    this.isBspFilterMultiple = this.hasLeanPermission;
    this.isMonthPeriodPicker = this.selectedBsps.length !== 1;
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readNetRemitDocuments;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private initializeBspListener(): void {
    FormUtil.get<NetRemitDocumentFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(async value => {
        const bspToArray = Array.isArray(value) ? value : [value];
        this.selectedBsps = value ? bspToArray : [];
        this.isMonthPeriodPicker = this.selectedBsps.length !== 1;

        await this.populateFilterDropdowns();

        this.updateFilterValues();
      });
  }

  private initializeSelectedBsps(): void {
    const firstListBsp = this.userBspOptions[0].value;
    const defaultBsp = this.userBspOptions.find(
      ({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc
    )?.value;

    if (this.userBspOptions.length === 1) {
      this.selectedBsps = [firstListBsp];
    } else {
      this.selectedBsps = this.hasLeanPermission ? [] : [defaultBsp || firstListBsp];
    }
  }

  /** Since this method is only available for Airline users with LEAN permission, we do not need to update Airline filter */
  private updateFilterValues(): void {
    this.updatePeriodFilterValue();
    this.updateCurrencyFilterValue();
    this.updateAgentFilterValue();
  }

  private updatePeriodFilterValue(): void {
    const periodRangeControl = FormUtil.get<NetRemitDocumentFilter>(this.searchForm, 'periodRange');
    const monthPeriodControl = FormUtil.get<NetRemitDocumentFilter>(this.searchForm, 'monthPeriod');

    if (this.selectedBsps.length === 1) {
      periodRangeControl.patchValue(periodRangeControl.value || this.currentPeriodValue);
      monthPeriodControl.reset();
    } else {
      monthPeriodControl.patchValue(monthPeriodControl.value || this.currentPeriodValue);
      periodRangeControl.reset();
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodMonthPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private updateCurrencyFilterValue(): void {
    // If there are not selected BSPs, dropdown has all currencies from all BSPs so there is no reason to patch currency control value
    if (!this.selectedBsps.length) {
      return;
    }

    const currencyControl = FormUtil.get<NetRemitDocumentFilter>(this.searchForm, 'currency');
    const currenciesSelected: Currency[] = currencyControl.value;

    if (currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyDropdownOptions.some(({ value }) => value.id === currency.id)
      );
      currencyControl.patchValue(currenciesToPatch);
    }
  }

  private updateAgentFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentControl = FormUtil.get<NetRemitDocumentFilter>(this.searchForm, 'agent');
    const agentsSelected: AgentSummary[] = agentControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.selectedBsps.some(({ id }) => agent.bsp?.id === id));
      agentControl.patchValue(agentsToPatch);
    }
  }

  private async populateFilterDropdowns(): Promise<void> {
    const bspSelectedIds = this.selectedBsps.map(({ id }) => id);
    const firstBspId = bspSelectedIds.length ? bspSelectedIds[0] : this.userBspOptions[0]?.value.id;
    const params = bspSelectedIds.length ? { bspId: bspSelectedIds } : null;

    this.periodOptions = await this.periodDictionary.getByBsp(firstBspId).toPromise();
    this.currencyDropdownOptions = await this.currencyDictionary.getFilteredDropdownOptions(params).toPromise();

    // Filter for airlines only
    if (this.loggedUserIsOfType(UserType.AIRLINE)) {
      this.agentDropdownOptions$ = this.agentDictionary.getDropdownOptions(params);
    }

    // Filter for agents only
    if (this.loggedUserIsOfType(UserType.AGENT)) {
      this.airlineDropdownOptions$ = this.airlineDictionary.getDropdownOptions(firstBspId);
    }
  }

  /** Set predefined filters adding `periodRange`, only available if user has not LEAN permission */
  private setPredefinedFilters(): void {
    if (this.hasLeanPermission) {
      return;
    }

    this.predefinedFilters = {
      bsp: this.selectedBsps[0],
      periodRange: this.currentPeriodValue
    };
  }

  /** Handles query filter adding `monthPeriod` or `periodRange` depending on filtered BSPs and user LEAN permission */
  private handleQueryFilter(queryFilter: Partial<NetRemitDocumentFilter>): Partial<NetRemitDocumentFilter> {
    if (!this.hasLeanPermission) {
      return queryFilter;
    }

    // BSP filter is always an array if user has LEAN permission
    const filteredBsps = (queryFilter.bsp || this.selectedBsps) as Bsp[];

    return {
      ...queryFilter,
      bsp: filteredBsps,
      monthPeriod: filteredBsps.length !== 1 ? queryFilter.monthPeriod || this.currentPeriodValue : null,
      periodRange: filteredBsps.length === 1 ? queryFilter.periodRange || this.currentPeriodValue : null
    };
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<NetRemitDocumentFilter>({
      periodRange: [],
      monthPeriod: [],
      bsp: [],
      airline: [],
      agent: [],
      currency: [],
      tourCode: [],
      fareTicketDesignator: [],
      commercialAgreementReference: [],
      netReportingMethodIndicator: [],
      netReportingCalculationType: [],
      transactionCode: [],
      originAirport: [],
      destinationAirport: []
    });
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: 'document.netRemit.columns.bsp',
        prop: 'bsp.isoCountryCode',
        flexGrow: 0.7,
        minWidth: 75
      },
      {
        name: 'document.netRemit.columns.documentNumber',
        prop: 'documentNumber',
        flexGrow: 1.5,
        cellTemplate: 'commonLinkFromObjCellTmpl'
      },
      {
        name: 'document.netRemit.columns.transactionCode',
        prop: 'transactionCode',
        flexGrow: 1.5
      },
      {
        name: 'document.netRemit.columns.period',
        prop: 'period',
        flexGrow: 1
      },
      {
        name: 'document.netRemit.columns.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 1,
        hidden: this.loggedUserIsOfType(UserType.AIRLINE)
      },
      {
        name: 'document.netRemit.columns.airlineName',
        prop: 'airline.localName',
        flexGrow: 2.5,
        sortable: false,
        hidden: this.loggedUserIsOfType(UserType.AIRLINE)
      },
      {
        name: 'document.netRemit.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 1,
        hidden: this.loggedUserIsOfType(UserType.AGENT)
      },
      {
        name: 'document.netRemit.columns.agentName',
        prop: 'agent.name',
        flexGrow: 2.5,
        sortable: false,
        hidden: this.loggedUserIsOfType(UserType.AGENT)
      },
      {
        name: 'document.netRemit.columns.tourCode',
        prop: 'tourCode',
        flexGrow: 1
      },
      {
        name: 'document.netRemit.columns.commercialAgreementReference',
        prop: 'commercialAgreementReference',
        flexGrow: 1.5
      },
      {
        name: 'document.netRemit.columns.fareTicketDesignator',
        prop: 'fareTicketDesignator',
        flexGrow: 2
      },
      {
        name: 'document.netRemit.columns.originAirport',
        prop: 'originAirportCode',
        flexGrow: 1
      },
      {
        name: 'document.netRemit.columns.destinationAirport',
        prop: 'destinationAirportCode',
        flexGrow: 1.5
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
