import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NetRemitDocumentListComponent } from './net-remit-document-list/net-remit-document-list.component';
import { NetRemitDocumentsRoutingModule } from './net-remit-documents-routing.module';
import { NetRemitDocumentFilterFormatter } from './services/net-remit-document-filter-formatter';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, NetRemitDocumentsRoutingModule],
  declarations: [NetRemitDocumentListComponent],
  providers: [NetRemitDocumentFilterFormatter]
})
export class NetRemitDocumentsModule {}
