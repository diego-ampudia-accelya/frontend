import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface NetRemitDocument {
  id: string;
  bsp: BspDto;
  airline: AirlineSummaryAux;
  agent: AgentSummaryAux;
  currency: Currency;
  documentNumber: string;
  billingAnalysisEndingDate: Date;
  period?: string;
  ticketAmount: number;
  commissionableAmount: number;
  commissionAmount: number;
  commissionAmountOnCancellationPenalty: number;
  supplementaryAmount: number;
  remittanceAmount: number;
  transactionCode: NetRemitDocumentType;
  tourCode: string;
  commercialAgreementReference: string;
  netReportingIndicator: string;
  netReportingMethodIndicator?: NetReportingMethodIndicator;
  netReportingCalculationType?: NetReportingCalculationType;
  fareTicketDesignator: string;
  originAirportCode: string;
  destinationAirportCode: string;
}

export interface AirlineSummaryAux {
  id: number;
  iataCode: string;
  localName: string;
}

export interface AgentSummaryAux {
  id: number;
  iataCode: string;
  name: string;
  bsp: BspDto;
}

export enum NetRemitDocumentType {
  EMDS = 'EMDS',
  EMDA = 'EMDA',
  TKTT = 'TKTT',
  ACMA = 'ACMA',
  ACMD = 'ACMD',
  ACNT = 'ACNT',
  ADMA = 'ADMA',
  ADMD = 'ADMD',
  ADNT = 'ADNT',
  RFNC = 'RFNC',
  RFND = 'RFND',
  SPCR = 'SPCR',
  SPDR = 'SPDR',
  TASF = 'TASF'
}

export enum NetReportingMethodIndicator {
  Blank = 'Blank',
  One = '1',
  Two = '2',
  Three = '3',
  Four = '4',
  Five = '5'
}

export enum NetReportingCalculationType {
  Blank = 'Blank',
  A = 'A',
  B = 'B',
  C = 'C',
  D = 'D',
  E = 'E',
  F = 'F',
  G = 'G'
}
