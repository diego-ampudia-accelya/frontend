import {
  NetRemitDocumentType,
  NetReportingCalculationType,
  NetReportingMethodIndicator
} from './net-remit-document.model';
import { PeriodOption } from '~app/shared/components';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface NetRemitDocumentFilter {
  periodRange?: PeriodOption[];
  monthPeriod?: PeriodOption[];
  bsp?: Bsp | Bsp[];
  agent?: AgentSummary[];
  airline?: AirlineSummary[];
  currency?: Currency[];
  transactionCode?: NetRemitDocumentType[];
  tourCode?: string;
  fareTicketDesignator?: string;
  commercialAgreementReference?: string;
  netReportingMethodIndicator?: NetReportingMethodIndicator[];
  netReportingCalculationType?: NetReportingCalculationType[];
  originAirport?: string;
  destinationAirport?: string;
}
