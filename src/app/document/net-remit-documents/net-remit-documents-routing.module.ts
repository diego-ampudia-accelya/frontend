import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NetRemitDocumentListComponent } from './net-remit-document-list/net-remit-document-list.component';
import { ROUTES } from '~app/shared/constants/routes';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: NetRemitDocumentListComponent,
        data: {
          tab: ROUTES.NET_REMIT_DOCUMENTS
        }
      }
    ])
  ]
})
export class NetRemitDocumentsRoutingModule {}
