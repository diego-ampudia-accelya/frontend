import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA, TemplateRef } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { RejectedDocumentFilter } from '../models/rejected-document-filter.model';
import { RejectedDocumentFilterFormatter } from '../services/rejected-document-filter-formatter';
import { RejectedDocumentsService } from '../services/rejected-documents.service';
import { RejectedListComponent } from './rejected-list.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { AppState } from '~app/reducers';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { Permissions, ROUTES } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('RejectedListComponent', () => {
  let component: RejectedListComponent;
  let fixture: ComponentFixture<RejectedListComponent>;
  let mockStore: MockStore<AppState>;

  const dataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const airlineDictionarySpy = createSpyObject(AirlineDictionaryService);
  const agentDictionarySpy = createSpyObject(AgentDictionaryService);
  const gdsDictionarySpy = createSpyObject(GdsDictionaryService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const periodServiceSpy = createSpyObject(PeriodService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const dataServiceSpy = createSpyObject(RejectedDocumentsService);
  const routerSpy = createSpyObject(Router);

  const userMock: any = {
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    defaultIsoc: 'MT',
    permissions: [Permissions.readRejectedDocuments, Permissions.readDocumentDetails, Permissions.lean],
    bspPermissions: [{ bspId: 1, permissions: [Permissions.readRejectedDocuments] }]
  };

  const initialState = {
    auth: { user: userMock }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RejectedListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        FormBuilder,
        PermissionsService,
        FormBuilder,
        RejectedDocumentFilterFormatter,
        { provide: PeriodPipe, useClass: PeriodPipeMock },
        { provide: AirlineDictionaryService, useValue: airlineDictionarySpy },
        { provide: AgentDictionaryService, useValue: agentDictionarySpy },
        { provide: GdsDictionaryService, useValue: gdsDictionarySpy },
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: RejectedDocumentsService, useValue: dataServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy }
      ]
    })
      .overrideComponent(RejectedListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: dataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    const mockBsp = [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }];

    // Initialize spies
    periodServiceSpy.getByBsp.and.returnValue(of([]));
    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(of(mockBsp));
    airlineDictionarySpy.getDropdownOptions.and.returnValue(of([]));
    gdsDictionarySpy.getDropdownOptions.and.returnValue(of([]));

    fixture = TestBed.createComponent(RejectedListComponent);
    component = fixture.componentInstance;

    mockStore = TestBed.inject(MockStore);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('initialize BSP filter dropdown', () => {
    it('should initialize logged user on init', fakeAsync(() => {
      let user: User;

      fixture.detectChanges();
      component['loggedUser$'].subscribe(u => (user = u));

      expect(user).toEqual(userMock);
    }));

    it('should initialize BSP options with permissions on init', fakeAsync(() => {
      const mockBsp = [
        {
          id: 1,
          name: 'SPAIN',
          isoCountryCode: 'ES'
        },
        {
          id: 2,
          name: 'MALTA',
          isoCountryCode: 'MT'
        }
      ];
      const bspOption = toValueLabelObjectBsp(mockBsp[0]) as DropdownOption<BspDto>;

      fixture.detectChanges();
      expect(component.userBspOptions).toEqual([bspOption]);
    }));

    it('should update BSP filter with first BSP option (just 1 BSP option in dropdown) on init', fakeAsync(() => {
      const mockBsp = [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }];

      fixture.detectChanges();

      expect(component.selectedBsps).toEqual(mockBsp);
      expect(component.predefinedFilters).toEqual(jasmine.objectContaining({ bsp: mockBsp[0] }));
      expect(component.isBspFilterLocked).toBe(true);
    }));
  });

  describe('initializeLeanBspFilter$', () => {
    describe('only 1 BSP option & LEAN User', () => {
      const mockBspValue = {
        id: 1,
        name: 'SPAIN',
        isoCountryCode: 'ES'
      };

      it('should have `selectedBsps` and `predefinedFilters` values', fakeAsync(() => {
        fixture.detectChanges();

        expect(component.selectedBsps).toEqual([mockBspValue]);
        expect(component.predefinedFilters).toEqual(jasmine.objectContaining({ bsp: mockBspValue }));
      }));

      it('should have filter flags values depending on `selectedBsps`', fakeAsync(() => {
        fixture.detectChanges();

        expect(component.isMonthPeriodPicker).toBe(false);
        expect(component.isBspFilterLocked).toBe(true);
      }));

      it('should have `userBspOptions` value', fakeAsync(() => {
        fixture.detectChanges();

        expect(component.userBspOptions).toEqual([{ value: mockBspValue, label: 'ES - SPAIN' }]);
      }));
    });

    describe('multiple BSP options & LEAN User', () => {
      beforeEach(() => {
        const mockBsps = [
          { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
          { id: 2, name: 'MALTA', isoCountryCode: 'MT' }
        ];

        bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(of(mockBsps));

        fixture.detectChanges();
      });

      it('should have `selectedBsps` and `predefinedFilters` values', fakeAsync(() => {
        expect(component.selectedBsps).toEqual([]);
        expect(component.predefinedFilters).toBeUndefined();
      }));

      it('should have filter flags values depending on `selectedBsps`', fakeAsync(() => {
        expect(component.isMonthPeriodPicker).toBe(true);
        expect(component.isBspFilterLocked).toBe(false);
      }));

      it('should have `userBspOptions` value', fakeAsync(() => {
        expect(component.userBspOptions).toEqual([
          { value: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' }, label: 'ES - SPAIN' },
          { value: { id: 2, name: 'MALTA', isoCountryCode: 'MT' }, label: 'MT - MALTA' }
        ]);
      }));
    });

    describe('no-LEAN user', () => {
      beforeEach(() => {
        mockStore.overrideSelector(getUser, {
          ...userMock,
          permissions: [Permissions.readRejectedDocuments, Permissions.readDocumentDetails]
        });

        fixture.detectChanges();
      });

      const mockBspValue = {
        id: 1,
        name: 'SPAIN',
        isoCountryCode: 'ES'
      };

      it('should have `selectedBsps` and `predefinedFilters` values', fakeAsync(() => {
        expect(component.selectedBsps).toEqual([mockBspValue]);
        expect(component.predefinedFilters).toEqual(jasmine.objectContaining({ bsp: mockBspValue }));
      }));

      it('should have filter flags values depending on `selectedBsps`', fakeAsync(() => {
        fixture.detectChanges();

        expect(component.isMonthPeriodPicker).toBe(false);
        expect(component.isBspFilterLocked).toBe(true);
      }));

      it('should have `userBspOptions` value', fakeAsync(() => {
        fixture.detectChanges();

        expect(component.userBspOptions).toEqual([{ value: mockBspValue, label: 'ES - SPAIN' }]);
      }));
    });
  });

  describe('getAgentDropDownByUserType', () => {
    it('Should popluate agentDropdownOptions for Agent group users with query permission', fakeAsync(() => {
      mockStore.overrideSelector(getUser, {
        ...userMock,
        UserType: UserType.AGENT_GROUP,
        bspPermissions: [{ bspId: 1, permissions: [Permissions.readRejectedDocuments] }]
      });

      const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
        {
          value: {
            id: '1',
            name: 'AGENT 1111111',
            code: '1111111',
            bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
          },
          label: '1111111 / AGENT 1111111'
        }
      ];

      agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));

      let result;
      fixture.detectChanges();

      component.agentDropdownOptions$.subscribe(data => (result = data));

      expect(result).toEqual(mockAgentDropdownOptions);
    }));

    it('Should popluate agentDropdownOptions for non Agent group users', fakeAsync(() => {
      mockStore.overrideSelector(getUser, {
        ...userMock,
        UserType: UserType.AIRLINE,
        bspPermissions: [{ bspId: 1 }]
      });

      const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
        {
          value: {
            id: '1',
            name: 'AGENT 1111111',
            code: '1111111',
            bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
          },
          label: '1111111 / AGENT 1111111'
        }
      ];

      agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));

      let result;
      fixture.detectChanges();

      component.agentDropdownOptions$.subscribe(data => (result = data));

      expect(result).toEqual(mockAgentDropdownOptions);
    }));

    it('Should not populuate agentDropdownOptions for Agent group users without query permission', () => {
      mockStore.overrideSelector(getUser, {
        ...userMock,
        UserType: UserType.AGENT_GROUP
      });

      agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([]));

      let result;
      fixture.detectChanges();

      component.agentDropdownOptions$.subscribe(data => (result = data));

      expect(result).toEqual([]);
    });
  });

  describe('initializeBspListener', () => {
    it('should update `selectedBsps` and corresponding flags on BSP selection', fakeAsync(() => {
      const filterValue: RejectedDocumentFilter = {
        bsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT' },
        rejectionPeriodRange: [
          { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' },
          { period: '2020122', dateFrom: '12/16/2020', dateTo: '12/31/2020' }
        ],
        rejectionMonthPeriod: [],
        type: [],
        rejectionDateRange: [],
        agent: [],
        airline: [],
        errorLevel: [],
        gds: [],
        errorCode: '',
        documentNumber: ''
      };

      fixture.detectChanges();

      component.searchForm.setValue(filterValue);
      tick();

      expect(component.selectedBsps).toEqual([{ id: 2, name: 'MALTA', isoCountryCode: 'MT' }]);
      expect(component.isMonthPeriodPicker).toBe(false);
    }));

    it('should update rejectionPeriodRange field on single BSP selection', () => {
      const filterValue: RejectedDocumentFilter = {
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
        rejectionPeriodRange: [
          { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/15/2020' },
          { period: '2020122', dateFrom: '12/16/2020', dateTo: '12/31/2020' }
        ],
        rejectionMonthPeriod: [],
        airline: [{ id: 1, name: 'AIRLINE 001', code: '001', designator: 'D1' }],
        agent: [{ id: '1', name: 'AGENT 001', code: '001' }],
        gds: [{ id: 1, version: 1, gdsCode: '001', name: 'GDS 001' }],
        errorLevel: [],
        type: [],
        rejectionDateRange: [],
        errorCode: '',
        documentNumber: ''
      };

      fixture.detectChanges();

      component.searchForm.setValue(filterValue);
      const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT' };

      component['selectedBsps'] = [mockBsp];
      component['updatePeriodFilterValue']();

      expect(component.searchForm.controls.rejectionPeriodRange.value).toEqual(filterValue.rejectionPeriodRange);
      expect(component.searchForm.controls.rejectionMonthPeriod.value).toBeNull();
    });

    it('should initialize filter dropdowns with selected BSP', fakeAsync(() => {
      spyOn<any>(component, 'getAgentDropDownByUserType').and.callThrough();

      fixture.detectChanges();

      const mockBsp = [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }];
      component['selectedBsps'] = mockBsp;

      expect(airlineDictionarySpy.getDropdownOptions).toHaveBeenCalledWith(1);
      expect(component['getAgentDropDownByUserType']).toHaveBeenCalledWith({ bspId: [1] });
      expect(gdsDictionarySpy.getDropdownOptions).toHaveBeenCalledWith([1]);
      expect(periodServiceSpy.getByBsp).toHaveBeenCalledWith(1);
    }));
  });

  describe('build columns', () => {
    const columns = [
      jasmine.objectContaining({
        name: 'document.rejected.columns.bsp',
        prop: 'isoCountryCode',
        sortable: false
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.documentNumber',
        prop: 'documentNumber',
        cellTemplate: jasmine.any(TemplateRef)
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.type',
        prop: 'type'
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.gds',
        prop: 'gds.name'
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.errorLevel',
        prop: 'errorLevel'
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.errorCode',
        prop: 'errorCode'
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.reasonForRejection',
        prop: 'reasonForRejection'
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.amount',
        prop: 'amount'
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.currency',
        prop: 'currency'
      }),
      jasmine.objectContaining({
        name: 'document.rejected.columns.rejectionDate',
        prop: 'rejectionDate',
        cellTemplate: 'dayMonthYearCellTmpl'
      })
    ];

    it('should build columns as AIRLINE on init', () => {
      const expectedColumns = jasmine.arrayContaining<any>([
        ...columns,
        jasmine.objectContaining({
          name: 'document.rejected.columns.airline',
          prop: 'airline',
          hidden: true,
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'document.rejected.columns.agent',
          prop: 'agent',
          hidden: false,
          pipe: jasmine.anything()
        })
      ]);

      fixture.detectChanges();
      expect(component.columns).toEqual(expectedColumns);
    });

    it('should build columns as AGENT on init', () => {
      const expectedColumns = jasmine.arrayContaining<any>([
        ...columns,
        jasmine.objectContaining({
          name: 'document.rejected.columns.airline',
          prop: 'airline',
          hidden: false,
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'document.rejected.columns.agent',
          prop: 'agent',
          hidden: true,
          pipe: jasmine.anything()
        })
      ]);

      const service = TestBed.inject(PermissionsService);
      spyOn(service, 'hasUserType').and.callFake((type: UserType) => type === UserType.AGENT);

      fixture.detectChanges();
      expect(component.columns).toEqual(expectedColumns);
    });
  });

  it('should have default sorting', () => {
    const expectedQuery = jasmine.objectContaining({
      sortBy: [{ attribute: 'rejectionDate', sortType: SortOrder.Desc }]
    });

    fixture.detectChanges();

    component.loadData();
    expect(dataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
  });

  it('download should call dialog service open', fakeAsync(() => {
    fixture.detectChanges();

    component.onDownload();
    tick();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  }));

  it('should navigate to document enquiry when onOpenDocument is called.', fakeAsync(() => {
    const documentNumber = '7890123456';

    fixture.detectChanges();

    component.onOpenDocument(documentNumber);
    tick();

    expect(routerSpy.navigate).toHaveBeenCalledWith([ROUTES.DOCUMENT_ENQUIRY.url, documentNumber]);
  }));
});
