import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEmpty } from 'lodash';
import moment from 'moment-mini';
import { Observable, of, Subject } from 'rxjs';
import { first, map, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';

import { ErrorLevel } from '../models/error-level.enum';
import { RejectedDocumentFilter } from '../models/rejected-document-filter.model';
import { RejectedDocument } from '../models/rejected-document.model';
import { RejectedDocumentFilterFormatter } from '../services/rejected-document-filter-formatter';
import { RejectedDocumentsService } from '../services/rejected-documents.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  DialogService,
  DownloadFileComponent,
  FooterButton,
  PeriodOption,
  PeriodUtils
} from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { DocumentType, SelectMode, SortOrder } from '~app/shared/enums';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GdsSummary } from '~app/shared/models/dictionary/gds-summary.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-rejected-list',
  templateUrl: './rejected-list.component.html',
  providers: [DefaultQueryStorage, QueryableDataSource, { provide: QUERYABLE, useExisting: RejectedDocumentsService }]
})
export class RejectedListComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public columns: Array<GridColumn>;

  public userBspOptions: DropdownOption<BspDto>[] = [];
  public selectedBsps: BspDto[] = [];
  public buttonDesign = ButtonDesign;
  public selectMode = SelectMode;

  public predefinedFilters: {
    bsp: BspDto | BspDto[];
    rejectionPeriodRange: PeriodOption[];
  };

  public canQueryAgent: boolean;
  public canQueryAirline: boolean;

  public airlineDropdownOptions$: Observable<DropdownOption[]>;
  public agentDropdownOptions$: Observable<DropdownOption[]>;
  public currencyDropdownOptions$: Observable<DropdownOption[]>;
  public gdsOptions: DropdownOption<GdsSummary>[];

  public periodDropdownOptions: PeriodOption[];
  public errorLevelOptions: DropdownOption<ErrorLevel>[] = ArrayHelper.toDropdownOptions(Object.values(ErrorLevel));
  public typeOptions: DropdownOption<DocumentType>[] = ArrayHelper.toDropdownOptions(Object.values(DocumentType));
  public maxDate = moment().startOf('day').toDate();
  public minDate = moment(this.maxDate).subtract(2, 'years').toDate();
  public canReadDocuments = this.permissionsService.hasPermission(Permissions.readDocumentDetails);
  public isBspFilterLocked: boolean;

  @ViewChild('documentLinkTemplate', { static: true })
  private documentLinkTemplate: TemplateRef<any>;

  private formFactory: FormUtil;

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  public periodPickerYearsBack: number;
  public isMonthPeriodPicker: boolean;
  public hasLeanPermission: boolean;
  private destroy$ = new Subject();

  private get currentPeriodValue(): PeriodOption[] {
    const currentPeriod = PeriodUtils.findCurrentPeriod(this.periodDropdownOptions || []);

    return currentPeriod ? [currentPeriod, currentPeriod] : null;
  }

  private loggedUser$: Observable<User> = this.store.pipe(select(getUser), first());

  constructor(
    public dataSource: QueryableDataSource<RejectedDocument>,
    public displayFilterFormatter: RejectedDocumentFilterFormatter,
    public periodPipe: PeriodPipe,
    private queryStorage: DefaultQueryStorage,
    private permissionsService: PermissionsService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private airlineDictionary: AirlineDictionaryService,
    private agentDictionary: AgentDictionaryService,
    private gdsDictionary: GdsDictionaryService,
    private periodDictionary: PeriodService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private rejectedDocumentsService: RejectedDocumentsService,
    private bspsDictionaryService: BspsDictionaryService,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializePermissions();

    this.searchForm = this.buildSearchForm();
    this.columns = this.buildColumns();

    this.setPeriodPickerYearsBack();

    this.initializeBspListener();

    const storedQuery = this.queryStorage.get();

    this.loggedUser$
      .pipe(
        switchMap(user => this.initializeLeanBspFilter$(user)),
        takeUntil(this.destroy$)
      )
      .subscribe(async () => {
        await this.initializeFilterDropdowns();
        this.loadData(storedQuery);
      });
  }

  public getPeriod(period: string): string {
    return this.periodPipe.transform(period);
  }

  private initializeLeanBspFilter$(loggedUser: User): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(loggedUser.bspPermissions, [Permissions.readRejectedDocuments])
      .pipe(
        tap(bspList => {
          const onlyOneBsp = bspList.length === 1;

          if (bspList.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === loggedUser.defaultIsoc);

            const firstListBsp = bspList[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            this.selectedBsps = [filterValue];

            this.predefinedFilters = {
              ...this.predefinedFilters,
              bsp: filterValue
            };
          }

          this.isMonthPeriodPicker = this.selectedBsps.length !== 1;
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspList => (this.isBspFilterLocked = bspList.length === 1)),
        tap(bspOptions => (this.userBspOptions = bspOptions))
      );
  }

  private initializePermissions(): void {
    this.canQueryAgent = !this.permissionsService.hasUserType(UserType.AGENT);
    this.canQueryAirline = !this.permissionsService.hasUserType(UserType.AIRLINE);
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  public loadData(query?: DataQuery): void {
    query = query || cloneDeep(defaultQuery);

    let dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy,
        ...this.handleQueryFilter(query.filterBy)
      }
    };

    if (isEmpty(dataQuery.sortBy)) {
      const sortBy = [{ attribute: 'rejectionDate', sortType: SortOrder.Desc }];
      dataQuery = { ...dataQuery, sortBy };
    }

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  /** Handles query filter adding `monthPeriod` or `periodRange` depending on filtered BSPs and user LEAN permission */
  private handleQueryFilter(queryFilter: Partial<RejectedDocumentFilter>): Partial<RejectedDocumentFilter> {
    const filteredBsps = (queryFilter.bsp || this.selectedBsps) as BspDto[];
    const modifiedBspFilter = this.getArrayOfBsps(filteredBsps);

    return {
      ...this.predefinedFilters,
      ...queryFilter,
      ...queryFilter,
      rejectionMonthPeriod:
        modifiedBspFilter.length !== 1 ? queryFilter.rejectionMonthPeriod || this.currentPeriodValue : null,
      rejectionPeriodRange:
        modifiedBspFilter.length === 1 ? queryFilter.rejectionPeriodRange || this.currentPeriodValue : null,
      ...(this.hasLeanPermission && { bsp: filteredBsps })
    };
  }

  public onDownload(): void {
    const requestQuery = this.queryStorage.get();

    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: requestQuery
      },
      apiService: this.rejectedDocumentsService
    });
  }

  public onOpenDocument(documentNumber: string): void {
    this.router.navigate([ROUTES.DOCUMENT_ENQUIRY.url, documentNumber]);
  }

  private async initializeFilterDropdowns(): Promise<void> {
    const bspSelectedIds = this.selectedBsps.map(({ id }) => id);
    const firstBsp = this.selectedBsps.length ? this.selectedBsps[0] : this.userBspOptions[0]?.value;
    const params = bspSelectedIds.length ? { bspId: bspSelectedIds } : null;

    this.airlineDropdownOptions$ = this.airlineDictionary.getDropdownOptions(firstBsp.id);
    this.agentDropdownOptions$ = this.getAgentDropDownByUserType(params);
    this.gdsDictionary.getDropdownOptions(bspSelectedIds).subscribe(gds => (this.gdsOptions = gds));

    await this.initializePeriodFilterDropdown(firstBsp);
  }

  private async initializePeriodFilterDropdown(selectedBsp: BspDto): Promise<void> {
    this.periodDropdownOptions = await this.periodDictionary.getByBsp(selectedBsp.id).toPromise();
    this.updatePeriodFilterValue();
  }

  private getAgentDropDownByUserType(params: { bspId: number[] }): Observable<DropdownOption<AgentSummary>[]> {
    const isAgentGroupUser = this.permissionsService.hasUserType(UserType.AGENT_GROUP);

    return isAgentGroupUser ? this.getAgentGroupDropDown(params) : this.agentDictionary.getDropdownOptions(params);
  }

  private getAgentGroupDropDown(queryFilters): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = Permissions.readRejectedDocuments;
    const aGroupUserHasPermission = this.permissionsService.hasPermission(aGroupUserPermission);
    let agentGroupDropdown: Observable<DropdownOption<AgentSummary>[]> = of(null);

    if (aGroupUserHasPermission) {
      const filter = { ...queryFilters, permission: aGroupUserPermission };

      agentGroupDropdown = this.agentDictionary.getDropdownOptions(filter);
    }

    return agentGroupDropdown;
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<RejectedDocumentFilter>({
      bsp: [],
      rejectionPeriodRange: [],
      rejectionMonthPeriod: [],
      type: [],
      rejectionDateRange: [],
      agent: [],
      airline: [],
      errorLevel: [],
      gds: [],
      errorCode: [],
      documentNumber: []
    });
  }

  private initializeBspListener(): void {
    FormUtil.get<RejectedDocumentFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(async value => {
        this.selectedBsps = this.getArrayOfBsps(value);
        this.isMonthPeriodPicker = this.selectedBsps.length !== 1;

        await this.initializeFilterDropdowns();

        this.updatePeriodFilterValue();
        this.updateAgentFilterValue();
        this.updateGDSFilterValue();
      });
  }

  private getArrayOfBsps(bsps): BspDto[] {
    if (!bsps) {
      return [];
    }

    return Array.isArray(bsps) ? bsps : [bsps];
  }

  private updateAgentFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentControl = FormUtil.get<RejectedDocumentFilter>(this.searchForm, 'agent');
    const agentsSelected: AgentSummary[] = agentControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.selectedBsps.some(({ id }) => agent.bsp?.id === id));
      agentControl.patchValue(agentsToPatch);
    }
  }

  private updateGDSFilterValue(): void {
    // If there are not selected BSPs, dropdown has all gds from all BSPs so there is no reason to patch gds control value
    if (!this.selectedBsps.length) {
      return;
    }

    const gdsControl = FormUtil.get<RejectedDocumentFilter>(this.searchForm, 'gds');
    const gdsSelected: GdsSummary[] = gdsControl.value;

    if (gdsSelected?.length) {
      const gdsToPatch = gdsSelected.filter(gds => this.gdsOptions.some(({ value }) => value.id === gds.id));
      gdsControl.patchValue(gdsToPatch);
    }
  }

  private updatePeriodFilterValue(): void {
    const periodRangeControl = FormUtil.get<RejectedDocumentFilter>(this.searchForm, 'rejectionPeriodRange');
    const monthPeriodControl = FormUtil.get<RejectedDocumentFilter>(this.searchForm, 'rejectionMonthPeriod');

    if (this.selectedBsps.length === 1) {
      periodRangeControl.patchValue(periodRangeControl.value || this.currentPeriodValue);
      monthPeriodControl.reset();
    } else {
      monthPeriodControl.patchValue(monthPeriodControl.value || this.currentPeriodValue);
      periodRangeControl.reset();
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: 'document.rejected.columns.bsp',
        prop: 'isoCountryCode',
        width: 55,
        sortable: false
      },
      {
        name: 'document.rejected.columns.documentNumber',
        prop: 'documentNumber',
        width: 120,
        cellTemplate: this.documentLinkTemplate
      },
      {
        name: 'document.rejected.columns.type',
        prop: 'type',
        width: 120
      },
      {
        name: 'document.rejected.columns.airline',
        prop: 'airline',
        width: 250,
        hidden: !this.canQueryAirline,
        pipe: { transform: airline => `${airline.iataCode} / ${airline.localName}` }
      },
      {
        name: 'document.rejected.columns.agent',
        prop: 'agent',
        width: 250,
        hidden: !this.canQueryAgent,
        pipe: { transform: agent => `${agent.iataCode} / ${agent.name}` }
      },
      {
        name: 'document.rejected.columns.gds',
        prop: 'gds.name',
        width: 120
      },
      {
        name: 'document.rejected.columns.errorLevel',
        prop: 'errorLevel',
        width: 85
      },
      {
        name: 'document.rejected.columns.errorCode',
        prop: 'errorCode',
        width: 85
      },
      {
        name: 'document.rejected.columns.reasonForRejection',
        prop: 'reasonForRejection',
        width: 350
      },
      {
        name: 'document.rejected.columns.amount',
        prop: 'amount',
        width: 120
      },
      {
        name: 'document.rejected.columns.currency',
        prop: 'currency',
        width: 120
      },
      {
        name: 'document.rejected.columns.rejectionDate',
        prop: 'rejectionDate',
        width: 100,
        cellTemplate: 'dayMonthYearCellTmpl'
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
