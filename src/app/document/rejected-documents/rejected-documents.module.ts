import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RejectedDocumentsRoutingModule } from './rejected-documents-routing.module';
import { RejectedListComponent } from './rejected-list/rejected-list.component';
import { RejectedDocumentFilterFormatter } from './services/rejected-document-filter-formatter';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, RejectedDocumentsRoutingModule],
  declarations: [RejectedListComponent],
  providers: [RejectedDocumentFilterFormatter]
})
export class RejectedDocumentModule {}
