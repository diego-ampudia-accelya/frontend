import { ErrorLevel } from './error-level.enum';
import { DocumentType } from '~app/shared/enums';

export class RejectedDocument {
  bspId: number;
  isoCountryCode: string;
  documentNumber: string;
  relatedDocumentNumber?: string;
  type: DocumentType;
  agent: {
    id: number;
    iataCode: string;
    name: string;
  };
  airline: {
    id: number;
    iataCode: string;
    localName: string;
  };
  gds: {
    id: number;
    gdsCode: string;
    name: string;
  };
  rejectionPeriod: {
    id: number;
    period: string;
  };
  rejectionDate: string;
  errorLevel: ErrorLevel;
  errorCode: string;
  reasonForRejection: string;
  errorValue: string;
  amount: string;
  currency: string;
  dishElement: string;
}
