export enum ErrorLevel {
  Warning = 'Warning',
  Modify = 'Modify',
  Rejection = 'Rejection'
}
