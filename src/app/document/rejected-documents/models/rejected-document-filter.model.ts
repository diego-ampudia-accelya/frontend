import { ErrorLevel } from './error-level.enum';
import { PeriodOption } from '~app/shared/components';
import { DocumentType } from '~app/shared/enums';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GdsSummary } from '~app/shared/models/dictionary/gds-summary.model';

export interface RejectedDocumentFilter {
  bsp: BspDto | BspDto[];
  rejectionPeriodRange?: PeriodOption[];
  rejectionMonthPeriod: PeriodOption[];
  agent?: AgentSummary[];
  airline?: AirlineSummary[];
  gds?: GdsSummary[];
  errorLevel?: ErrorLevel[];
  type?: DocumentType[];
  rejectionDateRange?: Date[];
  errorCode?: string;
  documentNumber?: string;
}
