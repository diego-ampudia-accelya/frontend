import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { RejectedDocumentFilter } from '../models/rejected-document-filter.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import {
  agentFilterTagMapper,
  airlineFilterTagMapper,
  bspFilterTagMapper,
  gdsFilterTagMapper,
  identityMapper,
  rangeDateFilterTagMapper
} from '~app/shared/helpers';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

type DisplayFilterConfig<T> = {
  [P in keyof T]?: {
    label?: string;
    format?(value: T[P]): string | string[];
  };
};

@Injectable()
export class RejectedDocumentFilterFormatter {
  private configuration: DisplayFilterConfig<RejectedDocumentFilter> = {
    bsp: {
      label: 'document.rejected.filters.common.labels.bsp',
      format: bspFilterTagMapper
    },
    rejectionPeriodRange: { format: range => this.periodPipe.transform(range) },
    rejectionMonthPeriod: { format: range => this.periodPipe.transform(range, true) },
    type: { label: 'document.rejected.filters.type.label' },
    agent: { format: agentFilterTagMapper },
    airline: { format: airlineFilterTagMapper },
    gds: {
      label: 'document.rejected.filters.gds.label',
      format: gdsFilterTagMapper
    },
    errorLevel: { label: 'document.rejected.filters.errorLevel.label' },
    rejectionDateRange: {
      label: 'document.rejected.filters.rejectionDateRange.label',
      format: rangeDateFilterTagMapper
    },
    errorCode: {
      label: 'document.rejected.filters.errorCode.label',
      format: identityMapper
    },
    documentNumber: {
      label: 'document.rejected.filters.documentNumber.label',
      format: identityMapper
    }
  };

  constructor(private periodPipe: PeriodPipe, private translation: L10nTranslationService) {}

  public format(filters: RejectedDocumentFilter): AppliedFilter[] {
    return Object.entries(this.configuration)
      .filter(([filterKey]) => filters && !isEmpty(filters[filterKey]))
      .map(([filterKey, config]) => {
        const formatValue = config.format || identity;
        let formattedValue = formatValue(filters[filterKey]);
        if (Array.isArray(formattedValue)) {
          formattedValue = formattedValue.join(', ');
        }

        let label = formattedValue;
        if (config.label) {
          label = `${this.translation.translate(config.label)} - ${formattedValue}`;
        }

        return { label, keys: [filterKey] };
      });
  }
}
