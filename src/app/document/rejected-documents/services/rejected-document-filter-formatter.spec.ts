import { DatePipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { ErrorLevel } from '../models/error-level.enum';
import { RejectedDocumentFilter } from '../models/rejected-document-filter.model';
import { RejectedDocumentFilterFormatter } from './rejected-document-filter-formatter';
import { AppliedFilter } from '~app/shared/components/list-view';
import { DocumentType } from '~app/shared/enums';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

describe('RejectedDocumentFilterFormatter', () => {
  let formatter: RejectedDocumentFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const requiredFilter: RejectedDocumentFilter = {
    bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
    rejectionPeriodRange: [
      { period: '2021021', dateFrom: '02/01/2021', dateTo: '02/15/2021' },
      { period: '2021022', dateFrom: '02/16/2021', dateTo: '02/29/2021' }
    ],
    rejectionMonthPeriod: []
  };

  const requiredFilterResult: AppliedFilter[] = [
    {
      keys: ['bsp'],
      label: 'document.rejected.filters.common.labels.bsp - SPAIN (ES)'
    },
    {
      keys: ['rejectionPeriodRange'],
      label: 'P1 Feb 2021 - P2 Feb 2021'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RejectedDocumentFilterFormatter,
        PeriodPipe,
        DatePipe,
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    });

    translationServiceSpy.translate.and.callFake(identity);
    formatter = TestBed.inject(RejectedDocumentFilterFormatter);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format required fields', () => {
    expect(formatter.format(requiredFilter)).toEqual(requiredFilterResult);
  });

  it('should format specified fields if required filters are missing', () => {
    const filter: any = { type: [DocumentType.ACMA, DocumentType.CANX] };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['type'],
        label: 'document.rejected.filters.type.label - ACMA, CANX'
      }
    ]);
  });

  it('should format required fields and document types if they exist', () => {
    const filter: RejectedDocumentFilter = {
      ...requiredFilter,
      type: [DocumentType.EMDA, DocumentType.SPDR]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['type'],
        label: 'document.rejected.filters.type.label - EMDA, SPDR'
      }
    ]);
  });

  it('should format required fields and agents if they exist', () => {
    const filter: RejectedDocumentFilter = {
      ...requiredFilter,
      agent: [
        { id: '1', code: '001', name: 'Agent 1' },
        { id: '2', code: '002', name: 'Agent 2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['agent'],
        label: '001 / Agent 1, 002 / Agent 2'
      }
    ]);
  });

  it('should format required fields and airlines if they exist', () => {
    const filter: RejectedDocumentFilter = {
      ...requiredFilter,
      airline: [
        { id: 1, code: '001', name: 'Airline 1', designator: 'DC' },
        { id: 2, code: '002', name: 'Airline 2', designator: 'AC' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['airline'],
        label: '001 / Airline 1, 002 / Airline 2'
      }
    ]);
  });

  it('should format required fields and gds if they exist', () => {
    const filter: RejectedDocumentFilter = {
      ...requiredFilter,
      gds: [
        { id: 1, version: 11, gdsCode: 'AGPD', name: 'AMADEUS' },
        { id: 2, version: 12, gdsCode: 'WSPN', name: 'WORLDSPAN' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['gds'],
        label: 'document.rejected.filters.gds.label - AGPD / AMADEUS, WSPN / WORLDSPAN'
      }
    ]);
  });

  it('should format required fields and error levels if they exist', () => {
    const filter: RejectedDocumentFilter = {
      ...requiredFilter,
      errorLevel: [ErrorLevel.Warning, ErrorLevel.Rejection]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['errorLevel'],
        label: 'document.rejected.filters.errorLevel.label - Warning, Rejection'
      }
    ]);
  });

  it('should format required fields and rejection dates if they exist with or without range', () => {
    // Date range has been specified
    const filterWithRangeDate: RejectedDocumentFilter = {
      ...requiredFilter,
      rejectionDateRange: [new Date('2020/07/14'), new Date('2020/07/21')]
    };
    // Unique date has been specified
    const filterWithUniqueDate: RejectedDocumentFilter = {
      ...requiredFilter,
      rejectionDateRange: [new Date('2020/07/14')]
    };

    expect(formatter.format(filterWithRangeDate)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['rejectionDateRange'],
        label: 'document.rejected.filters.rejectionDateRange.label - 14/07/2020 - 21/07/2020'
      }
    ]);

    expect(formatter.format(filterWithUniqueDate)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['rejectionDateRange'],
        label: 'document.rejected.filters.rejectionDateRange.label - 14/07/2020'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: RejectedDocumentFilter = {
      bsp: null,
      rejectionPeriodRange: [],
      rejectionMonthPeriod: []
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      ...requiredFilter,
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual(requiredFilterResult);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
