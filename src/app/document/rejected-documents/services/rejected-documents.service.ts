import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse, formatMonthPeriod } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import { RejectedDocumentFilter } from '../models/rejected-document-filter.model';
import { RejectedDocument } from '../models/rejected-document.model';

@Injectable({
  providedIn: 'root'
})
export class RejectedDocumentsService implements Queryable<RejectedDocument> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/documents/rejected-documents`;

  private sortMapping = [
    { from: 'airline', to: 'airlineCode' },
    { from: 'agent', to: 'agentCode' },
    { from: 'gds.name', to: 'gdsCode' }
  ];

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<RejectedDocumentFilter>): Observable<PagedData<RejectedDocument>> {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery({
      ...query,
      filterBy: this.formatFilter(query.filterBy),
      sortBy: formatSortBy(query.sortBy, this.sortMapping)
    });

    return this.http.get<PagedData<RejectedDocument>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<RejectedDocumentFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery({
      ...query,
      filterBy: this.formatFilter(query.filterBy),
      sortBy: formatSortBy(query.sortBy, this.sortMapping)
    });
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatFilter(filter: Partial<RejectedDocumentFilter>) {
    const {
      bsp,
      rejectionPeriodRange,
      rejectionMonthPeriod,
      agent,
      airline,
      gds,
      errorLevel,
      type,
      rejectionDateRange,
      errorCode,
      documentNumber
    } = filter;

    return {
      bspId: bsp && (!Array.isArray(bsp) ? [bsp] : bsp).map(({ id }) => id),
      rejectionPeriodFrom: rejectionPeriodRange && rejectionPeriodRange[0].period,
      rejectionPeriodTo: rejectionPeriodRange && rejectionPeriodRange[1].period,
      periodYearMonth: rejectionMonthPeriod && formatMonthPeriod(rejectionMonthPeriod[0].period),
      agentIataCode: agent && agent.map(item => item.code),
      airlineIataCode: airline && airline.map(item => item.code),
      gdsCode: gds && gds.map(item => item.gdsCode),
      rejectionDateFrom: rejectionDateRange && toShortIsoDate(rejectionDateRange[0]),
      rejectionDateTo: rejectionDateRange && toShortIsoDate(rejectionDateRange[1]),
      errorLevel,
      type,
      errorCode,
      documentNumber
    };
  }
}
