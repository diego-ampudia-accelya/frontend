import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { Period } from '~app/master-data/periods/shared/period.models';
import { DataQuery } from '~app/shared/components/list-view';
import { DocumentType, SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services';
import { ErrorLevel } from '../models/error-level.enum';
import { RejectedDocumentFilter } from '../models/rejected-document-filter.model';
import { RejectedDocumentsService } from './rejected-documents.service';

describe('RejectedDocumentsService', () => {
  let service: RejectedDocumentsService;
  let httpMock: SpyObject<HttpClient>;

  beforeEach(() => {
    httpMock = createSpyObject(HttpClient);
    httpMock.get.and.returnValue(of());

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpMock },
        {
          provide: AppConfigurationService,
          useValue: createSpyObject(AppConfigurationService, {
            baseApiPath: ''
          })
        }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(RejectedDocumentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('find', () => {
    const baseQuery: DataQuery<RejectedDocumentFilter> = {
      filterBy: {
        bsp: { id: 123 } as Bsp,
        rejectionPeriodRange: [{ period: '2020021' } as Period, { period: '2020024' } as Period]
      },
      sortBy: [],
      paginateBy: { size: 20, page: 1 }
    };

    it('should call rejected documents endpoint', () => {
      service.find(baseQuery).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('/documents/rejected-documents'));
    });

    it('should include pagination parameters in request', () => {
      service.find(baseQuery).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('page=1&size=20'));
    });

    it('should include `bsp id` parameter in request', () => {
      service.find(baseQuery).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('bspId=123'));
    });

    it('should include rejected period range parameters in request', () => {
      service.find(baseQuery).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(
        jasmine.stringMatching('rejectionPeriodFrom=2020021&rejectionPeriodTo=2020024')
      );
    });

    it('should include month period parameters in request', () => {
      const currentQuery = {
        sortBy: [],
        paginateBy: { size: 20, page: 1 },
        filterBy: {
          bsp: { id: 123 } as Bsp,
          rejectionMonthPeriod: [{ period: '2020021' } as Period, { period: '2020024' } as Period]
        }
      };

      service.find(currentQuery).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('periodYearMonth=2020-02'));
    });

    it('should include `agent` filters in request when specified', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          agent: [
            { id: '1', code: '001', name: 'Agent 1' },
            { id: '2', code: '002', name: 'Agent 2' }
          ]
        }
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('agentIataCode=001,002'));
    });

    it('should include `airline` filters in request when specified', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          airline: [
            { id: 1, code: '001', name: 'Airline 1', designator: 'DC' },
            { id: 2, code: '002', name: 'Airline 2', designator: 'AC' }
          ]
        }
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('airlineIataCode=001,002'));
    });

    it('should include `GDS` filters in request when specified', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          gds: [
            { id: 1, gdsCode: '001', name: 'GDS 1', version: 1 },
            { id: 2, gdsCode: '002', name: 'GDS 2', version: 1 }
          ]
        }
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('gdsCode=001,002'));
    });

    it('should include `error level` filters in request when specified', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          errorLevel: [ErrorLevel.Rejection, ErrorLevel.Modify]
        }
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('errorLevel=Rejection,Modify'));
    });

    it('should include `type` filters in request when specified', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          type: [DocumentType.TKTT, DocumentType.RFND]
        }
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('type=TKTT,RFND'));
    });

    it('should include `rejection date range` filters in request when specified', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          rejectionDateRange: [new Date('2020-02-01'), new Date('2020-02-28')]
        }
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(
        jasmine.stringMatching('rejectionDateFrom=2020-02-01&rejectionDateTo=2020-02-28')
      );
    });

    it('should map `airline` sort criteria to `airlineCode`', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'airline', sortType: SortOrder.Asc }]
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('sort=airlineCode,ASC'));
    });

    it('should map `agent` sort criteria to `agentCode`', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'agent', sortType: SortOrder.Asc }]
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('sort=agentCode,ASC'));
    });

    it('should map `gds.name` sort criteria to `gdsCode`', () => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'gds.name', sortType: SortOrder.Asc }]
      };

      service.find(query).subscribe();

      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('sort=gdsCode,ASC'));
    });

    it('should call rejected document endpoint with correct options', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.CSV).subscribe();
      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('/download'), {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));

    it('should include actual query in request', fakeAsync(() => {
      const query: DataQuery<RejectedDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy
        }
      };

      service.download(query, DownloadFormat.CSV).subscribe();
      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('page=1&size=20'), jasmine.anything());
    }));

    it('should call endpoint with specified download format', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.TXT).subscribe();
      expect(httpMock.get).toHaveBeenCalledWith(jasmine.stringMatching('exportAs=txt'), jasmine.anything());
    }));
  });
});
