import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RejectedListComponent } from './rejected-list/rejected-list.component';
import { ROUTES } from '~app/shared/constants/routes';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: RejectedListComponent,
        data: {
          tab: ROUTES.REJECTED_DOCUMENTS
        }
      }
    ])
  ]
})
export class RejectedDocumentsRoutingModule {}
