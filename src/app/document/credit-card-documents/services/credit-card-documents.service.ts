import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import moment from 'moment-mini';
import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { CreditCardDocumentFilter } from '../models/credit-card-document-filter.model';
import { CreditCardDocument } from '../models/credit-card-document.model';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { toPeriodEntityData } from '~app/shared/utils/entity-period-mapper.utils';

@Injectable({
  providedIn: 'root'
})
export class CreditCardDocumentsService implements Queryable<CreditCardDocument> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/globalsearch/hot-documents/credit-cards`;

  private sortMapper = [
    { from: 'currency.code', to: 'currencyCode' },
    { from: 'period', to: 'billingAnalysisEndingDate' }
  ];

  private _creditCardNumber: string;
  private _storedQuery: DataQuery<CreditCardDocumentFilter>;

  public get creditCardNumber(): string {
    return this._creditCardNumber;
  }

  public set creditCardNumber(cardNumber: string) {
    this._creditCardNumber = cardNumber;
  }

  public get storedQuery(): DataQuery<CreditCardDocumentFilter> {
    return this._storedQuery;
  }

  public set storedQuery(storedQuery: DataQuery<CreditCardDocumentFilter>) {
    this._storedQuery = storedQuery;
  }

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private periodDictionary: PeriodService
  ) {}

  public find(query: DataQuery<CreditCardDocumentFilter>): Observable<PagedData<CreditCardDocument>> {
    const options = this.formatOptions(query);
    const payload = this.formatFilter(query.filterBy);

    return this.http.post<PagedData<CreditCardDocument>>(this.baseUrl, payload, options).pipe(
      switchMap(data =>
        combineLatest([
          of(data),
          data.records.length ? this.periodDictionary.getByBsp(data.records[0].bsp.id) : of(null)
        ])
      ),
      map(([data, periods]) => (periods ? toPeriodEntityData<CreditCardDocument>(data, periods) : data))
    );
  }

  private formatOptions(query: DataQuery<CreditCardDocumentFilter>) {
    const sort = formatSortBy(query.sortBy, this.sortMapper);

    const params = {
      page: `${query.paginateBy.page}`,
      size: `${query.paginateBy.size}`,
      totalElements: `${query.paginateBy.totalElements}`
    };

    return query.sortBy.length
      ? { params: { ...params, sort: `${sort[0]?.attribute},${sort[0]?.sortType}` } }
      : { params };
  }

  private formatFilter(filter: Partial<CreditCardDocumentFilter>) {
    const { creditCardNumber, bsp, periodRange, monthPeriod, agent, airline, currency } = filter;

    return {
      formOfPaymentAccountNumber: creditCardNumber,
      isoCountryCode: bsp && (!Array.isArray(bsp) ? [bsp] : bsp).map(({ isoCountryCode }) => isoCountryCode),
      billingAnalysisEndingDateFrom: periodRange && this.formatPeriod(periodRange[0].dateFrom),
      billingAnalysisEndingDateTo: periodRange && this.formatPeriod(periodRange[1].dateTo),
      periodYearMonth: monthPeriod && this.formatMonthPeriod(monthPeriod[0].period),
      agentCode: agent && agent.map(item => item.code),
      airlineCode: airline && airline.map(item => item.code),
      currencyCode: currency && currency.map(item => item.code)
    };
  }

  private formatPeriod(date: string): string {
    return date ? moment(date, 'MM/DD/YYYY').format('YYYY-MM-DD') : null;
  }

  /**
   * Format month period, removing last specific period digit
   *
   * @param period with format YYYYMMP
   * @returns `periodYearMonth` with format YYYYMM
   */
  private formatMonthPeriod(period: string): string {
    return period.substring(0, period.length - 1);
  }

  public download(
    query: DataQuery<CreditCardDocumentFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const payload = this.formatFilter(query.filterBy);
    const url = `${this.baseUrl}?exportAs=${downloadFormat.toUpperCase()}`;

    return this.http
      .post<HttpResponse<ArrayBuffer>>(url, payload, downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }
}
