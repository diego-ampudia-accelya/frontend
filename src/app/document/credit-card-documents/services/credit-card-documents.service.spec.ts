import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { CreditCardDocumentFilter } from '../models/credit-card-document-filter.model';
import { CreditCardDocumentsService } from './credit-card-documents.service';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

describe('CreditCardDocumentsService', () => {
  let service: CreditCardDocumentsService;
  let baseQuery: DataQuery<CreditCardDocumentFilter>;
  let baseUrl: string;

  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy },
        {
          provide: AppConfigurationService,
          useValue: createSpyObject(AppConfigurationService, {
            baseApiPath: ''
          })
        }
      ]
    });
  });

  beforeEach(() => {
    baseQuery = {
      filterBy: {
        creditCardNumber: '1234567890123456'
      },
      sortBy: [],
      paginateBy: { size: 20, page: 0 }
    };

    baseUrl = '/globalsearch/hot-documents/credit-cards';
    service = TestBed.inject(CreditCardDocumentsService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    beforeEach(() => httpClientSpy.post.and.returnValue(of()));

    it('should call credit card documents endpoint', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.objectContaining({ formOfPaymentAccountNumber: '1234567890123456' }),
        jasmine.anything()
      );
    }));

    it('should include BSP in payload body when filtering by period', fakeAsync(() => {
      const query: DataQuery<CreditCardDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          bsp: [
            { id: 1, name: 'Spain', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
            { id: 2, name: 'Malta', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.objectContaining({ isoCountryCode: ['ES', 'MT'] }),
        jasmine.anything()
      );
    }));

    it('should include period range in payload body when filter by period', fakeAsync(() => {
      const query: DataQuery<CreditCardDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          periodRange: [
            { period: '2020121', dateFrom: '12/01/2020', dateTo: '12/07/2020' },
            { period: '2020122', dateFrom: '12/07/2020', dateTo: '12/15/2020' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.objectContaining({
          billingAnalysisEndingDateFrom: '2020-12-01',
          billingAnalysisEndingDateTo: '2020-12-15'
        }),
        jasmine.anything()
      );
    }));

    it('should include currency in payload body when filter by currency', fakeAsync(() => {
      const query: DataQuery<CreditCardDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          currency: [{ id: 10, code: '001', decimals: 2 }]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.objectContaining({
          currencyCode: ['001']
        }),
        jasmine.anything()
      );
    }));

    it('should include agent code in payload body when filter by agent', fakeAsync(() => {
      const query: DataQuery<CreditCardDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          agent: [{ id: '10', name: 'Agent Name', code: '001' }]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.objectContaining({
          agentCode: ['001']
        }),
        jasmine.anything()
      );
    }));

    it('should include airline code in payload body when filter by airline', fakeAsync(() => {
      const query: DataQuery<CreditCardDocumentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          airline: [{ id: 10, name: 'Airline Name', code: '001', designator: '01' }]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.objectContaining({
          airlineCode: ['001']
        }),
        jasmine.anything()
      );
    }));

    it('should sort by period', fakeAsync(() => {
      const query: DataQuery<CreditCardDocumentFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'period', sortType: SortOrder.Desc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.anything(),
        jasmine.objectContaining({ params: jasmine.objectContaining({ sort: 'billingAnalysisEndingDate,DESC' }) })
      );
    }));

    it('should sort by currency code', fakeAsync(() => {
      const query: DataQuery<CreditCardDocumentFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'currency.code', sortType: SortOrder.Asc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        baseUrl,
        jasmine.anything(),
        jasmine.objectContaining({ params: jasmine.objectContaining({ sort: 'currencyCode,ASC' }) })
      );
    }));
  });

  describe('download', () => {
    beforeEach(() => httpClientSpy.post.and.returnValue(of()));

    it('should call credit card documents download endpoint with proper options', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(`${baseUrl}?exportAs=CSV`, jasmine.anything(), {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));

    it('should include query in download request', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        `${baseUrl}?exportAs=CSV`,
        jasmine.objectContaining({ formOfPaymentAccountNumber: '1234567890123456' }),
        jasmine.anything()
      );
    }));

    it('should call endpoint with specified download format', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.TXT).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        jasmine.stringMatching('exportAs=TXT'),
        jasmine.anything(),
        jasmine.anything()
      );
    }));
  });
});
