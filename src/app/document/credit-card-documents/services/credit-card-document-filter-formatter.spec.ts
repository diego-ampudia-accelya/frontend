import { DatePipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { CreditCardDocumentFilter } from '../models/credit-card-document-filter.model';
import { CreditCardDocumentFilterFormatter } from './credit-card-document-filter-formatter';
import { PeriodOption } from '~app/shared/components';
import { AppliedFilter } from '~app/shared/components/list-view';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

describe('CreditCardDocumentFilterFormatter', () => {
  let formatter: CreditCardDocumentFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const requiredFilter: CreditCardDocumentFilter = {
    creditCardNumber: '1234567890123456'
  };

  const requiredFilterResult: AppliedFilter[] = [
    {
      keys: ['creditCardNumber'],
      label: 'document.creditCard.filters.creditCardNumber.label - 123456XXXXXX3456'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CreditCardDocumentFilterFormatter,
        PeriodPipe,
        DatePipe,
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    });

    translationServiceSpy.translate.and.callFake(identity);
    formatter = TestBed.inject(CreditCardDocumentFilterFormatter);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format required fields', () => {
    expect(formatter.format(requiredFilter)).toEqual(requiredFilterResult);
  });

  it('should format specified fields if required filters are missing', () => {
    const filter: any = {
      currency: [
        { id: 1, code: 'EUR', decimals: 2 },
        { id: 2, code: 'USD', decimals: 2 }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['currency'],
        label: 'document.creditCard.filters.currency.label - EUR, USD'
      }
    ]);
  });

  it('should format required filter and period filter if request', () => {
    const filter: CreditCardDocumentFilter = {
      ...requiredFilter,
      periodRange: [
        { period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020' },
        { period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020' }
      ] as PeriodOption[]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['periodRange'],
        label: 'P1 Feb 2020 - P2 Feb 2020'
      }
    ]);
  });

  it('should format required filter and month period filter if request', () => {
    const filter: CreditCardDocumentFilter = {
      ...requiredFilter,
      monthPeriod: [
        { period: '2020021', dateFrom: '02/01/2020', dateTo: '02/07/2020' },
        { period: '2020024', dateFrom: '02/21/2020', dateTo: '02/29/2020' }
      ] as PeriodOption[]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['monthPeriod'],
        label: 'Feb 2020'
      }
    ]);
  });

  it('should format required filter and airline filter if request', () => {
    const filter: CreditCardDocumentFilter = {
      ...requiredFilter,
      airline: [
        { id: 1, code: '001', name: 'Airline 1', designator: 'DC' },
        { id: 2, code: '002', name: 'Airline 2', designator: 'AC' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['airline'],
        label: 'document.creditCard.filters.airline.label - 001 / Airline 1, 002 / Airline 2'
      }
    ]);
  });

  it('should format required filter and agents filter if request', () => {
    const filter: CreditCardDocumentFilter = {
      ...requiredFilter,
      agent: [
        { id: '1', code: '001', name: 'Agent 1' },
        { id: '2', code: '002', name: 'Agent 2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['agent'],
        label: 'document.creditCard.filters.agent.label - 001 / Agent 1, 002 / Agent 2'
      }
    ]);
  });

  it('should format required filter and currencies if request', () => {
    const filter: CreditCardDocumentFilter = {
      ...requiredFilter,
      currency: [
        { id: 1, code: 'EUR', decimals: 2 },
        { id: 2, code: 'USD', decimals: 2 }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['currency'],
        label: 'document.creditCard.filters.currency.label - EUR, USD'
      }
    ]);
  });

  it('should format required filter and BSP if request', () => {
    const filter: CreditCardDocumentFilter = {
      ...requiredFilter,
      bsp: [
        {
          id: 7376,
          isoCountryCode: 'IL',
          name: 'ISRAEL',
          effectiveFrom: '2000-01-01',
          effectiveTo: null,
          version: 65,
          defaultCurrencyCode: null,
          active: true
        },
        {
          id: 7784,
          isoCountryCode: 'MT',
          name: 'MALTA',
          effectiveFrom: '2000-01-01',
          effectiveTo: null,
          version: 219,
          defaultCurrencyCode: null,
          active: true
        },
        {
          id: 6983,
          isoCountryCode: 'ES',
          name: 'SPAIN',
          effectiveFrom: '2000-01-01',
          effectiveTo: null,
          version: 186,
          defaultCurrencyCode: null,
          active: true
        }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      ...requiredFilterResult,
      {
        keys: ['bsp'],
        label: 'document.creditCard.filters.bsp.label - ISRAEL (IL), MALTA (MT), SPAIN (ES)'
      }
    ]);
  });

  it('should ignore unknown filter', () => {
    const filter: any = {
      ...requiredFilter,
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual(requiredFilterResult);
  });

  it('should return empty array if filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
