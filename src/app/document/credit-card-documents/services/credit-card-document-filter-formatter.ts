import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { CreditCardDocumentFilter } from '../models/credit-card-document-filter.model';
import { maskCreditCardHelper } from '~app/document/helpers/mask-credit-card.helper';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { agentFilterTagMapper, airlineFilterTagMapper, bspFilterTagMapper } from '~app/shared/helpers';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class CreditCardDocumentFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService, private periodPipe: PeriodPipe) {}

  public format(filter: CreditCardDocumentFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<CreditCardDocumentFilter>> = {
      creditCardNumber: creditcardNumber =>
        `${this.translate('creditCardNumber')} - ${maskCreditCardHelper(creditcardNumber)}`,
      bsp: bsp => `${this.translate('bsp')} - ${bspFilterTagMapper(bsp).join(', ')}`,
      periodRange: periodRange => this.periodPipe.transform(periodRange),
      monthPeriod: monthPeriod => this.periodPipe.transform(monthPeriod, true),
      agent: agent => `${this.translate('agent')} - ${agentFilterTagMapper(agent).join(', ')}`,
      airline: airline => `${this.translate('airline')} - ${airlineFilterTagMapper(airline).join(', ')}`,
      currency: currency => `${this.translate('currency')} - ${currency.map(curr => curr.code).join(', ')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`document.creditCard.filters.${key}.label`);
  }
}
