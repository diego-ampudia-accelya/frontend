import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface CreditCardDocument {
  id: string;
  bsp: BspDto;
  airline: AirlineSummaryAux;
  agent: AgentSummaryAux;
  billingAnalysisEndingDate: string;
  currency: Currency;
  documentNumber: string;
  formOfPayment: FormOfPayment;
  ticketAmount: number;
  period?: string;
}

export interface AirlineSummaryAux {
  id: number;
  iataCode: string;
  localName: string;
}

export interface AgentSummaryAux {
  id: number;
  iataCode: string;
  name: string;
  bsp: BspDto;
}

export interface FormOfPayment {
  approvalCode: string;
  expiryDate: string;
  extendedPaymentCode: string;
  formOfPaymentAmount: number;
  formOfPaymentType: string;
  invoiceDate: string;
  invoiceNumber: string;
}
