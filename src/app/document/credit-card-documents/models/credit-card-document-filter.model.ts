import { PeriodOption } from '~app/shared/components';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface CreditCardDocumentFilter {
  creditCardNumber?: string;
  periodRange?: PeriodOption[];
  monthPeriod?: PeriodOption[];
  bsp?: Bsp | Bsp[];
  agent?: AgentSummary[];
  airline?: AirlineSummary[];
  currency?: Currency[];
  documentNumber?: string;
  ticketAmount?: string;
  formOfPaymentType?: string;
  formOfPaymentAmount?: string;
  expiryDate?: string;
  extendedPaymentCode?: string;
  approvalCode?: string;
  invoiceDate?: string;
  invoiceNumber?: number;
}
