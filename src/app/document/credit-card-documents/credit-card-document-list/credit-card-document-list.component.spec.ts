import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { CreditCardDocument } from '../models/credit-card-document.model';
import { CreditCardDocumentFilterFormatter } from '../services/credit-card-document-filter-formatter';
import { CreditCardDocumentsService } from '../services/credit-card-documents.service';
import { CreditCardDocumentListComponent } from './credit-card-document-list.component';
import { getUser, getUserBsps } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { UserInfo } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  AppConfigurationService,
  CurrencyDictionaryService
} from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('CreditCardDocumentListComponent', () => {
  let component: CreditCardDocumentListComponent;
  let fixture: ComponentFixture<CreditCardDocumentListComponent>;

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const periodServiceSpy = createSpyObject(PeriodService);
  const airlineDictionarySpy = createSpyObject(AirlineDictionaryService);
  const agentDictionarySpy = createSpyObject(AgentDictionaryService);
  const currencyDictionarySpy = createSpyObject(CurrencyDictionaryService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);
  const creditCardDocumentsServiceSpy = createSpyObject(CreditCardDocumentsService);
  let mockStore: MockStore;

  const expectedUserDetails: Partial<UserInfo> = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: [Permissions.lean, Permissions.readCreditCardDocuments],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readCreditCardDocuments] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const mockBsp: Bsp = { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '01/01/2000' };

  const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
    {
      value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
      label: '001 / AIRLINE 001'
    },
    {
      value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
      label: '002 / AIRLINE 002'
    }
  ];

  const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
    {
      value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
      label: '1111111 / AGENT 1111111'
    },
    {
      value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
      label: '2222222 / AGENT 2222222'
    }
  ];

  const mockCurrencyDropdownOptions: DropdownOption<Currency>[] = [
    {
      value: { id: 1, code: 'EUR', decimals: 2 },
      label: 'EUR'
    },
    {
      value: { id: 1, code: 'USD', decimals: 2 },
      label: 'USD'
    }
  ];

  const mockPeriods: Period[] = [
    { id: 1, period: '2020021', dateFrom: '02/01/2020', dateTo: '02/15/2020', isoCountryCode: 'ES' },
    { id: 2, period: '2020022', dateFrom: '02/16/2020', dateTo: '02/29/2020', isoCountryCode: 'ES' }
  ];

  airlineDictionarySpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));
  agentDictionarySpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));
  currencyDictionarySpy.getDropdownOptions.and.returnValue(of(mockCurrencyDropdownOptions));
  currencyDictionarySpy.getFilteredDropdownOptions.and.returnValue(of(mockCurrencyDropdownOptions));
  periodServiceSpy.getByBsp.and.returnValue(of(mockPeriods));
  creditCardDocumentsServiceSpy.creditCardNumber = '1234567890123456';

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CreditCardDocumentListComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        CreditCardDocumentFilterFormatter,
        FormBuilder,
        L10nTranslationService,
        AppConfigurationService,
        PeriodPipe,
        DatePipe,
        PermissionsService,
        provideMockStore({
          selectors: [
            { selector: getUser, value: expectedUserDetails },
            { selector: getUserBsps, value: [mockBsp] }
          ]
        }),
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionarySpy },
        { provide: AgentDictionaryService, useValue: agentDictionarySpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionarySpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({}),
            queryParams: of({})
          }
        }
      ]
    })
      .overrideComponent(CreditCardDocumentListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardDocumentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user on component create', fakeAsync(() => {
    tick();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  }));

  it('should set correct columns on init as an AIRLINE', fakeAsync(() => {
    component['loggedUser'] = expectedUserDetails as User;

    const expectedColumns = jasmine.arrayContaining([
      {
        name: 'document.creditCard.columns.airlineCode',
        prop: 'airline.iataCode',
        width: 70,
        hidden: true
      },
      {
        name: 'document.creditCard.columns.agentCode',
        prop: 'agent.iataCode',
        width: 70,
        hidden: false
      }
    ]);

    const columns = component['buildColumns']();
    tick();
    expect(columns).toEqual(expectedColumns);
  }));

  it('should set correct columns on init as an AGENT', fakeAsync(() => {
    const expectedAgent = { ...expectedUserDetails, userType: UserType.AGENT };
    mockStore.overrideSelector(getUser, expectedAgent as User);
    component['loggedUser'] = expectedAgent as User;

    const expectedColumns = jasmine.arrayContaining([
      {
        name: 'document.creditCard.columns.airlineCode',
        prop: 'airline.iataCode',
        width: 70,
        hidden: false
      },
      {
        name: 'document.creditCard.columns.agentCode',
        prop: 'agent.iataCode',
        width: 70,
        hidden: true
      }
    ]);

    const columns = component['buildColumns']();
    tick();
    expect(columns).toEqual(expectedColumns);
  }));

  it('onViewDocument - should call navigate with correct params', fakeAsync(() => {
    const creditCardDoc: CreditCardDocument = {
      id: '1',
      documentNumber: '1234567890123456',
      agent: null,
      airline: null,
      bsp: mockBsp,
      billingAnalysisEndingDate: null,
      currency: null,
      formOfPayment: null,
      ticketAmount: 99
    };
    component.onViewDocument(creditCardDoc);
    tick();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/documents/enquiry', '1234567890123456']);
  }));

  it('onFilterButtonClicked - should apply credit card mask ', fakeAsync(() => {
    component.ngOnInit();
    component.predefinedFilters.creditCardNumber = '1234567890123456';
    component.onFilterButtonClicked(true);
    tick();
    expect(component.searchForm.get('creditCardNumber').value).toBe('123456XXXXXX3456');
  }));

  describe('filter dropdowns initialization', () => {
    it('should initialize PERIOD options dropdown', fakeAsync(() => {
      component.ngOnInit();
      tick();
      expect(periodServiceSpy.getByBsp).toHaveBeenCalledWith(1);
      expect(component.periodOptions).toEqual(mockPeriods);
    }));

    it('should initialize CURRENCY dropdown', fakeAsync(() => {
      component.ngOnInit();
      tick();
      expect(component.currencyDropdownOptions).toEqual(mockCurrencyDropdownOptions);
    }));
  });

  describe('load data', () => {
    it('should get data when credit card number is not null ', fakeAsync(() => {
      component.searchForm.get('creditCardNumber').patchValue('1234567890123456');
      spyOn<any>(component, 'getDataSource').and.returnValue(true);
      spyOn(component, 'loadData').and.callThrough();

      component.loadData();
      tick();
      expect(component['getDataSource']).toHaveBeenCalledTimes(1);
    }));

    it('should navigate successfully when no credit card number', fakeAsync(() => {
      component.searchForm.get('creditCardNumber').patchValue(null);

      spyOn<any>(component, 'getDataSource').and.returnValue(true);

      component.loadData();
      tick();
      expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/documents/credit-card/query');
      expect(component['getDataSource']).toHaveBeenCalledTimes(0);
    }));
  });
  describe('set predefined filters', () => {
    it('should set predefined credit card number filter if available', fakeAsync(() => {
      component.predefinedFilters = {};
      component.searchForm.get('creditCardNumber').patchValue('1234567890123456');

      component.loadData();
      tick();
      expect(component.predefinedFilters).toEqual({ creditCardNumber: '1234567890123456' });
    }));

    it('should NOT set predefined credit card number filter when credit card number is NOT available', fakeAsync(() => {
      component.searchForm.get('creditCardNumber').patchValue(null);

      component.loadData();
      tick();
      expect(component.predefinedFilters).toEqual({
        creditCardNumber: undefined
      });
    }));

    it('download should call dialog service open', fakeAsync(() => {
      component.onDownload();
      tick();
      expect(dialogServiceSpy.open).toHaveBeenCalled();
    }));
  });
});
