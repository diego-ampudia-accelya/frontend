import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { first, skipWhile, takeUntil } from 'rxjs/operators';

import { CreditCardDocumentFilter } from '../models/credit-card-document-filter.model';
import { CreditCardDocument } from '../models/credit-card-document.model';
import { CreditCardDocumentFilterFormatter } from '../services/credit-card-document-filter-formatter';
import { CreditCardDocumentsService } from '../services/credit-card-documents.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { maskCreditCardHelper } from '~app/document/helpers/mask-credit-card.helper';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogService, DownloadFileComponent, FooterButton, PeriodOption } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { ROUTES } from '~app/shared/constants/routes';
import { SelectMode } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService, CurrencyDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-credit-card-document-list',
  templateUrl: './credit-card-document-list.component.html',
  styleUrls: ['./credit-card-document-list.component.scss'],
  providers: [DefaultQueryStorage, QueryableDataSource, { provide: QUERYABLE, useExisting: CreditCardDocumentsService }]
})
export class CreditCardDocumentListComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public columns: Array<GridColumn>;

  public selectedBsps: Bsp[] = [];
  public buttonDesign = ButtonDesign;
  public selectMode = SelectMode;
  public userTypes = UserType;

  public predefinedFilters: CreditCardDocumentFilter = {};

  public airlineDropdownOptions$: Observable<DropdownOption[]>;
  public agentDropdownOptions$: Observable<DropdownOption[]>;

  public userBspOptions: DropdownOption<Bsp>[] = [];
  public periodOptions: PeriodOption[];
  public currencyDropdownOptions: DropdownOption<Currency>[];

  public isAirlineLoggedUser: boolean;
  public isAgentLoggedUser: boolean;

  public isMonthPeriodPicker: boolean;
  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;

  private LEAN_USERS_YEARS = 5;
  private NON_LEAN_USERS_YEARS = 2;
  public periodPickerYearsBack: number;

  private loggedUser: User;
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private hasLeanPermission: boolean;

  private formFactory: FormUtil;

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<CreditCardDocument>,
    public displayFilterFormatter: CreditCardDocumentFilterFormatter,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private periodDictionary: PeriodService,
    private airlineDictionary: AirlineDictionaryService,
    private agentDictionary: AgentDictionaryService,
    private currencyDictionary: CurrencyDictionaryService,
    private creditCardDocumentsService: CreditCardDocumentsService,
    private permissionsService: PermissionsService,
    private router: Router,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private route: ActivatedRoute
  ) {
    this.formFactory = new FormUtil(this.formBuilder);

    this.reloadComponentData();
    this.initializeLoggedUser();
    this.initializePermissions();
  }

  public ngOnInit(): void {
    this.checkSelectedCreditCard();
    this.searchForm = this.buildSearchForm();
    this.columns = this.buildColumns();

    this.initializeBspFeatures();
    this.setPeriodPickerYearsBack();
    this.setResolversData();
    this.populateFilterDropdowns();
  }

  private checkSelectedCreditCard(): void {
    const removeSelectedCreditCard = this.route.snapshot?.data?.removeSelectedCreditCard;

    if (removeSelectedCreditCard) {
      this.creditCardDocumentsService.creditCardNumber = null;
    }
  }

  private reloadComponentData(): void {
    if (this.router && this.router.routeReuseStrategy) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    this.router.events?.pipe(takeUntil(this.destroy$)).subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  private setResolversData(): void {
    const storedQuery = this.creditCardDocumentsService.storedQuery;

    this.setPredefinedFilters();
    this.loadData(storedQuery);
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      creditCardNumber: this.creditCardDocumentsService.creditCardNumber,
      ...(!this.hasLeanPermission && { bsp: this.selectedBsps[0] })
    };
  }

  public loadData(query?: DataQuery<CreditCardDocumentFilter>): void {
    const { creditCardNumber } = this.searchForm.controls;

    //* This is to start the whole process if the user refreshes the page
    if (!creditCardNumber.value && !this.creditCardDocumentsService.creditCardNumber) {
      this.router.navigateByUrl(`${ROUTES.CREDIT_CARD_DOCUMENTS.url}/query`);
    } else {
      this.updatePredefinedFilters(creditCardNumber.value);
      this.getDataSource(query);
    }
  }

  private updatePredefinedFilters(ccNumber: string): void {
    if (ccNumber && ccNumber !== maskCreditCardHelper(this.predefinedFilters.creditCardNumber)) {
      this.predefinedFilters = {
        ...this.predefinedFilters,
        creditCardNumber: ccNumber
      };
    }
  }

  private getDataSource(query: DataQuery<CreditCardDocumentFilter>): void {
    query = query || cloneDeep(defaultQuery);

    // Credit card number is taken unmasked from the predefined filter
    const { creditCardNumber, ...filterBy } = query.filterBy;
    const dataQuery: DataQuery<CreditCardDocumentFilter> = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...this.handleQueryFilter(filterBy)
      }
    };

    this.dataSource.get(dataQuery);
    this.creditCardDocumentsService.storedQuery = dataQuery;
    this.creditCardDocumentsService.creditCardNumber = creditCardNumber;
  }

  /** Handles query filter adding `bsp` property depending on filtered BSPs */
  private handleQueryFilter(queryFilter: Partial<CreditCardDocumentFilter>): Partial<CreditCardDocumentFilter> {
    // BSP filter is always an array if user has LEAN permission
    const filteredBsps = (queryFilter.bsp || this.selectedBsps) as Bsp[];

    return this.hasLeanPermission ? { ...queryFilter, bsp: filteredBsps } : queryFilter;
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    const { creditCardNumber } = this.searchForm.controls;

    if (isSearchPanelVisible) {
      creditCardNumber.patchValue(maskCreditCardHelper(this.predefinedFilters.creditCardNumber));
    }
  }

  public loggedUserIsOfType(type: UserType): boolean {
    return type.toUpperCase() === this.loggedUser.userType.toUpperCase();
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => (this.loggedUser = loggedUser));
    this.isAirlineLoggedUser = this.loggedUserIsOfType(UserType.AIRLINE);
    this.isAgentLoggedUser = this.loggedUserIsOfType(UserType.AGENT);
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeBspFeatures(): void {
    if (this.hasLeanPermission) {
      this.initializeBspListener();
    }

    // Initialize BSP filter dropdown
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    this.initializeSelectedBsps();

    // Initialize BSP related flags
    this.isBspFilterLocked = !this.hasLeanPermission;
    this.isBspFilterMultiple = this.hasLeanPermission;
    this.isMonthPeriodPicker = this.selectedBsps.length !== 1;
  }

  private initializeBspListener(): void {
    FormUtil.get<CreditCardDocumentFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(async value => {
        const bspToArray = Array.isArray(value) ? value : [value];
        this.selectedBsps = value ? bspToArray : [];
        this.isMonthPeriodPicker = this.selectedBsps.length !== 1;

        await this.populateFilterDropdowns();

        this.updateFilterValues();
      });
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readCreditCardDocuments;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private initializeSelectedBsps(): void {
    const firstListBsp = this.userBspOptions[0].value;
    const defaultBsp = this.userBspOptions.find(
      ({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc
    )?.value;

    if (this.userBspOptions.length === 1) {
      this.selectedBsps = [firstListBsp];
    } else {
      this.selectedBsps = this.hasLeanPermission ? [] : [defaultBsp || firstListBsp];
    }
  }

  /** Since this method is only available for Airline users with LEAN permission, we do not need to update Airline filter */
  private updateFilterValues(): void {
    this.updatePeriodFilterValue();
    this.updateCurrencyFilterValue();
    this.updateAgentFilterValue();
  }

  private updatePeriodFilterValue(): void {
    const periodRangeControl = FormUtil.get<CreditCardDocumentFilter>(this.searchForm, 'periodRange');
    const monthPeriodControl = FormUtil.get<CreditCardDocumentFilter>(this.searchForm, 'monthPeriod');

    if (this.selectedBsps.length === 1) {
      monthPeriodControl.reset();
    } else {
      periodRangeControl.reset();
    }
  }

  private updateCurrencyFilterValue(): void {
    // If there are not selected BSPs, dropdown has all currencies from all BSPs so there is no reason to patch currency control value
    if (!this.selectedBsps.length) {
      return;
    }

    const currencyControl = FormUtil.get<CreditCardDocumentFilter>(this.searchForm, 'currency');
    const currenciesSelected: Currency[] = currencyControl.value;

    if (currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyDropdownOptions.some(({ value }) => value.id === currency.id)
      );
      currencyControl.patchValue(currenciesToPatch);
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.LEAN_USERS_YEARS : this.NON_LEAN_USERS_YEARS;
  }

  private updateAgentFilterValue(): void {
    // If there are not selected BSPs, dropdown has all agents from all BSPs so there is no reason to patch agent control value
    if (!this.selectedBsps.length) {
      return;
    }

    const agentControl = FormUtil.get<CreditCardDocumentFilter>(this.searchForm, 'agent');
    const agentsSelected: AgentSummary[] = agentControl.value;

    if (agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.selectedBsps.some(({ id }) => agent.bsp?.id === id));
      agentControl.patchValue(agentsToPatch);
    }
  }

  private async populateFilterDropdowns(): Promise<void> {
    const bspSelectedIds = this.selectedBsps.map(({ id }) => id);
    const firstBspId = bspSelectedIds.length ? bspSelectedIds[0] : this.userBspOptions[0]?.value.id;
    const params = bspSelectedIds.length ? { bspId: bspSelectedIds } : null;

    this.periodOptions = await this.periodDictionary.getByBsp(firstBspId).toPromise();
    this.currencyDropdownOptions = await this.currencyDictionary.getFilteredDropdownOptions(params).toPromise();

    // Filter for airlines only
    if (this.isAirlineLoggedUser) {
      this.agentDropdownOptions$ = this.agentDictionary.getDropdownOptions(params);
    }

    // Filter for agents only
    if (this.isAgentLoggedUser) {
      this.airlineDropdownOptions$ = this.airlineDictionary.getDropdownOptions(firstBspId);
    }
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<CreditCardDocumentFilter>({
      creditCardNumber: [],
      bsp: [],
      periodRange: [],
      monthPeriod: [],
      airline: [],
      agent: [],
      currency: []
    });
  }

  public onViewDocument(document: CreditCardDocument): void {
    this.router.navigate([ROUTES.DOCUMENT_ENQUIRY.url, document.documentNumber]);
  }

  public onDownload(): void {
    const requestQuery = this.creditCardDocumentsService.storedQuery;

    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: requestQuery
      },
      apiService: this.creditCardDocumentsService
    });
  }

  // TODO FCA-10608: review columns
  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: 'document.creditCard.columns.bsp',
        prop: 'bsp.isoCountryCode',
        flexGrow: 0.7,
        minWidth: 75
      },
      {
        name: 'document.creditCard.columns.documentNumber',
        prop: 'documentNumber',
        cellTemplate: 'commonLinkFromObjCellTmpl',
        sortable: false,
        width: 90
      },
      {
        name: 'document.creditCard.columns.airlineCode',
        prop: 'airline.iataCode',
        width: 70,

        hidden: this.loggedUserIsOfType(UserType.AIRLINE)
      },
      {
        name: 'document.creditCard.columns.agentCode',
        prop: 'agent.iataCode',
        width: 70,
        hidden: this.loggedUserIsOfType(UserType.AGENT)
      },
      {
        name: 'document.creditCard.columns.period',
        prop: 'period',
        width: 80
      },
      {
        name: 'document.creditCard.columns.currency',
        prop: 'currency.code',
        width: 70
      },
      {
        name: 'document.creditCard.columns.ticketAmount',
        prop: 'ticketAmount',
        cellTemplate: 'amountCellTmpl',
        cellClass: 'text-right',
        sortable: false,
        width: 80
      },
      {
        name: 'document.creditCard.columns.formOfPaymentType',
        prop: 'formOfPayment.formOfPaymentType',
        sortable: false,
        width: 70
      },
      {
        name: 'document.creditCard.columns.formOfPaymentAmount',
        prop: 'formOfPayment.formOfPaymentAmount',
        cellTemplate: 'amountCellTmpl',
        cellClass: 'text-right',
        sortable: false,
        width: 80
      },
      {
        name: 'document.creditCard.columns.approvalCode',
        prop: 'formOfPayment.approvalCode',
        sortable: false,
        width: 80
      },
      {
        name: 'document.creditCard.columns.invoiceDate',
        prop: 'formOfPayment.invoiceDate',
        sortable: false,
        width: 80
      },
      {
        name: 'document.creditCard.columns.invoiceNumber',
        prop: 'formOfPayment.invoiceNumber',
        sortable: false,
        width: 80
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
