import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CreditCardDocumentListComponent } from './credit-card-document-list/credit-card-document-list.component';
import { CreditCardDocumentsRoutingModule } from './credit-card-documents-routing.module';
import { CreditCardDocumentFilterFormatter } from './services/credit-card-document-filter-formatter';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, CreditCardDocumentsRoutingModule],
  declarations: [CreditCardDocumentListComponent],
  providers: [CreditCardDocumentFilterFormatter]
})
export class CreditCardDocumentsModule {}
