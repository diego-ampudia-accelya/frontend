import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CreditCardDocumentListComponent } from './credit-card-document-list/credit-card-document-list.component';
import { ROUTES } from '~app/shared/constants/routes';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: CreditCardDocumentListComponent,
        data: {
          tab: ROUTES.CREDIT_CARD_DOCUMENTS
        }
      }
    ])
  ]
})
export class CreditCardDocumentsRoutingModule {}
