import { IssueAction } from '../models/issue-action.model';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { UserType } from '~app/shared/models/user.model';

export class IssueActions {
  public static get ACM(): IssueAction {
    return {
      label: 'document.ticket.issueButton.issueAcm',
      redirectUrl: ROUTES.ACM_ISSUE.url,
      permissionKey: Permissions.issueActionAcm,
      allowedUserTypes: [UserType.AIRLINE]
    };
  }

  public static get ADM(): IssueAction {
    return {
      label: 'document.ticket.issueButton.issueAdm',
      redirectUrl: ROUTES.ADM_ISSUE.url,
      permissionKey: Permissions.issueActionAdm,
      allowedUserTypes: [UserType.AIRLINE]
    };
  }

  public static get RA(): IssueAction {
    return {
      label: 'document.ticket.issueButton.issueRa',
      redirectUrl: ROUTES.REFUNDS_APP_ISSUE_TICKET.url,
      permissionKey: Permissions.issueActionRefundApplication,
      allowedUserTypes: [UserType.AGENT, UserType.AGENT_GROUP]
    };
  }

  public static get RN(): IssueAction {
    return {
      label: 'document.ticket.issueButton.issueRn',
      redirectUrl: ROUTES.REFUNDS_NOTICE_ISSUE_TICKET.url,
      permissionKey: Permissions.issueActionRefundNotice,
      allowedUserTypes: [UserType.AIRLINE, UserType.AGENT, UserType.AGENT_GROUP]
    };
  }
}
