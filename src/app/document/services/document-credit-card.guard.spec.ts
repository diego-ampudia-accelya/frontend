import { discardPeriodicTasks, fakeAsync, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { CreditCardDialogComponent } from '../components/credit-card-dialog/credit-card-dialog.component';
import { DocumentCreditCardGuard } from './document-credit-card.guard';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';

const initialState = {
  core: {
    menu: {
      tabs: {
        tabId: {}
      },
      activeTabId: 'tabId'
    }
  }
};

const dialogConfig: DialogConfig = {
  data: {
    title: null,
    isSearching: false,
    mainButtonType: 'search',
    footerButtonsType: [
      { type: FooterButton.Search, isDisabled: true, tooltipText: 'document.creditCardDialog.tooltipMessage' }
    ]
  }
};

const currentNavigationMock = {
  extras: { state: { creditCardNumber: '1234567890123456' } }
} as any;

describe('DocumentCreditCardGuard', () => {
  let guard: DocumentCreditCardGuard;

  const routerSpy = createSpyObject(Router, {
    getCurrentNavigation: () => currentNavigationMock
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DocumentCreditCardGuard,
        {
          provide: DialogService,
          useValue: {
            open: () => {}
          }
        },
        { provide: Router, useValue: routerSpy },
        mockProvider(L10nTranslationService),
        provideMockStore({ initialState })
      ]
    });

    guard = TestBed.inject(DocumentCreditCardGuard);
  });

  it('should create', () => {
    expect(guard).toBeDefined();
  });

  //TODO(PPavlov): Rewrite the whole test, for now disable
  xit('should open the dialog', fakeAsync(() => {
    guard['dialogService'] = createSpyObject(DialogService);
    (guard['dialogService'] as SpyObject<DialogService>).open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));

    guard.canActivate().subscribe();

    expect(guard['dialogService'].open).toHaveBeenCalledWith(CreditCardDialogComponent, dialogConfig);

    discardPeriodicTasks();
  }));

  it('should navigate to the correct url and correct state', fakeAsync(() => {
    guard['dialogService'] = createSpyObject(DialogService);
    (guard['dialogService'] as SpyObject<DialogService>).open.and.returnValue(
      of({
        clickedBtn: FooterButton.Search,
        contentComponentRef: { form: { controls: { creditCardNumber: { value: '1234567890123456' } } } }
      })
    );

    guard.canActivate().subscribe();

    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith(`${ROUTES.CREDIT_CARD_DOCUMENTS.url}/list`, {
      state: { creditCardNumber: '1234567890123456' }
    });
  }));
});
