import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

@Injectable()
export class DocumentNotFoundResolver implements Resolve<string> {
  resolve(route: ActivatedRouteSnapshot): string {
    return route.params.documentNumber;
  }
}
