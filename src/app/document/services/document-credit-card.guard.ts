import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { CreditCardDialogComponent, EOption } from '../components/credit-card-dialog/credit-card-dialog.component';
import { CreditCardDialogData } from '../models/credit-card-dialog-data.model';
import { getActiveTab } from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ROUTES } from '~app/shared/constants/routes';

@Injectable()
export class DocumentCreditCardGuard implements CanActivate {
  private activeTab$ = this.store.pipe(select(getActiveTab));
  constructor(
    private dialogService: DialogService,
    private router: Router,
    private translationService: L10nTranslationService,
    private store: Store<AppState>
  ) {}

  canActivate(): Observable<boolean> {
    const title = this.translationService.translate('document.creditCardDialog.title');

    return this.dialogService
      .open(CreditCardDialogComponent, {
        data: {
          title,
          isSearching: false,
          mainButtonType: FooterButton.Search,
          footerButtonsType: [
            {
              type: FooterButton.Search,
              isDisabled: false
            }
          ]
        } as CreditCardDialogData
      })
      .pipe(
        switchMap(searchAction => {
          if (searchAction.clickedBtn === FooterButton.Cancel) {
            return of(null);
          }

          if (searchAction.clickedBtn === FooterButton.Search) {
            const selected: EOption = searchAction.contentComponentRef.selected;

            if (selected === EOption.DocumentsQuery) {
              this.router.navigateByUrl(`${ROUTES.CREDIT_CARD_DOCUMENTS.url}/query`);
            } else {
              const { creditCardNumber } = searchAction.contentComponentRef.form.controls;

              return of(creditCardNumber.value);
            }
          }

          return EMPTY;
        }),
        first(Boolean),
        withLatestFrom(this.activeTab$),
        tap(([creditCard, activeTab]) => {
          if (creditCard) {
            const stateCreditCard = { creditCardNumber: creditCard };
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigateByUrl(`${ROUTES.CREDIT_CARD_DOCUMENTS.url}/list`, { state: stateCreditCard });
          } else {
            this.router.navigateByUrl(activeTab.url || ROUTES.DASHBOARD.url);
          }
        }),
        mapTo(false),
        finalize(() => {
          this.dialogService.close();
        })
      );
  }
}
