import {
  NfeDocumentType,
  NfeRelatedDocument,
  NfeStatusType,
  RelatedDocument,
  RelatedDocumentCategory
} from '../models';

import { fromNfeRelatedDocument } from './document-converters';

describe('document-converters', () => {
  describe('fromNfeRelatedDocument()', () => {
    it('should convert to RelatedDocument properly', () => {
      const expected: RelatedDocument = {
        documentId: '0',
        documentNumber: '6990810009',
        type: 'ADMA',
        category: RelatedDocumentCategory.Nfe,
        airline: { code: '081', name: 'QANTAS' },
        agent: { code: '0230032', name: 'AGENT TEST' },
        issueDate: '2020-05-08',
        currency: {
          id: 1,
          code: 'AUD',
          decimals: 2
        },
        billingPeriod: '2020054',
        amountPaid: 30,
        status: 'PENDING'
      };
      const nfeDocument: NfeRelatedDocument = {
        id: 0,
        documentNumber: '6990810009',
        documentType: NfeDocumentType.ADMA,
        agent: { id: 1, iataCode: '0230032', name: 'AGENT TEST' },
        airline: { id: 1, iataCode: '081', localName: 'QANTAS' },
        billingPeriod: { id: 1, period: '2020054' },
        issueDate: '2020-05-08',
        status: NfeStatusType.Pending,
        currency: {
          id: 1,
          code: 'AUD',
          decimals: 2
        },
        amountPaid: 30
      };

      const actual = fromNfeRelatedDocument(nfeDocument);

      expect(actual).toEqual(expected);
    });

    it('should handle null values properly', () => {
      const expected: RelatedDocument = {
        documentId: null,
        documentNumber: null,
        type: null,
        category: RelatedDocumentCategory.Nfe,
        airline: { code: null, name: null },
        agent: { code: null, name: null },
        issueDate: null,
        currency: null,
        billingPeriod: null,
        amountPaid: null,
        status: null
      };
      const nfeDocument: NfeRelatedDocument = {
        id: null,
        documentNumber: null,
        documentType: null,
        agent: null,
        airline: null,
        billingPeriod: null,
        issueDate: null,
        status: null,
        currency: null,
        amountPaid: null
      };

      const actual = fromNfeRelatedDocument(nfeDocument);

      expect(actual).toEqual(expected);
    });
  });
});
