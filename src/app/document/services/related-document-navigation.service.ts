import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { NfeDocumentType, RelatedDocument, RelatedDocumentCategory } from '~app/document/models';
import { ROUTES } from '~app/shared/constants/routes';

const documentPath = ROUTES.DOCUMENT_VIEW.url;
const nfeDocumentsUrlMap: { [key in NfeDocumentType]: string } = {
  [NfeDocumentType.ADMA]: ROUTES.ADM_VIEW.url,
  [NfeDocumentType.ACMA]: ROUTES.ACM_VIEW.url,
  [NfeDocumentType.ADMQ]: ROUTES.ADM_REQUEST_VIEW.url,
  [NfeDocumentType.ACMQ]: ROUTES.ACM_REQUEST_VIEW.url,
  [NfeDocumentType.SPDR]: ROUTES.SPDR_VIEW.url,
  [NfeDocumentType.SPCR]: ROUTES.SPCR_VIEW.url,
  [NfeDocumentType.ADMD]: ROUTES.ADMD_VIEW.url,
  [NfeDocumentType.ACMD]: ROUTES.ACMD_VIEW.url,
  [NfeDocumentType.RA]: ROUTES.REFUNDS_APP_VIEW.url,
  [NfeDocumentType.RN]: ROUTES.REFUNDS_NOTICE_VIEW.url
};

@Injectable({ providedIn: 'root' })
export class RelatedDocumentNavigationService {
  private urlMapPerCategory = {
    [RelatedDocumentCategory.Nfe]: { ...nfeDocumentsUrlMap },
    [RelatedDocumentCategory.Billed]: {
      TKTT: documentPath,
      ACM: documentPath,
      ACMA: documentPath,
      ADM: documentPath,
      ADMA: documentPath,
      RFND: documentPath,
      SPDR: documentPath,
      ADMD: documentPath,
      ADNT: documentPath
    }
  };

  constructor(private router: Router) {}

  public open(document: RelatedDocument): Promise<boolean> {
    const urlMap = this.urlMapPerCategory[document.category] || {};
    const baseUrl = urlMap[document.type];
    const { documentId } = document;

    if (!baseUrl) {
      throw new Error(`Failed to open document of unknown type ${document.type}`);
    }

    return this.router.navigate([baseUrl, documentId]);
  }
}
