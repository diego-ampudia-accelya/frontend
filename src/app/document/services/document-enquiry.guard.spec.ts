import { fakeAsync, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { EnquiryDialogComponent } from '../components/enquiry-dialog/enquiry-dialog.component';
import { DocumentSummary } from '../models';

import { DocumentEnquiryGuard } from './document-enquiry.guard';
import { DocumentService } from './document.service';
import { ROUTES } from '~app/shared/constants/routes';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

const initialState = {
  core: {
    menu: {
      tabs: {
        tabId: {}
      },
      activeTabId: 'tabId'
    }
  }
};

const dialogConfig: DialogConfig = {
  data: {
    title: 'document.enquiryDialog.title',
    documents: [],
    isSearching: false,
    initialDocumentNumber: '',
    mainButtonType: 'search',
    footerButtonsType: [
      { type: FooterButton.Search, isDisabled: true, tooltipText: 'document.enquiryDialog.tooltipMessage' }
    ]
  }
};

describe('DocumentEnquiryGuard', () => {
  let guard: DocumentEnquiryGuard;
  let dialogServiceSpy: SpyObject<DialogService>;
  let routerSpy: SpyObject<Router>;
  let documentServiceSpy: SpyObject<DocumentService>;

  beforeEach(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    routerSpy = createSpyObject(Router);
    documentServiceSpy = createSpyObject(DocumentService);

    dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));

    TestBed.configureTestingModule({
      providers: [
        DocumentEnquiryGuard,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy },
        mockProvider(L10nTranslationService),
        { provide: DocumentService, useValue: documentServiceSpy },
        provideMockStore({ initialState })
      ]
    });

    guard = TestBed.inject(DocumentEnquiryGuard);
  });

  it('should create', () => {
    expect(guard).toBeDefined();
  });

  it('should open the dialog when there is no initial document number', fakeAsync(() => {
    guard.canActivate({ params: { documentNumber: null } } as any).subscribe();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(EnquiryDialogComponent, dialogConfig);
  }));

  it('should NOT open the dialog when there is a document number and a single result', fakeAsync(() => {
    documentServiceSpy.getBy.and.returnValue(of([{ id: '123' }]));

    guard.canActivate({ params: { documentNumber: '123' } } as any).subscribe();

    expect(dialogServiceSpy.open).not.toHaveBeenCalled();
  }));

  it('should close the dialog', fakeAsync(() => {
    documentServiceSpy.getBy.and.returnValue(of([{ id: '123' }]));

    guard.canActivate({ params: { documentNumber: '123' } } as any).subscribe();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
  }));

  it('should navigate when a single document is found', fakeAsync(() => {
    documentServiceSpy.getBy.and.returnValue(of([{ id: '123' }]));

    guard.canActivate({ params: { documentNumber: '123' } } as any).subscribe();

    expect(routerSpy.navigate).toHaveBeenCalledWith([ROUTES.DOCUMENT_VIEW.url, '123']);
  }));

  it('should open the dialog when there is a document number and a multiple results', fakeAsync(() => {
    const documents = [{ id: '123' }, { id: '1234' }] as DocumentSummary[];
    documentServiceSpy.getBy.and.returnValue(of(documents));

    guard.canActivate({ params: { documentNumber: '123' } } as any).subscribe();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(EnquiryDialogComponent, {
      ...dialogConfig,
      data: {
        ...dialogConfig.data,
        initialDocumentNumber: '123',
        documents
      }
    });
  }));
});
