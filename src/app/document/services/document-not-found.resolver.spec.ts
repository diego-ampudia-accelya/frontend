import { DocumentNotFoundResolver } from './document-not-found.resolver';

describe('DocumentNotFoundResolver', () => {
  let resolver: DocumentNotFoundResolver;

  beforeEach(() => {
    resolver = new DocumentNotFoundResolver();
  });

  it('should create', () => {
    expect(resolver).toBeDefined();
  });

  it('should get document number from route params', () => {
    const result = resolver.resolve({ params: { documentNumber: '1234567890' } } as any);

    expect(result).toBe('1234567890');
  });
});
