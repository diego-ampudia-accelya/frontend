import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of, throwError } from 'rxjs';

import { DocumentTransactionCode, FormOfPaymentType } from '../enums';
import relatedDocuments from '../mocks/related-documents.json';
import {
  DocumentBase,
  DocumentSummary,
  EasyPayInformation,
  ExchangeInformation,
  PaymentCardInformation,
  RelatedDocument
} from '../models';
import { MiscellaneousCreditInformation } from '../models/additional-information/miscellanious-credit.model';
import { FormOfPaymentSummary } from '../models/form-of-payment-summary.model';

import { DocumentService } from './document.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { ResponseErrorBE } from '~app/shared/models';
import { NotFoundError } from '~app/shared/errors';

describe('Document Service', () => {
  let httpClientStub: SpyObject<HttpClient>;
  let service: DocumentService;

  beforeEach(waitForAsync(() => {
    httpClientStub = createSpyObject(HttpClient);
    httpClientStub.get.and.returnValue(of([]));

    TestBed.configureTestingModule({
      providers: [DocumentService, { provide: HttpClient, useValue: httpClientStub }]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(DocumentService);
  });

  it('should initialize', () => {
    expect(service).toBeTruthy();
  });

  describe('getBy', () => {
    let document: DocumentSummary;

    beforeEach(() => {
      document = {
        id: '3684802151-11-c291cmNlLnppcA==',
        documentNumber: '3684802151',
        transactionCode: 'TKTT',
        dateOfIssue: '2019-01-01',
        agentCode: '0230023',
        airlineCode: '220',
        readableByUser: true
      };
    });

    it('should call document endpont to get document by number', fakeAsync(() => {
      const documentNumber = '100000000';
      httpClientStub.get.and.returnValue(of([document]));

      let documents: DocumentSummary[];
      service.getBy(documentNumber).subscribe(results => (documents = results));
      tick();

      expect(documents.length).toBe(1);
      expect(httpClientStub.get).toHaveBeenCalledWith(`${appConfiguration.baseApiPath}/document-enquiry/documents`, {
        params: { documentNumber }
      });
    }));

    it('should NOT throw an error when no matching documents are found', fakeAsync(() => {
      const documentNumber = '100000000';
      httpClientStub.get.and.returnValue(of([]));

      let wasErrorThrown = false;
      service.getBy(documentNumber).subscribe(
        () => null,
        () => (wasErrorThrown = true)
      );
      tick();

      expect(wasErrorThrown).toBe(false);
    }));

    it('should exclude specified documents from results', fakeAsync(() => {
      const documentNumber = '100000000';
      const documentToExclude = document.id;
      httpClientStub.get.and.returnValue(of([document]));

      let documents: DocumentSummary[];
      service.getBy(documentNumber, documentToExclude).subscribe(response => (documents = response));
      tick();

      expect(documents).toEqual([]);
    }));
  });

  describe('getRelatedDocuments', () => {
    it('should return related documents', fakeAsync(() => {
      const documentNumber = '3684809079';
      const bspId = 1;
      httpClientStub.get.and.returnValue(of(relatedDocuments));

      let documents: RelatedDocument[];
      service.getRelatedDocuments(documentNumber, bspId).subscribe(results => (documents = results));
      tick();

      expect(documents.length).toBeGreaterThan(0);
      expect(httpClientStub.get).toHaveBeenCalledWith(
        `${appConfiguration.baseApiPath}/globalsearch/related-documents`,
        {
          params: { documentNumber }
        }
      );
    }));

    it('should return empty list when no related documents are found', fakeAsync(() => {
      const documentNumber = '3684809079';
      const bspId = 1;
      httpClientStub.get.and.returnValue(throwError(new NotFoundError({ errorCode: 404 } as ResponseErrorBE)));

      let documents: RelatedDocument[];
      service.getRelatedDocuments(documentNumber, bspId).subscribe(results => (documents = results));
      tick();

      expect(documents.length).toBe(0);
    }));

    it('should throw error when the BE service returns an error response', fakeAsync(() => {
      const documentNumber = '3684809079';
      const bspId = 1;
      httpClientStub.get.and.returnValue(throwError(new HttpErrorResponse({ status: 500 })));

      let wasErrorThrown = false;
      service.getRelatedDocuments(documentNumber, bspId).subscribe(
        () => null,
        () => (wasErrorThrown = true)
      );
      tick();

      expect(wasErrorThrown).toBeTruthy();
    }));

    it('should call related issued documents without BSP if not specified', fakeAsync(() => {
      const documentNumber = '3684809079';
      httpClientStub.get.and.returnValue(of());

      service.getRelatedDocuments(documentNumber).subscribe();
      tick();

      expect(httpClientStub.get).toHaveBeenCalledWith(
        `${appConfiguration.baseApiPath}/documents/related-issued-documents`,
        {
          params: { documentNumber }
        }
      );
    }));
  });

  describe('getPaymentInformation', () => {
    const document: DocumentBase = {
      id: '854dsa',
      type: 'TICKET',
      airline: null,
      agent: null,
      gds: null,
      currencyType: 'EUR',
      isoCountryCode: 'AU',
      cashAmount: 0,
      easyPayAmount: 0,
      creditAmount: 0,
      numberOfDecimals: 2,
      transaction: null,
      documentIdentification: null,
      stdAmounts: null,
      auditCouponAgentCode: '',
      formOfPaymentSummary: null,
      formOfPayments: {
        totalPaymentAmount: 20,
        allPayments: []
      }
    };

    it('should return empty arrays for each group if there no payments', () => {
      const payments = service.getPaymentInformation(document);
      const expectedResult = {
        paymentCardInformation: [],
        easyPayInformation: [],
        exchangeInformation: []
      };

      expect(payments).toEqual(expectedResult);
    });

    it('should distribute allPayments by type', () => {
      const documentWithPayments: DocumentBase = {
        ...document,
        formOfPayments: {
          totalPaymentAmount: 20,
          allPayments: [
            {
              type: FormOfPaymentType.CC,
              amount: 2,
              description: 'dsa'
            },
            {
              type: FormOfPaymentType.EX,
              amount: 6,
              description: 'dsa'
            },
            {
              type: FormOfPaymentType.CC,
              amount: 1,
              description: 'dsa'
            },
            {
              type: FormOfPaymentType.EP,
              amount: 3,
              description: 'dsa'
            }
          ]
        }
      };

      const payments = service.getPaymentInformation(documentWithPayments);

      const expectedResult = {
        paymentCardInformation: [
          {
            type: FormOfPaymentType.CC,
            amount: 2,
            description: 'dsa'
          },
          {
            type: FormOfPaymentType.CC,
            amount: 1,
            description: 'dsa'
          }
        ] as PaymentCardInformation[],
        easyPayInformation: [
          {
            type: FormOfPaymentType.EP,
            amount: 3,
            description: 'dsa'
          }
        ] as EasyPayInformation[],
        exchangeInformation: [
          {
            type: FormOfPaymentType.EX,
            amount: 6,
            description: 'dsa'
          }
        ] as ExchangeInformation[]
      };

      expect(payments).toEqual(expectedResult);
    });

    it('should distribute allPayments by СС and MSCC types', () => {
      const documentWithPayments: DocumentBase = {
        ...document,
        documentIdentification: { transactionCode: DocumentTransactionCode.EMDA },
        formOfPayments: {
          totalPaymentAmount: 20,
          allPayments: [
            {
              type: FormOfPaymentType.CC,
              amount: 2,
              description: 'dsa'
            },
            {
              type: FormOfPaymentType.MSCC,
              amount: 10,
              description: 'dsa'
            },
            {
              type: FormOfPaymentType.CC,
              amount: 1,
              description: 'dsa'
            }
          ]
        }
      } as any;

      const payments = service.getPaymentInformation(documentWithPayments);

      const expectedResult = {
        paymentCardInformation: [
          {
            type: FormOfPaymentType.CC,
            amount: 2,
            description: 'dsa'
          },
          {
            type: FormOfPaymentType.CC,
            amount: 1,
            description: 'dsa'
          },
          {
            type: FormOfPaymentType.MSCC,
            amount: 10,
            description: 'dsa'
          }
        ] as (PaymentCardInformation | MiscellaneousCreditInformation)[],
        easyPayInformation: [],
        exchangeInformation: []
      };

      expect(payments).toEqual(expectedResult);
    });

    it('should distribute allPayments by MSCC type', () => {
      const documentWithPayments: DocumentBase = {
        ...document,
        documentIdentification: { transactionCode: DocumentTransactionCode.EMDA },
        formOfPayments: {
          totalPaymentAmount: 20,
          allPayments: [
            {
              type: FormOfPaymentType.MSCC,
              amount: 10,
              description: 'dsa'
            }
          ]
        }
      } as any;

      const payments = service.getPaymentInformation(documentWithPayments);

      const expectedResult = {
        paymentCardInformation: [
          {
            type: FormOfPaymentType.MSCC,
            amount: 10,
            description: 'dsa'
          }
        ] as MiscellaneousCreditInformation[],
        easyPayInformation: [],
        exchangeInformation: []
      };

      expect(payments).toEqual(expectedResult);
    });
  });

  describe('getFormOfPaymentSummary', () => {
    const document: DocumentBase = {
      id: '854dsa',
      type: 'TICKET',
      airline: null,
      agent: null,
      gds: null,
      currencyType: 'EUR',
      isoCountryCode: 'AU',
      cashAmount: 0,
      easyPayAmount: 0,
      creditAmount: 0,
      numberOfDecimals: 2,
      transaction: null,
      documentIdentification: null,
      stdAmounts: null,
      auditCouponAgentCode: '',
      formOfPaymentSummary: null,
      formOfPayments: {
        totalPaymentAmount: 20,
        allPayments: [
          {
            type: FormOfPaymentType.CC,
            amount: 2,
            entity: 'AX',
            maskedAccountNumber: '94145XXXXXX',
            description: 'dsa'
          },
          {
            type: FormOfPaymentType.EX,
            amount: 6,
            description: 'dsa'
          },
          {
            type: FormOfPaymentType.CC,
            amount: 1,
            description: 'dsa'
          },
          {
            type: FormOfPaymentType.EP,
            amount: 3,
            description: 'dsa'
          }
        ]
      }
    };

    it('should return a summary for formOfPayments', () => {
      httpClientStub.get.and.returnValue(of(document));
      const expectedResult = {
        type: FormOfPaymentType.CC,
        amount: 20,
        description: 'dsa',
        entity: 'CCAX',
        maskedAccountNumber: '94145XXXXXX',
        addInfo: undefined
      };

      service.getById('68548564dsasa').subscribe(d => {
        expect(d.formOfPaymentSummary).toEqual(expectedResult);
      });
    });

    it('should add addInfo to the summary for formOfPayments if first formOfPayment is MSCA', () => {
      const doc: DocumentBase = {
        ...cloneDeep(document),
        formOfPaymentAdditionalInformation: [
          {
            paymentInformation: 'Test'
          }
        ],
        formOfPayments: {
          totalPaymentAmount: 20,
          allPayments: [
            {
              type: FormOfPaymentType.MSCA,
              amount: 2,
              description: undefined
            }
          ]
        }
      };

      const expectedResult: FormOfPaymentSummary = {
        type: FormOfPaymentType.MSCA,
        amount: 20,
        description: undefined,
        entity: undefined,
        maskedAccountNumber: undefined,
        addInfo: 'Test'
      };

      httpClientStub.get.and.returnValue(of(cloneDeep(doc)));

      service.getById('68548564dsdds').subscribe(d => {
        expect(d.formOfPaymentSummary).toEqual(expectedResult);
      });
    });

    it('should add addInfo to the summary if first form of payment is CC and has entity GR', () => {
      const doc: DocumentBase = {
        ...cloneDeep(document),
        formOfPaymentAdditionalInformation: [
          {
            paymentInformation: 'Test'
          }
        ],
        formOfPayments: {
          totalPaymentAmount: 20,
          allPayments: [
            {
              type: FormOfPaymentType.CC,
              amount: 2,
              entity: 'GR',
              description: 'test',
              maskedAccountNumber: '94145XXXXXX'
            }
          ]
        }
      };
      httpClientStub.get.and.returnValue(of(cloneDeep(doc)));

      const expectedResult: FormOfPaymentSummary = {
        type: FormOfPaymentType.CC,
        amount: 20,
        description: 'test',
        entity: 'CCGR',
        maskedAccountNumber: '94145XXXXXX',
        addInfo: 'Test'
      };

      service.getById('68548564dwwq').subscribe(d => {
        expect(d.formOfPaymentSummary).toEqual(expectedResult);
      });
    });

    it('should add entity and maskedAccountNumber to the summary if first form of payment is MSCC', done => {
      const doc: DocumentBase = {
        ...cloneDeep(document),
        formOfPayments: {
          totalPaymentAmount: 3507.77,
          allPayments: [
            {
              type: FormOfPaymentType.MSCC,
              description: 'MISCELLANEOUS CREDIT',
              entity: 'VI',
              maskedAccountNumber: '490172XXXXXX2868',
              amount: 3507.77,
              approvalCode: '027757',
              invoiceNumber: '00126848'
            }
          ]
        }
      };
      httpClientStub.get.and.returnValue(of(cloneDeep(doc)));
      const expectedResult: FormOfPaymentSummary = {
        type: FormOfPaymentType.MSCC,
        amount: 3507.77,
        description: 'MISCELLANEOUS CREDIT',
        entity: 'MSCCVI',
        maskedAccountNumber: '490172XXXXXX2868',
        addInfo: undefined
      };

      service.getById('68548564dwwqq').subscribe(d => {
        expect(d.formOfPaymentSummary).toEqual(expectedResult);
        done();
      });
    });
  });
});
