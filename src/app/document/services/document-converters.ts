import { NfeRelatedDocument, RelatedDocument, RelatedDocumentCategory } from '../models';

export const fromNfeRelatedDocument = (document: NfeRelatedDocument): RelatedDocument => ({
  documentId: document.id != null ? document.id.toString() : null,
  documentNumber: document.documentNumber,
  type: document.documentType,
  category: RelatedDocumentCategory.Nfe,
  airline: {
    code: document.airline ? document.airline.iataCode : null,
    name: document.airline ? document.airline.localName : null
  },
  agent: {
    code: document.agent ? document.agent.iataCode : null,
    name: document.agent ? document.agent.name : null
  },
  issueDate: document.issueDate,
  currency: document.currency,
  billingPeriod: document.billingPeriod ? document.billingPeriod.period : null,
  amountPaid: document.amountPaid,
  status: document.status
});
