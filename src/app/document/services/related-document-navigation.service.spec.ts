import { Router } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';

import { NfeDocumentType, RelatedDocument, RelatedDocumentCategory } from '../models';

import { RelatedDocumentNavigationService } from './related-document-navigation.service';

describe('RelatedDocumentNavigationService', () => {
  let service: RelatedDocumentNavigationService;
  let routerStub: SpyObject<Router>;

  beforeEach(() => {
    routerStub = createSpyObject(Router);
    service = new RelatedDocumentNavigationService(routerStub);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('should open BSPLink (Nfe) type of documents', () => {
    let document: RelatedDocument;

    beforeEach(() => {
      document = {
        documentId: 'test-id',
        documentNumber: 'test-number',
        category: RelatedDocumentCategory.Nfe
      } as RelatedDocument;
    });

    it('for ADMA', async () => {
      document.type = NfeDocumentType.ADMA;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/acdms/adm/view', 'test-id']);
    });

    it('for ACMA', async () => {
      document.type = NfeDocumentType.ACMA;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/acdms/acm/view', 'test-id']);
    });

    it('for ADMQ', async () => {
      document.type = NfeDocumentType.ADMQ;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/acdms/adm-request/view', 'test-id']);
    });

    it('for ACMQ', async () => {
      document.type = NfeDocumentType.ACMQ;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/acdms/acm-request/view', 'test-id']);
    });

    it('for SPDR', async () => {
      document.type = NfeDocumentType.SPDR;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/bsp-adjustments/spdr/view', 'test-id']);
    });

    it('for SPCR', async () => {
      document.type = NfeDocumentType.SPCR;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/bsp-adjustments/spcr/view', 'test-id']);
    });

    it('for ADMD', async () => {
      document.type = NfeDocumentType.ADMD;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/bsp-adjustments/admd/view', 'test-id']);
    });

    it('for ACMD', async () => {
      document.type = NfeDocumentType.ACMD;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/bsp-adjustments/acmd/view', 'test-id']);
    });

    it('for RA', async () => {
      document.type = NfeDocumentType.RA;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/refunds/app/view', 'test-id']);
    });

    it('for RN', async () => {
      document.type = NfeDocumentType.RN;
      await service.open(document);

      expect(routerStub.navigate).toHaveBeenCalledWith(['/refunds/notice/view', 'test-id']);
    });
  });

  it('should open Billed documents', async () => {
    const document = {
      documentId: 'test-id',
      type: 'TKTT',
      category: RelatedDocumentCategory.Billed
    } as RelatedDocument;

    await service.open(document);

    expect(routerStub.navigate).toHaveBeenCalledWith(['/documents/view', 'test-id']);
  });

  it('should throw error when opening a document with unknown type', async () => {
    const document = {
      documentId: 'test-id',
      documentNumber: 'test-number',
      type: 'unknown',
      category: RelatedDocumentCategory.Nfe
    } as RelatedDocument;

    let hasError = false;
    try {
      await service.open(document);
    } catch {
      hasError = true;
    }

    expect(hasError).toBe(true);
  });
});
