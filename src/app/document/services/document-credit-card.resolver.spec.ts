import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';

import { CreditCardDocumentsService } from '../credit-card-documents/services/credit-card-documents.service';

import { DocumentCreditCardResolver } from './document-credit-card.resolver';

describe('Document Credit Card Resolver', () => {
  const currentNavigationMock = {
    extras: { state: { creditCardNumber: '1234567890123456' } }
  } as any;

  let documentCreditCardResolver: DocumentCreditCardResolver;

  const routerSpy = createSpyObject(Router, {
    getCurrentNavigation: () => currentNavigationMock
  });

  const creditCardDocumentsService = createSpyObject(CreditCardDocumentsService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DocumentCreditCardResolver,
        { provide: CreditCardDocumentsService, useValue: creditCardDocumentsService },
        { provide: Router, useValue: routerSpy }
      ]
    });

    documentCreditCardResolver = TestBed.inject(DocumentCreditCardResolver);
  });

  it('should create', () => {
    expect(documentCreditCardResolver).toBeTruthy();
  });

  it('should get creditcardnumber from router state and set it in creditCardDocumentsService', fakeAsync(() => {
    documentCreditCardResolver.resolve();
    tick();
    expect(creditCardDocumentsService.creditCardNumber).toBe('1234567890123456');
  }));
});
