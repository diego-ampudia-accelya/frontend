import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { Observable, of } from 'rxjs';

import { CreditCardDocumentsService } from '../credit-card-documents/services/credit-card-documents.service';
import { CreditCardDialogData } from '../models/credit-card-dialog-data.model';

@Injectable()
export class DocumentCreditCardResolver implements Resolve<CreditCardDialogData> {
  constructor(private router: Router, private creditCardDocumentsService: CreditCardDocumentsService) {}

  resolve(): Observable<CreditCardDialogData> {
    const creditCardNumber = this.router.getCurrentNavigation().extras.state?.creditCardNumber;

    if (creditCardNumber) {
      this.creditCardDocumentsService.creditCardNumber = creditCardNumber;
      const storedquery = cloneDeep(this.creditCardDocumentsService.storedQuery);
      if (storedquery) {
        this.creditCardDocumentsService.storedQuery = {
          ...storedquery,
          filterBy: { creditCardNumber }
        };
      }
    }

    return of(creditCardNumber);
  }
}
