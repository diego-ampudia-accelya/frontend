import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { catchError, first, mapTo, tap } from 'rxjs/operators';

import { DocumentService } from './document.service';
import { getActiveTab } from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';

@Injectable()
export class DocumentViewGuard implements CanActivate {
  private activeTab$ = this.store.pipe(select(getActiveTab));

  constructor(
    private router: Router,
    private documentService: DocumentService,
    private dialogService: DialogService,
    private store: Store<AppState>
  ) {}

  canActivate(activatedRoute: ActivatedRouteSnapshot): Observable<boolean> {
    const documentId = activatedRoute.params.documentId;

    return this.documentService.getById(documentId).pipe(
      mapTo(true),
      catchError(error => {
        // TODO: Remove when error log service is ready.
        // When we have for ex. missing key of object to the backend side we need to show where exactly is this problem.
        // eslint-disable-next-line no-console
        console.error(error);

        this.dialogService.openInfoDialog(
          'modalDialogs.documentNotFound.content',
          'modalDialogs.documentNotFound.title'
        );

        return this.activeTab$.pipe(
          first(),
          tap(activeTab => this.router.navigateByUrl(activeTab.url || ROUTES.DASHBOARD.url)),
          mapTo(false)
        );
      })
    );
  }
}
