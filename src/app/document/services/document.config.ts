export const documentConfig = Object.freeze({
  requiredMaxLength: 10,
  ticketNumberRegEpx: `^[0-9]{1,10}$`
});
