import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { DocumentBase } from '../models';

import { DocumentService } from './document.service';

@Injectable()
export class DocumentResolver implements Resolve<DocumentBase> {
  constructor(private documentService: DocumentService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<DocumentBase> {
    const { documentId } = route.params;

    return this.documentService.getById(documentId);
  }
}
