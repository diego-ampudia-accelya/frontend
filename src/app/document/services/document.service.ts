import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { flatten } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Dictionary, groupBy, isEmpty } from 'lodash';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';
import { NotFoundError } from '~app/shared/errors';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { DocumentTransactionCode, FormOfPaymentType } from '../enums';
import {
  DocumentBase,
  DocumentSummary,
  EasyPayInformation,
  ExchangeInformation,
  FormOfPayment,
  NetRemit,
  NfeRelatedDocument,
  PaymentCardInformation,
  RelatedDocument,
  RelatedDocumentCategory,
  Ticket
} from '../models';
import { MiscellaneousCreditInformation } from '../models/additional-information/miscellanious-credit.model';
import { MiscellaneousCreditPaymentInformation } from '../models/additional-information/payment-information.model';
import { EMD } from '../models/emd.model';
import { FormOfPaymentSummary } from '../models/form-of-payment-summary.model';
import { SummaryTicket } from '../models/summary-ticket.model';
import { fromNfeRelatedDocument } from './document-converters';

const NET_REMIT = 'NR';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  constructor(private http: HttpClient) {}

  @Cacheable()
  public getById(documentId: string): Observable<DocumentBase> {
    return this.http.get<DocumentBase>(`${appConfiguration.baseApiPath}/document-enquiry/documents/${documentId}`).pipe(
      map(document => {
        const hasConjunction = !isEmpty(document.conjunctionDocumentNumber);

        if (document.type === 'TICKET' && hasConjunction) {
          const ticket = document as Ticket;
          document = this.createConjunctionTicket(ticket);
        }

        if (document.type === 'EMD' && hasConjunction) {
          const emd = document as EMD;
          document = this.createConjunctionEMD(emd);
        }

        const numberOfDecimal = document.numberOfDecimals;
        const decimalPrecision = `1.${numberOfDecimal}-${numberOfDecimal}`;

        return { ...document, formOfPaymentSummary: this.getFormOfPaymentSummary(document), decimalPrecision };
      })
    );
  }

  public getBy(documentNumber: string, documentIdToExclude?: string): Observable<DocumentSummary[]> {
    return this.http
      .get<DocumentSummary[]>(`${appConfiguration.baseApiPath}/document-enquiry/documents`, {
        params: { documentNumber }
      })
      .pipe(
        map(documents =>
          // Returning unique documents
          documents.filter(document => document.id !== documentIdToExclude)
        )
      );
  }

  public getRelatedDocuments(
    documentNumber: string,
    bspId?: number,
    documentIdToExclude?: string
  ): Observable<RelatedDocument[]> {
    return forkJoin([
      this.getGlobalSearchRelatedDocuments(documentNumber),
      this.getNfeRelatedDocuments(documentNumber, bspId)
    ]).pipe(
      map(documents => flatten(documents)),
      map((relatedDocuments = []) => relatedDocuments.filter(document => document.documentId !== documentIdToExclude)),
      catchError((error: HttpErrorResponse) => {
        let result$: Observable<any> = throwError(error);

        if (error instanceof NotFoundError) {
          // If no results are found just return an empty list
          result$ = of([]);
        }

        return result$;
      })
    );
  }

  public getDocumentSummary(documentNumber: string): Observable<SummaryTicket> {
    return this.http
      .get(`${appConfiguration.baseApiPath}/document-enquiry/documents/${documentNumber}/summary`)
      .pipe(map((summary: SummaryTicket) => summary));
  }

  public getNetRemitInformation(document: DocumentBase): NetRemit[] {
    let netRemitList = [];
    if (document.transaction.netReportingIndicator === NET_REMIT && document.commissions) {
      netRemitList = document.commissions.map(commission => ({
        documentNumber: document.documentIdentification.documentNumber,
        deal: document.transaction.commercialAgreementReference,
        incentive: commission.supplementaryAmount
      }));
    }

    return netRemitList;
  }

  public getPaymentInformation(document: DocumentBase): MiscellaneousCreditPaymentInformation {
    if (document && document.formOfPayments) {
      const groupedPaymentType = groupBy(document.formOfPayments.allPayments, 'type');

      return {
        paymentCardInformation: this.getPaymentCardInformation(
          groupedPaymentType,
          document.documentIdentification?.transactionCode
        ),
        easyPayInformation: (groupedPaymentType.EP as EasyPayInformation[]) || [],
        exchangeInformation: (groupedPaymentType.EX as ExchangeInformation[]) || []
      };
    }

    return null;
  }

  private createConjunctionTicket(ticket: Ticket): Ticket {
    const conjunctionTicket = ticket.conjunctionTickets.find(
      cnj => cnj.documentIdentification.documentNumber === ticket.conjunctionDocumentNumber
    );

    return {
      ...ticket,
      agent: conjunctionTicket.agent,
      auditCouponAgentCode: conjunctionTicket.auditCouponAgentCode,
      documentIdentification: {
        ...ticket.documentIdentification,
        checkDigit: conjunctionTicket.documentIdentification.checkDigit,
        agentCode: conjunctionTicket.documentIdentification.agentCode,
        transactionCode: DocumentTransactionCode.TKTT_CNJ,
        pnrReference: conjunctionTicket.documentIdentification.pnrReference,
        tourCode: conjunctionTicket.documentIdentification.tourCode,
        originCityCodes: conjunctionTicket.documentIdentification.originCityCodes
      },
      fareCalculationArea: null,
      documentAmounts: {
        fareCalculationModeIndicator: ticket.documentAmounts.fareCalculationModeIndicator,
        fare: null,
        total: null,
        equivalentFarePaid: null,
        fareCalculationPricingIndicator: ticket.documentAmounts.fareCalculationPricingIndicator
      },
      commissionPercent: null,
      easyPayAmount: null,
      cashAmount: null,
      creditAmount: null
    };
  }

  private createConjunctionEMD(emd: EMD): EMD {
    const conjunctionEmd = emd.conjunctionTickets.find(
      cnj => cnj.documentIdentification.documentNumber === emd.conjunctionDocumentNumber
    );

    return {
      ...emd,
      auditCouponAgentCode: conjunctionEmd.auditCouponAgentCode,
      documentIdentification: {
        ...emd.documentIdentification,
        dateOfIssue: conjunctionEmd.documentIdentification.dateOfIssue,
        tourCode: conjunctionEmd.documentIdentification.tourCode,
        documentNumber: conjunctionEmd.documentIdentification.documentNumber,
        checkDigit: conjunctionEmd.documentIdentification.checkDigit,
        pnrReference: conjunctionEmd.documentIdentification.pnrReference,
        transactionCode: `${emd.documentIdentification.transactionCode} - CNJ`
      },
      itineraryData: conjunctionEmd.itineraryData,
      documentAmounts: {
        fareCalculationModeIndicator: emd.documentAmounts.fareCalculationModeIndicator,
        fare: null,
        total: null,
        equivalentFarePaid: null,
        fareCalculationPricingIndicator: emd.documentAmounts.fareCalculationPricingIndicator
      },
      emdCouponDetails: conjunctionEmd.emdCouponDetails,
      emdCouponRemarks: conjunctionEmd.emdCouponRemarks
    };
  }

  private getPaymentCardInformation(
    groupedPaymentType: Dictionary<FormOfPayment[]>,
    documentTransactionCode: string
  ): (PaymentCardInformation | MiscellaneousCreditInformation)[] {
    let res = [];

    if (groupedPaymentType.CC) {
      res = res.concat(groupedPaymentType.CC as PaymentCardInformation[]);
    }

    if (
      groupedPaymentType.MSCC &&
      (documentTransactionCode === DocumentTransactionCode.EMDA ||
        documentTransactionCode === DocumentTransactionCode.EMDS ||
        documentTransactionCode === DocumentTransactionCode.RFND ||
        documentTransactionCode === DocumentTransactionCode.TKTT)
    ) {
      res = res.concat(groupedPaymentType.MSCC as MiscellaneousCreditInformation[]);
    }

    if (groupedPaymentType.EP && documentTransactionCode === DocumentTransactionCode.TKTT) {
      res = res.concat(groupedPaymentType.EP as EasyPayInformation[]);
    }

    return res;
  }

  private getFormOfPaymentSummary(document: DocumentBase): FormOfPaymentSummary {
    if (document) {
      const firstFormOfPayment: FormOfPayment = document.formOfPayments.allPayments[0];
      const specificCreditCardName = 'CCGR';

      let entity: string;
      let creditCardNumber: string;
      let addInfo: string;

      if (firstFormOfPayment.type === FormOfPaymentType.CC || firstFormOfPayment.type === FormOfPaymentType.MSCC) {
        entity = firstFormOfPayment.type + firstFormOfPayment.entity;
        creditCardNumber = firstFormOfPayment.maskedAccountNumber;
      }

      if (firstFormOfPayment.type === FormOfPaymentType.EP) {
        entity = firstFormOfPayment.type + firstFormOfPayment.entity + firstFormOfPayment.easyPayCode;
        creditCardNumber = firstFormOfPayment.maskedAccountNumber;
      }

      const firstAdditionalFormOfPayment =
        document.formOfPaymentAdditionalInformation && document.formOfPaymentAdditionalInformation[0];

      const cashPaymentTypes = [FormOfPaymentType.CA, FormOfPaymentType.MSCA];
      const isCashPayment = cashPaymentTypes.includes(firstFormOfPayment.type);
      const hasSpecificCardName = entity === specificCreditCardName;

      const showAdditionalInfo = firstAdditionalFormOfPayment && (isCashPayment || hasSpecificCardName);

      if (showAdditionalInfo) {
        addInfo = firstAdditionalFormOfPayment.paymentInformation;
      }

      return {
        type: firstFormOfPayment.type,
        description: firstFormOfPayment.description,
        amount: document.formOfPayments.totalPaymentAmount,
        entity,
        maskedAccountNumber: creditCardNumber,
        addInfo
      };
    }

    return null;
  }

  private getGlobalSearchRelatedDocuments(documentNumber: string): Observable<RelatedDocument[]> {
    return this.http
      .get<RelatedDocument[]>(`${appConfiguration.baseApiPath}/globalsearch/related-documents`, {
        params: { documentNumber }
      })
      .pipe(
        map(documents =>
          // Filtering Nfe documents because we are fetching them from BSPlink. See getNfeRelatedDocuments().
          documents.filter(document => document.category !== RelatedDocumentCategory.Nfe)
        )
      );
  }

  private getNfeRelatedDocuments(documentNumber: string, bspId: number): Observable<RelatedDocument[]> {
    return this.http
      .get<NfeRelatedDocument[]>(`${appConfiguration.baseApiPath}/documents/related-issued-documents`, {
        params: {
          ...(bspId && { bspId: '' + bspId }),
          documentNumber
        }
      })
      .pipe(map((documents = []) => documents.map(fromNfeRelatedDocument)));
  }
}
