import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { defer, EMPTY, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { EnquiryDialogComponent } from '../components/enquiry-dialog/enquiry-dialog.component';
import { DocumentSummary } from '../models';
import { DocumentEnquiry } from '../models/document-enquiry.model';
import { EnquiryDialogData } from '../models/enquiry-dialog-data.model';
import { DocumentService } from './document.service';
import { getActiveTab } from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { DialogConfig } from '~app/shared/components';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ROUTES } from '~app/shared/constants/routes';
import { MenuTab } from '~app/shared/models/menu-tab.model';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Injectable()
export class DocumentEnquiryGuard implements CanActivate {
  private activeTab$ = this.store.pipe(select(getActiveTab));

  constructor(
    private dialogService: DialogService,
    private router: Router,
    private documentService: DocumentService,
    private store: Store<AppState>
  ) {}

  public canActivate(activatedRoute: ActivatedRouteSnapshot): Observable<boolean> {
    const initialDocumentNumber = activatedRoute.params.documentNumber;
    const documentToExclude = activatedRoute?.queryParams?.documentToExclude;

    return of(initialDocumentNumber).pipe(
      switchMap(initialNumber => {
        if (!initialNumber) {
          return this.openDialog$();
        }

        return this.documentService.getBy(initialNumber, documentToExclude).pipe(
          switchMap(documents => {
            // Initialized for `!documents.length` case
            let enquiry$: Observable<DocumentEnquiry> = of({
              documentId: null,
              documentNumber: initialNumber,
              foundDocument: false
            });

            if (documents.length === 1) {
              enquiry$ = of({ documentId: documents[0].id, foundDocument: true });
            } else if (documents.length > 1) {
              enquiry$ = this.openDialog$(initialNumber, documents);
            }

            return enquiry$;
          })
        );
      }),
      withLatestFrom(this.activeTab$),
      tap(([response, activeTab]) => this.handleNavigation(response, activeTab)),
      mapTo(false),
      finalize(() => this.dialogService.close())
    );
  }

  private buildDialogConfig(
    initialDocumentNumber: string,
    documents: DocumentSummary[]
  ): DialogConfig<EnquiryDialogData> {
    return {
      data: {
        title: 'document.enquiryDialog.title',
        documents,
        isSearching: false,
        initialDocumentNumber,
        mainButtonType: FooterButton.Search,
        footerButtonsType: [
          { type: FooterButton.Search, isDisabled: true, tooltipText: 'document.enquiryDialog.tooltipMessage' }
        ]
      } as EnquiryDialogData
    };
  }

  private openDialog$(initialDocumentNumber: string = '', documents: DocumentSummary[] = []): Observable<any> {
    const config = this.buildDialogConfig(initialDocumentNumber, documents);

    return this.dialogService.open(EnquiryDialogComponent, config).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) => {
        let enquiry$: Observable<DocumentEnquiry> = EMPTY;

        switch (clickedBtn) {
          case FooterButton.Search:
            enquiry$ = this.search$(config, contentComponentRef);
            break;

          case FooterButton.ViewDocument:
            enquiry$ = this.viewDocument$(config, contentComponentRef);
            break;

          case FooterButton.Cancel:
            enquiry$ = of(null);
            break;
        }

        return enquiry$;
      }),
      // If `true`, means that dialog should be kept open: multiple documents have been found or the document found is not readable by the user
      first(enquiry => !(!enquiry?.documentId && enquiry?.foundDocument))
    );
  }

  private search$(
    config: DialogConfig<EnquiryDialogData>,
    component: EnquiryDialogComponent
  ): Observable<DocumentEnquiry> {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      const { documentNumber } = component.form.value;

      return this.documentService.getBy(documentNumber).pipe(
        map(documents => {
          let enquiry: DocumentEnquiry = null;

          if (!documents.length) {
            enquiry = { documentId: null, documentNumber, foundDocument: false };
          } else if (documents.length === 1) {
            config.data.isDocumentNotReadable = !documents[0].readableByUser;
            enquiry = { documentId: !config.data.isDocumentNotReadable ? documents[0].id : null, foundDocument: true };
          } else if (documents.length > 1) {
            config.data.documents = documents;
            enquiry = { documentId: null, foundDocument: true };
          }

          return enquiry;
        }),
        finalize(() => setLoading(false))
      );
    });
  }

  private viewDocument$(
    config: DialogConfig<EnquiryDialogData>,
    component: EnquiryDialogComponent
  ): Observable<DocumentEnquiry> {
    const { selectedDocument } = component;

    if (!selectedDocument.readableByUser) {
      config.data.isDocumentNotReadable = true;
    }

    return of({ documentId: selectedDocument.readableByUser ? selectedDocument.id : null, foundDocument: true });
  }

  private handleNavigation(response: DocumentEnquiry, activeTab: MenuTab): void {
    if (response) {
      const { documentId, documentNumber, foundDocument } = response;

      if (foundDocument) {
        this.router.navigate([ROUTES.DOCUMENT_VIEW.url, documentId]);
      } else {
        this.router.navigate([ROUTES.DOCUMENT_VIEW_NOT_FOUND.url, documentNumber]);
      }
    } else {
      this.router.navigateByUrl(activeTab.url || ROUTES.DASHBOARD.url);
    }
  }
}
