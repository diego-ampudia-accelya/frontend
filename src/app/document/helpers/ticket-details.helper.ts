import { Ticket } from '../models';
import { ItineraryData } from '../models/itinerary-data.model';

const blankLine: ItineraryData = {
  stopoverCode: '',
  originAirportCode: '',
  destinationAirportCode: '',
  carrier: '',
  flightNumber: '',
  reservationBookingDesignator: '',
  flightDate: '',
  flightDepartureTime: '',
  flightBookingStatus: '',
  fareTicketDesignator: '',
  notValidBeforeDate: '',
  notValidAfterDate: '',
  baggageAllowance: ''
};

export const generateFlightData = (ticket: Ticket): ItineraryData[] => {
  const ticketData: ItineraryData[] = getTicketItineraryData(ticket);

  // numbers in getSegment() are index or index+1 of array
  return [
    getTranformedFlightDataForTheFirstLine(getSegment(ticketData, 1), ticket),
    getTranformedFlightDataForTwoToFourLines(getSegment(ticketData, 2), ticket, getSegment(ticketData, 1), 1),
    getTranformedFlightDataForTwoToFourLines(getSegment(ticketData, 3), ticket, getSegment(ticketData, 2), 2),
    getTranformedFlightDataForTwoToFourLines(getSegment(ticketData, 4), ticket, getSegment(ticketData, 3), 3),
    getTranformedFlightDataForTheLastLine(getSegment(ticketData, 5), ticket, getSegment(ticketData, 4))
  ].filter((itineraryData: ItineraryData | null) => itineraryData != null);
};

function getTranformedFlightDataForTheFirstLine(originalCurrentSegment: ItineraryData, ticket: Ticket): ItineraryData {
  if (originalCurrentSegment?.segmentIdentifier === 1) {
    // First normal line
    return {
      ...originalCurrentSegment,
      stopoverCode: '',
      destinationAirportCode: ''
    };
  }

  // First blank line
  let result: ItineraryData = {
    ...originalCurrentSegment,
    ...blankLine,
    // No matter if we update here origin or destination airport code
    originAirportCode: getFirstBlankLineAirportCode(ticket)
  };

  if (!isLastTicketLastSegment(ticket, 1)) {
    // First void line
    result = {
      ...result,
      fareTicketDesignator: 'VOID'
    };
  }

  return result;
}

function getTranformedFlightDataForTwoToFourLines(
  originalCurrentSegment: ItineraryData,
  ticket: Ticket,
  previousSegment: ItineraryData,
  index: number
): ItineraryData | null {
  if (originalCurrentSegment) {
    return {
      ...originalCurrentSegment,
      stopoverCode: previousSegment?.stopoverCode || '',
      originAirportCode: !previousSegment ? originalCurrentSegment.originAirportCode : '',
      destinationAirportCode: previousSegment?.destinationAirportCode || ''
    };
  }

  if (!isLastTicketLastSegment(ticket, index + 1)) {
    // Void since segment is not the last one in the series.
    return {
      ...originalCurrentSegment,
      ...blankLine,
      stopoverCode: previousSegment?.stopoverCode || '',
      destinationAirportCode: previousSegment?.destinationAirportCode || '',
      fareTicketDesignator: 'VOID'
    };
  }

  if (previousSegment) {
    // If !previousSegment, it will be filtered as blank line.
    return {
      ...originalCurrentSegment,
      ...blankLine,
      stopoverCode: previousSegment.stopoverCode,
      destinationAirportCode: previousSegment.destinationAirportCode
    };
  }

  // Blank line. It will be filtered.
  return null;
}

function getTranformedFlightDataForTheLastLine(
  originalCurrentSegment: ItineraryData,
  ticket: Ticket,
  previousSegment: ItineraryData
): ItineraryData | null {
  const lastLineAirportCode = getLastLineAirportCode(ticket, previousSegment, originalCurrentSegment);

  if (previousSegment || lastLineAirportCode) {
    return {
      ...originalCurrentSegment,
      ...blankLine,
      stopoverCode: previousSegment?.stopoverCode || '',
      destinationAirportCode: lastLineAirportCode
    };
  }

  // Blank line. It will be filtered.
  return null;
}

const isLastTicketLastSegment = (ticket: Ticket, segment: number): boolean => {
  const ticketDocumentNumber = getTicketDocumentNumber(ticket);

  if (ticket.conjunctionTickets?.length) {
    if (!ticketDocumentNumber) {
      return true;
    }

    const lastT = ticket.conjunctionTickets[ticket.conjunctionTickets.length - 1];
    if (!lastT.documentIdentification?.documentNumber) {
      return true;
    }
    if (lastT.documentIdentification.documentNumber !== ticketDocumentNumber) {
      return false;
    }
  }

  if (!getTicketItineraryData(ticket)) {
    return true;
  }

  return getTicketItineraryData(ticket).find(x => x.segmentIdentifier > segment) === undefined;
};

const getFirstBlankLineAirportCode = (ticket: Ticket): string => {
  let airportCode = blankLine.originAirportCode;

  if (ticket.conjunctionTickets?.length) {
    const index = ticket.conjunctionTickets.findIndex(
      conj =>
        conj.documentIdentification?.documentNumber &&
        conj.documentIdentification.documentNumber === getTicketDocumentNumber(ticket)
    );

    if (index >= 0) {
      const itineraryDataFrom =
        index === 0 ? getTicketItineraryData(ticket, true) : ticket.conjunctionTickets[index - 1].itineraryData;
      const segment = getSegment(itineraryDataFrom, 4);

      airportCode = segment.destinationAirportCode;
    }
  }

  return airportCode;
};

const getLastLineAirportCode = (
  ticket: Ticket,
  previousSegment: ItineraryData,
  currentSegment: ItineraryData
): string => {
  if (!currentSegment && !isLastTicketLastSegment(ticket, 5) && ticket.conjunctionTickets?.length) {
    const index: number = ticket.conjunctionTickets.findIndex(
      conj =>
        conj.documentIdentification?.documentNumber &&
        conj.documentIdentification.documentNumber === getTicketDocumentNumber(ticket)
    );

    const segment: ItineraryData = getSegment(ticket.conjunctionTickets[index + 1].itineraryData, 1);
    if (segment && index !== ticket.conjunctionTickets.length - 1) {
      return segment.originAirportCode;
    }
  }

  return previousSegment?.destinationAirportCode;
};

const getSegment = (itineraryData: ItineraryData[], segment: number): ItineraryData =>
  itineraryData?.find(data => data.segmentIdentifier === segment);

/** Retrieve document number from conjunction or main ticket depending on whether it is conjunction or not */
const getTicketDocumentNumber = (ticket: Ticket): string =>
  ticket.conjunctionDocumentNumber || ticket.documentIdentification?.documentNumber;

/**
 * Retrieve itinerary data from conjunction or main ticket depending on whether it is conjunction or not
 *
 * @remarks The purpose of `mainTicket` parameter is that sometimes we will need itinerary data from main ticket even if we are working with a conjunction ticket
 * @param ticket Ticket referred to
 * @param mainTicket Flag to indicate if we want to get the itinerary data of the main ticket if we are working with a conjunction ticket.
 */
const getTicketItineraryData = (ticket: Ticket, mainTicket = false): ItineraryData[] => {
  const conjunctionTicket = ticket.conjunctionTickets.find(
    cnj => cnj.documentIdentification.documentNumber === ticket.conjunctionDocumentNumber
  );

  return ticket.conjunctionDocumentNumber && !mainTicket ? conjunctionTicket.itineraryData : ticket.itineraryData;
};
