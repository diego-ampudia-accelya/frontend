import { SimpleTableColumn } from '~app/shared/components';

export function withHeadersBasedOnFields<T>(
  columns: SimpleTableColumn<T>[],
  baseHeaderKey: string
): SimpleTableColumn<T>[] {
  return columns.map(column => ({ ...column, header: `${baseHeaderKey}.${column.field}` }));
}
