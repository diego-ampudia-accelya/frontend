export function maskCreditCardHelper(creditCardNumber: string): string {
  const REPLACEMENT = 'X';
  const FIRST_CARACTERS_LENGTH = 6;
  const CREDIT_CARD_MIN_LENGTH_TO_MASK = 10;

  let maskCreditCard = creditCardNumber;

  if (creditCardNumber?.length > CREDIT_CARD_MIN_LENGTH_TO_MASK) {
    const caractersNumbersToMask = creditCardNumber.length - CREDIT_CARD_MIN_LENGTH_TO_MASK;
    const replacementCaracters = REPLACEMENT.repeat(caractersNumbersToMask);
    maskCreditCard = `${creditCardNumber.substring(
      0,
      FIRST_CARACTERS_LENGTH
    )}${replacementCaracters}${creditCardNumber.substring(FIRST_CARACTERS_LENGTH + caractersNumbersToMask)}`;
  }

  return maskCreditCard;
}
