import { ItineraryData } from '../models/itinerary-data.model';

import * as hepler from './ticket-details.helper';

const blankLine: ItineraryData = {
  stopoverCode: '',
  originAirportCode: '',
  destinationAirportCode: '',
  carrier: '',
  flightNumber: '',
  reservationBookingDesignator: '',
  flightDate: '',
  flightDepartureTime: '',
  flightBookingStatus: '',
  fareTicketDesignator: '',
  notValidBeforeDate: '',
  notValidAfterDate: '',
  baggageAllowance: ''
};

describe('generateFlightData', () => {
  it('should return result when all segments exist', () => {
    const paramTicket = {
      itineraryData: [
        {
          originAirportCode: 'originAirportCode0',
          segmentIdentifier: 0
        },
        {
          originAirportCode: 'originAirportCode1',
          segmentIdentifier: 1,
          stopoverCode: 'stopoverCode1',
          destinationAirportCode: 'destinationAirportCode1'
        },
        {
          originAirportCode: 'originAirportCode2',
          segmentIdentifier: 2,
          stopoverCode: 'stopoverCode2',
          destinationAirportCode: 'destinationAirportCode2'
        },
        {
          originAirportCode: 'originAirportCode3',
          segmentIdentifier: 3,
          stopoverCode: 'stopoverCode3',
          destinationAirportCode: 'destinationAirportCode3'
        },
        {
          originAirportCode: 'originAirportCode4',
          segmentIdentifier: 4,
          stopoverCode: 'stopoverCode4',
          destinationAirportCode: 'destinationAirportCode4'
        },
        {
          originAirportCode: 'originAirportCode5',
          segmentIdentifier: 5
        }
      ],
      conjunctionDocumentNumber: null,
      conjunctionTickets: []
    } as any;
    const expectedRes = [
      {
        originAirportCode: 'originAirportCode1',
        segmentIdentifier: 1,
        stopoverCode: '',
        destinationAirportCode: ''
      },
      {
        originAirportCode: '',
        segmentIdentifier: 2,
        stopoverCode: 'stopoverCode1',
        destinationAirportCode: 'destinationAirportCode1'
      },
      {
        originAirportCode: '',
        segmentIdentifier: 3,
        stopoverCode: 'stopoverCode2',
        destinationAirportCode: 'destinationAirportCode2'
      },
      {
        originAirportCode: '',
        segmentIdentifier: 4,
        stopoverCode: 'stopoverCode3',
        destinationAirportCode: 'destinationAirportCode3'
      },
      {
        originAirportCode: 'originAirportCode5',
        segmentIdentifier: 5,
        ...blankLine,
        stopoverCode: 'stopoverCode4',
        destinationAirportCode: 'destinationAirportCode4'
      }
    ] as any;

    const res = hepler.generateFlightData(paramTicket);

    expect(res).toEqual(expectedRes);
  });

  it('should return result when 1 and 3 segments are absent', () => {
    const paramTicket = {
      itineraryData: [
        {
          originAirportCode: 'originAirportCode0',
          segmentIdentifier: 0
        },
        {
          originAirportCode: 'originAirportCode2',
          segmentIdentifier: 2,
          stopoverCode: 'stopoverCode2',
          destinationAirportCode: 'destinationAirportCode2'
        },
        {
          originAirportCode: 'originAirportCode4',
          segmentIdentifier: 4,
          stopoverCode: 'stopoverCode4',
          destinationAirportCode: 'destinationAirportCode4'
        }
      ],
      conjunctionDocumentNumber: null,
      conjunctionTickets: []
    } as any;
    const expectedRes = [
      {
        ...blankLine,
        originAirportCode: blankLine.originAirportCode,
        fareTicketDesignator: 'VOID'
      },
      {
        segmentIdentifier: 2,
        stopoverCode: '',
        originAirportCode: 'originAirportCode2',
        destinationAirportCode: ''
      },
      {
        ...blankLine,
        stopoverCode: 'stopoverCode2',
        destinationAirportCode: 'destinationAirportCode2',
        fareTicketDesignator: 'VOID'
      },
      {
        segmentIdentifier: 4,
        stopoverCode: '',
        originAirportCode: 'originAirportCode4',
        destinationAirportCode: ''
      },
      {
        ...blankLine,
        stopoverCode: 'stopoverCode4',
        destinationAirportCode: 'destinationAirportCode4'
      }
    ] as any;

    const res = hepler.generateFlightData(paramTicket);

    expect(res).toEqual(expectedRes);
  });

  it('should return result when first is last', () => {
    const paramTicket = {
      itineraryData: [
        {
          originAirportCode: 'originAirportCode0',
          segmentIdentifier: 0
        }
      ],
      conjunctionDocumentNumber: null,
      conjunctionTickets: []
    } as any;
    const expectedRes = [
      {
        ...blankLine,
        originAirportCode: blankLine.originAirportCode
      }
    ] as any;

    const res = hepler.generateFlightData(paramTicket);

    expect(res).toEqual(expectedRes);
  });

  it('should return result when 2 and 4 segments are absent', () => {
    const paramTicket = {
      itineraryData: [
        {
          originAirportCode: 'originAirportCode0',
          segmentIdentifier: 0
        },
        {
          originAirportCode: 'originAirportCode1',
          segmentIdentifier: 1,
          stopoverCode: 'stopoverCode1',
          destinationAirportCode: 'destinationAirportCode1'
        },
        {
          originAirportCode: 'originAirportCode3',
          segmentIdentifier: 3,
          stopoverCode: 'stopoverCode3',
          destinationAirportCode: 'destinationAirportCode3'
        }
      ],
      conjunctionDocumentNumber: null,
      conjunctionTickets: []
    } as any;
    const expectedRes = [
      {
        originAirportCode: 'originAirportCode1',
        segmentIdentifier: 1,
        stopoverCode: '',
        destinationAirportCode: ''
      },
      {
        ...blankLine,
        stopoverCode: 'stopoverCode1',
        destinationAirportCode: 'destinationAirportCode1',
        fareTicketDesignator: 'VOID'
      },
      {
        segmentIdentifier: 3,
        stopoverCode: '',
        originAirportCode: 'originAirportCode3',
        destinationAirportCode: ''
      },
      {
        ...blankLine,
        originAirportCode: '',
        stopoverCode: 'stopoverCode3',
        destinationAirportCode: 'destinationAirportCode3'
      }
    ] as any;

    const res = hepler.generateFlightData(paramTicket);

    expect(res).toEqual(expectedRes);
  });

  it('should return result when 1,2,3,4 segments are absent', () => {
    const paramTicket = {
      itineraryData: [
        {
          originAirportCode: 'originAirportCode0',
          segmentIdentifier: 0
        },
        {
          originAirportCode: 'originAirportCode5',
          segmentIdentifier: 6
        }
      ],
      conjunctionDocumentNumber: '4',
      conjunctionTickets: [
        {
          documentIdentification: { documentNumber: '0' },
          itineraryData: [{ originAirportCode: 'originAirportCode0' }]
        },
        {
          documentIdentification: { documentNumber: '1' },
          itineraryData: [{ originAirportCode: 'originAirportCode1' }]
        },
        {
          documentIdentification: { documentNumber: '2' },
          itineraryData: [{ originAirportCode: 'originAirportCode2' }]
        },
        {
          documentIdentification: { documentNumber: '3' },
          itineraryData: [
            {
              originAirportCode: 'originAirportCode3',
              destinationAirportCode: 'destinationAirportCode3',
              segmentIdentifier: 4
            }
          ]
        },
        {
          documentIdentification: { documentNumber: '4' },
          itineraryData: [
            {
              originAirportCode: 'originAirportCode4',
              destinationAirportCode: 'destinationAirportCode4',
              segmentIdentifier: 6
            }
          ]
        },
        {
          documentIdentification: { documentNumber: '5' },
          itineraryData: [{ originAirportCode: 'originAirportCode5', segmentIdentifier: 1 }]
        },
        {
          documentIdentification: { documentNumber: '6' },
          itineraryData: [{ originAirportCode: 'originAirportCode6' }]
        }
      ]
    } as any;
    const expectedRes = [
      {
        ...blankLine,
        originAirportCode: 'destinationAirportCode3',
        fareTicketDesignator: 'VOID'
      },
      {
        ...blankLine,
        stopoverCode: '',
        destinationAirportCode: '',
        fareTicketDesignator: 'VOID'
      },
      {
        ...blankLine,
        stopoverCode: '',
        destinationAirportCode: '',
        fareTicketDesignator: 'VOID'
      },
      {
        ...blankLine,
        stopoverCode: '',
        destinationAirportCode: '',
        fareTicketDesignator: 'VOID'
      },
      {
        ...blankLine,
        stopoverCode: '',
        destinationAirportCode: 'originAirportCode5'
      }
    ] as any;

    const res = hepler.generateFlightData(paramTicket);

    expect(res).toEqual(expectedRes);
  });
});
