import { UserType } from '~app/shared/models/user.model';

export interface IssueAction {
  label: string;
  redirectUrl: string;
  permissionKey: string;
  allowedUserTypes?: UserType[];
}
