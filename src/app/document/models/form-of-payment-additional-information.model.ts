export interface FormOfPaymentAdditionalInformation {
  paymentSequenceNumber?: number;
  paymentInformation: string;
}
