export enum ErrorLevel {
  Rejection = 'R',
  Modified = 'M',
  Warning = 'W'
}
