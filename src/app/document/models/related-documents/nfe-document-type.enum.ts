export enum NfeDocumentType {
  ADMA = 'ADMA',
  ACMA = 'ACMA',
  ADMQ = 'ADM Request',
  ACMQ = 'ACM Request',
  SPDR = 'SPDR',
  SPCR = 'SPCR',
  ADMD = 'ADMD',
  ACMD = 'ACMD',
  RA = 'RA',
  RN = 'RN'
}
