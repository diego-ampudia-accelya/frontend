export enum NfeStatusType {
  Pending = 'PENDING',
  Disputed = 'DISPUTED',
  DisputeApproved = 'DISPUTE_APPROVED',
  DisputeRejected = 'DISPUTE_REJECTED',
  Deactivated = 'DEACTIVATED',
  Deleted = 'DELETED',
  PendingRequest = 'PENDING_REQUEST',
  Authorized = 'AUTHORIZED',
  Rejected = 'REJECTED',
  AuthorizedRequest = 'AUTHORIZED_REQUEST',
  RejectedRequest = 'REJECTED_REQUEST',
  BillingDisputed = 'BILLING_DISPUTED',
  PendingAuthorization = 'PENDING_AUTHORIZATION',
  Issued = 'ISSUED',
  UnderInvestigation = 'UNDER_INVESTIGATION',
  SentToAirline = 'SENT_TO_AIRLINE'
}
