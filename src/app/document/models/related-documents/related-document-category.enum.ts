export enum RelatedDocumentCategory {
  Nfe = 'Nfe',
  Billed = 'Billed',
  Rejected = 'Rejected'
}
