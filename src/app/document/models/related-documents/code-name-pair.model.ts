export interface CodeNamePair {
  code: string;
  name: string;
}
