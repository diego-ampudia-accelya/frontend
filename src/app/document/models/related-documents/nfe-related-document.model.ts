import { NfeDocumentType } from './nfe-document-type.enum';
import { NfeStatusType } from './nfe-status-type.enum';
import { Currency } from '~app/shared/models/currency.model';

export interface NfeRelatedDocument {
  id: number;
  documentNumber: string;
  documentType: NfeDocumentType;
  agent: {
    id: number;
    iataCode: string;
    name: string;
  };
  airline: {
    id: number;
    iataCode: string;
    localName: string;
  };
  billingPeriod: {
    id: number;
    period: string;
  };
  issueDate: string;
  status: NfeStatusType;
  currency: Currency;
  amountPaid: number;
}
