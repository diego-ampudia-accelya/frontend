import { CodeNamePair } from './code-name-pair.model';
import { ErrorLevel } from './error-level.enum';
import { RelatedDocumentCategory } from './related-document-category.enum';
import { Currency } from '~app/shared/models/currency.model';

export interface RelatedDocument {
  documentId: string;
  origin?: string;
  documentNumber: string;
  type: string;
  category: RelatedDocumentCategory;
  airline: CodeNamePair;
  agent: CodeNamePair;

  issueDate?: string;
  currency?: string | Currency;
  billingPeriod?: string;
  amountPaid?: number;
  gds?: CodeNamePair;
  status?: string;
  coupons?: string;
  errorLevel?: ErrorLevel;
  reasonForRejection?: string;
  rejectionDate?: string;
}
