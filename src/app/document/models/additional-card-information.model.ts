export interface AdditionalCardInformation {
  formOfPaymentType: string;
  formOfPaymentTransactionIdentifier: string;
}
