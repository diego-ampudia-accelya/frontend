import { GlobalAirline } from '~app/master-data/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Tax } from './tax.model';

export interface SummaryTicket extends DocumentTicketModel {
  agent: AgentTicketModel;
  airline: AirlineTicketModel;
  bsp: BspDto;
  conjunctionTickets?: Array<DocumentTicketModel>;
  tourCode?: string;
  currency: CurrencyTicketModel;
  customerFileReference?: string;
  dateOfIssue: string;
  formOfPayment: Array<FormPaymentTicketModel>;
  id: string;
  netReportingIndicator?: string;
  originalIssueDetails?: OriginalIssueTicket;
  passengerName: string;
  settlementAuthorisationCode?: string;
  statisticalCode: string;
  sumCommissionAmount: number;
  sumSupplementaryAmount: number;
  taxes: Array<Tax>;
  ticketAmount: number;
  totalPaymentAmount: number;
  transactionCode: string;
  vatGstAmount: number;
}

interface AgentTicketModel {
  id: number;
  iataCode: string;
  name: string;
  vatNumber: string;
}

export interface AirlineTicketModel {
  id: number;
  iataCode?: string;
  localName: string;
  vatNumber: string;
  bsp: BspDto;
  globalAirline: GlobalAirline;
}

interface CurrencyTicketModel {
  code: string;
  decimals: number;
  id: number;
  isDefault: boolean;
}

export interface FormPaymentTicketModel {
  amount: number;
  type: string;
  creditCardNumber?: string;
  creditCardEntity?: string;
  maskedCreditCardNumber?: string;
}

interface DocumentTicketModel {
  couponUseIndicator?: string;
  documentNumber: string;
  fullDocumentNumber: string;
  documentNumbers?: string[];
}

interface OriginalIssueTicket {
  originalIssueAirlineCode?: string;
  originalIssueAgentCode: string;
  originalIssueDocumentNumber: string;
  originalIssueDate: string;
  originalIssueCityCode: string;
}
