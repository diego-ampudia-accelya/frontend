import { EasyPayInformation, ExchangeInformation, NetRemit, PaymentCardInformation } from './additional-information';
import { DocumentBase } from './document-base.model';

export interface TASF extends DocumentBase {
  billingAnalysisPeriodCode: string;
  paymentCardInformation?: PaymentCardInformation[];
  exchangeInformation?: ExchangeInformation[];
  netRemit?: NetRemit[];
  easyPayInformation?: EasyPayInformation[];
}
