export interface PassengerInformation {
  name?: string;
  specificData?: string;
  dateOfBirth?: string;
  typeCode?: string;
}
