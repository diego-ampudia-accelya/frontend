export interface Commissions {
  statisticalCode?: string;
  commissionType?: string;
  commissionRate?: number;
  commissionAmount?: number;
  supplementaryType?: string;
  supplementaryRate?: number;
  supplementaryAmount?: number;
  effectiveCommissionRate: number;
  effectiveCommissionAmount: number;
  amountPaidByCustomer?: number;
  routingIndicator?: string;
}
