import { AdditionalCardInformation } from './additional-card-information.model';
import {
  ConjunctionTicket,
  EasyPayInformation,
  ExchangeInformation,
  NetRemit,
  PaymentCardInformation
} from './additional-information';
import { DocumentAmounts } from './document-amount.model';
import { DocumentBase } from './document-base.model';
import { IssueInformationForSales } from './issue-information-for-sales.model';
import { ItineraryData } from './itinerary-data.model';
import { PassengerInformation } from './passenger-information.model';
import { RemittanceArea } from './remittance-area.model';

export interface Ticket extends DocumentBase {
  billingAnalysisPeriodCode: string;
  originalIssue?: string[];
  issueInformationForSales?: Array<IssueInformationForSales>;
  itineraryData: Array<ItineraryData>;
  documentAmounts: DocumentAmounts;
  passengerInformation?: PassengerInformation;
  fareCalculationArea?: string;
  additionalCardInformation?: Array<AdditionalCardInformation>;
  commissionPercent?: number;
  remittanceArea: RemittanceArea;
  paymentCardInformation?: PaymentCardInformation[];
  easyPayInformation?: EasyPayInformation[];
  exchangeInformation?: ExchangeInformation[];
  netRemitInformation?: NetRemit[];
  conjunctionTickets?: ConjunctionTicket[];
  conjunctionTicketNumbers?: string[];
}
