import { DocumentSummary } from './document-summary.model';
import { FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';

export interface EnquiryDialogData {
  title: string;
  initialDocumentNumber: string;
  isSearching: boolean;
  isDocumentFound: boolean;
  isDocumentNotReadable: boolean;
  documents: DocumentSummary[];
  footerButtonsType: (string | ModalAction)[];
  buttons?: ModalAction[];
  mainButtonType: FooterButton;
}
