import { GlobalAirline } from '~app/master-data/models';

export interface DocumentAirline {
  id?: number;
  localName?: string;
  vatNumber?: string;
  globalAirline: Partial<GlobalAirline>;
}
