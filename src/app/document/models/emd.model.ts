import {
  ConjunctionTicket,
  EasyPayInformation,
  ExchangeInformation,
  NetRemit,
  PaymentCardInformation
} from './additional-information';
import { MiscellaneousCreditInformation } from './additional-information/miscellanious-credit.model';
import { DocumentAmounts } from './document-amount.model';
import { DocumentBase } from './document-base.model';
import { EmdCouponDetails } from './emd-coupon-details.model';
import { EmdCouponRemarks } from './emd-coupon-remarks.model';
import { IssueInformationForSales } from './issue-information-for-sales.model';
import { ItineraryData } from './itinerary-data.model';
import { PassengerInformation } from './passenger-information.model';

export interface EMD extends DocumentBase {
  issueInformationForSales: IssueInformationForSales[];
  itineraryData: Partial<ItineraryData>[];
  documentAmounts: DocumentAmounts;
  passengerInformation?: PassengerInformation;
  paymentCardInformation?: (PaymentCardInformation | MiscellaneousCreditInformation)[];
  easyPayInformation?: EasyPayInformation[];
  exchangeInformation?: ExchangeInformation[];
  conjunctionTickets?: ConjunctionTicket[];
  emdCouponDetails: EmdCouponDetails[];
  emdCouponRemarks: EmdCouponRemarks[];
  netRemitInformation?: NetRemit[];
  conjunctionTicketNumbers?: string[];
}
