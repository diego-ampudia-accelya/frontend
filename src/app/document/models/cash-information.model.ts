import { FormOfPaymentType } from '../enums';

import { FormOfPaymentBase } from './form-of-payment-base.model';

export interface CashInformation extends FormOfPaymentBase {
  type: FormOfPaymentType.CA;
  addInfo?: string;
}

export interface MiscellaneousCashInformation extends FormOfPaymentBase {
  type: FormOfPaymentType.MSCA;
}
