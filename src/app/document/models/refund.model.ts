import { PaymentCardInformation } from './additional-information';
import { DocumentBase } from './document-base.model';
import { RelatedDocumentInformation } from './related-document-information.model';

export interface Refund extends DocumentBase {
  captureDate: string;
  settlementPeriod: string;
  relatedDocumentInformation?: RelatedDocumentInformation[];
  paymentCardInformation?: PaymentCardInformation[];
}
