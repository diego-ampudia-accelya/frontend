export interface FormOfPaymentSummary {
  type: string;
  amount: number;
  description: string;
  entity: string;
  maskedAccountNumber: string;
  addInfo: string;
}
