import { AccountInformation } from './account-information.model';
import { EasyPayInformation, ExchangeInformation, PaymentCardInformation } from './additional-information';
import { MiscellaneousCreditInformation } from './additional-information/miscellanious-credit.model';
import { VoidInformation } from './additional-information/void.model';
import { CashInformation, MiscellaneousCashInformation } from './cash-information.model';
import { Commissions } from './commissions.model';
import { DocumentAgent } from './document-agent.model';
import { DocumentAirline } from './document-airline.model';
import { DocumentIdentification } from './document-identification.model';
import { FormOfPaymentAdditionalInformation } from './form-of-payment-additional-information.model';
import { FormOfPaymentSummary } from './form-of-payment-summary.model';
import { StdAmounts } from './std-amounts.model';
import { Transaction } from './transaction.model';
import { Bsp } from '~app/shared/models/bsp.model';
import { Gds } from '~app/master-data/models/gds.model';

export interface DocumentBase {
  id: string;
  type: string;
  airline: DocumentAirline;
  agent: DocumentAgent;
  bsp?: Partial<Bsp>;
  gds: Gds;
  isoCountryCode: string;
  currencyType: string;
  numberOfDecimals: number;
  auditCouponAgentCode: string;
  cashAmount: number;
  creditAmount: number;
  easyPayAmount: number;
  transaction: Transaction;
  documentIdentification: DocumentIdentification;
  stdAmounts: StdAmounts;
  formOfPayments: {
    totalPaymentAmount: number;
    allPayments: FormOfPayment[];
  };
  formOfPaymentAdditionalInformation?: Array<FormOfPaymentAdditionalInformation>;
  accountInformation?: AccountInformation;
  commissions?: Commissions[];
  formOfPaymentSummary?: FormOfPaymentSummary;
  decimalPrecision?: string;
  conjunctionDocumentNumber?: string;
}

export type FormOfPayment =
  | PaymentCardInformation
  | ExchangeInformation
  | EasyPayInformation
  | CashInformation
  | VoidInformation
  | MiscellaneousCashInformation
  | MiscellaneousCreditInformation;
