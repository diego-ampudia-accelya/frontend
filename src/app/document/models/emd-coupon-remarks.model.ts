export interface EmdCouponRemarks {
  couponNumber: number;
  remarks: string;
}
