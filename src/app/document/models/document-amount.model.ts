import { CurrencyAmount } from './currency-amount.model';

export interface DocumentAmounts {
  fare: CurrencyAmount;
  total: CurrencyAmount;
  fareCalculationModeIndicator: string;
  fareCalculationPricingIndicator: string;
  equivalentFarePaid?: CurrencyAmount;
  servicingAirlineProviderIdentifier?: string;
  bookingAgentIdentification?: string;
  bookingEntityOutletType?: string;
  ticketingModeIndicator?: string;
  airlineIssuingAgent?: string;
}
