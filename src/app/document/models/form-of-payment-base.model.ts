import { FormOfPaymentType } from '../enums';

export interface FormOfPaymentBase {
  type: FormOfPaymentType;
  description: string;
  amount: number;
}
