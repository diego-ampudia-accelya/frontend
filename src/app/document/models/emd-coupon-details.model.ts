export interface EmdCouponDetails {
  couponNumber: number;
  reasonForIssuanceCode: string;
  relatedDocument: string;
}
