export interface AccountInformation {
  vatGstAmount?: number;
  agentCommissionPercent?: number;
  agentCommissionAmount?: number;
  fareAdjustmentAmount?: number;
  cancellationFees?: number;
  cancellationFeesCommission?: number;
  onlineBillingStatementForAgent?: number;
  balancePayableAirline?: number;
  netFareAmount?: number;
}
