import { EasyPayInformation } from './easy-pay.model';
import { ExchangeInformation } from './exchange-info.model';
import { MiscellaneousCreditInformation } from './miscellanious-credit.model';
import { PaymentCardInformation } from './payment-card.model';

export interface CardPaymentInformation {
  paymentCardInformation: PaymentCardInformation[];
  easyPayInformation: EasyPayInformation[];
  exchangeInformation: ExchangeInformation[];
}

export type MiscellaneousCreditPaymentInformation = Omit<CardPaymentInformation, 'paymentCardInformation'> & {
  paymentCardInformation: (PaymentCardInformation | MiscellaneousCreditInformation)[];
};
