import { FormOfPaymentBase } from '../form-of-payment-base.model';
import { FormOfPaymentType } from '~app/document/enums';

export interface MiscellaneousCreditInformation extends FormOfPaymentBase {
  type: FormOfPaymentType.MSCC;
  entity?: string;
  maskedAccountNumber?: string;
  approvalCode?: string;
  expiryDate?: Date | string;
  invoiceNumber?: string;
}
