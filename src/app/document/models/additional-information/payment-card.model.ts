import { FormOfPaymentBase } from '../form-of-payment-base.model';
import { FormOfPaymentType } from '~app/document/enums';

export interface PaymentCardInformation extends FormOfPaymentBase {
  type: FormOfPaymentType.CC;
  entity?: string;
  maskedAccountNumber?: string;
  approvalCode?: string;
  expiryDate?: Date | string;
  invoiceNumber?: string;
}
