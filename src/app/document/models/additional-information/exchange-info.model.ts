import { FormOfPaymentBase } from '../form-of-payment-base.model';
import { FormOfPaymentType } from '~app/document/enums';

export interface ExchangeInformation extends FormOfPaymentBase {
  type: FormOfPaymentType.EX;
  documentNumber?: string;
  airlineCode?: string;
  coupons?: string;
  accountNumber?: string;
  maskedAccountNumber?: string;
}
