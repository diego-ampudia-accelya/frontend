import { FormOfPaymentBase } from '../form-of-payment-base.model';
import { FormOfPaymentType } from '~app/document/enums';

export interface EasyPayInformation extends FormOfPaymentBase {
  type: FormOfPaymentType.EP;
  entity?: string;
  easyPayCode?: string;
  maskedAccountNumber?: string;
}
