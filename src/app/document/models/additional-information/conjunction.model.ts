import { DocumentAgent } from '../document-agent.model';
import { DocumentIdentification } from '../document-identification.model';
import { EmdCouponDetails } from '../emd-coupon-details.model';
import { EmdCouponRemarks } from '../emd-coupon-remarks.model';
import { ItineraryData } from '../itinerary-data.model';

export interface ConjunctionTicket {
  documentIdentification?: DocumentIdentification;
  agent: DocumentAgent;
  auditCouponAgentCode: string;
  itineraryData?: Array<ItineraryData>;
  emdCouponDetails?: EmdCouponDetails[];
  emdCouponRemarks?: EmdCouponRemarks[];
}
