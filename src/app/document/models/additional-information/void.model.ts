import { FormOfPaymentBase } from '../form-of-payment-base.model';
import { FormOfPaymentType } from '~app/document/enums';

export interface VoidInformation extends FormOfPaymentBase {
  type: FormOfPaymentType.VD;
}
