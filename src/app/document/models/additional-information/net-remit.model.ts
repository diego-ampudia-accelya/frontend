export interface NetRemit {
  documentNumber: string;
  deal: string;
  incentive: number;
}
