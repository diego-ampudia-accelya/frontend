export interface Transaction {
  netReportingIndicator?: string;
  airlineCode: string;
  commercialAgreementReference?: string;
  customerFileReference?: string;
  reportingSystemIdentifier: string; // for now gds system
  dataInputStatusIndicator?: string;
}
