import { FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';

export interface CreditCardDialogData {
  title: string;
  creditCardNumber?: string;
  isSearching: boolean;
  footerButtonsType: (string | ModalAction)[];
  buttons?: ModalAction[];
  mainButtonType: FooterButton;
}
