export interface IssueInformationForSales {
  originalIssueDocumentNumber?: string;
  originalIssueCityCode?: string;
  originalIssueDate?: string;
  originalIssueDateString?: string;
  iataNumber: string;
  endorsementsRestrictions: string;
}
