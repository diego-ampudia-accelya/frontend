export interface DocumentEnquiry {
  documentId: string;
  documentNumber?: string;
  foundDocument: boolean;
}
