export interface DocumentAgent {
  id?: number;
  name?: string;
  iataCode: string;
  vatNumber?: string;
}
