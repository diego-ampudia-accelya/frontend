export interface Tax {
  amount: number;
  type?: string;
  currency?: string;
}
