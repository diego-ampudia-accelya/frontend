import { Tax } from './tax.model';

export interface StdAmounts {
  commissionableAmount?: number;
  ticketAmount: number;
  netFareAmount?: number;
  taxes: Array<Tax>;
  summarizedTaxes?: Tax[];
  totalTaxes: number;
}
