export interface DocumentIdentification {
  dateOfIssue: string;
  documentNumber: string;
  checkDigit: string;
  couponUseIndicator?: string;
  conjunctionTicketIndicator?: string;
  agentCode: string;
  tourCode?: string;
  transactionCode: string;
  originCityCodes?: string;
  pnrReference?: string;
  timeOfIssue?: string;
  reasonIssuanceCode?: string;
  fullDocumentNumber?: string;
}
