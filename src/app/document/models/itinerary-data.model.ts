export interface ItineraryData {
  segmentIdentifier?: number;
  stopoverCode?: string;
  notValidBeforeDate?: string;
  notValidAfterDate?: string;
  // For now only airport code
  originAirportCode: string;
  destinationAirportCode: string;
  carrier?: string;
  soldPassengerCabin?: string;
  flightNumber?: string;
  reservationBookingDesignator: string;
  flightDate?: string;
  flightDepartureTime?: string;
  flightBookingStatus?: string;
  baggageAllowance: string;
  fareTicketDesignator: string;
  frequentFlyerReference?: string;
  fareComponentPricedPassengerTypeCode?: string;
  equipmentCode?: string;
  gaugeIndicator?: string;
}
