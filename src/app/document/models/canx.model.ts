import { DocumentBase } from './document-base.model';

export interface CANX extends DocumentBase {
  captureDate: string;
  settlementPeriod: string;
  billingAnalysisPeriodCode?: string;
}
