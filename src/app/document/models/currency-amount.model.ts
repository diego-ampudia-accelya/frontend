export interface CurrencyAmount {
  currency?: string;
  amount?: string;
}
