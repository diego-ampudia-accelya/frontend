import { DocumentBase } from './document-base.model';
import { RelatedDocumentInformation } from './related-document-information.model';

export interface ACDM extends DocumentBase {
  captureDate: string;
  settlementPeriod: string;
  relatedDocumentInformation: RelatedDocumentInformation[];
}
