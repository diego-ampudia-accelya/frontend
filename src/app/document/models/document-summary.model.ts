export interface DocumentSummary {
  id: string;
  isoCountryCode?: string;
  documentNumber: string;
  transactionCode: string;
  dateOfIssue: string;
  agentCode: string;
  airlineCode: string;
  readableByUser: boolean;
}
