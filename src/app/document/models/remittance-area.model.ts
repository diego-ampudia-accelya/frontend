export interface RemittanceArea {
  sumCash: number;
  sumCredit: number;
  sumEasyPay: number;
  commissionPercent: number;
  sumTaxAmount: number;
}
