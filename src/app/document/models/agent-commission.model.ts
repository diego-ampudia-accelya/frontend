export interface AgentCommission {
  sumCommissionPercent: number;
  sumCommissionAmount: number;
}
