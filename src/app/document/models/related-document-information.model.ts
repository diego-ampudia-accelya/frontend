export interface RelatedDocumentInformation {
  remittancePeriodEndingDate?: string;
  waiverCode?: string;
  airlineCode?: string;
  relatedDocument?: string;
  memoIssuanceCode?: string;
  couponNumberIdentifier?: string;
  dateOfIssue?: string;
}
