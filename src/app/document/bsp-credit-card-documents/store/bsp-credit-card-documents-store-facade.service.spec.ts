import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/bsp-credit-card-documents.actions';
import * as fromSelectors from '../selectors/bsp-credit-card-documents.selectors';

import { CreditCardDocumentsStoreFacadeService } from './bsp-credit-card-documents-store-facade.service';
import { bspCreditCardDocumentsStateFeatureKey, initialState } from './bsp-credit-card-documents.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('CreditCardDocumentsStoreFacadeService', () => {
  let service: CreditCardDocumentsStoreFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CreditCardDocumentsStoreFacadeService,
        provideMockStore({
          initialState: {
            [bspCreditCardDocumentsStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(CreditCardDocumentsStoreFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('should when searchCreditCardDocuments is invoked dispatch a new searchCreditCardDocumentsRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchCreditCardDocuments(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchCreditCardDocumentsRequest({ payload: { query: defaultQuery } })
      );
    });
  });

  describe('selectors', () => {
    it('should when query$ is invoked select query', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectCreditCardDocumentsQuery);
    });

    it('should when data$ is invoke select data', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectCreditCardDocuments);
    });

    it('should when hasData$ is invoked select hasData', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('should when searchCreditCardDocuments is invoked select selectCreditCardDocumentsLoading', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchCreditCardDocuments; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectCreditCardDocumentsLoading);
    });
  });
});
