import { HttpErrorResponse } from '@angular/common/http';

import { CreditCardDocument, CreditCardDocumentFilters } from '../models/bsp-credit-card-documents.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const bspCreditCardDocumentsStateFeatureKey = 'bspCreditCardDocumentsStateFeatureKey';

export interface CreditCardDocumentsState {
  query: DataQuery<CreditCardDocumentFilters>;
  data: CreditCardDocument[];
  loading: {
    searchCreditCardDocuments: boolean;
  };
  error: {
    searchCreditCardDocuments: HttpErrorResponse | null;
  };
}

export const initialState: CreditCardDocumentsState = {
  query: defaultQuery,
  data: [],
  loading: {
    searchCreditCardDocuments: false
  },
  error: {
    searchCreditCardDocuments: null
  }
};
