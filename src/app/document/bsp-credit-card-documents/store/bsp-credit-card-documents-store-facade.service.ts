import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/bsp-credit-card-documents.actions';
import { CreditCardDocumentFilters } from '../models/bsp-credit-card-documents.models';
import * as fromSelectors from '../selectors/bsp-credit-card-documents.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class CreditCardDocumentsStoreFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchCreditCardDocuments: (query?: DataQuery<CreditCardDocumentFilters>) =>
        this.store.dispatch(fromActions.searchCreditCardDocumentsRequest({ payload: { query } })),
      updateFilterBy: (filterBy: Partial<CreditCardDocumentFilters>) =>
        this.store.dispatch(fromActions.updateCreditCardDocumentsFilterByQuery({ payload: { filterBy } })),
      downloadCreditCardDocuments: (detailed: boolean) =>
        this.store.dispatch(fromActions.downloadCreditCardDocumentsRequest({ payload: { detailed } }))
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectCreditCardDocumentsQuery),
      data$: this.store.select(fromSelectors.selectCreditCardDocuments),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser)
    };
  }

  public get loading() {
    return {
      searchCreditCardDocuments: this.store.select(fromSelectors.selectCreditCardDocumentsLoading)
    };
  }
}
