import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import { CreditCardDocument, CreditCardDocumentFilters } from '../models/bsp-credit-card-documents.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchCreditCardDocumentsRequest = createAction(
  '[Credit Card Documents]Search Credit Card Documents Request',
  props<{ payload: { query?: DataQuery<CreditCardDocumentFilters> } }>()
);

export const searchCreditCardDocumentsSuccess = createAction(
  '[Credit Card Documents]Search Credit Card Documents Success',
  props<{ payload: { pagedData: PagedData<CreditCardDocument> } }>()
);

export const searchCreditCardDocumentsFailure = createAction(
  '[Credit Card Documents]Search Credit Card Documents Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const updateCreditCardDocumentsFilterByQuery = createAction(
  '[Credit Card Documents]Update Credit Card Documents FilterBy Query',
  props<{ payload: { filterBy: Partial<CreditCardDocumentFilters> } }>()
);

export const downloadCreditCardDocumentsRequest = createAction(
  '[Credit Card Documents]Download  Credit Card Documents Request',
  props<{ payload: { detailed: boolean } }>()
);
