import { ComponentFixture, TestBed } from '@angular/core/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { CreditCardDocumentsFilterFormatterService } from '../../services/bsp-credit-card-documents-filter-formatter.service';
import { CreditCardDocumentsStoreFacadeService } from '../../store/bsp-credit-card-documents-store-facade.service';
import * as fromStub from '../../stubs/bsp-credit-card-documents.stubs';
import { CreditCardDocumentsListComponent } from './bsp-credit-card-documents-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { ListViewComponent } from '~app/shared/components/list-view';
import { AgentDictionaryService, AirlineDictionaryService, CurrencyDictionaryService } from '~app/shared/services';

describe('CreditCardDocumentsListComponent', () => {
  let component: CreditCardDocumentsListComponent;
  let fixture: ComponentFixture<CreditCardDocumentsListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule)],
      declarations: [
        CreditCardDocumentsListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: CreditCardDocumentsFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: CreditCardDocumentsStoreFacadeService,
          useValue: {
            actions: {
              searchCreditCardDocuments: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null),
              userBsps$: of(fromStub.userBsps),
              user$: of(fromStub.user)
            },
            loading: {
              searchCreditCardDocuments: of(null)
            }
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        },
        {
          provide: AirlineDictionaryService,
          useValue: {
            getDropdownOptions: () => of([])
          }
        },
        {
          provide: AgentDictionaryService,
          useValue: {
            getDropdownOptions: () => of([])
          }
        },
        {
          provide: CurrencyDictionaryService,
          useValue: {
            getDropdownOptions: () => of([])
          }
        },
        {
          provide: PeriodService,
          useValue: {
            getByBsp: () => of([])
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardDocumentsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
