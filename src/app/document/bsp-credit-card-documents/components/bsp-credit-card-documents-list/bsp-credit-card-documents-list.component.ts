import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { combineLatest, Observable, Subject, Subscription } from 'rxjs';
import { distinct, first, map, takeUntil } from 'rxjs/operators';

import {
  CREDIT_CARD_DOCUMENTS,
  LEAN_USERS_YEARS,
  NON_LEAN_USERS_YEARS
} from '../../constants/bsp-credit-card-documents.constants';
import {
  CreditCardDocument,
  CreditCardDocumentFilters,
  TransactionCode
} from '../../models/bsp-credit-card-documents.models';
import { CreditCardDocumentsFilterFormatterService } from '../../services/bsp-credit-card-documents-filter-formatter.service';
import { CreditCardDocumentsStoreFacadeService } from '../../store/bsp-credit-card-documents-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { PeriodOption, PeriodUtils } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { DocumentType } from '~app/shared/enums';
import { ArrayHelper, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService, CurrencyDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-credit-card-documents-list',
  templateUrl: './bsp-credit-card-documents-list.component.html'
})
export class CreditCardDocumentsListComponent implements OnInit, OnDestroy {
  public columns: TableColumn[] = CREDIT_CARD_DOCUMENTS.COLUMNS;
  public hasAllBspsPermission: boolean;
  public userBspDropdownOptions$: Observable<DropdownOption<BspDto>[]>;
  public userDefaultBsp$: Observable<Bsp>;

  public data$: Observable<CreditCardDocument[]> = this.creditCardDocumentsStoreFacadeService.selectors.data$;
  public query$: Observable<DataQuery<CreditCardDocumentFilters>> =
    this.creditCardDocumentsStoreFacadeService.selectors.query$;
  public loading$: Observable<boolean> = this.creditCardDocumentsStoreFacadeService.loading.searchCreditCardDocuments;
  public hasData$: Observable<boolean> = this.creditCardDocumentsStoreFacadeService.selectors.hasData$;
  public airlineCodesList$: Observable<DropdownOption[]>;
  public agentCodesList$: Observable<DropdownOption[]>;
  public currencyList$: Observable<DropdownOption<Currency>[]>;
  public periodList$: Observable<Period[]>;
  public documentTypeList: DropdownOption[];
  public filterFormGroup: FormGroup;
  public hasLeanPermission: boolean;
  public periodPickerYearsBack: number;
  public UserType = UserType;
  public currentPeriod: PeriodOption;

  private readonly subscription = new Subscription();
  private agentDropdownParam = null;
  private destroy$ = new Subject();

  constructor(
    public readonly creditCardDocumentsFilterFormatterService: CreditCardDocumentsFilterFormatterService,
    private readonly creditCardDocumentsStoreFacadeService: CreditCardDocumentsStoreFacadeService,
    private readonly translationService: L10nTranslationService,
    private readonly permissionService: PermissionsService,
    private readonly airlineDictionaryService: AirlineDictionaryService,
    private readonly agentDictionaryService: AgentDictionaryService,
    private readonly currencyDictionaryService: CurrencyDictionaryService,
    private readonly periodDictionaryService: PeriodService
  ) {
    this.documentTypeList = ArrayHelper.toDropdownOptions(Object.values(DocumentType));
    this.documentTypeList = ArrayHelper.toDropdownOptions(Object.values(TransactionCode));
    this.periodPickerYearsBack = this.hasLeanPermission ? LEAN_USERS_YEARS : NON_LEAN_USERS_YEARS;
  }

  public ngOnInit(): void {
    this.initializeAgentOptions();
    this.initializePermissions();
    this.initializeFormGroup();
    this.initializeBspDropdownOptions();
    this.initializeDefaultBsp();
    this.initializeQuery();
    this.handleSwitchBetweenPeriodAndMonthSelection();
  }

  public get title$(): Observable<string> {
    return this.query$.pipe(
      map(query => query?.paginateBy?.totalElements),
      map(length => `${this.translationService.translate('bsp-credit-card-documents.view.title')} (${length || 0})`)
    );
  }

  public formatInvoiceDate(invoiceDate: string): string {
    return moment(invoiceDate).format('YYDDMM');
  }

  public get user$(): Observable<User> {
    return this.creditCardDocumentsStoreFacadeService.selectors.user$;
  }

  public get isUserAgent$(): Observable<boolean> {
    return this.user$.pipe(map(user => user.userType === UserType.AGENT));
  }

  public get isUserAirline$(): Observable<boolean> {
    return this.user$.pipe(map(user => user.userType === UserType.AIRLINE));
  }

  public get predefinedFilters$() {
    return this.userDefaultBsp$.pipe(
      map(bsp => ({
        isoCountryCode: [bsp],
        periods: [this.currentPeriod, this.currentPeriod]
      }))
    );
  }

  public get isBspFilterDisabled$(): Observable<boolean> {
    return this.userBspDropdownOptions$.pipe(map(userBspDropdownOptions => Boolean(userBspDropdownOptions?.length)));
  }

  public get isoCountryCode(): Bsp[] {
    return this.filterFormGroup.get('isoCountryCode').value;
  }

  public onQueryChanged(query: DataQuery<CreditCardDocumentFilters>): void {
    this.creditCardDocumentsStoreFacadeService.actions.searchCreditCardDocuments(query);
  }

  public get monthControl(): FormControl {
    return this.filterFormGroup.get('periodYearMonth') as FormControl;
  }

  public get periodsControl(): FormControl {
    return this.filterFormGroup.get('periods') as FormControl;
  }

  public get isoCountryCodeControl(): FormControl {
    return this.filterFormGroup.get('isoCountryCode') as FormControl;
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.destroy$.next();
  }

  public onDownload(): void {
    this.creditCardDocumentsStoreFacadeService.actions.downloadCreditCardDocuments(false);
  }

  public onDetailedDownload(): void {
    this.creditCardDocumentsStoreFacadeService.actions.downloadCreditCardDocuments(true);
  }

  private initializeAgentOptions(): void {
    this.user$.pipe(takeUntil(this.destroy$)).subscribe(user => {
      if (user.userType === UserType.AGENT_GROUP) {
        this.agentDropdownParam = {
          agentGroupId: user.id
        };
      }
    });
  }

  private handleSwitchBetweenPeriodAndMonthSelection() {
    this.subscription.add(
      this.isoCountryCodeControl.valueChanges.pipe(distinct()).subscribe(
        function (value) {
          if (value?.length < 2) {
            if (!this.periodsControl?.value?.length) {
              this.monthControl.setValue([]);
              this.periodsControl.setValue([this.currentPeriod, this.currentPeriod]);
            }
          } else {
            if (!this.monthControl?.value?.length) {
              this.monthControl.setValue(this.currentPeriod ? [this.currentPeriod] : []);
              this.periodsControl.setValue([]);
            }
          }
        }.bind(this)
      )
    );
  }

  private initializeDefaultBsp() {
    this.userDefaultBsp$ = combineLatest([
      this.creditCardDocumentsStoreFacadeService.selectors.userBsps$,
      this.creditCardDocumentsStoreFacadeService.selectors.user$
    ]).pipe(map(([bspList, user]) => this.getDefaultBsp(bspList, user)));
  }

  private initializeBspDropdownOptions() {
    this.userBspDropdownOptions$ = this.creditCardDocumentsStoreFacadeService.selectors.userBsps$.pipe(
      map(bspList => bspList.map(bsp => toValueLabelObjectBsp(bsp)))
    );
  }

  private initializeFormGroup() {
    this.filterFormGroup = new FormGroup({
      isoCountryCode: new FormControl([], []),
      periodYearMonth: new FormControl([], []),
      periods: new FormControl([], []),
      transactionCode: new FormControl([], []),
      formOfPaymentType: new FormControl(null, []),
      currencyCode: new FormControl(null, []),
      binRange: new FormControl(null, []),
      dateOfIssue: new FormControl(null, []),
      agentCode: new FormControl([], []),
      airlineCode: new FormControl([], [])
    });
  }

  private initializeQuery() {
    this.userDefaultBsp$.pipe(first()).subscribe(bsp => {
      this.airlineCodesList$ = this.airlineDictionaryService.getDropdownOptions();
      this.agentCodesList$ = this.agentDictionaryService.getDropdownOptions(this.agentDropdownParam);
      this.currencyList$ = this.currencyDictionaryService.getDropdownOptions(bsp.id);
      this.periodList$ = this.periodDictionaryService.getByBsp(bsp.id);

      this.periodList$.pipe(first()).subscribe(periods => {
        this.currentPeriod = PeriodUtils.findCurrentPeriod(periods);

        this.creditCardDocumentsStoreFacadeService.actions.searchCreditCardDocuments({
          ...defaultQuery,
          filterBy: {
            isoCountryCode: [bsp],
            periods: [this.currentPeriod, this.currentPeriod],
            ...defaultQuery.filterBy
          }
        });
      });
    });
  }

  private initializePermissions(): void {
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
    this.hasLeanPermission = this.permissionService.hasPermission(Permissions.lean);
  }

  private getDefaultBsp(bspList: Bsp[], user: User): Bsp {
    return bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
  }
}
