import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/bsp-credit-card-documents.actions';
import * as fromSelectors from '../selectors/bsp-credit-card-documents.selectors';
import { CreditCardDocumentsService } from '../services/bsp-credit-card-documents.service';
import * as fromState from '../store/bsp-credit-card-documents.state';
import * as fromStub from '../stubs/bsp-credit-card-documents.stubs';

import { CreditCardDocumentEffects } from './bsp-credit-card-documents.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('CreditCardDocumentEffects', () => {
  let actions$: Observable<any>;
  let effects: CreditCardDocumentEffects;
  let service: CreditCardDocumentsService;

  const initialState = {
    [fromState.bspCreditCardDocumentsStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CreditCardDocumentEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectCreditCardDocumentsQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: CreditCardDocumentsService,
          useValue: {
            find: () => {},
            download: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<CreditCardDocumentEffects>(CreditCardDocumentEffects);
    service = TestBed.inject<CreditCardDocumentsService>(CreditCardDocumentsService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchCreditCardDocumentsEffect', () => {
    it('should return searchCreditCardDocumentsSuccess with paged data', () => {
      const action = fromActions.searchCreditCardDocumentsRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchCreditCardDocumentsSuccess({
        payload: {
          pagedData: fromStub.bspCreditCardDocumentPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.bspCreditCardDocumentPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchCreditCardDocumentsEffect$).toBeObservable(expected);
    });

    it('should return searchCreditCardDocumentsFailure when find fails', () => {
      const action = fromActions.searchCreditCardDocumentsRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchCreditCardDocumentsFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchCreditCardDocumentsEffect$).toBeObservable(expected);
    });
  });
});
