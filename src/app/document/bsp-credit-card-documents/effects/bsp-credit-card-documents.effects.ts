import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as fromActions from '../actions/bsp-credit-card-documents.actions';
import * as fromSelectors from '../selectors/bsp-credit-card-documents.selectors';
import { CreditCardDocumentsService } from '../services/bsp-credit-card-documents.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

@Injectable()
export class CreditCardDocumentEffects {
  public constructor(
    private readonly actions$: Actions,
    private readonly creditCardDocumentsService: CreditCardDocumentsService,
    private readonly dialogService: DialogService,
    private readonly store: Store<unknown>
  ) {}

  public searchCreditCardDocumentsEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchCreditCardDocumentsRequest>>(
        fromActions.searchCreditCardDocumentsRequest
      ),
      switchMap(({ payload: { query } }) =>
        this.creditCardDocumentsService.find(query).pipe(
          map(data =>
            fromActions.searchCreditCardDocumentsSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchCreditCardDocumentsFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public downloadCreditCardDocumentsEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.downloadCreditCardDocumentsRequest>>(
          fromActions.downloadCreditCardDocumentsRequest
        ),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectCreditCardDocumentsQuery))),
        tap(([action, query]) => {
          const modalTitle = action.payload.detailed
            ? `bsp-credit-card-documents.download.detailedDownloadTitle`
            : `bsp-credit-card-documents.download.downloadTitle`;

          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: modalTitle,
              footerButtonsType: FooterButton.Download,
              downloadQuery: query,
              detailedDownload: action.payload.detailed
            },
            apiService: this.creditCardDocumentsService
          });
        })
      ),
    { dispatch: false }
  );
}
