import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreditCardDocumentsComponent } from './bsp-credit-card-documents.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: CreditCardDocumentsComponent,
    data: {
      tab: ROUTES.CREDIT_CARD_DOCUMENTS
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BspCreditCardDocumentsRoutingModule {}
