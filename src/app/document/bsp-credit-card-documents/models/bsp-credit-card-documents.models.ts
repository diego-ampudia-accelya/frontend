import { PeriodOption } from '~app/shared/components';
import { DocumentType } from '~app/shared/enums';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface CreditCardDocument {
  bsp: Bsp;
  airline: AirlinePartial;
  agent: AgentPartial;
  currency: Currency;
  documentNumber: string;
  billingAnalysisEndingDate: string;
  period: string;
  id: string;
  dateOfIssue: string;
  transactionCode: DocumentType;
  formOfPayment: FormOfPayment;
}

export interface FormOfPayment {
  expiryDate?: string;
  formOfPaymentType: string;
  formOfPaymentAmount: number;
  extendedPaymentCode?: string;
  approvalCode?: string;
  invoiceDate: string;
  creditCardNumber: string;
}

export interface CreditCardDocumentFilters {
  isoCountryCode: Bsp[];
  periodYearMonth: PeriodOption[];
  currencyCode: Currency[];
  agentCode: AgentSummary[];
  airlineCode: AirlineSummary[];
  dateOfIssue: Date[];
  periods: PeriodOption[];
  transactionCode: DocumentType[];
  binRange: string;
  formOfPaymentType: string;
}

export interface AirlinePartial {
  id: number;
  iataCode: string;
  localName: string;
}

export interface AgentPartial {
  id: number;
  iataCode: string;
  name: string;
}

export enum TransactionCode {
  TASF = 'TASF',
  SSAC = 'SSAC',
  SSAD = 'SSAD',
  EMDA = 'EMDA',
  SALE = 'SALE',
  ACNT = 'ACNT',
  RFNC = 'RFNC',
  RFND = 'RFND',
  SPDR = 'SPDR',
  SPCR = 'SPCR',
  ACMA = 'ACMA',
  ADMA = 'ADMA',
  ACMD = 'ACMD',
  ADMD = 'ADMD',
  CANN = 'CANN',
  CANR = 'CANR',
  ADNT = 'ADNT',
  TKTT = 'TKTT',
  CANX = 'CANX',
  EMDS = 'EMDS'
}
