import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { BspCreditCardDocumentsRoutingModule } from './bsp-credit-card-documents-routing.module';
import { CreditCardDocumentsComponent } from './bsp-credit-card-documents.component';
import { CreditCardDocumentsListComponent } from './components/bsp-credit-card-documents-list/bsp-credit-card-documents-list.component';
import { CreditCardDocumentEffects } from './effects/bsp-credit-card-documents.effects';
import { creditCardDocumentsReducer } from './reducers/bsp-credit-card-documents.reducer';
import { CreditCardDocumentsFilterFormatterService } from './services/bsp-credit-card-documents-filter-formatter.service';
import { CreditCardDocumentsService } from './services/bsp-credit-card-documents.service';
import { CreditCardDocumentsStoreFacadeService } from './store/bsp-credit-card-documents-store-facade.service';
import { bspCreditCardDocumentsStateFeatureKey } from './store/bsp-credit-card-documents.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    BspCreditCardDocumentsRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(bspCreditCardDocumentsStateFeatureKey, creditCardDocumentsReducer),
    EffectsModule.forFeature([CreditCardDocumentEffects])
  ],
  declarations: [CreditCardDocumentsComponent, CreditCardDocumentsListComponent],
  providers: [
    CreditCardDocumentsService,
    CreditCardDocumentsStoreFacadeService,
    CreditCardDocumentsFilterFormatterService
  ]
})
export class BspCreditCardDocumentsModule {}
