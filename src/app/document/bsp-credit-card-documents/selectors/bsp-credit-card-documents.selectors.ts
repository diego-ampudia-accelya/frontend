import { createFeatureSelector, createSelector } from '@ngrx/store';

import {
  bspCreditCardDocumentsStateFeatureKey,
  CreditCardDocumentsState
} from '../store/bsp-credit-card-documents.state';

export const selectCreditCardDocumentsStoreStateFeature = createFeatureSelector<CreditCardDocumentsState>(
  bspCreditCardDocumentsStateFeatureKey
);

export const selectCreditCardDocuments = createSelector(
  selectCreditCardDocumentsStoreStateFeature,
  state => state.data
);
export const selectCreditCardDocumentsQuery = createSelector(
  selectCreditCardDocumentsStoreStateFeature,
  state => state.query
);
export const selectCreditCardDocumentsLoading = createSelector(
  selectCreditCardDocumentsStoreStateFeature,
  state => state.loading.searchCreditCardDocuments
);

export const selectHasData = createSelector(selectCreditCardDocumentsStoreStateFeature, state =>
  Boolean(state.data && state.data.length)
);
