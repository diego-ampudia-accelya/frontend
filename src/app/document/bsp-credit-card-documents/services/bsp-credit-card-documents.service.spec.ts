import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { CreditCardDocumentsStoreFacadeService } from '../store/bsp-credit-card-documents-store-facade.service';

import { CreditCardDocumentsService } from './bsp-credit-card-documents.service';
import { AppConfigurationService } from '~app/shared/services';

describe('CreditCardDocumentsService', () => {
  let service: CreditCardDocumentsService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CreditCardDocumentsService,
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: ''
          }
        },
        {
          provide: CreditCardDocumentsStoreFacadeService,
          useValue: {
            actions: {
              searchCreditCardDocuments: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null)
            },
            loading: {
              searchCreditCardDocuments$: of(null)
            }
          }
        }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(CreditCardDocumentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
