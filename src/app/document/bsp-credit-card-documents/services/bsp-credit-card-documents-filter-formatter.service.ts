import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';

import { CreditCardDocumentFilters } from '../models/bsp-credit-card-documents.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import {
  agentFilterTagMapper,
  airlineFilterTagMapper,
  joinMapper,
  rangeDateFilterTagMapper
} from '~app/shared/helpers';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

type CreditCardDocumentMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class CreditCardDocumentsFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService, private readonly periodPipe: PeriodPipe) {}

  public format(filter: CreditCardDocumentFilters) {
    const filterMappers: Partial<CreditCardDocumentMapper<CreditCardDocumentFilters>> = {
      isoCountryCode: isoCountryCode =>
        `${this.translate('isoCountryCode')} - ${joinMapper(isoCountryCode?.map(bsp => bsp.isoCountryCode))}`,
      periodYearMonth: periodYearMonth => this.periodPipe.transform(periodYearMonth, true),
      currencyCode: currencyCode =>
        `${this.translate('currencyCode')} - ${joinMapper(currencyCode?.map(currency => currency.code))}`,
      agentCode: agentCode => `${this.translate('agentCode')} - ${agentFilterTagMapper(agentCode)}`,
      airlineCode: airlineCode => `${this.translate('airlineCode')} - ${airlineFilterTagMapper(airlineCode)}`,
      dateOfIssue: dateOfIssue => `${this.translate('dateOfIssue')} - ${rangeDateFilterTagMapper(dateOfIssue)}`,
      periods: periods => this.periodPipe.transform(periods),
      transactionCode: transactionCode => `${this.translate('transactionCode')} - ${joinMapper(transactionCode)}`,
      binRange: binRange => `${this.translate('binRange')} - ${binRange}`,
      formOfPaymentType: formOfPaymentType => `${this.translate('formOfPaymentType')} - ${formOfPaymentType}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`bsp-credit-card-documents.filters.${key}.label`);
  }
}
