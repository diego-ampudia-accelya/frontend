import { inject, TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { CreditCardDocumentsFilterFormatterService } from './bsp-credit-card-documents-filter-formatter.service';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

describe('CreditCardDocumentsFilterFormatterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CreditCardDocumentsFilterFormatterService,
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: PeriodPipe,
          useValue: {
            transform: () => ''
          }
        }
      ]
    });
  });

  it('should be created', inject(
    [CreditCardDocumentsFilterFormatterService],
    (service: CreditCardDocumentsFilterFormatterService) => {
      expect(service).toBeTruthy();
    }
  ));
});
