import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { clone } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CreditCardDocument, CreditCardDocumentFilters } from '../models/bsp-credit-card-documents.models';
import { Period } from '~app/master-data/periods/shared/period.models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class CreditCardDocumentsService implements Queryable<CreditCardDocument> {
  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  private baseUrl = `${this.appConfiguration.baseApiPath}/globalsearch/hot-documents/credit-cards/type`;

  public find(query: DataQuery<CreditCardDocumentFilters>): Observable<PagedData<CreditCardDocument>> {
    const filterBy = this.buildFilterBy(query);

    const requestQuery = this.buildRequestQuery(query);

    return this.http.post<PagedData<CreditCardDocument>>(this.baseUrl + requestQuery.getQueryString(), filterBy);
  }

  private buildFilterBy(query: DataQuery<CreditCardDocumentFilters>) {
    const filterBy = JSON.parse(JSON.stringify(query.filterBy));
    if (filterBy.periodYearMonth) {
      const [{ period }] = filterBy.periodYearMonth;

      filterBy.periodYearMonth = this.formatMonthPeriod(period);
    }

    if (filterBy.isoCountryCode) {
      filterBy.isoCountryCode = filterBy.isoCountryCode.map(({ isoCountryCode }) => isoCountryCode);
    }

    if (filterBy.dateOfIssue) {
      const [dateOfIssueFrom, dateOfIssueTo] = clone(filterBy.dateOfIssue);
      delete filterBy.dateOfIssue;
      filterBy['dateOfIssueFrom'] = toShortIsoDate(dateOfIssueFrom);
      filterBy['dateOfIssueTo'] = toShortIsoDate(dateOfIssueTo);
    }

    if (filterBy.periods) {
      filterBy.periods = this.formatPeriods(filterBy.periods);
    }

    if (filterBy.agentCode) {
      filterBy.agentCode = filterBy.agentCode.map(({ code }) => code);
    }

    if (filterBy.airlineCode) {
      filterBy.airlineCode = filterBy.airlineCode.map(({ code }) => code);
    }

    if (filterBy.currencyCode) {
      filterBy.currencyCode = filterBy.currencyCode.map(({ code }) => code);
    }

    return filterBy;
  }

  private buildRequestQuery(query: DataQuery<CreditCardDocumentFilters>) {
    const _query = JSON.parse(JSON.stringify(query));

    _query.sortBy = formatSortBy(_query.sortBy, [{ from: 'currency.code', to: 'currencyCode' }]);

    delete _query.filterBy;

    return RequestQuery.fromDataQuery(_query);
  }

  public download(
    query: DataQuery<CreditCardDocumentFilters>,
    downloadFormat: DownloadFormat,
    _downloadBody: any = {},
    detailedDownload?: boolean
  ): Observable<{ blob: Blob; fileName: string }> {
    const filterBy = this.buildFilterBy(query);

    const requestQuery = this.buildRequestQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat.toLocaleUpperCase() };

    const url = detailedDownload
      ? `${this.baseUrl}${requestQuery.getQueryString()}&content=EXTENDED_DETAILS`
      : `${this.baseUrl}${requestQuery.getQueryString()}&content=DETAILS`;

    return this.http
      .post<HttpResponse<ArrayBuffer>>(url, filterBy, downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  /**
   * Format month period, removing last specific period digit
   *
   * @param period with format YYYYMM
   * @returns `periodYearMonth` with format YYYYMM
   */
  private formatMonthPeriod(period: string): string {
    return period.substring(0, period.length - 1);
  }

  private formatPeriods(periods: Period[]): string[] {
    const datePeriod = this.formatMonthPeriod(periods[0].period);
    const startPeriod = periods[0].period;
    const endPeriod = periods[1].period;
    const fromPeriod = startPeriod.charAt(startPeriod.length - 1);
    const toPeriod = endPeriod.charAt(endPeriod.length - 1);
    const periodsFilter: string[] = [];

    for (let i: number = parseInt(fromPeriod, 10); i <= parseInt(toPeriod, 10); i++) {
      const period = `${datePeriod}${i}`;
      periodsFilter.push(period);
    }

    return periodsFilter;
  }
}
