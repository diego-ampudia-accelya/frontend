import { CreditCardDocument } from '../models/bsp-credit-card-documents.models';
import { DocumentType } from '~app/shared/enums';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { User, UserType } from '~app/shared/models/user.model';

const creditCardDocuments: CreditCardDocument[] = [
  {
    bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
    airline: { id: 6983505048, iataCode: '220', localName: 'lufthansi' },
    agent: {
      id: 69837821526,
      iataCode: '7821526',
      name: 'NOMBRE 7821526'
    },
    currency: { id: 698582, code: 'EUR', decimals: 2 },
    documentNumber: '6523294025',
    billingAnalysisEndingDate: '2021-11-30',
    period: '2021114',
    id: '6523294025-8345-RVNfTUlTWUlOREhPVF8yMDIxMTEzMC56aXA=',
    dateOfIssue: '2021-11-30',
    transactionCode: DocumentType.TKTT,
    formOfPayment: {
      expiryDate: '0323',
      formOfPaymentType: 'CCDC3603',
      formOfPaymentAmount: 835.89,
      approvalCode: '000784',
      invoiceDate: '2021-12-01',
      creditCardNumber: '360396XXXX0136'
    }
  },
  {
    bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
    airline: { id: 6983505048, iataCode: '220', localName: 'lufthansi' },
    agent: {
      id: 69837821282,
      iataCode: '7821282',
      name: 'NOMBRE 7821282'
    },
    currency: { id: 698582, code: 'EUR', decimals: 2 },
    documentNumber: '6523298226',
    billingAnalysisEndingDate: '2021-11-30',
    period: '2021114',
    id: '6523298226-6380-RVNfTUlTWUlOREhPVF8yMDIxMTEzMC56aXA=',
    dateOfIssue: '2021-11-30',
    transactionCode: DocumentType.TKTT,
    formOfPayment: {
      expiryDate: '0325',
      formOfPaymentType: 'CCAX3798',
      formOfPaymentAmount: 504.78,
      approvalCode: '111423',
      invoiceDate: '2021-12-01',
      creditCardNumber: '379882XXXXX5832'
    }
  },
  {
    bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
    airline: { id: 6983505048, iataCode: '220', localName: 'lufthansi' },
    agent: {
      id: 69837821282,
      iataCode: '7821282',
      name: 'NOMBRE 7821282'
    },
    currency: { id: 698582, code: 'EUR', decimals: 2 },
    documentNumber: '6523298237',
    billingAnalysisEndingDate: '2021-11-30',
    period: '2021114',
    id: '6523298237-6391-RVNfTUlTWUlOREhPVF8yMDIxMTEzMC56aXA=',
    dateOfIssue: '2021-11-30',
    transactionCode: DocumentType.TKTT,
    formOfPayment: {
      expiryDate: '0324',
      formOfPaymentType: 'CCVI4277',
      formOfPaymentAmount: 416.46,
      approvalCode: '053898',
      invoiceDate: '2021-12-01',
      creditCardNumber: '427730XXXXXX4389'
    }
  }
];

export const bspCreditCardDocumentPagedData: PagedData<CreditCardDocument> = {
  pageNumber: 0,
  pageSize: 20,
  total: 474,
  records: creditCardDocuments
};

export const userBsps: Bsp[] = [
  {
    id: 6983,
    isoCountryCode: 'ES',
    name: 'SPAIN',
    effectiveFrom: '2000-01-01',
    effectiveTo: null,
    version: 3232,
    defaultCurrencyCode: null,
    active: true
  }
];

export const user: User = {
  email: 'martin.sautter@iata.org',
  firstName: null,
  lastName: 'User0BSPES',
  telephone: '021224836126969',
  organization: "organisation ' comilla",
  address: '19 Mayis Cad. 4',
  locality: "icity ' comillas",
  city: null,
  zip: '123',
  country: 'Turquia',
  registerDate: '2000-09-06',
  expiryDate: null,
  active: true,
  id: 69830,
  userType: UserType.IATA,
  iataCode: null,
  language: 'en',
  bspPermissions: [
    {
      bspId: 6983,
      permissions: ['rMonHot']
    }
  ],
  permissions: ['rMonHot'],
  bsps: [
    {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      version: 3232,
      defaultCurrencyCode: null,
      active: true
    }
  ],
  isImpersonating: false,
  defaultIsoc: 'ES',
  template: 'STREAMLINED',
  isSuperUser: false
};
