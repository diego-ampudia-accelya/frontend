import moment from 'moment-mini';

import { GridColumn } from '~app/shared/models';

export const CREDIT_CARD_DOCUMENTS = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'bsp.isoCountryCode',
        name: 'bsp-credit-card-documents.columns.isoCountryCode',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'transactionCode',
        name: 'bsp-credit-card-documents.columns.transactionCode',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'documentNumber',
        name: 'bsp-credit-card-documents.columns.documentNumber',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'agent.iataCode',
        name: 'bsp-credit-card-documents.columns.agent-iataCode',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'airline.iataCode',
        name: 'bsp-credit-card-documents.columns.airline-iataCode',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'formOfPayment.formOfPaymentType',
        name: 'bsp-credit-card-documents.columns.formOfPaymentType',
        resizeable: true,
        draggable: false,
        sortable: false,
        pipe: { transform: (value: string) => value.split('').slice(2, 4).join('') }
      },
      {
        prop: 'formOfPayment.creditCardNumber',
        name: 'bsp-credit-card-documents.columns.creditCardNumber',
        resizeable: true,
        draggable: false,
        sortable: false
      },
      {
        prop: 'dateOfIssue',
        name: 'bsp-credit-card-documents.columns.dateOfIssue',
        resizeable: false,
        draggable: false,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY') }
      },
      {
        prop: 'period',
        name: 'bsp-credit-card-documents.columns.period',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'currency.code',
        name: 'bsp-credit-card-documents.columns.currency-code',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'formOfPayment.formOfPaymentAmount',
        name: 'bsp-credit-card-documents.columns.formOfPaymentAmount',
        resizeable: true,
        draggable: false,
        sortable: false
      }
    ];
  }
};

export const LEAN_USERS_YEARS = 5;
export const NON_LEAN_USERS_YEARS = 2;
