import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/bsp-credit-card-documents.actions';
import * as fromState from '../store/bsp-credit-card-documents.state';
import * as fromStub from '../stubs/bsp-credit-card-documents.stubs';

import * as fromReducer from './bsp-credit-card-documents.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('creditCardDocumentsReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.creditCardDocumentsReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchCreditCardDocumentsRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchCreditCardDocumentsRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.creditCardDocumentsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchCreditCardDocuments: true
        },
        error: {
          ...fromState.initialState.error,
          searchCreditCardDocuments: null
        }
      });
    });
  });

  describe('searchCreditCardDocumentsFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchCreditCardDocumentsFailure({ payload: { error } });
      const result = fromReducer.creditCardDocumentsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchCreditCardDocuments: false
        },
        error: {
          ...fromState.initialState.error,
          searchCreditCardDocuments: error
        }
      });
    });
  });

  describe('searchCreditCardDocumentsSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchCreditCardDocumentsSuccess({
        payload: { pagedData: fromStub.bspCreditCardDocumentPagedData }
      });
      const result = fromReducer.creditCardDocumentsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.bspCreditCardDocumentPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.bspCreditCardDocumentPagedData.pageNumber,
            size: fromStub.bspCreditCardDocumentPagedData.pageSize,
            totalElements: fromStub.bspCreditCardDocumentPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchCreditCardDocuments: false
        },
        error: {
          ...fromState.initialState.error,
          searchCreditCardDocuments: null
        }
      });
    });
  });
});
