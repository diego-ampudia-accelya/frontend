import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/bsp-credit-card-documents.actions';
import { initialState } from '../store/bsp-credit-card-documents.state';

export const creditCardDocumentsReducer = createReducer(
  initialState,
  on(fromActions.searchCreditCardDocumentsRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchCreditCardDocuments: true
    },
    error: {
      ...state.error,
      searchCreditCardDocuments: null
    }
  })),
  on(fromActions.searchCreditCardDocumentsFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchCreditCardDocuments: false
    },
    error: {
      ...state.error,
      searchCreditCardDocuments: error
    }
  })),
  on(fromActions.searchCreditCardDocumentsSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchCreditCardDocuments: false
    },
    error: {
      ...state.error,
      searchCreditCardDocuments: null
    }
  })),
  on(fromActions.updateCreditCardDocumentsFilterByQuery, (state, { payload: { filterBy } }) => ({
    ...state,
    query: {
      ...state.query,
      filterBy: {
        ...state.query.filterBy,
        ...filterBy
      }
    }
  }))
);
