import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AuditAccessComponent } from './audit-access/audit-access.component';
import { AuditAdmAcmComponent } from './audit-adm-acm/audit-adm-acm.component';
import { AuditPbdsComponent } from './audit-pbds/audit-pbds.component';
import { AuditRefundsComponent } from './audit-refunds/audit-refunds.component';
import { AuditRoutingModule } from './audit-routing,module';
import { AuditAccessFilterFormatter } from './services/audit-access-filter-formatter';
import { AuditAccessService } from './services/audit-access.service';
import { AuditAdmAcmFilterFormatter } from './services/audit-adm-acm-filter-formatter';
import { AuditAdmAcmService } from './services/audit-adm-acm.service';
import { AuditPbdsFilterFormatter } from './services/audit-pbds-filter-formatter';
import { AuditPbdsService } from './services/audit-pbds.service';
import { AuditRefundsFilterFormatter } from './services/audit-refunds-filter-formatter';
import { AuditRefundsService } from './services/audit-refunds.service';
import { SharedModule } from '~app/shared/shared.module';
import { ConfigurationModule } from '~app/master-data/configuration';

@NgModule({
  imports: [CommonModule, AuditRoutingModule, SharedModule, ConfigurationModule],
  declarations: [AuditAccessComponent, AuditAdmAcmComponent, AuditRefundsComponent, AuditPbdsComponent],
  providers: [
    AuditAccessFilterFormatter,
    AuditAccessService,
    AuditAdmAcmService,
    AuditAdmAcmFilterFormatter,
    AuditRefundsService,
    AuditRefundsFilterFormatter,
    AuditPbdsService,
    AuditPbdsFilterFormatter
  ]
})
export class AuditModule {}
