import { Bsp } from '~app/shared/models/bsp.model';

export interface AuditPbdsFilter {
  isoCountryCode: Bsp[];
  transactionCode: string;
  documentNumber: string;
  time: Date[];
  attribute: AuditDropdownOption[];
  typeUser: AuditDropdownOption[];
  codeUser: string;
  emailUser: string;
}

export interface AuditDropdownOption {
  translationKey: string;
  value: string;
}

export interface AuditPredefinedFilters {
  isoCountryCode: Bsp;
}
