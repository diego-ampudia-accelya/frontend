import { Bsp } from '~app/shared/models/bsp.model';

export interface AuditAccess {
  isoCountryCode: Bsp[];
  email: string;
  operation: string[];
  time: Date[];
}

export interface AuditAccessBE {
  isoCountryCode: string[];
  userType: string[];
  email: string;
  operation: string[];
  time: string[];
  dateFrom: string;
  dateTo: string;
}
