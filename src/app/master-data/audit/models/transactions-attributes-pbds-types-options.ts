export enum AuditPbdsTransactionsAttr {
  AgentCode = 'Agent Code',
  AirlineCode = 'Airline Code',
  PBDDate = 'Dispute date',
  ResolutionDate = 'Resolution date',
  Status = 'Status'
}
