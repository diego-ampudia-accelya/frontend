export enum UserTypeForFilter {
  Airline = 'Airline',
  Agent = 'Agent',
  AgentGroup = 'Agent_Group',
  Dpc = 'Dpc',
  Gds = 'Gds',
  Iata = 'Iata',
  ThirdParty = 'Third_Party'
}

export const USER_TYPE_FILTERNAME = 'userType';
