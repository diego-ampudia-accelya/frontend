import { Bsp } from '~app/shared/models/bsp.model';

export interface AuditAccessFilter {
  isoCountryCode: Bsp[];
  userType: AuditDropdownFilterOption[];
  iataCode: string;
  email: string;
  operation: AuditDropdownFilterOption[];
  time: Date[];
}

export interface AuditDropdownFilterOption {
  translationKey: string;
  value: string;
}

export interface AuditPredefinedFilters {
  isoCountryCode: Bsp;
}
