import { Bsp } from '~app/shared/models/bsp.model';

export interface AuditAdmAcm {
  isoCountryCode: Bsp[];
  documentNumber: string;
  typeUser: string[];
  emailUser: string;
  transactionCode: string;
  attribute: string[];
  time: Date[];
  dateFrom: string;
  dateTo: string;
  documentId: number;
}

export interface AuditAdmAcmBE {
  isoCountryCode: string[];
  typeUser: string[];
  email: string;
  attribute: string[];
  time: string[];
  dateFrom: string;
  dateTo: string;
}
