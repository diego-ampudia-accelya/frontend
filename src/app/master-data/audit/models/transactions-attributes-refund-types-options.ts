export enum AuditRefundTransactionsAttr {
  AirlineCode = 'Airline Code',
  AgentCode = 'Agent Code',
  Status = 'Status',
  Currency = 'Currency',
  Amount = 'Amount',
  Related = 'Related',
  Passenger = 'Passenger',
  ReportingDate = 'Reporting Date',
  RejectionDate = 'Rejection Date',
  Sent_to_dpc = 'Sent to DPC',
  Sent_to_airline = 'Sent To Airline',
  Changed_by_agent = 'Changed By Agent',
  ReportingPeriod = 'Reporting Period',
  Daysleft = 'Days left',
  STAT = 'STAT'
}
