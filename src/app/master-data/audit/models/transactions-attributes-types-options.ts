export enum AuditAdmAcmTransactionsAttr {
  AirlineCode = 'Airline Code',
  AgentCode = 'Agent Code',
  Status = 'Status',
  Currency = 'Currency',
  Amount = 'Amount',
  RTDNType = 'RTDN Type',
  IssueDate = 'Issue Date',
  ReportingDate = 'Reporting Date',
  DeleteDate = 'Delete Date',
  SenttoDPC = 'Sent to DPC',
  RMIC = 'RMIC',
  STAT = 'STAT',
  AirlineFare = 'Airline Fare',
  AgentFare = 'Agent Fare',
  AirlineTaxes = 'Airline Taxes',
  AgentTaxes = 'Agent Taxes',
  AirlineCommission = 'Airline Commission',
  AgentCommission = 'Agent Commission',
  AirlineSPAM = 'Airline SPAM',
  AgentSPAM = 'Agent SPAM',
  AirlineTOCA = 'Airline TOCA',
  AgentTOCA = 'Agent TOCA',
  AirlineCP = 'Airline CP',
  AgentCP = 'Agent CP',
  AirlineMF = 'Airline MF',
  AgentMF = 'Agent MF',
  DisputeDate = 'Dispute Date',
  ContactName = 'Contact Name',
  ContactEmail = 'Contact Email',
  ContactPhone = 'Contact Phone',
  DIRD = 'DIRD',
  Period = 'Period'
}
