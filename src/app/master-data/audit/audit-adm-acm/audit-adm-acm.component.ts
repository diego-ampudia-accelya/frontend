import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize, cloneDeep, isEmpty } from 'lodash';
import { combineLatest, Observable, Subject } from 'rxjs';
import { first, map, takeUntil, tap } from 'rxjs/operators';

import { AuditAdmAcmFilter, AuditDropdownOption, AuditPredefinedFilters } from '../models/audit-adm-acm-filter.model';
import { AuditAdmAcm } from '../models/audit-adm-acm.model';
import { AuditAdmAcmTransactionsAttr } from '../models/transactions-attributes-types-options';
import { UserTypeForFilter } from '../models/user-types-options';
import { AuditAdmAcmFilterFormatter } from '../services/audit-adm-acm-filter-formatter';
import { AuditAdmAcmService } from '../services/audit-adm-acm.service';
import { urlView } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

const DEFAULT_SORTBY = 'time';
const USER_TYPE_FILTERNAME = 'userType';

@Component({
  selector: 'bspl-audit-adm-acm',
  templateUrl: './audit-adm-acm.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: AuditAdmAcmService }
  ]
})
export class AuditAdmAcmComponent implements OnInit, OnDestroy {
  public title = this.translationService.translate('MENU.AUDIT.ADM/ACM.title');
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public userBsps: DropdownOption<BspDto>[];
  public isBspFilterLocked: boolean;
  public isBspFilterMultiple: boolean;
  public userTypeOptionList: DropdownOption<AuditDropdownOption>[];
  public attributeOptionList: DropdownOption<AuditDropdownOption>[];
  public predefinedFilters: AuditPredefinedFilters;
  public GDP_USER_TYPE = UserType.GDS;

  private hasLeanPermission: boolean;
  private hasAllBspsPermission: boolean;
  private formFactory: FormUtil;
  private dateTimeFormat = 'dd/MM/yyyy HH:mm:ss';
  private defaultBsp: Bsp;
  private bspControl: FormControl;
  private destroy$ = new Subject();
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private loggedUser: User;
  private get selectedBsp(): Bsp {
    const bspControl = FormUtil.get<AuditAdmAcmFilter>(this.searchForm, 'isoCountryCode');

    return bspControl.value;
  }
  private set selectedBsp(selectedBsp: Bsp) {
    const bspControl = FormUtil.get<AuditAdmAcmFilter>(this.searchForm, 'isoCountryCode');
    const selectedBspAdapted = this.isBspFilterMultiple ? [selectedBsp] : selectedBsp;

    bspControl.setValue(selectedBspAdapted);
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(fromAuth.getUser), first());
  }

  constructor(
    public displayFormatter: AuditAdmAcmFilterFormatter,
    public dataSource: QueryableDataSource<AuditAdmAcm>,
    private datePipe: DatePipe,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private queryStorage: DefaultQueryStorage,
    private permissionService: PermissionsService,
    private store: Store<AppState>,
    private router: Router,
    private bspsDictionaryService: BspsDictionaryService,
    private dialogService: DialogService,
    private auditAdmAcmService: AuditAdmAcmService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.initializeLoggedUser();
    this.initColumns();
    this.searchForm = this.buildForm();

    this.initializePermissions();
    this.initializeBspFilterAndLoadData();
    this.populateDropdownFilters();
  }

  public loggedUserIsOfType(userType: string): boolean {
    return userType.toUpperCase() === this.loggedUser.userType.toUpperCase();
  }

  public onViewDocument(event: AuditAdmAcm): void {
    const { documentId, transactionCode } = event;
    this.router.navigate([urlView[transactionCode.toLowerCase()], documentId]);
  }

  public loadData(query: DataQuery<AuditAdmAcmFilter>): void {
    query = query || cloneDeep(defaultQuery);
    let dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    if (isEmpty(dataQuery.sortBy)) {
      const sortBy = [{ attribute: DEFAULT_SORTBY, sortType: SortOrder.Desc }];
      dataQuery = { ...dataQuery, sortBy };
    }

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.AUDIT.ADM/ACM.downloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.auditAdmAcmService
    });
  }

  private populateDropdownFilters(): void {
    this.userTypeOptionList = this.generateOptionList(UserTypeForFilter, USER_TYPE_FILTERNAME);
    this.attributeOptionList = this.generateOptionList(AuditAdmAcmTransactionsAttr);
  }

  private generateOptionList(enumOptions, filterName?: string): DropdownOption<AuditDropdownOption>[] {
    return Object.entries(enumOptions)
      .filter(([key, value]: [string, string]) => this.userTypeFilteringCallBack(value, filterName))
      .map(([key, val]: [string, string]) => ({
        value: { translationKey: key, value: val },
        label: this.translationService.translate(`MENU.AUDIT.ADM/ACM.filter.label.${key}`)
      }));
  }

  private userTypeFilteringCallBack(optionValue: string, filterName?: string): boolean | string {
    if (!filterName) {
      return optionValue;
    } else {
      return this.checkUserTypeOption(optionValue);
    }
  }

  private checkUserTypeOption(optionValue: string): boolean | string {
    if (
      optionValue.toUpperCase() === UserType.AGENT_GROUP.toUpperCase() ||
      optionValue.toUpperCase() === UserType.AGENT.toUpperCase() ||
      optionValue.toUpperCase() === UserType.IATA.toUpperCase() ||
      optionValue.toUpperCase() === UserType.AIRLINE.toUpperCase()
    ) {
      return optionValue;
    }
  }

  private buildForm(): FormGroup {
    this.bspControl = new FormControl();

    return this.formFactory.createGroup<AuditAdmAcmFilter>({
      isoCountryCode: this.bspControl,
      transactionCode: [],
      documentNumber: [],
      time: [],
      attribute: [],
      typeUser: [],
      codeUser: [],
      emailUser: []
    });
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      isoCountryCode: this.selectedBsp || this.defaultBsp
    };
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionService.hasPermission(Permissions.lean);
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private initializeLoggedUser() {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });
  }

  private initializeBspFilterAndLoadData(): void {
    this.initializeLeanBspFilterAndDefaultBsp$().subscribe(() => {
      this.storedQuery = this.queryStorage.get();
      this.loadData(this.storedQuery);
    });
  }

  private initializeLeanBspFilterAndDefaultBsp$(): Observable<DropdownOption<BspDto>[]> {
    if (this.loggedUser.userType.toUpperCase() === UserType.IATA.toUpperCase() && this.hasAllBspsPermission) {
      return this.initializeMcIataBspFilterAndDefaultBsp$();
    } else {
      return combineLatest([this.store.select(fromAuth.getUserBsps), this.store.select(fromAuth.getUser)]).pipe(
        first(),
        tap(() => (this.isBspFilterMultiple = this.hasLeanPermission || this.hasAllBspsPermission)),
        map(([bspList, user]) => {
          this.getDefaultAndSetSelectedBsp(bspList, user);
          if (!this.isBspFilterMultiple || bspList.length === 1) {
            this.setPredefinedFilters();
          }

          return [bspList] as [Bsp[]];
        }),
        map(([bspList]) => bspList.map(bsp => toValueLabelObjectBsp(bsp))),
        tap(bspList => (this.userBsps = bspList)),
        tap(bspList => (this.isBspFilterLocked = this.setBspFilterLockedCases(bspList)))
      );
    }
  }

  private initializeMcIataBspFilterAndDefaultBsp$(): Observable<DropdownOption<BspDto>[]> {
    return combineLatest([
      this.bspsDictionaryService.getAllBspDropdownOptions(),
      this.store.select(fromAuth.getUser)
    ]).pipe(
      first(),
      tap(() => (this.isBspFilterMultiple = this.hasAllBspsPermission)),
      map(([bspList, user]) => {
        this.getDefaultAndSetSelectedBspForMcIata(bspList, user);

        if (!this.isBspFilterMultiple || bspList.length === 1) {
          this.setPredefinedFilters();
        }

        return bspList;
      }),
      tap(bspList => (this.userBsps = bspList)),
      tap(bspList => (this.isBspFilterLocked = this.setBspFilterLockedCases(bspList)))
    );
  }

  private setBspFilterLockedCases(bspList: DropdownOption<BspDto>[]): boolean {
    return bspList.length === 1 || (!this.hasLeanPermission && !this.hasAllBspsPermission);
  }

  private getDefaultAndSetSelectedBspForMcIata(bspList: DropdownOption<BspDto>[], user: User): void {
    this.defaultBsp = user.bsps.find(bspUser =>
      bspList.some(bsp => bsp.value.isoCountryCode === bspUser.isoCountryCode)
    );
    this.selectedBsp = this.defaultBsp;
  }

  private getDefaultAndSetSelectedBsp(bspList: Bsp[], user: User): void {
    this.defaultBsp = bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
    this.selectedBsp = this.defaultBsp;
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'isoCountryCode',
        name: 'MENU.AUDIT.ADM/ACM.columns.bsp',
        maxWidth: 80
      },
      {
        prop: 'transactionCode',
        name: 'MENU.AUDIT.ADM/ACM.columns.trnc'
      },
      {
        prop: 'documentNumber',
        name: 'MENU.AUDIT.ADM/ACM.columns.documentNumber',
        cellTemplate: 'commonLinkFromObjCellTmpl'
      },
      {
        prop: 'attribute',
        name: 'MENU.AUDIT.ADM/ACM.columns.attribute',
        sortable: false
      },
      {
        prop: 'oldValue',
        name: 'MENU.AUDIT.ADM/ACM.columns.oldValue',
        sortable: false
      },
      {
        prop: 'newValue',
        name: 'MENU.AUDIT.ADM/ACM.columns.newValue',
        sortable: false
      },
      {
        prop: 'time',
        name: 'MENU.AUDIT.ADM/ACM.columns.time',
        pipe: {
          transform: value => this.datePipe.transform(value, this.dateTimeFormat)
        }
      },
      {
        prop: 'typeUser',
        name: 'MENU.AUDIT.ADM/ACM.columns.userType',
        maxWidth: 110,
        pipe: {
          transform: value => capitalize(value)
        }
      },
      {
        prop: 'codeUser',
        name: 'MENU.AUDIT.ADM/ACM.columns.userCode',
        maxWidth: 120
      },
      {
        prop: 'emailUser',
        name: 'MENU.AUDIT.ADM/ACM.columns.email',
        hidden: this.loggedUserIsOfType(UserType.GDS)
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
