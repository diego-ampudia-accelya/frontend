import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AuditAdmAcm } from '../models/audit-adm-acm.model';
import { AuditAdmAcmFilterFormatter } from '../services/audit-adm-acm-filter-formatter';
import { AuditAdmAcmService } from '../services/audit-adm-acm.service';
import { AuditAdmAcmComponent } from './audit-adm-acm.component';
import { urlView } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { DialogService } from '~app/shared/components';

describe('AuditAdmAcmComponent', () => {
  let component: AuditAdmAcmComponent;
  let fixture: ComponentFixture<AuditAdmAcmComponent>;
  let datePipeMock: jasmine.SpyObj<DatePipe>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const AuditAdmAcmServiceSpy: SpyObject<AuditAdmAcmService> = createSpyObject(AuditAdmAcmService);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  const bspsDictionaryServiceSpy: SpyObject<BspsDictionaryService> = createSpyObject(BspsDictionaryService);
  const routerSpy: SpyObject<Router> = createSpyObject(Router);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { auditQuery: { ...ROUTES.AUDIT_ADM_ACM, id: 'auditQuery' } },
        activeTabId: 'auditQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  const bspListDropdownOptionsMock = [
    { value: { id: 7376, isoCountryCode: 'IL', name: 'ISRAEL' }, label: 'IL - ISRAEL' },
    { value: { id: 6554, isoCountryCode: 'A6', name: 'AUSTRALIA' }, label: 'A6 - AUSTRALIA' },
    { value: { id: 6671, isoCountryCode: 'BG', name: 'Bulgaria' }, label: 'BG - Bulgaria' },
    { value: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' }, label: 'ES - SPAIN' },
    { value: { id: 7784, isoCountryCode: 'MT', name: 'MALTA' }, label: 'MT - MALTA' }
  ] as DropdownOption<BspDto>[];

  const bspList = [
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      version: 3212
    },
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6984,
      isoCountryCode: 'MT',
      name: 'SPAIN',
      version: 3212
    }
  ] as Bsp[];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AuditAdmAcmComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        AuditAdmAcmFilterFormatter,
        FormBuilder,
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    })
      .overrideComponent(AuditAdmAcmComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: AuditAdmAcmService, useValue: AuditAdmAcmServiceSpy },
            { provide: PermissionsService, useValue: permissionsServiceSpy },
            { provide: Router, useValue: routerSpy },
            { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditAdmAcmComponent);
    component = fixture.componentInstance;
    permissionsServiceSpy.hasPermission.and.returnValue(true);
    datePipeMock = jasmine.createSpyObj('DatePipe', ['transform']);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call several methods in ngOnInit hook cycle', () => {
    const initColumnsSpy = spyOn<any>(component, 'initColumns');
    const buildFormSpy = spyOn<any>(component, 'buildForm');
    const initializePermissionsSpy = spyOn<any>(component, 'initializePermissions');
    const initializeBspFilterAndLoadDataSpy = spyOn<any>(component, 'initializeBspFilterAndLoadData');

    component.ngOnInit();

    expect(initColumnsSpy).toHaveBeenCalled();
    expect(buildFormSpy).toHaveBeenCalled();
    expect(initializePermissionsSpy).toHaveBeenCalled();
    expect(initializeBspFilterAndLoadDataSpy).toHaveBeenCalled();
  });

  it('should return a false when the user type checked is the different as the logged user', () => {
    const isAgentLogged = component['loggedUserIsOfType'](UserType.AGENT_GROUP);
    expect(isAgentLogged).toBe(false);
  });

  it('should get default and set selected Bsp For Iata Multicountry', () => {
    component['getDefaultAndSetSelectedBspForMcIata'](bspListDropdownOptionsMock, initialState.auth.user);

    expect(component['defaultBsp']).toEqual({
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    });
  });

  it('should call loadData method when the user susbcribes to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    const loadDataSpy = spyOn(component, 'loadData');
    spyOn<any>(component, 'initializeLeanBspFilterAndDefaultBsp$').and.returnValue(of({}));

    component['initializeBspFilterAndLoadData']();
    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(loadDataSpy).toHaveBeenCalled();
  }));

  it('should not set predefined filters when isBspFilterMultiple is not true and the user suscribed to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    const setPredefinedFiltersSpy = spyOn<any>(component, 'setPredefinedFilters');
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListDropdownOptionsMock]);

    spyOn<any>(component, 'getDefaultAndSetSelectedBspForMcIata').and.stub();

    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(setPredefinedFiltersSpy).not.toHaveBeenCalled();
  }));

  it('should get default and set selected BSP when the user suscribes to initializeLeanBspFilterAndDefaultBsp$ and it has not hasAllBspsPermission', fakeAsync(() => {
    component['hasAllBspsPermission'] = false;
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListDropdownOptionsMock]);
    const getDefaultAndSetSelectedBspSpy = spyOn<any>(component, 'getDefaultAndSetSelectedBsp');

    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(getDefaultAndSetSelectedBspSpy).toHaveBeenCalled();
  }));

  it('should router navigate and view the document clicked', () => {
    const eventDocumentMock = {
      isoCountryCode: null,
      documentNumber: '0622054558 6',
      typeUser: ['airline'],
      emailUser: 'tihomir.arahangelov@accelya.com',
      transactionCode: 'ADMA',
      attribute: ['Period'],
      time: [new Date()],
      dateFrom: '2023-05-18T16:51:38.849',
      dateTo: '2023-05-18T16:51:38.849',
      documentId: 698300000444603
    } as AuditAdmAcm;
    component.onViewDocument(eventDocumentMock);

    expect(routerSpy.navigate).toHaveBeenCalledWith([
      urlView[eventDocumentMock.transactionCode.toLowerCase()],
      eventDocumentMock.documentId
    ]);
  });

  it('should set default BSP by calling getDefaultAndSetSelectedBsp method', () => {
    const findSpy = spyOn(bspList, 'find').and.returnValue(bspList[0]);
    component['getDefaultAndSetSelectedBsp'](bspList, initialState.auth.user);

    expect(findSpy).toHaveBeenCalledWith(jasmine.any(Function));
    expect(component['defaultBsp']).toEqual(bspList[0]);
  });

  it('should check if Bsp filter is Locked', () => {
    component['hasAllBspsPermission'] = false;
    component['hasLeanPermission'] = false;
    const isBspFilterLocked = component['setBspFilterLockedCases'](bspListDropdownOptionsMock);

    expect(isBspFilterLocked).toBe(true);
  });

  it('should transform typeUser values using capitalize function', () => {
    const testValue = 'admin';
    const expectedTransformedValue = 'Admin';
    const typeUserColumn = component.columns.find(column => column.prop === 'typeUser');

    component['initColumns']();

    expect(typeUserColumn.pipe.transform(testValue)).toBe(expectedTransformedValue);
  });

  it('should transform time values using DatePipe', () => {
    const testTime = new Date('2022-01-01T12:34:56');
    const expectedFormattedTime = '01/01/2022 12:34:56';
    const timeColumn = component.columns.find(column => column.prop === 'time');

    datePipeMock.transform.and.returnValue(expectedFormattedTime);
    component['initColumns']();

    expect(timeColumn.pipe.transform(testTime)).toBe(expectedFormattedTime);
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
