import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuditAccessFilter } from '../models/audit-access-filter.model';
import { AuditAccess, AuditAccessBE } from '../models/audit-access.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class AuditAccessService implements Queryable<AuditAccess> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/audit/user-events`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<AuditAccessFilter>): Observable<PagedData<AuditAccess>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<AuditAccess>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<AuditAccessFilter>,
    format: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}/download`;
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: format.toUpperCase() };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(url + requestQuery.getQueryString({ omitPaging: true }), downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<AuditAccessFilter>>): RequestQuery<AuditAccessBE> {
    const { isoCountryCode, userType, operation, time, ...filterBy } = query.filterBy;

    const bspsFormatted = Array.isArray(isoCountryCode) ? isoCountryCode : [isoCountryCode];

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        isoCountryCode: isoCountryCode && bspsFormatted.map(bsp => bsp.isoCountryCode),
        userType: userType && userType.map(type => type.value.toUpperCase()),
        operation: operation && operation.map(o => o.value.toUpperCase()),
        dateFrom: time && toShortIsoDate(time[0]),
        dateTo: time && toShortIsoDate(time[1] ? time[1] : time[0])
      }
    });
  }
}
