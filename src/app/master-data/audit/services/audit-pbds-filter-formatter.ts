import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AuditPbdsFilter } from '../models/audit-pbds-filter.model';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { mapJoinAndRemoveUnderscoreMapper, mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };
@Injectable()
export class AuditPbdsFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translation: L10nTranslationService) {
    super();
  }

  public format(filter: AuditPbdsFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<AuditPbdsFilter>> = {
      isoCountryCode: bsps => {
        const bspsFormatted = Array.isArray(bsps) ? bsps : [bsps];

        return `${this.translate('bsp')} - ${bspsFormatted.map(bsp => bsp.isoCountryCode).join(', ')}`;
      },
      transactionCode: code => `${this.translate('trnc')} - ${code}`,
      documentNumber: documentNumber => `${this.translate('documentNumber')} - ${documentNumber}`,
      typeUser: type => `${this.translate('userType')} - ${mapJoinAndRemoveUnderscoreMapper(type, 'value')}`,
      emailUser: email => `${this.translate('email')} - ${email}`,
      codeUser: codeUser => `${this.translate('userCode')} - ${codeUser}`,
      attribute: attribute => `${this.translate('attribute')} - ${mapJoinMapper(attribute, 'value')}`,
      time: time => `${this.translate('time')} - ${rangeDateFilterTagMapper(time)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.AUDIT.PBDS.filter.label.${key}`);
  }
}
