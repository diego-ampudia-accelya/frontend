import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { AuditRefundsFilter } from '../models/audit-refunds-filter.model';
import { AuditRefunds } from '../models/audit-refunds.model';
import { AuditRefundsService } from './audit-refunds.service';

describe('AuditRefundsService', () => {
  let service: AuditRefundsService;
  let httpClientSpy: SpyObject<HttpClient>;

  const auditRecordsMock: AuditRefunds[] = [
    {
      documentId: 698300000447348,
      isoCountryCode: null,
      documentNumber: '6000001421 1',
      transactionCode: 'ADM_REQUEST',
      typeUser: ['agent'],
      emailUser: '',
      time: [new Date()],
      attribute: null,
      dateFrom: null,
      dateTo: null
    },
    {
      documentId: 698300000447348,
      isoCountryCode: null,
      documentNumber: '6000001421 1',
      transactionCode: 'ADM_REQUEST',
      typeUser: ['agent'],
      emailUser: '',
      time: [new Date()],
      attribute: null,
      dateFrom: new Date().toString(),
      dateTo: new Date().toString()
    }
  ];

  const queryCloneDeep = cloneDeep(defaultQuery);

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        AuditRefundsService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(AuditRefundsService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/audit/entity-events/refunds');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let auditReportData: PagedData<AuditRefunds>;

    const auditDataMock: PagedData<AuditRefunds> = {
      records: auditRecordsMock,
      total: auditRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };
    const query: DataQuery = {
      filterBy: {},
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      auditReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/audit/entity-events/refunds?page=0&size=20');
    expect(auditReportData).toEqual(auditDataMock);
  }));

  it('should return isoCountryCode formatted calling formatQuery method', () => {
    const isoCountryCodeFormatted = ['ES'];
    const time = [new Date(), new Date()];
    const attribute = ['attr1', 'attr2'];
    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        isoCountryCode: [{ isoCountryCode: 'ES' }],
        typeUser: [{ translationKey: 'Airline', value: 'Airline' }],
        attribute,
        dateTo: time,
        dateFrom: time,
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual(isoCountryCodeFormatted);
  });

  it('should send proper request when download() is called', fakeAsync(() => {
    const query: DataQuery<AuditRefundsFilter> = {
      ...queryCloneDeep,
      filterBy: {
        isoCountryCode: [{ isoCountryCode: 'ES', id: 5, name: 'Spain' }],
        transactionCode: '',
        documentNumber: '',
        time: [new Date('2023-07-13')],
        attribute: [{ translationKey: 'LOGIN', value: 'LOGIN' }],
        typeUser: [{ translationKey: 'Airline', value: 'Airline' }],
        codeUser: '',
        emailUser: '',
        ...queryCloneDeep.filterBy
      }
    };
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl =
      '/audit/entity-events/refunds/download?isoCountryCode=ES&typeUser=AIRLINE&attribute=LOGIN&dateFrom=2023-07-13&dateTo=2023-07-13&exportAs=TXT';

    service.download(query, exportOptions).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
