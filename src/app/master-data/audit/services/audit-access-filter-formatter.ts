import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AuditAccessFilter } from './../models/audit-access-filter.model';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { mapJoinAndRemoveUnderscoreMapper, mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };
@Injectable()
export class AuditAccessFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translation: L10nTranslationService) {
    super();
  }

  public format(filter: AuditAccessFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<AuditAccessFilter>> = {
      isoCountryCode: bsps => {
        const bspsFormatted = Array.isArray(bsps) ? bsps : [bsps];

        return `${this.translate('bsp')} - ${bspsFormatted.map(bsp => bsp.isoCountryCode).join(', ')}`;
      },
      userType: type => `${this.translate('userType')} - ${mapJoinAndRemoveUnderscoreMapper(type, 'value')}`,
      iataCode: iataCode => `${this.translate('iataCode')} - ${iataCode}`,
      email: email => `${this.translate('email')} - ${email}`,
      operation: operation => `${this.translate('eventType')} - ${mapJoinMapper(operation, 'value')}`,
      time: time => `${this.translate('time')} - ${rangeDateFilterTagMapper(time)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.AUDIT.APPLICATION_ACCESS.filter.label.${key}`);
  }
}
