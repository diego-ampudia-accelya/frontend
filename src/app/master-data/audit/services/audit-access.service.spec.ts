import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { AuditAccess } from '../models/audit-access.model';
import { AuditAccessService } from './audit-access.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('AuditAccessService', () => {
  let service: AuditAccessService;
  let httpClientSpy: SpyObject<HttpClient>;

  const auditRecordsMock: AuditAccess[] = [
    {
      isoCountryCode: null,
      email: 'test@email.com',
      operation: ['LOGIN'],
      time: null
    },
    {
      isoCountryCode: null,
      email: 'test@email.com',
      operation: ['LOGIN'],
      time: null
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const queryCloneDeep = cloneDeep(defaultQuery);
  const dataQuery: DataQuery = {
    ...queryCloneDeep,
    filterBy: {
      time: [new Date(), new Date()],
      isoCountryCode: [{ isoCountryCode: 'ES' }],
      operation: [{ translationKey: 'LOGIN', value: 'LOGIN' }],
      userType: [{ translationKey: 'Airline', value: 'Airline' }],
      ...queryCloneDeep.filterBy
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        AuditAccessService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(AuditAccessService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/audit/user-events');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let auditReportData: PagedData<AuditAccess>;

    const auditDataMock: PagedData<AuditAccess> = {
      records: auditRecordsMock,
      total: auditRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      auditReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/audit/user-events?page=0&size=20');
    expect(auditReportData).toEqual(auditDataMock);
  }));

  it('should return isoCountryCode formatted calling formatQuery method', () => {
    const isoCountryCodeFormatted = ['ES'];
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual(isoCountryCodeFormatted);
  });

  it('should send proper request when download() is called', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/audit/user-events/download?exportAs=TXT';

    service.download(query, exportOptions).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
