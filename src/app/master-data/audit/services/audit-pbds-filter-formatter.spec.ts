import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AuditPbdsFilter } from '../models/audit-pbds-filter.model';

import { AuditPbdsFilterFormatter } from './audit-pbds-filter-formatter';

describe('AuditPbdsFilterFormatter', () => {
  let formatter: AuditPbdsFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  const filters = {
    isoCountryCode: [
      {
        active: true,
        defaultCurrencyCode: null,
        effectiveFrom: '2000-01-01',
        effectiveTo: null,
        id: 6983,
        isoCountryCode: 'ES',
        name: 'SPAIN',
        version: 3127
      }
    ],
    transactionCode: 'a',
    documentNumber: '123',
    emailUser: '123@accelya.com',
    codeUser: '144',
    time: [new Date('2022-02-10')],
    attribute: [
      {
        translationKey: 'testing',
        value: 'testing'
      }
    ],
    typeUser: [
      {
        translationKey: 'iata',
        value: 'iata'
      }
    ]
  } as AuditPbdsFilter;

  const translateKey = 'MENU.AUDIT.PBDS.filter.label';

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new AuditPbdsFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['isoCountryCode'],
        label: `${translateKey}.bsp - ES`
      },
      {
        keys: ['transactionCode'],
        label: `${translateKey}.trnc - a`
      },
      {
        keys: ['documentNumber'],
        label: `${translateKey}.documentNumber - 123`
      },
      {
        keys: ['emailUser'],
        label: `${translateKey}.email - 123@accelya.com`
      },
      {
        keys: ['codeUser'],
        label: `${translateKey}.userCode - 144`
      },
      {
        keys: ['time'],
        label: `${translateKey}.time - 10/02/2022`
      },
      {
        keys: ['attribute'],
        label: `${translateKey}.attribute - testing`
      },
      {
        keys: ['typeUser'],
        label: `${translateKey}.userType - iata`
      }
    ]);
  });
});
