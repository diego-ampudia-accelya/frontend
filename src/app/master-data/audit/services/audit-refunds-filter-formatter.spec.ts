import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AuditRefundsFilter } from '../models/audit-refunds-filter.model';

import { AuditRefundsFilterFormatter } from './audit-refunds-filter-formatter';

describe('AuditRefundsFilterFormatter', () => {
  let formatter: AuditRefundsFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  const filters = {
    isoCountryCode: [
      {
        active: true,
        defaultCurrencyCode: null,
        effectiveFrom: '2000-01-01',
        effectiveTo: null,
        id: 6983,
        isoCountryCode: 'ES',
        name: 'SPAIN',
        version: 3127
      }
    ],
    transactionCode: 'a',
    documentNumber: '123',
    time: [new Date('2022-02-10')],
    attribute: [
      {
        translationKey: 'testing',
        value: 'testing'
      }
    ],
    emailUser: '123@accelya.com',
    codeUser: '001',
    typeUser: [
      {
        translationKey: 'iata',
        value: 'iata'
      }
    ]
  } as AuditRefundsFilter;

  const translateKey = 'MENU.AUDIT.REFUNDS.filter.label';

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new AuditRefundsFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['isoCountryCode'],
        label: `${translateKey}.bsp - ES`
      },
      {
        keys: ['transactionCode'],
        label: `${translateKey}.trnc - a`
      },
      {
        keys: ['documentNumber'],
        label: `${translateKey}.documentNumber - 123`
      },
      {
        keys: ['time'],
        label: `${translateKey}.time - 10/02/2022`
      },
      {
        keys: ['attribute'],
        label: `${translateKey}.attribute - testing`
      },
      {
        keys: ['emailUser'],
        label: `${translateKey}.email - 123@accelya.com`
      },
      {
        keys: ['codeUser'],
        label: `${translateKey}.userCode - 001`
      },
      {
        keys: ['typeUser'],
        label: `${translateKey}.userType - iata`
      }
    ]);
  });
});
