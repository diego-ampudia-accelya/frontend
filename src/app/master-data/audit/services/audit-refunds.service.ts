import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import { AuditRefundsFilter } from '../models/audit-refunds-filter.model';
import { AuditRefunds, AuditRefundsBE } from '../models/audit-refunds.model';

@Injectable()
export class AuditRefundsService implements Queryable<AuditRefunds> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/audit/entity-events/refunds`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<AuditRefundsFilter>): Observable<PagedData<AuditRefunds>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<AuditRefunds>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<AuditRefundsFilter>,
    format: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}/download`;
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: format.toUpperCase() };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(url + requestQuery.getQueryString({ omitPaging: true }), downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<AuditRefundsFilter>>): RequestQuery<AuditRefundsBE> {
    const { isoCountryCode, typeUser, attribute, time, ...filterBy } = query.filterBy;

    const bspsFormatted = Array.isArray(isoCountryCode) ? isoCountryCode : [isoCountryCode];

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        isoCountryCode: isoCountryCode && bspsFormatted.map(bsp => bsp.isoCountryCode),
        typeUser: typeUser && typeUser.map(type => type.value.toUpperCase()),
        attribute: attribute && attribute.map(attr => attr.value),
        dateFrom: time && toShortIsoDate(time[0]),
        dateTo: time && toShortIsoDate(time[1] ? time[1] : time[0])
      }
    });
  }
}
