import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AuditAccessFilter } from '../models/audit-access-filter.model';

import { AuditAccessFilterFormatter } from './audit-access-filter-formatter';

const filters = {
  isoCountryCode: [
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      version: 3127
    }
  ],
  email: 'testing@accelya.com',
  operation: [
    {
      translationKey: 'LOGIN',
      value: 'LOGIN'
    }
  ],
  time: [new Date('2022-02-10'), new Date('2022-02-12')],
  userType: [
    {
      translationKey: 'Iata',
      value: 'Iata'
    },
    {
      translationKey: 'Gds',
      value: 'Gds'
    }
  ],
  iataCode: '060'
} as AuditAccessFilter;

const translateKey = 'MENU.AUDIT.APPLICATION_ACCESS.filter.label';

describe('AuditAccessFilterFormatter', () => {
  let formatter: AuditAccessFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new AuditAccessFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['isoCountryCode'],
        label: `${translateKey}.bsp - ES`
      },
      {
        keys: ['email'],
        label: `${translateKey}.email - testing@accelya.com`
      },
      {
        keys: ['operation'],
        label: `${translateKey}.eventType - LOGIN`
      },
      {
        keys: ['time'],
        label: `${translateKey}.time - 10/02/2022 - 12/02/2022`
      },
      {
        keys: ['userType'],
        label: `${translateKey}.userType - Iata, Gds`
      },
      {
        keys: ['iataCode'],
        label: `${translateKey}.iataCode - 060`
      }
    ]);
  });
});
