import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { AuditAdmAcm } from '../models/audit-adm-acm.model';
import { AuditAdmAcmService } from './audit-adm-acm.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { DownloadFormat } from '~app/shared/models';

describe('AuditAdmAcmService', () => {
  let service: AuditAdmAcmService;
  let httpClientSpy: SpyObject<HttpClient>;

  const auditRecordsMock: AuditAdmAcm[] = [
    {
      documentId: 698300000447348,
      isoCountryCode: null,
      documentNumber: '6000001421 1',
      transactionCode: 'ADM_REQUEST',
      typeUser: ['agent'],
      emailUser: '',
      time: [new Date()],
      attribute: null,
      dateFrom: null,
      dateTo: null
    },
    {
      documentId: 698300000447348,
      isoCountryCode: null,
      documentNumber: '6000001421 1',
      transactionCode: 'ADM_REQUEST',
      typeUser: ['agent'],
      emailUser: '',
      time: [new Date()],
      attribute: null,
      dateFrom: new Date().toString(),
      dateTo: new Date().toString()
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const queryCloneDeep = cloneDeep(defaultQuery);
  const time = [new Date(), new Date()];
  const dataQuery: DataQuery = {
    ...queryCloneDeep,
    filterBy: {
      isoCountryCode: [{ isoCountryCode: 'ES' }],
      typeUser: [{ translationKey: 'Airline', value: 'Airline' }],
      attribute: ['attr1', 'attr2'],
      dateFrom: time,
      dateTo: time,
      ...queryCloneDeep.filterBy
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        AuditAdmAcmService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(AuditAdmAcmService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/audit/entity-events/acdms');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let auditReportData: PagedData<AuditAdmAcm>;

    const auditDataMock: PagedData<AuditAdmAcm> = {
      records: auditRecordsMock,
      total: auditRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      auditReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/audit/entity-events/acdms?page=0&size=20');
    expect(auditReportData).toEqual(auditDataMock);
  }));

  it('should return isoCountryCode formatted calling formatQuery method', () => {
    const isoCountryCodeFormatted = ['ES'];
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual(isoCountryCodeFormatted);
  });

  it('should send proper request when download() is called', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl =
      '/audit/entity-events/acdms/download?isoCountryCode=ES&typeUser=AIRLINE&attribute=undefined,undefined&exportAs=TXT';

    service.download(dataQuery, exportOptions).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
