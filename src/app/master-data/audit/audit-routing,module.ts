import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuditAccessComponent } from './audit-access/audit-access.component';
import { AuditAdmAcmComponent } from './audit-adm-acm/audit-adm-acm.component';
import { AuditPbdsComponent } from './audit-pbds/audit-pbds.component';
import { AuditRefundsComponent } from './audit-refunds/audit-refunds.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: ROUTES.AUDIT_APPLICATION_ACCESS.path,
    component: AuditAccessComponent,
    data: {
      tab: ROUTES.AUDIT_APPLICATION_ACCESS
    }
  },
  {
    path: ROUTES.AUDIT_ADM_ACM.path,
    component: AuditAdmAcmComponent,
    data: {
      tab: ROUTES.AUDIT_ADM_ACM
    }
  },
  {
    path: ROUTES.AUDIT_PBDS.path,
    component: AuditPbdsComponent,
    data: {
      tab: ROUTES.AUDIT_PBDS
    }
  },
  {
    path: ROUTES.AUDIT_REFUNDS.path,
    component: AuditRefundsComponent,
    data: {
      tab: ROUTES.AUDIT_REFUNDS
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditRoutingModule {}
