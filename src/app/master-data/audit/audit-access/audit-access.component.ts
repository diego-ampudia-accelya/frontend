import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEmpty } from 'lodash';
import { combineLatest, Observable, Subject } from 'rxjs';
import { first, map, takeUntil, tap } from 'rxjs/operators';

import {
  AuditAccessFilter,
  AuditDropdownFilterOption,
  AuditPredefinedFilters
} from '../models/audit-access-filter.model';
import { AuditAccess } from '../models/audit-access.model';
import { OperationTypeFilter } from '../models/operation-types-options';
import { UserTypeForFilter, USER_TYPE_FILTERNAME } from '../models/user-types-options';
import { AuditAccessFilterFormatter } from '../services/audit-access-filter-formatter';
import { AuditAccessService } from '../services/audit-access.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-audit-access',
  templateUrl: './audit-access.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: AuditAccessService }
  ]
})
export class AuditAccessComponent implements OnInit, OnDestroy {
  public title = this.translationService.translate('MENU.AUDIT.APPLICATION_ACCESS.title');
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public userBsps: DropdownOption<BspDto>[];
  public isBspFilterLocked: boolean;
  public isBspFilterMultiple: boolean;
  public operationOptionList: DropdownOption<AuditDropdownFilterOption>[];
  public userTypeOptionList: DropdownOption<AuditDropdownFilterOption>[];

  public predefinedFilters: AuditPredefinedFilters;
  public isLoggedUserTypeIata: boolean;
  public isLoggedUserTypeAgentGroup: boolean;

  private hasLeanPermission: boolean;
  private hasAllBspsPermission: boolean;
  private formFactory: FormUtil;
  private dateTimeFormat = 'dd/MM/yyyy HH:mm:ss';
  private defaultBsp: Bsp;
  private bspControl: FormControl;
  private destroy$ = new Subject();
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private loggedUser: User;
  private get selectedBsp(): Bsp {
    const bspControl = FormUtil.get<AuditAccessFilter>(this.searchForm, 'isoCountryCode');

    return bspControl.value;
  }
  private set selectedBsp(selectedBsp: Bsp) {
    const bspControl = FormUtil.get<AuditAccessFilter>(this.searchForm, 'isoCountryCode');
    const selectedBspAdapted = this.isBspFilterMultiple ? [selectedBsp] : selectedBsp;

    bspControl.setValue(selectedBspAdapted);
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(fromAuth.getUser), first());
  }

  constructor(
    public displayFormatter: AuditAccessFilterFormatter,
    public dataSource: QueryableDataSource<AuditAccess>,
    private datePipe: DatePipe,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private formBuilder: FormBuilder,
    private queryStorage: DefaultQueryStorage,
    private permissionService: PermissionsService,
    private store: Store<AppState>,
    private bspsDictionaryService: BspsDictionaryService,
    private auditAccessService: AuditAccessService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.initializeLoggedUser();
    this.initColumns();
    this.searchForm = this.buildForm();

    this.initializePermissions();
    this.initializeBspFilterAndLoadData();
    this.populateDropdownFilters();
  }

  public loggedUserIsOfType(userType: string): boolean {
    return userType.toUpperCase() === this.loggedUser.userType.toUpperCase();
  }

  public loadData(query: DataQuery<AuditAccessFilter>): void {
    query = query || cloneDeep(defaultQuery);
    let dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    if (isEmpty(dataQuery.sortBy)) {
      const sortBy = [{ attribute: 'time', sortType: SortOrder.Desc }];
      dataQuery = { ...dataQuery, sortBy };
    }

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.AUDIT.APPLICATION_ACCESS.downloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.auditAccessService
    });
  }

  private populateDropdownFilters(): void {
    this.operationOptionList = this.generateOptionList(OperationTypeFilter);
    this.userTypeOptionList = this.generateOptionList(UserTypeForFilter, USER_TYPE_FILTERNAME);
  }

  private generateOptionList(enumOptions: any, filterName?: string): DropdownOption<AuditDropdownFilterOption>[] {
    return Object.entries(enumOptions)
      .filter(([key, value]: [string, string]) => this.userTypeFilteringCallBack(value, filterName))
      .map(([key, val]: [string, string]) => ({
        value: { translationKey: key, value: val },
        label: this.translationService.translate(`MENU.AUDIT.APPLICATION_ACCESS.filter.label.${key}`)
      }));
  }

  private userTypeFilteringCallBack(optionValue: string, filterName: string): boolean | string {
    if (this.isLoggedUserTypeAgentGroup && filterName === USER_TYPE_FILTERNAME) {
      return (
        optionValue.toUpperCase() === UserType.AGENT_GROUP.toUpperCase() ||
        optionValue.toUpperCase() === UserType.AGENT.toUpperCase()
      );
    } else {
      return optionValue;
    }
  }

  private buildForm(): FormGroup {
    this.bspControl = new FormControl();

    return this.formFactory.createGroup<AuditAccessFilter>({
      isoCountryCode: this.bspControl,
      userType: [],
      iataCode: [],
      email: [],
      operation: [],
      time: []
    });
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      isoCountryCode: this.selectedBsp || this.defaultBsp
    };
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionService.hasPermission(Permissions.lean);
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private initializeLoggedUser() {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.setLoggedUserTypesVariables(loggedUser);
    });
  }

  private setLoggedUserTypesVariables(loggedUser: User): void {
    this.loggedUser = loggedUser;
    this.isLoggedUserTypeIata = this.loggedUserIsOfType(UserType.IATA);
    this.isLoggedUserTypeAgentGroup = this.loggedUserIsOfType(UserType.AGENT_GROUP);
  }

  private initializeBspFilterAndLoadData(): void {
    this.initializeLeanBspFilterAndDefaultBsp$().subscribe(() => {
      this.storedQuery = this.queryStorage.get();
      this.loadData(this.storedQuery);
    });
  }

  private initializeLeanBspFilterAndDefaultBsp$(): Observable<DropdownOption<BspDto>[]> {
    if (this.loggedUser.userType.toUpperCase() === UserType.IATA.toUpperCase() && this.hasAllBspsPermission) {
      return this.initializeMcIataBspFilterAndDefaultBsp$();
    } else {
      return combineLatest([this.store.select(fromAuth.getUserBsps), this.store.select(fromAuth.getUser)]).pipe(
        first(),
        tap(() => (this.isBspFilterMultiple = this.hasLeanPermission || this.hasAllBspsPermission)),
        map(([bspList, user]) => {
          this.getDefaultAndSetSelectedBsp(bspList, user);
          if (!this.isBspFilterMultiple || bspList.length === 1) {
            this.setPredefinedFilters();
          }

          return [bspList] as [Bsp[]];
        }),
        map(([bspList]) => bspList.map(bsp => toValueLabelObjectBsp(bsp))),
        tap(bspList => (this.userBsps = bspList)),
        tap(bspList => (this.isBspFilterLocked = this.setBspFilterLockedCases(bspList)))
      );
    }
  }

  private initializeMcIataBspFilterAndDefaultBsp$(): Observable<DropdownOption<BspDto>[]> {
    return combineLatest([
      this.bspsDictionaryService.getAllBspDropdownOptions(),
      this.store.select(fromAuth.getUser)
    ]).pipe(
      first(),
      tap(() => (this.isBspFilterMultiple = this.hasAllBspsPermission)),
      map(([bspList, user]) => {
        this.getDefaultAndSetSelectedBspForMcIata(bspList, user);

        if (!this.isBspFilterMultiple || bspList.length === 1) {
          this.setPredefinedFilters();
        }

        return bspList;
      }),
      tap(bspList => (this.userBsps = bspList)),
      tap(bspList => (this.isBspFilterLocked = this.setBspFilterLockedCases(bspList)))
    );
  }

  private setBspFilterLockedCases(bspList: DropdownOption<BspDto>[]): boolean {
    return (
      bspList.length === 1 ||
      (!this.hasLeanPermission && !this.isLoggedUserTypeIata) ||
      (!this.hasAllBspsPermission && this.isLoggedUserTypeIata)
    );
  }

  private getDefaultAndSetSelectedBspForMcIata(bspList: DropdownOption<BspDto>[], user: User): void {
    this.defaultBsp = user.bsps.find(bspUser =>
      bspList.some(bsp => bsp.value.isoCountryCode === bspUser.isoCountryCode)
    );
    this.selectedBsp = this.defaultBsp;
  }

  private getDefaultAndSetSelectedBsp(bspList: Bsp[], user: User): void {
    this.defaultBsp = bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
    this.selectedBsp = this.defaultBsp;
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'isoCountryCode',
        name: 'MENU.AUDIT.APPLICATION_ACCESS.columns.bsp'
      },
      {
        prop: 'userType',
        name: 'MENU.AUDIT.APPLICATION_ACCESS.columns.userType',
        hidden: !this.isLoggedUserTypeIata && !this.isLoggedUserTypeAgentGroup
      },
      {
        prop: 'iataCode',
        name: 'MENU.AUDIT.APPLICATION_ACCESS.columns.code',
        hidden: !this.isLoggedUserTypeIata && !this.isLoggedUserTypeAgentGroup
      },
      {
        prop: 'email',
        name: 'MENU.AUDIT.APPLICATION_ACCESS.columns.email'
      },
      {
        prop: 'operation',
        name: 'MENU.AUDIT.APPLICATION_ACCESS.columns.eventType'
      },
      {
        prop: 'time',
        name: 'MENU.AUDIT.APPLICATION_ACCESS.columns.time',
        pipe: {
          transform: value => this.datePipe.transform(value, this.dateTimeFormat)
        }
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
