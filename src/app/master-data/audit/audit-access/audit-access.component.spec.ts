import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { USER_TYPE_FILTERNAME } from '../models/user-types-options';
import { AuditAccessFilterFormatter } from '../services/audit-access-filter-formatter';
import { AuditAccessService } from '../services/audit-access.service';
import { AuditAccessComponent } from './audit-access.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('AuditAccessComponent', () => {
  let component: AuditAccessComponent;
  let fixture: ComponentFixture<AuditAccessComponent>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const auditServiceSpy: SpyObject<AuditAccessService> = createSpyObject(AuditAccessService);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  const bspsDictionaryServiceSpy: SpyObject<BspsDictionaryService> = createSpyObject(BspsDictionaryService);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { auditQuery: { ...ROUTES.AUDIT_APPLICATION_ACCESS, id: 'auditQuery' } },
        activeTabId: 'auditQuery'
      },
      viewListsInfo: {}
    }
  };

  const bspList = [
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      version: 3212
    },
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6984,
      isoCountryCode: 'MT',
      name: 'SPAIN',
      version: 3212
    }
  ] as Bsp[];

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  const bspListDropdownOptionsMock = [
    { value: { id: 7376, isoCountryCode: 'IL', name: 'ISRAEL' }, label: 'IL - ISRAEL' },
    { value: { id: 6554, isoCountryCode: 'A6', name: 'AUSTRALIA' }, label: 'A6 - AUSTRALIA' },
    { value: { id: 6671, isoCountryCode: 'BG', name: 'Bulgaria' }, label: 'BG - Bulgaria' },
    { value: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' }, label: 'ES - SPAIN' },
    { value: { id: 7784, isoCountryCode: 'MT', name: 'MALTA' }, label: 'MT - MALTA' }
  ] as DropdownOption<BspDto>[];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AuditAccessComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        AuditAccessFilterFormatter,
        FormBuilder,
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    })
      .overrideComponent(AuditAccessComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: AuditAccessService, useValue: auditServiceSpy },
            { provide: PermissionsService, useValue: permissionsServiceSpy },
            { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditAccessComponent);
    component = fixture.componentInstance;
    permissionsServiceSpy.hasPermission.and.returnValue(true);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call several methods in ngOnInit hook cycle when it is called', () => {
    const initColumnsSpy = spyOn<any>(component, 'initColumns');
    const buildFormSpy = spyOn<any>(component, 'buildForm');
    const initializePermissionsSpy = spyOn<any>(component, 'initializePermissions');
    const initializeBspFilterAndLoadDataSpy = spyOn<any>(component, 'initializeBspFilterAndLoadData');

    component.ngOnInit();

    expect(initColumnsSpy).toHaveBeenCalled();
    expect(buildFormSpy).toHaveBeenCalled();
    expect(initializePermissionsSpy).toHaveBeenCalled();
    expect(initializeBspFilterAndLoadDataSpy).toHaveBeenCalled();
  });

  it('should return a boolean when the logged user is an Agent Group and it matchs the userType filter name', () => {
    component.isLoggedUserTypeAgentGroup = true;
    const agentGroupFilteringCallBackValueReturned = component['userTypeFilteringCallBack'](
      UserType.AGENT_GROUP,
      USER_TYPE_FILTERNAME
    );
    const agentFilteringCallBackValueReturned = component['userTypeFilteringCallBack'](
      UserType.AGENT,
      USER_TYPE_FILTERNAME
    );

    expect(agentGroupFilteringCallBackValueReturned).toBe(true);
    expect(agentFilteringCallBackValueReturned).toBe(true);
  });

  it('should lock bsp filter when user is not Iata User, has not leaPermissions and bspList length > 1', () => {
    (component as any).hasLeanPermission = false;
    (component as any).isLoggedUserTypeIata = false;
    const bspListDropDown = bspList.map(bsp => toValueLabelObjectBsp(bsp));

    const result = component['setBspFilterLockedCases'](bspListDropDown);
    expect(result).toBe(true);
  });

  it('should lock bsp filter when Iata User has not AllBspsPermission and bspList length > 1', () => {
    (component as any).hasAllBspsPermission = false;
    (component as any).isLoggedUserTypeIata = true;
    const bspListDropDown = bspList.map(bsp => toValueLabelObjectBsp(bsp));

    const result = component['setBspFilterLockedCases'](bspListDropDown);
    expect(result).toBe(true);
  });

  it('should get default and set selected Bsp For Iata Multicountry', () => {
    component['getDefaultAndSetSelectedBspForMcIata'](bspListDropdownOptionsMock, initialState.auth.user);

    expect(component['defaultBsp']).toEqual({
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    });
  });

  it('should call loadData method when the user susbcribes to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    const loadDataSpy = spyOn(component, 'loadData');
    spyOn<any>(component, 'initializeLeanBspFilterAndDefaultBsp$').and.returnValue(of({}));

    component['initializeBspFilterAndLoadData']();
    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(loadDataSpy).toHaveBeenCalled();
  }));

  it('should not set predefined filters when isBspFilterMultiple is not true and the user suscribed to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    const setPredefinedFiltersSpy = spyOn<any>(component, 'setPredefinedFilters');
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListDropdownOptionsMock]);

    spyOn<any>(component, 'getDefaultAndSetSelectedBspForMcIata').and.stub();

    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(setPredefinedFiltersSpy).not.toHaveBeenCalled();
  }));

  it('should get default and set selected BSP when the user suscribes to initializeLeanBspFilterAndDefaultBsp$ and it has not hasAllBspsPermission', fakeAsync(() => {
    component['hasAllBspsPermission'] = false;
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListDropdownOptionsMock]);
    const getDefaultAndSetSelectedBspSpy = spyOn<any>(component, 'getDefaultAndSetSelectedBsp');

    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(getDefaultAndSetSelectedBspSpy).toHaveBeenCalled();
  }));

  it('should set default BSP by calling getDefaultAndSetSelectedBsp method', () => {
    component['getDefaultAndSetSelectedBsp'](bspList, initialState.auth.user);

    expect(component['defaultBsp']).toEqual(bspList[0]);
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
