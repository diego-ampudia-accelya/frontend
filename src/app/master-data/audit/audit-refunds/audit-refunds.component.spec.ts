import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { AuditPbds } from '../models/audit-pbds.model';
import { AuditRefundsFilterFormatter } from '../services/audit-refunds-filter-formatter';
import { AuditRefundsService } from '../services/audit-refunds.service';
import { AuditRefundsComponent } from './audit-refunds.component';

describe(' AuditRefundsComponent', () => {
  let component: AuditRefundsComponent;
  let fixture: ComponentFixture<AuditRefundsComponent>;

  let datePipeMock: jasmine.SpyObj<DatePipe>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const AuditRefundsServiceSpy: SpyObject<AuditRefundsService> = createSpyObject(AuditRefundsService);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  const bspsDictionaryServiceSpy: SpyObject<BspsDictionaryService> = createSpyObject(BspsDictionaryService);
  const routerSpy: SpyObject<Router> = createSpyObject(Router);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { auditQuery: { ...ROUTES.AUDIT_REFUNDS, id: 'auditQuery' } },
        activeTabId: 'auditQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  const bspListDropdownOptionsMock = [
    { value: { id: 7376, isoCountryCode: 'IL', name: 'ISRAEL' }, label: 'IL - ISRAEL' },
    { value: { id: 6554, isoCountryCode: 'A6', name: 'AUSTRALIA' }, label: 'A6 - AUSTRALIA' },
    { value: { id: 6671, isoCountryCode: 'BG', name: 'Bulgaria' }, label: 'BG - Bulgaria' },
    { value: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' }, label: 'ES - SPAIN' },
    { value: { id: 7784, isoCountryCode: 'MT', name: 'MALTA' }, label: 'MT - MALTA' }
  ] as DropdownOption<BspDto>[];

  const bspList = [
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      version: 3212
    },
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6984,
      isoCountryCode: 'MT',
      name: 'SPAIN',
      version: 3212
    }
  ] as Bsp[];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AuditRefundsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        AuditRefundsFilterFormatter,
        FormBuilder,
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    })
      .overrideComponent(AuditRefundsComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: AuditRefundsService, useValue: AuditRefundsServiceSpy },
            { provide: PermissionsService, useValue: permissionsServiceSpy },
            { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
            { provide: Router, useValue: routerSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditRefundsComponent);
    component = fixture.componentInstance;
    datePipeMock = jasmine.createSpyObj('DatePipe', ['transform']);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return a false when the user type checked is the different as the logged user', () => {
    const isAgentLogged = component['loggedUserIsOfType'](UserType.AGENT_GROUP);
    expect(isAgentLogged).toBe(false);
  });

  it('should call several methods in ngOnInit hook cycle', () => {
    const initColumnsSpy = spyOn<any>(component, 'initColumns');
    const buildFormSpy = spyOn<any>(component, 'buildForm');
    const initializePermissionsSpy = spyOn<any>(component, 'initializePermissions');
    const initializeBspFilterAndLoadDataSpy = spyOn<any>(component, 'initializeBspFilterAndLoadData');

    component.ngOnInit();

    expect(initColumnsSpy).toHaveBeenCalled();
    expect(buildFormSpy).toHaveBeenCalled();
    expect(initializePermissionsSpy).toHaveBeenCalled();
    expect(initializeBspFilterAndLoadDataSpy).toHaveBeenCalled();
  });

  it('should return a false when the user type checked is the different as the logged user', () => {
    const isAgentLogged = component['loggedUserIsOfType'](UserType.AGENT_GROUP);
    expect(isAgentLogged).toBe(false);
  });

  it('should get default and set selected Bsp For Iata Multicountry', () => {
    component['getDefaultAndSetSelectedBspForMcIata'](bspListDropdownOptionsMock, initialState.auth.user);

    expect(component['defaultBsp']).toEqual({
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    });
  });

  it('should check if Bsp filter is Locked', () => {
    component['hasAllBspsPermission'] = false;
    component['hasLeanPermission'] = false;
    const isBspFilterLocked = component['setBspFilterLockedCases'](bspListDropdownOptionsMock);

    expect(isBspFilterLocked).toBe(true);
  });

  it('should transform typeUser values using capitalize function', () => {
    const testValue = 'admin';
    const expectedTransformedValue = 'Admin';
    const typeUserColumn = component.columns.find(column => column.prop === 'typeUser');

    component['initColumns']();

    expect(typeUserColumn.pipe.transform(testValue)).toBe(expectedTransformedValue);
  });

  it('should transform time values using DatePipe', () => {
    const testTime = new Date('2022-01-01T12:34:56');
    const expectedFormattedTime = '01/01/2022 12:34:56';
    const timeColumn = component.columns.find(column => column.prop === 'time');

    datePipeMock.transform.and.returnValue(expectedFormattedTime);
    component['initColumns']();

    expect(timeColumn.pipe.transform(testTime)).toBe(expectedFormattedTime);
  });

  it('should router navigate and view the document clicked', () => {
    const eventDocumentMock = {
      isoCountryCode: null,
      documentNumber: '0622054558 6',
      typeUser: ['airline'],
      emailUser: 'tihomir.arahangelov@accelya.com',
      transactionCode: 'ADMA',
      attribute: ['Period'],
      time: [new Date()],
      dateFrom: '2023-05-18T16:51:38.849',
      dateTo: '2023-05-18T16:51:38.849',
      documentId: 698300000444603
    } as AuditPbds;
    component.onViewRefund(eventDocumentMock);

    expect(routerSpy.navigate).toHaveBeenCalledWith([
      ROUTES.REFUNDS_NOTICE_VIEW.url.toLowerCase(),
      eventDocumentMock.documentId
    ]);
  });

  it('should set default BSP by calling getDefaultAndSetSelectedBsp method', () => {
    component['getDefaultAndSetSelectedBsp'](bspList, initialState.auth.user);

    expect(component['defaultBsp']).toEqual(bspList[0]);
  });

  it('should predefinedFilters to be defined when setPredefinedFilters method is called', () => {
    component['setPredefinedFilters']();

    expect(component.predefinedFilters).toBeDefined();
  });

  it('should set predefined filters when the user has not All Bsps Permissions and suscribed to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    const setPredefinedFiltersSpy = spyOn<any>(component, 'setPredefinedFilters');
    permissionsServiceSpy.hasPermission.and.returnValue(false);
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListDropdownOptionsMock]);

    spyOn<any>(component, 'getDefaultAndSetSelectedBspForMcIata').and.stub();

    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(setPredefinedFiltersSpy).toHaveBeenCalled();
  }));

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
