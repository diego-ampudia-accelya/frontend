export enum AgentGroupIndividualAccess {
  Permitted = 'Yes',
  Forbidden = 'No'
}
