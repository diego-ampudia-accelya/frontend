/* eslint-disable @typescript-eslint/naming-convention */
export enum TaUnsavedChangesDialogType {
  ApplyChanges,
  UnsavedChanges
}
