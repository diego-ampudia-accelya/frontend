/* eslint-disable @typescript-eslint/naming-convention */
export enum AgentGroupStatus {
  ACTIVE = 'Active',
  DEACTIVATED = 'Deactivated'
}
