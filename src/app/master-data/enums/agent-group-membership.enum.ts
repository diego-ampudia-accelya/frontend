/* eslint-disable @typescript-eslint/naming-convention */
export enum AgentGroupMembership {
  Creator = 'Creator',
  Member = 'Member',
  Invited = 'Invited'
}
