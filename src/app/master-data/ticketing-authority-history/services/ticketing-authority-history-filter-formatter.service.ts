import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { bspFilterTagMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';
import { TicketingAuthorityHistoryFilter } from '../models/ticketing-authority-history.models';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class TicketingAuthorityHistoryFilterFormatter implements FilterFormatter {
  public ignoredFilters: Array<keyof TicketingAuthorityHistoryFilter> = [];

  constructor(private translation: L10nTranslationService) {}

  public format(filter: TicketingAuthorityHistoryFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<TicketingAuthorityHistoryFilter>> = {
      airline: airline => `${this.translate('airlineCode')} - ${airline.code}`,
      agent: agent => `${this.translate('agentCode')} - ${agent.code}`,
      bsp: bspValue => `${this.translate('bsp')} - ${bspFilterTagMapper(bspValue)}`,
      periodRange: periods => rangeDateFilterTagMapper(periods),
      action: action => `${this.translate('action')} - ${this.formatSwitchOptions(action)}`,
      performedBy: value => `${this.translate('performedBy')} - ${value}`,
      email: value => `${this.translate('email')} - ${value}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public formatSwitchOptions(value: boolean): string {
    return this.translation.translate(`LIST.MASTER_DATA.ticketingAuthorityHistory.actionValue.${value}`);
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.MASTER_DATA.ticketingAuthorityHistory.filters.labels.' + key);
  }
}
