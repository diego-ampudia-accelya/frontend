import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { downloadRequestOptions } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { AuthorType, TicketingAuthorityHistoryFilter } from '../models/ticketing-authority-history.models';
import { TicketingAuthorityHistoryListService } from './ticketing-authority-history-list.service';

const filterByQuery: TicketingAuthorityHistoryFilter = {
  periodRange: [new Date('03/30/2020'), new Date('04/26/2020')],
  airline: { id: 1, name: 'AIRLINE TEST', code: '001', designator: '' },
  agent: { id: '65540230010', name: 'AGENT TEST', code: '0230010' },
  bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
  action: true,
  performedBy: AuthorType.AIRLINE,
  email: 'testemail'
};
describe('TicketingAuthorityHistoryListService', () => {
  let service: TicketingAuthorityHistoryListService;
  let httpSpy: SpyObject<HttpClient>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [TicketingAuthorityHistoryListService, { provide: HttpClient, useValue: httpSpy }]
    });

    service = TestBed.inject(TicketingAuthorityHistoryListService);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should send proper find request', fakeAsync(() => {
    const query: DataQuery = {
      filterBy: filterByQuery,
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    };
    const expectedUrl =
      '/ta-management/tas-log?page=0&size=20&action.bsp.id=1&dateFrom=2020-03-30&dateTo=2020-04-26' +
      '&action.airline.iataCode=001&action.agent.iataCode=0230010&action.ticketingAuthority=true' +
      '&author.type=Airline&author.contact.email=testemail';

    service.find(query).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should download file ', fakeAsync(() => {
    const body: ArrayBuffer = new ArrayBuffer(1133825);
    const headers: Map<string, string> = new Map();
    headers.set('Content-Disposition', 'attachment;filename="File_Name_testing_123123.txt"');
    const response = {
      headers: {
        get: key => headers.get(key)
      },
      status: 200,
      statusText: 'OK',
      url: 'test',
      ok: true,
      type: 4,
      body
    };

    const query = {
      ...defaultQuery,
      filterBy: { ...defaultQuery.filterBy, action: true }
    } as DataQuery<TicketingAuthorityHistoryFilter>;

    httpSpy.get.and.returnValue(of(response));

    let fileDownloaded: any;
    service.download(query, DownloadFormat.TXT).subscribe(file => (fileDownloaded = file));
    tick();

    expect(fileDownloaded.fileName).toEqual('File_Name_testing_123123.txt');
    expect(httpSpy.get).toHaveBeenCalledWith(
      '/ta-management/tas-log?action.ticketingAuthority=true&exportAs=txt',
      downloadRequestOptions
    );
  }));
});
