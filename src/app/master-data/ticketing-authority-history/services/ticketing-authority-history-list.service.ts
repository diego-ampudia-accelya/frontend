import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse, formatPeriod } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { appConfiguration } from '~app/shared/services';
import {
  TicketingAuthorityHistory,
  TicketingAuthorityHistoryAttributes,
  TicketingAuthorityHistoryFilter
} from '../models/ticketing-authority-history.models';

@Injectable()
export class TicketingAuthorityHistoryListService implements Queryable<TicketingAuthorityHistory> {
  private baseUrl = `${appConfiguration.baseApiPath}/ta-management/tas-log`;

  constructor(private http: HttpClient) {}

  public find(query: DataQuery<{ [key: string]: any }>): Observable<PagedData<TicketingAuthorityHistory>> {
    return this.requestData(this.baseUrl, query);
  }

  public download(
    query: DataQuery<TicketingAuthorityHistoryFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = {
      ...requestQuery.filterBy,
      exportAs: downloadFormat
    };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(
        this.baseUrl + requestQuery.getQueryString({ omitPaging: true }),
        downloadRequestOptions
      )
      .pipe(map(formatDownloadResponse));
  }

  // eslint-disable-next-line @typescript-eslint/member-ordering
  @Cacheable()
  private requestData<T>(url: string, dataQuery: Partial<DataQuery>): Observable<T> {
    const formattedQuery = this.formatQuery(dataQuery);

    return this.http.get<T>(url + formattedQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery>): RequestQuery {
    const { airline, agent, bsp, action, performedBy, email } = query.filterBy;
    const periodRange = query.filterBy.periodRange || [];
    const [periodFrom, periodTo] = periodRange;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        [TicketingAuthorityHistoryAttributes.bspId]: bsp && (!Array.isArray(bsp) ? [bsp] : bsp).map(({ id }) => id),
        dateFrom: periodFrom && formatPeriod(periodFrom),
        dateTo: periodFrom && formatPeriod(periodTo || periodFrom),
        [TicketingAuthorityHistoryAttributes.airlineCode]: airline && airline.code,
        [TicketingAuthorityHistoryAttributes.agentCode]: agent && agent.code,
        [TicketingAuthorityHistoryAttributes.ticketingAuthority]: action,
        [TicketingAuthorityHistoryAttributes.authorType]: performedBy,
        [TicketingAuthorityHistoryAttributes.email]: email
      }
    });
  }
}
