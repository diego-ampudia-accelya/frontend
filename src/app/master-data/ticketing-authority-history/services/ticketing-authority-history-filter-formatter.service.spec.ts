import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { AuthorType, TicketingAuthorityHistoryFilter } from '../models/ticketing-authority-history.models';
import { TicketingAuthorityHistoryFilterFormatter } from './ticketing-authority-history-filter-formatter.service';

const filters: TicketingAuthorityHistoryFilter = {
  periodRange: [new Date('03/30/2020'), new Date('04/05/2020')],
  airline: { id: 1, name: 'AIRLINE TEST', code: '001', designator: '' },
  agent: { id: '65540230010', name: 'AGENT TEST', code: '0230010' },
  bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
  action: true,
  performedBy: AuthorType.AIRLINE,
  email: 'testemail'
};
describe('TicketingAuthorityHistoryFilterFormatterService', () => {
  let formatter: TicketingAuthorityHistoryFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new TicketingAuthorityHistoryFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      { keys: ['periodRange'], label: '30/03/2020 - 05/04/2020' },
      { keys: ['airline'], label: 'LIST.MASTER_DATA.ticketingAuthorityHistory.filters.labels.airlineCode - 001' },
      { keys: ['agent'], label: 'LIST.MASTER_DATA.ticketingAuthorityHistory.filters.labels.agentCode - 0230010' },
      { keys: ['bsp'], label: 'LIST.MASTER_DATA.ticketingAuthorityHistory.filters.labels.bsp - SPAIN (ES)' },
      {
        keys: ['action'],
        label:
          'LIST.MASTER_DATA.ticketingAuthorityHistory.filters.labels.action - LIST.MASTER_DATA.ticketingAuthorityHistory.actionValue.true'
      },
      {
        keys: ['performedBy'],
        label: 'LIST.MASTER_DATA.ticketingAuthorityHistory.filters.labels.performedBy - Airline'
      },
      { keys: ['email'], label: 'LIST.MASTER_DATA.ticketingAuthorityHistory.filters.labels.email - testemail' }
    ]);
  });
});
