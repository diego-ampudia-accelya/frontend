import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import moment from 'moment-mini';
import { Observable, Subject } from 'rxjs';
import { first, skipWhile, takeUntil } from 'rxjs/operators';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import {
  AuthorType,
  TicketingAuthorityHistory,
  TicketingAuthorityHistoryFilter
} from '../models/ticketing-authority-history.models';
import { TicketingAuthorityHistoryFilterFormatter } from '../services/ticketing-authority-history-filter-formatter.service';
import { TicketingAuthorityHistoryListService } from '../services/ticketing-authority-history-list.service';

const YEARS_BACK = 1;

@Component({
  selector: 'bspl-ticketing-authority-history-list',
  templateUrl: 'ticketing-authority-history-list.component.html',
  providers: [
    TicketingAuthorityHistoryFilterFormatter,
    DefaultQueryStorage,
    { provide: QUERYABLE, useClass: TicketingAuthorityHistoryListService },
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [TicketingAuthorityHistoryListService] }
  ]
})
export class TicketingAuthorityHistoryListComponent implements OnInit, OnDestroy {
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public formFactory: FormUtil;

  public userBspOptions: DropdownOption<Bsp>[] = [];
  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public airlineDropdownOptions$: Observable<DropdownOption<AirlineSummary>[]>;
  public actionOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.filterFormatter.formatSwitchOptions(value)
  }));
  public performedByOptions: DropdownOption[] = Object.values(AuthorType).map(value => ({
    value,
    label: this.translationService.translate(`LIST.MASTER_DATA.ticketingAuthorityHistory.performedByValue.${value}`)
  }));

  public isBspFilterLocked = false;
  public selectedBsp: Bsp;
  public predefinedFilters: any;
  public emailControlLock = false;

  public maxDate = this.localBspTimeService.getDate();
  public minDate = this.localBspTimeService.getDate(moment().subtract(YEARS_BACK, 'years').toDate());

  public loggedUser: User;
  public isGdsView = false;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }
  private hasLeanPermission: boolean;
  private destroy$ = new Subject();

  constructor(
    public filterFormatter: TicketingAuthorityHistoryFilterFormatter,
    public dataSource: QueryableDataSource<TicketingAuthorityHistory>,
    private queryStorage: DefaultQueryStorage,
    private datePipe: DatePipe,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private router: Router,
    private permissionsService: PermissionsService,
    private agentDictionaryService: AgentDictionaryService,
    private airlineDictionaryService: AirlineDictionaryService,
    private dialogService: DialogService,
    private ticketingAuthorityHistoryListService: TicketingAuthorityHistoryListService,
    private localBspTimeService: LocalBspTimeService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);

    this.initializeLoggedUser();
    this.initializePermissions();
  }

  public ngOnInit(): void {
    this.searchForm = this.buildSearchForm();
    this.columns = this.buildColumns();

    const storedQuery = this.queryStorage.get() || defaultQuery;

    this.initializeBspFeatures(storedQuery);

    this.populateFilterDropdowns();

    this.initializePerformedByListeners();
    this.setPredefinedFilters();
    this.loadData(storedQuery);
  }

  public loadData(query: DataQuery): void {
    query = query || cloneDeep(defaultQuery);

    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    const data = {
      title: this.translationService.translate('common.download'),
      footerButtonsType: FooterButton.Download,
      downloadQuery: this.queryStorage.get()
    };

    this.dialogService.open(DownloadFileComponent, {
      data,
      apiService: this.ticketingAuthorityHistoryListService
    });
  }

  public onViewAgent(row: TicketingAuthorityHistory): void {
    const agentId = row.action.agent.id;
    const url = `${ROUTES.AGENT_PROFILE.url}/${agentId}/properties`;
    this.router.navigateByUrl(url);
  }

  private initializeBspFeatures(storedQuery: DataQuery): void {
    if (storedQuery) {
      // Restore BSP selection if user change between open tabs
      this.selectedBsp = storedQuery.filterBy.bsp || this.selectedBsp;
    }

    if (this.hasLeanPermission) {
      this.initializeBspListener();
    }

    // Initialize BSP filter dropdown
    this.userBspOptions = this.filterBspsByPermission().map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    this.initializeSelectedBsps();

    // Initialize BSP related flags
    this.isBspFilterLocked = !this.hasLeanPermission || this.userBspOptions?.length === 1;
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readTicketingAuthorityHistory;
    const { bsps, bspPermissions } = this.loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }
  private initializePerformedByListeners() {
    FormUtil.get<TicketingAuthorityHistoryFilter>(this.searchForm, 'performedBy')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(value => {
        this.emailControlLock = false;
        if (value === AuthorType.SYSTEM) {
          FormUtil.get<TicketingAuthorityHistoryFilter>(this.searchForm, 'email').reset();
          this.emailControlLock = true;
        }
      });
  }

  private initializeBspListener(): void {
    FormUtil.get<TicketingAuthorityHistoryFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(value => {
        this.selectedBsp = value;

        this.populateFilterDropdowns();
        FormUtil.get<TicketingAuthorityHistoryFilter>(this.searchForm, 'agent').reset();
      });
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      bsp: this.selectedBsp
    };
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<TicketingAuthorityHistoryFilter>({
      bsp: [],
      action: [],
      airline: [],
      agent: [],
      periodRange: [],
      performedBy: [],
      email: []
    });
  }

  private initializeSelectedBsps(): void {
    const firstListBsp = this.userBspOptions[0].value;
    const defaultBsp = this.userBspOptions.find(
      ({ value }) => value.isoCountryCode === this.loggedUser.defaultIsoc
    )?.value;

    this.selectedBsp = defaultBsp || firstListBsp;
  }

  private populateFilterDropdowns(): void {
    if (this.selectedBsp) {
      const bspSelectedId = this.selectedBsp.id;
      const params = bspSelectedId ? { bspId: bspSelectedId } : null;

      this.agentDropdownOptions$ = this.agentDictionaryService.getDropdownOptions(params);
      this.airlineDropdownOptions$ = this.airlineDictionaryService.getDropdownOptions(bspSelectedId || null);
    }
  }

  private buildColumns(): GridColumn[] {
    return [
      {
        prop: 'action.bsp.isoCountryCode',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.bsp',
        resizeable: true,
        sortable: false,
        flexGrow: 1,
        hidden: this.isGdsView
      },
      {
        prop: 'action.airline.iataCode',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.airlineCode',
        resizeable: true,
        draggable: false,
        sortable: true,
        flexGrow: 2,
        hidden: !this.isGdsView
      },
      {
        prop: 'action.airline.name',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.airlineName',
        resizeable: true,
        sortable: false,
        flexGrow: 2,
        hidden: !this.isGdsView
      },
      {
        prop: 'action.agent.iataCode',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.agentCode',
        resizeable: true,
        draggable: false,
        sortable: true,
        flexGrow: 2
      },
      {
        prop: 'action.agent.name',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.agentName',
        resizeable: true,
        sortable: false,
        flexGrow: 2,
        cellTemplate: 'commonLinkFromObjCellTmpl'
      },
      {
        prop: 'date',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.date',
        resizeable: true,
        sortable: false,
        pipe: { transform: date => this.datePipe.transform(date, 'dd/MM/yyyy - HH:mm:ss') },
        flexGrow: 2
      },
      {
        prop: 'action.ticketingAuthority',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.action',
        sortable: false,
        pipe: {
          transform: value =>
            this.translationService.translate(
              `LIST.MASTER_DATA.ticketingAuthorityHistory.actionValue.${value.toString()}`
            )
        },
        resizeable: true,
        flexGrow: 1
      },
      {
        prop: 'author.type',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.performedBy',
        sortable: false,
        resizeable: true,
        flexGrow: 1
      },
      {
        prop: 'author.contact.email',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.email',
        sortable: false,
        resizeable: true,
        flexGrow: 2
      }
    ];
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.isGdsView = this.loggedUser.userType === UserType.GDS;
    });
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
