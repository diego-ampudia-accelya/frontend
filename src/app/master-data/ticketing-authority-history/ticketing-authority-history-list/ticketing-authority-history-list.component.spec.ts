import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';
import { AuthorType, TicketingAuthorityHistoryFilter } from '../models/ticketing-authority-history.models';
import { TicketingAuthorityHistoryFilterFormatter } from '../services/ticketing-authority-history-filter-formatter.service';
import { TicketingAuthorityHistoryListService } from '../services/ticketing-authority-history-list.service';
import { TicketingAuthorityHistoryListComponent } from './ticketing-authority-history-list.component';

const expectedUserDetails: Partial<User> = {
  id: 10126,
  email: 'airline1@example.com',
  firstName: 'firstName',
  lastName: 'lastName',
  userType: UserType.AIRLINE,
  defaultIsoc: 'ES',
  permissions: [Permissions.readTicketingAuthorityHistory, Permissions.lean],
  bspPermissions: [
    { bspId: 1, permissions: [Permissions.readTicketingAuthorityHistory] },
    { bspId: 2, permissions: [] }
  ],
  bsps: [
    { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
    { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
  ]
};

const initialState = {
  auth: {
    user: expectedUserDetails
  },
  core: {
    menu: {
      tabs: { dashboard: { ...ROUTES.DASHBOARD, id: 'dashboard' } },
      activeTabId: 'dashboard'
    },
    viewListsInfo: {}
  }
};
const query = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery;

const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
  {
    value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
    label: '1111111 / AGENT 1111111'
  },
  {
    value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
    label: '2222222 / AGENT 2222222'
  }
];

const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
  {
    value: { id: 1, name: 'AIRLINE 1111111', code: '111', designator: '' },
    label: '1111111 / AGENT 1111111'
  },
  {
    value: { id: 2, name: 'AIRLINE 2222222', code: '222', designator: '' },
    label: '2222222 / AGENT 2222222'
  }
];

describe('TicketingAuthorityHistoryListComponent', () => {
  let component: TicketingAuthorityHistoryListComponent;
  let fixture: ComponentFixture<TicketingAuthorityHistoryListComponent>;
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, { appliedQuery$: of(query) });
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const periodServiceSpy = createSpyObject(PeriodService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);
  let mockStore: MockStore<AppState>;

  queryableDataSourceSpy.hasData$ = of(true);
  queryableDataSourceSpy.appliedQuery$ = of(query);
  agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));
  airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TicketingAuthorityHistoryListComponent, TranslatePipeMock],
      imports: [],
      providers: [
        TicketingAuthorityHistoryFilterFormatter,
        FormBuilder,
        PermissionsService,
        mockProvider(LocalBspTimeService),
        mockProvider(DatePipe),
        mockProvider(L10nTranslationService),
        mockProvider(TicketingAuthorityHistoryListService),
        provideMockStore({ initialState, selectors: [{ selector: getUser, value: expectedUserDetails }] }),
        { provide: PeriodPipe, useClass: PeriodPipeMock },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(TicketingAuthorityHistoryListComponent, {
        set: {
          providers: [
            TicketingAuthorityHistoryFilterFormatter,
            mockProvider(TicketingAuthorityHistoryListService),
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    mockStore = TestBed.inject<any>(Store);
    fixture = TestBed.createComponent(TicketingAuthorityHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(expectedUserDetails as User);
  });

  it('should set correct columns on init as an AIRLINE', fakeAsync(() => {
    const expectedColumns = jasmine.arrayContaining<any>([
      jasmine.objectContaining({
        prop: 'action.bsp.isoCountryCode',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.bsp',
        resizeable: true,
        sortable: false,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'action.agent.iataCode',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.agentCode',
        resizeable: true,
        draggable: false,
        sortable: true,
        flexGrow: 2
      }),
      jasmine.objectContaining({
        prop: 'action.agent.name',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.agentName',
        resizeable: true,
        sortable: false,
        flexGrow: 2
      }),
      jasmine.objectContaining({
        prop: 'date',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.date',
        resizeable: true,
        sortable: false,
        pipe: jasmine.anything(),
        flexGrow: 2
      }),
      jasmine.objectContaining({
        prop: 'action.ticketingAuthority',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.action',
        sortable: false,
        pipe: jasmine.anything(),
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'author.type',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.performedBy',
        sortable: false,
        resizeable: true,
        flexGrow: 1
      }),
      jasmine.objectContaining({
        prop: 'author.contact.email',
        name: 'LIST.MASTER_DATA.ticketingAuthorityHistory.columns.email',
        sortable: false,
        resizeable: true,
        flexGrow: 2
      })
    ]);

    component.ngOnInit();
    const columns = component['buildColumns']();
    tick();
    expect(columns).toEqual(expectedColumns);
  }));

  it('should set default value BSP', fakeAsync(() => {
    const expectedBsp = { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' };

    component.ngOnInit();
    tick();

    expect(component.selectedBsp).toEqual(expectedBsp);
  }));

  it('should set predefined filters', fakeAsync(() => {
    const expectedPredefinedFilters = {
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
    };

    component.ngOnInit();
    tick();

    expect(component.predefinedFilters).toEqual(expectedPredefinedFilters);
  }));

  it('should initialize search form', fakeAsync(() => {
    const expectedSearchForm = {
      bsp: null,
      action: null,
      agent: null,
      airline: null,
      periodRange: null,
      performedBy: null,
      email: null
    };

    component.ngOnInit();
    tick();

    expect(component.searchForm.value).toEqual(expectedSearchForm);
  }));

  it('should initialize BSP listener if user has LEAN permission', fakeAsync(() => {
    const mockBsp = { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' };

    component.ngOnInit();
    component.searchForm.get('bsp').patchValue(mockBsp);
    tick();

    expect(component.selectedBsp).toEqual(mockBsp);
  }));

  it('should initialize PerformedBy listener to lock email input filter', fakeAsync(() => {
    component.ngOnInit();
    component.searchForm.get('performedBy').patchValue(AuthorType.SYSTEM);
    tick();

    expect(component.emailControlLock).toBeTruthy();
  }));

  describe('load data', () => {
    const storedQuery: DataQuery<TicketingAuthorityHistoryFilter> = {
      ...defaultQuery
    };

    beforeEach(() => {
      queryStorageSpy.get.and.returnValue(storedQuery);
    });

    it('should build query with predefined filters', fakeAsync(() => {
      const expectedQuery = {
        filterBy: jasmine.objectContaining({
          ...storedQuery.filterBy,
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        }),
        paginateBy: storedQuery.paginateBy,
        sortBy: storedQuery.sortBy
      };

      component.ngOnInit();
      tick();

      expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expectedQuery);
      expect(queryStorageSpy.save).toHaveBeenCalledWith(expectedQuery);
    }));
  });

  describe('filter dropdowns initialization', () => {
    beforeEach(() => {
      agentDictionaryServiceSpy.getDropdownOptions.calls.reset();
    });

    it('should initialize AGENT dropdown', fakeAsync(() => {
      let agentDropdownOptions: DropdownOption<AgentSummary>[];

      component.ngOnInit();
      tick();

      component.agentDropdownOptions$.subscribe(options => (agentDropdownOptions = options));

      expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith({ bspId: 1 });
      expect(agentDropdownOptions).toEqual(mockAgentDropdownOptions);
    }));
  });

  it('should initialize AIRLINE dropdown', fakeAsync(() => {
    airlineDictionaryServiceSpy.getDropdownOptions.calls.reset();
    let airlineDropdownOptions: DropdownOption<AirlineSummary>[];

    component.ngOnInit();
    tick();

    component.airlineDropdownOptions$.subscribe(options => (airlineDropdownOptions = options));

    expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith(1);
    expect(airlineDropdownOptions).toEqual(mockAirlineDropdownOptions);
  }));

  it('should open download dialog correctly on download', () => {
    queryStorageSpy.get.and.returnValue(query);
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should set isGdsView to false when user is airline', fakeAsync(() => {
    component.isGdsView = true;

    (component as any).initializeLoggedUser();
    tick();

    expect(component.isGdsView).toBe(false);
  }));

  it('should set isGdsView to true when user is GDS', fakeAsync(() => {
    component.isGdsView = false;
    mockStore.overrideSelector(getUser, { ...expectedUserDetails, userType: UserType.GDS } as any);

    (component as any).initializeLoggedUser();
    tick();

    expect(component.isGdsView).toBe(true);
  }));
});
