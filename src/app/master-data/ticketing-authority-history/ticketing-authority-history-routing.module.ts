import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TicketingAuthorityHistoryListComponent } from './ticketing-authority-history-list/ticketing-authority-history-list.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: TicketingAuthorityHistoryListComponent,
    data: {
      tab: ROUTES.TICKETING_AUTHORITY_HISTORY
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketingAuthorityHistoryRoutingModule {}
