import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TicketingAuthorityHistoryListService } from './services/ticketing-authority-history-list.service';
import { TicketingAuthorityHistoryListComponent } from './ticketing-authority-history-list/ticketing-authority-history-list.component';
import { TicketingAuthorityHistoryRoutingModule } from './ticketing-authority-history-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [TicketingAuthorityHistoryListComponent],
  imports: [CommonModule, SharedModule, TicketingAuthorityHistoryRoutingModule],
  providers: [TicketingAuthorityHistoryListService]
})
export class TicketingAuthorityHistoryModule {}
