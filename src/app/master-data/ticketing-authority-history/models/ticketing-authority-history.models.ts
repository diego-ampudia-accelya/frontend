/* eslint-disable @typescript-eslint/naming-convention */
import { ActivityContact } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';

export interface TicketingAuthorityHistory {
  action: TicketingAuthorityAction;
  author: Author;
  date: string;
}

export interface TicketingAuthorityAction {
  id: number;
  agent: { id: number; iataCode: string; name: string };
  airline: { id: number; iataCode: string; name: string };
  bsp: BspDto;
  ticketingAuthority: boolean;
}

export interface Author {
  contact: ActivityContact;
  type: AuthorType;
  code: string;
}

export enum AuthorType {
  SYSTEM = 'System',
  AIRLINE = 'Airline',
  IATA = 'IATA'
}

export interface TicketingAuthorityHistoryFilter {
  bsp: Bsp;
  airline: AirlineSummary;
  agent: AgentSummary;
  periodRange: Date[];
  action: boolean;
  performedBy: string;
  email: string;
}

export enum TicketingAuthorityHistoryAttributes {
  bspId = 'action.bsp.id',
  airlineCode = 'action.airline.iataCode',
  agentCode = 'action.agent.iataCode',
  ticketingAuthority = 'action.ticketingAuthority',
  authorType = 'author.type',
  email = 'author.contact.email'
}
