import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ViewAs } from './models/view-as.model';
import { VariableRemittanceComponent } from './variable-remittance/variable-remittance.component';
import { CanComponentDeactivateGuard } from '~app/core/services';
import { Permissions, ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: VariableRemittanceComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      tab: ROUTES.VARIABLE_REMITTANCE,
      title: 'LIST.MASTER_DATA.variableRemittance.tabLabel',
      viewAs: ViewAs.Iata,
      requiredPermissions: [
        Permissions.readOverrideFrequency,
        Permissions.createOverrideFrequency,
        Permissions.updateOverrideFrequency,
        Permissions.deleteOverrideFrequency
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VariableRemittanceRoutingModule {}
