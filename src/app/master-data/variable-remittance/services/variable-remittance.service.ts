import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { VariableRemittanceFilter } from '../models/variable-remittance-filter.model';
import { VariableRemittance } from '../models/variable-remittance.model';
import { ViewAs } from '../models/view-as.model';
import { Configuration } from '~app/master-data/configuration/models/configuration.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ArrayHelper, downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class VariableRemittanceService implements Queryable<VariableRemittance> {
  private baseUrl: string;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public buildBaseUrl(viewAs: ViewAs): void {
    const isIataView: boolean = viewAs === ViewAs.Iata;

    this.baseUrl = isIataView
      ? `${this.appConfiguration.baseApiPath}/variable-remittance/frequencies/override`
      : `${this.appConfiguration.baseApiPath}/variable-remittance/frequencies`;
  }

  public find(query: DataQuery<VariableRemittanceFilter>): Observable<PagedData<VariableRemittance>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<VariableRemittance>>(this.baseUrl + requestQuery.getQueryString());
  }

  public save(items: VariableRemittance[]): Observable<VariableRemittance[]> {
    return this.http.put<VariableRemittance[]>(this.baseUrl, items);
  }

  public create(item: Partial<VariableRemittance>): Observable<any> {
    return this.http.post(this.baseUrl, item);
  }

  public delete(item: VariableRemittance): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${item.id}`);
  }

  public download(
    query: DataQuery<VariableRemittanceFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<VariableRemittanceFilter>): RequestQuery {
    const { bsp, airline, agent, frequency } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        bspId: bsp && bsp.id,
        'airline.iataCode': airline && airline.map(item => item.code),
        'agent.iataCode': agent && agent.map(item => item.code),
        frequency
      }
    });
  }

  @Cacheable()
  private getFrequencies(bspId: number): Observable<Array<number>> {
    const url = `${this.appConfiguration.baseApiPath}/bsp-management/bsps/${bspId}/configuration`;
    const params = { name: 'BVALID_REMITTANCE_FREQUENCIES' };

    return this.http
      .get<Configuration>(url, { params })
      .pipe(map(({ parameters }) => parameters && (parameters[0]?.value as string[]).map(freq => +freq)));
  }

  public getFrequencyDropdownOptions(bspId: number): Observable<DropdownOption<number>[]> {
    return this.getFrequencies(bspId).pipe(map(ArrayHelper.toDropdownOptions));
  }
}
