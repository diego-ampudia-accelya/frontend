import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { VariableRemittanceFilter } from '../models/variable-remittance-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { agentFilterTagMapper, airlineFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class VariableRemittanceFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: VariableRemittanceFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<VariableRemittanceFilter>> = {
      bsp: bsp => `${this.translate('bsp')} - ${bsp.isoCountryCode}`,
      airline: airline => airlineFilterTagMapper(airline).join(', '),
      agent: agent => agentFilterTagMapper(agent).join(', '),
      frequency: frequency => `${this.translate('frequency')} - ${frequency.join(', ')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`LIST.MASTER_DATA.variableRemittance.filters.${key}.label`);
  }
}
