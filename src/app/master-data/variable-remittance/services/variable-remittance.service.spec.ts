import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { globalCacheBusterNotifier } from 'ts-cacheable';

import { VariableRemittanceFilter } from '../models/variable-remittance-filter.model';
import { Frequency, VariableRemittance } from '../models/variable-remittance.model';
import { ViewAs } from '../models/view-as.model';
import { VariableRemittanceService } from './variable-remittance.service';
import { ParameterType } from '~app/master-data/configuration/enum/parameter-type.enum';
import { Configuration } from '~app/master-data/configuration/models/configuration.model';
import { DataQuery } from '~app/shared/components/list-view';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

describe('VariableRemittanceService', () => {
  let service: VariableRemittanceService;

  const httpClientSpy = createSpyObject(HttpClient);
  const baseQuery: DataQuery<VariableRemittanceFilter> = {
    filterBy: {},
    sortBy: [],
    paginateBy: { size: 20, page: 0 }
  };

  const configuration: Configuration = {
    id: 6983,
    descriptorId: 6983,
    scopeId: '6983',
    scopeType: 'bsps',
    service: 'Basic Configuration',
    parameters: [
      {
        id: 69830036,
        descriptorId: 69830036,
        type: ParameterType.StringList,
        name: 'BVALID_REMITTANCE_FREQUENCIES',
        value: ['1', '2', '4'],
        readonly: false
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VariableRemittanceService, AppConfigurationService, { provide: HttpClient, useValue: httpClientSpy }]
    });
  });

  beforeEach(() => {
    httpClientSpy.get.and.returnValue(of());
    service = TestBed.inject(VariableRemittanceService);

    service.buildBaseUrl(ViewAs.Iata);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should build base url depending on view as parameter', () => {
    // ViewAs: IATA
    service.buildBaseUrl(ViewAs.Iata);
    expect(service['baseUrl']).toContain('/variable-remittance/frequencies/override');

    // ViewAs: not IATA
    service.buildBaseUrl(ViewAs.Agent);
    expect(service['baseUrl']).not.toContain('/override');
  });

  describe('find', () => {
    beforeEach(() => httpClientSpy.get.calls.reset());

    it('should call variable remittance endpoint', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/variable-remittance/frequencies'));
    }));

    it('should include pagination parameters in request', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('page=0&size=20'));
    }));

    it('should include AIRLINE filters in request when specified', fakeAsync(() => {
      const query: DataQuery<VariableRemittanceFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          airline: [
            { id: 1, code: '001', name: 'Airline 1', designator: 'DC' },
            { id: 2, code: '002', name: 'Airline 2', designator: 'AC' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('airline.iataCode=001,002'));
    }));

    it('should include AGENT filters in request when specified', fakeAsync(() => {
      const query: DataQuery<VariableRemittanceFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          agent: [
            { id: '1', name: 'AGENT 78200001', code: '78200001' },
            { id: '2', name: 'AGENT 78200002', code: '78200002' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agent.iataCode=78200001,78200002'));
    }));

    it('should include FREQUENCY filters in request when specified', fakeAsync(() => {
      const query: DataQuery<VariableRemittanceFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          frequency: [Frequency.Two, Frequency.Three]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('frequency=2,3'));
    }));
  });

  it('should call variable remittance SAVE endpoint with correct options', fakeAsync(() => {
    const items: VariableRemittance[] = [
      {
        id: '69834855557820010',
        bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
        airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
        agent: { id: 69837820010, iataCode: '7820010', name: 'NOMBRE 7820010' },
        frequency: Frequency.Two
      },
      {
        id: '69834855557820012',
        bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
        airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
        agent: { id: 69837820012, iataCode: '7820012', name: 'NOMBRE 7820012' },
        frequency: Frequency.Two
      }
    ];

    httpClientSpy.put.and.returnValue(of());
    service.save(items).subscribe();

    expect(httpClientSpy.put).toHaveBeenCalledWith(jasmine.stringMatching('/variable-remittance/frequencies'), items);
  }));

  it('should call variable remittance CREATE endpoint with correct options', fakeAsync(() => {
    const item: Partial<VariableRemittance> = {
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      agent: { id: 69837820010, iataCode: '7820010', name: 'NOMBRE 7820010' },
      frequency: Frequency.Two
    };

    httpClientSpy.post.and.returnValue(of());
    service.create(item).subscribe();

    expect(httpClientSpy.post).toHaveBeenCalledWith(jasmine.stringMatching('/variable-remittance/frequencies'), item);
  }));

  it('should call variable remittance DELETE endpoint', fakeAsync(() => {
    const item: VariableRemittance = {
      id: '69834855557820010',
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
      agent: { id: 69837820010, iataCode: '7820010', name: 'NOMBRE 7820010' },
      frequency: Frequency.Two
    };

    httpClientSpy.delete.and.returnValue(of());
    service.delete(item).subscribe();

    expect(httpClientSpy.delete).toHaveBeenCalledWith(
      jasmine.stringMatching(`${service['baseUrl']}/69834855557820010`)
    );
  }));

  describe('download', () => {
    beforeEach(() => httpClientSpy.get.calls.reset());

    it('should call variable remittance endpoint with correct options', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/download'), {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));

    it('should include actual query in request', fakeAsync(() => {
      const query: DataQuery<VariableRemittanceFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          frequency: [Frequency.Two, Frequency.Three]
        }
      };

      service.download(query, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('page=0&size=20&frequency=2,3'),
        jasmine.anything()
      );
    }));

    it('should call endpoint with specified download format', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.TXT).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('exportAs=txt'), jasmine.anything());
    }));
  });

  describe('get frequencies', () => {
    beforeEach(() => {
      httpClientSpy.get.and.returnValue(of(configuration));
      globalCacheBusterNotifier.next();
    });

    it('should call bsp management endpoint for frequencies', fakeAsync(() => {
      service.getFrequencyDropdownOptions(1).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/bsp-management/bsps/1/configuration'), {
        params: { name: 'BVALID_REMITTANCE_FREQUENCIES' }
      });
    }));

    it('should map bsp configuration response to frequency number array', fakeAsync(() => {
      let frequencies: Array<number>;

      service['getFrequencies'](1).subscribe(results => (frequencies = results));
      expect(frequencies).toEqual([1, 2, 4]);
    }));

    it('should map frequencies to dropdown options', fakeAsync(() => {
      let options: DropdownOption[];

      service.getFrequencyDropdownOptions(1).subscribe(results => (options = results));
      expect(options).toEqual([
        { value: 1, label: '1' },
        { value: 2, label: '2' },
        { value: 4, label: '4' }
      ]);
    }));
  });
});
