import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { VariableRemittanceFilter } from '../models/variable-remittance-filter.model';
import { Frequency } from '../models/variable-remittance.model';

import { VariableRemittanceFormatter } from './variable-remittance-formatter.service';

describe('VariableRemittanceFormatter', () => {
  let formatter: VariableRemittanceFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new VariableRemittanceFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format airlines if they exist', () => {
    const filter: VariableRemittanceFilter = {
      airline: [
        { id: 1, name: 'AIRLINE NAME 220', code: '220', designator: 'L8' },
        { id: 2, name: 'AIRLINE NAME 005', code: '005', designator: 'L5' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airline'],
        label: '220 / AIRLINE NAME 220, 005 / AIRLINE NAME 005'
      }
    ]);
  });

  it('should format agents if they exist', () => {
    const filter: VariableRemittanceFilter = {
      agent: [
        { id: '1', name: 'AGENT 78200001', code: '78200001' },
        { id: '2', name: 'AGENT 78200002', code: '78200002' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agent'],
        label: '78200001 / AGENT 78200001, 78200002 / AGENT 78200002'
      }
    ]);
  });

  it('should format frequencies if they exist', () => {
    const filter: VariableRemittanceFilter = {
      frequency: [Frequency.Four, Frequency.Two]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['frequency'],
        label: 'LIST.MASTER_DATA.variableRemittance.filters.frequency.label - 4, 2'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: VariableRemittanceFilter = {
      airline: [],
      agent: [],
      frequency: null
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      frequency: [Frequency.Four, Frequency.Two],
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['frequency'],
        label: 'LIST.MASTER_DATA.variableRemittance.filters.frequency.label - 4, 2'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
