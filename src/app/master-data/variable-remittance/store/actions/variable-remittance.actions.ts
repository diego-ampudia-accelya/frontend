import { createAction, props } from '@ngrx/store';

import { VariableRemittanceFilter } from '../../models/variable-remittance-filter.model';
import { VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const search = createAction(
  '[Variable Remittance] Search',
  props<{ query?: DataQuery<VariableRemittanceFilter> }>()
);
export const searchConfirmed = createAction('[Variable Remittance] Search Confirmed');
export const searchCancelled = createAction('[Variable Remittance] Search Cancelled');
export const searchSuccess = createAction(
  '[Variable Remittance] Search Success',
  props<{ data: PagedData<VariableRemittanceViewModel> }>()
);
export const searchError = createAction('[Variable Remittance] Search Error');

export const changePredefinedFilters = createAction(
  '[Variable Remittance] Change Predefined Filters',
  props<{ predefinedFilters: Partial<VariableRemittanceFilter> }>()
);

export const add = createAction('[Variable Remittance] Add');
export const addSuccess = createAction('[Variable Remittance] Add Success');

export const modify = createAction('[Variable Remittance] Modify', props<{ item: VariableRemittanceViewModel }>());

export const remove = createAction('[Variable Remittance] Remove', props<{ item: VariableRemittanceViewModel }>());
export const removeSuccess = createAction('[Variable Remittance] Remove Success');

export const applyChanges = createAction('[Variable Remittance] Apply Changes');
export const applyChangesSuccess = createAction('[Variable Remittance] Apply Changes Success');
export const applyChangesError = createAction('[Variable Remittance] Apply Changes Error');

export const discard = createAction('[Variable Remittance] Discard');

export const download = createAction('[Variable Remittance] Download');
export const downloadConfirmed = createAction('[Variable Remittance] Download Confirmed');
