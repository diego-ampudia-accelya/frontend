import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';

import { ChangesDialogService } from '../../dialogs/changes-dialog/changes-dialog.service';
import { OverrideDialogService } from '../../dialogs/override-dialog/override-dialog.service';
import { RemoveDialogService } from '../../dialogs/remove-dialog/remove-dialog.service';
import { VariableRemittanceFilter } from '../../models/variable-remittance-filter.model';
import { Frequency, VariableRemittance, VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { converter } from '../../shared/converter.utils';
import { VariableRemittanceListActions } from '../actions';
import * as fromVariableRemittance from '../reducers';

import { VariableRemittanceEffects } from './variable-remittance.effects';
import { NotificationService } from '~app/shared/services';
import { PagedData } from '~app/shared/models/paged-data.model';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DataQuery } from '~app/shared/components/list-view';
import { DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('VariableRemittanceEffects', () => {
  let effects: VariableRemittanceEffects;
  let actions$: Observable<Action>;
  let mockStore: MockStore<AppState>;

  const variableRemittanceServiceSpy = createSpyObject(VariableRemittanceService);
  const overrideDialogServiceSpy = createSpyObject(OverrideDialogService);
  const changesDialogServiceSpy = createSpyObject(ChangesDialogService);
  const removeDialogServiceSpy = createSpyObject(RemoveDialogService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const dialogServiceSpy = createSpyObject(DialogService);

  const variableRemittances: VariableRemittance[] = [
    {
      id: '69834855557820010',
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
      agent: { id: 69837820010, iataCode: '7820010', name: 'NOMBRE 7820010' },
      frequency: Frequency.Two
    },
    {
      id: '69834855557820012',
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
      agent: { id: 69837820012, iataCode: '7820012', name: 'NOMBRE 7820012' },
      frequency: Frequency.Two
    }
  ];

  const variableRemittancePagedData: PagedData<VariableRemittance> = {
    records: variableRemittances,
    pageSize: 20,
    total: variableRemittances.length,
    totalPages: 1,
    pageNumber: 0
  };

  const variableRemittanceViewModelPagedData: PagedData<VariableRemittanceViewModel> =
    converter.toViewModels(variableRemittancePagedData);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        VariableRemittanceEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          selectors: [{ selector: fromVariableRemittance.getListQuery, value: defaultQuery }]
        }),
        { provide: VariableRemittanceService, useValue: variableRemittanceServiceSpy },
        { provide: OverrideDialogService, useValue: overrideDialogServiceSpy },
        { provide: ChangesDialogService, useValue: changesDialogServiceSpy },
        { provide: RemoveDialogService, useValue: removeDialogServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.inject(VariableRemittanceEffects);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('search effect', () => {
    beforeEach(() => {
      actions$ = of(VariableRemittanceListActions.search({ query: defaultQuery }));
      changesDialogServiceSpy.confirmUnsavedChanges.calls.reset();
    });

    it('should dispatch search confirmed action on apply/discard dialog button click', fakeAsync(() => {
      changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Apply));

      let dispatchedAction: Action;
      effects.search$.subscribe(action => (dispatchedAction = action));

      expect(changesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.searchConfirmed());
    }));

    it('should dispatch search cancelled action on cancel dialog button click', fakeAsync(() => {
      changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));

      let dispatchedAction: Action;
      effects.search$.subscribe(action => (dispatchedAction = action));

      expect(changesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.searchCancelled());
    }));
  });

  describe('search confirmed effect', () => {
    beforeEach(() => (actions$ = of(VariableRemittanceListActions.searchConfirmed())));

    it('should dispatch search success action with the corresponding data', fakeAsync(() => {
      const query: DataQuery<VariableRemittanceFilter> = {
        ...defaultQuery,
        filterBy: { frequency: [Frequency.Two] }
      };

      mockStore.overrideSelector(fromVariableRemittance.getListQuery, query);
      variableRemittanceServiceSpy.find.and.returnValue(of(variableRemittancePagedData));

      let dispatchedAction: Action;
      effects.searchConfirmed$.subscribe(action => (dispatchedAction = action));

      expect(variableRemittanceServiceSpy.find).toHaveBeenCalledWith(query);
      expect(dispatchedAction).toEqual(
        VariableRemittanceListActions.searchSuccess({ data: variableRemittanceViewModelPagedData })
      );
    }));

    it('should dispatch search error action if an error is thrown', fakeAsync(() => {
      mockStore.overrideSelector(fromVariableRemittance.getListQuery, defaultQuery);
      variableRemittanceServiceSpy.find.and.returnValue(throwError(new Error()));

      let dispatchedAction: Action;
      let actualError: Error;
      effects.searchConfirmed$.subscribe(
        action => (dispatchedAction = action),
        error => (actualError = error)
      );

      expect(dispatchedAction).toEqual(VariableRemittanceListActions.searchError());
      expect(actualError).toBeDefined();
    }));
  });

  describe('add effect', () => {
    beforeEach(() => {
      actions$ = of(VariableRemittanceListActions.add());
      overrideDialogServiceSpy.open.calls.reset();
    });

    it('should dispatch add success action on add dialog button click', fakeAsync(() => {
      overrideDialogServiceSpy.open.and.returnValue(of(FooterButton.Add));

      let dispatchedAction: Action;
      effects.add$.subscribe(action => (dispatchedAction = action));

      expect(overrideDialogServiceSpy.open).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.addSuccess());
    }));

    it('should not dispatch any action on cancel dialog button click', fakeAsync(() => {
      overrideDialogServiceSpy.open.and.returnValue(of(FooterButton.Cancel));

      let dispatchedAction: Action;
      effects.add$.subscribe(action => (dispatchedAction = action));

      expect(overrideDialogServiceSpy.open).toHaveBeenCalled();
      expect(dispatchedAction).toBeUndefined();
    }));
  });

  describe('add success effect', () => {
    it('should dispatch search action and emit notification', fakeAsync(() => {
      actions$ = of(VariableRemittanceListActions.addSuccess());

      let dispatchedAction: Action;
      effects.addSuccess$.subscribe(action => (dispatchedAction = action));

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith(
        'LIST.MASTER_DATA.variableRemittance.notification.success.add'
      );
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.search({}));
    }));
  });

  describe('remove effect', () => {
    beforeEach(() => {
      const item = converter.toViewModel(variableRemittances[0]);

      actions$ = of(VariableRemittanceListActions.remove({ item }));
      removeDialogServiceSpy.open.calls.reset();
    });

    it('should dispatch search action on remove dialog button click', fakeAsync(() => {
      removeDialogServiceSpy.open.and.returnValue(of(FooterButton.Remove));

      let dispatchedAction: Action;
      effects.remove$.subscribe(action => (dispatchedAction = action));

      expect(removeDialogServiceSpy.open).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.search({}));
    }));

    it('should not dispatch any action on cancel dialog button click', fakeAsync(() => {
      removeDialogServiceSpy.open.and.returnValue(of(FooterButton.Cancel));

      let dispatchedAction: Action;
      effects.remove$.subscribe(action => (dispatchedAction = action));

      expect(removeDialogServiceSpy.open).toHaveBeenCalled();
      expect(dispatchedAction).toBeUndefined();
    }));
  });

  describe('remove success effect', () => {
    it('should emit notification', fakeAsync(() => {
      actions$ = of(VariableRemittanceListActions.removeSuccess());

      effects.removeSuccess$.subscribe();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith(
        'LIST.MASTER_DATA.variableRemittance.notification.success.remove'
      );
    }));
  });

  describe('apply changes effect', () => {
    beforeEach(() => {
      actions$ = of(VariableRemittanceListActions.applyChanges());
      changesDialogServiceSpy.confirmApplyChanges.calls.reset();
    });

    it('should dispatch search action on apply dialog button click', fakeAsync(() => {
      changesDialogServiceSpy.confirmApplyChanges.and.returnValue(of(FooterButton.Apply));

      let dispatchedAction: Action;
      effects.applyChanges$.subscribe(action => (dispatchedAction = action));

      expect(changesDialogServiceSpy.confirmApplyChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.search({}));
    }));

    it('should not dispatch any action on cancel or discard dialog button click', fakeAsync(() => {
      changesDialogServiceSpy.confirmApplyChanges.and.returnValue(of(FooterButton.Discard));

      let dispatchedAction: Action;
      effects.applyChanges$.subscribe(action => (dispatchedAction = action));

      expect(changesDialogServiceSpy.confirmApplyChanges).toHaveBeenCalled();
      expect(dispatchedAction).toBeUndefined();
    }));

    it('should dispatch apply changes error action when an error is returned in changes dialog', fakeAsync(() => {
      changesDialogServiceSpy.confirmApplyChanges.and.returnValue(throwError(new Error()));

      let dispatchedAction: Action;
      let actualError: Error;
      effects.applyChanges$.subscribe(
        action => (dispatchedAction = action),
        error => (actualError = error)
      );

      expect(changesDialogServiceSpy.confirmApplyChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.applyChangesError());
      expect(actualError).toBeDefined();
    }));
  });

  describe('apply changes success effect', () => {
    it('should not dispatch any action and emit notification', fakeAsync(() => {
      actions$ = of(VariableRemittanceListActions.applyChangesSuccess());

      effects.applyChangesSuccess$.subscribe();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalledWith(
        'LIST.MASTER_DATA.variableRemittance.notification.success.applyChanges'
      );
    }));
  });

  describe('download effect', () => {
    beforeEach(() => {
      actions$ = of(VariableRemittanceListActions.download());
      changesDialogServiceSpy.confirmUnsavedChanges.calls.reset();
    });

    it('should dispatch download confirmed action on not cancel dialog button click', fakeAsync(() => {
      changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Download));

      let dispatchedAction: Action;
      effects.download$.subscribe(action => (dispatchedAction = action));

      expect(changesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(VariableRemittanceListActions.downloadConfirmed());
    }));

    it('should not dispatch any action on cancel dialog button click', fakeAsync(() => {
      changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));

      let dispatchedAction: Action;
      effects.download$.subscribe(action => (dispatchedAction = action));

      expect(changesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction).toBeUndefined();
    }));
  });

  describe('download confirmed effect', () => {
    it('should not dispatch any action and trigger dialog with the corresponding query', fakeAsync(() => {
      const query: DataQuery<VariableRemittanceFilter> = {
        ...defaultQuery,
        filterBy: { frequency: [Frequency.Four] }
      };

      actions$ = of(VariableRemittanceListActions.downloadConfirmed());
      mockStore.overrideSelector(fromVariableRemittance.getListQuery, query);
      dialogServiceSpy.open.and.returnValue(of());

      let dispatchedAction: Action;
      effects.downloadConfirmed$.subscribe(action => (dispatchedAction = action));

      expect(dialogServiceSpy.open).toHaveBeenCalledWith(
        jasmine.anything(),
        jasmine.objectContaining({ data: jasmine.objectContaining({ downloadQuery: query }) })
      );
      expect(dispatchedAction).toBeUndefined();
    }));
  });
});
