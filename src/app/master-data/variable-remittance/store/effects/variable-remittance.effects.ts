import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { EMPTY, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { ChangesDialogService } from '../../dialogs/changes-dialog/changes-dialog.service';
import { OverrideDialogService } from '../../dialogs/override-dialog/override-dialog.service';
import { RemoveDialogService } from '../../dialogs/remove-dialog/remove-dialog.service';
import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { converter } from '../../shared/converter.utils';
import { VariableRemittanceListActions } from '../actions';
import * as fromVariableRemittance from '../reducers';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class VariableRemittanceEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private dataService: VariableRemittanceService,
    private overrideDialogService: OverrideDialogService,
    private changesDialogService: ChangesDialogService,
    private removeDialogService: RemoveDialogService,
    private notificationService: NotificationService,
    private dialogService: DialogService
  ) {}

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariableRemittanceListActions.search),
      switchMap(() => this.changesDialogService.confirmUnsavedChanges()),
      switchMap(result =>
        result === FooterButton.Cancel
          ? of(VariableRemittanceListActions.searchCancelled())
          : of(VariableRemittanceListActions.searchConfirmed())
      )
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariableRemittanceListActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromVariableRemittance.getListQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(converter.toViewModels),
          map(data => VariableRemittanceListActions.searchSuccess({ data })),
          rethrowError(() => VariableRemittanceListActions.searchError())
        )
      )
    )
  );

  public add$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariableRemittanceListActions.add),
      switchMap(() => this.overrideDialogService.open()),
      switchMap(result => (result === FooterButton.Add ? of(VariableRemittanceListActions.addSuccess()) : EMPTY))
    )
  );

  public addSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariableRemittanceListActions.addSuccess),
      tap(() => this.notificationService.showSuccess('LIST.MASTER_DATA.variableRemittance.notification.success.add')),
      switchMap(() => of(VariableRemittanceListActions.search({})))
    )
  );

  public remove$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariableRemittanceListActions.remove),
      switchMap(({ item }) => this.removeDialogService.open(item)),
      switchMap(result => (result === FooterButton.Remove ? of(VariableRemittanceListActions.search({})) : EMPTY))
    )
  );

  public removeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(VariableRemittanceListActions.removeSuccess),
        tap(() =>
          this.notificationService.showSuccess('LIST.MASTER_DATA.variableRemittance.notification.success.remove')
        )
      ),
    { dispatch: false }
  );

  public applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariableRemittanceListActions.applyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(result => (result === FooterButton.Apply ? of(VariableRemittanceListActions.search({})) : EMPTY)),
      rethrowError(() => VariableRemittanceListActions.applyChangesError())
    )
  );

  public applyChangesSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(VariableRemittanceListActions.applyChangesSuccess),
        tap(() =>
          this.notificationService.showSuccess('LIST.MASTER_DATA.variableRemittance.notification.success.applyChanges')
        )
      ),
    { dispatch: false }
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariableRemittanceListActions.download),
      switchMap(() => this.changesDialogService.confirmUnsavedChanges()),
      switchMap(result =>
        result !== FooterButton.Cancel ? of(VariableRemittanceListActions.downloadConfirmed()) : EMPTY
      )
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(VariableRemittanceListActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromVariableRemittance.getListQuery))),
        switchMap(([_, query]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );
}
