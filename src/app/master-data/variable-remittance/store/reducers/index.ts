import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './variable-remittance.reducer';
import { AppState } from '~app/reducers';

export const variableRemittanceFeatureKey = 'variableRemittance';

export interface State extends AppState {
  [variableRemittanceFeatureKey]: VariableRemittanceState;
}

export interface VariableRemittanceState {
  [fromList.key]: fromList.State;
}

export function reducers(state: VariableRemittanceState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getVariableRemittance = createFeatureSelector<State, VariableRemittanceState>(
  variableRemittanceFeatureKey
);

export const getList = createSelector(getVariableRemittance, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getListQuery = createSelector(getList, fromList.getQuery);

export const getChanges = createSelector(getList, fromList.getChanges);

export const hasChanges = createSelector(getList, fromList.hasChanges);
