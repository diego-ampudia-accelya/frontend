import { createReducer, on } from '@ngrx/store';
import { isEqual } from 'lodash';

import { VariableRemittanceFilter } from '../../models/variable-remittance-filter.model';
import { VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { converter } from '../../shared/converter.utils';
import { VariableRemittanceListActions } from '../actions';
import { PagedData } from '~app/shared/models/paged-data.model';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DataQuery } from '~app/shared/components/list-view';

export const key = 'list';

export interface State {
  query: DataQuery<VariableRemittanceFilter>;
  previousQuery: DataQuery<VariableRemittanceFilter>;
  predefinedFilters: Partial<VariableRemittanceFilter>;
  data: VariableRemittanceViewModel[];
  loading: boolean;
}

export const initialState: State = {
  query: defaultQuery,
  previousQuery: null,
  predefinedFilters: {},
  data: [],
  loading: false
};

const loadData = (state: State, data: PagedData<VariableRemittanceViewModel>) => ({
  ...state,
  loading: false,
  data: data.records,
  predefinedFilters: state.predefinedFilters,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const loadItem = (state: State, modified: VariableRemittanceViewModel) => ({
  ...state,
  data: state.data.map(({ original, ...item }) => {
    if (item.id !== modified.id) {
      return { original, ...item };
    }

    return original ? { ...modified, original } : { ...modified, original: item };
  })
});

export const reducer = createReducer(
  initialState,
  on(VariableRemittanceListActions.search, (state, { query = state.query }) => ({
    ...state,
    query: { ...query, filterBy: { ...state.predefinedFilters, ...query.filterBy } },
    previousQuery: state.query
  })),
  on(VariableRemittanceListActions.searchConfirmed, state => ({
    ...state,
    previousQuery: null,
    loading: true
  })),
  on(VariableRemittanceListActions.searchCancelled, state => ({
    ...state,
    query: state.previousQuery,
    previousQuery: null
  })),
  on(VariableRemittanceListActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(VariableRemittanceListActions.searchError, state => ({
    ...state,
    loading: false
  })),
  on(VariableRemittanceListActions.changePredefinedFilters, (state, { predefinedFilters }) => ({
    ...state,
    predefinedFilters
  })),
  on(VariableRemittanceListActions.modify, (state, { item }) => loadItem(state, item)),
  on(VariableRemittanceListActions.applyChangesSuccess, state => ({
    ...state,
    data: state.data.map(({ original, ...modified }) => converter.toViewModel(modified))
  })),
  on(VariableRemittanceListActions.discard, state => ({
    ...state,
    data: state.data.map(item => (item.original ? converter.toViewModel(item.original) : item))
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;

export const hasChanges = (state: State) => getChanges(state).length > 0;

export const getChanges = (state: State) =>
  state.data.filter(({ original, ...item }) => original && !isEqual(item, original));
