import { VariableRemittanceFilter } from '../../models/variable-remittance-filter.model';
import { Frequency, VariableRemittance, VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { converter } from '../../shared/converter.utils';
import { VariableRemittanceListActions } from '../actions';

import * as fromVariableRemittance from './variable-remittance.reducer';
import { PagedData } from '~app/shared/models/paged-data.model';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DataQuery } from '~app/shared/components/list-view';

describe('Variable Remittance Reducer', () => {
  const query: DataQuery<VariableRemittanceFilter> = {
    ...defaultQuery,
    filterBy: {
      airline: [
        { id: 1, code: '001', name: 'AIRLINE NAME 001', designator: 'L1' },
        { id: 2, code: '002', name: 'AIRLINE NAME 002', designator: 'L2' }
      ]
    }
  };

  const mockVariableRemittance: VariableRemittance = {
    id: '69834855557820015',
    bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
    airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
    agent: { id: 69837820015, iataCode: '7820015', name: 'NOMBRE 7820015' },
    frequency: Frequency.One
  };

  const mockVariableRemittanceOriginal: VariableRemittanceViewModel = {
    ...mockVariableRemittance,
    original: {
      ...mockVariableRemittance,
      frequency: Frequency.Two
    }
  };

  const variableRemittances: VariableRemittanceViewModel[] = [
    {
      id: '69834855557820010',
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
      agent: { id: 69837820010, iataCode: '7820010', name: 'NOMBRE 7820010' },
      frequency: Frequency.Two
    },
    {
      id: '69834855557820012',
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
      agent: { id: 69837820012, iataCode: '7820012', name: 'NOMBRE 7820012' },
      frequency: Frequency.Two
    }
  ].map(converter.toViewModel);

  it('should update state correctly on SEARCH action', () => {
    const action = VariableRemittanceListActions.search({ query });
    const state = fromVariableRemittance.reducer(fromVariableRemittance.initialState, action);

    expect(state.query).toEqual(query);
    expect(state.previousQuery).toEqual(defaultQuery);
  });

  it('should update state correctly on SEARCH CONFIRMED action', () => {
    const initialState: fromVariableRemittance.State = {
      ...fromVariableRemittance.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = VariableRemittanceListActions.searchConfirmed();
    const state = fromVariableRemittance.reducer(initialState, action);

    expect(state.previousQuery).toBeNull();
    expect(state.loading).toBe(true);
  });

  it('should update state correctly on SEARCH CANCELLED action', () => {
    const initialState: fromVariableRemittance.State = {
      ...fromVariableRemittance.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = VariableRemittanceListActions.searchCancelled();
    const state = fromVariableRemittance.reducer(initialState, action);

    expect(state.query).toEqual(defaultQuery);
    expect(state.previousQuery).toBeNull();
  });

  it('should update state correctly on SEARCH SUCCESS action', () => {
    const data: PagedData<VariableRemittanceViewModel> = {
      records: variableRemittances,
      pageSize: 20,
      total: variableRemittances.length,
      totalPages: 1,
      pageNumber: 0
    };

    const action = VariableRemittanceListActions.searchSuccess({ data });
    const state = fromVariableRemittance.reducer(fromVariableRemittance.initialState, action);

    expect(state.loading).toBe(false);
    expect(state.data).toEqual(variableRemittances);
    expect(state.query).toEqual(
      jasmine.objectContaining({ paginateBy: { page: 0, size: 20, totalElements: variableRemittances.length } })
    );
  });

  it('should set loading false on SEARCH ERROR action', () => {
    const action = VariableRemittanceListActions.searchError();
    const state = fromVariableRemittance.reducer(fromVariableRemittance.initialState, action);

    expect(state.loading).toBe(false);
  });

  describe('MODIFY action', () => {
    const initialState: fromVariableRemittance.State = {
      ...fromVariableRemittance.initialState,
      data: variableRemittances
    };

    it('should update state data correctly when `original` not exists on modified item', () => {
      const item: VariableRemittanceViewModel = {
        ...variableRemittances[0],
        frequency: Frequency.Four
      };

      const action = VariableRemittanceListActions.modify({ item });
      const state = fromVariableRemittance.reducer(initialState, action);

      expect(state.data).toContain({ ...item, original: converter.toDataModel(variableRemittances[0]) });
    });

    it('should update state data correctly when `original` already exists on modified item', () => {
      const original: VariableRemittance = {
        ...converter.toDataModel(variableRemittances[0]),
        frequency: Frequency.One
      };

      const item: VariableRemittanceViewModel = {
        ...variableRemittances[0],
        frequency: Frequency.Four,
        original
      };

      const action = VariableRemittanceListActions.modify({ item });
      const state = fromVariableRemittance.reducer({ ...initialState, data: [...variableRemittances, item] }, action);

      expect(state.data).toContain({ ...item, original });
    });
  });

  it('should update state data correctly on APPLY CHANGES SUCCESS action', () => {
    const action = VariableRemittanceListActions.applyChangesSuccess();
    const state = fromVariableRemittance.reducer(
      { ...fromVariableRemittance.initialState, data: [...variableRemittances, mockVariableRemittanceOriginal] },
      action
    );

    expect(state.data).toContain({ ...mockVariableRemittance, original: null });
  });

  it('should update state data correctly on DISCARD action', () => {
    const action = VariableRemittanceListActions.discard();
    const state = fromVariableRemittance.reducer(
      { ...fromVariableRemittance.initialState, data: [...variableRemittances, mockVariableRemittanceOriginal] },
      action
    );

    expect(state.data).toContain({ ...mockVariableRemittance, frequency: Frequency.Two, original: null });
  });

  it('should return if state data HAS CHANGES', () => {
    // HAS CHANGES
    expect(
      fromVariableRemittance.hasChanges({
        ...fromVariableRemittance.initialState,
        data: [...variableRemittances, mockVariableRemittanceOriginal]
      })
    ).toBe(true);

    // HAS NO CHANGES
    expect(
      fromVariableRemittance.hasChanges({ ...fromVariableRemittance.initialState, data: variableRemittances })
    ).toBe(false);
  });

  it('should GET state data items CHANGED', () => {
    // HAS CHANGES
    expect(
      fromVariableRemittance.getChanges({
        ...fromVariableRemittance.initialState,
        data: [...variableRemittances, mockVariableRemittanceOriginal]
      })
    ).toEqual([mockVariableRemittanceOriginal]);

    // HAS NO CHANGES
    expect(
      fromVariableRemittance.getChanges({ ...fromVariableRemittance.initialState, data: variableRemittances })
    ).toEqual([]);
  });
});
