import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { Frequency } from '../../models/variable-remittance.model';
import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { OverrideDialogComponent } from './override-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { AgentDictionaryService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('OverrideDialogComponent', () => {
  let component: OverrideDialogComponent;
  let fixture: ComponentFixture<OverrideDialogComponent>;
  let mockStore: MockStore;

  const dataServiceSpy = createSpyObject(VariableRemittanceService);
  const agentDictionarySpy = createSpyObject(AgentDictionaryService);

  const mockBsps: Bsp[] = [
    {
      id: 1,
      name: 'SPAIN',
      isoCountryCode: 'ESP',
      effectiveFrom: '2000-01-01'
    }
  ];

  const initialState = {
    auth: { user: { bsps: mockBsps } }
  };

  const mockConfig: DialogConfig = {
    data: {
      title: 'title',
      footerButtonsType: FooterButton.Add,
      buttons: [{ type: FooterButton.Cancel }, { type: FooterButton.Add }]
    }
  };

  // SPIES
  const frequencyOptions: DropdownOption<number>[] = [{ value: Frequency.One, label: '' + Frequency.One }];
  const agentOptions: DropdownOption<AgentSummary>[] = [
    { value: { id: '1', name: 'AGENT 78200010', code: '78200010' }, label: 'AGENT 78200010' }
  ];

  const frequencyDropdownSpy = dataServiceSpy.getFrequencyDropdownOptions.and.returnValue(of(frequencyOptions));
  const agentDropdownSpy = agentDictionarySpy.getDropdownOptions.and.returnValue(of(agentOptions));

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OverrideDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        provideMockStore({ initialState }),
        { provide: VariableRemittanceService, useValue: dataServiceSpy },
        { provide: AgentDictionaryService, useValue: agentDictionarySpy },
        { provide: DialogConfig, useValue: mockConfig }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverrideDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);

    // SPIES RESET
    frequencyDropdownSpy.calls.reset();
    agentDropdownSpy.calls.reset();
  });

  it('should be created', () => {
    expect(component).toBeDefined();
  });

  describe('initialize dropdown options', () => {
    it('should initialize agent dropdown options', fakeAsync(() => {
      let agents: DropdownOption<AgentSummary>[];

      component.ngOnInit();
      component.agentDropdownOptions$.subscribe(options => (agents = options));

      expect(agents).toEqual(agentOptions);
      expect(agentDropdownSpy).toHaveBeenCalled();
    }));

    it('should initialize frequency dropdown options patching form with bsp', fakeAsync(() => {
      let frequencies: DropdownOption<number>[];

      component.ngOnInit();
      component.frequencyDropdownOptions$.subscribe(options => (frequencies = options));
      tick();

      expect(frequencies).toEqual(frequencyOptions);
      expect(frequencyDropdownSpy).toHaveBeenCalledWith(1);
      expect(component.form.value.bsp).toEqual(mockBsps[0]);
    }));

    it('should initialize empty the frequency dropdown options with no bsp found', fakeAsync(() => {
      let frequencies: DropdownOption<number>[];

      mockStore.setState({ auth: { user: { bsps: [] } } });

      component.ngOnInit();
      component.frequencyDropdownOptions$.subscribe(options => (frequencies = options));
      tick();

      expect(frequencies).toEqual([]);
      expect(frequencyDropdownSpy).not.toHaveBeenCalled();
      expect(component.form.value.bsp).toBeNull();
    }));
  });

  it('should initialize form listener correctly', fakeAsync(() => {
    const value = {
      agent: { id: '1', name: 'AGENT 78200010', code: '78200010' },
      frequency: Frequency.One,
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ESP', effectiveFrom: '2000-01-01' }
    };

    component.ngOnInit();
    component.form.patchValue(value);
    tick();

    expect(component['config'].data.buttons[1].isDisabled).toBe(false);
  }));
});
