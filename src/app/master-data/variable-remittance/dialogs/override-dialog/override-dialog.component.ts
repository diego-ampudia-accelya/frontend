import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable, of, Subject } from 'rxjs';
import { map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { AgentDictionaryService } from '~app/shared/services';
import { VariableRemittanceService } from '../../services/variable-remittance.service';

@Component({
  selector: 'bspl-override-dialog',
  templateUrl: './override-dialog.component.html',
  styleUrls: ['./override-dialog.component.scss']
})
export class OverrideDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public frequencyDropdownOptions$: Observable<DropdownOption<number>[]>;

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private dataService: VariableRemittanceService,
    private agentDictionary: AgentDictionaryService
  ) {}

  public ngOnInit(): void {
    this.form = this.buildForm();

    this.initializeDropdownOptions();
    this.initializeFormListener();
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      agent: [null, Validators.required],
      frequency: [null, Validators.required],
      bsp: []
    });
  }

  private initializeDropdownOptions(): void {
    this.agentDropdownOptions$ = this.agentDictionary.getDropdownOptions();
    this.frequencyDropdownOptions$ = this.store.pipe(
      select(getUser),
      map(({ bsps }) => (bsps?.length ? bsps[0] : null)),
      tap(bsp => this.form.patchValue({ bsp })),
      switchMap(bsp => (bsp ? this.dataService.getFrequencyDropdownOptions(bsp.id) : of([])))
    );
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const addButton: ModalAction = this.config.data.buttons.find((btn: ModalAction) => btn.type === FooterButton.Add);
      addButton.isDisabled = status === 'INVALID';
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
