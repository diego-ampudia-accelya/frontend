import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap } from 'rxjs/operators';

import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { OverrideDialogComponent } from './override-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Injectable()
export class OverrideDialogService {
  constructor(private dialogService: DialogService, private dataService: VariableRemittanceService) {}

  public open(): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'LIST.MASTER_DATA.variableRemittance.dialog.add.title',
        footerButtonsType: [{ type: FooterButton.Add, isDisabled: true }]
      }
    };

    return this.dialogService.open(OverrideDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Add ? this.override(dialogConfig, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private override(config: DialogConfig, component: OverrideDialogComponent): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      const { agent, frequency, bsp } = component.form.value;

      setLoading(true);

      return this.dataService.create({ agent, frequency, bsp }).pipe(
        mapTo(FooterButton.Add),
        finalize(() => setLoading(false))
      );
    });
  }
}
