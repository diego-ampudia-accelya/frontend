import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of, throwError } from 'rxjs';

import { Frequency, VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { VariableRemittanceService } from '../../services/variable-remittance.service';

import { OverrideDialogComponent } from './override-dialog.component';
import { OverrideDialogService } from './override-dialog.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

describe('OverrideDialogService', () => {
  let service: OverrideDialogService;

  const dataServiceSpy = createSpyObject(VariableRemittanceService);
  const dialogServiceSpy = createSpyObject(DialogService);

  const item: VariableRemittanceViewModel = {
    id: '1',
    bsp: { id: 1 },
    agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
    frequency: Frequency.Four,
    original: {
      id: '1',
      bsp: { id: 1 },
      agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
      frequency: Frequency.Two
    }
  };

  const mockConfig: DialogConfig = {
    data: {
      title: 'LIST.MASTER_DATA.variableRemittance.dialog.add.title',
      footerButtonsType: [{ type: FooterButton.Add, isDisabled: true }]
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        OverrideDialogService,
        { provide: VariableRemittanceService, useValue: dataServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(OverrideDialogService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('override', () => {
    const value = {
      agent: item.agent,
      frequency: item.frequency,
      bsp: item.bsp
    };
    const mockComponent: any = {
      form: { value }
    };
    const mockDialogConfig: DialogConfig = {
      data: { buttons: [{ type: '', isDisabled: false }] }
    };

    it('should override item calling service method correctly', fakeAsync(() => {
      dataServiceSpy.create.and.returnValue(of(null));

      let result: FooterButton;
      service['override'](mockDialogConfig, mockComponent).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Add);
      expect(dataServiceSpy.create).toHaveBeenCalledWith(value);
    }));

    it('should return create error response from service', fakeAsync(() => {
      dataServiceSpy.create.and.returnValue(throwError(new Error()));

      let error: Error;
      service['override'](mockDialogConfig, mockComponent).subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dataServiceSpy.create).toHaveBeenCalledWith(value);
    }));
  });

  describe('open', () => {
    it('should open and close override dialog correctly with ADD action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(
        of({ clickedBtn: FooterButton.Add, contentComponentRef: OverrideDialogComponent })
      );
      spyOn<any>(service, 'override').and.returnValue(of(FooterButton.Add));

      let result: FooterButton;
      service.open().subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Add);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(OverrideDialogComponent, mockConfig);
      expect(service['override']).toHaveBeenCalledWith(mockConfig, OverrideDialogComponent);
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close override dialog correctly with CANCEL action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(
        of({ clickedBtn: FooterButton.Cancel, contentComponentRef: OverrideDialogComponent })
      );
      spyOn<any>(service, 'override');

      let result: FooterButton;
      service.open().subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Cancel);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(OverrideDialogComponent, mockConfig);
      expect(service['override']).not.toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close override dialog correctly even with an ERROR from override method', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(
        of({ clickedBtn: FooterButton.Add, contentComponentRef: OverrideDialogComponent })
      );
      spyOn<any>(service, 'override').and.returnValue(throwError(new Error()));

      let error: Error;
      service.open().subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(OverrideDialogComponent, mockConfig);
      expect(service['override']).toHaveBeenCalledWith(mockConfig, OverrideDialogComponent);
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });
});
