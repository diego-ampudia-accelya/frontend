export const translations = {
  remove: {
    title: 'LIST.MASTER_DATA.variableRemittance.dialog.remove.title',
    details: 'LIST.MASTER_DATA.variableRemittance.dialog.remove.message'
  },
  applyChanges: {
    title: 'LIST.MASTER_DATA.variableRemittance.dialog.applyChanges.title',
    details: 'LIST.MASTER_DATA.variableRemittance.dialog.applyChanges.message'
  },
  unsavedChanges: {
    title: 'LIST.MASTER_DATA.variableRemittance.dialog.unsavedChanges.title',
    details: 'LIST.MASTER_DATA.variableRemittance.dialog.unsavedChanges.message'
  },
  groupTitle: 'LIST.MASTER_DATA.variableRemittance.dialog.groupTitle',
  itemTitle: 'LIST.MASTER_DATA.variableRemittance.dialog.itemTitle'
};
