import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'rxjs';

import { Frequency, VariableRemittanceViewModel } from '../../models/variable-remittance.model';

import { RemoveDialogConfigService } from './remove-dialog-config.service';
import { FooterButton } from '~app/shared/components';

describe('RemoveDialogConfigService', () => {
  let service: RemoveDialogConfigService;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const item: VariableRemittanceViewModel = {
    id: '1',
    bsp: { id: 1 },
    agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
    frequency: Frequency.Four,
    original: {
      id: '1',
      bsp: { id: 1 },
      agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
      frequency: Frequency.Two
    }
  };

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    service = new RemoveDialogConfigService(translationServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should format item changes correctly', () => {
    expect(service['formatChanges'](item)).toEqual([{ group: '', name: '', value: '78200010: 4' }]);
  });

  it('should build changes dialog configuration', () => {
    const question = { title: 'title', details: 'details' };

    expect(service.build(item, question)).toEqual({
      data: { title: question.title, footerButtonsType: [{ type: FooterButton.Remove }] },
      changes: [{ group: '', name: '', value: '78200010: 4' }],
      message: question.details
    });
  });
});
