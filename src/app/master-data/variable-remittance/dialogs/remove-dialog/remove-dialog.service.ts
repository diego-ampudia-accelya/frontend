import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';

import { VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { converter } from '../../shared/converter.utils';
import { VariableRemittanceListActions } from '../../store/actions';
import { translations } from '../translations';

import { RemoveDialogConfigService } from './remove-dialog-config.service';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class RemoveDialogService {
  constructor(
    private store: Store<AppState>,
    private dialogService: DialogService,
    private configBuilder: RemoveDialogConfigService,
    private dataService: VariableRemittanceService
  ) {}

  public open(item: VariableRemittanceViewModel): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(item, translations.remove);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap((result: FooterButton) =>
        result === FooterButton.Remove ? this.remove(item, dialogConfig) : of(result)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private remove(item: VariableRemittanceViewModel, dialogConfig: ChangesDialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      dialogConfig.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.delete(converter.toDataModel(item)).pipe(
        tap(() => this.store.dispatch(VariableRemittanceListActions.removeSuccess())),
        mapTo(FooterButton.Remove),
        finalize(() => setLoading(false))
      );
    });
  }
}
