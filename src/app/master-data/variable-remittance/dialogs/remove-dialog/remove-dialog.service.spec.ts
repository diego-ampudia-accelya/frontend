import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { of, throwError } from 'rxjs';

import { Frequency, VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { VariableRemittanceListActions } from '../../store/actions';
import { translations } from '../translations';

import { RemoveDialogConfigService } from './remove-dialog-config.service';
import { RemoveDialogService } from './remove-dialog.service';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('RemoveDialogService', () => {
  let service: RemoveDialogService;
  let mockStore: MockStore<AppState>;

  const dataServiceSpy = createSpyObject(VariableRemittanceService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const item: VariableRemittanceViewModel = {
    id: '1',
    bsp: { id: 1 },
    agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
    frequency: Frequency.Four,
    original: {
      id: '1',
      bsp: { id: 1 },
      agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
      frequency: Frequency.Two
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RemoveDialogService,
        RemoveDialogConfigService,
        provideMockStore(),
        { provide: VariableRemittanceService, useValue: dataServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(RemoveDialogService);
    mockStore = TestBed.inject<any>(Store);

    translationServiceSpy.translate.and.callFake(identity);
    spyOn(mockStore, 'dispatch');
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('remove', () => {
    const mockConfig: ChangesDialogConfig = {
      data: { buttons: [{ type: '', isDisabled: false }] },
      message: '',
      changes: []
    };

    it('should remove item calling service method and dispatching action correctly', fakeAsync(() => {
      dataServiceSpy.delete.and.returnValue(of(null));

      let result: FooterButton;
      service['remove'](item, mockConfig).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Remove);
      expect(dataServiceSpy.delete).toHaveBeenCalledWith({
        id: '1',
        bsp: { id: 1 },
        agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
        frequency: Frequency.Four
      });
      expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.removeSuccess());
    }));

    it('should not dispatch any success action on delete error response from service', fakeAsync(() => {
      dataServiceSpy.delete.and.returnValue(throwError(new Error()));

      let error: Error;
      service['remove'](item, mockConfig).subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dataServiceSpy.delete).toHaveBeenCalledWith({
        id: '1',
        bsp: { id: 1 },
        agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
        frequency: Frequency.Four
      });
      expect(mockStore.dispatch).not.toHaveBeenCalled();
    }));
  });

  describe('open', () => {
    const configResult: ChangesDialogConfig = {
      data: {
        title: translations.remove.title,
        footerButtonsType: [{ type: FooterButton.Remove }]
      },
      changes: [{ group: '', name: '', value: '78200010: 4' }],
      message: translations.remove.details
    };

    it('should open and close changes dialog correctly with REMOVE action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Remove }));
      spyOn<any>(service, 'remove').and.returnValue(of(FooterButton.Remove));

      let result: FooterButton;
      service.open(item).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Remove);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(ChangesDialogComponent, configResult);
      expect(service['remove']).toHaveBeenCalledWith(item, configResult);
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close changes dialog correctly with CANCEL action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));
      spyOn<any>(service, 'remove');

      let result: FooterButton;
      service.open(item).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Cancel);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(ChangesDialogComponent, configResult);
      expect(service['remove']).not.toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close changes dialog correctly even with an ERROR from remove method', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Remove }));
      spyOn<any>(service, 'remove').and.returnValue(throwError(new Error()));

      let error: Error;
      service.open(item).subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(ChangesDialogComponent, configResult);
      expect(service['remove']).toHaveBeenCalledWith(item, configResult);
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });
});
