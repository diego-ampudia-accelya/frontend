import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable()
export class RemoveDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(item: VariableRemittanceViewModel, question: { title: string; details: string }): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Remove }]
      },
      changes: this.formatChanges(item),
      message
    };
  }

  private formatChanges(item: VariableRemittanceViewModel): ChangeModel[] {
    return [
      {
        group: '',
        name: '',
        value: `${item.agent.iataCode}: ${item.frequency}`
      }
    ];
  }
}
