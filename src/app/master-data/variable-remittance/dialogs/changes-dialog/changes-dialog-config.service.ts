import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: VariableRemittanceViewModel[],
    question: { title: string; details: string }
  ): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: this.formatChanges(modifications),
      message
    };
  }

  private formatChanges(modifications: VariableRemittanceViewModel[]): ChangeModel[] {
    return modifications.map(item => ({
      group: '',
      name: '',
      value: `${item.agent.iataCode}: ${item.frequency}`
    }));
  }
}
