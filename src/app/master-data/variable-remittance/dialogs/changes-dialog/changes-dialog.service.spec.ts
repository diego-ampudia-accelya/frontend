import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { of, throwError } from 'rxjs';

import { Frequency, VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { VariableRemittanceListActions } from '../../store/actions';
import * as fromVariableRemittance from '../../store/reducers';
import { translations } from '../translations';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ChangesDialogService } from './changes-dialog.service';
import {
  ButtonDesign,
  ChangesDialogComponent,
  ChangesDialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('ChangesDialogService', () => {
  let service: ChangesDialogService;
  let mockStore: MockStore<AppState>;

  const dataServiceSpy = createSpyObject(VariableRemittanceService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const modifications: VariableRemittanceViewModel[] = [
    {
      id: '1',
      bsp: { id: 1 },
      agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
      frequency: Frequency.Four,
      original: {
        id: '1',
        bsp: { id: 1 },
        agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
        frequency: Frequency.Two
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ChangesDialogService,
        ChangesDialogConfigService,
        provideMockStore(),
        { provide: VariableRemittanceService, useValue: dataServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(ChangesDialogService);
    mockStore = TestBed.inject<any>(Store);

    translationServiceSpy.translate.and.callFake(identity);
    spyOn(mockStore, 'dispatch');
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('apply', () => {
    const mockConfig: ChangesDialogConfig = {
      data: { buttons: [{ type: FooterButton.Apply, isDisabled: false }] },
      message: '',
      changes: []
    };

    it('should save modified items calling service method and dispatching action correctly', fakeAsync(() => {
      dataServiceSpy.save.and.returnValue(of(null));

      let result: FooterButton;
      service['apply'](modifications, mockConfig).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Apply);
      expect(dataServiceSpy.save).toHaveBeenCalledWith([
        {
          id: '1',
          bsp: { id: 1 },
          agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
          frequency: Frequency.Four
        }
      ]);
      expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.applyChangesSuccess());
    }));

    it('should not dispatch any success action on save error response from service', fakeAsync(() => {
      dataServiceSpy.save.and.returnValue(throwError(new Error()));

      let error: Error;
      service['apply'](modifications, mockConfig).subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dataServiceSpy.save).toHaveBeenCalledWith([
        {
          id: '1',
          bsp: { id: 1 },
          agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
          frequency: Frequency.Four
        }
      ]);
      expect(mockStore.dispatch).not.toHaveBeenCalled();
    }));
  });

  describe('open', () => {
    const configResult: ChangesDialogConfig = {
      data: {
        title: translations.applyChanges.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: [{ group: '', name: '', value: '78200010: 4' }],
      message: translations.applyChanges.details
    };

    it('should open and close changes dialog correctly with APPLY action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Apply }));
      spyOn<any>(service, 'apply').and.returnValue(of(FooterButton.Apply));

      let result: FooterButton;
      service['open'](modifications, translations.applyChanges).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Apply);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(ChangesDialogComponent, configResult);
      expect(service['apply']).toHaveBeenCalledWith(modifications, configResult);
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close changes dialog correctly with DISCARD action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Discard }));
      spyOn<any>(service, 'apply');

      let result: FooterButton;
      service['open'](modifications, translations.applyChanges).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Discard);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(ChangesDialogComponent, configResult);
      expect(service['apply']).not.toHaveBeenCalled();
      expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.discard());
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close changes dialog correctly with CANCEL action', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));
      spyOn<any>(service, 'apply');

      let result: FooterButton;
      service['open'](modifications, translations.applyChanges).subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Cancel);
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(ChangesDialogComponent, configResult);
      expect(service['apply']).not.toHaveBeenCalled();
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));

    it('should open and close changes dialog correctly even with an ERROR from remove method', fakeAsync(() => {
      dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Apply }));
      spyOn<any>(service, 'apply').and.returnValue(throwError(new Error()));

      let error: Error;
      service['open'](modifications, translations.applyChanges).subscribe(
        _ => {},
        err => (error = err)
      );

      expect(error).toBeDefined();
      expect(dialogServiceSpy.open).toHaveBeenCalledWith(ChangesDialogComponent, configResult);
      expect(service['apply']).toHaveBeenCalledWith(modifications, configResult);
      expect(dialogServiceSpy.close).toHaveBeenCalled();
    }));
  });

  describe('confirm', () => {
    it('should confirm apply changes correctly with modifications', fakeAsync(() => {
      mockStore.overrideSelector(fromVariableRemittance.getChanges, modifications);
      spyOn<any>(service, 'open').and.returnValue(of(FooterButton.Apply));
      spyOn<any>(service, 'confirm').and.callThrough();

      let result: FooterButton;
      service.confirmApplyChanges().subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Apply);
      expect(service['confirm']).toHaveBeenCalledWith(translations.applyChanges);
      expect(service['open']).toHaveBeenCalledWith(modifications, translations.applyChanges);
    }));

    it('should discard unsaved changes without modifications', fakeAsync(() => {
      mockStore.overrideSelector(fromVariableRemittance.getChanges, []);
      spyOn<any>(service, 'confirm').and.callThrough();
      spyOn<any>(service, 'open');

      let result: FooterButton;
      service.confirmUnsavedChanges().subscribe(button => (result = button));

      expect(result).toEqual(FooterButton.Discard);
      expect(service['confirm']).toHaveBeenCalledWith(translations.unsavedChanges);
      expect(service['open']).not.toHaveBeenCalled();
    }));
  });
});
