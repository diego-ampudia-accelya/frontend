import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';

import { VariableRemittanceViewModel } from '../../models/variable-remittance.model';
import { VariableRemittanceService } from '../../services/variable-remittance.service';
import { converter } from '../../shared/converter.utils';
import { VariableRemittanceListActions } from '../../store/actions';
import * as fromVariableRemittance from '../../store/reducers';
import { translations } from '../translations';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class ChangesDialogService {
  constructor(
    private store: Store<AppState>,
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private dataService: VariableRemittanceService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  private confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return this.store.pipe(
      select(fromVariableRemittance.getChanges),
      first(),
      switchMap(modifications =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    modifications: VariableRemittanceViewModel[],
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, question);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap((result: FooterButton) => {
        let dialogResult$ = of(result);

        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(modifications, dialogConfig);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(VariableRemittanceListActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(
    modifications: VariableRemittanceViewModel[],
    dialogConfig: ChangesDialogConfig
  ): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      dialogConfig.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(modifications.map(converter.toDataModel)).pipe(
        tap(() => this.store.dispatch(VariableRemittanceListActions.applyChangesSuccess())),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
