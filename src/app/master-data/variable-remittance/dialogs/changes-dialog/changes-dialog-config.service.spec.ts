import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'rxjs';

import { Frequency, VariableRemittanceViewModel } from '../../models/variable-remittance.model';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ButtonDesign, FooterButton } from '~app/shared/components';

describe('ChangesDialogConfigService', () => {
  let service: ChangesDialogConfigService;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const modifications: VariableRemittanceViewModel[] = [
    {
      id: '1',
      bsp: { id: 1 },
      agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
      frequency: Frequency.Four,
      original: {
        id: '1',
        bsp: { id: 1 },
        agent: { id: 1, iataCode: '78200010', name: 'AGENT 1' },
        frequency: Frequency.Two
      }
    }
  ];

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    service = new ChangesDialogConfigService(translationServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should format modifications changes correctly', () => {
    expect(service['formatChanges'](modifications)).toEqual([{ group: '', name: '', value: '78200010: 4' }]);
  });

  it('should build changes dialog configuration', () => {
    const question = { title: 'title', details: 'details' };

    expect(service.build(modifications, question)).toEqual({
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: [{ group: '', name: '', value: '78200010: 4' }],
      message: question.details
    });
  });
});
