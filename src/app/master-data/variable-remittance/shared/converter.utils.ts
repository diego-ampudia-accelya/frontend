import { VariableRemittance, VariableRemittanceViewModel } from '../models/variable-remittance.model';
import { PagedData } from '~app/shared/models/paged-data.model';

const toDataModel = (viewModel: VariableRemittanceViewModel): VariableRemittance => {
  const { original, ...dataModel } = viewModel;

  return dataModel;
};

const toViewModel = (dataModel: VariableRemittance): VariableRemittanceViewModel => ({
  ...dataModel,
  original: null
});

const toViewModels = (pagedData: PagedData<VariableRemittance>): PagedData<VariableRemittanceViewModel> => ({
  ...pagedData,
  records: (pagedData.records || []).map(toViewModel)
});

export const converter = { toDataModel, toViewModel, toViewModels };
