import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ChangesDialogConfigService } from './dialogs/changes-dialog/changes-dialog-config.service';
import { ChangesDialogService } from './dialogs/changes-dialog/changes-dialog.service';
import { OverrideDialogComponent } from './dialogs/override-dialog/override-dialog.component';
import { OverrideDialogService } from './dialogs/override-dialog/override-dialog.service';
import { RemoveDialogConfigService } from './dialogs/remove-dialog/remove-dialog-config.service';
import { RemoveDialogService } from './dialogs/remove-dialog/remove-dialog.service';
import { VariableRemittanceService } from './services/variable-remittance.service';
import { VariableRemittanceEffects } from './store/effects/variable-remittance.effects';
import * as fromVariableRemittance from './store/reducers';
import { VariableRemittanceRoutingModule } from './variable-remittance-routing.module';
import { VariableRemittanceComponent } from './variable-remittance/variable-remittance.component';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [VariableRemittanceComponent, OverrideDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    VariableRemittanceRoutingModule,
    StoreModule.forFeature(fromVariableRemittance.variableRemittanceFeatureKey, fromVariableRemittance.reducers),
    EffectsModule.forFeature([VariableRemittanceEffects])
  ],
  exports: [VariableRemittanceComponent],
  providers: [
    VariableRemittanceService,
    OverrideDialogService,
    ChangesDialogService,
    ChangesDialogConfigService,
    RemoveDialogService,
    RemoveDialogConfigService
  ]
})
export class VariableRemittanceModule {}
