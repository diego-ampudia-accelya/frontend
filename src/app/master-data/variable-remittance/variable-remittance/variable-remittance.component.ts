import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { isEmpty } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, first, map, skipWhile, switchMap, takeUntil } from 'rxjs/operators';

import { ChangesDialogService } from '../dialogs/changes-dialog/changes-dialog.service';
import { VariableRemittanceFilter } from '../models/variable-remittance-filter.model';
import { Frequency, VariableRemittanceViewModel } from '../models/variable-remittance.model';
import { ViewAs } from '../models/view-as.model';
import { VariableRemittanceFormatter } from '../services/variable-remittance-formatter.service';
import { VariableRemittanceService } from '../services/variable-remittance.service';
import { VariableRemittanceListActions } from '../store/actions';
import * as fromVariableRemittance from '../store/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { ButtonDesign, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-variable-remittance',
  templateUrl: './variable-remittance.component.html',
  styleUrls: ['./variable-remittance.component.scss'],
  providers: [VariableRemittanceFormatter]
})
export class VariableRemittanceComponent implements OnInit, CanComponentDeactivate, OnDestroy {
  public predefinedFilters: Partial<VariableRemittanceFilter> = {};

  public loggedUser: User;

  public data$ = this.store.pipe(select(fromVariableRemittance.getListData));
  public loading$ = this.store.pipe(select(fromVariableRemittance.getListLoading));
  public query$ = this.store.pipe(select(fromVariableRemittance.getListQuery));
  public hasData$ = this.store.pipe(select(fromVariableRemittance.hasData));
  public hasChanges$ = this.store.pipe(select(fromVariableRemittance.hasChanges));

  public downloadQuery: Partial<DataQuery>;

  public btnDesign = ButtonDesign;
  public listTitle = !this.isIataView ? 'LIST.MASTER_DATA.variableRemittance.title' + this.viewAs : null;
  public customLabels = { apply: 'LIST.MASTER_DATA.variableRemittance.buttonLabels.overrideFrequency' };

  public columns: Array<GridColumn>;
  public actions = [{ action: GridTableActionType.RemoveOverride }];

  public searchForm: FormGroup;

  public airlineDropdownOptions$: Observable<DropdownOption<AirlineSummary>[]>;
  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public frequencyDropdownOptions$: Observable<DropdownOption<number>[]>;
  public frequencyColumnDropdownOptions$: Observable<DropdownOption<number>[]>;
  public userBspOptions: DropdownOption<Bsp>[] = [];

  public agentDropdownPlaceholder: string;
  public isAgentDropdownLocked: boolean;
  public frequencyDropdownPlaceholder: string;
  public isFrequencyDropdownLocked: boolean;

  // Flags
  public hasLeanPermission: boolean;
  public isBspFilterLocked: boolean;
  public isSearchEnabled = false;
  public canShowDataQuery = true;
  public hasFilteringExpanded = false;

  public get viewAs(): ViewAs {
    return this.activatedRoute.snapshot.data.viewAs;
  }

  public get isAgentView(): boolean {
    return this.viewAs === ViewAs.Agent;
  }

  public get isAirlineView(): boolean {
    return this.viewAs === ViewAs.Airline;
  }

  public get isIataView(): boolean {
    return this.viewAs === ViewAs.Iata;
  }

  private selectedBsp: Bsp;
  private originalSelectedBsp: Bsp;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private destroy$ = new Subject();

  constructor(
    public displayFormatter: VariableRemittanceFormatter,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dataService: VariableRemittanceService,
    private airlineDictionary: AirlineDictionaryService,
    private agentDictionary: AgentDictionaryService,
    private changesDialogService: ChangesDialogService,
    private permissionsService: PermissionsService
  ) {
    this.dataService.buildBaseUrl(this.viewAs);
  }

  public ngOnInit(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    this.searchForm = this.buildForm();

    this.initializeFeaturesByLoggedUser();
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  public override(): void {
    this.store.dispatch(VariableRemittanceListActions.add());
  }

  public onDownload(): void {
    this.store.dispatch(VariableRemittanceListActions.download());
  }

  public onApplyChanges(): void {
    this.store.dispatch(VariableRemittanceListActions.applyChanges());
  }

  public onQueryChanged(query: DataQuery<VariableRemittanceFilter>): void {
    // Filtering stays expanded while the perform of the query is decided.
    // If it is finally perform, it will close (it should stay open otherwise)
    this.hasFilteringExpanded = true;

    if (!this.originalSelectedBsp || this.originalSelectedBsp === this.selectedBsp) {
      // If no bsp selected originally (first case scenario) or there is no change of bsp, we just perform the query
      this.performQueryOnChange(query);
    } else {
      this.changesDialogService
        .confirmUnsavedChanges()
        .pipe(map(result => result !== FooterButton.Cancel))
        .subscribe(shouldPerformQuery => {
          // If Discard or Apply buttons are pressed, we should proceed and perform the query. With Cancel no action is needed.
          if (shouldPerformQuery) {
            this.performQueryOnChange(query);
          }
        });
    }
  }

  public onRowPropertyChange(item: VariableRemittanceViewModel): void {
    this.store.dispatch(VariableRemittanceListActions.modify({ item }));
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: VariableRemittanceViewModel }): void {
    if (action.actionType === GridTableActionType.RemoveOverride) {
      this.store.dispatch(VariableRemittanceListActions.remove({ item: row }));
    }
  }

  private performQueryOnChange(query: DataQuery<VariableRemittanceFilter>): void {
    // Query is performed so the originalBsp is now the selected one
    this.originalSelectedBsp = this.selectedBsp;

    this.initializeSearchFlags(query);
    // We rebuild the columns, since the frequency dropdown could have changed
    this.initializeColumns();

    this.store.dispatch(VariableRemittanceListActions.search({ query }));
  }

  private initializeColumns(): void {
    this.columns = this.buildColumns();
  }

  private initializeFeaturesByLoggedUser(): void {
    combineLatest([this.loggedUser$, this.query$.pipe(first())]).subscribe(([loggedUser, query]) => {
      this.loggedUser = loggedUser;
      if (query) {
        if (this.hasLeanPermission) {
          this.initializeBsp(query);
        }

        this.initializeSearchFlags(query);
        this.initializeFiltersDropdown();
        this.initializeColumns();
        this.initializeData();
      }
    });
  }

  private initializeBsp(query: DataQuery<VariableRemittanceFilter>): void {
    const bspFromQuery = query.filterBy.bsp;

    if (bspFromQuery) {
      this.selectedBsp = bspFromQuery;
      this.setBspPredefinedFilter(bspFromQuery);
    }

    this.initializeBspListener();
    this.initializeBspOptions();
  }

  private setBspPredefinedFilter(bsp: Bsp): void {
    // We save predefined filters in our component (to provide it to list-view) and in the state (so Search action has it available)
    this.predefinedFilters = { ...this.predefinedFilters, bsp };
    this.store.dispatch(
      VariableRemittanceListActions.changePredefinedFilters({ predefinedFilters: this.predefinedFilters })
    );
  }

  private initializeBspOptions(): void {
    this.userBspOptions = this.filterBspsByPermission()?.map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    const firstBsp = this.userBspOptions[0].value;
    const oneBspWithPermission = this.userBspOptions?.length === 1;

    this.isBspFilterLocked = oneBspWithPermission;

    if (oneBspWithPermission && this.selectedBsp !== firstBsp) {
      // One bsp and selectedBsp !== firstBsp --> means either there is no bsp in query or it's different from the one provided now from BE
      const bspControl = FormUtil.get<VariableRemittanceFilter>(this.searchForm, 'bsp');
      bspControl.patchValue(firstBsp, { emitEvent: false });

      // We are manually patching the bsp, so we should call onBspSelectionChange to perform related side effects
      this.onBspSelectionChange(firstBsp);
    }
  }

  private initializeBspListener(): void {
    FormUtil.get<VariableRemittanceFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        distinctUntilChanged(),
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(selectedBspValue => {
        this.onBspSelectionChange(selectedBspValue);
      });
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readVariableRemittanceSettings;
    if (this.loggedUser) {
      const { bsps, bspPermissions } = this.loggedUser;

      return bsps.filter(bsp =>
        bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
      );
    }
  }

  private onBspSelectionChange(selectedBspValue: Bsp): void {
    if (selectedBspValue) {
      this.selectedBsp = selectedBspValue;
      this.isSearchEnabled = !isEmpty(selectedBspValue);

      this.setBspPredefinedFilter(this.selectedBsp);

      // Related bsp filters should be updated when bsp changes
      this.resetRelatedBspFilters();
      this.initializeFiltersDropdown();
    }
  }

  private resetRelatedBspFilters(): void {
    FormUtil.get<VariableRemittanceFilter>(this.searchForm, 'agent').reset();
    FormUtil.get<VariableRemittanceFilter>(this.searchForm, 'frequency').reset();
  }

  private initializeFiltersDropdown() {
    switch (this.viewAs) {
      case ViewAs.Agent:
        this.airlineDropdownOptions$ = this.airlineDictionary.getDropdownOptions();
        this.frequencyDropdownOptions$ = of(
          ArrayHelper.toDropdownOptions(Object.values(Frequency).filter(x => typeof x === 'number'))
        );

        break;

      case ViewAs.Airline:
      case ViewAs.Iata:
        if (this.hasLeanPermission) {
          if (this.selectedBsp) {
            // With a BSP selected, agent and frequency filters are now enabled
            this.enableBspDependentFilters();
            this.agentDropdownOptions$ = this.agentDictionary.getDropdownOptions({ bspId: this.selectedBsp.id });
            this.frequencyDropdownOptions$ = this.dataService.getFrequencyDropdownOptions(this.selectedBsp.id);
          } else {
            // With no BSP selected, we disable agent and frequency filters
            this.disableBspDependentFilters();
          }
        } else {
          // No lean permission, we keep previous behaviour
          this.agentDropdownOptions$ = this.agentDictionary.getDropdownOptions();
          this.frequencyDropdownOptions$ = this.store.pipe(
            select(getUser),
            switchMap(({ bsps }) => (bsps?.length ? this.dataService.getFrequencyDropdownOptions(bsps[0].id) : of([])))
          );
        }

        break;
    }
  }

  private enableBspDependentFilters(): void {
    this.agentDropdownPlaceholder = 'LIST.MASTER_DATA.variableRemittance.filters.agent.placeholder';
    this.isAgentDropdownLocked = false;

    this.frequencyDropdownPlaceholder = 'LIST.MASTER_DATA.variableRemittance.filters.frequency.placeholder';
    this.isFrequencyDropdownLocked = false;
  }

  private disableBspDependentFilters(): void {
    this.agentDropdownPlaceholder = 'LIST.MASTER_DATA.variableRemittance.filters.agent.disabledPlaceholder';
    this.isAgentDropdownLocked = true;

    this.frequencyDropdownPlaceholder = 'LIST.MASTER_DATA.variableRemittance.filters.frequency.disabledPlaceholder';
    this.isFrequencyDropdownLocked = true;
  }

  private initializeSearchFlags(query: DataQuery<VariableRemittanceFilter>): void {
    const isBspSelectedValue = !isEmpty(query.filterBy.bsp) || this.selectedBsp;
    const isSearchEnabled = !(this.hasLeanPermission && !isBspSelectedValue);

    this.canShowDataQuery = isSearchEnabled;
    this.hasFilteringExpanded = !isSearchEnabled;
    this.isSearchEnabled = isSearchEnabled;
  }

  private buildColumns(): Array<GridColumn> {
    // Search action has been dispatched so we frequencies values are both the same in filtering and columns (same bsp)
    this.frequencyColumnDropdownOptions$ = this.frequencyDropdownOptions$;

    return [
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 2,
        hidden: this.isAirlineView || this.isIataView
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineName',
        prop: 'airline.name',
        flexGrow: 4,
        hidden: this.isAirlineView || this.isIataView
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 2,
        hidden: this.isAgentView
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentName',
        prop: 'agent.name',
        flexGrow: 4,
        hidden: this.isAgentView
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.frequency',
        prop: 'frequency',
        flexGrow: 1,
        ...((this.isAirlineView || this.isIataView) && {
          dropdownOptions: this.frequencyColumnDropdownOptions$,
          cellTemplate: 'dropdownCellTmpl',
          cellClass: this.isChanged.bind(this)
        })
      }
    ];
  }

  private isChanged({ row, column }: { row: VariableRemittanceViewModel; column: TableColumn }): {
    [key: string]: boolean;
  } {
    const { prop } = column;

    return {
      'dropdown-control-changed': row.original && row.original[prop] !== row[prop],
      'dropdown-control': true
    };
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      bsp: [],
      airline: [],
      agent: [],
      frequency: []
    });
  }

  private initializeData(): void {
    if (!this.hasLeanPermission || (this.hasLeanPermission && !!this.selectedBsp)) {
      // We fetch query data only if the user is not Lean or they are Lean and have already selected a BSP
      this.store.dispatch(VariableRemittanceListActions.search({}));
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
