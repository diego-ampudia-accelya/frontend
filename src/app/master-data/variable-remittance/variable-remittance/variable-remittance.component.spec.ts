import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, of } from 'rxjs';

import { ChangesDialogService } from '../dialogs/changes-dialog/changes-dialog.service';
import { VariableRemittanceFilter } from '../models/variable-remittance-filter.model';
import { Frequency, VariableRemittanceViewModel } from '../models/variable-remittance.model';
import { ViewAs } from '../models/view-as.model';
import { VariableRemittanceFormatter } from '../services/variable-remittance-formatter.service';
import { VariableRemittanceService } from '../services/variable-remittance.service';
import { VariableRemittanceListActions } from '../store/actions';
import * as fromVariableRemittance from '../store/reducers';
import * as fromVariableRemittanceList from '../store/reducers/variable-remittance.reducer';
import { VariableRemittanceComponent } from './variable-remittance.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ArrayHelper } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('VariableRemittanceComponent', () => {
  let component: VariableRemittanceComponent;
  let fixture: ComponentFixture<VariableRemittanceComponent>;
  let mockStore: MockStore;

  const dataServiceSpy = createSpyObject(VariableRemittanceService);
  const airlineDictionarySpy = createSpyObject(AirlineDictionaryService);
  const agentDictionarySpy = createSpyObject(AgentDictionaryService);
  const changesDialogServiceSpy = createSpyObject(ChangesDialogService);
  const translationSpy = createSpyObject(L10nTranslationService);
  const permissionsSpy = createSpyObject(PermissionsService);

  const initialState = fromVariableRemittanceList.initialState;

  const mockActivatedRoute = {
    snapshot: {
      data: { viewAs: ViewAs.Agent }
    }
  };

  const mockBsp: Bsp = {
    id: 1,
    isoCountryCode: 'ES',
    name: 'Spain',
    effectiveFrom: '2021-05-10',
    effectiveTo: '2021-08-22',
    version: 1
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VariableRemittanceComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        VariableRemittanceFormatter,
        FormBuilder,
        provideMockStore({
          initialState: {
            auth: { user: { bsps: [] } },
            variableRemittance: { list: initialState }
          },
          selectors: [
            { selector: fromVariableRemittance.getListQuery, value: initialState.query },
            { selector: fromVariableRemittance.getListData, value: initialState.data },
            { selector: fromVariableRemittance.getListLoading, value: initialState.loading }
          ]
        }),
        { provide: VariableRemittanceService, useValue: dataServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionarySpy },
        { provide: AgentDictionaryService, useValue: agentDictionarySpy },
        { provide: ChangesDialogService, useValue: changesDialogServiceSpy },
        { provide: L10nTranslationService, useValue: translationSpy },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: PermissionsService, useValue: permissionsSpy }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VariableRemittanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should set correct columns on init as AGENT', () => {
    const expectedColumns: Array<GridColumn> = [
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 2,
        hidden: false
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineName',
        prop: 'airline.name',
        flexGrow: 4,
        hidden: false
      },

      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 2,
        hidden: true
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentName',
        prop: 'agent.name',
        flexGrow: 4,
        hidden: true
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.frequency',
        prop: 'frequency',
        flexGrow: 1
      }
    ];

    mockStore.setState({
      auth: { userType: UserType.AGENT, user: {} },
      variableRemittance: { list: initialState }
    });

    setViewAs(ViewAs.Agent);
    component.ngOnInit();

    expect(component.columns).toEqual(expectedColumns);
  });

  it('should set correct columns on init as AIRLINE', () => {
    const expectedColumns: Array<any> = [
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 2,
        hidden: true
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineName',
        prop: 'airline.name',
        flexGrow: 4,
        hidden: true
      },

      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 2,
        hidden: false
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentName',
        prop: 'agent.name',
        flexGrow: 4,
        hidden: false
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.frequency',
        prop: 'frequency',
        flexGrow: 1,
        dropdownOptions: jasmine.anything(),
        cellTemplate: 'dropdownCellTmpl',
        cellClass: jasmine.any(Function)
      }
    ];

    spyOn<any>(component, 'buildColumns').and.callThrough();
    mockStore.setState({
      auth: { userType: UserType.AIRLINE, user: {} },
      variableRemittance: { list: initialState }
    });

    setViewAs(ViewAs.Airline);
    component.ngOnInit();

    expect(component['buildColumns']).toHaveBeenCalled();
    expect(component.columns).toEqual(expectedColumns);
  });

  it('should set correct columns on init as IATA', () => {
    const expectedColumns: Array<any> = [
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 2,
        hidden: true
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.airlineName',
        prop: 'airline.name',
        flexGrow: 4,
        hidden: true
      },

      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 2,
        hidden: false
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.agentName',
        prop: 'agent.name',
        flexGrow: 4,
        hidden: false
      },
      {
        name: 'LIST.MASTER_DATA.variableRemittance.columns.frequency',
        prop: 'frequency',
        flexGrow: 1,
        dropdownOptions: jasmine.anything(),
        cellTemplate: 'dropdownCellTmpl',
        cellClass: jasmine.any(Function)
      }
    ];

    spyOn<any>(component, 'buildColumns').and.callThrough();
    mockStore.setState({
      auth: { userType: UserType.IATA, user: {} },
      variableRemittance: { list: initialState }
    });

    setViewAs(ViewAs.Iata);
    component.ngOnInit();

    expect(component['buildColumns']).toHaveBeenCalled();
    expect(component.columns).toEqual(expectedColumns);
  });

  describe('initialize column dropdowns', () => {
    let frequencies: DropdownOption<number>[];

    const mockFrequencies: DropdownOption<number>[] = [{ value: 1, label: '1' }];
    const frequencyDropdownSpy = dataServiceSpy.getFrequencyDropdownOptions.and.returnValue(of(mockFrequencies));

    beforeEach(() => frequencyDropdownSpy.calls.reset());

    it('should initialize frequency dropdown with correct bsp', fakeAsync(() => {
      mockStore.setState({
        auth: { userType: UserType.IATA, user: { bsps: [mockBsp] } },
        variableRemittance: { list: initialState }
      });

      setViewAs(ViewAs.Iata);
      component.ngOnInit();
      component.frequencyDropdownOptions$.subscribe(result => (frequencies = result));

      expect(frequencyDropdownSpy).toHaveBeenCalledWith(mockBsp.id);
      expect(frequencies).toEqual([{ value: 1, label: '1' }]);
    }));

    it('should initialize an empty frequency dropdown if there is no bsps property', fakeAsync(() => {
      mockStore.setState({
        auth: { userType: UserType.AIRLINE, user: {} },
        variableRemittance: { list: initialState }
      });

      setViewAs(ViewAs.Airline);
      component.ngOnInit();
      component.frequencyDropdownOptions$.subscribe(result => (frequencies = result));

      expect(frequencyDropdownSpy).not.toHaveBeenCalled();
      expect(frequencies).toEqual([]);
    }));

    it('should initialize an empty frequency dropdown if there is no bsps', fakeAsync(() => {
      mockStore.setState({
        auth: { userType: UserType.IATA, user: { bsps: [] } },
        variableRemittance: { list: initialState }
      });

      setViewAs(ViewAs.Iata);
      component.ngOnInit();
      component.frequencyDropdownOptions$.subscribe(result => (frequencies = result));

      expect(frequencyDropdownSpy).not.toHaveBeenCalled();
      expect(frequencies).toEqual([]);
    }));
  });

  it('should initialize data and dispatch search action with an empty query on init', () => {
    setViewAs(ViewAs.Iata);
    component.ngOnInit();

    expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.search({}));
  });

  it('should dispatch add action on override', () => {
    component.override();
    expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.add());
  });

  describe('initialize filter dropdowns', () => {
    beforeEach(() => {
      airlineDictionarySpy.getDropdownOptions.and.returnValue(of([]));
      agentDictionarySpy.getDropdownOptions.and.returnValue(of([]));
      spyOn(ArrayHelper, 'toDropdownOptions').and.callThrough();
    });

    it('should keep bsp dependent filters locked if there is no bsp selected and is Lean user', fakeAsync(() => {
      setViewAs(ViewAs.Airline);
      component.hasLeanPermission = true;
      component['selectedBsp'] = null;
      component['initializeFiltersDropdown']();
      expect(component.isAgentDropdownLocked).toBe(true);
      expect(component.isFrequencyDropdownLocked).toBe(true);
    }));

    it('should enable bsp dependent filters if there is bsp selected and is Lean user', fakeAsync(() => {
      setViewAs(ViewAs.Airline);
      component.hasLeanPermission = true;
      component['selectedBsp'] = mockBsp;
      component['initializeFiltersDropdown']();
      expect(component.isAgentDropdownLocked).toBe(false);
      expect(component.isFrequencyDropdownLocked).toBe(false);
    }));

    it('should initialize correct filter dropdowns on filter button click viewing as AGENT', fakeAsync(() => {
      let airlines: DropdownOption<AirlineSummary>[];
      let frequencies: DropdownOption<number>[];

      setViewAs(ViewAs.Agent);
      component['initializeFiltersDropdown']();
      component.airlineDropdownOptions$.subscribe(result => (airlines = result));
      component.frequencyDropdownOptions$.subscribe(result => (frequencies = result));

      expect(airlines).toEqual([]);
      expect(ArrayHelper.toDropdownOptions).toHaveBeenCalledWith([1, 2, 3, 4]);
      expect(frequencies).toEqual([
        { value: 1, label: '1' },
        { value: 2, label: '2' },
        { value: 3, label: '3' },
        { value: 4, label: '4' }
      ]);
    }));

    it('should initialize correct filter dropdowns on filter button click viewing as AIRLINE', fakeAsync(() => {
      let agents: DropdownOption<AgentSummary>[];

      setViewAs(ViewAs.Airline);
      component['selectedBsp'] = mockBsp;
      component['initializeFiltersDropdown']();
      component.agentDropdownOptions$.subscribe(result => (agents = result));

      expect(agents).toEqual([]);
      expect(ArrayHelper.toDropdownOptions).not.toHaveBeenCalled();
    }));

    it('should initialize correct filter dropdowns on filter button click viewing as IATA', fakeAsync(() => {
      let agents: DropdownOption<AgentSummary>[];

      setViewAs(ViewAs.Iata);
      component['selectedBsp'] = mockBsp;
      component['initializeFiltersDropdown']();
      component.agentDropdownOptions$.subscribe(result => (agents = result));

      expect(agents).toEqual([]);
      expect(ArrayHelper.toDropdownOptions).not.toHaveBeenCalled();
    }));
  });

  it('should dispatch download action on download', () => {
    component.onDownload();
    expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.download());
  });

  it('should dispatch apply changes action on apply changes button click', () => {
    component.onApplyChanges();
    expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.applyChanges());
  });

  it('should dispatch search action on query change if it is the first search', () => {
    const query: DataQuery<VariableRemittanceFilter> = {
      ...defaultQuery,
      filterBy: { frequency: [Frequency.Two] }
    };

    component.onQueryChanged(query);
    expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.search({ query }));
  });

  it('should open confirm changes dialog if changing between BSPs in Lean users', () => {
    const query: DataQuery<VariableRemittanceFilter> = {
      ...defaultQuery,
      filterBy: { frequency: [Frequency.Two] }
    };

    changesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(EMPTY));

    component['originalSelectedBsp'] = mockBsp;
    component['selectedBsp'] = { ...mockBsp, id: 3 };

    component.onQueryChanged(query);
    expect(changesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
  });

  it('should dispatch modify action on row property change', () => {
    const item: VariableRemittanceViewModel = {
      id: '69834855557820010',
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
      agent: { id: 69837820010, iataCode: '7820010', name: 'NOMBRE 7820010' },
      frequency: Frequency.Two,
      original: null
    };

    component.onRowPropertyChange(item);
    expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.modify({ item }));
  });

  it('should dispatch remove action on remove override action click', () => {
    const action: GridTableAction = { actionType: GridTableActionType.RemoveOverride, methodName: null };
    const item: VariableRemittanceViewModel = {
      id: '69834855557820010',
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      airline: { id: 6983485555, iataCode: '077', name: 'AIRLINE NAME 077' },
      agent: { id: 69837820010, iataCode: '7820010', name: 'NOMBRE 7820010' },
      frequency: Frequency.Two,
      original: null
    };

    component.onActionClick({ action, row: item });
    expect(mockStore.dispatch).toHaveBeenCalledWith(VariableRemittanceListActions.remove({ item }));
  });

  it('should NOT dispatch remove action on any other than remove override action click', () => {
    const action: GridTableAction = { actionType: GridTableActionType.Remove, methodName: null };

    component.onActionClick({ action, row: null });
    expect(mockStore.dispatch).not.toHaveBeenCalled();
  });

  function setViewAs(viewAs: ViewAs): void {
    component['activatedRoute'].snapshot.data.viewAs = viewAs;
  }
});
