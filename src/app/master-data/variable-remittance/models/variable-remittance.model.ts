/* eslint-disable @typescript-eslint/naming-convention */
import { AgentDto } from '~app/shared/models/agent.model';
import { BspDto } from '~app/shared/models/bsp.model';

export interface VariableRemittance {
  id: string;
  bsp: BspDto;
  agent: AgentDto;
  airline?: AirlineDto;
  frequency: Frequency;
}

export interface VariableRemittanceViewModel extends VariableRemittance {
  original: VariableRemittance;
}

interface AirlineDto {
  id: number;
  iataCode: string;
  name: string;
}

export enum Frequency {
  One = 1,
  Two = 2,
  Three = 3,
  Four = 4
}
