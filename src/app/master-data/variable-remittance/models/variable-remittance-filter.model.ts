import { Frequency } from './variable-remittance.model';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

export interface VariableRemittanceFilter {
  bsp?: Bsp;
  airline?: AirlineSummary[];
  agent?: AgentSummary[];
  frequency?: Frequency[];
}
