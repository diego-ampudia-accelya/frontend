/* eslint-disable @typescript-eslint/naming-convention */
export enum ViewAs {
  Agent = 'Agent',
  Airline = 'Airline',
  Iata = 'Iata'
}
