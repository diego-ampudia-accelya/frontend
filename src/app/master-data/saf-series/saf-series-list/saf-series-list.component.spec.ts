import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';

import { SafSeriesFilterFormatter } from '../shared/saf-series-filter-formatter.service';
import { SafSeriesListService } from '../shared/saf-series-list.service';
import * as fromSafSeries from '../store/reducers';
import * as fromSafSeriesList from '../store/reducers/saf-series.reducer';
import { SafSeriesListComponent } from './saf-series-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SafTypesListService } from '~app/master-data/saf-types/shared/saf-types-list.service';
import { GridTableComponent, InputComponent, SelectComponent } from '~app/shared/components';
import { ListViewComponent } from '~app/shared/components/list-view';
import { RangeDropdownComponent } from '~app/shared/components/range-dropdown/range-dropdown.component';
import { TranslatePipeMock } from '~app/test';

describe('SafSeriesListComponent', () => {
  let component: SafSeriesListComponent;
  let fixture: ComponentFixture<SafSeriesListComponent>;
  let mockStore: MockStore;
  let activatedRoute;
  const safTypesServiceSpy = createSpyObject(SafTypesListService);
  const safSeriesServiceSpy = createSpyObject(SafSeriesListService);

  const permissionServiceMock = jasmine.createSpyObj<PermissionsService>('PermissionsService', ['hasPermission']);

  permissionServiceMock.hasPermission.and.returnValue(true);

  const translationSpy = createSpyObject(L10nTranslationService);

  const mockSafTypeDropdownOptions = [
    {
      value: {
        id: 698360,
        type: 'ACM',
        description: 'ACM'
      },
      label: 'ACM'
    },
    {
      value: {
        id: 698353,
        type: 'ACM',
        description: 'ACM 53'
      },
      label: 'ACM 53'
    }
  ];

  const initialState = fromSafSeriesList.initialState;

  beforeEach(() => {
    activatedRoute = {
      snapshot: {
        data: {}
      },
      queryParams: of({ safType: 'ACM', description: 'ACM' })
    };

    TestBed.configureTestingModule({
      declarations: [
        SafSeriesListComponent,
        TranslatePipeMock,
        MockComponents(InputComponent, ListViewComponent, GridTableComponent, SelectComponent, RangeDropdownComponent)
      ],
      providers: [
        SafSeriesFilterFormatter,
        FormBuilder,
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: PermissionsService, useValue: permissionServiceMock },
        { provide: SafSeriesListService, useValue: safSeriesServiceSpy },
        { provide: SafTypesListService, useValue: safTypesServiceSpy },
        { provide: L10nTranslationService, useValue: translationSpy },
        provideMockStore({
          selectors: [
            { selector: fromSafSeries.getListQuery, value: initialState.query },
            { selector: fromSafSeries.getListData, value: initialState.data },
            { selector: fromSafSeries.getListLoading, value: initialState.loading }
          ]
        })
      ],
      imports: [ReactiveFormsModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SafSeriesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');

    safTypesServiceSpy.getDropdownOptions.and.returnValue(of(mockSafTypeDropdownOptions));
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
