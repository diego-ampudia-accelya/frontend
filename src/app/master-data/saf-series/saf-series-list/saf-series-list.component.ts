import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash/fp';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { SafSeriesFilterFormatter } from '../shared/saf-series-filter-formatter.service';
import { SafSeriesListService } from '../shared/saf-series-list.service';
import { SafSeries, SafSeriesFilter, SafType, SafTypeOptionForTranslate } from '../shared/saf-series.models';
import { SafSeriesActions } from '../store/actions';
import * as fromSafSeries from '../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SafTypesListService } from '~app/master-data/saf-types/shared/saf-types-list.service';
import { SafTypeDescription } from '~app/master-data/saf-types/shared/saf-types.models';
import { AppState } from '~app/reducers';
import { DataQuery, QUERYABLE } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-saf-series',
  templateUrl: './saf-series-list.component.html',
  providers: [DatePipe, { provide: QUERYABLE, useExisting: SafSeriesListService }]
})
export class SafSeriesListComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];

  public safTypesOptions = this.getEnumOptions(SafType, 'type');
  public safTypeDescriptions$: Observable<DropdownOption<SafTypeDescription>[]>;
  public customLabels = { create: 'LIST.MASTER_DATA.safSeries.createButtonLabel' };
  public actions: Array<{ action: GridTableActionType; disabled: boolean }>;
  public canCreateSafSerie =
    this.permissionsService.hasPermission(Permissions.createSafTypes) ||
    this.permissionsService.hasPermission(Permissions.createDocumentSeries);

  public data$ = this.store.pipe(select(fromSafSeries.getListData));
  public loading$ = this.store.pipe(select(fromSafSeries.getListLoading));
  public hasData$ = this.store.pipe(select(fromSafSeries.hasData));
  public query$ = this.store.pipe(select(fromSafSeries.getListQuery));

  private formFactory: FormUtil;
  private predefinedFilters: SafSeriesFilter;

  constructor(
    public displayFormatter: SafSeriesFilterFormatter,
    private formBuilder: FormBuilder,
    private permissionsService: PermissionsService,
    private safTypesListService: SafTypesListService,
    private activatedRoute: ActivatedRoute,
    protected store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.checkPermissionAndLoadData();
    this.initColumns();
    this.safTypeDescriptions$ = this.safTypesListService.getDropdownOptions();
  }

  public onQueryChanged(query: DataQuery<SafSeriesFilter>): void {
    this.store.dispatch(SafSeriesActions.search({ query }));
  }

  public onActionMenuClick(): void {
    const hasRemovePermission = this.permissionsService.hasPermission(
      Permissions.deleteDocumentSeries || Permissions.deleteDocumentTypes
    );

    this.actions = [{ action: GridTableActionType.Delete, disabled: !hasRemovePermission }];
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: SafSeries }) {
    if (action.actionType === GridTableActionType.Delete) {
      this.store.dispatch(SafSeriesActions.remove({ value: row }));
    }
  }

  public onDownload(): void {
    this.store.dispatch(SafSeriesActions.download());
  }

  public createSafSerie(): void {
    this.store.dispatch(SafSeriesActions.openAdd());
  }

  private checkPermissionAndLoadData(): void {
    if (
      this.permissionsService.hasPermission([
        Permissions.readSafTypes,
        Permissions.readDocumentTypes,
        Permissions.readDocumentSeries
      ])
    ) {
      this.setPredefinedFilters();
      this.getQuery();
    }
  }

  private setPredefinedFilters(): void {
    this.activatedRoute.queryParams.pipe(first()).subscribe(params => {
      if (Object.keys(params).length !== 0) {
        const type = params.safType.toLowerCase();
        const safType: SafTypeOptionForTranslate = {
          translationKey: `type.options.${type}`,
          value: params.safType
        };

        const description = params.description || undefined;

        this.predefinedFilters = {
          type: safType ? [safType] : [],
          description: description
            ? [
                {
                  id: 1,
                  type: safType ? safType.value : '',
                  description
                }
              ]
            : []
        };
      }
    });
  }

  private getQuery(): void {
    this.query$.pipe(first()).subscribe(query => {
      this.loadData(query);
    });
  }

  public loadData(query: DataQuery<SafSeriesFilter>): void {
    query = query || cloneDeep(defaultQuery);

    const dataQuery: DataQuery<SafSeriesFilter> = {
      ...query,
      filterBy: {
        ...query.filterBy,
        ...this.predefinedFilters
      }
    };

    this.store.dispatch(SafSeriesActions.search({ query: dataQuery }));
    this.predefinedFilters = {};
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<SafSeriesFilter>({
      type: [],
      description: [],
      rangeFrom: [],
      rangeTo: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'type',
        name: 'LIST.MASTER_DATA.safSeries.columns.type',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'description',
        name: 'LIST.MASTER_DATA.safSeries.columns.description',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'rangeFrom',
        name: 'LIST.MASTER_DATA.safSeries.columns.rangeFrom',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'rangeTo',
        name: 'LIST.MASTER_DATA.safSeries.columns.rangeTo',
        width: 140,
        resizeable: true,
        draggable: false
      }
    ];
  }

  private getEnumOptions(options: any, filterName: string): DropdownOption<SafTypeOptionForTranslate>[] {
    return Object.keys(options).map((key: string) => ({
      value: { translationKey: `${filterName}.options.${key}`, value: options[key] },
      label: this.displayFormatter.translate(`${filterName}.options.${key}`)
    }));
  }
}
