import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SafSeriesFilter } from './saf-series.models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { joinMapper, rangeFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class SafSeriesFilterFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: SafSeriesFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<SafSeriesFilter>> = {
      type: type =>
        `${this.translate('type.label')} - ${joinMapper(type.map(safType => this.translate(safType.translationKey)))}`,
      description: description =>
        `${this.translate('description.label')} - ${joinMapper(
          description.map(safDescription => safDescription.description)
        )}`,
      rangeFrom: rangeFrom => `${this.translate('rangeFrom.label')}: ${rangeFilterTagMapper(rangeFrom)}`,
      rangeTo: rangeTo => `${this.translate('rangeTo.label')}: ${rangeFilterTagMapper(rangeTo)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public translate(key: string): string {
    return this.translationService.translate(`LIST.MASTER_DATA.safSeries.filters.${key}`);
  }
}
