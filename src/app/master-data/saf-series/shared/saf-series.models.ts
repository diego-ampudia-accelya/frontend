import { SafTypeDescription } from '~app/master-data/saf-types/shared/saf-types.models';

export interface SafTypeOptionForTranslate {
  translationKey: string;
  value: string;
}

export enum SafType {
  adm = 'ADM',
  acm = 'ACM',
  ra = 'RA',
  rn = 'RN'
}

export interface SafSeries {
  id: number;
  type: SafTypeOptionForTranslate[];
  typeId: number;
  description: SafTypeDescription[];
  rangeFrom?: number[];
  rangeTo?: number[];
}

export interface SafSeriesFilter {
  type?: SafTypeOptionForTranslate[];
  description?: SafTypeDescription[];
  rangeFrom?: number[];
  rangeTo?: number[];
}

export interface SafSeriesFilterBE {
  type?: string[];
  description?: string[];
  fromRangeFrom?: number;
  toRangeFrom?: number;
  fromRangeTo?: number;
  toRangeTo?: number;
}
