import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { SafSeries, SafSeriesFilter, SafSeriesFilterBE } from './saf-series.models';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class SafSeriesListService implements Queryable<SafSeries> {
  private bspId: number;
  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(fromAuth.getUserBsps), first()).subscribe(bsps => {
      this.bspId = bsps && bsps[0] ? bsps[0].id : null;
      this.baseUrl = `${this.appConfiguration.baseApiPath}/bsp-management/bsps/${this.bspId}/saf-series`;
    });
  }

  public find(query: DataQuery<SafSeriesFilter>): Observable<PagedData<SafSeries>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<SafSeries>>(this.baseUrl + requestQuery.getQueryString());
  }

  public create(item: SafSeriesFilter): Observable<SafSeries> {
    return this.http.post<SafSeries>(this.baseUrl, item);
  }

  public delete(item: SafSeries): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${item.id}`);
  }

  public download(
    query: DataQuery<SafSeriesFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<SafSeriesFilter>>): RequestQuery<SafSeriesFilterBE> {
    const { type, description, rangeFrom, rangeTo, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        type: type && type.map(v => v.value),
        description: description && description.map(d => d.description),
        fromRangeFrom: rangeFrom && rangeFrom[0],
        toRangeFrom: rangeFrom && rangeFrom[1],
        fromRangeTo: rangeTo && rangeTo[0],
        toRangeTo: rangeTo && rangeTo[1]
      }
    });
  }
}
