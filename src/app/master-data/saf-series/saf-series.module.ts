import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { SafTypesListService } from '../saf-types/shared/saf-types-list.service';

import { DeleteDialogConfigService } from './dialogs/delete-dialog/delete-dialog-config.service';
import { DeleteDialogService } from './dialogs/delete-dialog/delete-dialog.service';
import { SafSeriesChangeDialogComponent } from './dialogs/saf-series-changes-dialog/saf-series-changes-dialog/saf-series-changes-dialog.component';
import { ChangesDialogConfigService } from './dialogs/saf-series-changes-dialog/services/changes-dialog-config.service';
import { ChangesDialogService } from './dialogs/saf-series-changes-dialog/services/changes-dialog.service';
import { SafSeriesListComponent } from './saf-series-list/saf-series-list.component';
import { SafSeriesRoutingModule } from './saf-series-routing.module';
import { SafSeriesFilterFormatter } from './shared/saf-series-filter-formatter.service';
import { SafSeriesListService } from './shared/saf-series-list.service';
import { SafSeriesEffects } from './store/effects/saf-series.effects';
import * as fromSafSeries from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [SafSeriesListComponent, SafSeriesChangeDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    SafSeriesRoutingModule,
    L10nTranslationModule,
    StoreModule.forFeature(fromSafSeries.safSeriesFeatureKey, fromSafSeries.reducers),
    EffectsModule.forFeature([SafSeriesEffects])
  ],
  providers: [
    SafSeriesListService,
    SafSeriesFilterFormatter,
    SafTypesListService,
    DeleteDialogService,
    DeleteDialogConfigService,
    ChangesDialogService,
    ChangesDialogConfigService
  ]
})
export class SafSeriesModule {}
