import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { DeleteDialogService } from '../../dialogs/delete-dialog/delete-dialog.service';
import { ChangesDialogService } from '../../dialogs/saf-series-changes-dialog/services/changes-dialog.service';
import { SafSeriesListService } from '../../shared/saf-series-list.service';
import { SafSeriesActions } from '../actions';
import * as fromSafSeries from '../reducers';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class SafSeriesEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private dataService: SafSeriesListService,
    private dialogService: DialogService,
    private changesDialogService: ChangesDialogService,
    private removeDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafSeriesActions.search),
      switchMap(() => of(SafSeriesActions.searchConfirmed()))
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafSeriesActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromSafSeries.getListQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(data => SafSeriesActions.searchSuccess({ data })),
          rethrowError(() => SafSeriesActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafSeriesActions.download),
      switchMap(result => (result ? of(SafSeriesActions.downloadConfirmed()) : EMPTY))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafSeriesActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromSafSeries.getListQuery))),
        switchMap(([_, query]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  public remove$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafSeriesActions.remove),
        switchMap(({ value }) => this.removeDialogService.open(value))
      ),
    { dispatch: false }
  );

  public removeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafSeriesActions.removeSuccess),
        tap(({ value }) => {
          const deleteMessage = this.translationService.translate(
            'LIST.MASTER_DATA.safSeries.dialog.delete.deleteMessage',
            {
              item: value.description
            }
          );
          this.notificationService.showSuccess(deleteMessage);

          return this.store.dispatch(SafSeriesActions.searchConfirmed());
        })
      ),

    { dispatch: false }
  );

  public $openAdd = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafSeriesActions.openAdd),
        switchMap(() => this.changesDialogService.open())
      ),
    { dispatch: false }
  );

  public add$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafSeriesActions.add),
      switchMap(({ value }) =>
        this.changesDialogService.create(value).pipe(
          map(() => SafSeriesActions.addSuccess()),
          rethrowError(error => SafSeriesActions.addError(error))
        )
      )
    )
  );

  public addSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafSeriesActions.addSuccess),
        tap(() => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(
            'LIST.MASTER_DATA.safSeries.dialog.create.createMessage'
          );
          this.notificationService.showSuccess(successMessage);

          return this.store.dispatch(SafSeriesActions.searchConfirmed());
        })
      ),
    { dispatch: false }
  );
}
