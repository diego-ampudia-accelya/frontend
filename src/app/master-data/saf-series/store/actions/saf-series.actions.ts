import { createAction, props } from '@ngrx/store';

import { SafSeries, SafSeriesFilter } from '../../shared/saf-series.models';
import { DataQuery } from '~app/shared/components/list-view';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { PagedData } from '~app/shared/models/paged-data.model';

export const search = createAction('[Saf Series] Search', props<{ query?: DataQuery<SafSeriesFilter> }>());
export const searchConfirmed = createAction('[Saf Series] Search Confirmed');
export const searchCancelled = createAction('[Saf Series] Search Cancelled');
export const searchSuccess = createAction('[Saf Series] Search Success', props<{ data: PagedData<SafSeries> }>());
export const searchError = createAction('[Saf Series] Search Error');

export const download = createAction('[Saf Series] Download');
export const downloadConfirmed = createAction('[Saf Series] Download Confirmed');

export const remove = createAction('[Saf Series] Remove', props<{ value: SafSeries }>());
export const removeSuccess = createAction('[Saf Series] Remove Success', props<{ value: SafSeries }>());
export const removeError = createAction('[Saf Series] Remove Error', props<{ error: ResponseErrorBE }>());

export const openAdd = createAction('[Saf Series] Open Add');
export const add = createAction('[Saf Series] Add', props<{ value: SafSeriesFilter }>());
export const addSuccess = createAction('[Saf Series] Add Success');
export const addError = createAction('[Saf Series] Add Error', props<{ error }>());
