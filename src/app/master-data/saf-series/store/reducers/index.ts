import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './saf-series.reducer';
import { AppState } from '~app/reducers';

export const safSeriesFeatureKey = 'safSeries';

export interface State extends AppState {
  [safSeriesFeatureKey]: SafSeriesState;
}

export interface SafSeriesState {
  [fromList.key]: fromList.State;
}

export function reducers(state: SafSeriesState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getSafSeries = createFeatureSelector<State, SafSeriesState>(safSeriesFeatureKey);

export const getList = createSelector(getSafSeries, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getListQuery = createSelector(getList, fromList.getQuery);
