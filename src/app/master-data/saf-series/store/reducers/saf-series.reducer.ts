import { createReducer, on } from '@ngrx/store';

import { SafSeries, SafSeriesFilter } from '../../shared/saf-series.models';
import { SafSeriesActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ResponseErrorBE } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<SafSeriesFilter>;
  previousQuery: DataQuery<SafSeriesFilter>;
  data: SafSeries[];
  loading: boolean;
  errors: { error: ResponseErrorBE };
}

export const initialState: State = {
  query: defaultQuery,
  previousQuery: null,
  data: [],
  loading: false,
  errors: null
};

const loadData = (state: State, data: PagedData<SafSeries>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

export const reducer = createReducer(
  initialState,
  on(SafSeriesActions.openAdd, state => ({ ...state })),
  on(SafSeriesActions.search, (state, { query = state.query }) => ({
    ...state,
    previousQuery: state.query,
    query
  })),
  on(SafSeriesActions.searchConfirmed, state => ({
    ...state,
    previousQuery: null,
    loading: true
  })),
  on(SafSeriesActions.searchCancelled, state => ({
    ...state,
    query: state.previousQuery,
    previousQuery: null
  })),
  on(SafSeriesActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(SafSeriesActions.searchError, state => ({
    ...state,
    loading: false
  })),
  on(SafSeriesActions.add, state => ({ ...state, loading: true })),
  on(SafSeriesActions.addSuccess, state => ({ ...state, loading: false })),
  on(SafSeriesActions.addError, (state, { error }) => ({
    ...state,
    loading: false,
    errors: error
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;
