import { SafSeries, SafSeriesFilter, SafType } from '../../shared/saf-series.models';
import { SafSeriesActions } from '../actions';
import * as fromSafSeries from './saf-series.reducer';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

describe('Saf Series Reducer', () => {
  const query: DataQuery<SafSeriesFilter> = {
    ...defaultQuery,
    filterBy: {
      description: [
        {
          id: 698353,
          type: SafType.adm,
          description: 'ACM 53'
        }
      ]
    }
  };

  const safSeries: SafSeries[] = [
    {
      id: 999,
      typeId: 999,
      type: [
        {
          translationKey: 'test',
          value: SafType.adm
        }
      ],
      description: [
        {
          id: 698353,
          type: SafType.adm,
          description: 'ACM 53'
        }
      ]
    }
  ];

  it('should update state correctly on SEARCH action', () => {
    const action = SafSeriesActions.search({ query });
    const state = fromSafSeries.reducer(fromSafSeries.initialState, action);
    const geQuery = fromSafSeries.getQuery(state);

    expect(state.query).toEqual(query);
    expect(state.previousQuery).toEqual(defaultQuery);
    expect(geQuery).toEqual(state.query);
  });

  it('should update state correctly on SEARCH CONFIRMED action', () => {
    const initialState: fromSafSeries.State = {
      ...fromSafSeries.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = SafSeriesActions.searchConfirmed();
    const state = fromSafSeries.reducer(initialState, action);

    expect(state.previousQuery).toBeNull();
    expect(state.loading).toBe(true);
  });

  it('should update state correctly on SEARCH CANCELLED action', () => {
    const initialState: fromSafSeries.State = {
      ...fromSafSeries.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = SafSeriesActions.searchCancelled();
    const state = fromSafSeries.reducer(initialState, action);

    expect(state.query).toEqual(defaultQuery);
    expect(state.previousQuery).toBeNull();
  });

  it('should update state correctly on SEARCH SUCCESS action', () => {
    const data: PagedData<SafSeries> = {
      records: safSeries,
      pageSize: 20,
      total: safSeries.length,
      totalPages: 1,
      pageNumber: 0
    };

    const action = SafSeriesActions.searchSuccess({ data });
    const state = fromSafSeries.reducer(fromSafSeries.initialState, action);
    const getData = fromSafSeries.getData(state);

    expect(state.loading).toBeFalsy();
    expect(state.data).toEqual(safSeries);
    expect(state.query).toEqual(
      jasmine.objectContaining({ paginateBy: { page: 0, size: 20, totalElements: safSeries.length } })
    );
    expect(getData).toEqual(state.data);
  });

  it('should set loading false on SEARCH ERROR action', () => {
    const action = SafSeriesActions.searchError();
    const state = fromSafSeries.reducer(fromSafSeries.initialState, action);

    expect(state.loading).toBeFalsy();
  });
});
