import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SafSeriesListComponent } from './saf-series-list/saf-series-list.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: SafSeriesListComponent,
    data: {
      tab: ROUTES.SAF_SERIES
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SafSeriesRoutingModule {}
