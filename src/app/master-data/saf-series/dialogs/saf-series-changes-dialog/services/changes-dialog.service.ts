import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { translations } from '../../translations';
import { SafSeriesChangeDialogComponent } from '../saf-series-changes-dialog/saf-series-changes-dialog.component';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { SafSeriesListService } from '~app/master-data/saf-series/shared/saf-series-list.service';
import { SafSeries, SafSeriesFilter } from '~app/master-data/saf-series/shared/saf-series.models';
import { DialogService } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private dataService: SafSeriesListService
  ) {}

  public open(item?: SafSeries): Observable<any> {
    const buildTranslations = item ? translations.edit : translations.create;
    const dialogConfig = this.configBuilder.build(buildTranslations);

    return this.dialogService.open(SafSeriesChangeDialogComponent, dialogConfig);
  }

  public create(item: SafSeriesFilter): Observable<SafSeries> {
    return this.dataService.create(item);
  }
}
