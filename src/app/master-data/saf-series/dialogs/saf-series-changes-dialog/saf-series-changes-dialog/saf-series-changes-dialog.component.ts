import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

import { SafSeries, SafType, SafTypeOptionForTranslate } from '../../../shared/saf-series.models';
import { SafSeriesFilterFormatter } from '~app/master-data/saf-series/shared/saf-series-filter-formatter.service';
import { SafSeriesActions } from '~app/master-data/saf-series/store/actions';
import { SafTypesListService } from '~app/master-data/saf-types/shared/saf-types-list.service';
import { SafTypeDescription } from '~app/master-data/saf-types/shared/saf-types.models';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-saf-series-changes-dialog',
  templateUrl: './saf-series-changes-dialog.component.html',
  styleUrls: ['./saf-series-changes-dialog.component.scss']
})
export class SafSeriesChangeDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;
  public typeControl: FormControl;
  public descriptionControl: FormControl;
  public rangeFromControl: FormControl;
  public rangeToControl: FormControl;
  public safTypesOptions = this.getEnumOptions(SafType, 'type');
  public safTypeDescriptions$: Observable<DropdownOption<SafTypeDescription>[]>;

  private createButton: ModalAction;

  private destroy$ = new Subject();

  private createButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Create),
    takeUntil(this.destroy$)
  );

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    public store: Store<AppState>,
    public displayFormatter: SafSeriesFilterFormatter,
    private safTypesListService: SafTypesListService
  ) {}

  public ngOnInit(): void {
    this.buildForm();
    this.initializeButtons();
    this.initializeFormListeners();
    this.initializeSafTypeDescriptions();
  }

  public ngAfterViewInit(): void {
    this.subscribeToButtonStatus();
  }

  private subscribeToButtonStatus(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const isFormInvalid = status !== 'VALID';
      this.setIsDisabledForSaveButton(isFormInvalid);
    });
  }

  private setIsDisabledForSaveButton(isDisabled: boolean): void {
    if (this.createButton) {
      this.createButton.isDisabled = isDisabled;
    }
  }

  private buildForm(): void {
    this.typeControl = new FormControl(null, Validators.required);
    this.descriptionControl = new FormControl(null, Validators.required);
    this.rangeFromControl = new FormControl(null, Validators.required);
    this.rangeToControl = new FormControl(null, Validators.required);

    this.form = this.formBuilder.group({
      type: this.typeControl,
      description: this.descriptionControl,
      rangeFrom: this.rangeFromControl,
      rangeTo: this.rangeToControl
    });
  }

  private initializeButtons(): void {
    this.createButton = this.config.data.buttons.find(button => button.type === FooterButton.Create);

    if (this.createButton) {
      this.createButton.isDisabled = true;
    }
  }

  private initializeFormListeners(): void {
    this.createButtonClick$.subscribe(() => {
      const valueToDispatch: Partial<SafSeries> = {
        typeId: this.descriptionControl.value.id,
        rangeFrom: this.rangeFromControl.value,
        rangeTo: this.rangeToControl.value
      };
      this.store.dispatch(SafSeriesActions.add({ value: valueToDispatch }));
    });
  }

  private getEnumOptions(options: any, filterName: string): DropdownOption<SafTypeOptionForTranslate>[] {
    return Object.keys(options).map((key: string) => ({
      value: { translationKey: `${filterName}.options.${key}`, value: options[key] },
      label: this.displayFormatter.translate(`${filterName}.options.${key}`)
    }));
  }

  private initializeSafTypeDescriptions(): void {
    this.typeControl.valueChanges.subscribe(filterString => {
      this.safTypeDescriptions$ = this.safTypesListService
        .getDropdownOptions()
        .pipe(
          map(descriptions =>
            descriptions.filter(description => !filterString || description.value.type === filterString.value)
          )
        );
      this.descriptionControl.reset();
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
