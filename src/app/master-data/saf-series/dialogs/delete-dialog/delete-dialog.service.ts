import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';

import { SafSeriesListService } from '../../shared/saf-series-list.service';
import { SafSeries } from '../../shared/saf-series.models';
import { SafSeriesActions } from '../../store/actions';
import { translations } from '../translations';

import { DeleteDialogConfigService } from './delete-dialog-config.service';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class DeleteDialogService {
  constructor(
    private store: Store<AppState>,
    private dialogService: DialogService,
    private configBuilder: DeleteDialogConfigService,
    private dataService: SafSeriesListService
  ) {}

  public open(item: SafSeries): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(item, translations.delete);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap((result: FooterButton) =>
        result === FooterButton.Delete ? this.remove(item, dialogConfig) : of(result)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private remove(item: SafSeries, dialogConfig: ChangesDialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      dialogConfig.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.delete(item).pipe(
        tap(() => this.store.dispatch(SafSeriesActions.removeSuccess({ value: item }))),
        mapTo(FooterButton.Delete),
        finalize(() => setLoading(false))
      );
    });
  }
}
