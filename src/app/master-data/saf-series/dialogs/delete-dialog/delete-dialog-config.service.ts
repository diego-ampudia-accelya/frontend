import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SafSeries } from '../../shared/saf-series.models';
import { ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable()
export class DeleteDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(item: SafSeries, question: { title: string; details: string }): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Delete }]
      },
      changes: this.formatChanges(item),
      message
    };
  }

  private formatChanges(item: SafSeries): ChangeModel[] {
    return [
      {
        group: '',
        name: this.translation.translate('LIST.MASTER_DATA.safSeries.dialog.edit.type.label'),
        value: `${item.type}`
      },
      {
        group: '',
        name: this.translation.translate('LIST.MASTER_DATA.safSeries.dialog.edit.description.label'),
        value: `${item.description}`
      },
      {
        group: '',
        name: this.translation.translate('LIST.MASTER_DATA.safSeries.dialog.edit.rangeFrom.label'),
        value: `${item.rangeFrom}`
      },
      {
        group: '',
        name: this.translation.translate('LIST.MASTER_DATA.safSeries.dialog.edit.rangeTo.label'),
        value: `${item.rangeTo}`
      }
    ];
  }
}
