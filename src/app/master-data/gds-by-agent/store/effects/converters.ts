import { GdsByAgent, GdsByAgentViewModel } from '../../models';
import { PagedData } from '~app/shared/models/paged-data.model';

const toViewModel = (dataModel: GdsByAgent): GdsByAgentViewModel => ({ ...dataModel, original: dataModel });

const toDataModel = (viewModel: GdsByAgentViewModel): GdsByAgent => {
  const { original, ...dataModel } = viewModel;

  return dataModel;
};

const toViewModels = (pagedData: PagedData<GdsByAgent>): PagedData<GdsByAgentViewModel> => ({
  ...pagedData,
  records: (pagedData?.records || []).map(toViewModel)
});

export const converter = { toViewModel, toDataModel, toViewModels };
