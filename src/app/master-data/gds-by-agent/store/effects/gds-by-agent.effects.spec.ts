import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, ReplaySubject, throwError } from 'rxjs';

import { GdsByAgent, GdsByAgentFilter, GdsByAgentViewModel } from '../../models';
import { GdsByAgentService } from '../../services/gds-by-agent.service';
import { GdsByAgentListActions } from '../actions';
import * as fromGdsByAgent from '../reducers';
import { GdsByAgentEffects } from './gds-by-agent.effects';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';
import { NotificationService } from '~app/shared/services/notification.service';

describe('GdsByAgentEffects', () => {
  let effects: GdsByAgentEffects;
  let gdsAgentListServiceStub: SpyObject<GdsByAgentService>;
  let dialogServiceStub: SpyObject<DialogService>;
  let notificationServiceStub: SpyObject<NotificationService>;
  let actions$: Observable<Action>;
  let mockStore: MockStore<AppState>;

  const gdsByAgent1: GdsByAgent = {
    id: 1,
    bsp: { id: 1, isoCountryCode: 'Spain', name: 'GDS1' },
    gds: { id: 2, gdsCode: '202054', name: 'GdsName1' }
  };
  const gdsByAgent2: GdsByAgent = {
    id: 2,
    bsp: { id: 3, isoCountryCode: 'Spain', name: 'GDS2' },
    gds: { id: 4, gdsCode: '202055', name: 'GdsName2' }
  };

  beforeEach(waitForAsync(() => {
    gdsAgentListServiceStub = createSpyObject(GdsByAgentService);
    notificationServiceStub = createSpyObject(NotificationService);
    dialogServiceStub = createSpyObject(DialogService);
    dialogServiceStub.open.and.returnValue(of(null));

    TestBed.configureTestingModule({
      providers: [
        GdsByAgentEffects,
        provideMockStore({
          selectors: [
            { selector: fromGdsByAgent.getRequiredFilter, value: {} },
            { selector: fromGdsByAgent.getListQuery, value: defaultQuery }
          ]
        }),
        provideMockActions(() => actions$),
        { provide: GdsByAgentService, useValue: gdsAgentListServiceStub },
        { provide: NotificationService, useValue: notificationServiceStub },
        { provide: DialogService, useValue: dialogServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(GdsByAgentEffects);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('`changeRequiredFilter$`', () => {
    it('should dispatch `changeRequiredFilterSuccess` with the found data and options', fakeAsync(() => {
      const data: PagedData<GdsByAgent> = {
        records: [gdsByAgent1, gdsByAgent2],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const expectedData: PagedData<GdsByAgentViewModel> = {
        records: [
          { ...gdsByAgent1, original: gdsByAgent1 },
          { ...gdsByAgent2, original: gdsByAgent2 }
        ],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const requiredFilter: GdsByAgentFilter = {
        gdsAssignment: true
      };

      const expectedAction = GdsByAgentListActions.changeRequiredFilterSuccess({
        data: expectedData
      });

      mockStore.overrideSelector(fromGdsByAgent.getRequiredFilter, requiredFilter);
      gdsAgentListServiceStub.find.and.returnValue(of(data));

      actions$ = of(GdsByAgentListActions.changeRequiredFilter({ requiredFilter }));

      effects.changeRequiredFilter$.subscribe((action: any) => {
        expect(action).toEqual(expectedAction);
      });
    }));

    it('should dispatch `changeRequiredFilterError` when `find` throws an error', fakeAsync(() => {
      const requiredFilter: GdsByAgentFilter = {
        gdsAssignment: true
      };
      const expectedAction = GdsByAgentListActions.changeRequiredFilterError();

      mockStore.overrideSelector(fromGdsByAgent.getRequiredFilter, requiredFilter);
      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));

      actions$ = of(GdsByAgentListActions.changeRequiredFilter({ requiredFilter }));

      effects.changeRequiredFilter$.subscribe(
        (action: any) => {
          expect(action).toEqual(expectedAction);
        },
        error => expect(error).toBeDefined()
      );
    }));

    it('should continue processing actions after an error', fakeAsync(() => {
      const requiredFilter: GdsByAgentFilter = {
        gdsAssignment: true
      };
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const actionsSubject = new ReplaySubject<Action>(1);

      actions$ = actionsSubject.asObservable();

      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));
      actionsSubject.next(GdsByAgentListActions.changeRequiredFilter({ requiredFilter }));
      tick();

      gdsAgentListServiceStub.find.and.returnValue(of(data));
      actionsSubject.next(GdsByAgentListActions.changeRequiredFilter({ requiredFilter }));
      tick();

      const successAction = GdsByAgentListActions.changeRequiredFilterSuccess({
        data
      });

      const actions = [];
      effects.changeRequiredFilter$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );

      tick();
      expect(actions).toEqual([successAction]);
    }));
  });

  describe('`search$`', () => {
    it('should dispatch searchConfirmed action when the user has confirmed the operation', fakeAsync(() => {
      actions$ = of(GdsByAgentListActions.search({ query: defaultQuery }));

      let result;
      effects.search$.subscribe(action => (result = action));

      expect(result.type).toEqual(GdsByAgentListActions.searchConfirmed.type);
    }));
  });

  describe('`searchConfirmed$`', () => {
    it('should dispatch `searchSuccess` with found data', fakeAsync(() => {
      const searchAction = GdsByAgentListActions.searchConfirmed();
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 0,
        pageSize: 20,
        pageNumber: 0
      };
      const expectedAction = GdsByAgentListActions.searchSuccess({ data });
      mockStore.overrideSelector(fromGdsByAgent.getListQuery, defaultQuery);
      actions$ = of(searchAction);
      gdsAgentListServiceStub.find.and.returnValue(of(data));

      const actions = [];
      effects.searchConfirmed$.subscribe(action => actions.push(action));

      expect(actions).toEqual([expectedAction]);
    }));

    it('should dispatch `searchError` when find throws an error', fakeAsync(() => {
      const searchAction = GdsByAgentListActions.searchConfirmed();
      const expectedAction = GdsByAgentListActions.searchError();
      actions$ = of(searchAction);
      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));

      const actions = [];
      effects.searchConfirmed$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );
      tick();
      expect(actions).toEqual([expectedAction]);
    }));

    it('should continue processing actions after an error', fakeAsync(() => {
      const searchAction = GdsByAgentListActions.searchConfirmed();
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 0,
        pageSize: 20,
        pageNumber: 0
      };
      const actionsSubject = new ReplaySubject<Action>(1);

      actions$ = actionsSubject.asObservable();

      const actions = [];

      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));
      actionsSubject.next(searchAction);

      gdsAgentListServiceStub.find.and.returnValue(of(data));
      actionsSubject.next(searchAction);

      effects.searchConfirmed$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );
      tick();
      const successAction = GdsByAgentListActions.searchSuccess({ data });
      expect(actions).toEqual([successAction]);
    }));
  });
});
