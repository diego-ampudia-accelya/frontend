import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';

import { GdsByAgentService } from '../../services/gds-by-agent.service';
import { GdsByAgentListActions } from '../actions';
import * as fromGdsByAgent from '../reducers';

import { converter } from './converters';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class GdsByAgentEffects {
  constructor(
    private actions$: Actions,
    private dataService: GdsByAgentService,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {}

  public changeRequiredFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsByAgentListActions.changeRequiredFilter),
      withLatestFrom(
        this.store.pipe(select(fromGdsByAgent.getListQuery)),
        this.store.pipe(select(fromGdsByAgent.getRequiredFilter))
      ),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(result =>
            GdsByAgentListActions.changeRequiredFilterSuccess({
              data: converter.toViewModels(result)
            })
          ),
          rethrowError(() => GdsByAgentListActions.changeRequiredFilterError())
        )
      )
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsByAgentListActions.search),
      map(dialogResult =>
        !dialogResult ? GdsByAgentListActions.searchCancelled() : GdsByAgentListActions.searchConfirmed()
      )
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsByAgentListActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromGdsByAgent.getListQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(converter.toViewModels),
          map(data => GdsByAgentListActions.searchSuccess({ data })),
          rethrowError(() => GdsByAgentListActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsByAgentListActions.download),
      switchMap(() => of(GdsByAgentListActions.downloadConfirmed()))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(GdsByAgentListActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromGdsByAgent.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );
}
