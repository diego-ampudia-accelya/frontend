import { createAction, props } from '@ngrx/store';

import { GdsByAgentFilter, GdsByAgentViewModel } from '../../models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const changeRequiredFilter = createAction(
  '[Gds By Agent] Change Required Filter',
  props<{ requiredFilter: GdsByAgentFilter }>()
);
export const changeRequiredFilterSuccess = createAction(
  '[Gds By Agent] Change Required Filter Success',
  props<{
    data: PagedData<GdsByAgentViewModel>;
  }>()
);
export const changeRequiredFilterError = createAction('[Gds By Agent] Change Required Filter Error');

export const search = createAction('[Gds By Agent] Search', props<{ query?: DataQuery<GdsByAgentFilter> }>());
export const searchConfirmed = createAction('[Gds By Agent] Search Confirmed');
export const searchCancelled = createAction('[Gds By Agent] Search Cancelled');
export const searchSuccess = createAction(
  '[Gds By Agent] Search Success',
  props<{ data: PagedData<GdsByAgentViewModel> }>()
);
export const searchError = createAction('[Gds By Agent] Search Error');
export const download = createAction('[Gds By Agent] Download');
export const downloadConfirmed = createAction('[Gds By Agent] Download Confirmed');
