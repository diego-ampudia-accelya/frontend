/* eslint-disable @typescript-eslint/naming-convention */
import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './list.reducer';
import { AppState } from '~app/reducers';

export const GdsByAgentFeatureKey = 'gdsByAgent';

export interface State extends AppState {
  [GdsByAgentFeatureKey]: GdsByAgentState;
}

export interface GdsByAgentState {
  [fromList.key]: fromList.State;
}

export function reducers(state: GdsByAgentState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getGdsByAgent = createFeatureSelector<State, GdsByAgentState>(GdsByAgentFeatureKey);

export const getList = createSelector(getGdsByAgent, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getListQuery = createSelector(getList, fromList.getQuery);

export const getDownloadQuery = createSelector(
  getListQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

export const getRequiredFilter = createSelector(getList, fromList.getRequiredFilter);
