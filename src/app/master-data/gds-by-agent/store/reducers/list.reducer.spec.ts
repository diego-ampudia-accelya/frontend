import { GdsByAgentFilter, GdsByAgentViewModel } from '../../models';
import { GdsByAgentListActions } from '../actions';

import * as fromList from './list.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DataQuery } from '~app/shared/components/list-view';

describe('gds by agent reducer', () => {
  describe('on search', () => {
    it('should update query on load', () => {
      const action = GdsByAgentListActions.search({ query: defaultQuery });
      const expected: DataQuery<GdsByAgentFilter> = {
        ...defaultQuery
      };

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.query).toEqual(expected);
    });

    it('should set loading to true on searchConfirmed', () => {
      const action = GdsByAgentListActions.searchConfirmed();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeTruthy();
    });

    it('should set loading to false on searchSuccess', () => {
      const action = GdsByAgentListActions.searchSuccess({
        data: {
          records: [{} as GdsByAgentViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set totalElements and data on searchSuccess', () => {
      const action = GdsByAgentListActions.searchSuccess({
        data: {
          records: [{} as GdsByAgentViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data.length).toBe(1);
      expect(newState.query.paginateBy).toEqual({ page: 0, size: 20, totalElements: 1 });
    });

    it('should set loading to false on searchError', () => {
      const action = GdsByAgentListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set data to [] on searchError', () => {
      const action = GdsByAgentListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data).toEqual([]);
    });
  });

  describe('on changeRequiredFilter', () => {
    it('should preserve query when it matches required filter', () => {
      const existingQuery: DataQuery<GdsByAgentFilter> = {
        ...defaultQuery,
        filterBy: { gdsAssignment: true }
      };

      const requiredFilter = { gdsAssignment: true };

      const state: fromList.State = {
        ...fromList.initialState,
        requiredFilter,
        query: existingQuery
      };
      const action = GdsByAgentListActions.changeRequiredFilter({
        requiredFilter
      });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(existingQuery);
    });

    it('should reset to default query when the query does not match required filter', () => {
      const existingQuery: DataQuery<GdsByAgentFilter> = {
        ...defaultQuery,
        filterBy: { gdsAssignment: true }
      };
      const requiredFilter: GdsByAgentFilter = { gdsAssignment: true };
      const expectedQuery: DataQuery<GdsByAgentFilter> = {
        ...defaultQuery,
        filterBy: requiredFilter
      };
      const state: fromList.State = {
        ...fromList.initialState,
        query: existingQuery
      };
      const action = GdsByAgentListActions.changeRequiredFilter({ requiredFilter });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(expectedQuery);
    });
  });
});
