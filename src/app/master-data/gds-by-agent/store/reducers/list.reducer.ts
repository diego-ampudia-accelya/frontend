import { createReducer, on } from '@ngrx/store';
import { cloneDeep, isEqual } from 'lodash';

import { GdsByAgent, GdsByAgentFilter, GdsByAgentViewModel } from '../../models';
import { GdsByAgentListActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<GdsByAgentFilter>;
  previousQuery: DataQuery<GdsByAgentFilter>;
  data: GdsByAgentViewModel[];
  loading: boolean;
  requiredFilter: GdsByAgentFilter;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false,
  requiredFilter: {}
};

const loadData = (state: State, data: PagedData<GdsByAgentViewModel>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (
  newRequiredFilter: GdsByAgentFilter,
  oldRequiredFilter: GdsByAgentFilter,
  storedQuery: DataQuery<GdsByAgentFilter>
): DataQuery<GdsByAgentFilter> => {
  let query: DataQuery<GdsByAgentFilter> = storedQuery || cloneDeep(defaultQuery);

  if (!isEqual(newRequiredFilter, oldRequiredFilter)) {
    query = cloneDeep(defaultQuery);
    query.filterBy = newRequiredFilter;
  }

  return query;
};

export const reducer = createReducer(
  initialState,
  on(GdsByAgentListActions.changeRequiredFilter, (state, { requiredFilter }) => ({
    ...state,
    requiredFilter,
    loading: true,
    query: determineInitialQuery(requiredFilter, state.requiredFilter, state.query)
  })),
  on(GdsByAgentListActions.changeRequiredFilterSuccess, (state, { data }) => ({
    ...loadData(state, data)
  })),
  on(GdsByAgentListActions.search, (state, { query = state.query }) => ({
    ...state,
    query: {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    },
    previousQuery: state.query
  })),
  on(GdsByAgentListActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(GdsByAgentListActions.searchCancelled, state => ({
    ...state,
    previousQuery: null,
    query: state.previousQuery
  })),
  on(GdsByAgentListActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(GdsByAgentListActions.searchError, GdsByAgentListActions.changeRequiredFilterError, state => ({
    ...state,
    loading: false,
    data: []
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;

export const getRequiredFilter = ({ requiredFilter }: State) => requiredFilter;

export const hasChanges = (state: State) => getChanges(state).length > 0;

export const getChanges = ({ data }: State) => data.filter(({ original, ...item }) => !isEqual(item, original));

export const haveSameId = (a: GdsByAgent, b: GdsByAgent) => a.id === b.id;
