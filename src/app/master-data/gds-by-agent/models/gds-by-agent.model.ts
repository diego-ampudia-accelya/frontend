export interface GdsByAgent {
  id: number;
  gds: { id: number; gdsCode: string; name: string };
  bsp: { id: number; isoCountryCode: string; name: string };
}

export interface GdsByAgentViewModel extends GdsByAgent {
  original: GdsByAgent;
}
