import { Gds } from '~app/master-data/models/gds.model';

export interface GdsByAgentFilter {
  gds?: Gds[];
  gdsAssignment?: boolean;
}
