import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';

import { GdsByAgentService } from './gds-by-agent.service';
import { GdsService } from '~app/master-data/services';
import { AppConfigurationService } from '~app/shared/services';

describe('GdsAgentListService', () => {
  let service: GdsByAgentService;

  const activatedRouteStub = {
    snapshot: {
      data: {},
      params: {}
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GdsService,
        GdsByAgentService,
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, { baseApiPath: 'api' }),
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ]
    });
    service = TestBed.inject(GdsByAgentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
