import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GdsByAgent, GdsByAgentFilter } from '../models';
import { GdsService } from '~app/master-data/services/gds.service';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class GdsByAgentService implements Queryable<GdsByAgent> {
  private baseUrl: string;
  private gdsId: string;

  private sortMapper = [
    {
      from: 'gds.name',
      to: 'gds.name'
    },
    {
      from: 'gds.gdsCode',
      to: 'gds.gdsCode'
    }
  ];

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private gdsService: GdsService
  ) {
    this.baseUrl = `${this.appConfiguration.baseApiPath}/agent-management/agents/`;
  }

  public setGdsId(gdsId) {
    this.gdsId = gdsId;
  }

  public find(query: DataQuery<GdsByAgentFilter>): Observable<PagedData<GdsByAgent>> {
    const requestQuery = this.formatQuery(query);
    const url = `${this.baseUrl}${this.gdsId}/gds-assignments` + requestQuery.getQueryString();

    return this.http.get<PagedData<GdsByAgent>>(url);
  }

  public download(
    query: DataQuery<GdsByAgentFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, downloadType: downloadFormat };
    const url = `${this.baseUrl}${this.gdsId}/gds-assignments/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public getGdsCodeNameDropdownOptions(): Observable<DropdownOption[]> {
    return this.gdsService.getByAgentCodeNameDropdownOptions();
  }

  private formatQuery(query: DataQuery<GdsByAgentFilter>): RequestQuery {
    const { gds, gdsAssignment } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        'gds.id': gds && gds.map(gd => gd.id),
        gdsAssignment
      },
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
  }
}
