import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { memoize } from 'lodash';

import { GdsByAgentFilter, ViewGdsByAgentUse } from '../models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class GdsByAgentFormatterService implements FilterFormatter {
  public ignoredFilters: Array<keyof GdsByAgentFilter> = [];

  constructor(private translation: L10nTranslationService) {
    this.formatUsed = memoize(this.formatUsed).bind(this);
  }

  public format(filter: GdsByAgentFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<GdsByAgentFilter>> = {
      gds: gds => `${this.translate('gdsCodeFilterLabel')} - ${gds.map(gd => gd.name).join(', ')}`,
      gdsAssignment: use => `${this.formatUsed(use)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public formatUsed(use: boolean): string {
    return this.translate(use ? ViewGdsByAgentUse.use : ViewGdsByAgentUse.notUse);
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.MASTER_DATA.gdsByAgent.' + key);
  }
}
