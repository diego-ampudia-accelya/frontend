import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { GdsByAgentFilter } from '../models';

import { GdsByAgentFormatterService } from './gds-by-agent-formatter.service';
import { Gds } from '~app/master-data/models/gds.model';

describe('GdsByAgentFormatterService', () => {
  let formatter: GdsByAgentFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new GdsByAgentFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format when gds assignment filter is defined', () => {
    const filter: GdsByAgentFilter = {
      gdsAssignment: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['gdsAssignment'],
        label: 'LIST.MASTER_DATA.gdsByAgent.gdsAssigned'
      }
    ]);
  });

  it('should format when gds filter is defined', () => {
    const filter: GdsByAgentFilter = {
      gdsAssignment: true,
      gds: [
        {
          id: 0,
          gdsCode: '123',
          name: 'GDS0'
        },
        {
          id: 1,
          gdsCode: '456',
          name: 'GDS1'
        }
      ] as Gds[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['gdsAssignment'],
        label: 'LIST.MASTER_DATA.gdsByAgent.gdsAssigned'
      },
      {
        keys: ['gds'],
        label: 'LIST.MASTER_DATA.gdsByAgent.gdsCodeFilterLabel - GDS0, GDS1'
      }
    ]);
  });

  it('should return empty array when the filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      unknownProp: 'test'
    };
    expect(formatter.format(filter)).toEqual([]);
  });
});
