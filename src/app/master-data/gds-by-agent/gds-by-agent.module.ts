import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { GdsByAgentComponent } from './gds-by-agent/gds-by-agent.component';
import { GdsByAgentEffects } from './store/effects/gds-by-agent.effects';
import * as fromGdsByAgent from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [GdsByAgentComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromGdsByAgent.GdsByAgentFeatureKey, fromGdsByAgent.reducers),
    EffectsModule.forFeature([GdsByAgentEffects])
  ],
  exports: [GdsByAgentComponent]
})
export class GdsByAgentModule {}
