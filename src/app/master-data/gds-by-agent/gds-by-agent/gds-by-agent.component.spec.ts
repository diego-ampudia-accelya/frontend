import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { GdsByAgentFormatterService } from '../services/gds-by-agent-formatter.service';
import { GdsByAgentService } from '../services/gds-by-agent.service';
import { GdsByAgentListActions } from '../store/actions';
import * as fromList from '../store/reducers/list.reducer';

import { GdsByAgentComponent } from './gds-by-agent.component';
import { TranslatePipeMock } from '~app/test';
import { UserType } from '~app/shared/models/user.model';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

describe('GdsByAgentComponent', () => {
  let component: GdsByAgentComponent;

  let fixture: ComponentFixture<GdsByAgentComponent>;
  let displayFormatterStub: SpyObject<GdsByAgentFormatterService>;
  let activatedRoute;
  let gdsByAgentServiceStub: SpyObject<GdsByAgentService>;
  let mockStore: MockStore;

  const expectedUserDetails = {
    id: 12345,
    email: 'iata@user.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.IATA,
    permissions: []
  };

  beforeEach(waitForAsync(() => {
    displayFormatterStub = createSpyObject(GdsByAgentFormatterService);
    activatedRoute = {
      snapshot: {
        data: {}
      }
    };

    gdsByAgentServiceStub = createSpyObject(GdsByAgentService);

    TestBed.configureTestingModule({
      declarations: [GdsByAgentComponent, TranslatePipeMock],
      providers: [
        provideMockStore({
          initialState: {
            auth: {
              user: expectedUserDetails
            },
            gdsByAgent: {
              list: fromList.initialState
            }
          },
          selectors: [{ selector: fromAuth.hasPermission('READ-GDS-BY-AGENT'), value: true }]
        }),
        FormBuilder,
        { provide: ActivatedRoute, useValue: activatedRoute },
        {
          provide: GdsByAgentService,
          useValue: gdsByAgentServiceStub
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(GdsByAgentComponent, {
        set: {
          providers: [{ provide: GdsByAgentFormatterService, useValue: displayFormatterStub }]
        }
      })
      .compileComponents();

    mockStore = TestBed.inject<any>(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdsByAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch search when query change', () => {
    spyOn(mockStore, 'dispatch');

    component.onQueryChanged(defaultQuery);

    expect(mockStore.dispatch).toHaveBeenCalledWith(GdsByAgentListActions.search({ query: defaultQuery }));
  });

  it('should dispatch download', () => {
    spyOn(mockStore, 'dispatch');

    component.onDownload();

    expect(mockStore.dispatch).toHaveBeenCalledWith(GdsByAgentListActions.download());
  });
});
