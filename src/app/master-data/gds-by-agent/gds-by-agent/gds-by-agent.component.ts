/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { GdsByAgentFilter, ViewAs } from '../models';
import { GdsByAgentFormatterService } from '../services/gds-by-agent-formatter.service';
import { GdsByAgentService } from '../services/gds-by-agent.service';
import { GdsByAgentListActions } from '../store/actions';
import * as fromGdsByAgent from '../store/reducers';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User, UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-gds-by-agent',
  templateUrl: './gds-by-agent.component.html',
  styleUrls: ['./gds-by-agent.component.scss'],
  providers: [GdsByAgentFormatterService]
})
export class GdsByAgentComponent implements OnInit {
  public loading$ = this.store.pipe(select(fromGdsByAgent.getListLoading));
  public data$ = this.store.pipe(select(fromGdsByAgent.getListData));
  public query$ = this.store.pipe(select(fromGdsByAgent.getListQuery));
  public hasData$ = this.store.pipe(select(fromGdsByAgent.hasData));
  public predefinedFilters: GdsByAgentFilter = { gdsAssignment: true };

  public columns$: Observable<TableColumn[]>;
  public searchForm: FormGroup;

  public loggedUser: User;

  public gdsUseOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatUsed(value)
  }));

  public get isAgentView(): boolean {
    return this.viewAs === ViewAs.Agent;
  }

  public get isIataUser(): boolean {
    return this.loggedUser.userType === UserType.IATA;
  }

  public gdsCodeOptions$: Observable<DropdownOption[]>;

  public get viewAs(): ViewAs {
    return this.activatedRoute.snapshot.data.viewAs;
  }

  public ButtonDesign = ButtonDesign;

  public isUpdateAllVisible = false;

  constructor(
    public displayFormatter: GdsByAgentFormatterService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private dataService: GdsByAgentService
  ) {}

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.columns$ = this.store.pipe(
      select(fromAuth.hasPermission(Permissions.readGdsByAgent)),
      map(canReadAll => (canReadAll ? this.getColumns() : []))
    );
    this.searchForm = this.buildForm();
    this.displayFormatter.ignoredFilters = this.getIgnoredFilters();
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(fromAuth.getUser), first());
  }

  private initializeLoggedUser() {
    this.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });
  }

  public onQueryChanged(query: DataQuery<GdsByAgentFilter>): void {
    this.store.dispatch(GdsByAgentListActions.search({ query }));
  }

  public onDownload(): void {
    this.store.dispatch(GdsByAgentListActions.download());
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.initializeFiltersDropdown();
    }
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      gds: [],
      gdsAssignment: []
    });
  }

  private initializeFiltersDropdown() {
    this.gdsCodeOptions$ = this.dataService.getGdsCodeNameDropdownOptions();
  }

  private getColumns(): Array<TableColumn & { viewAs?: ViewAs }> {
    return [
      {
        name: 'LIST.MASTER_DATA.GDS.COLUMNS.CODE',
        prop: 'gds.gdsCode',
        flexGrow: 5,
        viewAs: ViewAs.Agent
      },
      {
        name: 'LIST.MASTER_DATA.GDS.COLUMNS.NAME',
        prop: 'gds.name',
        flexGrow: 5,
        viewAs: ViewAs.Agent
      }
    ].filter(column => !column.viewAs || column.viewAs === this.viewAs);
  }

  private getIgnoredFilters(): Array<keyof GdsByAgentFilter> {
    return !this.isIataUser ? ['gdsAssignment'] : [];
  }
}
