import { Component, OnDestroy, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { clone } from 'lodash';
import { Subject } from 'rxjs';
import { filter, finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { AgentGroupService } from '../services/agent-group.service';
import { AgentGroupIndividualAccess } from '~app/master-data/enums';
import { MasterDataAgentGroup, MasterDataAgentGroupMember } from '~app/master-data/models/agent-group.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-modify-individual-access-dialog',
  templateUrl: './modify-individual-access-dialog.component.html',
  styleUrls: ['./modify-individual-access-dialog.component.scss']
})
export class ModifyIndividualAccessDialogComponent implements OnInit, OnDestroy {
  public member: MasterDataAgentGroupMember;
  public agentGroup: MasterDataAgentGroup;

  public individualAccessOptions: DropdownOption<AgentGroupIndividualAccess>[];

  public get modifyButton(): ModalAction {
    return this.config.data?.buttons?.find((b: ModalAction) => b.type === FooterButton.Modify);
  }

  private destroy$ = new Subject();
  private initialIndividualAccess: AgentGroupIndividualAccess;

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private agentGroupService: AgentGroupService
  ) {}

  public ngOnInit(): void {
    this.individualAccessOptions = [AgentGroupIndividualAccess.Forbidden, AgentGroupIndividualAccess.Permitted].map(
      value => ({
        value,
        label: this.generateIndividualAccessOptionLabel(value)
      })
    );

    this.member = clone(this.config.data.member);
    this.agentGroup = clone(this.config.data.agentGroup);
    this.initialIndividualAccess = this.member.individualAccess;

    this.modifyButton.isDisabled = true;

    this.handleActions();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  public handleIndividualAccessChange(access: AgentGroupIndividualAccess): void {
    this.modifyButton.isDisabled = this.initialIndividualAccess === access;
  }

  private handleActions(): void {
    this.reactiveSubject.asObservable
      .pipe(
        filter(res => res.clickedBtn === FooterButton.Modify),
        switchMap(() => {
          this.modifyButton.isLoading = true;

          const groupId = this.agentGroup.id;
          const memberId = this.member.id;
          const individualAccess = this.member.individualAccess;

          return this.agentGroupService
            .modifyMemberIndividualAccess(groupId, memberId, individualAccess)
            .pipe(finalize(() => this.dialogService.close()));
        }),
        tap(() => {
          this.notificationService.showSuccess('MASTER_DATA.agentGroupMembers.modifyIndividualAccessDialog.successMsg');
          this.reactiveSubject.emit(FooterButton.Modify);
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private generateIndividualAccessOptionLabel(value: AgentGroupIndividualAccess) {
    return this.translationService.translate(`MASTER_DATA.agentGroupMembers.individualAccess.${value}`);
  }
}
