import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AgentGroupService } from '../services/agent-group.service';
import { ModifyIndividualAccessDialogComponent } from './modify-individual-access-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('ModifyIndividualAccessDialogComponent', () => {
  let component: ModifyIndividualAccessDialogComponent;
  let fixture: ComponentFixture<ModifyIndividualAccessDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ModifyIndividualAccessDialogComponent, TranslatePipeMock],
      providers: [
        mockProvider(DialogConfig, {
          data: {
            member: {},
            agentGroup: {},
            buttons: [
              {
                type: FooterButton.Modify,
                isDisabled: false,
                isLoading: false
              }
            ]
          }
        }),
        mockProvider(ReactiveSubject, { asObservable: of() }),
        mockProvider(DialogService),
        mockProvider(L10nTranslationService),
        mockProvider(NotificationService),
        mockProvider(AgentGroupService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyIndividualAccessDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
