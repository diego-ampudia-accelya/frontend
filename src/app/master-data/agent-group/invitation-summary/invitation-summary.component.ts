import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { AgentGroupInvitation } from '~app/master-data/models/agent-group.model';

@Component({
  selector: 'bspl-invitation-summary',
  templateUrl: './invitation-summary.component.html',
  styleUrls: ['./invitation-summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvitationSummaryComponent {
  @Input() invitation: AgentGroupInvitation;
}
