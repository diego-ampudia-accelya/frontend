import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';

import { InvitationSummaryComponent } from './invitation-summary.component';
import { AgentGroupIndividualAccess } from '~app/master-data/enums';

describe('InvitationSummaryComponent', () => {
  let component: InvitationSummaryComponent;
  let fixture: ComponentFixture<InvitationSummaryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InvitationSummaryComponent, MockPipe(L10nTranslatePipe)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitationSummaryComponent);
    component = fixture.componentInstance;
    component.invitation = {
      id: '1',
      group: {
        user: {
          userTypeDetails: { iataCode: '001' }
        }
      } as any,
      individualAccess: AgentGroupIndividualAccess.Permitted
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
