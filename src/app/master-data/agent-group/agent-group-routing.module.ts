import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { NotificationSettingsResolver } from '../airline/services/notification-settings.resolver';
import { ConfigurationComponent } from '../configuration';
import { SettingsViewComponent } from '../configuration/settings-view/settings-view.component';
import { UnsavedChangesGuard } from '../configuration/unsaved-changes.guard';
import { RefundAuthorityComponent, ViewAs } from '../refund-authority';
import { SftpConfigurationsResolver } from '../sftp/services/sftp-configurations-resolver.service';
import { SftpAccountsComponent } from '../sftp/sftp-accounts/sftp-accounts.component';
import { TicketingAuthorityComponent } from '../ticketing-authority';

import { EmailAlertsSettingsComponent } from '../configuration/email-alert-settings/email-alert-settings.component';
import { AgentGroupListComponent } from './agent-group-list/agent-group-list.component';
import { AgentGroupMemberListComponent } from './agent-group-member-list/agent-group-member-list.component';
import { AgentGroupProfileComponent } from './agent-group-profile/agent-group-profile.component';
import { AgentGroupResolver } from './services/agent-group.resolver';
import { EmailAlertsSettingResolver } from './services/email-alerts-setting.resolver';
import { MyAgentGroupResolver } from './services/my-agent-group.resolver';
import { RefundAuthorityRequiredFilterResolver } from './services/refund-authority-required-filter.resolver';
import { TicketingAuthorityResolver } from './services/ticketing-authority.resolver';
import { UserType } from '~app/shared/models/user.model';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';
import { CanComponentDeactivateGuard } from '~app/core/services';

const configurationRoute: Route = {
  path: 'configuration',
  component: ConfigurationComponent,
  data: {
    title: 'menu.masterData.configuration.title',
    requiredPermissions: [Permissions.readEmailAlerts, Permissions.readSftpAccount]
  },
  children: [
    {
      path: 'email-alerts-settings',
      resolve: {
        loadSettings: EmailAlertsSettingResolver
      },
      component: EmailAlertsSettingsComponent,

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.emailAlerts.groupName',
        title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.emailAlerts.sectionTitle'
        },
        requiredPermissions: Permissions.readEmailAlerts
      }
    },
    {
      path: 'sftp-settings',
      resolve: {
        loadSettings: SftpConfigurationsResolver
      },
      component: SftpAccountsComponent,
      data: {
        group: 'menu.masterData.configuration.sftpConfigurations.groupName',
        title: 'menu.masterData.configuration.sftpConfigurations.sectionTitle',
        requiredPermissions: [Permissions.readSftpAccount]
      }
    },
    {
      path: 'notifications',
      component: SettingsViewComponent,
      resolve: {
        loadSettings: NotificationSettingsResolver
      },

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.notificationsGroupName',
        title: 'menu.masterData.configuration.notificationsSettings',
        configuration: {
          title: 'menu.masterData.configuration.notificationsSettingsTitle',
          permissions: {
            read: Permissions.readNotificationSettings,
            update: Permissions.updateNotificationSettings
          }
        },
        requiredPermissions: [Permissions.readNotificationSettings]
      },
      runGuardsAndResolvers: 'always'
    }
  ]
};

const agentGroupProfileChildren: Routes = [
  configurationRoute,
  {
    path: 'members',
    component: AgentGroupMemberListComponent,
    resolve: {
      agentGroup: AgentGroupResolver
    },
    data: {
      title: 'menu.masterData.memberList.title',
      viewAs: ViewAs.AgentGroup,
      requiredPermissions: Permissions.readAgentGroup
    }
  },
  {
    path: 'ticketing-authority',
    canDeactivate: [CanComponentDeactivateGuard],
    component: TicketingAuthorityComponent,
    resolve: {
      requiredFilter: TicketingAuthorityResolver
    },
    data: {
      title: 'menu.masterData.ticketingAuthority.title',
      viewAs: ViewAs.AgentGroup,
      requiredPermissions: Permissions.readTicketingAuthority
    }
  },
  {
    path: 'refund-authority',
    component: RefundAuthorityComponent,
    resolve: {
      requiredFilter: RefundAuthorityRequiredFilterResolver
    },
    data: {
      title: 'LIST.MASTER_DATA.refundAuthority.rnTabLabel',
      viewAs: ViewAs.AgentGroup,
      requiredPermissions: [Permissions.readRNIssueAgent]
    }
  }
];

const routes: Routes = [
  {
    path: 'my',
    component: AgentGroupProfileComponent,
    resolve: {
      agent: MyAgentGroupResolver
    },
    data: {
      tab: ROUTES.MY_AGENT_GROUP,
      allowedUserTypes: [UserType.AGENT_GROUP]
    },
    children: agentGroupProfileChildren
  },
  {
    path: '',
    component: AgentGroupListComponent,
    data: {
      tab: ROUTES.AGENT_GROUP,
      requiredPermissions: Permissions.readAgentGroup
    }
  },
  {
    path: ROUTES.AGENT_GROUP_MEMBERS.path,
    component: AgentGroupMemberListComponent,
    resolve: {
      agentGroup: AgentGroupResolver
    },
    data: {
      tab: ROUTES.AGENT_GROUP_MEMBERS
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentGroupRoutingModule {}
