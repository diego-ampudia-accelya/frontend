import { AgentGroupStatus } from '~app/master-data/enums/agent-group-status.enum';
import { Bsp } from '~app/shared/models/bsp.model';

export interface AgentGroupFilter {
  bspId: Bsp;
  iataCode: string;
  creator: string;
  organization: string;
  name: string;
  email: string;
  status: AgentGroupStatus;
  address: string;
  locality: string;
  postCode: string;
  telephone: string;
  members: string;
}
