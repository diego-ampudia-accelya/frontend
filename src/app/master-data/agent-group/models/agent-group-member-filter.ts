import { AgentGroupIndividualAccess, AgentGroupMembership } from '~app/master-data/enums';
import { AgentSummary } from '~app/shared/models';

export interface AgentGroupMemberFilter {
  agents: AgentSummary[];
  membership: AgentGroupMembership[];
  individualAccess: AgentGroupIndividualAccess;
  organization: string;
  name: string;
  email: string;
  telephone: string;
  address: string;
  locality: string;
  postCode: string;
}
