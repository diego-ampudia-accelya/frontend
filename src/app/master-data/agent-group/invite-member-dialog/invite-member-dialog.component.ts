import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';
import { catchError, filter, finalize, map, startWith, switchMap, tap } from 'rxjs/operators';

import { AgentGroupService } from '../services/agent-group.service';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { AgentGroupIndividualAccess } from '~app/master-data/enums';
import { AppState } from '~app/reducers';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AlertMessageType } from '~app/shared/enums';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-invite-member-dialog',
  templateUrl: './invite-member-dialog.component.html',
  styleUrls: ['./invite-member-dialog.component.scss']
})
export class InviteMemberDialogComponent implements OnInit {
  public agentOptions: Observable<DropdownOption<AgentSummary>[]> = this.store.pipe(
    select(getUserBsps),
    switchMap(bsps => this.agentDictionaryService.getDropdownOptions({ bspId: bsps[0]?.id }))
  );
  public individualAccessOptions = [
    {
      label: this.translateIndividualAccess(AgentGroupIndividualAccess.Permitted),
      value: AgentGroupIndividualAccess.Permitted
    },
    {
      label: this.translateIndividualAccess(AgentGroupIndividualAccess.Forbidden),
      value: AgentGroupIndividualAccess.Forbidden
    }
  ];
  public formGroup: FormGroup;
  public message: string;
  public AlertMessageType = AlertMessageType;

  public get inviteButton() {
    return this.config.data.buttons.find(button => button.type === FooterButton.Invite);
  }

  constructor(
    private config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private agentDictionaryService: AgentDictionaryService,
    private store: Store<AppState>,
    private fb: FormBuilder,
    private messageExtractorService: MessageExtractorService,
    private agentGroupService: AgentGroupService
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      agents: [null, Validators.required],
      individualAccess: [AgentGroupIndividualAccess.Permitted]
    });

    this.formGroup.statusChanges
      .pipe(startWith(false))
      .subscribe(() => (this.inviteButton.isDisabled = !this.formGroup.valid));

    this.observeButtonClicked();
  }

  private observeButtonClicked(): void {
    this.reactiveSubject.asObservable
      .pipe(
        filter(action => action.clickedBtn === FooterButton.Invite),
        map(() => this.formGroup?.value),
        switchMap(formValues => this.invite(formValues))
      )
      .subscribe();
  }

  private invite(formValues: any): Observable<any> {
    this.message = null;
    this.inviteButton.isLoading = true;

    return this.agentGroupService
      .inviteAgentGroupMember(this.config.data.agentGroup.id, formValues.agents.code, formValues.individualAccess)
      .pipe(
        tap(() => {
          this.reactiveSubject.emit(FooterButton.Done);
          this.dialogService.close();
        }),
        catchError(error => {
          this.message = this.messageExtractorService.extract(error);

          return of(null);
        }),
        finalize(() => (this.inviteButton.isLoading = false))
      );
  }

  private translateIndividualAccess(value: AgentGroupIndividualAccess): string {
    return this.translationService.translate(`MASTER_DATA.agentGroupMembers.individualAccess.${value}`);
  }
}
