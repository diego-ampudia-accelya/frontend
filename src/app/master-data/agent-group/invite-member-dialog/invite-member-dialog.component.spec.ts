import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AgentGroupService } from '../services/agent-group.service';
import { InviteMemberDialogComponent } from './invite-member-dialog.component';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';
import { AgentDictionaryService, NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('InviteMemberDialogComponent', () => {
  let component: InviteMemberDialogComponent;
  let fixture: ComponentFixture<InviteMemberDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InviteMemberDialogComponent, TranslatePipeMock],
      providers: [
        FormBuilder,
        mockProvider(DialogConfig, { data: { buttons: [{ type: FooterButton.Invite }] } }),
        mockProvider(ReactiveSubject, { asObservable: of() }),
        mockProvider(L10nTranslationService),
        mockProvider(NotificationService),
        mockProvider(AgentDictionaryService, {
          getDropdownOptions: jasmine.createSpy().and.returnValue(of([]))
        }),
        provideMockStore({ selectors: [{ selector: getUserBsps, value: [{ id: 1 }] }] }),
        mockProvider(AgentGroupService),
        mockProvider(MessageExtractorService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteMemberDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
