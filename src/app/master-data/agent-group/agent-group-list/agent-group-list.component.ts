import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { mapValues } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';

import { AgentGroupFilter } from '../models';
import { AgentGroupFilterFormatter } from '../services/agent-group-formatter.service';
import { SEARCH_FORM_DEFAULT_VALUE, SHARED } from './agent-group-list.constants';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AgentGroupService } from '~app/master-data/agent-group/services/agent-group.service';
import { AgentGroupStatus } from '~app/master-data/enums/agent-group-status.enum';
import { MasterDataAgentGroup } from '~app/master-data/models/agent-group.model';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { ROUTES } from '~app/shared/constants/routes';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-agent-group-list',
  templateUrl: './agent-group-list.component.html',
  styleUrls: ['./agent-group-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    AgentGroupFilterFormatter,
    AgentGroupService,
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [AgentGroupService] }
  ]
})
export class AgentGroupListComponent implements OnInit, OnDestroy {
  public tableTitle: string;

  public columns: Array<GridColumn> = [];
  public primaryActions: Observable<{ action: GridTableActionType }[]>;
  public searchForm: FormGroup;

  public statusOptions: DropdownOption<AgentGroupStatus>[];

  public userBsps: DropdownOption<BspDto>[];
  public isBspFilterLocked: boolean;
  private defaultBsp: Bsp;
  private get selectedBsp(): Bsp {
    const bspControl = FormUtil.get<AgentGroupFilter>(this.searchForm, 'bspId');

    return bspControl.value;
  }
  private set selectedBsp(selectedBsp: Bsp) {
    const bspControl = FormUtil.get<AgentGroupFilter>(this.searchForm, 'bspId');
    bspControl.setValue(selectedBsp);
  }
  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<MasterDataAgentGroup>,
    public displayFormatter: AgentGroupFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private agentGroupService: AgentGroupService,
    private dialogService: DialogService,
    private fb: FormBuilder,
    private translationService: L10nTranslationService,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.tableTitle = 'MASTER_DATA.agentGroup.query.title';
  }

  public ngOnInit() {
    this.setColumns();
    this.buildSearchForm();

    this.getQuery$().subscribe(query => this.loadData(query));
  }

  private setColumns() {
    this.columns = SHARED.COLUMNS;
  }

  private getQuery$(): Observable<DataQuery> {
    return this.initializeLeanBspFilter$().pipe(
      map(() => {
        let query = this.queryStorage.get() || defaultQuery;

        if (this.selectedBsp) {
          query = { ...query, filterBy: { ...query.filterBy, bspId: this.selectedBsp } };
        }

        return query;
      })
    );
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.store.select(getUser).pipe(
      first(),
      map(user => {
        // Filtering bsp list depending on permissions
        const requiredPermissions = [Permissions.readAgentGroup];

        const filteredBsps = user.bsps.filter(bsp => {
          const bspPermissions = user.bspPermissions?.find(
            bspPermission => bspPermission.bspId === bsp.id
          )?.permissions;

          return bspPermissions && requiredPermissions.some(permission => bspPermissions.includes(permission));
        });

        return [filteredBsps, user] as [Bsp[], User];
      }),
      tap(([bspList, user]) => {
        // Selecting default Bsp
        this.defaultBsp = bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc) || bspList[0];
        this.selectedBsp = this.defaultBsp;
      }),
      map(([bspList]) => bspList.map(bsp => toValueLabelObjectBsp(bsp))),
      tap(bspList => (this.userBsps = bspList)),
      tap(bspList => (this.isBspFilterLocked = bspList.length === 1))
    );
  }

  public loadData(query: DataQuery) {
    const modifiedQuery = this.applyPredefinedFilters(query);
    this.dataSource.get(modifiedQuery);
    this.queryStorage.save(modifiedQuery);
  }

  public onGetActionList() {
    this.primaryActions = of([{ action: GridTableActionType.Members }]);
  }

  public onActionClick(event) {
    const actionType: GridTableActionType = event.action.actionType;

    if (actionType === GridTableActionType.Members) {
      const url = ROUTES.AGENT_GROUP_MEMBERS.url;
      const id = event.row.id;
      this.router.navigate([url, id]);
    }
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.initilizeFiltersDropdown();
    }
  }

  private applyPredefinedFilters(query: DataQuery): DataQuery {
    return { ...query, filterBy: { ...query.filterBy, bspId: this.selectedBsp || this.defaultBsp } };
  }

  private initilizeFiltersDropdown() {
    this.statusOptions = Object.values(AgentGroupStatus).map(value => ({
      value,
      label: this.translationService.translate(`MASTER_DATA.agentGroup.query.statusOptions.${value}`)
    }));
  }

  public onDownload(): void {
    const requestQuery = this.queryStorage.get();
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MASTER_DATA.agentGroup.query.downloadTitle'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: requestQuery
      },
      apiService: this.agentGroupService
    });
  }

  private buildSearchForm() {
    this.searchForm = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
