import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AgentGroupFilterFormatter } from '../services/agent-group-formatter.service';
import { AgentGroupListComponent } from './agent-group-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AgentGroupService } from '~app/master-data/agent-group/services/agent-group.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants/routes';
import { createAgentUser } from '~app/shared/mocks/agent-user';
import { Bsp } from '~app/shared/models/bsp.model';

const query: DataQuery = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
};

describe('AgentGroupListComponent', () => {
  let component: AgentGroupListComponent;
  let fixture: ComponentFixture<AgentGroupListComponent>;

  let queryableDataSourceSpy: SpyObject<QueryableDataSource<AgentGroupService>>;
  const initialState = {
    auth: {
      user: createAgentUser()
    },
    core: {
      menu: {
        tabs: { dashboard: { ...ROUTES.AGENT_GROUPS, id: 'agent-groups' } },
        activeTabId: 'agent-groups'
      },
      viewListsInfo: {}
    }
  };

  const activatedRouteStub = {
    snapshot: {
      params: {}
    }
  };

  beforeEach(waitForAsync(() => {
    queryableDataSourceSpy = createSpyObject(QueryableDataSource, { appliedQuery$: of(query) });
    queryableDataSourceSpy.get.and.returnValue(of(query));

    TestBed.configureTestingModule({
      declarations: [AgentGroupListComponent],
      imports: [HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        L10nTranslationService,
        provideMockStore({ initialState }),
        mockProvider(AgentGroupService),
        mockProvider(AgentGroupFilterFormatter),
        mockProvider(DefaultQueryStorage),
        mockProvider(PermissionsService),
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ]
    })
      .overrideComponent(AgentGroupListComponent, {
        set: {
          providers: [{ provide: QueryableDataSource, useValue: queryableDataSourceSpy }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentGroupListComponent);
    component = fixture.componentInstance;
    TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should apply bsp default filter to query', () => {
    const mockBsp: Bsp = { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '01/01/2000' };
    component['defaultBsp'] = mockBsp;
    expect(component['applyPredefinedFilters'](query)).toEqual({
      ...query,
      filterBy: { ...query.filterBy, bspId: mockBsp }
    });
  });
});
