import { AgentGroupFilter } from '../models';
import { GridColumn } from '~app/shared/models';

const translationPrefix = 'MASTER_DATA.agentGroup.query.columns';

export const SHARED = {
  get COLUMNS(): GridColumn[] {
    return [
      {
        name: `${translationPrefix}.bsp`,
        prop: 'user.bsp',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 110,
        pipe: { transform: bsp => ` ${bsp.isoCountryCode}` }
      },
      {
        name: `${translationPrefix}.userID`,
        prop: 'user.userTypeDetails.iataCode',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 115
      },
      {
        name: `${translationPrefix}.creator`,
        prop: 'creator',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 100
      },
      {
        name: `${translationPrefix}.organization`,
        prop: 'user.contactInfo.organization',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 200
      },
      {
        name: `${translationPrefix}.name`,
        prop: 'user.userTypeDetails.name',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 200
      },
      {
        name: `${translationPrefix}.email`,
        prop: 'user.contactInfo.email',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 200
      },
      {
        name: `${translationPrefix}.numberMembers`,
        prop: 'members',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.status`,
        prop: 'user.status',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 116
      }
    ];
  }
};

export const SEARCH_FORM_DEFAULT_VALUE: AgentGroupFilter = {
  bspId: null,
  iataCode: null,
  creator: null,
  organization: null,
  name: null,
  email: null,
  status: null,
  address: null,
  locality: null,
  postCode: null,
  telephone: null,
  members: null
};
