import { Component, Input } from '@angular/core';

import { MasterDataAgentGroupMember } from '~app/master-data/models/agent-group.model';

@Component({
  selector: 'bspl-agent-group-member-info',
  templateUrl: './agent-group-member-info.component.html',
  styleUrls: ['./agent-group-member-info.component.scss']
})
export class AgentGroupMemberInfoComponent {
  @Input() member: MasterDataAgentGroupMember;
}
