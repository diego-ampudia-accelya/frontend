import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import * as fromSettings from '~app/master-data/configuration';
import { MenuBuilder, SettingConfigurationActions } from '~app/master-data/configuration';
import { ChangesDialogService as SettingsChangesDialogService } from '~app/master-data/configuration/changes-dialog/changes-dialog.service';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { AgentGroupUser, UserType } from '~app/shared/models/user.model';
import { AgentGroupProfileComponent } from './agent-group-profile.component';

describe('AgentGroupProfileComponent', () => {
  let fixture: ComponentFixture<AgentGroupProfileComponent>;
  let component: AgentGroupProfileComponent;
  let permissionsServiceSpy: SpyObject<PermissionsService>;
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const stUnsavedChangesDialogServiceMock = createSpyObject(SettingsChangesDialogService);

  stUnsavedChangesDialogServiceMock.confirmApplyChanges.and.returnValue(of(FooterButton.Cancel));
  let mockStore: MockStore<AppState>;

  const menuBuilderMock = jasmine.createSpyObj<MenuBuilder>('MenuBuilder', {
    buildMenuItemsFrom: [
      {
        route: '/master-data/agent/12578/configuration/email-alerts-settings',
        title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
        isAccessible: true
      }
    ]
  });

  const activatedRouteStub = {
    snapshot: {
      data: {
        tab: ROUTES.AGENT_GROUP
      }
    }
  };

  const user: AgentGroupUser = {
    email: 'agent-group-user-nfe@accelya.com',
    firstName: 'Thomas',
    lastName: 'Anderson',
    userType: UserType.AGENT_GROUP,
    id: 10005,

    active: true,
    permissions: ['uEmailAlrt']
  };

  const initialState = {
    auth: {
      user
    },
    core: {
      menu: {
        tabs: {}
      }
    }
  };

  let routerMock;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/configuration/email-alerts-settings', './configuration/email-alerts-settings')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './configuration/email-alerts-settings',
      createUrlTree: () => {},
      isActive: (res = false) => res
    };

    permissionsServiceSpy = createSpyObject(PermissionsService, { permissions$: of([]) });
    permissionsServiceSpy.hasPermission.and.returnValue(false);

    TestBed.configureTestingModule({
      declarations: [AgentGroupProfileComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        provideMockStore({
          initialState,
          selectors: [{ selector: fromSettings.canApplyChanges, value: false }]
        }),
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy },
        { provide: SettingsChangesDialogService, useValue: stUnsavedChangesDialogServiceMock },
        { provide: MenuBuilder, useValue: menuBuilderMock },
        {
          provide: Router,
          useValue: routerMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentGroupProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch EAlert Open Apply Changes action when applyChanges method is called', () => {
    spyOn(routerMock, 'isActive').and.returnValue(true);
    component.currentSavableTab = {
      url: 'EAlertUrl',
      applyChangesAction: SettingConfigurationActions.openApplyChanges()
    };

    component.applyChanges();

    expect(mockStore.dispatch).toHaveBeenCalledWith(SettingConfigurationActions.openApplyChanges());
  });

  it('button ApplyChanges must not be visible if PermissionsService return false', () => {
    spyOn(routerMock, 'isActive').and.returnValue(true);
    component.currentSavableTab = {
      url: 'EAlertUrl',
      applyChangesPermission: 'AnyPermissions'
    };

    expect(component.isApplyChangesVisible).toBe(false);
  });
});
