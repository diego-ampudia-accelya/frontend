import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AgentGroupFilter } from '../models';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AgentGroupFilterFormatter extends DefaultDisplayFilterFormatter {
  public ignoredFilters: Array<keyof AgentGroupFilter> = [];

  constructor(private translation: L10nTranslationService) {
    super();
  }

  public format(filter: AgentGroupFilter): AppliedFilter[] {
    const formattedFilters: AppliedFilter[] = [];
    const { iataCode, bspId, ...rest } = filter;

    // Defaults formats using key - value from filter fields
    formattedFilters.push(...super.format(rest));

    //Custom filter formats
    const filterMappers: Partial<FilterMappers<AgentGroupFilter>> = {
      iataCode: code => `${this.translate('columns.userID')} - ${code}`,
      bspId: bsp => `${this.translate('isoc')} - ${bsp.name} (${bsp.isoCountryCode})`
    };

    const customFormats: AppliedFilter[] = Object.entries({ iataCode, bspId } || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
    formattedFilters.push(...customFormats);

    return formattedFilters;
  }

  private translate(key: string): string {
    return this.translation.translate('MASTER_DATA.agentGroup.query.' + key);
  }
}
