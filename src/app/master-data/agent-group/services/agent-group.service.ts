import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  AgentGroupCreateForm,
  MainUserAgentGroup,
  MasterDataAgentGroup,
  MasterDataAgentGroupMember
} from '../../models/agent-group.model';
import { AgentGroupFilter, AgentGroupMemberFilter } from '../models';
import { AgentGroupIndividualAccess, AgentGroupMembership } from '~app/master-data/enums';
import { ViewAs } from '~app/master-data/ticketing-authority';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { RequestMethod } from '~app/shared/enums';
import { AgentSummary } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService, appConfiguration } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class AgentGroupService extends ApiService<MasterDataAgentGroup> {
  constructor(http: HttpClient, private activatedRoute: ActivatedRoute) {
    super(http, 'group');
    this.createUrl();
  }

  public find(
    query: DataQuery<AgentGroupFilter & AgentGroupMemberFilter>
  ): Observable<PagedData<MasterDataAgentGroup>> {
    const requestQuery = this.formatQuery(query);
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http.get<PagedData<MasterDataAgentGroup>>(url);
  }

  public download(requestQueryBody: any, downloadFormat: string) {
    const requestQuery = this.formatQuery(requestQueryBody);
    const query: string = requestQuery.getQueryString();

    return super.download({}, `/download?exportAs=${downloadFormat}&${query}`, RequestMethod.Get);
  }

  public getOneGroup(id: number): Observable<MasterDataAgentGroup> {
    const url = `${appConfiguration.baseApiPath}/group-management/agent-groups`;

    return this.http.get<MasterDataAgentGroup>(`${url}/${id}`);
  }

  public createGroup(group: AgentGroupCreateForm): Observable<MasterDataAgentGroup> {
    const url = `${appConfiguration.baseApiPath}/group-management/agent-groups`;

    return this.http.post<MasterDataAgentGroup>(url, group);
  }

  public getGroupMembers(id: number): Observable<PagedData<MasterDataAgentGroupMember>> {
    return this.http.get<PagedData<MasterDataAgentGroupMember>>(`${this.baseUrl}/${id}/members`);
  }

  public inviteAgentGroupMember(id: number, agentIataCode: string, individualAccess: AgentGroupIndividualAccess) {
    const url = `${appConfiguration.baseApiPath}/group-management/agent-groups/${id}/members`;
    const body = {
      user: {
        userTypeDetails: {
          iataCode: agentIataCode
        }
      },
      individualAccess
    };

    return this.http.post(url, body);
  }

  public modifyMemberIndividualAccess(groupId: number, memberId: string, individualAccess: AgentGroupIndividualAccess) {
    const url = `${appConfiguration.baseApiPath}/group-management/agent-groups/${groupId}/members/${memberId}`;
    const body = {
      individualAccess
    };

    return this.http.put(url, body);
  }

  public deleteMember(groupId: number, memberId: string) {
    const url = `${appConfiguration.baseApiPath}/group-management/agent-groups/${groupId}/members/${memberId}`;

    return this.http.delete(url);
  }

  public acceptInvitation(groupId: number, memberId: string): Observable<MasterDataAgentGroupMember> {
    const url = `${appConfiguration.baseApiPath}/group-management/agent-groups/${groupId}/members/${memberId}`;

    return this.http.put<any>(url, { membership: AgentGroupMembership.Member });
  }

  public getAgentUserAgentGroup(): Observable<MasterDataAgentGroup> {
    const query = {
      ...defaultQuery,
      filterBy: {
        membership: [AgentGroupMembership.Creator, AgentGroupMembership.Member]
      }
    };

    const requestQuery = this.formatQuery(query);
    const url = `${appConfiguration.baseApiPath}/group-management/agent-groups` + requestQuery.getQueryString();

    return this.http.get<PagedData<MasterDataAgentGroup>>(url).pipe(map(r => r.records?.[0]));
  }

  public getAgentGroupMembers(): Observable<MainUserAgentGroup[]> {
    const url = `${appConfiguration.baseApiPath}/group-management/members/dictionary`;

    return this.http.get<MainUserAgentGroup[]>(url);
  }

  private createUrl() {
    const agentGroupParamId = this.activatedRoute.snapshot.params.id;
    const agentGroup: MasterDataAgentGroup = this.activatedRoute.snapshot.data.agentGroup;
    const viewAs: ViewAs = this.activatedRoute.snapshot.data.viewAs;
    const isViewingMembers = agentGroupParamId || viewAs === ViewAs.AgentGroup || viewAs === ViewAs.Agent;
    const baseUrl = `${appConfiguration.baseApiPath}/group-management/agent-groups`;

    if (isViewingMembers) {
      this.baseUrl = `${baseUrl}/${agentGroup.id}/members`;
    } else {
      this.baseUrl = baseUrl;
    }
  }

  private formatQuery(query: DataQuery<AgentGroupFilter & AgentGroupMemberFilter>): RequestQuery {
    const { agents, membership, locality, bspId, ...rest } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        iataCode: agents?.map((agent: AgentSummary) => agent.code),
        membership: membership?.join(),
        city: locality,
        bspId: bspId && bspId.id,
        ...rest
      }
    });
  }
}
