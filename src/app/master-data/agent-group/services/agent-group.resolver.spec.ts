import { TestBed } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

import { AgentGroupResolver } from './agent-group.resolver';
import { AgentGroupService } from './agent-group.service';

describe('AgentGroupResolver', () => {
  let resolver: AgentGroupResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgentGroupResolver, provideMockStore({ initialState: {} }), mockProvider(AgentGroupService)]
    });

    resolver = TestBed.inject<AgentGroupResolver>(AgentGroupResolver);
  });

  it('should create', () => {
    expect(resolver).toBeTruthy();
  });
});
