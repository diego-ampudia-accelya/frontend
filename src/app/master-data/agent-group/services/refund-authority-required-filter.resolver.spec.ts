import { fakeAsync, TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Data } from '@angular/router';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { RefundAuthorityRequiredFilterResolver } from './refund-authority-required-filter.resolver';
import { RefundAuthorityFilter, ViewAs } from '~app/master-data/refund-authority';
import { RefundAuthorityListActions } from '~app/master-data/refund-authority/store/actions';
import { Bsp } from '~app/shared/models/bsp.model';
import { AgentGroupUser, UserType } from '~app/shared/models/user.model';

describe('RefundAuthorityRequiredFilterResolver for agent groups', () => {
  let resolver: RefundAuthorityRequiredFilterResolver;
  let mockStore: MockStore;

  const bspMock: Bsp = {
    id: 1,
    name: 'SPAIN',
    isoCountryCode: 'ES',
    effectiveFrom: '2000-01-01'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({}), RefundAuthorityRequiredFilterResolver]
    });
  });

  beforeEach(fakeAsync(() => {
    mockStore = TestBed.inject(MockStore);
    resolver = TestBed.inject(RefundAuthorityRequiredFilterResolver);
  }));

  it('should be created', () => {
    expect(resolver).toBeDefined();
  });

  it('should dispatch the correct action with proper filter and resolve it', () => {
    const route = {
      parent: {
        data: {
          agent: {
            userType: UserType.AGENT_GROUP,
            email: 'william.agentgroup@accelya.com',
            firstName: 'William',
            lastName: 'Herschel',
            bsps: [bspMock],
            permission: 'rAGroup'
          } as AgentGroupUser
        } as Data
      }
    } as ActivatedRouteSnapshot;

    spyOn(mockStore, 'dispatch');

    const expectedFilter: RefundAuthorityFilter = {
      viewAs: ViewAs.AgentGroup,
      bsp: bspMock,
      permission: 'rRnPerm'
    };

    const requiredFilter: RefundAuthorityFilter = resolver.resolve(route);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      RefundAuthorityListActions.changeRequiredFilter({ requiredFilter: expectedFilter })
    );
    expect(requiredFilter).toEqual(expectedFilter);
  });

  it('should dispatch the correct action with proper filter without bsps and resolve it', () => {
    const route = {
      parent: {
        data: {
          agent: {
            userType: UserType.AGENT_GROUP,
            email: 'william.agentgroup@accelya.com',
            firstName: 'William',
            lastName: 'Herschel'
          } as AgentGroupUser
        } as Data
      }
    } as ActivatedRouteSnapshot;

    spyOn(mockStore, 'dispatch');

    const expectedFilter: RefundAuthorityFilter = {
      viewAs: ViewAs.AgentGroup,
      bsp: undefined,
      permission: 'rRnPerm'
    };

    const requiredFilter: RefundAuthorityFilter = resolver.resolve(route);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      RefundAuthorityListActions.changeRequiredFilter({ requiredFilter: expectedFilter })
    );
    expect(requiredFilter).toEqual(expectedFilter);
  });

  it('should throw an access denied error when there is no agent in route', () => {
    const route = {
      parent: {
        data: {}
      }
    } as ActivatedRouteSnapshot;

    expect(() => resolver.resolve(route)).toThrow(new Error('Unsupported user type for Refund issuing authority.'));
  });
});
