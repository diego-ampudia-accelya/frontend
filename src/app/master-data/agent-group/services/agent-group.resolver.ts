import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { AgentGroupService } from './agent-group.service';
import { getUserId } from '~app/auth/selectors/auth.selectors';
import { MasterDataAgentGroup } from '~app/master-data/models/agent-group.model';
import { ViewAs } from '~app/master-data/ticketing-authority';
import { AppState } from '~app/reducers';

@Injectable({
  providedIn: 'root'
})
export class AgentGroupResolver implements Resolve<MasterDataAgentGroup> {
  private loggedUserId$: Observable<number> = this.store.pipe(select(getUserId), first());

  constructor(private store: Store<AppState>, private agentGroupService: AgentGroupService) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<MasterDataAgentGroup> {
    const viewAs: ViewAs = route.data.viewAs;
    let agentGroupId$: Observable<number>;

    if (viewAs === ViewAs.Agent) {
      return this.agentGroupService.getAgentUserAgentGroup();
    }

    if (viewAs === ViewAs.AgentGroup) {
      agentGroupId$ = this.loggedUserId$;
    } else {
      agentGroupId$ = of(route.params.id);
    }

    return agentGroupId$.pipe(switchMap(id => this.agentGroupService.getOneGroup(id)));
  }
}
