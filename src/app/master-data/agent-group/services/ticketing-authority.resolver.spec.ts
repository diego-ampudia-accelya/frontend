import { fakeAsync, TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Data } from '@angular/router';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import * as fromTicketingAuthority from '../../ticketing-authority/store/reducers';
import { TicketingAuthorityResolver } from './ticketing-authority.resolver';
import { TicketingAuthorityFilter } from '~app/master-data/ticketing-authority';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import { AppState } from '~app/reducers';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AirlineSummary } from '~app/shared/models';
import { AgentGroupUser, UserType } from '~app/shared/models/user.model';

describe('TicketingAuthorityResolver for agent groups', () => {
  let resolver: TicketingAuthorityResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({
          selectors: [
            { selector: fromTicketingAuthority.getRequiredFilter, value: {} },
            { selector: fromTicketingAuthority.getListQuery, value: defaultQuery }
          ]
        }),
        TicketingAuthorityResolver
      ]
    });
  });

  beforeEach(fakeAsync(() => {
    mockStore = TestBed.inject<any>(Store);
    resolver = TestBed.inject(TicketingAuthorityResolver);
  }));

  it('should be created', () => {
    expect(resolver).toBeDefined();
  });

  it('should dispatch the correct action with empty filter and resolve it', () => {
    const route = {
      parent: {
        data: {
          agent: {
            userType: UserType.AGENT_GROUP,
            email: 'william.agentgroup@accelya.com',
            firstName: 'William',
            lastName: 'Herschel'
          } as AgentGroupUser
        } as Data
      }
    } as ActivatedRouteSnapshot;

    const airline1 = { id: 1, name: 'Airline 1' } as AirlineSummary;

    const requiredFilter: TicketingAuthorityFilter = {
      airlines: [airline1]
    };

    mockStore.overrideSelector(fromTicketingAuthority.getRequiredFilter, requiredFilter);

    spyOn(mockStore, 'dispatch');

    const filter: TicketingAuthorityFilter = resolver.resolve(route);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter: { airlines: [airline1] } })
    );
    expect(filter).toEqual({ airlines: [airline1] });
  });

  it('should throw an access denied error when there is no agent in route', () => {
    const route = {
      parent: {
        data: {}
      }
    } as ActivatedRouteSnapshot;

    expect(() => resolver.resolve(route)).toThrow(new Error('Unsupported user type for Ticketing authority.'));
  });
});
