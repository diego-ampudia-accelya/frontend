import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AgentGroupInvitation } from '~app/master-data/models/agent-group.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({ providedIn: 'root' })
export class AgentGroupInvitationsService {
  constructor(private http: HttpClient, private appConfigurationService: AppConfigurationService) {}

  public getInvitations(): Observable<AgentGroupInvitation[]> {
    const url = `${this.appConfigurationService.baseApiPath}/group-management/invitations`;

    return this.http.get<AgentGroupInvitation[]>(url);
  }
}
