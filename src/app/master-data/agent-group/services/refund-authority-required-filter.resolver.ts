import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Store } from '@ngrx/store';

import { RefundAuthorityFilter, ViewAs } from '~app/master-data/refund-authority/models';
import { RefundAuthorityListActions } from '~app/master-data/refund-authority/store/actions';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { AgentGroupUser } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class RefundAuthorityRequiredFilterResolver implements Resolve<RefundAuthorityFilter> {
  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot): RefundAuthorityFilter {
    const agent: AgentGroupUser = route.parent.data.agent;

    if (!agent) {
      throw new Error('Unsupported user type for Refund issuing authority.');
    }

    const requiredFilter: RefundAuthorityFilter = {
      viewAs: ViewAs.AgentGroup,
      bsp: agent.bsps && agent.bsps[0],
      permission: Permissions.readRNIssueAgent
    };

    this.store.dispatch(RefundAuthorityListActions.changeRequiredFilter({ requiredFilter }));

    return requiredFilter;
  }
}
