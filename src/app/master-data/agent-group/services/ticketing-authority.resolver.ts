import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { first } from 'rxjs/operators';

import { TicketingAuthorityFilter } from '~app/master-data/ticketing-authority';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import * as fromTicketingAuthority from '~app/master-data/ticketing-authority/store/reducers';
import { AppState } from '~app/reducers';
import { AirlineSummary } from '~app/shared/models';
import { AgentGroupUser } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class TicketingAuthorityResolver implements Resolve<TicketingAuthorityFilter> {
  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot): TicketingAuthorityFilter {
    const agent: AgentGroupUser = route.parent.data.agent;
    let airlines: AirlineSummary[] = [];

    if (!agent) {
      throw new Error('Unsupported user type for Ticketing authority.');
    }

    this.store.pipe(first(), select(fromTicketingAuthority.getRequiredFilter)).subscribe(filter => {
      airlines = filter?.airlines;
    });

    const requiredFilter = {
      airlines
    };

    this.store.dispatch(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));

    return requiredFilter;
  }
}
