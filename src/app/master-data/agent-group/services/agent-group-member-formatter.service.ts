import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AgentGroupMemberFilter } from '../models';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';

@Injectable()
export class AgentGroupMemberFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translation: L10nTranslationService) {
    super();
  }

  public format(filter: AgentGroupMemberFilter): AppliedFilter[] {
    const { agents, membership, individualAccess, ...rest } = filter;
    const formattedFilters: AppliedFilter[] = [];

    if (agents) {
      formattedFilters.push({
        label: `${this.translate('agentCode')} - ${agents.map(a => a.code).join(', ')}`,
        keys: ['agents']
      });
    }

    if (membership) {
      const translatedMemberships = membership.map(m =>
        this.translation.translate(`MASTER_DATA.agentGroupMembers.membership.${m}`)
      );
      formattedFilters.push({
        label: `${this.translate('membership')} - ${translatedMemberships.join(', ')}`,
        keys: ['membership']
      });
    }

    if (individualAccess) {
      const individualAccessTranslation = this.translation.translate(
        `MASTER_DATA.agentGroupMembers.individualAccess.${individualAccess}`
      );
      formattedFilters.push({
        label: `${this.translate('individualAccess')} - ${individualAccessTranslation}`,
        keys: ['individualAccess']
      });
    }

    formattedFilters.push(...super.format(rest));

    return formattedFilters;
  }

  private translate(key: string): string {
    return this.translation.translate('MASTER_DATA.agentGroupMembers.query.columns.' + key);
  }
}
