import { fakeAsync, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';
import { noop } from 'rxjs';

import { MyAgentGroupResolver } from './my-agent-group.resolver';
import { State } from '~app/auth/selectors/auth.selectors';
import { AgentGroupUser, AgentUser, UserType } from '~app/shared/models/user.model';

describe('MyAgentGroupResolver', () => {
  let mockStore: MockStore<State>;
  let resolver: MyAgentGroupResolver;

  const initialState: State = {
    router: null,
    auth: {
      user: null
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), MyAgentGroupResolver]
    });
  });

  beforeEach(fakeAsync(() => {
    mockStore = TestBed.inject(MockStore);
    resolver = TestBed.inject(MyAgentGroupResolver);
  }));

  it('should be created', () => {
    expect(resolver).toBeDefined();
  });

  it('should resolve agent group when current user is an agent group user', fakeAsync(() => {
    let resultUser: AgentGroupUser;

    const agentGroupUser = {
      userType: UserType.AGENT_GROUP
    } as AgentGroupUser;

    mockStore.setState({
      ...initialState,
      auth: { user: agentGroupUser }
    });

    resolver.resolve().subscribe(user => (resultUser = user));

    expect(resultUser).toEqual(agentGroupUser);
  }));

  it('should not resolve agent group when user information is not available', fakeAsync(() => {
    expect(resolver.resolve()).toBeObservable(cold('-'));
  }));

  it('should throw error when user is not of type agent group', fakeAsync(() => {
    let error: Error;

    const agentUser = {
      userType: UserType.AGENT,
      agent: null
    } as AgentUser;

    mockStore.setState({
      ...initialState,
      auth: { user: agentUser }
    });

    resolver.resolve().subscribe(noop, err => (error = err));

    expect(error).toEqual(new Error('Unsupported user type for Refund issuing authority.'));
  }));
});
