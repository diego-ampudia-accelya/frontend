import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';

import { AgentGroupService } from './agent-group.service';
import { MasterDataAgentGroup } from '~app/master-data/models/agent-group.model';
import { AppConfigurationService } from '~app/shared/services';

describe('AgentGroupService', () => {
  let service: AgentGroupService;

  const activatedRouteStub = {
    snapshot: {
      data: { agentGroup: {} as MasterDataAgentGroup },
      params: {}
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AgentGroupService,
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, { baseApiPath: 'api' }),
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ]
    });
    service = TestBed.inject(AgentGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
