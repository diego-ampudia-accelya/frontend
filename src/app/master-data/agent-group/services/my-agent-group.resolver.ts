import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { AgentGroupUser, UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class MyAgentGroupResolver implements Resolve<AgentGroupUser> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<AgentGroupUser> {
    return this.store.pipe(
      select(getUser),
      filter(user => !!user),
      first(),
      map((user: AgentGroupUser) => {
        if (user.userType !== UserType.AGENT_GROUP) {
          throw new Error('Unsupported user type for Refund issuing authority.');
        }

        return user;
      })
    );
  }
}
