import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { defer, Observable } from 'rxjs';
import { filter, finalize, switchMap, tap } from 'rxjs/operators';

import { AgentGroupService } from '../services/agent-group.service';
import { Logout } from '~app/auth/actions/auth.actions';
import { AuthState } from '~app/auth/reducers/auth.reducer';
import { AgentGroupIndividualAccess } from '~app/master-data/enums';
import { AgentGroupInvitation } from '~app/master-data/models/agent-group.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService, RouterUtilsService } from '~app/shared/services';

@Component({
  selector: 'bspl-accept-invitation-dialog',
  templateUrl: './accept-invitation-dialog.component.html',
  styleUrls: ['./accept-invitation-dialog.component.scss']
})
export class AcceptInvitationDialogComponent implements OnInit {
  public individualAccess = AgentGroupIndividualAccess;

  public get invitation(): AgentGroupInvitation {
    return this.config.data.invitation;
  }

  private get acceptButton(): ModalAction {
    return this.config.data.buttons.find(({ type }) => this.isAcceptButton(type));
  }

  // Passing the activated route from the outer component because it seems that a dynamic component does
  // not resolves it properly. It seems that if it is injected we are receiving the root ActivatedRoute.
  private get route(): ActivatedRoute {
    return this.config.data.route;
  }

  private acceptAndLogout$ = this.reactiveSubject.asObservable.pipe(
    filter(({ clickedBtn }) => clickedBtn === FooterButton.AcceptAndLogout),
    switchMap(() => this.accept()),
    tap(() => this.store.dispatch(new Logout()))
  );

  private accept$ = this.reactiveSubject.asObservable.pipe(
    filter(({ clickedBtn }) => clickedBtn === FooterButton.Accept),
    switchMap(() => this.accept()),
    tap(() => {
      this.routerUtilsService.hardReloadCurrentRoute(this.route);
    })
  );

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private agentGroupService: AgentGroupService,
    private store: Store<AuthState>,
    private routerUtilsService: RouterUtilsService
  ) {}

  ngOnInit(): void {
    this.accept$.subscribe();
    this.acceptAndLogout$.subscribe();
  }

  private accept(): Observable<any> {
    return defer(() => {
      this.acceptButton.isLoading = true;

      return this.agentGroupService.acceptInvitation(this.invitation.group.id, this.invitation.id).pipe(
        tap(() => {
          this.reactiveSubject.emit(FooterButton.Accept);
          this.notificationService.showSuccess('MASTER_DATA.agentGroupInvitations.accept.success');
        }),
        // Close the modal regardless of the operation failure/success
        finalize(() => {
          this.dialogService.close();
          this.acceptButton.isLoading = false;
        })
      );
    });
  }

  private isAcceptButton(type: FooterButton): boolean {
    return [FooterButton.Accept, FooterButton.AcceptAndLogout].includes(type);
  }
}
