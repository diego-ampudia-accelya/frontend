import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AgentGroupService } from '../services/agent-group.service';
import { AcceptInvitationDialogComponent } from './accept-invitation-dialog.component';
import { DialogConfig, DialogService } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService, RouterUtilsService } from '~app/shared/services';

describe('AcceptInvitationDialogComponent', () => {
  let component: AcceptInvitationDialogComponent;
  let fixture: ComponentFixture<AcceptInvitationDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        mockProvider(DialogConfig, {
          data: { invitation: {} }
        }),
        provideMockStore(),
        mockProvider(DialogService),
        mockProvider(AgentGroupService),
        mockProvider(L10nTranslationService),
        mockProvider(NotificationService),
        mockProvider(ReactiveSubject, { asObservable: of() }),
        mockProvider(RouterUtilsService)
      ],
      declarations: [AcceptInvitationDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptInvitationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
