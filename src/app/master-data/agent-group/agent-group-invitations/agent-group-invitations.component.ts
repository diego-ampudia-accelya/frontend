import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { defer, Observable } from 'rxjs';
import { filter, finalize, shareReplay } from 'rxjs/operators';

import { AcceptInvitationDialogComponent } from '../accept-invitation-dialog/accept-invitation-dialog.component';
import { RejectInvitationDialogComponent } from '../reject-invitation-dialog/reject-invitation-dialog.component';
import { AgentGroupInvitationsService } from '../services/agent-group-invitations.service';
import { AgentGroupIndividualAccess } from '~app/master-data/enums';
import { AgentGroupInvitation } from '~app/master-data/models/agent-group.model';
import { ButtonDesign, DialogService, FooterButton, SimpleTableColumn } from '~app/shared/components';
import { Permissions } from '~app/shared/constants/permissions';

@Component({
  selector: 'bspl-agent-group-invitations',
  templateUrl: './agent-group-invitations.component.html',
  styleUrls: ['./agent-group-invitations.component.scss']
})
export class AgentGroupInvitationsComponent implements OnInit {
  @Output() createGroup = new EventEmitter();

  @ViewChild('individualAccess', { static: true }) individualAccessTemplate: TemplateRef<any>;
  @ViewChild('actions', { static: true }) actionsTemplate: TemplateRef<any>;

  public columns: SimpleTableColumn[];
  public isLoading: boolean;
  public data$ = this.getInvitations();
  public Permissions = Permissions;
  public ButtonDesign = ButtonDesign;

  constructor(
    private agentGroupInvitationsService: AgentGroupInvitationsService,
    private dialogService: DialogService,
    private route: ActivatedRoute
  ) {}

  public ngOnInit(): void {
    this.columns = this.getColumns();
  }

  public reject(invitation: AgentGroupInvitation): void {
    this.dialogService
      .open(RejectInvitationDialogComponent, {
        data: {
          title: 'MASTER_DATA.agentGroupInvitations.reject.title',
          footerButtonsType: FooterButton.Reject,
          invitation
        }
      })
      .pipe(filter(action => action === FooterButton.Reject))
      .subscribe(() => (this.data$ = this.getInvitations()));
  }

  public accept(invitation: AgentGroupInvitation): void {
    const hasIndividualAccess = invitation.individualAccess === AgentGroupIndividualAccess.Permitted;
    this.dialogService
      .open(AcceptInvitationDialogComponent, {
        data: {
          title: 'MASTER_DATA.agentGroupInvitations.accept.title',
          footerButtonsType: hasIndividualAccess ? FooterButton.Accept : FooterButton.AcceptAndLogout,
          invitation,
          route: this.route
        }
      })
      .subscribe();
  }

  public createAgentGroup(): void {
    this.createGroup.emit();
  }

  private getColumns(): SimpleTableColumn[] {
    return [
      {
        header: 'MASTER_DATA.agentGroupInvitations.agentGroupUserId',
        field: 'group.user.userTypeDetails.iataCode'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.individualAccess',
        field: 'individualAccess',
        cellTemplate: this.individualAccessTemplate
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.name',
        field: 'group.user.userTypeDetails.name'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.creator',
        field: 'group.creator'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.organization',
        field: 'group.user.contactInfo.organization'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.email',
        field: 'group.user.contactInfo.email'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.phone',
        field: 'group.user.contactInfo.telephone'
      },
      {
        field: '',
        cellTemplate: this.actionsTemplate
      }
    ];
  }

  private getInvitations(): Observable<AgentGroupInvitation[]> {
    return defer(() => {
      this.isLoading = true;

      return this.agentGroupInvitationsService.getInvitations();
    }).pipe(
      finalize(() => (this.isLoading = false)),
      shareReplay(1)
    );
  }
}
