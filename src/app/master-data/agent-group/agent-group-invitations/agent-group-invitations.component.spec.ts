import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { MockPipe } from 'ng-mocks';
import { of } from 'rxjs';

import { AgentGroupInvitationsService } from '../services/agent-group-invitations.service';
import { AgentGroupInvitationsComponent } from './agent-group-invitations.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { TranslatePipeMock } from '~app/test';

describe('AgentGroupInvitationsComponent', () => {
  let component: AgentGroupInvitationsComponent;
  let fixture: ComponentFixture<AgentGroupInvitationsComponent>;
  const agentGroupInvitationsServiceSpy: SpyObject<AgentGroupInvitationsService> =
    createSpyObject(AgentGroupInvitationsService);
  agentGroupInvitationsServiceSpy.getInvitations.and.returnValue(of([]));

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AgentGroupInvitationsComponent, TranslatePipeMock, MockPipe(HasPermissionPipe)],
      providers: [
        mockProvider(AgentGroupInvitationsService, agentGroupInvitationsServiceSpy),
        mockProvider(ActivatedRoute)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentGroupInvitationsComponent);
    component = fixture.componentInstance;
    component.isLoading = false;
    // spyOn<any>(component, 'getInvitations').and.callFake()
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set columns on ngOnInit', () => {
    const expectedColumns: any[] = [
      {
        header: 'MASTER_DATA.agentGroupInvitations.agentGroupUserId',
        field: 'group.user.userTypeDetails.iataCode'
      },
      jasmine.objectContaining({
        header: 'MASTER_DATA.agentGroupInvitations.individualAccess',
        field: 'individualAccess'
      }),
      {
        header: 'MASTER_DATA.agentGroupInvitations.name',
        field: 'group.user.userTypeDetails.name'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.creator',
        field: 'group.creator'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.organization',
        field: 'group.user.contactInfo.organization'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.email',
        field: 'group.user.contactInfo.email'
      },
      {
        header: 'MASTER_DATA.agentGroupInvitations.phone',
        field: 'group.user.contactInfo.telephone'
      },
      jasmine.objectContaining({ field: '' })
    ];

    component.columns = [];
    component.ngOnInit();

    expect(component.columns).toEqual(expectedColumns);
  });
});
