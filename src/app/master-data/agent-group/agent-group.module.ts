import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';

import { AdmAcmAuthorityModule } from '../adm-acm-authority/adm-acm-authority.module';
import { ConfigurationModule } from '../configuration';
import { ConfigurationEffects } from '../configuration/store/effects/configuration.effect';
import { RefundAuthorityModule } from '../refund-authority/refund-authority.module';
import { SftpModule } from '../sftp/sftp.module';
import { TicketingAuthorityModule } from '../ticketing-authority/ticketing-authority.module';

import { AcceptInvitationDialogComponent } from './accept-invitation-dialog/accept-invitation-dialog.component';
import { AgentGroupInvitationsComponent } from './agent-group-invitations/agent-group-invitations.component';
import { AgentGroupListComponent } from './agent-group-list/agent-group-list.component';
import { AgentGroupMemberInfoComponent } from './agent-group-member-info/agent-group-member-info.component';
import { AgentGroupMemberListComponent } from './agent-group-member-list/agent-group-member-list.component';
import { AgentGroupProfileComponent } from './agent-group-profile/agent-group-profile.component';
import { AgentGroupRoutingModule } from './agent-group-routing.module';
import { AgentGroupSubTabContainerComponent } from './agent-group-sub-tab-container/agent-group-sub-tab-container.component';
import { CreateAgentGroupComponent } from './create-agent-group/create-agent-group.component';
import { InvitationSummaryComponent } from './invitation-summary/invitation-summary.component';
import { InviteMemberDialogComponent } from './invite-member-dialog/invite-member-dialog.component';
import { ModifyIndividualAccessDialogComponent } from './modify-individual-access-dialog/modify-individual-access-dialog.component';
import { RejectInvitationDialogComponent } from './reject-invitation-dialog/reject-invitation-dialog.component';
import { RemoveAgentGroupMemberDialogComponent } from './remove-agent-group-member-dialog/remove-agent-group-member-dialog.component';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    AgentGroupRoutingModule,
    SharedModule,
    AdmAcmAuthorityModule,
    ConfigurationModule,
    RefundAuthorityModule,
    TicketingAuthorityModule,
    EffectsModule.forFeature([ConfigurationEffects]),
    SftpModule
  ],
  declarations: [
    AgentGroupProfileComponent,
    AgentGroupListComponent,
    AgentGroupMemberListComponent,
    InviteMemberDialogComponent,
    ModifyIndividualAccessDialogComponent,
    RemoveAgentGroupMemberDialogComponent,
    AgentGroupMemberInfoComponent,
    AgentGroupInvitationsComponent,
    RejectInvitationDialogComponent,
    InvitationSummaryComponent,
    AcceptInvitationDialogComponent,
    AgentGroupSubTabContainerComponent,
    CreateAgentGroupComponent
  ],
  providers: [],
  exports: []
})
export class AgentGroupModule {}
