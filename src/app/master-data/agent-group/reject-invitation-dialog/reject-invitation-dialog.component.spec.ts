import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AgentGroupService } from '../services/agent-group.service';
import { RejectInvitationDialogComponent } from './reject-invitation-dialog.component';
import { DialogConfig, DialogService } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';

describe('RejectInvitationDialogComponent', () => {
  let component: RejectInvitationDialogComponent;
  let fixture: ComponentFixture<RejectInvitationDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        mockProvider(DialogConfig, {
          data: { invitation: null }
        }),
        mockProvider(DialogService),
        mockProvider(AgentGroupService),
        mockProvider(L10nTranslationService),
        mockProvider(NotificationService),
        mockProvider(ReactiveSubject, { asObservable: of() })
      ],
      declarations: [RejectInvitationDialogComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectInvitationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
