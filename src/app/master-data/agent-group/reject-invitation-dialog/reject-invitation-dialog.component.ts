import { Component, OnInit } from '@angular/core';
import { filter, finalize, switchMap, tap } from 'rxjs/operators';

import { AgentGroupService } from '../services/agent-group.service';
import { AgentGroupInvitation } from '~app/master-data/models/agent-group.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-reject-invitation-dialog',
  templateUrl: './reject-invitation-dialog.component.html',
  styleUrls: ['./reject-invitation-dialog.component.scss']
})
export class RejectInvitationDialogComponent implements OnInit {
  public get invitation(): AgentGroupInvitation {
    return this.config.data.invitation;
  }

  private get rejectButton() {
    return this.config.data.buttons.find(button => button.type === FooterButton.Reject);
  }

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private agentGroupService: AgentGroupService,
    private dialogService: DialogService,
    private notificationService: NotificationService
  ) {}

  public ngOnInit(): void {
    this.reactiveSubject.asObservable
      .pipe(
        filter(({ clickedBtn }) => clickedBtn === FooterButton.Reject),
        tap(() => (this.rejectButton.isLoading = true)),
        switchMap(() =>
          this.agentGroupService.deleteMember(this.invitation.group.id, this.invitation.id).pipe(
            tap(() => {
              this.reactiveSubject.emit(FooterButton.Reject);
              this.notificationService.showSuccess('MASTER_DATA.agentGroupInvitations.reject.success');
            }),
            // Close the modal regardless of the operation failure/success
            finalize(() => {
              this.dialogService.close();
              this.rejectButton.isLoading = false;
            })
          )
        )
      )
      .subscribe();
  }
}
