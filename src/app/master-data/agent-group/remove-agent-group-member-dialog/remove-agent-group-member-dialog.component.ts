import { Component, OnDestroy, OnInit } from '@angular/core';
import { clone } from 'lodash';
import { Subject } from 'rxjs';
import { filter, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { AgentGroupService } from '../services/agent-group.service';
import { MasterDataAgentGroup, MasterDataAgentGroupMember } from '~app/master-data/models/agent-group.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-remove-agent-group-member-dialog',
  templateUrl: './remove-agent-group-member-dialog.component.html',
  styleUrls: ['./remove-agent-group-member-dialog.component.scss']
})
export class RemoveAgentGroupMemberDialogComponent implements OnInit, OnDestroy {
  public member: MasterDataAgentGroupMember;
  public agentGroup: MasterDataAgentGroup;
  public description: string;
  public successMsg: string;

  private destroy$ = new Subject();
  private removeButton: ModalAction;

  constructor(
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private agentGroupService: AgentGroupService
  ) {}

  public ngOnInit(): void {
    this.member = clone(this.config.data.member);
    this.agentGroup = clone(this.config.data.agentGroup);
    this.description = this.config.data.description;
    this.successMsg = this.config.data.successMsg;

    this.removeButton = this.config.data?.buttons?.find(b => this.isRemoveButton(b.type));

    this.handleActions();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private handleActions(): void {
    this.reactiveSubject.asObservable
      .pipe(
        map(res => res.clickedBtn),
        filter(this.isRemoveButton),
        switchMap(clickedBtn => {
          this.removeButton.isLoading = true;

          const groupId = this.agentGroup.id;
          const memberId = this.member.id;

          return this.agentGroupService.deleteMember(groupId, memberId).pipe(
            map(() => clickedBtn),
            finalize(() => this.dialogService.close())
          );
        }),
        tap((clickedBtn: FooterButton) => {
          this.notificationService.showSuccess(this.successMsg);
          this.reactiveSubject.emit(clickedBtn);
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private isRemoveButton(button: FooterButton): boolean {
    return button === FooterButton.Remove || button === FooterButton.CancelInvitation;
  }
}
