import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { of } from 'rxjs';

import { AgentGroupService } from '../services/agent-group.service';
import { RemoveAgentGroupMemberDialogComponent } from './remove-agent-group-member-dialog.component';
import { DialogConfig, DialogService } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('RemoveAgentGroupMemberDialogComponent', () => {
  let component: RemoveAgentGroupMemberDialogComponent;
  let fixture: ComponentFixture<RemoveAgentGroupMemberDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RemoveAgentGroupMemberDialogComponent, TranslatePipeMock],
      providers: [
        mockProvider(DialogConfig, {
          data: {
            member: {},
            agentGroup: {}
          }
        }),
        mockProvider(ReactiveSubject, { asObservable: of() }),
        mockProvider(DialogService),
        mockProvider(NotificationService),
        mockProvider(AgentGroupService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveAgentGroupMemberDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
