import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { finalize, first, tap } from 'rxjs/operators';

import { AgentGroupService } from '../services/agent-group.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AgentGroupCreateForm } from '~app/master-data/models/agent-group.model';
import { AppState } from '~app/reducers';
import { User } from '~app/shared/models/user.model';
import { NotificationService, RouterUtilsService } from '~app/shared/services';
import { alphanumericValidator } from '~app/shared/validators';

@Component({
  selector: 'bspl-create-agent-group',
  templateUrl: './create-agent-group.component.html',
  styleUrls: ['./create-agent-group.component.scss']
})
export class CreateAgentGroupComponent implements OnInit {
  @Output() cancel = new EventEmitter();

  public readonly iataCodeMinLength = 6;
  public readonly iataCodeMaxLength = 7;

  public agentGroupForm: FormGroup;
  public isCreating = false;

  private loggedUser$: Observable<User> = this.store.pipe(select(getUser), first());

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private agentGroupService: AgentGroupService,
    private notificationService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private routerUtilsService: RouterUtilsService
  ) {}

  public ngOnInit(): void {
    this.buildForm();
  }

  public createAgentGroup(group: AgentGroupCreateForm): void {
    this.isCreating = true;

    this.agentGroupService
      .createGroup(group)
      .pipe(
        tap(() => {
          this.cancelCreation();
          this.routerUtilsService.hardReloadCurrentRoute(this.activatedRoute);
          this.notificationService.showSuccess('MASTER_DATA.createAgentGroup.createSuccessMsg');
        }),
        finalize(() => (this.isCreating = false))
      )
      .subscribe();
  }

  public cancelCreation(): void {
    this.cancel.emit();
  }

  public capitalizeIataCode(): void {
    const iataCodeKey: keyof AgentGroupCreateForm = 'iataCode';
    const iataCode: string = this.agentGroupForm.get(iataCodeKey).value;
    this.agentGroupForm.patchValue({
      [iataCodeKey]: iataCode.toUpperCase()
    });
  }

  private buildForm(): void {
    this.loggedUser$.subscribe(user => {
      const form: { [key in keyof AgentGroupCreateForm]: any } = {
        iataCode: [
          '',
          [Validators.required, Validators.minLength(this.iataCodeMinLength), alphanumericValidator(false, 1, 1)]
        ],
        portalEmail: [user.email],
        name: [user.lastName],
        organization: [user.organization],
        telephone: [user.telephone],
        address: [user.address],
        postCode: [user.zip],
        city: [user.locality],
        country: [user.country]
      };

      this.agentGroupForm = this.formBuilder.group(form);
    });
  }
}
