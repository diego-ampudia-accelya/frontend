import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

import { AgentGroupService } from '../services/agent-group.service';
import { CreateAgentGroupComponent } from './create-agent-group.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { NotificationService, RouterUtilsService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('CreateAgentGroupComponent', () => {
  let component: CreateAgentGroupComponent;
  let fixture: ComponentFixture<CreateAgentGroupComponent>;
  const initialState = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAgentGroupComponent, TranslatePipeMock],
      providers: [
        FormBuilder,
        provideMockStore({ selectors: [{ selector: getUser, value: {} }], initialState }),
        mockProvider(AgentGroupService),
        mockProvider(NotificationService),
        mockProvider(ActivatedRoute),
        mockProvider(RouterUtilsService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAgentGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
