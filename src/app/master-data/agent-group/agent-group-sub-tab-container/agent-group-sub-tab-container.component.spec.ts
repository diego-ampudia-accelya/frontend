import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';

import { AgentGroupSubTabContainerComponent } from './agent-group-sub-tab-container.component';

describe('AgentGroupSubTabContainerComponent', () => {
  let component: AgentGroupSubTabContainerComponent;
  let fixture: ComponentFixture<AgentGroupSubTabContainerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AgentGroupSubTabContainerComponent],
      providers: [
        mockProvider(ActivatedRoute, {
          snapshot: {
            data: {
              agentGroup: {}
            }
          }
        })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentGroupSubTabContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
