import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MasterDataAgentGroup } from '~app/master-data/models/agent-group.model';

@Component({
  selector: 'bspl-agent-group-sub-tab-container',
  templateUrl: './agent-group-sub-tab-container.component.html'
})
export class AgentGroupSubTabContainerComponent {
  public isCreatingAgentGroup: boolean;

  public get agentGroup(): MasterDataAgentGroup {
    return this.activatedRoute.snapshot.data.agentGroup;
  }

  constructor(private activatedRoute: ActivatedRoute) {}
}
