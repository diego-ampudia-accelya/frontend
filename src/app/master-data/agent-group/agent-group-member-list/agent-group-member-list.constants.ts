import { AgentGroupMemberFilter } from '../models';
import { GridColumn } from '~app/shared/models';

const translationPrefix = 'MASTER_DATA.agentGroupMembers.query.columns';

export const SHARED = {
  get COLUMNS(): GridColumn[] {
    return [
      {
        name: `${translationPrefix}.bsp`,
        prop: 'user.bsp.isoCountryCode',
        draggable: false,
        resizeable: true,
        sortable: false,
        width: 100
      },
      {
        name: `${translationPrefix}.agentCode`,
        prop: 'user.userTypeDetails.iataCode',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 90
      },
      {
        name: `${translationPrefix}.membership`,
        prop: 'membership',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 125
      },
      {
        name: `${translationPrefix}.individualAccess`,
        prop: 'individualAccess',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.organization`,
        prop: 'user.contactInfo.organization',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 145
      },
      {
        name: `${translationPrefix}.name`,
        prop: 'user.userTypeDetails.name',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 140
      },
      {
        name: `${translationPrefix}.email`,
        prop: 'user.contactInfo.email',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 145
      },
      {
        name: `${translationPrefix}.telephone`,
        prop: 'user.contactInfo.telephone',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 105
      },
      {
        name: `${translationPrefix}.address`,
        prop: 'user.contactInfo.address',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.locality`,
        prop: 'user.contactInfo.city',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 105
      },
      {
        name: `${translationPrefix}.postCode`,
        prop: 'user.contactInfo.postCode',
        draggable: false,
        resizeable: false,
        sortable: true,
        width: 105
      }
    ];
  }
};

export const SEARCH_FORM_DEFAULT_VALUE: AgentGroupMemberFilter = {
  agents: null,
  membership: null,
  individualAccess: null,
  organization: null,
  name: null,
  email: null,
  telephone: null,
  address: null,
  locality: null,
  postCode: null
};
