import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';
import { of } from 'rxjs';

import { AgentGroupMemberFormatter } from '../services/agent-group-member-formatter.service';
import { AgentGroupService } from '../services/agent-group.service';
import { AgentGroupMemberListComponent } from './agent-group-member-list.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataAgentGroup } from '~app/master-data/models/agent-group.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants/routes';
import { createAgentUser } from '~app/shared/mocks/agent-user';
import { AgentDictionaryService, NotificationService } from '~app/shared/services';

const query: DataQuery = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
};

describe('AgentGroupMemberListComponent', () => {
  let component: AgentGroupMemberListComponent;
  let fixture: ComponentFixture<AgentGroupMemberListComponent>;
  let queryableDataSourceSpy: SpyObject<QueryableDataSource<AgentGroupService>>;
  const activatedRouteStub = {
    snapshot: {
      data: {
        agentGroup: {
          user: {
            userTypeDetails: {
              name: 'Group Name'
            }
          }
        } as MasterDataAgentGroup
      },
      params: {
        id: 1
      }
    }
  };

  const initialState = {
    auth: {
      user: createAgentUser()
    },
    core: {
      menu: {
        tabs: { dashboard: { ...ROUTES.AGENT_GROUP_MEMBERS, id: 'agent-groups-members' } },
        activeTabId: 'agent-groups-members'
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    queryableDataSourceSpy = createSpyObject(QueryableDataSource, { appliedQuery$: of(query) });
    queryableDataSourceSpy.get.and.returnValue(of(query));

    TestBed.configureTestingModule({
      declarations: [AgentGroupMemberListComponent, MockPipe(HasPermissionPipe)],
      imports: [HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        FormBuilder,
        L10nTranslationService,
        provideMockStore({ initialState }),
        AgentGroupService,
        mockProvider(NotificationService),
        mockProvider(AgentGroupMemberFormatter),
        mockProvider(DefaultQueryStorage),
        mockProvider(AgentDictionaryService),
        mockProvider(PermissionsService)
      ]
    })
      .overrideComponent(AgentGroupMemberListComponent, {
        set: {
          providers: [{ provide: QueryableDataSource, useValue: queryableDataSourceSpy }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentGroupMemberListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
