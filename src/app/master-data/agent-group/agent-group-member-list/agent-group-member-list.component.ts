import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty, mapValues } from 'lodash';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { InviteMemberDialogComponent } from '../invite-member-dialog/invite-member-dialog.component';
import { AgentGroupMemberFilter } from '../models';
import { ModifyIndividualAccessDialogComponent } from '../modify-individual-access-dialog/modify-individual-access-dialog.component';
import { RemoveAgentGroupMemberDialogComponent } from '../remove-agent-group-member-dialog/remove-agent-group-member-dialog.component';
import { AgentGroupMemberFormatter } from '../services/agent-group-member-formatter.service';
import { AgentGroupService } from '../services/agent-group.service';
import { SEARCH_FORM_DEFAULT_VALUE, SHARED } from './agent-group-member-list.constants';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AgentGroupIndividualAccess, AgentGroupMembership } from '~app/master-data/enums';
import { MasterDataAgentGroup, MasterDataAgentGroupMember } from '~app/master-data/models/agent-group.model';
import { ViewAs } from '~app/master-data/ticketing-authority';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FakePermissions } from '~app/shared/constants/fake-permissions';
import { Permissions } from '~app/shared/constants/permissions';
import { SortOrder } from '~app/shared/enums';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { AgentDictionaryService, NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-agent-group-member-list',
  templateUrl: './agent-group-member-list.component.html',
  providers: [
    DefaultQueryStorage,
    AgentGroupMemberFormatter,
    AgentGroupService,
    { provide: QueryableDataSource, useClass: QueryableDataSource, deps: [AgentGroupService] }
  ]
})
export class AgentGroupMemberListComponent implements OnInit {
  public columns: Array<GridColumn> = [];
  public searchForm: FormGroup;
  public downloadQuery: DataQuery<AgentGroupMemberFilter>;
  public tableTitle: string;
  public customLabels = {
    create: 'MASTER_DATA.agentGroupMembers.inviteMember'
  };
  public permissions = Permissions;

  public agentOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public membershipOptions: DropdownOption<AgentGroupMembership>[];
  public individualAccessOptions: DropdownOption<AgentGroupIndividualAccess>[];

  private modifyAction = GridTableActionType.ModifyIndividualAccess;
  private removeAction = GridTableActionType.RemoveMember;
  private cancelAction = GridTableActionType.CancelInvitation;

  public tableActions = [
    {
      action: this.modifyAction,
      hidden: row => this.isTableMenuActionHidden(row, this.modifyAction)
    },
    {
      action: this.removeAction,
      hidden: row => this.isTableMenuActionHidden(row, this.removeAction)
    },
    {
      action: this.cancelAction,
      hidden: row => this.isTableMenuActionHidden(row, this.cancelAction)
    }
  ];

  public get viewAs(): ViewAs {
    return this.activatedRoute.snapshot.data.viewAs;
  }

  public get isAgentGroupView(): boolean {
    return this.viewAs === ViewAs.AgentGroup;
  }

  private agentGroup: MasterDataAgentGroup = this.activatedRoute.snapshot.data.agentGroup;

  constructor(
    public dataSource: QueryableDataSource<MasterDataAgentGroupMember>,
    public displayFormatter: AgentGroupMemberFormatter,
    private queryStorage: DefaultQueryStorage,
    private translationService: L10nTranslationService,
    private agentGroupService: AgentGroupService,
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private agentDictionaryService: AgentDictionaryService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.setTableTitle();
    this.setResolversData();
    this.buildSearchForm();
  }

  public setResolversData(): void {
    this.setColumns();

    const query: DataQuery = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  public loadData(query: DataQuery = this.queryStorage.get()): void {
    let dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      },
      sortBy: {
        ...query.sortBy
      }
    };

    if (isEmpty(dataQuery.sortBy)) {
      const sortBy = [{ attribute: 'user.userTypeDetails.iataCode', sortType: SortOrder.Asc }];
      dataQuery = { ...dataQuery, sortBy };
    }

    this.downloadQuery = dataQuery;

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.initializeFiltersDropdown();
    }
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: 'MASTER_DATA.agentGroupMembers.query.downloadTitle',
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery
      },
      apiService: this.agentGroupService
    });
  }

  public inviteMember(): void {
    this.dialogService
      .open(InviteMemberDialogComponent, {
        data: {
          title: 'MASTER_DATA.agentGroupMembers.inviteMemberDialog.title',
          footerButtonsType: FooterButton.Invite,
          agentGroup: this.agentGroup
        }
      })
      .pipe(filter(action => action === FooterButton.Done))
      .subscribe(() => {
        this.notificationService.showSuccess('MASTER_DATA.agentGroupMembers.inviteMemberDialog.success');
        this.loadData();
      });
  }

  public executeAction({ action, row }: { event: Event; action: GridTableAction; row: MasterDataAgentGroupMember }) {
    if (action.actionType === GridTableActionType.ModifyIndividualAccess) {
      this.openModifyIndividualAccessDialog(row);
    }

    if (action.actionType === GridTableActionType.RemoveMember) {
      this.openRemoveAgentGroupMemberDialog(row);
    }

    if (action.actionType === GridTableActionType.CancelInvitation) {
      this.openCancelAgentGroupMemberDialog(row);
    }
  }

  private setColumns() {
    this.columns = SHARED.COLUMNS;

    const individualAccessColumn = this.columns.find(c => c.prop === 'individualAccess');
    if (individualAccessColumn) {
      individualAccessColumn.pipe = {
        transform: value => this.translateIndividualAccess(value)
      };
    }
  }

  private buildSearchForm() {
    this.searchForm = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));
  }

  private setTableTitle(): void {
    if (this.viewAs === ViewAs.AgentGroup) {
      this.tableTitle = 'MASTER_DATA.agentGroupMembers.query.agentGroupTitle';
    } else {
      this.tableTitle = this.translationService.translate('MASTER_DATA.agentGroupMembers.query.title', {
        name: this.agentGroup.user.userTypeDetails.iataCode?.toUpperCase()
      });
    }
  }

  private initializeFiltersDropdown() {
    this.agentOptions$ = this.getAgentDropDownByUserType();
    this.membershipOptions = [
      AgentGroupMembership.Creator,
      AgentGroupMembership.Invited,
      AgentGroupMembership.Member
    ].map(value => ({
      value,
      label: this.translationService.translate(`MASTER_DATA.agentGroupMembers.membership.${value}`)
    }));

    this.individualAccessOptions = [AgentGroupIndividualAccess.Forbidden, AgentGroupIndividualAccess.Permitted].map(
      value => ({
        value,
        label: this.translateIndividualAccess(value)
      })
    );
  }

  private getAgentDropDownByUserType(): Observable<DropdownOption<AgentSummary>[]> {
    return this.isAgentGroupView
      ? this.agentDictionaryService.getAgentGroupUserDropdownOptions(FakePermissions.readAgentDictionary)
      : this.agentDictionaryService.getDropdownOptions();
  }

  private translateIndividualAccess(value: AgentGroupIndividualAccess): string {
    return this.translationService.translate(`MASTER_DATA.agentGroupMembers.individualAccess.${value}`);
  }

  private openModifyIndividualAccessDialog(member: MasterDataAgentGroupMember): void {
    this.dialogService
      .open(ModifyIndividualAccessDialogComponent, {
        data: {
          title: 'MASTER_DATA.agentGroupMembers.modifyIndividualAccessDialog.title',
          footerButtonsType: FooterButton.Modify,
          member,
          agentGroup: this.agentGroup
        }
      })
      .pipe(filter(action => action === FooterButton.Modify))
      .subscribe(() => {
        this.loadData();
      });
  }

  private openRemoveAgentGroupMemberDialog(member: MasterDataAgentGroupMember): void {
    this.dialogService
      .open(RemoveAgentGroupMemberDialogComponent, {
        data: {
          title: 'MASTER_DATA.agentGroupMembers.removeAgentGroupMemberDialog.title',
          description: 'MASTER_DATA.agentGroupMembers.removeAgentGroupMemberDialog.description',
          successMsg: 'MASTER_DATA.agentGroupMembers.removeAgentGroupMemberDialog.successMsg',
          footerButtonsType: FooterButton.Remove,
          member,
          agentGroup: this.agentGroup
        }
      })
      .pipe(filter(action => action === FooterButton.Remove))
      .subscribe(() => {
        this.loadData();
      });
  }

  private openCancelAgentGroupMemberDialog(member: MasterDataAgentGroupMember): void {
    this.dialogService
      .open(RemoveAgentGroupMemberDialogComponent, {
        data: {
          title: 'MASTER_DATA.agentGroupMembers.cancelAgentGroupMemberDialog.title',
          description: 'MASTER_DATA.agentGroupMembers.cancelAgentGroupMemberDialog.description',
          successMsg: 'MASTER_DATA.agentGroupMembers.cancelAgentGroupMemberDialog.successMsg',
          footerButtonsType: FooterButton.CancelInvitation,
          member,
          agentGroup: this.agentGroup
        }
      })
      .pipe(filter(action => action === FooterButton.CancelInvitation))
      .subscribe(() => {
        this.loadData();
      });
  }

  private isTableMenuActionHidden(row: MasterDataAgentGroupMember, action: GridTableActionType): boolean {
    const hasPermissions = this.permissionsService.hasPermission(Permissions.updateAgentGroup);
    if (row.membership === AgentGroupMembership.Creator || !hasPermissions) {
      return true;
    }

    if (row.membership === AgentGroupMembership.Member) {
      return action === this.cancelAction;
    }

    if (row.membership === AgentGroupMembership.Invited) {
      return action === this.removeAction;
    }

    return false;
  }
}
