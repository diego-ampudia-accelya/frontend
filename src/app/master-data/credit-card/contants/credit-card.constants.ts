import { GridColumn } from '~app/shared/models';

export const CREDIT_CARDS_COLUMNS = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'abbreviation',
        name: 'LIST.MASTER_DATA.creditCards.columns.abbreviation',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'name',
        name: 'LIST.MASTER_DATA.creditCards.columns.name',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'prefix',
        name: 'LIST.MASTER_DATA.creditCards.columns.prefix',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'longcode',
        name: 'LIST.MASTER_DATA.creditCards.columns.longcode',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'address',
        name: 'LIST.MASTER_DATA.creditCards.columns.address',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'locality',
        name: 'LIST.MASTER_DATA.creditCards.columns.locality',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'city',
        name: 'LIST.MASTER_DATA.creditCards.columns.city',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'zip',
        name: 'LIST.MASTER_DATA.creditCards.columns.zip',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'country',
        name: 'LIST.MASTER_DATA.creditCards.columns.country',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'luhnException',
        name: 'LIST.MASTER_DATA.creditCards.columns.luhnException',
        resizeable: true,
        draggable: false,
        cellTemplate: 'yesNoTmpl'
      }
    ];
  }
};
