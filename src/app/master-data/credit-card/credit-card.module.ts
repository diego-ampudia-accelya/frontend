import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { CreditCardChangesDialogComponent } from './components/credit-card-changes-dialog/credit-card-changes-dialog.component';
import { ChangesDialogConfigService } from './components/credit-card-changes-dialog/services/changes-dialog-config.service';
import { ChangesDialogService } from './components/credit-card-changes-dialog/services/changes-dialog.service';
import { CreditCardListComponent } from './components/credit-card-list/credit-card-list.component';
import { CreditCardRoutingModule } from './credit-card-routing.module';
import { CreditCardComponent } from './credit-card.component';
import { CreditCardFilterFormatterService } from './services/credit-card-filter-formatter.service';
import { CreditCardService } from './services/credit-card.service';
import { CreditCardStoreFacadeService } from './store/credit-card-store-facade.service';
import { creditCardFeatureKey } from './store/credit-card.state';
import { CreditCardEffects } from './store/effects/credit-card.effects';
import { creditCardReducer } from './store/reducers/credit-card.reducer';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CreditCardRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(creditCardFeatureKey, creditCardReducer),
    EffectsModule.forFeature([CreditCardEffects])
  ],
  declarations: [CreditCardComponent, CreditCardListComponent, CreditCardChangesDialogComponent],
  providers: [
    CreditCardService,
    CreditCardStoreFacadeService,
    CreditCardFilterFormatterService,
    ChangesDialogService,
    ChangesDialogConfigService
  ]
})
export class CreditCardModule {}
