import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';
import { of } from 'rxjs';

import { CreditCardListComponent } from './components/credit-card-list/credit-card-list.component';
import { CreditCardComponent } from './credit-card.component';
import { CreditCardStoreFacadeService } from './store/credit-card-store-facade.service';

describe('CreditCardComponent', () => {
  let component: CreditCardComponent;
  let fixture: ComponentFixture<CreditCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CreditCardComponent, MockComponent(CreditCardListComponent)],
      providers: [
        {
          provide: CreditCardStoreFacadeService,
          useValue: {
            actions: {
              searchCreditCardRequest: () => {},
              downloadCreditCardRequest: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              bspId$: of(null),
              hasData$: of(null)
            },
            loading: {
              searchCreditCardRequest$: of(null)
            }
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
