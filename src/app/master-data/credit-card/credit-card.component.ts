import { Component, OnInit } from '@angular/core';

import { CreditCardStoreFacadeService } from './store/credit-card-store-facade.service';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

@Component({
  selector: 'bspl-credit-card',
  templateUrl: './credit-card.component.html'
})
export class CreditCardComponent implements OnInit {
  constructor(private creditCardStoreFacadeService: CreditCardStoreFacadeService) {}

  public ngOnInit(): void {
    this.creditCardStoreFacadeService.actions.searchCreditCardRequest(defaultQuery);
  }
}
