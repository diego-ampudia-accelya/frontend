import { CreditCard } from '../models/credit-card.models';
import { PagedData } from '~app/shared/models/paged-data.model';

export const userBspId = 0;

export const creditCardQueryResponse: PagedData<CreditCard> = {
  pageNumber: 0,
  pageSize: 20,
  total: 83,
  totalPages: 5,
  records: [
    {
      id: '698314049050051052',
      abbreviation: '""',
      name: 'comillas',
      prefix: '1234',
      longcode: '14',
      address: '1',
      locality: '1',
      city: '1',
      zip: '1234AAAA',
      country: '1',
      luhnException: false
    },
    {
      id: '698399056056',
      abbreviation: '<<',
      name: '<',
      prefix: '88',
      longcode: '99',
      address: '<',
      locality: '<',
      city: '<',
      zip: '<',
      country: '<',
      luhnException: false
    },
    {
      id: '698301049',
      abbreviation: '@@',
      name: '@',
      prefix: '1',
      longcode: '1',
      address: '@',
      locality: '@',
      city: '@',
      zip: '@',
      country: '@',
      luhnException: false
    },
    {
      id: '698316050057052050',
      abbreviation: 'AC',
      name: 'AEROMAR CARD',
      prefix: '2942',
      longcode: '16',
      address: '-',
      locality: '-',
      city: '-',
      zip: '-',
      country: '-',
      luhnException: false
    },
    {
      id: '698300065079',
      abbreviation: 'AO',
      name: 'AO',
      prefix: 'AO',
      longcode: '0',
      address: 'TEST',
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698310065079',
      abbreviation: 'AO',
      name: 'asd',
      prefix: 'AO',
      longcode: '10',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698315051055',
      abbreviation: 'AX',
      name: 'XXX',
      prefix: '37',
      longcode: '15',
      address: null,
      locality: null,
      city: null,
      zip: '-',
      country: null,
      luhnException: false
    },
    {
      id: '698300066067',
      abbreviation: 'BC',
      name: 'BC',
      prefix: 'BC',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698300066077',
      abbreviation: 'BM',
      name: 'BM',
      prefix: 'BM',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698300066079',
      abbreviation: 'BO',
      name: 'BO',
      prefix: 'BO',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698300067057',
      abbreviation: 'C9',
      name: 'c9name',
      prefix: 'C9',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698316053052',
      abbreviation: 'CA',
      name: 'MASTERCARD',
      prefix: '54',
      longcode: '16',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698316053049',
      abbreviation: 'CA',
      name: 'MASTERCARD',
      prefix: '51',
      longcode: '16',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698316053056',
      abbreviation: 'CA',
      name: 'MASTERCARD',
      prefix: '58',
      longcode: '16',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698316053051',
      abbreviation: 'CA',
      name: 'MASTERCARD',
      prefix: '53',
      longcode: '16',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698316053050',
      abbreviation: 'CA',
      name: 'MASTERCARD',
      prefix: '52',
      longcode: '16',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698300067048',
      abbreviation: 'CN',
      name: 'C0',
      prefix: 'C0',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698300067049',
      abbreviation: 'CN',
      name: 'C1',
      prefix: 'C1',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698300067056',
      abbreviation: 'CN',
      name: 'C8',
      prefix: 'C8',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    },
    {
      id: '698300067050',
      abbreviation: 'CN',
      name: 'C2',
      prefix: 'C2',
      longcode: '0',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    }
  ]
};
