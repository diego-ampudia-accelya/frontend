import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from './actions/credit-card.actions';
import { CreditCardStoreFacadeService } from './credit-card-store-facade.service';
import * as fromSelectors from './credit-card.selectors';
import { creditCardFeatureKey, initialState } from './credit-card.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

describe('Service: CreditCardStoreFacade', () => {
  let service: CreditCardStoreFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CreditCardStoreFacadeService,
        provideMockStore({
          initialState: {
            [creditCardFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(CreditCardStoreFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('searchCreditCardRequest should dispatch a new searchCreditCardRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchCreditCardRequest(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchCreditCardRequest({ payload: { query: defaultQuery } })
      );
    });

    it('downloadCreditCardRequest should dispatch a new downloadCreditCardRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.downloadCreditCardRequest();

      expect(store.dispatch).toHaveBeenCalledWith(fromActions.downloadCreditCardRequest());
    });
  });

  describe('selectors', () => {
    it('query$ should select query', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectCreditCardQuery);
    });

    it('data$ should select data', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectCreditCardData);
    });

    it('bspId$ should select bspId', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.bspId$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromAuth.getUserBspId);
    });

    it('hasData$ should select hasData', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('searchCreditCardRequest$ should select SearchCreditCardRequestLoading', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchCreditCardLoading);
    });
  });
});
