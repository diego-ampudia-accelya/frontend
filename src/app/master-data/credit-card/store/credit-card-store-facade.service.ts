import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { CreditCardFilters } from '../models/credit-card.models';
import * as fromActions from './actions/credit-card.actions';
import * as fromSelectors from './credit-card.selectors';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';

@Injectable()
export class CreditCardStoreFacadeService {
  constructor(private store: Store<AppState>) {}

  public get actions() {
    return {
      searchCreditCardRequest: (query?: DataQuery<CreditCardFilters>) =>
        this.store.dispatch(fromActions.searchCreditCardRequest({ payload: { query } })),
      downloadCreditCardRequest: () => this.store.dispatch(fromActions.downloadCreditCardRequest())
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectCreditCardQuery),
      data$: this.store.select(fromSelectors.selectCreditCardData),
      bspId$: this.store.select(fromAuth.getUserBspId),
      hasData$: this.store.select(fromSelectors.selectHasData)
    };
  }

  public get loading$(): Observable<boolean> {
    return this.store.select(fromSelectors.selectSearchCreditCardLoading);
  }
}
