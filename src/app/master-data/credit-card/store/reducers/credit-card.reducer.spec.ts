import { HttpErrorResponse } from '@angular/common/http';

import * as fromStub from '../../stubs/credit-card.stub';
import * as fromActions from '../actions/credit-card.actions';
import * as fromState from '../credit-card.state';
import * as fromReducer from '../reducers/credit-card.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('creditCardReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.creditCardReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchCreditCardRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchCreditCardRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.creditCardReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: true,
        errors: null
      });
    });
  });

  describe('searchCreditCardFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchCreditCardFailure({ payload: { error } });
      const result = fromReducer.creditCardReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: false,
        errors: error
      });
    });
  });

  describe('searchCreditCardSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchCreditCardSuccess({ payload: { pagedData: fromStub.creditCardQueryResponse } });
      const result = fromReducer.creditCardReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.creditCardQueryResponse.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.creditCardQueryResponse.pageNumber,
            size: fromStub.creditCardQueryResponse.pageSize,
            totalElements: fromStub.creditCardQueryResponse.total
          }
        },
        loading: false,
        errors: null
      });
    });
  });
});
