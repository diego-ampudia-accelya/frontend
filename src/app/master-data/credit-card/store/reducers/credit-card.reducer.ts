import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/credit-card.actions';
import { initialState } from '../credit-card.state';

export const creditCardReducer = createReducer(
  initialState,
  on(fromActions.searchCreditCardRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: true
  })),
  on(fromActions.searchCreditCardFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: false,
    errors: error
  })),
  on(fromActions.searchCreditCardSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: false,
    errors: null
  })),
  on(fromActions.addCreditCard, state => ({
    ...state,
    loading: true
  })),
  on(fromActions.addCreditCardSuccess, state => ({
    ...state,
    loading: false
  })),
  on(fromActions.addCreditCardError, (state, { error }) => ({
    ...state,
    loading: false,
    errors: error
  }))
);
