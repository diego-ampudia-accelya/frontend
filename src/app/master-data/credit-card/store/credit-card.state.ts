import { CreditCard, CreditCardFilters } from '../models/credit-card.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ResponseErrorBE } from '~app/shared/models';

export const creditCardFeatureKey = 'creditCardFeatureKey';

export interface CreditCardState {
  query: DataQuery<CreditCardFilters>;
  data: CreditCard[];
  loading: boolean;
  errors: { error: ResponseErrorBE };
}

export const initialState: CreditCardState = {
  query: defaultQuery,
  data: [],
  loading: false,
  errors: null
};
