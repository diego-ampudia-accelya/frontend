import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { ChangesDialogService } from '../../components/credit-card-changes-dialog/services/changes-dialog.service';
import { translations } from '../../components/credit-card-changes-dialog/translations';
import { CreditCardService } from '../../services/credit-card.service';
import * as fromActions from '../actions/credit-card.actions';
import * as fromSelectors from '../credit-card.selectors';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class CreditCardEffects {
  public constructor(
    private readonly actions$: Actions,
    private readonly creditCardService: CreditCardService,
    private readonly store: Store<unknown>,
    private readonly dialogService: DialogService,
    private readonly changesDialogService: ChangesDialogService,
    private readonly notificationService: NotificationService,
    private readonly translationService: L10nTranslationService
  ) {}

  public searchCreditCardsEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchCreditCardRequest>>(fromActions.searchCreditCardRequest),
      switchMap(({ payload: { query } }) =>
        this.creditCardService.find(query).pipe(
          map(data =>
            fromActions.searchCreditCardSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchCreditCardFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public downloadCreditCardEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.downloadCreditCardRequest>>(fromActions.downloadCreditCardRequest),
        withLatestFrom(
          this.store.pipe(select(fromSelectors.selectCreditCardQuery)),
          this.store.pipe(select(fromAuth.getUserBspId))
        ),
        tap(([_, query, bspId]) => {
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: {
                ...query,
                filterBy: {
                  ...query?.filterBy,
                  bspId
                }
              }
            },
            apiService: this.creditCardService
          });
        })
      ),
    { dispatch: false }
  );

  public openAdd$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromActions.openAdd),
        switchMap(() => this.changesDialogService.open())
      ),
    { dispatch: false }
  );

  public addCreditCard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.addCreditCard),
      switchMap(({ value }) =>
        this.changesDialogService.create(value).pipe(
          map(() => fromActions.addCreditCardSuccess()),
          rethrowError(error => fromActions.addCreditCardError({ error }))
        )
      )
    )
  );

  public addCreditCardSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromActions.addCreditCardSuccess),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectCreditCardQuery))),
        tap(([_, query]) => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(translations.create.details);
          this.notificationService.showSuccess(successMessage);

          this.store.dispatch(fromActions.searchCreditCardRequest({ payload: { query } }));
        })
      ),
    { dispatch: false }
  );

  public openApplyChanges$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromActions.openApplyChanges),
        switchMap(({ value }) => this.changesDialogService.open(value))
      ),
    { dispatch: false }
  );

  public applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.applyChanges),
      switchMap(({ value }) =>
        this.changesDialogService.edit(value).pipe(
          map(() => fromActions.applyChangesSuccess({ value })),
          rethrowError(error => fromActions.applyChangesError({ error }))
        )
      )
    )
  );

  public applyChangesSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromActions.applyChangesSuccess),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectCreditCardQuery))),
        tap(([_, query]) => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(translations.edit.details);
          this.notificationService.showSuccess(successMessage);

          this.store.dispatch(fromActions.searchCreditCardRequest({ payload: { query } }));
        })
      ),
    { dispatch: false }
  );

  public openRemove$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromActions.openRemove),
        switchMap(({ value }) => this.changesDialogService.open(value, true))
      ),
    { dispatch: false }
  );

  public remove$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.remove),
      switchMap(({ value }) =>
        this.changesDialogService.delete(value).pipe(
          map(() => fromActions.removeSuccess({ value })),
          rethrowError(error => fromActions.removeError({ error }))
        )
      )
    )
  );

  public removeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromActions.removeSuccess),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectCreditCardQuery))),
        tap(([_, query]) => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(translations.delete.details);
          this.notificationService.showSuccess(successMessage);

          this.store.dispatch(fromActions.searchCreditCardRequest({ payload: { query } }));
        })
      ),
    { dispatch: false }
  );
}
