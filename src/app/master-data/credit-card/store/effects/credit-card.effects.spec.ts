import { HttpErrorResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { cold, hot } from 'jasmine-marbles';
import { MockProviders } from 'ng-mocks';
import { Observable, of, throwError } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import { TestScheduler } from 'rxjs/testing';

import { ChangesDialogService } from '../../components/credit-card-changes-dialog/services/changes-dialog.service';
import { translations } from '../../components/credit-card-changes-dialog/translations';
import { CreditCardService } from '../../services/credit-card.service';
import * as fromStub from '../../stubs/credit-card.stub';
import * as fromActions from '../actions/credit-card.actions';
import * as fromSelectors from '../credit-card.selectors';
import * as fromState from '../credit-card.state';
import { CreditCardEffects } from '../effects/credit-card.effects';
import { NotificationService } from '~app/shared/services';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

describe('CreditCardEffects', () => {
  let actions$: Observable<any>;
  let effects: CreditCardEffects;
  let service: CreditCardService;
  let dialogService: DialogService;
  let scheduler: TestScheduler;
  let mockStore: MockStore;

  const initialState = {
    [fromState.creditCardFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CreditCardEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectCreditCardQuery,
              value: defaultQuery
            },
            {
              selector: fromAuth.getUserBspId,
              value: fromStub.userBspId
            }
          ]
        }),
        {
          provide: CreditCardService,
          useValue: {
            find: () => {},
            download: () => {}
          }
        },
        {
          provide: DialogService,
          useValue: {
            open: () => {},
            close: () => {}
          }
        },
        {
          provide: ChangesDialogService,
          useValue: {
            open: () => {},
            create: () => of({}),
            edit: () => of({}),
            delete: () => of({})
          }
        },
        MockProviders(NotificationService, L10nTranslationService)
      ]
    });

    effects = TestBed.inject<CreditCardEffects>(CreditCardEffects);
    service = TestBed.inject<CreditCardService>(CreditCardService);
    dialogService = TestBed.inject<DialogService>(DialogService);
    mockStore = TestBed.inject<any>(Store);
  });

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchCreditCardsEffect', () => {
    it('should return searchCreditCardSuccess with paged data', () => {
      const action = fromActions.searchCreditCardRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchCreditCardSuccess({
        payload: {
          pagedData: fromStub.creditCardQueryResponse
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.creditCardQueryResponse });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchCreditCardsEffect$).toBeObservable(expected);
    });

    it('should return searchCreditCardFailure when credit card service find fails', () => {
      const action = fromActions.searchCreditCardRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchCreditCardFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchCreditCardsEffect$).toBeObservable(expected);
    });
  });

  // TODO Fix -> TypeError: Cannot call a class as a function
  xdescribe('downloadCreditCardEffect', () => {
    it('should open download dialog when downloadCreditCardRequest is called', () => {
      scheduler.run(helpers => {
        const action = fromActions.downloadCreditCardRequest();
        spyOn(dialogService, 'open');

        actions$ = helpers.hot('-a', { a: action });

        effects.downloadCreditCardEffect$.subscribe();
      });

      expect(dialogService.open).toHaveBeenCalledWith(DownloadFileComponent, {
        data: {
          title: 'BUTTON.DEFAULT.DOWNLOAD',
          footerButtonsType: FooterButton.Download,
          downloadQuery: {
            ...defaultQuery,
            filterBy: {
              ...defaultQuery?.filterBy,
              bspId: fromStub.userBspId
            }
          }
        },
        apiService: service
      });
    });
  });

  describe('openAdd', () => {
    it('should call changesDialogService.open', fakeAsync(() => {
      (effects as any).changesDialogService.open = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.openAdd());

      effects.openAdd$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).changesDialogService.open).toHaveBeenCalledWith();
    }));
  });

  describe('addCreditCard', () => {
    it('should call changesDialogService.create', fakeAsync(() => {
      (effects as any).changesDialogService.create = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.addCreditCard({ value: { name: 'name' } } as any));

      effects.addCreditCard$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).changesDialogService.create).toHaveBeenCalledWith({ name: 'name' });
    }));

    it('should map to addCreditCardSuccess', fakeAsync(() => {
      (effects as any).changesDialogService.create = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.addCreditCard({ value: { name: 'name' } } as any));
      let actualAction;

      effects.addCreditCard$.pipe(take(1)).subscribe(action => (actualAction = action));
      tick();

      expect(actualAction).toEqual(fromActions.addCreditCardSuccess());
    }));

    it('should map to addCreditCardError when error is occured', fakeAsync(() => {
      (effects as any).changesDialogService.create = jasmine.createSpy().and.returnValue(throwError({ status: 500 }));
      actions$ = of(fromActions.addCreditCard({ value: { name: 'name' } } as any));
      let actualAction;

      effects.addCreditCard$
        .pipe(
          take(1),
          catchError(err => of(err))
        )
        .subscribe(action => (actualAction = action));
      tick();

      expect(actualAction).toEqual(fromActions.addCreditCardError({ error: { status: 500 } }));
    }));
  });

  describe('addCreditCardSuccess', () => {
    beforeEach(() => {
      mockStore.overrideSelector(fromSelectors.selectCreditCardQuery, { filter: {} } as any);
    });

    it('should call dialogService.close', fakeAsync(() => {
      (effects as any).dialogService.close = jasmine.createSpy();
      actions$ = of(fromActions.addCreditCardSuccess());

      effects.addCreditCardSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).dialogService.close).toHaveBeenCalled();
    }));

    it('should call translationService.translate with edit message', fakeAsync(() => {
      (effects as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      actions$ = of(fromActions.addCreditCardSuccess());

      effects.addCreditCardSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).translationService.translate).toHaveBeenCalledWith(translations.create.details);
    }));

    it('should call notificationService.showSuccess with success message', fakeAsync(() => {
      (effects as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      (effects as any).notificationService.showSuccess = jasmine.createSpy();
      actions$ = of(fromActions.addCreditCardSuccess());

      effects.addCreditCardSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).notificationService.showSuccess).toHaveBeenCalledWith('test');
    }));

    it('should dispatch searchCreditCardRequest with query', fakeAsync(() => {
      (effects as any).store.dispatch = jasmine.createSpy();
      actions$ = of(fromActions.addCreditCardSuccess());

      effects.addCreditCardSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).store.dispatch).toHaveBeenCalledWith(
        fromActions.searchCreditCardRequest({ payload: { query: { filter: {} } } } as any)
      );
    }));
  });

  describe('openApplyChanges', () => {
    it('should call changesDialogService.open', fakeAsync(() => {
      (effects as any).changesDialogService.open = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.openApplyChanges({ value: { id: '5' } } as any));

      effects.openApplyChanges$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).changesDialogService.open).toHaveBeenCalledWith({ id: '5' });
    }));
  });

  describe('applyChanges', () => {
    it('should call changesDialogService.create', fakeAsync(() => {
      (effects as any).changesDialogService.edit = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.applyChanges({ value: { id: '5' } } as any));

      effects.applyChanges$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).changesDialogService.edit).toHaveBeenCalledWith({ id: '5' });
    }));

    it('should map to applyChangesSuccess', fakeAsync(() => {
      (effects as any).changesDialogService.edit = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.applyChanges({ value: { id: '5' } } as any));
      let actualAction;

      effects.applyChanges$.pipe(take(1)).subscribe(action => (actualAction = action));
      tick();

      expect(actualAction).toEqual(fromActions.applyChangesSuccess({ value: { id: '5' } } as any));
    }));

    it('should map to applyChangesError when error is occured', fakeAsync(() => {
      (effects as any).changesDialogService.edit = jasmine.createSpy().and.returnValue(throwError({ status: 500 }));
      actions$ = of(fromActions.applyChanges({ value: { id: '5' } } as any));
      let actualAction;

      effects.applyChanges$
        .pipe(
          take(1),
          catchError(err => of(err))
        )
        .subscribe(action => (actualAction = action));
      tick();

      expect(actualAction).toEqual(fromActions.applyChangesError({ error: { status: 500 } }));
    }));
  });

  describe('applyChangesSuccess', () => {
    beforeEach(() => {
      mockStore.overrideSelector(fromSelectors.selectCreditCardQuery, { filter: {} } as any);
    });

    it('should call dialogService.close', fakeAsync(() => {
      (effects as any).dialogService.close = jasmine.createSpy();
      actions$ = of(fromActions.applyChangesSuccess({ value: { id: '5' } } as any));

      effects.applyChangesSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).dialogService.close).toHaveBeenCalled();
    }));

    it('should call translationService.translate with edit message', fakeAsync(() => {
      (effects as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      actions$ = of(fromActions.applyChangesSuccess({ value: { id: '5' } } as any));

      effects.applyChangesSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).translationService.translate).toHaveBeenCalledWith(translations.edit.details);
    }));

    it('should call notificationService.showSuccess with success message', fakeAsync(() => {
      (effects as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      (effects as any).notificationService.showSuccess = jasmine.createSpy();
      actions$ = of(fromActions.applyChangesSuccess({ value: { id: '5' } } as any));

      effects.applyChangesSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).notificationService.showSuccess).toHaveBeenCalledWith('test');
    }));

    it('should dispatch searchCreditCardRequest with query', fakeAsync(() => {
      (effects as any).store.dispatch = jasmine.createSpy();
      actions$ = of(fromActions.applyChangesSuccess({ value: { id: '5' } } as any));

      effects.applyChangesSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).store.dispatch).toHaveBeenCalledWith(
        fromActions.searchCreditCardRequest({ payload: { query: { filter: {} } } } as any)
      );
    }));
  });

  describe('openRemove', () => {
    it('should call changesDialogService.open', fakeAsync(() => {
      (effects as any).changesDialogService.open = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.openRemove({ value: { id: '5' } } as any));

      effects.openRemove$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).changesDialogService.open).toHaveBeenCalledWith({ id: '5' }, true);
    }));
  });

  describe('remove', () => {
    it('should call changesDialogService.create', fakeAsync(() => {
      (effects as any).changesDialogService.delete = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.remove({ value: { id: '5' } } as any));

      effects.remove$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).changesDialogService.delete).toHaveBeenCalledWith({ id: '5' });
    }));

    it('should map to removeSuccess', fakeAsync(() => {
      (effects as any).changesDialogService.delete = jasmine.createSpy().and.returnValue(of({}));
      actions$ = of(fromActions.remove({ value: { id: '5' } } as any));
      let actualAction;

      effects.remove$.pipe(take(1)).subscribe(action => (actualAction = action));
      tick();

      expect(actualAction).toEqual(fromActions.removeSuccess({ value: { id: '5' } } as any));
    }));

    it('should map to removeError when error is occured', fakeAsync(() => {
      (effects as any).changesDialogService.delete = jasmine.createSpy().and.returnValue(throwError({ status: 500 }));
      actions$ = of(fromActions.remove({ value: { id: '5' } } as any));
      let actualAction;

      effects.remove$
        .pipe(
          take(1),
          catchError(err => of(err))
        )
        .subscribe(action => (actualAction = action));
      tick();

      expect(actualAction).toEqual(fromActions.removeError({ error: { status: 500 } }));
    }));
  });

  describe('removeSuccess', () => {
    beforeEach(() => {
      mockStore.overrideSelector(fromSelectors.selectCreditCardQuery, { filter: {} } as any);
    });

    it('should call dialogService.close', fakeAsync(() => {
      (effects as any).dialogService.close = jasmine.createSpy();
      actions$ = of(fromActions.removeSuccess({ value: { id: '5' } } as any));

      effects.removeSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).dialogService.close).toHaveBeenCalled();
    }));

    it('should call translationService.translate with edit message', fakeAsync(() => {
      (effects as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      actions$ = of(fromActions.removeSuccess({ value: { id: '5' } } as any));

      effects.removeSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).translationService.translate).toHaveBeenCalledWith(translations.delete.details);
    }));

    it('should call notificationService.showSuccess with success message', fakeAsync(() => {
      (effects as any).translationService.translate = jasmine.createSpy().and.returnValue('test');
      (effects as any).notificationService.showSuccess = jasmine.createSpy();
      actions$ = of(fromActions.removeSuccess({ value: { id: '5' } } as any));

      effects.removeSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).notificationService.showSuccess).toHaveBeenCalledWith('test');
    }));

    it('should dispatch searchCreditCardRequest with query', fakeAsync(() => {
      (effects as any).store.dispatch = jasmine.createSpy();
      actions$ = of(fromActions.removeSuccess({ value: { id: '5' } } as any));

      effects.removeSuccess$.pipe(take(1)).subscribe();
      tick();

      expect((effects as any).store.dispatch).toHaveBeenCalledWith(
        fromActions.searchCreditCardRequest({ payload: { query: { filter: {} } } } as any)
      );
    }));
  });
});
