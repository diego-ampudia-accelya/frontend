import { createFeatureSelector, createSelector } from '@ngrx/store';

import { creditCardFeatureKey, CreditCardState } from './credit-card.state';

export const selectCreditCardStateFeature = createFeatureSelector<CreditCardState>(creditCardFeatureKey);

export const selectCreditCardData = createSelector(selectCreditCardStateFeature, state => state.data);
export const selectCreditCardQuery = createSelector(selectCreditCardStateFeature, state => state.query);
export const selectSearchCreditCardLoading = createSelector(selectCreditCardStateFeature, state => state.loading);
export const selectErrors = createSelector(selectCreditCardStateFeature, state => state.errors);

export const selectHasData = createSelector(selectCreditCardStateFeature, state =>
  Boolean(state.data && state.data.length)
);
