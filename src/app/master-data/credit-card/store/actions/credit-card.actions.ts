import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import { CreditCard, CreditCardFilters } from '../../models/credit-card.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchCreditCardRequest = createAction(
  '[Credit Card] Search Credit Card Request',
  props<{ payload: { query?: DataQuery<CreditCardFilters> } }>()
);
export const searchCreditCardSuccess = createAction(
  '[Credit Card] Search Credit Card Success',
  props<{ payload: { pagedData: PagedData<CreditCard> } }>()
);
export const searchCreditCardFailure = createAction(
  '[Credit Card] Search Credit Card Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const downloadCreditCardRequest = createAction('[Credit Card] Download Credit Card Request');

// Create Credit Card
export const openAdd = createAction('[Credit Card] Open Add');
export const addCreditCard = createAction('[Credit Card] Add Credit Card', props<{ value: CreditCardFilters }>());
export const addCreditCardSuccess = createAction('[Credit Card] Add Credit Card Success');
export const addCreditCardError = createAction('[Credit Card] Add Credit Card Error', props<{ error }>());

// Edit Credit Card
export const openApplyChanges = createAction('[Credit Card] Open Apply Changes', props<{ value: CreditCard }>());
export const applyChanges = createAction('[Credit Card] Apply Changes', props<{ value: CreditCard }>());
export const applyChangesSuccess = createAction('[Credit Card] Apply Changes Success', props<{ value: CreditCard }>());
export const applyChangesError = createAction('[Credit Card] Apply Changes Error', props<{ error }>());

// Remove Credit Card
export const openRemove = createAction('[Credit Card] Open Remove', props<{ value: CreditCard }>());
export const remove = createAction('[Credit Card] Remove', props<{ value: CreditCard }>());
export const removeSuccess = createAction('[Credit Card] Remove Success', props<{ value: CreditCard }>());
export const removeError = createAction('[Credit Card] Remove Error', props<{ error }>());
