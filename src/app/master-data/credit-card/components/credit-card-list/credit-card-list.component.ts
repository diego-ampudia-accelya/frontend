import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';

import { CREDIT_CARDS_COLUMNS } from '../../contants/credit-card.constants';
import { CreditCard, CreditCardFilters } from '../../models/credit-card.models';
import { CreditCardFilterFormatterService } from '../../services/credit-card-filter-formatter.service';
import { CreditCardActions } from '../../store/actions';
import { CreditCardStoreFacadeService } from '../../store/credit-card-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-credit-card-list',
  templateUrl: './credit-card-list.component.html'
})
export class CreditCardListComponent implements OnInit {
  public columns: TableColumn[] = CREDIT_CARDS_COLUMNS.COLUMNS;
  public actions: Array<{ action: GridTableActionType; disabled: boolean }> = [
    {
      action: GridTableActionType.Edit,
      disabled: !this.permissionService.hasPermission(Permissions.updateCreditCards)
    },
    {
      action: GridTableActionType.Delete,
      disabled: !this.permissionService.hasPermission(Permissions.deleteCreditCards)
    }
  ];

  public luhnExceptionOptions = [
    {
      value: true,
      label: this.translationService.translate('LIST.MASTER_DATA.creditCards.filters.luhnException.option.yes')
    },
    {
      value: false,
      label: this.translationService.translate('LIST.MASTER_DATA.creditCards.filters.luhnException.option.no')
    }
  ];
  public customLabels = { create: 'LIST.MASTER_DATA.creditCards.createCreditCardLabel' };

  public data$: Observable<CreditCard[]> = this.creditCardStoreFacadeService.selectors.data$;
  public query$: Observable<DataQuery<CreditCardFilters>> = this.creditCardStoreFacadeService.selectors.query$;
  public loading$: Observable<boolean> = this.creditCardStoreFacadeService.loading$;
  public hasData$: Observable<boolean> = this.creditCardStoreFacadeService.selectors.hasData$;

  public filterFormGroup: FormGroup;

  public hasCreating = this.permissionService.hasPermission(Permissions.createCreditCards);

  constructor(
    public readonly creditCardFilterFormatterService: CreditCardFilterFormatterService,
    private readonly creditCardStoreFacadeService: CreditCardStoreFacadeService,
    private readonly translationService: L10nTranslationService,
    private readonly permissionService: PermissionsService,
    private readonly store: Store<AppState>
  ) {}

  public ngOnInit(): void {
    this.filterFormGroup = new FormGroup({
      abbreviation: new FormControl(null, []),
      name: new FormControl(null, []),
      prefix: new FormControl(null, []),
      longcode: new FormControl(null, []),
      address: new FormControl(null, []),
      locality: new FormControl(null, []),
      city: new FormControl(null, []),
      zip: new FormControl(null, []),
      country: new FormControl(null, []),
      luhnException: new FormControl(null, [])
    });
  }

  public onQueryChanged(query: DataQuery<CreditCardFilters>): void {
    this.creditCardStoreFacadeService.actions.searchCreditCardRequest(query);
  }

  public onDownload(): void {
    this.creditCardStoreFacadeService.actions.downloadCreditCardRequest();
  }

  public createCreditCard(): void {
    this.store.dispatch(CreditCardActions.openAdd());
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: CreditCard }): void {
    if (action.actionType === GridTableActionType.Edit) {
      this.store.dispatch(CreditCardActions.openApplyChanges({ value: row }));

      return;
    }

    if (action.actionType === GridTableActionType.Delete) {
      this.store.dispatch(CreditCardActions.openRemove({ value: row }));
    }
  }
}
