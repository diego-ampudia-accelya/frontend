import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule, MockProvider } from 'ng-mocks';
import { of } from 'rxjs';

import { CreditCardFilterFormatterService } from '../../services/credit-card-filter-formatter.service';
import { CreditCardActions } from '../../store/actions';
import { CreditCardStoreFacadeService } from '../../store/credit-card-store-facade.service';
import { CreditCardListComponent } from './credit-card-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { ListViewComponent } from '~app/shared/components/list-view';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

describe('CreditCardListComponent', () => {
  let component: CreditCardListComponent;
  let fixture: ComponentFixture<CreditCardListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule)],
      declarations: [
        CreditCardListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: CreditCardFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: CreditCardStoreFacadeService,
          useValue: {
            actions: {
              searchCreditCardRequest: () => {},
              downloadCreditCardRequest: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              bspId$: of(null),
              hasData$: of(null)
            },
            loading: {
              searchCreditCardRequest$: of(null)
            }
          }
        },
        provideMockStore({}),
        MockProvider(PermissionsService)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('createCreditCard', () => {
    it('should dispatch openAdd action', () => {
      (component as any).store.dispatch = jasmine.createSpy();

      component.createCreditCard();

      expect((component as any).store.dispatch).toHaveBeenCalledWith(CreditCardActions.openAdd());
    });
  });

  describe('onActionClick', () => {
    it('should dispatch openApplyChanges when action is Edit', () => {
      (component as any).store.dispatch = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Edit }, row: { id: '5' } } as any);

      expect((component as any).store.dispatch).toHaveBeenCalledWith(
        CreditCardActions.openApplyChanges({ value: { id: '5' } } as any)
      );
    });

    it('should dispatch remove when action is Delete', () => {
      (component as any).store.dispatch = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Delete }, row: { id: '5' } } as any);

      expect((component as any).store.dispatch).toHaveBeenCalledWith(
        CreditCardActions.openRemove({ value: { id: '5' } } as any)
      );
    });
  });
});
