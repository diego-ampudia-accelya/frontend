import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MockProviders } from 'ng-mocks';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { CreditCardChangesDialogComponent } from '../credit-card-changes-dialog.component';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ChangesDialogService } from './changes-dialog.service';
import { DialogService } from '~app/shared/components';
import { CreditCardService } from '~app/master-data/credit-card/services/credit-card.service';

describe('ChangesDialogService', () => {
  let service: ChangesDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangesDialogService, MockProviders(DialogService, ChangesDialogConfigService, CreditCardService)]
    });
    service = TestBed.inject(ChangesDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('open', () => {
    beforeEach(() => {
      (service as any).dialogService.open = jasmine.createSpy().and.returnValue(of({ instance: {} }));
    });

    it('should call configBuider.open with empty item and false', fakeAsync(() => {
      (service as any).configBuilder.build = jasmine.createSpy().and.returnValue({});

      service.open().pipe(take(1)).subscribe();
      tick();

      expect((service as any).configBuilder.build).toHaveBeenCalledWith(undefined, false);
    }));

    it('should call configBuider.open with existing item and false', fakeAsync(() => {
      (service as any).configBuilder.build = jasmine.createSpy().and.returnValue({});

      service
        .open({ id: '5' } as any)
        .pipe(take(1))
        .subscribe();
      tick();

      expect((service as any).configBuilder.build).toHaveBeenCalledWith({ id: '5' }, false);
    }));

    it('should call configBuider.open with existing item and true', fakeAsync(() => {
      (service as any).configBuilder.build = jasmine.createSpy().and.returnValue({});

      service
        .open({ id: '5' } as any, true)
        .pipe(take(1))
        .subscribe();
      tick();

      expect((service as any).configBuilder.build).toHaveBeenCalledWith({ id: '5' }, true);
    }));

    it('should call dialogService.open with Edit configuration', fakeAsync(() => {
      (service as any).configBuilder.build = jasmine.createSpy().and.returnValue({ data: { title: 'create' } });

      service.open().pipe(take(1)).subscribe();
      tick();

      expect((service as any).dialogService.open).toHaveBeenCalledWith(CreditCardChangesDialogComponent, {
        data: { title: 'create' }
      });
    }));

    it('should return result from dialogService.open', fakeAsync(() => {
      let res;
      (service as any).configBuilder.build = jasmine.createSpy().and.returnValue({ data: { title: 'create' } });

      service
        .open()
        .pipe(take(1))
        .subscribe(data => (res = data));
      tick();

      expect(res).toEqual({ instance: {} });
    }));
  });

  describe('create', () => {
    it('should call dataService.create with item', fakeAsync(() => {
      (service as any).dataService.create = jasmine.createSpy().and.returnValue(of({ id: '50', name: 'test' }));

      service
        .create({ name: 'test' } as any)
        .pipe(take(1))
        .subscribe();

      expect((service as any).dataService.create).toHaveBeenCalledWith({ name: 'test' });
    }));

    it('should return result from dataService.create', fakeAsync(() => {
      let res;
      (service as any).dataService.create = jasmine.createSpy().and.returnValue(of({ id: '50', name: 'test' }));

      service
        .create({ name: 'test' } as any)
        .pipe(take(1))
        .subscribe(data => (res = data));

      expect(res).toEqual({ id: '50', name: 'test' });
    }));
  });

  describe('edit', () => {
    it('should call dataService.save with item', fakeAsync(() => {
      (service as any).dataService.save = jasmine.createSpy().and.returnValue(of({ id: '50', name: 'test-2' }));

      service
        .edit({ id: '50', name: 'test' } as any)
        .pipe(take(1))
        .subscribe();

      expect((service as any).dataService.save).toHaveBeenCalledWith({ id: '50', name: 'test' });
    }));

    it('should return result from dataService.save', fakeAsync(() => {
      let res;
      (service as any).dataService.save = jasmine.createSpy().and.returnValue(of({ id: '50', name: 'test-2' }));

      service
        .edit({ id: '50', name: 'test' } as any)
        .pipe(take(1))
        .subscribe(data => (res = data));

      expect(res).toEqual({ id: '50', name: 'test-2' });
    }));
  });

  describe('delete', () => {
    it('should call dataService.save with item', fakeAsync(() => {
      (service as any).dataService.delete = jasmine.createSpy().and.returnValue(of(null));

      service
        .delete({ id: '50', name: 'test' } as any)
        .pipe(take(1))
        .subscribe();

      expect((service as any).dataService.delete).toHaveBeenCalledWith({ id: '50', name: 'test' });
    }));
  });
});
