import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { translations } from '../translations';
import {
  CreditCardDialogConfig,
  CreditCardDialogOption,
  CreditCardDialogOptions
} from '~app/master-data/credit-card/models/credit-card-dialog';
import { CreditCard } from '~app/master-data/credit-card/models/credit-card.models';
import { FooterButton } from '~app/shared/components';

const CREATE_TRANSLATION_KEY = 'SAVE';
const MODIFY_TRANSLATION_KEY = 'MODIFY';
const DELETE_TRANSLATION_KEY = 'DELETE';

@Injectable()
export class ChangesDialogConfigService {
  private dialogOptions: CreditCardDialogOptions = {
    [FooterButton.Create]: {
      questionTitle: translations.create.title,
      footerButton: [{ type: FooterButton.Create, title: this.getButtonTitle(CREATE_TRANSLATION_KEY) }]
    },
    [FooterButton.Modify]: {
      questionTitle: translations.edit.title,
      footerButton: [{ type: FooterButton.Modify, title: this.getButtonTitle(MODIFY_TRANSLATION_KEY) }]
    },
    [FooterButton.Delete]: {
      questionTitle: translations.delete.title,
      footerButton: [{ type: FooterButton.Delete, title: this.getButtonTitle(DELETE_TRANSLATION_KEY) }]
    }
  };

  constructor(private translation: L10nTranslationService) {}

  public build(item?: CreditCard, isDelete = false): CreditCardDialogConfig {
    const dialogType: FooterButton = this.getDialogType(item, isDelete);
    const options: CreditCardDialogOption = this.dialogOptions[dialogType];

    return {
      data: {
        title: options.questionTitle,
        footerButtonsType: options.footerButton
      },
      item,
      isDelete
    };
  }

  private getButtonTitle(key: string): string {
    return this.translation.translate(`BUTTON.DEFAULT.${key}`);
  }

  private getDialogType(item?: CreditCard, isDelete = false): FooterButton {
    if (!item) {
      return FooterButton.Create;
    }

    if (!isDelete) {
      return FooterButton.Modify;
    }

    return FooterButton.Delete;
  }
}
