import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CreditCardChangesDialogComponent } from '../credit-card-changes-dialog.component';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { CreditCardDialogConfig } from '~app/master-data/credit-card/models/credit-card-dialog';
import { CreditCard, CreditCardFilters } from '~app/master-data/credit-card/models/credit-card.models';
import { CreditCardService } from '~app/master-data/credit-card/services/credit-card.service';
import { DialogService } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private dataService: CreditCardService
  ) {}

  public open(item?: CreditCard, isDelete = false): Observable<any> {
    const dialogConfig: CreditCardDialogConfig = this.configBuilder.build(item, isDelete);

    return this.dialogService.open(CreditCardChangesDialogComponent, dialogConfig);
  }

  public create(item: CreditCardFilters): Observable<CreditCard> {
    return this.dataService.create(item);
  }

  public edit(item: CreditCard): Observable<CreditCard> {
    return this.dataService.save(item);
  }

  public delete(item: CreditCard): Observable<CreditCard> {
    return this.dataService.delete(item);
  }
}
