import { TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockProviders } from 'ng-mocks';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { FooterButton } from '~app/shared/components';

describe('ChangesDialogConfigService', () => {
  let service: ChangesDialogConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangesDialogConfigService, MockProviders(L10nTranslationService)]
    });
    service = TestBed.inject(ChangesDialogConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('build', () => {
    beforeEach(() => {
      (service as any).getDialogType = jasmine.createSpy().and.returnValue(FooterButton.Modify);
    });

    it('should call getDialogType with existing item and isDisabled', () => {
      service.build({ id: 5 } as any);

      expect((service as any).getDialogType).toHaveBeenCalledWith({ id: 5 }, false);
    });

    it('should call getDialogType with empty item and isDisabled', () => {
      service.build();

      expect((service as any).getDialogType).toHaveBeenCalledWith(undefined, false);
    });

    it('should return result with proper data and Modify button type', () => {
      (service as any).dialogOptions = {
        [FooterButton.Modify]: {
          questionTitle: 'Modify',
          footerButton: [{ type: FooterButton.Modify, title: 'test-modify-button-title' }]
        }
      };
      const expectedRes = {
        data: {
          title: 'Modify',
          footerButtonsType: [{ type: FooterButton.Modify, title: 'test-modify-button-title' }]
        },
        item: { id: '5' },
        isDelete: false
      } as any;

      const res = service.build({ id: '5' } as any);

      expect(res).toEqual(expectedRes);
    });
  });

  describe('getButtonTitle', () => {
    beforeEach(() => {
      (service as any).translation.translate = jasmine.createSpy().and.returnValue('test');
    });

    it('should call translate with key', () => {
      (service as any).getButtonTitle('CREATE');

      expect((service as any).translation.translate).toHaveBeenCalledWith('BUTTON.DEFAULT.CREATE');
    });

    it('should return value from translate', () => {
      const res = (service as any).getButtonTitle('CREATE');

      expect(res).toBe('test');
    });
  });

  describe('getDialogType', () => {
    it('should return Create type when there is no item', () => {
      const result = (service as any).getDialogType();

      expect(result).toBe(FooterButton.Create);
    });

    it('should return Create type when item exists and isDelete is false', () => {
      const result = (service as any).getDialogType({ id: '5' }, false);

      expect(result).toBe(FooterButton.Modify);
    });

    it('should return Create type when item exists and isDelete is true', () => {
      const result = (service as any).getDialogType({ id: '5' }, true);

      expect(result).toBe(FooterButton.Delete);
    });
  });
});
