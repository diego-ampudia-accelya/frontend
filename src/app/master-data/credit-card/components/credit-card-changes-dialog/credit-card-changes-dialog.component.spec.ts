import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { MockComponents, MockProviders } from 'ng-mocks';
import { of, Subject } from 'rxjs';

import { CreditCardActions } from '../../store/actions';
import * as fromSelectors from '../../store/credit-card.selectors';
import { CreditCardChangesDialogComponent } from './credit-card-changes-dialog.component';
import { DialogConfig, FooterButton, InputComponent, SelectComponent } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TranslatePipeMock } from '~app/test';

const initialState = {};

describe('CreditCardChangesDialogComponent', () => {
  let component: CreditCardChangesDialogComponent;
  let fixture: ComponentFixture<CreditCardChangesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        CreditCardChangesDialogComponent,
        MockComponents(InputComponent, SelectComponent),
        TranslatePipeMock
      ],
      providers: [
        {
          provide: DialogConfig,
          useValue: {
            data: { buttons: [{ type: FooterButton.Create }], item: null }
          }
        },
        {
          provide: ReactiveSubject,
          useValue: {
            asObservable: of({})
          }
        },
        MockProviders(L10nTranslationService),
        FormBuilder,
        provideMockStore({
          initialState,
          selectors: [{ selector: fromSelectors.selectErrors, value: {} }]
        })
      ],
      imports: [ReactiveFormsModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardChangesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('initializeFormListeners', () => {
    const value = {
      abbreviation: 'abbr',
      name: 'name',
      prefix: 'pr',
      longcode: '11',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    };

    beforeEach(() => {
      (component as any).store.dispatch = jasmine.createSpy();
    });

    it('should dispatch addCreditCard action with form value', () => {
      component.form.setValue(value);
      (component as any).createButtonClick$ = new Subject();

      (component as any).initializeFormListeners();
      (component as any).createButtonClick$.next();

      expect((component as any).store.dispatch).toHaveBeenCalledWith(CreditCardActions.addCreditCard({ value }));
    });

    it('should dispatch applyChanges action with form value and id', () => {
      component.form.setValue(value);
      (component as any).updateButtonClick$ = new Subject();
      (component as any).item = { id: 'test-id' };

      (component as any).initializeFormListeners();
      (component as any).updateButtonClick$.next();

      expect((component as any).store.dispatch).toHaveBeenCalledWith(
        CreditCardActions.applyChanges({
          value: {
            ...value,
            id: 'test-id'
          }
        })
      );
    });

    it('should dispatch remove action with form value and id', () => {
      component.form.setValue(value);
      (component as any).deleteButtonClick$ = new Subject();
      (component as any).item = { id: 'test-id' };

      (component as any).initializeFormListeners();
      (component as any).deleteButtonClick$.next();

      expect((component as any).store.dispatch).toHaveBeenCalledWith(
        CreditCardActions.remove({
          value: {
            ...value,
            id: 'test-id'
          }
        })
      );
    });

    it('should call setFieldErrors when get error from store', () => {
      (component as any).createCreditCardError$ = new Subject();
      (component as any).setFieldErrors = jasmine.createSpy();

      (component as any).initializeFormListeners();
      (component as any).createCreditCardError$.next({ error: { code: 400 } });

      expect((component as any).setFieldErrors).toHaveBeenCalledWith({ code: 400 });
    });
  });

  describe('initializeSaveButton', () => {
    it('should set data to save button', () => {
      (component as any).saveButton = undefined;
      (component as any).item = null;

      (component as any).initializeSaveButton();

      expect((component as any).saveButton).toBeDefined();
    });

    it('should set isDisabled to create button', () => {
      (component as any).item = null;
      (component as any).saveButton = undefined;

      (component as any).initializeSaveButton();

      expect((component as any).saveButton.isDisabled).toBe(true);
    });
  });

  describe('initializeForm', () => {
    it('should set form controls with null values', () => {
      (component as any).item = null;
      component.form = null;
      const formExpected = {
        abbreviation: null,
        name: null,
        prefix: null,
        longcode: null,
        address: null,
        locality: null,
        city: null,
        zip: null,
        country: null,
        luhnException: null
      };

      (component as any).initializeForm();

      expect(component.form.value).toEqual(formExpected);
    });

    it('should set form controls with values from item', () => {
      component.form = null;
      const formExpected = {
        abbreviation: 'ES',
        name: 'test-name',
        prefix: 'CC',
        longcode: '11',
        address: 'test-address',
        locality: 'test-locality',
        city: 'test-city',
        zip: '123',
        country: 'test-country',
        luhnException: false
      };
      (component as any).item = { ...formExpected, id: 'test-id' };

      (component as any).initializeForm();

      expect(component.form.value).toEqual(formExpected);
    });
  });

  describe('setFieldErrors', () => {
    it('should set errors to prefix', () => {
      const error = {
        errorCode: 400,
        messages: [{ message: 'Already exist', messageParams: [{ name: 'fieldName', value: 'prefix' }] }]
      };
      (component as any).initializeForm();
      expect(component.form.get('prefix').getError('requestError')).not.toBeDefined();

      (component as any).setFieldErrors(error);

      expect(component.form.get('prefix').getError('requestError')).toEqual({ message: 'Already exist' });
    });
  });

  describe('subscribeToButtonStatus', () => {
    const validFormValue = {
      abbreviation: 'ES',
      name: 'test-name',
      prefix: 'CC',
      longcode: '11',
      address: null,
      locality: null,
      city: null,
      zip: null,
      country: null,
      luhnException: false
    };

    beforeEach(() => {
      (component as any).initializeForm();
      component.form.setValue(validFormValue);
      (component as any).setIsDisabledForSaveButton = jasmine.createSpy();
    });

    describe('when edit mode is false', () => {
      it('should call setIsDisabledForSaveButton with true when form is invalid', () => {
        (component as any).subscribeToButtonStatus();

        component.form.get('longcode').setValue('test');

        expect((component as any).setIsDisabledForSaveButton).toHaveBeenCalledWith(true);
      });

      it('should call setIsDisabledForSaveButton with false when fomr is valid', () => {
        (component as any).subscribeToButtonStatus();

        component.form.get('name').setValue('test');

        expect((component as any).setIsDisabledForSaveButton).toHaveBeenCalledWith(false);
      });
    });

    describe('when edit mode is true', () => {
      it('should call setIsDisabledForSaveButton with true when form is invalid', () => {
        (component as any).subscribeToButtonStatus();

        component.form.get('longcode').setValue('test');

        expect((component as any).setIsDisabledForSaveButton).toHaveBeenCalledWith(true);
      });

      it('should call setIsDisabledForSaveButton with false when form is valid and form has changes', () => {
        (component as any).subscribeToButtonStatus();
        (component as any).item = cloneDeep(validFormValue);

        component.form.get('name').setValue('test');

        expect((component as any).setIsDisabledForSaveButton).toHaveBeenCalledWith(false);
      });

      it('should call setIsDisabledForSaveButton with true when form is valid and form has no changes', () => {
        (component as any).subscribeToButtonStatus();
        (component as any).item = cloneDeep(validFormValue);

        component.form.get('name').setValue('');
        component.form.get('name').setValue('test-name');

        expect((component as any).setIsDisabledForSaveButton).toHaveBeenCalledWith(true);
      });
    });
  });
});
