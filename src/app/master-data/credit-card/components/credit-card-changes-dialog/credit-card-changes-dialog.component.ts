import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import isEqual from 'lodash/isEqual';
import omit from 'lodash/omit';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { CreditCard, CreditCardFilters } from '../../models/credit-card.models';
import * as fromSelectors from '../../store/credit-card.selectors';
import { CreditCardActions } from '~app/master-data/credit-card/store/actions';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, ResponseErrorBE } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { numericValidator } from '~app/shared/validators/numeric.validator';

const MIN_LONGCODE_VALUE = 0;
const MAX_LONGCODE_VALUE = 99;

const VALIDATION_ERROR_CODE = 400;

@Component({
  selector: 'bspl-credit-card-changes-dialog',
  templateUrl: './credit-card-changes-dialog.component.html',
  styleUrls: ['./credit-card-changes-dialog.component.scss']
})
export class CreditCardChangesDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;
  public luhnExceptionOptions: Array<DropdownOption<boolean>> = [
    {
      value: true,
      label: this.translationService.translate('LIST.MASTER_DATA.creditCards.filters.luhnException.option.yes')
    },
    {
      value: false,
      label: this.translationService.translate('LIST.MASTER_DATA.creditCards.filters.luhnException.option.no')
    }
  ];

  private item: CreditCard = this.config.item;
  public readonly isEditMode: boolean = !!this.item;
  public readonly isDeleteMode: boolean = this.config.isDelete;

  private saveButton: ModalAction;
  private formFactory: FormUtil;

  private destroy$ = new Subject();

  private updateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Modify),
    takeUntil(this.destroy$)
  );
  private createButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Create),
    takeUntil(this.destroy$)
  );
  private deleteButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Delete),
    takeUntil(this.destroy$)
  );
  private createCreditCardError$ = this.store.pipe(
    select(fromSelectors.selectErrors),
    filter(error => !!error),
    takeUntil(this.destroy$)
  );

  constructor(
    private config: DialogConfig,
    private translationService: L10nTranslationService,
    private reactiveSubject: ReactiveSubject,
    private formBuilder: FormBuilder,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.initializeForm();
    this.initializeSaveButton();
    this.initializeFormListeners();
  }

  public ngAfterViewInit(): void {
    this.subscribeToButtonStatus();
  }

  private initializeFormListeners(): void {
    this.createButtonClick$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(CreditCardActions.addCreditCard({ value: this.form.value }));
    });

    this.updateButtonClick$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      const value: CreditCard = { ...this.form.value, id: this.item.id };
      this.store.dispatch(CreditCardActions.applyChanges({ value }));
    });

    this.deleteButtonClick$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      const value: CreditCard = { ...this.form.value, id: this.item.id };
      this.store.dispatch(CreditCardActions.remove({ value }));
    });

    this.createCreditCardError$.subscribe(({ error }) => this.setFieldErrors(error));
  }

  private initializeSaveButton(): void {
    let footerButtonType: FooterButton = FooterButton.Create;

    if (this.isEditMode) {
      footerButtonType = FooterButton.Modify;
    }

    if (this.isDeleteMode) {
      footerButtonType = FooterButton.Delete;
    }

    this.saveButton = this.config.data.buttons.find(button => button.type === footerButtonType);

    this.setIsDisabledForSaveButton(true);
  }

  private initializeForm(): void {
    this.form = this.formFactory.createGroup<CreditCardFilters>({
      abbreviation: new FormControl(null, [Validators.required, Validators.maxLength(2)]),
      name: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      prefix: new FormControl(null, [Validators.required, Validators.maxLength(6)]),
      longcode: new FormControl(null, [
        Validators.required,
        numericValidator,
        Validators.min(MIN_LONGCODE_VALUE),
        Validators.max(MAX_LONGCODE_VALUE)
      ]),
      address: new FormControl(null, [Validators.maxLength(30)]),
      locality: new FormControl(null, [Validators.maxLength(30)]),
      city: new FormControl(null, [Validators.maxLength(30)]),
      zip: new FormControl(null, [Validators.maxLength(10)]),
      country: new FormControl(null, [Validators.maxLength(26)]),
      luhnException: new FormControl(null, [Validators.required])
    });

    if (this.item) {
      this.form.patchValue(omit(this.item, 'id'));
    }
  }

  private setFieldErrors(error: ResponseErrorBE): void {
    if (error?.errorCode !== VALIDATION_ERROR_CODE || !error?.messages?.length) {
      return;
    }

    // Validation error
    error.messages.forEach(({ message, messageParams }) => {
      const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
      const invalidControl = this.form.get(controlName);
      if (invalidControl) {
        invalidControl.setErrors({ requestError: { message } });
        FormUtil.showControlState(invalidControl);
      }
    });
  }

  private subscribeToButtonStatus(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const isFormInvalid = status !== 'VALID';

      if (!this.isEditMode) {
        this.setIsDisabledForSaveButton(isFormInvalid);
      } else {
        const hasNoChangesInForm = isEqual(this.form.value, omit(this.item, 'id'));
        this.setIsDisabledForSaveButton(isFormInvalid || hasNoChangesInForm);
      }
    });
  }

  private setIsDisabledForSaveButton(isDisabled: boolean): void {
    if (this.saveButton && !this.isDeleteMode) {
      this.saveButton.isDisabled = isDisabled;
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
