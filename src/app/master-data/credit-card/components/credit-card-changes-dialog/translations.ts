export const translations = {
  delete: {
    title: 'LIST.MASTER_DATA.creditCards.dialog.delete.title',
    details: 'LIST.MASTER_DATA.creditCards.dialog.delete.message'
  },
  edit: {
    title: 'LIST.MASTER_DATA.creditCards.dialog.edit.title',
    details: 'LIST.MASTER_DATA.creditCards.dialog.edit.message'
  },
  create: {
    title: 'LIST.MASTER_DATA.creditCards.dialog.create.title',
    details: 'LIST.MASTER_DATA.creditCards.dialog.create.message'
  }
};
