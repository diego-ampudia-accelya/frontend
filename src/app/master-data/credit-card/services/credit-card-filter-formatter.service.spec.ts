/* eslint-disable @typescript-eslint/no-unused-vars */

import { inject, TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { CreditCardFilterFormatterService } from './credit-card-filter-formatter.service';

describe('Service: CreditCardFilterFormatter', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CreditCardFilterFormatterService,
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        }
      ]
    });
  });

  it('should be created', inject([CreditCardFilterFormatterService], (service: CreditCardFilterFormatterService) => {
    expect(service).toBeTruthy();
  }));
});
