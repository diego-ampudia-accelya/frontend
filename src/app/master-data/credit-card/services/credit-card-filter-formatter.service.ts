import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { CreditCardFilters } from '../models/credit-card.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';

type CreditCardMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class CreditCardFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: CreditCardFilters) {
    const filterMappers: Partial<CreditCardMapper<CreditCardFilters>> = {
      abbreviation: abbreviation => `${this.translate('abbreviation')} - ${abbreviation}`,
      name: name => `${this.translate('name')} - ${name}`,
      prefix: prefix => `${this.translate('prefix')} - ${prefix}`,
      longcode: longcode => `${this.translate('longcode')} - ${longcode}`,
      address: address => `${this.translate('address')} - ${address}`,
      locality: locality => `${this.translate('locality')} - ${locality}`,
      city: city => `${this.translate('city')} - ${city}`,
      zip: zip => `${this.translate('zip')} - ${zip}`,
      country: country => `${this.translate('country')} - ${country}`,
      luhnException: luhnException =>
        `${this.translate('luhnException')} - ${
          luhnException
            ? this.translationService.translate('LIST.MASTER_DATA.creditCards.filters.luhnException.option.yes')
            : this.translationService.translate('LIST.MASTER_DATA.creditCards.filters.luhnException.option.no')
        }`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`LIST.MASTER_DATA.creditCards.filters.${key}.label`);
  }
}
