import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { CreditCard, CreditCardFilters } from '../models/credit-card.models';
import { CreditCardStoreFacadeService } from '../store/credit-card-store-facade.service';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class CreditCardService implements Queryable<CreditCard> {
  private bspId: number;
  private baseUrlFunc = (bspId: number) =>
    `${this.appConfiguration.baseApiPath}/bsp-management/bsps/${bspId}/creditcards`;
  private get baseUrl(): string {
    return this.baseUrlFunc(this.bspId);
  }

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private creditCardStoreFacadeService: CreditCardStoreFacadeService
  ) {
    this.creditCardStoreFacadeService.selectors.bspId$.pipe(first()).subscribe(bspId => (this.bspId = bspId));
  }

  public find(query: DataQuery<CreditCardFilters>): Observable<PagedData<CreditCard>> {
    const requestQuery = RequestQuery.fromDataQuery(query);

    return this.http.get<PagedData<CreditCard>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<CreditCardFilters>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = RequestQuery.fromDataQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public create(item: CreditCardFilters): Observable<CreditCard> {
    return this.http.post<CreditCard>(this.baseUrl, item);
  }

  public save(item: CreditCard): Observable<CreditCard> {
    const url = `${this.baseUrl}/${item.id}`;

    return this.http.put<CreditCard>(url, item);
  }

  public delete(item: CreditCard): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${item.id}`);
  }
}
