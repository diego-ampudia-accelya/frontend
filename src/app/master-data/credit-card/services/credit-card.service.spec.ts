/* eslint-disable @typescript-eslint/no-unused-vars */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { CreditCardStoreFacadeService } from '../store/credit-card-store-facade.service';

import { CreditCardService } from './credit-card.service';
import { AppConfigurationService } from '~app/shared/services';

describe('CreditCardService', () => {
  let service: CreditCardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CreditCardService,
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: ''
          }
        },
        {
          provide: CreditCardStoreFacadeService,
          useValue: {
            actions: {
              searchCreditCardRequest: () => {},
              downloadCreditCardRequest: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              bspId$: of(null),
              hasData$: of(null)
            },
            loading: of(null)
          }
        }
      ]
    });
    service = TestBed.inject(CreditCardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('create', () => {
    it('should call post with baseUrl and item', fakeAsync(() => {
      (service as any).http.post = jasmine.createSpy().and.returnValue(of({}));
      (service as any).baseUrlFunc = jasmine.createSpy().and.returnValue('test-url');

      service
        .create({ id: '5' } as any)
        .pipe(take(1))
        .subscribe();
      tick();

      expect((service as any).http.post).toHaveBeenCalledWith('test-url', { id: '5' });
    }));
  });

  describe('save', () => {
    it('should call put with url and item', fakeAsync(() => {
      (service as any).http.put = jasmine.createSpy().and.returnValue(of({}));
      (service as any).baseUrlFunc = jasmine.createSpy().and.returnValue('test-url');

      service
        .save({ id: '5' } as any)
        .pipe(take(1))
        .subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith('test-url/5', { id: '5' });
    }));
  });

  describe('delete', () => {
    it('should call delete with url', fakeAsync(() => {
      (service as any).http.delete = jasmine.createSpy().and.returnValue(of({}));
      (service as any).baseUrlFunc = jasmine.createSpy().and.returnValue('test-url');

      service
        .delete({ id: '5' } as any)
        .pipe(take(1))
        .subscribe();
      tick();

      expect((service as any).http.delete).toHaveBeenCalledWith('test-url/5');
    }));
  });
});
