export interface CreditCard {
  id: string;
  abbreviation: string;
  name: string;
  prefix: string;
  longcode: string;
  address: string;
  locality: string;
  city: string;
  zip: string;
  country: string;
  luhnException: boolean;
}

export interface CreditCardFilters {
  abbreviation: string;
  name: string;
  prefix: string;
  longcode: string;
  address: string;
  locality: string;
  city: string;
  zip: string;
  country: string;
  luhnException: boolean;
}
