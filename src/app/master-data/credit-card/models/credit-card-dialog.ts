import { CreditCard } from './credit-card.models';
import { DialogConfigData, FooterButton } from '~app/shared/components';

export interface CreditCardDialogConfig {
  data: DialogConfigData;
  item: CreditCard;
  isDelete: boolean;
}

export interface CreditCardDialogOptions {
  [key: string]: CreditCardDialogOption;
}

export interface CreditCardDialogOption {
  questionTitle: string;
  footerButton: CreditCardFooterButton[];
}

interface CreditCardFooterButton {
  type: FooterButton;
  title: string;
}
