import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, Observable, of } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { CompanyCalendarDialogComponent } from './company-calendar-dialog/company-calendar-dialog.component';
import { DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class CompanyCalendarGuard implements CanActivate {
  constructor(private dialogService: DialogService, private translationService: L10nTranslationService) {}

  public canActivate(next: ActivatedRouteSnapshot): Observable<boolean> {
    return this.openDialogObservable().pipe(
      switchMap(({ clickedBtn }: { clickedBtn: FooterButton }) => {
        if (clickedBtn === FooterButton.Next) {
          window.open('https://portal.iata.org/s/company-profile?tab=calendar', '_blank');
          this.dialogService.close();
        }

        if (clickedBtn === FooterButton.Cancel) {
          return of(false);
        }

        return EMPTY;
      }),
      first()
    );
  }

  private openDialogObservable(): Observable<any> {
    const title = this.translationService.translate('MASTER_DATA.companyCalendar.dialog.title');

    return this.dialogService.open(CompanyCalendarDialogComponent, {
      data: {
        title,
        mainButtonType: FooterButton.Next,
        footerButtonsType: [{ type: FooterButton.Next }]
      }
    });
  }
}
