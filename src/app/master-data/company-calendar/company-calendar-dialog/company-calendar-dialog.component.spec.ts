import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { CompanyCalendarDialogComponent } from './company-calendar-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { TranslatePipeMock } from '~app/test';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Continue,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel'
      },
      {
        title: 'BUTTON.DEFAULT.CONTINUE',
        buttonDesign: 'primary',
        type: 'continue',
        isDisabled: false
      }
    ]
  }
} as DialogConfig;

describe('CompanyCalendarDialogComponent', () => {
  let component: CompanyCalendarDialogComponent;
  let fixture: ComponentFixture<CompanyCalendarDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyCalendarDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [mockProvider(L10nTranslationService), FormBuilder, { provide: DialogConfig, useValue: mockConfig }]
    }).compileComponents();
  }));

  beforeEach(() => {
    // Initialize component
    fixture = TestBed.createComponent(CompanyCalendarDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
