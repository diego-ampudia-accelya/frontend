import { Component } from '@angular/core';

import { DialogConfig } from '~app/shared/components/dialog/dialog.config';

@Component({
  selector: 'bspl-company-calendar-dialog',
  templateUrl: './company-calendar-dialog.component.html',
  styleUrls: ['./company-calendar-dialog.component.scss']
})
export class CompanyCalendarDialogComponent {
  constructor(public config: DialogConfig) {}
}
