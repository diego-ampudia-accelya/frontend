import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { CompanyCalendarDialogComponent } from './company-calendar-dialog/company-calendar-dialog.component';
import { CompanyCalendarGuard } from './company-calendar.guard';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

const dialogConfig: DialogConfig = {
  data: {
    title: null,
    mainButtonType: 'next',
    footerButtonsType: [{ type: FooterButton.Next }]
  }
};

describe('CompanyCalendarGuard', () => {
  let guard: CompanyCalendarGuard;
  let dialogServiceSpy: SpyObject<DialogService>;
  let routerSpy: SpyObject<Router>;
  const routerSnapshotSpy: SpyObject<ActivatedRouteSnapshot> = createSpyObject(ActivatedRouteSnapshot);

  beforeEach(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    routerSpy = createSpyObject(Router);

    dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Cancel }));

    TestBed.configureTestingModule({
      providers: [
        CompanyCalendarGuard,
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: ActivatedRouteSnapshot, useValue: routerSnapshotSpy },
        mockProvider(L10nTranslationService)
      ]
    });

    guard = TestBed.inject(CompanyCalendarGuard);
  });

  it('should create', () => {
    expect(guard).toBeDefined();
  });

  it('should open dialog', () => {
    const openDialogObservableSpy = spyOn<any>(guard, 'openDialogObservable').and.callThrough();

    guard.canActivate(routerSnapshotSpy).subscribe();

    expect(openDialogObservableSpy).toHaveBeenCalled();
    expect(dialogServiceSpy.open).toHaveBeenCalledWith(CompanyCalendarDialogComponent, dialogConfig);
  });

  it('should not pass guard if dialog return cancel event', fakeAsync(() => {
    spyOn<any>(guard, 'openDialogObservable').and.returnValue(
      of({ clickedBtn: FooterButton.Cancel, contentComponentRef: null })
    );

    let result;

    guard.canActivate(routerSnapshotSpy).subscribe(data => (result = data));

    tick();

    expect(result).toBeFalsy();
  }));
});
