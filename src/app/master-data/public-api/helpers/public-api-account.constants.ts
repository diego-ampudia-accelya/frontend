import { PublicApiAccountActionType } from '../models/public-api-account-action-type.model';
import { FooterButton } from '~app/shared/components';

export const actionButtonMapper: { [key in PublicApiAccountActionType]: FooterButton } = {
  [PublicApiAccountActionType.Create]: FooterButton.Create,
  [PublicApiAccountActionType.Modify]: FooterButton.Modify,
  [PublicApiAccountActionType.Enable]: FooterButton.Enable,
  [PublicApiAccountActionType.Disable]: FooterButton.Disable,
  [PublicApiAccountActionType.Delete]: FooterButton.Delete
};
