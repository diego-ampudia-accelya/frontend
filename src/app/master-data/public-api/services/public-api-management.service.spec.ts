import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { PublicApiAccountActionType } from '../models/public-api-account-action-type.model';
import { PublicApiAccountView } from '../models/public-api-account-view.model';
import { PublicApiAccount } from '../models/public-api-account.model';

import { PublicApiManagementService } from './public-api-management.service';
import { AppConfigurationService } from '~app/shared/services';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('PublicApiManagementService', () => {
  let service: PublicApiManagementService;

  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        PublicApiManagementService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } }
      ]
    });

    service = TestBed.inject(PublicApiManagementService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should get public API accounts', fakeAsync(() => {
    let result: PublicApiAccountView[];

    const accountsMock: PublicApiAccount[] = [
      { id: 1, userInternalId: 1, username: 'user1', password: null, enabled: false }
    ];

    httpClientSpy.get.and.returnValue(of(accountsMock));
    service.getAccounts().subscribe(accounts => (result = accounts));

    expect(httpClientSpy.get).toHaveBeenCalledWith('/user-mgmt-integration/public-api/users');
    expect(result).toEqual([
      {
        id: 1,
        userInternalId: 1,
        username: 'user1',
        password: null,
        enabled: false,
        status: 'menu.masterData.configuration.publicApi.status.disabled',
        actions: [
          PublicApiAccountActionType.Enable,
          PublicApiAccountActionType.Modify,
          PublicApiAccountActionType.Delete
        ]
      }
    ]);
  }));

  it('should create public API accounts', fakeAsync(() => {
    let result: PublicApiAccount;

    const accountMock: PublicApiAccount = {
      id: 1,
      userInternalId: 1,
      username: 'user1',
      password: null,
      enabled: true
    };

    httpClientSpy.post.and.returnValue(of(accountMock));
    service
      .createAccount({ username: 'user1', password: null, enabled: true })
      .subscribe(account => (result = account));

    expect(httpClientSpy.post).toHaveBeenCalledWith('/user-mgmt-integration/public-api/users', {
      username: 'user1',
      password: null,
      enabled: true
    });
    expect(result).toEqual(accountMock);
  }));

  it('should update public API accounts', fakeAsync(() => {
    let result: PublicApiAccount;

    const accountMock: PublicApiAccount = {
      id: 1,
      userInternalId: 1,
      username: 'user11',
      password: null,
      enabled: true
    };

    httpClientSpy.put.and.returnValue(of(accountMock));
    service.updateAccount(1, { username: 'user11' }).subscribe(account => (result = account));

    expect(httpClientSpy.put).toHaveBeenCalledWith('/user-mgmt-integration/public-api/users/1', { username: 'user11' });
    expect(result).toEqual(accountMock);
  }));

  it('should delete public API accounts', fakeAsync(() => {
    httpClientSpy.delete.and.returnValue(of());
    service.deleteAccount(1).subscribe();

    expect(httpClientSpy.delete).toHaveBeenCalledWith('/user-mgmt-integration/public-api/users/1');
  }));
});
