import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';

import { PublicApiAccountActionDialogComponent } from '../components/public-api-account-action-dialog/public-api-account-action-dialog.component';
import { actionButtonMapper } from '../helpers/public-api-account.constants';
import { PublicApiAccountActionType } from '../models/public-api-account-action-type.model';
import { PublicApiAccountView } from '../models/public-api-account-view.model';
import { PublicApiAccount } from '../models/public-api-account.model';

import { PublicApiManagementService } from './public-api-management.service';
import { NotificationService } from '~app/shared/services';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';

const AIRLINE_CODE_DIGITS = 3;

@Injectable()
export class PublicApiAccountActionDialogService {
  private userAirlineCode$ = this.store.pipe(
    select(getUser),
    map(user => user.iataCode)
  );

  constructor(
    private dialogService: DialogService,
    private dataService: PublicApiManagementService,
    private notificationService: NotificationService,
    private store: Store<AppState>
  ) {}

  public open(action: PublicApiAccountActionType, account: PublicApiAccountView): Observable<FooterButton> {
    let dialogResponse: Observable<FooterButton>;

    const config: DialogConfig = {
      data: {
        title: `menu.masterData.configuration.publicApi.dialogs.${action}.title`,
        footerButtonsType: [{ type: actionButtonMapper[action], isDisabled: this.isDialogFormDisabled(action) }],
        action,
        ...(action !== PublicApiAccountActionType.Create && { account: this.extractUsernameAirlineCode(account) })
      }
    };

    switch (action) {
      case PublicApiAccountActionType.Create:
        dialogResponse = this.openCreationDialog(config);
        break;

      case PublicApiAccountActionType.Modify:
        dialogResponse = this.openModificationDialog(config);
        break;

      case PublicApiAccountActionType.Enable:
      case PublicApiAccountActionType.Disable:
        dialogResponse = this.openEnablementChangeDialog(config);
        break;

      case PublicApiAccountActionType.Delete:
        dialogResponse = this.openDeletionDialog(config);
        break;

      default:
        throw new Error('Action not included');
    }

    return dialogResponse;
  }

  private openCreationDialog(config: DialogConfig): Observable<FooterButton> {
    return this.dialogService.open(PublicApiAccountActionDialogComponent, config).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Create ? this.create(config, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private openModificationDialog(config: DialogConfig): Observable<FooterButton> {
    return this.dialogService.open(PublicApiAccountActionDialogComponent, config).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Modify ? this.modify(config, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private openEnablementChangeDialog(config: DialogConfig): Observable<FooterButton> {
    return this.dialogService.open(PublicApiAccountActionDialogComponent, config).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Enable || clickedBtn === FooterButton.Disable
          ? this.changeEnablement(config, contentComponentRef)
          : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private openDeletionDialog(config: DialogConfig): Observable<FooterButton> {
    return this.dialogService.open(PublicApiAccountActionDialogComponent, config).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Delete ? this.delete(config, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private create(config: DialogConfig, component: PublicApiAccountActionDialogComponent): Observable<FooterButton> {
    return defer(() => {
      const { username, password } = component.dialogForm.value;

      this.setLoading(config, true);

      return this.userAirlineCode$.pipe(
        switchMap(code => {
          const account: Partial<PublicApiAccount> = { username: code + username, password, enabled: true };

          return this.dataService.createAccount(account);
        }),
        tap(() => this.showSuccessNotification(component.actionType)),
        mapTo(FooterButton.Create),
        finalize(() => this.setLoading(config, false))
      );
    });
  }

  private modify(config: DialogConfig, component: PublicApiAccountActionDialogComponent): Observable<FooterButton> {
    return defer(() => {
      const account = component.account;
      const { username, password } = component.dialogForm.value;

      this.setLoading(config, true);

      return this.userAirlineCode$.pipe(
        switchMap(code => {
          const modifiedAccount: Partial<PublicApiAccount> = {
            ...(username && { username: code + username }),
            ...(password && { password })
          };

          return this.dataService.updateAccount(account.id, modifiedAccount);
        }),
        tap(() => this.showSuccessNotification(component.actionType)),
        mapTo(actionButtonMapper[component.actionType]),
        finalize(() => this.setLoading(config, false))
      );
    });
  }

  private changeEnablement(
    config: DialogConfig,
    component: PublicApiAccountActionDialogComponent
  ): Observable<FooterButton> {
    return defer(() => {
      const account = component.account;

      this.setLoading(config, true);

      return this.dataService.updateAccount(account.id, { enabled: !account.enabled }).pipe(
        tap(() => this.showSuccessNotification(component.actionType)),
        mapTo(actionButtonMapper[component.actionType]),
        finalize(() => this.setLoading(config, false))
      );
    });
  }

  private delete(config: DialogConfig, component: PublicApiAccountActionDialogComponent): Observable<FooterButton> {
    return defer(() => {
      const account = component.account;

      this.setLoading(config, true);

      return this.dataService.deleteAccount(account.id).pipe(
        tap(() => this.showSuccessNotification(component.actionType)),
        mapTo(actionButtonMapper[component.actionType]),
        finalize(() => this.setLoading(config, false))
      );
    });
  }

  private setLoading(config: DialogConfig, loading: boolean): void {
    config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));
  }

  private showSuccessNotification(action: PublicApiAccountActionType): void {
    this.notificationService.showSuccess(`menu.masterData.configuration.publicApi.dialogs.${action}.successMessage`);
  }

  private isDialogFormDisabled(action: PublicApiAccountActionType): boolean {
    return !(action === PublicApiAccountActionType.Enable || action === PublicApiAccountActionType.Disable);
  }

  private extractUsernameAirlineCode(account: PublicApiAccountView): PublicApiAccountView {
    return {
      ...account,
      username: account.username.substring(AIRLINE_CODE_DIGITS)
    };
  }
}
