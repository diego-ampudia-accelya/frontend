import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PublicApiAccountActionType } from '../models/public-api-account-action-type.model';
import { PublicApiAccountView } from '../models/public-api-account-view.model';
import { PublicApiAccount } from '../models/public-api-account.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class PublicApiManagementService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-mgmt-integration/public-api/users`;

  constructor(
    private httpClient: HttpClient,
    private appConfiguration: AppConfigurationService,
    private translationService: L10nTranslationService
  ) {}

  public getAccounts(): Observable<PublicApiAccountView[]> {
    return this.httpClient
      .get<PublicApiAccount[]>(this.baseUrl)
      .pipe(map(accounts => accounts.map(acc => this.toViewModel(acc))));
  }

  public createAccount(account: Partial<PublicApiAccount>): Observable<PublicApiAccount> {
    return this.httpClient.post<PublicApiAccount>(this.baseUrl, account);
  }

  public updateAccount(id: number, account: Partial<PublicApiAccount>): Observable<PublicApiAccount> {
    const url = `${this.baseUrl}/${id}`;

    return this.httpClient.put<PublicApiAccount>(url, account);
  }

  public deleteAccount(id: number): Observable<void> {
    const url = `${this.baseUrl}/${id}`;

    return this.httpClient.delete<void>(url);
  }

  private toViewModel(account: PublicApiAccount): PublicApiAccountView {
    const status = account.enabled ? 'enabled' : 'disabled';

    return {
      ...account,
      status: this.translationService.translate(`menu.masterData.configuration.publicApi.status.${status}`),
      actions: [
        account.enabled ? PublicApiAccountActionType.Disable : PublicApiAccountActionType.Enable,
        PublicApiAccountActionType.Modify,
        PublicApiAccountActionType.Delete
      ]
    };
  }
}
