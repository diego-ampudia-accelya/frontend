export interface PublicApiAccount {
  id: number;
  userInternalId: number;
  username: string;
  password: string;
  enabled: boolean;
}
