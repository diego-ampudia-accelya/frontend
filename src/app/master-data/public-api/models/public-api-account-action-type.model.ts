/* eslint-disable @typescript-eslint/naming-convention */
export enum PublicApiAccountActionType {
  Create = 'create',
  Enable = 'enable',
  Disable = 'disable',
  Modify = 'modify',
  Delete = 'delete'
}
