import { PublicApiAccountActionType } from './public-api-account-action-type.model';
import { PublicApiAccount } from './public-api-account.model';

export interface PublicApiAccountView extends PublicApiAccount {
  status: string;
  actions: PublicApiAccountActionType[];
}
