import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PublicApiAccountActionDialogComponent } from './components/public-api-account-action-dialog/public-api-account-action-dialog.component';
import { PublicApiAccountsComponent } from './components/public-api-accounts/public-api-accounts.component';
import { PublicApiAccountActionDialogService } from './services/public-api-account-action-dialog.service';
import { PublicApiManagementService } from './services/public-api-management.service';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [PublicApiAccountsComponent, PublicApiAccountActionDialogComponent],
  imports: [CommonModule, SharedModule],
  providers: [PublicApiManagementService, PublicApiAccountActionDialogService]
})
export class PublicApiModule {}
