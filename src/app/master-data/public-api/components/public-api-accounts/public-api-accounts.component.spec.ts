import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { of, throwError } from 'rxjs';

import { PublicApiAccountActionType } from '../../models/public-api-account-action-type.model';
import { PublicApiAccountView } from '../../models/public-api-account-view.model';
import { PublicApiAccountActionDialogService } from '../../services/public-api-account-action-dialog.service';
import { PublicApiManagementService } from '../../services/public-api-management.service';

import { PublicApiAccountsComponent } from './public-api-accounts.component';
import { TranslatePipeMock } from '~app/test';
import { FooterButton } from '~app/shared/components';
import { PermissionsService } from '~app/auth/services/permissions.service';

describe('PublicApiAccountsComponent', () => {
  let component: PublicApiAccountsComponent;
  let fixture: ComponentFixture<PublicApiAccountsComponent>;

  const dataServiceSpy = createSpyObject(PublicApiManagementService);
  const dialogServiceSpy = createSpyObject(PublicApiAccountActionDialogService);
  const permissionsServiceSpy = createSpyObject(PermissionsService);

  const initialState = {
    auth: {
      user: { iataCode: '016' }
    }
  };

  const accountsMock: PublicApiAccountView[] = [
    {
      id: 1,
      userInternalId: 1,
      username: 'user1',
      password: '',
      enabled: true,
      status: 'enabled',
      actions: [
        PublicApiAccountActionType.Disable,
        PublicApiAccountActionType.Modify,
        PublicApiAccountActionType.Delete
      ]
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PublicApiAccountsComponent, TranslatePipeMock],
      providers: [
        provideMockStore({ initialState }),
        { provide: PublicApiManagementService, useValue: dataServiceSpy },
        { provide: PublicApiAccountActionDialogService, useValue: dialogServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicApiAccountsComponent);
    component = fixture.componentInstance;

    dataServiceSpy.getAccounts.and.returnValue(of(accountsMock));
  });

  it('should be created', () => {
    expect(component).toBeDefined();
  });

  it('should initialize columns with airline code', fakeAsync(() => {
    const expectedColumns: any[] = [
      jasmine.objectContaining({ field: 'username' }),
      jasmine.objectContaining({ field: 'status' }),
      jasmine.objectContaining({ field: 'actions' })
    ];

    component.ngOnInit();
    fixture.detectChanges();

    expect(component.columns).toEqual(expectedColumns);
  }));

  it('should retrieve correct data from service', fakeAsync(() => {
    let result: PublicApiAccountView[];

    component.ngOnInit();
    component.data$.subscribe(data => (result = data));
    fixture.detectChanges();
    tick();

    expect(result).toEqual(accountsMock);
  }));

  it('should fill empty data from service with creation row', fakeAsync(() => {
    let result: Partial<PublicApiAccountView>[];

    dataServiceSpy.getAccounts.and.returnValue(of([]));

    component.ngOnInit();
    component.data$.subscribe(data => (result = data));
    fixture.detectChanges();
    tick();

    expect(result).toEqual([{ actions: [PublicApiAccountActionType.Create] }]);
  }));

  it('should set loading false when data is returned without errors', fakeAsync(() => {
    expect(component['_loadingSubject'].value).toBe(true);

    component.ngOnInit();
    fixture.detectChanges();
    tick();

    expect(component['_loadingSubject'].value).toBe(false);
  }));

  it('should set loading false when no data is returned but an error', fakeAsync(() => {
    expect(component['_loadingSubject'].value).toBe(true);

    dataServiceSpy.getAccounts.and.returnValue(throwError(null));

    component.ngOnInit();
    fixture.detectChanges();
    tick();

    expect(component['_loadingSubject'].value).toBe(false);
  }));

  describe(`onActionClick`, () => {
    beforeEach(() => dialogServiceSpy.open.calls.reset());

    it('should initialize account list after a successful action', () => {
      const setLoadingSpy = spyOn<any>(component, 'setLoading');
      const initializeDataSpy = spyOn<any>(component, 'initializeData');

      // Mocking update permissions to be false
      permissionsServiceSpy.hasPermission.and.returnValue(true);
      dialogServiceSpy.open.and.returnValue(of(FooterButton.Create));

      component.ngOnInit();
      fixture.detectChanges();

      component.onActionClick(
        { preventDefault: () => {} } as MouseEvent,
        PublicApiAccountActionType.Create,
        {} as PublicApiAccountView
      );

      expect(dialogServiceSpy.open).toHaveBeenCalledWith(PublicApiAccountActionType.Create, {});
      expect(setLoadingSpy).toHaveBeenCalledWith(true);
      expect(initializeDataSpy).toHaveBeenCalled();
    });

    it('should NOT open any dialog if user has not update permissions', () => {
      // Mocking update permissions to be false
      permissionsServiceSpy.hasPermission.and.returnValue(false);

      component.ngOnInit();
      fixture.detectChanges();

      component.onActionClick({} as MouseEvent, PublicApiAccountActionType.Create, {} as PublicApiAccountView);

      expect(dialogServiceSpy.open).not.toHaveBeenCalled();
    });
  });
});
