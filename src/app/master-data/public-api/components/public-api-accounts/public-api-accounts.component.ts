import { ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { actionButtonMapper } from '../../helpers/public-api-account.constants';
import { PublicApiAccountActionType } from '../../models/public-api-account-action-type.model';
import { PublicApiAccountView } from '../../models/public-api-account-view.model';
import { PublicApiAccountActionDialogService } from '../../services/public-api-account-action-dialog.service';
import { PublicApiManagementService } from '../../services/public-api-management.service';
import { Permissions } from '~app/shared/constants';
import { SimpleTableColumn } from '~app/shared/components';
import { PermissionsService } from '~app/auth/services/permissions.service';

@Component({
  selector: 'bspl-public-api-accounts',
  templateUrl: './public-api-accounts.component.html',
  styleUrls: ['./public-api-accounts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicApiAccountsComponent implements OnInit {
  public columns: SimpleTableColumn<PublicApiAccountView>[];
  public data$: Observable<PublicApiAccountView[]>;

  private _loadingSubject = new BehaviorSubject<boolean>(true);
  public loading$ = this._loadingSubject.asObservable();

  public hasUpdatePermission: boolean;

  @ViewChild('actionsTemplate', { static: true })
  private actionsTemplate: TemplateRef<any>;

  constructor(
    private dataService: PublicApiManagementService,
    private dialogService: PublicApiAccountActionDialogService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.hasUpdatePermission = this.permissionsService.hasPermission(Permissions.updatePublicApiAccounts);

    this.initializeColumns();
    this.initializeData();
  }

  public onActionClick(event: MouseEvent, action: PublicApiAccountActionType, account: PublicApiAccountView) {
    if (!this.hasUpdatePermission) {
      return;
    }

    event.preventDefault();

    this.dialogService.open(action, account).subscribe(clickedBtn => {
      if (clickedBtn === actionButtonMapper[action]) {
        // Initialize account list (loading spinner and data)
        this.setLoading(true);
        this.initializeData();
      }
    });
  }

  private initializeColumns(): void {
    this.columns = [
      {
        header: 'menu.masterData.configuration.publicApi.columns.username',
        field: 'username'
      },
      {
        header: 'menu.masterData.configuration.publicApi.columns.status',
        field: 'status'
      },
      {
        header: 'menu.masterData.configuration.publicApi.columns.action',
        field: 'actions',
        cellTemplate: this.actionsTemplate
      }
    ];
  }

  private initializeData(): void {
    this.data$ = this.dataService.getAccounts().pipe(
      map(data => (data.length ? data : this.handleEmptyData())),
      finalize(() => this.setLoading(false))
    );
  }

  private handleEmptyData(): PublicApiAccountView[] {
    const creationRow = { actions: [PublicApiAccountActionType.Create] } as PublicApiAccountView;

    return [creationRow];
  }

  private setLoading(loading: boolean): void {
    this._loadingSubject.next(loading);
  }
}
