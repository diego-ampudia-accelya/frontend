import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { actionButtonMapper } from '../../helpers/public-api-account.constants';
import { PublicApiAccountActionType } from '../../models/public-api-account-action-type.model';
import { PublicApiAccountView } from '../../models/public-api-account-view.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { AlertMessageType } from '~app/shared/enums';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { confirmPasswordValidator } from '~app/shared/validators';

@Component({
  selector: 'bspl-public-api-account-action-dialog',
  templateUrl: './public-api-account-action-dialog.component.html',
  styleUrls: ['./public-api-account-action-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublicApiAccountActionDialogComponent implements OnInit, OnDestroy {
  public actionType: PublicApiAccountActionType;
  public account: PublicApiAccountView;

  public keysRoot: string;

  public dialogForm = new FormGroup({});
  public usernameCheckboxControl: FormControl;
  public passwordCheckboxControl: FormControl;
  public confirmCheckboxControl: FormControl;

  public get isDialogFormDisplayed(): boolean {
    return (
      this.actionType === PublicApiAccountActionType.Create || this.actionType === PublicApiAccountActionType.Modify
    );
  }

  public get isModifyParagraphDisplayed(): boolean {
    return this.actionType === PublicApiAccountActionType.Modify;
  }

  public actionAlertMessageTypeMapper: { [key in PublicApiAccountActionType]: AlertMessageType } = {
    [PublicApiAccountActionType.Create]: AlertMessageType.info,
    [PublicApiAccountActionType.Modify]: AlertMessageType.info,
    [PublicApiAccountActionType.Enable]: AlertMessageType.info,
    [PublicApiAccountActionType.Disable]: AlertMessageType.info,
    [PublicApiAccountActionType.Delete]: AlertMessageType.warning
  };

  public userAirlineCode$ = this.store.pipe(
    select(getUser),
    map(user => user.iataCode)
  );

  private destroy$ = new Subject();

  constructor(private config: DialogConfig, private store: Store<AppState>) {}

  public ngOnInit(): void {
    this.initializeDialogData();
  }

  private initializeDialogData(): void {
    const dialogData = this.config.data;

    this.actionType = dialogData.action;
    this.account = dialogData.account;

    this.keysRoot = `menu.masterData.configuration.publicApi.dialogs.${this.actionType}`;

    switch (this.actionType) {
      case PublicApiAccountActionType.Create:
        // Build creation form
        this.dialogForm.addControl('username', new FormControl(null, Validators.required));
        this.dialogForm.addControl('password', new FormControl(null, Validators.required));
        this.dialogForm.addControl(
          'confirmPassword',
          new FormControl(null, [Validators.required, confirmPasswordValidator])
        );

        // Initialize dialog form status listener
        this.initializeFormStatusListener();

        break;

      case PublicApiAccountActionType.Modify:
        // Build modification form
        this.dialogForm.addControl(
          'username',
          new FormControl({ value: this.account.username, disabled: true }, Validators.required)
        );
        this.dialogForm.addControl('password', new FormControl({ value: null, disabled: true }, Validators.required));
        this.dialogForm.addControl(
          'confirmPassword',
          new FormControl({ value: null, disabled: true }, [Validators.required, confirmPasswordValidator])
        );

        // Initialize dialog form status listener
        this.initializeFormStatusListener();

        // Build field checkboxes controls
        this.usernameCheckboxControl = new FormControl(false);
        this.passwordCheckboxControl = new FormControl(false);

        // Initialize checkboxes controls listeners
        this.initializeUsernameCheckboxListener();
        this.initializePasswordCheckboxListener();

        break;

      case PublicApiAccountActionType.Delete:
        // Build confirm checkbox control
        this.confirmCheckboxControl = new FormControl(null, Validators.requiredTrue);

        // Initialize confirm checkbox listener
        this.initializeConfirmCheckboxListener();

        break;
    }
  }

  private initializeFormStatusListener(): void {
    this.dialogForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => this.setSubmitButtonState(status));
  }

  private initializeUsernameCheckboxListener(): void {
    this.usernameCheckboxControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      const usernameControl = this.dialogForm.get('username');

      if (value) {
        usernameControl.enable();
      } else {
        usernameControl.disable();
        usernameControl.reset(this.account.username);
      }
    });
  }

  private initializePasswordCheckboxListener(): void {
    this.passwordCheckboxControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      const passwordControl = this.dialogForm.get('password');
      const confirmPasswordControl = this.dialogForm.get('confirmPassword');

      if (value) {
        passwordControl.enable();
        confirmPasswordControl.enable();
      } else {
        passwordControl.disable();
        confirmPasswordControl.disable();
        passwordControl.reset();
        confirmPasswordControl.reset();
      }
    });
  }

  private initializeConfirmCheckboxListener(): void {
    this.confirmCheckboxControl.statusChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(status => this.setSubmitButtonState(status));
  }

  private setSubmitButtonState(status: string): void {
    const submitButton: ModalAction = this.config.data.buttons.find(
      (btn: ModalAction) => btn.type === actionButtonMapper[this.actionType]
    );
    submitButton.isDisabled = status === 'INVALID';
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
