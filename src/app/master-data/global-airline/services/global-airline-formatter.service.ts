import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { BlankTemplate, GlobalAirlineFilter } from '../models/global-airline-list.model';
import { GlobalAirlineTemplate } from '~app/master-data/models';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { globalAirlineFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class GlobalAirlinesFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translationService: L10nTranslationService) {
    super();
  }

  public format(filter: GlobalAirlineFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<GlobalAirlineFilter> = {
      iataCode: airlineGlobalSummary => globalAirlineFilterTagMapper(airlineGlobalSummary).join(', '),
      designator: designator => `${this.translate('designator')} - ${designator.join(', ')}`,
      template: template =>
        `${this.translate('template')} - ${template.map(temp => this.translateTemplate(temp)).join(', ')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`MASTER_DATA.globalAirline.filters.${key}.label`);
  }

  private translateTemplate(key: BlankTemplate | GlobalAirlineTemplate): string {
    return this.translationService.translate('MASTER_DATA.globalAirline.templates.' + key);
  }
}
