import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, mapValues } from 'lodash';
import { NEVER, of } from 'rxjs';

import { GlobalAirlinesFilterFormatter } from '../../services/global-airline-formatter.service';
import { GlobalAirlineListComponent } from './global-airline-list.component';
import { SEARCH_FORM_DEFAULT_VALUE } from './global-airline-list.constants';
import { GlobalAirlineService } from '~app/master-data/services';
import { DialogComponent, DialogService } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models';
import { GlobalAirlineSummary } from '~app/shared/models/dictionary/global-airline-summary.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService } from '~app/shared/services';
import { GlobalAirlineDesignatorDictionaryService } from '~app/shared/services/dictionary/global-airline-designator-dictionary.service';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';
import { TranslatePipeMock } from '~app/test';

describe('GlobalAirlineList component', () => {
  let component: GlobalAirlineListComponent;
  let fixture: ComponentFixture<GlobalAirlineListComponent>;

  let queryableDataSourceMock: SpyObject<QueryableDataSource<any>>;
  let globalAirlineServiceMock: SpyObject<GlobalAirlineService>;
  let queryStorageMock: SpyObject<DefaultQueryStorage>;
  let dialogServiceMock: SpyObject<DialogService>;
  let formBuilderMock: SpyObject<FormBuilder>;
  let designatorDictionaryServiceMock: SpyObject<GlobalAirlineDesignatorDictionaryService>;
  let globalAirlineDictionaryServiceMock: SpyObject<GlobalAirlineDictionaryService>;

  const query = {
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  } as DataQuery;

  const globalAirlineOptionsMock: DropdownOption<GlobalAirlineSummary>[] = [
    { value: { id: 1, iataCode: '2', globalName: 'Test' }, label: '2 / Test' },
    { value: { id: 2, iataCode: '3', globalName: 'Test3' }, label: '3 / Test' }
  ];

  const designatorOptionsMock: DropdownOption<string>[] = [
    { value: 'AA', label: 'AA' },
    { value: 'BB', label: 'BB' },
    { value: 'CC', label: 'CC' }
  ];

  beforeEach(waitForAsync(() => {
    queryableDataSourceMock = createSpyObject(QueryableDataSource, { appliedQuery$: of(query) });
    queryableDataSourceMock.get.and.returnValue(of(query));

    queryStorageMock = createSpyObject(DefaultQueryStorage);
    queryStorageMock.get.and.returnValue(query);

    TestBed.configureTestingModule({
      declarations: [GlobalAirlineListComponent, TranslatePipeMock, DialogComponent],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        mockProvider(FormBuilder),
        mockProvider(L10nTranslationService),
        mockProvider(GlobalAirlinesFilterFormatter),
        mockProvider(GlobalAirlineService),
        mockProvider(GlobalAirlineDesignatorDictionaryService),
        mockProvider(GlobalAirlineDictionaryService),
        mockProvider(NotificationService),
        mockProvider(DialogService)
      ]
    })
      .overrideComponent(GlobalAirlineListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceMock },
            { provide: DefaultQueryStorage, useValue: queryStorageMock }
          ]
        }
      })
      .compileComponents();

    globalAirlineServiceMock = TestBed.inject<any>(GlobalAirlineService);
    globalAirlineServiceMock.find.and.returnValue(of([]));

    formBuilderMock = TestBed.inject<any>(FormBuilder);
    formBuilderMock.group.and.returnValue(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));

    dialogServiceMock = TestBed.inject<any>(DialogService);
    dialogServiceMock.open.and.returnValue(of(NEVER));

    designatorDictionaryServiceMock = TestBed.inject<any>(GlobalAirlineDesignatorDictionaryService);
    designatorDictionaryServiceMock.getDropdownOptions.and.returnValue(of(designatorOptionsMock));
    globalAirlineDictionaryServiceMock = TestBed.inject<any>(GlobalAirlineDictionaryService);
    globalAirlineDictionaryServiceMock.getDropdownOptions.and.returnValue(of(globalAirlineOptionsMock));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalAirlineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('loadData should set a default sorting if there is no such', () => {
    component.loadData(query);
    const expected = jasmine.objectContaining({
      sortBy: [{ attribute: 'iataCode', sortType: SortOrder.Asc }]
    });

    expect(queryableDataSourceMock.get).toHaveBeenCalledWith(expected);
  });

  it('loadData should NOT set a default sorting if there is such', () => {
    component.loadData({ ...query, sortBy: [{ attribute: 'key', sortType: SortOrder.Desc }] });
    const expected = jasmine.objectContaining({
      sortBy: [{ attribute: 'key', sortType: SortOrder.Desc }]
    });

    expect(queryableDataSourceMock.get).toHaveBeenCalledWith(expected);
  });

  it('should set columns in ngOnInit', () => {
    component.columns = null;

    const expectedColumns = [
      jasmine.objectContaining({
        prop: 'iataCode',
        name: 'MASTER_DATA.globalAirline.columns.code',
        resizeable: false,
        draggable: false,
        minWidth: 100
      }),
      jasmine.objectContaining({
        prop: 'globalName',
        name: 'MASTER_DATA.globalAirline.columns.name',
        resizeable: true,
        draggable: false
      }),
      jasmine.objectContaining({
        prop: 'designator',
        name: 'MASTER_DATA.globalAirline.columns.designator',
        resizeable: true,
        draggable: false
      }),
      jasmine.objectContaining({
        prop: 'template',
        name: 'MASTER_DATA.globalAirline.columns.template',
        resizeable: true,
        draggable: false
      })
    ] as TableColumn[];

    component.ngOnInit();

    expect(component.columns).toEqual(expectedColumns);
  });

  it('should set search form fields in ngOnInit', () => {
    component.searchForm = null;

    const expectedFields = formBuilderMock.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));

    component.ngOnInit();

    expect(component.searchForm).toEqual(expectedFields);
  });

  it('should load data with current query', () => {
    spyOn(component, 'loadData');

    component.ngOnInit();

    expect(component.loadData).toHaveBeenCalledWith(query);
  });

  it('should load data with default query if there is no current', () => {
    queryStorageMock.get.and.returnValue(null);

    spyOn(component, 'loadData');

    component.ngOnInit();

    expect(component.loadData).toHaveBeenCalledWith(cloneDeep(defaultQuery));
  });

  it('should open dialog when Template action button is clicked', () => {
    spyOn<any>(component, 'openTemplateDialog');

    component.onActionClick({ action: { actionType: GridTableActionType.Template }, row: null, event: null });

    expect(component['openTemplateDialog']).toHaveBeenCalled();
  });
});
