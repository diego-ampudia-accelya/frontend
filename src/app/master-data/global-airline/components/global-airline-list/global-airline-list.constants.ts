/* eslint-disable @typescript-eslint/naming-convention */
import { GlobalAirlineFilter } from '../../models/global-airline-list.model';
import { GridColumn } from '~app/shared/models';

export const GLOBAL_AIRLINE_LIST = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'iataCode',
        name: 'MASTER_DATA.globalAirline.columns.code',
        resizeable: false,
        draggable: false,
        minWidth: 100
      },
      {
        prop: 'globalName',
        name: 'MASTER_DATA.globalAirline.columns.name',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'designator',
        name: 'MASTER_DATA.globalAirline.columns.designator',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'template',
        name: 'MASTER_DATA.globalAirline.columns.template',
        resizeable: true,
        draggable: false
      }
    ];
  }
};

export const SEARCH_FORM_DEFAULT_VALUE: GlobalAirlineFilter = {
  iataCode: null,
  designator: null,
  template: null
};
