import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty, mapValues } from 'lodash';
import { Observable } from 'rxjs';
import { filter, finalize, first, switchMap, tap } from 'rxjs/operators';

import { BlankTemplate } from '../../models/global-airline-list.model';
import { GlobalAirlinesFilterFormatter } from '../../services/global-airline-formatter.service';
import { GlobalAirlineTemplateDialogComponent } from '../global-airline-template-dialog/global-airline-template-dialog.component';
import { GLOBAL_AIRLINE_LIST, SEARCH_FORM_DEFAULT_VALUE } from './global-airline-list.constants';
import { GlobalAirline, GlobalAirlineTemplate } from '~app/master-data/models';
import { GlobalAirlineService } from '~app/master-data/services';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { GlobalAirlineSummary } from '~app/shared/models/dictionary/global-airline-summary.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService } from '~app/shared/services';
import { GlobalAirlineDesignatorDictionaryService } from '~app/shared/services/dictionary/global-airline-designator-dictionary.service';
import { GlobalAirlineDictionaryService } from '~app/shared/services/dictionary/global-airline-dictionary.service';

@Component({
  selector: 'bspl-global-airline-list',
  templateUrl: './global-airline-list.component.html',
  styleUrls: ['./global-airline-list.component.scss'],
  providers: [
    GlobalAirlineService,
    DefaultQueryStorage,
    { provide: QUERYABLE, useClass: GlobalAirlineService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [GlobalAirlineService]
    }
  ]
})
export class GlobalAirlineListComponent implements OnInit {
  public columns: TableColumn[];
  public searchForm: FormGroup;
  public actions = [{ action: GridTableActionType.Template }];

  public globalAirlineCodeOptions$: Observable<DropdownOption<GlobalAirlineSummary>[]>;
  public designatorOptions$: Observable<DropdownOption<string>[]>;
  public templateOptions: DropdownOption<GlobalAirlineTemplate | BlankTemplate>[];

  public header = this.translationService.translate('global-airline.viewTitle');

  constructor(
    public dataSource: QueryableDataSource<GlobalAirlineService>,
    public filterFormatter: GlobalAirlinesFilterFormatter,
    private globalAirlineService: GlobalAirlineService,
    private queryStorage: DefaultQueryStorage,
    private designatorDictionaryService: GlobalAirlineDesignatorDictionaryService,
    private globalAirlineDictionaryService: GlobalAirlineDictionaryService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private fb: FormBuilder
  ) {}

  public ngOnInit(): void {
    this.buildSearchForm();
    this.columns = GLOBAL_AIRLINE_LIST.COLUMNS;
    this.populateFilterDropdowns();
    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  public loadData(query: DataQuery): void {
    if (isEmpty(query.sortBy)) {
      const sortBy = [{ attribute: 'iataCode', sortType: SortOrder.Asc }];
      query = { ...query, sortBy };
    }

    this.dataSource.get(query);
    this.queryStorage.save(query);
  }

  public onActionClick({ action, row }: { action: any; row: any; event: MouseEvent }) {
    if (action.actionType === GridTableActionType.Template) {
      this.openTemplateDialog(row);
    }
  }

  public download() {
    const requestQuery = this.queryStorage.get();

    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: requestQuery
      },
      apiService: this.globalAirlineService
    });
  }

  private refreshTable(): void {
    this.loadData(this.queryStorage.get());
  }

  private openTemplateDialog(item: GlobalAirline): void {
    const dialogForm = this.fb.group([]);
    let applyBtn;

    this.dialogService
      .open(GlobalAirlineTemplateDialogComponent, {
        data: {
          title: this.translationService.translate('MASTER_DATA.globalAirline.dialog.template.title'),
          footerButtonsType: FooterButton.Apply,
          item,
          dialogForm
        }
      })
      .pipe(
        first(),
        filter(({ clickedBtn }) => clickedBtn === FooterButton.Apply),
        tap(({ contentComponentRef }) => {
          applyBtn = contentComponentRef.config.data.buttons.find(btn => btn.type === FooterButton.Apply);
          applyBtn.isLoading = true;
        }),
        switchMap(() => {
          const template = dialogForm.get('template').value;

          return this.globalAirlineService.updateTemplate(item.id, template);
        }),
        finalize(() => this.dialogService.close())
      )
      .subscribe(() => this.onTemplateUpdate());
  }

  private onTemplateUpdate(): void {
    this.notificationService.showSuccess('MASTER_DATA.globalAirline.templateSuccessMessage');
    this.refreshTable();
  }

  private populateFilterDropdowns() {
    this.templateOptions = [...Object.keys(BlankTemplate), ...Object.keys(GlobalAirlineTemplate)].map(
      (value: BlankTemplate | GlobalAirlineTemplate) => ({
        value: BlankTemplate[value] || GlobalAirlineTemplate[value],
        label: this.translationService.translate(
          `MASTER_DATA.globalAirline.templates.${BlankTemplate[value] || GlobalAirlineTemplate[value]}`
        )
      })
    );

    this.globalAirlineCodeOptions$ = this.globalAirlineDictionaryService.getDropdownOptions();
    this.designatorOptions$ = this.designatorDictionaryService.getDropdownOptions();
  }

  private buildSearchForm(): void {
    this.searchForm = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));
  }
}
