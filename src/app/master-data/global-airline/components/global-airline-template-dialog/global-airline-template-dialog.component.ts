import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { templateValidator } from './validators/template.validator';
import { GlobalAirline, GlobalAirlineTemplate } from '~app/master-data/models';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';

@Component({
  selector: 'bspl-global-airline-template-dialog',
  templateUrl: './global-airline-template-dialog.component.html',
  styleUrls: ['./global-airline-template-dialog.component.scss']
})
export class GlobalAirlineTemplateDialogComponent implements OnInit, OnDestroy {
  public globalAirline: GlobalAirline;
  public dialogForm: FormGroup;
  public templateOptions: DropdownOption<GlobalAirlineTemplate>[];

  private templateControl: FormControl;

  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    private translationService: L10nTranslationService,
    private fb: FormBuilder
  ) {}

  public ngOnInit(): void {
    this.globalAirline = this.config.data.item;
    this.buildForm();
    this.populateTemplateOptions();
    this.initializeFormStatusListener();
  }

  private buildForm(): void {
    this.templateControl = this.fb.control(this.globalAirline.template, [
      Validators.required,
      templateValidator(this.globalAirline.template)
    ]);

    this.dialogForm = this.config.data.dialogForm;
    this.dialogForm.addControl('template', this.templateControl);
    this.dialogForm.addControl('confirm', this.fb.control(null, Validators.requiredTrue));
  }

  private populateTemplateOptions(): void {
    this.templateOptions = Object.keys(GlobalAirlineTemplate).map((value: GlobalAirlineTemplate) => ({
      value: GlobalAirlineTemplate[value],
      label: this.translationService.translate(`MASTER_DATA.globalAirline.templates.${GlobalAirlineTemplate[value]}`)
    }));
  }

  private initializeFormStatusListener(): void {
    const applyBtn = this.config.data.buttons.find(btn => btn.type === FooterButton.Apply);
    applyBtn.isDisabled = this.dialogForm.invalid;

    this.dialogForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      applyBtn.isDisabled = this.dialogForm.invalid;
      FormUtil.showControlState(this.templateControl);
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
