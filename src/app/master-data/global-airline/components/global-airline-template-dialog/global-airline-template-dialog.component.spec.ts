import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { GlobalAirlineTemplateDialogComponent } from './global-airline-template-dialog.component';
import { GlobalAirline, GlobalAirlineTemplate } from '~app/master-data/models';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ButtonsFooterFactory } from '~app/shared/components/dialog/elements/buttons-footer-factory';
import { TranslatePipeMock } from '~app/test';

describe('GlobalAirlineTemplateDialogComponent', () => {
  let component: GlobalAirlineTemplateDialogComponent;
  let fixture: ComponentFixture<GlobalAirlineTemplateDialogComponent>;

  const globalAirlineMock: GlobalAirline = {
    globalName: 'Test',
    id: 1,
    iataCode: '123',
    template: GlobalAirlineTemplate.Efficient,
    designator: 'AA'
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GlobalAirlineTemplateDialogComponent, TranslatePipeMock],
      providers: [
        mockProvider(DialogConfig, {
          data: {
            item: globalAirlineMock,
            buttons: ButtonsFooterFactory.getFooterButtonsByType([{ type: FooterButton.Apply }]),
            dialogForm: new FormGroup({})
          }
        }),
        mockProvider(L10nTranslationService),
        FormBuilder
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalAirlineTemplateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable Apply Button when created', () => {
    const applyBtn = component.config.data.buttons.find(btn => btn.type === FooterButton.Apply);

    expect(applyBtn.isDisabled).toBe(true);
  });

  it('should keep Apply Button disabled when template is not changed', () => {
    const applyBtn = component.config.data.buttons.find(btn => btn.type === FooterButton.Apply);
    component.dialogForm.setValue({ template: GlobalAirlineTemplate.Efficient, confirm: true });

    expect(applyBtn.isDisabled).toBe(true);
  });

  it('should enabled Apply Button when providing a different template and confirm button is checked', () => {
    const applyBtn = component.config.data.buttons.find(btn => btn.type === FooterButton.Apply);
    component.dialogForm.setValue({ template: GlobalAirlineTemplate.Lean, confirm: true });

    expect(applyBtn.isDisabled).toBe(false);
  });
});
