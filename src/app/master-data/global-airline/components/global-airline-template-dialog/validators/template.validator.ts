import { AbstractControl, ValidatorFn } from '@angular/forms';

import { GlobalAirlineTemplate } from '~app/master-data/models';

export function templateValidator(originalTemplate: GlobalAirlineTemplate): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    let result = null;

    if (control.value === originalTemplate) {
      result = {
        differentTemplate: {}
      };
    }

    return result;
  };
}
