import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { GlobalAirlineListComponent } from './components/global-airline-list/global-airline-list.component';
import { GlobalAirlineTemplateDialogComponent } from './components/global-airline-template-dialog/global-airline-template-dialog.component';
import { GlobalAirlineRoutingRoutingModule } from './global-airline-routing.module';
import { GlobalAirlinesFilterFormatter } from './services/global-airline-formatter.service';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [GlobalAirlineListComponent, GlobalAirlineTemplateDialogComponent],
  imports: [CommonModule, SharedModule, GlobalAirlineRoutingRoutingModule],
  providers: [GlobalAirlinesFilterFormatter]
})
export class GlobalAirlineModule {}
