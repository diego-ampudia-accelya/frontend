/* eslint-disable @typescript-eslint/naming-convention */
import { GlobalAirlineTemplate } from '~app/master-data/models';
import { GlobalAirlineSummary } from '~app/shared/models/dictionary/global-airline-summary.model';

export interface GlobalAirlineFilter {
  iataCode: GlobalAirlineSummary[];
  designator: string[];
  template: Array<GlobalAirlineTemplate | BlankTemplate>;
}

export enum BlankTemplate {
  Blank = 'BLANK'
}
