import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GlobalAirlineListComponent } from './components/global-airline-list/global-airline-list.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: GlobalAirlineListComponent,
    data: {
      tab: ROUTES.GLOBAL_AIRLINE
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GlobalAirlineRoutingRoutingModule {}
