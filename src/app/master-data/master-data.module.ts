import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';

import { MasterDataRoutingModule } from './master-data-routing.module';
import { DialogService } from '~app/shared/components';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, L10nTranslationModule, MasterDataRoutingModule, SharedModule],
  providers: [DialogService]
})
export class MasterDataModule {}
