import { BehaviorSubject, forkJoin, Observable, timer } from 'rxjs';
import { finalize, map, tap } from 'rxjs/operators';

import { DescriptionService } from './api/description-service';
import { EditableDescription } from './models/editable-description.model';
import { GLOBALS } from '~app/shared/constants/globals';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

export class EditableDescriptionDataSource {
  public items$: Observable<EditableDescription[]>;
  public isLoading$: Observable<boolean>;

  public get count(): number {
    return this.itemsSubject.getValue().length;
  }

  private parentEntityId: number;

  private itemsSubject = new BehaviorSubject<EditableDescription[]>([]);
  private isLoadingSubject = new BehaviorSubject<boolean>(false);

  constructor(private descriptionService: DescriptionService) {
    this.items$ = this.itemsSubject.asObservable();
    this.isLoading$ = this.isLoadingSubject.asObservable();
  }

  public get(parentEntityId: number): Observable<EditableDescription[]> {
    this.parentEntityId = parentEntityId;

    return this.useSpinner(this.loadItems(parentEntityId));
  }

  public delete(item: EditableDescription): Observable<void> {
    return this.useSpinner(
      this.descriptionService.delete(item).pipe(
        tap(() => {
          const models = this.itemsSubject.getValue().filter(model => model.id !== item.id);
          this.itemsSubject.next(models);
        })
      )
    );
  }

  public save(item: EditableDescription): Observable<any> {
    return this.useSpinner(
      this.descriptionService.save(item).pipe(
        tap(result => {
          let models = this.itemsSubject.getValue();
          const itemIndex = models.findIndex(model => model.id === item.id);
          models = Object.assign([], models, { [itemIndex]: result });
          this.itemsSubject.next(models);
        }),
        // TODO: Figure out a way to avoid reloading all items
        rethrowError(() => this.loadItems(item.parentId))
      )
    );
  }

  public createEmpty(): EditableDescription {
    return {
      id: null,
      parentId: this.parentEntityId,
      sequenceNumber: this.getNextSequenceNumber(),
      label: '',
      description: ''
    };
  }

  private getNextSequenceNumber(): number {
    const first = this.itemsSubject.getValue()[0];

    return first ? first.sequenceNumber + 1 : 1;
  }

  /**
   * Sort items by sequenceNumber in `descending` order
   *
   * @param items Items to sort
   */
  private sortItems(items: EditableDescription[]): EditableDescription[] {
    return items.sort((itemA, itemB) => itemB.sequenceNumber - itemA.sequenceNumber);
  }

  private loadItems(parentId: number): Observable<EditableDescription[]> {
    return this.descriptionService.get(parentId).pipe(
      map(items => this.sortItems(items || [])),
      tap(items => this.itemsSubject.next(items))
    );
  }

  private useSpinner<T>(observable: Observable<T>): Observable<T> {
    this.isLoadingSubject.next(true);

    // Make sure that the spinner is shown for at least a short while before hiding it.
    return forkJoin([timer(GLOBALS.LOADING_SPINNER_DELAY), observable]).pipe(
      map(([, result]) => result),
      finalize(() => this.isLoadingSubject.next(false))
    );
  }
}
