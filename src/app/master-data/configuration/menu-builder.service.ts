import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Route } from '@angular/router';

import { MENU } from './menu.constants';
import { RoutedMenuItem } from './models/routed-menu-item.model';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SubUserLevel } from '~app/sub-users/shared/sub-users.models';

@Injectable({
  providedIn: 'root'
})
export class MenuBuilder {
  constructor(private permissionsService: PermissionsService) {}

  public buildMenuItemsFrom(activatedRoute: ActivatedRouteSnapshot, bspId?: number): RoutedMenuItem[] {
    const { routeConfig, data } = activatedRoute;
    const { subUser } = data;

    return routeConfig.children
      .filter(child => child.data && child.data.title)
      .map(child => {
        const route: string = child.path;
        let isAccessibleByUser = this.checkHasPermission(child, bspId);

        if (subUser) {
          isAccessibleByUser = this.checkPermissionsTabAndSubUserLevel(child, subUser) ? false : isAccessibleByUser;
        }

        return {
          group: child.data.group,
          title: child.data.title,
          route,
          isAccessible: isAccessibleByUser
        };
      })
      .filter(item => item.isAccessible);
  }

  private checkHasPermission(child: Route, bspId?: number): boolean {
    return bspId
      ? this.permissionsService.hasPermission(child.data.requiredPermissions, bspId)
      : this.permissionsService.hasPermission(child.data.requiredPermissions);
  }

  private checkPermissionsTabAndSubUserLevel(child, subUser): boolean {
    // Only users with property level 1 have to see Permissions Tab
    return child.path === MENU.TABS.PERMISSIONS && subUser.level !== SubUserLevel.level1;
  }
}
