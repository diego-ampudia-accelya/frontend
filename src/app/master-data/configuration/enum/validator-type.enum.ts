export enum ValidatorType {
  Regex = 'REGEX',
  Min = 'MIN',
  Max = 'MAX'
}
