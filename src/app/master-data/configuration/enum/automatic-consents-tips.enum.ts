export enum AutomaticConsents {
  Ancr = 'yes',
  Acr = 'costRecovery',
  Nc = 'no',
  Mco = 'manual'
}
