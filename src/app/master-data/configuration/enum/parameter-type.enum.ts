export enum ParameterType {
  Boolean = 'BOOLEAN',
  Enum = 'ENUM',
  Integer = 'INTEGER',
  String = 'STRING',
  StringList = 'STRING_LIST',
  Date = 'DATE'
}
