export enum CostRecoveryType {
  Fixed = 'fixedAmount',
  Percentage = 'percentage'
}
