import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { EditableDescription, EditableDescriptionVM } from '../models/editable-description.model';
import { limitDescriptionValidator } from './validators/limit-description.validator';
import { isNew } from '~app/shared/utils/api-utils';
import { TextareaComponent } from '~app/shared/components';

export enum FieldType {
  Label = 'label',
  Description = 'description'
}
@Component({
  selector: 'bspl-editable-description',
  templateUrl: './editable-description.component.html',
  styleUrls: ['./editable-description.component.scss']
})
export class EditableDescriptionComponent implements OnInit, OnDestroy {
  @Input() labelRegexValidation: string | RegExp;
  @Input() labelAllCaps: boolean;
  @Input() labelMaxLenght: number;

  @Input() descriptionRegexValidation: string | RegExp;
  @Input() descriptionItem: EditableDescription;
  @Input() descriptionLineLimit: number;
  @Input() descriptionRowsLimit: number;
  @Input() descriptionMaxLength: number;

  @Input() hasDisabledActions = false;
  @Input() mandatoryDescription = true;
  @Input() mandatoryLabel = true;
  @Input() canEdit: boolean;
  @Input() canDelete: boolean;

  @Input() canValidateDescription = false;

  @Output() saveRequest = new EventEmitter<EditableDescription>();
  @Output() deleteRequest = new EventEmitter<EditableDescription>();
  @Output() itemChange = new EventEmitter<EditableDescriptionVM>();
  @Output() editModeChange = new EventEmitter<boolean>();

  @ViewChild(TextareaComponent, { static: false }) textarea: TextareaComponent;

  // form reference for interaction with inptut fields
  public form: FormGroup;
  // flag determines if the component is showing `read` or `edit` mode
  public editMode = false;
  public textareaRows = 1;
  // flag determines if the `save` button is enabled or disabled (validation)
  public canSave = false;
  public showDescriptionTooltip: boolean;
  public showLabelTooltip: boolean;
  protected destroy$ = new Subject();
  protected actionsThatCanBeDisabled = ['edit', 'delete'];

  // Object used for storing the instance values during editing
  protected oldValues = {
    label: '',
    description: ''
  };

  protected get requiredFields(): FieldType[] {
    const items = [];
    if (this.mandatoryDescription) {
      items.push(FieldType.Description);
    }

    if (this.mandatoryLabel) {
      items.push(FieldType.Label);
    }

    return items;
  }

  get descriptionPlaceholder(): string {
    return this.translationService.translate('editableDescription.addDescriptionPlaceholder', {
      maxLength: this.descriptionMaxLength
    });
  }

  protected caretPosition = 0;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    protected formBuilder: FormBuilder,
    protected changeDetector: ChangeDetectorRef,
    private translationService: L10nTranslationService
  ) {}

  public ngOnInit() {
    if (!this.descriptionMaxLength && this.descriptionRowsLimit && this.descriptionLineLimit) {
      this.descriptionMaxLength = this.descriptionRowsLimit * this.descriptionLineLimit;
    }

    this.createForm();
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  /**
   * Remembers current values and enters `edit` mode.
   *
   * @returns void
   */
  public actionEdit(): void {
    if (this.editMode) {
      return;
    }

    // disable the `save` button until any value has changed in a valid way (e.g. not empty)
    this.canSave = false;

    // remember form values for later
    this.oldValues = this.form.value;

    this.toggleEditMode();
    this.changeDetector.detectChanges();
    this.adjustTextareaHeight(this.textarea?.textarea);
    this.onItemChange();
  }

  /**
   * Handles `save` action.
   *
   * @returns void
   */
  public actionSave(): void {
    if (this.canSave) {
      this.trimInputs();
      const data = { ...this.descriptionItem, ...this.form.value };
      this.saveRequest.emit(data);

      this.toggleEditMode();
    }
  }

  /**
   * Discards changes and exits `edit` mode.
   *
   * @returns void
   */
  public actionCancel(): void {
    if (isNew(this.descriptionItem)) {
      this.actionDelete();
    } else {
      this.form.setValue(this.oldValues);
    }

    // Exit edit mode.
    this.toggleEditMode();
  }

  /**
   * Handles `delete` action.
   */
  public actionDelete() {
    this.deleteRequest.emit(this.descriptionItem);
  }

  /**
   * Handles `click` events.
   * Detects clicks on control buttons (aka actions)
   *
   * @param event - The event object
   * @returns void
   */
  public onClick(event: any): void {
    // consider event targets that have `data-title` attribute to be `actions`
    const targetTitle = event.target.attributes['data-title'];

    if (!targetTitle || this.isActionDisabled(targetTitle.value)) {
      return;
    }

    const action = targetTitle.value; // e.g. save
    // e.g. actionSave
    const methodName = `action${action[0].toUpperCase()}${action.slice(1)}`;

    if (!this[methodName]) {
      return;
    }

    // trigger the action method e.g. this['actionSave']()
    this[methodName]();
  }

  /**
   * Switches between `edit` and `read` modes.
   *
   * @returns void
   */
  public toggleEditMode(): void {
    this.editMode = !this.editMode;

    this.editModeChange.emit(this.editMode);
  }

  public applyCssClassesByConditions() {
    return {
      'disabled-hover': this.hasDisabledActions
    };
  }

  public adjustTextareaHeight(elementRef: ElementRef<HTMLTextAreaElement>): void {
    const { nativeElement } = elementRef;
    nativeElement.style.height = `${nativeElement.scrollHeight + 2}px`;
  }

  public onItemChange(): void {
    this.itemChange.emit({
      value: { ...this.descriptionItem, ...this.form.value },
      canSave: this.canSave
    });
  }

  /**
   * Creates the input fields for the component instance.
   *
   * @returns void
   */
  protected createForm(): void {
    this.form = this.formBuilder.group({
      label: [this.descriptionItem.label, Validators.pattern(this.labelRegexValidation)],
      description: [
        this.descriptionItem.description,
        [
          Validators.pattern(this.descriptionRegexValidation),
          limitDescriptionValidator(
            this.canValidateDescription,
            this.descriptionMaxLength,
            this.descriptionRowsLimit,
            this.descriptionLineLimit
          )
        ]
      ]
    });

    this.labelValidationOnChange().subscribe();
    this.descriptionValidationOnChange().subscribe();

    this.form
      .get(FieldType.Description)
      .valueChanges.pipe(tap(this.onValueChanges.bind(this)))
      .subscribe();
  }

  /**
   * Handles input limitations.
   *
   * @param value The new value of the input field
   */
  protected onValueChanges(value: string): void {
    if (!this.canValidateDescription && this.descriptionLineLimit) {
      this.applyCustomWordwrap(value);
    }

    this.adjustTextareaHeight(this.textarea?.textarea);
  }

  protected applyCustomWordwrap(value: string): void {
    this.caretPosition = this.textarea?.textarea.nativeElement.selectionStart || 0;
    value = this.customWordWrap(value, this.descriptionLineLimit);
    this.form.get('description').patchValue(value, { emitEvent: false });
    this.setCaretPosition(this.textarea?.textarea.nativeElement, this.caretPosition);
  }

  protected setCaretPosition(input: HTMLInputElement, caretPosition: number): void {
    this.setSelectionRange(input, caretPosition, caretPosition);
  }

  protected setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
      const range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', selectionEnd);
      range.moveStart('character', selectionStart);
      range.select();
    }
  }

  protected customWordWrap(value: string, lineLength: number): string {
    // Make sure the last line (descriptionRowsLimit) is not longer than the line limitation
    let lines = value.split('\n');
    const lastLineIndex = this.descriptionRowsLimit - 1;
    if (lines[lastLineIndex]) {
      lines[lastLineIndex] = lines[lastLineIndex].substr(0, this.descriptionLineLimit);
      value = lines.join('\n');
    }

    value = value.replace(new RegExp(`(?![^\\n]{1,${lineLength}}$)([^\\n]{1,${lineLength}})\\s`, 'g'), '$1\n');

    // limit the number of lines
    lines = value.split('\n', this.descriptionRowsLimit);
    value = lines.join('\n');

    return value;
  }

  protected descriptionValidationOnChange(): Observable<any> {
    const descriptionInput = this.form.get(FieldType.Description);
    let previous = descriptionInput.value;

    return descriptionInput.valueChanges.pipe(
      tap(description => {
        let newDescription = description;

        if (descriptionInput.errors && descriptionInput.errors.pattern) {
          newDescription = previous;
        }

        if (newDescription !== description) {
          descriptionInput.setValue(newDescription, { emitEvent: false });
        }

        previous = newDescription;
      }),
      takeUntil(this.destroy$)
    );
  }

  protected labelValidationOnChange(): Observable<any> {
    const labelInput = this.form.get(FieldType.Label);
    let previous = labelInput.value;

    return labelInput.valueChanges.pipe(
      tap(label => {
        let newLabel = label;

        if (this.labelAllCaps) {
          newLabel = label.toUpperCase();
        }

        if (labelInput.errors && labelInput.errors.pattern) {
          newLabel = previous;
        }

        if (newLabel !== label) {
          labelInput.setValue(newLabel, { emitEvent: false });
        }

        previous = newLabel;
      }),
      takeUntil(this.destroy$)
    );
  }

  /**
   * Handles `change` events.
   * Controls the state of the `save` button
   *
   * @returns void
   */
  protected onNgModelChange(): void {
    this.determineSaveButtonState();
  }

  /**
   * Changes `save` button state between enable/disabled.
   *
   * @returns void
   */
  protected determineSaveButtonState(): void {
    let hasChangedField = false;
    // by default consider it enabled
    this.canSave = this.form.valid;
    for (const field of Object.keys(this.form.controls)) {
      // when at least one field value is required and empty - disable saving
      if (this.isFieldRequiredAndEmpty(field)) {
        this.canSave = false;
      }

      // ToDo: Check if this can be achieved with the `dirty` flag of FormGroup
      // determine if at least one form field has changed, but ignore surrounding empty spaces
      if (this.form.controls[field].value.trim() !== this.oldValues[field].trim()) {
        hasChangedField = true;
      }
    }

    if (!hasChangedField) {
      this.canSave = false;
    }
  }

  protected isActionDisabled(searchedAction: string): boolean {
    const canDisableAction = this.actionsThatCanBeDisabled.some(action => action === searchedAction);

    return this.hasDisabledActions && canDisableAction;
  }

  protected isFieldRequiredAndEmpty(searchedField: string): boolean {
    const isSearchedFiledRequired = this.requiredFields.some(field => field === searchedField);
    const isFiledValueEmpty = !this.form.controls[searchedField].value;

    return isSearchedFiledRequired && isFiledValueEmpty;
  }

  /**
   * Trim the input values of the form input controls.
   *
   * @returns void
   */
  protected trimInputs(): void {
    Object.keys(this.form.controls).forEach(field =>
      this.form.controls[field].setValue(this.form.controls[field].value.trim(), { emitEvent: false })
    );
  }
}
