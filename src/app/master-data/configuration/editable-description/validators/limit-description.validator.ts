import { FormControl, ValidatorFn } from '@angular/forms';

export function limitDescriptionValidator(
  candValidate: boolean,
  maxLength: number,
  rowsLimit: number,
  charactersLimit: number
): ValidatorFn {
  return (control: FormControl): { [key: string]: { [key: string]: any } } | null => {
    if (!candValidate || (!maxLength && !charactersLimit && !rowsLimit) || !control.value) {
      return null;
    }

    return getError(control, maxLength, rowsLimit, charactersLimit);
  };
}

function getError(
  control: FormControl,
  maxLength: number,
  rowsLimit: number,
  charactersLimit: number
): { [key: string]: { [key: string]: any } } | null {
  const lines = control.value.split('\n');
  const errorMessage = getLimitMessage(rowsLimit, charactersLimit);
  let error = null;

  if (
    (rowsLimit && lines.length > rowsLimit) ||
    (charactersLimit && lines.some(line => line.length > charactersLimit))
  ) {
    error = {
      descriptionLimit: errorMessage
    };
  }

  if (maxLength && !rowsLimit && !charactersLimit && control.value.length > maxLength) {
    error = {
      descriptionLimit: { key: 'editableDescription.lengthLimitError', characters: maxLength }
    };
  }

  return error;
}

function getLimitMessage(rowsLimit: number, charactersLimit: number): { [key: string]: any } {
  if (charactersLimit && rowsLimit) {
    return { key: 'editableDescription.limitError', lines: rowsLimit, characters: charactersLimit };
  }

  if (rowsLimit) {
    return { key: 'editableDescription.lineLimitError', lines: rowsLimit };
  }

  return { key: 'editableDescription.charactersLimitError', characters: charactersLimit };
}
