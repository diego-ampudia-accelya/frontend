import { FormControl } from '@angular/forms';

import { limitDescriptionValidator } from './limit-description.validator';

describe('limitDescriptionValidator', () => {
  let rowsLimit;
  let charactersLimit;
  let maxLength;

  describe('with rowsLimit and charactersLimit', () => {
    const incorrectValue = `1234567891234567891234567891234567891234567890
  1234567891234567891234567891234567891234567890
  1234567891234567891234567891234567891234567890
  1234567891234567891234567891234567891234567890
  1234567891234567891234567891234567891234567890
  1234567891234567891234567891234567891234567890`;

    it('should return null error when validation is forbidden', () => {
      rowsLimit = 5;
      charactersLimit = 45;
      maxLength = rowsLimit * charactersLimit;
      const result = new FormControl(`abc`, [limitDescriptionValidator(false, maxLength, rowsLimit, charactersLimit)]);

      expect(result.errors).toBeNull();
    });

    it('should return null error when no limits', () => {
      rowsLimit = 0;
      charactersLimit = 0;
      maxLength = rowsLimit * charactersLimit;
      const result = new FormControl(`abc`, [limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)]);

      expect(result.errors).toBeNull();
    });

    it('should return null error when control value is empty', () => {
      rowsLimit = 5;
      charactersLimit = 45;
      maxLength = rowsLimit * charactersLimit;
      const result = new FormControl(null, [limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)]);

      expect(result.errors).toBeNull();
    });

    it('should return error with rowsLimit message when charactersLimit is empty', () => {
      rowsLimit = 5;
      charactersLimit = 0;
      maxLength = rowsLimit * charactersLimit;
      const result = new FormControl(incorrectValue, [
        limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)
      ]);

      expect(result.errors).toEqual({
        descriptionLimit: { key: 'editableDescription.lineLimitError', lines: 5 }
      });
    });

    it('should return error with charactersLimit message when rowsLimit is empty', () => {
      rowsLimit = 0;
      charactersLimit = 45;
      maxLength = rowsLimit * charactersLimit;
      const result = new FormControl(incorrectValue, [
        limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)
      ]);

      expect(result.errors).toEqual({
        descriptionLimit: { key: 'editableDescription.charactersLimitError', characters: 45 }
      });
    });

    it('should return error with rowsLimit and charactersLimit message when both limits are set', () => {
      rowsLimit = 5;
      charactersLimit = 45;
      maxLength = rowsLimit * charactersLimit;
      const result = new FormControl(incorrectValue, [
        limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)
      ]);

      expect(result.errors).toEqual({
        descriptionLimit: { key: 'editableDescription.limitError', lines: 5, characters: 45 }
      });
    });

    it('should return no errors when control value correspond to limits', () => {
      rowsLimit = 5;
      charactersLimit = 45;
      maxLength = rowsLimit * charactersLimit;
      const result = new FormControl('1234567890', [
        limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)
      ]);

      expect(result.errors).toBeNull();
    });
  });

  describe('with max length only', () => {
    it('should return error with max length message when only maxLength was set', () => {
      rowsLimit = null;
      charactersLimit = null;
      maxLength = 10;
      const result = new FormControl('1234567890 1', [
        limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)
      ]);

      expect(result.errors).toEqual({
        descriptionLimit: { key: 'editableDescription.lengthLimitError', characters: maxLength }
      });
    });

    it('should return no errors when control value correspond to max length', () => {
      rowsLimit = null;
      charactersLimit = null;
      maxLength = 10;
      const result = new FormControl('1234567890', [
        limitDescriptionValidator(true, maxLength, rowsLimit, charactersLimit)
      ]);

      expect(result.errors).toBeNull();
    });
  });
});
