import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { MockComponent } from 'ng-mocks';
import { EditableDescription } from '../models/editable-description.model';

import { EditableDescriptionComponent } from './editable-description.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { TextareaComponent } from '~app/shared/components';

describe('EditableDescriptionComponent', () => {
  let component: EditableDescriptionComponent;
  let fixture: ComponentFixture<EditableDescriptionComponent>;

  const translationServiceSpy = jasmine.createSpyObj('L10nTranslationService', ['translate']);
  translationServiceSpy.translate.and.returnValue('translated');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableDescriptionComponent, MockComponent(TextareaComponent)],
      providers: [{ provide: L10nTranslationService, useValue: translationServiceSpy }],
      imports: [FormsModule, ReactiveFormsModule, L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableDescriptionComponent);
    component = fixture.componentInstance;
    component.descriptionItem = {
      description: '123',
      id: 121,
      parentId: 10001,
      sequenceNumber: 1,
      label: 'test'
    };
    // TODO: The textarea ViewChild is always undefined in the tests and they fail.
    // For now just stub the adjustTextareaHeight method.
    spyOn(component, 'adjustTextareaHeight').and.callFake(() => null);
    spyOn(component as any, 'setCaretPosition').and.callFake(() => null);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle edition mode', () => {
    component.editMode = false;

    component.toggleEditMode();

    expect(component.editMode).toBe(true);
  });

  it('should emit descriptionItem when you delete the item', fakeAsync(() => {
    spyOn(component.deleteRequest, 'emit');

    component.actionDelete();
    tick();

    expect(component.deleteRequest.emit).toHaveBeenCalledWith(component.descriptionItem);
  }));

  it('should call toggleEditMode when you choose action edit and editMode is not enabled ', () => {
    component.editMode = false;
    spyOn(component, 'toggleEditMode');
    spyOn(component, 'onItemChange');
    component.textarea = {} as any;

    component.actionEdit();

    expect(component.toggleEditMode).toHaveBeenCalled();
  });

  it('should not call toggleEditMode when you choose action edit and editMode is enabled ', () => {
    component.editMode = true;
    spyOn(component, 'toggleEditMode');

    component.actionEdit();

    expect(component.toggleEditMode).not.toHaveBeenCalled();
  });

  it('should call correct action method when you call onClick', () => {
    const event = {
      target: {
        attributes: {
          'data-title': {
            value: 'delete'
          }
        }
      }
    };
    spyOn(component, 'actionDelete');

    component.onClick(event);

    expect(component.actionDelete).toHaveBeenCalled();
  });

  it('should delete item when click cancel and description item is new and have no id', () => {
    component.descriptionItem.id = null;

    spyOn(component, 'actionDelete');

    component.actionCancel();

    expect(component.actionDelete).toHaveBeenCalled();
  });

  it('should call toggleEditMode when click cancel and description item is new and have no id', () => {
    component.descriptionItem.id = null;

    spyOn(component, 'toggleEditMode');

    component.actionCancel();

    expect(component.toggleEditMode).toHaveBeenCalled();
  });

  it('should call toggleEditMode when click cancel', () => {
    component.editMode = true;

    spyOn(component, 'toggleEditMode');

    component.actionCancel();

    expect(component.toggleEditMode).toHaveBeenCalled();
  });

  it('should emit saveRequest when click save', () => {
    component.canSave = true;

    spyOn(component.saveRequest, 'emit');
    spyOn(component as any, 'trimInputs');

    component.actionSave();

    const exepectedResult: EditableDescription = {
      id: 121,
      sequenceNumber: 1,
      label: 'test',
      description: '123',
      parentId: 10001
    };

    expect(component.saveRequest.emit).toHaveBeenCalledWith(exepectedResult);
  });

  it('should not  emit saveRequest when click save', () => {
    component.canSave = false;

    spyOn(component.saveRequest, 'emit');
    spyOn(component as any, 'trimInputs');

    component.actionSave();

    expect(component.saveRequest.emit).not.toHaveBeenCalled();
  });

  it('should set correct descriptionMaxLength when initializing the component', () => {
    component.descriptionMaxLength = 0;
    component.descriptionRowsLimit = 2;
    component.descriptionLineLimit = 3;

    component.ngOnInit();

    expect(component.descriptionMaxLength).toBe(6);
  });
});
