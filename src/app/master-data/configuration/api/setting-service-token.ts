import { InjectionToken } from '@angular/core';

import { SettingsService } from './settings-service';

export const SETTINGS_SERVICE = new InjectionToken<SettingsService<any>>('Settings Service');
