import { Observable } from 'rxjs';

import { ConfigurationScope } from '../models/configuration-scope.model';

export interface SettingsService<T> {
  scopeType: string;

  get(scope: ConfigurationScope): Observable<T>;
  save(model: T): Observable<T>;
}
