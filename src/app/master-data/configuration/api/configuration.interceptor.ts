import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash';
import { Observable, of } from 'rxjs';

import {
  acdmBasicBspParameters,
  acdmBspConfigurationDescriptor,
  pbdConfigurationDescriptor,
  pdbParameters
} from '../mocks/bsp-configuration.mock';

@Injectable()
export class ConfigurationInterceptor implements HttpInterceptor {
  private configurationDescriptorMapper = {
    'acdm-management/bsps/[0-9]+/configuration/descriptors$': acdmBspConfigurationDescriptor,
    'acdm-management/bsps/[0-9]+/configuration$': acdmBasicBspParameters,
    'pbd-management/bsps/[0-9]+/configuration/descriptors$': pbdConfigurationDescriptor,
    'pbd-management/bsps/[0-9]+/configuration$': pdbParameters
  };

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const foundedConfiguration = Object.keys(this.configurationDescriptorMapper).find(conf => {
      const confRegex = new RegExp(conf);

      return confRegex.test(request.url);
    });

    if (request.method === 'GET' && foundedConfiguration) {
      const mockedConfiguration = this.configurationDescriptorMapper[foundedConfiguration];

      return of(new HttpResponse({ status: 200, body: mockedConfiguration }));
    }

    if (request.method === 'PUT' && foundedConfiguration) {
      this.configurationDescriptorMapper[foundedConfiguration] = cloneDeep(request.body);

      return of(new HttpResponse({ status: 200, body: cloneDeep(request.body) }));
    }

    return next.handle(request);
  }
}
