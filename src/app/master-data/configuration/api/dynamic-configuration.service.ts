import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { isArray } from 'lodash';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ParameterType } from '../enum/parameter-type.enum';
import { ConfigurationDescriptor } from '../models/configuration-descriptor.model';
import { Configuration } from '../models/configuration.model';
import { Parameter } from '../models/parameters.model';
import { tryParseBoolean } from '~app/shared/utils';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class DynamicConfigurationService {
  private baseUrl = this.appConfiguration.baseApiPath;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private translationService: L10nTranslationService
  ) {}

  public getConfigurationDescriptors(
    service: string,
    scopeType: string,
    scopeId: string,
    scopeConfigName = 'configuration'
  ): Observable<ConfigurationDescriptor> {
    const url = `${this.baseUrl}/${service}/${scopeType}/${scopeId}/${scopeConfigName}/descriptors`;
    const locale = this.translationService.getLocale();

    return this.http.get<ConfigurationDescriptor>(url, {
      headers: {
        'Accept-Language': locale.language || 'en-US'
      }
    });
  }

  public getConfigurationValues(
    service: string,
    scopeType: string,
    scopeId: string,
    scopeConfigName = ''
  ): Observable<Configuration> {
    const url = `${this.baseUrl}/${service}/${scopeType}/${scopeId}${
      scopeConfigName ? '/' + scopeConfigName : ''
    }/configuration`;

    return this.http
      .get<Configuration>(url)
      .pipe(tap(config => (config.parameters = this.parseParameters(config.parameters))));
  }

  public save(
    service: string,
    scopeType: string,
    scopeId: string,
    configuration: Configuration,
    scopeConfigName = ''
  ): Observable<Configuration> {
    const url = `${this.baseUrl}/${service}/${scopeType}/${scopeId}${
      scopeConfigName ? '/' + scopeConfigName : ''
    }/configuration`;
    configuration = {
      ...configuration,
      parameters: this.stringifyParameters(configuration.parameters)
    };

    return this.http
      .put<Configuration>(url, configuration)
      .pipe(tap(config => (config.parameters = this.parseParameters(config.parameters))));
  }

  private parseParameters(parameters: Parameter[]): Parameter[] {
    return parameters?.map(param => {
      const value = param.value;
      let parsedValue: any;

      if (param.type === ParameterType.Boolean) {
        parsedValue = tryParseBoolean(value);
      }

      return { ...param, value: parsedValue ?? value };
    });
  }

  private stringifyParameters(parameters: Parameter[]): Parameter[] {
    return parameters?.map(param => ({ ...param, value: this.stringifyValue(param.value) }));
  }

  private stringifyValue(value: any): string | string[] {
    if (isArray(value)) {
      return value.map(v => v.toString());
    }

    return value?.toString();
  }
}
