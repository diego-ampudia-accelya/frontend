import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ConfigurationScope } from '../models/configuration-scope.model';
import { NotificationService } from '~app/shared/services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class NotifierService {
  constructor(private notification: NotificationService, private translation: L10nTranslationService) {}

  public notifySaveSuccess(scope: ConfigurationScope) {
    const key = `LIST.MASTER_DATA.configuration.saveSuccess`;
    const message = this.translation.translate(key, { bspName: scope.name });

    this.notification.showSuccess(message);
  }

  public notifySaveError(scope: ConfigurationScope) {
    const key = `LIST.MASTER_DATA.configuration.error`;
    const message = this.translation.translate(key, { bspName: scope.name });

    this.notification.showError(message);
  }
}
