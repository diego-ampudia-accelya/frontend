import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { IpAddressConfigBE } from '../models/ip-address.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class IpAddressConfigurationService {
  private basePath = this.appConfiguration.baseApiPath;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public get(url: string): Observable<IpAddressConfigBE> {
    return this.http.get<IpAddressConfigBE>(`${this.basePath}/${url}`);
  }

  public create(url: string, ipAddressConfig: IpAddressConfigBE): Observable<IpAddressConfigBE> {
    return this.http.post<IpAddressConfigBE>(`${this.basePath}/${url}`, ipAddressConfig);
  }

  public save(url: string, ipAddressConfig: IpAddressConfigBE): Observable<IpAddressConfigBE> {
    return this.http.put<IpAddressConfigBE>(`${this.basePath}/${url}`, ipAddressConfig);
  }

  public delete(url: string): Observable<any> {
    return this.http.delete(`${this.basePath}/${url}`);
  }
}
