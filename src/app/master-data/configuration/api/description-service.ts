import { Observable } from 'rxjs';

import { EditableDescription } from '../models/editable-description.model';

export interface DescriptionService {
  get(parentId: number): Observable<EditableDescription[]>;
  delete(model: EditableDescription): Observable<void>;
  save(model: EditableDescription): Observable<EditableDescription>;
}
