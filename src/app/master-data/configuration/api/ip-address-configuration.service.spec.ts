import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { IpAddressConfigurationService } from './ip-address-configuration.service';
import { AppConfigurationService } from '~app/shared/services';

describe('IpAddressConfigurationService', () => {
  let service: IpAddressConfigurationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        IpAddressConfigurationService,
        { provide: AppConfigurationService, useValue: { baseApiPath: 'test' } }
      ]
    });
    service = TestBed.inject(IpAddressConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
