import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { ParameterType } from '../enum/parameter-type.enum';
import { Configuration } from '../models/configuration.model';
import { Parameter } from '../models/parameters.model';

import { DynamicConfigurationService } from './dynamic-configuration.service';
import { AppConfigurationService } from '~app/shared/services';

describe('DynamicConfigurationService', () => {
  let httpClientSpy: SpyObject<HttpClient>;
  let service: DynamicConfigurationService;

  beforeEach(() => {
    httpClientSpy = createSpyObject(HttpClient);

    TestBed.configureTestingModule({
      providers: [
        DynamicConfigurationService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppConfigurationService, useValue: { baseApiPath: '', baseUploadPath: '' } },
        mockProvider(L10nTranslationService)
      ]
    });

    service = TestBed.inject(DynamicConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getConfigurationDescriptors()', () => {
    it('should call http.get with the proper url without configiration name', fakeAsync(() => {
      const scope = {
        serviceName: 'my-service',
        scopeType: 'type',
        scopeId: 'my-id'
      } as any;
      const expectedUrl = `/my-service/type/my-id/configuration/descriptors`;
      httpClientSpy.get.and.returnValue(of({}));
      (service as any).translationService.getLocale = jasmine.createSpy().and.returnValue({});

      service
        .getConfigurationDescriptors(scope.serviceName, scope.scopeType, scope.scopeId, scope.scopeConfigName)
        .subscribe();

      const args = httpClientSpy.get.calls.first().args;
      expect(args[0]).toEqual(expectedUrl);
    }));

    it('should call http.get with the proper url with configuration name', fakeAsync(() => {
      const scope = {
        serviceName: 'my-service',
        scopeType: 'type',
        scopeId: 'my-id',
        scopeConfigName: 'my-configuration'
      } as any;
      const expectedUrl = `/my-service/type/my-id/my-configuration/descriptors`;
      httpClientSpy.get.and.returnValue(of({}));
      (service as any).translationService.getLocale = jasmine.createSpy().and.returnValue({});

      service
        .getConfigurationDescriptors(scope.serviceName, scope.scopeType, scope.scopeId, scope.scopeConfigName)
        .subscribe();

      const args = httpClientSpy.get.calls.first().args;
      expect(args[0]).toEqual(expectedUrl);
    }));
  });

  describe('getConfigurationValues()', () => {
    it('should call http.get with the proper url without configiration name', fakeAsync(() => {
      const scope = {
        serviceName: 'my-service',
        scopeType: 'type',
        scopeId: 'my-id'
      } as any;
      const expectedUrl = `/my-service/type/my-id/configuration`;

      httpClientSpy.get.and.returnValue(of({}));
      service
        .getConfigurationValues(scope.serviceName, scope.scopeType, scope.scopeId, scope.scopeConfigName)
        .subscribe();

      expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl);
    }));

    it('should call http.get with the proper url with configuration name', fakeAsync(() => {
      const scope = {
        serviceName: 'my-service',
        scopeType: 'type',
        scopeId: 'my-id',
        scopeConfigName: 'my-configuration'
      } as any;
      const expectedUrl = `/my-service/type/my-id/my-configuration/configuration`;

      httpClientSpy.get.and.returnValue(of({}));
      service
        .getConfigurationValues(scope.serviceName, scope.scopeType, scope.scopeId, scope.scopeConfigName)
        .subscribe();

      expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl);
    }));

    it('should parse different kind of parameters properly after the response is received', fakeAsync(() => {
      const expectedParams = [
        { type: ParameterType.Enum, value: 'val_0' },
        { type: ParameterType.Integer, value: '0' },
        { type: ParameterType.String, value: 'some string' }
      ] as Parameter[];
      const response = {
        parameters: [
          {
            type: ParameterType.Enum,
            value: 'val_0'
          },
          {
            type: ParameterType.Integer,
            value: '0'
          },
          {
            type: ParameterType.String,
            value: 'some string'
          }
        ]
      } as Configuration;
      let actualData: Configuration;
      httpClientSpy.get.and.returnValue(of(response));

      service.getConfigurationValues('', '', '').subscribe(data => (actualData = data));

      expect(actualData.parameters).toEqual(expectedParams);
    }));

    it('should parse boolean parameters properly after the response is received', fakeAsync(() => {
      const expectedParams = [{ type: ParameterType.Boolean, value: false }] as Parameter[];
      const response = {
        parameters: [
          {
            type: ParameterType.Boolean,
            value: 'false'
          }
        ]
      } as Configuration;
      let actualData: Configuration;
      httpClientSpy.get.and.returnValue(of(response));

      service.getConfigurationValues('', '', '').subscribe(data => (actualData = data));

      expect(actualData.parameters).toEqual(expectedParams);
    }));
  });

  describe('save()', () => {
    it('should call http.put with the proper url and data without configiration name', fakeAsync(() => {
      const scope = {
        serviceName: 'my-service',
        scopeType: 'type',
        scopeId: 'my-id'
      } as any;
      const data = { parameters: [] } as Configuration;
      const expectedUrl = `/my-service/type/my-id/configuration`;

      httpClientSpy.put.and.returnValue(of({}));
      service.save(scope.serviceName, scope.scopeType, scope.scopeId, data, scope.scopeConfigName).subscribe();

      expect(httpClientSpy.put).toHaveBeenCalledWith(expectedUrl, data);
    }));

    it('should call http.put with the proper url and data with configuration name', fakeAsync(() => {
      const scope = {
        serviceName: 'my-service',
        scopeType: 'type',
        scopeId: 'my-id',
        scopeConfigName: 'my-configuration'
      } as any;
      const data = { parameters: [] } as Configuration;
      const expectedUrl = `/my-service/type/my-id/my-configuration/configuration`;

      httpClientSpy.put.and.returnValue(of({}));
      service.save(scope.serviceName, scope.scopeType, scope.scopeId, data, scope.scopeConfigName).subscribe();

      expect(httpClientSpy.put).toHaveBeenCalledWith(expectedUrl, data);
    }));

    it('should parse boolean parameters properly after the response is received', fakeAsync(() => {
      const expectedParams = [{ type: ParameterType.Boolean, value: false }] as Parameter[];
      const response = {
        parameters: [
          {
            type: ParameterType.Boolean,
            value: 'false'
          }
        ]
      } as Configuration;
      let actualData: Configuration;
      httpClientSpy.put.and.returnValue(of(response));

      service.save('', '', '', {} as Configuration).subscribe(data => (actualData = data));

      expect(actualData.parameters).toEqual(expectedParams);
    }));

    it('should stringify the boolean parameters before sending a request', fakeAsync(() => {
      const config = { parameters: [{ type: ParameterType.Boolean, value: false }] } as Configuration;
      const expectedConfig = { parameters: [{ type: ParameterType.Boolean, value: 'false' }] } as Configuration;

      httpClientSpy.put.and.returnValue(of({}));

      service.save('a', 'b', 'c', config).subscribe();

      expect(httpClientSpy.put).toHaveBeenCalledWith('/a/b/c/configuration', expectedConfig);
    }));

    it('should stringify the STRING_LIST parameter when it`s empty before sending a request', fakeAsync(() => {
      const config = { parameters: [{ type: ParameterType.StringList, value: [] }] } as Configuration;
      const expectedConfig = { parameters: [{ type: ParameterType.StringList, value: [] }] } as Configuration;

      httpClientSpy.put.and.returnValue(of({}));

      service.save('a', 'b', 'c', config).subscribe();

      expect(httpClientSpy.put).toHaveBeenCalledWith('/a/b/c/configuration', expectedConfig);
    }));

    it('should stringify the STRING_LIST parameter when it has values before sending a request', fakeAsync(() => {
      const config = { parameters: [{ type: ParameterType.StringList, value: ['a', 'b'] }] } as Configuration;
      const expectedConfig = { parameters: [{ type: ParameterType.StringList, value: ['a', 'b'] }] } as Configuration;

      httpClientSpy.put.and.returnValue(of({}));

      service.save('a', 'b', 'c', config).subscribe();

      expect(httpClientSpy.put).toHaveBeenCalledWith('/a/b/c/configuration', expectedConfig);
    }));
  });
});
