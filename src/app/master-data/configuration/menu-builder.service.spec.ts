import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Route } from '@angular/router';
import { mockProvider, SpyObject } from '@ngneat/spectator';

import { MenuBuilder } from './menu-builder.service';
import { MENU } from './menu.constants';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Permissions } from '~app/shared/constants/permissions';
import { SubUserLevel } from '~app/sub-users/shared/sub-users.models';

describe('MenuBuilder', () => {
  let menuBuilder: MenuBuilder;
  let permissionSpy: SpyObject<PermissionsService>;

  const allowedItem: Route = {
    path: 'allowed-item',
    data: { title: 'Allowed Item', group: 'Test', requiredPermissions: Permissions.readAgent }
  };
  const forbiddenItem: Route = {
    path: 'forbidden-item',
    data: { title: 'Forbidden Item', requiredPermissions: Permissions.readAirline }
  };
  const incompatibleItem: Route = {
    path: 'incompatible-item'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MenuBuilder, mockProvider(PermissionsService)]
    });

    menuBuilder = TestBed.inject(MenuBuilder);
    permissionSpy = TestBed.inject<any>(PermissionsService);
  });

  it('should create', () => {
    expect(menuBuilder).toBeDefined();
  });

  describe('without subUser', () => {
    const activatedRoute = {
      data: {} as any,
      routeConfig: {
        children: [allowedItem, forbiddenItem, incompatibleItem]
      }
    } as ActivatedRouteSnapshot;

    it('should create menu items based on routing configuration with allowed route', () => {
      permissionSpy.hasPermission.and.callFake(permission => permission === Permissions.readAgent);

      const items = menuBuilder.buildMenuItemsFrom(activatedRoute);

      expect(items).toEqual([{ group: 'Test', title: 'Allowed Item', route: 'allowed-item', isAccessible: true }]);
    });

    it('should create empty menu items list because of permissions', () => {
      permissionSpy.hasPermission.and.callFake(permission => permission === Permissions.readGds);

      const items = menuBuilder.buildMenuItemsFrom(activatedRoute);

      expect(items.length).toEqual(0);
    });

    it('should check if the user has permission and hasPermission method has been called', () => {
      permissionSpy.hasPermission.and.callFake(permission => permission === Permissions.readAgent);

      menuBuilder.buildMenuItemsFrom(activatedRoute);

      expect(permissionSpy.hasPermission).toHaveBeenCalled();
    });
  });

  describe('with subUser', () => {
    const activatedRoute = {
      data: {
        subUser: {
          level: 1
        }
      } as any,
      routeConfig: {
        children: [allowedItem, forbiddenItem, incompatibleItem]
      }
    } as ActivatedRouteSnapshot;

    it('should create menu items with allowed when checkPermissionsTabAndSubUserLevel returns false', () => {
      spyOn<any>(menuBuilder, 'checkPermissionsTabAndSubUserLevel').and.returnValue(false);
      permissionSpy.hasPermission.and.callFake(permission => permission === Permissions.readAgent);

      const items = menuBuilder.buildMenuItemsFrom(activatedRoute);

      expect(items).toEqual([{ group: 'Test', title: 'Allowed Item', route: 'allowed-item', isAccessible: true }]);
    });

    it('should create empty menu items list when checkPermissionsTabAndSubUserLevel returns true', () => {
      spyOn<any>(menuBuilder, 'checkPermissionsTabAndSubUserLevel').and.returnValue(true);

      const items = menuBuilder.buildMenuItemsFrom(activatedRoute);

      expect(items.length).toEqual(0);
    });
  });

  describe('checkPermissionsTabAndSubUserLevel', () => {
    it('should return true when path is permissions and subUser level is NOT 1', () => {
      const child = {
        path: MENU.TABS.PERMISSIONS
      };
      const subUser = {
        level: SubUserLevel.level2
      };

      const result = menuBuilder['checkPermissionsTabAndSubUserLevel'](child, subUser);

      expect(result).toBe(true);
    });

    it('should return false when path is NOT permissions', () => {
      const child = {
        path: 'properties'
      };
      const subUser = {
        level: SubUserLevel.level2
      };

      const result = menuBuilder['checkPermissionsTabAndSubUserLevel'](child, subUser);

      expect(result).toBe(false);
    });

    it('should return false when path is permissions and subUser level is 1', () => {
      const child = {
        path: MENU.TABS.PERMISSIONS
      };
      const subUser = {
        level: SubUserLevel.level1
      };

      const result = menuBuilder['checkPermissionsTabAndSubUserLevel'](child, subUser);

      expect(result).toBe(false);
    });
  });
});
