import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { State } from '../store/reducers';
import { EmailAlertsSettingsComponent } from './email-alert-settings.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ParameterType } from '../enum/parameter-type.enum';

describe('EmailAlertsSettingsComponent', () => {
  let activatedRouteMock: any;
  let component: EmailAlertsSettingsComponent;
  let fixture: ComponentFixture<EmailAlertsSettingsComponent>;
  let initialState: State;

  beforeEach(waitForAsync(() => {
    activatedRouteMock = {
      snapshot: {
        data: {
          configuration: {
            title: 'Title'
          }
        }
      }
    };

    initialState = {
      router: null,
      configuration: {
        admPolicies: null,
        tipGlobalAirlines: null,
        ipAddressSettings: null,
        consentSettings: null,
        settings: {
          isLoading: false,
          isChanged: false,
          configurationDescriptor: null,
          originalConfigurationParameters: null,
          configurationParameters: null,
          scope: null
        }
      }
    };

    TestBed.configureTestingModule({
      declarations: [EmailAlertsSettingsComponent],
      imports: [HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({ initialState }),
        { provide: ActivatedRoute, useValue: activatedRouteMock },
        PermissionsService
      ]
    }).compileComponents();
  }));

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(EmailAlertsSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change Basic Setting Value', () => {
    const storeDispatchSpy = spyOn<any>(component['store'], 'dispatch');

    const param = {
      id: 12,
      descriptorId: 13,
      type: ParameterType.Boolean,
      name: 'name',
      value: true,
      readonly: false
    };
    const change = {
      parameter: param,
      isFormValid: true
    };

    component.changeBasicSettingValue(change);
    expect(storeDispatchSpy).toHaveBeenCalled();
  });
});
