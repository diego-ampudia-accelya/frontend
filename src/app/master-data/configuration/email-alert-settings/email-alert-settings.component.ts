import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { Parameter } from '../models/parameters.model';
import { SettingConfigurationActions } from '../store/actions';
import * as fromConfiguration from '../store/reducers';
import { SettingAlertMessage } from '../setting-list/models/setting-alert-message.model';
import { AppState } from '~app/reducers';
import { AlertMessageType } from '~app/shared/enums';
import { ActivatedRoute } from '@angular/router';
import { PermissionsService } from '~app/auth/services/permissions.service';

@Component({
  selector: 'bspl-email-settings',
  templateUrl: './email-alert-settings.component.html',
  styleUrls: ['./email-alert-settings.component.scss']
})
export class EmailAlertsSettingsComponent implements OnInit {
  public configurationDescriptor$ = this.store.pipe(select(fromConfiguration.getConfigurationDescriptor));
  public configurationParameters$ = this.store.pipe(select(fromConfiguration.getConfigurationParameters));
  public disableAllActions = false;

  public stringListSeparators$: Observable<string[]>;

  public configuration;

  public isLoading$ = this.store.pipe(
    select(fromConfiguration.getSettingsState),
    map(state => state.isLoading)
  );

  public settingAlertMessages: SettingAlertMessage[];

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.setStringListSeparators();
    this.getDisableAllActions();
    this.initializeSettingAlertMessages();
  }

  public changeBasicSettingValue(change: { parameter: Parameter; isFormValid: boolean }): void {
    this.store.dispatch(SettingConfigurationActions.modify(change));
  }

  private setStringListSeparators() {
    // TODO: Right now only 'Email alert' is using tags input. In the future we might need
    // to configure the special separators on BE side.
    this.stringListSeparators$ = this.configurationDescriptor$.pipe(
      filter(d => d.displayName != null),
      map(descriptor => {
        if (descriptor.displayName.toLowerCase() === 'email alert') {
          return [' ', ',', ';'];
        }

        return null;
      })
    );
  }

  private getDisableAllActions() {
    const permission = this.activatedRoute.snapshot.data.disableAllPermission;
    this.disableAllActions = !this.permissionsService.hasPermission(permission);
  }

  private initializeSettingAlertMessages(): void {
    const alertEmailEntered: SettingAlertMessage = {
      message: 'menu.masterData.configuration.emailSettings.emailEntered',
      type: AlertMessageType.info,
      calculateVisibility: (categories, formValue) =>
        // To be visible, at least one parameter has a value
        categories.some(category => category.parameters.some(parameter => !!formValue[parameter.id]?.length))
    };

    const alertNoEmailEntered: SettingAlertMessage = {
      message: 'menu.masterData.configuration.emailSettings.noEmailEntered',
      type: AlertMessageType.info,
      calculateVisibility: (categories, formValue) =>
        // To be visible, every parameter has to have no value
        categories.every(category => category.parameters.every(parameter => !formValue[parameter.id]?.length))
    };

    this.settingAlertMessages = [alertEmailEntered, alertNoEmailEntered];
  }
}
