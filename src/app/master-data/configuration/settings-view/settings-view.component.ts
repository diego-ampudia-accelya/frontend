import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';

import { Parameter } from '../models/parameters.model';
import { SettingConfigurationActions } from '../store/actions';
import * as fromConfiguration from '../store/reducers';
import { AppState } from '~app/reducers';
import { ActivatedRoute } from '@angular/router';
import { PermissionsService } from '~app/auth/services/permissions.service';

@Component({
  selector: 'bspl-settings-view',
  templateUrl: './settings-view.component.html',
  styleUrls: ['./settings-view.component.scss']
})
export class SettingsViewComponent implements OnInit {
  public disableAllActions = false;
  public configurationDescriptor$ = this.store.pipe(select(fromConfiguration.getConfigurationDescriptor));
  public configurationParameters$ = this.store.pipe(select(fromConfiguration.getConfigurationParameters));

  public configuration;

  public isLoading$ = this.store.pipe(
    select(fromConfiguration.getSettingsState),
    map(state => state.isLoading)
  );

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private permissionsService: PermissionsService
  ) {}

  public changeBasicSettingValue(change: { parameter: Parameter; isFormValid: boolean }): void {
    this.store.dispatch(SettingConfigurationActions.modify(change));
  }

  public ngOnInit(): void {
    this.getDisableAllActions();
  }

  private getDisableAllActions() {
    const permission = this.activatedRoute.snapshot.data.disableAllPermission;
    this.disableAllActions = !this.permissionsService.hasPermission(permission);
  }
}
