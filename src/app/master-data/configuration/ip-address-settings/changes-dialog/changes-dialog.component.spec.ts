import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MockComponents } from 'ng-mocks';

import { IpAddressChangesDialogComponent } from './changes-dialog.component';
import { CheckboxComponent, DialogConfig } from '~app/shared/components';
import { AlertMessageComponent } from '~app/shared/components/alert-message/alert-message.component';
import { PillInformationComponent } from '~app/shared/components/pill-information/pill-information.component';
import { TranslatePipeMock } from '~app/test';

describe('IpAddressChangesDialogComponent', () => {
  let component: IpAddressChangesDialogComponent;
  let fixture: ComponentFixture<IpAddressChangesDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        IpAddressChangesDialogComponent,
        TranslatePipeMock,
        MockComponents(CheckboxComponent, AlertMessageComponent, PillInformationComponent)
      ],
      providers: [{ provide: DialogConfig, useValue: {} }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpAddressChangesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
