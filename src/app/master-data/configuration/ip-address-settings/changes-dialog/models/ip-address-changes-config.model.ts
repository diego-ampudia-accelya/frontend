import { DialogConfig } from '~app/shared/components';

export interface IpAddressChangesDialogConfig extends DialogConfig {
  changes: string[];
  message: string;
  hasConfirmation: boolean;
  confirmationMessage: string;
  hasInvalidChanges?: boolean;
}
