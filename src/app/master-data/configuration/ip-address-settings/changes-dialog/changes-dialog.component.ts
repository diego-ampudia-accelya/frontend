import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { IpAddressChangesDialogConfig } from './models/ip-address-changes-config.model';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { AlertMessageType } from '~app/shared/enums';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-ip-address-changes-dialog',
  templateUrl: './changes-dialog.component.html',
  styleUrls: ['./changes-dialog.component.scss']
})
export class IpAddressChangesDialogComponent implements OnInit, OnDestroy {
  public items: string[] = this.config.changes;

  public AlertMessageType = AlertMessageType;

  public hasConfirmation = this.config.hasConfirmation;
  public confirmControl = new FormControl(null, Validators.requiredTrue);

  private applyButton: ModalAction;
  private destroy$ = new Subject();

  constructor(@Inject(DialogConfig) public config: IpAddressChangesDialogConfig) {}

  public ngOnInit(): void {
    this.applyButton = this.config.data?.buttons?.find((b: ModalAction) => b.type === FooterButton.Apply);

    if (this.applyButton) {
      this.setDisabledForAppyButton(this.hasConfirmation);

      if (this.hasConfirmation) {
        this.subscribeToControlChanges();
      }
    }
  }

  private setDisabledForAppyButton(hasConfirmation = true): void {
    const notConfirmed = hasConfirmation && this.confirmControl.invalid;

    this.applyButton.isDisabled = this.config.hasInvalidChanges || notConfirmed;
  }

  private subscribeToControlChanges(): void {
    this.confirmControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.setDisabledForAppyButton();
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
