import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import * as fromIpAddressConfiguration from '../../../store/reducers';
import { IpAddressChangesDialogConfigService } from './changes-dialog-config.service';
import { IpAddressChangesDialogService } from './changes-dialog.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { IpAddressConfigurationService } from '~app/master-data/configuration/api/ip-address-configuration.service';
import { IpAddressConfig } from '~app/master-data/configuration/models/ip-address.model';
import { DialogService } from '~app/shared/components';

const configuration: IpAddressConfig = {
  ipType: null,
  ips: [null, null, null],
  range: null
};

describe('IpAddressChangesDialogService', () => {
  let service: IpAddressChangesDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        IpAddressChangesDialogService,
        provideMockStore({
          initialState: {},
          selectors: [
            { selector: fromIpAddressConfiguration.getIpAddressConfiguration, value: configuration },
            { selector: fromIpAddressConfiguration.getIpSettingsModifications, value: [] },
            { selector: fromIpAddressConfiguration.getIpSettingsIsInvalid, value: true },
            { selector: fromIpAddressConfiguration.getIpSettingsUrl, value: '' },
            { selector: fromIpAddressConfiguration.getIpSettingsIsExisting, value: false },
            { selector: getUser, value: { ip: '' } }
          ]
        }),
        {
          provide: IpAddressChangesDialogConfigService,
          useValue: {
            build: () => ({}),
            getMappedValueForBackend: () => ({})
          }
        },
        {
          provide: DialogService,
          useValue: {
            build: () => of({}),
            close: () => of({})
          }
        },
        {
          provide: IpAddressConfigurationService,
          useValue: {
            create: () => of({}),
            save: () => of({})
          }
        }
      ]
    });
    service = TestBed.inject(IpAddressChangesDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
