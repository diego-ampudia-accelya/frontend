import { TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { IpAddressRemoveDialogConfigService } from './remove-dialog-config.service';

describe('IpAddressRemoveDialogConfigService', () => {
  let service: IpAddressRemoveDialogConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        IpAddressRemoveDialogConfigService,
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => ''
          }
        }
      ]
    });
    service = TestBed.inject(IpAddressRemoveDialogConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
