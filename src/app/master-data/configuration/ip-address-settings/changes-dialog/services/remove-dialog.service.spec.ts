import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import * as fromIpAddressConfiguration from '../../../store/reducers';

import { IpAddressRemoveDialogConfigService } from './remove-dialog-config.service';
import { IpAddressRemoveDialogService } from './remove-dialog.service';
import { DialogService } from '~app/shared/components';
import { IpAddressConfigurationService } from '~app/master-data/configuration/api/ip-address-configuration.service';

describe('IpAddressRemoveDialogService', () => {
  let service: IpAddressRemoveDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        IpAddressRemoveDialogService,
        provideMockStore({
          initialState: {},
          selectors: [
            { selector: fromIpAddressConfiguration.getIpSettingsUrl, value: '' },
            { selector: fromIpAddressConfiguration.getIpSettingsIsExisting, value: false }
          ]
        }),
        {
          provide: DialogService,
          useValue: {
            open: () => of({}),
            close: () => of({})
          }
        },
        {
          provide: IpAddressRemoveDialogConfigService,
          useValue: {
            build: () => ({})
          }
        },
        {
          provide: IpAddressConfigurationService,
          useValue: {
            delete: () => of({})
          }
        }
      ]
    });
    service = TestBed.inject(IpAddressRemoveDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
