import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { IpAddressChange, IpAddressConfig, IpAddressConfigBE, IpRange } from '../../../models/ip-address.model';
import { IpType } from '../../../models/ip-type.model';
import { getIsLessThanEndInRange } from '../../utils/is-less-than-end-ip';
import { IpAddressChangesDialogConfig } from '../models/ip-address-changes-config.model';
import { ButtonDesign, FooterButton } from '~app/shared/components';

@Injectable()
export class IpAddressChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    config: IpAddressConfig,
    modifications: IpAddressChange[],
    question: { title: string; details: string },
    isInvalidState: boolean,
    currentUserIp: string
  ): IpAddressChangesDialogConfig {
    const confirmationMessage = this.getConfirmationMessage(config, currentUserIp);

    return {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: this.formatChanges(modifications),
      hasInvalidChanges: isInvalidState,
      message: this.translation.translate(question.details),
      hasConfirmation: !!confirmationMessage,
      confirmationMessage
    };
  }

  public getMappedValueForBackend(value: IpAddressConfig): IpAddressConfigBE | null {
    if (value.ipType === IpType.Permanent) {
      return { ips: value.ips, range: null };
    }

    if (value.ipType === IpType.Range) {
      return { range: value.range, ips: null };
    }

    return null;
  }

  private formatChanges(modifications: IpAddressChange[]): string[] {
    return modifications.map((change: IpAddressChange) => this.toChangeModel(change));
  }

  private toChangeModel({ label, value, index }: IpAddressChange): string {
    const mappedLabel = this.getMappedLabel(label, index);
    const gap = mappedLabel && value ? ': ' : '';

    return `${mappedLabel}${gap}${value || ''}`;
  }

  private getMappedLabel(label: string, index: number): string {
    if (!label) {
      return '';
    }

    return `${this.translation.translate(label, index ? { index } : null)}`;
  }

  private getConfirmationMessage(config: IpAddressConfig, currentUserIp: string): string {
    // none of the three IP addresses is the one that used for the current connection
    if (config.ipType === IpType.Permanent && this.shouldShowConfirmationMsgForPermanentIp(config.ips, currentUserIp)) {
      return 'menu.masterData.configuration.ipSettings.changesDialog.template.labels.confirmPermanentIpsText';
    }

    // IP address that I use for the current connection is not in the configured range
    if (config.ipType === IpType.Range && !this.isCurrentIpInRange(config.range, currentUserIp)) {
      return 'menu.masterData.configuration.ipSettings.changesDialog.template.labels.confirmRangeIpsText';
    }

    return null;
  }

  private shouldShowConfirmationMsgForPermanentIp(ips: string[], currentUserIp: string): boolean {
    if (!currentUserIp) {
      return true;
    }

    const mappedIps = ips.filter(Boolean);
    if (!mappedIps.length) {
      return false;
    }

    return !this.isCurrentIpIncludedInPermanentIps(mappedIps, currentUserIp);
  }

  private isCurrentIpIncludedInPermanentIps(ips: string[], currentUserIp: string): boolean {
    const separator = '.';
    const currentIpArray = currentUserIp.split(separator).map(Number);

    return ips.some(configIp => {
      const mappedIp = configIp.split(separator).map(Number);

      return mappedIp.every((value: number, index: number) => value === currentIpArray[index]);
    });
  }

  private isCurrentIpInRange({ beginIp, endIp }: IpRange, currentUserIp: string): boolean {
    if (!currentUserIp || !beginIp || !endIp) {
      return false;
    }

    const separator = '.';
    const beginArray = beginIp.split(separator).map(Number);
    const endArray = endIp.split(separator).map(Number);
    const currentIpArray = currentUserIp.split(separator).map(Number);

    return (
      this.getIsMoreThanStartInRange(currentIpArray, beginArray) && getIsLessThanEndInRange(currentIpArray, endArray)
    );
  }

  private getIsMoreThanStartInRange(currentIpArray: number[], beginArray: number[]): boolean {
    let moreThanBegin = true;

    for (let index = 0; index < currentIpArray.length; index++) {
      const currentValue = currentIpArray[index];
      const startValue = beginArray[index];

      if (currentValue > startValue) {
        break;
      }

      if (currentValue < startValue) {
        moreThanBegin = false;
        break;
      }
    }

    return moreThanBegin;
  }
}
