import { TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { IpAddressChangesDialogConfigService } from './changes-dialog-config.service';

describe('IpAddressChangesDialogConfigService', () => {
  let service: IpAddressChangesDialogConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        IpAddressChangesDialogConfigService,
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => ''
          }
        }
      ]
    });
    service = TestBed.inject(IpAddressChangesDialogConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('shouldShowConfirmationMsgForPermanentIp', () => {
    it('should return true when current ip is not defined', () => {
      const res = (service as any).shouldShowConfirmationMsgForPermanentIp(['111.111.111.111', '255.255.255.255'], '');

      expect(res).toBe(true);
    });

    it('should return false when permanent ips list is empty', () => {
      const res = (service as any).shouldShowConfirmationMsgForPermanentIp([], '111.111.222.111');

      expect(res).toBe(false);
    });
    it('should return true when current ip is not included', () => {
      const res = (service as any).shouldShowConfirmationMsgForPermanentIp(
        ['111.111.111.111', '255.255.255.255'],
        '111.111.222.111'
      );

      expect(res).toBe(true);
    });

    it('should return false when current ip is included', () => {
      const res = (service as any).shouldShowConfirmationMsgForPermanentIp(
        ['011.89.42.100', '11.089.42.100', '11.089.042.100'],
        '11.89.42.100'
      );

      expect(res).toBe(false);
    });
  });

  describe('isCurrentIpInRange', () => {
    it('should return false when current ip is not defined', () => {
      const res = (service as any).isCurrentIpInRange({ beginIp: '111.111.111.111', endIp: '255.255.255.255' }, '');

      expect(res).toBe(false);
    });

    it('should return false when current ip is less than begin ip', () => {
      const res = (service as any).isCurrentIpInRange(
        { beginIp: '222.111.111.111', endIp: '255.255.255.255' },
        '111.222.222.222'
      );

      expect(res).toBe(false);
    });

    it('should return false when current ip is more than end ip', () => {
      const res = (service as any).isCurrentIpInRange(
        { beginIp: '012.0.42.100', endIp: '011.89.042.100' },
        '11.89.42.100'
      );

      expect(res).toBe(false);
    });

    it('should return true when current ip is in range', () => {
      const res = (service as any).isCurrentIpInRange(
        { beginIp: '011.89.042.100', endIp: '012.0.42.100' },
        '11.89.42.100'
      );

      expect(res).toBe(true);
    });
  });
});
