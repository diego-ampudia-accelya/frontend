export const translations = {
  applyChanges: {
    title: 'menu.masterData.configuration.ipSettings.changesDialog.applyChanges.title',
    details: 'menu.masterData.configuration.ipSettings.changesDialog.applyChanges.message'
  },
  unsavedChanges: {
    title: 'menu.masterData.configuration.ipSettings.changesDialog.unsavedChanges.title',
    details: 'menu.masterData.configuration.ipSettings.changesDialog.unsavedChanges.message'
  },
  remove: {
    title: 'menu.masterData.configuration.ipSettings.changesDialog.remove.title',
    details: 'menu.masterData.configuration.ipSettings.changesDialog.remove.message'
  }
};
