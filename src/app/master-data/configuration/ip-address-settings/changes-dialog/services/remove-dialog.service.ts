import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as fromIpAddressConfiguration from '../../../store/reducers';
import { IpAddressRemoveDialogConfigService } from './remove-dialog-config.service';
import { translations } from './translations';
import { IpAddressConfigurationService } from '~app/master-data/configuration/api/ip-address-configuration.service';
import { IpAddressConfigurationActions } from '~app/master-data/configuration/store/actions';
import { AppState } from '~app/reducers';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Injectable()
export class IpAddressRemoveDialogService {
  constructor(
    private store: Store<AppState>,
    private dialogService: DialogService,
    private configBuilder: IpAddressRemoveDialogConfigService,
    private dataService: IpAddressConfigurationService
  ) {}

  public open(): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(translations.remove);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      withLatestFrom(this.store.pipe(select(fromIpAddressConfiguration.getIpSettingsIsExisting))),
      switchMap(([result, isExisting]) =>
        result === FooterButton.Delete && isExisting ? this.remove(dialogConfig) : of(result)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private remove(dialogConfig: ChangesDialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) =>
      dialogConfig.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.store.pipe(select(fromIpAddressConfiguration.getIpSettingsUrl)).pipe(
        first(),
        switchMap(url =>
          this.dataService.delete(url).pipe(
            tap(() => this.store.dispatch(IpAddressConfigurationActions.deleteSuccess())),
            mapTo(FooterButton.Remove),
            finalize(() => setLoading(false))
          )
        )
      );
    });
  }
}
