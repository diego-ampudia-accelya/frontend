import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { combineLatest, defer, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { IpAddressConfigurationService } from '../../../api/ip-address-configuration.service';
import { IpAddressChange, IpAddressConfig, IpAddressConfigBE } from '../../../models/ip-address.model';
import { IpAddressConfigurationActions } from '../../../store/actions';
import * as fromIpAddressConfiguration from '../../../store/reducers';
import { IpAddressChangesDialogComponent } from '../changes-dialog.component';
import { IpAddressChangesDialogConfig } from '../models/ip-address-changes-config.model';

import { IpAddressChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { User } from '~app/shared/models/user.model';
import { DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';

@Injectable()
export class IpAddressChangesDialogService {
  constructor(
    private dialogService: DialogService,
    public configBuilder: IpAddressChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: IpAddressConfigurationService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  private confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return combineLatest([
      this.store.pipe(select(fromIpAddressConfiguration.getIpAddressConfiguration)),
      this.store.pipe(select(fromIpAddressConfiguration.getIpSettingsModifications)),
      this.store.pipe(select(fromIpAddressConfiguration.getIpSettingsIsInvalid)),
      this.store.pipe(select(getUser))
    ]).pipe(
      first(),
      switchMap(([resultConfig, modifications, isInvalidState, loggedUser]) => {
        if (!modifications) {
          return of(FooterButton.Discard);
        }

        return defer(() => this.open(resultConfig, modifications, question, isInvalidState, loggedUser));
      })
    );
  }

  private open(
    config: IpAddressConfig,
    modifications: IpAddressChange[],
    question: { title: string; details: string },
    isInvalidState: boolean,
    { ip }: User
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(config, modifications, question, isInvalidState, ip);

    return this.dialogService.open(IpAddressChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      withLatestFrom(this.store.pipe(select(fromIpAddressConfiguration.getIpAddressConfiguration))),
      switchMap(([action, value]) => {
        let dialogResult$ = of(action);
        if (action === FooterButton.Apply) {
          dialogResult$ = this.apply(value, dialogConfig);
        } else if (action === FooterButton.Discard) {
          this.store.dispatch(IpAddressConfigurationActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(value: IpAddressConfig, dialogConfig: IpAddressChangesDialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));
    const mappedValue: IpAddressConfigBE = this.configBuilder.getMappedValueForBackend(value);

    return defer(() => {
      setLoading(true);

      return combineLatest([
        this.store.pipe(select(fromIpAddressConfiguration.getIpSettingsUrl)),
        this.store.pipe(select(fromIpAddressConfiguration.getIpSettingsIsExisting))
      ]).pipe(
        first(),
        switchMap(([url, isExisting]) =>
          isExisting
            ? this.modifyIpConfiguration(url, mappedValue, setLoading)
            : this.createIpConfiguration(url, mappedValue, setLoading)
        )
      );
    });
  }

  private createIpConfiguration(
    url: string,
    mappedValue: IpAddressConfigBE,
    setLoading: (loading: boolean) => any
  ): Observable<FooterButton> {
    return this.dataService.create(url, mappedValue).pipe(
      tap((ipAddressConfiguration: IpAddressConfigBE) =>
        this.store.dispatch(IpAddressConfigurationActions.applyChangesSuccess({ ipAddressConfiguration }))
      ),
      mapTo(FooterButton.Apply),
      finalize(() => setLoading(false))
    );
  }

  private modifyIpConfiguration(
    url: string,
    mappedValue: IpAddressConfigBE,
    setLoading: (loading: boolean) => any
  ): Observable<FooterButton> {
    return this.dataService.save(url, mappedValue).pipe(
      tap((ipAddressConfiguration: IpAddressConfigBE) =>
        this.store.dispatch(IpAddressConfigurationActions.applyChangesSuccess({ ipAddressConfiguration }))
      ),
      mapTo(FooterButton.Apply),
      finalize(() => setLoading(false))
    );
  }
}
