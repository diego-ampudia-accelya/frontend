import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { isEqual } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, skip, switchMap, takeUntil } from 'rxjs/operators';

import { IpAddressConfig, IpRange } from '../models/ip-address.model';
import { IpType } from '../models/ip-type.model';
import { IpAddressConfigurationActions } from '../store/actions';
import * as fromIpAddressConfiguration from '../store/reducers';
import { IpAddressChangesDialogService } from './changes-dialog/services/changes-dialog.service';
import { lessThanEndIpValueValidator } from './validators/less-ip-value-control.validator';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { transformControlValueValidator } from '~app/master-data/configuration/ip-address-settings/validators/transform-control.validator';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { GLOBALS, Permissions } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { ResponseErrorBE } from '~app/shared/models';

const PERMANENT_IPS_COUNT = 3;
const VALIDATION_ERROR_CODE = 400;

@Component({
  selector: 'bspl-ip-address-settings',
  templateUrl: './ip-address-settings.component.html',
  styleUrls: ['./ip-address-settings.component.scss']
})
export class IpAddressSettingsComponent implements OnInit {
  public form: FormGroup;
  public permanentIpsFormArray: FormArray;
  public rangeFormGroup: FormGroup;

  public ipOptions = [
    {
      label: this.translationService.translate('menu.masterData.configuration.ipSettings.options.ips'),
      value: IpType.Permanent
    },
    {
      label: this.translationService.translate('menu.masterData.configuration.ipSettings.options.range'),
      value: IpType.Range
    }
  ];
  public ipType$: Observable<IpType>;
  public ipType = IpType;
  public canDelete = this.permissionsService.hasPermission(Permissions.deleteIpAddressSettings);

  public isLoading$ = this.store.pipe(select(fromIpAddressConfiguration.getIpSettingsIsLoading));
  public isNotExisting$ = this.store.pipe(
    select(fromIpAddressConfiguration.getIpSettingsIsExisting),
    distinctUntilChanged(),
    map((isExisting: boolean) => !isExisting)
  );

  private destroy$ = new Subject();
  private configuration$ = this.store.pipe(select(fromIpAddressConfiguration.getIpAddressConfiguration));
  private errors$ = this.store.pipe(
    select(fromIpAddressConfiguration.getIpSettingsErrors),
    filter(error => !!error)
  );

  private formFactory: FormUtil;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private changesDialogService: IpAddressChangesDialogService,
    private translationService: L10nTranslationService,
    private permissionsService: PermissionsService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.form = this.buildForm();
    this.ipType$ = this.form.get('ipType').valueChanges;

    this.updateFormValue();
    this.subscribeToFormValueChanges();
    this.subscribeToErrors();
    this.subscribeToIsExisting();
  }

  public deleteConfig(): void {
    this.store.dispatch(IpAddressConfigurationActions.openDelete());
  }

  public canDeactivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromIpAddressConfiguration.getIpSettingsIsChanged),
      switchMap((isChanged: boolean) =>
        isChanged
          ? this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel))
          : of(true)
      )
    );
  }

  private buildForm(): FormGroup {
    const permanentIpControl = () =>
      new FormControl(null, [transformControlValueValidator, Validators.pattern(GLOBALS.PATTERNS.IP_ADDRESS)]);
    this.permanentIpsFormArray = this.formFactory.builder.array(
      Array(PERMANENT_IPS_COUNT).fill(null).map(permanentIpControl)
    );
    this.rangeFormGroup = this.formFactory.createGroup<IpRange>({
      endIp: [
        null,
        [Validators.required, transformControlValueValidator, Validators.pattern(GLOBALS.PATTERNS.IP_ADDRESS)]
      ]
    } as any);

    this.rangeFormGroup.addControl(
      'beginIp',
      new FormControl(null, [
        Validators.required,
        transformControlValueValidator,
        Validators.pattern(GLOBALS.PATTERNS.IP_ADDRESS),
        lessThanEndIpValueValidator(this.rangeFormGroup.get('endIp'))
      ])
    );

    return this.formFactory.createGroup<IpAddressConfig>({
      ipType: [],
      range: this.rangeFormGroup,
      ips: this.permanentIpsFormArray
    });
  }

  private updateFormValue(): void {
    this.configuration$
      .pipe(
        distinctUntilChanged((_, current) => isEqual(this.form.value, current)),
        takeUntil(this.destroy$)
      )
      .subscribe((config: IpAddressConfig) => {
        this.form.patchValue(config, { emitEvent: false });
        this.form.get('ipType').updateValueAndValidity();
      });
  }

  private subscribeToFormValueChanges(): void {
    this.rangeFormGroup
      .get('endIp')
      .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$))
      .subscribe(() => {
        this.rangeFormGroup.get('beginIp').updateValueAndValidity({ emitEvent: false });
      });

    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((value: IpAddressConfig) => {
      this.store.dispatch(IpAddressConfigurationActions.modify({ config: value, isFormValid: this.getIsFormValid() }));
    });
  }

  private subscribeToErrors(): void {
    this.errors$.pipe(takeUntil(this.destroy$)).subscribe(({ error }) => this.setFieldErrors(error));
  }

  private subscribeToIsExisting(): void {
    this.isNotExisting$.pipe(filter(Boolean), skip(1), debounceTime(100), takeUntil(this.destroy$)).subscribe(() => {
      this.form.reset({ emitEvent: false });
    });
  }

  private getIsFormValid(): boolean {
    const currentIpType = this.form.get('ipType').value;

    if (currentIpType === IpType.Permanent) {
      return this.permanentIpsFormArray.valid;
    }

    if (currentIpType === IpType.Range) {
      return this.rangeFormGroup.valid;
    }

    return false;
  }

  private setFieldErrors(error: ResponseErrorBE): void {
    if (error?.errorCode !== VALIDATION_ERROR_CODE || !error?.messages?.length) {
      return;
    }

    error.messages.forEach(({ message, messageParams }) => {
      const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
      const invalidControl = controlName.includes('ips') ? this.getIpsElement(controlName) : this.form.get(controlName);
      if (invalidControl) {
        invalidControl.setErrors({ requestError: { message } });
        FormUtil.showControlState(invalidControl);
      }
    });
  }

  private getIpsElement(controlName: string): AbstractControl {
    const index = controlName.split(/\[|\]/).filter(Boolean)[1];

    return index ? this.permanentIpsFormArray.at(+index) : null;
  }
}
