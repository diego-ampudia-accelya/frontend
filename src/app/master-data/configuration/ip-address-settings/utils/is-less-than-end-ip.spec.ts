import { getIsLessThanEndInRange } from './is-less-than-end-ip';

describe('getIsLessThanEndInRange', () => {
  it('should return true when first ip is equal to end ip', () => {
    const ip1 = '10.089.42.100'.split('.').map(Number);
    const ip2 = '010.89.042.100'.split('.').map(Number);
    const res = getIsLessThanEndInRange(ip1, ip2);

    expect(res).toBe(true);
  });
  it('should return true when first ip is less than end ip', () => {
    const ip1 = '10.89.42.100'.split('.').map(Number);
    const ip2 = '011.89.042.100'.split('.').map(Number);
    const res = getIsLessThanEndInRange(ip1, ip2);

    expect(res).toBe(true);
  });

  it('should return false when first ip is more than end ip', () => {
    const ip1 = '11.89.042.120'.split('.').map(Number);
    const ip2 = '011.89.042.100'.split('.').map(Number);
    const res = getIsLessThanEndInRange(ip1, ip2);

    expect(res).toBe(false);
  });
});
