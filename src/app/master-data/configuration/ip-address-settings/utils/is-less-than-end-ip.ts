export function getIsLessThanEndInRange(currentIpArray: number[], endArray: number[]): boolean {
  let lessThanEnd = true;

  for (let index = 0; index < currentIpArray.length; index++) {
    const currentValue = currentIpArray[index];
    const endValue = endArray[index];

    if (currentValue < endValue) {
      break;
    }

    if (currentValue > endValue) {
      lessThanEnd = false;
      break;
    }
  }

  return lessThanEnd;
}
