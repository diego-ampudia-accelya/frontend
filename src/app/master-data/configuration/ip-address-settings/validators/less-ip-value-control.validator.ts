import { AbstractControl, ValidatorFn } from '@angular/forms';

import { getIsLessThanEndInRange } from '../utils/is-less-than-end-ip';

export function lessThanEndIpValueValidator(endControl: AbstractControl): ValidatorFn {
  return (control: AbstractControl) => {
    if (
      control?.value &&
      endControl?.valid &&
      endControl?.value &&
      isStartIpMoreThanEndIp(control.value, endControl.value)
    ) {
      return {
        lessThanEndIp: {
          key: 'menu.masterData.configuration.ipSettings.errors.lessThanEndIp'
        }
      };
    }

    return null;
  };
}

function isStartIpMoreThanEndIp(beginIp: string, endIp: string): boolean {
  const separator = '.';
  const beginArray = beginIp.split(separator).map(Number);
  const endArray = endIp.split(separator).map(Number);

  return !getIsLessThanEndInRange(beginArray, endArray);
}
