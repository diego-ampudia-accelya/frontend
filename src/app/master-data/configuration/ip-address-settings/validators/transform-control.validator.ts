import { AbstractControl } from '@angular/forms';
import isNull from 'lodash/isNull';

export function transformControlValueValidator(control: AbstractControl): null {
  if (control && control.value) {
    if (control.value.trim() !== control.value) {
      control.setValue(control.value.trim());
      control.updateValueAndValidity();
    }
  }

  if (control && !control.value && !isNull(control.value)) {
    control.setValue(null);
    control.updateValueAndValidity();
  }

  return null;
}
