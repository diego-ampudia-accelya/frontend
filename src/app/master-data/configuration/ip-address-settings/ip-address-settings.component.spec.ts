import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';

import { IpAddressConfig } from '../models/ip-address.model';
import * as fromIpAddressConfiguration from '../store/reducers';

import { IpAddressChangesDialogService } from './changes-dialog/services/changes-dialog.service';
import { IpAddressSettingsComponent } from './ip-address-settings.component';
import { TranslatePipeMock } from '~app/test';
import { ButtonComponent, InputComponent, RadioButtonGroupComponent, SpinnerComponent } from '~app/shared/components';
import { PermissionsService } from '~app/auth/services/permissions.service';

const initialState = {};

const configuration: IpAddressConfig = {
  ipType: null,
  ips: [null, null, null],
  range: {
    beginIp: null,
    endIp: null
  }
};

describe('IpAddressSettingsComponent', () => {
  let component: IpAddressSettingsComponent;
  let fixture: ComponentFixture<IpAddressSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        IpAddressSettingsComponent,
        MockComponents(SpinnerComponent, RadioButtonGroupComponent, InputComponent, ButtonComponent),
        TranslatePipeMock
      ],
      providers: [
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromIpAddressConfiguration.getIpAddressConfiguration, value: configuration },
            { selector: fromIpAddressConfiguration.getIpSettingsIsLoading, value: true }
          ]
        }),
        FormBuilder,
        { provide: IpAddressChangesDialogService, useValue: { confirmUnsavedChanges: of({}) } },
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => ''
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IpAddressSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
