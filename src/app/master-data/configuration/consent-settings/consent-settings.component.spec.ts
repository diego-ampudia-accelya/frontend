import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsentSettingsComponent } from './consent-settings.component';

// TODO
xdescribe('ConsentSettingsComponent', () => {
  let component: ConsentSettingsComponent;
  let fixture: ComponentFixture<ConsentSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsentSettingsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
