import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { isEqual } from 'lodash';
import { combineLatest, iif, Observable, of, Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { greaterEqualValidator } from '~app/shared/validators/greater-equal.validator';
import { ConsentSettingsActions } from '../store/actions';
import * as fromConsentSettings from '../store/reducers';
import { TipGlobalConfiguration, TipGlobalConfigurationViewModel } from './models/tip-global-configuration.model';
import { ConsentSettingsChangesDialogService } from './services/changes-dialog/changes-dialog.service';
import { TipGlobalConfigurationService } from './services/tip-global-configuration.service';

@Component({
  selector: 'bspl-consent-settings',
  templateUrl: './consent-settings.component.html',
  styleUrls: ['./consent-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConsentSettingsComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public bspOptions: DropdownOption<BspDto>[];

  public isLoading: boolean;

  private originalConfig: TipGlobalConfigurationViewModel;

  private minimumOfDaysToActivateAConsentControl: FormControl;
  private maximumOfDaysToActivateAConsentControl: FormControl;

  private formUtil: FormUtil;

  private destroy$ = new Subject();

  constructor(
    private bspsDictionaryService: BspsDictionaryService,
    private tipGlobalConfigurationService: TipGlobalConfigurationService,
    private store: Store<AppState>,
    private actions$: Actions,
    private changesDialog: ConsentSettingsChangesDialogService,
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.buildForm();
    this.initializeFormListener();
    this.fetchData();
    this.initializeApplyChanges();
  }

  public canDeactivate(): Observable<boolean> {
    return this.store
      .select(fromConsentSettings.getCanApplyChanges)
      .pipe(
        switchMap(canApplyChanges =>
          iif(
            () => canApplyChanges,
            this.changesDialog
              .confirmUnsavedChanges({ original: this.originalConfig, value: this.form.value })
              .pipe(map(result => result !== FooterButton.Cancel)),
            of(true)
          )
        )
      );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private buildForm(): void {
    this.minimumOfDaysToActivateAConsentControl = new FormControl(null, [Validators.min(1), Validators.max(999)]);
    this.maximumOfDaysToActivateAConsentControl = new FormControl(null, [
      Validators.min(1),
      Validators.max(999),
      greaterEqualValidator(this.minimumOfDaysToActivateAConsentControl)
    ]);

    this.form = this.formUtil.createGroup<TipGlobalConfigurationViewModel>(
      {
        minimumOfDaysToActivateAConsent: this.minimumOfDaysToActivateAConsentControl,
        maximumOfDaysToActivateAConsent: this.maximumOfDaysToActivateAConsentControl,
        bspIsoCountryCodesToShowAgentLevelConsents: new FormControl()
      },
      { updateOn: 'change' }
    );
  }

  private fetchData(): void {
    this.isLoading = true;

    combineLatest([
      this.tipGlobalConfigurationService.getGlobalConfig(),
      this.bspsDictionaryService.getAllBspDropdownOptions()
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([config, bsps]) => {
        const viewModel = this.transformBEModelToViewModel(config, bsps);
        this.originalConfig = viewModel;
        this.bspOptions = bsps;
        this.form.patchValue(viewModel);
        this.isLoading = false;
        this.cd.markForCheck();
      });
  }

  private initializeFormListener(): void {
    this.minimumOfDaysToActivateAConsentControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.maximumOfDaysToActivateAConsentControl.updateValueAndValidity();
      FormUtil.showControlState(this.maximumOfDaysToActivateAConsentControl, 'dirty');
    });

    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((value: TipGlobalConfigurationViewModel) =>
      this.store.dispatch(
        ConsentSettingsActions.setChanges({
          hasChanges: !isEqual(value, this.originalConfig)
        })
      )
    );

    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(
        ConsentSettingsActions.setFormValid({
          isValid: this.form.valid
        })
      );
    });
  }

  private initializeApplyChanges(): void {
    this.actions$
      .pipe(
        ofType(ConsentSettingsActions.openApplyChanges),
        switchMap(() =>
          this.changesDialog.confirmApplyChanges({ original: this.originalConfig, value: this.form.value })
        ),
        takeUntil(this.destroy$)
      )
      .subscribe();

    this.actions$.pipe(ofType(ConsentSettingsActions.discard), takeUntil(this.destroy$)).subscribe(() => {
      this.form.reset(this.originalConfig);
      this.cd.markForCheck();
    });

    this.actions$.pipe(ofType(ConsentSettingsActions.applyChangesSuccess), takeUntil(this.destroy$)).subscribe(() => {
      this.originalConfig = this.form.value;
      this.form.updateValueAndValidity();
      this.notificationService.showSuccess('menu.masterData.configuration.tipConsents.changesDialog.successMsg');
    });
  }

  private transformBEModelToViewModel(
    config: TipGlobalConfiguration,
    bsps: DropdownOption<BspDto>[]
  ): TipGlobalConfigurationViewModel {
    return {
      ...config,
      bspIsoCountryCodesToShowAgentLevelConsents: bsps
        ?.map(bsp => bsp.value)
        .filter(bsp => config.bspIsoCountryCodesToShowAgentLevelConsents?.includes(bsp.isoCountryCode))
    };
  }
}
