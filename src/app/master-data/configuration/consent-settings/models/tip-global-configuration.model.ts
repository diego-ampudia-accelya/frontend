import { BspDto } from '~app/shared/models/bsp.model';

export interface TipGlobalConfiguration {
  minimumOfDaysToActivateAConsent: string;
  maximumOfDaysToActivateAConsent: string;
  bspIsoCountryCodesToShowAgentLevelConsents: string[];
}

export interface TipGlobalConfigurationViewModel {
  minimumOfDaysToActivateAConsent: string;
  maximumOfDaysToActivateAConsent: string;
  bspIsoCountryCodesToShowAgentLevelConsents: BspDto[];
}

export interface TipGlobalConfigurationChangesViewModel {
  original: TipGlobalConfigurationViewModel;
  value: TipGlobalConfigurationViewModel;
}
