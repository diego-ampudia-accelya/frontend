import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfigurationService } from '~app/shared/services';
import { TipGlobalConfiguration, TipGlobalConfigurationViewModel } from '../models/tip-global-configuration.model';

@Injectable({
  providedIn: 'root'
})
export class TipGlobalConfigurationService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/global-configuration`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public getGlobalConfig(): Observable<TipGlobalConfiguration> {
    return this.http.get<TipGlobalConfiguration>(this.baseUrl);
  }

  public save(config: TipGlobalConfigurationViewModel): Observable<TipGlobalConfiguration> {
    return this.http.put<TipGlobalConfiguration>(this.baseUrl, this.transformViewModelToBEModel(config));
  }

  private transformViewModelToBEModel(config: TipGlobalConfigurationViewModel): TipGlobalConfiguration {
    return {
      ...config,
      bspIsoCountryCodesToShowAgentLevelConsents: config.bspIsoCountryCodesToShowAgentLevelConsents?.map(
        bsp => bsp.isoCountryCode
      )
    };
  }
}
