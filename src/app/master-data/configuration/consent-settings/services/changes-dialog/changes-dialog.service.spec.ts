import { TestBed, waitForAsync } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { TipGlobalConfigurationService } from '../tip-global-configuration.service';
import { ConsentSettingsChangesDialogConfigService } from './changes-dialog-config.service';
import { ConsentSettingsChangesDialogService } from './changes-dialog.service';

// TODO
xdescribe('ConsentSettingsChangesDialogService', () => {
  let service: ConsentSettingsChangesDialogService;
  let mockStore: MockStore<AppState>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        ConsentSettingsChangesDialogService,
        {
          provide: DialogService,
          useValue: {
            open: () => of({}),
            close: () => {}
          }
        },
        {
          provide: ConsentSettingsChangesDialogConfigService,
          useValue: {
            build: () => {}
          }
        },
        {
          provide: TipGlobalConfigurationService,
          useValue: {
            save: () => of({})
          }
        }
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ConsentSettingsChangesDialogService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });
});
