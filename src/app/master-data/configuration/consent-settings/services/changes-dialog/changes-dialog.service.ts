import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';
import { ConsentSettingsActions } from '~app/master-data/configuration/store/actions';
import { AppState } from '~app/reducers';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { TipGlobalConfigurationChangesViewModel } from '../../models/tip-global-configuration.model';
import { TipGlobalConfigurationService } from '../tip-global-configuration.service';
import { ConsentSettingsChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';

@Injectable()
export class ConsentSettingsChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ConsentSettingsChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: TipGlobalConfigurationService
  ) {}

  public confirmApplyChanges(changes: TipGlobalConfigurationChangesViewModel): Observable<FooterButton> {
    return this.confirm(translations.applyChanges, changes);
  }

  public confirmUnsavedChanges(changes: TipGlobalConfigurationChangesViewModel): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges, changes);
  }

  public confirm(
    question: { title: string; details: string },
    changes: TipGlobalConfigurationChangesViewModel
  ): Observable<FooterButton> {
    return of(null).pipe(
      switchMap(() =>
        iif(
          () => !!changes,
          defer(() => this.open(changes, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    changes: TipGlobalConfigurationChangesViewModel,
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(changes, question);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(changes, dialogConfig);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(ConsentSettingsActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(
    changes: TipGlobalConfigurationChangesViewModel,
    dialogConfig: ChangesDialogConfig
  ): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(changes.value).pipe(
        tap(() => this.store.dispatch(ConsentSettingsActions.applyChangesSuccess())),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
