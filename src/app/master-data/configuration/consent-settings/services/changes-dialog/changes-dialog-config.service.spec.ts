import { TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';
import { ConsentSettingsChangesDialogConfigService } from './changes-dialog-config.service';

// TODO
xdescribe('ConsentSettingsChangesDialogConfigService', () => {
  let service: ConsentSettingsChangesDialogConfigService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        ConsentSettingsChangesDialogConfigService,
        { provide: L10nTranslationService, useValue: { translate: () => '' } }
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ConsentSettingsChangesDialogConfigService);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });
});
