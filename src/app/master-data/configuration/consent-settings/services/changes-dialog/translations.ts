export const translations = {
  applyChanges: {
    title: 'menu.masterData.configuration.tipConsents.changesDialog.applyChanges.title',
    details: 'menu.masterData.configuration.tipConsents.changesDialog.applyChanges.details'
  },
  unsavedChanges: {
    title: 'menu.masterData.configuration.tipConsents.changesDialog.unsavedChanges.title',
    details: 'menu.masterData.configuration.tipConsents.changesDialog.unsavedChanges.details'
  },
  minimumOfDaysToActivateAConsent: 'menu.masterData.configuration.tipConsents.controls.minimum.label',
  maximumOfDaysToActivateAConsent: 'menu.masterData.configuration.tipConsents.controls.maximum.label',
  bspIsoCountryCodesToShowAgentLevelConsents: 'menu.masterData.configuration.tipConsents.controls.bsps.label'
};
