import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { isEqual } from 'lodash';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import {
  TipGlobalConfigurationChangesViewModel,
  TipGlobalConfigurationViewModel
} from '../../models/tip-global-configuration.model';
import { translations } from './translations';

@Injectable()
export class ConsentSettingsChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: TipGlobalConfigurationChangesViewModel,
    question: { title: string; details: string }
  ): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: modifications
          ? [{ type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary }, { type: FooterButton.Apply }]
          : [{ type: FooterButton.Apply }]
      },
      changes: modifications ? this.formatChanges(modifications) : null,
      message
    };
  }

  private formatChanges(modifications: TipGlobalConfigurationChangesViewModel): ChangeModel[] {
    return Object.keys(modifications.original)
      .filter(key => !isEqual(modifications.original[key], modifications.value[key]))
      .map(key => {
        let changeModel = {
          group: '',
          name: this.translation.translate(translations[key]),
          value: modifications.value[key],
          originalValue: modifications.original[key]
        };

        if ((key as keyof TipGlobalConfigurationViewModel) === 'bspIsoCountryCodesToShowAgentLevelConsents') {
          changeModel = {
            ...changeModel,
            value: modifications.value.bspIsoCountryCodesToShowAgentLevelConsents.map(
              bsp => toValueLabelObjectBsp(bsp).label
            ),
            originalValue: modifications.original.bspIsoCountryCodesToShowAgentLevelConsents.map(
              bsp => toValueLabelObjectBsp(bsp).label
            )
          };
        }

        return changeModel;
      });
  }
}
