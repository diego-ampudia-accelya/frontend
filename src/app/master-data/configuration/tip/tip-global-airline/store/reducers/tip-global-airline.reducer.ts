import { createReducer, on } from '@ngrx/store';
import { differenceWith, isEqual } from 'lodash';

import { TipGlobalAirlineActions } from '../actions';
import {
  GlobalCountryLevelConsentRule,
  TipGlobalAirlineModel
} from '~app/master-data/configuration/models/tip-global-airline.model';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';

export const tipGlobalAirlineKey = 'tipGlobalAirline';

export interface State {
  tipGlobalAirline: TipGlobalAirlineState;
}

export interface TipGlobalAirlineState {
  isReadOnlyMode: boolean;
  resourceId: string;
  isLoading: boolean;
  originalValue: TipGlobalAirlineModel;
  value: TipGlobalAirlineModel;
  isInvalid: boolean;
  isChanged: boolean;
  errors: { error: ResponseErrorBE };
}

const initialState: TipGlobalAirlineState = {
  isReadOnlyMode: false,
  resourceId: null,
  isLoading: false,
  originalValue: null,
  value: null,
  isInvalid: false,
  isChanged: false,
  errors: null
};

export const reducer = createReducer(
  initialState,
  on(TipGlobalAirlineActions.load, (state, { isReadOnlyMode, resourceId }) => ({
    ...state,
    isReadOnlyMode,
    resourceId,
    isLoading: true
  })),
  on(TipGlobalAirlineActions.loadSuccess, (state, { value }) => ({
    ...state,
    isLoading: false,
    value,
    originalValue: value,
    isChanged: false,
    isInvalid: false,
    errors: null
  })),
  on(TipGlobalAirlineActions.loadError, state => ({
    ...state,
    originalValue: null,
    value: null,
    isChanged: false,
    isLoading: false,
    isInvalid: false,
    errors: null
  })),
  on(TipGlobalAirlineActions.modify, (state, { value }) => ({
    ...state,
    value: changeValue(state.value, value),
    isChanged: !isEqual(state.originalValue, { ...state.value, ...value }),
    errors: null
  })),
  on(TipGlobalAirlineActions.discard, state => ({
    ...state,
    isChanged: false,
    value: state.originalValue
  })),
  on(TipGlobalAirlineActions.applyChangesSuccess, (state, { value }) => ({
    ...state,
    value,
    originalValue: value,
    isChanged: false,
    isInvalid: false,
    errors: null
  })),
  on(TipGlobalAirlineActions.applyChangesError, (state, { error }) => ({
    ...state,
    isChanged: !error,
    isInvalid: !!error,
    errors: error
  })),
  on(TipGlobalAirlineActions.setStatusForm, (state, { value }) => ({
    ...state,
    isInvalid: value
  }))
);

export const changeValue = (currentValue: TipGlobalAirlineModel, newValue: TipGlobalAirlineModel) => ({
  ...currentValue,
  ...newValue
});

// Selectors from this state are in configuration src\app\master-data\configuration\store\reducers\index.ts

export const getTipGlobalAirlineChanges = ({ value, originalValue }: TipGlobalAirlineState) => {
  const costRecoveryKey: keyof GlobalCountryLevelConsentRule = 'costRecoveries';

  const modifications: Array<{ [P in keyof Partial<TipGlobalAirlineModel>]: TipGlobalAirlineModel[P] }> = [];

  if (value && originalValue) {
    // Check which keys from fist level have different values
    Object.keys(value)
      .filter(key => !isEqual(value[key], originalValue[key]))
      .forEach(modifiedKey => {
        if (typeof value[modifiedKey] === 'object') {
          // If it is an object with subkeys, check which subkey is different from originalValue
          // TODO: implement it with a `reduce` function instead of using `forEach`
          Object.keys(value[modifiedKey])
            .filter(property => !isEqual(value[modifiedKey][property], originalValue[modifiedKey][property]))
            .forEach(prop => {
              let newValue = value[modifiedKey][prop];
              if (newValue?.length && prop === costRecoveryKey) {
                newValue = differenceWith(value[modifiedKey][prop], originalValue[modifiedKey][prop], isEqual);
              }
              modifications.push({ [modifiedKey]: { [prop]: newValue } });
            });
        } else {
          modifications.push({ [modifiedKey]: value[modifiedKey] });
        }
      });
  }

  return modifications;
};
