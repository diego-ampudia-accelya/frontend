import { createAction, props } from '@ngrx/store';

import { TipGlobalAirlineModel } from '~app/master-data/configuration/models/tip-global-airline.model';

export const load = createAction(
  '[TIP Global Airline Settings] Load',
  props<{ isReadOnlyMode: boolean; resourceId: string }>()
);

export const loadSuccess = createAction(
  '[TIP Global Airline Settings] LoadSuccess',
  props<{ value: TipGlobalAirlineModel }>()
);

export const loadError = createAction('[TIP Global Airline Settings] LoadError');

export const modify = createAction('[TIP Global Airline Settings] Modify', props<{ value: TipGlobalAirlineModel }>());

export const openApplyChanges = createAction('[TIP Global Airline Settings] Apply Changes');

export const discard = createAction('[TIP Global Airline Settings] Discard');

export const applyChangesSuccess = createAction(
  '[TIP Global Airline Settings] Apply Changes Success',
  props<{ value: TipGlobalAirlineModel }>()
);

export const applyChangesError = createAction('[TIP Global Airline Settings] Apply Changes Error', props<{ error }>());

export const setStatusForm = createAction(
  '[TIP Global Airline Settings] Update Status Form',
  props<{ value: boolean }>()
);
