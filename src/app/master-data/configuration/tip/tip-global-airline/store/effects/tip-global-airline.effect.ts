import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { TipGlobalAirlineActions } from '../actions';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class TipGlobalAirlineEffects {
  constructor(
    private actions$: Actions,
    private changesDialogService: ChangesDialogService,
    private notification: NotificationService
  ) {}

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TipGlobalAirlineActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(() => EMPTY),
      rethrowError(error => TipGlobalAirlineActions.applyChangesError({ error }))
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(TipGlobalAirlineActions.applyChangesSuccess),
        tap(() =>
          this.notification.showSuccess(
            'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.applyChanges.success'
          )
        )
      ),
    { dispatch: false }
  );
}
