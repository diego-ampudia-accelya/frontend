import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { uniqBy } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { filter, first, map, takeUntil } from 'rxjs/operators';

import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { TipGlobalAirlineRemoveDialogService } from './remove-dialog/tip-global-airline-remove-dialog.service';
import { isCostRecoveryType, stringToBoolean } from './services/tip-global.airline.converter';
import { TipGlobalAirlineActions } from './store/actions';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { CanComponentDeactivateGuard } from '~app/core/services';
import { YesNoOptions } from '~app/files/model/yes-no.enum';
import { AutomaticConsents } from '~app/master-data/configuration/enum/automatic-consents-tips.enum';
import { CostRecoveryType } from '~app/master-data/configuration/enum/cost-recovery-type.enum';
import {
  CostRecovery,
  CostRecoveryAmount,
  GlobalCountryLevelConsentRule,
  TipGlobalAirlineModel
} from '~app/master-data/configuration/models/tip-global-airline.model';
import {
  getTipGlobalAirlineIsLoading,
  getTipGlobalAirlineIsReadonlyMode,
  getTipGlobalAirlineValue
} from '~app/master-data/configuration/store/reducers';
import { tipLocalAirlineFixAmountValidator } from '~app/master-data/configuration/tip/tip-local-airline/tip-local-airline/validators/tip-local-airline-fix-amount.validator';
import { tipLocalAirlinePercentageValidator } from '~app/master-data/configuration/tip/tip-local-airline/tip-local-airline/validators/tip-local-airline-percentage.validator';
import { AppState } from '~app/reducers';
import { ButtonDesign, FooterButton } from '~app/shared/components';
import { GLOBALS } from '~app/shared/constants';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { User } from '~app/shared/models/user.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { positiveNumberValidator } from '~app/shared/validators';

@Component({
  selector: 'bspl-tip-global-airline',
  templateUrl: './tip-global-airline.component.html',
  styleUrls: ['./tip-global-airline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipGlobalAirlineComponent implements OnInit, OnDestroy, CanComponentDeactivateGuard {
  public form: FormGroup;
  private formUtil: FormUtil;
  private $destroy = new Subject();
  private loadingCostRecoveries = false;

  public productCountryOptions: DropdownOption[] = [];
  public productAgentOptions: DropdownOption[] = [];
  public cardOptions: DropdownOption[] = [];
  public productCountryConsentOptions: DropdownOption[] = [];
  public productAgentConsentOptions: DropdownOption[] = [];
  public costRecoveryTypeOptions: DropdownOption[] = [];
  public bspOptions$: Observable<DropdownOption[]>;
  public bspOptions: DropdownOption[];

  public btnDesign = ButtonDesign;

  public loggedUser: User;
  public isLoading$ = this.store.pipe(select(getTipGlobalAirlineIsLoading));
  public formData$: TipGlobalAirlineModel;
  public isReadOnlyMode$ = this.store.pipe(select(getTipGlobalAirlineIsReadonlyMode));

  public productCostRecoveries: FormArray;
  public cardCostRecoveries: FormArray;

  public isConsentManagementProduct: boolean;
  public isProductCountryRuleType: boolean;
  public isCardCountryRuleType: boolean;
  public isConsentManagementCard: boolean;

  public currencyOptions$: { [key: number]: Observable<DropdownOption<Currency>[]> } = {};
  private formData: TipGlobalAirlineModel;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  public get decimalsPattern(): RegExp {
    return GLOBALS.PATTERNS.DECIMALS_PATTERN;
  }

  public canDeactivate(): Observable<boolean> {
    return this.changeDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  constructor(
    private translationService: L10nTranslationService,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private cd: ChangeDetectorRef,
    private currencyDictionaryService: CurrencyDictionaryService,
    private changeDialogService: ChangesDialogService,
    private bspsDictionaryService: BspsDictionaryService,
    private removeDialogService: TipGlobalAirlineRemoveDialogService
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.initializeDropdowns();
    this.buildForm();
    this.initializeData();
    this.initializeFormChangesEmitters();
    this.checkCurrenciesFromCostRecoveries();
  }

  private initializeFormChangesEmitters() {
    this.form.valueChanges
      .pipe(
        filter(() => !this.loadingCostRecoveries),
        takeUntil(this.$destroy)
      )
      .subscribe((value: TipGlobalAirlineModel) => {
        if (this.form.valid) {
          this.store.dispatch(TipGlobalAirlineActions.modify({ value }));
        }
      });

    this.form.statusChanges.pipe(takeUntil(this.$destroy)).subscribe(status => {
      const value = status === 'INVALID';
      this.store.dispatch(TipGlobalAirlineActions.setStatusForm({ value }));
    });

    const { consentManagementCardType, consentManagementProductType, consentManagementCard, consentManagementProduct } =
      this.getManagementAndTypeFormsControls();

    consentManagementCard.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(value => {
      if (!value) {
        consentManagementCardType.reset(null, { emitEvent: false });
        consentManagementCardType.clearValidators();
        this.cardCostRecoveries.clear();
        consentManagementCardType.updateValueAndValidity({ emitEvent: false });
      }
    });

    consentManagementProduct.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(value => {
      if (!value) {
        consentManagementProductType.reset(null, { emitEvent: false });
        consentManagementProductType.clearValidators();
        this.productCostRecoveries.clear();
        consentManagementProductType.updateValueAndValidity({ emitEvent: false });
      }
    });

    consentManagementProductType.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(value => {
      if (!isCostRecoveryType(value.type)) {
        this.productCostRecoveries.clear();
      }
    });

    consentManagementCardType.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(value => {
      if (!isCostRecoveryType(value.type)) {
        this.cardCostRecoveries.clear();
      }
    });
  }

  private initializeLoggedUser() {
    this.loggedUser$.pipe(takeUntil(this.$destroy)).subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });
  }

  private initializeDropdowns(): void {
    this.productCountryOptions =
      this.productAgentOptions =
      this.cardOptions =
        Object.values(YesNoOptions).map((item: string) => ({
          value: stringToBoolean(item),
          label: this.translate(`options.${item.toString()}`)
        }));

    this.productCountryConsentOptions = this.productAgentConsentOptions = Object.values(AutomaticConsents).map(
      (item: string) => ({
        value: item,
        label: this.translate(`consentsOptions.${item}`)
      })
    );

    this.costRecoveryTypeOptions = Object.values(CostRecoveryType).map((item: string) => ({
      value: item,
      label: this.translate(`costRecoveryType.${item}`)
    }));

    this.bspOptions$ = this.bspsDictionaryService.getDropdownOptions(this.loggedUser.id);

    this.bspOptions$.subscribe(values => (this.bspOptions = values));
  }

  private buildForm() {
    this.productCostRecoveries = this.fb.array([]);
    this.cardCostRecoveries = this.fb.array([]);

    this.form = this.formUtil.createGroup<TipGlobalAirlineModel>({
      globalProductCountryLevelConsentManagement: this.fb.control(null, [Validators.required]),
      globalProductCountryLevelConsentRule: this.formUtil.createGroup<GlobalCountryLevelConsentRule>({
        type: this.fb.control(null),
        costRecoveries: this.productCostRecoveries
      }),
      globalProductAgentLevelConsentManagement: this.fb.control(null, [Validators.required]),
      globalCardConsentManagement: this.fb.control(null),
      globalCardCountryLevelConsentRule: this.formUtil.createGroup<GlobalCountryLevelConsentRule>({
        type: this.fb.control(null),
        costRecoveries: this.cardCostRecoveries
      })
    });
  }

  public initializeData() {
    this.store
      .pipe(select(getTipGlobalAirlineValue))
      .pipe(takeUntil(this.$destroy))
      .subscribe((data: TipGlobalAirlineModel) => {
        this.formData = data;
        this.form.patchValue(data, { emitEvent: false });

        this.isConsentManagementProduct = data.globalProductCountryLevelConsentManagement;
        this.isProductCountryRuleType = isCostRecoveryType(data.globalProductCountryLevelConsentRule?.type);
        this.isConsentManagementCard = data.globalCardConsentManagement;
        this.isCardCountryRuleType = isCostRecoveryType(data.globalCardCountryLevelConsentRule?.type);

        this.setValidators();
        this.loadProductCostRecoveries(data);
        this.loadCardCostRecoveries(data);
        this.cd.markForCheck();
      });
  }

  private setValidators() {
    const { consentManagementProductType, consentManagementCardType } = this.getManagementAndTypeFormsControls();

    if (this.isConsentManagementProduct) {
      consentManagementProductType.setValidators([Validators.required]);
      consentManagementProductType.updateValueAndValidity({ emitEvent: false });
    }
    if (this.isConsentManagementCard) {
      consentManagementCardType.setValidators([Validators.required]);
      consentManagementCardType.updateValueAndValidity({ emitEvent: false });
    }
  }

  private getManagementAndTypeFormsControls() {
    const consentMangementProductRule = FormUtil.get<TipGlobalAirlineModel>(
      this.form,
      'globalProductCountryLevelConsentRule'
    ) as FormGroup;
    const consentMangementCardRule = FormUtil.get<TipGlobalAirlineModel>(
      this.form,
      'globalCardCountryLevelConsentRule'
    ) as FormGroup;
    const consentManagementProductType = FormUtil.get<GlobalCountryLevelConsentRule>(
      consentMangementProductRule,
      'type'
    ) as FormGroup;
    const consentManagementProductCostRecoveries = FormUtil.get<GlobalCountryLevelConsentRule>(
      consentMangementProductRule,
      'costRecoveries'
    ) as FormGroup;
    const consentManagementCardType = FormUtil.get<GlobalCountryLevelConsentRule>(
      consentMangementCardRule,
      'type'
    ) as FormGroup;
    const consentManagementCardCostRecoveries = FormUtil.get<GlobalCountryLevelConsentRule>(
      consentMangementCardRule,
      'costRecoveries'
    ) as FormGroup;
    const consentManagementProduct = FormUtil.get<TipGlobalAirlineModel>(
      this.form,
      'globalProductCountryLevelConsentManagement'
    ) as FormGroup;

    const consentManagementCard = FormUtil.get<TipGlobalAirlineModel>(
      this.form,
      'globalCardConsentManagement'
    ) as FormGroup;

    return {
      consentManagementProduct,
      consentManagementCard,
      consentMangementProductRule,
      consentMangementCardRule,
      consentManagementProductType,
      consentManagementCardType,
      consentManagementProductCostRecoveries,
      consentManagementCardCostRecoveries
    };
  }

  private loadProductCostRecoveries(storeData: TipGlobalAirlineModel) {
    this.loadCostRecoveries(storeData.globalProductCountryLevelConsentRule.costRecoveries, this.productCostRecoveries);
  }

  private loadCardCostRecoveries(storeData: TipGlobalAirlineModel) {
    this.loadCostRecoveries(storeData.globalCardCountryLevelConsentRule.costRecoveries, this.cardCostRecoveries);
  }

  private loadCostRecoveries(costRecoveries: CostRecovery[], formArray: FormArray) {
    this.loadingCostRecoveries = true;

    formArray.clear();

    if (costRecoveries?.length) {
      costRecoveries.forEach(costRecoveryData => {
        if (costRecoveryData.bsp) {
          this.checkAndLoadCurrencyByBsp(costRecoveryData.bsp);
        }
        formArray.push(this.getCostRecoveryGroup(costRecoveryData));
      });
    }

    this.loadingCostRecoveries = false;
  }

  public translate(key: string): string {
    return this.translationService.translate(`menu.masterData.configuration.tipSettings.tipGlobal.${key}`);
  }

  public removeItemRow(collection: FormArray, index: number) {
    this.removeDialogService.open().subscribe((action: FooterButton) => {
      if (action === FooterButton.Remove) {
        collection.removeAt(index);
      }
    });
  }

  public addCostRecovery(collection: FormArray) {
    collection.push(this.getCostRecoveryGroup());
  }

  public isFixedTypeCostRecoveryAmount(line: FormGroup) {
    const costRecovery = FormUtil.get<CostRecovery>(line, 'costRecovery') as FormGroup;

    return FormUtil.get<CostRecoveryAmount>(costRecovery, 'type')?.value === CostRecoveryType.Fixed;
  }
  public isAvaliableToAmount(currencyValue, costRecoveryType) {
    const result = true;
    if (costRecoveryType === CostRecoveryType.Percentage || (costRecoveryType && currencyValue)) {
      return false;
    }

    return result;
  }

  private getCostRecoveryGroup(data?: CostRecovery): FormGroup {
    const decimals = data?.costRecovery?.currency?.decimals ?? 2;
    const bspValidators = [Validators.required];
    const typeValidators = [Validators.required];
    const amountValidators =
      data?.costRecovery?.type === CostRecoveryType.Fixed
        ? [
            Validators.required,
            positiveNumberValidator,
            Validators.pattern(this.decimalsPattern),
            tipLocalAirlineFixAmountValidator(decimals)
          ]
        : [Validators.required, tipLocalAirlinePercentageValidator];
    const currencyValidators = data?.costRecovery?.type === CostRecoveryType.Fixed ? [Validators.required] : [];

    const costRecoveryGroup = this.formUtil.createGroup<CostRecovery>({
      bsp: new FormControl(null, bspValidators),
      costRecovery: this.formUtil.createGroup<CostRecoveryAmount>({
        type: [null, typeValidators],
        amount: [null, amountValidators],
        currency: new FormControl(null, currencyValidators)
      })
    });

    if (data) {
      const adaptedData = this.formatDataCostRecovery(data);
      costRecoveryGroup.patchValue(adaptedData, { emitEvent: false });
      if (data.costRecovery.amount) {
        costRecoveryGroup.get('costRecovery.amount').markAsTouched();
      }
    }

    return costRecoveryGroup;
  }

  // Check constraints to reset fields in Cost recovery elements
  private formatDataCostRecovery({ bsp, costRecovery }: CostRecovery) {
    let currency = null;
    let decimal = 2;
    let amount = null;

    if (costRecovery) {
      currency = costRecovery.type === CostRecoveryType.Fixed && bsp?.id ? costRecovery.currency : currency;
      decimal = currency ? currency.decimals : decimal;
      amount = costRecovery.type && costRecovery.amount ? this.format(costRecovery.amount, decimal) : amount;
    }

    const costRecoveryValue = {
      type: costRecovery?.type,
      amount,
      currency
    };

    return {
      bsp,
      costRecovery: costRecoveryValue
    };
  }

  private checkCurrenciesFromCostRecoveries() {
    if (this.formData.globalCardCountryLevelConsentRule && this.formData.globalProductCountryLevelConsentRule) {
      const productCurrencies = this.formData.globalProductCountryLevelConsentRule.costRecoveries.filter(
        item => item.costRecovery.type === CostRecoveryType.Fixed && item.bsp?.id && item.costRecovery.currency
      );
      const cardCurrencies = this.formData.globalCardCountryLevelConsentRule.costRecoveries.filter(
        item => item.costRecovery.type === CostRecoveryType.Fixed && item.bsp?.id && item.costRecovery.currency
      );
      //* Check and add to the list of currencies by bsp if there are any case that the response of backend lost any currency
      uniqBy([...productCurrencies, ...cardCurrencies], 'costRecovery.currency.id').forEach(costRecovery =>
        this.checkAndLoadLostCurrency(costRecovery.costRecovery.currency, costRecovery.bsp.id)
      );
    }
  }

  // Add a currency deactivated that not being received from back end and it is necessary when already exist in the costRecovery
  private checkAndLoadLostCurrency(currency: Currency, bspId: number) {
    const lostCurrency: DropdownOption<Currency> = {
      label: currency.code,
      value: currency
    };

    this.currencyOptions$[bspId].pipe(first()).subscribe((items: DropdownOption<Currency>[]) => {
      if (!items.map(x => x.label).includes(lostCurrency.label)) {
        this.currencyOptions$[bspId] = of([lostCurrency, ...items]);
      }
    });
  }

  public checkAndLoadCurrencyByBsp(event) {
    const bspId = event?.id;
    if (bspId && !this.currencyOptions$[bspId]) {
      this.currencyOptions$[bspId] = this.currencyDictionaryService
        .getTipDropdownOptions(bspId, true)
        .pipe(takeUntil(this.$destroy));
    }
  }

  public onChangeBsp(bsp, collection: FormArray, costRecoveryType: string, index: number) {
    if (costRecoveryType === CostRecoveryType.Fixed) {
      const currency = collection.at(index).get('costRecovery.currency');
      const amount = collection.at(index).get('costRecovery.amount');
      currency.reset();
      amount.reset();
    }
    this.checkAndLoadCurrencyByBsp(bsp);
  }

  public showProductTypeControlError() {
    const element = this.form.get('globalProductCountryLevelConsentRule.type');
    FormUtil.showControlState(element);
  }

  public showCardTypeControlError() {
    const element = this.form.get('globalCardCountryLevelConsentRule.type');
    FormUtil.showControlState(element);
  }

  public showCostRecoveryTypeControlError(collection: FormArray, index: number) {
    FormUtil.showControlState(collection.controls[index].get('costRecovery.type'));
  }

  public showCostRecoveryCurrencyControlError(collection: FormArray, index: number) {
    FormUtil.showControlState(collection.controls[index].get('costRecovery.currency'));
  }

  public showCostRecoveryBspControlError(collection: FormArray, index: number) {
    FormUtil.showControlState(collection.controls[index].get('bsp'));
  }

  private format(value, decimals = 2): string {
    return NumberHelper.formatNumber(value, decimals);
  }

  public ngOnDestroy() {
    this.$destroy.next();
  }
}
