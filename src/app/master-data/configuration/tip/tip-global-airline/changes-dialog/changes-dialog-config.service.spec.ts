import { TestBed } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { EMPTY_VALUE } from '~app/shared/pipes/empty.pipe';

describe('ChangesDialogConfigService', () => {
  let service: ChangesDialogConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangesDialogConfigService, mockProvider(L10nTranslationService)]
    });

    service = TestBed.inject(ChangesDialogConfigService);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('getArrayItemsValuesFormatter', () => {
    it('should return empty valuewhen costRecoveries is empty', () => {
      const res = (service as any).getArrayItemsValuesFormatter([]);

      expect(res).toEqual(EMPTY_VALUE);
    });

    it('should return corresponding value with not empty properties when costRecoveries is NOT empty', () => {
      const param = [
        {
          bsp: {
            isoCountryCode: 'bspCode'
          },
          costRecovery: {
            type: 'type',
            currency: {
              code: 'currencyCode'
            },
            amount: '10'
          }
        }
      ] as any;
      (service as any).translation.translate = jasmine.createSpy().and.returnValue('type');
      const expectedResult = 'bspCode /   type /  currencyCode /  10';

      const res = (service as any).getArrayItemsValuesFormatter(param);

      expect(res).toEqual(expectedResult);
    });

    it('should return corresponding value with empty properties when costRecoveries is NOT empty', () => {
      const param = [
        {
          costRecovery: {}
        }
      ] as any;

      const res = (service as any).getArrayItemsValuesFormatter(param);

      expect(res).toEqual('');
    });

    it('should return value with empty properties when costRecoveries is NOT empty and costRecovery is empty', () => {
      const param = [{}] as any;

      const res = (service as any).getArrayItemsValuesFormatter(param);

      expect(res).toEqual('');
    });
  });
});
