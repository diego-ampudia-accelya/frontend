export const translations = {
  applyChanges: {
    title: 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.applyChanges.title',
    details: 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.applyChanges.message'
  },
  unsavedChanges: {
    title: 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.unsavedChanges.title',
    details: 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.unsavedChanges.message'
  },
  remove: {
    title: 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.remove.title',
    details: 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.remove.message'
  }
};
