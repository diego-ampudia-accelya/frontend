import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { combineLatest, defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { TipGlobalAirlineService } from '../services/tip-globall-airline.service';
import { TipGlobalAirlineActions } from '../store/actions';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { TipGlobalAirlineModel } from '~app/master-data/configuration/models/tip-global-airline.model';
import * as fromConfigurationTips from '~app/master-data/configuration/store/reducers';
import { AppState } from '~app/reducers';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    public configBuilder: ChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: TipGlobalAirlineService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return combineLatest([
      this.store.pipe(select(fromConfigurationTips.getChangesTipGlobalAirline)),
      this.store.pipe(select(fromConfigurationTips.getHasInvalidStatusTipGlobalAirline))
    ]).pipe(
      first(),
      switchMap(([modifications, isInvalidState]) =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, question, isInvalidState)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    modifications: Array<{ [P in keyof Partial<TipGlobalAirlineModel>]: TipGlobalAirlineModel[P] }>,
    question: { title: string; details: string },
    isInvalidState: boolean
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, question, isInvalidState);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      withLatestFrom(this.store.pipe(select(fromConfigurationTips.getTipGlobalAirlineValue))),
      switchMap(([action, value]) => {
        let dialogResult$ = of(action);
        if (action === FooterButton.Apply) {
          dialogResult$ = this.apply(value, dialogConfig);
        } else if (action === FooterButton.Discard) {
          this.store.dispatch(TipGlobalAirlineActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(value: TipGlobalAirlineModel, dialogConfig: ChangesDialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(value).pipe(
        tap((newValue: TipGlobalAirlineModel) =>
          this.store.dispatch(TipGlobalAirlineActions.applyChangesSuccess({ value: newValue }))
        ),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
