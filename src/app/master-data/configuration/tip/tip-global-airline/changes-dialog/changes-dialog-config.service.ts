import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { CostRecovery, TipGlobalAirlineModel } from '~app/master-data/configuration/models/tip-global-airline.model';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';
import { EMPTY_VALUE } from '~app/shared/pipes/empty.pipe';

@Injectable({ providedIn: 'root' })
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: Array<{ [P in keyof Partial<TipGlobalAirlineModel>]: TipGlobalAirlineModel[P] }>,
    question: { title: string; details: string },
    isInvalidState: boolean
  ): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: this.formatChanges(modifications),
      hasInvalidChanges: isInvalidState,
      message,
      usePillsForChanges: true
    };
  }

  private formatChanges(
    modifications: Array<{ [P in keyof Partial<TipGlobalAirlineModel>]: TipGlobalAirlineModel[P] }>
  ): ChangeModel[] {
    return modifications.reduce(
      (acc: ChangeModel[], item: { [P in keyof Partial<TipGlobalAirlineModel>]: TipGlobalAirlineModel[P] }) =>
        this.isConsentRule(Object.keys(item)[0])
          ? [...acc, ...this.getArrayItemsFormat(item)]
          : [...acc, this.getItemFormat(item)],
      []
    );
  }

  private isConsentRule(key): boolean {
    const managementRules: Array<keyof Partial<TipGlobalAirlineModel>> = [
      'globalProductCountryLevelConsentRule',
      'globalCardCountryLevelConsentRule'
    ];

    return managementRules.includes(key);
  }

  private getItemFormat(item: { [P in keyof Partial<TipGlobalAirlineModel>]: TipGlobalAirlineModel[P] }): ChangeModel {
    const sectionEnrolledProducts: Array<keyof Partial<TipGlobalAirlineModel>> = [
      'globalProductCountryLevelConsentManagement',
      'globalProductCountryLevelConsentRule',
      'globalProductAgentLevelConsentManagement'
    ];

    const key: keyof TipGlobalAirlineModel = Object.keys(item)[0] as keyof TipGlobalAirlineModel;

    const groupValue = sectionEnrolledProducts.includes(key)
      ? 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.productGroup'
      : 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.cardGroup';

    const value = this.translation.translate(
      `menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.values.${item[key]}`
    );

    const summary = `menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.sectionsSummary.${key}`;
    const description = `menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.sections.${key}`;

    const groupValueTranslated = this.translation.translate(groupValue);

    return this.toChangeModel(groupValueTranslated, summary, description, value);
  }

  private getArrayItemsFormat(item: {
    [P in keyof Partial<TipGlobalAirlineModel>]: TipGlobalAirlineModel[P];
  }): ChangeModel[] {
    const result: ChangeModel[] = [];

    const sectionEnrolledProducts: Array<keyof Partial<TipGlobalAirlineModel>> = [
      'globalProductCountryLevelConsentManagement',
      'globalProductCountryLevelConsentRule',
      'globalProductAgentLevelConsentManagement'
    ];

    const key: keyof TipGlobalAirlineModel = Object.keys(item)[0] as keyof TipGlobalAirlineModel;

    const groupValue = sectionEnrolledProducts.includes(key)
      ? 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.productGroup'
      : 'menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.cardGroup';

    Object.keys(item[key]).forEach(property => {
      const itemValue = item[key][property];

      const value: string = this.getValueForChangesModel(itemValue);

      const summary = `menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.sectionsSummary.${key}.${property}`;
      const description = `menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.sections.${key}.${property}`;
      const groupValueTranslated = this.translation.translate(groupValue);

      result.push(this.toChangeModel(groupValueTranslated, summary, description, value));
    });

    return result;
  }

  private getValueForChangesModel(itemValue: any): string {
    if (Array.isArray(itemValue)) {
      return this.getArrayItemsValuesFormatter(itemValue);
    }

    return itemValue
      ? this.translation.translate(
          `menu.masterData.configuration.tipSettings.tipGlobal.changesDialog.values.${itemValue}`
        )
      : EMPTY_VALUE;
  }

  private getArrayItemsValuesFormatter(costRecoveries: CostRecovery[]): string {
    let result: string[] = [EMPTY_VALUE];

    if (costRecoveries.length) {
      result = costRecoveries.map(this.getFormattedCostRecovery);
    }

    return result.toString();
  }

  private toChangeModel(group: string, summary: string, description: string, value: string): ChangeModel {
    const summaryTranslation = `${this.translation.translate(summary)}: ${value}`;
    const descriptionTranslation = `${this.translation.translate(description)}: ${value}`;

    return {
      group,
      name: summaryTranslation,
      value,
      tooltip: descriptionTranslation
    };
  }

  private getFormattedCostRecovery = (costRecovery: CostRecovery): string => {
    const bsp = costRecovery.bsp ? `${costRecovery.bsp.isoCountryCode} / ` : '';

    const currentCostRecovery = costRecovery.costRecovery;

    if (currentCostRecovery) {
      const costRecoveryTypeKey = `menu.masterData.configuration.tipSettings.tipGlobal.costRecoveryType.${currentCostRecovery.type}`;
      const costRecoveryType = currentCostRecovery.type ? ` ${this.translation.translate(costRecoveryTypeKey)} / ` : '';
      const currency = currentCostRecovery.currency ? `${currentCostRecovery.currency.code} / ` : '';
      const amount = currentCostRecovery.amount ? `${currentCostRecovery.amount}` : '';

      return `${bsp} ${costRecoveryType} ${currency} ${amount}`.trim();
    }

    return `${bsp}`.trim();
  };
}
