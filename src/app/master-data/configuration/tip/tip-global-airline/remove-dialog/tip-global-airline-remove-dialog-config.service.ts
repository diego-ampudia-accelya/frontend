import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable({ providedIn: 'root' })
export class TipGlobalAirlineRemoveDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(question: { title: string; details: string }): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Remove }]
      },
      changes: null,
      message
    };
  }
}
