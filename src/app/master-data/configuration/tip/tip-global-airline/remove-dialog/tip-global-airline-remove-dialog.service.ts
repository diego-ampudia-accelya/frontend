import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize, first, map } from 'rxjs/operators';

import { translations } from '../changes-dialog/translations';

import { TipGlobalAirlineRemoveDialogConfigService } from './tip-global-airline-remove-dialog-config.service';
import { ChangesDialogComponent, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class TipGlobalAirlineRemoveDialogService {
  constructor(private dialogService: DialogService, private configBuilder: TipGlobalAirlineRemoveDialogConfigService) {}

  public open(): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(translations.remove);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      finalize(() => this.dialogService.close()),
      first()
    );
  }
}
