import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';

import { TipGlobalAirlineActions } from '../store/actions';
import { TipGlobalAirlineService } from './tip-globall-airline.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { getConfigurationAirline, getSelectedAirline } from '~app/master-data/airline/reducers';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';

@Injectable()
export class TipGlobalAirlineResolver implements Resolve<any> {
  constructor(
    private permissionsService: PermissionsService,
    private store: Store<AppState>,
    private tipGlobalAirlineService: TipGlobalAirlineService
  ) {}

  resolve(): Observable<any> {
    const isReadOnlyMode = !this.permissionsService.hasPermission(Permissions.updateGlobalAirlineConfiguration);
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    const getAirline$ = hasLeanPermission
      ? this.store.pipe(select(getConfigurationAirline), first())
      : this.store.pipe(select(getSelectedAirline), first());

    return getAirline$.pipe(
      tap(airline =>
        this.store.dispatch(TipGlobalAirlineActions.load({ isReadOnlyMode, resourceId: '' + airline.id }))
      ),
      // TODO: refactor within an effect `load$`
      switchMap(_ => this.tipGlobalAirlineService.get()),
      map(data => this.store.dispatch(TipGlobalAirlineActions.loadSuccess({ value: data })))
    );
  }
}
