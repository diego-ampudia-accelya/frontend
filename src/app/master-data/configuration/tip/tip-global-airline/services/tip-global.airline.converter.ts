import { AutomaticConsents } from '~app/master-data/configuration/enum/automatic-consents-tips.enum';
import { CostRecoveryType } from '~app/master-data/configuration/enum/cost-recovery-type.enum';
import {
  CostRecovery,
  GlobalCountryLevelConsentRule,
  TipGlobalAirlineModel
} from '~app/master-data/configuration/models/tip-global-airline.model';

export const stringToBoolean = (value: string) => value === 'true';
export const isCostRecoveryType = (value: string) => value === AutomaticConsents.Acr;
export const isTypePercentage = (value: string) => value === CostRecoveryType.Percentage;

const formatConsentRule = (
  consentType: AutomaticConsents,
  costRecoveries: CostRecovery[]
): GlobalCountryLevelConsentRule => ({
  type: consentType,
  costRecoveries: isCostRecoveryType(consentType) ? costRecoveries : []
});

export const tipGlobalAirlineAdjustment = (data: TipGlobalAirlineModel): TipGlobalAirlineModel => ({
  ...data,
  globalProductCountryLevelConsentRule: data.globalProductCountryLevelConsentManagement
    ? formatConsentRule(
        data.globalProductCountryLevelConsentRule?.type,
        data.globalProductCountryLevelConsentRule?.costRecoveries
      )
    : { type: null, costRecoveries: [] },
  globalCardCountryLevelConsentRule: data.globalCardConsentManagement
    ? formatConsentRule(
        data.globalCardCountryLevelConsentRule?.type,
        data.globalCardCountryLevelConsentRule?.costRecoveries
      )
    : { type: null, costRecoveries: [] }
});

const formatConsentRuleToBE = (
  consentType: AutomaticConsents,
  costRecoveries: CostRecovery[]
): GlobalCountryLevelConsentRule => ({
  type: consentType,
  costRecoveries: isCostRecoveryType(consentType) ? costRecoveries : null
});

export const tipGlobalAirlineToBE = (data: TipGlobalAirlineModel) => ({
  ...data,
  globalCardCountryLevelConsentRule: data.globalCardConsentManagement
    ? formatConsentRuleToBE(
        data.globalCardCountryLevelConsentRule.type,
        data.globalCardCountryLevelConsentRule.costRecoveries
      )
    : null,
  globalProductCountryLevelConsentRule: data.globalProductCountryLevelConsentManagement
    ? formatConsentRuleToBE(
        data.globalProductCountryLevelConsentRule.type,
        data.globalProductCountryLevelConsentRule.costRecoveries
      )
    : null
});
