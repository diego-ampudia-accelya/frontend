import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { tipGlobalAirlineAdjustment, tipGlobalAirlineToBE } from './tip-global.airline.converter';
import { TipGlobalAirlineModel } from '~app/master-data/configuration/models/tip-global-airline.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class TipGlobalAirlineService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/global-airline-configuration`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public get(): Observable<TipGlobalAirlineModel> {
    return this.http.get<TipGlobalAirlineModel>(this.baseUrl).pipe(map(tipGlobalAirlineAdjustment));
  }

  public save(tipGlobalAirline: TipGlobalAirlineModel): Observable<TipGlobalAirlineModel> {
    return this.http
      .put<TipGlobalAirlineModel>(this.baseUrl, tipGlobalAirlineToBE(tipGlobalAirline))
      .pipe(map(tipGlobalAirlineAdjustment));
  }
}
