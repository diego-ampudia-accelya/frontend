import { BspDto } from '~app/shared/models/bsp.model';

export interface TipReport {
  id: string;
  bsp: BspDto;
  reportDescription: string;
  fileId: number;
  formatValues: TipReportFormat[];
  format: TipReportFormat;
  frequencyValues: TipReportFrequency[];
  frequency: TipReportFrequency;
  channelValues: TipReportChannel[];
  channel: TipReportChannel;
  bspCode: string;
  user?: string;
  threshold?: string;
  airline?: AirlineDto;
  agent?: AgentDto;
}

export interface TipReportOption {
  id: string;
  bsp: BspDto;
  reportDescription: string;
  fileId: number;
  formatValues: TipReportFormat[];
  frequencyValues: TipReportFrequency[];
  channelValues: TipReportChannel[];
  airline?: AirlineDto;
  agent?: AgentDto;
  threshold?: boolean;
}

export interface TipReportViewModel {
  option: TipReportOptionViewModel;
  format: TipReportFormat;
  frequency: TipReportFrequency;
  channel: TipReportChannel;
  bsp: BspDto;
  bspCode?: string;
  user?: string;
  threshold?: string;
}

export interface TipReportOptionViewModel {
  /** This `id` has the same value as the `fileId` property of the model, it's necessary to populate the dropdown correctly */
  id: number;
  reportDescription: string;
  fileId: number;
  formatValues: TipReportFormat[];
  frequencyValues: TipReportFrequency[];
  channelValues: TipReportChannel[];
  threshold?: boolean;
  bspCode?: string;
}

interface AirlineDto {
  id: number;
  iataCode: string;
}

interface AgentDto {
  id: number;
  iataCode: string;
}

export enum TipReportFormat {
  Csv = 'CSV',
  Pdf = 'PDF'
}

export enum TipReportFrequency {
  Daily = 'Daily',
  Monthly = 'Monthly'
}

export enum TipReportChannel {
  Bsplink = 'BSPlink',
  Portal = 'Portal'
}
