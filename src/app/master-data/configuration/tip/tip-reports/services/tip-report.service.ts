import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Queryable } from '~app/shared/components/list-view';
import { DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { TipReport, TipReportOption, TipReportOptionViewModel, TipReportViewModel } from '../models/tip-report.model';

@Injectable()
export class TipReportService implements Queryable<TipReport> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/reports-config`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(): Observable<PagedData<TipReport>> {
    const url = `${this.baseUrl}/user`;

    return this.http.get<PagedData<TipReport>>(url);
  }

  public findOptions(): Observable<DropdownOption<TipReportOptionViewModel>[]> {
    const url = `${this.baseUrl}/type`;

    return this.http
      .get<PagedData<TipReportOption>>(url)
      .pipe(map(({ records }) => records.map(this.toDropdownOptionViewModel)));
  }

  public create(report: TipReportViewModel): Observable<any> {
    const url = `${this.baseUrl}/user`;

    return this.http.post(url, [this.toDataModel(report)]);
  }

  public update(report: TipReport & TipReportViewModel): Observable<any> {
    const url = `${this.baseUrl}/user/${report.id}`;

    return this.http.put(url, this.toDataModel(report));
  }

  public delete(report: TipReport): Observable<any> {
    const url = `${this.baseUrl}/user/${report.id}`;

    return this.http.delete(url);
  }

  public toFormModel(report: TipReport): TipReportViewModel {
    const {
      format,
      frequency,
      channel,
      bsp,
      user,
      threshold,
      reportDescription,
      fileId,
      formatValues,
      frequencyValues,
      channelValues,
      bspCode
    } = report;

    return {
      format,
      frequency,
      channel,
      bsp,
      user,
      threshold,
      option: { id: fileId, reportDescription, fileId, formatValues, frequencyValues, channelValues, bspCode }
    };
  }

  private toDropdownOptionViewModel(report: TipReportOption): DropdownOption<TipReportOptionViewModel> {
    const { reportDescription, fileId, formatValues, frequencyValues, channelValues, threshold } = report;

    return {
      value: { id: fileId, reportDescription, fileId, formatValues, frequencyValues, channelValues, threshold },
      label: reportDescription
    };
  }

  private toDataModel(report: TipReportViewModel | (TipReport & TipReportViewModel)) {
    const { option, format, frequency, channel, bsp, user, threshold } = report;

    return { fileId: option.fileId, format, frequency, channel, bspCode: bsp?.isoCountryCode, user, threshold };
  }
}
