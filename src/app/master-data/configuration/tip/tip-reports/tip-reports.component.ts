import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter, finalize, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ConfirmationDialogComponent,
  DialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import {
  EditableListColumn,
  EditableListColumnControlType
} from '~app/shared/components/editable-list/models/editable-list-column.model';
import { QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants';
import { ArrayHelper, FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import {
  TipReport,
  TipReportChannel,
  TipReportFormat,
  TipReportFrequency,
  TipReportOption,
  TipReportViewModel
} from './models/tip-report.model';
import { TipReportService } from './services/tip-report.service';

@Component({
  selector: 'bspl-tip-reports',
  templateUrl: './tip-reports.component.html',
  providers: [QueryableDataSource, { provide: QUERYABLE, useExisting: TipReportService }]
})
export class TipReportsComponent implements OnInit, OnDestroy {
  public columns: EditableListColumn[];
  public form: FormGroup;

  public translationPrefix = 'menu.masterData.configuration.tipSettings.tipReports';
  public buttonDesign = ButtonDesign;

  private formFactory: FormUtil;

  private destroy$ = new Subject();
  private _optionControlLockedSubject: Subject<boolean> = new BehaviorSubject(false);
  private optionControlLocked$: Observable<boolean> = this._optionControlLockedSubject.asObservable();

  private _formatOptionsSubject: Subject<DropdownOption<TipReportFormat>[]> = new BehaviorSubject([]);
  private formatOptions$: Observable<DropdownOption<TipReportFormat>[]> = this._formatOptionsSubject.asObservable();

  private _frequencyOptionsSubject: Subject<DropdownOption<TipReportFrequency>[]> = new BehaviorSubject([]);
  private frequencyOptions$: Observable<DropdownOption<TipReportFrequency>[]> =
    this._frequencyOptionsSubject.asObservable();

  private _channelOptionsSubject: Subject<DropdownOption<TipReportChannel>[]> = new BehaviorSubject([]);
  private channelOptions$: Observable<DropdownOption<TipReportChannel>[]> = this._channelOptionsSubject.asObservable();

  private bspOptions$ = this.loggedUser$.pipe(switchMap(user => this.bspDictionary.getDropdownOptions(user.id)));
  private bspOptions: DropdownOption<BspDto>[];

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  constructor(
    public dataSource: QueryableDataSource<TipReport>,
    public dataService: TipReportService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private bspDictionary: BspsDictionaryService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.columns = this.buildColumns();
    this.initializeForm();
    // Load data
    this.dataSource.get();
    this.getBspOptions();
  }

  public onCreateReport(report: TipReportViewModel): void {
    this.dataService
      .create(report)
      .pipe(
        finalize(() => {
          this.initializeForm();
          this.dataSource.get();
        })
      )
      .subscribe(() => {
        this.notificationService.showSuccess(
          this.translationService.translate(`${this.translationPrefix}.actions.create.successMessage`, {
            report: report.option.reportDescription
          })
        );
      });
  }

  public onUpdateReport(report: TipReport & TipReportViewModel): void {
    this.dataService
      .update(report)
      .pipe(
        finalize(() => {
          // Option selection is locked on edit action, so we revert it here
          this._optionControlLockedSubject.next(false);

          this.initializeForm();
          this.dataSource.get();
        })
      )
      .subscribe(() => {
        this.notificationService.showSuccess(
          this.translationService.translate(`${this.translationPrefix}.actions.update.successMessage`, {
            report: report.option.reportDescription
          })
        );
      });
  }

  public onDeleteReport(report: TipReport): void {
    this.confirmDelete(report)
      .pipe(
        filter(isConfirmed => isConfirmed),
        switchMap(() => this.dataService.delete(report))
      )
      .subscribe(() => {
        this.dataSource.get();

        this.notificationService.showSuccess(
          this.translationService.translate(`${this.translationPrefix}.actions.delete.successMessage`, {
            report: report.reportDescription
          })
        );
      });
  }

  public onCancelReport(): void {
    this.initializeForm();
  }

  /** Manages form controls on editable item `create` and `edit` action */
  public onEditModeChange(inCreateEditMode: boolean) {
    if (!inCreateEditMode) {
      return;
    }

    const optionControl = FormUtil.get<TipReportViewModel>(this.form, 'option');

    // If option control has value editable item is in `edit` mode, if not editable item is in `create` mode
    if (optionControl.value) {
      // Load dropdown options
      this._formatOptionsSubject.next(ArrayHelper.toDropdownOptions(optionControl.value.formatValues));
      this._frequencyOptionsSubject.next(ArrayHelper.toDropdownOptions(optionControl.value.frequencyValues));
      this._channelOptionsSubject.next(ArrayHelper.toDropdownOptions(optionControl.value.channelValues));
      this.setBspFormValue(optionControl.value.bspCode);

      // Option selection is locked on edit action
      this._optionControlLockedSubject.next(true);
    } else {
      const formatControl = FormUtil.get<TipReportViewModel>(this.form, 'format');
      const frequencyControl = FormUtil.get<TipReportViewModel>(this.form, 'frequency');
      const channelControl = FormUtil.get<TipReportViewModel>(this.form, 'channel');

      // All these controls are disabled on create action
      formatControl.disable();
      frequencyControl.disable();
      channelControl.disable();
      this._optionControlLockedSubject.next(false);
    }
  }

  public checkReports(): void {
    this.router.navigate([ROUTES.REPORTS.url]);
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  private getBspOptions(): void {
    this.bspOptions$.pipe(takeUntil(this.destroy$)).subscribe(options => (this.bspOptions = options));
  }

  private setBspFormValue(bspCode: string): void {
    const bsp = this.bspOptions.find(option => option.value.isoCountryCode === bspCode);
    if (bsp) {
      const bspValue = {
        id: bsp.value.id,
        isoCountryCode: bsp.value.isoCountryCode,
        name: ''
      };
      this.form.get('bsp').setValue(bspValue, { emitEvent: false });
    }
  }

  private buildColumns(): EditableListColumn[] {
    const translationColumnPrefix = `${this.translationPrefix}.columns`;

    return [
      {
        name: `${translationColumnPrefix}.description.name`,
        prop: 'reportDescription',
        width: 40,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'option',
          placeholder: `${translationColumnPrefix}.description.placeholder`,
          locked: this.optionControlLocked$,
          options: this.dataService.findOptions()
        }
      },
      {
        name: `${translationColumnPrefix}.format.name`,
        prop: 'format',
        width: 15,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'format',
          placeholder: `${translationColumnPrefix}.format.placeholder`,
          tooltip: `${translationColumnPrefix}.format.tooltip`,
          options: this.formatOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.frequency.name`,
        prop: 'frequency',
        width: 15,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'frequency',
          placeholder: `${translationColumnPrefix}.frequency.placeholder`,
          tooltip: `${translationColumnPrefix}.frequency.tooltip`,
          options: this.frequencyOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.channel.name`,
        prop: 'channel',
        width: 15,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'channel',
          placeholder: `${translationColumnPrefix}.channel.placeholder`,
          tooltip: `${translationColumnPrefix}.channel.tooltip`,
          options: this.channelOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.bsp.name`,
        prop: 'bspCode',
        width: 15,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'bsp',
          placeholder: `${translationColumnPrefix}.bsp.placeholder`,
          options: this.bspOptions$
        }
      }
    ];
  }

  private initializeForm(): void {
    this.form = this.formFactory.createGroup<TipReportViewModel>({
      option: [],
      format: [],
      frequency: [],
      channel: [],
      bsp: []
    });

    this.initializeFormListeners();
  }

  private initializeFormListeners(): void {
    const optionControl = FormUtil.get<TipReportViewModel>(this.form, 'option');
    const formatControl = FormUtil.get<TipReportViewModel>(this.form, 'format');
    const frequencyControl = FormUtil.get<TipReportViewModel>(this.form, 'frequency');
    const channelControl = FormUtil.get<TipReportViewModel>(this.form, 'channel');

    optionControl.valueChanges.subscribe((value: TipReportOption) => {
      // Load dropdown options
      this._formatOptionsSubject.next(ArrayHelper.toDropdownOptions(value.formatValues));
      this._frequencyOptionsSubject.next(ArrayHelper.toDropdownOptions(value.frequencyValues));
      this._channelOptionsSubject.next(ArrayHelper.toDropdownOptions(value.channelValues));

      formatControl.reset();
      frequencyControl.reset();
      channelControl.reset();

      // Enabling form and child controls when a report option is selected
      this.form.enable({ emitEvent: false });
    });
  }

  private confirmDelete(report: TipReport): Observable<boolean> {
    const config: DialogConfig = {
      data: {
        title: `${this.translationPrefix}.actions.delete.dialog.title`,
        question: `${this.translationPrefix}.actions.delete.dialog.question`,
        pills: [{ value: report.reportDescription, tooltip: report.reportDescription }],
        hasCancelButton: true,
        footerButtonsType: FooterButton.Delete
      }
    };

    return this.dialogService.open(ConfirmationDialogComponent, config).pipe(
      map(action => action.clickedBtn === FooterButton.Delete),
      tap(() => this.dialogService.close())
    );
  }
}
