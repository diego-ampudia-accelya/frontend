import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, first, map, skipWhile, switchMap, takeUntil } from 'rxjs/operators';

import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { CanComponentDeactivate } from '~app/core/services';
import { ChangesDialogService } from '~app/master-data/configuration/tip/tip-local-airline/changes-dialog/changes-dialog.service';
import {
  ConsentRuleModel,
  CostRecoveryGroup,
  CostRecoveryModel,
  TipConsents,
  TipCostRecoveryType,
  TipCostRecoveryTypeLabel,
  TipLocalAirlineModel
} from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';
import { tipAirlineConverter } from '~app/master-data/configuration/tip/tip-local-airline/services/tip-airline-converter';
import { TipLocalAirlineActions } from '~app/master-data/configuration/tip/tip-local-airline/store/actions';
import { getTipLocalAirline } from '~app/master-data/configuration/tip/tip-local-airline/store/reducers/tip-local-airline.reducer';
import { tipLocalAirlineFixAmountValidator } from '~app/master-data/configuration/tip/tip-local-airline/tip-local-airline/validators/tip-local-airline-fix-amount.validator';
import { tipLocalAirlinePercentageValidator } from '~app/master-data/configuration/tip/tip-local-airline/tip-local-airline/validators/tip-local-airline-percentage.validator';
import { AppState } from '~app/reducers';
import { ButtonDesign, FooterButton } from '~app/shared/components';
import { GLOBALS } from '~app/shared/constants';
import { FormUtil, NumberHelper, toValueLabelObject } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { positiveNumberValidator } from '~app/shared/validators';

@Component({
  selector: 'bspl-tip-local-airline',
  templateUrl: './tip-local-airline.component.html',
  styleUrls: ['./tip-local-airline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipLocalAirlineComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  public form: FormGroup;
  public costRecoveryForm: FormGroup;
  public tipLocalConsentsOptions: DropdownOption[] = [];
  public tipLocalCostRecoveryOptions: DropdownOption[] = [];
  public currenciesOptions$: Observable<DropdownOption<Currency>[]>;
  public isCostRecoveryParamsHidden = true;
  public isCurrencyLocked = true;
  private currencySelected: Currency;

  private tipInfoData: TipLocalAirlineModel;
  public productCountryLevelConsentRule: keyof TipLocalAirlineModel = 'productCountryLevelConsentRule';
  public costRecovery: keyof ConsentRuleModel = 'costRecovery';

  public btnDesign = ButtonDesign;
  public selectPlaceholder = `menu.masterData.configuration.tipSettings.tipLocal.selectPlaceholder`;
  public amountPlaceholder = `menu.masterData.configuration.tipSettings.tipLocal.amountPlaceholder`;
  public amountPercentageBaseLabel = `menu.masterData.configuration.tipSettings.tipLocal.costRecovery.labels.`;
  public costRecoveryLabel = `${this.amountPercentageBaseLabel}${TipCostRecoveryTypeLabel.Fixed}`;

  private $destroy = new Subject();
  private formUtil: FormUtil;
  private userBsps$ = this.store.pipe(select(fromAuth.getUserBsps), first());
  private canUpdateForm = false;

  private readonly DEFAULT_DECIMALS = 2;

  public get decimalsPattern(): RegExp {
    return GLOBALS.PATTERNS.DECIMALS_PATTERN;
  }

  private get decimals(): number {
    return this.currencySelected ? this.currencySelected.decimals : this.DEFAULT_DECIMALS;
  }

  constructor(
    private changesDialogService: ChangesDialogService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private store: Store<AppState>,
    private cd: ChangeDetectorRef
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.buildForm();
    this.initializeFormData();
    this.initializeConsentsDropdown();
    this.initializeConsentListener();
    this.initializeFormChangesListener();
    this.initializeDropDowns();
    this.initializeAmountListener();
  }

  private initializeFormData() {
    this.getTipLocalAirline$()
      .pipe(takeUntil(this.$destroy))
      .subscribe(value => {
        if (value) {
          this.tipInfoData = value;
          this.updateForm();
          this.cd.markForCheck();
        }
      });
  }

  private initializeFormChangesListener() {
    this.form.valueChanges
      .pipe(
        takeUntil(this.$destroy),
        filter(() => !this.canUpdateForm)
      )
      .subscribe(() => {
        this.dispatchToStore();
      });
  }

  private dispatchToStore() {
    if (this.form.value && this.form.valid) {
      this.store.dispatch(TipLocalAirlineActions.modify({ value: tipAirlineConverter(this.form.value) }));
    } else {
      this.store.dispatch(TipLocalAirlineActions.invalid());
    }
  }

  private getTipLocalAirline$(): Observable<TipLocalAirlineModel> {
    return this.store.select(getTipLocalAirline);
  }

  private buildForm() {
    this.form = this.formUtil.createGroup<TipLocalAirlineModel>({
      productCountryLevelConsentRule: this.getTypeGroup()
    });
  }

  private getTypeGroup(): FormGroup {
    return this.formUtil.createGroup<ConsentRuleModel>({
      type: new FormControl(null, [Validators.required]),
      costRecovery: this.getCostRecoveryGroup()
    });
  }

  private getCostRecoveryGroup(): FormGroup {
    return this.formUtil.createGroup<CostRecoveryModel>({
      type: new FormControl(null),
      amount: new FormControl({ value: null, disabled: true }, { updateOn: 'blur' }),
      currency: new FormControl(null)
    });
  }

  private getFormsControls(): CostRecoveryGroup {
    const mainForm = FormUtil.get<TipLocalAirlineModel>(this.form, 'productCountryLevelConsentRule') as FormGroup;
    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(mainForm, 'costRecovery') as FormGroup;
    const consentTypeControl = FormUtil.get<ConsentRuleModel>(mainForm, 'type') as FormControl;
    const typeControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'type') as FormControl;
    const currencyGroup = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'currency') as FormControl;
    const amountControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'amount') as FormControl;

    const formControls: CostRecoveryGroup = {
      costRecoveryGroup,
      consentTypeControl,
      typeControl,
      currencyGroup,
      amountControl
    };

    return formControls;
  }

  private initializeConsentListener() {
    const { consentTypeControl } = this.getFormsControls();

    consentTypeControl.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(typeValue => {
      this.resetCostRecoveryGroup();

      if (typeValue === TipConsents.AcceptAllWithCost) {
        this.isCurrencyLocked = true;
        this.initializeCostRecoveryGroup();
        this.isCostRecoveryParamsHidden = false;
      } else {
        this.isCostRecoveryParamsHidden = true;
      }
    });
  }

  private resetCostRecoveryGroup() {
    const { costRecoveryGroup, typeControl, currencyGroup, amountControl } = this.getFormsControls();

    costRecoveryGroup.reset();

    typeControl.clearValidators();
    typeControl.updateValueAndValidity();

    currencyGroup.clearValidators();
    currencyGroup.updateValueAndValidity();

    amountControl.clearValidators();
    amountControl.updateValueAndValidity();
  }

  private initializeCostRecoveryGroup(recoveryTypeValue?: CostRecoveryModel) {
    this.canUpdateForm = true;
    if (recoveryTypeValue) {
      this.updateControlsFromData(recoveryTypeValue);
    }
    this.setCostRecoveryControlsValidators();
    this.initializeCostRecoveryTypeListener();
    this.initializeCurrencyListener();
    this.canUpdateForm = false;
  }

  private setCostRecoveryControlsValidators() {
    const { typeControl, currencyGroup } = this.getFormsControls();
    typeControl.setValidators(Validators.required);
    typeControl.updateValueAndValidity();
    currencyGroup.updateValueAndValidity();
  }

  private updateForm() {
    const { consentTypeControl } = this.getFormsControls();
    const isTypeCostRecovery = Object.values(this.tipInfoData).some(
      item => item?.type === TipConsents.AcceptAllWithCost
    );

    if (!this.tipInfoData.productCountryLevelConsentRule) {
      consentTypeControl.patchValue('', { emitEvent: false });
    }

    if (isTypeCostRecovery) {
      const recoveryTypeValue = this.tipInfoData.productCountryLevelConsentRule?.costRecovery;
      this.initializeCostRecoveryGroup(recoveryTypeValue);

      this.form.patchValue(this.tipInfoData, { emitEvent: false });
      this.isCostRecoveryParamsHidden = false;
    } else {
      consentTypeControl.patchValue(this.tipInfoData.productCountryLevelConsentRule?.type, { emitEvent: false });
      this.isCostRecoveryParamsHidden = true;
    }
  }

  private updateControlsFromData(recoveryTypeValue: CostRecoveryModel) {
    if (recoveryTypeValue.type === TipCostRecoveryType.Percentage) {
      this.updateControlsForPercentage();
    }
    if (recoveryTypeValue.type === TipCostRecoveryType.Fixed) {
      this.updateControlsForAmount();
      this.isCurrencyLocked = false;
    }
  }

  private initializeCostRecoveryTypeListener() {
    const { typeControl, amountControl, currencyGroup } = this.getFormsControls();

    typeControl.valueChanges
      .pipe(
        skipWhile(value => !value),
        takeUntil(this.$destroy)
      )
      .subscribe(() => {
        if (typeControl.value === TipCostRecoveryType.Percentage) {
          this.updateControlsForPercentage();
        }

        if (typeControl.value === TipCostRecoveryType.Fixed) {
          if (!currencyGroup.value) {
            amountControl.disable();
          } else {
            this.updateControlsForAmount();
          }
          this.isCurrencyLocked = false;
          this.initializeCurrencyListener();
        }

        amountControl.patchValue('');
        amountControl.markAsPristine();
        amountControl.markAsUntouched();
        amountControl.updateValueAndValidity();
      });
  }

  private updateControlsForPercentage() {
    const { amountControl, currencyGroup } = this.getFormsControls();

    this.isCurrencyLocked = true;
    this.costRecoveryLabel = `${this.amountPercentageBaseLabel}${TipCostRecoveryTypeLabel.Percentage}`;
    currencyGroup.clearValidators();
    currencyGroup.reset();
    amountControl.setValidators([tipLocalAirlinePercentageValidator, Validators.required]);
    amountControl.enable();
    this.initializeAmountListener();
  }

  private updateControlsForAmount() {
    const { amountControl, currencyGroup } = this.getFormsControls();
    this.costRecoveryLabel = `${this.amountPercentageBaseLabel}${TipCostRecoveryTypeLabel.Fixed}`;

    currencyGroup.setValidators([Validators.required]);
    amountControl.setValidators([
      Validators.required,
      positiveNumberValidator,
      Validators.pattern(this.decimalsPattern),
      tipLocalAirlineFixAmountValidator(this.decimals)
    ]);

    if (amountControl.value) {
      amountControl.patchValue(NumberHelper.formatNumber(amountControl.value, this.decimals), { emitEvent: false });
    }

    if (amountControl.disabled) {
      amountControl.enable();
    }
  }

  private initializeAmountListener() {
    const { amountControl, typeControl } = this.getFormsControls();

    amountControl.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(() => {
      if (
        amountControl.valid &&
        amountControl.value !== NumberHelper.formatNumber(amountControl.value, this.decimals)
      ) {
        if (typeControl.value === TipCostRecoveryType.Percentage) {
          amountControl.patchValue(NumberHelper.formatNumber(amountControl.value, this.DEFAULT_DECIMALS), {
            emitEvent: false
          });
        }

        if (typeControl.value === TipCostRecoveryType.Fixed) {
          amountControl.patchValue(NumberHelper.formatNumber(amountControl.value, this.decimals), {
            emitEvent: false
          });
        }
      }
    });
  }

  private initializeCurrencyListener() {
    const { costRecoveryGroup } = this.getFormsControls();

    costRecoveryGroup.controls.currency?.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(currencyValue => {
      if (currencyValue) {
        this.currencySelected = currencyValue;
        this.updateControlsForAmount();
      }
    });
  }

  private initializeDropDowns() {
    this.initializeCostRecoveryDropdown();
    this.initializeCurrencyDropdown();
  }

  private initializeCurrencyDropdown() {
    this.currenciesOptions$ = this.userBsps$.pipe(
      switchMap(bsps => {
        let lostCurrency: DropdownOption<Currency>;
        if (this.tipInfoData.productCountryLevelConsentRule?.costRecovery?.currency) {
          lostCurrency = {
            label: this.tipInfoData.productCountryLevelConsentRule?.costRecovery?.currency.code,
            value: this.tipInfoData.productCountryLevelConsentRule?.costRecovery?.currency
          };
        }

        return this.currencyDictionaryService.getTipDropdownOptions(bsps[0].id, true).pipe(
          map((items: DropdownOption<Currency>[]) => {
            //* if costRecovery have a currency deactivated we need add it to the list
            if (lostCurrency && !items.map(x => x.label).includes(lostCurrency.label)) {
              return [lostCurrency, ...items];
            }

            return items;
          })
        );
      })
    );
  }

  private initializeCostRecoveryDropdown() {
    this.tipLocalCostRecoveryOptions = Object.values(TipCostRecoveryType)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.translationService.translate(
          `menu.masterData.configuration.tipSettings.tipLocal.costRecovery.typeOptions.${label}`
        )
      }));
  }

  private initializeConsentsDropdown() {
    this.tipLocalConsentsOptions = Object.values(TipConsents)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.translationService.translate(
          `menu.masterData.configuration.tipSettings.tipLocal.consentRule.labels.${label}`
        )
      }));
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  public ngOnDestroy() {
    this.$destroy.next();
  }
}
