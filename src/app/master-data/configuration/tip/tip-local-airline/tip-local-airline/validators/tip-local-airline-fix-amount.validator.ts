import { AbstractControl, ValidatorFn } from '@angular/forms';

import { NumberHelper } from '~app/shared/helpers/numberHelper';

export function tipLocalAirlineFixAmountValidator(decimals: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    let result = null;

    if (control.value) {
      const rounded = NumberHelper.roundString(control.value, decimals);
      const length = control.value.length;

      if (length > rounded.length) {
        result = {
          currencyDecimals: {
            actualLength: (control.value + '').length,
            requiredLength: decimals
          }
        };
      }
    }

    return result;
  };
}
