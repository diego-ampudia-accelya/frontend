import { AbstractControl } from '@angular/forms';

export function tipLocalAirlinePercentageValidator(control: AbstractControl): {
  [key: string]: { [key: string]: any };
} {
  const regexPositiveNumberWithThreeDigitsAndTwoDecimals = /^([0-9]{1,3}){1}(\.[0-9]{1,2})?$/;
  let result = null;
  const value = control.value;
  const valueNumber = parseFloat(value);

  if (
    !value ||
    !value?.match(regexPositiveNumberWithThreeDigitsAndTwoDecimals) ||
    valueNumber < 0 ||
    valueNumber > 100
  ) {
    result = {
      locationCodePattern: {
        key: 'menu.masterData.configuration.tipSettings.tipLocal.costRecovery.percentageAmountError'
      }
    };
  }

  return result;
}
