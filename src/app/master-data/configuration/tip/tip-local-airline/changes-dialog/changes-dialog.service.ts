import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { translations } from '~app/master-data/configuration/tip/tip-local-airline/changes-dialog/translations';
import { TipLocalAirlineModel } from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';
import { tipAirlineConverter } from '~app/master-data/configuration/tip/tip-local-airline/services/tip-airline-converter';
import { TipLocalAirlineService } from '~app/master-data/configuration/tip/tip-local-airline/services/tip-local-airline.service';
import { TipLocalAirlineActions } from '~app/master-data/configuration/tip/tip-local-airline/store/actions';
import * as fromTipLocalAirline from '~app/master-data/configuration/tip/tip-local-airline/store/reducers/tip-local-airline.reducer';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ChangeModel,
  ChangesDialogComponent,
  DialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private dataService: TipLocalAirlineService,
    private translationService: L10nTranslationService
  ) {}

  private BASE_LABEL = 'menu.masterData.configuration.tipSettings.tipLocal';

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<any> {
    return combineLatest([
      this.store.pipe(select(fromTipLocalAirline.getChanges)),
      this.store.pipe(select(fromTipLocalAirline.canApplyChanges))
    ]).pipe(
      first(),
      switchMap(([modifications, canApplyChanges]) =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, question, canApplyChanges)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    modifications: Array<{ key: keyof TipLocalAirlineModel; value: any }>,
    question: { title: string; details: string },
    canApplyChanges: boolean
  ): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      message: question.details,
      changes: this.formatChanges(modifications),
      hasInvalidChanges: !canApplyChanges,
      usePillsForChanges: true
    };

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      withLatestFrom(this.store.pipe(select(fromTipLocalAirline.getTipLocalAirline))),
      switchMap(([action, value]) => {
        let dialogResult$ = of(action);
        if (action === FooterButton.Apply) {
          dialogResult$ = this.apply(value, dialogConfig);
        } else if (action === FooterButton.Discard) {
          this.store.dispatch(TipLocalAirlineActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(value: TipLocalAirlineModel, dialogConfig: DialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(tipAirlineConverter(value)).pipe(
        tap(() => this.store.dispatch(TipLocalAirlineActions.applyChangesSuccess({ value }))),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }

  private formatChanges(modifications: Array<{ key: string; value: any }>): ChangeModel[] {
    return modifications.map(item => ({
      group: `${this.BASE_LABEL}.changesDialog.name`,
      name: this.formatLabel(item),
      value: this.formatValues(item),
      tooltip: this.formatLabel(item)
    }));
  }

  private formatLabel(item): string {
    const labelChange = this.translationService.translate(`${this.BASE_LABEL}.changesDialog.sections.${item.key}`);

    return `${labelChange}: ${this.formatValues(item)}`;
  }

  private formatValues(item) {
    let result: any;

    switch (item.key) {
      case 'currency':
        result = item.value?.code;
        break;

      case 'amount':
        result = item.value;
        break;

      default:
        result = this.translationService.translate(`${this.BASE_LABEL}.changesDialog.sections.values.${item.value}`);
    }

    return result;
  }
}
