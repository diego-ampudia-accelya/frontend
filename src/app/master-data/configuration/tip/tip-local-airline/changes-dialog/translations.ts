export const translations = {
  applyChanges: {
    title: 'menu.masterData.configuration.tipSettings.tipLocal.changesDialog.applyChanges.title',
    details: 'menu.masterData.configuration.tipSettings.tipLocal.changesDialog.applyChanges.message'
  },
  unsavedChanges: {
    title: 'menu.masterData.configuration.tipSettings.tipLocal.changesDialog.unsavedChanges.title',
    details: 'menu.masterData.configuration.tipSettings.tipLocal.changesDialog.unsavedChanges.message'
  }
};
