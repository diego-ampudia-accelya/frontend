import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { isEqual } from 'lodash';

import {
  ConsentRuleModel,
  CostRecoveryModel,
  TipLocalAirlineModel
} from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';
import { tipAirlineConverter } from '~app/master-data/configuration/tip/tip-local-airline/services/tip-airline-converter';
import { TipLocalAirlineActions } from '~app/master-data/configuration/tip/tip-local-airline/store/actions';
import { AppState } from '~app/reducers';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';

export const tipLocalAirlineKey = 'tipLocalAirline';

export interface State extends AppState {
  tipLocalAirline: TipLocalAirlineState;
}

export interface TipLocalAirlineState {
  originalValue: TipLocalAirlineModel;
  value: TipLocalAirlineModel;
  isChanged: boolean;
  isLoading: boolean;
  isInvalid: boolean;
  errors: { error: ResponseErrorBE };
}

const initialState: TipLocalAirlineState = {
  originalValue: null,
  value: null,
  isChanged: false,
  isLoading: false,
  isInvalid: false,
  errors: null
};

export const reducer = createReducer(
  initialState,
  on(TipLocalAirlineActions.load, state => ({
    ...state,
    isLoading: true
  })),
  on(TipLocalAirlineActions.loadSuccess, (state, { value }) => ({
    ...state,
    isLoading: false,
    value,
    originalValue: value
  })),
  on(TipLocalAirlineActions.loadError, state => ({
    ...state,
    originalValue: null,
    value: null,
    isChanged: false,
    isLoading: false,
    isInvalid: false,
    errors: null
  })),
  on(TipLocalAirlineActions.modify, (state, { value }) => ({
    ...state,
    value: changeValue(state.value, value),
    isChanged: !isEqual(state.originalValue, { ...state.value, ...value }),
    isInvalid: false,
    errors: null
  })),
  on(TipLocalAirlineActions.invalid, state => ({
    ...state,
    isInvalid: true
  })),
  on(TipLocalAirlineActions.discard, state => ({
    ...state,
    isChanged: false,
    value: state.originalValue
  })),
  on(TipLocalAirlineActions.applyChangesSuccess, (state, { value }) => ({
    ...state,
    value,
    originalValue: value,
    isChanged: false,
    isInvalid: false,
    errors: null
  })),
  on(TipLocalAirlineActions.applyChangesError, (state, { error }) => ({
    ...state,
    isChanged: !error,
    isInvalid: !!error,
    errors: error
  }))
);

const getModifiedKeys = (seccion, originalSeccion?) =>
  Object.keys(seccion).filter(sectionKey => {
    let keyFilter = false;

    if (!originalSeccion || typeof seccion[sectionKey] === 'object') {
      if (
        seccion[sectionKey] &&
        originalSeccion &&
        originalSeccion[sectionKey] &&
        Object.keys(seccion[sectionKey]).some(
          property =>
            JSON.stringify(seccion[sectionKey][property]) !== JSON.stringify(originalSeccion[sectionKey][property])
        )
      ) {
        keyFilter = seccion[sectionKey] !== originalSeccion[sectionKey];
      }
      if ((!originalSeccion && seccion[sectionKey]) || (seccion[sectionKey] && !originalSeccion[sectionKey])) {
        keyFilter = true;
      }
    } else {
      keyFilter = seccion[sectionKey] !== originalSeccion[sectionKey];
    }

    return keyFilter;
  });

const getTipLocalAirlineChanges = ({ value, originalValue }: TipLocalAirlineState) => {
  const modifications: Array<{ key: keyof TipLocalAirlineModel; value: any }> = [];
  const consentRuleKey: keyof TipLocalAirlineModel = 'productCountryLevelConsentRule';
  const seccionValue = tipAirlineConverter(value)[consentRuleKey];
  const seccionOriginalValue = tipAirlineConverter(originalValue)[consentRuleKey];

  if (seccionValue) {
    getModifiedKeys(seccionValue, seccionOriginalValue).forEach((firstLevelKey: keyof ConsentRuleModel) => {
      if (typeof seccionValue[firstLevelKey] === 'object') {
        const originalSeccionSecondLevel = seccionOriginalValue ? seccionOriginalValue[firstLevelKey] : null;

        // TODO: implement it with a `reduce` function instead of using `forEach`
        getModifiedKeys(seccionValue[firstLevelKey], originalSeccionSecondLevel).forEach(
          (secondLevelKey: keyof CostRecoveryModel) => {
            modifications.push({
              key: secondLevelKey as keyof TipLocalAirlineModel,
              value: seccionValue[firstLevelKey][secondLevelKey]
            });
          }
        );
      } else {
        modifications.push({
          key: seccionValue[firstLevelKey] as keyof TipLocalAirlineModel,
          value: seccionValue[firstLevelKey]
        });
      }
    });
  }

  return modifications;
};

export const getTipLocalAirlineState = createFeatureSelector<State, TipLocalAirlineState>(tipLocalAirlineKey);
export const getTipLocalAirline = createSelector(getTipLocalAirlineState, state => state.value);

export const getChanges = createSelector(getTipLocalAirlineState, getTipLocalAirlineChanges);

export const changeValue = (currentValue: TipLocalAirlineModel, newValue: TipLocalAirlineModel) => ({
  ...currentValue,
  ...newValue
});

export const canApplyChanges = createSelector(
  getTipLocalAirlineState,
  state => state && !state.isInvalid && state.isChanged
);
