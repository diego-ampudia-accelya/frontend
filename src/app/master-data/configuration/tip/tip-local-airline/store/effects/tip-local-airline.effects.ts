import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { ChangesDialogService } from '~app/master-data/configuration/tip/tip-local-airline/changes-dialog/changes-dialog.service';
import { TipLocalAirlineActions } from '~app/master-data/configuration/tip/tip-local-airline/store/actions';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class TipLocalAirlineEffects {
  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TipLocalAirlineActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(() => EMPTY),
      rethrowError(error => TipLocalAirlineActions.applyChangesError({ error }))
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(TipLocalAirlineActions.applyChangesSuccess),
        tap(() =>
          this.notification.showSuccess(
            'menu.masterData.configuration.tipSettings.tipLocal.changesDialog.applyChanges.success'
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private changesDialogService: ChangesDialogService,
    private notification: NotificationService
  ) {}
}
