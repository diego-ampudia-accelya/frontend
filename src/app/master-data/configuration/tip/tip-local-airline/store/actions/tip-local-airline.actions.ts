import { createAction, props } from '@ngrx/store';

import { TipLocalAirlineModel } from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';

export const modify = createAction('[Tip Local Airline] Modify', props<{ value: TipLocalAirlineModel }>());

export const load = createAction('[Tip Local Airline] Load', props<{ airlineId: string }>());

export const loadSuccess = createAction('[Tip Local Airline] Load Success', props<{ value: TipLocalAirlineModel }>());

export const loadError = createAction('[Tip Local Airline] Load Error');

export const invalid = createAction('[Tip Local Airline] Invalid');

export const openApplyChanges = createAction('[Tip Local Airline] Open Apply Changes');

export const applyChangesSuccess = createAction(
  '[Tip Local Airline] Apply Changes Success',
  props<{ value: TipLocalAirlineModel }>()
);

export const applyChangesError = createAction('[Tip Local Airline] Apply Changes Error', props<{ error }>());

export const discard = createAction('[Tip Local Airline] Discard');
