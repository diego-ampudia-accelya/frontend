import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { TipLocalAirlineComponent } from '~app/master-data/configuration/tip/tip-local-airline';
import { ChangesDialogService } from '~app/master-data/configuration/tip/tip-local-airline/changes-dialog/changes-dialog.service';
import { TipLocalAirlineService } from '~app/master-data/configuration/tip/tip-local-airline/services/tip-local-airline.service';
import { TipLocalAirlineEffects } from '~app/master-data/configuration/tip/tip-local-airline/store/effects/tip-local-airline.effects';
import { fromTipLocalAirline } from '~app/master-data/configuration/tip/tip-local-airline/store/reducers/index';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [TipLocalAirlineComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromTipLocalAirline.tipLocalAirlineKey, fromTipLocalAirline.reducer),
    EffectsModule.forFeature([TipLocalAirlineEffects]),
    SharedModule
  ],
  providers: [TipLocalAirlineService, ChangesDialogService],
  exports: [TipLocalAirlineComponent]
})
export class TipLocalAirlineModule {}
