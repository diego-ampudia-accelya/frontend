import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { TipLocalAirlineModel } from '../models/tip-local-airline.model';
import { TipLocalAirlineState } from '../store/reducers/tip-local-airline.reducer';
import { TipLocalAirlineService } from './tip-local-airline.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { AirlineUser } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';

const mockTipLocalAirlineAmount: TipLocalAirlineModel = {
  productCountryLevelConsentRule: {
    type: 'costRecovery',
    costRecovery: {
      type: 'fixedAmount',
      amount: 12,
      currency: {
        id: 10,
        code: 'EUR',
        decimals: 2
      }
    }
  }
};

const initialState: TipLocalAirlineState = {
  originalValue: mockTipLocalAirlineAmount,
  value: mockTipLocalAirlineAmount,
  isChanged: false,
  isLoading: false,
  isInvalid: false,
  errors: null
};

const user: AirlineUser = {
  ...createAirlineUser(),
  globalAirline: {
    ...createAirlineUser().globalAirline,
    airlines: [
      {
        id: 1112,
        localName: '',
        bsp: null,
        designator: '',
        effectiveFrom: '',
        active: true,
        globalAirline: null
      }
    ]
  }
};

describe('AddressMaintenanceService', () => {
  let service: TipLocalAirlineService;
  let store: MockStore;
  let httpStub: SpyObject<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TipLocalAirlineService,
        {
          provide: AppConfigurationService,
          useValue: createSpyObject(AppConfigurationService, {
            baseApiPath: ''
          })
        },
        mockProvider(HttpClient),
        provideMockStore({ initialState })
      ]
    });

    store = TestBed.inject(MockStore);
    service = TestBed.inject(TipLocalAirlineService);
    httpStub = TestBed.inject<any>(HttpClient);
    httpStub.get.and.returnValue(of());
    httpStub.put.and.returnValue(of());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should build baseUrl from Store getUser when calling find', fakeAsync(() => {
    store.overrideSelector(fromAuth.getUser, user);
    store.refreshState();

    service['buildBaseUrl']();
    service.find().subscribe();

    tick();
    expect(httpStub.get).toHaveBeenCalledWith('/tip-management/airlines/1112/tip-configuration');
  }));

  it('should call with proper url to update tip configuration', fakeAsync(() => {
    store.overrideSelector(fromAuth.getUser, user);
    store.refreshState();

    service['buildBaseUrl']();
    service.save(mockTipLocalAirlineAmount).subscribe();

    tick();
    expect(httpStub.put).toHaveBeenCalledWith(
      '/tip-management/airlines/1112/tip-configuration',
      mockTipLocalAirlineAmount
    );
  }));
});
