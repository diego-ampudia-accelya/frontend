import { cloneDeep } from 'lodash';

import {
  TipConsents,
  TipCostRecoveryType,
  TipLocalAirlineModel
} from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';

export const tipAirlineConverter = (tipLocalAirlineData: TipLocalAirlineModel): TipLocalAirlineModel => {
  const tiplocalAirlineValues: Array<any> = Object.values(tipLocalAirlineData);
  const isNotTypeCostRecovery = tiplocalAirlineValues.some(item => item?.type !== TipConsents.AcceptAllWithCost);
  const result = cloneDeep(tipLocalAirlineData);

  if (tipLocalAirlineData.productCountryLevelConsentRule) {
    if (isNotTypeCostRecovery) {
      result.productCountryLevelConsentRule.costRecovery = null;
    } else {
      const isCostRecoveryPercentage = Object.values(tiplocalAirlineValues).some(
        item => item.costRecovery.type === TipCostRecoveryType.Percentage
      );
      if (isCostRecoveryPercentage) {
        result.productCountryLevelConsentRule.costRecovery.currency = null;
      }
    }
  }

  return result;
};
