import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { TipLocalAirlineModel } from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';
import { AppState } from '~app/reducers';
import { AirlineUser } from '~app/shared/models/user.model';
import { appConfiguration } from '~app/shared/services';

@Injectable()
export class TipLocalAirlineService {
  private baseUrl: string;

  constructor(private http: HttpClient, private store: Store<AppState>) {
    this.buildBaseUrl();
  }

  private buildBaseUrl() {
    this.store.pipe(select(getUser), first()).subscribe((user: AirlineUser) => {
      const airlineId = user?.globalAirline?.airlines[0].id;
      this.baseUrl = `${appConfiguration.baseApiPath}/tip-management/airlines/${airlineId}/tip-configuration`;
    });
  }

  public find(): Observable<TipLocalAirlineModel> {
    return this.http.get<TipLocalAirlineModel>(this.baseUrl);
  }

  public save(tipLocalAirlineModel: TipLocalAirlineModel): Observable<TipLocalAirlineModel> {
    return this.http.put<TipLocalAirlineModel>(this.baseUrl, tipLocalAirlineModel);
  }
}
