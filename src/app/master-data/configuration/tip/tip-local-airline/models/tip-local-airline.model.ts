import { FormControl, FormGroup } from '@angular/forms';

import { Currency } from '~app/shared/models/currency.model';

export interface TipLocalAirlineModel {
  productCountryLevelConsentRule: ConsentRuleModel;
}

export interface ConsentRuleModel {
  type: string;
  costRecovery?: CostRecoveryModel;
}

export interface CostRecoveryModel {
  type: string;
  amount: number;
  currency?: Currency;
}

export interface CostRecoveryGroup {
  costRecoveryGroup: FormGroup;
  consentTypeControl: FormControl;
  typeControl: FormControl;
  currencyGroup: FormControl;
  amountControl: FormControl;
}

export enum TipConsents {
  AcceptAllWithoutCost = 'yes',
  AcceptAllWithCost = 'costRecovery',
  NoConsentToAll = 'no',
  ManualConfiguration = 'manual'
}

export enum TipCostRecoveryType {
  Fixed = 'fixedAmount',
  Percentage = 'percentage'
}

export enum TipCostRecoveryTypeLabel {
  Fixed = 'amount',
  Percentage = 'percentage'
}
