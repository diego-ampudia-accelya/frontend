import { DescriptionViewConfig } from './models/description-view-config.model';
import { Permissions } from '~app/shared/constants/permissions';

export const defaultPermissions = Object.freeze({
  read: Permissions.notProtected,
  create: Permissions.notProtected,
  update: Permissions.notProtected,
  delete: Permissions.notProtected
});

export const baseIssueReasonConfig: DescriptionViewConfig = Object.freeze({
  entityName: 'editableDescription.issueReason',
  title: 'editableDescription.admAcmIssueReasons',
  apiService: null,
  maxItems: 10,
  createButtonLabel: 'Reason',
  label: Object.freeze({
    header: 'editableDescription.reasonTitle',
    maxLength: 45
  }),
  description: Object.freeze({
    header: 'editableDescription.reasonDescription',
    isRequired: true,
    lineLimit: 45,
    rowsLimit: 5
  }),
  permissions: defaultPermissions
});

export const baseDisputeReasonsConfig: DescriptionViewConfig = Object.freeze({
  entityName: 'editableDescription.disputeReason',
  title: 'editableDescription.admAcmDisputeReasons',
  apiService: null,
  maxItems: 10,
  createButtonLabel: 'Reason',
  label: Object.freeze({
    header: 'editableDescription.reasonTitle',
    maxLength: 45
  }),
  description: Object.freeze({
    header: 'editableDescription.reasonDescription',
    isRequired: true,
    lineLimit: 45,
    rowsLimit: 5,
    canValidate: false
  }),
  permissions: defaultPermissions
});

export const baseRejectionReasonsConfig: DescriptionViewConfig = Object.freeze({
  entityName: 'editableDescription.rejectionReason',
  title: 'editableDescription.raRejectionReasons',
  apiService: null,
  maxItems: 10,
  createButtonLabel: 'Reason',
  label: Object.freeze({
    header: 'editableDescription.reasonTitle',
    maxLength: 50
  }),
  description: Object.freeze({
    header: 'editableDescription.reasonDescription',
    isRequired: true,
    maxLength: 70
  }),
  permissions: defaultPermissions
});
