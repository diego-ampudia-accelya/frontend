import {
  ChangeDetectorRef,
  Component,
  ErrorHandler,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren
} from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { catchError, delay, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { EditableDescriptionDataSource } from '../editable-description-data-source';
import { EditableDescriptionComponent } from '../editable-description/editable-description.component';
import { EditableDescription, EditableDescriptionVM } from '../models/editable-description.model';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { NotificationService } from '~app/shared/services/notification.service';
import { isNew } from '~app/shared/utils/api-utils';

@Component({
  selector: 'bspl-editable-description-list',
  templateUrl: './editable-description-list.component.html',
  styleUrls: ['./editable-description-list.component.scss']
})
export class EditableDescriptionListComponent implements OnInit, OnDestroy, OnChanges {
  @Input() labelRegexValidation: string | RegExp;
  @Input() descriptionRegexValidation: string | RegExp;
  @Input() labelAllCaps: boolean;
  @Input() labelName = 'Label';
  @Input() labelMaxLenght: number;

  @Input() descriptionName = 'Description';
  @Input() descriptionMaxLength: number;
  @Input() descriptionLineLimit: number;
  @Input() descriptionRowsLimit: number;
  @Input() mandatoryDescription = true;

  @Input() entityName: string;
  @Input() dataSource: EditableDescriptionDataSource;
  @Input() parentId: number;

  @Input() canEdit: boolean;
  @Input() canDelete: boolean;
  @Input() canValidateDescription: boolean;

  @Output() editModeChange = new EventEmitter<boolean>();
  @Output() itemChange = new EventEmitter<EditableDescriptionVM>();

  @ViewChildren(EditableDescriptionComponent) editableComponents: QueryList<EditableDescriptionComponent>;

  public descriptionItems: EditableDescription[];
  public hasItemInEditMode = false;

  // This subject replays the last published parentId on subscribe.
  protected parentId$ = new ReplaySubject<number>(1);
  protected destroy$ = new Subject<any>();

  public constructor(
    protected cd: ChangeDetectorRef,
    protected notification: NotificationService,
    protected dialogService: DialogService,
    private translationService: L10nTranslationService,
    private globalErrorHandlerService: ErrorHandler
  ) {}

  public ngOnInit() {
    this.dataSource.items$
      .pipe(
        tap(items => (this.descriptionItems = items)),
        takeUntil(this.destroy$)
      )
      .subscribe();

    this.parentId$
      .pipe(
        // Avoid ExpressionChangedAfterItHasBeenCheckedError
        delay(0),
        switchMap(parentId => this.loadItems(parentId)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.parentId) {
      this.parentId$.next(this.parentId);
    }
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  public addItem() {
    this.descriptionItems.unshift(this.dataSource.createEmpty());

    // Wait for the newly added EditableDescriptionComponent to be available.
    this.cd.detectChanges();
    this.editableComponents.first.actionEdit();
  }

  protected onActionRequest(action: string, data: EditableDescription) {
    // e.g. `actionDelete`
    const methodName = `action${capitalize(action)}`;
    this[methodName](data);
  }

  protected actionChange(model: EditableDescription) {
    this.dataSource
      .save(model)
      .pipe(
        // ToDo: translation
        tap(() =>
          this.notification.showSuccess(
            this.translationService.translate('editableDescription.successfullyActed', {
              item: model.label,
              action: 'saved'
            })
          )
        ),
        catchError(error => {
          if (isNew(model)) {
            this.descriptionItems.shift();
          }
          this.globalErrorHandlerService.handleError(error);

          return of(null);
        })
      )
      .subscribe();
  }

  protected actionDelete(model: EditableDescription) {
    // Handle `cancel` action of newly created reasons
    if (isNew(model)) {
      this.descriptionItems.shift();

      return;
    }

    this.confirmDelete(model)
      .pipe(
        filter(isConfirmed => isConfirmed),
        switchMap(() => this.dataSource.delete(model)),
        tap(() =>
          this.notification.showSuccess(
            this.translationService.translate('editableDescription.successfullyActed', {
              item: model.label,
              action: 'deleted'
            })
          )
        )
      )
      .subscribe();
  }

  protected onEditModeChange(inEditMode: boolean) {
    this.hasItemInEditMode = inEditMode;
    this.editModeChange.emit(this.hasItemInEditMode);
  }

  protected confirmDelete(data: EditableDescription): Observable<boolean> {
    const config: DialogConfig = {
      data: {
        title: `${this.translationService.translate('common.delete')} ${this.translationService.translate(
          this.entityName
        )}`,
        hasCancelButton: true,
        footerButtonsType: FooterButton.Delete,
        details: `${data.label || ''}: ${data.description || ''}`,
        entityName: this.entityName
      }
    };

    return this.dialogService.open(ConfirmationDialogComponent, config).pipe(
      map(action => action.clickedBtn === FooterButton.Delete),
      tap(() => this.dialogService.close())
    );
  }

  protected loadItems(parentId: number): Observable<any> {
    return this.dataSource.get(parentId).pipe(
      tap(
        items => (this.descriptionItems = items),
        () => (this.descriptionItems = [])
      )
    );
  }
}
