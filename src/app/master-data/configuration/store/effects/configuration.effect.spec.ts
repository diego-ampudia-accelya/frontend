import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of, ReplaySubject } from 'rxjs';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';

import { ConfigurationEffects } from './configuration.effect';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

describe('ConfigurationEffects', () => {
  let actions: ReplaySubject<any>;
  let effects: ConfigurationEffects;

  const initialState = {
    configuration: {
      settings: {
        scope: 'bsp'
      }
    }
  };
  const appConfigServiceMock = createSpyObject(AppConfigurationService);
  const changesDialogServiceMock = createSpyObject(ChangesDialogService);
  const translationServiceMock = createSpyObject(L10nTranslationService);
  const notificationServiceMock = createSpyObject(NotificationService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ConfigurationEffects,
        provideMockStore({ initialState }),
        provideMockActions(() => actions),
        { provide: AppConfigurationService, useValue: appConfigServiceMock },
        { provide: L10nTranslationService, useValue: translationServiceMock },
        { provide: ChangesDialogService, useValue: changesDialogServiceMock },
        { provide: NotificationService, useValue: notificationServiceMock }
      ]
    });
    effects = TestBed.inject(ConfigurationEffects);
  });

  it('should be created', async () => {
    expect(effects).toBeTruthy();
  });

  it('should call confirmApplyChanges method on openApplyChanges effect ', fakeAsync(() => {
    actions = new ReplaySubject(1);
    changesDialogServiceMock.confirmApplyChanges.and.returnValue(of({}));

    actions.next({
      type: '[Setting Configuration] Open Apply Changes',
      payload: {}
    });

    effects.openApplyChanges$.subscribe(() => {});
    expect(changesDialogServiceMock.confirmApplyChanges).toHaveBeenCalled();
  }));

  it('should call notifySaveSuccess method on applySuccess effect ', fakeAsync(() => {
    actions = new ReplaySubject(1);
    notificationServiceMock.showSuccess.and.returnValue(of({}));

    actions.next({
      type: '[Setting Configuration] Apply Changes Success',
      payload: {
        updatedParameters: {
          service: 'acdm-management'
        }
      }
    });

    effects.applySuccess$.subscribe(() => {});
    expect(notificationServiceMock.showSuccess).toHaveBeenCalled();
  }));
});
