import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of, throwError } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { IpAddressConfigurationService } from '../../api/ip-address-configuration.service';
import { IpAddressChangesDialogService } from '../../ip-address-settings/changes-dialog/services/changes-dialog.service';
import { IpAddressRemoveDialogService } from '../../ip-address-settings/changes-dialog/services/remove-dialog.service';
import { IpAddressConfigurationActions } from '../actions';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';

const CONFIG_NOT_EXIST_ERROR = 404;

@Injectable()
export class IpAddressConfigurationEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(IpAddressConfigurationActions.load),
      switchMap(({ url }) =>
        this.ipAddressConfigurationService.get(url).pipe(
          map(ipAddressConfig =>
            IpAddressConfigurationActions.loadSuccess({
              ipAddressConfiguration: ipAddressConfig
            })
          ),
          catchError(err =>
            err.status === CONFIG_NOT_EXIST_ERROR ? of(IpAddressConfigurationActions.loadNotExist()) : throwError(err)
          ),
          rethrowError(error => IpAddressConfigurationActions.loadError({ error }))
        )
      )
    )
  );

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(IpAddressConfigurationActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(() => EMPTY),
      rethrowError(error => IpAddressConfigurationActions.applyChangesError({ error }))
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(IpAddressConfigurationActions.applyChangesSuccess),
        tap(() => this.notificationService.showSuccess('menu.masterData.configuration.ipSettings.saveSuccess'))
      ),
    { dispatch: false }
  );

  public openDelete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(IpAddressConfigurationActions.openDelete),
      switchMap(() => this.removeDialogService.open()),
      switchMap(() => EMPTY),
      rethrowError(error => IpAddressConfigurationActions.deleteError({ error }))
    )
  );

  public deleteSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(IpAddressConfigurationActions.deleteSuccess),
        tap(() => this.notificationService.showSuccess('menu.masterData.configuration.ipSettings.deleteSuccess'))
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private ipAddressConfigurationService: IpAddressConfigurationService,
    private notificationService: NotificationService,
    private changesDialogService: IpAddressChangesDialogService,
    private removeDialogService: IpAddressRemoveDialogService
  ) {}
}
