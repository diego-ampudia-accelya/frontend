import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, forkJoin } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { DynamicConfigurationService } from '../../api/dynamic-configuration.service';
import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { ParameterType } from '../../enum/parameter-type.enum';
import { ConfigurationScope } from '../../models/configuration-scope.model';
import { ParameterDescriptor } from '../../models/parameter-descriptor.model';
import { SettingConfigurationActions } from '../actions';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class ConfigurationEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SettingConfigurationActions.load),
      switchMap(scope => this.load(scope.scope))
    )
  );

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SettingConfigurationActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(() => EMPTY),
      rethrowError(() => SettingConfigurationActions.applyChangesError())
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SettingConfigurationActions.applyChangesSuccess),
        tap(() => this.notificationService.showSuccess('LIST.MASTER_DATA.configuration.saveSuccess'))
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dynamicConfigurationService: DynamicConfigurationService,
    private notificationService: NotificationService,
    private changesDialogService: ChangesDialogService,
    private translationService: L10nTranslationService
  ) {}

  private load(scope: ConfigurationScope) {
    return forkJoin([
      this.dynamicConfigurationService.getConfigurationDescriptors(
        scope.service,
        scope.scopeType,
        scope.scopeId,
        scope.scopeConfigName
      ),
      this.dynamicConfigurationService.getConfigurationValues(
        scope.service,
        scope.scopeType,
        scope.scopeId,
        scope.scopeConfigName
      )
    ]).pipe(
      map(([configurationDescriptor, configurationParameters]) => {
        configurationDescriptor = {
          ...configurationDescriptor,
          categories: configurationDescriptor.categories.map(category => ({
            ...category,
            parameters: this.updateCategoryParametersPossibleValues(category.parameters)
          }))
        };

        return SettingConfigurationActions.loadSuccess({
          configurationParameters,
          configurationDescriptor
        });
      }),
      rethrowError(() => SettingConfigurationActions.loadError())
    );
  }

  private updateCategoryParametersPossibleValues(parameters: ParameterDescriptor[]): ParameterDescriptor[] {
    const none = this.translationService.translate('LIST.MASTER_DATA.airline.basicSettings.none');

    return parameters.map(param => {
      if (param.type === ParameterType.Enum) {
        return {
          ...param,
          possibleValues: param.possibleValues.map(v => ({
            label: v.displayName || none,
            value: v.value,
            displayName: v.displayName || none
          }))
        };
      }

      return param;
    });
  }
}
