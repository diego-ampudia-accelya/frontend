import { createReducer, on } from '@ngrx/store';
import { get, isEqual } from 'lodash';

import { ConfigurationScope } from '../../models/configuration-scope.model';
import { SettingCategory } from '../../setting-list/models/setting-category.model';
import { AdmPoliciesActions } from '~app/master-data/airline/actions';
import { AdmPolicyConfiguration } from '~app/master-data/airline/configuration/models/adm-policy.model';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';

export interface AdmPolicyConfigurationState {
  scope: ConfigurationScope;
  originalValue: AdmPolicyConfiguration;
  value: AdmPolicyConfiguration;
  isChanged: boolean;
  isLoading: boolean;
  settingConfigurations: SettingCategory[];
  isInvalid: boolean;
  errors: { error: ResponseErrorBE };
}

const initialState: AdmPolicyConfigurationState = {
  scope: null,
  originalValue: null,
  value: null,
  isChanged: false,
  isLoading: false,
  settingConfigurations: [],
  isInvalid: false,
  errors: null
};

export const reducer = createReducer(
  initialState,
  on(AdmPoliciesActions.load, (state, { scope, settingConfigurations }) => ({
    ...state,
    isLoading: true,
    scope,
    settingConfigurations
  })),
  on(AdmPoliciesActions.loadSuccess, (state, { value }) => ({
    ...state,
    isLoading: false,
    value,
    originalValue: value,
    settingConfigurations: setSettingValues(state, value)
  })),
  on(AdmPoliciesActions.loadError, state => ({
    ...state,
    originalValue: null,
    value: null,
    isChanged: false,
    isLoading: false,
    settingConfigurations: [],
    isInvalid: false,
    errors: null
  })),
  on(AdmPoliciesActions.modify, (state, { value }) => ({
    ...state,
    value: changeValue(state.value, value),
    isChanged: !isEqual(state.originalValue, { ...state.value, ...value }),
    settingConfigurations: setSettingValues(state, value),
    isInvalid: false,
    errors: null
  })),
  on(AdmPoliciesActions.invalid, state => ({
    ...state,
    isInvalid: true
  })),
  on(AdmPoliciesActions.discard, state => ({
    ...state,
    isChanged: false,
    value: state.originalValue,
    settingConfigurations: setSettingValues(state, state.originalValue)
  })),
  on(AdmPoliciesActions.applyChangesSuccess, (state, { value }) => ({
    ...state,
    value,
    originalValue: value,
    settingConfigurations: setSettingValues(state, value),
    isChanged: false,
    isInvalid: false,
    errors: null
  })),
  on(AdmPoliciesActions.applyChangesError, (state, { error }) => ({
    ...state,
    isChanged: !error,
    isInvalid: !!error,
    errors: error
  }))
);

export const setSettingValues = ({ settingConfigurations }: AdmPolicyConfigurationState, value: any) =>
  settingConfigurations.map(category => ({
    ...category,
    settings: category.settings.map(setting => {
      const settingValue = get(value, setting.field);

      let newSetting = setting;
      if (settingValue != null) {
        newSetting = { ...setting, value: settingValue };
      }

      return newSetting;
    })
  }));

export const getAdmPoliciesChanges = ({ value, originalValue }: AdmPolicyConfigurationState) => {
  const modifications = {};

  if (value && originalValue) {
    Object.keys(value)
      .filter(key => value[key] !== originalValue[key])
      .forEach(modifiedKey => {
        modifications[modifiedKey] = value[modifiedKey];
      });
  }

  return modifications;
};

export const changeValue = (currentValue: AdmPolicyConfiguration, newValue: AdmPolicyConfiguration) => ({
  ...currentValue,
  ...newValue
});
