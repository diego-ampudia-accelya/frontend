import { createReducer, on } from '@ngrx/store';
import { isEqual } from 'lodash';

import { ConfigurationDescriptor } from '../../models/configuration-descriptor.model';
import { ConfigurationScope } from '../../models/configuration-scope.model';
import { Configuration } from '../../models/configuration.model';
import { Parameter } from '../../models/parameters.model';
import { SettingConfigurationActions } from '../actions';

export interface SettingConfigurationState {
  scope: ConfigurationScope;
  originalConfigurationParameters: Configuration;
  configurationParameters: Configuration;
  isChanged: boolean;
  isInvalid?: boolean;
  isLoading: boolean;
  configurationDescriptor: ConfigurationDescriptor;
}

const initialState: SettingConfigurationState = {
  scope: null,
  originalConfigurationParameters: null,
  configurationParameters: null,
  isChanged: false,
  isInvalid: false,
  isLoading: false,
  configurationDescriptor: null
};

export const reducer = createReducer(
  initialState,
  on(SettingConfigurationActions.modify, (state, { parameter, isFormValid }) => ({
    ...state,
    configurationParameters: changeValue(state.configurationParameters, parameter),
    isChanged: !isEqual(state.originalConfigurationParameters, changeValue(state.configurationParameters, parameter)),
    isInvalid: !isFormValid
  })),
  on(SettingConfigurationActions.applyChangesSuccess, (state, { updatedParameters }) => ({
    ...state,
    originalConfigurationParameters: updatedParameters,
    configurationParameters: updatedParameters,
    isChanged: false,
    isInvalid: false
  })),
  on(SettingConfigurationActions.discard, state => ({
    ...state,
    isChanged: false,
    isInvalid: false,
    configurationParameters: state.originalConfigurationParameters
  })),
  on(SettingConfigurationActions.load, (state, { scope }) => ({
    ...state,
    isLoading: true,
    scope,
    originalConfigurationParameters: null,
    configurationParameters: null,
    configurationDescriptor: null
  })),
  on(SettingConfigurationActions.loadSuccess, (state, { configurationParameters, configurationDescriptor }) => ({
    ...state,
    isLoading: false,
    configurationParameters,
    originalConfigurationParameters: configurationParameters,
    configurationDescriptor
  })),
  on(SettingConfigurationActions.loadError, state => ({
    ...state,
    originalConfigurationParameters: null,
    configurationParameters: null,
    isChanged: false,
    isLoading: false,
    configurationDescriptor: null
  })),
  on(SettingConfigurationActions.applyChangesError, state => ({
    ...state,
    isLoading: false
  }))
);

export const getChanges = ({
  configurationParameters,
  originalConfigurationParameters
}: SettingConfigurationState): Parameter[] => {
  let changes = [];

  if (configurationParameters && originalConfigurationParameters) {
    changes = configurationParameters.parameters.filter((item, index) => {
      const originalParameter = originalConfigurationParameters.parameters[index];

      return item.id === originalParameter.id && item.value !== originalParameter.value;
    });
  }

  return changes;
};

export const changeValue = (currentConfigurationParameter: Configuration, newValue: Parameter): Configuration => {
  const configurationParameters = currentConfigurationParameter.parameters.map(param =>
    param.id === newValue.id ? newValue : param
  );

  return {
    ...currentConfigurationParameter,
    parameters: configurationParameters
  };
};
