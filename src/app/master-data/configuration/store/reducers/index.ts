import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '~app/reducers';
import { ConfigurationDescriptor } from '../../models/configuration-descriptor.model';
import { Configuration } from '../../models/configuration.model';
import { FromTipGlobalAirline } from '../../tip/tip-global-airline/store/reducers';
import { getTipGlobalAirlineChanges } from '../../tip/tip-global-airline/store/reducers/tip-global-airline.reducer';
import * as fromAdmPolicies from './adm-policies.reducer';
import * as fromConfiguration from './configuration.reducer';
import * as fromConsentSettings from './consent-settings.reducer';
import * as fromIpAddressConfiguration from './ip-address-configuration.reducer';

export const configurationFeatureKey = 'configuration';
export interface ConfigurationState {
  settings: fromConfiguration.SettingConfigurationState;
  admPolicies: fromAdmPolicies.AdmPolicyConfigurationState;
  tipGlobalAirlines: FromTipGlobalAirline.TipGlobalAirlineState;
  ipAddressSettings: fromIpAddressConfiguration.IpAddressConfigurationState;
  consentSettings: fromConsentSettings.ConsentSettingsState;
}

export interface State extends AppState {
  configuration: ConfigurationState;
}

export function reducers(state: ConfigurationState, action: Action) {
  return combineReducers({
    settings: fromConfiguration.reducer,
    admPolicies: fromAdmPolicies.reducer,
    tipGlobalAirlines: FromTipGlobalAirline.reducer,
    ipAddressSettings: fromIpAddressConfiguration.reducer,
    consentSettings: fromConsentSettings.reducer
  })(state, action);
}

export const getConfigurationState = createFeatureSelector<State, ConfigurationState>('configuration');

// Selectors from settings
export const getSettingsState = createSelector(getConfigurationState, state => state && state.settings);

export const hasChanges = createSelector(getSettingsState, state => state && state.isChanged);

export const canApplyChanges = createSelector(getSettingsState, state => state && !state.isInvalid && state.isChanged);

export const getModifications = createSelector(getSettingsState, fromConfiguration.getChanges);

export const getConfigurationDescriptor = createSelector(
  getSettingsState,
  state => state.configurationDescriptor || ({} as ConfigurationDescriptor)
);

export const getConfigurationParameters = createSelector(
  getSettingsState,
  state => state.configurationParameters || ({} as Configuration)
);

// Selectors from AdmPolicies
export const getPolicyState = createSelector(getConfigurationState, state => state && state.admPolicies);

export const getPolicyConfigurations = createSelector(getPolicyState, state => state.settingConfigurations);

export const hasPolicyValidChanges = createSelector(getPolicyState, state => state.isChanged && !state.isInvalid);

export const getPolicyChanges = createSelector(getPolicyState, fromAdmPolicies.getAdmPoliciesChanges);

// Selectors from TipGlobalAirlines
export const getTipGlobalAirlineState = createSelector(
  getConfigurationState,
  state => state && state.tipGlobalAirlines
);
export const getTipGlobalAirlineIsLoading = createSelector(getTipGlobalAirlineState, state => state.isLoading);
export const getTipGlobalAirlineIsReadonlyMode = createSelector(
  getTipGlobalAirlineState,
  state => state.isReadOnlyMode
);
export const getTipGlobalAirlineValue = createSelector(getTipGlobalAirlineState, state => state.value);

export const hasChangesTipsGlobalAirline = createSelector(
  getTipGlobalAirlineState,
  state => state.isChanged && !state.isInvalid
);

export const getChangesTipGlobalAirline = createSelector(getTipGlobalAirlineState, getTipGlobalAirlineChanges);

export const getHasInvalidStatusTipGlobalAirline = createSelector(getTipGlobalAirlineState, state => state.isInvalid);

// Selectors from IP Address Settings
export const getIpSettingsState = createSelector(getConfigurationState, state => state && state.ipAddressSettings);

export const getIpAddressConfiguration = createSelector(
  getIpSettingsState,
  state => state && state.ipAddressConfiguration
);

export const getIpSettingsUrl = createSelector(getIpSettingsState, state => state && state.url);

export const getIpSettingsIsLoading = createSelector(getIpSettingsState, state => state && state.isLoading);

export const getIpSettingsIsChanged = createSelector(getIpSettingsState, state => state && state.isChanged);

export const getIpSettingsIsInvalid = createSelector(getIpSettingsState, state => state && state.isInvalid);

export const getIpSettingsErrors = createSelector(getIpSettingsState, state => state && state.errors);

export const getIpSettingsIsExisting = createSelector(getIpSettingsState, state => state && state.isExisting);

export const canIpSettingsApplyChanges = createSelector(
  getIpSettingsState,
  state => state && !state.isInvalid && state.isChanged
);

export const getIpSettingsModifications = createSelector(getIpSettingsState, fromIpAddressConfiguration.getChanges);

// Selectors from Consent Settings
export const getConsentSettingsState = createSelector(getConfigurationState, state => state && state.consentSettings);

export const getCanApplyChanges = createSelector(getConsentSettingsState, state => state.hasChanges && state.isValid);
