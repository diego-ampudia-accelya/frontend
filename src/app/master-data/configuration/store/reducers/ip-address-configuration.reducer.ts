import { createReducer, on } from '@ngrx/store';
import { isEqual } from 'lodash';

import { IpAddressChange, IpAddressConfig, IpAddressConfigBE } from '../../models/ip-address.model';
import { IpType } from '../../models/ip-type.model';
import { IpAddressConfigurationActions } from '../actions';
import { ResponseErrorBE } from '~app/shared/models';

export interface IpAddressConfigurationState {
  url: string;
  originalIpAddressConfiguration: IpAddressConfig;
  ipAddressConfiguration: IpAddressConfig;
  isChanged: boolean;
  isInvalid?: boolean;
  isLoading: boolean;
  isExisting: boolean;
  errors: { error: ResponseErrorBE };
}

const DEFAULT_FORM_VALUE: IpAddressConfig = {
  ipType: null,
  ips: [null, null, null],
  range: {
    beginIp: null,
    endIp: null
  }
};

const initialState: IpAddressConfigurationState = {
  url: null,
  originalIpAddressConfiguration: DEFAULT_FORM_VALUE,
  ipAddressConfiguration: DEFAULT_FORM_VALUE,
  isChanged: false,
  isInvalid: false,
  isLoading: false,
  isExisting: false,
  errors: null
};

export const reducer = createReducer(
  initialState,
  on(IpAddressConfigurationActions.load, (state, { url }) => ({
    ...state,
    url,
    ipAddressConfiguration: DEFAULT_FORM_VALUE,
    originalIpAddressConfiguration: DEFAULT_FORM_VALUE,
    isLoading: true
  })),
  on(IpAddressConfigurationActions.loadSuccess, (state, { ipAddressConfiguration }) => ({
    ...state,
    ipAddressConfiguration: transformToFEConfig(ipAddressConfiguration),
    originalIpAddressConfiguration: transformToFEConfig(ipAddressConfiguration),
    isLoading: false,
    isExisting: true,
    errors: null
  })),
  on(IpAddressConfigurationActions.loadError, (state, { error }) => ({
    ...state,
    isChanged: false,
    isLoading: false,
    errors: error
  })),
  on(IpAddressConfigurationActions.loadNotExist, state => ({
    ...state,
    isChanged: false,
    isLoading: false,
    isExisting: false
  })),
  on(IpAddressConfigurationActions.modify, (state, { config, isFormValid }) => ({
    ...state,
    ipAddressConfiguration: config,
    isChanged: !isEqual(state.originalIpAddressConfiguration, config),
    isInvalid: !isFormValid
  })),
  on(IpAddressConfigurationActions.applyChangesSuccess, (state, { ipAddressConfiguration }) => ({
    ...state,
    ipAddressConfiguration: transformToFEConfig(ipAddressConfiguration),
    originalIpAddressConfiguration: transformToFEConfig(ipAddressConfiguration),
    isChanged: false,
    isInvalid: false,
    isExisting: true,
    errors: null
  })),
  on(IpAddressConfigurationActions.discard, state => ({
    ...state,
    isChanged: false,
    isInvalid: false,
    ipAddressConfiguration: state.originalIpAddressConfiguration,
    errors: null
  })),
  on(
    IpAddressConfigurationActions.applyChangesError,
    IpAddressConfigurationActions.deleteError,
    (state, { error }) => ({
      ...state,
      isLoading: false,
      errors: error
    })
  ),
  on(IpAddressConfigurationActions.deleteSuccess, state => ({
    ...state,
    ipAddressConfiguration: DEFAULT_FORM_VALUE,
    originalIpAddressConfiguration: DEFAULT_FORM_VALUE,
    isExisting: false,
    isLoading: false
  }))
);

const transformToFEConfig = (beConfig: IpAddressConfigBE): IpAddressConfig => {
  const haveIpsLength = beConfig?.ips?.length;
  if (!beConfig || (!haveIpsLength && !beConfig.range)) {
    return DEFAULT_FORM_VALUE;
  }

  return {
    ipType: haveIpsLength ? IpType.Permanent : IpType.Range,
    ips: haveIpsLength ? getIps(beConfig.ips) : DEFAULT_FORM_VALUE.ips,
    range: beConfig.range ? beConfig.range : DEFAULT_FORM_VALUE.range
  };
};

const getIps = (beIps: string[]): string[] =>
  DEFAULT_FORM_VALUE.ips.map((defaultIp: null, index: number) => (beIps[index] ? beIps[index] : defaultIp));

export const getChanges = ({
  ipAddressConfiguration,
  originalIpAddressConfiguration
}: IpAddressConfigurationState): IpAddressChange[] | null => {
  if (ipAddressConfiguration && originalIpAddressConfiguration && ipAddressConfiguration.ipType) {
    return [
      {
        label: `menu.masterData.configuration.ipSettings.options.${ipAddressConfiguration.ipType}`
      },
      ...getIpsChanges(ipAddressConfiguration, originalIpAddressConfiguration)
    ];
  }

  return null;
};

const getIpsChanges = (
  ipAddressConfiguration: IpAddressConfig,
  originalIpAddressConfiguration: IpAddressConfig
): IpAddressChange[] => {
  if (ipAddressConfiguration.ipType === IpType.Permanent) {
    return getPermanentIpsChanges(ipAddressConfiguration, originalIpAddressConfiguration);
  }

  if (ipAddressConfiguration.ipType === IpType.Range) {
    return getRangeIpsChanges(ipAddressConfiguration, originalIpAddressConfiguration);
  }

  return [];
};

const getPermanentIpsChanges = (
  ipAddressConfiguration: IpAddressConfig,
  originalIpAddressConfiguration: IpAddressConfig
) =>
  ipAddressConfiguration.ips
    .map((ip: string, index: number) => {
      const originalIp = originalIpAddressConfiguration.ips[index];
      if (ip === originalIp || (!ip && !originalIp)) {
        return null;
      }

      if (!ip && originalIp) {
        return {
          label: `menu.masterData.configuration.ipSettings.changesDialog.changes.labels.removedIpAddress`,
          index: index + 1
        };
      }

      return {
        label: 'menu.masterData.configuration.ipSettings.changesDialog.changes.labels.ipAddress',
        index: index + 1,
        value: ip
      };
    })
    .filter(Boolean);

const getRangeIpsChanges = (ipAddressConfiguration: IpAddressConfig, originalIpAddressConfiguration: IpAddressConfig) =>
  [
    ipAddressConfiguration.range.beginIp &&
    ipAddressConfiguration.range.beginIp !== originalIpAddressConfiguration.range.beginIp
      ? {
          label: 'menu.masterData.configuration.ipSettings.labels.from',
          value: ipAddressConfiguration.range.beginIp
        }
      : null,
    ipAddressConfiguration.range.endIp &&
    ipAddressConfiguration.range.endIp !== originalIpAddressConfiguration.range.endIp
      ? { label: 'menu.masterData.configuration.ipSettings.labels.to', value: ipAddressConfiguration.range.endIp }
      : null
  ].filter(Boolean);
