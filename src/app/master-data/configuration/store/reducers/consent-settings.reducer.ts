import { createReducer, on } from '@ngrx/store';
import { ConsentSettingsActions } from '../actions';

export interface ConsentSettingsState {
  hasChanges: boolean;
  isValid: boolean;
}

export const initialState: ConsentSettingsState = {
  hasChanges: false,
  isValid: true
};

export const reducer = createReducer(
  initialState,
  on(ConsentSettingsActions.setChanges, (state, { hasChanges }) => ({
    ...state,
    hasChanges
  })),
  on(ConsentSettingsActions.setFormValid, (state, { isValid }) => ({
    ...state,
    isValid
  }))
);
