import { createAction, props } from '@ngrx/store';

import { IpAddressConfig, IpAddressConfigBE } from '../../models/ip-address.model';

export const modify = createAction(
  '[IP Address Configuration] Modify',
  props<{ config: IpAddressConfig; isFormValid: boolean }>()
);

export const openApplyChanges = createAction('[IP Address Configuration] Open Apply Changes');

export const applyChangesSuccess = createAction(
  '[IP Address Configuration] Apply Changes Success',
  props<{ ipAddressConfiguration: IpAddressConfigBE }>()
);

export const applyChangesError = createAction('[IP Address Configuration] Apply Changes Error', props<{ error }>());

export const load = createAction('[IP Address Configuration] Load', props<{ url: string }>());

export const loadSuccess = createAction(
  '[IP Address Configuration] Load Success',
  props<{ ipAddressConfiguration: IpAddressConfigBE }>()
);

export const loadError = createAction('[IP Address Configuration] Load Error', props<{ error }>());

export const loadNotExist = createAction('[IP Address Configuration] Load Not Exist');

export const discard = createAction('[IP Address Configuration] Discard');

export const openDelete = createAction('[IP Address Configuration] Open Delete');

export const deleteSuccess = createAction('[IP Address Configuration] Delete Success');

export const deleteError = createAction('[IP Address Configuration] Delete Error', props<{ error }>());
