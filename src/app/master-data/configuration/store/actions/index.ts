import * as SettingConfigurationActions from './configuration.action';
import * as ConsentSettingsActions from './consent-settings.actions';
import * as IpAddressConfigurationActions from './ip-address-configuration.action';

export { SettingConfigurationActions, IpAddressConfigurationActions, ConsentSettingsActions };
