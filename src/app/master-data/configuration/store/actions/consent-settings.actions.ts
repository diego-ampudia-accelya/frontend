import { createAction, props } from '@ngrx/store';

export const openApplyChanges = createAction('[Consent Settings] Open Apply Changes');
export const discard = createAction('[Consent Settings] Discard');
export const applyChangesSuccess = createAction('[Consent Settings] Apply Changes Success');

export const setChanges = createAction(
  '[Consent Settings] Set Changes',
  props<{
    hasChanges: boolean;
  }>()
);

export const setFormValid = createAction(
  '[Consent Settings] Set Form Valid',
  props<{
    isValid: boolean;
  }>()
);
