import { createAction, props } from '@ngrx/store';

import { ConfigurationDescriptor } from '../../models/configuration-descriptor.model';
import { ConfigurationScope } from '../../models/configuration-scope.model';
import { Configuration } from '../../models/configuration.model';
import { Parameter } from '../../models/parameters.model';

export const modify = createAction(
  '[Setting Configuration] Modify',
  props<{ parameter: Parameter; isFormValid: boolean }>()
);

export const openApplyChanges = createAction('[Setting Configuration] Open Apply Changes');

export const applyChangesSuccess = createAction(
  '[Setting Configuration] Apply Changes Success',
  props<{ updatedParameters: Configuration }>()
);

export const applyChangesError = createAction('[Setting Configuration] Apply Changes Error');

export const load = createAction('[Setting Configuration] Load', props<{ scope: ConfigurationScope }>());

export const loadSuccess = createAction(
  '[Setting Configuration] Load Success',
  props<{ configurationParameters: Configuration; configurationDescriptor: ConfigurationDescriptor }>()
);

export const loadError = createAction('[Setting Configuration] Load Error');

export const discard = createAction('[Setting Configuration] Discard');
