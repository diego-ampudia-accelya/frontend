import { TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { AppState } from '~app/reducers';

describe('ChangesDialogConfigService', () => {
  let service: ChangesDialogConfigService;
  let translationStub: SpyObject<L10nTranslationService>;
  let mockStore: MockStore<AppState>;

  beforeEach(waitForAsync(() => {
    translationStub = createSpyObject(L10nTranslationService);
    translationStub.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      providers: [
        ChangesDialogConfigService,
        { provide: L10nTranslationService, useValue: translationStub },
        provideMockStore()
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ChangesDialogConfigService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });
});
