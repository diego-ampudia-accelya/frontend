export const translations = {
  applyChanges: {
    bsp: {
      title: 'LIST.MASTER_DATA.changesDialog.apply.bsp.title',
      details: 'LIST.MASTER_DATA.changesDialog.apply.bsp.message'
    },
    airline: {
      title: 'LIST.MASTER_DATA.changesDialog.apply.airline.title',
      details: 'LIST.MASTER_DATA.changesDialog.apply.airline.message'
    }
  },
  unsavedChanges: {
    bsp: {
      title: 'LIST.MASTER_DATA.changesDialog.unsaved.bsp.title',
      details: 'LIST.MASTER_DATA.changesDialog.unsaved.bsp.message'
    },
    airline: {
      title: 'LIST.MASTER_DATA.changesDialog.unsaved.airline.title',
      details: 'LIST.MASTER_DATA.changesDialog.unsaved.airline.message'
    }
  },
  enabled: 'LIST.MASTER_DATA.changesDialog.enabled',
  disabled: 'LIST.MASTER_DATA.changesDialog.disabled',
  refundTitle: 'menu.masterData.configuration.refundSettings.sectionTitle',
  ticketingAuthorityTitle: 'menu.masterData.configuration.taSettings.sectionTitle'
};
