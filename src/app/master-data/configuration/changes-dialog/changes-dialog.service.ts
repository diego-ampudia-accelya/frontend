import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { isEmpty, isEqual } from 'lodash';
import { combineLatest, defer, iif, Observable, of } from 'rxjs';
import { distinctUntilChanged, finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';

import { DynamicConfigurationService } from '../api/dynamic-configuration.service';
import { ConfigurationScope } from '../models/configuration-scope.model';
import { Parameter } from '../models/parameters.model';
import { SettingConfigurationActions } from '../store/actions';
import * as fromConfiguration from '../store/reducers';
import { SettingConfigurationState } from '../store/reducers/configuration.reducer';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';
import * as fromAirline from '~app/master-data/airline/reducers';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private store: Store<AppState>,
    private dynamicConfigurationService: DynamicConfigurationService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { [key: string]: { title: string; details: string } }): Observable<FooterButton> {
    return combineLatest([
      this.store.pipe(select(fromConfiguration.getSettingsState)),
      this.store.pipe(select(fromConfiguration.getModifications)),
      this.store.pipe(select(fromAirline.getSelectedAirline))
    ]).pipe(
      first(),
      switchMap(([settings, modifications, selectedAirline]) => {
        const airlineName = selectedAirline && selectedAirline.localName;
        const questionType = airlineName ? 'airline' : 'bsp';

        return iif(
          () => !isEmpty(modifications),
          defer(() => this.open(modifications, settings, question[questionType], airlineName)),
          of(FooterButton.Discard)
        );
      })
    );
  }

  private open(
    modifications: Parameter[],
    settings: SettingConfigurationState,
    question: { title: string; details: string },
    airlineName?: string
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, settings, question, airlineName);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(dialogConfig, settings.scope);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(SettingConfigurationActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(dialogConfig: ChangesDialogConfig, scope: ConfigurationScope): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.store.pipe(
        select(fromConfiguration.getSettingsState),
        distinctUntilChanged((prev, curr) => !isEqual(prev, curr)),
        switchMap(settings =>
          this.dynamicConfigurationService.save(
            scope.service,
            scope.scopeType,
            scope.scopeId,
            settings.configurationParameters,
            scope.scopeConfigName
          )
        ),
        tap(updatedParameters =>
          this.store.dispatch(SettingConfigurationActions.applyChangesSuccess({ updatedParameters }))
        ),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
