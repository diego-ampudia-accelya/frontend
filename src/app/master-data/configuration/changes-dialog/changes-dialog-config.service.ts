import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { isBoolean } from 'lodash';

import { ParameterType } from '../enum/parameter-type.enum';
import { ConfigurationDescriptor } from '../models/configuration-descriptor.model';
import { ConfigurationScope } from '../models/configuration-scope.model';
import { Parameter } from '../models/parameters.model';
import { SettingConfigurationState } from '../store/reducers/configuration.reducer';

import { translations } from './translations';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable({ providedIn: 'root' })
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: any,
    settings: SettingConfigurationState,
    question: { title: string; details: string },
    airlineName: string
  ): ChangesDialogConfig {
    const message = this.translation.translate(
      question.details,
      this.getTranslationValues(settings.scope, airlineName)
    );

    return {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ],
        mainButtonType: FooterButton.Apply
      },
      changes: this.formatChanges(modifications, settings),
      hasInvalidChanges: settings.isInvalid,
      message
    };
  }

  private formatChanges(modifications: Parameter[], settings: SettingConfigurationState): ChangeModel[] {
    const translateStatus = (status: boolean) =>
      this.translation.translate(status ? translations.enabled : translations.disabled);

    modifications = modifications || [];

    return modifications.map(item => {
      const modifiedSetting = this.getItemDescriptor(item, settings.configurationDescriptor);

      if (isBoolean(item.value)) {
        modifiedSetting.value = translateStatus(item.value);
        modifiedSetting.originalValue = item.value;
      }

      return modifiedSetting;
    });
  }

  private getTranslationValues(scope: ConfigurationScope, airlineName: string): { bspName: string; name: string } {
    return {
      bspName: scope.name,
      name: airlineName
    };
  }

  private getItemDescriptor(modifiedParameter: Parameter, configuration: ConfigurationDescriptor): ChangeModel {
    let modifiedSetting: ChangeModel = null;

    configuration.categories.forEach(category => {
      const parameterDescriptor = category.parameters.find(param => param.id === modifiedParameter.descriptorId);
      let value;

      if (parameterDescriptor) {
        if (modifiedParameter.type === ParameterType.Enum) {
          value = parameterDescriptor.possibleValues.find(option => option.value === modifiedParameter.value);
        }

        modifiedSetting = {
          name: parameterDescriptor.displayName,
          value: (value && value.displayName) || modifiedParameter.value,
          group: category.displayName || configuration.displayName
        };
      }
    });

    return modifiedSetting;
  }
}
