import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of, throwError } from 'rxjs';

import { DynamicConfigurationService } from '../api/dynamic-configuration.service';
import { ParameterType } from '../enum/parameter-type.enum';
import { SettingConfigurationActions } from '../store/actions';
import * as fromConfiguration from '../store/reducers';
import { SettingConfigurationState } from '../store/reducers/configuration.reducer';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ChangesDialogService } from './changes-dialog.service';
import { translations } from './translations';
import { DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';
import { Airline } from '~app/master-data/models';
import { getSelectedAirline } from '~app/master-data/airline/reducers';

describe('ChangesDialogService', () => {
  let service: ChangesDialogService;
  let dialogServiceStub: SpyObject<DialogService>;
  let dataService: SpyObject<DynamicConfigurationService>;
  let configService: SpyObject<ChangesDialogConfigService>;
  let mockStore: MockStore<AppState>;

  beforeEach(waitForAsync(() => {
    configService = createSpyObject(ChangesDialogConfigService);
    dialogServiceStub = createSpyObject(DialogService);
    dataService = createSpyObject(DynamicConfigurationService);
    dataService.save.and.returnValue(of({}));

    TestBed.configureTestingModule({
      providers: [
        ChangesDialogService,
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: ChangesDialogConfigService, useValue: configService },
        { provide: DynamicConfigurationService, useValue: dataService },
        provideMockStore()
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ChangesDialogService);
    mockStore = TestBed.inject<any>(Store);
    mockStore.overrideSelector(fromConfiguration.getSettingsState, {
      scope: { scopeType: 'bsp' }
    } as SettingConfigurationState);
    mockStore.overrideSelector(fromConfiguration.getModifications, []);
    mockStore.overrideSelector(getSelectedAirline, {} as Airline);

    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('confirm', () => {
    it('should return `Discard` when there are no changes', fakeAsync(() => {
      mockStore.overrideSelector(fromConfiguration.getModifications, []);

      let dialogResult: FooterButton;
      service.confirm(translations.applyChanges).subscribe(result => (dialogResult = result));
      tick();

      expect(dialogResult).toBe(FooterButton.Discard);
    }));

    it('should open confirmation dialog when there are changes', fakeAsync(() => {
      mockStore.overrideSelector(fromConfiguration.getModifications, [
        {
          id: 0,
          descriptorId: 0,
          type: ParameterType.Boolean,
          name: 'assignTaAutomaticallyForNewAgents',
          value: 'false',
          readonly: false
        }
      ]);
      dialogServiceStub.open.and.returnValue(of(FooterButton.Cancel));

      service.confirm(translations.applyChanges).subscribe();
      tick();

      expect(dialogServiceStub.open).toHaveBeenCalledTimes(1);
    }));

    it('should dispatch a Discard action', fakeAsync(() => {
      mockStore.overrideSelector(fromConfiguration.getModifications, [
        {
          id: 0,
          descriptorId: 0,
          type: ParameterType.Boolean,
          name: 'assignTaAutomaticallyForNewAgents',
          value: 'false',
          readonly: false
        }
      ]);
      dialogServiceStub.open.and.returnValue(of({ clickedBtn: FooterButton.Discard }));

      let dialogResult: FooterButton;
      service.confirm(translations.applyChanges).subscribe(result => (dialogResult = result));
      tick();

      expect(dialogResult).toBe(FooterButton.Discard);
      expect(mockStore.dispatch).toHaveBeenCalledWith(SettingConfigurationActions.discard());
    }));

    it('should dispatch an applySuccess action when user has chosen to apply the changes', fakeAsync(() => {
      mockStore.overrideSelector(fromConfiguration.getModifications, [
        {
          id: 0,
          descriptorId: 0,
          type: ParameterType.Boolean,
          name: 'assignTaAutomaticallyForNewAgents',
          value: 'false',
          readonly: false
        }
      ]);
      dialogServiceStub.open.and.returnValue(of({ clickedBtn: FooterButton.Apply }));
      dataService.save.and.returnValue(of(null));
      configService.build.and.returnValue({ data: { buttons: [] } });

      let dialogResult: FooterButton;
      service.confirm(translations.applyChanges).subscribe(result => (dialogResult = result));
      tick();

      expect(dialogResult).toBe(FooterButton.Apply);
      expect(mockStore.dispatch).toHaveBeenCalledWith(
        SettingConfigurationActions.applyChangesSuccess({ updatedParameters: null })
      );
    }));

    it('should dispatch an applyError action if there is a problem to save the changes', fakeAsync(() => {
      mockStore.overrideSelector(fromConfiguration.getModifications, [
        {
          id: 0,
          descriptorId: 0,
          type: ParameterType.Boolean,
          name: 'assignRaAutomaticallyForNewAgents',
          value: 'true',
          readonly: false
        }
      ]);
      dialogServiceStub.open.and.returnValue(of({ clickedBtn: FooterButton.Apply }));
      dataService.save.and.returnValue(throwError({ message: 'Service Unavailable' }));
      configService.build.and.returnValue({ data: { buttons: [] } });

      service.confirm(translations.applyChanges).subscribe(
        () => {},
        error => {
          expect(error).toBeDefined();
        }
      );
      tick();
    }));
  });
});
