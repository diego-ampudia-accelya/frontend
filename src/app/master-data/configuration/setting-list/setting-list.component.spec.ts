import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationModule } from 'angular-l10n';
import moment from 'moment-mini';
import { MockComponents } from 'ng-mocks';

import { ParameterType } from '../enum/parameter-type.enum';
import { ValidatorType } from '../enum/validator-type.enum';
import { CategoryDescriptor } from '../models/category-descriptor.model';
import { ParameterDescriptor } from '../models/parameter-descriptor.model';
import { Parameter } from '../models/parameters.model';

import { SettingListComponent } from './setting-list.component';
import { TagsInputComponent } from '~app/shared/components/tags-input/tags-input.component';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { CheckboxComponent, InputComponent, SelectComponent } from '~app/shared/components';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('SettingListComponent', () => {
  let component: SettingListComponent;
  let fixture: ComponentFixture<SettingListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SettingListComponent,
        MockComponents(CheckboxComponent, InputComponent, SelectComponent, TagsInputComponent, DatepickerComponent)
      ],
      imports: [HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should build reactive form properly', () => {
    const categoryDescriptors = [{ id: 1, parameters: [{ id: 20 }] }] as CategoryDescriptor[];
    const configurationParameters = [{ id: 20, descriptorId: 20, value: false }] as Parameter[];
    const expectedFormValue = { '20': false };

    component.categoryDescriptors = categoryDescriptors;
    component.configurationParameters = configurationParameters;
    component.ngOnChanges();

    expect(component.form.value).toEqual(expectedFormValue);
  });

  it('should build reactive form with validators', () => {
    const categoryDescriptors = [
      {
        id: 1,
        parameters: [
          {
            id: 20,
            type: ParameterType.String,
            validators: [
              { type: ValidatorType.Regex, value: 'test' },
              { type: ValidatorType.Max, value: '5' }
            ]
          }
        ]
      }
    ] as CategoryDescriptor[];
    const configurationParameters = [{ id: 20, descriptorId: 20, value: 'value1' }] as Parameter[];

    component.categoryDescriptors = categoryDescriptors;
    component.configurationParameters = configurationParameters;
    component.ngOnChanges();

    const formControl = component.form.get('20');

    expect(Object.keys(formControl.errors).length).toEqual(1);
    expect(formControl.errors.pattern.requiredPattern).toEqual('^test$');
  });

  it('should update the form value when the configurationParameters change', () => {
    const categoryDescriptors = [{ id: 1, parameters: [{ id: 20 }] }] as CategoryDescriptor[];
    const initialParameters = [{ id: 20, descriptorId: 20, value: false }] as Parameter[];
    const changedParameters = [{ id: 20, descriptorId: 20, value: true }] as Parameter[];
    const expectedFormValue = { '20': true };

    component.categoryDescriptors = categoryDescriptors;
    component.configurationParameters = initialParameters;
    component.ngOnChanges();

    component.configurationParameters = changedParameters;
    component.ngOnChanges();

    expect(component.form.value).toEqual(expectedFormValue);
  });

  it('should ignore parameters without descriptors when setting the form value', () => {
    const categoryDescriptors = [{ id: 1, parameters: [{ id: 20 }] }] as CategoryDescriptor[];
    const configurationParameters = [
      { id: 20, descriptorId: 20, value: false },
      { id: 10, descriptorId: 10, value: true, name: 'unknown' }
    ] as Parameter[];
    const expectedFormValue = { '20': false };

    component.categoryDescriptors = categoryDescriptors;
    component.configurationParameters = configurationParameters;
    component.ngOnChanges();

    expect(component.form.value).toEqual(expectedFormValue);
  });

  it('should set form control value to `null` when there is no corresponding parameter', () => {
    const categoryDescriptors = [{ id: 1, parameters: [{ id: 20 }, { id: 10 }] }] as CategoryDescriptor[];
    const configurationParameters = [{ id: 20, descriptorId: 20, value: false }] as Parameter[];
    const expectedFormValue = { '20': false, '10': null };

    component.categoryDescriptors = categoryDescriptors;
    component.configurationParameters = configurationParameters;
    component.ngOnChanges();

    expect(component.form.value).toEqual(expectedFormValue);
  });

  describe('changeDateSettingValue', () => {
    it('should call changeSettingValue with formatted date and id', () => {
      component.changeSettingValue = jasmine.createSpy();
      const date = moment('20221225').toDate();

      component.changeDateSettingValue(date, 5);

      expect(component.changeSettingValue).toHaveBeenCalledWith('2022-12-25', 5);
    });

    it('should call changeSettingValue with null and id', () => {
      component.changeSettingValue = jasmine.createSpy();

      component.changeDateSettingValue(null, 5);

      expect(component.changeSettingValue).toHaveBeenCalledWith(null, 5);
    });
  });

  it('should change setting value', () => {
    spyOn(component.valueChange, 'emit');
    component.configurationParameters = [
      {
        id: 1,
        descriptorId: 1,
        type: ParameterType.Boolean,
        name: 'sett 1 name',
        value: 'sett 1 value',
        readonly: false
      },
      {
        id: 2,
        descriptorId: 2,
        type: ParameterType.Integer,
        name: 'sett 2 name',
        value: 'sett 2 value',
        readonly: true
      }
    ];

    component.changeSettingValue('test value 3', 2);
    expect(component.valueChange.emit).toHaveBeenCalledWith({
      parameter: {
        id: 2,
        descriptorId: 2,
        type: ParameterType.Integer,
        name: 'sett 2 name',
        value: 'test value 3',
        readonly: true
      },
      isFormValid: true
    });
  });

  it('should get parameter readonly state', () => {
    component.configurationParameters = [
      {
        id: 1,
        descriptorId: 1,
        type: ParameterType.Boolean,
        name: 'sett 1 name',
        value: 'sett 1 value',
        readonly: false
      },
      {
        id: 2,
        descriptorId: 2,
        type: ParameterType.Integer,
        name: 'sett 2 name',
        value: 'sett 2 value',
        readonly: true
      }
    ];

    const result = component.getParameterReadonlyState(2);
    expect(result).toBe(true);
  });

  it('should not return parameter readonly state when there are no configurationParameters', () => {
    component.configurationParameters = [];

    expect(component.getParameterReadonlyState(2)).toBe(undefined);
  });

  it('should get item id when tracked by category', () => {
    const result = component.trackByCategories(null, {
      id: 1,
      parameters: []
    });
    expect(result).toBe(1);
  });

  it('should get item id when tracked by settings', () => {
    const result = component.trackBySettings(null, {
      id: 2,
      descriptorId: 2,
      type: ParameterType.Integer,
      name: 'sett 2 name',
      value: 'sett 2 value',
      readonly: true
    });
    expect(result).toBe(2);
  });

  describe('isParamSupported()', () => {
    it('should return true for BOOLEAN', () => {
      const parameter = { type: ParameterType.Boolean } as ParameterDescriptor;
      const result = component.isParamSupported(parameter);

      expect(result).toBe(true);
    });

    it('should return true for ENUM', () => {
      const parameter = { type: ParameterType.Enum } as ParameterDescriptor;
      const result = component.isParamSupported(parameter);

      expect(result).toBe(true);
    });

    it('should return true for INTEGER', () => {
      const parameter = { type: ParameterType.Integer } as ParameterDescriptor;
      const result = component.isParamSupported(parameter);

      expect(result).toBe(true);
    });

    it('should return true for STRING', () => {
      const parameter = { type: ParameterType.String } as ParameterDescriptor;
      const result = component.isParamSupported(parameter);

      expect(result).toBe(true);
    });

    it('should return true for STRING_LIST', () => {
      const parameter = { type: ParameterType.StringList } as ParameterDescriptor;
      const result = component.isParamSupported(parameter);

      expect(result).toBe(true);
    });

    it('should return false for some invalid type', () => {
      const parameter = { type: 'some invalid type' as ParameterType } as ParameterDescriptor;
      const result = component.isParamSupported(parameter);

      expect(result).toBe(false);
    });

    it('should return false when the parameter is missing', () => {
      const result = component.isParamSupported(null);

      expect(result).toBe(false);
    });
  });
});
