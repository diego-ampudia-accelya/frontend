import { InputSetting } from './input-setting.model';

export interface CheckboxSetting extends InputSetting {
  displayType: 'checkbox';
}
