import { CategoryDescriptor } from '../../models/category-descriptor.model';
import { AlertMessageType } from '~app/shared/enums';

export interface SettingAlertMessage {
  type: AlertMessageType;
  message: string;
  // Function with the logic neccessary to show/hide alert message from the categories and its values
  calculateVisibility: (categories: CategoryDescriptor[], formValue: any) => boolean;
  isVisible?: boolean;
}
