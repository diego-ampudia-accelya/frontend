import { InputSetting } from './input-setting.model';

export interface NumericInputSetting extends InputSetting {
  displayType: 'numeric';
  value: number;
  minValue?: number;
  maxValue?: number;
}
