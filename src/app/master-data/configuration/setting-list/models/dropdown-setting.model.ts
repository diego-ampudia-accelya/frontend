import { DropdownOption } from '~app/shared/models/dropdown-option.model';

export interface DropdownSetting {
  displayType: string;
  options: DropdownOption[];
}
