import { InputSetting } from './input-setting.model';
import { RadioButton } from '~app/shared/models/radio-button.model';

export interface RadioButtonSetting extends InputSetting {
  displayType: 'radio';
  options: RadioButton[];
}
