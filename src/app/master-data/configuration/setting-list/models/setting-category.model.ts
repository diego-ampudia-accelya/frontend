import { InputSetting } from './input-setting.model';

export interface SettingCategory {
  label: string;
  settings: InputSetting[];
}
