import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { RadioButton } from '~app/shared/models/radio-button.model';

export interface InputSetting {
  label: string;
  value: string | boolean | number;
  displayType: string | DisplayTypes;
  field: string;
  description?: string;
  options?: RadioButton[] | DropdownOption[];
  minValue?: number;
  maxValue?: number;
  placeholder?: string;
  validators?:
    | [(control: AbstractControl) => ValidationErrors, ValidatorFn]
    | [(control: AbstractControl) => ValidationErrors]
    | [(control: AbstractControl) => ValidationErrors, ValidatorFn, ValidatorFn];
  disabled?: boolean;
}

export enum DisplayTypes {
  input = 'input',
  textarea = 'textarea',
  checkbox = 'checkbox',
  radio = 'radio',
  numeric = 'numeric',
  dropdown = 'dropdown'
}
