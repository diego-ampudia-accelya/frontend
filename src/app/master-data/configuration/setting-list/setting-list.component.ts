import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { chain, Dictionary, isEmpty } from 'lodash';
import moment from 'moment-mini';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ParameterType } from '../enum/parameter-type.enum';
import { ValidatorType } from '../enum/validator-type.enum';
import { CategoryDescriptor } from '../models/category-descriptor.model';
import { ParameterDescriptor } from '../models/parameter-descriptor.model';
import { Parameter } from '../models/parameters.model';
import { SettingAlertMessage } from './models/setting-alert-message.model';
import { multiValueValidator } from '~app/shared/validators/multi-value.validator';

@Component({
  selector: 'bspl-setting-list',
  templateUrl: './setting-list.component.html',
  styleUrls: ['./setting-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingListComponent implements OnChanges, OnDestroy {
  @Input() categoryDescriptors: CategoryDescriptor[] = [];
  @Input() configurationParameters: Parameter[] = [];
  @Input() stringListSeparators: string[];

  @Input() settingAlertMessages: SettingAlertMessage[] = [];
  @Input() disableAllActions: boolean;

  @Output() valueChange = new EventEmitter<any>();

  public parameterType = ParameterType;
  public validatorType = ValidatorType;
  public form: FormGroup = new FormGroup({});

  public destroy$ = new Subject();

  public ngOnChanges(): void {
    const hasConfig = !isEmpty(this.categoryDescriptors) && !isEmpty(this.configurationParameters);
    if (hasConfig) {
      if (isEmpty(this.form.value)) {
        this.form = this.buildForm();
        this.initializeSettingAlertMessages();
      }
      this.form.patchValue(this.buildFormValue());
    }
  }

  public changeDateSettingValue(newValue: Date | null, parameterDescriptorId: number): void {
    const stringDate = newValue ? moment(newValue).format('YYYY-MM-DD') : newValue;

    this.changeSettingValue(stringDate, parameterDescriptorId);
  }

  public changeSettingValue(newValue: any, parameterDescriptorId: number): void {
    let parameter = this.configurationParameters.find(param => param.descriptorId === parameterDescriptorId);
    // Some of the bspl input components emit a change event when their value changes
    // Check if the value has actually changed to avoid an infinite loop
    if (parameter.value !== newValue) {
      parameter = { ...parameter, value: newValue };

      this.valueChange.emit({ parameter, isFormValid: this.form.valid });
    }
  }

  public getParameterReadonlyState(parameterDescriptorId: number): boolean {
    if (!isEmpty(this.configurationParameters)) {
      const parameter = this.configurationParameters.find(
        (param: Parameter) => param.descriptorId === parameterDescriptorId
      );

      return parameter?.readonly;
    }
  }

  public trackByCategories(_, item: CategoryDescriptor) {
    return item.id;
  }

  public trackBySettings(_, item: Parameter) {
    return item.id;
  }

  public areAllCategorySettingsInReadonlyState(category: CategoryDescriptor): boolean {
    return category?.parameters?.every((param: ParameterDescriptor) => this.getParameterReadonlyState(param.id));
  }

  public isParamSupported(param: ParameterDescriptor): boolean {
    const supportedTypes: ParameterType[] = [
      ParameterType.Boolean,
      ParameterType.Enum,
      ParameterType.Integer,
      ParameterType.String,
      ParameterType.StringList,
      ParameterType.Date
    ];

    return supportedTypes.some(t => param?.type === t);
  }

  public updateSettingAlertMessagesVisibility(): void {
    // Setting isVisible attribute to show/hide alert depending on result of the function
    this.settingAlertMessages.forEach(
      alertMessage =>
        (alertMessage.isVisible = alertMessage.calculateVisibility(this.categoryDescriptors, this.form.value))
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeSettingAlertMessages(): void {
    if (this.settingAlertMessages.length) {
      this.form.valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => this.updateSettingAlertMessagesVisibility());
    }
  }

  private getConfigurationParameterValidators(parameterDescriptor: ParameterDescriptor): ValidatorFn[] {
    const validators: ValidatorFn[] = [];
    const regexValidators: ValidatorFn[] = this.getValidatorValues(parameterDescriptor, ValidatorType.Regex).map(v =>
      Validators.pattern(v)
    );

    if (parameterDescriptor.type === ParameterType.String) {
      validators.push(...regexValidators);
    }

    if (parameterDescriptor.type === ParameterType.StringList) {
      const multipleValueValidator = multiValueValidator(regexValidators);

      validators.push(multipleValueValidator);
    }

    return validators;
  }

  private getValidatorValues(parameter: ParameterDescriptor, type: ValidatorType): string[] {
    const validators = parameter?.validators?.filter(v => v.type === type).map(v => v.value);

    return validators || [];
  }

  private buildForm(): FormGroup {
    // Each form control is named after the `id` of its corresponding parameter descriptor
    const parameterFormControls = chain(this.categoryDescriptors)
      .map(c => c.parameters)
      .flatten()
      .keyBy(p => p.id)
      .mapValues(param => new FormControl(null, this.getConfigurationParameterValidators(param)))
      .value();

    return new FormGroup(parameterFormControls);
  }

  private buildFormValue(): Dictionary<any> {
    // The form value is a dictionary of parameter values keyed by `descriptorId`
    // Parameters without a descriptor are ignored
    return chain(this.configurationParameters)
      .filter(param => this.form.contains(param.id.toString()))
      .keyBy(p => p.descriptorId)
      .mapValues(p => p.value)
      .value();
  }
}
