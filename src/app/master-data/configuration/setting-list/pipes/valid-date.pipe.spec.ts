import moment from 'moment-mini';

import { ParameterType } from '../../enum/parameter-type.enum';
import { ValidatorType } from '../../enum/validator-type.enum';

import { ValidDatePipe } from './valid-date.pipe';

describe('ValidDatePipe', () => {
  const parameter = {
    displayName: 'End Date',
    id: 69830088,
    name: 'End Date',
    type: ParameterType.Date,
    validators: [
      { type: ValidatorType.Min, value: '2022-12-10' },
      { type: ValidatorType.Max, value: '2023-02-05' }
    ]
  };

  it('create an instance', () => {
    const pipe = new ValidDatePipe();
    expect(pipe).toBeTruthy();
  });

  it('should return max date', () => {
    const pipe = new ValidDatePipe();
    const res = pipe.transform(parameter, ValidatorType.Max);
    const expectedDate = moment('2023-02-05').toDate();

    expect(res).toEqual(expectedDate);
  });

  it('should return min date', () => {
    const pipe = new ValidDatePipe();
    const res = pipe.transform(parameter, ValidatorType.Min);
    const expectedDate = moment('2022-12-10').toDate();

    expect(res).toEqual(expectedDate);
  });

  it('should return null for min date validator', () => {
    const pipe = new ValidDatePipe();
    const param = { ...parameter, validators: [] };

    const res = pipe.transform(param, ValidatorType.Min);
    expect(res).toEqual(null);
  });

  it('should return default for max date validator', () => {
    const pipe = new ValidDatePipe();
    const param = { ...parameter, validators: [] };

    const res = pipe.transform(param, ValidatorType.Max);
    expect(res).toEqual(moment().add(1, 'years').endOf('year').toDate());
  });
});
