import { ParameterType } from '../../enum/parameter-type.enum';
import { ValidatorType } from '../../enum/validator-type.enum';

import { ValidNumberPipe } from './valid-number.pipe';

describe('ValidNumberPipe', () => {
  const parameter = {
    displayName: 'Number',
    id: 69830088,
    name: 'Number',
    type: ParameterType.Integer,
    validators: [
      { type: ValidatorType.Max, value: '15' },
      { type: ValidatorType.Min, value: '5' }
    ]
  };

  it('create an instance', () => {
    const pipe = new ValidNumberPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return max value', () => {
    const pipe = new ValidNumberPipe();
    const res = pipe.transform(parameter, ValidatorType.Max);

    expect(res).toEqual('15');
  });

  it('should return max validator value', () => {
    const pipe = new ValidNumberPipe();
    const res = pipe.transform(parameter, ValidatorType.Min);

    expect(res).toEqual('5');
  });

  it('should return null for max validator', () => {
    const pipe = new ValidNumberPipe();
    const param = { ...parameter, validators: [] };
    const res = pipe.transform(param, ValidatorType.Min);

    expect(res).toEqual(null);
  });

  it('should return null for max validator', () => {
    const pipe = new ValidNumberPipe();
    const param = { ...parameter, validators: [] };
    const res = pipe.transform(param, ValidatorType.Max);

    expect(res).toEqual(null);
  });
});
