import { Pipe, PipeTransform } from '@angular/core';

import { ValidatorType } from '../../enum/validator-type.enum';
import { ParameterDescriptor } from '../../models/parameter-descriptor.model';

@Pipe({
  name: 'validNumber'
})
export class ValidNumberPipe implements PipeTransform {
  transform(parameter: ParameterDescriptor, type: ValidatorType): string {
    const validatorValue: string = parameter.validators?.find(v => v.type === type)?.value;

    return validatorValue || null;
  }
}
