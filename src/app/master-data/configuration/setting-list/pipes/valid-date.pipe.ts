import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment-mini';

import { ValidatorType } from '../../enum/validator-type.enum';
import { ParameterDescriptor } from '../../models/parameter-descriptor.model';

const defaultMaxDate = moment().add(1, 'years').endOf('year').toDate();

@Pipe({
  name: 'validDate'
})
export class ValidDatePipe implements PipeTransform {
  transform(parameter: ParameterDescriptor, type: ValidatorType): Date | null {
    const stringValidationDate: string = parameter.validators?.find(v => v.type === type)?.value;

    if (stringValidationDate) {
      return moment(stringValidationDate).toDate();
    }

    return type === ValidatorType.Max ? defaultMaxDate : null;
  }
}
