import { ParameterType } from '../enum/parameter-type.enum';
import { ValidatorType } from '../enum/validator-type.enum';
import { ConfigurationDescriptor } from '../models/configuration-descriptor.model';
import { Configuration } from '../models/configuration.model';

export const acdmBspConfigurationDescriptor: ConfigurationDescriptor = {
  id: 0,
  scopeType: 'bsps',
  service: 'acdm-management',
  displayName: 'ADM/ACM Basic Settings',
  categories: [
    {
      id: 0,
      displayName: 'ADM/ACM Actions',
      parameters: [
        {
          id: 320,
          type: ParameterType.Integer,
          name: 'admLatencyDays',
          displayName: 'ADM Latency Days',
          description: 'The number of days that ADMs should remain pending to be disputed',
          validators: [
            {
              type: ValidatorType.Min,
              value: '0'
            },
            {
              type: ValidatorType.Max,
              value: '15'
            }
          ]
        },
        {
          id: 1564,
          type: ParameterType.Integer,
          name: 'acmLatencyDays',
          displayName: 'ACM Latency Days',
          description: 'The number of days that ACMs should remain pending to be disputed',
          validators: [
            {
              type: ValidatorType.Min,
              value: '0'
            },
            {
              type: ValidatorType.Max,
              value: '99'
            }
          ]
        },
        {
          id: 254,
          type: ParameterType.Boolean,
          name: 'acdnt',
          displayName: 'ADNT/ACNT',
          description: 'Non-ticket related debits and credits are allowed'
        },
        {
          id: 3564,
          type: ParameterType.Boolean,
          name: 'admGdsForward',
          displayName: 'ADM GDS Forward',
          description: 'ADM GDS forward functionality is available'
        },
        {
          id: 544,
          type: ParameterType.Boolean,
          name: 'duplicatedACDMNumbers',
          displayName: 'Duplicated ADM/ACM Numbers',
          description: 'Duplicated ACDM numbers (TDNR) are allowed'
        },
        {
          id: 875,
          type: ParameterType.Boolean,
          name: 'acdmFormForSPCDR',
          displayName: 'Standard ADM/ACM form for SPDR/SPCR',
          description: 'SPDR/SPCR issuance through standard ADM/ACM forms'
        }
      ]
    },
    {
      id: 2,
      displayName: 'ADM/ACM Settings',
      parameters: [
        {
          id: 564,
          type: ParameterType.Boolean,
          name: 'cpOnACDMConcerningIssue',
          displayName: 'CP on ADMs/ACMs concerning issue',
          description: 'CP (Cancellation Penalty) taxes are allowed in ADMs/ACMs concerning an issue'
        },
        {
          id: 341,
          type: ParameterType.Boolean,
          name: 'mfOnACDMConcerningIssue',
          displayName: 'MF on ADMs/ACMs concerning issue',
          description: 'MF (Miscellaneous Fee) taxes are allowed in ADMs/ACMs concerning an issue'
        },
        {
          id: 26,
          type: ParameterType.Boolean,
          name: 'acdmSpam',
          displayName: 'ADM/ACM SPAM',
          description: 'NR (Net Remit) and Supplementary Commission are allowed in ADMs/ACMs'
        },
        {
          id: 63,
          type: ParameterType.Boolean,
          name: 'acdmToca',
          displayName: 'ADM/ACM TOCA',
          description: 'Tax on Commission is allowed'
        },
        {
          id: 74,
          type: ParameterType.Boolean,
          name: 'positiveVat',
          displayName: 'Positive VAT',
          description:
            'Vat on Commission Positive for Airlines. If not selected, the Agent will receive the amount of the VAT on Commission.'
        },
        {
          id: 877,
          type: ParameterType.Enum,
          name: 'taxBreakdown',
          displayName: 'Tax breakdown in ADM/ACM transactions',
          description: 'Airline and Agents input tax breakdown and difference or only the difference.',
          possibleValues: [
            {
              displayName: 'Airline Configuration',
              value: 'AIRLINE_CONFIGURATION'
            },
            {
              displayName: 'Yes',
              value: 'YES'
            },
            {
              displayName: 'No',
              value: 'NO'
            }
          ]
        }
      ]
    }
  ]
};

export const acdmBasicBspParameters: Configuration = {
  id: 0,
  descriptorId: 0,
  scopeType: 'bsps',
  scopeId: '6458',
  service: 'acdm-management',
  parameters: [
    {
      id: 0,
      descriptorId: 320,
      type: ParameterType.Integer,
      name: 'admLatencyDays',
      value: '1',
      readonly: false
    },
    {
      id: 1,
      descriptorId: 1564,
      type: ParameterType.Integer,
      name: 'acmLatencyDays',
      value: '0',
      readonly: false
    },
    {
      id: 2,
      descriptorId: 254,
      type: ParameterType.Boolean,
      name: 'acdnt',
      value: 'false',
      readonly: false
    },
    {
      id: 3,
      descriptorId: 3564,
      type: ParameterType.Boolean,
      name: 'admGdsForward',
      value: 'false',
      readonly: false
    },
    {
      id: 4,
      descriptorId: 544,
      type: ParameterType.Boolean,
      name: 'duplicatedACDMNumbers',
      value: 'false',
      readonly: false
    },
    {
      id: 5,
      descriptorId: 875,
      type: ParameterType.Boolean,
      name: 'acdmFormForSPCDR',
      value: 'false',
      readonly: false
    },
    {
      id: 6,
      descriptorId: 564,
      type: ParameterType.Boolean,
      name: 'cpOnACDMConcerningIssue',
      value: 'false',
      readonly: false
    },
    {
      id: 7,
      descriptorId: 341,
      type: ParameterType.Boolean,
      name: 'mfOnACDMConcerningIssue',
      value: 'false',
      readonly: false
    },
    {
      id: 8,
      descriptorId: 26,
      type: ParameterType.Boolean,
      name: 'acdmSpam',
      value: 'false',
      readonly: false
    },
    {
      id: 9,
      descriptorId: 63,
      type: ParameterType.Boolean,
      name: 'acdmToca',
      value: 'false',
      readonly: false
    },
    {
      id: 10,
      descriptorId: 74,
      type: ParameterType.Boolean,
      name: 'positiveVat',
      value: 'false',
      readonly: false
    },
    {
      id: 11,
      descriptorId: 877,
      type: ParameterType.Enum,
      name: 'taxBreakdown',
      value: 'AIRLINE_CONFIGURATION',
      readonly: false
    }
  ]
};

export const pbdConfigurationDescriptor: ConfigurationDescriptor = {
  id: 0,
  scopeType: 'bsps',
  service: 'pbd-management',
  displayName: 'PBD General PArameters',
  categories: [
    {
      id: 0,
      parameters: [
        {
          id: 0,
          type: ParameterType.Integer,
          name: 'maxPbdStartingPeriod',
          displayName: 'Maximum period for initiating Post Billing Dispute (months)',
          description:
            'The number of months that a PBD will be possible, counting from the date when the documents were issued. Allowed values from 1 to 12.',
          validators: [
            {
              type: ValidatorType.Min,
              value: '1'
            },
            {
              type: ValidatorType.Max,
              value: '12'
            }
          ]
        },
        {
          id: 1,
          type: ParameterType.Integer,
          name: 'maxPbdResolutionPeriod',
          displayName: 'Maximum Post Billing Dispute resolution period (days)',
          description:
            'The maximum number of days to solve the PBD between the involved parties. Allowed values from 1 to 365.',
          validators: [
            {
              type: ValidatorType.Min,
              value: '1'
            },
            {
              type: ValidatorType.Max,
              value: '365'
            }
          ]
        }
      ]
    }
  ]
};

export const pdbParameters: Configuration = {
  id: 0,
  descriptorId: 0,
  scopeType: 'bsps',
  scopeId: '856458',
  service: 'pbd-management',
  parameters: [
    {
      id: 0,
      descriptorId: 0,
      type: ParameterType.Integer,
      name: 'maxPbdStartingPeriod',
      value: '12',
      readonly: false
    },
    {
      id: 1,
      descriptorId: 1,
      type: ParameterType.Integer,
      name: 'maxPbdResolutionPeriod',
      value: '30',
      readonly: false
    }
  ]
};
