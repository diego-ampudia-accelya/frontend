import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router, UrlTree } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { filter, first, map, takeUntil, tap } from 'rxjs/operators';

import { MenuBuilder } from '../menu-builder.service';
import { RoutedMenuItem } from '../models/routed-menu-item.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ConfigurationActions } from '~app/master-data/airline/actions';
import * as fromAirlineConfiguration from '~app/master-data/airline/reducers';
import { AirlinesDisplayFilterFormatter } from '~app/master-data/airline/services/airline-display-filter-formatter.service';
import { AirlineProfileBspFilter } from '~app/master-data/models/airline-profile-bsp-filter.model';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { TabService } from '~app/shared/services';

@Component({
  selector: 'bspl-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
  providers: [AirlinesDisplayFilterFormatter]
})
export class ConfigurationComponent implements OnInit {
  @Input() showBspFilter = true;

  public menuItems: RoutedMenuItem[];
  public activeItem$: Observable<RoutedMenuItem>;
  public bspCountriesList$: Observable<DropdownOption<Bsp>[]>;
  public predefinedFilters: Partial<AirlineProfileBspFilter> = { bsp: null };
  public isFilterValid$: Observable<boolean>;
  public isBspFilterVisible: boolean;

  private navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));
  private loggedUser$ = this.store.pipe(select(getUser), first());

  public hasLeanPermission: boolean;
  public isConfigurationSearched: boolean;
  public isFilterEnabled = true;
  public subtabTitle = 'menu.masterData.configuration.title';
  public filterForm: FormGroup;
  private formUtil: FormUtil;

  private destroy$ = new Subject();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    private permissionsService: PermissionsService,
    private store: Store<AppState>,
    public displayFormatter: AirlinesDisplayFilterFormatter,
    private tabService: TabService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.isBspFilterVisible = this.hasLeanPermission && this.showBspFilter;

    if (this.isBspFilterVisible) {
      this.buildFilterForm();
      this.initializeBspDropdown();
      this.initializeStoredBspFilterListener();
      this.initializeFormValidityListener();
    } else {
      this.buildMenuItems();
    }
  }

  private buildMenuItems(bspId?: number): void {
    this.menuItems = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot, bspId);
    this.getActiveItems();
  }

  private getActiveItems(): void {
    this.activeItem$ = merge(of(null), this.navigated$).pipe(
      map(() => {
        let active = this.findActiveItem();

        if (active == null) {
          active = this.menuItems[0];
          this.selectMenuItem({ item: active });
        }

        return active;
      })
    );
  }

  public selectMenuItem(event: { item: RoutedMenuItem; preventDefault?: () => any }): void {
    if (event.preventDefault) {
      event.preventDefault();
    }
    this.router.navigateByUrl(this.getUrl(event.item));
  }

  public saveBspFilter(value?: AirlineProfileBspFilter): void {
    const { bsp } = value || this.filterForm.value;

    this.store.dispatch(ConfigurationActions.updateSelectedBsp({ selectedBsp: bsp }));
    this.buildMenuItems(bsp.id);
    this.tabService.refreshCurrentTab();
  }

  private buildFilterForm(): void {
    this.filterForm = this.formUtil.createGroup<AirlineProfileBspFilter>({
      bsp: [null, Validators.required]
    });
  }

  private getUrl(item: RoutedMenuItem): UrlTree {
    return this.router.createUrlTree([item.route], { relativeTo: this.activatedRoute });
  }

  private findActiveItem(): RoutedMenuItem {
    return this.menuItems.find(item => this.router.isActive(this.getUrl(item), false));
  }

  private initializeBspDropdown(): void {
    this.bspCountriesList$ = this.loggedUser$.pipe(
      map(
        loggedUser =>
          this.filterBspsByPermission(loggedUser).map(bsp => toValueLabelObjectBsp(bsp)) as DropdownOption<Bsp>[]
      ),
      tap(bspOptions => (this.isFilterEnabled = bspOptions.length > 1))
    );
    this.cd.detectChanges();
  }

  private filterBspsByPermission(loggedUser: User): Bsp[] {
    const permission = Permissions.readAirlineConfiguration;
    const { bsps, bspPermissions } = loggedUser;

    return bsps.filter(bsp =>
      bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
    );
  }

  private initializeStoredBspFilterListener(): void {
    this.store
      .pipe(select(fromAirlineConfiguration.getConfigurationSelectedBsp), takeUntil(this.destroy$))
      .subscribe(selectedBsp => {
        const filterFormBsp: Bsp = this.filterForm.value.bsp;

        if (selectedBsp && filterFormBsp?.id !== selectedBsp.id) {
          this.filterForm.patchValue({ bsp: selectedBsp });
        }

        this.buildMenuItems(selectedBsp.id);
      });
  }

  private initializeFormValidityListener(): void {
    // Filter form will be invalid if selected BSP and filter form BSP are equal
    this.isFilterValid$ = combineLatest([
      this.store.pipe(select(fromAirlineConfiguration.getConfigurationSelectedBsp)),
      this.filterForm.valueChanges
    ]).pipe(map(([selectedBsp, filterFormValue]) => selectedBsp.id !== filterFormValue.bsp?.id));
  }
}
