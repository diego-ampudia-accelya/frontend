import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockComponent } from 'ng-mocks';
import { ReplaySubject } from 'rxjs';

import { MenuBuilder } from '../menu-builder.service';
import { RoutedMenuItem } from '../models/routed-menu-item.model';
import { ConfigurationComponent } from './configuration.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ConfigurationActions } from '~app/master-data/airline/actions';
import { getConfigurationSelectedBsp } from '~app/master-data/airline/reducers';
import { AppState } from '~app/reducers';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { SideMenuComponent } from '~app/shared/components/side-menu/side-menu.component';
import { createAirlineMultiCountryUser } from '~app/shared/mocks/airline-user';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { AirlineUser } from '~app/shared/models/user.model';

describe('ConfigurationComponent', () => {
  let component: ConfigurationComponent;
  let fixture: ComponentFixture<ConfigurationComponent>;
  let mockStore: MockStore<AppState>;
  let routerSpy: SpyObject<Router>;

  const routerEvents$ = new ReplaySubject(1);
  const permissionsServiceSpy = createSpyObject(PermissionsService);

  const airlineMultiCountryUserMock = createAirlineMultiCountryUser();

  const initialState = {
    auth: { user: airlineMultiCountryUserMock },
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  const configurationBsp: Bsp = {
    id: 1,
    isoCountryCode: 'ES',
    name: 'SPAIN',
    effectiveFrom: '2000-01-01'
  };

  const menuItems: RoutedMenuItem[] = [
    {
      title: 'Item 1',
      route: 'item-1',
      isAccessible: true
    },
    {
      title: 'Item 2',
      route: 'item-2',
      isAccessible: true
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ConfigurationComponent, MockComponent(SideMenuComponent)],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        L10nTranslationService,
        FormBuilder,
        mockProvider(MenuBuilder),
        mockProvider(Router, { events: routerEvents$.asObservable() }),
        mockProvider(ActivatedRoute),
        { provide: PermissionsService, useValue: permissionsServiceSpy },
        provideMockStore({
          initialState,
          selectors: [
            { selector: getUser, value: createAirlineUser() },
            { selector: getConfigurationSelectedBsp, value: configurationBsp }
          ]
        })
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    mockStore = TestBed.inject<any>(Store);
    const menuBuilder: SpyObject<MenuBuilder> = TestBed.inject<any>(MenuBuilder);
    menuBuilder.buildMenuItemsFrom.and.returnValue(menuItems);
    routerSpy = TestBed.inject<any>(Router);
    routerSpy.createUrlTree.and.callFake(path => path);

    fixture = TestBed.createComponent(ConfigurationComponent);
    component = fixture.componentInstance;
    permissionsServiceSpy.hasPermission.and.returnValue(true);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create 2 menu items', () => {
    expect(component.menuItems.length).toBe(2);
  });

  it('should activate first item when it matches the active route', fakeAsync(() => {
    component.activeItem$.subscribe(activeItem => {
      expect(activeItem.route).toBe('item-1');
      expect(routerSpy.navigateByUrl).toHaveBeenCalledWith(['item-1']);
    });
  }));

  it('should navigate to route when an item is selected', () => {
    const secondItem = menuItems[1];
    const event = {
      item: secondItem,
      preventDefault: jasmine.createSpy()
    };

    component.selectMenuItem(event);

    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith([secondItem.route]);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should activate item when the active route is changed', () => {
    routerSpy.isActive.and.callFake(([url]) => url === 'item-2');

    routerEvents$.next(new NavigationEnd(1, 'item-2', 'item-2'));

    component.activeItem$.subscribe(activeItem => {
      expect(activeItem.route).toBe('item-2');
    });
  });

  describe('Lean Permission', () => {
    describe('isFilterValid$', () => {
      it('filter form should NOT be valid if selected BSP and filter form BSP are equal', fakeAsync(() => {
        let isFilterValid: boolean;

        component.isFilterValid$.subscribe(value => (isFilterValid = value));

        // Patching the existing selected BSP
        component.filterForm.patchValue({
          bsp: {
            id: 1,
            isoCountryCode: 'ES',
            name: 'SPAIN',
            effectiveFrom: '2000-01-01'
          }
        });
        tick();
        expect(isFilterValid).toBe(false);

        // Patching another different BSP
        component.filterForm.patchValue({
          bsp: {
            id: 10006,
            isoCountryCode: 'GR',
            name: 'Greece',
            effectiveFrom: '2019-05-10'
          }
        });
        tick();
        expect(isFilterValid).toBe(true);
      }));
    });

    describe('saveFilters', () => {
      it('should update selected BSP', fakeAsync(() => {
        permissionsServiceSpy.hasPermission.and.returnValue(true);
        mockStore.overrideSelector(getUser, createAirlineMultiCountryUser());

        const mockBsp: Bsp = {
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN',
          effectiveFrom: '2000-01-01'
        };

        spyOn(mockStore, 'dispatch').and.callThrough();

        component.saveBspFilter({ bsp: mockBsp });
        tick();

        expect(mockStore.dispatch).toHaveBeenCalledWith(
          ConfigurationActions.updateSelectedBsp({ selectedBsp: mockBsp })
        );
      }));
    });

    describe('bsp dropdown', () => {
      it('should initialize stored filters in form', fakeAsync(() => {
        permissionsServiceSpy.hasPermission.and.returnValue(true);
        mockStore.overrideSelector(getUser, createAirlineMultiCountryUser());

        component.ngOnInit();
        tick();

        expect(component.filterForm.value.bsp).toEqual({
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN',
          effectiveFrom: '2000-01-01'
        });
      }));

      it('should initialize BSP dropdown options with bsps read configuration permission', fakeAsync(() => {
        let options: DropdownOption<Bsp>[];
        permissionsServiceSpy.hasPermission.and.returnValue(true);

        const bspPermissions = [
          {
            bspId: 1,
            permissions: ['rAirlCfg']
          },
          {
            bspId: 10000,
            permissions: ['rAirlCfg']
          },
          {
            bspId: 10006,
            permissions: ['noPermission']
          },
          {
            bspId: 10018,
            permissions: ['noPermission']
          }
        ];

        const userWithBspPermission: AirlineUser = { ...createAirlineMultiCountryUser(), bspPermissions };

        mockStore.overrideSelector(getUser, userWithBspPermission);
        mockStore.refreshState();

        component.ngOnInit();
        tick();
        component.bspCountriesList$.subscribe(list => (options = list));
        tick();

        expect(options).toEqual([
          {
            value: {
              id: 1,
              isoCountryCode: 'ES',
              name: 'SPAIN',
              effectiveFrom: '2000-01-01',
              effectiveTo: null,
              version: 192
            },
            label: 'ES - SPAIN'
          },
          {
            value: {
              id: 10000,
              isoCountryCode: 'BG',
              name: 'Bulgaria',
              effectiveFrom: '2019-05-10',
              effectiveTo: '2019-05-15',
              version: 4
            },
            label: 'BG - Bulgaria'
          }
        ]);
      }));
    });
  });
});
