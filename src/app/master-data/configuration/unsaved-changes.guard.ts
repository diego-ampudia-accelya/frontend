import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { SettingsViewComponent } from './settings-view/settings-view.component';
import { FooterButton } from '~app/shared/components';

@Injectable()
export class UnsavedChangesGuard implements CanDeactivate<SettingsViewComponent> {
  constructor(private dialogService: ChangesDialogService) {}

  public canDeactivate(): Observable<boolean> {
    return this.handleUnsavedChanges();
  }

  /**
   * Handles unsaved changes of basic settings configuration and returns an info about whether you can continue.
   */
  public handleUnsavedChanges(): Observable<boolean> {
    return this.dialogService.confirmUnsavedChanges().pipe(map(btnAction => btnAction !== FooterButton.Cancel));
  }
}
