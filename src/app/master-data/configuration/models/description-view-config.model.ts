import { InjectionToken, Type } from '@angular/core';

import { DescriptionService } from '../api/description-service';

export interface DescriptionViewConfig {
  entityName: string;
  title: string;
  apiService: Type<DescriptionService> | InjectionToken<DescriptionService>;
  maxItems?: number;
  createButtonLabel?: string;
  description?: {
    header?: string;
    lineLimit?: number;
    rowsLimit?: number;
    isRequired?: boolean;
    maxLength?: number;
    restrictToPattern?: RegExp;
    canValidate?: boolean;
  };
  label?: {
    header?: string;
    maxLength?: number;
    allCaps?: boolean;
    restrictToPattern?: RegExp;
  };
  permissions?: Partial<{
    read: string;
    create: string;
    update: string;
    delete: string;
  }>;
}
