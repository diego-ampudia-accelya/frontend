import { ParameterType } from '../enum/parameter-type.enum';
import { ValidatorType } from '../enum/validator-type.enum';

export interface ParameterDescriptor {
  id: number;
  type: ParameterType;
  name: string;
  displayName: string;
  description?: string;
  validators?: Validator[];
  possibleValues?: PossibleValue[];
}

interface Validator {
  type: ValidatorType;
  value: string;
}

interface PossibleValue {
  value: string;
  displayName?: string;
  label?: string;
}
