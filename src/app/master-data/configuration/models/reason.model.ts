import { ReasonType } from './reason-type.model';

export interface Reason {
  id: number;
  type?: ReasonType;
  sequenceNumber: number;
  description: string;
  title: string;
  version?: number;
}
