import { ParameterDescriptor } from './parameter-descriptor.model';

export interface CategoryDescriptor {
  id: number;
  displayName?: string;
  description?: string;
  parameters: ParameterDescriptor[];
}
