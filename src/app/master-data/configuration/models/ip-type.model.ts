export enum IpType {
  Permanent = 'ips',
  Range = 'range'
}
