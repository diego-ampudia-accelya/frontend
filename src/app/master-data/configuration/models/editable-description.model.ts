export interface EditableDescription {
  id: number;
  parentId: number;
  sequenceNumber: number;
  description: string;
  label: string;
  version?: number;
}

export interface EditableDescriptionVM {
  value: EditableDescription;
  canSave: boolean;
}
