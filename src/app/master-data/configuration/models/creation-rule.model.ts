export interface CreationRule {
  message: string;
  isValid(): boolean;
}
