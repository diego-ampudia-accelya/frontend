import { CategoryDescriptor } from './category-descriptor.model';

export interface ConfigurationDescriptor {
  id: number;
  scopeType: string;
  service: string;
  displayName: string;
  categories: CategoryDescriptor[];
}
