import { ParameterType } from '../enum/parameter-type.enum';

export interface Parameter {
  id: number;
  descriptorId: number;
  type: ParameterType;
  name: string;
  value: string | string[] | boolean;
  readonly: boolean;
}
