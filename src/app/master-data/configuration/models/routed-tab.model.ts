import { Tab } from '~app/shared/components/tabs/tab.model';

export interface RoutedTab extends Tab {
  route: string;
  url: string;
  isAccessible: boolean;
}
