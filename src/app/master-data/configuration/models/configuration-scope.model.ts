export class ConfigurationScope {
  scopeId: string;
  name?: string;
  scopeType: string;
  service?: string;
  scopeConfigName?: string;
}
