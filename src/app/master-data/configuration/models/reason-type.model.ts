export enum ReasonType {
  Issue = 'ISSUE',
  Dispute = 'DISPUTE',
  Rejection = 'REJECTION'
}
