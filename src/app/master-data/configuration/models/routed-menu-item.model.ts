import { SideMenuItem } from '../../../shared/components/side-menu/side-menu-item.model';

export interface RoutedMenuItem extends SideMenuItem {
  route: string;
  isAccessible: boolean;
}
