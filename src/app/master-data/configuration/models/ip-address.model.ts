import { IpType } from './ip-type.model';

export interface IpAddressConfigBE {
  [IpType.Permanent]: string[];
  [IpType.Range]: IpRange;
}

export type IpAddressConfig = IpAddressConfigBE & {
  ipType: IpType;
};

export interface IpRange {
  beginIp: string;
  endIp: string;
}

export interface IpAddressChange {
  label?: string;
  value?: string;
  index?: number;
}
