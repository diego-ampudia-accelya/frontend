import { AutomaticConsents } from '../enum/automatic-consents-tips.enum';
import { CostRecoveryType } from '../enum/cost-recovery-type.enum';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface TipGlobalAirlineModel {
  globalProductCountryLevelConsentManagement: boolean;
  globalProductCountryLevelConsentRule: GlobalCountryLevelConsentRule;
  globalProductAgentLevelConsentManagement: boolean;
  globalCardConsentManagement: boolean;
  globalCardCountryLevelConsentRule: GlobalCountryLevelConsentRule;
}

export interface GlobalCountryLevelConsentRule {
  type?: AutomaticConsents;
  costRecoveries?: CostRecovery[];
}

export interface CostRecovery {
  bsp: BspDto;
  costRecovery: CostRecoveryAmount;
}

export interface CostRecoveryAmount {
  type: CostRecoveryType;
  amount: string;
  currency?: Currency;
}
