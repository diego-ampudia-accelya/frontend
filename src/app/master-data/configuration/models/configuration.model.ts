import { Parameter } from './parameters.model';

export interface Configuration {
  id: number;
  descriptorId: number;
  scopeType: string;
  scopeId: string;
  service: string;
  parameters: Parameter[];
}
