import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { SharedModule } from '~app/shared/shared.module';
import { IpAddressConfigurationService } from './api/ip-address-configuration.service';
import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { ConfigurationComponent } from './configuration/configuration.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ConsentSettingsComponent } from './consent-settings/consent-settings.component';
import { ConsentSettingsChangesDialogConfigService } from './consent-settings/services/changes-dialog/changes-dialog-config.service';
import { ConsentSettingsChangesDialogService } from './consent-settings/services/changes-dialog/changes-dialog.service';
import { TipGlobalConfigurationService } from './consent-settings/services/tip-global-configuration.service';
import { DescriptionViewComponent } from './description-view/description-view.component';
import { EditableDescriptionListComponent } from './editable-description-list/editable-description-list.component';
import { EditableDescriptionComponent } from './editable-description/editable-description.component';
import { EmailAlertsSettingsComponent } from './email-alert-settings/email-alert-settings.component';
import { IpAddressChangesDialogComponent } from './ip-address-settings/changes-dialog/changes-dialog.component';
import { IpAddressChangesDialogConfigService } from './ip-address-settings/changes-dialog/services/changes-dialog-config.service';
import { IpAddressChangesDialogService } from './ip-address-settings/changes-dialog/services/changes-dialog.service';
import { IpAddressRemoveDialogConfigService } from './ip-address-settings/changes-dialog/services/remove-dialog-config.service';
import { IpAddressRemoveDialogService } from './ip-address-settings/changes-dialog/services/remove-dialog.service';
import { IpAddressSettingsComponent } from './ip-address-settings/ip-address-settings.component';
import { ValidDatePipe } from './setting-list/pipes/valid-date.pipe';
import { ValidNumberPipe } from './setting-list/pipes/valid-number.pipe';
import { SettingListComponent } from './setting-list/setting-list.component';
import { SettingsViewComponent } from './settings-view/settings-view.component';
import { ConfigurationEffects } from './store/effects/configuration.effect';
import { IpAddressConfigurationEffects } from './store/effects/ip-address-configuration.effect';
import * as fromConfiguration from './store/reducers';
import { ChangesDialogService as TipGlobalAirlineChangeDialogService } from './tip/tip-global-airline/changes-dialog/changes-dialog.service';
import { TipGlobalAirlineRemoveDialogService } from './tip/tip-global-airline/remove-dialog/tip-global-airline-remove-dialog.service';
import { TipGlobalAirlineService } from './tip/tip-global-airline/services/tip-globall-airline.service';
import { TipGlobalAirlineEffects } from './tip/tip-global-airline/store/effects/tip-global-airline.effect';
import { TipGlobalAirlineComponent } from './tip/tip-global-airline/tip-global-airline.component';
import { TipReportService } from './tip/tip-reports/services/tip-report.service';
import { TipReportsComponent } from './tip/tip-reports/tip-reports.component';
import { UnsavedChangesGuard } from './unsaved-changes.guard';

@NgModule({
  declarations: [
    DescriptionViewComponent,
    ConfirmationDialogComponent,
    EditableDescriptionComponent,
    EditableDescriptionListComponent,
    SettingListComponent,
    SettingsViewComponent,
    ConfigurationComponent,
    TipGlobalAirlineComponent,
    TipReportsComponent,
    ValidDatePipe,
    ValidNumberPipe,
    IpAddressSettingsComponent,
    IpAddressChangesDialogComponent,
    EmailAlertsSettingsComponent,
    ConsentSettingsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    StoreModule.forFeature(fromConfiguration.configurationFeatureKey, fromConfiguration.reducers),
    EffectsModule.forFeature([ConfigurationEffects, TipGlobalAirlineEffects, IpAddressConfigurationEffects])
  ],
  providers: [
    DialogService,
    UnsavedChangesGuard,
    ChangesDialogService,
    TipGlobalAirlineService,
    TipGlobalAirlineChangeDialogService,
    TipGlobalAirlineRemoveDialogService,
    TipReportService,
    IpAddressChangesDialogService,
    IpAddressChangesDialogConfigService,
    IpAddressConfigurationService,
    IpAddressRemoveDialogService,
    IpAddressRemoveDialogConfigService,
    TipGlobalConfigurationService,
    ConsentSettingsChangesDialogService,
    ConsentSettingsChangesDialogConfigService
  ],
  exports: [
    DescriptionViewComponent,
    EditableDescriptionListComponent,
    EditableDescriptionComponent,
    SettingListComponent,
    SettingsViewComponent,
    ConfigurationComponent,
    TipGlobalAirlineComponent,
    TipReportsComponent,
    IpAddressSettingsComponent,
    IpAddressChangesDialogComponent,
    EmailAlertsSettingsComponent,
    ConsentSettingsComponent
  ]
})
export class ConfigurationModule {}
