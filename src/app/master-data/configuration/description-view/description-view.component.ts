import { Component, Injector, ViewChild } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { defaultsDeep } from 'lodash';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { EditableDescriptionDataSource } from '../editable-description-data-source';
import { EditableDescriptionListComponent } from '../editable-description-list/editable-description-list.component';
import { CreationRule } from '../models/creation-rule.model';
import { DescriptionViewConfig } from '../models/description-view-config.model';
import { EditableDescriptionVM } from '../models/editable-description.model';

import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { defaultConfiguration } from './default-configuration.constant';
import { FooterButton } from '~app/shared/components';
import { CanComponentDeactivate } from '~app/core/services';
import { PermissionsService } from '~app/auth/services/permissions.service';

@Component({
  selector: 'bspl-description-view',
  templateUrl: './description-view.component.html',
  styleUrls: ['./description-view.component.scss'],
  providers: [ChangesDialogService]
})
export class DescriptionViewComponent implements CanComponentDeactivate {
  public inEditMode = false;
  public itemToSave: EditableDescriptionVM;

  public configuration: DescriptionViewConfig;
  public dataSource: EditableDescriptionDataSource;

  @ViewChild(EditableDescriptionListComponent, { static: true }) editableList: EditableDescriptionListComponent;

  public get invalidCreationRule(): CreationRule {
    return this.creationRules.find(rule => !rule.isValid());
  }

  public parentId$ = this.activatedRoute.data.pipe(map(data => this.getParentIdFrom(data)));

  public get canCreate(): boolean {
    return this.permissionsService.hasPermission(this.configuration.permissions.create);
  }

  public get canEdit(): boolean {
    return this.permissionsService.hasPermission(this.configuration.permissions.update);
  }

  public get canDelete(): boolean {
    return this.permissionsService.hasPermission(this.configuration.permissions.delete);
  }

  private creationRules: CreationRule[];

  constructor(
    private injector: Injector,
    private activatedRoute: ActivatedRoute,
    private translationService: L10nTranslationService,
    private permissionsService: PermissionsService,
    private changesDialogService: ChangesDialogService
  ) {
    const { configuration } = this.activatedRoute.snapshot.data;
    this.configuration = defaultsDeep(configuration, defaultConfiguration);
    this.dataSource = new EditableDescriptionDataSource(this.injector.get(this.configuration.apiService));
    this.creationRules = this.getItemCreationRules();
  }

  public addItem(): void {
    this.editableList.addItem();
  }

  public canDeactivate(): Observable<boolean> {
    if (!this.inEditMode) {
      return of(true);
    }

    return this.changesDialogService
      .confirmUnsavedChanges(this.itemToSave, this.configuration)
      .pipe(map(result => result !== FooterButton.Cancel));
  }

  private getItemCreationRules(): CreationRule[] {
    return [
      {
        isValid: () => this.dataSource.count < this.configuration.maxItems,
        message: this.translationService.translate('editableDescription.maxItemsError', {
          maxItems: this.configuration.maxItems,
          typeTitle: this.configuration.entityName
        })
      },
      {
        isValid: () => !this.inEditMode,
        message: this.translationService.translate('editableDescription.finishEditing')
      }
    ];
  }

  private getParentIdFrom(routeData: Data): number {
    const { parentEntity } = routeData;

    return parentEntity.id;
  }
}
