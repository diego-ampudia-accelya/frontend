import { DescriptionViewConfig } from '../models/description-view-config.model';

export const defaultConfiguration: DescriptionViewConfig = Object.freeze({
  entityName: 'Item',
  title: null,
  apiService: null,
  maxItems: Number.MAX_VALUE,
  createButtonLabel: 'Item',
  label: Object.freeze({
    header: 'Item Title'
  }),
  description: Object.freeze({
    header: 'Item Description'
  })
});
