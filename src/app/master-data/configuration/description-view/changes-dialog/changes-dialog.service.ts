import { Injectable, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { finalize, map, mapTo, switchMap } from 'rxjs/operators';

import { DescriptionViewConfig } from '../../models/description-view-config.model';
import { EditableDescription, EditableDescriptionVM } from '../../models/editable-description.model';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ChangesDialogComponent, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private injector: Injector
  ) {}

  public confirmUnsavedChanges(change: EditableDescriptionVM, config: DescriptionViewConfig): Observable<FooterButton> {
    return this.open(change, config);
  }

  private open(modification: EditableDescriptionVM, config: DescriptionViewConfig): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modification, config);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(modification.value, config);
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close())
    );
  }

  private apply(modification: EditableDescription, descViewConfig: DescriptionViewConfig): Observable<FooterButton> {
    const apiService = this.injector.get(descViewConfig.apiService);

    return apiService.save(modification).pipe(mapTo(FooterButton.Apply));
  }
}
