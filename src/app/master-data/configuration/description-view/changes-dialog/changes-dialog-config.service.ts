import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { DescriptionViewConfig } from '../../models/description-view-config.model';
import { EditableDescription, EditableDescriptionVM } from '../../models/editable-description.model';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable({ providedIn: 'root' })
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(modification: EditableDescriptionVM, config: DescriptionViewConfig): ChangesDialogConfig {
    const title = this.translation.translate('common.unsavedChangesDialogTitle');
    const message = this.translation.translate('common.unsavedChangesDialogMessage');

    return {
      data: {
        title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply, isDisabled: !modification.canSave }
        ]
      },
      changes: this.formatChanges(modification.value, config),
      message
    };
  }

  private formatChanges(modification: EditableDescription, config: DescriptionViewConfig): ChangeModel[] {
    const group = this.translation.translate(config.title);

    return [
      {
        group,
        name: this.translation.translate(config.label.header),
        value: modification.label
      },
      {
        group,
        name: this.translation.translate(config.description.header),
        value: modification.description
      }
    ];
  }
}
