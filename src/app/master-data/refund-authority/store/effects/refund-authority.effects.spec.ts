import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { RefundAuthority, RefundAuthorityFilter, RefundAuthorityViewModel, ViewAs } from '../../models';
import { RefundAuthorityService } from '../../services/refund-authority.service';
import { RefundAuthorityListActions } from '../actions';
import * as fromRefundAuthority from '../reducers';
import { RefundAuthorityEffects } from './refund-authority.effects';
import { getGlobalAirlineForUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { AirlineConfigParamName, RefundAirlineConfiguration } from '~app/refund/models/refund.model';
import { FooterButton } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { NotificationService } from '~app/shared/services/notification.service';

describe('RefundAuthorityEffects', () => {
  let effects: RefundAuthorityEffects;
  let refundAuthorityServiceStub: SpyObject<RefundAuthorityService>;
  let changesDialogServiceStub: SpyObject<ChangesDialogService>;
  let notificationServiceStub: SpyObject<NotificationService>;
  let actions$: Observable<Action>;
  let mockStore: MockStore<AppState>;

  const agent1 = { id: '1', name: 'Agent 1' } as AgentSummary;
  const agent2 = { id: '2', name: 'Agent 2' } as AgentSummary;
  const agents = [agent1, agent2];

  const airline1 = { id: 1, name: 'Airline 1' } as AirlineSummary;
  const airline2 = { id: 2, name: 'Airline 2' } as AirlineSummary;
  const airlines = [airline1, airline2];

  const bsp = { id: 1, name: 'TRAPPIST-1e' } as Bsp;

  const agent1Authority: RefundAuthority = {
    agent: { id: 1, name: 'Agent 1', iataCode: '1000000' },
    airline: { id: 1, localName: 'Airline 1', iataCode: '001' },
    hasRA: true,
    hasRN: true
  };
  const agent2Authority: RefundAuthority = {
    agent: { id: 2, name: 'Agent 2', iataCode: '2000000' },
    airline: { id: 1, localName: 'Airline 1', iataCode: '001' },
    hasRA: true,
    hasRN: true
  };

  const rnfdAirlineConfig: RefundAirlineConfiguration = {
    id: null,
    descriptorId: 69834849541100,
    scopeType: 'airlines',
    scopeId: '6983484954',
    service: 'refund-management',
    parameters: [
      {
        descriptorId: 69834849541101,
        id: 69834849541101,
        name: AirlineConfigParamName.RfndAplIssue,
        readonly: false,
        type: 'BOOLEAN',
        value: 'true'
      }
    ]
  };

  beforeEach(waitForAsync(() => {
    refundAuthorityServiceStub = createSpyObject(RefundAuthorityService);
    changesDialogServiceStub = createSpyObject(ChangesDialogService);
    notificationServiceStub = createSpyObject(NotificationService);
    TestBed.configureTestingModule({
      providers: [
        RefundAuthorityEffects,
        provideMockStore({
          selectors: [
            { selector: getGlobalAirlineForUser, value: { airlines: [{ id: 1 }] } },
            { selector: fromRefundAuthority.getRequiredFilter, value: {} },
            { selector: fromRefundAuthority.getListQuery, value: defaultQuery }
          ]
        }),
        provideMockActions(() => actions$),
        { provide: RefundAuthorityService, useValue: refundAuthorityServiceStub },
        { provide: ChangesDialogService, useValue: changesDialogServiceStub },
        { provide: NotificationService, useValue: notificationServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(RefundAuthorityEffects);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('`changeRequiredFilter$`', () => {
    it('should dispatch `changeRequiredFilterSuccess` with the found data and options', fakeAsync(() => {
      const data: PagedData<RefundAuthority> = {
        records: [agent1Authority, agent2Authority],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const expectedData: PagedData<RefundAuthorityViewModel> = {
        records: [
          { ...agent1Authority, original: agent1Authority },
          { ...agent2Authority, original: agent2Authority }
        ],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const requiredFilter: RefundAuthorityFilter = {
        viewAs: ViewAs.Airline,
        bsp,
        permission: 'rRnPerm',
        airlines: [airline1]
      };

      const expectedAction = RefundAuthorityListActions.changeRequiredFilterSuccess({
        agents,
        airlines,
        config: rnfdAirlineConfig,
        data: expectedData
      });

      mockStore.overrideSelector(fromRefundAuthority.getRequiredFilter, requiredFilter);
      refundAuthorityServiceStub.find.and.returnValue(of(data));
      refundAuthorityServiceStub.getAgents.and.returnValue(of(agents));
      refundAuthorityServiceStub.getAirlines.and.returnValue(of(airlines));
      refundAuthorityServiceStub.getRefundAirlineConfiguration.and.returnValue(of(rnfdAirlineConfig));

      actions$ = of(RefundAuthorityListActions.changeRequiredFilter({ requiredFilter }));

      let dispatchedAction: Action;
      effects.changeRequiredFilter$.subscribe(action => (dispatchedAction = action));

      expect(dispatchedAction).toEqual(expectedAction);
      expect(refundAuthorityServiceStub.getAgents).toHaveBeenCalledWith(requiredFilter.bsp, requiredFilter.permission);
      expect(refundAuthorityServiceStub.getAirlines).toHaveBeenCalledWith(bsp);
      expect(refundAuthorityServiceStub.getRefundAirlineConfiguration).toHaveBeenCalledWith(1);
    }));

    it('should dispatch `changeRequiredFilterError` when `find` throws an error', fakeAsync(() => {
      const requiredFilter: RefundAuthorityFilter = {
        viewAs: ViewAs.Airline,
        bsp,
        airlines: [airline1]
      };
      const expectedAction = RefundAuthorityListActions.changeRequiredFilterError();

      mockStore.overrideSelector(fromRefundAuthority.getRequiredFilter, requiredFilter);
      refundAuthorityServiceStub.find.and.returnValue(throwError('Error'));
      refundAuthorityServiceStub.getAgents.and.returnValue(of(agents));
      refundAuthorityServiceStub.getAirlines.and.returnValue(of(airlines));

      actions$ = of(RefundAuthorityListActions.changeRequiredFilter({ requiredFilter }));

      effects.changeRequiredFilter$.subscribe(
        (action: any) => {
          expect(action).toEqual(expectedAction);
        },
        error => expect(error).toBeDefined()
      );
    }));
  });

  describe('`search$`', () => {
    it('should dispatch searchConfirmed action when the user has confirmed the operation', fakeAsync(() => {
      changesDialogServiceStub.confirmUnsavedChanges.and.returnValue(of(FooterButton.Apply));
      actions$ = of(RefundAuthorityListActions.search({ query: defaultQuery }));

      let result;
      effects.search$.subscribe(action => (result = action));
      tick();
      expect(result.type).toEqual(RefundAuthorityListActions.searchConfirmed.type);
    }));

    it('should dispatch searchCancelled action when the user has canceled the operation', fakeAsync(() => {
      changesDialogServiceStub.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));
      actions$ = of(RefundAuthorityListActions.search({ query: defaultQuery }));

      let result;
      effects.search$.subscribe(action => (result = action));

      expect(result.type).toBe(RefundAuthorityListActions.searchCancelled.type);
    }));
  });

  describe('`searchConfirmed$`', () => {
    it('should dispatch `searchSuccess` with found data', fakeAsync(() => {
      const searchAction = RefundAuthorityListActions.searchConfirmed();
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 0,
        pageSize: 20,
        pageNumber: 0
      };
      const expectedAction = RefundAuthorityListActions.searchSuccess({ data, config: rnfdAirlineConfig });
      mockStore.overrideSelector(fromRefundAuthority.getListQuery, defaultQuery);
      actions$ = of(searchAction);
      refundAuthorityServiceStub.find.and.returnValue(of(data));
      refundAuthorityServiceStub.getRefundAirlineConfiguration.and.returnValue(of(rnfdAirlineConfig));
      const actions = [];
      effects.searchConfirmed$.subscribe(action => actions.push(action));

      expect(actions).toEqual([expectedAction]);
    }));

    it('should dispatch `searchError` when find throws an error', fakeAsync(() => {
      const searchAction = RefundAuthorityListActions.searchConfirmed();
      const expectedAction = RefundAuthorityListActions.searchError();
      mockStore.overrideSelector(fromRefundAuthority.getListQuery, defaultQuery);
      actions$ = of(searchAction);
      refundAuthorityServiceStub.find.and.returnValue(throwError('Error'));

      const actions = [];
      effects.searchConfirmed$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );

      expect(actions).toEqual([expectedAction]);
    }));
  });

  describe('`openApplyChanges$`', () => {
    it('should dispatch `searchConfirmed` when the user has chosen to apply changes', fakeAsync(() => {
      changesDialogServiceStub.confirmApplyChanges.and.returnValue(of(FooterButton.Apply));
      actions$ = of(RefundAuthorityListActions.openApplyChanges());

      let result;
      effects.openApplyChanges$.subscribe(action => (result = action));

      expect(result.type).toBe(RefundAuthorityListActions.searchConfirmed.type);
    }));

    it('should dispatch NO action when the user has chosen to discard changes', fakeAsync(() => {
      changesDialogServiceStub.confirmApplyChanges.and.returnValue(of(FooterButton.Discard));
      actions$ = of(RefundAuthorityListActions.openApplyChanges());

      let result;
      effects.openApplyChanges$.subscribe(action => (result = action));

      expect(result).not.toBeDefined();
    }));
  });

  describe('`applySuccess$`', () => {
    it('should show notification message', fakeAsync(() => {
      actions$ = of(RefundAuthorityListActions.applyChangesSuccess());

      effects.applySuccess$.subscribe();

      expect(notificationServiceStub.showSuccess).toHaveBeenCalledTimes(1);
    }));
  });
});
