import { RefundAuthority, RefundAuthorityViewModel } from '../../models';
import { PagedData } from '~app/shared/models/paged-data.model';

const toViewModel = (dataModel: RefundAuthority): RefundAuthorityViewModel => ({ ...dataModel, original: dataModel });

const toDataModel = (viewModel: RefundAuthorityViewModel): RefundAuthority => {
  const { original, ...dataModel } = viewModel;

  return dataModel;
};

const toViewModels = (pagedData: PagedData<RefundAuthority>): PagedData<RefundAuthorityViewModel> => ({
  ...pagedData,
  records: (pagedData.records || []).map(toViewModel)
});

export const converter = { toViewModel, toDataModel, toViewModels };
