import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { EMPTY, forkJoin, Observable, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { RefundAuthorityService } from '../../services/refund-authority.service';
import { RefundAuthorityListActions } from '../actions';
import * as fromRefundAuthority from '../reducers';
import { converter } from './converters';
import { getGlobalAirlineForUser } from '~app/auth/selectors/auth.selectors';
import { Airline } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { RefundAirlineConfiguration } from '~app/refund/models/refund.model';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { NotificationService } from '~app/shared/services/notification.service';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class RefundAuthorityEffects {
  public changeRequiredFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RefundAuthorityListActions.changeRequiredFilter),
      withLatestFrom(
        this.store.pipe(select(fromRefundAuthority.getListQuery)),
        this.store.pipe(select(fromRefundAuthority.getRequiredFilter)),
        this.store.pipe(select(getGlobalAirlineForUser))
      ),
      switchMap(([_, query, reqFilter, globalAirline]) =>
        forkJoin([
          this.dataService.getAgents(reqFilter.bsp, reqFilter.permission),
          this.dataService.getAirlines(reqFilter.bsp),
          this.getRefundAirlineConfiguration(globalAirline?.airlines && globalAirline.airlines[0]),
          this.dataService.find(query)
        ])
      ),
      map(([agents, airlines, config, data]) =>
        RefundAuthorityListActions.changeRequiredFilterSuccess({
          agents,
          airlines,
          config,
          data: converter.toViewModels(data)
        })
      ),
      rethrowError(() => RefundAuthorityListActions.changeRequiredFilterError())
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RefundAuthorityListActions.search),
      switchMap(() =>
        this.changesDialogService
          .confirmUnsavedChanges()
          .pipe(
            map(dialogResult =>
              dialogResult === FooterButton.Cancel
                ? RefundAuthorityListActions.searchCancelled()
                : RefundAuthorityListActions.searchConfirmed()
            )
          )
      )
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RefundAuthorityListActions.searchConfirmed),
      withLatestFrom(
        this.store.pipe(select(fromRefundAuthority.getListQuery)),
        this.store.pipe(select(getGlobalAirlineForUser))
      ),
      switchMap(([_, query, globalAirline]) =>
        forkJoin([
          this.getRefundAirlineConfiguration(globalAirline?.airlines && globalAirline.airlines[0]),
          this.dataService.find(query)
        ])
      ),
      map(([config, data]) =>
        RefundAuthorityListActions.searchSuccess({
          config,
          data: converter.toViewModels(data)
        })
      ),
      rethrowError(() => RefundAuthorityListActions.searchError())
    )
  );

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RefundAuthorityListActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(dialogResult =>
        dialogResult === FooterButton.Apply ? of(RefundAuthorityListActions.searchConfirmed()) : EMPTY
      ),
      rethrowError(() => RefundAuthorityListActions.applyChangesError())
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(RefundAuthorityListActions.applyChangesSuccess),
        tap(() => this.notificationService.showSuccess('LIST.MASTER_DATA.refundAuthority.applyChanges.success'))
      ),
    { dispatch: false }
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RefundAuthorityListActions.download),
      switchMap(() => this.changesDialogService.confirmUnsavedChanges()),
      switchMap(result => (result !== FooterButton.Cancel ? of(RefundAuthorityListActions.downloadConfirmed()) : EMPTY))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(RefundAuthorityListActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromRefundAuthority.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: RefundAuthorityService,
    private store: Store<AppState>,
    private changesDialogService: ChangesDialogService,
    private notificationService: NotificationService,
    private dialogService: DialogService
  ) {}

  private getRefundAirlineConfiguration(airline: Airline): Observable<RefundAirlineConfiguration> {
    return airline ? this.dataService.getRefundAirlineConfiguration(airline.id) : of(null);
  }
}
