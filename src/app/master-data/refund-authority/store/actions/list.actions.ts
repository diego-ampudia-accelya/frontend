import { createAction, props } from '@ngrx/store';

import { RefundAuthorityFilter, RefundAuthorityViewModel } from '../../models';
import { RefundAirlineConfiguration } from '~app/refund/models/refund.model';
import { DataQuery } from '~app/shared/components/list-view';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';

export const changeRequiredFilter = createAction(
  '[Refund Authority] Change Required Filter',
  props<{ requiredFilter: RefundAuthorityFilter }>()
);
export const changeRequiredFilterSuccess = createAction(
  '[Refund Authority] Change Required Filter Success',
  props<{
    data: PagedData<RefundAuthorityViewModel>;
    config: RefundAirlineConfiguration;
    agents?: AgentSummary[];
    airlines?: AirlineSummary[];
  }>()
);
export const changeRequiredFilterError = createAction('[Refund Authority] Change Required Filter Error');

export const search = createAction('[Refund Authority] Search', props<{ query?: DataQuery<RefundAuthorityFilter> }>());
export const searchConfirmed = createAction('[Refund Authority] Search Confirmed');
export const searchCancelled = createAction('[Refund Authority] Search Cancelled');
export const searchSuccess = createAction(
  '[Refund Authority] Search Success',
  props<{
    data: PagedData<RefundAuthorityViewModel>;
    config: RefundAirlineConfiguration;
  }>()
);
export const searchError = createAction('[Refund Authority] Search Error');

export const modify = createAction('[Refund Authority] Modify', props<{ items: RefundAuthorityViewModel[] }>());

export const openApplyChanges = createAction('[Refund Authority] Open Apply Changes');

export const applyChangesSuccess = createAction('Refund Authority] Apply Success');
export const applyChangesError = createAction('[Refund Authority] Apply Error');

export const discard = createAction('[Refund Authority] Discard');

export const download = createAction('[Refund Authority] Download');
export const downloadConfirmed = createAction('[Refund Authority] Download Confirmed');
