import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './list.reducer';
import { AppState } from '~app/reducers';

export const refundAuthorityFeatureKey = 'refundAuthority';

export interface State extends AppState {
  [refundAuthorityFeatureKey]: RefundAuthorityState;
}

export interface RefundAuthorityState {
  [fromList.key]: fromList.State;
}

export function reducers(state: RefundAuthorityState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getRefundAuthority = createFeatureSelector<State, RefundAuthorityState>(refundAuthorityFeatureKey);

export const getList = createSelector(getRefundAuthority, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const getListQuery = createSelector(getList, fromList.getQuery);

export const getDownloadQuery = createSelector(
  getListQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

export const getAgentOptions = createSelector(getList, state =>
  state.options.agents.map(agent => ({
    value: agent,
    label: `${agent.code} / ${agent.name}`
  }))
);

export const getAirlineOptions = createSelector(getList, state =>
  state.options.airlines.map(airline => ({
    value: airline,
    label: `${airline.code} / ${airline.name}`
  }))
);

export const getDesignatorOptions = createSelector(getList, state =>
  state.options.airlines.map(airline => ({
    value: airline,
    label: airline.designator
  }))
);

export const getRequiredFilter = createSelector(getList, fromList.getRequiredFilter);

export const getConfig = createSelector(getList, fromList.getConfig);

export const hasApplyChanges = createSelector(getList, fromList.hasApplyChanges);

export const getChanges = createSelector(getList, fromList.getChanges);

export const hasChanges = createSelector(getList, fromList.hasChanges);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const canApplyChanges = createSelector(
  getList,
  state => fromList.hasChanges(state) && !fromList.getLoading(state)
);
