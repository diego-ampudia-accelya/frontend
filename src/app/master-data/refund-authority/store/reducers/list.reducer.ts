import { createReducer, on } from '@ngrx/store';
import { cloneDeep, isEqual } from 'lodash';

import { RefundAuthority, RefundAuthorityFilter, RefundAuthorityViewModel } from '../../models';
import { RefundAuthorityListActions } from '../actions';
import {
  AirlineConfigParameters,
  AirlineConfigParamName,
  RefundAirlineConfiguration
} from '~app/refund/models/refund.model';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<RefundAuthorityFilter>;
  previousQuery: DataQuery<RefundAuthorityFilter>;
  data: RefundAuthorityViewModel[];
  loading: boolean;
  requiredFilter: RefundAuthorityFilter;
  options: { agents?: AgentSummary[]; airlines?: AirlineSummary[] };
  config: RefundAirlineConfiguration;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false,
  requiredFilter: { bsp: null, viewAs: null },
  options: { agents: [], airlines: [] },
  config: null
};

const loadData = (state: State, data: PagedData<RefundAuthorityViewModel>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (
  newRequiredFilter: RefundAuthorityFilter,
  oldRequiredFilter: RefundAuthorityFilter,
  storedQuery: DataQuery<RefundAuthorityFilter>
): DataQuery<RefundAuthorityFilter> => {
  let query: DataQuery<RefundAuthorityFilter> = storedQuery || cloneDeep(defaultQuery);

  // when the required filter has changed
  if (!isEqual(newRequiredFilter, oldRequiredFilter)) {
    query = cloneDeep(defaultQuery);
    query.filterBy = newRequiredFilter;
  }

  return query;
};

export const reducer = createReducer(
  initialState,
  on(RefundAuthorityListActions.changeRequiredFilter, (state, { requiredFilter }) => ({
    ...state,
    requiredFilter,
    loading: true,
    query: determineInitialQuery(requiredFilter, state.requiredFilter, state.query)
  })),
  on(RefundAuthorityListActions.changeRequiredFilterSuccess, (state, { data, airlines, agents, config }) => ({
    ...loadData(state, data),
    options: { agents, airlines },
    config
  })),
  on(RefundAuthorityListActions.search, (state, { query = state.query }) => ({
    ...state,
    previousQuery: state.query,
    requiredFilter: { ...state.requiredFilter, ...query.filterBy },
    query
  })),
  on(RefundAuthorityListActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(RefundAuthorityListActions.searchCancelled, state => ({
    ...state,
    previousQuery: null,
    query: state.previousQuery
  })),
  on(RefundAuthorityListActions.searchSuccess, (state, { data, config }) => ({ ...loadData(state, data), config })),
  on(RefundAuthorityListActions.searchError, RefundAuthorityListActions.changeRequiredFilterError, state => ({
    ...state,
    loading: false,
    data: []
  })),
  on(RefundAuthorityListActions.modify, (state, { items }) => ({
    ...state,
    data: items
  })),
  on(RefundAuthorityListActions.applyChangesSuccess, state => ({
    ...state,
    data: state.data.map(({ original, ...modified }) => ({ ...modified, original: modified }))
  })),
  on(RefundAuthorityListActions.discard, state => ({
    ...state,
    data: state.data.map(({ original }) => ({ ...original, original }))
  }))
);

// Exported functions
export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;

export const getRequiredFilter = ({ requiredFilter }: State) => requiredFilter;

export const getConfig = (state: State) => state.config;

export const hasApplyChanges = (state: State) => !state.config || getHasApplyChanges(state.config);

export const hasChanges = (state: State) => getChanges(state).length > 0;

export const getChanges = ({ data }: State) => data.filter(({ original, ...item }) => !isEqual(item, original));

export const haveSameId = (a: RefundAuthority, b: RefundAuthority) =>
  a.agent.id === b.agent.id && a.airline.id === b.airline.id;

// Private auxiliar functions
const getHasApplyChanges = (config: RefundAirlineConfiguration) => {
  const rnIssueParam: AirlineConfigParameters = config.parameters?.find(
    param => param.name === AirlineConfigParamName.RfndNoticeIssue
  );
  const raIssueParam: AirlineConfigParameters = config.parameters?.find(
    param => param.name === AirlineConfigParamName.RfndAplIssue
  );

  const allAgentsRnIssue = rnIssueParam && rnIssueParam.value === 'true';
  const allAgentsRaIssue = raIssueParam && raIssueParam.value === 'true';

  return !allAgentsRnIssue || !allAgentsRaIssue;
};
