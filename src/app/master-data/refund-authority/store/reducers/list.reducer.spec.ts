import {
  AirlineConfigParameters,
  AirlineConfigParamName,
  RefundAirlineConfiguration
} from '~app/refund/models/refund.model';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AirlineSummary } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RefundAuthority, RefundAuthorityFilter, RefundAuthorityViewModel, ViewAs } from '../../models';
import { RefundAuthorityListActions } from '../actions';
import * as fromList from './list.reducer';

const mockRefundAuthorities: RefundAuthorityViewModel[] = [
  {
    agent: { id: 1, iataCode: 'ES', name: 'Agent 1' },
    airline: { id: 101, iataCode: 'AIR', localName: 'Airline 1' },
    hasRA: true,
    hasRN: false,
    original: {
      agent: { id: 1, iataCode: 'ES', name: 'Agent original' },
      airline: { id: 101, iataCode: 'AIR', localName: 'Airline original' },
      hasRA: true,
      hasRN: false
    }
  }
];

describe('refund authority list reducer', () => {
  it('should set agents and airlines options when changeRequiredFilterSuccess action is triggered', () => {
    const mockAirlineConfigParameters: AirlineConfigParameters[] = [
      {
        id: 1,
        descriptorId: 101,
        type: 'string',
        name: AirlineConfigParamName.RfndNoticeIssue,
        value: 'true',
        readonly: false
      },
      {
        id: 2,
        descriptorId: 102,
        type: 'string',
        name: AirlineConfigParamName.RfndAplIssue,
        value: 'false',
        readonly: false
      }
    ];
    const pagedDataMock: PagedData<RefundAuthorityViewModel> = {
      records: [],
      pageSize: 0,
      total: 0,
      totalPages: 0,
      pageNumber: 1
    };
    const mockConfig: RefundAirlineConfiguration = {
      id: 1,
      descriptorId: 1,
      scopeType: '',
      scopeId: '',
      service: '',
      parameters: mockAirlineConfigParameters
    };

    const mockChangeFilter = {
      data: pagedDataMock,
      config: mockConfig
    };
    const action = RefundAuthorityListActions.changeRequiredFilterSuccess(mockChangeFilter);
    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.config).toEqual(mockChangeFilter.config);
  });

  it('should modify state data when modify action is triggered with the given items', () => {
    const action = RefundAuthorityListActions.modify({ items: mockRefundAuthorities });
    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.data).toEqual(mockRefundAuthorities);
  });

  it('should check if the agent and the airline have the same id when haveSameId method is triggered', () => {
    const mockRefundAuthorityOne: RefundAuthority = {
      agent: { id: 1, iataCode: 'ES', name: 'Agent 1' },
      airline: { id: 101, iataCode: 'AIR', localName: 'Airline 1' },
      hasRA: true,
      hasRN: false
    };
    const result = fromList.haveSameId(mockRefundAuthorityOne, mockRefundAuthorities[0].original);

    expect(result).toBeTruthy();
  });

  it('should discard changes when discard action is triggered', () => {
    const action = RefundAuthorityListActions.discard();
    const modificatedState = {
      ...fromList.initialState,
      data: mockRefundAuthorities
    };

    const newState = fromList.reducer(modificatedState, action);
    expect(newState.data[0]['original']).toEqual(mockRefundAuthorities[0].original);
  });

  describe('on search', () => {
    it('should update query on load', () => {
      const action = RefundAuthorityListActions.search({ query: defaultQuery });
      const expected: DataQuery<RefundAuthorityFilter> = { ...defaultQuery, filterBy: {} };

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.query).toEqual(expected);
    });

    it('should set loading to true on searchConfirmed', () => {
      const action = RefundAuthorityListActions.searchConfirmed();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeTruthy();
    });

    it('should set loading to false on searchSuccess', () => {
      const action = RefundAuthorityListActions.searchSuccess({
        data: {
          records: [{} as RefundAuthorityViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        },
        config: {
          id: null,
          descriptorId: 69834849541100,
          scopeType: 'airlines',
          scopeId: '6983484954',
          service: 'refund-management',
          parameters: [
            {
              descriptorId: 69834849541101,
              id: 69834849541101,
              name: AirlineConfigParamName.RfndAplIssue,
              readonly: false,
              type: 'BOOLEAN',
              value: 'true'
            }
          ]
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set totalElements and data on searchSuccess', () => {
      const action = RefundAuthorityListActions.searchSuccess({
        data: {
          records: [{} as RefundAuthorityViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        },
        config: {
          id: null,
          descriptorId: 69834849541100,
          scopeType: 'airlines',
          scopeId: '6983484954',
          service: 'refund-management',
          parameters: [
            {
              descriptorId: 69834849541101,
              id: 69834849541101,
              name: AirlineConfigParamName.RfndAplIssue,
              readonly: false,
              type: 'BOOLEAN',
              value: 'true'
            }
          ]
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data.length).toBe(1);
      expect(newState.query.paginateBy).toEqual({ page: 0, size: 20, totalElements: 1 });
    });

    it('should set loading to false on searchError', () => {
      const action = RefundAuthorityListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set data to [] on searchError', () => {
      const action = RefundAuthorityListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data).toEqual([]);
    });

    it('should cancel the serarch and set previour query to null when searchCancelled action is triggered', () => {
      const action = RefundAuthorityListActions.searchCancelled();

      const newState = fromList.reducer(fromList.initialState, action);
      expect(newState.previousQuery).toBe(null);
    });
  });

  describe('on changeRequiredFilter', () => {
    it('should preserve query when it matches required filter', () => {
      const airline = { id: 1 } as AirlineSummary;
      const existingQuery: DataQuery<RefundAuthorityFilter> = {
        ...defaultQuery,
        filterBy: { airlines: [airline], hasRA: true }
      };
      const requiredFilter = { viewAs: ViewAs.Airline, bsp: null, airlines: [airline] };

      const state: fromList.State = {
        ...fromList.initialState,
        requiredFilter,
        query: existingQuery
      };
      const action = RefundAuthorityListActions.changeRequiredFilter({
        requiredFilter
      });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(existingQuery);
    });

    it('should reset to default query when the query does not match required filter', () => {
      const airline = { id: 1 } as AirlineSummary;
      const otherAirline = { id: 2 } as AirlineSummary;
      const existingQuery: DataQuery<RefundAuthorityFilter> = {
        ...defaultQuery,
        filterBy: { airlines: [airline], hasRA: true }
      };
      const requiredFilter: RefundAuthorityFilter = { airlines: [otherAirline], viewAs: ViewAs.Airline, bsp: null };
      const expectedQuery: DataQuery<RefundAuthorityFilter> = {
        ...defaultQuery,
        filterBy: requiredFilter
      };
      const state: fromList.State = {
        ...fromList.initialState,
        query: existingQuery
      };
      const action = RefundAuthorityListActions.changeRequiredFilter({ requiredFilter });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(expectedQuery);
    });
  });

  describe('hasChanges', () => {
    it('should return true when hasRA was changed', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: [
          {
            airline: null,
            agent: null,
            hasRA: true,
            hasRN: true,
            original: { airline: null, agent: null, hasRA: false, hasRN: false }
          }
        ]
      };

      expect(fromList.hasChanges(state)).toBeTruthy();
    });

    it('should return false when hasRA was not changed', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: [
          {
            airline: null,
            agent: null,
            hasRA: false,
            hasRN: true,
            original: { airline: null, agent: null, hasRA: false, hasRN: true }
          }
        ]
      };

      expect(fromList.hasChanges(state)).toBeFalsy();
    });
  });
});

describe('selectors', () => {
  it('should get loading from the state', () => {
    const result = fromList.getLoading(fromList.initialState);

    expect(result).toBeFalsy();
  });

  it('should get data from the state', () => {
    const result = fromList.getData(fromList.initialState);

    expect(result).toEqual(fromList.initialState.data);
  });

  it('should get query from the state', () => {
    const result = fromList.getQuery(fromList.initialState);

    expect(result).toEqual(fromList.initialState.query);
  });

  it('should get Required Filter from the state', () => {
    const result = fromList.getRequiredFilter(fromList.initialState);

    expect(result).toEqual(fromList.initialState.requiredFilter);
  });

  it('should get Configuration from the state', () => {
    const result = fromList.getConfig(fromList.initialState);

    expect(result).toEqual(fromList.initialState.config);
  });

  it('should apply changes and get Configuration from the state', () => {
    const mockAirlineConfigParameters: AirlineConfigParameters[] = [
      {
        id: 1,
        descriptorId: 101,
        type: 'string',
        name: AirlineConfigParamName.RfndNoticeIssue,
        value: 'true',
        readonly: false
      },
      {
        id: 2,
        descriptorId: 102,
        type: 'string',
        name: AirlineConfigParamName.RfndAplIssue,
        value: 'false',
        readonly: false
      }
    ];
    const mockConfig: RefundAirlineConfiguration = {
      id: 1,
      descriptorId: 1,
      scopeType: '',
      scopeId: '',
      service: '',
      parameters: mockAirlineConfigParameters
    };
    const modificatedState = {
      ...fromList.initialState,
      config: mockConfig
    };

    const result = fromList.hasApplyChanges(modificatedState);

    expect(result).toBeTruthy();
  });
});
