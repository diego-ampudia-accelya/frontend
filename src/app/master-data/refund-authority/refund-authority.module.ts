import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { RefundAuthorityComponent } from './refund-authority/refund-authority.component';
import { RefundAuthorityService } from './services/refund-authority.service';
import { RefundAuthorityEffects } from './store/effects/refund-authority.effects';
import * as fromRefundAuthority from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [RefundAuthorityComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromRefundAuthority.refundAuthorityFeatureKey, fromRefundAuthority.reducers),
    EffectsModule.forFeature([RefundAuthorityEffects])
  ],
  exports: [RefundAuthorityComponent],
  providers: [RefundAuthorityService, ChangesDialogService]
})
export class RefundAuthorityModule {}
