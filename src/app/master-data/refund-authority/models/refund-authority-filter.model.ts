import { ViewAs } from './view-as.model';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

export interface RefundAuthorityFilter {
  viewAs: ViewAs;
  bsp: Bsp;
  airlines?: AirlineSummary[];
  agents?: AgentSummary[];
  designators?: AirlineSummary[];
  hasRA?: boolean;
  hasRN?: boolean;
  permission?: string;
}
