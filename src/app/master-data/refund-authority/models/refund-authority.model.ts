export interface RefundAuthority {
  agent: { id: number; iataCode: string; name: string };
  airline: { id: number; iataCode: string; localName: string; designator?: string };
  hasRA: boolean;
  hasRN: boolean;
}

export interface RefundAuthorityViewModel extends RefundAuthority {
  original: RefundAuthority;
}
