import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of, throwError } from 'rxjs';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AgentSummary, AirlineSummary, DownloadFormat } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';
import { AirlineDictionaryService } from '~app/shared/services/dictionary/airline-dictionary.service';
import { RefundAuthority } from '../models/refund-authority.model';
import { RefundAuthorityService } from './refund-authority.service';

describe('RefundAuthorityService', () => {
  let service: RefundAuthorityService;
  const agentServiceSpy = createSpyObject(AgentDictionaryService);
  const airlineServiceSpy = createSpyObject(AirlineDictionaryService);
  const httpClientSpy = createSpyObject(HttpClient);
  const appConfigurationSpy = createSpyObject(AppConfigurationService);

  const mockRefundAuthorities: RefundAuthority[] = [
    {
      agent: { id: 1, iataCode: 'ES', name: 'Agent 1' },
      airline: { id: 101, iataCode: 'AIR', localName: 'Airline 1' },
      hasRA: true,
      hasRN: false
    },
    {
      agent: { id: 2, iataCode: 'ES', name: 'Agent 2' },
      airline: { id: 102, iataCode: 'AIR', localName: 'Airline 2', designator: 'A2' },
      hasRA: false,
      hasRN: true
    }
  ];

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        RefundAuthorityService,
        { provide: AgentDictionaryService, useValue: agentServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineServiceSpy },
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(RefundAuthorityService);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should convert to Dto Refund Authority items when convertToDto is triggered', () => {
    const resultConverted = service['convertToDto'](mockRefundAuthorities);

    expect(resultConverted).toEqual([
      { agentId: 1, airlineId: 101, hasRA: true, hasRN: false },
      { agentId: 2, airlineId: 102, hasRA: false, hasRN: true }
    ]);
  });

  it('should format Query when formatQuery is triggered with a given query', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const mockAirlines = [
      {
        id: 1,
        name: 'Airline 1',
        code: 'Code 1',
        designator: 'Designator 1'
      }
    ];
    const mockAgents = [
      {
        id: 1,
        name: 'Agent',
        code: '1'
      }
    ];
    const mockBsp = {
      id: 1,
      name: 'SPAIN',
      isoCountryCode: 'ES'
    };
    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        airlines: mockAirlines,
        designators: mockAirlines,
        agents: mockAgents,
        bsp: mockBsp,
        ...queryCloneDeep.filterBy
      }
    };
    const result = service['formatQuery'](dataQuery);

    expect(result.filterBy['bspCode']).toEqual(mockBsp.isoCountryCode);
  });

  describe('Httpclient request methods', () => {
    it('should trigger get method from httpClient when getRefundAirlineConfiguration method is triggered', fakeAsync(() => {
      const mockId = 1;

      service.getRefundAirlineConfiguration(mockId).subscribe(() => {
        expect(httpClientSpy.get).toHaveBeenCalled();
      });

      tick();
    }));

    it('should send a download request with CSV format when download method is triggered', fakeAsync(() => {
      service.download(defaultQuery, DownloadFormat.CSV).subscribe();
      tick();

      expect(httpClientSpy.get).toHaveBeenCalled();
    }));

    it('should send a get request when find method is triggered', fakeAsync(() => {
      service.find(defaultQuery).subscribe();
      tick();

      expect(httpClientSpy.get).toHaveBeenCalled();
    }));

    it('should send a put request when save method is triggered', fakeAsync(() => {
      httpClientSpy.put.and.returnValue(of([]));
      service.save(mockRefundAuthorities).subscribe();
      tick();

      expect(httpClientSpy.put).toHaveBeenCalled();
    }));
  });

  describe('getAgents', () => {
    it('should get all agents in a bsp', waitForAsync(async () => {
      const agent1 = { name: 'Agent 1', code: '001' } as AgentSummary;
      const agent2 = { name: 'Agent 2', code: '002' } as AgentSummary;
      const agents: AgentSummary[] = [agent1, agent2];

      const bsp = { id: 1 } as Bsp;
      agentServiceSpy.get.and.returnValue(of(agents));

      const result = await service.getAgents(bsp).toPromise();
      expect(result).toEqual([agent1, agent2]);
    }));

    it('should return an empty list when an error occurs', waitForAsync(async () => {
      const bsp = { id: 1 } as Bsp;
      agentServiceSpy.get.and.returnValue(throwError(new Error()));

      const result = await service.getAgents(bsp).toPromise();
      expect(result).toEqual([]);
    }));
  });

  describe('getAirline', () => {
    it('should get all airlines in a bsp', waitForAsync(async () => {
      const airline1 = {
        name: 'Airline 1',
        code: '001'
      } as AirlineSummary;
      const airline2 = {
        name: 'Airline 2',
        code: '002'
      } as AirlineSummary;
      const agents: AirlineSummary[] = [airline1, airline2];

      const bsp = { id: 1 } as Bsp;
      airlineServiceSpy.get.and.returnValue(of(agents));

      const result = await service.getAirlines(bsp).toPromise();
      expect(result).toEqual([airline1, airline2]);
    }));

    it('should return an empty list when an error occurs', waitForAsync(async () => {
      const bsp = { id: 1 } as Bsp;
      airlineServiceSpy.get.and.returnValue(throwError(new Error()));

      const result = await service.getAirlines(bsp).toPromise();
      expect(result).toEqual([]);
    }));
  });
});
