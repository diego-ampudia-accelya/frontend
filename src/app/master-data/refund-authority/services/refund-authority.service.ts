import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { RefundAuthority, RefundAuthorityFilter } from '../models';
import { BulkUpdateResponse } from '~app/master-data/models/bulk-update-response.model';
import { RefundAirlineConfiguration } from '~app/refund/models/refund.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { ServerError } from '~app/shared/errors';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { AgentSummary, AirlineSummary, DownloadFormat } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services/dictionary';

@Injectable()
export class RefundAuthorityService implements Queryable<RefundAuthority> {
  private baseUrl: string;
  private sortMapper = [
    {
      from: 'agent.name',
      to: 'agentName'
    },
    {
      from: 'agent.iataCode',
      to: 'agentCode'
    }
  ];

  constructor(
    private http: HttpClient,
    private agentService: AgentDictionaryService,
    private airlineService: AirlineDictionaryService,
    private appConfiguration: AppConfigurationService
  ) {
    this.baseUrl = `${this.appConfiguration.baseApiPath}/agent-management/agents/refund-issuing-authority`;
  }

  public find(query: DataQuery<RefundAuthorityFilter>): Observable<PagedData<RefundAuthority>> {
    const requestQuery = this.formatQuery(query);
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http.get<PagedData<RefundAuthority>>(url);
  }

  public save(items: RefundAuthority[]): Observable<BulkUpdateResponse<RefundAuthority>[]> {
    const dto = this.convertToDto(items);

    return this.http.put<BulkUpdateResponse<RefundAuthority>[]>(this.baseUrl, dto).pipe(
      map(response => {
        const errorResponse = response.find(res => !!res.error);
        if (errorResponse) {
          throw new ServerError(errorResponse.error);
        }

        return response;
      })
    );
  }

  public download(
    query: DataQuery<RefundAuthority>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, downloadType: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public getAgents(bsp: Bsp, permissions?: string): Observable<AgentSummary[]> {
    const bspFilter = { bspId: bsp.id };
    const filter = permissions ? { ...bspFilter, permission: permissions } : bspFilter;

    return this.agentService.get(filter).pipe(catchError(() => of([])));
  }

  public getAirlines(bsp: Bsp): Observable<AirlineSummary[]> {
    return this.airlineService.get(bsp.id).pipe(catchError(() => of([])));
  }

  public getRefundAirlineConfiguration(id: number): Observable<RefundAirlineConfiguration> {
    const url = `${this.appConfiguration.baseApiPath}/refund-management/airlines/${id}/configuration`;

    return this.http.get<RefundAirlineConfiguration>(url);
  }

  private formatQuery(query: DataQuery<RefundAuthorityFilter>): RequestQuery {
    const { airlines, agents, designators, viewAs, bsp, ...rest } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...rest,
        airlineCode: airlines && airlines.map(item => item.code),
        airlineDesignator: designators && designators.map(item => item.designator),
        agentCode: agents && agents.map(item => item.code),
        bspCode: bsp && bsp?.isoCountryCode
      },
      sortBy: query.sortBy && formatSortBy(query.sortBy, this.sortMapper)
    });
  }

  private convertToDto(items: RefundAuthority[]) {
    return items.map(item => ({
      agentId: item.agent.id,
      airlineId: item.airline.id,
      hasRA: item.hasRA,
      hasRN: item.hasRN
    }));
  }
}
