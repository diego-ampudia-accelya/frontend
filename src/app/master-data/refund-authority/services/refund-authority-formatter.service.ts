import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { memoize } from 'lodash';

import { RefundAuthorityFilter } from '../models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class RefundAuthorityFormatter implements FilterFormatter {
  public ignoredFilters: Array<keyof RefundAuthorityFilter> = [];

  constructor(private translation: L10nTranslationService) {
    this.formatStatus = memoize(this.formatStatus).bind(this);
  }

  public format(filter: RefundAuthorityFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<RefundAuthorityFilter>> = {
      bsp: bsp => `${this.translate('filters.label.bsp')} - ${bsp.isoCountryCode}`,
      airlines: airlines => airlines.map(airline => `${airline.code} / ${airline.name}`).join(', '),
      designators: designators =>
        `${this.translate('filters.label.designator')} - ${designators.map(airline => airline.designator).join(', ')}`,
      agents: agents => agents.map(agent => `${agent.code} / ${agent.name}`).join(', '),
      hasRA: status => `${this.translate('raAuthorityStatus')} - ${this.formatStatus(status)}`,
      hasRN: status => `${this.translate('rnAuthorityStatus')} - ${this.formatStatus(status)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public formatStatus(status: boolean): string {
    return this.translate(status ? 'enabled' : 'disabled');
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.MASTER_DATA.refundAuthority.' + key);
  }
}
