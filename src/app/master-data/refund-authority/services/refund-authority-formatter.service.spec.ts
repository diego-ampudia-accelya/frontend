import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { RefundAuthorityFilter, ViewAs } from '../models';

import { RefundAuthorityFormatter } from './refund-authority-formatter.service';
import { AgentSummary, AirlineSummary } from '~app/shared/models';

describe('RefundAuthorityFormatterService', () => {
  let formatter: RefundAuthorityFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);
  const requiredFilter: RefundAuthorityFilter = { viewAs: ViewAs.Airline, bsp: null };

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new RefundAuthorityFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format when hasRN filter is defined', () => {
    const filter: RefundAuthorityFilter = {
      ...requiredFilter,
      hasRN: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['hasRN'],
        label: 'LIST.MASTER_DATA.refundAuthority.rnAuthorityStatus - LIST.MASTER_DATA.refundAuthority.enabled'
      }
    ]);
  });

  it('should format when hasRA filter is defined', () => {
    const filter: RefundAuthorityFilter = {
      ...requiredFilter,
      hasRA: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['hasRA'],
        label: 'LIST.MASTER_DATA.refundAuthority.raAuthorityStatus - LIST.MASTER_DATA.refundAuthority.enabled'
      }
    ]);
  });

  it('should format when agents are specified', () => {
    const filter: RefundAuthorityFilter = {
      ...requiredFilter,
      agents: [
        {
          code: '123',
          name: 'Agent Smith'
        },
        {
          code: '456',
          name: 'Agent Anderson'
        }
      ] as AgentSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agents'],
        label: '123 / Agent Smith, 456 / Agent Anderson'
      }
    ]);
  });

  it('should format when airlines are specified', () => {
    const filter: RefundAuthorityFilter = {
      ...requiredFilter,
      airlines: [
        {
          code: '123',
          name: 'Lufthansa'
        },
        {
          code: '456',
          name: 'WizzAir'
        }
      ] as AirlineSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airlines'],
        label: '123 / Lufthansa, 456 / WizzAir'
      }
    ]);
  });

  it('should format when designators are specified', () => {
    const filter: RefundAuthorityFilter = {
      ...requiredFilter,
      designators: [{ designator: 'LA' }, { designator: 'QA' }] as AirlineSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['designators'],
        label: 'LIST.MASTER_DATA.refundAuthority.filters.label.designator - LA, QA'
      }
    ]);
  });

  it('should omit ignored filters', () => {
    const filter: RefundAuthorityFilter = {
      ...requiredFilter,
      airlines: [
        {
          code: '123',
          name: 'Lufthansa'
        }
      ] as AirlineSummary[],
      hasRA: true,
      hasRN: true
    };

    formatter.ignoredFilters = ['airlines', 'hasRA'];

    expect(formatter.format(filter).length).toEqual(1);
  });

  it('should return empty array when the filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });

  it('should ignore null values', () => {
    const filter: RefundAuthorityFilter = {
      ...requiredFilter,
      agents: null,
      hasRA: null,
      hasRN: null
    };
    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      unknownProp: 'test'
    };
    expect(formatter.format(filter)).toEqual([]);
  });
});
