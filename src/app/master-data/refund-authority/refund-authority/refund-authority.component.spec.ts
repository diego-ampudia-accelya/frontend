import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { RefundAuthorityViewModel, ViewAs } from '../models';
import { RefundAuthorityFormatter } from '../services/refund-authority-formatter.service';
import * as fromRefundAuthority from '../store/reducers';
import * as fromList from '../store/reducers/list.reducer';
import { RefundAuthorityComponent } from './refund-authority.component';

describe('RefundAuthorityComponent', () => {
  let component: RefundAuthorityComponent;
  let fixture: ComponentFixture<RefundAuthorityComponent>;
  let displayFormatterStub: SpyObject<RefundAuthorityFormatter>;
  let changesDialogServiceStub: SpyObject<ChangesDialogService>;
  let agentDictionaryServiceStub: SpyObject<AgentDictionaryService>;
  let activatedRoute;
  let permissionsServiceStub: SpyObject<PermissionsService>;
  let mockStore: MockStore<AppState>;
  const airlineUser = createAirlineUser();
  const airline = { ...airlineUser.globalAirline, airlines: createAirlineUser() };
  const initialUser = { ...airlineUser, globalAirline: airline };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: []
  };

  const mockSelectedBsp: Bsp = {
    id: 1,
    name: 'BSP',
    isoCountryCode: 'US',
    effectiveFrom: '2023-01-01',
    effectiveTo: '2023-12-31',
    version: 1,
    active: true,
    gdsAssignments: [],
    currencyAssignments: [],
    defaultCurrencyCode: 'USD'
  };

  const mockItemRow: RefundAuthorityViewModel = {
    agent: {
      id: 1,
      iataCode: 'ES',
      name: 'Travel Agency'
    },
    airline: {
      id: 101,
      iataCode: 'AA',
      localName: 'Airlines',
      designator: 'AW'
    },
    hasRA: true,
    hasRN: false,
    original: {
      agent: {
        id: 1,
        iataCode: 'ES',
        name: 'Travel Agency'
      },
      airline: {
        id: 101,
        iataCode: 'AA',
        localName: 'Airlines',
        designator: 'AW'
      },
      hasRA: true,
      hasRN: false
    }
  };

  beforeEach(waitForAsync(() => {
    activatedRoute = {
      snapshot: {
        data: {}
      }
    };

    displayFormatterStub = createSpyObject(RefundAuthorityFormatter);
    changesDialogServiceStub = createSpyObject(ChangesDialogService);
    permissionsServiceStub = createSpyObject(PermissionsService);
    agentDictionaryServiceStub = createSpyObject(AgentDictionaryService);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [RefundAuthorityComponent, TranslatePipeMock, HasPermissionPipe],
      providers: [
        provideMockStore({
          initialState: {
            auth: {
              user: expectedUserDetails
            },

            refundAuthority: {
              list: { ...fromList.initialState }
            }
          },
          selectors: [
            { selector: fromAuth.getUser, value: initialUser },
            {
              selector: fromRefundAuthority.getListQuery,
              value: {
                filterBy: { bsp: mockSelectedBsp },
                paginateBy: {
                  page: 1,
                  pageSize: 10
                },
                sortBy: [
                  {
                    field: 'name',
                    order: 'asc'
                  }
                ]
              }
            }
          ]
        }),
        FormBuilder,
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: ChangesDialogService, useValue: changesDialogServiceStub },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: PermissionsService, useValue: permissionsServiceStub },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(RefundAuthorityComponent, {
        set: {
          providers: [{ provide: RefundAuthorityFormatter, useValue: displayFormatterStub }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    mockStore = TestBed.inject<any>(Store);
    fixture = TestBed.createComponent(RefundAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should configure columns when viewing as Airline', () => {
    setViewAs(ViewAs.Airline);
    component.ngOnInit();
    expect(component.columns.length).toBe(2);
  });

  it('should configure columns when viewing as Agent', () => {
    setViewAs(ViewAs.Agent);
    component.ngOnInit();

    expect(component.columns.length).toBe(5);
  });

  it('should configure columns when viewing as Agent Group', () => {
    setViewAs(ViewAs.AgentGroup);
    component.ngOnInit();

    expect(component.columns.length).toBe(7);
  });

  it("should set hidden to true when doesn't have access", () => {
    setViewAs(ViewAs.Agent);

    permissionsServiceStub.hasPermission.and.returnValue(false);
    component.ngOnInit();

    component.columns
      .filter(c => c.prop === 'hasRA' || c.prop === 'hasRN')
      .forEach(column => expect(column.hidden).toBe(true));
  });

  it('should set hidden to false when user has access', () => {
    setViewAs(ViewAs.Agent);

    permissionsServiceStub.hasPermission.and.returnValue(true);
    component.ngOnInit();

    component.columns
      .filter(c => c.prop === 'hasRA' || c.prop === 'hasRN')
      .forEach(column => expect(column.hidden).toBe(false));
  });

  it("should be read only, when user doesn't have update permission", () => {
    setViewAs(ViewAs.Agent);

    permissionsServiceStub.hasPermission.and.returnValue(false);
    component.ngOnInit();

    component.columns
      .filter(c => c.prop === 'hasRA' || c.prop === 'hasRN')
      .forEach(column => {
        expect(column.pipe.transform).toBeDefined();
      });
  });

  it('should be editable, when user has update permission', () => {
    setViewAs(ViewAs.Agent);

    permissionsServiceStub.hasPermission.and.returnValue(true);
    component.ngOnInit();

    component.columns
      .filter(c => c.prop === 'hasRA' || c.prop === 'hasRN')
      .forEach(column => {
        expect(column.cellTemplate).toBe('defaultLabeledCheckboxCellTmpl');
        expect(column.headerTemplate).toBe('checkboxHeaderTmpl');
      });
  });

  it('should ignore airlines filter when viewing as Airline', () => {
    setViewAs(ViewAs.Airline);
    component.ngOnInit();

    expect(component.isAirlineView).toBe(true);
    expect(component.displayFormatter.ignoredFilters).toEqual(['airlines', 'bsp']);
  });

  it('should ignore agents filter when viewing as Agent', () => {
    setViewAs(ViewAs.Agent);
    component.ngOnInit();

    expect(component.isAgentView).toBe(true);
    expect(component.displayFormatter.ignoredFilters).toEqual(['agents', 'bsp']);
  });

  it('should not ignore filters when viewing as Agent Group', () => {
    setViewAs(ViewAs.AgentGroup);
    component.ngOnInit();

    expect(component.isAgentGroupView).toBe(true);
    expect(component.displayFormatter.ignoredFilters).toEqual(['bsp']);
  });

  it('should return an object with a checkbox control and a boolean when isChanged is triggered', () => {
    const mockGridColumn: GridColumn = {};
    const result = component['isChanged']({ row: mockItemRow, column: mockGridColumn });
    expect(result).toEqual({
      'checkbox-control-changed': false,
      'checkbox-control': true
    });
  });

  it('should initialize lean query and dispatch changeRequiredFilter action when initializeLeanQuery method is triggered', fakeAsync(() => {
    component['selectedBsp'] = mockSelectedBsp;
    spyOn(mockStore, 'dispatch');

    component['initializeLeanQuery']();
    tick();

    expect(component['canShowDataQuery']).toBe(true);
    expect(component['hasFilteringExpanded']).toBe(false);
    expect(component['isAgentFilterLocked']).toBe(false);
    expect(mockStore.dispatch).toHaveBeenCalled();
  }));

  it('should initialize Bsp Options and patch bsp value when initializeBspOptions method is triggered', () => {
    const mockLoggedUser: User = {
      userType: UserType.IATA,
      email: 'iata@mail.com',
      firstName: 'Iata Name',
      lastName: 'Iata Last Name'
    };
    component['loggedUser'] = mockLoggedUser;
    spyOn<any>(component, 'filterBspsByPermission').and.returnValue([mockSelectedBsp]);
    spyOn(FormUtil, 'get').and.returnValue({
      patchValue: jasmine.createSpy()
    });

    component['initializeBspOptions']();

    expect(FormUtil.get).toHaveBeenCalled();
    expect(FormUtil.get(component.searchForm, 'bsp').patchValue).toHaveBeenCalledWith(mockSelectedBsp);
  });

  it('should set isAgentFilterLocked variable to false when enableBspDependentFilters is triggered', () => {
    component['enableBspDependentFilters']();

    expect(component.isAgentFilterLocked).toBeFalsy();
  });

  it('should check if can deactivate when canDeactivate method is triggered', fakeAsync(() => {
    changesDialogServiceStub.confirmUnsavedChanges.and.returnValue(of(true));

    component.canDeactivate().subscribe(result => {
      expect(result).toBe(true);
    });
  }));

  it('should dispatch search action when has Lean permission, Bsp value is selected and onQueryChanged is triggered', () => {
    component['selectedBsp'] = mockSelectedBsp;
    component.hasLeanPermission = true;

    spyOn(mockStore, 'dispatch');
    component.onQueryChanged(defaultQuery);

    expect(mockStore.dispatch).toHaveBeenCalled();
  });

  it('should initialize Lean flags when has Lean permission, Bsp value is not selected and onQueryChanged is triggered', () => {
    component['selectedBsp'] = null;
    component.hasLeanPermission = true;
    spyOn<any>(component, 'initializeLeanFlags');

    spyOn(mockStore, 'dispatch');
    component.onQueryChanged(defaultQuery);

    expect(component['initializeLeanFlags']).toHaveBeenCalled();
  });

  it('should initialize Lean flags when has Lean permission, Bsp value is not selected and onQueryChanged is triggered', () => {
    component.hasLeanPermission = false;
    spyOn<any>(component, 'initializeLeanFlags');

    spyOn(mockStore, 'dispatch');
    component.onQueryChanged(defaultQuery);

    expect(mockStore.dispatch).toHaveBeenCalled();
  });

  it('should dispatch download and modify actions when onModify and onDownload methods are triggering', () => {
    spyOn(mockStore, 'dispatch');

    component.onModify([mockItemRow]);
    component.onDownload();

    expect(mockStore.dispatch).toHaveBeenCalled();
  });

  it('should update Agent when there is a BSP selected and updateAgent method is triggered', () => {
    component['selectedBsp'] = mockSelectedBsp;
    component['updateAgent']();

    expect(agentDictionaryServiceStub.getDropdownOptions).toHaveBeenCalled();
  });

  it('should initialize bsp listener when value changes', () => {
    const bspControl = component.searchForm.get('bsp');
    spyOn(bspControl.valueChanges, 'pipe').and.returnValue(of('new value'));

    spyOn<any>(component, 'enableBspDependentFilters');
    spyOn<any>(component, 'updateAgent');

    component['initializeBspListener']();

    expect(component['enableBspDependentFilters']).toHaveBeenCalled();
    expect(component['updateAgent']).toHaveBeenCalled();
    expect(component['selectedBsp']).not.toBeUndefined();
    expect(component['isSearchEnabled']).toBe(true);
  });

  function setViewAs(viewAs: ViewAs): void {
    component['activatedRoute'].snapshot.data.viewAs = viewAs;
  }
});
