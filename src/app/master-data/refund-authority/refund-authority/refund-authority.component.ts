import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { isEmpty } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, first, map, skipWhile, takeUntil } from 'rxjs/operators';

import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { RefundAuthorityFilter, RefundAuthorityViewModel, ViewAs } from '../models';
import { RefundAuthorityFormatter } from '../services/refund-authority-formatter.service';
import { RefundAuthorityListActions } from '../store/actions';
import * as fromRefundAuthority from '../store/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { AirlineConfigParameters, AirlineConfigParamName } from '~app/refund/models/refund.model';
import { FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { GridColumn } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { AirlineUser, User } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-refund-authority',
  templateUrl: './refund-authority.component.html',
  styleUrls: ['./refund-authority.component.scss'],
  providers: [RefundAuthorityFormatter]
})
export class RefundAuthorityComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  public get isAgentView(): boolean {
    return this.viewAs === ViewAs.Agent;
  }

  public get isAirlineView(): boolean {
    return this.viewAs === ViewAs.Airline;
  }

  public get isAgentGroupView(): boolean {
    return this.viewAs === ViewAs.AgentGroup;
  }

  public get viewAs(): ViewAs {
    return this.activatedRoute.snapshot.data.viewAs;
  }

  public loading$ = this.store.pipe(select(fromRefundAuthority.getListLoading));
  public data$ = this.store.pipe(select(fromRefundAuthority.getListData));
  public query$ = this.store.pipe(select(fromRefundAuthority.getListQuery));
  public hasData$ = this.store.pipe(select(fromRefundAuthority.hasData));

  public agents$ = this.store.pipe(select(fromRefundAuthority.getAgentOptions));
  public airlines$ = this.store.pipe(select(fromRefundAuthority.getAirlineOptions));
  public designators$ = this.store.pipe(select(fromRefundAuthority.getDesignatorOptions));

  public config$ = this.store.pipe(select(fromRefundAuthority.getConfig));

  public permissions = Permissions;
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public statusOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatStatus(value)
  }));
  public userBspOptions: DropdownOption<Bsp>[] = [];

  public allAgentsRnIssue: boolean;
  public allAgentsRaIssue: boolean;
  public hasLeanPermission: boolean;
  public isBspFilterLocked: boolean;

  public hasFilteringExpanded = false;
  public isSearchEnabled = false;
  public canShowDataQuery = true;
  public isAgentFilterLocked = false;
  public agentDropdownPlaceholder: string;

  private loggedUser: User;
  private selectedBsp: Bsp;
  private requiredFilters: RefundAuthorityFilter;
  public predefinedFilters: RefundAuthorityFilter;

  private destroy$ = new Subject();

  constructor(
    public displayFormatter: RefundAuthorityFormatter,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private changesDialogService: ChangesDialogService,
    private permissionsService: PermissionsService,
    private agentDictionaryService: AgentDictionaryService,
    private airlineDictionaryService: AirlineDictionaryService
  ) {}

  public ngOnInit(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.initializeColumns();
    this.searchForm = this.buildForm();
    this.displayFormatter.ignoredFilters = this.getIgnoredFilters();
    this.initializeFeaturesByLoggedUser();
  }

  private initializeFeaturesByLoggedUser(): void {
    this.store.pipe(select(getUser), first()).subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      if (this.hasLeanPermission) {
        const selectedAirline = this.loggedUser as AirlineUser;
        const airline = selectedAirline.globalAirline.airlines[0];
        const airlineFilter = [this.airlineDictionaryService.summarize(airline)];
        this.requiredFilters = {
          ...this.requiredFilters,
          ...(airlineFilter && { airlines: airlineFilter }),
          viewAs: ViewAs.Airline
        };
        this.predefinedFilters = { ...this.predefinedFilters, bsp: this.selectedBsp };
        this.initializeLeanFlags();
        this.initializeBspListener();
        this.initializeBspOptions();
        this.initializeLeanQuery();
        this.disableBspDependentFilters();
      } else {
        this.isSearchEnabled = true;
      }
    });
  }

  private initializeLeanFlags(): void {
    this.canShowDataQuery = this.isSearchEnabled;
    this.hasFilteringExpanded = !this.isSearchEnabled;
    this.isAgentFilterLocked = !this.isSearchEnabled;
  }

  private initializeLeanQuery(): void {
    this.query$.pipe(first()).subscribe(query => {
      if (!isEmpty(query?.filterBy?.bsp) || this.selectedBsp) {
        this.canShowDataQuery = true;
        this.hasFilteringExpanded = false;
        this.isAgentFilterLocked = false;
        const requiredFilter = { ...query?.filterBy, ...this.requiredFilters };
        this.store.dispatch(RefundAuthorityListActions.changeRequiredFilter({ requiredFilter }));
      }
    });
  }

  private initializeBspOptions(): void {
    this.userBspOptions = this.filterBspsByPermission()?.map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    const oneBspWithPermission = this.userBspOptions?.length === 1;

    this.isBspFilterLocked = oneBspWithPermission;

    if (oneBspWithPermission) {
      const bspControl = FormUtil.get<RefundAuthorityFilter>(this.searchForm, 'bsp');
      bspControl.patchValue(this.userBspOptions[0].value);
    }
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readRefundIssueAirline;
    if (this.loggedUser) {
      const { bsps, bspPermissions } = this.loggedUser;

      return bsps.filter(bsp =>
        bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
      );
    }
  }

  private initializeBspListener(): void {
    FormUtil.get<RefundAuthorityFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        distinctUntilChanged(),
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(selectedBspValue => {
        this.selectedBsp = selectedBspValue;
        this.isSearchEnabled = true;
        this.enableBspDependentFilters();
        this.updateAgent();
        this.requiredFilters = { ...this.requiredFilters, bsp: this.selectedBsp };
        this.predefinedFilters = { ...this.predefinedFilters, bsp: this.selectedBsp };
      });
  }

  private updateAgent(): void {
    const agentsControl = FormUtil.get<RefundAuthorityFilter>(this.searchForm, 'agents');
    agentsControl.reset();
    if (this.selectedBsp) {
      const param = { bspId: this.selectedBsp.id };
      this.agents$ = this.agentDictionaryService.getDropdownOptions(param);
    }
  }

  private enableBspDependentFilters(): void {
    this.agentDropdownPlaceholder = 'LIST.MASTER_DATA.ticketingAuthority.filters.placeholders.agent';
    this.isAgentFilterLocked = false;
  }

  private disableBspDependentFilters(): void {
    this.agentDropdownPlaceholder = 'LIST.MASTER_DATA.ticketingAuthority.filters.placeholders.disableAgent';
    this.isAgentFilterLocked = true;
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  public onQueryChanged(query: DataQuery<RefundAuthorityFilter>): void {
    const isBspSelectedValue = !isEmpty(this.selectedBsp);
    this.canShowDataQuery = true;

    if (this.hasLeanPermission) {
      if (isBspSelectedValue) {
        const requiredFilter = { ...query.filterBy, ...this.requiredFilters };
        const queryToDispatch = { ...query, filterBy: requiredFilter };
        this.store.dispatch(RefundAuthorityListActions.search({ query: queryToDispatch }));
      } else {
        this.initializeLeanFlags();
      }
    } else {
      const requiredFilter = { ...query.filterBy, ...this.requiredFilters, bsp: this.loggedUser.bsps[0] };
      const queryToDispatch = { ...query, filterBy: requiredFilter };
      this.store.dispatch(RefundAuthorityListActions.search({ query: queryToDispatch }));
    }
  }

  public onModify(items: RefundAuthorityViewModel[]): void {
    this.store.dispatch(RefundAuthorityListActions.modify({ items }));
  }

  public onDownload(): void {
    this.store.dispatch(RefundAuthorityListActions.download());
  }

  private initializeColumns(): void {
    if (this.isAirlineView) {
      this.config$.pipe(first(config => !!config)).subscribe(config => {
        const rnIssueParam: AirlineConfigParameters = config.parameters?.find(
          param => param.name === AirlineConfigParamName.RfndNoticeIssue
        );
        const raIssueParam: AirlineConfigParameters = config.parameters?.find(
          param => param.name === AirlineConfigParamName.RfndAplIssue
        );

        this.allAgentsRnIssue = rnIssueParam && rnIssueParam.value === 'true';
        this.allAgentsRaIssue = raIssueParam && raIssueParam.value === 'true';

        this.columns = this.getColumns();
      });
    } else {
      this.columns = this.getColumns();
    }
  }

  private getColumns(): Array<GridColumn & { viewAs?: ViewAs }> {
    const editableStatusColumn: Partial<GridColumn> = {
      headerTemplate: 'checkboxHeaderTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl',
      cellClass: this.isChanged.bind(this)
    };
    const readOnlyStatusColumn: Partial<GridColumn> = {
      pipe: { transform: this.displayFormatter.formatStatus }
    };

    // On other than Airline view, `allAgentsIssue` variables should be always false so we can avoid to check here the view
    const canUpdate = this.permissionsService.hasPermission(Permissions.updateRefundIssueAirline);
    const raAuthStatusColumn = canUpdate && !this.allAgentsRaIssue ? editableStatusColumn : readOnlyStatusColumn;
    const rnAuthStatusColumn = canUpdate && !this.allAgentsRnIssue ? editableStatusColumn : readOnlyStatusColumn;

    return [
      {
        name: 'LIST.MASTER_DATA.refundAuthority.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 2,
        viewAs: ViewAs.Airline
      },
      {
        name: 'LIST.MASTER_DATA.refundAuthority.columns.agentName',
        prop: 'agent.name',
        flexGrow: 8,
        viewAs: ViewAs.Airline
      },
      {
        name: 'LIST.MASTER_DATA.refundAuthority.columns.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 2,
        viewAs: ViewAs.Agent
      },
      {
        name: 'LIST.MASTER_DATA.refundAuthority.columns.airlineName',
        prop: 'airline.localName',
        flexGrow: 8,
        viewAs: ViewAs.Agent
      },
      {
        name: 'LIST.MASTER_DATA.refundAuthority.columns.designator',
        prop: 'airline.designator',
        flexGrow: 3,
        viewAs: ViewAs.Agent
      },
      {
        name: 'LIST.MASTER_DATA.refundAuthority.columns.raAuthorityStatus',
        prop: 'hasRA',
        flexGrow: 3,
        ...raAuthStatusColumn,
        hidden: !this.permissionsService.hasPermission([
          Permissions.readRAIssueAgent,
          Permissions.readRefundIssueAirline
        ])
      },
      {
        name: 'LIST.MASTER_DATA.refundAuthority.columns.rnAuthorityStatus',
        prop: 'hasRN',
        flexGrow: 3,
        ...rnAuthStatusColumn,
        hidden: !this.permissionsService.hasPermission([
          Permissions.readRNIssueAgent,
          Permissions.readRefundIssueAirline
        ])
      }
    ].filter(column => !column.viewAs || this.isAgentGroupView || column.viewAs === this.viewAs);
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      bsp: [],
      airlines: [],
      designators: [],
      agents: [],
      hasRA: [],
      hasRN: []
    });
  }

  private getIgnoredFilters(): Array<keyof RefundAuthorityFilter> {
    const isAirlines: Array<keyof RefundAuthorityFilter> = this.isAirlineView ? ['airlines'] : ['agents'];
    const ignoredFiltersByView: Array<keyof RefundAuthorityFilter> = this.isAgentGroupView ? [] : isAirlines;

    return this.hasLeanPermission ? ignoredFiltersByView : [...ignoredFiltersByView, 'bsp'];
  }

  private isChanged({ row, column }: { row: RefundAuthorityViewModel; column: GridColumn }): {
    [key: string]: boolean;
  } {
    const { prop } = column;

    return {
      'checkbox-control-changed': row.original[prop] !== row[prop],
      'checkbox-control': true
    };
  }
  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
