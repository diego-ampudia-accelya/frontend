import { TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { RefundAuthority, RefundAuthorityFilter, RefundAuthorityViewModel, ViewAs } from '../models';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { AppState } from '~app/reducers';
import { ChangesDialogConfig } from '~app/shared/components';
import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

describe('ChangesDialogConfigService', () => {
  let service: ChangesDialogConfigService;
  let translationStub: SpyObject<L10nTranslationService>;
  let mockStore: MockStore<AppState>;

  const bsp = { id: 1, name: 'TRAPPIST-1e' } as Bsp;
  const agent1 = { id: 1, name: 'Agent 1', iataCode: '1000000' };
  const agent2 = { id: 2, name: 'Agent 2', iataCode: '2000000' };
  const airline = { id: 1, localName: 'Airline 1', iataCode: '001' };
  const agentWithEnabledAuthority: RefundAuthorityViewModel = {
    agent: agent1,
    airline,
    hasRA: true,
    hasRN: true,
    original: { hasRA: false, hasRN: false } as RefundAuthority
  };
  const agentWithDisabledAuthority: RefundAuthorityViewModel = {
    agent: agent2,
    airline,
    hasRA: false,
    hasRN: false,
    original: { hasRA: true, hasRN: true } as RefundAuthority
  };
  const requiredFilter: RefundAuthorityFilter = {
    bsp,
    viewAs: ViewAs.Airline,
    airlines: [{ id: 1, name: 'Airline 1' } as AirlineSummary]
  };

  beforeEach(waitForAsync(() => {
    translationStub = createSpyObject(L10nTranslationService);
    translationStub.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      providers: [
        ChangesDialogConfigService,
        { provide: L10nTranslationService, useValue: translationStub },
        provideMockStore()
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ChangesDialogConfigService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should build configuration with formatted modifications', () => {
    const modifications: RefundAuthorityViewModel[] = [agentWithEnabledAuthority, agentWithDisabledAuthority];
    const expectedConfiguration = jasmine.objectContaining<ChangesDialogConfig>({
      changes: [
        {
          group: translations.raGroupTitle,
          name: agent1.name,
          value: translations.enabled,
          originalValue: agentWithEnabledAuthority.hasRA
        },
        {
          group: translations.raGroupTitle,
          name: agent2.name,
          value: translations.disabled,
          originalValue: agentWithDisabledAuthority.hasRA
        },
        {
          group: translations.rnGroupTitle,
          name: agent1.name,
          value: translations.enabled,
          originalValue: agentWithEnabledAuthority.hasRN
        },
        {
          group: translations.rnGroupTitle,
          name: agent2.name,
          value: translations.disabled,
          originalValue: agentWithDisabledAuthority.hasRN
        }
      ]
    });

    const configuration = service.build(modifications, requiredFilter, translations.applyChanges);
    expect(configuration).toEqual(expectedConfiguration);
  });
});
