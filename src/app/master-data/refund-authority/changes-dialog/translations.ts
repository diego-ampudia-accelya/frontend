export const translations = {
  applyChanges: {
    title: 'LIST.MASTER_DATA.refundAuthority.applyChanges.title',
    details: 'LIST.MASTER_DATA.refundAuthority.applyChanges.message'
  },
  unsavedChanges: {
    title: 'LIST.MASTER_DATA.refundAuthority.unsavedChanges.title',
    details: 'LIST.MASTER_DATA.refundAuthority.unsavedChanges.message'
  },
  raGroupTitle: 'LIST.MASTER_DATA.refundAuthority.raAuthorityStatus',
  rnGroupTitle: 'LIST.MASTER_DATA.refundAuthority.rnAuthorityStatus',
  enabled: 'LIST.MASTER_DATA.refundAuthority.enabled',
  disabled: 'LIST.MASTER_DATA.refundAuthority.disabled'
};
