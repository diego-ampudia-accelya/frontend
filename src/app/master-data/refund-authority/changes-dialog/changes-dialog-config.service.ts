import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { RefundAuthorityFilter, RefundAuthorityViewModel, ViewAs } from '../models';

import { translations } from './translations';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable({ providedIn: 'root' })
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: RefundAuthorityViewModel[],
    requiredFilter: RefundAuthorityFilter,
    question: { title: string; details: string }
  ): ChangesDialogConfig {
    const message = this.translation.translate(question.details, this.getTranslationValues(requiredFilter));

    return {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: this.formatChanges(modifications, requiredFilter.viewAs),
      message
    };
  }

  private formatChanges(modifications: RefundAuthorityViewModel[], viewAs: ViewAs = ViewAs.Airline): ChangeModel[] {
    const translateStatus = (status: boolean) =>
      this.translation.translate(status ? translations.enabled : translations.disabled);
    const getName = (item: RefundAuthorityViewModel) =>
      viewAs === ViewAs.Airline ? item.agent.name : item.airline.localName;

    const raChanges = modifications
      .filter(item => item.hasRA !== item.original.hasRA)
      .map(item => ({
        group: translations.raGroupTitle,
        name: getName(item),
        value: translateStatus(item.hasRA),
        originalValue: item.hasRA
      }));
    const rnChanges: ChangeModel[] = modifications
      .filter(item => item.hasRN !== item.original.hasRN)
      .map(item => ({
        group: this.translation.translate(translations.rnGroupTitle),
        name: getName(item),
        value: translateStatus(item.hasRN),
        originalValue: item.hasRN
      }));

    return [...raChanges, ...rnChanges];
  }

  private getTranslationValues(requiredFilter: RefundAuthorityFilter): { bspName: string; name: string } {
    const { viewAs, airlines, agents, bsp } = requiredFilter;

    return {
      bspName: bsp.name,
      name: viewAs === ViewAs.Airline ? airlines[0].name : agents[0].name
    };
  }
}
