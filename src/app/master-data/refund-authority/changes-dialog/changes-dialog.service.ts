import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { RefundAuthorityFilter, RefundAuthorityViewModel } from '../models';
import { RefundAuthorityService } from '../services/refund-authority.service';
import { RefundAuthorityListActions } from '../store/actions';
import { converter } from '../store/effects/converters';
import * as fromRefundAuthority from '../store/reducers';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: RefundAuthorityService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return of(null).pipe(
      withLatestFrom(
        this.store.pipe(select(fromRefundAuthority.getChanges)),
        this.store.pipe(select(fromRefundAuthority.getRequiredFilter))
      ),
      switchMap(([_, modifications, requiredFilter]) =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, requiredFilter, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    modifications: RefundAuthorityViewModel[],
    requiredFilter: RefundAuthorityFilter,
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, requiredFilter, question);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(modifications, dialogConfig);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(RefundAuthorityListActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(
    modifications: RefundAuthorityViewModel[],
    dialogConfig: ChangesDialogConfig
  ): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(modifications.map(converter.toDataModel)).pipe(
        tap(() => this.store.dispatch(RefundAuthorityListActions.applyChangesSuccess())),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
