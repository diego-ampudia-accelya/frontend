import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { RefundAuthorityViewModel } from '../models';
import { RefundAuthorityService } from '../services/refund-authority.service';
import { RefundAuthorityListActions } from '../store/actions';
import * as fromRefundAuthority from '../store/reducers';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { ChangesDialogService } from './changes-dialog.service';
import { translations } from './translations';
import { DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('ChangesDialogService', () => {
  let service: ChangesDialogService;
  let dialogServiceStub: SpyObject<DialogService>;
  let dataService: SpyObject<RefundAuthorityService>;
  let configService: SpyObject<ChangesDialogConfigService>;
  let mockStore: MockStore<AppState>;

  beforeEach(waitForAsync(() => {
    configService = createSpyObject(ChangesDialogConfigService);
    dialogServiceStub = createSpyObject(DialogService);
    dataService = createSpyObject(RefundAuthorityService);
    dataService.save.and.returnValue(of({}));
    dataService.find.and.returnValue(of({}));

    TestBed.configureTestingModule({
      providers: [
        ChangesDialogService,
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: ChangesDialogConfigService, useValue: configService },
        { provide: RefundAuthorityService, useValue: dataService },
        provideMockStore()
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ChangesDialogService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('confirm', () => {
    it('should return `Discard` when there are no changes', fakeAsync(() => {
      mockStore.overrideSelector(fromRefundAuthority.getRequiredFilter, null);
      mockStore.overrideSelector(fromRefundAuthority.getChanges, []);

      let dialogResult: FooterButton;
      service.confirm(translations.applyChanges).subscribe(result => (dialogResult = result));

      expect(dialogResult).toBe(FooterButton.Discard);
    }));

    it('should open confirmation dialog when there are changes', fakeAsync(() => {
      mockStore.overrideSelector(fromRefundAuthority.getRequiredFilter, null);
      mockStore.overrideSelector(fromRefundAuthority.getChanges, [{} as RefundAuthorityViewModel]);
      dialogServiceStub.open.and.returnValue(of(FooterButton.Cancel));

      service.confirm(translations.applyChanges).subscribe();
      tick();

      expect(dialogServiceStub.open).toHaveBeenCalledTimes(1);
    }));

    it('shoud dispatch a Discard action', fakeAsync(() => {
      mockStore.overrideSelector(fromRefundAuthority.getRequiredFilter, null);
      mockStore.overrideSelector(fromRefundAuthority.getChanges, [{} as RefundAuthorityViewModel]);
      dialogServiceStub.open.and.returnValue(of({ clickedBtn: FooterButton.Discard }));

      let dialogResult: FooterButton;
      service.confirm(translations.applyChanges).subscribe(result => (dialogResult = result));

      expect(dialogResult).toBe(FooterButton.Discard);
      expect(mockStore.dispatch).toHaveBeenCalledWith(RefundAuthorityListActions.discard());
    }));

    it('should dispatch an applySuccess action when user has chosen to apply the changes', fakeAsync(() => {
      mockStore.overrideSelector(fromRefundAuthority.getRequiredFilter, null);
      mockStore.overrideSelector(fromRefundAuthority.getChanges, [{} as RefundAuthorityViewModel]);
      dialogServiceStub.open.and.returnValue(of({ clickedBtn: FooterButton.Apply }));
      dataService.save.and.returnValue(of(null));
      configService.build.and.returnValue({ data: { buttons: [] } });

      let dialogResult: FooterButton;
      service.confirm(translations.applyChanges).subscribe(result => (dialogResult = result));
      tick();

      expect(dialogResult).toBe(FooterButton.Apply);
      expect(mockStore.dispatch).toHaveBeenCalledWith(RefundAuthorityListActions.applyChangesSuccess());
    }));
  });
});
