import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { AddressMaintenanceComponent } from '../address-maintenance';
import { IpAddressSettingsComponent } from '../configuration/ip-address-settings/ip-address-settings.component';
import { baseDisputeReasonsConfig, baseIssueReasonConfig } from '../configuration/reason.config';
import { TipGlobalAirlineResolver } from '../configuration/tip/tip-global-airline/services/tip-global-airline.resolver';
import { TipLocalAirlineComponent } from '../configuration/tip/tip-local-airline';
import { TipReportsComponent } from '../configuration/tip/tip-reports/tip-reports.component';
import { PublicApiAccountsComponent } from '../public-api/components/public-api-accounts/public-api-accounts.component';
import { SftpConfigurationsResolver } from '../sftp/services/sftp-configurations-resolver.service';
import { SftpAccountsComponent } from '../sftp/sftp-accounts/sftp-accounts.component';
import { TicketingAuthorityComponent, ViewAs } from '../ticketing-authority';
import { VariableRemittanceComponent } from '../variable-remittance/variable-remittance/variable-remittance.component';

import { EmailAlertsSettingsComponent } from '../configuration/email-alert-settings/email-alert-settings.component';
import { AirlineListComponent } from './components/airline-list/airline-list.component';
import { AirlineProfileComponent } from './components/airline-profile/airline-profile.component';
import { AirlinePropertiesComponent } from './components/airline-properties/airline-properties.component';
import { AirlineAdmPolicySettingsComponent } from './configuration/airline-adm-policy-settings/airline-adm-policy-settings.component';
import { DisputeReasonService } from './configuration/api/dispute-reason.service';
import { IssueReasonService } from './configuration/api/issue-reason.service';
import {
  BasicSettingsResolver,
  MyAirlineProfileResolver,
  ReasonsResolver,
  RefundAuthorityRequiredFilterResolver
} from './services';
import { AirlineAddressMaintenanceResolver } from './services/airline-address-maintenance.resolver';
import { AirlineAdmPoliciesResolver } from './services/airline-adm-policies.resolver';
import { AirlineConfigurationResolver } from './services/airline-configuration.resolver';
import { AirlineIpAddressSettingsResolver } from './services/airline-ip-address-settings.resolver';
import { AirlineProfileResolver } from './services/airline-profile.resolver';
import { AirlinePropertiesResolver } from './services/airline-properties.resolver';
import { EmailAlertsSettingResolver } from './services/email-alerts-setting.resolver';
import { NotificationSettingsResolver } from './services/notification-settings.resolver';
import { PbdSettingResolver } from './services/pbd-setting.resolver';
import { RefundSettingResolver } from './services/refund-setting.resolver';
import { TaSettingResolver } from './services/ta-setting.resolver';
import { TicketingAuthorityResolver } from './services/ticketing-authority.resolver';
import { TipLocalAirlineSettingResolver } from './services/tip-local-airline-setting.resolver';
import { VariableRemittanceResolver } from './services/variable-remittance-setting.resolver';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';
import { RefundAuthorityComponent } from '~app/master-data/refund-authority';
import { TipGlobalAirlineComponent } from '~app/master-data/configuration/tip/tip-global-airline/tip-global-airline.component';
import {
  ConfigurationComponent,
  DescriptionViewComponent,
  SettingsViewComponent,
  UnsavedChangesGuard
} from '~app/master-data/configuration';
import { AdmAcmAuthorityComponent } from '~app/master-data/adm-acm-authority';
import { CanComponentDeactivateGuard } from '~app/core/services';
import { admPoliciesPermissions } from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';

const propertiesRoute: Route = {
  path: 'properties',
  component: AirlinePropertiesComponent,
  data: {
    title: 'menu.masterData.airlineProfile.properties.title'
  },
  resolve: { airline: AirlinePropertiesResolver }
};

const defaultRoute = { path: '', redirectTo: 'properties', pathMatch: 'full' };

const configurationRoute: Route = {
  path: 'configuration',
  component: ConfigurationComponent,
  data: {
    title: 'menu.masterData.configuration.title',
    requiredPermissions: [Permissions.readAirlineConfiguration]
  },
  resolve: {
    loadSettings: AirlineConfigurationResolver
  },
  children: [
    // Notifications
    {
      path: 'notifications',
      component: SettingsViewComponent,
      resolve: {
        loadSettings: NotificationSettingsResolver
      },

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.notificationsGroupName',
        title: 'menu.masterData.configuration.notificationsSettings',
        configuration: {
          title: 'menu.masterData.configuration.notificationsSettingsTitle',
          permissions: {
            read: Permissions.readNotificationSettings,
            update: Permissions.updateNotificationSettings
          }
        },
        requiredPermissions: [Permissions.readNotificationSettings]
      },
      runGuardsAndResolvers: 'always'
    },
    // ACDM Configuration
    {
      path: 'basic-settings',
      component: SettingsViewComponent,
      resolve: {
        loadSettings: BasicSettingsResolver
      },

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.acdmSettingsGroupName',
        title: 'menu.masterData.configuration.basicSettings',
        configuration: {
          title: 'LIST.MASTER_DATA.airline.basicSettings.title'
        },
        requiredPermissions: [Permissions.readAirlineSettings]
      },
      runGuardsAndResolvers: 'always'
    },
    {
      path: 'issue-reasons',
      component: DescriptionViewComponent,
      canDeactivate: [CanComponentDeactivateGuard],
      resolve: {
        parentEntity: ReasonsResolver
      },

      data: {
        group: 'menu.masterData.configuration.acdmSettingsGroupName',
        title: 'menu.masterData.configuration.issueReasons',
        configuration: {
          ...baseIssueReasonConfig,
          description: { ...baseIssueReasonConfig.description, canValidate: true },
          permissions: {
            read: Permissions.readIssueReason,
            create: Permissions.createIssueReason,
            update: Permissions.updateIssueReason,
            delete: Permissions.deleteIssueReason
          },
          apiService: IssueReasonService
        },
        requiredPermissions: Permissions.readIssueReason
      },
      runGuardsAndResolvers: 'always'
    },
    {
      path: 'dispute-reasons',
      component: DescriptionViewComponent,
      canDeactivate: [CanComponentDeactivateGuard],
      resolve: {
        parentEntity: ReasonsResolver
      },

      data: {
        group: 'menu.masterData.configuration.acdmSettingsGroupName',
        title: 'menu.masterData.configuration.disputeReasons',
        configuration: {
          ...baseDisputeReasonsConfig,
          description: { ...baseDisputeReasonsConfig.description, canValidate: true },
          permissions: {
            read: Permissions.readDisputeReason,
            create: Permissions.createDisputeReason,
            update: Permissions.updateDisputeReason,
            delete: Permissions.deleteDisputeReason
          },
          apiService: DisputeReasonService
        },
        requiredPermissions: Permissions.readDisputeReason
      },
      runGuardsAndResolvers: 'always'
    },
    {
      path: 'adm-policies',
      component: AirlineAdmPolicySettingsComponent,
      resolve: {
        loadPolicies: AirlineAdmPoliciesResolver
      },
      data: {
        group: 'menu.masterData.configuration.acdmSettingsGroupName',
        title: 'menu.masterData.configuration.admPolicies.sectionTitle',
        configuration: {
          title: 'LIST.MASTER_DATA.airline.basicSettings.admPolicies.title',
          permissions: {
            read: admPoliciesPermissions.readAdmPolicies,
            create: admPoliciesPermissions.createAdmPolicies,
            update: admPoliciesPermissions.updateAdmPolicies,
            delete: admPoliciesPermissions.deleteAdmPolicies
          }
        },
        requiredPermissions: admPoliciesPermissions.readAdmPolicies
      },
      runGuardsAndResolvers: 'always'
    },

    // PBD Configuration
    {
      path: 'pbd-settings',
      resolve: {
        loadSettings: PbdSettingResolver
      },
      component: SettingsViewComponent,

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.pbdSettingsAirl.groupName',
        title: 'menu.masterData.configuration.pbdSettingsAirl.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.pbdSettingsAirl.sectionTitle',
          permissions: {
            read: Permissions.readPBDAirline,
            update: Permissions.updatePBDAirline
          }
        },
        requiredPermissions: Permissions.readPBDAirline
      },
      runGuardsAndResolvers: 'always'
    },

    // Refund Configuration
    {
      path: 'refund-settings',
      resolve: {
        loadSettings: RefundSettingResolver
      },
      component: SettingsViewComponent,

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.refundSettings.groupName',
        title: 'menu.masterData.configuration.refundSettings.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.refundSettings.sectionTitle'
        },
        requiredPermissions: Permissions.readAirlineSettings
      },
      runGuardsAndResolvers: 'always'
    },

    // TA Configuration
    {
      path: 'ta-settings',
      resolve: {
        loadSettings: TaSettingResolver
      },
      component: SettingsViewComponent,

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.taSettings.groupName',
        title: 'menu.masterData.configuration.taSettings.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.taSettings.sectionTitle'
        },
        requiredPermissions: Permissions.readAirlineSettings
      },
      runGuardsAndResolvers: 'always'
    },

    // SFTP Configuration
    {
      path: 'sftp-settings',
      resolve: {
        loadSettings: SftpConfigurationsResolver
      },
      component: SftpAccountsComponent,

      canDeactivate: [],
      data: {
        group: 'menu.masterData.configuration.sftpConfigurations.groupName',
        title: 'menu.masterData.configuration.sftpConfigurations.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.sftpConfigurations.sectionTitle'
        },
        requiredPermissions: [
          Permissions.readSftpAccount,
          Permissions.readAdditionalUploadSftpAccount,
          Permissions.readMassDownloadSftpAccount
        ]
      },
      runGuardsAndResolvers: 'always'
    },

    // Public API Configuration
    {
      path: 'public-api-settings',
      component: PublicApiAccountsComponent,
      data: {
        group: 'menu.masterData.configuration.publicApi.groupName',
        title: 'menu.masterData.configuration.publicApi.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.publicApi.sectionTitle'
        },
        requiredPermissions: Permissions.readPublicApiAccounts
      }
    },

    // Email Alert Configuration
    {
      path: 'email-alerts-settings',
      resolve: {
        loadSettings: EmailAlertsSettingResolver
      },
      component: EmailAlertsSettingsComponent,

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.emailAlerts.groupName',
        title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.emailAlerts.sectionTitle'
        },
        requiredPermissions: Permissions.readEmailAlerts
      },
      runGuardsAndResolvers: 'always'
    },

    // Variable Remittance Configuration
    {
      path: 'var-rem-settings',
      resolve: {
        loadSettings: VariableRemittanceResolver
      },
      component: SettingsViewComponent,

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.varRemSettings.groupName',
        title: 'menu.masterData.configuration.varRemSettings.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.varRemSettings.sectionTitle'
        },
        requiredPermissions: Permissions.readVariableRemittanceSettings
      },
      runGuardsAndResolvers: 'always'
    },

    // TIP Configuration
    {
      path: 'tip-global-airline',
      resolve: {
        loadSettings: TipGlobalAirlineResolver
      },
      component: TipGlobalAirlineComponent,

      canDeactivate: [CanComponentDeactivateGuard],
      data: {
        group: 'menu.masterData.configuration.tipSettings.groupName',
        title: 'menu.masterData.configuration.tipSettings.tipGlobal.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.tipSettings.tipGlobal.sectionTitle'
        },
        requiredPermissions: Permissions.readGlobalAirlineConfiguration
      }
    },
    {
      path: 'tip-local-settings',
      component: TipLocalAirlineComponent,
      resolve: {
        loadTips: TipLocalAirlineSettingResolver
      },

      canDeactivate: [CanComponentDeactivateGuard],
      data: {
        group: 'menu.masterData.configuration.tipSettings.groupName',
        title: 'menu.masterData.configuration.tipSettings.tipLocal.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.tipSettings.tipLocal.sectionTitle'
        },
        requiredPermissions: Permissions.readLocalTipConfiguration
      }
    },
    {
      path: 'tip-reports',
      component: TipReportsComponent,

      canDeactivate: [CanComponentDeactivateGuard],
      data: {
        group: 'menu.masterData.configuration.tipSettings.groupName',
        title: 'menu.masterData.configuration.tipSettings.tipReports.sectionTitle',
        configuration: {
          title: 'menu.masterData.configuration.tipSettings.tipReports.sectionTitle'
        },
        requiredPermissions: Permissions.readTipReport
      }
    },

    // IP Address
    {
      path: 'ip-address-settings',
      component: IpAddressSettingsComponent,
      resolve: {
        loadIpSettings: AirlineIpAddressSettingsResolver
      },
      canDeactivate: [CanComponentDeactivateGuard],
      data: {
        group: 'menu.masterData.configuration.ipAddressGroupName',
        title: 'menu.masterData.configuration.ipAddressSettingsTitle',
        configuration: {
          title: 'menu.masterData.configuration.ipAddressSettingsTitle'
        },
        requiredPermissions: [Permissions.readIpAddressSettings]
      },
      runGuardsAndResolvers: 'always'
    }
  ]
};

const ticketingAuthorityRoute: Route = {
  path: 'ticketing-authority',
  component: TicketingAuthorityComponent,
  resolve: {
    requiredFilter: TicketingAuthorityResolver
  },
  canDeactivate: [CanComponentDeactivateGuard],
  data: {
    title: 'menu.masterData.ticketingAuthority.title',
    viewAs: ViewAs.Airline,
    requiredPermissions: Permissions.readTicketingAuthority
  }
};

const refundAuthorityRoute: Route = {
  path: 'refund-authority',
  component: RefundAuthorityComponent,
  canDeactivate: [CanComponentDeactivateGuard],
  resolve: {
    requiredFilter: RefundAuthorityRequiredFilterResolver
  },
  data: {
    title: 'LIST.MASTER_DATA.refundAuthority.tabLabel',
    viewAs: ViewAs.Airline,
    requiredPermissions: [
      Permissions.readRefundIssueAirline,
      Permissions.readRAIssueAgent,
      Permissions.readRNIssueAgent
    ]
  }
};

const acdmAuthorityRoute: Route = {
  path: 'adm-acm-authority',
  component: AdmAcmAuthorityComponent,
  canDeactivate: [CanComponentDeactivateGuard],
  data: {
    title: 'LIST.MASTER_DATA.admAcmAuthority.tabLabel',
    viewAs: ViewAs.Airline,
    requiredPermissions: [Permissions.readACDMAuthorityAirline]
  }
};

const variableRemittanceRoute: Route = {
  path: 'variable-remittance',
  component: VariableRemittanceComponent,
  canDeactivate: [CanComponentDeactivateGuard],
  data: {
    title: 'LIST.MASTER_DATA.variableRemittance.tabLabel',
    viewAs: ViewAs.Airline,
    requiredPermissions: [Permissions.readRemittanceFrequency, Permissions.updateRemittanceFrequency]
  }
};

const contactInfoRoute: Route = {
  path: 'contact-info',
  component: AddressMaintenanceComponent,
  canDeactivate: [CanComponentDeactivateGuard],
  resolve: {
    loadAddress: AirlineAddressMaintenanceResolver
  },
  data: {
    title: 'LIST.MASTER_DATA.addressMaintenance.tabLabel',
    requiredPermissions: [Permissions.readAirlineAddressMaintenance]
  }
};

const routes: Routes = [
  {
    path: '',
    component: AirlineListComponent,
    data: {
      tab: ROUTES.AIRLINE
    }
  },
  { path: 'my', redirectTo: 'my/', pathMatch: 'full' },
  {
    path: 'my/:my-airline-id',
    component: AirlineProfileComponent,
    data: {
      tab: ROUTES.MY_AIRLINE
    },
    resolve: {
      airline: MyAirlineProfileResolver
    },
    children: [
      propertiesRoute,
      configurationRoute,
      ticketingAuthorityRoute,
      refundAuthorityRoute,
      acdmAuthorityRoute,
      variableRemittanceRoute,
      contactInfoRoute,
      defaultRoute
    ]
  },
  {
    path: ':airline-id',
    component: AirlineProfileComponent,
    data: {
      tab: ROUTES.AIRLINE_PROFILE
    },
    resolve: {
      // Load airline into activated route for compatibility with AirlineTAComponent
      airline: AirlineProfileResolver
    },
    children: [propertiesRoute, contactInfoRoute, ticketingAuthorityRoute]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AirlineRoutingModule {}
