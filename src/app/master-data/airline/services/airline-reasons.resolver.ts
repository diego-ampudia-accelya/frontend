import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { getConfigurationAirline, getSelectedAirline } from '../reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Airline } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';

@Injectable({
  providedIn: 'root'
})
export class ReasonsResolver implements Resolve<Airline> {
  constructor(private store: Store<AppState>, private permissionsService: PermissionsService) {}

  public resolve(): Observable<Airline> {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    return hasLeanPermission
      ? this.store.pipe(select(getConfigurationAirline), first())
      : this.store.pipe(select(getSelectedAirline), first());
  }
}
