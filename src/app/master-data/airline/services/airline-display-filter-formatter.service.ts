import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { bspFilterTagMapper, dateFilterTagMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

@Injectable()
export class AirlinesDisplayFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translationService: L10nTranslationService) {
    super();
  }

  public format(filter: any): AppliedFilter[] {
    const { bsp, airlines, active, registerDate, expiryDate, easyPayEffectiveFrom, ...rest } = filter;
    const formattedFilters = super.format(rest);

    if (bsp) {
      formattedFilters.push({
        label: `${this.translate('bsp')} - ${bspFilterTagMapper(bsp)}`,
        keys: ['bsp']
      });
    }

    if (airlines) {
      formattedFilters.push({
        label: airlines.map(record => `${record.code} / ${record.name}`).join(', '),
        keys: ['airlines']
      });
    }

    if (active != null) {
      const activeLabel = this.translate('activeOptions.active');
      const inactiveLabel = this.translate('activeOptions.inactive');
      const statusValue = active ? activeLabel : inactiveLabel;

      formattedFilters.push({
        label: `${this.translate('status')} - ${statusValue}`,
        keys: ['active']
      });
    }

    if (registerDate) {
      const registerDateLabel = this.translate('registerDate');

      formattedFilters.push({
        label: `${registerDateLabel} - ${rangeDateFilterTagMapper(registerDate)}`,
        keys: ['registerDate']
      });
    }

    if (expiryDate) {
      const expiryDateLabel = this.translate('expiryDate');

      formattedFilters.push({
        label: `${expiryDateLabel} - ${rangeDateFilterTagMapper(expiryDate)}`,
        keys: ['expiryDate']
      });
    }

    if (easyPayEffectiveFrom) {
      const easyPayEffectiveFromLabel = this.translate('easyPayEffectiveFrom');

      formattedFilters.push({
        label: `${easyPayEffectiveFromLabel} - ${dateFilterTagMapper(easyPayEffectiveFrom)}`,
        keys: ['easyPayEffectiveFrom']
      });
    }

    return formattedFilters;
  }

  private translate(key: string): string {
    return this.translationService.translate('MASTER_DATA.airline.' + key);
  }
}
