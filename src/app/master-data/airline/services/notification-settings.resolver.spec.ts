import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { getConfigurationSelectedBsp } from '../reducers';
import { NotificationSettingsResolver } from './notification-settings.resolver';

describe('NotificationSettingsResolver', () => {
  let resolver: NotificationSettingsResolver;
  let mockStore: MockStore;

  const initialState = {
    profile: [],
    properties: [],
    configuration: []
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getConfigurationSelectedBsp, value: [] }]
        }),
        mockProvider(Router),
        NotificationSettingsResolver
      ]
    });
    resolver = TestBed.inject(NotificationSettingsResolver);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should trigger resolver and dispatch load action from setting configuration actions', fakeAsync(() => {
    const mockScope = { scopeType: 'bsps', scopeId: 'undefined', name: undefined, service: 'notification-management' };
    resolver.resolve().subscribe();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith({ scope: mockScope, type: SettingConfigurationActions.load.type });
  }));
});
