import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { getAirlines } from '../reducers';
import { RefundAuthorityRequiredFilterResolver } from './refund-authority-required-filter.resolver';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Airline } from '~app/master-data/models';
import { RefundAuthorityFilter, ViewAs } from '~app/master-data/refund-authority';
import { RefundAuthorityListActions } from '~app/master-data/refund-authority/store/actions';
import * as fromRefundAuthority from '~app/master-data/refund-authority/store/reducers/index';
import { DataQuery } from '~app/shared/components/list-view/data-query';
import { AirlineDictionaryService } from '~app/shared/services';

describe('Refund authority required filter resolver', () => {
  let refundAuthorityRequiredFilterResolver: RefundAuthorityRequiredFilterResolver;
  let mockStore: MockStore;
  let airlineDictionaryServiceMock: SpyObject<AirlineDictionaryService>;

  const airline: Airline = {
    id: 6554485649,
    localName: 'Qatar',
    designator: 'QA',
    effectiveFrom: '2020-02-10',
    active: true,
    globalAirline: null,
    bsp: { id: 123, effectiveFrom: '2020-01-10', isoCountryCode: 'ES', name: 'Spain' }
  };

  const expected = {
    airlines: [
      {
        id: 6554485649,
        name: 'Qatar',
        designator: 'QA',
        code: '081'
      }
    ],
    viewAs: ViewAs.Airline,
    bsp: { id: 123, effectiveFrom: '2020-01-10', isoCountryCode: 'ES', name: 'Spain' }
  };

  const query: DataQuery<RefundAuthorityFilter> = {
    filterBy: expected,
    paginateBy: null,
    sortBy: null
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RefundAuthorityRequiredFilterResolver,
        provideMockStore({}),
        mockProvider(AirlineDictionaryService),
        PermissionsService
      ]
    });

    mockStore = TestBed.inject<any>(Store);
    mockStore.overrideSelector(getAirlines, [airline]);
    mockStore.overrideSelector(fromRefundAuthority.getListQuery, query);

    refundAuthorityRequiredFilterResolver = TestBed.inject(RefundAuthorityRequiredFilterResolver);
    airlineDictionaryServiceMock = TestBed.inject<any>(AirlineDictionaryService);
    airlineDictionaryServiceMock.summarize.and.returnValue({
      id: 6554485649,
      name: 'Qatar',
      designator: 'QA',
      code: '081'
    });
  });

  it('should create', () => {
    expect(refundAuthorityRequiredFilterResolver).toBeTruthy();
  });

  it('should dispatch RefundAuthorityListAction change required filter', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    refundAuthorityRequiredFilterResolver.resolve().subscribe();

    tick();
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      RefundAuthorityListActions.changeRequiredFilter({ requiredFilter: expected })
    );
  }));
});
