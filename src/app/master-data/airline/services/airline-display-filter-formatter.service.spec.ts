import { TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { AirlinesDisplayFilterFormatter } from './airline-display-filter-formatter.service';
import { AirlineSummary } from '~app/shared/models';

describe('Airline display filter formatter', () => {
  let airlineDisplayFilterFormatter: AirlinesDisplayFilterFormatter;
  let translationServiceMock: SpyObject<L10nTranslationService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [mockProvider(L10nTranslationService)]
    });

    translationServiceMock = TestBed.inject<any>(L10nTranslationService);
    translationServiceMock.translate.and.callFake(key => key);
    airlineDisplayFilterFormatter = new AirlinesDisplayFilterFormatter(translationServiceMock);
  });

  it('should create', () => {
    expect(airlineDisplayFilterFormatter).toBeTruthy();
  });

  it('should format BSP filter', () => {
    const bsp = {
      id: 1,
      isoCountryCode: 'ES',
      name: 'Spain',
      effectiveFrom: '2000-01-01'
    };

    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.bsp - Spain (ES)',
        keys: ['bsp']
      }
    ];

    const result = airlineDisplayFilterFormatter.format({ bsp });

    expect(result).toEqual(expectedResult);
  });

  it('should format airline filter', () => {
    const airlines = [
      {
        name: 'Qantas',
        code: '081',
        designator: 'QA'
      } as AirlineSummary
    ];

    const expectedResult = [
      {
        label: '081 / Qantas',
        keys: ['airlines']
      }
    ];

    const result = airlineDisplayFilterFormatter.format({ airlines });

    expect(result).toEqual(expectedResult);
  });

  it('should format active status filter', () => {
    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.status - MASTER_DATA.airline.activeOptions.active',
        keys: ['active']
      }
    ];
    const result = airlineDisplayFilterFormatter.format({ active: true });

    expect(result).toEqual(expectedResult);
  });

  it('should format inactive status filter', () => {
    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.status - MASTER_DATA.airline.activeOptions.inactive',
        keys: ['active']
      }
    ];
    const result = airlineDisplayFilterFormatter.format({ active: false });

    expect(result).toEqual(expectedResult);
  });

  it('should format register date filter when single date is selected', () => {
    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.registerDate - 20/02/2020',
        keys: ['registerDate']
      }
    ];
    const result = airlineDisplayFilterFormatter.format({
      registerDate: [new Date('2020-02-20')]
    });

    expect(result).toEqual(expectedResult);
  });

  it('should format register date filter', () => {
    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.registerDate - 20/02/2020 - 22/02/2020',
        keys: ['registerDate']
      }
    ];
    const result = airlineDisplayFilterFormatter.format({
      registerDate: [new Date('2020-02-20'), new Date('2020-02-22')]
    });

    expect(result).toEqual(expectedResult);
  });

  it('should format expiry date filter when single date is selected', () => {
    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.expiryDate - 20/02/2020',
        keys: ['expiryDate']
      }
    ];
    const result = airlineDisplayFilterFormatter.format({
      expiryDate: [new Date('2020-02-20')]
    });

    expect(result).toEqual(expectedResult);
  });

  it('should format expiry date filter', () => {
    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.expiryDate - 20/02/2020 - 22/02/2020',
        keys: ['expiryDate']
      }
    ];
    const result = airlineDisplayFilterFormatter.format({
      expiryDate: [new Date('2020-02-20'), new Date('2020-02-22')]
    });

    expect(result).toEqual(expectedResult);
  });

  it('should format easyPayEffectiveFrom filter', () => {
    const expectedResult = [
      {
        label: 'MASTER_DATA.airline.easyPayEffectiveFrom - 23/02/2020',
        keys: ['easyPayEffectiveFrom']
      }
    ];
    const result = airlineDisplayFilterFormatter.format({
      easyPayEffectiveFrom: new Date('2020-02-23')
    });

    expect(result).toEqual(expectedResult);
  });
});
