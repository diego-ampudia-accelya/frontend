import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { getCustomRouterSateSerializerMock, getInitialAirlineState } from '../mocks/airline-services-mocks';
import { BasicSettingsResolver } from './airline-basic-settings.resolver';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { CustomRouterSateSerializer } from '~app/shared/utils';

describe('Airline Setting Resolver', () => {
  let airlineSettingResolver: BasicSettingsResolver;
  let store: MockStore;

  const customRouterSateSerializerMock = createSpyObject(CustomRouterSateSerializer);
  customRouterSateSerializerMock.getParamsFrom.and.callFake(getCustomRouterSateSerializerMock);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState: getInitialAirlineState() }),
        BasicSettingsResolver,
        PermissionsService
      ]
    });

    airlineSettingResolver = TestBed.inject(BasicSettingsResolver);
    store = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(airlineSettingResolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(store, 'dispatch');

    airlineSettingResolver.resolve().subscribe();

    tick();

    expect(store.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'airlines',
          scopeId: '6554485649',
          name: 'Spain',
          service: 'acdm-management'
        }
      })
    );
  }));
});
