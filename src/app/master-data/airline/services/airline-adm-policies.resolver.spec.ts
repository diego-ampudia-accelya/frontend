import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { AdmPoliciesActions } from '../actions';
import { getAdmPolicyConfiguration } from '../configuration/adm-policies-config';
import { getCustomRouterSateSerializerMock, getInitialAirlineState } from '../mocks/airline-services-mocks';

import { AirlineAdmPoliciesResolver } from './airline-adm-policies.resolver';
import { CustomRouterSateSerializer } from '~app/shared/utils';
import { PermissionsService } from '~app/auth/services/permissions.service';

describe('Airline Adm Policies Resolver', () => {
  let airlineAdmPoliciesResolver: AirlineAdmPoliciesResolver;
  let store: MockStore;

  const customRouterSateSerializerMock = createSpyObject(CustomRouterSateSerializer);
  customRouterSateSerializerMock.getParamsFrom.and.callFake(getCustomRouterSateSerializerMock);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState: getInitialAirlineState() }),
        AirlineAdmPoliciesResolver,
        PermissionsService
      ]
    });

    airlineAdmPoliciesResolver = TestBed.inject(AirlineAdmPoliciesResolver);
    store = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(airlineAdmPoliciesResolver).toBeTruthy();
  });

  it('should dispatch load AdmPolicies', fakeAsync(() => {
    spyOn(store, 'dispatch');

    airlineAdmPoliciesResolver.resolve().subscribe();

    expect(store.dispatch).toHaveBeenCalledWith(
      AdmPoliciesActions.load({
        scope: {
          scopeType: 'adm-policies',
          scopeId: '6554485649',
          name: 'Spain'
        },
        settingConfigurations: getAdmPolicyConfiguration()
      })
    );
  }));
});
