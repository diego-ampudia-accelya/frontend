import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ProfileActions } from '../actions';
import { GlobalAirline } from '~app/master-data/models';
import { GlobalAirlineService } from '~app/master-data/services';
import { AppState } from '~app/reducers';

@Injectable({
  providedIn: 'root'
})
export class AirlineProfileResolver implements Resolve<GlobalAirline> {
  private readonly idKey = 'airline-id';

  constructor(private store: Store<AppState>, private globalAirlineService: GlobalAirlineService) {}

  public resolve(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): Observable<GlobalAirline> {
    const airlineId = Number(route.paramMap.get(this.idKey));

    if (!airlineId) {
      throw new Error(`Missing required path parameter "${this.idKey}" in route ${router.url}`);
    }

    return this.globalAirlineService.getBy(airlineId, true).pipe(
      tap(globalAirline => {
        this.store.dispatch(ProfileActions.loadGlobalAirline({ globalAirline, isOwnProfile: false }));
      })
    );
  }
}
