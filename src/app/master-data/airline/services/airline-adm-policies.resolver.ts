import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { AdmPoliciesActions } from '../actions';
import { getAdmPolicyConfiguration, scopeType } from '../configuration/adm-policies-config';
import { getConfigurationAirline, getSelectedAirline } from '../reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';

@Injectable({
  providedIn: 'root'
})
export class AirlineAdmPoliciesResolver implements Resolve<any> {
  constructor(private store: Store<AppState>, private permissionsService: PermissionsService) {}

  resolve(): Observable<any> {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    const getAirline$ = hasLeanPermission
      ? this.store.pipe(select(getConfigurationAirline), first())
      : this.store.pipe(select(getSelectedAirline), first());

    return getAirline$.pipe(
      tap(airline => {
        this.store.dispatch(
          AdmPoliciesActions.load({
            scope: {
              scopeType,
              scopeId: `${airline.id}`,
              name: airline.bsp.name
            },
            settingConfigurations: getAdmPolicyConfiguration()
          })
        );
      }),
      mapTo(null)
    );
  }
}
