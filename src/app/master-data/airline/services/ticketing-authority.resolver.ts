import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';

import { getSelectedAirline } from '../reducers';
import { TicketingAuthorityFilter } from '~app/master-data/ticketing-authority';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import * as fromTicketingAuthority from '~app/master-data/ticketing-authority/store/reducers';
import { AppState } from '~app/reducers';
import { AirlineDictionaryService } from '~app/shared/services/dictionary/airline-dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class TicketingAuthorityResolver implements Resolve<TicketingAuthorityFilter> {
  constructor(private store: Store<AppState>, private airlineService: AirlineDictionaryService) {}

  resolve(): Observable<TicketingAuthorityFilter> {
    return combineLatest([
      this.store.pipe(select(getSelectedAirline)),
      this.store.pipe(select(fromTicketingAuthority.getRequiredFilter))
    ]).pipe(
      map(
        ([selectedAirline, requiredFilter]) =>
          selectedAirline && {
            airlines: [this.airlineService.summarize(selectedAirline)],
            agents: requiredFilter.agents
          }
      ),
      tap(requiredFilter => {
        this.store.dispatch(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));
      }),
      first()
    );
  }
}
