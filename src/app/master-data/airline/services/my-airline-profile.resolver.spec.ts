import { TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { ProfileActions } from '../actions';
import { MyAirlineProfileResolver } from './my-airline-profile.resolver';
import { AuthState } from '~app/auth/reducers/auth.reducer';
import { Airline, GlobalAirline } from '~app/master-data/models';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { Bsp } from '~app/shared/models/bsp.model';
import { AirlineUser, User } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services/notification.service';

// TODO: to be done in FCA-11481
xdescribe('MyAirlineProfileResolver', () => {
  let resolver: MyAirlineProfileResolver;
  let mockStore: MockStore<{ auth: AuthState }>;
  let initialState: { auth: AuthState };
  let mockRouterSnapshot: RouterStateSnapshot;
  let route: ActivatedRouteSnapshot;
  let notificationServiceStub: SpyObject<NotificationService>;

  beforeEach(waitForAsync(() => {
    initialState = {
      auth: {
        user: {} as User
      }
    };
    notificationServiceStub = createSpyObject(NotificationService);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      providers: [
        provideMockStore({ initialState }),
        MyAirlineProfileResolver,
        { provide: NotificationService, useValue: notificationServiceStub }
      ]
    });

    resolver = TestBed.inject(MyAirlineProfileResolver);
    mockStore = TestBed.inject<any>(Store);
    route = jasmine.createSpyObj<ActivatedRouteSnapshot>('ActivatedRouteSnapshot', ['params', 'root']);
    mockRouterSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
  }));

  it('should create', () => {
    expect(resolver).toBeTruthy();
  });

  describe('when current user is an airline user', () => {
    let user: AirlineUser;
    let globalAirline: GlobalAirline;
    let airlineGermany: Airline;
    let airlineEngland: Airline;
    let expectedGlobalAirline: GlobalAirline;

    beforeEach(() => {
      globalAirline = {
        id: 4,
        globalName: 'Lufthansa'
      } as GlobalAirline;

      airlineGermany = {
        id: 1,
        bsp: { id: 2, name: 'Germany' } as Bsp,
        globalAirline
      } as Airline;

      airlineEngland = {
        id: 2,
        bsp: { id: 3, name: 'Great Britain' } as Bsp,
        globalAirline
      } as Airline;

      user = {
        ...createAirlineUser(),
        globalAirline: {
          id: 4,
          globalName: 'Lufthansa',
          airlines: [airlineGermany, airlineEngland]
        }
      } as AirlineUser;

      expectedGlobalAirline = {
        ...globalAirline,
        airlines: [
          { ...airlineGermany, globalAirline },
          { ...airlineEngland, globalAirline }
        ]
      } as GlobalAirline;
    });

    it('should return an global airline with two airlines when user has two airlines assigned', waitForAsync(() => {
      mockStore.setState({ auth: { user } });

      resolver.resolve(route, mockRouterSnapshot).subscribe(actualGlobalAirline => {
        expect(actualGlobalAirline).toEqual(expectedGlobalAirline);
      });
    }));

    it('should dispatch ProfileActions.LoadAirlineSuccess', waitForAsync(() => {
      mockStore.setState({ auth: { user } });
      const dispatchSpy = spyOn(mockStore, 'dispatch');
      const expectedAction = ProfileActions.loadGlobalAirline({
        globalAirline: expectedGlobalAirline,
        isOwnProfile: true
      });

      resolver.resolve(route, mockRouterSnapshot).subscribe();

      expect(dispatchSpy).toHaveBeenCalledWith(expectedAction);
    }));
  });

  describe('when current user is not an airline user', () => {
    it('should throw an error', waitForAsync(() => {
      resolver.resolve(route, mockRouterSnapshot).subscribe(
        () => expect(true).toBeFalsy('Should throw'),
        error => {
          expect(error).toBeTruthy();
        }
      );
    }));
  });
});
