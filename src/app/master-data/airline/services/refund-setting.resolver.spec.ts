import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import { getCustomRouterSateSerializerMock, getInitialAirlineState } from '../mocks/airline-services-mocks';
import { RefundSettingResolver } from './refund-setting.resolver';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { CustomRouterSateSerializer } from '~app/shared/utils';

describe('Refund Setting Resolver', () => {
  let refundSettingResolver: RefundSettingResolver;
  let store: Store<AppState>;

  const customRouterSateSerializerMock = createSpyObject(CustomRouterSateSerializer);
  customRouterSateSerializerMock.getParamsFrom.and.callFake(getCustomRouterSateSerializerMock);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState: getInitialAirlineState() }),
        RefundSettingResolver,
        PermissionsService
      ]
    });

    refundSettingResolver = TestBed.inject(RefundSettingResolver);
    store = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(refundSettingResolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(store, 'dispatch');

    refundSettingResolver.resolve().subscribe();

    expect(store.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'airlines',
          scopeId: '6554485649',
          name: 'Spain',
          service: 'refund-management'
        }
      })
    );
  }));
});
