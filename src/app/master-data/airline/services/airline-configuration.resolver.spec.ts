import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { ConfigurationActions } from '../actions';
import { AirlineConfigurationResolver } from './airline-configuration.resolver';

describe('AirlineConfigurationResolver', () => {
  let resolver: AirlineConfigurationResolver;
  let mockStore: MockStore;

  const initialState = {
    profile: null,
    properties: null,
    configuration: null
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getUserDefaultBsp, value: [] }]
        }),
        mockProvider(Router)
      ]
    });
    resolver = TestBed.inject(AirlineConfigurationResolver);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should update selected BSP by calling the resolver and dispatching updateSelectedBsp action', fakeAsync(() => {
    resolver.resolve().subscribe();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith({
      selectedBsp: [],
      type: ConfigurationActions.updateSelectedBsp.type
    });
  }));
});
