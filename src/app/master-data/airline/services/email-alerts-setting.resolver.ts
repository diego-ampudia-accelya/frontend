import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap, withLatestFrom } from 'rxjs/operators';

import { getConfigurationAirline, getSelectedAirline } from '../reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';

@Injectable({
  providedIn: 'root'
})
export class EmailAlertsSettingResolver implements Resolve<any> {
  constructor(private store: Store<AppState>, private permissionsService: PermissionsService) {}

  resolve(): Observable<any> {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    const getAirline$ = hasLeanPermission
      ? this.store.pipe(select(getConfigurationAirline), first())
      : this.store.pipe(select(getSelectedAirline), first());

    return this.store.pipe(
      select(getUser),
      withLatestFrom(getAirline$),
      tap(([user, airline]) => {
        this.store.dispatch(
          SettingConfigurationActions.load({
            scope: {
              scopeType: hasLeanPermission ? 'users/airlines' : 'users',
              scopeId: hasLeanPermission ? `${airline.id}` : `${user.id}`,
              name: airline.bsp.name,
              service: 'user-profile-management'
            }
          })
        );
      }),
      mapTo(null),
      first()
    );
  }
}
