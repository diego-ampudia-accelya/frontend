import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { getSelectedAirline } from '../reducers';
import { ReasonsResolver } from './airline-reasons.resolver';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Airline } from '~app/master-data/models';

describe('Airline resolver', () => {
  let airlineResolver: ReasonsResolver;
  let mockStore: MockStore;

  const airline = {
    id: 1234,
    localName: 'Qantas',
    bsp: {
      name: 'Spain',
      id: 123,
      isoCountryCode: 'ES'
    },
    designator: 'QA'
  } as Airline;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({}), ReasonsResolver, PermissionsService]
    });

    mockStore = TestBed.inject<any>(Store);

    airlineResolver = TestBed.inject(ReasonsResolver);
  });

  it('should create', () => {
    expect(airlineResolver).toBeTruthy();
  });

  it('should return selectedAirline', fakeAsync(() => {
    mockStore.overrideSelector(getSelectedAirline, airline);
    airlineResolver.resolve().subscribe(result => {
      expect(result).toEqual(airline);
    });
  }));
});
