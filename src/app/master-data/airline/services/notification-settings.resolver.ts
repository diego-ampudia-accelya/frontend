import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { getConfigurationSelectedBsp } from '~app/master-data/airline/reducers';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';

@Injectable({
  providedIn: 'root'
})
export class NotificationSettingsResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<any> {
    const getSelectedBsp$ = combineLatest([
      this.store.pipe(select(getConfigurationSelectedBsp), first()),
      this.store.pipe(select(getUserDefaultBsp), first())
    ]);

    return getSelectedBsp$.pipe(
      tap(([selectedBsp, userDefaultBsp]) => {
        const bsp = selectedBsp || userDefaultBsp;

        this.store.dispatch(
          SettingConfigurationActions.load({
            scope: {
              scopeType: 'bsps',
              scopeId: `${bsp.id}`,
              name: bsp.name,
              service: 'notification-management'
            }
          })
        );
      }),
      mapTo(null)
    );
  }
}
