import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { getCustomRouterSateSerializerMock, getInitialAirlineUserState } from '../mocks/airline-services-mocks';
import { VariableRemittanceResolver } from './variable-remittance-setting.resolver';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { CustomRouterSateSerializer } from '~app/shared/utils';

describe('Variable Remittance Setting Resolver', () => {
  let variableRemittanceSettingResolver: VariableRemittanceResolver;
  let store: MockStore;

  const customRouterSateSerializerMock = createSpyObject(CustomRouterSateSerializer);
  customRouterSateSerializerMock.getParamsFrom.and.callFake(getCustomRouterSateSerializerMock);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState: getInitialAirlineUserState() }),
        VariableRemittanceResolver,
        PermissionsService
      ]
    });

    variableRemittanceSettingResolver = TestBed.inject(VariableRemittanceResolver);
    store = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(variableRemittanceSettingResolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(store, 'dispatch');

    variableRemittanceSettingResolver.resolve().subscribe();

    tick();

    expect(store.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'airlines',
          scopeId: '6554485649',
          name: 'Spain',
          service: 'variable-remittance'
        }
      })
    );
  }));
});
