import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { isEmpty } from 'lodash';
import { Observable, of } from 'rxjs';
import { first, map, withLatestFrom } from 'rxjs/operators';

import { getSelectedAirline } from '../reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { RefundAuthorityFilter, ViewAs } from '~app/master-data/refund-authority/models';
import { RefundAuthorityListActions } from '~app/master-data/refund-authority/store/actions';
import * as fromRefundAuthority from '~app/master-data/refund-authority/store/reducers/index';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { AirlineDictionaryService } from '~app/shared/services/dictionary/airline-dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class RefundAuthorityRequiredFilterResolver implements Resolve<RefundAuthorityFilter> {
  constructor(
    private store: Store<AppState>,
    private airlineService: AirlineDictionaryService,
    private permissionsService: PermissionsService
  ) {}

  resolve(): Observable<RefundAuthorityFilter> {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    return hasLeanPermission
      ? of(null)
      : this.store.pipe(
          select(getSelectedAirline),
          first(),
          map(
            selectedAirline =>
              selectedAirline && {
                airlines: [this.airlineService.summarize(selectedAirline)],
                viewAs: ViewAs.Airline,
                bsp: selectedAirline.bsp
              }
          ),
          withLatestFrom(this.store.pipe(select(fromRefundAuthority.getListQuery))),
          map(([reqFilter, query]) => {
            const requiredFilter = !isEmpty(query?.filterBy) ? { ...query?.filterBy, ...reqFilter } : reqFilter;

            this.store.dispatch(RefundAuthorityListActions.changeRequiredFilter({ requiredFilter }));

            return requiredFilter;
          })
        );
  }
}
