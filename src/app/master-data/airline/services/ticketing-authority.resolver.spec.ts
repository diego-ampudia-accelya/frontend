import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import * as fromTicketingAuthority from '../../ticketing-authority/store/reducers';
import { getCustomRouterSateSerializerMock, getInitialAirlineState } from '../mocks/airline-services-mocks';
import { TicketingAuthorityResolver } from './ticketing-authority.resolver';
import { TicketingAuthorityFilter } from '~app/master-data/ticketing-authority';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import { AppState } from '~app/reducers';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AirlineSummary } from '~app/shared/models';
import { AirlineDictionaryService } from '~app/shared/services';
import { CustomRouterSateSerializer } from '~app/shared/utils';

describe('Ticketing Authority resolver', () => {
  let ticketingAuthorityResolver: TicketingAuthorityResolver;
  let store: MockStore;
  let airlineServiceMock: SpyObject<AirlineDictionaryService>;
  let mockStore: MockStore<AppState>;

  const customRouterSateSerializerMock = createSpyObject(CustomRouterSateSerializer);
  customRouterSateSerializerMock.getParamsFrom.and.returnValue(getCustomRouterSateSerializerMock);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TicketingAuthorityResolver,
        mockProvider(AirlineDictionaryService),
        provideMockStore({
          initialState: getInitialAirlineState(),
          selectors: [
            { selector: fromTicketingAuthority.getRequiredFilter, value: {} },
            { selector: fromTicketingAuthority.getListQuery, value: defaultQuery }
          ]
        })
      ]
    });

    store = TestBed.inject<any>(Store);
    mockStore = TestBed.inject<any>(Store);
    ticketingAuthorityResolver = TestBed.inject(TicketingAuthorityResolver);
    airlineServiceMock = TestBed.inject<any>(AirlineDictionaryService);
    airlineServiceMock.summarize.and.returnValue({
      name: 'Airline',
      id: 6554485649,
      code: '081',
      designator: 'QA'
    });
  });

  it('should create', () => {
    expect(ticketingAuthorityResolver).toBeTruthy();
  });

  it('should dispatch ticketingAuthorityListAction changeRequiredFilter with selectedAirline', fakeAsync(() => {
    const airline1 = { id: 1, name: 'Airline 1' } as AirlineSummary;

    const requiredFilter: TicketingAuthorityFilter = {
      airlines: [airline1],
      agents: []
    };

    mockStore.overrideSelector(fromTicketingAuthority.getRequiredFilter, requiredFilter);
    spyOn(store, 'dispatch');

    ticketingAuthorityResolver.resolve().subscribe();
    tick();

    expect(store.dispatch).toHaveBeenCalledWith(
      TicketingAuthorityListActions.changeRequiredFilter({
        requiredFilter: {
          airlines: [
            {
              name: 'Airline',
              id: 6554485649,
              code: '081',
              designator: 'QA'
            }
          ],
          agents: []
        }
      })
    );
  }));
});
