import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';

import { getSelectedAirline } from '../reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AddressMaintenanceResourceType } from '~app/master-data/address-maintenance/models/address-maintenance.model';
import { AddressMaintenanceService } from '~app/master-data/address-maintenance/services/address-maintenance.service';
import { AddressMaintenanceActions } from '~app/master-data/address-maintenance/store';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class AirlineAddressMaintenanceResolver implements Resolve<any> {
  constructor(
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private addressMaintenanceService: AddressMaintenanceService
  ) {}

  resolve(): Observable<any> {
    const resourceType = AddressMaintenanceResourceType.airline;
    const isReadonlyMode = !this.permissionsService.hasPermission(Permissions.updateAirlineAddressMaintenance);

    return this.store.pipe(
      select(getSelectedAirline),
      first(),
      tap(airline =>
        this.store.dispatch(
          AddressMaintenanceActions.load({ resourceType, resourceId: '' + airline.id, isReadonlyMode })
        )
      ),
      // TODO: refactor within an effect `load$`
      switchMap(_ => this.addressMaintenanceService.get()),
      map(addressMaintenance =>
        this.store.dispatch(AddressMaintenanceActions.loadSuccess({ value: addressMaintenance }))
      ),
      rethrowError(() => AddressMaintenanceActions.loadError())
    );
  }
}
