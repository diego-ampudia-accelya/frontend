import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap, withLatestFrom } from 'rxjs/operators';

import { ConfigurationActions } from '../actions';
import { getConfigurationSelectedBsp } from '../reducers';
import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';

@Injectable({
  providedIn: 'root'
})
export class AirlineConfigurationResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getConfigurationSelectedBsp),
      withLatestFrom(this.store.pipe(select(getUserDefaultBsp))),
      tap(([selectedBsp, defaultBsp]) => {
        if (!selectedBsp) {
          this.store.dispatch(ConfigurationActions.updateSelectedBsp({ selectedBsp: defaultBsp }));
        }
      }),
      mapTo(null),
      first()
    );
  }
}
