import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { getConfigurationAirline, getSelectedAirline } from '~app/master-data/airline/reducers';
import { IpAddressConfigurationActions } from '~app/master-data/configuration';
import { Airline } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';

@Injectable()
export class AirlineIpAddressSettingsResolver implements Resolve<any> {
  constructor(private store: Store<AppState>, private permissionsService: PermissionsService) {}

  resolve(): Observable<any> {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    const getAirline$ = hasLeanPermission
      ? this.store.pipe(select(getConfigurationAirline), first())
      : this.store.pipe(select(getSelectedAirline), first());

    return getAirline$.pipe(
      tap((airline: Airline) => {
        this.store.dispatch(
          IpAddressConfigurationActions.load({
            url: `user-management/users/bsps/${airline.bsp.id}/accepted-ips`
          })
        );
      }),
      mapTo(null)
    );
  }
}
