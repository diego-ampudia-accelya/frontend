import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { isEmpty } from 'lodash';
import { Observable, of, throwError } from 'rxjs';
import { first, mapTo, switchMap, tap } from 'rxjs/operators';

import { ProfileActions } from '../actions';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Airline, GlobalAirline } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { AirlineUser } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services/notification.service';
import { replaceRouteParamKey } from '~app/shared/utils/path-builder';

@Injectable({
  providedIn: 'root'
})
export class MyAirlineProfileResolver implements Resolve<any> {
  constructor(
    private store: Store<AppState>,
    private notificationService: NotificationService,
    private permissionsService: PermissionsService,
    private router: Router
  ) {}

  /**
   * Resolves global airline with its airlines based on the current user and depending on LEAN permission
   *
   * @throws Error when no global airline is found
   */
  public resolve(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<any> {
    return this.store.pipe(
      select(getUser),
      switchMap((user: AirlineUser) => {
        const userGlobalAirline = user.globalAirline;
        const userHasAirlines = userGlobalAirline != null && !isEmpty(userGlobalAirline.airlines);

        let result$ = of({ globalAirline: userGlobalAirline, defaultIsoc: user.defaultIsoc });

        if (!userHasAirlines) {
          this.notificationService.showError('MASTER_DATA.globalAirline.accessDeniedErrorMessage');

          // Throwing an error is needed in order to prevent further navigation
          result$ = throwError(new Error('User not assigned to airline.'));
        }

        return result$;
      }),
      tap(({ globalAirline, defaultIsoc }) => {
        // Handle airline id in navigation and redirects if needed
        this.handleNavigation(globalAirline, defaultIsoc, route, routerState);
        // Load global airline in store
        this.store.dispatch(ProfileActions.loadGlobalAirline({ globalAirline, isOwnProfile: true, defaultIsoc }));
      }),
      mapTo(null),
      first()
    );
  }

  private handleNavigation(
    userGlobalAirline: GlobalAirline,
    defaultIsoc: string,
    route: ActivatedRouteSnapshot,
    routerState: RouterStateSnapshot
  ): void {
    const airlineIdParam: string = route.params['my-airline-id'];
    const selectedAirlineId: number = this.getSelectedAirlineId(userGlobalAirline, defaultIsoc, +airlineIdParam);

    const currentUrl: string = routerState.url;
    const destinationUrl: string = replaceRouteParamKey(route, ':my-airline-id', '' + selectedAirlineId, true);
    // Prevent infinite redirects to the current url
    if (currentUrl !== destinationUrl) {
      this.router.navigateByUrl(destinationUrl);
    }
  }

  private getSelectedAirlineId(userGlobalAirline: GlobalAirline, defaultIsoc: string, airlineId: number): number {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    let selectedAirlineId: number;

    if (hasLeanPermission) {
      selectedAirlineId = userGlobalAirline.id;
    } else {
      const existingAirline: Airline = userGlobalAirline.airlines.find(
        airline => !!airlineId && airline.id === airlineId
      );

      // Handling a case where the user does not have assigned airlines should be handled outside
      selectedAirlineId = !existingAirline
        ? userGlobalAirline.airlines.find(airline => airline.bsp.isoCountryCode === defaultIsoc)?.id
        : existingAirline.id;
    }

    return selectedAirlineId;
  }
}
