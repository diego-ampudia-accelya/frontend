import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';

import { getConfigurationAirline, getSelectedAirline } from '../reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { TipLocalAirlineService } from '~app/master-data/configuration/tip/tip-local-airline/services/tip-local-airline.service';
import { TipLocalAirlineActions } from '~app/master-data/configuration/tip/tip-local-airline/store/actions';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class TipLocalAirlineSettingResolver implements Resolve<any> {
  constructor(
    private store: Store<AppState>,
    private tipLocalAirlineService: TipLocalAirlineService,
    private permissionsService: PermissionsService
  ) {}

  resolve(): Observable<any> {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    const getAirline$ = hasLeanPermission
      ? this.store.pipe(select(getConfigurationAirline), first())
      : this.store.pipe(select(getSelectedAirline), first());

    return getAirline$.pipe(
      tap(airline => this.store.dispatch(TipLocalAirlineActions.load({ airlineId: '' + airline.id }))),
      // TODO: refactor within an effect `load$`
      switchMap(_ => this.tipLocalAirlineService.find()),
      map(tipLocalAirline => this.store.dispatch(TipLocalAirlineActions.loadSuccess({ value: tipLocalAirline }))),
      rethrowError(() => TipLocalAirlineActions.loadError())
    );
  }
}
