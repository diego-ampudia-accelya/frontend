import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { ProfileActions } from '../actions';
import { AirlineProfileResolver } from './airline-profile.resolver';
import { GlobalAirline } from '~app/master-data/models';
import { GlobalAirlineService } from '~app/master-data/services';
import { AppState } from '~app/reducers';

describe('Airline profile resolver', () => {
  const initialState = {};
  let airlineProfileResolver: AirlineProfileResolver;
  let globalAirlineServiceMock: SpyObject<GlobalAirlineService>;
  let storeMock: SpyObject<Store<AppState>>;
  let activatedRouteSnapshotMock: SpyObject<ActivatedRouteSnapshot>;
  const routerStateSnapshot = {} as RouterStateSnapshot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AirlineProfileResolver,
        mockProvider(Router),
        mockProvider(GlobalAirlineService),
        mockProvider(ActivatedRouteSnapshot, { paramMap: { get: () => '1234' } }),
        provideMockStore({ initialState })
      ]
    });

    activatedRouteSnapshotMock = TestBed.inject<any>(ActivatedRouteSnapshot);
    airlineProfileResolver = TestBed.inject(AirlineProfileResolver);
    storeMock = TestBed.inject<any>(Store);

    globalAirlineServiceMock = TestBed.inject<any>(GlobalAirlineService);
    globalAirlineServiceMock.getBy.and.returnValue(of({} as GlobalAirline));
  });

  it('should create', () => {
    expect(airlineProfileResolver).toBeTruthy();
  });

  it('should dispatch loadGlobalAirline with the globalAirline', fakeAsync(() => {
    spyOn(storeMock, 'dispatch');

    airlineProfileResolver.resolve(activatedRouteSnapshotMock, routerStateSnapshot).subscribe();
    tick();

    expect(storeMock.dispatch).toHaveBeenCalledWith(
      ProfileActions.loadGlobalAirline({ globalAirline: {} as GlobalAirline, isOwnProfile: false })
    );
  }));
});
