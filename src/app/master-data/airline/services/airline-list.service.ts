import { Injectable } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable';
import { cloneDeep } from 'lodash';

import { AIRLINES_LIST } from '~app/master-data/airline/airline-list.constants';
import { EQ, GT, IN, LIKE, LT } from '~app/shared/constants/operations';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';

const PROPERTIES = AIRLINES_LIST.DATA_PROPERTIES;

@Injectable({ providedIn: 'root' })
export class AirlinesListService {
  generateColumns(): Array<TableColumn> {
    return cloneDeep(AIRLINES_LIST.COLUMNS);
  }

  formatRequestFilters(data: any): RequestQueryFilter[] {
    const formattedRequestFilters: RequestQueryFilter[] = [];

    Object.entries<any>(data).forEach(([key, value]) => {
      let attribute = '';
      let operation = EQ;
      let attributeValue = value;

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'registerDate') {
        attribute = PROPERTIES.REGISTER_DATE;

        if (value[1] === null) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: toShortIsoDate(value[0])
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: toShortIsoDate(value[0])
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: toShortIsoDate(value[1])
          });
        }

        return;
      }

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'expiryDate') {
        attribute = PROPERTIES.EXPIRATION_DATE;

        if (value[1] === null) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: toShortIsoDate(value[0])
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: toShortIsoDate(value[0])
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: toShortIsoDate(value[1])
          });
        }

        return;
      }

      switch (key) {
        case 'airlines':
          attribute = PROPERTIES.AIRLINE_CODE;
          operation = IN;
          attributeValue = value.map(v => v.code).join(',');
          break;
        case 'designator':
          attribute = PROPERTIES.DESIGNATOR;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'vatNumber':
          attribute = PROPERTIES.VAT_NUMBER;
          operation = LIKE;
          break;
        case 'active':
          attribute = PROPERTIES.ACTIVE;
          break;
        case 'easyPayStatus':
          attribute = PROPERTIES.EASY_PAY_STATUS;
          break;
        case 'easyPayEffectiveFrom':
          attribute = PROPERTIES.EASY_PAY_EFFECTIVE_FROM;
          operation = GT;
          attributeValue = toShortIsoDate(value);
          break;
        case 'city':
          attribute = PROPERTIES.CITY;
          operation = LIKE;
          break;
        case 'state':
          attribute = PROPERTIES.STATE;
          operation = LIKE;
          break;
        case 'addressCountry':
          attribute = PROPERTIES.ADDRESS_COUNTRY;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'postalCode':
          attribute = PROPERTIES.POSTAL_CODE;
          operation = LIKE;
          break;
        case 'email':
          attribute = PROPERTIES.EMAIL;
          operation = LIKE;
          break;
        case 'telephone':
          attribute = PROPERTIES.TELEPHONE;
          operation = LIKE;
          break;
        case 'name':
          attribute = PROPERTIES.LAST_NAME;
          operation = LIKE;
          break;
        case 'locality':
          attribute = PROPERTIES.LOCALITY;
          operation = LIKE;
          break;
      }

      formattedRequestFilters.push({
        attribute,
        operation,
        attributeValue
      });
    });

    return formattedRequestFilters;
  }
}
