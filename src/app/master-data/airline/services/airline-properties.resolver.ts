import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap, withLatestFrom } from 'rxjs/operators';

import { PropertiesActions } from '../actions';
import { getPropertiesSelectedBsp } from '../reducers';
import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';

@Injectable({
  providedIn: 'root'
})
export class AirlinePropertiesResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getPropertiesSelectedBsp),
      withLatestFrom(this.store.pipe(select(getUserDefaultBsp))),
      tap(([selectedBsp, defaultBsp]) => {
        if (!selectedBsp) {
          this.store.dispatch(PropertiesActions.updateBsp({ selectedBsp: defaultBsp }));
        }
      }),
      mapTo(null),
      first()
    );
  }
}
