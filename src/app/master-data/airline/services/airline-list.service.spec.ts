import { TestBed } from '@angular/core/testing';

import { AIRLINES_LIST } from '../airline-list.constants';

import { AirlinesListService } from './airline-list.service';

describe('AirlinesListService', () => {
  let airlinesListService: AirlinesListService;
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [AirlinesListService]
    })
  );

  beforeEach(() => {
    airlinesListService = new AirlinesListService();
  });

  it('should be created', () => {
    expect(airlinesListService).toBeTruthy();
  });

  describe('generateColumns', () => {
    it('should return correct data', () => {
      const result = airlinesListService.generateColumns();
      expect(result).toEqual(AIRLINES_LIST.COLUMNS);
    });
  });

  it('should format basic filters', () => {
    const filter = {
      active: false,
      airlines: [{ id: 6554484854, name: 'AIRLINE TEST', code: '006', designator: 'DL' }],
      expiryDate: [new Date('2020-01-02'), new Date('2020-03-02')],
      registerDate: [new Date('2020-02-02'), new Date('2020-04-02')]
    };

    const formattedFilters = airlinesListService.formatRequestFilters(filter);
    const expectedFormattedFilters = [
      { attribute: 'active', operation: '=', attributeValue: false },
      { attribute: 'globalAirline.iataCode', operation: 'in', attributeValue: '006' },
      { attribute: 'effectiveTo', operation: '>', attributeValue: '2020-01-02' },
      { attribute: 'effectiveTo', operation: '<', attributeValue: '2020-03-02' },
      { attribute: 'effectiveFrom', operation: '>', attributeValue: '2020-02-02' },
      { attribute: 'effectiveFrom', operation: '<', attributeValue: '2020-04-02' }
    ];

    expect(formattedFilters).toEqual(expectedFormattedFilters);
  });

  it('should format advanced filters', () => {
    const filter = {
      addressCountry: ['Bolivia'],
      city: 's',
      designator: ['JQ'],
      easyPayEffectiveFrom: new Date('2020-02-03'),
      easyPayStatus: 'OPTIN',
      email: '12',
      name: '12',
      postalCode: '1234',
      state: 's',
      telephone: '12',
      vatNumber: '00'
    };

    const formattedAdvancedFilters = airlinesListService.formatRequestFilters(filter);
    const expectedFormattedFilters = [
      { attribute: 'address.country', operation: 'in', attributeValue: 'Bolivia' },
      { attribute: 'address.city', operation: 'like', attributeValue: 's' },
      { attribute: 'designator', operation: 'in', attributeValue: 'JQ' },
      { attribute: 'easyPayEffectiveFrom', operation: '>', attributeValue: '2020-02-03' },
      { attribute: 'easyPayStatus', operation: '=', attributeValue: 'OPTIN' },
      { attribute: 'contact.email', operation: 'like', attributeValue: '12' },
      { attribute: 'contact.lastName', operation: 'like', attributeValue: '12' },
      { attribute: 'address.postalCode', operation: 'like', attributeValue: '1234' },
      { attribute: 'address.state', operation: 'like', attributeValue: 's' },
      { attribute: 'contact.telephone', operation: 'like', attributeValue: '12' },
      { attribute: 'vatNumber', operation: 'like', attributeValue: '00' }
    ];

    expect(formattedAdvancedFilters).toEqual(expectedFormattedFilters);
  });
});
