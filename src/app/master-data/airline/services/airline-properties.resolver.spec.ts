import { fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { PropertiesActions } from '../actions';
import { getPropertiesSelectedBsp } from '../reducers';

import { AirlinePropertiesResolver } from './airline-properties.resolver';
import { Bsp } from '~app/shared/models/bsp.model';
import { AppState } from '~app/reducers';

describe('AirlinePropertiesResolver', () => {
  let resolver: AirlinePropertiesResolver;
  let mockStore: MockStore<AppState>;

  const mockBsp: Bsp = {
    id: 1,
    isoCountryCode: 'ES',
    name: 'Spain',
    effectiveFrom: '2000-01-01'
  };

  const initialState = {
    auth: {
      user: {
        defaultIsoc: 'ES',
        bsps: [mockBsp]
      }
    },
    airline: {
      properties: {
        selectedBsp: null
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [AirlinePropertiesResolver, provideMockStore({ initialState })]
    });

    resolver = TestBed.inject(AirlinePropertiesResolver);
    mockStore = TestBed.inject<any>(Store);
  }));

  it('should update BSP if there is no one selected', fakeAsync(() => {
    let result: any;

    spyOn(mockStore, 'dispatch').and.callThrough();

    resolver.resolve().subscribe(value => (result = value));

    expect(mockStore.dispatch).toHaveBeenCalledWith(PropertiesActions.updateBsp({ selectedBsp: mockBsp }));
    expect(result).toBeNull();
  }));

  it('should NOT update BSP if there is one selected', fakeAsync(() => {
    let result: any;

    spyOn(mockStore, 'dispatch').and.callThrough();
    mockStore.overrideSelector(getPropertiesSelectedBsp, mockBsp);

    resolver.resolve().subscribe(value => (result = value));

    expect(mockStore.dispatch).not.toHaveBeenCalled();
    expect(result).toBeNull();
  }));
});
