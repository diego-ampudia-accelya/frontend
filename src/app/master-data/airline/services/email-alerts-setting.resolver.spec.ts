import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { getCustomRouterSateSerializerMock, getInitialAirlineUserState } from '../mocks/airline-services-mocks';
import { EmailAlertsSettingResolver } from './email-alerts-setting.resolver';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { CustomRouterSateSerializer } from '~app/shared/utils';

describe('Email Alerts Setting Resolver', () => {
  let emailAlertsSettingResolver: EmailAlertsSettingResolver;
  let store: MockStore;

  const customRouterSateSerializerMock = createSpyObject(CustomRouterSateSerializer);
  customRouterSateSerializerMock.getParamsFrom.and.callFake(getCustomRouterSateSerializerMock);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState: getInitialAirlineUserState() }),
        EmailAlertsSettingResolver,
        PermissionsService
      ]
    });

    emailAlertsSettingResolver = TestBed.inject(EmailAlertsSettingResolver);
    store = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(emailAlertsSettingResolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(store, 'dispatch');

    emailAlertsSettingResolver.resolve().subscribe();

    tick();

    expect(store.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'users',
          scopeId: '555',
          name: 'Spain',
          service: 'user-profile-management'
        }
      })
    );
  }));
});
