import { ProfileActions } from '../actions';

import * as fromAirlineProfile from './airline-profile.reducer';
import { GlobalAirline } from '~app/master-data/models';

describe('Airline profile reducer', () => {
  const mockGlobalAirline: GlobalAirline = {
    id: 1,
    globalName: 'IBERIA',
    iataCode: '001',
    airlines: [
      {
        id: 1,
        localName: 'IBERIA ES',
        designator: 'IBES',
        bsp: {
          id: 1,
          name: 'Spain',
          isoCountryCode: 'ES',
          effectiveFrom: '2000-01-01'
        },
        effectiveFrom: '2000-01-01',
        active: true,
        globalAirline: null
      },
      {
        id: 2,
        localName: 'IBERIA MT',
        designator: 'IBMT',
        bsp: {
          id: 1,
          name: 'Malta',
          isoCountryCode: 'MT',
          effectiveFrom: '2000-01-01'
        },
        effectiveFrom: '2000-01-01',
        active: true,
        globalAirline: null
      }
    ]
  };

  it('should update global airline and `isOwnProfile` when `loadGlobalAirline` action is called', () => {
    const action = ProfileActions.loadGlobalAirline({ globalAirline: mockGlobalAirline, isOwnProfile: true });
    const state = fromAirlineProfile.reducer(fromAirlineProfile.initialState, action);

    expect(state.globalAirline).toEqual(mockGlobalAirline);
    expect(state.isOwnProfile).toBe(true);
  });

  describe('defaultAirline', () => {
    it('should update default airline with first airline if no `defaultIsoc` is specified calling `loadGlobalAirline` action', () => {
      const action = ProfileActions.loadGlobalAirline({ globalAirline: mockGlobalAirline, isOwnProfile: true });
      const state = fromAirlineProfile.reducer(fromAirlineProfile.initialState, action);

      expect(state.defaultAirline).toEqual({
        id: 1,
        localName: 'IBERIA ES',
        designator: 'IBES',
        bsp: {
          id: 1,
          name: 'Spain',
          isoCountryCode: 'ES',
          effectiveFrom: '2000-01-01'
        },
        effectiveFrom: '2000-01-01',
        active: true,
        globalAirline: null
      });
    });

    it('should update default airline with default ISOC specified when `loadGlobalAirline` action is called', () => {
      const action = ProfileActions.loadGlobalAirline({
        globalAirline: mockGlobalAirline,
        isOwnProfile: true,
        defaultIsoc: 'MT'
      });
      const state = fromAirlineProfile.reducer(fromAirlineProfile.initialState, action);

      expect(state.defaultAirline).toEqual({
        id: 2,
        localName: 'IBERIA MT',
        designator: 'IBMT',
        bsp: {
          id: 1,
          name: 'Malta',
          isoCountryCode: 'MT',
          effectiveFrom: '2000-01-01'
        },
        effectiveFrom: '2000-01-01',
        active: true,
        globalAirline: null
      });
    });
  });

  it('should NOT throw any error if user has not global airline and call `loadGlobalAirline` action', () => {
    const action = ProfileActions.loadGlobalAirline({ globalAirline: undefined, isOwnProfile: false });
    const state = fromAirlineProfile.reducer(fromAirlineProfile.initialState, action);

    expect(state.defaultAirline).toBeFalsy();
  });
});
