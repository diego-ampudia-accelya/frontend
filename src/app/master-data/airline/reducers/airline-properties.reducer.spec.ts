import { PropertiesActions } from '../actions';

import * as fromAirlineProperties from './airline-properties.reducer';
import { Bsp } from '~app/shared/models/bsp.model';

describe('Airline properties reducer', () => {
  const mockBsp: Bsp = {
    id: 1,
    isoCountryCode: 'ES',
    name: 'Spain',
    effectiveFrom: '2000-01-01'
  };

  it('should update selected BSP when `updateBsp` action is called', () => {
    const action = PropertiesActions.updateBsp({ selectedBsp: mockBsp });
    const state = fromAirlineProperties.reducer(fromAirlineProperties.initialState, action);

    expect(state.selectedBsp).toEqual(mockBsp);
  });
});
