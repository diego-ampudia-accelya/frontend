import { createReducer, on } from '@ngrx/store';

import { ConfigurationActions } from '../actions';
import { Bsp } from '~app/shared/models/bsp.model';

export interface ConfigurationState {
  selectedBsp: Bsp;
}

const initialState: ConfigurationState = {
  selectedBsp: null
};

export const reducer = createReducer(
  initialState,
  on(ConfigurationActions.updateSelectedBsp, (state, { selectedBsp }) => ({
    ...state,
    selectedBsp
  }))
);
