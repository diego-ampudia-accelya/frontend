import { createReducer, on } from '@ngrx/store';

import { PropertiesActions } from '../actions';
import { Bsp } from '~app/shared/models/bsp.model';

export interface PropertiesState {
  selectedBsp: Bsp;
}

export const initialState: PropertiesState = {
  selectedBsp: null
};

export const reducer = createReducer(
  initialState,
  on(PropertiesActions.updateBsp, (state, { selectedBsp }) => ({
    ...state,
    selectedBsp
  }))
);
