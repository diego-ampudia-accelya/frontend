import { createReducer, on } from '@ngrx/store';

import { ProfileActions } from '../actions';
import { Airline, GlobalAirline } from '~app/master-data/models';

export interface ProfileState {
  globalAirline: GlobalAirline;
  defaultAirline: Airline;
  isOwnProfile: boolean;
}

export const initialState: ProfileState = {
  globalAirline: null,
  defaultAirline: null,
  isOwnProfile: false
};

export const reducer = createReducer(
  initialState,
  on(ProfileActions.loadGlobalAirline, (state, { globalAirline, isOwnProfile, defaultIsoc }) => ({
    ...state,
    globalAirline,
    defaultAirline: getAirlineByIsoc(globalAirline, defaultIsoc),
    isOwnProfile
  }))
);

/**
 * Get the airline associated with ISOC specified
 *
 * @param globalAirline Global airline from which we will draw the returned airline
 * @param isoCountryCode ISOC from the BSP
 */
export const getAirlineByIsoc = (globalAirline: GlobalAirline, isoCountryCode: string): Airline => {
  const airlines: Airline[] = globalAirline ? globalAirline.airlines : [];

  return airlines.find(airline => airline.bsp.isoCountryCode === isoCountryCode) || airlines[0];
};
