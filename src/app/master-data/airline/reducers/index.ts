import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
import { chain } from 'lodash';

import * as fromAirlineConfiguration from './airline-configuration.reducer';
import * as fromAirlineProfile from './airline-profile.reducer';
import * as fromAirlineProperties from './airline-properties.reducer';
import { AppState, getRouterState } from '~app/reducers';
import { Airline } from '~app/master-data/models';

export interface AirlineState {
  profile: fromAirlineProfile.ProfileState;
  properties: fromAirlineProperties.PropertiesState;
  configuration: fromAirlineConfiguration.ConfigurationState;
}

export interface State extends AppState {
  airline: AirlineState;
}

export function reducers(state: AirlineState | undefined, action: Action) {
  return combineReducers({
    profile: fromAirlineProfile.reducer,
    properties: fromAirlineProperties.reducer,
    configuration: fromAirlineConfiguration.reducer
  })(state, action);
}

export const getAirlineState = createFeatureSelector<State, AirlineState>('airline');

export const getGlobalAirline = createSelector(
  getAirlineState,
  state => state && state.profile && state.profile.globalAirline
);

export const getDefaultAirline = createSelector(
  getAirlineState,
  state => state && state.profile && state.profile.defaultAirline
);

export const getIsOwnProfile = createSelector(
  getAirlineState,
  state => state && state.profile && state.profile.isOwnProfile
);

export const getPropertiesSelectedBsp = createSelector(
  getAirlineState,
  state => state && state.properties && state.properties.selectedBsp
);

export const getConfigurationSelectedBsp = createSelector(
  getAirlineState,
  state => state && state.configuration && state.configuration.selectedBsp
);

export const getAirlines = createSelector(getGlobalAirline, globalAirline =>
  globalAirline ? globalAirline.airlines : null
);

export const getAirlineOptions = createSelector(getAirlines, airlines =>
  airlines ? airlines.map(airline => ({ value: airline, label: airline.bsp.name })) : null
);

const getSelectedAirlineId = createSelector(getRouterState, routerState => {
  let airlineId: number = null;

  if (routerState != null) {
    const airlineIdParam = routerState.state.params['my-airline-id'] || routerState.state.params['airline-id'];

    airlineId = Number(airlineIdParam);
    airlineId = isNaN(airlineId) ? null : airlineId;
  }

  return airlineId;
});

/**
 * Get current airline from airline ID route parameter.
 * If it does not exist among the airlines of the global airline, the default airline is returned
 */
export const getSelectedAirline = createSelector(
  getGlobalAirline,
  getDefaultAirline,
  getSelectedAirlineId,
  (globalAirline, defaultAirline, localAirlineId) => {
    const airlines: Airline[] = globalAirline ? globalAirline.airlines : [];

    return chain(airlines)
      .find({ id: localAirlineId })
      .defaultTo(defaultAirline || airlines[0])
      .value();
  }
);

export const getPropertiesAirline = createSelector(
  getGlobalAirline,
  getPropertiesSelectedBsp,
  (globalAirline, selectedBsp) => fromAirlineProfile.getAirlineByIsoc(globalAirline, selectedBsp.isoCountryCode)
);

export const getConfigurationAirline = createSelector(
  getGlobalAirline,
  getConfigurationSelectedBsp,
  (globalAirline, selectedBsp) => fromAirlineProfile.getAirlineByIsoc(globalAirline, selectedBsp?.isoCountryCode)
);
