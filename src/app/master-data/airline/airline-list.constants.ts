export const AIRLINES_LIST = {
  DATA_PROPERTIES: {
    AIRLINE_CODE: 'globalAirline.iataCode',
    REGISTER_DATE: 'effectiveFrom',
    EXPIRATION_DATE: 'effectiveTo',
    DESIGNATOR: 'designator',
    VAT_NUMBER: 'vatNumber',
    ACTIVE: 'active',
    EASY_PAY_STATUS: 'easyPayStatus',
    EASY_PAY_EFFECTIVE_FROM: 'easyPayEffectiveFrom',
    CITY: 'address.city',
    STATE: 'address.state',
    ADDRESS_COUNTRY: 'address.country',
    POSTAL_CODE: 'address.postalCode',
    LOCALITY: 'address.address2',
    EMAIL: 'contact.email',
    TELEPHONE: 'contact.telephone',
    FIRST_NAME: 'contact.firstName',
    LAST_NAME: 'contact.lastName'
  },
  COLUMNS: [
    {
      prop: 'globalAirline.iataCode',
      name: 'MASTER_DATA.airline.columns.code',
      resizeable: false,
      draggable: false,
      minWidth: 100
    },
    {
      prop: 'localName',
      name: 'MASTER_DATA.airline.columns.name',
      cellTemplate: 'commonLinkCellTmpl',
      resizeable: true,
      draggable: false
    },
    {
      prop: 'designator',
      name: 'MASTER_DATA.airline.columns.designator',
      resizeable: true,
      draggable: false
    },
    {
      prop: 'effectiveFrom',
      name: 'MASTER_DATA.airline.columns.registerDate',
      resizeable: true,
      cellTemplate: 'dayMonthYearCellTmpl',
      draggable: false
    },
    {
      prop: 'effectiveTo',
      name: 'MASTER_DATA.airline.columns.expiryDate',
      resizeable: true,
      cellTemplate: 'dayMonthYearCellTmpl',
      draggable: false
    },
    {
      prop: 'active',
      name: 'MASTER_DATA.airline.columns.status',
      resizeable: true,
      draggable: false,
      cellTemplate: 'statusTmpl'
    }
  ],
  SEARCH_FORM_DEFAULT_VALUE: {
    airlines: null,
    country: null,
    status: null,
    registerDate: '',
    expiryDate: '',
    designator: '',
    vatNumber: '',
    active: '',
    easyPayStatus: null,
    easyPayEffectiveFrom: '',
    city: '',
    state: '',
    addressCountry: '',
    postalCode: '',
    email: '',
    telephone: '',
    name: '',
    locality: ''
  }
};
