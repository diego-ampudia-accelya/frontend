import { createAction, props } from '@ngrx/store';

import { Bsp } from '~app/shared/models/bsp.model';

export const updateBsp = createAction('[Airline Properties] Update BSP', props<{ selectedBsp: Bsp }>());
