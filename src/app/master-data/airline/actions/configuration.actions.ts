import { createAction, props } from '@ngrx/store';

import { Airline } from '~app/master-data/models';
import { Bsp } from '~app/shared/models/bsp.model';

// TODO: not used, to be removed (?)
export const selectAirline = createAction('[Airline Configuration] Select Airline', props<{ airline: Airline }>());

export const updateSelectedBsp = createAction(
  '[Airline Configuration] Update selected BSP',
  props<{ selectedBsp: Bsp }>()
);
