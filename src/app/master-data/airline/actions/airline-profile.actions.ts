import { createAction, props } from '@ngrx/store';

import { GlobalAirline } from '~app/master-data/models';

export const loadGlobalAirline = createAction(
  '[Airline Profile] Load Global Airline and Default Airline',
  props<{ globalAirline: GlobalAirline; isOwnProfile: boolean; defaultIsoc?: string }>()
);
