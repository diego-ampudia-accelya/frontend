import { createAction, props } from '@ngrx/store';

import { AdmPolicyConfiguration } from '../configuration/models/adm-policy.model';
import { ConfigurationScope, SettingCategory } from '~app/master-data/configuration';

export const modify = createAction('[Adm Policies Configuration] Modify', props<{ value: AdmPolicyConfiguration }>());

export const load = createAction(
  '[Adm Policies Configuration] Load',
  props<{ scope: ConfigurationScope; settingConfigurations: SettingCategory[] }>()
);

export const loadSuccess = createAction(
  '[Adm Policies Configuration] Load Success',
  props<{ value: AdmPolicyConfiguration }>()
);

export const loadError = createAction('[Adm Policies Configuration] Load Error');

export const invalid = createAction('[Adm Policies Configuration] Invalid');

export const openApplyChanges = createAction('[Adm Policies Configuration] Open Apply Changes');

export const applyChangesSuccess = createAction(
  '[Adm Policies Configuration] Apply Changes Success',
  props<{ value: AdmPolicyConfiguration }>()
);

export const applyChangesError = createAction('[Adm Policies Configuration] Apply Changes Error', props<{ error }>());

export const discard = createAction('[Adm Policies Configuration] Discard');
