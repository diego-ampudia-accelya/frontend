import * as AdmPoliciesActions from './adm-policies.actions';
import * as ProfileActions from './airline-profile.actions';
import * as PropertiesActions from './airline-properties.actions';
import * as ConfigurationActions from './configuration.actions';

export { ProfileActions, PropertiesActions, ConfigurationActions, AdmPoliciesActions };
