import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AirlineService } from '~app/master-data/services';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import { DialogComponent, DialogService } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { SortOrder } from '~app/shared/enums';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AirlineSummary } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { AirlineDictionaryService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { AirlinesListService } from '../../services';
import { AirlinesDisplayFilterFormatter } from '../../services/airline-display-filter-formatter.service';
import { AirlineListComponent } from './airline-list.component';

describe('AirlineList component', () => {
  let component: AirlineListComponent;
  let fixture: ComponentFixture<AirlineListComponent>;
  let queryableDataSourceMock: SpyObject<QueryableDataSource<any>>;
  let airlineServiceMock: SpyObject<AirlineService>;
  let queryStorageMock: SpyObject<DefaultQueryStorage>;
  let routerMock: SpyObject<Router>;
  let airlineListServiceMock: SpyObject<AirlinesListService>;
  let formBuilderMock: SpyObject<FormBuilder>;
  let activatedRouteMock: SpyObject<ActivatedRoute>;
  let mockStore: MockStore;
  const dialogServiceSpy = createSpyObject(DialogService);

  const query = {
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  } as DataQuery;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    core: {
      menu: {
        tabs: { dashboard: { ...ROUTES.DASHBOARD, id: 'dashboard' } },
        activeTabId: 'dashboard'
      },
      viewListsInfo: {}
    }
  };

  const columns: TableColumn[] = [
    {
      prop: 'globalAirline.iataCode',
      name: 'MASTER_DATA.airline.columns.code',
      resizeable: false,
      draggable: false,
      minWidth: 100
    },
    {
      prop: 'localName',
      name: 'MASTER_DATA.airline.columns.name',
      cellTemplate: 'commonLinkCellTmpl',
      resizeable: true,
      draggable: false
    },
    {
      prop: 'designator',
      name: 'MASTER_DATA.airline.columns.designator',
      resizeable: true,
      draggable: false
    },
    {
      prop: 'effectiveFrom',
      name: 'MASTER_DATA.airline.columns.registerDate',
      resizeable: true,
      cellTemplate: 'dayMonthYearCellTmpl',
      draggable: false
    },
    {
      prop: 'effectiveTo',
      name: 'MASTER_DATA.airline.columns.expiryDate',
      resizeable: true,
      cellTemplate: 'dayMonthYearCellTmpl',
      draggable: false
    },
    {
      prop: 'active',
      name: 'MASTER_DATA.airline.columns.status',
      resizeable: true,
      draggable: false,
      cellTemplate: 'statusTmpl'
    }
  ];

  const mockRow = {
    id: 1,
    globalAirline: {
      iataCode: 'iataCode'
    },
    localName: 'name',
    designator: 'designator'
  };

  beforeEach(waitForAsync(() => {
    queryableDataSourceMock = createSpyObject(QueryableDataSource, { appliedQuery$: of(query) });
    queryableDataSourceMock.get.and.returnValue(of(query));

    queryStorageMock = createSpyObject(DefaultQueryStorage);
    queryStorageMock.get.and.returnValue(query);

    TestBed.configureTestingModule({
      declarations: [AirlineListComponent, TranslatePipeMock, DialogComponent],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        mockProvider(FormBuilder),
        mockProvider(AirlinesListService),
        mockProvider(L10nTranslationService),
        mockProvider(AirlinesDisplayFilterFormatter),
        provideMockStore({ initialState }),
        mockProvider(Router),
        mockProvider(AirlineService),
        mockProvider(AirlineDictionaryService),
        mockProvider(ActivatedRoute),
        mockProvider(PermissionsService)
      ]
    })
      .overrideComponent(AirlineListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceMock },
            { provide: DefaultQueryStorage, useValue: queryStorageMock },
            { provide: DialogService, useValue: dialogServiceSpy }
          ]
        }
      })
      .compileComponents();

    airlineListServiceMock = TestBed.inject<any>(AirlinesListService);
    airlineListServiceMock.generateColumns.and.returnValue(columns);

    airlineServiceMock = TestBed.inject<any>(AirlineService);
    airlineServiceMock.find.and.returnValue(of([]));
    routerMock = TestBed.inject<any>(Router);

    formBuilderMock = TestBed.inject<any>(FormBuilder);
    formBuilderMock.group.and.returnValue({
      iataCode: null,
      country: null,
      status: null,
      registerDate: '',
      expiryDate: '',
      designator: '',
      vatNumber: '',
      active: '',
      easyPayStatus: null,
      easyPayEffectiveFrom: '',
      city: '',
      state: '',
      addressCountry: '',
      postalCode: '',
      email: '',
      telephone: '',
      name: ''
    });

    activatedRouteMock = TestBed.inject<any>(ActivatedRoute);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirlineListComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('loadData should set a default sorting if there is no such', () => {
    component.loadData(query);
    const expected = jasmine.objectContaining({
      sortBy: [{ attribute: 'globalAirline.iataCode', sortType: SortOrder.Asc }]
    });

    expect(queryableDataSourceMock.get).toHaveBeenCalledWith(expected);
  });

  it('loadData should NOT set a default sorting if there is such', () => {
    component.loadData({ ...query, sortBy: [{ attribute: 'key', sortType: SortOrder.Desc }] });
    const expected = jasmine.objectContaining({
      sortBy: [{ attribute: 'key', sortType: SortOrder.Desc }]
    });

    expect(queryableDataSourceMock.get).toHaveBeenCalledWith(expected);
  });

  it('should set columns in ngOnInit', () => {
    component.columns = null;

    const expectedColumns = [
      jasmine.objectContaining({
        prop: 'globalAirline.iataCode',
        name: 'MASTER_DATA.airline.columns.code',
        resizeable: false,
        draggable: false,
        minWidth: 100
      }),
      jasmine.objectContaining({
        prop: 'localName',
        name: 'MASTER_DATA.airline.columns.name',
        cellTemplate: 'commonLinkCellTmpl',
        resizeable: true,
        draggable: false
      }),
      jasmine.objectContaining({
        prop: 'designator',
        name: 'MASTER_DATA.airline.columns.designator',
        resizeable: true,
        draggable: false
      }),
      jasmine.objectContaining({
        prop: 'effectiveFrom',
        name: 'MASTER_DATA.airline.columns.registerDate',
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        draggable: false
      }),
      jasmine.objectContaining({
        prop: 'effectiveTo',
        name: 'MASTER_DATA.airline.columns.expiryDate',
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        draggable: false
      }),
      jasmine.objectContaining({
        prop: 'active',
        name: 'MASTER_DATA.airline.columns.status',
        resizeable: true,
        draggable: false,
        cellTemplate: 'statusTmpl'
      })
    ] as TableColumn[];

    component.ngOnInit();

    expect(component.columns).toEqual(expectedColumns);
  });

  it('should set search form fields in ngOnInit', () => {
    component.searchForm = null;

    const expectedFields = formBuilderMock.group({
      iataCode: null,
      country: null,
      status: null,
      registerDate: '',
      expiryDate: '',
      designator: '',
      vatNumber: '',
      active: '',
      easyPayStatus: null,
      easyPayEffectiveFrom: '',
      city: '',
      state: '',
      addressCountry: '',
      postalCode: '',
      email: '',
      telephone: '',
      name: ''
    });

    component.ngOnInit();

    expect(component.searchForm).toEqual(expectedFields);
  });

  it('should load data with current query', () => {
    spyOn(component, 'loadData');

    component.ngOnInit();

    expect(component.loadData).toHaveBeenCalledWith(query);
  });

  it('should load data with default query if there is no current', () => {
    queryStorageMock.get.and.returnValue(null);

    spyOn(component, 'loadData');

    component.ngOnInit();

    expect(component.loadData).toHaveBeenCalledWith(cloneDeep(defaultQuery));
  });

  it('should navigate to selected airline profile on OnViewAirline', () => {
    component.onViewAirline(123);

    expect(routerMock.navigate).toHaveBeenCalledWith([123], { relativeTo: activatedRouteMock });
  });

  it('should call onViewAirline if from action menu is chosen View', () => {
    spyOn(component, 'onViewAirline');

    const params = {
      action: {
        actionType: GridTableActionType.View
      },
      row: {
        id: 123
      },
      event: null
    };

    component.onActionClick(params);

    expect(component.onViewAirline).toHaveBeenCalledWith(123);
  });

  it('should navigate to Ticketing Authority if from action menu is chosen ManageTa', () => {
    const params = {
      action: {
        actionType: GridTableActionType.ViewTA
      },
      row: {
        id: 123
      },
      event: null
    };

    component.onActionClick(params);

    expect(routerMock.navigate).toHaveBeenCalledWith([123, 'ticketing-authority'], { relativeTo: activatedRouteMock });
  });

  it('should change the state of showAdvancedSearch', () => {
    component.showAdvancedSearch = false;

    component.toggleAdvancedSearch();

    expect(component.showAdvancedSearch).toBe(true);
  });

  it('should not set ticketing authority filters if the users are Agent or Agent Group when setTicketingAuthorityFilters method is triggered', () => {
    component['setTicketingAuthorityFilters'](mockRow);
    spyOn(mockStore, 'dispatch');

    expect(mockStore.dispatch).not.toHaveBeenCalled();
  });

  it('should set ticketing authority filters if the users are different than Agent or Agent Group when setTicketingAuthorityFilters method is triggered', () => {
    component['userType'] = UserType.AGENT_GROUP;
    const mockAirline: AirlineSummary = {
      id: mockRow.id,
      code: mockRow.globalAirline.iataCode,
      name: mockRow.localName,
      designator: mockRow.designator
    };
    const requiredFilter = { airlines: [mockAirline] };
    spyOn(mockStore, 'dispatch');

    component['setTicketingAuthorityFilters'](mockRow);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter })
    );
  });

  it('should get route to ticketing authority when the user type is an agent group', () => {
    component['userType'] = UserType.AGENT_GROUP;

    const expectedResult = ['/master-data/agent-group/my/ticketing-authority'];
    const result = component['geRouteToTicketingAuthority'](mockRow);

    expect(result).toEqual(expectedResult);
  });

  it('should get route to ticketing authority when the user type is an agent', () => {
    component['userType'] = UserType.AGENT;

    const expectedResult = ['/master-data/agent/my/ticketing-authority'];
    const result = component['geRouteToTicketingAuthority'](mockRow);

    expect(result).toEqual(expectedResult);
  });

  it('should trigger download and open a dialog from dialog service', () => {
    component.download();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
