import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';
import moment from 'moment-mini';
import { Observable, Subject } from 'rxjs';
import { first, map, shareReplay, takeUntil } from 'rxjs/operators';

import { AIRLINES_LIST } from '../../airline-list.constants';
import { AirlinesListService } from '../../services';
import { AirlinesDisplayFilterFormatter } from '../../services/airline-display-filter-formatter.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AirlineService } from '~app/master-data/services';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { countryList } from '~app/shared/constants/country-list';
import { EasyPayStatus, SortOrder } from '~app/shared/enums';
import { ArrayHelper } from '~app/shared/helpers';
import { AirlineSummary } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AirlineDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-airline-list',
  templateUrl: './airline-list.component.html',
  styleUrls: ['./airline-list.component.scss'],
  providers: [
    AirlineService,
    AirlinesDisplayFilterFormatter,
    DefaultQueryStorage,
    { provide: QUERYABLE, useClass: AirlineService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [AirlineService]
    }
  ]
})
export class AirlineListComponent implements OnInit, OnDestroy {
  public columns: TableColumn[];
  public searchForm: FormGroup;
  public actions: Array<{ action: GridTableActionType; hidden?: boolean }>;
  public showAdvancedSearch = false;

  public activeOptions: DropdownOption[] = [
    { value: true, label: this.translationService.translate('MASTER_DATA.airline.activeOptions.active') },
    { value: false, label: this.translationService.translate('MASTER_DATA.airline.activeOptions.inactive') }
  ];
  public easyPayStatusOptions: DropdownOption[] = ArrayHelper.toDropdownOptions(Object.values(EasyPayStatus));
  public addressCountriesList: DropdownOption[] = ArrayHelper.toSameValueAndLabelByProperty(countryList, 'name');

  public airlineCodeOptions$: Observable<DropdownOption<AirlineSummary>[]>;
  public designatorOptions$: Observable<DropdownOption[]>;
  public header = this.translationService.translate('airline.viewTitle');
  public userBsps$ = this.store.select(fromAuth.getAllBspNamesJoined);
  public moment = moment;

  private userType: UserType;
  private destroy$ = new Subject();
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(fromAuth.getUser), first());
  }
  private hasReadTicketingAuthorityPermission = this.permissionsService.hasPermission(
    Permissions.readTicketingAuthority
  );

  constructor(
    public dataSource: QueryableDataSource<AirlineService>,
    public filterFormatter: AirlinesDisplayFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private airlineListService: AirlinesListService,
    private airlineService: AirlineService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private airlineDictionaryService: AirlineDictionaryService,
    private store: Store<AppState>,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.buildSearchForm();
    this.columns = this.airlineListService.generateColumns();
    this.populateFilterDropdowns();
    const query = this.queryStorage.get() || defaultQuery;
    this.loadData(query);
  }

  private initializeLoggedUser() {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.userType = loggedUser.userType;
      this.initializeActions();
    });
  }

  private initializeActions(): void {
    this.actions = [
      { action: GridTableActionType.View },
      {
        action: this.userType === UserType.IATA ? GridTableActionType.ManageTA : GridTableActionType.ViewTA,
        hidden:
          !this.hasReadTicketingAuthorityPermission ||
          (this.userType !== UserType.IATA && this.userType !== UserType.GDS)
      }
    ];
  }

  public loadData(query: DataQuery): void {
    if (isEmpty(query.sortBy)) {
      const sortBy = [{ attribute: 'globalAirline.iataCode', sortType: SortOrder.Asc }];
      query = { ...query, sortBy };
    }

    this.dataSource.get(query);
    this.queryStorage.save(query);
  }

  public onViewAirline(id: number) {
    this.router.navigate([id], { relativeTo: this.activatedRoute });
  }

  private setTicketingAuthorityFilters(row: any): void {
    if (this.userType !== UserType.AGENT && this.userType !== UserType.AGENT_GROUP) {
      return;
    }

    const airline: AirlineSummary = {
      id: row.id,
      code: row.globalAirline.iataCode,
      name: row.localName,
      designator: row.designator
    };
    const requiredFilter = { airlines: [airline] };
    // With this actions the List query is updated with the airline filter
    this.store.dispatch(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter: { airlines: [] } }));
    this.store.dispatch(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));
  }

  public onActionClick({ action, row }: { action: any; row: any; event: MouseEvent }) {
    if (action.actionType === GridTableActionType.ViewTA || action.actionType === GridTableActionType.ManageTA) {
      this.setTicketingAuthorityFilters(row);
      const route = this.geRouteToTicketingAuthority(row);

      this.router.navigate(route, { relativeTo: this.activatedRoute });
    }

    if (action.actionType === GridTableActionType.View) {
      this.onViewAirline(row.id);
    }
  }

  public download() {
    const requestQuery = this.queryStorage.get();

    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: requestQuery
      },
      apiService: this.airlineService
    });
  }

  public toggleAdvancedSearch() {
    this.showAdvancedSearch = !this.showAdvancedSearch;
  }

  private geRouteToTicketingAuthority(row: any): string[] {
    if (this.userType === UserType.AGENT_GROUP) {
      return [ROUTES.MY_AGENT_GROUP.url + '/ticketing-authority'];
    }

    if (this.userType === UserType.AGENT) {
      return [ROUTES.MY_AGENT.url + '/ticketing-authority'];
    }

    return [row.id, 'ticketing-authority'];
  }

  private populateFilterDropdowns() {
    const query = {
      filterBy: [],
      sortBy: [],
      paginateBy: {
        page: 0,
        size: 9999
      }
    };

    const airlines$ = this.airlineService.find(query).pipe(
      map(results => results.records || []),
      shareReplay()
    );

    this.airlineCodeOptions$ = this.airlineDictionaryService.getDropdownOptions();

    this.designatorOptions$ = airlines$.pipe(
      map(pagedData => ArrayHelper.toSameValueAndLabelByProperty(pagedData, 'designator'))
    );
  }

  private buildSearchForm(): void {
    this.searchForm = this.formBuilder.group(AIRLINES_LIST.SEARCH_FORM_DEFAULT_VALUE);
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
