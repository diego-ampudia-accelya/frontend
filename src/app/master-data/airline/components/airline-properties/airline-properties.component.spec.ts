import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { PropertiesActions } from '../../actions';
import { getIsOwnProfile } from '../../reducers';
import { AirlinesDisplayFilterFormatter } from '../../services/airline-display-filter-formatter.service';
import { AirlinePropertiesComponent } from './airline-properties.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { createAirlineMultiCountryUser } from '~app/shared/mocks/airline-user';
import { DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { TranslatePipeMock } from '~app/test';

describe('AirlinePropertiesComponent', () => {
  let fixture: ComponentFixture<AirlinePropertiesComponent>;
  let component: AirlinePropertiesComponent;
  let mockStore: MockStore<AppState>;

  const permissionsServiceSpy = createSpyObject(PermissionsService);

  const airlineUserMock = createAirlineMultiCountryUser();

  const initialState = {
    auth: { user: airlineUserMock },
    airline: {
      profile: {
        globalAirline: airlineUserMock.globalAirline,
        defaultAirline: airlineUserMock.globalAirline.airlines[0],
        isOwnProfile: true
      },
      properties: {
        selectedBsp: {
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN',
          effectiveFrom: '2000-01-01'
        }
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AirlinePropertiesComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        provideMockStore({ initialState }),
        { provide: PermissionsService, useValue: permissionsServiceSpy }
      ]
    })
      .overrideComponent(AirlinePropertiesComponent, {
        set: { providers: [mockProvider(AirlinesDisplayFilterFormatter)] }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirlinePropertiesComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject<any>(Store);

    // Setting LEAN permission by default
    permissionsServiceSpy.hasPermission.and.returnValue(true);

    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  describe('isFilterValid$', () => {
    it('filter form should NOT be valid if selected BSP and filter form BSP are equal', fakeAsync(() => {
      let isFilterValid: boolean;

      component.isFilterValid$.subscribe(value => (isFilterValid = value));

      // Patching the existing selected BSP
      component.filterForm.patchValue({
        bsp: {
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN',
          effectiveFrom: '2000-01-01'
        }
      });
      tick();
      expect(isFilterValid).toBe(false);

      // Patching another different BSP
      component.filterForm.patchValue({
        bsp: {
          id: 10006,
          isoCountryCode: 'GR',
          name: 'Greece',
          effectiveFrom: '2019-05-10'
        }
      });
      tick();
      expect(isFilterValid).toBe(true);
    }));
  });

  describe('isFilterEnabled', () => {
    it('should enable filter if it is own profile and user has LEAN permission', fakeAsync(() => {
      // `isOwnProfile` and LEAN permission are true by default
      expect(component.isFilterEnabled).toBe(true);
    }));

    it('should NOT enable filter if it is own profile and user has NOT LEAN permission', fakeAsync(() => {
      permissionsServiceSpy.hasPermission.and.returnValue(false);

      component.ngOnInit();
      tick();

      expect(component.isFilterEnabled).toBe(false);
    }));

    it('should NOT enable filter if it is NOT own profile and user has LEAN permission', fakeAsync(() => {
      mockStore.overrideSelector(getIsOwnProfile, false);

      component.ngOnInit();
      tick();

      expect(component.isFilterEnabled).toBe(false);
    }));
  });

  describe('saveFilters', () => {
    it('should set loading properties when saving filters', fakeAsync(() => {
      let loadingProperties: boolean;

      component.loadingProperties$.subscribe(value => (loadingProperties = value));
      component.saveFilters();

      expect(loadingProperties).toBe(true);
    }));

    it('should update selected BSP', fakeAsync(() => {
      const mockBsp: Bsp = {
        id: 1,
        isoCountryCode: 'ES',
        name: 'SPAIN',
        effectiveFrom: '2000-01-01'
      };

      spyOn(mockStore, 'dispatch').and.callThrough();

      component.saveFilters({ bsp: mockBsp });
      tick();

      expect(mockStore.dispatch).toHaveBeenCalledWith(PropertiesActions.updateBsp({ selectedBsp: mockBsp }));
    }));

    it('should unset loading properties when updating selected BSP', fakeAsync(() => {
      let loadingProperties: boolean;

      const mockBsp: Bsp = {
        id: 10006,
        isoCountryCode: 'GR',
        name: 'Greece',
        effectiveFrom: '2019-05-10'
      };

      component.selectedAirline$.subscribe();
      component.loadingProperties$.subscribe(value => (loadingProperties = value));
      component.saveFilters({ bsp: mockBsp });
      expect(loadingProperties).toBe(true);

      tick(1000);

      expect(loadingProperties).toBe(false);
    }));
  });

  it('should initialize BSP dropdown options', fakeAsync(() => {
    let options: DropdownOption<Bsp>[];

    component.ngOnInit();
    component.bspCountriesList$.subscribe(list => (options = list));
    tick();

    expect(options).toEqual([
      {
        value: {
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN',
          effectiveFrom: '2000-01-01',
          effectiveTo: null,
          version: 192,
          defaultCurrencyCode: null,
          active: true
        },
        label: 'ES - SPAIN'
      },
      {
        value: {
          id: 10006,
          isoCountryCode: 'GR',
          name: 'Greece',
          effectiveFrom: '2019-05-10',
          effectiveTo: '2019-05-31',
          version: 3
        },
        label: 'GR - Greece'
      }
    ]);
  }));

  it('should initialize stored filters in form', fakeAsync(() => {
    component.ngOnInit();
    tick();

    expect(component.filterForm.value.bsp).toEqual({
      id: 1,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01'
    });
  }));
});
