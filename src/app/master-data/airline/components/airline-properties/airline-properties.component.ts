import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { delay, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { PropertiesActions } from '../../actions';
import { getGlobalAirline, getIsOwnProfile, getPropertiesAirline, getPropertiesSelectedBsp } from '../../reducers';
import { AirlinesDisplayFilterFormatter } from '../../services/airline-display-filter-formatter.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Airline } from '~app/master-data/models';
import { AirlineProfileBspFilter } from '~app/master-data/models/airline-profile-bsp-filter.model';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Component({
  selector: 'bspl-airline-properties',
  templateUrl: './airline-properties.component.html',
  styleUrls: ['./airline-properties.component.scss'],
  providers: [AirlinesDisplayFilterFormatter]
})
export class AirlinePropertiesComponent implements OnInit {
  public subtabTitle = 'menu.masterData.airlineProfile.properties.title';
  public predefinedFilters: Partial<AirlineProfileBspFilter> = { bsp: null };
  public filterForm: FormGroup;

  public bspCountriesList$: Observable<DropdownOption<Bsp>[]>;

  public isFilterValid$: Observable<boolean>;
  public isFilterEnabled: boolean;

  public selectedAirline$: Observable<Airline> = this.store.pipe(
    select(getPropertiesAirline),
    // The response is instantaneous, delaying the response to keep the spinner a bit longer,
    // but first emitted value should not be delayed, only the next ones
    switchMap((airline, index) => of(airline).pipe(delay(index > 0 ? 500 : 0))),
    tap(_ => this.setLoadingProperties(false)) // Remove loading spinner
  );

  public logoUrl$: Observable<string> = this.selectedAirline$.pipe(
    map(airline => airline && appConfiguration.getAirlineLogoUrl(airline.globalAirline.iataCode))
  );

  private _loadingPropertiesSubject = new BehaviorSubject<boolean>(false);
  public loadingProperties$ = this._loadingPropertiesSubject.asObservable();

  private formUtil: FormUtil;
  private destroy$ = new Subject();

  constructor(
    public displayFormatter: AirlinesDisplayFilterFormatter,
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private fb: FormBuilder
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    this.store.pipe(select(getIsOwnProfile), first()).subscribe(isOwnProfile => {
      this.isFilterEnabled = isOwnProfile && hasLeanPermission;

      if (this.isFilterEnabled) {
        this.buildFilterForm();

        this.initializeFilterFormDropdowns();
        this.initializeFilterFormListeners();
      }
    });
  }

  public saveFilters(value?: AirlineProfileBspFilter): void {
    const { bsp } = value || this.filterForm.value;

    // Set loading spinner
    this.setLoadingProperties(true);

    this.store.dispatch(PropertiesActions.updateBsp({ selectedBsp: bsp }));
  }

  private buildFilterForm(): void {
    this.filterForm = this.formUtil.createGroup<AirlineProfileBspFilter>({
      bsp: [null, Validators.required]
    });
  }

  private initializeFilterFormDropdowns(): void {
    this.bspCountriesList$ = this.store.pipe(
      select(getGlobalAirline),
      map(globalAirline => globalAirline.airlines.map(({ bsp }) => bsp)),
      map(bsps => bsps.map(toValueLabelObjectBsp) as DropdownOption<Bsp>[])
    );
  }

  private initializeFilterFormListeners(): void {
    this.initializeStoredFiltersListener();
    this.initializeFormValidityListener();
  }

  private initializeStoredFiltersListener(): void {
    this.store.pipe(select(getPropertiesSelectedBsp), takeUntil(this.destroy$)).subscribe(selectedBsp => {
      const filterFormBsp: Bsp = this.filterForm.value.bsp;

      if (selectedBsp && filterFormBsp?.id !== selectedBsp.id) {
        this.filterForm.patchValue({ bsp: selectedBsp });
      }
    });
  }

  private initializeFormValidityListener(): void {
    // Filter form will be invalid if selected BSP and filter form BSP are equal
    this.isFilterValid$ = combineLatest([
      this.store.pipe(select(getPropertiesSelectedBsp)),
      this.filterForm.valueChanges
    ]).pipe(map(([selectedBsp, filterFormValue]) => selectedBsp.id !== filterFormValue.bsp?.id));
  }

  private setLoadingProperties(value: boolean): void {
    this._loadingPropertiesSubject.next(value);
  }
}
