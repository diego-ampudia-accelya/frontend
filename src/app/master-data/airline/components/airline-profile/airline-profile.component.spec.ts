import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockComponent } from 'ng-mocks';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ChangesDialogService as AcdmUnsavedChangesDialogService } from '~app/master-data/adm-acm-authority';
import { AdmAcmAuthorityListActions } from '~app/master-data/adm-acm-authority/store/actions';
import * as fromAdmAcmAuthority from '~app/master-data/adm-acm-authority/store/reducers';
import * as fromSettings from '~app/master-data/configuration';
import { MenuBuilder } from '~app/master-data/configuration';
import { ChangesDialogService as SettingChangesDialogService } from '~app/master-data/configuration/changes-dialog/changes-dialog.service';
import { ChangesDialogService } from '~app/master-data/refund-authority';
import * as fromRefundAuthority from '~app/master-data/refund-authority/store/reducers';
import { ChangesDialogService as TaUnsavedChangesDialogService } from '~app/master-data/ticketing-authority';
import * as fromTicketingAuthority from '~app/master-data/ticketing-authority/store/reducers';
import { SelectComponent } from '~app/shared/components';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { UserType } from '~app/shared/models/user.model';
import { TranslatePipeMock } from '~app/test';
import { AirlineProfileComponent } from './airline-profile.component';

const tab = {
  allowedUserTypes: ['iata'],
  route: '/master-data/airline/10234/configuration',
  title: 'menu.masterData.configuration.title',
  active: false,
  url: './configuration',
  isAccessible: true
};
class MenuBuilderMock {
  buildMenuItemsFrom(route) {
    return [tab];
  }
}

describe('AirlineProfileComponent', () => {
  let fixture: ComponentFixture<AirlineProfileComponent>;
  let component: AirlineProfileComponent;
  const activatedRouteStub = {
    snapshot: {}
  };
  let store: MockStore<{ initialState }>;
  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: { state: { params: {} } },
    airline: {
      profile: {
        globalAirline: {
          globalName: 'Aegean Airlines'
        }
      }
    },
    core: {
      menu: {
        tabs: {}
      }
    },
    configuration: {
      settings: {
        isChanged: false
      },
      admPolicies: {
        isLoading: false,
        isChanged: false,
        originalValue: null,
        value: null,
        scope: null,
        settingConfigurations: [],
        isInvalid: false,
        errors: null
      }
    }
  };

  let refundAuthorityChangesDialogStub: SpyObject<ChangesDialogService>;
  let taUnsavedChangesDialogServiceStub: SpyObject<TaUnsavedChangesDialogService>;
  let acdmUnsavedChangesDialogServiceStub: SpyObject<AcdmUnsavedChangesDialogService>;
  let configurationUnsavedChangesDialogServiceStub: SpyObject<SettingChangesDialogService>;
  let permissionsService: SpyObject<PermissionsService>;
  let router: Router;

  beforeEach(waitForAsync(() => {
    refundAuthorityChangesDialogStub = createSpyObject(ChangesDialogService);
    taUnsavedChangesDialogServiceStub = createSpyObject(TaUnsavedChangesDialogService);
    acdmUnsavedChangesDialogServiceStub = createSpyObject(AcdmUnsavedChangesDialogService);
    configurationUnsavedChangesDialogServiceStub = createSpyObject(SettingChangesDialogService);
    permissionsService = createSpyObject(PermissionsService);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [AirlineProfileComponent, TranslatePipeMock, MockComponent(SelectComponent)],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromRefundAuthority.hasChanges, value: false },
            { selector: fromTicketingAuthority.hasChanges, value: false },
            { selector: fromAdmAcmAuthority.hasChanges, value: false },
            { selector: fromSettings.canApplyChanges, value: false }
          ]
        }),
        { provide: MenuBuilder, useClass: MenuBuilderMock },
        { provide: TaUnsavedChangesDialogService, useValue: taUnsavedChangesDialogServiceStub },
        { provide: AcdmUnsavedChangesDialogService, useValue: acdmUnsavedChangesDialogServiceStub },
        { provide: ChangesDialogService, useValue: refundAuthorityChangesDialogStub },
        { provide: SettingChangesDialogService, useValue: configurationUnsavedChangesDialogServiceStub },
        { provide: PermissionsService, useValue: permissionsService }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirlineProfileComponent);
    store = TestBed.inject<any>(Store);
    router = TestBed.inject<any>(Router);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch save action when applyChanges method is called', () => {
    spyOn(store, 'dispatch');
    spyOn(router, 'isActive').and.returnValue(true);
    component.currentSavableTab = { url: 'someUrl', applyChangesAction: AdmAcmAuthorityListActions.openApplyChanges() };

    component.applyChanges();

    expect(store.dispatch).toHaveBeenCalledWith(AdmAcmAuthorityListActions.openApplyChanges());
  });

  it('should get tabs when the user is equal to an Agent and getTabs method is triggered', () => {
    const result = component['getTabs'](UserType.AGENT);

    expect(result).toEqual([tab]);
  });
});
