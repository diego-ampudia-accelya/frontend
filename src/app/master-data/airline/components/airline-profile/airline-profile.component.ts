import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { AdmPoliciesActions } from '../../actions';
import { getGlobalAirline } from '../../reducers';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AddressMaintenanceActions, fromAddressMaintenance } from '~app/master-data/address-maintenance/store';
import { AdmAcmAuthorityListActions } from '~app/master-data/adm-acm-authority/store/actions';
import * as fromAcdmAuthority from '~app/master-data/adm-acm-authority/store/reducers';
import * as fromSettings from '~app/master-data/configuration';
import { TipGlobalAirlineActions } from '~app/master-data/configuration/tip/tip-global-airline/store/actions';
import { TipLocalAirlineActions } from '~app/master-data/configuration/tip/tip-local-airline/store/actions';
import { fromTipLocalAirline } from '~app/master-data/configuration/tip/tip-local-airline/store/reducers/index';
import { GlobalAirline } from '~app/master-data/models';
import { RefundAuthorityListActions } from '~app/master-data/refund-authority/store/actions';
import * as fromRefundAuthority from '~app/master-data/refund-authority/store/reducers';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import * as fromTicketingAuthority from '~app/master-data/ticketing-authority/store/reducers';
import { VariableRemittanceListActions } from '~app/master-data/variable-remittance/store/actions';
import * as fromVariableRemittance from '~app/master-data/variable-remittance/store/reducers';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { Permissions } from '~app/shared/constants/permissions';
import { SelectMode } from '~app/shared/enums';
import { UserType } from '~app/shared/models/user.model';
import { or } from '~app/shared/utils';

@Component({
  selector: 'bspl-airline-profile',
  templateUrl: './airline-profile.component.html',
  styleUrls: ['./airline-profile.component.scss']
})
export class AirlineProfileComponent implements OnInit, OnDestroy {
  public globalAirline$: Observable<GlobalAirline> = this.store.pipe(select(getGlobalAirline));
  public tabs: fromSettings.RoutedMenuItem[];
  public isApplyChangesEnabled$: Observable<boolean>;
  public isApplyChangesVisible: boolean;

  public btnDesign = ButtonDesign;
  public selectMode = SelectMode;
  public currentSavableTab = null;

  private loggedUserType$: Observable<UserType> = this.store.pipe(select(getUserType));
  private destroyed$ = new Subject();

  private settingSavableTab = {
    canApplyChangesSelector: fromSettings.canApplyChanges,
    applyChangesAction: fromSettings.SettingConfigurationActions.openApplyChanges(),
    applyChangesPermission: Permissions.updateAirlineSettings
  };

  private savableTabs = [
    {
      url: './configuration/notifications',
      canApplyChangesSelector: fromSettings.canApplyChanges,
      applyChangesAction: fromSettings.SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateNotificationSettings
    },
    {
      url: './adm-acm-authority',
      canApplyChangesSelector: fromAcdmAuthority.hasChanges,
      applyChangesAction: AdmAcmAuthorityListActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateACDMAuthorityAirline
    },
    {
      url: './refund-authority',
      hasApplyChangesSelector: fromRefundAuthority.hasApplyChanges,
      canApplyChangesSelector: fromRefundAuthority.hasChanges,
      applyChangesAction: RefundAuthorityListActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateRefundIssueAirline
    },
    {
      url: './ticketing-authority',
      canApplyChangesSelector: fromTicketingAuthority.hasChanges,
      applyChangesAction: TicketingAuthorityListActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateTicketingAuthority
    },
    {
      url: './configuration/refund-settings',
      ...this.settingSavableTab
    },
    {
      url: './configuration/basic-settings',
      ...this.settingSavableTab
    },
    {
      url: './configuration/adm-policies',
      canApplyChangesSelector: fromSettings.hasPolicyValidChanges,
      applyChangesAction: AdmPoliciesActions.openApplyChanges()
    },
    {
      url: './configuration/ta-settings',
      ...this.settingSavableTab
    },
    {
      url: './configuration/pbd-settings',
      canApplyChangesSelector: fromSettings.canApplyChanges,
      applyChangesAction: fromSettings.SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updatePBDAirline
    },
    {
      url: './configuration/email-alerts-settings',
      canApplyChangesSelector: fromSettings.canApplyChanges,
      applyChangesAction: fromSettings.SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateEmailAlerts
    },
    {
      url: './variable-remittance',
      canApplyChangesSelector: fromVariableRemittance.hasChanges,
      applyChangesAction: VariableRemittanceListActions.applyChanges(),
      applyChangesPermission: Permissions.updateRemittanceFrequency
    },
    {
      url: './contact-info',
      canApplyChangesSelector: fromAddressMaintenance.canApplyChanges,
      applyChangesAction: AddressMaintenanceActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateAirlineAddressMaintenance
    },
    {
      url: './configuration/var-rem-settings',
      ...this.settingSavableTab,
      applyChangesPermission: Permissions.updateVariableRemittanceSettings
    },
    {
      url: './configuration/tip-global-airline',
      canApplyChangesSelector: fromSettings.hasChangesTipsGlobalAirline,
      applyChangesAction: TipGlobalAirlineActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateGlobalAirlineConfiguration
    },
    {
      url: './configuration/tip-local-settings',
      canApplyChangesSelector: fromTipLocalAirline.canApplyChanges,
      applyChangesAction: TipLocalAirlineActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateLocalTipConfiguration
    },
    {
      url: './configuration/ip-address-settings',
      canApplyChangesSelector: fromSettings.canIpSettingsApplyChanges,
      applyChangesAction: fromSettings.IpAddressConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIpAddressSettings
    }
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private menuBuilder: fromSettings.MenuBuilder,
    private permissionsService: PermissionsService
  ) {
    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab === savableTab)
        )
      )
    ).pipe(map(values => or(...values)));
  }

  public ngOnInit(): void {
    this.setTabs();

    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        switchMap(activeSavableTab => {
          this.currentSavableTab = activeSavableTab;

          if (!activeSavableTab) {
            return of(false);
          }

          return activeSavableTab.hasApplyChangesSelector
            ? this.store.pipe(select(activeSavableTab.hasApplyChangesSelector as MemoizedSelector<AppState, boolean>))
            : of(this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission));
        }),
        tap(applyChangesVisible => (this.isApplyChangesVisible = applyChangesVisible)),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public applyChanges(): void {
    if (this.currentSavableTab) {
      this.store.dispatch(this.currentSavableTab.applyChangesAction);
    }
  }

  private setTabs(): void {
    this.loggedUserType$.pipe(distinctUntilChanged(), takeUntil(this.destroyed$)).subscribe(userType => {
      this.tabs = this.getTabs(userType);
    });
  }

  private getTabs(userType: UserType): fromSettings.RoutedMenuItem[] {
    const originalTabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
    const shouldRemoveTicketingAuthority = userType === UserType.AGENT || userType === UserType.AGENT_GROUP;

    return shouldRemoveTicketingAuthority
      ? originalTabs.filter(tab => tab.route !== 'ticketing-authority')
      : originalTabs;
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
