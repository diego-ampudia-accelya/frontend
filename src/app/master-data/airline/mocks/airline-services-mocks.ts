export function getInitialAirlineState() {
  return {
    airline: {
      profile: {
        globalAirline: {
          id: 485649,
          iataCode: '081',
          globalName: 'Qantas Airways Ltd.',
          airlines: [
            {
              id: 6554485649,
              designator: 'QF',
              localName: 'QANTAS',
              bsp: {
                name: 'Spain'
              }
            }
          ]
        }
      }
    }
  };
}

const globalAirlineMocked = {
  id: 485649,
  iataCode: '081',
  globalName: 'Qantas Airways Ltd.',
  airlines: [
    {
      id: 6554485649,
      designator: 'QF',
      localName: 'QANTAS',
      bsp: {
        name: 'Spain'
      }
    }
  ]
};

export function getInitialAirlineUserState() {
  return {
    auth: {
      user: {
        id: '555',
        globalAirline: globalAirlineMocked
      }
    },
    airline: {
      profile: { globalAirline: globalAirlineMocked }
    }
  };
}

export function getCustomRouterSateSerializerMock() {
  return {
    parent: {
      params: {
        'my-airline-id': 6554485649
      }
    }
  };
}

export function getActivatedRouteMock() {
  return {
    url: 'master-data/airline/my/6554485649/configuration/basic-settings',
    params: {
      'my-airline-id': 6554485649
    },
    parent: {
      url: 'master-data/airline/my/6554485649/configuration/',
      params: {
        'my-airline-id': 6554485649
      }
    }
  };
}
