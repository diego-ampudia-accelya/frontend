import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdmPolicyChangesDialogComponent } from './adm-policy-changes-dialog.component';
import { DialogConfig, FooterButton } from '~app/shared/components';

describe('AdmPolicyChangesDialogComponent', () => {
  let component: AdmPolicyChangesDialogComponent;
  let fixture: ComponentFixture<AdmPolicyChangesDialogComponent>;

  const mockConfig = {
    data: {
      title: 'title',
      hasCancelButton: true,
      footerButtonsType: [{ type: FooterButton.Discard }, { type: FooterButton.Apply }]
    },
    changes: [{}],
    message: '',
    messageEmailAlert: '',
    messageRemarkAlert: ''
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AdmPolicyChangesDialogComponent],
      providers: [{ provide: DialogConfig, useValue: mockConfig }],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmPolicyChangesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
