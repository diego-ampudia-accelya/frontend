import { Inject, Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, defer, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap } from 'rxjs/operators';

import { AdmPoliciesActions } from '../../actions';
import { scopeType } from '../adm-policies-config';
import { AdmPolicyConfiguration } from '../models/adm-policy.model';
import { AdmPolicyChangesDialogComponent } from './adm-policy-changes-dialog.component';
import * as fromAirline from '~app/master-data/airline/reducers';
import { SettingsService, SETTINGS_SERVICE } from '~app/master-data/configuration';
import * as fromConfiguration from '~app/master-data/configuration/store/reducers';
import { AppState } from '~app/reducers';
import { ButtonDesign, ChangeModel, DialogConfig, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService,
    @Inject(SETTINGS_SERVICE) private apiServices: SettingsService<any>[]
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm();
  }

  private confirm(): Observable<FooterButton> {
    return combineLatest([
      this.store.pipe(select(fromConfiguration.getPolicyState)),
      this.store.pipe(select(fromConfiguration.getPolicyChanges)),
      this.store.pipe(select(fromAirline.getSelectedAirline))
    ]).pipe(
      first(),
      switchMap(result => this.open(result))
    );
  }

  private open(configurations): Observable<FooterButton> {
    const dialogConfig = this.createDialog(configurations);

    return this.dialogService.open(AdmPolicyChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(dialogConfig);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(AdmPoliciesActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private createDialog(configurations): DialogConfig {
    return {
      data: {
        title: this.translationService.translate('menu.masterData.configuration.admPolicies.titleDialog'),
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: this.formatChanges(configurations[1]),
      message: this.translationService.translate('menu.masterData.configuration.admPolicies.messageDialog', {
        airlineName: configurations[2].localName
      }),
      messageEmailAlert: this.getEmailAlert(configurations[1]),
      messageRemarkAlert: this.getRemarkAlert(configurations[1])
    } as DialogConfig;
  }

  private formatChanges(admPolicy: Partial<AdmPolicyConfiguration>): ChangeModel[] {
    return Object.keys(admPolicy).map(item => {
      const value = typeof admPolicy[item] === 'boolean' ? this.translateValue(admPolicy[item]) : admPolicy[item];

      return {
        group: item,
        name: this.translationService.translate(`menu.masterData.configuration.admPolicies.labels.${item}`),
        value
      };
    });
  }

  private getEmailAlert(admPolicy: Partial<AdmPolicyConfiguration>): string {
    const field = Object.keys(admPolicy).find(i => i === 'sendAdm');

    return field && !admPolicy[field]
      ? this.translationService.translate('menu.masterData.configuration.admPolicies.alertEmailConfirm')
      : null;
  }

  private getRemarkAlert(admPolicy: Partial<AdmPolicyConfiguration>): string {
    const field = Object.keys(admPolicy).find(i => i === 'notifyAgents');

    return field && !admPolicy[field]
      ? this.translationService.translate('menu.masterData.configuration.admPolicies.alertRemarkConfirm')
      : null;
  }

  private translateValue(value): string {
    return this.translationService.translate(
      value
        ? 'menu.masterData.configuration.admPolicies.labels.yes'
        : 'menu.masterData.configuration.admPolicies.labels.no'
    );
  }

  private apply(dialogConfig: DialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));
    const dataService = this.apiServices.find(service => service.scopeType === scopeType);

    return defer(() => {
      setLoading(true);

      return this.store.pipe(
        select(fromConfiguration.getPolicyState),
        switchMap(settings => dataService.save(this.formatDataForSubmit(settings.value))),
        tap(response => {
          this.store.dispatch(AdmPoliciesActions.applyChangesSuccess({ value: response }));
        }),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }

  private formatDataForSubmit(value: AdmPolicyConfiguration): AdmPolicyConfiguration {
    return {
      ...value,
      email: value.sendAdm ? value.email : '',
      comment: value.notifyAgents ? value.comment : ''
    };
  }
}
