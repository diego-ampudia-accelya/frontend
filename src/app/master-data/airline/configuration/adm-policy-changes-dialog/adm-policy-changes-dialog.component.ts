import { Component } from '@angular/core';

import { DialogConfig } from '~app/shared/components';
import { AlertMessageType } from '~app/shared/enums';

@Component({
  selector: 'bspl-adm-policy-changes-dialog',
  templateUrl: './adm-policy-changes-dialog.component.html',
  styleUrls: ['./adm-policy-changes-dialog.component.scss']
})
export class AdmPolicyChangesDialogComponent {
  public warning = AlertMessageType.warning;

  constructor(public config: DialogConfig) {}
}
