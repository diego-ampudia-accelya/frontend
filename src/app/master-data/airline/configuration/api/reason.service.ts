import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AirlineReason } from '../models/airline-reason.model';
import { getBaseConfigurationPath } from './base-path';
import { DescriptionService, EditableDescription, ReasonType } from '~app/master-data/configuration';
import { determineMethodToSave } from '~app/shared/utils/api-utils';
import { buildPath } from '~app/shared/utils/path-builder';

export class ReasonService implements DescriptionService {
  constructor(private http: HttpClient, public type: ReasonType) {}

  get(parentId: number): Observable<EditableDescription[]> {
    return this.http
      .get<any>(this.getUrl(parentId), {
        params: { reasonType: this.type }
      })
      .pipe(map(reasons => reasons.map(this.mapToEditableDescription.bind(this))));
  }

  delete(model: EditableDescription): Observable<void> {
    return this.http.delete<void>(this.getUrl(model.parentId, model.id), {
      params: { reasonType: this.type }
    });
  }

  save(model: EditableDescription): Observable<EditableDescription> {
    const url = this.getUrl(model.parentId, model.id);
    const method = determineMethodToSave(model);
    const reason = this.mapToReason(model);

    return this.http
      .request<AirlineReason>(method, url, {
        body: reason,
        params: { reasonType: this.type }
      })
      .pipe(map(this.mapToEditableDescription).bind(this));
  }

  private getUrl(airlineId: number, modelId?: number): string {
    return buildPath(getBaseConfigurationPath(airlineId), 'acdm-reason-configuration', modelId);
  }

  private mapToReason(model: EditableDescription): AirlineReason {
    const { parentId, label, ...otherProps } = model;

    return {
      ...otherProps,
      title: label,
      type: this.type,
      airlineId: parentId
    };
  }

  private mapToEditableDescription(reason: AirlineReason): EditableDescription {
    const { airlineId, title, ...otherProps } = reason;

    return {
      ...otherProps,
      label: title,
      parentId: airlineId
    };
  }
}
