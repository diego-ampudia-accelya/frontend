import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AdmPolicyConfiguration } from '../models/adm-policy.model';
import { AdmAcmPolicyModel } from '~app/adm-acm/models/adm-acm-policy.model';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ConfigurationScope, SettingsService } from '~app/master-data/configuration';
import { Permissions } from '~app/shared/constants/permissions';
import { PagedData } from '~app/shared/models/paged-data.model';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class AirlineAdmPoliciesService implements SettingsService<AdmAcmPolicyModel> {
  public scopeType = 'adm-policies';
  private baseUrl = `${appConfiguration.baseApiPath}/acdm-management/`;
  private hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

  constructor(private http: HttpClient, private permissionsService: PermissionsService) {}

  public get(scope: ConfigurationScope): Observable<AdmPolicyConfiguration> {
    const airlineLeanId = scope.scopeId;
    const url = this.hasLeanPermission ? this.getLeanUrl(airlineLeanId) : `${this.baseUrl}adm-policies`;

    return this.http
      .get<PagedData<AdmAcmPolicyModel>>(url)
      .pipe(map(admPolicies => this.initFields(admPolicies.records[0])));
  }

  public save(model: AdmAcmPolicyModel): Observable<AdmPolicyConfiguration> {
    if (model.id) {
      const putUrl = this.hasLeanPermission
        ? this.getLeanUrl(model.airline.id)
        : `${this.baseUrl}adm-policies/${model.id}`;

      return this.http.put<AdmAcmPolicyModel>(putUrl, model).pipe(map(admPolicies => this.initFields(admPolicies)));
    } else {
      const postUrl = this.hasLeanPermission ? this.getLeanUrl(model.airline.id) : `${this.baseUrl}adm-policies`;

      return this.http.post<AdmAcmPolicyModel>(postUrl, model).pipe(map(admPolicies => this.initFields(admPolicies)));
    }
  }

  private initFields(policy: AdmAcmPolicyModel): AdmPolicyConfiguration {
    return {
      ...policy,
      sendAdm: !!policy?.email,
      notifyAgents: !!policy?.comment
    };
  }

  private getLeanUrl(leanAirlineId): string {
    return `${this.baseUrl}airlines/${leanAirlineId}/adm-policies`;
  }
}
