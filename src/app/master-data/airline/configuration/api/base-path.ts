import { appConfiguration } from '~app/shared/services/app-configuration.service';

export const getBaseConfigurationPath = (airlineId: number) =>
  `${appConfiguration.baseApiPath}/acdm-management/airlines/${airlineId}`;
