import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';

import { AirlineAdmPolicySettingsComponent } from './airline-adm-policy-settings.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('AirlineAdmPolicySettingsComponent', () => {
  let component: AirlineAdmPolicySettingsComponent;
  let fixture: ComponentFixture<AirlineAdmPolicySettingsComponent>;

  const activatedRouteMock = {
    snapshot: {
      data: {
        configuration: {
          title: 'Title'
        }
      }
    }
  };

  const initialState = {
    router: null,
    configuration: {
      settings: null,
      admPolicies: {
        isLoading: false,
        isChanged: false,
        originalValue: null,
        value: null,
        scope: null,
        settingConfigurations: [],
        isInvalid: false,
        errors: null
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AirlineAdmPolicySettingsComponent],
      imports: [HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig), FormsModule, ReactiveFormsModule],
      providers: [provideMockStore({ initialState }), { provide: ActivatedRoute, useValue: activatedRouteMock }],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirlineAdmPolicySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
