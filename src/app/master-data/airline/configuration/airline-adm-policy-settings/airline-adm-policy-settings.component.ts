import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, first, map, takeUntil } from 'rxjs/operators';

import { AdmPoliciesActions } from '../../actions';
import { AdmPolicyConfiguration } from '../models/adm-policy.model';
import { DisplayTypes } from '~app/master-data/configuration';
import * as fromConfiguration from '~app/master-data/configuration/store/reducers';
import { AdmPolicyConfigurationState } from '~app/master-data/configuration/store/reducers/adm-policies.reducer';
import { AppState } from '~app/reducers';
import { FormUtil } from '~app/shared/helpers';

@Component({
  selector: 'bspl-airline-adm-policy-settings',
  templateUrl: './airline-adm-policy-settings.component.html',
  styleUrls: ['./airline-adm-policy-settings.component.scss']
})
export class AirlineAdmPolicySettingsComponent implements OnInit, OnDestroy {
  public policySettings$ = this.store.pipe(select(fromConfiguration.getPolicyConfigurations));
  private policyState$ = this.store.pipe(select(fromConfiguration.getPolicyState));
  public configuration;
  public form: FormGroup;

  public isLoading$ = this.policyState$.pipe(map(state => state.isLoading));

  private sendAdmCtrl: AbstractControl;
  private emailCtrl: AbstractControl;
  private notifyAgentsCtrl: AbstractControl;
  private commentCtrl: AbstractControl;

  private destroy$ = new Subject();

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute) {}

  public ngOnInit() {
    this.configuration = this.activatedRoute.snapshot.data.configuration;
    this.createForm();
    this.initializeStoreListener();
    this.initializeFormChangeListener();
  }

  public isInputType(displayType: DisplayTypes): boolean {
    return displayType === DisplayTypes.input;
  }

  public isCheckboxType(displayType: DisplayTypes): boolean {
    return displayType === DisplayTypes.checkbox;
  }

  public isTextareaType(displayType: DisplayTypes): boolean {
    return displayType === DisplayTypes.textarea;
  }

  private createForm() {
    this.form = new FormGroup({ id: new FormControl(null) });
    this.policySettings$.pipe(first()).subscribe(policies => {
      policies.forEach(policy => {
        policy.settings.forEach(setting => {
          if (this.isTextareaType(setting.displayType as DisplayTypes)) {
            this.form.addControl(
              setting.field,
              new FormControl(setting.value, { updateOn: 'blur', validators: setting.validators })
            );
          } else {
            this.form.addControl(setting.field, new FormControl(setting.value, setting.validators));
          }
        });
      });
    });

    this.sendAdmCtrl = FormUtil.get<AdmPolicyConfiguration>(this.form, 'sendAdm');
    this.emailCtrl = FormUtil.get<AdmPolicyConfiguration>(this.form, 'email');
    this.notifyAgentsCtrl = FormUtil.get<AdmPolicyConfiguration>(this.form, 'notifyAgents');
    this.commentCtrl = FormUtil.get<AdmPolicyConfiguration>(this.form, 'comment');
  }

  private initializeStoreListener() {
    this.policyState$
      .pipe(
        filter(settingsValue => !!settingsValue),
        takeUntil(this.destroy$)
      )
      .subscribe(settingsValue => {
        if (this.form.pristine || (!settingsValue.isChanged && !settingsValue.isInvalid)) {
          this.form.patchValue(settingsValue.value || {}, { emitEvent: false });
          this.updateForm();
        }
        if (settingsValue.errors) {
          this.handleErrors(settingsValue);
        }
      });
  }

  private initializeFormChangeListener() {
    this.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(() => {
      this.updateForm();
      this.dispatchToStore();
    });
  }

  private dispatchToStore() {
    if (this.form.value && this.form.valid) {
      this.store.dispatch(AdmPoliciesActions.modify({ value: this.form.value }));
    } else {
      this.store.dispatch(AdmPoliciesActions.invalid());
    }
  }

  private updateForm() {
    if (this.sendAdmCtrl && this.emailCtrl) {
      this.updateField(this.sendAdmCtrl.value, this.emailCtrl);
    }

    if (this.notifyAgentsCtrl && this.commentCtrl) {
      this.updateField(this.notifyAgentsCtrl.value, this.commentCtrl);
    }
  }

  private updateField(enable: boolean, control: AbstractControl) {
    if (enable) {
      control.enable({ emitEvent: false, onlySelf: true });
    } else {
      control.disable({ emitEvent: false, onlySelf: true });
    }

    this.form.updateValueAndValidity({ emitEvent: false });
  }

  private handleErrors(state: AdmPolicyConfigurationState) {
    if (state.isChanged) {
      this.store.dispatch(AdmPoliciesActions.applyChangesError(null));
    }

    state.errors.error.messages.forEach(element => {
      const messageText = element.message;

      if (element.messageParams.length && element.messageParams[0].name === 'fieldName') {
        const fieldPath = element.messageParams[0].value;
        const viewModelControl = this.form.get(fieldPath);

        if (viewModelControl) {
          FormUtil.showControlState(viewModelControl);
          viewModelControl.markAsDirty(); // TODO Remove when custom controls are unified regarding show of errors
          viewModelControl.setErrors({
            backendError: { message: messageText }
          });
        }
      }
    });
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
