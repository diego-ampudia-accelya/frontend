import { Validators } from '@angular/forms';
import { cloneDeep } from 'lodash';

import { DisplayTypes, InputSetting, SettingCategory } from '~app/master-data/configuration';
import { GLOBALS } from '~app/shared/constants/globals';

const checkboxSetting: InputSetting = {
  label: '',
  value: false,
  displayType: DisplayTypes.checkbox,
  field: ''
};

const inputSetting: InputSetting = {
  label: '',
  value: '',
  displayType: DisplayTypes.input,
  field: '',
  placeholder: ''
};
const textAreaSetting: InputSetting = {
  label: '',
  value: '',
  displayType: DisplayTypes.textarea,
  field: '',
  placeholder: ''
};

const urlPolicy: SettingCategory = {
  label: '',
  settings: [
    {
      ...inputSetting,
      label: 'menu.masterData.configuration.admPolicies.labels.url',
      field: 'url',
      validators: [Validators.required, Validators.pattern(GLOBALS.PATTERNS.URL), Validators.maxLength(200)]
    }
  ]
};

const emailPolicy: SettingCategory = {
  label: '',
  settings: [
    {
      ...checkboxSetting,
      label: 'menu.masterData.configuration.admPolicies.labels.sendAdm',
      field: 'sendAdm'
    },
    {
      ...inputSetting,
      label: 'menu.masterData.configuration.admPolicies.labels.email',
      field: 'email',
      validators: [Validators.required],
      placeholder: 'menu.masterData.configuration.admPolicies.placeholders.email'
    }
  ]
};

const notifyPolicy: SettingCategory = {
  label: '',
  settings: [
    {
      ...checkboxSetting,
      label: 'menu.masterData.configuration.admPolicies.labels.notifyAgents',
      field: 'notifyAgents'
    },
    {
      ...textAreaSetting,
      label: 'menu.masterData.configuration.admPolicies.labels.comment',
      field: 'comment',
      validators: [Validators.required, Validators.maxLength(350)],
      placeholder: 'menu.masterData.configuration.admPolicies.placeholders.comment'
    }
  ]
};

export const scopeType = 'adm-policies';

export function getAdmPolicyConfiguration(): SettingCategory[] {
  return cloneDeep([urlPolicy, emailPolicy, notifyPolicy]);
}
