import { AdmAcmPolicyModel } from '~app/adm-acm/models/adm-acm-policy.model';

export interface AdmPolicyConfiguration extends AdmAcmPolicyModel {
  sendAdm: boolean;
  notifyAgents: boolean;
}
