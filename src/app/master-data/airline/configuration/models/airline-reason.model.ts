import { Reason } from '~app/master-data/configuration';

export interface AirlineReason extends Reason {
  airlineId: number;
  oaAcdmConfigId?: number;
}
