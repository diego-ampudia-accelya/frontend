import { Inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, select, Store } from '@ngrx/store';
import { EMPTY, Observable } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AdmPoliciesActions } from '../actions';
import { ChangesDialogService } from '../configuration/adm-policy-changes-dialog/changes-dialog.service';
import { ConfigurationScope, NotifierService, SettingsService, SETTINGS_SERVICE } from '~app/master-data/configuration';
import * as fromSettings from '~app/master-data/configuration/store/reducers/index';
import { AppState } from '~app/reducers';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class AdmPoliciesEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdmPoliciesActions.load),
      switchMap(scope => this.load(scope.scope))
    )
  );

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdmPoliciesActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(() => EMPTY),
      rethrowError(error => AdmPoliciesActions.applyChangesError({ error }))
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AdmPoliciesActions.applyChangesSuccess),
        withLatestFrom(this.store.pipe(select(fromSettings.getPolicyState))),
        tap(([_, settings]) => this.notification.notifySaveSuccess(settings.scope))
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    @Inject(SETTINGS_SERVICE) private apiServices: SettingsService<any>[],
    private changesDialogService: ChangesDialogService,
    private notification: NotifierService
  ) {}

  private load(scope: ConfigurationScope): Observable<Action> {
    const apiService = this.getService(scope);

    return apiService.get(scope).pipe(
      map(settings => AdmPoliciesActions.loadSuccess({ value: settings })),
      rethrowError(() => AdmPoliciesActions.loadError())
    );
  }

  private getService(scope: ConfigurationScope): SettingsService<any> {
    if (scope == null || scope.scopeType == null) {
      throw new Error('scopeType is required in order to find the appropriate API service.');
    }

    return this.apiServices.find(service => service.scopeType === scope.scopeType);
  }
}
