import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { ConfigurationActions } from '../actions';
import { Airline } from '~app/master-data/models';
import { replaceRouteParamKey } from '~app/shared/utils/path-builder';

// TODO: not used, to be removed (?)
@Injectable()
export class ConfigurationEffects {
  @Effect({ dispatch: false })
  selectAirline$ = this.actions$.pipe(
    ofType(ConfigurationActions.selectAirline),
    switchMap(({ airline }) => this.navigateBy(this.router.url, airline))
  );

  constructor(private actions$: Actions, private router: Router, private activatedRoute: ActivatedRoute) {}

  private navigateBy(url: string, airline?: Airline): Observable<boolean> {
    if (airline != null) {
      if (url.indexOf('/my/') !== -1) {
        url = replaceRouteParamKey(this.activatedRoute.snapshot, ':my-airline-id', airline.id.toString());
      } else {
        url = replaceRouteParamKey(this.activatedRoute.snapshot, ':airline-id', airline.id.toString());
      }
    }

    return from(this.router.navigateByUrl(url));
  }
}
