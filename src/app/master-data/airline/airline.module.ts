import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AddressMaintenanceModule } from '../address-maintenance/address-maintenance.module';
import { AdmAcmAuthorityModule } from '../adm-acm-authority/adm-acm-authority.module';
import { PublicApiModule } from '../public-api/public-api.module';
import { RefundAuthorityModule } from '../refund-authority/refund-authority.module';
import { SftpModule } from '../sftp/sftp.module';
import { TicketingAuthorityModule } from '../ticketing-authority/ticketing-authority.module';
import { VariableRemittanceModule } from '../variable-remittance/variable-remittance.module';

import { AirlineRoutingModule } from './airline-routing.module';
import { AirlineListComponent } from './components/airline-list/airline-list.component';
import { AirlineProfileComponent } from './components/airline-profile/airline-profile.component';
import { AirlinePropertiesComponent } from './components/airline-properties/airline-properties.component';
import { AdmPolicyChangesDialogComponent } from './configuration/adm-policy-changes-dialog/adm-policy-changes-dialog.component';
import { ChangesDialogService } from './configuration/adm-policy-changes-dialog/changes-dialog.service';
import { AirlineAdmPolicySettingsComponent } from './configuration/airline-adm-policy-settings/airline-adm-policy-settings.component';
import { AirlineAdmPoliciesService } from './configuration/api/airline-adm-policies.service';
import { AdmPoliciesEffects } from './effects/adm-policies.effects';
import { AirlineProfileEffects } from './effects/airline-profile.effects';
import { ConfigurationEffects } from './effects/configuration.effects';
import { reducers } from './reducers';
import { AirlineAddressMaintenanceResolver } from './services/airline-address-maintenance.resolver';
import { AirlineIpAddressSettingsResolver } from './services/airline-ip-address-settings.resolver';
import { TipLocalAirlineSettingResolver } from './services/tip-local-airline-setting.resolver';
import { SharedModule } from '~app/shared/shared.module';
import { TipLocalAirlineModule } from '~app/master-data/configuration/tip/tip-local-airline/tip-local-airline.module';
import { TipLocalAirlineEffects } from '~app/master-data/configuration/tip/tip-local-airline/store/effects/tip-local-airline.effects';
import { TipGlobalAirlineService } from '~app/master-data/configuration/tip/tip-global-airline/services/tip-globall-airline.service';
import { TipGlobalAirlineResolver } from '~app/master-data/configuration/tip/tip-global-airline/services/tip-global-airline.resolver';
import { ConfigurationModule, SETTINGS_SERVICE } from '~app/master-data/configuration';

@NgModule({
  declarations: [
    AirlineListComponent,
    AirlineProfileComponent,
    AirlinePropertiesComponent,
    AirlineAdmPolicySettingsComponent,
    AdmPolicyChangesDialogComponent
  ],
  imports: [
    CommonModule,
    AirlineRoutingModule,
    SharedModule,
    ConfigurationModule,
    RefundAuthorityModule,
    SftpModule,
    TicketingAuthorityModule,
    AdmAcmAuthorityModule,
    AddressMaintenanceModule,
    VariableRemittanceModule,
    TipLocalAirlineModule,
    PublicApiModule,
    StoreModule.forFeature('airline', reducers),
    EffectsModule.forFeature([AirlineProfileEffects, ConfigurationEffects, AdmPoliciesEffects, TipLocalAirlineEffects])
  ],
  providers: [
    { provide: SETTINGS_SERVICE, useClass: AirlineAdmPoliciesService, multi: true },
    AirlineAddressMaintenanceResolver,
    ChangesDialogService,
    TipGlobalAirlineService,
    TipGlobalAirlineResolver,
    TipLocalAirlineSettingResolver,
    AirlineIpAddressSettingsResolver
  ],
  exports: []
})
export class AirlineModule {}
