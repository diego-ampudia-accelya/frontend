import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, defer, merge, Observable, of, Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

import { GlobalTaFopFilesActions } from '../configuration/global-ta-fop-files-settings/store/actions';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  canApplyChanges,
  MenuBuilder,
  RoutedMenuItem,
  SettingConfigurationActions
} from '~app/master-data/configuration';
import { ChangesDialogService as SettingsChangesDialogService } from '~app/master-data/configuration/changes-dialog/changes-dialog.service';
import * as fromGdsSettings from '~app/master-data/gds/configuration/store/reducers';
import { Gds } from '~app/master-data/models/gds.model';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { Permissions } from '~app/shared/constants/permissions';
import { or } from '~app/shared/utils';

@Component({
  selector: 'bspl-gds-profile',
  templateUrl: './gds-profile.component.html',
  styleUrls: ['./gds-profile.component.scss']
})
export class GdsProfileComponent implements OnInit {
  public gdsSelected: Gds;

  public tabs: RoutedMenuItem[];
  public isApplyChangesVisible: boolean;
  public currentSavableTab = null;
  public isApplyChangesEnabled$: Observable<boolean>;

  public beforeActivateTab$: Observable<boolean> = defer(() =>
    this.unsavedChangesDialogService.confirmUnsavedChanges().pipe(map(action => action !== FooterButton.Cancel))
  );
  private destroyed$ = new Subject();

  private savableTabs = [
    {
      url: './configuration/email-alerts-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateEmailAlerts
    },
    {
      url: './configuration/notifications',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateNotificationSettings
    },
    {
      url: './configuration/global-ta-fop-files',
      canApplyChangesSelector: fromGdsSettings.canGlobalTaFopFilesApplyChanges,
      applyChangesAction: GlobalTaFopFilesActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateGlobalTaFopFiles
    },
    {
      url: './gds-agent-list',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateGds
    }
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private menuBuilder: MenuBuilder,
    private unsavedChangesDialogService: SettingsChangesDialogService,
    private permissionsService: PermissionsService
  ) {
    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab === savableTab)
        )
      )
    ).pipe(map(values => or(...values)));
  }

  public ngOnInit(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));
    this.gdsSelected = this.activatedRoute.snapshot.data.gds;

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        tap(activeSavableTab => {
          this.isApplyChangesVisible = false;
          if (activeSavableTab != null) {
            this.isApplyChangesVisible = this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission);
          }
          this.currentSavableTab = activeSavableTab;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public applyChanges(): void {
    if (this.currentSavableTab) {
      this.store.dispatch(this.currentSavableTab.applyChangesAction);
    }
  }
}
