import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ConfigurationModule } from '../configuration';

import { GlobalTaFopFilesSettingsComponent } from './configuration/global-ta-fop-files-settings/global-ta-fop-files-settings.component';
import { ChangesDialogConfigService } from './configuration/global-ta-fop-files-settings/services/changes-dialog-config.service';
import { ChangesDialogService } from './configuration/global-ta-fop-files-settings/services/changes-dialog.service';
import { GlobalTaFopFilesService } from './configuration/global-ta-fop-files-settings/services/global-ta-fop-files.service';
import { GlobalTaFopFilesEffects } from './configuration/global-ta-fop-files-settings/store/effects/global-ta-fop-files.effects';
import * as fromGdsConfiguration from './configuration/store/reducers';
import { GdsAgentListModule } from './gds-agent-list/gds-agent-list.module';
import { GdsAddEditFormDialogComponent } from './gds-dialog/add-edit-form/gds-add-edit-form-dialog.component';
import { GdsDeleteFormDialogComponent } from './gds-dialog/delete-form/gds-delete-form-dialog.component';
import { GdsProfileComponent } from './gds-profile/gds-profile.component';
import { SharedModule } from '~app/shared/shared.module';
import { GdsRoutingModule } from '~app/master-data/gds/gds-routing.module';
import { GdsListComponent } from '~app/master-data/gds/gds-list/gds-list.component';

@NgModule({
  declarations: [
    GdsListComponent,
    GdsAddEditFormDialogComponent,
    GdsDeleteFormDialogComponent,
    GdsProfileComponent,
    GlobalTaFopFilesSettingsComponent
  ],
  imports: [
    ConfigurationModule,
    CommonModule,
    GdsRoutingModule,
    SharedModule,
    GdsAgentListModule,
    StoreModule.forFeature(fromGdsConfiguration.gdsConfigurationFeatureKey, fromGdsConfiguration.reducers),
    EffectsModule.forFeature([GlobalTaFopFilesEffects])
  ],
  providers: [GlobalTaFopFilesService, ChangesDialogService, ChangesDialogConfigService]
})
export class GdsModule {}
