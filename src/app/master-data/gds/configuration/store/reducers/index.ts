import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromGlobalTaFopFiles from '../../global-ta-fop-files-settings/store/reducers';
import { GlobalTaFopFilesOperation } from '../../models/global-ta-fop-files.model';
import { AppState } from '~app/reducers';

export const gdsConfigurationFeatureKey = 'gdsConfiguration';

export interface GdsConfigurationState {
  globalTaFopFiles: fromGlobalTaFopFiles.GlobalTaFopFilesConfigurationState;
}

export interface State extends AppState {
  gdsConfiguration: GdsConfigurationState;
}

export function reducers(state: GdsConfigurationState, action: Action) {
  return combineReducers({
    globalTaFopFiles: fromGlobalTaFopFiles.reducer
  })(state, action);
}

export const getGdsConfigurationState = createFeatureSelector<State, GdsConfigurationState>('gdsConfiguration');

export const getGlobalTaFopFilesState = createSelector(
  getGdsConfigurationState,
  state => state && state.globalTaFopFiles
);

export const getGlobalTaFopFilesValue = createSelector(getGlobalTaFopFilesState, state => state.value);

export const hasGlobalTaFopFilesValidChanges = createSelector(
  getGlobalTaFopFilesState,
  state => state.isChanged && !state.isInvalid
);

export const getGlobalTaFopFilesChanges = createSelector(
  getGlobalTaFopFilesState,
  fromGlobalTaFopFiles.getGlobalTaFopFilesChanges
);

export const getGlobalTaFopFilesLoading = createSelector(getGlobalTaFopFilesState, state => state.isLoading);

export const canGlobalTaFopFilesApplyChanges = createSelector(
  getGlobalTaFopFilesState,
  state => state && !state.isInvalid && state.isChanged
);

export const getGlobalTaFopFilesOperation = createSelector(getGlobalTaFopFilesState, state => {
  let operation;

  if (state && !state.isInvalid && state.isChanged) {
    if (state.originalValue.bsp === null) {
      operation = GlobalTaFopFilesOperation.Create;
    }
    if (state.originalValue.bsp !== null && state.value !== null) {
      operation = GlobalTaFopFilesOperation.Update;
    }
    if (state.value.bsp === null) {
      operation = GlobalTaFopFilesOperation.Delete;
    }
  }

  return operation;
});
