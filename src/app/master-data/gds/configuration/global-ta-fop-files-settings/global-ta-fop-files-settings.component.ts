import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

import { modifyPermissionsGlobalTaFopFiles } from '../models/global-ta-fop-files.model';
import { GlobalTaFopFilesService } from './services/global-ta-fop-files.service';
import { GlobalTaFopFilesActions } from './store/actions';
import { PermissionsService } from '~app/auth/services/permissions.service';
import * as fromGdsConfiguration from '~app/master-data/gds/configuration/store/reducers';
import { AppState } from '~app/reducers';
import { AlertMessageType } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-global-ta-fop-files-settings',
  templateUrl: './global-ta-fop-files-settings.component.html',
  styleUrls: ['./global-ta-fop-files-settings.component.scss']
})
export class GlobalTaFopFilesSettingsComponent implements OnInit, OnDestroy {
  public globalTaFopFilesValue$ = this.store.pipe(select(fromGdsConfiguration.getGlobalTaFopFilesValue));
  public form: FormGroup;
  public bspIdOptions$: Observable<DropdownOption<BspDto>[]>;
  public isBspLocked: boolean;

  public disclaimerType = AlertMessageType.info;

  public isLoading$ = this.store.pipe(select(fromGdsConfiguration.getGlobalTaFopFilesLoading));

  private globalTaFopFilesState$ = this.store.pipe(select(fromGdsConfiguration.getGlobalTaFopFilesState));

  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private bspDictionary: BspsDictionaryService,
    private permissionsService: PermissionsService,
    private globalTaFopFilesService: GlobalTaFopFilesService
  ) {}

  public ngOnInit() {
    this.createForm();
    this.initializeBspDropdown();
    this.initializeStoreListener();
    this.initializeFormChangeListener();
  }

  private initializeBspDropdown(): void {
    this.isBspLocked = !this.permissionsService.hasPermission(modifyPermissionsGlobalTaFopFiles);

    this.loadRegisteredBsp();

    this.bspIdOptions$ = this.bspDictionary.getAllBspDropdownOptions();
  }

  private loadRegisteredBsp(): void {
    this.store.dispatch(GlobalTaFopFilesActions.load());

    this.globalTaFopFilesService
      .getCurrentBspRegistered()
      .pipe(
        catchError(error => {
          if ((error as HttpErrorResponse).status === 404) {
            // 404 error response means there is no Bsp currently registered so we propagate blank
            return of(null);
          } else {
            // Different unexpected error, we continue error propagation
            this.store.dispatch(GlobalTaFopFilesActions.loadError());
            this.isBspLocked = true;

            return throwError(error);
          }
        })
      )
      .subscribe(bspRegistered =>
        this.store.dispatch(GlobalTaFopFilesActions.loadSuccess({ value: { bsp: bspRegistered } }))
      );
  }

  private createForm() {
    this.form = new FormGroup({ bsp: new FormControl(null) });
  }

  private initializeStoreListener() {
    this.globalTaFopFilesState$
      .pipe(
        filter(settingsState => !!settingsState.value),
        takeUntil(this.destroy$)
      )
      .subscribe(settingsState => {
        if (this.form.pristine || (!settingsState.isChanged && !settingsState.isInvalid)) {
          this.form.patchValue(settingsState.value || {}, { emitEvent: false });
        }
      });
  }

  private initializeFormChangeListener() {
    this.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(() => {
      this.dispatchToStore();
    });
  }

  private dispatchToStore() {
    if (this.form.valid) {
      this.store.dispatch(GlobalTaFopFilesActions.modify({ value: this.form.value }));
    } else {
      this.store.dispatch(GlobalTaFopFilesActions.invalid());
    }
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
