import { createReducer, on } from '@ngrx/store';
import { isEqualWith } from 'lodash';

import { GlobalTaFopFilesConfiguration } from '../../../models/global-ta-fop-files.model';
import * as actions from '~app/master-data/gds/configuration/global-ta-fop-files-settings/store/actions';

export interface GlobalTaFopFilesConfigurationState {
  originalValue: GlobalTaFopFilesConfiguration;
  value: GlobalTaFopFilesConfiguration;
  isChanged: boolean;
  isLoading: boolean;
  isInvalid: boolean;
}

const initialState: GlobalTaFopFilesConfigurationState = {
  originalValue: null,
  value: null,
  isChanged: false,
  isLoading: false,
  isInvalid: false
};

export const reducer = createReducer(
  initialState,
  on(actions.GlobalTaFopFilesActions.load, state => ({
    ...state,
    isLoading: true
  })),
  on(actions.GlobalTaFopFilesActions.loadSuccess, (state, { value }) => ({
    ...state,
    isLoading: false,
    isChanged: false,
    isInvalid: false,
    errors: null,
    value,
    originalValue: value
  })),
  on(actions.GlobalTaFopFilesActions.loadError, state => ({
    ...state,
    originalValue: null,
    value: null,
    isChanged: false,
    isLoading: false,
    settingConfigurations: [],
    isInvalid: false,
    errors: null
  })),
  on(actions.GlobalTaFopFilesActions.modify, (state, { value }) => ({
    ...state,
    value: changeValue(state.value, value),
    isChanged: !isEqualWith(state.originalValue, { ...state.value, ...value }, (a, b) => a.bsp?.id === b.bsp?.id),
    isInvalid: false,
    errors: null
  })),
  on(actions.GlobalTaFopFilesActions.invalid, state => ({
    ...state,
    isInvalid: true
  })),
  on(actions.GlobalTaFopFilesActions.discard, state => ({
    ...state,
    isChanged: false,
    value: state.originalValue
  })),
  on(actions.GlobalTaFopFilesActions.applyChangesSuccess, (state, { value }) => ({
    ...state,
    value,
    originalValue: value,
    isChanged: false,
    isInvalid: false,
    errors: null
  })),
  on(actions.GlobalTaFopFilesActions.applyChangesError, (state, { error }) => ({
    ...state,
    isChanged: !error,
    isInvalid: true,
    errors: error
  }))
);

export const getGlobalTaFopFilesChanges = ({ value, originalValue }: GlobalTaFopFilesConfigurationState) => {
  const modifications = {} as GlobalTaFopFilesConfiguration;

  if (value && originalValue) {
    Object.keys(value)
      .filter(key => value[key] !== originalValue[key])
      .forEach(modifiedKey => {
        modifications[modifiedKey] = value[modifiedKey];
      });
  }

  return modifications;
};

export const changeValue = (currentValue: GlobalTaFopFilesConfiguration, newValue: GlobalTaFopFilesConfiguration) => ({
  ...currentValue,
  ...newValue
});
