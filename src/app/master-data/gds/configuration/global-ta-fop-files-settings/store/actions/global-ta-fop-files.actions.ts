import { createAction, props } from '@ngrx/store';

import { GlobalTaFopFilesConfiguration } from '../../../models/global-ta-fop-files.model';

export const modify = createAction(
  '[Global TA FOP Files Configuration] Modify',
  props<{ value: GlobalTaFopFilesConfiguration }>()
);

export const load = createAction('[Global TA FOP Files Configuration] Load');

export const loadSuccess = createAction(
  '[Global TA FOP Files Configuration] Load Success',
  props<{ value: GlobalTaFopFilesConfiguration }>()
);

export const loadError = createAction('[Global TA FOP Files Configuration] Load Error');

export const invalid = createAction('[Global TA FOP Files Configuration] Invalid');

export const openApplyChanges = createAction('[Global TA FOP Files Configuration] Open Apply Changes');

export const applyChangesSuccess = createAction(
  '[Global TA FOP Files Configuration] Apply Changes Success',
  props<{ value: GlobalTaFopFilesConfiguration }>()
);

export const applyChangesError = createAction(
  '[Global TA FOP Files Configuration] Apply Changes Error',
  props<{ error }>()
);

export const discard = createAction('[Global TA FOP Files Configuration] Discard');
