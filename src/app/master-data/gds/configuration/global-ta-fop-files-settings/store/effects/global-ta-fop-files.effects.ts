import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { EMPTY } from 'rxjs';
import { switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { ChangesDialogService } from '../../services/changes-dialog.service';
import { GlobalTaFopFilesActions } from '../actions';
import * as fromGdsConfiguration from '~app/master-data/gds/configuration/store/reducers';
import { AppState } from '~app/reducers';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class GlobalTaFopFilesEffects {
  constructor(
    private actions$: Actions,
    private changesDialogService: ChangesDialogService,
    private notification: NotificationService,
    private store: Store<AppState>
  ) {}

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GlobalTaFopFilesActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(() => EMPTY),
      rethrowError(error => GlobalTaFopFilesActions.applyChangesError({ error }))
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(GlobalTaFopFilesActions.applyChangesSuccess),
        withLatestFrom(this.store.select(fromGdsConfiguration.getGlobalTaFopFilesValue)),
        tap(([_, newValue]) => {
          if (newValue.bsp) {
            this.notification.showSuccess(
              'menu.masterData.configuration.globalTaFopFiles.changesDialog.applyChanges.success'
            );
          } else {
            this.notification.showSuccess(
              'menu.masterData.configuration.globalTaFopFiles.changesDialog.applyChanges.successNone'
            );
          }
        })
      ),
    { dispatch: false }
  );
}
