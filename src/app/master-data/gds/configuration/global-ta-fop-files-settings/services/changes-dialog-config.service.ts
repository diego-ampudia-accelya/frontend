import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { GlobalTaFopFilesConfiguration } from '../../models/global-ta-fop-files.model';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { BspDto } from '~app/shared/models/bsp.model';

@Injectable()
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: Partial<GlobalTaFopFilesConfiguration>,
    question: { title: string; details: string },
    isInvalidState: boolean
  ): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: this.formatChanges(modifications),
      hasInvalidChanges: isInvalidState,
      message,
      usePillsForChanges: true
    };
  }

  private formatChanges(modifications: Partial<GlobalTaFopFilesConfiguration>): ChangeModel[] {
    return Object.keys(modifications)
      .map(key => {
        if ((key as keyof GlobalTaFopFilesConfiguration) === 'bsp') {
          const bsp: BspDto = modifications[key];

          return bsp ? { name: toValueLabelObjectBsp(bsp).label, value: '', group: '' } : null;
        } else {
          return { name: key, value: modifications[key], group: '' };
        }
      })
      .filter(modification => !!modification);
  }
}
