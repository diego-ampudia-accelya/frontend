import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { GlobalTaFopFilesConfiguration } from '../../models/global-ta-fop-files.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class GlobalTaFopFilesService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/xml-files/bsp`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public getCurrentBspRegistered(): Observable<BspDto> {
    return this.http.get<BspDto>(this.baseUrl);
  }

  public create(value: GlobalTaFopFilesConfiguration): Observable<BspDto> {
    return this.http.post<BspDto>(this.baseUrl, { id: value.bsp.id });
  }

  public update(value: GlobalTaFopFilesConfiguration): Observable<BspDto> {
    return this.http.put<BspDto>(this.baseUrl, { id: value.bsp.id });
  }

  public delete(): Observable<any> {
    return this.http.delete<any>(this.baseUrl);
  }
}
