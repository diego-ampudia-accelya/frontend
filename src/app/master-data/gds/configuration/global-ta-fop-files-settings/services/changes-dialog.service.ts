import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { isEmpty } from 'lodash';
import { combineLatest, defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { GlobalTaFopFilesConfiguration, GlobalTaFopFilesOperation } from '../../models/global-ta-fop-files.model';
import { GlobalTaFopFilesActions } from '../store/actions';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { GlobalTaFopFilesService } from './global-ta-fop-files.service';
import * as fromGlobalTaFopFiles from '~app/master-data/gds/configuration/store/reducers';
import { AppState } from '~app/reducers';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { BspDto } from '~app/shared/models/bsp.model';

@Injectable()
export class ChangesDialogService {
  private translations = {
    applyChanges: {
      title: 'menu.masterData.configuration.globalTaFopFiles.changesDialog.applyChanges.title',
      details: 'menu.masterData.configuration.globalTaFopFiles.changesDialog.applyChanges.message'
    },
    unsavedChanges: {
      title: 'menu.masterData.configuration.globalTaFopFiles.changesDialog.unsavedChanges.title',
      details: 'menu.masterData.configuration.globalTaFopFiles.changesDialog.unsavedChanges.message'
    },
    remove: {
      title: 'menu.masterData.configuration.globalTaFopFiles.changesDialog.remove.title',
      details: 'menu.masterData.configuration.globalTaFopFiles.changesDialog.remove.message'
    }
  };

  constructor(
    private dialogService: DialogService,
    public configBuilder: ChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: GlobalTaFopFilesService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(this.translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(this.translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return combineLatest([
      this.store.pipe(select(fromGlobalTaFopFiles.getGlobalTaFopFilesChanges)),
      this.store.pipe(select(fromGlobalTaFopFiles.hasGlobalTaFopFilesValidChanges)),
      this.store.pipe(select(fromGlobalTaFopFiles.getGlobalTaFopFilesOperation))
    ]).pipe(
      first(),
      switchMap(([modifications, isValidState, operation]) => {
        if (operation === GlobalTaFopFilesOperation.Delete) {
          question = this.translations.remove;
        }

        return iif(
          () => !isEmpty(modifications),
          defer(() => this.open(modifications, question, !isValidState)),
          of(FooterButton.Discard)
        );
      })
    );
  }

  private open(
    modifications: Partial<GlobalTaFopFilesConfiguration>,
    question: { title: string; details: string },
    isInvalidState: boolean
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, question, isInvalidState);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      withLatestFrom(this.store.pipe(select(fromGlobalTaFopFiles.getGlobalTaFopFilesValue))),
      switchMap(([action, value]) => {
        let dialogResult$ = of(action);
        if (action === FooterButton.Apply) {
          dialogResult$ = this.apply(value, dialogConfig);
        } else if (action === FooterButton.Discard) {
          this.store.dispatch(GlobalTaFopFilesActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(value: GlobalTaFopFilesConfiguration, dialogConfig: ChangesDialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.store.select(fromGlobalTaFopFiles.getGlobalTaFopFilesOperation).pipe(
        first(),
        switchMap(operation => {
          let apiCall;

          switch (operation) {
            case GlobalTaFopFilesOperation.Create:
              apiCall = this.dataService.create(value);
              break;
            case GlobalTaFopFilesOperation.Update:
              apiCall = this.dataService.update(value);
              break;
            case GlobalTaFopFilesOperation.Delete:
              apiCall = this.dataService.delete();
              break;
          }

          return apiCall;
        }),
        tap((newValue: BspDto) =>
          this.store.dispatch(GlobalTaFopFilesActions.applyChangesSuccess({ value: { bsp: newValue } }))
        ),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
