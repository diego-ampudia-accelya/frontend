/* eslint-disable @typescript-eslint/naming-convention */
import { Permissions } from '~app/shared/constants';
import { BspDto } from '~app/shared/models/bsp.model';

export interface GlobalTaFopFilesConfiguration {
  bsp: BspDto;
}

export const modifyPermissionsGlobalTaFopFiles = [
  Permissions.createGlobalTaFopFiles,
  Permissions.updateGlobalTaFopFiles,
  Permissions.deleteGlobalTaFopFiles
];

export enum GlobalTaFopFilesOperation {
  Create,
  Update,
  Delete
}
