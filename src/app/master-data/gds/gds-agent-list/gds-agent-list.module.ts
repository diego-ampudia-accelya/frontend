import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { GdsAgentListComponent } from './gds-agent-list/gds-agent-list.component';
import { GdsAgentListEffects } from './store/effects/agent-by-gds.effects';
import * as fromGdsAgentList from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [GdsAgentListComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromGdsAgentList.GdsAgentListFeatureKey, fromGdsAgentList.reducers),
    EffectsModule.forFeature([GdsAgentListEffects])
  ],
  providers: [],
  exports: [GdsAgentListComponent]
})
export class GdsAgentListModule {}
