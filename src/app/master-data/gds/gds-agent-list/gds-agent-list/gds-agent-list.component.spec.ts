import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AgentDictionaryService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { GdsAgentListActive, ViewAs } from '../models';
import { GdsAgentListFormatterService } from '../services/gds-agent-list-formatter.service';
import { GdsAgentListListActions } from '../store/actions';
import * as fromList from '../store/reducers/list.reducer';
import { GdsAgentListComponent } from './gds-agent-list.component';

describe('GdsAgentListComponent', () => {
  let component: GdsAgentListComponent;

  let fixture: ComponentFixture<GdsAgentListComponent>;
  let activatedRoute;
  let agentDictionaryServiceStub: SpyObject<AgentDictionaryService>;
  let mockStore: MockStore;
  const displayFormatterStub = createSpyObject(GdsAgentListFormatterService);

  beforeEach(waitForAsync(() => {
    activatedRoute = {
      snapshot: {
        data: {
          viewAs: ViewAs.Iata
        }
      }
    };

    agentDictionaryServiceStub = createSpyObject(AgentDictionaryService);

    TestBed.configureTestingModule({
      declarations: [GdsAgentListComponent, TranslatePipeMock],
      providers: [
        provideMockStore({
          initialState: {
            auth: {},
            gdsAgentList: {
              list: fromList.initialState
            }
          },
          selectors: [{ selector: fromAuth.hasPermission('READ-AGENT-LIST'), value: true }]
        }),
        FormBuilder,
        L10nTranslationService,
        { provide: ActivatedRoute, useValue: activatedRoute },
        {
          provide: AgentDictionaryService,
          useValue: agentDictionaryServiceStub
        }
      ],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(GdsAgentListComponent, {
        set: {
          providers: [{ provide: GdsAgentListFormatterService, useValue: displayFormatterStub }]
        }
      })
      .compileComponents();

    mockStore = TestBed.inject<any>(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdsAgentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch search when query change', () => {
    spyOn(mockStore, 'dispatch');

    component.onQueryChanged(defaultQuery);

    expect(mockStore.dispatch).toHaveBeenCalledWith(GdsAgentListListActions.search({ query: defaultQuery }));
  });

  it('should dispatch download', () => {
    spyOn(mockStore, 'dispatch');

    component.onDownload();

    expect(mockStore.dispatch).toHaveBeenCalledWith(GdsAgentListListActions.download());
  });

  it('should check if iata and gds users are logged in', () => {
    const isGdsUser = component.isGdsUser;
    const isIataUser = component.isIataUser;

    expect(isGdsUser).toBeFalsy();
    expect(isIataUser).toBeTruthy();
  });

  it('should trigger initializeFiltersDropdown method when onFilterButtonClicked is triggered with a true boolean argument', () => {
    const spy = spyOn<any>(component, 'initializeFiltersDropdown');

    component.onFilterButtonClicked(true);

    expect(spy).toHaveBeenCalled();
  });

  it('should not trigger initializeFiltersDropdown method when onFilterButtonClicked is triggered with a false boolean argument', () => {
    const spy = spyOn<any>(component, 'initializeFiltersDropdown');

    component.onFilterButtonClicked(false);

    expect(spy).not.toHaveBeenCalled();
  });

  it('should get columns and initialize status badge info when getColumns method is triggered', () => {
    const spy = spyOn<any>(component, 'initializeStatusBadgeInfo');
    component['getColumns']();

    expect(component.columns).not.toBeUndefined();
    expect(spy).toHaveBeenCalled();
  });

  it('should get dropdown options when initializeFiltersDropdown method is triggered', () => {
    component['initializeFiltersDropdown']();

    expect(agentDictionaryServiceStub.getDropdownOptions).toHaveBeenCalled();
  });

  it('should add badgeInfo when a column has an active property and initializeStatusBadgeInfo method is triggered', () => {
    component.columns = [
      {
        prop: 'active',
        draggable: false,
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        flexGrow: 5
      }
    ];

    displayFormatterStub.formatActive.and.returnValue(GdsAgentListActive.active);
    const statusColumn = component.columns.find(col => col.prop === 'active');
    statusColumn.pipe = {
      transform: value => displayFormatterStub.formatActive(value)
    };

    component['initializeStatusBadgeInfo']();
    expect(statusColumn.badgeInfo).toBeDefined();
    expect(statusColumn.pipe).toBeDefined();
  });

  it('should trigger the getColumns method when the user has the necessary permissions and ngOnInit is called', () => {
    spyOn(mockStore, 'pipe').and.returnValue(of(true));
    spyOn<any>(component, 'getColumns');
    component.ngOnInit();

    expect(component['getColumns']).toHaveBeenCalled();
  });
});
