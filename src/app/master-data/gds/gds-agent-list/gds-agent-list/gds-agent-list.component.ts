import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';

import { GdsAgentList, GdsAgentListFilter, ViewAs } from '../models';
import { GdsAgentListFormatterService } from '../services/gds-agent-list-formatter.service';
import { GdsAgentListListActions } from '../store/actions';
import * as fromGdsAgentList from '../store/reducers';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { GridColumn } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-gds-agent-list',
  templateUrl: './gds-agent-list.component.html',
  styleUrls: ['./gds-agent-list.component.scss'],
  providers: [GdsAgentListFormatterService]
})
export class GdsAgentListComponent implements OnInit {
  public loading$ = this.store.pipe(select(fromGdsAgentList.getListLoading));
  public data$ = this.store.pipe(select(fromGdsAgentList.getListData));
  public query$ = this.store.pipe(select(fromGdsAgentList.getListQuery));
  public hasData$ = this.store.pipe(select(fromGdsAgentList.hasData));
  public predefinedFilters: GdsAgentListFilter = { gdsAssignment: true };

  public columns: GridColumn[] = [];
  public searchForm: FormGroup;

  public agentOptions$: Observable<DropdownOption[]>;

  public gdsUseOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatUsed(value)
  }));

  public activeOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatActive(value)
  }));

  public get isGdsUser(): boolean {
    return this.viewAs === ViewAs.Gds;
  }

  public get isIataUser(): boolean {
    return this.viewAs === ViewAs.Iata;
  }

  public gdsCodeOptions$: Observable<DropdownOption[]>;

  public get viewAs(): ViewAs {
    return this.activatedRoute.snapshot.data.viewAs;
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  public ButtonDesign = ButtonDesign;

  public isUpdateAllVisible = false;

  constructor(
    public displayFormatter: GdsAgentListFormatterService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private agentDictionaryService: AgentDictionaryService,
    private translationService: L10nTranslationService
  ) {}

  public ngOnInit(): void {
    this.store.pipe(select(fromAuth.hasPermission(Permissions.readAgentGds))).subscribe(canReadAll => {
      if (canReadAll) {
        this.getColumns();
      }
    });
    this.searchForm = this.buildForm();
  }

  public onQueryChanged(query: DataQuery<GdsAgentListFilter>): void {
    this.store.dispatch(GdsAgentListListActions.search({ query }));
  }

  public onDownload(): void {
    this.store.dispatch(GdsAgentListListActions.download());
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.initializeFiltersDropdown();
    }
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      agentId: [''],
      gdsAssignment: '',
      active: ''
    });
  }

  private initializeFiltersDropdown() {
    this.agentOptions$ = this.agentDictionaryService.getDropdownOptions();
  }

  private getColumns() {
    this.columns = [
      {
        name: this.translationService.translate(`LIST.MASTER_DATA.GDS.COLUMNS.CODE`),
        prop: 'iataCode',
        flexGrow: 5
      },
      {
        name: this.translationService.translate(`LIST.MASTER_DATA.GDS.COLUMNS.AGENT-NAME`),
        prop: 'name',
        flexGrow: 5
      },
      {
        name: this.translationService.translate(`LIST.MASTER_DATA.GDS.COLUMNS.STATUS`),
        prop: 'active',
        draggable: false,
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        flexGrow: 5
      }
    ];
    this.initializeStatusBadgeInfo();
  }

  private initializeStatusBadgeInfo() {
    const statusColumn = this.columns.find(col => col.prop === 'active');

    if (statusColumn) {
      statusColumn.pipe = { transform: value => this.displayFormatter.formatActive(value) };

      statusColumn.badgeInfo = {
        hidden: (value: GdsAgentList) => !value.inProcess,
        value: this.translationService.translate(`LIST.MASTER_DATA.GDS.COLUMNS.IN_PROCESS`),
        type: (value: GdsAgentList) => {
          if (value.inProcess) {
            return BadgeInfoType.info;
          }
        },
        tooltipLabel: (value: GdsAgentList) =>
          value.inProcess ? this.translationService.translate(`LIST.MASTER_DATA.GDS.COLUMNS.IN_PROCESS`) : '',
        showIconType: true
      };
    }
  }
}
