import { createAction, props } from '@ngrx/store';

import { GdsAgentListFilter, GdsAgentListViewModel } from '../../models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const changeRequiredFilter = createAction(
  '[Agent By Gds] Change Required Filter',
  props<{ requiredFilter: GdsAgentListFilter }>()
);
export const changeRequiredFilterSuccess = createAction(
  '[Agent By Gds] Change Required Filter Success',
  props<{
    data: PagedData<GdsAgentListViewModel>;
  }>()
);
export const changeRequiredFilterError = createAction('[Agent By Gds] Change Required Filter Error');

export const search = createAction('[Agent By Gds] Search', props<{ query?: DataQuery<GdsAgentListFilter> }>());
export const searchConfirmed = createAction('[Agent By Gds] Search Confirmed');
export const searchCancelled = createAction('[Agent By Gds] Search Cancelled');
export const searchSuccess = createAction(
  '[Agent By Gds] Search Success',
  props<{ data: PagedData<GdsAgentListViewModel> }>()
);
export const searchError = createAction('[Agent By Gds] Search Error');
export const download = createAction('[Agent By Gds] Download');
export const downloadConfirmed = createAction('[Agent By Gds] Download Confirmed');
