import { GdsAgentList, GdsAgentListViewModel } from '../../models';
import { PagedData } from '~app/shared/models/paged-data.model';

const toViewModel = (dataModel: GdsAgentList): GdsAgentListViewModel => ({ ...dataModel, original: dataModel });

const toDataModel = (viewModel: GdsAgentListViewModel): GdsAgentList => {
  const { original, ...dataModel } = viewModel;

  return dataModel;
};

const toViewModels = (pagedData: PagedData<GdsAgentList>): PagedData<GdsAgentListViewModel> => ({
  ...pagedData,
  records: (pagedData?.records || []).map(toViewModel)
});

export const converter = { toViewModel, toDataModel, toViewModels };
