import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';

import { GdsAgentListService } from '../../services/gds-agent-list.service';
import { GdsAgentListListActions } from '../actions';
import * as fromAgentByGds from '../reducers';

import { converter } from './converters';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class GdsAgentListEffects {
  constructor(
    private actions$: Actions,
    private dataService: GdsAgentListService,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {}

  public changeRequiredFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsAgentListListActions.changeRequiredFilter),
      withLatestFrom(
        this.store.pipe(select(fromAgentByGds.getListQuery)),
        this.store.pipe(select(fromAgentByGds.getRequiredFilter))
      ),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(result =>
            GdsAgentListListActions.changeRequiredFilterSuccess({
              data: converter.toViewModels(result)
            })
          ),
          rethrowError(() => GdsAgentListListActions.changeRequiredFilterError())
        )
      )
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsAgentListListActions.search),
      map(dialogResult =>
        !dialogResult ? GdsAgentListListActions.searchCancelled() : GdsAgentListListActions.searchConfirmed()
      )
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsAgentListListActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromAgentByGds.getListQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(converter.toViewModels),
          map(data => GdsAgentListListActions.searchSuccess({ data })),
          rethrowError(() => GdsAgentListListActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GdsAgentListListActions.download),
      switchMap(() => of(GdsAgentListListActions.downloadConfirmed()))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(GdsAgentListListActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromAgentByGds.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );
}
