import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, ReplaySubject, throwError } from 'rxjs';

import { GdsAgentList, GdsAgentListFilter, GdsAgentListViewModel } from '../../models';
import { GdsAgentListService } from '../../services/gds-agent-list.service';
import { GdsAgentListListActions } from '../actions';
import * as fromGdsAgentList from '../reducers';
import { GdsAgentListEffects } from './agent-by-gds.effects';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';
import { NotificationService } from '~app/shared/services/notification.service';

describe('GdsAgentListEffects', () => {
  let effects: GdsAgentListEffects;
  let gdsAgentListServiceStub: SpyObject<GdsAgentListService>;
  let dialogServiceStub: SpyObject<DialogService>;
  let notificationServiceStub: SpyObject<NotificationService>;
  let actions$: Observable<Action>;
  let mockStore: MockStore<AppState>;

  const gdsAgentList1: GdsAgentList = {
    id: 1,
    bsp: { id: 1, isoCountryCode: 'Spain', name: 'GdsAgent1' },
    active: true,
    inProcess: false,
    iataCode: '202012',
    name: 'Agent1'
  };
  const gdsAgentList2: GdsAgentList = {
    id: 2,
    bsp: { id: 2, isoCountryCode: 'Spain', name: 'GdsAgent2' },
    active: true,
    inProcess: false,
    iataCode: '202013',
    name: 'Agent2'
  };

  beforeEach(waitForAsync(() => {
    gdsAgentListServiceStub = createSpyObject(GdsAgentListService);
    notificationServiceStub = createSpyObject(NotificationService);
    dialogServiceStub = createSpyObject(DialogService);
    dialogServiceStub.open.and.returnValue(of(null));

    TestBed.configureTestingModule({
      providers: [
        GdsAgentListEffects,
        provideMockStore({
          selectors: [
            { selector: fromGdsAgentList.getRequiredFilter, value: {} },
            { selector: fromGdsAgentList.getListQuery, value: defaultQuery }
          ]
        }),
        provideMockActions(() => actions$),
        { provide: GdsAgentListService, useValue: gdsAgentListServiceStub },
        { provide: NotificationService, useValue: notificationServiceStub },
        { provide: DialogService, useValue: dialogServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(GdsAgentListEffects);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('`changeRequiredFilter$`', () => {
    it('should dispatch `changeRequiredFilterSuccess` with the found data and options', fakeAsync(() => {
      const data: PagedData<GdsAgentList> = {
        records: [gdsAgentList1, gdsAgentList2],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const expectedData: PagedData<GdsAgentListViewModel> = {
        records: [
          { ...gdsAgentList1, original: gdsAgentList1 },
          { ...gdsAgentList2, original: gdsAgentList2 }
        ],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const requiredFilter: GdsAgentListFilter = {
        gdsAssignment: true
      };

      const expectedAction = GdsAgentListListActions.changeRequiredFilterSuccess({
        data: expectedData
      });

      mockStore.overrideSelector(fromGdsAgentList.getRequiredFilter, requiredFilter);
      gdsAgentListServiceStub.find.and.returnValue(of(data));

      actions$ = of(GdsAgentListListActions.changeRequiredFilter({ requiredFilter }));

      effects.changeRequiredFilter$.subscribe((action: any) => {
        expect(action).toEqual(expectedAction);
      });
    }));

    it('should dispatch `changeRequiredFilterError` when `find` throws an error', fakeAsync(() => {
      const requiredFilter: GdsAgentListFilter = {
        gdsAssignment: true
      };
      const expectedAction = GdsAgentListListActions.changeRequiredFilterError();

      mockStore.overrideSelector(fromGdsAgentList.getRequiredFilter, requiredFilter);
      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));

      actions$ = of(GdsAgentListListActions.changeRequiredFilter({ requiredFilter }));

      effects.changeRequiredFilter$.subscribe(
        (action: any) => {
          expect(action).toEqual(expectedAction);
        },
        error => expect(error).toBeDefined()
      );
    }));

    it('should continue processing actions after an error', fakeAsync(() => {
      const requiredFilter: GdsAgentListFilter = {
        gdsAssignment: true
      };
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const actionsSubject = new ReplaySubject<Action>(1);

      actions$ = actionsSubject.asObservable();

      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));
      actionsSubject.next(GdsAgentListListActions.changeRequiredFilter({ requiredFilter }));
      tick();

      gdsAgentListServiceStub.find.and.returnValue(of(data));
      actionsSubject.next(GdsAgentListListActions.changeRequiredFilter({ requiredFilter }));
      tick();

      const successAction = GdsAgentListListActions.changeRequiredFilterSuccess({
        data
      });

      const actions = [];
      effects.changeRequiredFilter$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );

      tick();
      expect(actions).toEqual([successAction]);
    }));
  });

  describe('`search$`', () => {
    it('should dispatch searchConfirmed action when the user has confirmed the operation', fakeAsync(() => {
      actions$ = of(GdsAgentListListActions.search({ query: defaultQuery }));

      let result;
      effects.search$.subscribe(action => (result = action));

      expect(result.type).toEqual(GdsAgentListListActions.searchConfirmed.type);
    }));
  });

  describe('`searchConfirmed$`', () => {
    it('should dispatch `searchSuccess` with found data', fakeAsync(() => {
      const searchAction = GdsAgentListListActions.searchConfirmed();
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 0,
        pageSize: 20,
        pageNumber: 0
      };
      const expectedAction = GdsAgentListListActions.searchSuccess({ data });
      mockStore.overrideSelector(fromGdsAgentList.getListQuery, defaultQuery);
      actions$ = of(searchAction);
      gdsAgentListServiceStub.find.and.returnValue(of(data));

      const actions = [];
      effects.searchConfirmed$.subscribe(action => actions.push(action));

      expect(actions).toEqual([expectedAction]);
    }));

    it('should dispatch `searchError` when find throws an error', fakeAsync(() => {
      const searchAction = GdsAgentListListActions.searchConfirmed();
      const expectedAction = GdsAgentListListActions.searchError();
      actions$ = of(searchAction);
      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));

      const actions = [];
      effects.searchConfirmed$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );
      tick();
      expect(actions).toEqual([expectedAction]);
    }));

    it('should should continue processing actions after an error', fakeAsync(() => {
      const searchAction = GdsAgentListListActions.searchConfirmed();
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 0,
        pageSize: 20,
        pageNumber: 0
      };
      const actionsSubject = new ReplaySubject<Action>(1);

      actions$ = actionsSubject.asObservable();

      const actions = [];

      gdsAgentListServiceStub.find.and.returnValue(throwError('Error'));
      actionsSubject.next(searchAction);

      gdsAgentListServiceStub.find.and.returnValue(of(data));
      actionsSubject.next(searchAction);

      effects.searchConfirmed$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );
      tick();
      const successAction = GdsAgentListListActions.searchSuccess({ data });
      expect(actions).toEqual([successAction]);
    }));
  });
});
