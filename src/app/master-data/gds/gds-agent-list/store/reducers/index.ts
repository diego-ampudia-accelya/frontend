/* eslint-disable @typescript-eslint/naming-convention */
import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './list.reducer';
import { AppState } from '~app/reducers';

export const GdsAgentListFeatureKey = 'gdsAgentList';

export interface State extends AppState {
  [GdsAgentListFeatureKey]: GdsAgentListState;
}

export interface GdsAgentListState {
  [fromList.key]: fromList.State;
}

export function reducers(state: GdsAgentListState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getAgentByGds = createFeatureSelector<State, GdsAgentListState>(GdsAgentListFeatureKey);

export const getList = createSelector(getAgentByGds, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getListQuery = createSelector(getList, fromList.getQuery);

export const getDownloadQuery = createSelector(
  getListQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

export const getRequiredFilter = createSelector(getList, fromList.getRequiredFilter);
