import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { GdsAgentList, GdsAgentListFilter, GdsAgentListViewModel } from '../../models';
import { GdsAgentListListActions } from '../actions';
import * as fromList from './list.reducer';

describe('gds agent list reducer', () => {
  describe('on search', () => {
    it('should cancel the search when searchCancelled action is triggered', () => {
      const action = GdsAgentListListActions.searchCancelled();
      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.previousQuery).toEqual(null);
      expect(newState.query).toEqual(fromList.initialState.previousQuery);
    });

    it('should update query on load', () => {
      const action = GdsAgentListListActions.search({ query: defaultQuery });
      const expected: DataQuery<GdsAgentListFilter> = {
        ...defaultQuery
      };

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.query).toEqual(expected);
    });

    it('should set loading to true on searchConfirmed', () => {
      const action = GdsAgentListListActions.searchConfirmed();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeTruthy();
    });

    it('should set loading to false on searchSuccess', () => {
      const action = GdsAgentListListActions.searchSuccess({
        data: {
          records: [{} as GdsAgentListViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set totalElements and data on searchSuccess', () => {
      const action = GdsAgentListListActions.searchSuccess({
        data: {
          records: [{} as GdsAgentListViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data.length).toBe(1);
      expect(newState.query.paginateBy).toEqual({ page: 0, size: 20, totalElements: 1 });
    });

    it('should set loading to false on searchError', () => {
      const action = GdsAgentListListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set data to [] on searchError', () => {
      const action = GdsAgentListListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data).toEqual([]);
    });
  });

  describe('on changeRequiredFilter', () => {
    it('should preserve query when it matches required filter', () => {
      const existingQuery: DataQuery<GdsAgentListFilter> = {
        ...defaultQuery,
        filterBy: { gdsAssignment: true }
      };

      const requiredFilter = { gdsAssignment: false };

      const state: fromList.State = {
        ...fromList.initialState,
        requiredFilter: { gdsAssignment: true }
      };
      const action = GdsAgentListListActions.changeRequiredFilter({
        requiredFilter
      });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(existingQuery);
    });

    it('should change required filter success when changeRequiredFilterSuccess action is triggered', () => {
      const mockData = {
        records: [
          {
            id: 1,
            bsp: { id: 101, isoCountryCode: 'US', name: 'BSP US' },
            active: true,
            inProcess: false,
            iataCode: 'IATA123',
            name: 'Agent 1',
            original: {
              id: 1,
              bsp: { id: 101, isoCountryCode: 'ES', name: 'BSP US' },
              active: true,
              inProcess: false,
              iataCode: 'IATA123',
              name: 'Agent 1'
            }
          } as GdsAgentListViewModel
        ],
        totalPages: 0,
        total: 0,
        pageNumber: 0,
        pageSize: 20
      };

      const existingQuery: DataQuery<GdsAgentListFilter> = {
        ...defaultQuery,
        filterBy: { gdsAssignment: true }
      };

      const requiredFilter = { gdsAssignment: true };

      const state: fromList.State = {
        ...fromList.initialState,
        requiredFilter,
        query: existingQuery
      };

      const action = GdsAgentListListActions.changeRequiredFilterSuccess({
        data: mockData
      });
      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(existingQuery);
    });

    it('should reset to default query when the query does not match required filter', () => {
      const existingQuery: DataQuery<GdsAgentListFilter> = {
        ...defaultQuery,
        filterBy: { gdsAssignment: true }
      };
      const requiredFilter: GdsAgentListFilter = { gdsAssignment: true };
      const expectedQuery: DataQuery<GdsAgentListFilter> = {
        ...defaultQuery,
        filterBy: requiredFilter
      };
      const state: fromList.State = {
        ...fromList.initialState,
        query: existingQuery
      };
      const action = GdsAgentListListActions.changeRequiredFilter({ requiredFilter });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(expectedQuery);
    });
  });

  describe('Selectors', () => {
    const mockData: GdsAgentListViewModel[] = [
      {
        id: 1,
        bsp: { id: 101, isoCountryCode: 'US', name: 'BSP US' },
        active: true,
        inProcess: false,
        iataCode: 'IATA123',
        name: 'Agent 1',
        original: {
          id: 1,
          bsp: { id: 101, isoCountryCode: 'ES', name: 'BSP US' },
          active: true,
          inProcess: false,
          iataCode: 'IATA123',
          name: 'Agent 1'
        }
      }
    ];
    it('should get loading as true when getLoading selector is triggered', () => {
      const state: fromList.State = {
        ...fromList.initialState
      };
      const result = fromList.getLoading(state);

      expect(result).toBeFalsy();
    });

    it('should get data when getData selector is triggered', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: mockData
      };
      const result = fromList.getData(state);

      expect(result).toEqual(mockData);
    });

    it('should get query when getQuery selector is triggered', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        query: defaultQuery
      };
      const result = fromList.getQuery(state);

      expect(result).toEqual(state.query);
    });

    it('should get query when getQuery selector is triggered', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        query: defaultQuery
      };
      const result = fromList.getQuery(state);

      expect(result).toEqual(state.query);
    });

    it('should get Required Filter when getRequiredFilter selector is triggered', () => {
      const mockRequiredFilter: GdsAgentListFilter = {
        gdsAssignment: true
      };
      const state: fromList.State = {
        ...fromList.initialState,
        requiredFilter: mockRequiredFilter
      };
      const result = fromList.getRequiredFilter(state);

      expect(result).toEqual(state.requiredFilter);
    });

    it('should get check if has changes when hasChanges selector is triggered', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: mockData
      };
      const result = fromList.hasChanges(state);

      expect(result).toBeTruthy();
    });

    it('should compare original and the item when getChanges selector is triggered', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: mockData
      };
      const result = fromList.getChanges(state);

      expect(result).toEqual(mockData);
    });

    it('should check if have same Id when haveSameId selector is triggered', () => {
      const mockDataA: GdsAgentList = {
        id: 1,
        bsp: { id: 101, isoCountryCode: 'US', name: 'BSP US' },
        active: true,
        inProcess: false,
        iataCode: 'IATA123',
        name: 'Agent 1'
      };
      const mockDataB: GdsAgentList = {
        id: 2,
        bsp: { id: 101, isoCountryCode: 'US', name: 'BSP US' },
        active: true,
        inProcess: false,
        iataCode: 'IATA123',
        name: 'Agent 1'
      };
      const result = fromList.haveSameId(mockDataA, mockDataB);

      expect(result).toBeFalsy();
    });
  });
});
