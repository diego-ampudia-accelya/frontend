import { createReducer, on } from '@ngrx/store';
import { cloneDeep, isEqual } from 'lodash';

import { GdsAgentList, GdsAgentListFilter, GdsAgentListViewModel } from '../../models';
import { GdsAgentListListActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<GdsAgentListFilter>;
  previousQuery: DataQuery<GdsAgentListFilter>;
  data: GdsAgentListViewModel[];
  loading: boolean;
  requiredFilter: GdsAgentListFilter;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false,
  requiredFilter: { gdsAssignment: true }
};

const loadData = (state: State, data: PagedData<GdsAgentListViewModel>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (
  newRequiredFilter: GdsAgentListFilter,
  oldRequiredFilter: GdsAgentListFilter,
  storedQuery: DataQuery<GdsAgentListFilter>
): DataQuery<GdsAgentListFilter> => {
  let query: DataQuery<GdsAgentListFilter> = storedQuery || cloneDeep(defaultQuery);

  if (!isEqual(newRequiredFilter, oldRequiredFilter)) {
    query = cloneDeep(defaultQuery);
    query.filterBy = newRequiredFilter;
  }

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = { ...query.filterBy, gdsAssignment: true };

  return initialQuery;
};

export const reducer = createReducer(
  initialState,
  on(GdsAgentListListActions.changeRequiredFilter, (state, { requiredFilter }) => ({
    ...state,
    requiredFilter,
    loading: true,
    query: determineInitialQuery(requiredFilter, state.requiredFilter, state.query)
  })),
  on(GdsAgentListListActions.changeRequiredFilterSuccess, (state, { data }) => ({
    ...loadData(state, data)
  })),
  on(GdsAgentListListActions.search, (state, { query = state.query }) => ({
    ...state,
    query: {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    },
    previousQuery: state.query
  })),
  on(GdsAgentListListActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(GdsAgentListListActions.searchCancelled, state => ({
    ...state,
    previousQuery: null,
    query: state.previousQuery
  })),
  on(GdsAgentListListActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(GdsAgentListListActions.searchError, GdsAgentListListActions.changeRequiredFilterError, state => ({
    ...state,
    loading: false,
    data: []
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;

export const getRequiredFilter = ({ requiredFilter }: State) => requiredFilter;

export const hasChanges = (state: State) => getChanges(state).length > 0;

export const getChanges = ({ data }: State) => data.filter(({ original, ...item }) => !isEqual(item, original));

export const haveSameId = (a: GdsAgentList, b: GdsAgentList) => a.id === b.id;
