export enum GdsAgentListUse {
  use = 'agentGdsAssigned',
  notUse = 'agentGdsUnassigned'
}

export enum GdsAgentListActive {
  active = 'active',
  inactive = 'inactive'
}
