import { ViewAs } from '.';
import { AgentSummary } from '~app/shared/models';

export interface GdsAgentListFilter {
  gdsAssignment: boolean;
  agentId?: AgentSummary[];
  active?: boolean;
  viewAs?: ViewAs;
}
