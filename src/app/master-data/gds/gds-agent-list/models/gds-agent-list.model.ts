export interface GdsAgentList {
  id: number;
  bsp: { id: number; isoCountryCode: string; name: string };
  active: boolean;
  inProcess: boolean;
  iataCode: string;
  name: string;
}

export interface GdsAgentListViewModel extends GdsAgentList {
  original: GdsAgentList;
}
