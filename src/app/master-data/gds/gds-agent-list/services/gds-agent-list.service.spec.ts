import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';
import { ViewAs } from '../models';
import { GdsAgentListService } from './gds-agent-list.service';

describe('GdsAgentListService', () => {
  let service: GdsAgentListService;
  let httpClientSpy: SpyObject<HttpClient>;

  const activatedRouteStub = {
    snapshot: {
      data: {},
      params: {}
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        GdsAgentListService,
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, { baseApiPath: 'api' }),
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });
    service = TestBed.inject(GdsAgentListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set gds Id', () => {
    service.setGdsId('1');

    expect(service['gdsId']).toEqual('1');
  });

  it('should set viewAs Iata when setViewAs is triggered', () => {
    service.setViewAs(ViewAs.Iata);

    expect(service['viewAs']).not.toBeUndefined();
    expect(service['viewAs']).toEqual(ViewAs.Iata);
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    service.find(defaultQuery).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/bsp-management/bsps/gds-assignments/mine/agents?page=0&size=20');
  }));

  it('should send httpClient get request when download method is triggered', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/download?page=0&size=20&downloadType=txt';

    service.download(defaultQuery, exportOptions).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));

  it('should create Url when viewAs is equal to Iata user', () => {
    service['gdsId'] = '1';
    service['viewAs'] = ViewAs.Iata;

    service['createUrl']();

    expect(service.baseUrl).toEqual('/bsp-management/bsps/gds-assignments/1/agents');
  });
});
