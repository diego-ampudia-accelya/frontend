import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GdsAgentList, GdsAgentListFilter, ViewAs } from '../models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService, appConfiguration } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class GdsAgentListService extends ApiService<GdsAgentList> implements Queryable<GdsAgentList> {
  private gdsId: string;
  private viewAs: string;

  private sortMapper = [
    {
      from: 'iataCode',
      to: 'iataCode'
    },
    {
      from: 'name',
      to: 'name'
    },
    {
      from: 'active',
      to: 'active'
    },
    {
      from: 'inProcess',
      to: 'inProcess'
    }
  ];

  constructor(public http: HttpClient) {
    super(http, 'bsp');
  }

  public setGdsId(id) {
    this.gdsId = id;
  }

  public setViewAs(viewas: ViewAs) {
    this.viewAs = viewas;
  }

  public find(query: DataQuery<GdsAgentListFilter>): Observable<PagedData<GdsAgentList>> {
    this.createUrl();
    const requestQuery = this.formatQuery(query);
    const url = `${this.baseUrl}` + requestQuery.getQueryString();

    return this.http.get<PagedData<GdsAgentList>>(url);
  }

  public download(
    query: DataQuery<GdsAgentListFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, downloadType: downloadFormat };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<GdsAgentListFilter>): RequestQuery {
    const { gdsAssignment, agentId, active } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        gdsAssignment,
        agentId: agentId && agentId.map(id => id.id),
        active
      },
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
  }

  private createUrl() {
    if (this.viewAs === ViewAs.Iata) {
      this.baseUrl = `${appConfiguration.baseApiPath}/bsp-management/bsps/gds-assignments/${this.gdsId}/agents`;
    } else {
      this.baseUrl = `${appConfiguration.baseApiPath}/bsp-management/bsps/gds-assignments/mine/agents`;
    }
  }
}
