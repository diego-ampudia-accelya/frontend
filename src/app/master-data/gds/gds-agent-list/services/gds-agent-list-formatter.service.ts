import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { memoize } from 'lodash';

import { GdsAgentListActive, GdsAgentListFilter, GdsAgentListUse } from '../models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class GdsAgentListFormatterService implements FilterFormatter {
  public ignoredFilters: Array<keyof GdsAgentListFilter> = [];

  constructor(private translation: L10nTranslationService) {
    this.formatUsed = memoize(this.formatUsed).bind(this);
  }

  public format(filter: GdsAgentListFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<GdsAgentListFilter>> = {
      agentId: agentId => `${this.translate('agentCodeFilterLabel')} - ${agentId.map(gd => gd.name).join(', ')}`,
      gdsAssignment: use => `${this.formatUsed(use)}`,
      active: active => `${this.formatActive(active)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public formatUsed(use: boolean): string {
    return this.translate(use ? GdsAgentListUse.use : GdsAgentListUse.notUse);
  }

  public formatActive(active: boolean): string {
    return this.translate(active ? GdsAgentListActive.active : GdsAgentListActive.inactive);
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.MASTER_DATA.agentByGds.' + key);
  }
}
