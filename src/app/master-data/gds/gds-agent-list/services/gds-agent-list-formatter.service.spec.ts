import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { GdsAgentListFilter } from '../models';

import { GdsAgentListFormatterService } from './gds-agent-list-formatter.service';
import { AgentSummary } from '~app/shared/models';

describe('GdsAgentListFormatterService', () => {
  let formatter: GdsAgentListFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new GdsAgentListFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format when gds assignment filter is defined', () => {
    const filter: GdsAgentListFilter = {
      gdsAssignment: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['gdsAssignment'],
        label: 'LIST.MASTER_DATA.agentByGds.agentGdsAssigned'
      }
    ]);
  });

  it('should format when agent active filter is defined', () => {
    const filter: GdsAgentListFilter = {
      gdsAssignment: true,
      active: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['gdsAssignment'],
        label: 'LIST.MASTER_DATA.agentByGds.agentGdsAssigned'
      },
      {
        keys: ['active'],
        label: 'LIST.MASTER_DATA.agentByGds.active'
      }
    ]);
  });

  it('should format when agents Id is defined', () => {
    const filter: GdsAgentListFilter = {
      gdsAssignment: true,
      agentId: [
        {
          id: '0',
          code: '123',
          name: 'Agent Smith'
        },
        { id: '1', code: '456', name: 'Agent Anderson' }
      ] as AgentSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['gdsAssignment'],
        label: 'LIST.MASTER_DATA.agentByGds.agentGdsAssigned'
      },
      {
        keys: ['agentId'],
        label: 'LIST.MASTER_DATA.agentByGds.agentCodeFilterLabel - Agent Smith, Agent Anderson'
      }
    ]);
  });

  it('should return empty array when the filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      unknownProp: 'test'
    };
    expect(formatter.format(filter)).toEqual([]);
  });
});
