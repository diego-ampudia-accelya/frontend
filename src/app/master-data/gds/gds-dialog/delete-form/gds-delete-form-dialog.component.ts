import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { ErrorMessageService } from '~app/master-data/services/error-message.service';
import { GdsService } from '~app/master-data/services/gds.service';
import { MasterDialogForm } from '~app/master-shared/master-dialog-form/master-dialog-form.class';
import { MasterDataForRequest } from '~app/master-shared/models/master.model';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-gds-delete-form-dialog',
  templateUrl: './gds-delete-form-dialog.component.html',
  styleUrls: ['./gds-delete-form-dialog.component.scss']
})
export class GdsDeleteFormDialogComponent extends MasterDialogForm implements OnInit {
  public form: FormGroup; // Not used. Only to satisfy parent class
  protected formConstants; // Not used. Only to satisfy parent class

  // Template variables
  public question: string;
  public code: string;
  public name: string;

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public gdsService: GdsService,
    public notificationService: NotificationService,
    public errorMessageService: ErrorMessageService,
    public translationService: L10nTranslationService
  ) {
    super(
      config,
      gdsService,
      reactiveSubject,
      dialogService,
      notificationService,
      errorMessageService,
      translationService
    );
  }

  ngOnInit() {
    super.setDialogClickCallback();
    this.question = this.dialogInjectedData.question;
    this.code = this.dialogInjectedData.rowTableContent.gdsCode;
    this.name = this.dialogInjectedData.rowTableContent.name;
  }

  protected generateDataForRequestByAction(action?: any): MasterDataForRequest {
    return { id: this.dialogInjectedData.rowTableContent.id };
  }
}
