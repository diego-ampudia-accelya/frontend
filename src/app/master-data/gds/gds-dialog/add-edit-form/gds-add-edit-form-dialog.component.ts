import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { GDS_FORM } from '~app/master-data/gds/gds-dialog/constants/gds-form.constants';
import { ErrorMessageService } from '~app/master-data/services/error-message.service';
import { GdsService } from '~app/master-data/services/gds.service';
import { MasterDialogForm } from '~app/master-shared/master-dialog-form/master-dialog-form.class';
import { MasterDataForRequest } from '~app/master-shared/models/master.model';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { InputComponent } from '~app/shared/components/input/input.component';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-gds-add-edit-form-dialog',
  templateUrl: './gds-add-edit-form-dialog.component.html',
  styleUrls: ['./gds-add-edit-form-dialog.component.scss']
})
export class GdsAddEditFormDialogComponent extends MasterDialogForm implements OnInit {
  @ViewChildren(InputComponent) bsplInputs: InputComponent;
  protected formConstants;
  public form: FormGroup;
  public isActionTypeAdd: boolean;

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public gdsService: GdsService,
    public notificationService: NotificationService,
    public errorMessageService: ErrorMessageService,
    public translationService: L10nTranslationService,
    private formBuilder: FormBuilder
  ) {
    super(
      config,
      gdsService,
      reactiveSubject,
      dialogService,
      notificationService,
      errorMessageService,
      translationService
    );
    this.formConstants = GDS_FORM;
  }

  ngOnInit() {
    this.createForm();
    super.setDialogClickCallback();

    this.isActionTypeAdd = this.dialogInjectedData.actionType === GridTableActionType.Add;
  }

  createForm() {
    this.form = this.formBuilder.group({
      name: [this.dialogInjectedData.rowTableContent.name, [Validators.required, Validators.maxLength(255)]],
      gdsCode: [
        this.dialogInjectedData.rowTableContent.gdsCode,
        [Validators.required, Validators.pattern('[A-Z0-9]{4}')]
      ]
    });
  }

  protected generateDataForRequestByAction(action?: any): MasterDataForRequest {
    const dataForRequest: MasterDataForRequest = { version: 0, ...this.form.value };

    if (action.clickedBtn === FooterButton.Update) {
      dataForRequest.id = this.dialogInjectedData.rowTableContent.id;
      dataForRequest.version = this.dialogInjectedData.rowTableContent.version;
    }

    return dataForRequest;
  }

  // TODO Check if this is neccessary to show errors in inputs
  /*
  initGdsCodeErrorMessage() {
    this.subscriptions.push(
      this.gdsNameInput.valueChange.subscribe(gdsCodeValue => {
        if (gdsCodeValue.length > 255) {
          this.gdsErrorMessage = 'The field value can not be greather than 255 symbols';
        } else {
          this.gdsErrorMessage = 'This field is required';
        }
      })
    );
  }
  */
}
