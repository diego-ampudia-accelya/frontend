/* eslint-disable @typescript-eslint/naming-convention */
import { MasterDataType, MasterTableConfig } from '~app/master-shared/models/master.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

export const GDS_LIST: MasterTableConfig = {
  get: {
    get [MasterDataType.GDS]() {
      return {
        COLUMNS: [
          {
            prop: 'gdsCode',
            name: 'LIST.MASTER_DATA.GDS.COLUMNS.CODE',
            resizeable: false,
            draggable: false,
            minWidth: 100
          },
          {
            prop: 'name',
            name: 'LIST.MASTER_DATA.GDS.COLUMNS.NAME',
            resizeable: false,
            draggable: false
          }
        ],
        ACTIONS: [GridTableActionType.Delete],
        PRIMARY_ACTIONS: [{ action: GridTableActionType.Edit }]
      };
    }
  }
};
