import { Component, ViewChild } from '@angular/core';

import { Gds } from '~app/master-data/models/gds.model';
import { GdsService } from '~app/master-data/services/gds.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ViewListComponent } from '~app/shared/components';
import { DataSource } from '~app/shared/services/data-source.service';

@Component({
  selector: 'bspl-gds-list',
  templateUrl: './gds-list.component.html',
  styleUrls: ['./gds-list.component.scss']
})
export class GdsListComponent {
  @ViewChild('gdsList', { static: true }) gdsList: ViewListComponent;

  public dataSource = new DataSource<Gds>(this.gdsService);
  public masterDataType: MasterDataType = MasterDataType.CURRENCY;
  public currentItem;

  constructor(public gdsService: GdsService) {
    this.masterDataType = MasterDataType.GDS;
  }
}
