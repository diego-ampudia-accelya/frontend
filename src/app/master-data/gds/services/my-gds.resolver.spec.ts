import { fakeAsync, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';

import { MyGdsResolver } from './my-gds.resolver';
import { State } from '~app/auth/selectors/auth.selectors';
import { GdsUser, UserType } from '~app/shared/models/user.model';

describe('MyGdsResolver', () => {
  let mockStore: MockStore<State>;
  let resolver: MyGdsResolver;

  const initialState: State = {
    router: null,
    auth: {
      user: null
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), MyGdsResolver]
    });
  });

  beforeEach(fakeAsync(() => {
    mockStore = TestBed.inject(MockStore);
    resolver = TestBed.inject(MyGdsResolver);
  }));

  it('should be created', () => {
    expect(resolver).toBeDefined();
  });

  it('should resolve gds user when current user is a gds user', fakeAsync(() => {
    let resultUser: GdsUser;

    const gdsUser = {
      userType: UserType.GDS
    } as GdsUser;

    mockStore.setState({
      ...initialState,
      auth: { user: gdsUser }
    });

    resolver.resolve().subscribe(user => (resultUser = user));

    expect(resultUser).toEqual(gdsUser);
  }));

  it('should not resolve gds user when user information is not available', fakeAsync(() => {
    expect(resolver.resolve()).toBeObservable(cold('-'));
  }));
});
