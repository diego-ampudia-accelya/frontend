import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { GdsUser, UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class MyGdsResolver implements Resolve<GdsUser> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<GdsUser> {
    return this.store.pipe(
      select(getUser),
      filter(user => !!user),
      first(),
      map((user: GdsUser) => {
        if (user.userType !== UserType.GDS) {
          throw new Error('Access Denied.');
        }

        return user;
      })
    );
  }
}
