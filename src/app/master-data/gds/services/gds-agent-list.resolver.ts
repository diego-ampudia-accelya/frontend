import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { first, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { GdsAgentListFilter, ViewAs } from '~app/master-data/gds/gds-agent-list/models';
import { GdsAgentListService } from '~app/master-data/gds/gds-agent-list/services/gds-agent-list.service';
import { GdsAgentListListActions } from '~app/master-data/gds/gds-agent-list/store/actions';
import { AppState } from '~app/reducers';
import { UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class GdsAgentListResolver implements Resolve<GdsAgentListFilter> {
  private readonly idKey = 'gds-id';
  constructor(
    private store: Store<AppState>,
    private gdsAgentListService: GdsAgentListService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot): GdsAgentListFilter {
    const requiredFilter: GdsAgentListFilter = { gdsAssignment: true };
    let viewAs: ViewAs;
    this.store
      .pipe(
        select(getUser),
        first(),
        tap(user => {
          if (user.userType === UserType.IATA) {
            this.setIataGdsId(route);
            viewAs = ViewAs.Iata;
          } else {
            viewAs = ViewAs.Gds;
          }
          this.gdsAgentListService.setViewAs(viewAs);
        })
      )
      .subscribe(() => {
        this.store.dispatch(GdsAgentListListActions.changeRequiredFilter({ requiredFilter }));
      });

    return requiredFilter;
  }

  private setIataGdsId(route: ActivatedRouteSnapshot) {
    const id: string = route.parent.paramMap.get('id');

    if (id == null) {
      this.handleNavigationOnError();
      throw new Error(`Missing required path parameter "${this.idKey}" in route ${this.router.url}`);
    } else {
      this.gdsAgentListService.setGdsId(id);
    }
  }

  private handleNavigationOnError() {
    const lastSuccessfulNavigation = this.router.getCurrentNavigation().previousNavigation;
    if (lastSuccessfulNavigation != null) {
      this.router.navigateByUrl(lastSuccessfulNavigation.finalUrl.toString());
    } else {
      this.router.navigate(['./']);
    }
  }
}
