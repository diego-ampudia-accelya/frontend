import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Data, Params } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { GdsByAgentResolver } from './gds-by-agent.resolver';
import { GdsByAgentFilter, ViewAs } from '~app/master-data/gds-by-agent/models';
import { GdsByAgentService } from '~app/master-data/gds-by-agent/services/gds-by-agent.service';
import { GdsByAgentListActions } from '~app/master-data/gds-by-agent/store/actions';
import { AppState } from '~app/reducers';
import { IataUser, UserType } from '~app/shared/models/user.model';

describe('GDS By Agent Resolver', () => {
  const user: IataUser = {
    email: 'iata-user-nfe@accelya.com',
    firstName: 'user',
    lastName: 'gdsUser',
    userType: UserType.IATA,
    id: 10005,
    active: true
  };

  const initialState = {
    auth: {
      user
    }
  };

  let gdsByAgentResolver: GdsByAgentResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GdsByAgentResolver, provideMockStore({ initialState }), mockProvider(GdsByAgentService)]
    });

    gdsByAgentResolver = TestBed.inject(GdsByAgentResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(gdsByAgentResolver).toBeTruthy();
  });

  it('should dispatch the correct action with proper required filter and resolve it', fakeAsync(() => {
    const route = {
      parent: {
        data: {
          viewAs: ViewAs.Iata,
          requiredPermissions: 'rGdsAgn',
          agent: {
            id: 200
          }
        } as Data,
        params: {
          id: 1
        } as Params
      }
    } as ActivatedRouteSnapshot;

    gdsByAgentResolver.resolve(route);
    tick();

    spyOn(mockStore, 'dispatch');

    const expectedFilter: GdsByAgentFilter = { gdsAssignment: true };

    const requiredFilter: GdsByAgentFilter = gdsByAgentResolver.resolve(route);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      GdsByAgentListActions.changeRequiredFilter({ requiredFilter: expectedFilter })
    );
    expect(requiredFilter).toEqual(expectedFilter);
  }));
});
