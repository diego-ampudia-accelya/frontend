import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Data, Router } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { GdsAgentListFilter, ViewAs } from '../gds-agent-list/models';
import { GdsAgentListService } from '../gds-agent-list/services/gds-agent-list.service';
import { GdsAgentListListActions } from '../gds-agent-list/store/actions';

import { GdsAgentListResolver } from './gds-agent-list.resolver';
import { GdsUser, UserType } from '~app/shared/models/user.model';
import { AppState } from '~app/reducers';

describe('Gds Agent List Resolver', () => {
  const user: GdsUser = {
    email: 'gds-user-nfe@accelya.com',
    firstName: 'user',
    lastName: 'gdsUser',
    userType: UserType.GDS,
    id: 10005,
    gds: {
      id: 76767676,
      name: 'gdsUser',
      gdsCode: 'gdsCode'
    },
    active: true
  };

  const initialState = {
    auth: {
      user
    }
  };

  let gdsAgentListResolver: GdsAgentListResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GdsAgentListResolver,
        provideMockStore({ initialState }),
        mockProvider(GdsAgentListService),
        mockProvider(Router)
      ]
    });

    gdsAgentListResolver = TestBed.inject(GdsAgentListResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(gdsAgentListResolver).toBeTruthy();
  });

  it('should dispatch the correct action with proper required filter and resolve it', () => {
    const route = {
      parent: {
        data: {
          viewAs: ViewAs.Iata,
          requiredPermissions: 'rAgnGds'
        } as Data
      }
    } as ActivatedRouteSnapshot;

    spyOn(mockStore, 'dispatch');

    const expectedFilter: GdsAgentListFilter = {
      gdsAssignment: true
    };

    const requiredFilter: GdsAgentListFilter = gdsAgentListResolver.resolve(route);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      GdsAgentListListActions.changeRequiredFilter({ requiredFilter: expectedFilter })
    );
    expect(requiredFilter).toEqual(expectedFilter);
  });
});
