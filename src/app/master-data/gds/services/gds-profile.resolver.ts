import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';

import { GdsAgentList } from '../gds-agent-list/models';

@Injectable({
  providedIn: 'root'
})
export class GdsProfileResolver implements Resolve<GdsAgentList> {
  constructor(private router: Router) {}

  public resolve(): GdsAgentList {
    return this.router.getCurrentNavigation().extras.state?.gds;
  }
}
