import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { first, tap } from 'rxjs/operators';

import { getUserType } from '~app/auth/selectors/auth.selectors';
import { GdsByAgentFilter } from '~app/master-data/gds-by-agent/models';
import { GdsByAgentService } from '~app/master-data/gds-by-agent/services/gds-by-agent.service';
import { GdsByAgentListActions } from '~app/master-data/gds-by-agent/store/actions';
import { AppState } from '~app/reducers';
import { UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class GdsByAgentResolver implements Resolve<GdsByAgentFilter> {
  constructor(private store: Store<AppState>, private gdsByAgentService: GdsByAgentService) {}

  resolve(route: ActivatedRouteSnapshot): GdsByAgentFilter {
    let requiredFilter: GdsByAgentFilter;

    this.store
      .pipe(
        select(getUserType),
        first(),
        tap(() => {
          const agentId = route.parent.data.agent.id;
          this.gdsByAgentService.setGdsId(agentId);
        })
      )
      .subscribe((userType: UserType) => {
        requiredFilter = userType === UserType.IATA ? { gdsAssignment: true } : {};
        this.store.dispatch(GdsByAgentListActions.changeRequiredFilter({ requiredFilter }));
      });

    return requiredFilter;
  }
}
