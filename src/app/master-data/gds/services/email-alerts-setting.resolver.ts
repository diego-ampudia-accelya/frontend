import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { GdsUser } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class EmailAlertsSettingResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getUser),
      first(),
      tap((userGds: GdsUser) => {
        this.store.dispatch(
          SettingConfigurationActions.load({
            scope: {
              scopeType: 'users',
              scopeId: `${userGds.id}`,
              name: userGds.gds.name,
              service: 'user-profile-management'
            }
          })
        );
      }),
      mapTo(null)
    );
  }
}
