import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { NotificationSettingsResolver } from '../airline/services/notification-settings.resolver';
import { ConfigurationComponent, SettingsViewComponent, UnsavedChangesGuard } from '../configuration';

import { EmailAlertsSettingsComponent } from '../configuration/email-alert-settings/email-alert-settings.component';
import { GlobalTaFopFilesSettingsComponent } from './configuration/global-ta-fop-files-settings/global-ta-fop-files-settings.component';
import { GdsAgentListComponent } from './gds-agent-list/gds-agent-list/gds-agent-list.component';
import { ViewAs } from './gds-agent-list/models';
import { GdsListComponent } from './gds-list/gds-list.component';
import { GdsProfileComponent } from './gds-profile/gds-profile.component';
import { EmailAlertsSettingResolver } from './services/email-alerts-setting.resolver';
import { GdsAgentListResolver } from './services/gds-agent-list.resolver';
import { MyGdsResolver } from './services/my-gds.resolver';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';

const emailAlertRoute: Route = {
  path: 'email-alerts-settings',
  resolve: {
    loadSettings: EmailAlertsSettingResolver
  },
  component: EmailAlertsSettingsComponent,

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.emailAlerts.groupName',
    title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
    viewAs: ViewAs.Gds,
    configuration: {
      title: 'menu.masterData.configuration.emailAlerts.sectionTitle'
    },
    requiredPermissions: Permissions.readEmailAlerts
  }
};

const notificationsRoute: Route = {
  path: 'notifications',
  component: SettingsViewComponent,
  resolve: {
    loadSettings: NotificationSettingsResolver
  },

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.notificationsGroupName',
    title: 'menu.masterData.configuration.notificationsSettings',
    configuration: {
      title: 'menu.masterData.configuration.notificationsSettingsTitle',
      permissions: {
        read: Permissions.readNotificationSettings,
        update: Permissions.updateNotificationSettings
      }
    },
    requiredPermissions: [Permissions.readNotificationSettings]
  },
  runGuardsAndResolvers: 'always'
};

const globalTaFopFilesRoute: Route = {
  path: 'global-ta-fop-files',
  component: GlobalTaFopFilesSettingsComponent,
  data: {
    group: 'menu.masterData.configuration.globalTaFopFiles.groupName',
    title: 'menu.masterData.configuration.globalTaFopFiles.sectionTitle',
    viewAs: ViewAs.Gds,
    configuration: {
      title: 'menu.masterData.configuration.globalTaFopFiles.sectionTitle'
    },
    requiredPermissions: Permissions.readGlobalTaFopFiles
  }
};

const configurationRoute: Route = {
  path: 'configuration',
  component: ConfigurationComponent,
  data: {
    title: 'menu.masterData.configuration.title',
    requiredPermissions: [Permissions.readGdsConfiguration]
  },
  children: [emailAlertRoute, notificationsRoute, globalTaFopFilesRoute]
};

const gdsAgentListRoute: Route = {
  path: 'gds-agent-list',
  component: GdsAgentListComponent,
  resolve: {
    requiredFilter: GdsAgentListResolver
  },
  data: {
    title: 'menu.masterData.gdsAgentList.title',
    viewAs: ViewAs.Gds,
    requiredPermissions: Permissions.readAgentGds
  }
};

const gdsProfileChildren: Route[] = [configurationRoute, gdsAgentListRoute];

const routes: Routes = [
  {
    path: '',
    component: GdsListComponent,
    data: {
      tab: ROUTES.GDS
    }
  },
  {
    path: 'my',
    component: GdsProfileComponent,
    resolve: {
      agent: MyGdsResolver
    },
    data: {
      tab: ROUTES.MY_GDS
    },
    children: gdsProfileChildren
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GdsRoutingModule {}
