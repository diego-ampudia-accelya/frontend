import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  canApplyChanges,
  MenuBuilder,
  RoutedMenuItem,
  SettingConfigurationActions
} from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { or } from '~app/shared/utils';

@Component({
  selector: 'bspl-dpc-profile',
  templateUrl: './dpc-profile.component.html',
  styleUrls: ['./dpc-profile.component.scss']
})
export class DpcProfileComponent implements OnInit {
  public tabs: RoutedMenuItem[];
  public isApplyChangesVisible: boolean;
  public isApplyChangesEnabled$: Observable<boolean>;

  public bspName$ = this.store.pipe(
    select(fromAuth.getUserBsps),
    map(bsps => bsps[0]?.name)
  );

  private currentSavableTab = null;
  private destroyed$ = new Subject();

  private savableTabs = [
    {
      url: './configuration/general-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateGeneralDpcConfiguration
    },
    {
      url: './configuration/email-alerts-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateEmailAlerts
    },
    {
      url: './configuration/master-data-reception-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateMasterDataReceptionSettings
    },
    {
      url: './configuration/notifications',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateNotificationSettings
    },
    {
      url: './bsp-configuration/adm-acm-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './bsp-configuration/agents-maintenance',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './bsp-configuration/settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './bsp-configuration/remittance',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './bsp-configuration/ra-rn-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    }
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private menuBuilder: MenuBuilder,
    private permissionsService: PermissionsService
  ) {
    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab === savableTab)
        )
      )
    ).pipe(map(values => or(...values)));
  }

  public ngOnInit(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        tap(activeSavableTab => {
          this.isApplyChangesVisible =
            activeSavableTab != null
              ? this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission)
              : false;

          this.currentSavableTab = activeSavableTab;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public applyChanges(): void {
    if (this.currentSavableTab) {
      this.store.dispatch(this.currentSavableTab.applyChangesAction);
    }
  }
}
