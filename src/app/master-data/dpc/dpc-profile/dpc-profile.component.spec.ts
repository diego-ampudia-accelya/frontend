import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import * as fromSettings from '~app/master-data/configuration';
import { MenuBuilder, SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { ROUTES } from '~app/shared/constants/routes';
import { DpcUser, UserType } from '~app/shared/models/user.model';
import { DpcProfileComponent } from './dpc-profile.component';

describe('AgentGroupProfileComponent', () => {
  let fixture: ComponentFixture<DpcProfileComponent>;
  let component: DpcProfileComponent;
  let permissionsServiceSpy: SpyObject<PermissionsService>;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  let mockStore: MockStore<AppState>;

  const menuBuilderMock = jasmine.createSpyObj<MenuBuilder>('MenuBuilder', {
    buildMenuItemsFrom: [
      {
        route: '/master-data/dpc/my/configuration/general-settings',
        title: 'menu.masterData.configuration.dpcGeneralSettings.sectionTitle',
        isAccessible: true
      }
    ]
  });

  const activatedRouteStub = {
    snapshot: {
      data: {
        tab: ROUTES.MY_DPC
      }
    }
  };

  const user: DpcUser = {
    email: 'dpc-user-nfe@accelya.com',
    firstName: 'Thomas',
    lastName: 'Anderson',
    userType: UserType.DPC,
    id: 10005,
    active: true,
    permissions: ['uDpcCfg']
  };

  const initialState = {
    auth: {
      user
    },
    core: {
      menu: {
        tabs: {}
      }
    }
  };

  let routerMock;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/configuration/general-settings', './configuration/general-settings')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './configuration/general-settings',
      createUrlTree: () => {},
      isActive: (res = false) => res
    };

    permissionsServiceSpy = createSpyObject(PermissionsService, { permissions$: of([]) });
    permissionsServiceSpy.hasPermission.and.returnValue(false);

    TestBed.configureTestingModule({
      declarations: [DpcProfileComponent],
      imports: [L10nTranslationModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        provideMockStore({
          initialState,
          selectors: [{ selector: fromSettings.canApplyChanges, value: false }]
        }),
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy },
        { provide: MenuBuilder, useValue: menuBuilderMock },
        {
          provide: Router,
          useValue: routerMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DpcProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch EAlert Open Apply Changes action when applyChanges method is called', () => {
    spyOn(routerMock, 'isActive').and.returnValue(true);
    (component as any).currentSavableTab = {
      url: 'GeneralUrl',
      applyChangesAction: SettingConfigurationActions.openApplyChanges()
    };

    component.applyChanges();

    expect(mockStore.dispatch).toHaveBeenCalledWith(SettingConfigurationActions.openApplyChanges());
  });

  it('button ApplyChanges must not be visible if PermissionsService return false', () => {
    spyOn(routerMock, 'isActive').and.returnValue(true);
    (component as any).currentSavableTab = {
      url: 'GeneralUrl',
      applyChangesPermission: 'AnyPermissions'
    };

    expect(component.isApplyChangesVisible).toBe(false);
  });
});
