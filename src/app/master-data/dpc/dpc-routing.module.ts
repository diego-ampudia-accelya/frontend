import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { NotificationSettingsResolver } from '../airline/services/notification-settings.resolver';
import { ViewAs } from '../dpc/models/view-as.model';

import { EmailAlertsSettingsComponent } from '../configuration/email-alert-settings/email-alert-settings.component';
import { DpcProfileComponent } from './dpc-profile/dpc-profile.component';
import { DpcAdmAcmSettingsResolver } from './resolvers/dpc-adm-acm-settings.resolver';
import { DpcAgentMaintenanceResolver } from './resolvers/dpc-agent-maintenance-settings.resolver';
import { DpcConfigResolver } from './resolvers/dpc-config.resolver';
import { DpcMasterDataReceptionSettingsResolver } from './resolvers/dpc-master-data-reception-settings.resolver';
import { DpcRefundSettingResolver } from './resolvers/dpc-refund-settings.resolver';
import { DpcRemittanceSettingsResolver } from './resolvers/dpc-remittance-settings.resolver';
import { DpcSettingsResolver } from './resolvers/dpc-settings.resolver';
import { EmailAlertsSettingResolver } from './resolvers/email-alerts-setting.resolver';
import { Permissions, ROUTES } from '~app/shared/constants';
import { ConfigurationComponent, SettingsViewComponent, UnsavedChangesGuard } from '~app/master-data/configuration';

// General Configuration
const generalSettingsRoute = {
  path: 'general-settings',
  resolve: {
    loadSettings: DpcConfigResolver
  },
  component: SettingsViewComponent,

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.dpcGeneralSettings.groupName',
    title: 'menu.masterData.configuration.dpcGeneralSettings.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.dpcGeneralSettings.sectionTitle'
    },
    requiredPermissions: Permissions.readIataSettings,
    disableAllPermission: Permissions.updateGeneralDpcConfiguration
  }
};

// BSP Configuration
const remittanceRoute = {
  path: 'remittance',
  resolve: {
    loadSettings: DpcRemittanceSettingsResolver
  },

  component: SettingsViewComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.bsp.groupName',
    title: 'menu.masterData.configuration.bsp.remittance.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.bsp.remittance.sectionTitle'
    },
    requiredPermissions: Permissions.readBasicDPCConfiguration,
    disableAllPermission: Permissions.updateIataSettings
  }
};

const settingsRoute = {
  path: 'settings',
  resolve: {
    loadSettings: DpcSettingsResolver
  },
  component: SettingsViewComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.bsp.groupName',
    title: 'menu.masterData.configuration.bsp.settings.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.bsp.settings.sectionTitle'
    },
    requiredPermissions: Permissions.readBasicDPCConfiguration,
    disableAllPermission: Permissions.updateIataSettings
  }
};

// ADM/ACM Settings
const admAcmSettingsRoute = {
  path: 'adm-acm-settings',
  resolve: {
    loadSettings: DpcAdmAcmSettingsResolver
  },
  component: SettingsViewComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.admAcmSettings.groupName',
    title: 'menu.masterData.configuration.admAcmSettings.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.admAcmSettings.sectionTitle'
    },
    requiredPermissions: Permissions.readBasicDPCConfiguration,
    disableAllPermission: Permissions.updateIataSettings
  }
};

// RA/RN Settings
const raRnSettingsRoute = {
  path: 'ra-rn-settings',
  resolve: {
    loadSettings: DpcRefundSettingResolver
  },
  component: SettingsViewComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.raRnSettings.groupName',
    title: 'menu.masterData.configuration.raRnSettings.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.raRnSettings.sectionTitle'
    },
    requiredPermissions: Permissions.readBasicDPCConfiguration,
    disableAllPermission: Permissions.updateIataSettings
  }
};

// Agents Maintenance
const agentsMaintenanceRoute = {
  path: 'agents-maintenance',
  resolve: {
    loadSettings: DpcAgentMaintenanceResolver
  },
  component: SettingsViewComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.agentsMaintenance.groupName',
    title: 'menu.masterData.configuration.agentsMaintenance.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.agentsMaintenance.sectionTitle'
    },
    requiredPermissions: Permissions.readBasicDPCConfiguration,
    disableAllPermission: Permissions.updateIataSettings
  }
};

const masterDataReceptionSettingsRoute = {
  path: 'master-data-reception-settings',
  resolve: {
    loadSettings: DpcMasterDataReceptionSettingsResolver
  },
  component: SettingsViewComponent,

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.masterData.groupName',
    title: 'menu.masterData.configuration.masterData.reception.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.masterData.reception.sectionTitle'
    },
    requiredPermissions: Permissions.readMasterDataReceptionSettings,
    disableAllPermission: Permissions.updateMasterDataReceptionSettings
  }
};

const emailAlertRoute: Route = {
  path: 'email-alerts-settings',
  resolve: {
    loadSettings: EmailAlertsSettingResolver
  },
  component: EmailAlertsSettingsComponent,

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.emailAlerts.groupName',
    title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
    viewAs: ViewAs.Dpc,
    configuration: {
      title: 'menu.masterData.configuration.emailAlerts.sectionTitle'
    },
    requiredPermissions: Permissions.readEmailAlerts,
    disableAllPermission: Permissions.updateEmailAlerts
  }
};

const notificationsRoute: Route = {
  path: 'notifications',
  component: SettingsViewComponent,
  resolve: {
    loadSettings: NotificationSettingsResolver
  },

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.notificationsGroupName',
    title: 'menu.masterData.configuration.notificationsSettings',
    configuration: {
      title: 'menu.masterData.configuration.notificationsSettingsTitle',
      permissions: {
        read: Permissions.readNotificationSettings,
        update: Permissions.updateNotificationSettings
      }
    },
    requiredPermissions: [Permissions.readNotificationSettings],
    disableAllPermission: Permissions.updateNotificationSettings
  },
  runGuardsAndResolvers: 'always'
};

const configurationRoute: Route = {
  path: 'configuration',
  component: ConfigurationComponent,
  data: {
    title: 'menu.masterData.configuration.title',
    requiredPermissions: [Permissions.readGeneralDpcConfiguration]
  },
  children: [generalSettingsRoute, emailAlertRoute, masterDataReceptionSettingsRoute, notificationsRoute]
};

const bspConfigurationRoute: Route = {
  path: 'bsp-configuration',
  component: ConfigurationComponent,
  data: {
    title: 'menu.masterData.configuration.bspTitle',
    requiredPermissions: [Permissions.readGeneralDpcConfiguration]
  },
  children: [admAcmSettingsRoute, agentsMaintenanceRoute, settingsRoute, remittanceRoute, raRnSettingsRoute]
};

const routes: Routes = [
  {
    path: 'my',
    component: DpcProfileComponent,
    data: {
      tab: ROUTES.MY_DPC
    },
    children: [configurationRoute, bspConfigurationRoute]
  },
  {
    path: '',
    redirectTo: 'my',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DpcRoutingModule {}
