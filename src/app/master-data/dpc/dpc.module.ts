import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DpcProfileComponent } from './dpc-profile/dpc-profile.component';
import { DpcRoutingModule } from './dpc-routing.module';
import { ConfigurationModule } from '~app/master-data/configuration';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [DpcProfileComponent],
  imports: [CommonModule, DpcRoutingModule, SharedModule, ConfigurationModule]
})
export class DpcModule {}
