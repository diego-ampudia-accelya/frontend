import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { DpcConfigResolver } from './dpc-config.resolver';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { DpcUser, UserType } from '~app/shared/models/user.model';

describe('Dpc Config Resolver', () => {
  const user: DpcUser = {
    email: 'gds-user-nfe@accelya.com',
    firstName: 'Stanley',
    lastName: 'Kubrick',
    userType: UserType.DPC,
    id: 10005,
    active: true,
    permissions: ['rBasicCfg'],
    bsps: [
      {
        effectiveFrom: '2000-01-01',
        id: 6983,
        isoCountryCode: 'ES',
        name: 'SPAIN'
      }
    ]
  };

  const initialState = {
    auth: {
      user
    }
  };

  let resolver: DpcConfigResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })]
    });

    resolver = TestBed.inject(DpcConfigResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    resolver.resolve().subscribe();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'bsps',
          scopeId: '6983',
          name: 'Kubrick',
          service: 'bsp-management',
          scopeConfigName: 'configuration-dpc'
        }
      })
    );
  }));
});
