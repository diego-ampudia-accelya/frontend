import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { DpcMasterDataReceptionSettingsResolver } from './dpc-master-data-reception-settings.resolver';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { DpcUser, UserType } from '~app/shared/models/user.model';

describe('Dpc Master Data Reception Settings Resolver', () => {
  const user: DpcUser = {
    email: 'gds-user-nfe@accelya.com',
    firstName: 'Stanley',
    lastName: 'Kubrick',
    userType: UserType.DPC,
    id: 10005,
    active: true,
    permissions: ['rBasicCfg'],
    bsps: [
      {
        effectiveFrom: '2000-01-01',
        id: 6983,
        isoCountryCode: 'ES',
        name: 'SPAIN'
      }
    ]
  };

  const initialState = {
    auth: {
      user
    }
  };

  let dpcMasterDataReceptionSettingsResolver: DpcMasterDataReceptionSettingsResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), DpcMasterDataReceptionSettingsResolver]
    });

    dpcMasterDataReceptionSettingsResolver = TestBed.inject(DpcMasterDataReceptionSettingsResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(dpcMasterDataReceptionSettingsResolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    dpcMasterDataReceptionSettingsResolver.resolve().subscribe();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'bsps',
          scopeId: '6983',
          name: 'Kubrick',
          service: 'bsp-management',
          scopeConfigName: 'master-tables-dpc'
        }
      })
    );
  }));
});
