import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';

import { getBsp } from '../reducers';
import { MenuTabActions } from '~app/core/actions';
import {
  getSettingsState,
  MenuBuilder,
  RoutedMenuItem,
  SettingConfigurationActions
} from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';

@Component({
  selector: 'bspl-bsp-profile',
  templateUrl: './bsp-profile.component.html',
  styleUrls: ['./bsp-profile.component.scss']
})
export class BspProfileComponent implements OnInit {
  public bsp$ = this.store.pipe(select(getBsp));
  public tabs: RoutedMenuItem[];
  public isApplyChangesEnabled$ = this.store.pipe(
    select(getSettingsState),
    map(state => state.isChanged)
  );
  public btnDesign = ButtonDesign;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private menuBuilder: MenuBuilder
  ) {}

  ngOnInit() {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }

  applyChanges() {
    this.store.dispatch(SettingConfigurationActions.openApplyChanges());
  }

  closeMainTab() {
    this.store.dispatch(new MenuTabActions.Close());
  }
}
