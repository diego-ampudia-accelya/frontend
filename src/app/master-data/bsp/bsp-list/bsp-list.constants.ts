import { MasterDataType, MasterTableConfig } from '~app/master-shared/models/master.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

export const BSP_LIST: MasterTableConfig = {
  get: {
    get [MasterDataType.BSP]() {
      return {
        COLUMNS: [
          {
            prop: 'isoCountryCode',
            name: 'MENU.MASTER_DATA.country.LBL',
            resizeable: false,
            draggable: false
          },
          {
            prop: 'name',
            name: 'LIST.MASTER_DATA.bsp.columns.name',
            cellTemplate: 'commonLinkCellTmpl',
            resizeable: false,
            draggable: false,
            width: 400
          },
          {
            prop: 'effectiveFrom',
            name: 'LIST.MASTER_DATA.bsp.columns.effectiveFrom',
            resizeable: false,
            draggable: false
          },
          {
            prop: 'effectiveTo',
            name: 'LIST.MASTER_DATA.bsp.columns.effectiveTo',
            resizeable: false,
            draggable: false
          },
          {
            prop: 'active',
            name: 'LIST.MASTER_DATA.bsp.columns.status',
            resizeable: false,
            draggable: false,
            cellTemplate: 'statusTmpl'
          }
        ],
        ACTIONS: [],
        PRIMARY_ACTIONS: [{ action: GridTableActionType.Edit }]
      };
    }
  }
};
