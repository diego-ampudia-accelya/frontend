import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BspService } from '~app/master-data/services/bsp.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ViewListComponent } from '~app/shared/components';
import { Bsp } from '~app/shared/models/bsp.model';
import { DataSource } from '~app/shared/services/data-source.service';

@Component({
  selector: 'bspl-bsp-list',
  templateUrl: './bsp-list.component.html'
})
export class BspListComponent {
  @ViewChild('bspList', { static: true }) bspList: ViewListComponent;

  public dataSource = new DataSource<Bsp>(this.bspService);
  public currentItem;
  public masterDataType = MasterDataType.BSP;

  constructor(public bspService: BspService, public activatedRoute: ActivatedRoute, public router: Router) {}

  public onCurrencyClick(id) {
    this.router.navigate([id], { relativeTo: this.bspList.route });
  }
}
