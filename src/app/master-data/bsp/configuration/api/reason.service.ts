import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BspReason } from '../models/bsp-reason.model';
import { getBasePath } from './base-path';
import { DescriptionService, EditableDescription, ReasonType } from '~app/master-data/configuration';
import { determineMethodToSave } from '~app/shared/utils/api-utils';
import { buildPath } from '~app/shared/utils/path-builder';

export class ReasonService implements DescriptionService {
  constructor(private http: HttpClient, public type: ReasonType) {}

  get(parentId: number): Observable<EditableDescription[]> {
    return this.http
      .get<BspReason[]>(this.getUrl(parentId), {
        params: { reasonType: this.type }
      })
      .pipe(map(reasons => reasons.map(this.mapToEditableDescription)));
  }

  delete(model: EditableDescription): Observable<void> {
    return this.http.delete<void>(this.getUrl(model.parentId, model.id), {
      params: { reasonType: this.type }
    });
  }

  save(model: EditableDescription): Observable<EditableDescription> {
    const url = this.getUrl(model.parentId, model.id);
    const method = determineMethodToSave(model);
    const reason = this.mapToReason(model);

    return this.http
      .request<BspReason>(method, url, {
        body: reason,
        params: { reasonType: this.type }
      })
      .pipe(map(this.mapToEditableDescription));
  }

  private getUrl(bspId: number, modelId?: number): string {
    return buildPath(`${getBasePath(bspId)}/acdm-reason-configuration`, modelId);
  }

  private mapToReason(model: EditableDescription): BspReason {
    const { parentId, label, ...otherProps } = model;

    return {
      ...otherProps,
      title: label,
      type: this.type,
      bspId: parentId
    };
  }

  private mapToEditableDescription(reason: BspReason): EditableDescription {
    const { bspId, title, ...otherProps } = reason;

    return {
      ...otherProps,
      label: title,
      parentId: bspId
    };
  }
}
