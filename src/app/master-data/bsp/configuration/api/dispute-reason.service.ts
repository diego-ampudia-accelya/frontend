import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ReasonService } from './reason.service';
import { ReasonType } from '~app/master-data/configuration';

@Injectable({
  providedIn: 'root'
})
export class DisputeReasonService extends ReasonService {
  constructor(http: HttpClient) {
    super(http, ReasonType.Dispute);
  }
}
