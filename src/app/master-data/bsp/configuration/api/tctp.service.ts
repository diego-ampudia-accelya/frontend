import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Tctp } from '../models/tctp.model';
import { getBasePath } from './base-path';
import { DescriptionService, EditableDescription } from '~app/master-data/configuration';
import { determineMethodToSave } from '~app/shared/utils/api-utils';
import { buildPath } from '~app/shared/utils/path-builder';

@Injectable({
  providedIn: 'root'
})
export class TctpService implements DescriptionService {
  constructor(private http: HttpClient) {}

  get(parentId: number): Observable<EditableDescription[]> {
    return this.http
      .get<Tctp[]>(this.getUrl(parentId))
      .pipe(map(results => results.map(this.mapToEditableDescription)));
  }

  delete(model: EditableDescription): Observable<void> {
    return this.http.delete<void>(this.getUrl(model.parentId, model.id));
  }

  save(model: EditableDescription): Observable<EditableDescription> {
    const url = this.getUrl(model.parentId, model.id);
    const method = determineMethodToSave(model);
    const tctp = this.mapToTctp(model);

    return this.http
      .request<Tctp>(method, url, {
        body: tctp
      })
      .pipe(map(this.mapToEditableDescription));
  }

  private getUrl(bspId: number, entityId?: number): string {
    return buildPath(`${getBasePath(bspId)}/acdm-tctp-configuration`, entityId);
  }

  private mapToTctp(model: EditableDescription): Tctp {
    const { parentId, label, ...otherProps } = model;

    return {
      ...otherProps,
      type: label,
      bspId: parentId
    };
  }

  private mapToEditableDescription(tctp: Tctp): EditableDescription {
    const { bspId, type, ...otherProps } = tctp;

    return {
      ...otherProps,
      label: type,
      parentId: bspId
    };
  }
}
