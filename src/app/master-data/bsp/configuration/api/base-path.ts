import { appConfiguration } from '~app/shared/services/app-configuration.service';

export const getBasePath = (bspId: number) => `${appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}`;
