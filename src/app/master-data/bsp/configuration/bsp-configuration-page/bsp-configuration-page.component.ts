import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { ConfigurationActions } from '../../actions';
import { getMenuItems, getSelectedMenuItem } from '../../reducers';
import { RoutedMenuItem } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';

@Component({
  selector: 'bspl-bsp-configuration-page',
  templateUrl: './bsp-configuration-page.component.html',
  styleUrls: ['./bsp-configuration-page.component.scss']
})
export class BspConfigurationPageComponent {
  public menuItems$ = this.store.pipe(select(getMenuItems));
  public activeItem$ = this.store.pipe(select(getSelectedMenuItem));

  constructor(private store: Store<AppState>) {}

  public selectMenuItem(event: { item: RoutedMenuItem; preventDefault?: () => any }): void {
    if (event.preventDefault) {
      event.preventDefault();
    }

    this.store.dispatch(new ConfigurationActions.SelectMenuItem(event.item));
  }
}
