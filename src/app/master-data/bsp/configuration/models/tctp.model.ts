export interface TctpIdentifier {
  id: number;
  bspId: number;
}

export interface Tctp extends TctpIdentifier {
  bspAcdmConfigId?: number;
  sequenceNumber: number;
  description: string;
  type: string;
  version?: number;
}
