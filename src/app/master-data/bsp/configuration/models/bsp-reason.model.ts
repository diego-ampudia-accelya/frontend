import { Reason } from '~app/master-data/configuration';

export interface BspReason extends Reason {
  bspId: number;
  bspAcdmConfigId?: number;
}
