import { TctpService } from './api/tctp.service';
import { defaultPermissions, DescriptionViewConfig } from '~app/master-data/configuration';
import { GLOBALS } from '~app/shared/constants/globals';

export const tctpConfig: DescriptionViewConfig = {
  entityName: 'TCTP',
  title: 'Tax on Commission Types',
  apiService: TctpService,
  maxItems: 5,
  createButtonLabel: 'TCTP',
  description: {
    header: 'Description',
    maxLength: 50,
    isRequired: false,
    restrictToPattern: GLOBALS.PATTERNS.TCTP_DESCRIPTION
  },
  label: {
    header: 'Type',
    maxLength: 6,
    allCaps: true,
    restrictToPattern: GLOBALS.PATTERNS.TCTP_TYPE
  },
  permissions: defaultPermissions
};
