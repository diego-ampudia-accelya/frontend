import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import { ProfileActions } from '../actions';
import { BspService } from '~app/master-data/services/bsp.service';

@Injectable()
export class BspProfileEffects {
  @Effect()
  loadBsp$ = this.actions$.pipe(
    ofType<ProfileActions.LoadBsp>(ProfileActions.Types.LoadBsp),
    switchMap(action => this.bspService.getOne(action.payload.id)),
    map(bsp => new ProfileActions.LoadBspSuccess(bsp))
  );

  constructor(private actions$: Actions, private bspService: BspService) {}
}
