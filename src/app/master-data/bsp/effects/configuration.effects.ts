import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { ConfigurationActions } from '../actions';

@Injectable()
export class ConfigurationEffects {
  @Effect({ dispatch: false })
  selectMenuItem$ = this.actions$.pipe(
    ofType<ConfigurationActions.SelectMenuItem>(ConfigurationActions.Types.SelectMenuItem),
    switchMap(action => this.router.navigate([action.payload.route], { queryParamsHandling: 'preserve' }))
  );

  constructor(private actions$: Actions, private router: Router) {}
}
