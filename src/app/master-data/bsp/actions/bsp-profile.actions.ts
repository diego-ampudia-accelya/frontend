/* eslint-disable @typescript-eslint/no-shadow */
import { Action } from '@ngrx/store';

import { Bsp } from '~app/shared/models/bsp.model';

export enum Types {
  LoadBsp = '[Bsp Profile] Load Bsp',
  LoadBspSuccess = '[Bsp Profile] Load Bsp Success'
}

export class LoadBsp implements Action {
  readonly type = Types.LoadBsp;
  constructor(public payload: { id: number }) {}
}

export class LoadBspSuccess implements Action {
  readonly type = Types.LoadBspSuccess;
  constructor(public payload: Bsp) {}
}

export type Actions = LoadBsp | LoadBspSuccess;
