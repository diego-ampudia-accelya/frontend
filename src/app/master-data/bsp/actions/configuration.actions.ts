/* eslint-disable @typescript-eslint/no-shadow */
import { Action } from '@ngrx/store';

import { RoutedMenuItem } from '~app/master-data/configuration';

export enum Types {
  SelectMenuItem = '[Bsp Configuration] Select Menu Item',
  LoadMenu = '[Bsp Configuration] Load Menu'
}

export class SelectMenuItem implements Action {
  readonly type = Types.SelectMenuItem;
  constructor(public payload: RoutedMenuItem) {}
}

export class LoadMenu implements Action {
  readonly type = Types.LoadMenu;
  constructor(public payload: RoutedMenuItem[]) {}
}

export type Actions = SelectMenuItem | LoadMenu;
