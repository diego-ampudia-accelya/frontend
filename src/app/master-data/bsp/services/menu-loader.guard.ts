import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { ConfigurationActions } from '../actions';
import { MenuBuilder } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';

@Injectable({
  providedIn: 'root'
})
export class MenuLoaderGuard implements CanActivate {
  constructor(private store: Store<AppState>, private menuBuilder: MenuBuilder) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const items = this.menuBuilder.buildMenuItemsFrom(route);
    this.store.dispatch(new ConfigurationActions.LoadMenu(items));

    return of(true);
  }
}
