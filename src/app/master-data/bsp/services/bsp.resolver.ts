import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { getBsp } from '../reducers';
import { AppState } from '~app/reducers';
import { Bsp } from '~app/shared/models/bsp.model';

@Injectable({
  providedIn: 'root'
})
export class BspResolver implements Resolve<Bsp> {
  constructor(private store: Store<AppState>) {}

  public resolve(): Observable<Bsp> {
    return this.store.pipe(select(getBsp), first());
  }
}
