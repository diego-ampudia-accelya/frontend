import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { getBsp, State } from '~app/master-data/bsp/reducers';
import { SettingConfigurationActions } from '~app/master-data/configuration';

@Injectable({
  providedIn: 'root'
})
export class BspSettingsResolver implements Resolve<any> {
  constructor(private store: Store<State>) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getBsp),
      first(),
      tap(bsp => {
        this.store.dispatch(
          SettingConfigurationActions.load({
            scope: {
              scopeType: 'bsps',
              scopeId: `${bsp.id}`,
              name: bsp.name,
              service: 'acdm-management'
            }
          })
        );
      }),
      mapTo(null)
    );
  }
}
