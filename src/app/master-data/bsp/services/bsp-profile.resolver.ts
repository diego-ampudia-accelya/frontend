import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { noop, Observable } from 'rxjs';
import { filter, first, tap } from 'rxjs/operators';

import { ProfileActions } from '../actions';
import { getBsp } from '../reducers';
import { AppState } from '~app/reducers';
import { Bsp } from '~app/shared/models/bsp.model';
import { NotificationService } from '~app/shared/services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class BspProfileResolver implements Resolve<Bsp> {
  private readonly idKey = 'bsp-id';

  constructor(
    private store: Store<AppState>,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Bsp> {
    if (!route.paramMap.has(this.idKey)) {
      throw new Error(`Missing required path parameter "${this.idKey}" in route ${state.url}`);
    }

    const id = Number(route.paramMap.get(this.idKey));

    return this.getFromStoreOrApi(id).pipe(tap(noop, () => this.showErrorMessage(id)));
  }

  private getFromStoreOrApi(id: number): Observable<Bsp> {
    return this.store.pipe(
      select(getBsp),
      tap(bsp => {
        if (!this.isMatchingBsp(bsp, id)) {
          this.store.dispatch(new ProfileActions.LoadBsp({ id }));
        }
      }),
      // Wait for a matching bsp to be available in the store
      filter(bsp => this.isMatchingBsp(bsp, id)),
      first()
    );
  }

  private showErrorMessage(id: number): void {
    const message = this.createErrorMessage(id);
    this.notificationService.showError(message);
  }

  private isMatchingBsp(bsp: Bsp, id: number): boolean {
    return !!bsp && bsp.id === id;
  }

  private createErrorMessage(id: number): string {
    return this.translationService.translate('errors.resolver.notFound', {
      id,
      entityName: this.translationService.translate('MENU.MASTER_DATA.BSP.LBL')
    });
  }
}
