import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { BspAddEditFormComponent } from './bsp-dialog/bsp-add-edit-form-dialog/bsp-add-edit-form.component';
import { BspListComponent } from './bsp-list/bsp-list.component';
import { BspProfileComponent } from './bsp-profile/bsp-profile.component';
import { BspRoutingModule } from './bsp-routing.module';
import { BspConfigurationPageComponent } from './configuration/bsp-configuration-page/bsp-configuration-page.component';
import { BspProfileEffects } from './effects/bsp-profile.effects';
import { ConfigurationEffects } from './effects/configuration.effects';
import { reducers } from './reducers';
import { SharedModule } from '~app/shared/shared.module';
import { ConfigurationModule } from '~app/master-data/configuration';

@NgModule({
  declarations: [BspListComponent, BspProfileComponent, BspAddEditFormComponent, BspConfigurationPageComponent],
  imports: [
    CommonModule,
    BspRoutingModule,
    SharedModule,
    ConfigurationModule,
    StoreModule.forFeature('bsp', reducers),
    EffectsModule.forFeature([BspProfileEffects, ConfigurationEffects])
  ]
})
export class BspModule {}
