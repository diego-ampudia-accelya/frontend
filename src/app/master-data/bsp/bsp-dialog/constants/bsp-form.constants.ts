//TODO Check if this is needed. Used in ErrorMessageService.processFormErrors
export const BSP_FORM = {
  ADD_EDIT_ERROR_COLUMN_MAP: {
    isoCountryCode: 'Country Code',
    name: 'Name',
    effectiveFrom: 'Register Date',
    effectiveTo: 'Expiry Date'
  }
};
