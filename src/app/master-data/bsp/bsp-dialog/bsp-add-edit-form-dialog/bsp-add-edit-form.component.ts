import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { BSP_FORM } from '~app/master-data/bsp/bsp-dialog/constants/bsp-form.constants';
import { BspService } from '~app/master-data/services/bsp.service';
import { ErrorMessageService } from '~app/master-data/services/error-message.service';
import { MasterDialogForm } from '~app/master-shared/master-dialog-form/master-dialog-form.class';
import { MasterDataForRequest } from '~app/master-shared/models/master.model';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { InputComponent } from '~app/shared/components/input/input.component';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-bsp-add-edit-form',
  templateUrl: './bsp-add-edit-form.component.html',
  styleUrls: ['./bsp-add-edit-form.component.scss']
})
export class BspAddEditFormComponent extends MasterDialogForm implements OnInit {
  @ViewChildren(InputComponent) bsplInputs: QueryList<InputComponent>;
  protected formConstants;
  public form: FormGroup;

  public isEditForm = this.dialogInjectedData.actionType === GridTableActionType.Edit;

  public effectiveFromMinDate = new Date();
  public effectiveToMinDate;

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public bspService: BspService,
    public dialogService: DialogService,
    public notificationService: NotificationService,
    public errorMessageService: ErrorMessageService,
    public translationService: L10nTranslationService,
    private formBuilder: FormBuilder
  ) {
    super(
      config,
      bspService,
      reactiveSubject,
      dialogService,
      notificationService,
      errorMessageService,
      translationService
    );
    this.formConstants = BSP_FORM;
  }

  ngOnInit() {
    this.createForm();
    this.initializeDateChangeListener();
    super.setDialogClickCallback();
  }

  createForm() {
    const rowTableContent = this.dialogInjectedData.rowTableContent;
    const isoCountryCode = rowTableContent.isoCountryCode;
    const name = rowTableContent.name;
    const effectiveFrom = rowTableContent.effectiveFrom ? new Date(rowTableContent.effectiveFrom) : null;
    const effectiveTo = rowTableContent.effectiveTo ? new Date(rowTableContent.effectiveTo) : null;

    this.form = this.formBuilder.group({
      isoCountryCode: [isoCountryCode, [Validators.required, Validators.pattern('[A-Z0-9]{2}')]],
      name: [name, [Validators.required, Validators.maxLength(255)]],
      effectiveFrom: [effectiveFrom, Validators.required],
      effectiveTo: [effectiveTo]
    });
    this.updateMinMaxDate();
  }

  updateMinMaxDate() {
    const effectiveTo = this.form.controls.effectiveTo;
    const effectiveFrom = this.form.controls.effectiveFrom;
    this.effectiveFromMinDate = this.getEffectiveFromValue();
    if (this.isEditForm) {
      if (effectiveTo.value) {
        this.effectiveToMinDate = effectiveTo.value < new Date() ? effectiveTo.value : new Date();
      } else {
        this.effectiveToMinDate = new Date();
      }
    }

    if (effectiveFrom.value) {
      this.effectiveFromMinDate = effectiveFrom.value < new Date() ? effectiveFrom.value : new Date();
    } else {
      this.effectiveFromMinDate = new Date();
    }
  }

  isDisabledEffectiveFromBtn(): boolean {
    return this.dialogInjectedData.rowTableContent.active;
  }

  getEffectiveFromValue(): Date {
    return (this.isDisabledEffectiveFromBtn() && this.form.controls.effectiveFrom.value) || new Date();
  }

  getEffectiveToValue(): Date {
    const effectiveFromValue = this.form.controls.effectiveFrom.value
      ? this.form.controls.effectiveFrom.value
      : new Date();
    const effectiveTo = new Date();

    if (this.isSameDay(effectiveFromValue, new Date())) {
      effectiveTo.setDate(effectiveTo.getDate() + 1); // Minimum is initialized at tomorrow
    }

    if (effectiveFromValue > new Date()) {
      effectiveTo.setDate(effectiveFromValue.getDate() + 1); // Minimum is initialized at effectiveFrom value plus one day
    }

    return effectiveTo;
  }

  initializeDateChangeListener() {
    if (this.isEditForm) {
      this.subscriptions.push(
        this.form
          .get('effectiveFrom')
          .valueChanges.subscribe(() => (this.effectiveToMinDate = this.getEffectiveToValue()))
      );
    }
  }

  private isSameDay(d1: Date, d2: Date) {
    return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();
  }

  //  TODO This method should be generic (moved to MasterDialogForm)
  //  TODO It should simply send the form?
  //  TODO action param should have type
  protected generateDataForRequestByAction(action?: any): MasterDataForRequest {
    const dataForRequest = { ...this.form.value };

    if (action.clickedBtn === FooterButton.Update) {
      dataForRequest.id = this.dialogInjectedData.rowTableContent.id;
      dataForRequest.version = this.dialogInjectedData.rowTableContent.version;
    }

    return dataForRequest;
  }
}
