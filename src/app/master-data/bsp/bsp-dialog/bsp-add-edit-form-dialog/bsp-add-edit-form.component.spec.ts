import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createComponentFactory, createSpyObject, Spectator } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';

import { BspAddEditFormComponent } from './bsp-add-edit-form.component';
import { MasterDialogForm } from '~app/master-shared/master-dialog-form/master-dialog-form.class';
import { ButtonDesign, DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { UserType } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services/notification.service';
import { TranslatePipeMock } from '~app/test';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Create,
    actionType: GridTableActionType.Edit,
    rowTableContent: {
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-09-24',
      effectiveTo: '2022-09-23'
    },
    buttons: [
      {
        title: 'test',
        buttonDesign: ButtonDesign.Primary,
        type: FooterButton.Create,
        isDisabled: false
      } as ModalAction
    ]
  }
} as DialogConfig;

describe('BspAddEditFormDialog', () => {
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  let spectator: Spectator<BspAddEditFormComponent>;

  const initialState = { router: null, auth: { user: UserType.IATA } };
  const createComponent = createComponentFactory({
    imports: [HttpClientTestingModule],
    component: BspAddEditFormComponent,
    declarations: [TranslatePipeMock],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [
      FormBuilder,
      DialogConfig,
      ReactiveSubject,
      provideMockStore({ initialState }),
      { provide: DialogConfig, useValue: mockConfig },
      { provide: NotificationService, useValue: notificationServiceSpy },
      { provide: L10nTranslationService, useValue: translationServiceSpy }
    ]
  });

  beforeEach(waitForAsync(() => {
    spectator = createComponent();
    spectator.component.config.data.buttons = [FooterButton.Search];
  }));

  it('should create component', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should initial isEditForm value be true', () => {
    expect(spectator.component.isEditForm).toBe(true);
  });

  it('should onInit call createForm, initializeDateChangeListener and setDialogClickCallback', () => {
    spyOn(spectator.component, 'createForm');
    spyOn(spectator.component, 'initializeDateChangeListener');
    spyOn(MasterDialogForm.prototype, 'setDialogClickCallback');

    spectator.component.ngOnInit();

    expect(spectator.component.createForm).toHaveBeenCalled();
    expect(spectator.component.initializeDateChangeListener).toHaveBeenCalled();
    expect(MasterDialogForm.prototype.setDialogClickCallback).toHaveBeenCalled();
  });

  it('should generate form when createForm method is called', () => {
    const controls = ['isoCountryCode', 'name', 'effectiveTo', 'effectiveFrom'];

    spectator.component.form = null;
    expect(spectator.component.form).toBe(null);

    spectator.component.createForm();
    expect(spectator.component.form).not.toBe(null);

    controls.forEach(controlName => {
      const formControlValue = spectator.component.form.value[controlName];
      const rowTableContent = mockConfig.data.rowTableContent[controlName];

      if (formControlValue instanceof Date) {
        const rowTableDateContent = new Date(rowTableContent);

        expect(formControlValue.getFullYear()).toEqual(rowTableDateContent.getFullYear());
        expect(formControlValue.getMonth()).toEqual(rowTableDateContent.getMonth());
        expect(formControlValue.getDate()).toEqual(rowTableDateContent.getDate());
      } else {
        expect(formControlValue).toEqual(rowTableContent);
      }
    });
  });

  it('should updateMinMaxDate be executed when createForm method is called', () => {
    spyOn(spectator.component, 'updateMinMaxDate');
    spectator.component.createForm();

    expect(spectator.component.updateMinMaxDate).toHaveBeenCalled();
  });
});

const areDatesEqual = (d1: Date, d2: Date) =>
  d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();

describe('BspAddEditFormDialog with missing dates', () => {
  const newMockConfig = cloneDeep(mockConfig);
  newMockConfig.data.rowTableContent.effectiveFrom = null;
  newMockConfig.data.rowTableContent.effectiveTo = null;
  const notificationServiceSpy = createSpyObject(NotificationService);

  const translationServiceSpy = createSpyObject(L10nTranslationService);
  let spectator: Spectator<BspAddEditFormComponent>;

  const initialState = { router: null, auth: { user: UserType.IATA } };
  const createComponent = createComponentFactory({
    imports: [HttpClientTestingModule],
    component: BspAddEditFormComponent,
    declarations: [TranslatePipeMock],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [
      FormBuilder,
      DialogConfig,
      ReactiveSubject,
      provideMockStore({ initialState }),
      { provide: DialogConfig, useValue: newMockConfig },
      { provide: L10nTranslationService, useValue: translationServiceSpy },
      { provide: NotificationService, useValue: notificationServiceSpy }
    ]
  });

  beforeEach(waitForAsync(() => {
    spectator = createComponent();
    spectator.component.config.data.buttons = [FooterButton.Search];
  }));

  it('should getEffectiveToValue method returns next date', () => {
    const effectiveTo = spectator.component.getEffectiveToValue();
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    expect(effectiveTo).not.toBeNull();
    expect(areDatesEqual(effectiveTo, tomorrow)).toBe(true);
  });

  it('should effectiveToMinDate and effectiveFromMinDate be current date', () => {
    spectator.component.updateMinMaxDate();
    const today = new Date();

    expect(areDatesEqual(spectator.component.effectiveToMinDate, today)).toBe(true);
    expect(areDatesEqual(spectator.component.effectiveFromMinDate, today)).toBe(true);
  });
});
