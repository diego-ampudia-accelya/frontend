import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BspListComponent } from './bsp-list/bsp-list.component';
import { BspProfileComponent } from './bsp-profile/bsp-profile.component';
import { DisputeReasonService } from './configuration/api/dispute-reason.service';
import { IssueReasonService } from './configuration/api/issue-reason.service';
import { BspConfigurationPageComponent } from './configuration/bsp-configuration-page/bsp-configuration-page.component';
import { tctpConfig } from './configuration/tctp.config';
import { BspProfileResolver } from './services/bsp-profile.resolver';
import { BspSettingsResolver } from './services/bsp-settings.resolver';
import { BspResolver } from './services/bsp.resolver';
import { MenuLoaderGuard } from './services/menu-loader.guard';
import { PbdSettingsResolver } from './services/pbd-settings.resolver';
import { ROUTES } from '~app/shared/constants/routes';
import {
  baseDisputeReasonsConfig,
  baseIssueReasonConfig,
  DescriptionViewComponent,
  SettingsViewComponent,
  UnsavedChangesGuard
} from '~app/master-data/configuration';
import { CanComponentDeactivateGuard } from '~app/core/services';

const routes: Routes = [
  {
    path: '',
    component: BspListComponent,
    data: {
      tab: ROUTES.BSP
    }
  },
  {
    path: ':bsp-id',
    component: BspProfileComponent,
    data: {
      tab: ROUTES.BSP_PROFILE
    },
    resolve: {
      bsp: BspProfileResolver
    },
    children: [
      {
        path: 'configuration',
        data: {
          title: 'menu.masterData.configuration.title'
        },
        component: BspConfigurationPageComponent,
        canActivate: [MenuLoaderGuard],
        children: [
          {
            path: 'basic-settings',
            component: SettingsViewComponent,
            canDeactivate: [UnsavedChangesGuard],
            resolve: {
              settings: BspSettingsResolver
            },
            data: {
              title: 'menu.masterData.configuration.basicSettings',
              configuration: {
                title: 'LIST.MASTER_DATA.bsp.basicSettings.title'
              }
            }
          },
          {
            path: 'issue-reasons',
            component: DescriptionViewComponent,
            canDeactivate: [CanComponentDeactivateGuard],
            resolve: {
              parentEntity: BspResolver
            },
            data: {
              title: 'menu.masterData.configuration.issueReasons',
              configuration: {
                ...baseIssueReasonConfig,
                apiService: IssueReasonService,
                description: { ...baseIssueReasonConfig.description, canValidate: true }
              }
            }
          },
          {
            path: 'dispute-reasons',
            component: DescriptionViewComponent,
            canDeactivate: [CanComponentDeactivateGuard],
            resolve: {
              parentEntity: BspResolver
            },
            data: {
              title: 'menu.masterData.configuration.disputeReasons',
              configuration: {
                ...baseDisputeReasonsConfig,
                apiService: DisputeReasonService,
                description: { ...baseDisputeReasonsConfig.description, canValidate: true }
              }
            }
          },
          {
            path: 'toc-types',
            component: DescriptionViewComponent,
            canDeactivate: [CanComponentDeactivateGuard],
            resolve: {
              parentEntity: BspResolver
            },
            data: {
              title: 'menu.masterData.configuration.taxOnCommissionTypes',
              configuration: tctpConfig
            }
          },
          {
            path: 'pbd-general-parameters',
            component: SettingsViewComponent,
            canDeactivate: [UnsavedChangesGuard],
            resolve: {
              settings: PbdSettingsResolver
            },
            data: {
              group: 'menu.masterData.configuration.pbdSettings.title',
              title: 'menu.masterData.configuration.pbdSettings.generalParameters.menuTitle',
              configuration: {
                title: 'menu.masterData.configuration.pbdSettings.generalParameters.pageTitle'
              }
            }
          },
          { path: '', redirectTo: 'basic-settings', pathMatch: 'full' }
        ]
      },
      { path: '**', redirectTo: 'configuration' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BspRoutingModule {}
