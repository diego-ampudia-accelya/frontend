import { Actions, Types } from '../actions/bsp-profile.actions';
import { Bsp } from '~app/shared/models/bsp.model';

export interface ProfileState {
  bsp: Bsp;
}

const initialState: ProfileState = {
  bsp: null
};

export function reducer(state: ProfileState = initialState, action: Actions): ProfileState {
  if (action.type === Types.LoadBspSuccess) {
    return {
      ...state,
      bsp: action.payload
    };
  }

  return state;
}
