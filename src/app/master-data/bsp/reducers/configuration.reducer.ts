import { ConfigurationActions } from '../actions';
import { RoutedMenuItem } from '~app/master-data/configuration';

export interface ConfigurationState {
  menuItems: RoutedMenuItem[];
}

const initialState: ConfigurationState = {
  menuItems: []
};

export function reducer(state = initialState, action: ConfigurationActions.Actions): ConfigurationState {
  if (action.type === ConfigurationActions.Types.LoadMenu) {
    return {
      ...state,
      menuItems: action.payload
    };
  }

  return state;
}
