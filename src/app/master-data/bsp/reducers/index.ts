import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { chain, first } from 'lodash';

import * as fromBspProfile from './bsp-profile.reducer';
import * as fromConfiguration from './configuration.reducer';
import { RoutedMenuItem } from '~app/master-data/configuration';
import { AppState, getRouterState } from '~app/reducers';

export interface BspState {
  profile: fromBspProfile.ProfileState;
  configuration: fromConfiguration.ConfigurationState;
}

export interface State extends AppState {
  bsp: BspState;
}

export const reducers: ActionReducerMap<BspState> = {
  profile: fromBspProfile.reducer,
  configuration: fromConfiguration.reducer
};

export const getBspState = createFeatureSelector<State, BspState>('bsp');

export const getConfigurationState = createSelector(getBspState, state => state.configuration);

export const getBsp = createSelector(getBspState, state => state.profile.bsp);

export const getMenuItems = createSelector(getConfigurationState, state => state.menuItems);

export const getSelectedMenuItem = createSelector(getMenuItems, getRouterState, (menuItems, routerState) => {
  const { url } = routerState.state;

  return chain(menuItems)
    .find((item: RoutedMenuItem) => url.includes(item.route))
    .defaultTo(first(menuItems))
    .value();
});
