import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { BspCurrencyAssignationFilterFormatter } from './bsp-currency-assignation-filter-formatter';
import { BspCurrencyAssignationFilter } from '~app/master-data/models/bsp-currency-assignation-filter.model';

const filters = {
  status: 'pending'
} as BspCurrencyAssignationFilter;

const translate = (key: string) => `${key}`;

describe('BspCurrencyAssignationFilterFormatter', () => {
  let formatter: BspCurrencyAssignationFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new BspCurrencyAssignationFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['status'],
        label: `${translate('status')} - pending`
      }
    ]);
  });
});
