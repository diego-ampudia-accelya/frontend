import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { BspCurrencyAssignationFilter } from '~app/master-data/models/bsp-currency-assignation-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class BspCurrencyAssignationFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: BspCurrencyAssignationFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<BspCurrencyAssignationFilter>> = {
      status: status => `${this.translate('status')} - ${status}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate('' + key);
  }
}
