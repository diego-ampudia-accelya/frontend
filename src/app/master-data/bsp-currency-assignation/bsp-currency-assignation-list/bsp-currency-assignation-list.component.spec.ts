import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { BspCurrencyAssignationCreateModifyComponent } from '../bsp-currency-assignation-create-modify/bsp-currency-assignation-create-modify.component';
import { BspCurrencyAssignationFilterFormatter } from '../services/bsp-currency-assignation-filter-formatter';
import { BspCurrencyAssignationListComponent } from './bsp-currency-assignation-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { BspCurrencyAssignation } from '~app/master-data/models/bsp-currency-assignation.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { FooterButton } from '~app/shared/components';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService } from '~app/shared/services/notification.service';

describe('BspCurrencyAssignationListComponent', () => {
  let component: BspCurrencyAssignationListComponent;
  let fixture: ComponentFixture<BspCurrencyAssignationListComponent>;

  const dialogServiceSpy = createSpyObject<DialogService>(DialogService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { currencyAssignation: { ...ROUTES.BSP_CURRENCY_ASSIGNATION, id: 'currencyAssignation' } },
        activeTabId: 'currencyAssignation'
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BspCurrencyAssignationListComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        L10nTranslationService,
        BspCurrencyAssignationFilterFormatter,
        PermissionsService,
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BspCurrencyAssignationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call openCreateModifyCurrencyDialog when onActionClick is triggered with an actionType as Edit', () => {
    const mockAction = { actionType: GridTableActionType.Edit } as GridTableAction;
    const openCreateModifyCurrencyDialogSpy = spyOn(component, 'openCreateModifyCurrencyDialog');

    component.onActionClick({ action: mockAction, row: null });

    expect(openCreateModifyCurrencyDialogSpy).toHaveBeenCalled();
  });

  it('should not call openCreateModifyCurrencyDialog when onActionClick is triggered with an actionType that differs from Edit', () => {
    const mockAction = { actionType: GridTableActionType.Download } as GridTableAction;
    const openCreateModifyCurrencyDialogSpy = spyOn(component, 'openCreateModifyCurrencyDialog');

    component.onActionClick({ action: mockAction, row: null });

    expect(openCreateModifyCurrencyDialogSpy).not.toHaveBeenCalled();
  });

  it('should call open method from dialogService with an item and open modify dialog when openCreateModifyCurrencyDialog is triggered', () => {
    const itemMock = {
      id: 11
    } as unknown as BspCurrencyAssignation;
    component.openCreateModifyCurrencyDialog(itemMock);

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(BspCurrencyAssignationCreateModifyComponent, {
      data: {
        title: component['modifyDialogTitle'],
        hasCancelButton: true,
        isClosable: true,
        actionEmitter: component.dialogFormSubmitEmitter,
        footerButtonsType: [{ type: FooterButton.Modify }],
        item: {
          id: 11
        }
      }
    });
  });

  it('should call open method from dialogService with no item and open create dialog when openCreateModifyCurrencyDialog is triggered', () => {
    component.openCreateModifyCurrencyDialog();
    component.dialogFormSubmitEmitter.next();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(BspCurrencyAssignationCreateModifyComponent, {
      data: {
        title: component['createDialogTitle'],
        hasCancelButton: true,
        isClosable: true,
        actionEmitter: component.dialogFormSubmitEmitter,
        footerButtonsType: [{ type: FooterButton.Create }],
        item: null
      }
    });
  });

  it('should populate columns when initColumns is triggered', () => {
    component['initColumns']();

    expect(component.columns).toBeDefined();
  });
});
