import { BSP_CURRENCY_ASSIGNATION_LIST } from './bsp-currency-assignation-list.constants';
import { MasterDataType } from '~app/master-shared/models/master.model';

describe('Bsp currency assignation list constants', () => {
  it('should get bsp currency assignation columns when get method is triggered', () => {
    const columnsResult = BSP_CURRENCY_ASSIGNATION_LIST.get[MasterDataType.BSPCurrencyAssignation];

    expect(columnsResult).toBeDefined();
  });
});
