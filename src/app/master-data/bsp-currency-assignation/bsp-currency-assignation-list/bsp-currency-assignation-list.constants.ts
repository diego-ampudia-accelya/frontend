import { MasterDataType, MasterTableConfig } from '~app/master-shared/models/master.model';

export const BSP_CURRENCY_ASSIGNATION_LIST: MasterTableConfig = {
  get: {
    get [MasterDataType.BSPCurrencyAssignation]() {
      return {
        COLUMNS: [
          {
            prop: 'currency.code',
            name: 'MENU.MASTER_DATA.CURRENCY.LBL',
            resizeable: false,
            draggable: false
          },
          {
            prop: 'currency.decimals',
            name: 'LIST.MASTER_DATA.CURRENCY.COLUMNS.DECIMALS',
            resizeable: false,
            draggable: false
          },
          {
            prop: 'isDefault',
            name: 'LIST.MASTER_DATA.CURRENCY.COLUMNS.default',
            resizeable: false,
            draggable: false,
            cellTemplate: 'yesNoTmpl'
          },
          {
            prop: 'effectiveTo',
            name: 'LIST.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.COLUMNS.ASSIGNMENT_EXPIRY_DATE',
            resizeable: false,
            cellTemplate: 'dayMonthYearCellTmpl',
            draggable: false
          },
          {
            prop: 'active',
            name: 'LIST.MASTER_DATA.bsp.columns.status',
            resizeable: false,
            draggable: false,
            cellTemplate: 'statusTmpl'
          }
        ],
        ACTIONS: [],
        PRIMARY_ACTIONS: []
      };
    }
  }
};
