import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BspCurrencyAssignationCreateModifyComponent } from '../bsp-currency-assignation-create-modify/bsp-currency-assignation-create-modify.component';
import { BspCurrencyAssignationFilterFormatter } from '../services/bsp-currency-assignation-filter-formatter';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { BspCurrencyAssignationFilter } from '~app/master-data/models/bsp-currency-assignation-filter.model';
import { BspCurrencyAssignation } from '~app/master-data/models/bsp-currency-assignation.model';
import { BspCurrencyAssignationService } from '~app/master-data/services/bsp-currency-assignation.service';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-bsp-currency-assignation-list',
  templateUrl: './bsp-currency-assignation-list.component.html',
  styleUrls: ['./bsp-currency-assignation-list.component.scss'],
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: BspCurrencyAssignationService }
  ]
})
export class BspCurrencyAssignationListComponent implements OnInit, OnDestroy {
  public columns: GridColumn[];
  public actions = [{ action: GridTableActionType.Edit }];
  public header = this.translationService.translate('MENU.MASTER_DATA.bspCurrencyAssignationLabel.LBL');
  public userBsps$ = this.store.select(fromAuth.getAllBspNamesJoined);
  public customLabels = { create: 'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.createCurrencyBtn' };
  public dialogFormSubmitEmitter = new Subject<any>();
  public hasUpdateCurrencyPermission = this.permissionsService.hasPermission(Permissions.updateCurrency);
  public hasCreateCurrencyPermission = this.permissionsService.hasPermission(Permissions.createCurrency);
  public storedQuery: DataQuery<{ [key: string]: any }>;
  private modifyDialogTitle = 'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.dialog.titleModify';
  private createDialogTitle = 'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.dialog.titleCreate';
  private destroy$ = new Subject();

  constructor(
    public displayFormatter: BspCurrencyAssignationFilterFormatter,
    public dataSource: QueryableDataSource<BspCurrencyAssignation>,
    public dialogService: DialogService,
    public translationService: L10nTranslationService,
    private queryStorage: DefaultQueryStorage,
    private store: Store<AppState>,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.storedQuery = this.queryStorage.get();
    this.initColumns();
    this.loadData(this.storedQuery);
    this.dialogFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.loadData(this.storedQuery));
  }

  public loadData(query: DataQuery<BspCurrencyAssignationFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: BspCurrencyAssignation }) {
    if (action.actionType === GridTableActionType.Edit) {
      this.openCreateModifyCurrencyDialog(row);
    }
  }

  public openCreateModifyCurrencyDialog(item?: BspCurrencyAssignation): any {
    const dialogConfig: DialogConfig = {
      data: {
        title: item ? this.modifyDialogTitle : this.createDialogTitle,
        hasCancelButton: true,
        isClosable: true,
        actionEmitter: this.dialogFormSubmitEmitter,
        footerButtonsType: [{ type: item ? FooterButton.Modify : FooterButton.Create }],
        item: item ? item : null
      }
    };

    return this.dialogService.open(BspCurrencyAssignationCreateModifyComponent, dialogConfig);
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'currency.code',
        name: 'MENU.MASTER_DATA.CURRENCY.LBL',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'currency.decimals',
        name: 'LIST.MASTER_DATA.CURRENCY.COLUMNS.DECIMALS',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'isDefault',
        name: 'LIST.MASTER_DATA.CURRENCY.COLUMNS.default',
        resizeable: false,
        draggable: false,
        cellTemplate: 'yesNoTmpl'
      },
      {
        prop: 'effectiveTo',
        name: 'LIST.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.COLUMNS.ASSIGNMENT_EXPIRY_DATE',
        resizeable: false,
        cellTemplate: 'dayMonthYearCellTmpl',
        draggable: false
      },
      {
        prop: 'active',
        name: 'LIST.MASTER_DATA.bsp.columns.status',
        resizeable: false,
        draggable: false,
        cellTemplate: 'statusTmpl'
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
