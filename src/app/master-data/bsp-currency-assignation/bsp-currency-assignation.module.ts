import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BspCurrencyAssignationCreateModifyComponent } from './bsp-currency-assignation-create-modify/bsp-currency-assignation-create-modify.component';
import { BspCurrencyAssignationListComponent } from './bsp-currency-assignation-list/bsp-currency-assignation-list.component';
import { BspCurrencyAssignationRoutingModule } from './bsp-currency-assignation-routing.module';
import { BspCurrencyAssignationFilterFormatter } from './services/bsp-currency-assignation-filter-formatter';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [BspCurrencyAssignationListComponent, BspCurrencyAssignationCreateModifyComponent],
  imports: [CommonModule, BspCurrencyAssignationRoutingModule, SharedModule],
  providers: [BspCurrencyAssignationFilterFormatter]
})
export class BspCurrencyAssignationModule {}
