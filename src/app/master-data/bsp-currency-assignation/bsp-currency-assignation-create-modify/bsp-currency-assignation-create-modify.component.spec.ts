import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { of, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { BspCurrencyAssignationCreateModifyComponent } from './bsp-currency-assignation-create-modify.component';
import { BspCurrencyAssignation } from '~app/master-data/models/bsp-currency-assignation.model';
import { BspCurrencyAssignationService } from '~app/master-data/services';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ROUTES } from '~app/shared/constants';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { NotificationService } from '~app/shared/services';

describe('BspCurrencyAssignationCreateModifyComponent', () => {
  let component: BspCurrencyAssignationCreateModifyComponent;
  let fixture: ComponentFixture<BspCurrencyAssignationCreateModifyComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;

  const bspCurrencyAssignationServiceSpy =
    createSpyObject<BspCurrencyAssignationService>(BspCurrencyAssignationService);

  const itemMock = {
    id: 6983886786,
    isDefault: false,
    effectiveFrom: '2000-01-01',
    effectiveTo: '2023-03-22',
    active: false,
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      active: true,
      version: 4490,
      defaultCurrencyCode: 'EUR'
    },
    currency: { id: 886786, code: 'XCV', decimals: 2, version: 79 }
  } as unknown as BspCurrencyAssignation;

  const dialogConfigSpy: DialogConfig = {
    data: {
      title: 'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.dialog.titleModify',
      buttons: [
        { title: 'BUTTON.DEFAULT.CANCEL', buttonDesign: 'tertiary', type: 'cancel', isDisabled: false },
        { title: 'BUTTON.DEFAULT.MODIFY', buttonDesign: 'primary', type: 'modify', isDisabled: true },
        { title: 'BUTTON.DEFAULT.CREATE', buttonDesign: 'primary', type: 'create', isDisabled: true }
      ],
      footerButtonsType: [
        { type: FooterButton.Modify, isDisabled: true },
        { type: FooterButton.Create, isDisabled: true }
      ],
      item: itemMock
    }
  };

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { currencyAssignation: { ...ROUTES.BSP_CURRENCY_ASSIGNATION, id: 'currencyAssignation' } },
        activeTabId: 'currencyAssignation'
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);

    TestBed.configureTestingModule({
      declarations: [BspCurrencyAssignationCreateModifyComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: DialogConfig, useValue: dialogConfigSpy },
        { provide: BspCurrencyAssignationService, useValue: bspCurrencyAssignationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        provideMockStore({ initialState }),
        NotificationService,
        BspCurrencyAssignationService,
        MessageService,
        ReactiveSubject,
        FormBuilder
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BspCurrencyAssignationCreateModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize dialog buttons', () => {
    dialogConfigSpy.data = {
      buttons: [
        { title: 'BUTTON.DEFAULT.MODIFY', buttonDesign: 'primary', type: 'modify', isDisabled: true },
        { title: 'BUTTON.DEFAULT.CREATE', buttonDesign: 'primary', type: 'create', isDisabled: true }
      ]
    };
    component['initializeButtons']();

    expect(component['modifyButton']).toEqual({
      title: 'BUTTON.DEFAULT.MODIFY',
      buttonDesign: 'primary',
      type: 'modify',
      isDisabled: true
    });
    expect(component['createButton']).toEqual({
      title: 'BUTTON.DEFAULT.CREATE',
      buttonDesign: 'primary',
      type: 'create',
      isDisabled: true
    });
  });

  it('should not set isDisabled property of the buttons to true when they are null or undefined', () => {
    dialogConfigSpy.data = {
      buttons: []
    };
    component['initializeButtons']();

    expect(component['modifyButton']).toBeUndefined();
    expect(component['createButton']).toBeUndefined();
  });

  it('should set modifyButton isDisabled to true if form is not valid', fakeAsync(() => {
    component['modifyButton'] = {
      title: 'BUTTON.DEFAULT.MODIFY',
      buttonDesign: 'primary',
      type: 'modify',
      isDisabled: true
    };
    spyOn<any>(component.form.statusChanges, 'pipe').and.returnValue(of('INVALID'));

    component.ngAfterViewInit();
    component.form.statusChanges.subscribe();
    tick();

    expect(component['modifyButton'].isDisabled).toBe(true);
  }));

  it('should set modifyButton isDisabled to false if form is valid', fakeAsync(() => {
    component['modifyButton'] = {
      title: 'BUTTON.DEFAULT.MODIFY',
      buttonDesign: 'primary',
      type: 'modify',
      isDisabled: true
    };
    spyOn<any>(component.form.statusChanges, 'pipe').and.returnValue(of('VALID'));

    component.ngAfterViewInit();
    component.form.statusChanges.subscribe();
    tick();

    expect(component['modifyButton'].isDisabled).toBe(false);
  }));

  it('should not set isDisabled property if modifyButton and createButton are not defined', fakeAsync(() => {
    component['modifyButton'] = undefined;
    component['createButton'] = undefined;
    spyOn<any>(component.form.statusChanges, 'pipe').and.returnValue(of('VALID'));

    component.ngAfterViewInit();
    component.form.statusChanges.subscribe();
    tick();

    expect(component['modifyButton']).not.toBeDefined();
  }));

  it('should trigger showSuccess and translate methods when initializeFormListeners is triggered', () => {
    (component as any).updateButtonClick$ = of({}).pipe(switchMap(() => of({})));
    (component as any).createButtonClick$ = of({}).pipe(switchMap(() => of({})));
    (component as any).actionEmitterChanged = new Subject<any>();

    spyOn<any>(component, 'requestCurrencyForm').and.returnValue(of({}));

    const notificationServiceSpy = spyOn(component.notificationService, 'showSuccess');
    const translationServiceSpy = spyOn(component.translationService, 'translate').and.returnValue('Translation');

    (component as any).initializeFormListeners();

    expect(notificationServiceSpy).toHaveBeenCalled();
    expect(translationServiceSpy).toHaveBeenCalled();
  });

  it('should close dialog and return bspService update when requestCurrencyForm  is triggered with an item', () => {
    component['item'] = itemMock;
    component['requestCurrencyForm']();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
  });

  it('should close dialog and return bspService create when requestCurrencyForm  is triggered with no item', () => {
    component['item'] = null;
    component['requestCurrencyForm']();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
  });

  it('should set isEditMode to true if there is an item setted', () => {
    dialogConfigSpy.data.item = itemMock;
    component['item'] = itemMock;
    component['setItemsFeatures']();

    expect(component.isEditMode).toBe(true);
  });

  it('should set isEditMode to false if there is not an item setted', () => {
    dialogConfigSpy.data.item = null;
    component['setItemsFeatures']();

    expect(component.isEditMode).toBe(false);
  });
});
