import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, first, switchMap, takeUntil } from 'rxjs/operators';

import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { BspCurrencyAssignation } from '~app/master-data/models/bsp-currency-assignation.model';
import { BspCurrencyRequest } from '~app/master-data/models/bsp-currency-request.model';
import { BspCurrency } from '~app/master-data/models/bsp-currency.model';
import { BspCurrencyAssignationService } from '~app/master-data/services';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-bsp-currency-assignation-create-modify',
  templateUrl: './bsp-currency-assignation-create-modify.component.html'
})
export class BspCurrencyAssignationCreateModifyComponent implements OnInit, OnDestroy, AfterViewInit {
  public form: FormGroup;
  public expiryMinDate = new Date();
  public decimalOptions: { value: number; label: number }[];
  public isEditMode = false;
  private decimalsOptionsValues = Array.from(Array(10).keys());
  private modifyButton: ModalAction;
  private createButton: ModalAction;
  private item: BspCurrencyAssignation;
  private currencyControl: FormControl;
  private decimalsControl: FormControl;
  private effectiveToControl: FormControl;
  private formFactory: FormUtil;
  private bspId: number;
  private confirmationMessageModification =
    'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.dialog.form.confirmationMessageModification';
  private confirmationMessageCreation =
    'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.dialog.form.confirmationMessageCreation';
  private actionEmitterChanged = new Subject<any>();

  private destroy$ = new Subject();

  private updateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    first(),
    filter(action => action?.clickedBtn === FooterButton.Modify)
  );

  private createButtonClick$ = this.reactiveSubject.asObservable.pipe(
    first(),
    filter(action => action?.clickedBtn === FooterButton.Create)
  );

  constructor(
    public dialogService: DialogService,
    public config: DialogConfig,
    public notificationService: NotificationService,
    public translationService: L10nTranslationService,
    public bspService: BspCurrencyAssignationService,
    private reactiveSubject: ReactiveSubject,
    private formBuilder: FormBuilder,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.initializeForm();
    this.populatedecimalOptions();
    this.initializeButtons();
    this.setItemsFeatures();
    this.initializeFormListeners();
    this.selectCurrentBspId();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.modifyButton) {
        this.modifyButton.isDisabled = status !== 'VALID';
      }
      if (this.createButton) {
        this.createButton.isDisabled = status !== 'VALID';
      }
    });
  }

  private selectCurrentBspId(): void {
    this.store
      .pipe(select(fromAuth.getUserBsps), first())
      .subscribe(bsps => (this.bspId = bsps && bsps[0] ? bsps[0].id : null));
  }

  private populatedecimalOptions(): void {
    this.decimalOptions = this.decimalsOptionsValues.map((value: number) => ({
      value,
      label: value
    }));
  }

  private initializeFormListeners(): void {
    this.updateButtonClick$.pipe(switchMap(() => this.requestCurrencyForm())).subscribe(() => {
      this.actionEmitterChanged.next();
      this.notificationService.showSuccess(
        this.translationService.translate(this.confirmationMessageModification, {
          code: this.currencyControl.value
        })
      );
    });

    this.createButtonClick$.pipe(switchMap(() => this.requestCurrencyForm())).subscribe(() => {
      this.actionEmitterChanged.next();
      this.notificationService.showSuccess(
        this.translationService.translate(this.confirmationMessageCreation, { code: this.currencyControl.value })
      );
    });
  }

  private requestCurrencyForm(): Observable<BspCurrencyAssignation> {
    this.dialogService.close();

    const requestValues: BspCurrencyRequest = {
      effectiveTo: this.effectiveToControl.value ? toShortIsoDate(this.effectiveToControl.value) : null,
      currency: {
        code: this.currencyControl.value,
        decimals: this.decimalsControl.value
      }
    };

    return this.item?.id
      ? this.bspService.update(this.bspId, this.item.id, requestValues)
      : this.bspService.create(this.bspId, requestValues);
  }

  private initializeButtons(): void {
    this.modifyButton = this.config.data.buttons.find(button => button.type === FooterButton.Modify);
    this.createButton = this.config.data.buttons.find(button => button.type === FooterButton.Create);

    if (this.modifyButton) {
      this.modifyButton.isDisabled = true;
    }
    if (this.createButton) {
      this.createButton.isDisabled = true;
    }
  }

  private initializeForm(): void {
    this.currencyControl = new FormControl(null, [
      Validators.required,
      Validators.maxLength(3),
      Validators.pattern('^[A-Z ]*$')
    ]);
    this.decimalsControl = new FormControl(null, [Validators.required, Validators.pattern('[0-9]')]);
    this.effectiveToControl = new FormControl(null);

    this.form = this.formFactory.createGroup<BspCurrency>({
      code: this.currencyControl,
      decimals: this.decimalsControl,
      effectiveTo: this.effectiveToControl
    });
  }

  private setItemsFeatures() {
    this.item = this.config.data.item;
    this.actionEmitterChanged = this.config.data.actionEmitter;
    this.isEditMode = !!this.item;

    if (this.item) {
      this.currencyControl.patchValue(this.item.currency.code);
      this.decimalsControl.patchValue(this.item.currency.decimals);
      this.effectiveToControl.patchValue(this.item.effectiveTo);
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
