import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BspCurrencyAssignationListComponent } from './bsp-currency-assignation-list/bsp-currency-assignation-list.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: BspCurrencyAssignationListComponent,
    data: {
      tab: ROUTES.BSP_CURRENCY_ASSIGNATION
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BspCurrencyAssignationRoutingModule {}
