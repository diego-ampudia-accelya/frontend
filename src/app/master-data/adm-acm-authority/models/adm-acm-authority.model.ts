import { ThirdParty } from '~app/master-data/models/third-party.model';

export interface AdmAcmAuthority {
  thirdParty: ThirdParty;
  airline: {
    id: number;
    iataCode: string;
    localName: string;
    designator?: string;
  };
  hasADM: boolean;
  hasACM: boolean;
}

export interface AdmAcmAuthorityViewModel extends AdmAcmAuthority {
  original: AdmAcmAuthority;
}
