import { ViewAs } from './view-as.model';
import { AirlineSummary, ThirdPartySummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

export interface AdmAcmAuthorityFilter {
  viewAs?: ViewAs;
  airlines?: AirlineSummary[];
  thirdParties?: ThirdPartySummary[];
  hasACM?: boolean;
  hasADM?: boolean;
  active?: boolean;
  bsp?: Bsp;
}
