import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { AdmAcmAuthorityFormatter } from '../services/adm-acm-authority-formatter.service';
import { AdmAcmAuthorityService } from '../services/adm-acm-authority.service';
import { AdmAcmAuthorityListActions } from '../store/actions';
import * as fromList from '../store/reducers/list.reducer';
import { AdmAcmAuthorityComponent } from './adm-acm-authority.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { createThirdPartyUser } from '~app/shared/mocks/third-party-user';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';
import { AirlineDictionaryService, AppConfigurationService, ThirdPartyDictionaryService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('AdmAcmAuthorityComponent', () => {
  let component: AdmAcmAuthorityComponent;
  let fixture: ComponentFixture<AdmAcmAuthorityComponent>;
  let displayFormatterSpy: SpyObject<AdmAcmAuthorityFormatter>;

  let changesDialogServiceSpy: SpyObject<ChangesDialogService>;
  let airlineDictionaryService: SpyObject<AirlineDictionaryService>;
  let thirdPartyDictionaryService: SpyObject<ThirdPartyDictionaryService>;
  let permissionsService: SpyObject<PermissionsService>;
  let mockStore: MockStore<AppState>;

  const airlineUser = createAirlineUser();
  const airlines = { ...airlineUser.globalAirline, airlines: createAirlineUser() };
  const initialUser = { ...airlineUser, globalAirline: airlines };

  beforeEach(waitForAsync(() => {
    displayFormatterSpy = createSpyObject(AdmAcmAuthorityFormatter);

    changesDialogServiceSpy = createSpyObject(ChangesDialogService);
    airlineDictionaryService = createSpyObject(AirlineDictionaryService);
    thirdPartyDictionaryService = createSpyObject(ThirdPartyDictionaryService);
    permissionsService = createSpyObject(PermissionsService);

    airlineDictionaryService.getDropdownOptions.and.returnValue(of([]));
    thirdPartyDictionaryService.getDropdownOptions.and.returnValue(of([]));
    permissionsService.hasPermission.and.returnValue(true);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AdmAcmAuthorityComponent, TranslatePipeMock],
      providers: [
        provideMockStore({
          selectors: [{ selector: fromAuth.getUser, value: initialUser }],
          initialState: {
            admAcmAuthority: {
              list: {
                ...fromList.initialState,
                requiredFilter: {
                  bsp: { id: 5, isoCountryCode: 'ES' }
                }
              }
            }
          }
        }),
        FormBuilder,
        EmptyPipe,
        { provide: PermissionsService, useValue: permissionsService },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: AdmAcmAuthorityService, useValue: airlineDictionaryService },
        { provide: ChangesDialogService, useValue: changesDialogServiceSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(AdmAcmAuthorityComponent, {
        set: {
          providers: [{ provide: AdmAcmAuthorityFormatter, useValue: displayFormatterSpy }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmAcmAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockStore = TestBed.inject(MockStore);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should configure columns when viewing as Airline', fakeAsync(() => {
    component.ngOnInit();

    tick();
    expect(component.columns.length).toBe(6);
  }));

  it('should configure columns when viewing as Third Party', fakeAsync(() => {
    mockStore.overrideSelector(fromAuth.getUser, createThirdPartyUser());
    mockStore.refreshState();

    component.ngOnInit();
    tick();
    expect(component.columns.length).toBe(4);
  }));

  it('should ignore airlines filter when viewing as Airline', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(component.displayFormatter.ignoredFilters).toEqual(['airlines']);
  }));

  it('should ignore third parties filter when viewing as Third Party', fakeAsync(() => {
    mockStore.overrideSelector(fromAuth.getUser, createThirdPartyUser());
    mockStore.refreshState();

    component.ngOnInit();
    tick();

    expect(component.displayFormatter.ignoredFilters).toEqual(['thirdParties', 'active']);
  }));

  it('should update query when user is lean and bsp is preselected', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    const mockBsp: any = { id: 1, isoCountryCode: 'ES' };
    component.isBspFilterLocked = true;
    component['selectedBsp'] = mockBsp;
    component['requiredFilters'] = { bsp: mockBsp };
    component['initializeQuery']();

    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      AdmAcmAuthorityListActions.changeRequiredFilter({ requiredFilter: component['requiredFilters'] })
    );
  }));
});
