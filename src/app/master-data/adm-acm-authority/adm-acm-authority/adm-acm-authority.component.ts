import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { isEmpty } from 'lodash';
import cloneDeep from 'lodash/cloneDeep';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, first, map, takeUntil } from 'rxjs/operators';

import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { AdmAcmAuthorityFilter, AdmAcmAuthorityViewModel, ViewAs } from '../models';
import { AdmAcmAuthorityFormatter } from '../services/adm-acm-authority-formatter.service';
import { AdmAcmAuthorityListActions } from '../store/actions';
import * as fromAdmAcmAuthority from '../store/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AirlineSummary, DropdownOption, ThirdPartySummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AirlineDictionaryService, ThirdPartyDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-adm-acm-authority',
  templateUrl: './adm-acm-authority.component.html',
  styleUrls: ['./adm-acm-authority.component.scss'],
  providers: [AdmAcmAuthorityFormatter]
})
export class AdmAcmAuthorityComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  public get isThirdPartyView(): boolean {
    return this.viewAs === ViewAs.ThirdParty;
  }

  public get isAirlineView(): boolean {
    return this.viewAs === ViewAs.Airline;
  }

  public loading$ = this.store.pipe(select(fromAdmAcmAuthority.getListLoading));
  public data$ = this.store.pipe(select(fromAdmAcmAuthority.getListData));
  public query$ = this.store.pipe(select(fromAdmAcmAuthority.getListQuery));
  public hasData$ = this.store.pipe(select(fromAdmAcmAuthority.hasData));
  public thirdPartyOptions$: Observable<DropdownOption<ThirdPartySummary>[]>;
  public airlines$: Observable<DropdownOption<AirlineSummary>[]>;

  public userBspOptions: DropdownOption<Bsp>[] = [];

  public columns: TableColumn[];
  public searchForm: FormGroup;
  public statusOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatStatus(value)
  }));

  public viewAs: ViewAs = ViewAs.Airline;

  public hasLeanPermission: boolean;
  public isBspFilterLocked: boolean;
  public isSearchEnabled = false;
  public canShowDataQuery = true;
  public hasFilteringExpanded = false;
  public isThirdPartyFilterLocked = false;

  public predefinedFilters: AdmAcmAuthorityFilter;
  private requiredFilters: AdmAcmAuthorityFilter;
  private loggedUser: User;
  private selectedBsp: Bsp;

  private destroy$ = new Subject();

  constructor(
    public displayFormatter: AdmAcmAuthorityFormatter,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private changesDialogService: ChangesDialogService,
    private airlineDictionaryService: AirlineDictionaryService,
    private thirdPartyDictionaryService: ThirdPartyDictionaryService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.searchForm = this.buildForm();

    this.initializeFeaturesByLoggedUser();
  }

  private initializeFeaturesByLoggedUser(): void {
    this.store.pipe(select(getUser), first()).subscribe(loggedUser => {
      this.loggedUser = loggedUser;

      this.viewAs = loggedUser.userType === UserType.AIRLINE ? ViewAs.Airline : ViewAs.ThirdParty;
      this.displayFormatter.ignoredFilters = this.getIgnoredFilters();

      this.columns = this.getColumns();

      this.initializeRequiredFilters();

      if (this.hasLeanPermission) {
        this.initializeLeanFlags();
        this.initializeBspListener();
        this.initializeBspOptions();
      } else {
        this.isSearchEnabled = true;
        this.initializeNoLeanDropDowns();
      }

      this.initializeQuery();
    });
  }

  private initializeNoLeanDropDowns(): void {
    const userBsp = this.loggedUser.bsps[0];
    this.thirdPartyOptions$ = this.thirdPartyDictionaryService.getDropdownOptions(userBsp.isoCountryCode);

    this.airlines$ = this.airlineDictionaryService.getDropdownOptions(userBsp.id);
  }

  private initializeLeanFlags(): void {
    this.canShowDataQuery = false;
    this.hasFilteringExpanded = true;
    this.isThirdPartyFilterLocked = true;
  }

  private initializeRequiredFilters(): void {
    if (this.loggedUser.userType === UserType.AIRLINE) {
      const selectedAirline = this.loggedUser;
      const airline = selectedAirline.globalAirline.airlines[0];
      const airlineFilter = [this.airlineDictionaryService.summarize(airline)];

      this.requiredFilters = {
        ...(airlineFilter && { airlines: airlineFilter }),
        viewAs: ViewAs.Airline
      };

      if (!this.hasLeanPermission) {
        this.requiredFilters = { ...this.requiredFilters, bsp: this.loggedUser.bsps[0] };
        this.setPredefinedFiltersAndBsp();
      }
    }

    if (this.loggedUser.userType === UserType.THIRD_PARTY) {
      this.requiredFilters = {
        bsp: this.loggedUser.bsps?.length ? this.loggedUser.bsps[0] : null,
        viewAs: ViewAs.ThirdParty
      };
      this.setPredefinedFiltersAndBsp();
    }
  }

  private setPredefinedFiltersAndBsp(): void {
    this.selectedBsp = this.loggedUser.bsps[0];
    this.predefinedFilters = cloneDeep(this.requiredFilters);
  }

  private initializeQuery(): void {
    this.query$.pipe(first()).subscribe(query => {
      // Bsp could have been stored or preselected if there is only one available
      if (!isEmpty(query?.filterBy?.bsp) || this.selectedBsp) {
        if (this.hasLeanPermission) {
          this.canShowDataQuery = true;
          this.hasFilteringExpanded = false;
          this.isThirdPartyFilterLocked = false;
        }
        const requiredFilter = { ...query?.filterBy, ...this.requiredFilters };

        this.store.dispatch(AdmAcmAuthorityListActions.changeRequiredFilter({ requiredFilter }));
      }
    });
  }

  private initializeBspOptions(): void {
    this.userBspOptions = this.filterBspsByPermission()?.map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    const oneBspWithPermission = this.userBspOptions?.length === 1;

    this.isBspFilterLocked = oneBspWithPermission;

    if (oneBspWithPermission) {
      const bspControl = FormUtil.get<AdmAcmAuthorityFilter>(this.searchForm, 'bsp');
      bspControl.patchValue(this.userBspOptions[0].value);
    }
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readACDMAuthorityAirline;
    if (this.loggedUser) {
      const { bsps, bspPermissions } = this.loggedUser;

      return bsps.filter(bsp =>
        bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
      );
    }
  }

  private initializeBspListener(): void {
    FormUtil.get<AdmAcmAuthorityFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        filter(val => !!val),
        distinctUntilChanged(),
        takeUntil(this.destroy$)
      )
      .subscribe(selectedBspValue => {
        this.selectedBsp = selectedBspValue;
        this.isSearchEnabled = selectedBspValue && !isEmpty(selectedBspValue);
        if (this.isSearchEnabled) {
          this.updateThirdParty();
          this.requiredFilters = { ...this.requiredFilters, bsp: this.selectedBsp };
          this.predefinedFilters = { bsp: this.selectedBsp };
        }
      });
  }

  private updateThirdParty(): void {
    this.isThirdPartyFilterLocked = false;
    const thirdPartiesControl = FormUtil.get<AdmAcmAuthorityFilter>(this.searchForm, 'thirdParties');
    thirdPartiesControl.reset();
    this.thirdPartyOptions$ = this.thirdPartyDictionaryService.getDropdownOptions(this.selectedBsp.isoCountryCode).pipe(
      map(options =>
        options.map(option => ({
          ...option,
          label: `${this.selectedBsp.isoCountryCode} - ${option.label}`
        }))
      )
    );
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  public onQueryChanged(query: DataQuery<AdmAcmAuthorityFilter>): void {
    const isBspSelectedValue = !isEmpty(this.selectedBsp);
    this.canShowDataQuery = true;

    if (this.hasLeanPermission && isBspSelectedValue) {
      const requiredFilter = { ...query.filterBy, ...this.requiredFilters };
      const queryToDispatch = { ...query, filterBy: requiredFilter };
      this.store.dispatch(AdmAcmAuthorityListActions.search({ query: queryToDispatch }));
    }

    if (this.hasLeanPermission && !isBspSelectedValue) {
      this.canShowDataQuery = false;
      this.isThirdPartyFilterLocked = true;
      this.hasFilteringExpanded = true;
    }

    if (!this.hasLeanPermission) {
      this.store.dispatch(AdmAcmAuthorityListActions.search({ query }));
    }
  }

  public onModify(items: AdmAcmAuthorityViewModel[]): void {
    this.store.dispatch(AdmAcmAuthorityListActions.modify({ items }));
  }

  public onDownload(): void {
    this.store.dispatch(AdmAcmAuthorityListActions.download());
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      thirdParties: [],
      airlines: [],
      hasADM: [],
      hasACM: [],
      active: [],
      bsp: []
    });
  }

  private getColumns(): Array<TableColumn & { viewAs?: ViewAs }> {
    const editableStatusColumn: Partial<TableColumn> = {
      headerTemplate: 'checkboxHeaderTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl',
      cellClass: this.isChanged.bind(this)
    };
    const readOnlyStatusColumn: Partial<TableColumn> = {
      pipe: { transform: this.displayFormatter.formatStatus }
    };

    const hasAcdmUpdatePermission = this.permissionsService.hasPermission(Permissions.updateACDMAuthorityAirline);
    const statusColumn = this.isAirlineView && hasAcdmUpdatePermission ? editableStatusColumn : readOnlyStatusColumn;

    return [
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.thirdPartyCode',
        prop: 'thirdParty.code',
        flexGrow: 1.1,
        viewAs: ViewAs.Airline
      },
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 0.7,
        viewAs: ViewAs.ThirdParty
      },
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.organization',
        prop: 'thirdParty.organization',
        flexGrow: 1.3,
        viewAs: ViewAs.Airline
      },
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.thirdPartyName',
        prop: 'thirdParty.name',
        flexGrow: 1.5,
        viewAs: ViewAs.Airline
      },
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.airlineName',
        prop: 'airline.localName',
        flexGrow: 2.5,
        viewAs: ViewAs.ThirdParty
      },
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.active',
        prop: 'thirdParty.active',
        flexGrow: 1,
        viewAs: ViewAs.Airline,
        cellTemplate: 'yesNoTmpl'
      },
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.admAuthorityStatus',
        prop: 'hasADM',
        flexGrow: 1,
        ...statusColumn
      },
      {
        name: 'LIST.MASTER_DATA.admAcmAuthority.acmAuthorityStatus',
        prop: 'hasACM',
        flexGrow: 1,
        ...statusColumn
      }
    ].filter(column => !column.viewAs || column.viewAs === this.viewAs);
  }

  private getIgnoredFilters(): Array<keyof AdmAcmAuthorityFilter> {
    const ignoredFiltersByView: Array<keyof AdmAcmAuthorityFilter> = this.isAirlineView
      ? ['airlines']
      : ['thirdParties', 'active'];

    return this.hasLeanPermission ? ignoredFiltersByView : [...ignoredFiltersByView, 'bsp'];
  }

  private isChanged({ row, column }: { row: AdmAcmAuthorityViewModel; column: TableColumn }): {
    [key: string]: boolean;
  } {
    const { prop } = column;

    return {
      'checkbox-control-changed': row.original[prop] !== row[prop],
      'checkbox-control': true
    };
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
