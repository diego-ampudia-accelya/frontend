import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AdmAcmAuthorityComponent } from './adm-acm-authority/adm-acm-authority.component';
import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { AdmAcmAuthorityEffects } from './store/effects/adm-acm-authority.effects';
import * as fromAdmAcmAuthority from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [AdmAcmAuthorityComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromAdmAcmAuthority.admAcmAuthorityFeatureKey, fromAdmAcmAuthority.reducers),
    EffectsModule.forFeature([AdmAcmAuthorityEffects])
  ],
  exports: [AdmAcmAuthorityComponent],
  providers: [ChangesDialogService]
})
export class AdmAcmAuthorityModule {}
