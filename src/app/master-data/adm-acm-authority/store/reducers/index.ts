import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './list.reducer';
import { AppState } from '~app/reducers';

export const admAcmAuthorityFeatureKey = 'admAcmAuthority';

export interface State extends AppState {
  [admAcmAuthorityFeatureKey]: AdmAcmAuthorityState;
}

export interface AdmAcmAuthorityState {
  [fromList.key]: fromList.State;
}

export function reducers(state: AdmAcmAuthorityState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getAdmAcmAuthority = createFeatureSelector<State, AdmAcmAuthorityState>(admAcmAuthorityFeatureKey);

export const getList = createSelector(getAdmAcmAuthority, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const getListQuery = createSelector(getList, fromList.getQuery);

export const getDownloadQuery = createSelector(
  getListQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

export const getBsp = createSelector(getList, fromList.getBsp);

export const getRequiredFilter = createSelector(getList, fromList.getRequiredFilter);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getChanges = createSelector(getList, fromList.getChanges);

export const hasChanges = createSelector(getList, fromList.hasChanges);
