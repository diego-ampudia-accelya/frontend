import { AdmAcmAuthorityFilter, AdmAcmAuthorityViewModel } from '../../models';
import { AdmAcmAuthorityListActions } from '../actions';
import * as fromList from './list.reducer';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AirlineSummary } from '~app/shared/models';

describe('Adm/Acm authority list reducer', () => {
  it('should update query on search', () => {
    const action = AdmAcmAuthorityListActions.search({ query: defaultQuery });

    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.query).toEqual(defaultQuery);
  });

  it('should set loading to true on searchConfirmed', () => {
    const action = AdmAcmAuthorityListActions.searchConfirmed();

    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.loading).toBeTruthy();
  });

  it('should set loading to false on searchSuccess', () => {
    const action = AdmAcmAuthorityListActions.searchSuccess({
      data: {
        records: [{} as AdmAcmAuthorityViewModel],
        totalPages: 1,
        total: 1,
        pageNumber: 0,
        pageSize: 20
      }
    });

    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.loading).toBeFalsy();
  });

  it('should set totalElements and data on searchSuccess', () => {
    const action = AdmAcmAuthorityListActions.searchSuccess({
      data: {
        records: [{} as AdmAcmAuthorityViewModel],
        totalPages: 1,
        total: 1,
        pageNumber: 0,
        pageSize: 20
      }
    });

    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.data.length).toBe(1);
    expect(newState.query.paginateBy).toEqual({
      page: 0,
      size: 20,
      totalElements: 1
    });
  });

  it('should set loading to false on searchError', () => {
    const action = AdmAcmAuthorityListActions.searchError();

    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.loading).toBeFalsy();
  });

  it('should set data to [] on searchError', () => {
    const action = AdmAcmAuthorityListActions.searchError();

    const newState = fromList.reducer(fromList.initialState, action);

    expect(newState.data).toEqual([]);
  });

  describe('on changeRequiredFilter', () => {
    it('should preserve query when it matches required filter', () => {
      const airline = { id: 1 } as AirlineSummary;
      const existingQuery: DataQuery<AdmAcmAuthorityFilter> = {
        ...defaultQuery,
        filterBy: { airlines: [airline], hasADM: true }
      };

      const requiredFilter = { airlines: [airline] };
      const state: fromList.State = {
        ...fromList.initialState,
        requiredFilter,
        query: existingQuery
      };
      const action = AdmAcmAuthorityListActions.changeRequiredFilter({
        requiredFilter
      });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(existingQuery);
    });

    it('should reset to default query when the query does not match required filter', () => {
      const airline = { id: 1 } as AirlineSummary;
      const otherAirline = { id: 2 } as AirlineSummary;
      const existingQuery: DataQuery<AdmAcmAuthorityFilter> = {
        ...defaultQuery,
        filterBy: { airlines: [airline], hasADM: true }
      };
      const requiredFilter: AdmAcmAuthorityFilter = {
        airlines: [otherAirline]
      };
      const expectedQuery: DataQuery<AdmAcmAuthorityFilter> = {
        ...defaultQuery,
        filterBy: requiredFilter
      };
      const state: fromList.State = {
        ...fromList.initialState,
        query: existingQuery
      };
      const action = AdmAcmAuthorityListActions.changeRequiredFilter({
        requiredFilter
      });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(expectedQuery);
    });
  });

  describe('hasChanges', () => {
    it('should return true when hasADM was changed', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: [
          {
            airline: null,
            thirdParty: null,
            hasADM: true,
            hasACM: true,
            original: {
              airline: null,
              thirdParty: null,
              hasADM: false,
              hasACM: false
            }
          }
        ]
      };

      expect(fromList.hasChanges(state)).toBeTruthy();
    });

    it('should return false when hasADM was not changed', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: [
          {
            airline: null,
            thirdParty: null,
            hasADM: false,
            hasACM: true,
            original: {
              airline: null,
              thirdParty: null,
              hasADM: false,
              hasACM: true
            }
          }
        ]
      };

      expect(fromList.hasChanges(state)).toBeFalsy();
    });
  });
});
