import { createReducer, on } from '@ngrx/store';
import { cloneDeep, isEqual } from 'lodash';

import { AdmAcmAuthorityFilter, AdmAcmAuthorityViewModel } from '../../models';
import { AdmAcmAuthorityListActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const key = 'list';

export interface State {
  previousQuery: DataQuery<AdmAcmAuthorityFilter>;
  query: DataQuery<AdmAcmAuthorityFilter>;
  data: AdmAcmAuthorityViewModel[];
  loading: boolean;
  requiredFilter: AdmAcmAuthorityFilter;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false,
  requiredFilter: {}
};

const determineInitialQuery = (
  newRequiredFilter: Partial<AdmAcmAuthorityFilter>,
  oldRequiredFilter: Partial<AdmAcmAuthorityFilter>,
  storedQuery: DataQuery<AdmAcmAuthorityFilter>
): DataQuery<AdmAcmAuthorityFilter> => {
  let query: DataQuery<AdmAcmAuthorityFilter> = storedQuery || cloneDeep(defaultQuery);

  // when the required filter has changed
  if (!isEqual(newRequiredFilter, oldRequiredFilter)) {
    query = cloneDeep(defaultQuery);
    query.filterBy = newRequiredFilter;
  }

  return query;
};

export const reducer = createReducer(
  initialState,
  on(AdmAcmAuthorityListActions.search, (state, { query }) => ({
    ...state,
    previousQuery: state.query,
    requiredFilter: { ...state.requiredFilter, ...query.filterBy },
    query
  })),
  on(AdmAcmAuthorityListActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(AdmAcmAuthorityListActions.searchCanceled, state => ({
    ...state,
    query: state.previousQuery,
    previousQuery: null
  })),
  on(AdmAcmAuthorityListActions.searchSuccess, (state, { data }) => ({
    ...state,
    loading: false,
    data: data.records,
    query: {
      ...state.query,
      paginateBy: {
        page: data.pageNumber,
        size: data.pageSize,
        totalElements: data.total
      }
    }
  })),
  on(AdmAcmAuthorityListActions.searchError, state => ({
    ...state,
    loading: false,
    data: []
  })),
  on(AdmAcmAuthorityListActions.changeRequiredFilter, (state, { requiredFilter }) => ({
    ...state,
    requiredFilter,
    query: determineInitialQuery(requiredFilter, state.requiredFilter, state.query)
  })),
  on(AdmAcmAuthorityListActions.modify, (state, { items }) => ({
    ...state,
    data: items
  })),
  on(AdmAcmAuthorityListActions.applySuccess, state => ({
    ...state,
    data: state.data.map(({ original, ...modified }) => ({
      ...modified,
      original: modified
    }))
  })),
  on(AdmAcmAuthorityListActions.discard, state => ({
    ...state,
    data: state.data.map(({ original }) => ({
      ...original,
      original
    }))
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;

export const getBsp = ({ requiredFilter }: State) => requiredFilter.bsp;

export const getRequiredFilter = ({ requiredFilter }: State) => requiredFilter;

export const hasChanges = (state: State) => getChanges(state).length > 0;

export const getChanges = ({ data }: State) => data.filter(({ original, ...modified }) => !isEqual(modified, original));
