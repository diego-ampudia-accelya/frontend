import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { AdmAcmAuthorityService } from '../../services/adm-acm-authority.service';
import { AdmAcmAuthorityListActions } from '../actions';
import * as fromAdmAcmAuthority from '../reducers';

import { AdmAcmAuthorityEffects } from './adm-acm-authority.effects';
import { NotificationService } from '~app/shared/services/notification.service';
import { PagedData } from '~app/shared/models/paged-data.model';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FooterButton } from '~app/shared/components';

describe('AdmAcmAuthorityEffects', () => {
  let effects: AdmAcmAuthorityEffects;
  let dataServiceStub: SpyObject<AdmAcmAuthorityService>;
  let actions$: Observable<Action>;
  let changesDialogServiceStub: SpyObject<ChangesDialogService>;
  let notificationServiceStub: SpyObject<NotificationService>;

  beforeEach(waitForAsync(() => {
    dataServiceStub = createSpyObject(AdmAcmAuthorityService);
    notificationServiceStub = createSpyObject(NotificationService);
    changesDialogServiceStub = createSpyObject(ChangesDialogService);

    TestBed.configureTestingModule({
      providers: [
        AdmAcmAuthorityEffects,
        provideMockStore({
          selectors: [
            { selector: fromAdmAcmAuthority.getBsp, value: { id: 1 } },
            { selector: fromAdmAcmAuthority.getListQuery, value: defaultQuery }
          ]
        }),
        provideMockActions(() => actions$),
        { provide: AdmAcmAuthorityService, useValue: dataServiceStub },
        { provide: ChangesDialogService, useValue: changesDialogServiceStub },
        { provide: NotificationService, useValue: notificationServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(AdmAcmAuthorityEffects);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  it('should dispatch searchConfirmed action on search when user has selected Apply', waitForAsync(() => {
    actions$ = of(AdmAcmAuthorityListActions.search({ query: defaultQuery }));
    const expected = AdmAcmAuthorityListActions.searchConfirmed();
    changesDialogServiceStub.confirmUnsavedChanges.and.returnValue(of(FooterButton.Apply));

    let actual: Action;
    effects.search$.subscribe(action => (actual = action));
    expect(actual).toEqual(expected);
  }));

  it('should dispatch searchSuccess after loading the data', waitForAsync(() => {
    const pagedData = { records: [] } as PagedData<any>;
    const expected = AdmAcmAuthorityListActions.searchSuccess({
      data: pagedData
    });
    actions$ = of(AdmAcmAuthorityListActions.searchConfirmed());
    dataServiceStub.find.and.returnValue(of(pagedData));

    let actual: Action;
    effects.searchConfirmed$.subscribe(action => (actual = action));
    expect(actual).toEqual(expected);
  }));

  it('should dispatch searchError when loading the data fails', fakeAsync(() => {
    actions$ = of(AdmAcmAuthorityListActions.searchConfirmed());
    dataServiceStub.find.and.returnValue(throwError(null));
    const expected = AdmAcmAuthorityListActions.searchError();

    let actual: Action;
    effects.searchConfirmed$.subscribe(action => (actual = action));
    tick();
    expect(actual).toEqual(expected);
  }));

  it('should dispatch searchConfirmed actions on changeRequiredFilter', waitForAsync(() => {
    actions$ = of(AdmAcmAuthorityListActions.changeRequiredFilter({ requiredFilter: null }));
    const searchConfirmedAction = AdmAcmAuthorityListActions.searchConfirmed();

    let actual: Action;
    effects.changeRequiredFilter$.subscribe(action => (actual = action));
    expect(actual).toEqual(searchConfirmedAction);
  }));
});
