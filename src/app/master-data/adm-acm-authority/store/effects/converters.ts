import { AdmAcmAuthority, AdmAcmAuthorityViewModel } from '../../models';
import { PagedData } from '~app/shared/models/paged-data.model';

const toViewModel = (dataModel: AdmAcmAuthority): AdmAcmAuthorityViewModel => ({ ...dataModel, original: dataModel });

const toDataModel = (viewModel: AdmAcmAuthorityViewModel): AdmAcmAuthority => {
  const { original, ...dataModel } = viewModel;

  return dataModel;
};

const toViewModels = (pagedData: PagedData<AdmAcmAuthority>): PagedData<AdmAcmAuthorityViewModel> => ({
  ...pagedData,
  records: (pagedData.records || []).map(toViewModel)
});

export const converter = { toViewModel, toDataModel, toViewModels };
