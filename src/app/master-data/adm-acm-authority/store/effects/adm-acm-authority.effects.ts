import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { EMPTY, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { AdmAcmAuthorityService } from '../../services/adm-acm-authority.service';
import { AdmAcmAuthorityListActions } from '../actions';
import * as fromAdmAcmAuthority from '../reducers';

import { converter } from './converters';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services/notification.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class AdmAcmAuthorityEffects {
  changeRequiredFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdmAcmAuthorityListActions.changeRequiredFilter),
      map(() => AdmAcmAuthorityListActions.searchConfirmed())
    )
  );

  search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdmAcmAuthorityListActions.search),
      switchMap(() => this.changesDialogService.confirmUnsavedChanges()),
      map(result =>
        result === FooterButton.Cancel
          ? AdmAcmAuthorityListActions.searchCanceled()
          : AdmAcmAuthorityListActions.searchConfirmed()
      )
    )
  );

  openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdmAcmAuthorityListActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(dialogResult =>
        dialogResult === FooterButton.Apply ? of(AdmAcmAuthorityListActions.searchConfirmed()) : EMPTY
      )
    )
  );

  applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AdmAcmAuthorityListActions.applySuccess),
        tap(() => this.notificationService.showSuccess('LIST.MASTER_DATA.admAcmAuthority.applyChanges.success'))
      ),

    { dispatch: false }
  );

  applyError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AdmAcmAuthorityListActions.applyError),
        tap(response => response.errors.forEach(item => this.notificationService.showError(item.error)))
      ),

    { dispatch: false }
  );

  download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdmAcmAuthorityListActions.download),
      switchMap(() => this.changesDialogService.confirmUnsavedChanges()),
      switchMap(result => (result !== FooterButton.Cancel ? of(AdmAcmAuthorityListActions.downloadConfirmed()) : EMPTY))
    )
  );

  downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AdmAcmAuthorityListActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromAdmAcmAuthority.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.admAcmAuthorityService
          })
        )
      ),
    { dispatch: false }
  );

  searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdmAcmAuthorityListActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromAdmAcmAuthority.getListQuery))),
      switchMap(([_, query]) => this.admAcmAuthorityService.find(query)),
      map(result =>
        AdmAcmAuthorityListActions.searchSuccess({
          data: converter.toViewModels(result)
        })
      ),
      rethrowError(() => AdmAcmAuthorityListActions.searchError())
    )
  );

  constructor(
    private actions$: Actions,
    private admAcmAuthorityService: AdmAcmAuthorityService,
    private store: Store<AppState>,
    private notificationService: NotificationService,
    private changesDialogService: ChangesDialogService,
    private dialogService: DialogService
  ) {}
}
