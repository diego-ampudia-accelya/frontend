import { createAction, props } from '@ngrx/store';

import { AdmAcmAuthorityFilter, AdmAcmAuthorityViewModel } from '../../models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const changeRequiredFilter = createAction(
  '[Adm Acm Authority] Change Required Filter',
  props<{ requiredFilter: AdmAcmAuthorityFilter }>()
);

export const search = createAction('[Adm Acm Authority] Search', props<{ query: DataQuery<AdmAcmAuthorityFilter> }>());
export const searchCanceled = createAction('[Adm Acm Authority] Search Canceled');
export const searchConfirmed = createAction('[Adm Acm Authority] Search Confirmed');
export const searchSuccess = createAction(
  '[Adm Acm Authority] Search Success',
  props<{ data: PagedData<AdmAcmAuthorityViewModel> }>()
);
export const searchError = createAction('[Adm Acm Authority] Search Error');

export const modify = createAction('[Adm Acm Authority] Modify', props<{ items: AdmAcmAuthorityViewModel[] }>());

export const openApplyChanges = createAction('[Adm Acm Authority] Apply Changes');

export const discard = createAction('[Adm Acm Authority] Discard');

export const download = createAction('[Adm Acm Authority] Download');
export const downloadConfirmed = createAction('[Adm Acm Authority] Download Confirmed');

export const applySuccess = createAction('[Adm Acm Authority] Apply Success');
export const applyError = createAction('[Adm Acm Authority] Apply Error', props<{ errors: [{ error: string }] }>());
