export const translations = {
  applyChanges: {
    title: 'LIST.MASTER_DATA.admAcmAuthority.applyChanges.title',
    details: 'LIST.MASTER_DATA.admAcmAuthority.applyChanges.message'
  },
  unsavedChanges: {
    title: 'LIST.MASTER_DATA.admAcmAuthority.unsavedChanges.title',
    details: 'LIST.MASTER_DATA.admAcmAuthority.unsavedChanges.message'
  },
  admGroupTitle: 'LIST.MASTER_DATA.admAcmAuthority.admAuthorityStatus',
  acmGroupTitle: 'LIST.MASTER_DATA.admAcmAuthority.acmAuthorityStatus',
  enabled: 'LIST.MASTER_DATA.admAcmAuthority.enabled',
  disabled: 'LIST.MASTER_DATA.admAcmAuthority.disabled'
};
