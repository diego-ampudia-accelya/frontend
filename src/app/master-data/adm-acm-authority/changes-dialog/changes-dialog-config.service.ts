import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AdmAcmAuthorityFilter, AdmAcmAuthorityViewModel, ViewAs } from '../models';

import { translations } from './translations';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable({ providedIn: 'root' })
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: AdmAcmAuthorityViewModel[],
    requiredFilter: AdmAcmAuthorityFilter,
    question: { title: string; details: string }
  ): ChangesDialogConfig {
    const message = this.translation.translate(question.details, this.getTranslationValues(requiredFilter));

    return {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      changes: this.formatChanges(modifications, requiredFilter.viewAs),
      message
    };
  }

  private formatChanges(modifications: AdmAcmAuthorityViewModel[], viewAs: ViewAs = ViewAs.Airline): ChangeModel[] {
    const translateStatus = (status: boolean) =>
      this.translation.translate(status ? translations.enabled : translations.disabled);
    const getName = (item: AdmAcmAuthorityViewModel) =>
      viewAs === ViewAs.Airline ? item.thirdParty.code : item.airline.localName;

    const admChanges = modifications
      .filter(item => item.hasADM !== item.original.hasADM)
      .map(item => ({
        group: this.translation.translate(translations.admGroupTitle),
        name: getName(item),
        value: translateStatus(item.hasADM),
        originalValue: item.hasADM
      }));
    const acmChanges: ChangeModel[] = modifications
      .filter(item => item.hasACM !== item.original.hasACM)
      .map(item => ({
        group: this.translation.translate(translations.acmGroupTitle),
        name: getName(item),
        value: translateStatus(item.hasACM),
        originalValue: item.hasACM
      }));

    return [...admChanges, ...acmChanges];
  }

  private getTranslationValues(requiredFilter: AdmAcmAuthorityFilter): { bspName: string; name: string } {
    const { viewAs, airlines, thirdParties: thirdParties, bsp } = requiredFilter;

    return {
      bspName: bsp.name,
      name: viewAs === ViewAs.Airline ? airlines[0].name : thirdParties[0].name
    };
  }
}
