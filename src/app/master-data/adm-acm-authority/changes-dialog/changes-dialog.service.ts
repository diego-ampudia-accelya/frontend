import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AdmAcmAuthorityFilter, AdmAcmAuthorityViewModel } from '../models';
import { AdmAcmAuthorityService } from '../services/adm-acm-authority.service';
import { AdmAcmAuthorityListActions } from '../store/actions';
import { converter } from '../store/effects/converters';
import * as fromAdmAcmAuthority from '../store/reducers';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: AdmAcmAuthorityService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return of(null).pipe(
      withLatestFrom(
        this.store.pipe(select(fromAdmAcmAuthority.getChanges)),
        this.store.pipe(select(fromAdmAcmAuthority.getRequiredFilter))
      ),
      switchMap(([_, modifications, requiredFilter]) =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, requiredFilter, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    modifications: AdmAcmAuthorityViewModel[],
    requiredFilter: AdmAcmAuthorityFilter,
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, requiredFilter, question);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(modifications, dialogConfig);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(AdmAcmAuthorityListActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(
    modifications: AdmAcmAuthorityViewModel[],
    dialogConfig: ChangesDialogConfig
  ): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(modifications.map(converter.toDataModel)).pipe(
        tap(() => this.store.dispatch(AdmAcmAuthorityListActions.applySuccess())),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
