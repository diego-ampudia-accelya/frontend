import { TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AdmAcmAuthority, AdmAcmAuthorityFilter, AdmAcmAuthorityViewModel, ViewAs } from '../models';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { ThirdParty } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { ChangesDialogConfig } from '~app/shared/components';
import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

describe('ChangesDialogConfigService', () => {
  let service: ChangesDialogConfigService;
  let translationStub: SpyObject<L10nTranslationService>;
  let mockStore: MockStore<AppState>;

  const bsp = { id: 1, name: 'TRAPPIST-1e' } as Bsp;
  const thirdParty1 = { id: 1, name: 'Third Party 1', code: '1000000' } as ThirdParty;
  const thirdParty2 = { id: 2, name: 'Third Party 2', code: '2000000' } as ThirdParty;
  const airline = { id: 1, localName: 'Airline 1', iataCode: '001' };
  const thirdPartyWithEnabledAuthority: AdmAcmAuthorityViewModel = {
    thirdParty: thirdParty1,
    airline,
    hasACM: true,
    hasADM: true,
    original: { hasACM: false, hasADM: false } as AdmAcmAuthority
  };
  const thirdPartyWithDisabledAuthority: AdmAcmAuthorityViewModel = {
    thirdParty: thirdParty2,
    airline,
    hasACM: false,
    hasADM: false,
    original: { hasACM: true, hasADM: true } as AdmAcmAuthority
  };
  const requiredFilter: AdmAcmAuthorityFilter = {
    bsp,
    viewAs: ViewAs.Airline,
    airlines: [{ id: 1, name: 'Airline 1' } as AirlineSummary]
  };

  beforeEach(waitForAsync(() => {
    translationStub = createSpyObject(L10nTranslationService);
    translationStub.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      providers: [
        ChangesDialogConfigService,
        { provide: L10nTranslationService, useValue: translationStub },
        provideMockStore()
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ChangesDialogConfigService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should build configuration with formatted modifications', () => {
    const modifications: AdmAcmAuthorityViewModel[] = [thirdPartyWithEnabledAuthority, thirdPartyWithDisabledAuthority];
    const expectedConfiguration = jasmine.objectContaining<ChangesDialogConfig>({
      changes: [
        {
          group: translations.admGroupTitle,
          name: thirdParty1.code,
          value: translations.enabled,
          originalValue: thirdPartyWithEnabledAuthority.hasADM
        },
        {
          group: translations.admGroupTitle,
          name: thirdParty2.code,
          value: translations.disabled,
          originalValue: thirdPartyWithDisabledAuthority.hasADM
        },
        {
          group: translations.acmGroupTitle,
          name: thirdParty1.code,
          value: translations.enabled,
          originalValue: thirdPartyWithEnabledAuthority.hasACM
        },
        {
          group: translations.acmGroupTitle,
          name: thirdParty2.code,
          value: translations.disabled,
          originalValue: thirdPartyWithDisabledAuthority.hasACM
        }
      ]
    });

    const configuration = service.build(modifications, requiredFilter, translations.applyChanges);
    expect(configuration).toEqual(expectedConfiguration);
  });
});
