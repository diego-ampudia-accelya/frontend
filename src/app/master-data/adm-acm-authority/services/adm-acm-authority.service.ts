import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { AdmAcmAuthority, AdmAcmAuthorityFilter } from '../models';
import { AdmAcmAuthorityListActions } from '../store/actions';
import { BulkUpdateResponse } from '~app/master-data/models/bulk-update-response.model';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class AdmAcmAuthorityService implements Queryable<AdmAcmAuthority> {
  private baseUrl: string;
  private sortMapper = [
    {
      from: 'airline.iataCode',
      to: 'airlineCode'
    },
    {
      from: 'airline.localName',
      to: 'airlineName'
    },
    {
      from: 'thirdParty.code',
      to: 'thirdPartyCode'
    },
    {
      from: 'thirdParty.organization',
      to: 'thirdPartyOrganization'
    },
    {
      from: 'thirdParty.name',
      to: 'thirdPartyName'
    },
    {
      from: 'thirdParty.active',
      to: 'thirdPartyActive'
    }
  ];

  constructor(
    private appConfiguration: AppConfigurationService,
    private http: HttpClient,
    private store: Store<AppState>
  ) {
    this.baseUrl = `${this.appConfiguration.baseApiPath}/third-parties/acdm-authority`;
  }

  public find(query: DataQuery<AdmAcmAuthorityFilter>): Observable<PagedData<AdmAcmAuthority>> {
    const requestQuery = this.formatQuery(query);
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http.get<PagedData<AdmAcmAuthority>>(url);
  }

  public save(items: AdmAcmAuthority[]): Observable<any> {
    const data = items.map(item => ({
      thirdPartyId: item.thirdParty.id,
      airlineId: item.airline.id,
      hasACM: item.hasACM,
      hasADM: item.hasADM
    }));

    return this.http.put<BulkUpdateResponse<AdmAcmAuthority>[]>(this.baseUrl, data).pipe(
      tap(response => {
        const errorResponse = response.filter(res => !!res.error);

        if (errorResponse.length) {
          const mappedErrorResponse = errorResponse.map(error => ({
            error: `${error.data.thirdParty.name}, ${error.error.messages[0].message}`
          }));
          this.store.dispatch(AdmAcmAuthorityListActions.applyError({ errors: mappedErrorResponse }));
        }
      })
    );
  }

  public download(
    query: DataQuery<AdmAcmAuthority>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, downloadType: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<AdmAcmAuthorityFilter>): RequestQuery {
    const { bsp, airlines = [], thirdParties: thirdParties = [], active, hasADM, hasACM } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        bspCode: bsp?.isoCountryCode,
        airlineCode: airlines.map(a => a.code),
        thirdPartyCode: thirdParties.map(tp => tp.code),
        thirdPartyActive: active,
        hasADM,
        hasACM
      },
      sortBy: query.sortBy && formatSortBy(query.sortBy, this.sortMapper)
    });
  }
}
