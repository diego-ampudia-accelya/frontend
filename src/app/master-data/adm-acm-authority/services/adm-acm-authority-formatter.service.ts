import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { memoize } from 'lodash';

import { AdmAcmAuthorityFilter } from '../models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class AdmAcmAuthorityFormatter implements FilterFormatter {
  public ignoredFilters: Array<keyof AdmAcmAuthorityFilter> = [];

  constructor(private translation: L10nTranslationService) {
    this.formatStatus = memoize(this.formatStatus).bind(this);
  }

  public format(filter: AdmAcmAuthorityFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<AdmAcmAuthorityFilter> = {
      airlines: airlines => airlines.map(airline => `${airline.code} / ${airline.name}`).join(', '),
      thirdParties: thirdParties =>
        thirdParties.map(thirdParty => `${thirdParty.code} / ${thirdParty.name}`).join(', '),
      hasADM: status => `${this.translate('admAuthorityStatus')} - ${this.formatStatus(status)}`,
      hasACM: status => `${this.translate('acmAuthorityStatus')} - ${this.formatStatus(status)}`,
      active: status => `${this.translate('active')} - ${this.formatStatus(status)}`,
      bsp: bsp => `${this.translate('bsp.label')} - ${bsp.isoCountryCode}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public formatStatus(status: boolean): string {
    return this.translate(status ? 'enabled' : 'disabled');
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.MASTER_DATA.admAcmAuthority.' + key);
  }
}
