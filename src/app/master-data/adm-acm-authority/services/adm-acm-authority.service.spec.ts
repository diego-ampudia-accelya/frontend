import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

import { AdmAcmAuthorityFilter } from '../models/adm-acm-authority-filter.model';
import { AdmAcmAuthorityService } from './adm-acm-authority.service';
import { DataQuery } from '~app/shared/components/list-view';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { Bsp } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services';

const bspES: Bsp = {
  id: 1,
  isoCountryCode: 'ES',
  name: 'Spain',
  effectiveFrom: '2020-01-01'
};

describe('admAcmAuthorityService', () => {
  let admAcmAuthorityService: AdmAcmAuthorityService;
  let httpStub: SpyObject<HttpClient>;

  beforeEach(() => {
    const initialState = {
      auth: {
        user: createAirlineUser()
      }
    };

    TestBed.configureTestingModule({
      providers: [
        AdmAcmAuthorityService,
        AppConfigurationService,
        mockProvider(HttpClient),
        provideMockStore({
          initialState
        })
      ]
    });

    admAcmAuthorityService = TestBed.inject(AdmAcmAuthorityService);
    httpStub = TestBed.inject<any>(HttpClient);
  });

  it('should be created', () => {
    expect(admAcmAuthorityService).toBeTruthy();
  });

  it('should fetch the items from the valid endpoint', () => {
    const query: DataQuery<Partial<AdmAcmAuthorityFilter>> = {
      filterBy: {
        bsp: { ...bspES }
      },
      paginateBy: {
        page: 0
      },
      sortBy: []
    };
    admAcmAuthorityService.find(query);

    expect(httpStub.get).toHaveBeenCalledWith('/third-parties/acdm-authority?page=0&bspCode=ES');
  });
});
