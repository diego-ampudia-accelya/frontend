import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { AdmAcmAuthorityFilter } from '../models';
import { AdmAcmAuthorityFormatter } from './adm-acm-authority-formatter.service';
import { ThirdParty } from '~app/master-data/models';
import { AirlineSummary } from '~app/shared/models';

describe('AdmAcmAuthorityFormatterService', () => {
  let formatter: AdmAcmAuthorityFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new AdmAcmAuthorityFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format when hasACM filter is defined', () => {
    const filter: AdmAcmAuthorityFilter = {
      hasACM: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['hasACM'],
        label: 'LIST.MASTER_DATA.admAcmAuthority.acmAuthorityStatus - LIST.MASTER_DATA.admAcmAuthority.enabled'
      }
    ]);
  });

  it('should format when hasADM filter is defined', () => {
    const filter: AdmAcmAuthorityFilter = {
      hasADM: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['hasADM'],
        label: 'LIST.MASTER_DATA.admAcmAuthority.admAuthorityStatus - LIST.MASTER_DATA.admAcmAuthority.enabled'
      }
    ]);
  });

  it('should format when third parties are specified', () => {
    const filter: AdmAcmAuthorityFilter = {
      thirdParties: [
        {
          code: '123',
          name: 'Agent Smith'
        },
        {
          code: '456',
          name: 'Agent Anderson'
        }
      ] as ThirdParty[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['thirdParties'],
        label: '123 / Agent Smith, 456 / Agent Anderson'
      }
    ]);
  });

  it('should format when airlines are specified', () => {
    const filter: AdmAcmAuthorityFilter = {
      airlines: [
        {
          code: '123',
          name: 'Lufthansa'
        },
        {
          code: '456',
          name: 'WizzAir'
        }
      ] as AirlineSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airlines'],
        label: '123 / Lufthansa, 456 / WizzAir'
      }
    ]);
  });

  it('should omit ignored filters', () => {
    const filter: AdmAcmAuthorityFilter = {
      airlines: [
        {
          code: '123',
          name: 'Lufthansa'
        }
      ] as AirlineSummary[],
      hasADM: true,
      hasACM: true
    };

    formatter.ignoredFilters = ['airlines', 'hasADM'];

    expect(formatter.format(filter).length).toEqual(1);
  });

  it('should return empty array when the filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });

  it('should ignore null values', () => {
    const filter: AdmAcmAuthorityFilter = {
      thirdParties: null,
      hasADM: null,
      hasACM: null,
      active: null
    };
    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      unknownProp: 'test'
    };
    expect(formatter.format(filter)).toEqual([]);
  });
});
