import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';

import { FileDescriptorsFormatter } from '../shared/file-descriptors-formatter.service';
import { FileDescriptors, FileDescriptorsFilters } from '../shared/file-descriptors.models';
import { FileDescriptorsActions } from '../store/actions';
import * as fromFileDescriptors from '../store/reducers';
import * as fromFileDescriptorsList from '../store/reducers/file-descriptors.reducer';
import { FileDescriptorsListComponent } from './file-descriptors-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DataQuery, ListViewComponent } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Bsp } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { TranslatePipeMock } from '~app/test';

describe('FileDescriptorsListComponent', () => {
  let component: FileDescriptorsListComponent;
  let fixture: ComponentFixture<FileDescriptorsListComponent>;
  let mockStore: MockStore;

  const permissionServiceMock = jasmine.createSpyObj<PermissionsService>('PermissionsService', ['hasPermission']);

  permissionServiceMock.hasPermission.and.returnValue(true);

  const translationSpy = createSpyObject(L10nTranslationService);

  const initialState = fromFileDescriptorsList.initialState;

  const file: FileDescriptors = {
    id: 1,
    descriptor: 'descriptor',
    description: 'description',
    readonly: false
  };

  const mockBsp: Bsp = {
    id: 1,
    isoCountryCode: 'ES',
    name: 'Spain',
    effectiveFrom: '2021-05-10',
    effectiveTo: '2021-08-22',
    version: 1
  };

  const mockQuery: DataQuery<FileDescriptorsFilters> = {
    ...defaultQuery,
    filterBy: {
      bspId: 1,
      descriptor: '09',
      description: 'Description'
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        FileDescriptorsListComponent,
        TranslatePipeMock,
        MockComponents(InputComponent, ListViewComponent, GridTableComponent)
      ],
      providers: [
        FileDescriptorsFormatter,
        FormBuilder,
        { provide: PermissionsService, useValue: permissionServiceMock },
        provideMockStore({
          initialState: {
            auth: { user: { bsps: [mockBsp] } }
          },
          selectors: [
            { selector: fromFileDescriptors.getListQuery, value: initialState.query },
            { selector: fromFileDescriptors.getListData, value: initialState.data },
            { selector: fromFileDescriptors.getListLoading, value: initialState.loading }
          ]
        }),
        { provide: L10nTranslationService, useValue: translationSpy }
      ],
      imports: [ReactiveFormsModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileDescriptorsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('createFileDescriptor should call dispatch with FileDescriptorsActions.openAdd', () => {
    component.createFileDescriptor();

    expect(mockStore.dispatch).toHaveBeenCalledWith(FileDescriptorsActions.openAdd());
  });

  it('onDownload should call dispatch with FileDescriptorsActions.download', () => {
    component.onDownload();

    expect(mockStore.dispatch).toHaveBeenCalledWith(FileDescriptorsActions.download());
  });

  it('onQueryChanged should call dispatchSearch', () => {
    component.onQueryChanged(mockQuery);

    expect(mockStore.dispatch).toHaveBeenCalledWith(FileDescriptorsActions.search({ query: mockQuery }));
  });

  describe('onActionMenuClick', () => {
    it('should set correct actions with disabled false', () => {
      permissionServiceMock.hasPermission.and.returnValue(true);
      file.readonly = false;
      const expectedActions = [
        { action: GridTableActionType.Edit, disabled: false },
        { action: GridTableActionType.Delete, disabled: false }
      ];

      component.onActionMenuClick(file);

      expect(component.actions).toEqual(expectedActions);
    });

    it('should set correct actions with disabled true when has no permissions', () => {
      file.readonly = false;
      permissionServiceMock.hasPermission.and.returnValue(false);
      const expectedActions = [
        { action: GridTableActionType.Edit, disabled: true },
        { action: GridTableActionType.Delete, disabled: true }
      ];

      component.onActionMenuClick(file);

      expect(component.actions).toEqual(expectedActions);
    });

    it('should set correct actions with disabled true when file is readonly', () => {
      permissionServiceMock.hasPermission.and.returnValue(true);
      file.readonly = true;
      const expectedActions = [
        { action: GridTableActionType.Edit, disabled: true },
        { action: GridTableActionType.Delete, disabled: true }
      ];

      component.onActionMenuClick(file);

      expect(component.actions).toEqual(expectedActions);
    });
  });

  describe('onActionClick', () => {
    it('should dispatch openApplyChanges action', () => {
      const action: GridTableAction = { actionType: GridTableActionType.Edit, methodName: null };

      component.onActionClick({ action, row: file });

      expect(mockStore.dispatch).toHaveBeenCalledWith(FileDescriptorsActions.openApplyChanges({ value: file }));
    });

    it('should dispatch remove action', () => {
      const action: GridTableAction = { actionType: GridTableActionType.Delete, methodName: null };

      component.onActionClick({ action, row: file });

      expect(mockStore.dispatch).toHaveBeenCalledWith(FileDescriptorsActions.remove({ value: file }));
    });
  });
});
