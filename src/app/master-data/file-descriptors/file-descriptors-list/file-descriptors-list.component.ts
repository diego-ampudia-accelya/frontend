import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { FileDescriptorsFormatter } from '../shared/file-descriptors-formatter.service';
import { FileDescriptors, FileDescriptorsFilters } from '../shared/file-descriptors.models';
import { FileDescriptorsActions } from '../store/actions';
import * as fromFileDescriptors from '../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-file-descriptors',
  templateUrl: './file-descriptors-list.component.html',
  providers: [FileDescriptorsFormatter]
})
export class FileDescriptorsListComponent implements OnInit, OnDestroy {
  public data$ = this.store.pipe(select(fromFileDescriptors.getListData));
  public loading$ = this.store.pipe(select(fromFileDescriptors.getListLoading));
  public query$ = this.store.pipe(select(fromFileDescriptors.getListQuery));
  public hasData$ = this.store.pipe(select(fromFileDescriptors.hasData));

  public downloadQuery: Partial<DataQuery>;

  public columns: Array<GridColumn>;
  public actions: Array<{ action: GridTableActionType; disabled: boolean }>;

  public searchForm: FormGroup;

  public canCreateFileDescriptor = this.permissionsService.hasPermission(Permissions.createFileDescriptors);
  public customLabels = { create: 'LIST.MASTER_DATA.fileDescriptors.createButtonLabel' };

  private destroy$ = new Subject();

  constructor(
    public fileDescriptorsFormatter: FileDescriptorsFormatter,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.initializeColumns();

    this.dispatchSearch();
  }

  public createFileDescriptor(): void {
    this.store.dispatch(FileDescriptorsActions.openAdd());
  }

  public onDownload(): void {
    this.store.dispatch(FileDescriptorsActions.download());
  }

  public onQueryChanged(query: DataQuery<FileDescriptorsFilters>): void {
    this.store.dispatch(FileDescriptorsActions.search({ query }));
  }

  public onActionMenuClick(file: FileDescriptors): void {
    const hasUpdatePermission = this.permissionsService.hasPermission(Permissions.updateFileDescriptors);
    const hasRemovePermission = this.permissionsService.hasPermission(Permissions.removeFileDescriptors);
    const isReadOnlyFile = file.readonly;
    this.actions = [
      { action: GridTableActionType.Edit, disabled: !hasUpdatePermission || isReadOnlyFile },
      { action: GridTableActionType.Delete, disabled: !hasRemovePermission || isReadOnlyFile }
    ];
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: FileDescriptors }): void {
    if (action.actionType === GridTableActionType.Edit) {
      this.store.dispatch(FileDescriptorsActions.openApplyChanges({ value: row }));

      return;
    }

    if (action.actionType === GridTableActionType.Delete) {
      this.store.dispatch(FileDescriptorsActions.remove({ value: row }));
    }
  }

  private initializeColumns(): void {
    this.columns = this.buildColumns();
  }

  private dispatchSearch(): void {
    this.query$.pipe(first()).subscribe(query => {
      this.store.dispatch(FileDescriptorsActions.search({ query }));
    });
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: 'LIST.MASTER_DATA.fileDescriptors.filters.descriptor.label',
        prop: 'descriptor',
        flexGrow: 0.3
      },
      {
        name: 'LIST.MASTER_DATA.fileDescriptors.filters.description.label',
        prop: 'description',
        flexGrow: 1
      }
    ];
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      descriptor: [],
      description: []
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
