import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { FileDescriptorsFormatter } from './file-descriptors-formatter.service';
import { FileDescriptorsFilters } from './file-descriptors.models';

describe('FileDescriptorsFormatter', () => {
  let formatter: FileDescriptorsFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new FileDescriptorsFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format descriptor if they exist', () => {
    const filter: FileDescriptorsFilters = {
      descriptor: '01'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['descriptor'],
        label: 'LIST.MASTER_DATA.fileDescriptors.filters.descriptor.label - 01'
      }
    ]);
  });

  it('should format description if they exist', () => {
    const filter: FileDescriptorsFilters = {
      description: 'Description'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['description'],
        label: 'LIST.MASTER_DATA.fileDescriptors.filters.description.label - Description'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
