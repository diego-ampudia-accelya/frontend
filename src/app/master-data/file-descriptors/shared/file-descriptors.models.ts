export interface FileDescriptors {
  id: number;
  descriptor: string;
  description: string;
  readonly: boolean;
  bspId?: number;
}

export interface FileDescriptorsFilters {
  descriptor?: string;
  description?: string;
  bspId?: number;
}
