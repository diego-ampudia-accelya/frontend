import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { FileDescriptorsFilters } from './file-descriptors.models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class FileDescriptorsFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: FileDescriptorsFilters): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<FileDescriptorsFilters>> = {
      descriptor: descriptor => `${this.translate('descriptor')} - ${descriptor}`,
      description: description => `${this.translate('description')} - ${description}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`LIST.MASTER_DATA.fileDescriptors.filters.${key}.label`);
  }
}
