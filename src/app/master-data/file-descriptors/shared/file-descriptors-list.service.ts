import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { FileDescriptors, FileDescriptorsFilters } from './file-descriptors.models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class FileDescriptorsListService implements Queryable<FileDescriptors> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/file-descriptors`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<FileDescriptorsFilters>): Observable<PagedData<FileDescriptors>> {
    const requestQuery = RequestQuery.fromDataQuery(query);

    return this.http.get<PagedData<FileDescriptors>>(this.baseUrl + requestQuery.getQueryString());
  }

  public save(item: FileDescriptors): Observable<FileDescriptors> {
    const url = `${this.baseUrl}/${item.id}`;

    return this.http.put<FileDescriptors>(url, item);
  }

  public create(item: FileDescriptorsFilters): Observable<FileDescriptors> {
    return this.http.post<FileDescriptors>(this.baseUrl, item);
  }

  public delete(item: FileDescriptors): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${item.id}`);
  }

  public download(
    query: DataQuery<FileDescriptorsFilters>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = RequestQuery.fromDataQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
