import { FileDescriptorsFilters } from '../../shared/file-descriptors.models';
import { DataQuery } from '~app/shared/components/list-view';
import { Bsp } from '~app/shared/models/bsp.model';

export function getQueryBsp(
  query: DataQuery<FileDescriptorsFilters>,
  userBsp: Bsp[]
): DataQuery<FileDescriptorsFilters> {
  return {
    ...query,
    filterBy: {
      ...query?.filterBy,
      bspId: userBsp[0].id
    }
  };
}

export function getValueBspId(item: FileDescriptorsFilters, userBsp: Bsp[]): FileDescriptorsFilters {
  return {
    ...item,
    bspId: userBsp[0].id
  };
}
