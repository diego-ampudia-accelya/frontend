import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { DeleteDialogService } from '../../dialogs/delete-dialog/delete-dialog.service';
import { ChangesDialogService } from '../../dialogs/file-descriptors-changes-dialog/services/changes-dialog.service';
import { FileDescriptorsListService } from '../../shared/file-descriptors-list.service';
import { FileDescriptorsActions } from '../actions';
import * as fromFileDescriptors from '../reducers';

import { getQueryBsp, getValueBspId } from './file-descriptors.helper';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class FileDescriptorsEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private dataService: FileDescriptorsListService,
    private dialogService: DialogService,
    private changesDialogService: ChangesDialogService,
    private removeDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FileDescriptorsActions.search),
      switchMap(() => of(FileDescriptorsActions.searchConfirmed()))
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FileDescriptorsActions.searchConfirmed),
      withLatestFrom(
        this.store.pipe(select(fromFileDescriptors.getListQuery)),
        this.store.pipe(select(fromAuth.getUserBsps))
      ),
      switchMap(([_, query, userBsp]) =>
        this.dataService.find(getQueryBsp(query, userBsp)).pipe(
          map(data => FileDescriptorsActions.searchSuccess({ data })),
          rethrowError(() => FileDescriptorsActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FileDescriptorsActions.download),
      switchMap(result => (result ? of(FileDescriptorsActions.downloadConfirmed()) : EMPTY))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FileDescriptorsActions.downloadConfirmed),
        withLatestFrom(
          this.store.pipe(select(fromFileDescriptors.getListQuery)),
          this.store.pipe(select(fromAuth.getUserBsps))
        ),
        switchMap(([_, query, userBsp]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: getQueryBsp(query, userBsp)
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  public remove$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FileDescriptorsActions.remove),
        switchMap(({ value }) => this.removeDialogService.open(value))
      ),
    { dispatch: false }
  );

  public removeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FileDescriptorsActions.removeSuccess),
        tap(({ value }) => {
          const deleteMessage = this.translationService.translate(
            'LIST.MASTER_DATA.fileDescriptors.dialog.delete.deleteMessage',
            {
              item: value.descriptor
            }
          );
          this.notificationService.showSuccess(deleteMessage);

          return this.store.dispatch(FileDescriptorsActions.searchConfirmed());
        })
      ),

    { dispatch: false }
  );

  public $openApplyChanges = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FileDescriptorsActions.openApplyChanges),
        switchMap(({ value }) => this.changesDialogService.open(value))
      ),
    { dispatch: false }
  );

  public applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FileDescriptorsActions.applyChanges),
      switchMap(({ value }) =>
        this.changesDialogService.edit(value).pipe(
          map(() => FileDescriptorsActions.applyChangesSuccess({ value })),
          rethrowError(error => FileDescriptorsActions.applyChangesError(error))
        )
      )
    )
  );

  public applyChangesSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FileDescriptorsActions.applyChangesSuccess),
        tap(({ value }) => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(
            'LIST.MASTER_DATA.fileDescriptors.dialog.edit.successMessage',
            {
              item: value.descriptor
            }
          );
          this.notificationService.showSuccess(successMessage);

          return this.store.dispatch(FileDescriptorsActions.searchConfirmed());
        })
      ),
    { dispatch: false }
  );

  public $openAdd = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FileDescriptorsActions.openAdd),
        switchMap(() => this.changesDialogService.open())
      ),
    { dispatch: false }
  );

  public add$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FileDescriptorsActions.add),
      withLatestFrom(this.store.pipe(select(fromAuth.getUserBsps))),
      switchMap(([{ value }, userBsp]) =>
        this.changesDialogService.create(getValueBspId(value, userBsp)).pipe(
          map(() => FileDescriptorsActions.addSuccess()),
          rethrowError(error => FileDescriptorsActions.addError(error))
        )
      )
    )
  );

  public addSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FileDescriptorsActions.addSuccess),
        tap(() => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(
            'LIST.MASTER_DATA.fileDescriptors.dialog.create.createMessage'
          );
          this.notificationService.showSuccess(successMessage);

          return this.store.dispatch(FileDescriptorsActions.searchConfirmed());
        })
      ),
    { dispatch: false }
  );
}
