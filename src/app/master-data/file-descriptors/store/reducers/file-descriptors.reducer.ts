import { createReducer, on } from '@ngrx/store';

import { FileDescriptors, FileDescriptorsFilters } from '../../shared/file-descriptors.models';
import { FileDescriptorsActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ResponseErrorBE } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<FileDescriptorsFilters>;
  previousQuery: DataQuery<FileDescriptorsFilters>;
  data: FileDescriptors[];
  loading: boolean;
  errors: { error: ResponseErrorBE };
}

export const initialState: State = {
  query: defaultQuery,
  previousQuery: null,
  data: [],
  loading: false,
  errors: null
};

const loadData = (state: State, data: PagedData<FileDescriptors>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

export const reducer = createReducer(
  initialState,
  on(FileDescriptorsActions.openApplyChanges, state => ({ ...state })),
  on(FileDescriptorsActions.openAdd, state => ({ ...state })),
  on(FileDescriptorsActions.search, (state, { query = state.query }) => ({
    ...state,
    previousQuery: state.query,
    query
  })),
  on(FileDescriptorsActions.searchConfirmed, state => ({
    ...state,
    previousQuery: null,
    loading: true
  })),
  on(FileDescriptorsActions.searchCancelled, state => ({
    ...state,
    query: state.previousQuery,
    previousQuery: null
  })),
  on(FileDescriptorsActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(FileDescriptorsActions.searchError, state => ({
    ...state,
    loading: false
  })),
  on(FileDescriptorsActions.add, state => ({ ...state, loading: true })),
  on(FileDescriptorsActions.addSuccess, state => ({ ...state, loading: false })),
  on(FileDescriptorsActions.applyChangesError, FileDescriptorsActions.addError, (state, { error }) => ({
    ...state,
    loading: false,
    errors: error
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;
