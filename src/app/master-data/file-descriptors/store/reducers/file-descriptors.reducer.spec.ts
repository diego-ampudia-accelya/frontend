import { FileDescriptors, FileDescriptorsFilters } from '../../shared/file-descriptors.models';
import { FileDescriptorsActions } from '../actions';
import * as fromFileDescriptors from './file-descriptors.reducer';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

describe('File Descriptors Reducer', () => {
  const query: DataQuery<FileDescriptorsFilters> = {
    ...defaultQuery,
    filterBy: {
      bspId: 1,
      descriptor: '09',
      description: 'Description'
    }
  };

  const fileDescriptors: FileDescriptors[] = [
    {
      id: 123,
      descriptor: '09',
      description: 'Description',
      readonly: false
    },
    {
      id: 1234,
      descriptor: '02',
      description: 'Description 1',
      readonly: false
    }
  ];

  it('should update state correctly on SEARCH action', () => {
    const action = FileDescriptorsActions.search({ query });
    const state = fromFileDescriptors.reducer(fromFileDescriptors.initialState, action);
    const geQuery = fromFileDescriptors.getQuery(state);

    expect(state.query).toEqual(query);
    expect(state.previousQuery).toEqual(defaultQuery);
    expect(geQuery).toEqual(state.query);
  });

  it('should update state correctly on SEARCH CONFIRMED action', () => {
    const initialState: fromFileDescriptors.State = {
      ...fromFileDescriptors.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = FileDescriptorsActions.searchConfirmed();
    const state = fromFileDescriptors.reducer(initialState, action);

    expect(state.previousQuery).toBeNull();
    expect(state.loading).toBe(true);
  });

  it('should update state correctly on SEARCH CANCELLED action', () => {
    const initialState: fromFileDescriptors.State = {
      ...fromFileDescriptors.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = FileDescriptorsActions.searchCancelled();
    const state = fromFileDescriptors.reducer(initialState, action);

    expect(state.query).toEqual(defaultQuery);
    expect(state.previousQuery).toBeNull();
  });

  it('should update state correctly on SEARCH SUCCESS action', () => {
    const data: PagedData<FileDescriptors> = {
      records: fileDescriptors,
      pageSize: 20,
      total: fileDescriptors.length,
      totalPages: 1,
      pageNumber: 0
    };

    const action = FileDescriptorsActions.searchSuccess({ data });
    const state = fromFileDescriptors.reducer(fromFileDescriptors.initialState, action);
    const getData = fromFileDescriptors.getData(state);

    expect(state.loading).toBeFalsy();
    expect(state.data).toEqual(fileDescriptors);
    expect(state.query).toEqual(
      jasmine.objectContaining({ paginateBy: { page: 0, size: 20, totalElements: fileDescriptors.length } })
    );
    expect(getData).toEqual(state.data);
  });

  it('should set loading false on SEARCH ERROR action', () => {
    const action = FileDescriptorsActions.searchError();
    const state = fromFileDescriptors.reducer(fromFileDescriptors.initialState, action);

    expect(state.loading).toBeFalsy();
  });

  it('should set loading true on APPLY CHANGE action', () => {
    const action = FileDescriptorsActions.applyChanges({ value: fileDescriptors[0] });
    const state = fromFileDescriptors.reducer(fromFileDescriptors.initialState, action);
    const getLoading = fromFileDescriptors.getLoading(state);

    expect(state.loading).toBeFalsy();
    expect(getLoading).toEqual(state.loading);
  });

  it('should set loading false on APPLY CHANGE SUCCESS action', () => {
    const action = FileDescriptorsActions.applyChangesSuccess({ value: fileDescriptors[0] });
    const state = fromFileDescriptors.reducer(fromFileDescriptors.initialState, action);

    expect(state.loading).toBeFalsy();
  });

  it('should not change state on OPEN APPLY CHANGE action', () => {
    const action = FileDescriptorsActions.openApplyChanges({ value: fileDescriptors[0] });
    const state = fromFileDescriptors.reducer(fromFileDescriptors.initialState, action);

    expect(state).toEqual(state);
  });

  it('should set error on OPEN APPLY CHANGE ERROR action', () => {
    const action = FileDescriptorsActions.applyChangesError({ error: null });
    const state = fromFileDescriptors.reducer(fromFileDescriptors.initialState, action);

    expect(state.errors).toEqual(null);
  });
});
