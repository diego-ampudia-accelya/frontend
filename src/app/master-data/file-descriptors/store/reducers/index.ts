import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './file-descriptors.reducer';
import { AppState } from '~app/reducers';

export const fileDescriptorsFeatureKey = 'fileDescriptors';

export interface State extends AppState {
  [fileDescriptorsFeatureKey]: FileDescriptorsState;
}

export interface FileDescriptorsState {
  [fromList.key]: fromList.State;
}

export function reducers(state: FileDescriptorsState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getfileDescriptors = createFeatureSelector<State, FileDescriptorsState>(fileDescriptorsFeatureKey);

export const getList = createSelector(getfileDescriptors, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getListQuery = createSelector(getList, fromList.getQuery);
