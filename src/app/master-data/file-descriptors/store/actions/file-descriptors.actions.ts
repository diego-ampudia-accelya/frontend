import { createAction, props } from '@ngrx/store';

import { FileDescriptors, FileDescriptorsFilters } from '../../shared/file-descriptors.models';
import { DataQuery } from '~app/shared/components/list-view';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { PagedData } from '~app/shared/models/paged-data.model';

export const search = createAction('[File Descriptors] Search', props<{ query?: DataQuery<FileDescriptorsFilters> }>());
export const searchConfirmed = createAction('[File Descriptors] Search Confirmed');
export const searchCancelled = createAction('[File Descriptors] Search Cancelled');
export const searchSuccess = createAction(
  '[File Descriptors] Search Success',
  props<{ data: PagedData<FileDescriptors> }>()
);
export const searchError = createAction('[File Descriptors] Search Error');

export const download = createAction('[File Descriptors] Download');
export const downloadConfirmed = createAction('[File Descriptors] Download Confirmed');

export const openApplyChanges = createAction(
  '[File Descriptors] Open Apply Changes',
  props<{ value?: FileDescriptors }>()
);
export const applyChanges = createAction('[File Descriptors] Apply Changes', props<{ value: FileDescriptors }>());
export const applyChangesSuccess = createAction(
  '[File Descriptors] Apply Changes Success',
  props<{ value: FileDescriptors }>()
);
export const applyChangesError = createAction('[File Descriptors] Apply Changes Error', props<{ error }>());

export const remove = createAction('[File Descriptors] Remove', props<{ value: FileDescriptors }>());
export const removeSuccess = createAction('[File Descriptors] Remove Success', props<{ value: FileDescriptors }>());
export const removeError = createAction('[File Descriptors] Remove Error', props<{ error: ResponseErrorBE }>());

export const openAdd = createAction('[File Descriptors] Open Add');
export const add = createAction('[File Descriptors] Add', props<{ value: FileDescriptorsFilters }>());
export const addSuccess = createAction('[File Descriptors] Add Success');
export const addError = createAction('[File Descriptors] Add Error', props<{ error }>());
