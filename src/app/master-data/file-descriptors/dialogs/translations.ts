export const translations = {
  delete: {
    title: 'LIST.MASTER_DATA.fileDescriptors.dialog.delete.title',
    details: 'LIST.MASTER_DATA.fileDescriptors.dialog.delete.message'
  },
  edit: {
    title: 'LIST.MASTER_DATA.fileDescriptors.dialog.edit.title',
    details: 'LIST.MASTER_DATA.fileDescriptors.dialog.edit.message'
  },
  create: {
    title: 'LIST.MASTER_DATA.fileDescriptors.dialog.create.title',
    details: 'LIST.MASTER_DATA.fileDescriptors.dialog.create.message'
  }
};
