import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'rxjs';

import { FileDescriptors } from '../../shared/file-descriptors.models';

import { DeleteDialogConfigService } from './delete-dialog-config.service';
import { FooterButton } from '~app/shared/components';

describe('DeleteDialogConfigService', () => {
  let service: DeleteDialogConfigService;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const item: FileDescriptors = {
    id: 1,
    descriptor: 'Krzysztof',
    description: 'Kieślowski',
    readonly: false
  };

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    service = new DeleteDialogConfigService(translationServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should format item changes correctly', () => {
    const format = service['formatChanges'](item);

    service['formatChanges'](item);

    expect(format).toEqual([{ group: '', name: '', value: 'Krzysztof - Kieślowski' }]);
  });

  it('should build changes dialog configuration', () => {
    const question = { title: 'title', details: 'details' };

    service.build(item, question);

    expect(service.build(item, question)).toEqual({
      data: { title: question.title, footerButtonsType: [{ type: FooterButton.Delete }] },
      changes: [{ group: '', name: '', value: 'Krzysztof - Kieślowski' }],
      message: question.details
    });
  });
});
