import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { FileDescriptors } from '../../shared/file-descriptors.models';
import { ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable()
export class DeleteDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(item: FileDescriptors, question: { title: string; details: string }): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Delete }]
      },
      changes: this.formatChanges(item),
      message
    };
  }

  private formatChanges(item: FileDescriptors): ChangeModel[] {
    return [
      {
        group: '',
        name: '',
        value: `${item.descriptor} - ${item.description}`
      }
    ];
  }
}
