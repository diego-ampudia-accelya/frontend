import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { FileDescriptors } from '~app/master-data/file-descriptors/shared/file-descriptors.models';
import { FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(question: { title: string; details: string }, item?: FileDescriptors): any {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: item ? [{ type: FooterButton.Update }] : [{ type: FooterButton.Create }]
      },
      item,
      message
    };
  }
}
