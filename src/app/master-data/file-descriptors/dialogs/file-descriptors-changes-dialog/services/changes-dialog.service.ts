import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { translations } from '../../translations';
import { FileDescriptorsChangeDialogComponent } from '../file-descriptors-changes-dialog/file-descriptors-changes-dialog.component';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { FileDescriptorsListService } from '~app/master-data/file-descriptors/shared/file-descriptors-list.service';
import {
  FileDescriptors,
  FileDescriptorsFilters
} from '~app/master-data/file-descriptors/shared/file-descriptors.models';
import { DialogService } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private dataService: FileDescriptorsListService
  ) {}

  public open(item?: FileDescriptors): Observable<any> {
    const buildTranslations = item ? translations.edit : translations.create;
    const dialogConfig = this.configBuilder.build(buildTranslations, item);

    return this.dialogService.open(FileDescriptorsChangeDialogComponent, dialogConfig);
  }

  public edit(item: FileDescriptors): Observable<FileDescriptors> {
    return this.dataService.save(item);
  }

  public create(item: FileDescriptorsFilters): Observable<FileDescriptors> {
    return this.dataService.create(item);
  }
}
