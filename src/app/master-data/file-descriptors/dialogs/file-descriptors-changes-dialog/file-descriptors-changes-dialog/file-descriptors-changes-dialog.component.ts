import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import {
  FileDescriptors,
  FileDescriptorsFilters
} from '~app/master-data/file-descriptors/shared/file-descriptors.models';
import { FileDescriptorsActions } from '~app/master-data/file-descriptors/store/actions';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-file-descriptors-changes-dialog',
  templateUrl: './file-descriptors-changes-dialog.component.html',
  styleUrls: ['./file-descriptors-changes-dialog.component.scss']
})
export class FileDescriptorsChangeDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;
  public descriptionControl: FormControl;

  public isEditMode = false;

  private descriptorControl: FormControl;
  private updateButton: ModalAction;
  private createButton: ModalAction;
  private item: FileDescriptors;

  private destroy$ = new Subject();

  private updateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Update),
    takeUntil(this.destroy$)
  );

  private createButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Create),
    takeUntil(this.destroy$)
  );

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    public store: Store<AppState>
  ) {}

  public ngOnInit(): void {
    this.buildForm();
    this.initializeButtons();
    this.setItemsFeatures();
    this.initializeFormListeners();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.updateButton) {
        this.updateButton.isDisabled = status !== 'VALID';
      }
      if (this.createButton) {
        this.createButton.isDisabled = status !== 'VALID';
      }
    });
  }

  private buildForm(): void {
    this.descriptionControl = new FormControl(null, Validators.required);
    this.descriptorControl = new FormControl(null, Validators.required);

    this.form = this.formBuilder.group({
      description: this.descriptionControl,
      descriptor: this.descriptorControl
    });
  }

  private initializeButtons(): void {
    this.updateButton = this.config.data.buttons.find(button => button.type === FooterButton.Update);
    this.createButton = this.config.data.buttons.find(button => button.type === FooterButton.Create);

    if (this.updateButton) {
      this.updateButton.isDisabled = true;
    }
    if (this.createButton) {
      this.createButton.isDisabled = true;
    }
  }

  private initializeFormListeners(): void {
    this.updateButtonClick$.subscribe(() => {
      if (this.item.description !== this.descriptionControl.value) {
        const valueToDispatch: FileDescriptors = { ...this.item, description: this.descriptionControl.value };
        this.store.dispatch(FileDescriptorsActions.applyChanges({ value: valueToDispatch }));
      }
    });

    this.createButtonClick$.subscribe(() => {
      const valueToDispatch: FileDescriptorsFilters = {
        descriptor: this.descriptorControl.value,
        description: this.descriptionControl.value
      };
      this.store.dispatch(FileDescriptorsActions.add({ value: valueToDispatch }));
    });
  }

  private setItemsFeatures() {
    this.item = this.config.item;
    this.isEditMode = !!this.item;

    if (this.item) {
      this.descriptionControl.patchValue(this.item.description);
      this.descriptorControl.patchValue(this.item.descriptor);
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
