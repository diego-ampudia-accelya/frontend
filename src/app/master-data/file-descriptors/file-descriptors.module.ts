import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { DeleteDialogConfigService } from './dialogs/delete-dialog/delete-dialog-config.service';
import { DeleteDialogService } from './dialogs/delete-dialog/delete-dialog.service';
import { FileDescriptorsChangeDialogComponent } from './dialogs/file-descriptors-changes-dialog/file-descriptors-changes-dialog/file-descriptors-changes-dialog.component';
import { ChangesDialogConfigService } from './dialogs/file-descriptors-changes-dialog/services/changes-dialog-config.service';
import { ChangesDialogService } from './dialogs/file-descriptors-changes-dialog/services/changes-dialog.service';
import { FileDescriptorsListComponent } from './file-descriptors-list/file-descriptors-list.component';
import { FileDescriptorsRoutingModule } from './file-descriptors-routing.module';
import { FileDescriptorsListService } from './shared/file-descriptors-list.service';
import { FileDescriptorsEffects } from './store/effects/file-descriptors.effects';
import * as fromFileDescriptors from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [FileDescriptorsListComponent, FileDescriptorsChangeDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    FileDescriptorsRoutingModule,
    L10nTranslationModule,
    StoreModule.forFeature(fromFileDescriptors.fileDescriptorsFeatureKey, fromFileDescriptors.reducers),
    EffectsModule.forFeature([FileDescriptorsEffects])
  ],
  providers: [
    FileDescriptorsListService,
    DeleteDialogService,
    DeleteDialogConfigService,
    ChangesDialogService,
    ChangesDialogConfigService
  ]
})
export class FileDescriptorsModule {}
