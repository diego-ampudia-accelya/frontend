import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FileDescriptorsListComponent } from './file-descriptors-list/file-descriptors-list.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: FileDescriptorsListComponent,
    data: {
      tab: ROUTES.FILE_DESCRIPTORS
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileDescriptorsRoutingModule {}
