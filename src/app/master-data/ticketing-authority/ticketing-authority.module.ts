import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { TicketingAuthorityEffects } from './store/effects/ticketing-authority.effects';
import * as fromTicketingAuthority from './store/reducers';
import { TicketingAuthorityComponent } from './ticketing-authority/ticketing-authority.component';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [TicketingAuthorityComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromTicketingAuthority.ticketingAuthorityFeatureKey, fromTicketingAuthority.reducers),
    EffectsModule.forFeature([TicketingAuthorityEffects])
  ],
  exports: [TicketingAuthorityComponent],
  providers: [ChangesDialogService]
})
export class TicketingAuthorityModule {}
