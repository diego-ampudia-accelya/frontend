import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { TicketingAuthority, TicketingAuthorityFilter, ViewType } from '../models';
import { convertToDto } from './dto-converter';
import { AgentGroup } from '~app/master-data/models/agent-group.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import {
  downloadRequestOptions,
  formatDownloadResponse,
  toValueLabelObjectDictionaryDesignator
} from '~app/shared/helpers';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';
import { AirlineDictionaryService } from '~app/shared/services/dictionary/airline-dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class TicketingAuthorityService implements Queryable<TicketingAuthority> {
  private baseUrl: string;

  private sortMapper = [
    {
      from: 'agent.name',
      to: 'agentName'
    },
    {
      from: 'agent.iataCode',
      to: 'agentCode'
    },
    {
      from: 'airline.name',
      to: 'airlineName'
    },
    {
      from: 'airline.iataCode',
      to: 'airlineCode'
    },
    {
      from: 'ticketingAuthority',
      to: 'ticketingAuthority'
    },
    {
      from: 'airline.abrev',
      to: 'airlineAbrev'
    },
    {
      from: 'agent.status',
      to: 'agentStatus'
    },
    {
      from: 'agent.address',
      to: 'agentAddress'
    },
    {
      from: 'agent.city',
      to: 'agentCity'
    }
  ];

  constructor(
    private http: HttpClient,
    private agentService: AgentDictionaryService,
    private airlineService: AirlineDictionaryService,
    private appConfiguration: AppConfigurationService
  ) {
    this.baseUrl = `${this.appConfiguration.baseApiPath}/ta-management`;
  }

  public find(query: DataQuery<TicketingAuthorityFilter>): Observable<PagedData<TicketingAuthority>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<TicketingAuthority>>(`${this.baseUrl}/tas` + requestQuery.getQueryString());
  }

  public saveMany(items: TicketingAuthority[]): Observable<any> {
    return this.save(items);
  }

  public saveAll(query: DataQuery<TicketingAuthorityFilter>): Observable<any> {
    const formattedQuery = this.formatQuery(query);

    const bodyAuthorize = !query.filterBy.ticketingAuthority;

    const url = `${this.baseUrl}/tas/all` + formattedQuery.getQueryString();

    return this.http.put(url, { authorize: bodyAuthorize });
  }

  public save(item: TicketingAuthority[]): Observable<any> {
    const dto = convertToDto(item);
    const url = `${this.baseUrl}/tas`;

    return this.http.put(url, dto);
  }

  public download(
    query: DataQuery<TicketingAuthorityFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, downloadType: downloadFormat };
    const url = `${this.baseUrl}/tas/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public getAgents(filter?): Observable<DropdownOption[]> {
    return this.agentService.getDropdownOptions(filter);
  }

  public getAirlines(): Observable<DropdownOption[]> {
    return this.airlineService.getDropdownOptions();
  }

  public getDesignators(): Observable<DropdownOption[]> {
    return this.airlineService
      .getDropdownOptions()
      .pipe(map(airlinesSummary => toValueLabelObjectDictionaryDesignator(airlinesSummary)));
  }

  public getAgentGroupOptions(bspId?: number): Observable<DropdownOption[]> {
    const url = `${this.baseUrl}/agent-groups`;

    return this.http.get<AgentGroup[]>(url, { params: { ...(bspId && { bspId: bspId.toString() }) } }).pipe(
      map(
        groups => this.formatGroup(groups),
        catchError(() => of([]))
      )
    );
  }

  private formatGroup(group) {
    return group.map(item => ({
      value: item,
      label: `${item.iataCode}`
    }));
  }

  private formatQuery(query: DataQuery<TicketingAuthorityFilter>): RequestQuery {
    const { airlines, agents, ticketingAuthority, abrev, agentStatus, agentGroup, agentAddress, agentCity, bsp } =
      query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ticketingAuthority,
        ['bsp.isoCountryCode']: bsp?.isoCountryCode,
        agentCode: agents && agents.map(item => (item.code ? item.code : item)),
        agentStatus,
        agentGroup: agentGroup && agentGroup.iataCode,
        agentAddress,
        agentCity,
        airlineCode: airlines && airlines.map(airline => (airline.code ? airline.code : airline)),
        airlineAbrev: abrev,
        viewType: airlines ? ViewType.Airline : ViewType.Agent
      },
      sortBy: query.sortBy && formatSortBy(query.sortBy, this.sortMapper)
    });
  }
}
