import { TicketingAuthority } from '../models';

import { convertToDto } from './dto-converter';

describe('Dto converter', () => {
  const ticketingAuthority: TicketingAuthority = {
    id: 1,
    agent: {
      id: '10',
      name: 'Test Agent',
      iataCode: '77',
      status: 'active'
    },
    airline: {
      id: 11,
      name: 'Test airline',
      iataCode: '001',
      abrev: 'TA'
    },
    bsp: {
      id: 6,
      name: 'Test BSP',
      isoCountryCode: 'TB'
    },
    ticketingAuthority: true
  };

  it('should convert ticketing authority dto to model', () => {
    const expectedDto = {
      id: 1,
      ticketingAuthority: true,
      bsp: {
        id: 6
      },
      agent: {
        id: '10'
      },
      airline: {
        id: 11
      }
    };
    expect(convertToDto([ticketingAuthority])).toEqual([expectedDto]);
  });
});
