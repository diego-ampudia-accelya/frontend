import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { memoize } from 'lodash';

import { TicketingAuthorityFilter, ViewTicketingAuthority } from '../models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class TicketingAuthorityFormatter implements FilterFormatter {
  public ignoredFilters: Array<keyof TicketingAuthorityFilter> = [];

  constructor(private translation: L10nTranslationService) {
    this.formatStatus = memoize(this.formatStatus).bind(this);
  }

  public format(filter: TicketingAuthorityFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<TicketingAuthorityFilter>> = {
      bsp: bsp => `${this.translate('bsp')} - ${bsp.isoCountryCode}`,
      airlines: airlines =>
        `${this.translate('airlineCode')} - ${airlines.map(airl => (airl.code ? airl.code : airl)).join(', ')}`,
      abrev: abrev =>
        `${this.translate('designator')} - ${abrev.map(abrv => (abrv.designator ? abrv.designator : abrv)).join(', ')}`,
      agents: agents => `${this.translate('agentCode')} - ${agents.map(agt => (agt.code ? agt.code : agt)).join(', ')}`,
      ticketingAuthority: ticketingAuthority =>
        `${this.translate('taStatus')} - ${this.formatStatus(ticketingAuthority)}`,
      agentStatus: agentStatus => `${this.translate('agentStatus')} - ${this.translate(agentStatus)}`,
      agentGroup: agentGroup => `${this.translate('agentGroup')} - ${agentGroup.iataCode}`,
      agentAddress: agentAddress => `${this.translate('agentAddress')} - ${agentAddress}`,
      agentCity: agentCity => `${this.translate('agentCity')} - ${agentCity}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public formatStatus(status: boolean): string {
    return this.translate(status ? ViewTicketingAuthority.Enabled : ViewTicketingAuthority.Disabled);
  }

  private translate(key: string): string {
    return this.translation.translate('LIST.MASTER_DATA.ticketingAuthority.' + key);
  }
}
