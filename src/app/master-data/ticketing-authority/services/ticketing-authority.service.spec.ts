import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { TicketingAuthority } from '../models';
import { TicketingAuthorityService } from './ticketing-authority.service';
import { initialState } from '~app/files/mocks/files.test.mocks';
import { AppState } from '~app/reducers';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AgentSummary, AirlineSummary, DownloadFormat, DropdownOption } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';
import { AirlineDictionaryService } from '~app/shared/services/dictionary/airline-dictionary.service';

describe('TicketingAuthorityService', () => {
  let service: TicketingAuthorityService;
  let agentServiceSpy: SpyObject<AgentDictionaryService>;
  let airlineServiceSpy: SpyObject<AirlineDictionaryService>;
  let httpClientSpy: SpyObject<HttpClient>;
  let mockStore: MockStore<AppState>;

  const appConfiguration = {
    baseApiPath: ''
  } as AppConfigurationService;

  beforeEach(() => {
    agentServiceSpy = createSpyObject(AgentDictionaryService);
    airlineServiceSpy = createSpyObject(AirlineDictionaryService);
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of({ records: [] }));
    httpClientSpy.put.and.returnValue(of({}));

    TestBed.configureTestingModule({
      providers: [
        TicketingAuthorityService,
        { provide: AgentDictionaryService, useValue: agentServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineServiceSpy },
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppConfigurationService, useValue: appConfiguration },
        provideMockStore({ initialState })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(TicketingAuthorityService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should make call to get ticketing-authorities only when BSP has values', fakeAsync(() => {
    service.find(defaultQuery).subscribe();
    const expectedUrl = '/ta-management/tas?page=0&size=20&viewType=agent';

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should send a request to save all ticketing authorities', fakeAsync(() => {
    service.saveAll(defaultQuery).subscribe();
    const expectedUrl = '/ta-management/tas/all?page=0&size=20&viewType=agent';

    expect(httpClientSpy.put).toHaveBeenCalledWith(expectedUrl, { authorize: true });
  }));

  it('should send a request to save one or more ticketing authorities', fakeAsync(() => {
    const item: TicketingAuthority[] = [
      {
        id: 1,
        agent: {
          id: '0',
          iataCode: '250',
          name: 'Test agent',
          status: 'active'
        },
        airline: {
          id: 1,
          name: 'Test airline',
          iataCode: '001',
          abrev: 'TR'
        },
        bsp: {
          id: 7,
          isoCountryCode: 'ES',
          name: 'Spain'
        },
        ticketingAuthority: true
      }
    ];

    const expectedDto = [
      {
        id: 1,
        ticketingAuthority: true,
        bsp: {
          id: 7
        },
        agent: {
          id: '0'
        },
        airline: {
          id: 1
        }
      }
    ];

    const expectedUrl = '/ta-management/tas';

    service.save(item).subscribe();

    expect(httpClientSpy.put).toHaveBeenCalledWith(expectedUrl, expectedDto);
  }));

  it('should call save when saveMany', fakeAsync(() => {
    const item: TicketingAuthority[] = [
      {
        id: 1,
        agent: {
          id: '0',
          iataCode: '250',
          name: 'Test agent',
          status: 'active'
        },
        airline: {
          id: 1,
          name: 'Test airline',
          iataCode: '001',
          abrev: 'TR'
        },
        bsp: {
          id: 7,
          isoCountryCode: 'ES',
          name: 'Spain'
        },
        ticketingAuthority: true
      }
    ];
    spyOn(service, 'save').and.returnValue(of({}));

    service.saveMany(item).subscribe();

    expect(service.save).toHaveBeenCalledWith(item);
  }));

  it('should make request to download ticketing authorities', fakeAsync(() => {
    const expectedUrl = '/ta-management/tas/download?viewType=agent&downloadType=txt';
    service.download({ filterBy: {}, paginateBy: {}, sortBy: [] }, DownloadFormat.TXT).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));

  it('should return dropdown options with agents', fakeAsync(() => {
    const expectedAgents: DropdownOption[] = [
      {
        label: '777 / Test',
        value: {
          id: '1',
          name: 'Test',
          code: '777'
        }
      }
    ];
    const agents: DropdownOption<AgentSummary>[] = [
      {
        label: '777 / Test',
        value: {
          id: '1',
          name: 'Test',
          code: '777'
        }
      }
    ];
    agentServiceSpy.getDropdownOptions.and.returnValue(of(agents));

    let result = [];

    service.getAgents().subscribe(agentDropdownOptions => {
      result = agentDropdownOptions;
    });

    tick();
    expect(result).toEqual(expectedAgents);
  }));

  it('should return dropdown options with airlines', fakeAsync(() => {
    const expectedAirlines: DropdownOption[] = [
      {
        label: '000 / Test Airline',
        value: {
          id: 0,
          name: 'Test Airline',
          code: '000',
          designator: 'TA'
        }
      }
    ];

    const airlines: DropdownOption<AirlineSummary>[] = [
      {
        label: '000 / Test Airline',
        value: {
          id: 0,
          name: 'Test Airline',
          code: '000',
          designator: 'TA'
        }
      }
    ];
    let result = [];
    airlineServiceSpy.getDropdownOptions.and.returnValue(of(airlines));

    service.getAirlines().subscribe(airlineDropdownOptions => {
      result = airlineDropdownOptions;
    });

    tick();
    expect(result).toEqual(expectedAirlines);
  }));

  it('should return dropdown options with airlines designators', fakeAsync(() => {
    const expectedAirlinesDesignators: DropdownOption[] = [
      {
        label: 'TA',
        value: 'TA'
      }
    ];

    const airlines: DropdownOption<AirlineSummary>[] = [
      {
        label: '000 / Test Airline',
        value: {
          id: 0,
          name: 'Test Airline',
          code: '000',
          designator: 'TA'
        }
      }
    ];
    let result = [];
    airlineServiceSpy.getDropdownOptions.and.returnValue(of(airlines));

    service.getDesignators().subscribe(airlineDropdownOptions => {
      result = airlineDropdownOptions;
    });

    tick();
    expect(result).toEqual(expectedAirlinesDesignators);
  }));

  it('should load agent groups', fakeAsync(() => {
    const bspId = 1;
    const expectedUrl = '/ta-management/agent-groups';
    const expectedParam = { params: { bspId: bspId.toString() } };
    httpClientSpy.get.and.returnValue(of([]));
    service.getAgentGroupOptions(bspId).subscribe();

    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, expectedParam);
  }));
});
