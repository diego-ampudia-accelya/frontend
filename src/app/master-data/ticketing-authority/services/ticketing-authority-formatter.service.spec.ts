import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { TicketingAuthorityFilter } from '../models';

import { TicketingAuthorityFormatter } from './ticketing-authority-formatter.service';
import { AgentSummary, AirlineSummary } from '~app/shared/models';

describe('TicketingAuthorityFormatterService', () => {
  let formatter: TicketingAuthorityFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new TicketingAuthorityFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format when enabled filter is defined', () => {
    const filter: TicketingAuthorityFilter = {
      ticketingAuthority: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['ticketingAuthority'],
        label: 'LIST.MASTER_DATA.ticketingAuthority.taStatus - LIST.MASTER_DATA.ticketingAuthority.enabled'
      }
    ]);
  });

  it('should format when agent status filter is defined', () => {
    const filter: TicketingAuthorityFilter = {
      agentStatus: 'Active'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agentStatus'],
        label: 'LIST.MASTER_DATA.ticketingAuthority.agentStatus - LIST.MASTER_DATA.ticketingAuthority.Active'
      }
    ]);
  });

  it('should format when agent group filter is defined', () => {
    const filter: TicketingAuthorityFilter = {
      agentGroup: {
        id: 0,
        iataCode: '123Group',
        lastName: 'Godard',
        active: true,
        organization: 'Nouvelle Agency'
      }
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agentGroup'],
        label: 'LIST.MASTER_DATA.ticketingAuthority.agentGroup - 123Group'
      }
    ]);
  });

  it('should format when agents are specified', () => {
    const filter: TicketingAuthorityFilter = {
      agents: [
        {
          id: '0',
          code: '123',
          name: 'Agent Smith'
        },
        { id: '1', code: '456', name: 'Agent Anderson' }
      ] as AgentSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agents'],
        label: 'LIST.MASTER_DATA.ticketingAuthority.agentCode - 123, 456'
      }
    ]);
  });

  it('should format when airlines are specified', () => {
    const filter: TicketingAuthorityFilter = {
      airlines: [
        {
          code: '123'
        },
        {
          code: '456'
        }
      ] as AirlineSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airlines'],
        label: 'LIST.MASTER_DATA.ticketingAuthority.airlineCode - 123, 456'
      }
    ]);
  });

  it('should format when designators are specified', () => {
    const filter: TicketingAuthorityFilter = {
      abrev: [{ designator: 'LA' }, { designator: 'QA' }] as AirlineSummary[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['abrev'],
        label: 'LIST.MASTER_DATA.ticketingAuthority.designator - LA, QA'
      }
    ]);
  });

  it('should omit ignored filters', () => {
    const filter: TicketingAuthorityFilter = {
      airlines: [
        {
          code: '123',
          name: 'Lufthansa'
        }
      ] as AirlineSummary[],
      ticketingAuthority: true
    };

    formatter.ignoredFilters = ['airlines', 'ticketingAuthority'];

    expect(formatter.format(filter).length).toEqual(0);
  });

  it('should return empty array when the filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });

  it('should ignore null values', () => {
    const filter: TicketingAuthorityFilter = {
      agents: null,
      ticketingAuthority: null
    };
    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      unknownProp: 'test'
    };
    expect(formatter.format(filter)).toEqual([]);
  });
});
