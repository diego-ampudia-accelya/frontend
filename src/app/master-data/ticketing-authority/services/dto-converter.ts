import { TicketingAuthority } from '../models';

export const convertToDto = (models: TicketingAuthority[]) =>
  models.map(model => ({
    id: model.id,
    ticketingAuthority: model.ticketingAuthority,
    bsp: {
      id: model.bsp.id
    },
    agent: {
      id: model.agent.id
    },
    airline: {
      id: model.airline.id
    }
  }));
