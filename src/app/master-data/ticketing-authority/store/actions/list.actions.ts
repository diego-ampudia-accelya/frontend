import { createAction, props } from '@ngrx/store';

import { TicketingAuthorityFilter, TicketingAuthorityViewModel } from '../../models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const changeRequiredFilter = createAction(
  '[Ticketing Authority] Change Required Filter',
  props<{ requiredFilter: TicketingAuthorityFilter }>()
);
export const changeRequiredFilterSuccess = createAction(
  '[Ticketing Authority] Change Required Filter Success',
  props<{
    data: PagedData<TicketingAuthorityViewModel>;
  }>()
);
export const changeRequiredFilterError = createAction('[Ticketing Authority] Change Required Filter Error');

export const changePredefinedFilters = createAction(
  '[Ticketing Authority] Change Predefined Filters',
  props<{ predefinedFilters: Partial<TicketingAuthorityFilter> }>()
);

export const search = createAction(
  '[Ticketing Authority] Search',
  props<{ query?: DataQuery<TicketingAuthorityFilter> }>()
);
export const searchConfirmed = createAction('[Ticketing Authority] Search Confirmed');
export const searchCancelled = createAction('[Ticketing Authority] Search Cancelled');
export const searchSuccess = createAction(
  '[Ticketing Authority] Search Success',
  props<{ data: PagedData<TicketingAuthorityViewModel> }>()
);
export const searchError = createAction('[Ticketing Authority] Search Error');

export const modify = createAction('[Ticketing Authority] Modify', props<{ items: TicketingAuthorityViewModel[] }>());

export const modifyAll = createAction('[Ticketing Authority] Modify All');

export const openApplyChanges = createAction('[Ticketing Authority] Open Apply Changes');

export const applyChangesSuccess = createAction('Ticketing Authority] Apply Success');
export const applyChangesError = createAction('[Ticketing Authority] Apply Error');

export const discard = createAction('[Ticketing Authority] Discard');

export const download = createAction('[Ticketing Authority] Download');
export const downloadConfirmed = createAction('[Ticketing Authority] Download Confirmed');
