import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './list.reducer';
import { AppState } from '~app/reducers';

export const ticketingAuthorityFeatureKey = 'ticketingAuthority';

export interface State extends AppState {
  [ticketingAuthorityFeatureKey]: TicketingAuthorityState;
}

export interface TicketingAuthorityState {
  [fromList.key]: fromList.State;
}

export function reducers(state: TicketingAuthorityState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getTicketingAuthority = createFeatureSelector<State, TicketingAuthorityState>(
  ticketingAuthorityFeatureKey
);

export const getList = createSelector(getTicketingAuthority, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getListQuery = createSelector(getList, fromList.getQuery);

export const getDownloadQuery = createSelector(
  getListQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

export const getRequiredFilter = createSelector(getList, fromList.getRequiredFilter);

export const getChanges = createSelector(getList, fromList.getChanges);

export const hasChanges = createSelector(getList, fromList.hasChanges);

export const canApplyChanges = createSelector(
  getList,
  state => fromList.hasChanges(state) && !fromList.getLoading(state)
);
