import { TicketingAuthorityFilter, TicketingAuthorityViewModel, ViewAs } from '../../models';
import { TicketingAuthorityListActions } from '../actions';
import * as fromList from './list.reducer';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AirlineSummary } from '~app/shared/models';

describe('ticketing authority list reducer', () => {
  describe('on search', () => {
    it('should update query on load', () => {
      const action = TicketingAuthorityListActions.search({ query: defaultQuery });
      const expected: DataQuery<TicketingAuthorityFilter> = {
        ...defaultQuery,
        filterBy: { airlines: null, agents: null }
      };

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.query).toEqual(expected);
    });

    it('should set loading to true on searchConfirmed', () => {
      const action = TicketingAuthorityListActions.searchConfirmed();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeTruthy();
    });

    it('should set loading to false on searchSuccess', () => {
      const action = TicketingAuthorityListActions.searchSuccess({
        data: {
          records: [{} as TicketingAuthorityViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set totalElements and data on searchSuccess', () => {
      const action = TicketingAuthorityListActions.searchSuccess({
        data: {
          records: [{} as TicketingAuthorityViewModel],
          totalPages: 1,
          total: 1,
          pageNumber: 0,
          pageSize: 20
        }
      });

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data.length).toBe(1);
      expect(newState.query.paginateBy).toEqual({ page: 0, size: 20, totalElements: 1 });
    });

    it('should set loading to false on searchError', () => {
      const action = TicketingAuthorityListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.loading).toBeFalsy();
    });

    it('should set data to [] on searchError', () => {
      const action = TicketingAuthorityListActions.searchError();

      const newState = fromList.reducer(fromList.initialState, action);

      expect(newState.data).toEqual([]);
    });
  });

  describe('on changeRequiredFilter', () => {
    it('should preserve query when it matches required filter', () => {
      const airline = { id: 1 } as AirlineSummary;
      const existingQuery: DataQuery<TicketingAuthorityFilter> = {
        ...defaultQuery,
        filterBy: { airlines: [airline], ticketingAuthority: true }
      };

      const requiredFilter = { viewAs: ViewAs.Airline, bsp: null, airlines: [airline] };

      const state: fromList.State = {
        ...fromList.initialState,
        requiredFilter,
        query: existingQuery
      };
      const action = TicketingAuthorityListActions.changeRequiredFilter({
        requiredFilter
      });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(existingQuery);
    });

    it('should reset to default query when the query does not match required filter', () => {
      const airline = { id: 1 } as AirlineSummary;
      const otherAirline = { id: 2 } as AirlineSummary;
      const existingQuery: DataQuery<TicketingAuthorityFilter> = {
        ...defaultQuery,
        filterBy: { airlines: [airline], ticketingAuthority: true }
      };
      const requiredFilter: TicketingAuthorityFilter = { airlines: [otherAirline], ticketingAuthority: true };
      const expectedQuery: DataQuery<TicketingAuthorityFilter> = {
        ...defaultQuery,
        filterBy: requiredFilter
      };
      const state: fromList.State = {
        ...fromList.initialState,
        query: existingQuery
      };
      const action = TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter });

      const newState = fromList.reducer(state, action);

      expect(newState.query).toEqual(expectedQuery);
    });
  });

  describe('hasChanges', () => {
    it('should return true when enabled was changed', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: [
          {
            id: 1,
            airline: null,
            agent: null,
            bsp: null,
            ticketingAuthority: true,
            original: { id: 1, airline: null, agent: null, bsp: null, ticketingAuthority: false }
          }
        ]
      };

      expect(fromList.hasChanges(state)).toBeTruthy();
    });

    it('should return false when enabled was not changed', () => {
      const state: fromList.State = {
        ...fromList.initialState,
        data: [
          {
            id: 1,
            airline: null,
            agent: null,
            ticketingAuthority: false,
            bsp: null,
            original: { id: 1, airline: null, agent: null, ticketingAuthority: false, bsp: null }
          }
        ]
      };

      expect(fromList.hasChanges(state)).toBeFalsy();
    });
  });
});
