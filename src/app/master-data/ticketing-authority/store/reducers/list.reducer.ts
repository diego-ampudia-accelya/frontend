import { createReducer, on } from '@ngrx/store';
import { cloneDeep, isEqual } from 'lodash';

import { TicketingAuthority, TicketingAuthorityFilter, TicketingAuthorityViewModel } from '../../models';
import { TicketingAuthorityListActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<TicketingAuthorityFilter>;
  previousQuery: DataQuery<TicketingAuthorityFilter>;
  predefinedFilters: Partial<TicketingAuthorityFilter>;
  data: TicketingAuthorityViewModel[];
  loading: boolean;
  requiredFilter: TicketingAuthorityFilter;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  predefinedFilters: { airlines: null, agents: null },
  loading: false,
  requiredFilter: { airlines: null, agents: null }
};

const loadData = (state: State, data: PagedData<TicketingAuthorityViewModel>) => ({
  ...state,
  loading: false,
  data: data.records,
  predefinedFilters: state.predefinedFilters,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (
  newRequiredFilter: TicketingAuthorityFilter,
  oldRequiredFilter: TicketingAuthorityFilter,
  storedQuery: DataQuery<TicketingAuthorityFilter>
): DataQuery<TicketingAuthorityFilter> => {
  let query: DataQuery<TicketingAuthorityFilter> = storedQuery || cloneDeep(defaultQuery);

  // when the required filter has changed
  if (!isEqual(newRequiredFilter, oldRequiredFilter)) {
    query = cloneDeep(defaultQuery);
    query.filterBy = newRequiredFilter;
  }

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = { ...query.filterBy, ticketingAuthority: true };

  return initialQuery;
};

export const reducer = createReducer(
  initialState,
  on(TicketingAuthorityListActions.changeRequiredFilter, (state, { requiredFilter }) => ({
    ...state,
    requiredFilter,
    loading: true,
    query: determineInitialQuery(requiredFilter, state.requiredFilter, state.query)
  })),
  on(TicketingAuthorityListActions.changeRequiredFilterSuccess, (state, { data }) => ({
    ...loadData(state, data)
  })),
  on(TicketingAuthorityListActions.search, (state, { query = state.query }) => ({
    ...state,
    query: { ...query, filterBy: { ...state.predefinedFilters, ...query.filterBy } },
    previousQuery: state.query
  })),
  on(TicketingAuthorityListActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(TicketingAuthorityListActions.searchCancelled, state => ({
    ...state,
    previousQuery: null,
    query: state.previousQuery
  })),
  on(TicketingAuthorityListActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(TicketingAuthorityListActions.changePredefinedFilters, (state, { predefinedFilters }) => ({
    ...state,
    predefinedFilters
  })),
  on(TicketingAuthorityListActions.searchError, TicketingAuthorityListActions.changeRequiredFilterError, state => ({
    ...state,
    loading: false,
    data: []
  })),
  on(TicketingAuthorityListActions.modify, (state, { items }) => ({
    ...state,
    data: items
  })),
  on(TicketingAuthorityListActions.applyChangesSuccess, state => ({
    ...state,
    data: state.data.map(({ original, ...modified }) => ({ ...modified, original: modified }))
  })),
  on(TicketingAuthorityListActions.discard, state => ({
    ...state,
    data: state.data.map(({ original }) => ({ ...original, original }))
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;

export const getRequiredFilter = ({ requiredFilter }: State) => requiredFilter;

export const hasChanges = (state: State) => getChanges(state).length > 0;

export const getChanges = ({ data }: State) => data.filter(({ original, ...item }) => !isEqual(item, original));

export const haveSameId = (a: TicketingAuthority, b: TicketingAuthority) =>
  a.agent.id === b.agent.id && a.airline.id === b.airline.id;
