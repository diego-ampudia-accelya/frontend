import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, ReplaySubject, throwError } from 'rxjs';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { TicketingAuthority, TicketingAuthorityFilter, TicketingAuthorityViewModel } from '../../models';
import { TicketingAuthorityService } from '../../services/ticketing-authority.service';
import { TicketingAuthorityListActions } from '../actions';
import * as fromTicketingAuthority from '../reducers';
import { TicketingAuthorityEffects } from './ticketing-authority.effects';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { NotificationService } from '~app/shared/services/notification.service';

describe('TicketingAuthorityEffects', () => {
  let effects: TicketingAuthorityEffects;
  let ticketingAuthorityServiceStub: SpyObject<TicketingAuthorityService>;
  let changesDialogServiceStub: SpyObject<ChangesDialogService>;
  let dialogServiceStub: SpyObject<DialogService>;
  let permissionsServiceStub: SpyObject<PermissionsService>;

  let notificationServiceStub: SpyObject<NotificationService>;
  let actions$: Observable<Action>;
  let mockStore: MockStore<AppState>;

  const airline1 = { id: 1, name: 'Airline 1' } as AirlineSummary;

  const agent1Authority: TicketingAuthority = {
    id: 1,
    agent: { id: '1', name: 'Agent 1', iataCode: '1000000', status: 'Active' },
    airline: { id: 1, name: 'Airline 1', iataCode: '001', abrev: 'AA' },
    bsp: { id: 1, name: 'TRAPPIST-1e', isoCountryCode: 'ES' } as Bsp,
    ticketingAuthority: true
  };
  const agent2Authority: TicketingAuthority = {
    id: 2,
    agent: { id: '2', name: 'Agent 2', iataCode: '2000000', status: 'Active' },
    airline: { id: 1, name: 'Airline 1', iataCode: '001', abrev: 'AA' },
    bsp: { id: 1, name: 'TRAPPIST-1e', isoCountryCode: 'ES' } as Bsp,
    ticketingAuthority: true
  };

  beforeEach(waitForAsync(() => {
    ticketingAuthorityServiceStub = createSpyObject(TicketingAuthorityService);
    changesDialogServiceStub = createSpyObject(ChangesDialogService);
    permissionsServiceStub = createSpyObject(PermissionsService);
    notificationServiceStub = createSpyObject(NotificationService);
    dialogServiceStub = createSpyObject(DialogService);
    dialogServiceStub.open.and.returnValue(of(null));

    TestBed.configureTestingModule({
      providers: [
        TicketingAuthorityEffects,
        provideMockStore({
          selectors: [
            { selector: fromTicketingAuthority.getRequiredFilter, value: {} },
            { selector: fromTicketingAuthority.getListQuery, value: defaultQuery }
          ]
        }),
        provideMockActions(() => actions$),
        { provide: TicketingAuthorityService, useValue: ticketingAuthorityServiceStub },
        { provide: ChangesDialogService, useValue: changesDialogServiceStub },
        { provide: NotificationService, useValue: notificationServiceStub },
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: PermissionsService, useValue: permissionsServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(TicketingAuthorityEffects);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('`changeRequiredFilter$`', () => {
    it('should dispatch `changeRequiredFilterSuccess` with the found data and options', fakeAsync(() => {
      const data: PagedData<TicketingAuthority> = {
        records: [agent1Authority, agent2Authority],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const expectedData: PagedData<TicketingAuthorityViewModel> = {
        records: [
          { ...agent1Authority, original: agent1Authority },
          { ...agent2Authority, original: agent2Authority }
        ],
        total: 2,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };
      const requiredFilter: TicketingAuthorityFilter = {
        airlines: [airline1]
      };

      const expectedAction = TicketingAuthorityListActions.changeRequiredFilterSuccess({
        data: expectedData
      });

      mockStore.overrideSelector(fromTicketingAuthority.getRequiredFilter, requiredFilter);
      ticketingAuthorityServiceStub.find.and.returnValue(of(data));

      actions$ = of(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));

      effects.changeRequiredFilter$.subscribe((action: any) => {
        expect(action).toEqual(expectedAction);
      });
    }));

    it('should dispatch `changeRequiredFilterError` when `find` throws an error', fakeAsync(() => {
      const requiredFilter: TicketingAuthorityFilter = {
        airlines: [airline1]
      };
      const expectedAction = TicketingAuthorityListActions.changeRequiredFilterError();

      mockStore.overrideSelector(fromTicketingAuthority.getRequiredFilter, requiredFilter);
      ticketingAuthorityServiceStub.find.and.returnValue(throwError('Error'));

      actions$ = of(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));

      effects.changeRequiredFilter$.subscribe(
        (action: any) => {
          expect(action).toEqual(expectedAction);
        },
        error => expect(error).toBeDefined()
      );
    }));

    it('should continue processing actions after an error if a BSP filter exists', fakeAsync(() => {
      const requiredFilter: TicketingAuthorityFilter = {
        airlines: [airline1],
        bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
      };
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 1,
        pageNumber: 0,
        pageSize: 20
      };

      const query = {
        ...defaultQuery,
        filterBy: { bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' } }
      };
      const actionsSubject = new ReplaySubject<Action>(1);

      actions$ = actionsSubject.asObservable();

      ticketingAuthorityServiceStub.find.and.returnValue(throwError('Error'));
      actionsSubject.next(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));
      tick();

      ticketingAuthorityServiceStub.find.and.returnValue(of(data));
      actionsSubject.next(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));
      tick();
      mockStore.overrideSelector(fromTicketingAuthority.getListQuery, query);
      tick();
      const successAction = TicketingAuthorityListActions.changeRequiredFilterSuccess({
        data
      });

      const actions = [];
      effects.changeRequiredFilter$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );

      tick();
      expect(actions).toEqual([successAction]);
    }));
  });

  describe('`search$`', () => {
    it('should dispatch searchConfirmed action when the user has confirmed the operation', fakeAsync(() => {
      changesDialogServiceStub.confirmUnsavedChanges.and.returnValue(of(FooterButton.Apply));
      actions$ = of(TicketingAuthorityListActions.search({ query: defaultQuery }));

      let result;
      effects.search$.subscribe(action => (result = action));

      expect(result.type).toEqual(TicketingAuthorityListActions.searchConfirmed.type);
    }));

    it('should dispatch searchCancelled action when the user has canceled the operation', fakeAsync(() => {
      changesDialogServiceStub.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));
      actions$ = of(TicketingAuthorityListActions.search({ query: defaultQuery }));

      let result;
      effects.search$.subscribe(action => (result = action));

      expect(result.type).toBe(TicketingAuthorityListActions.searchCancelled.type);
    }));
  });

  describe('`searchConfirmed$`', () => {
    it('should dispatch `searchSuccess` with found data', fakeAsync(() => {
      const searchAction = TicketingAuthorityListActions.searchConfirmed();
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 0,
        pageSize: 20,
        pageNumber: 0
      };
      const expectedAction = TicketingAuthorityListActions.searchSuccess({ data });
      mockStore.overrideSelector(fromTicketingAuthority.getListQuery, defaultQuery);
      actions$ = of(searchAction);
      ticketingAuthorityServiceStub.find.and.returnValue(of(data));

      const actions = [];
      effects.searchConfirmed$.subscribe(action => actions.push(action));

      expect(actions).toEqual([expectedAction]);
    }));

    it('should dispatch `searchError` when find throws an error', fakeAsync(() => {
      const searchAction = TicketingAuthorityListActions.searchConfirmed();
      const expectedAction = TicketingAuthorityListActions.searchError();
      actions$ = of(searchAction);
      ticketingAuthorityServiceStub.find.and.returnValue(throwError('Error'));

      const actions = [];
      effects.searchConfirmed$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );
      tick();
      expect(actions).toEqual([expectedAction]);
    }));

    it('should should continue processing actions after an error', fakeAsync(() => {
      const searchAction = TicketingAuthorityListActions.searchConfirmed();
      const data: PagedData<any> = {
        records: [],
        total: 0,
        totalPages: 0,
        pageSize: 20,
        pageNumber: 0
      };
      const actionsSubject = new ReplaySubject<Action>(1);

      actions$ = actionsSubject.asObservable();

      const actions = [];

      ticketingAuthorityServiceStub.find.and.returnValue(throwError('Error'));
      actionsSubject.next(searchAction);

      ticketingAuthorityServiceStub.find.and.returnValue(of(data));
      actionsSubject.next(searchAction);

      effects.searchConfirmed$.subscribe(
        action => actions.push(action),
        error => expect(error).toBeDefined()
      );
      tick();
      const successAction = TicketingAuthorityListActions.searchSuccess({ data });
      expect(actions).toEqual([successAction]);
    }));
  });

  describe('`openApplyChanges$`', () => {
    it('should dispatch `searchConfirmed` when the user has chosen to apply changes', fakeAsync(() => {
      changesDialogServiceStub.confirmApplyChanges.and.returnValue(of(FooterButton.Apply));
      actions$ = of(TicketingAuthorityListActions.openApplyChanges());

      let result;
      effects.openApplyChanges$.subscribe(action => (result = action));

      expect(result.type).toBe(TicketingAuthorityListActions.searchConfirmed.type);
    }));

    it('should dispatch NO action when the user has chosen to discard changes', fakeAsync(() => {
      changesDialogServiceStub.confirmApplyChanges.and.returnValue(of(FooterButton.Discard));
      actions$ = of(TicketingAuthorityListActions.openApplyChanges());

      let result;
      effects.openApplyChanges$.subscribe(action => (result = action));

      expect(result).not.toBeDefined();
    }));
  });

  describe('`applySuccess$`', () => {
    it('should show notification message', fakeAsync(() => {
      actions$ = of(TicketingAuthorityListActions.applyChangesSuccess());

      effects.applySuccess$.subscribe();

      expect(notificationServiceStub.showSuccess).toHaveBeenCalledTimes(1);
    }));
  });
});
