import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { EMPTY, iif, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { ChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { TicketingAuthorityService } from '../../services/ticketing-authority.service';
import { TicketingAuthorityListActions } from '../actions';
import * as fromTicketingAuthority from '../reducers';
import { converter } from './converters';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { Permissions } from '~app/shared/constants';
import { NotificationService } from '~app/shared/services/notification.service';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class TicketingAuthorityEffects {
  public changeRequiredFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TicketingAuthorityListActions.changeRequiredFilter),
      withLatestFrom(this.store.pipe(select(fromTicketingAuthority.getListQuery))),
      switchMap(([_, query]) => {
        const hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

        return iif(
          () => !hasLeanPermission || !!query.filterBy.bsp,
          this.dataService.find(query).pipe(
            map(result =>
              TicketingAuthorityListActions.changeRequiredFilterSuccess({
                data: converter.toViewModels(result)
              })
            ),
            rethrowError(() => TicketingAuthorityListActions.changeRequiredFilterError())
          ),
          EMPTY
        );
      })
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TicketingAuthorityListActions.search),
      switchMap(() =>
        this.changesDialogService
          .confirmUnsavedChanges()
          .pipe(
            map(dialogResult =>
              dialogResult === FooterButton.Cancel
                ? TicketingAuthorityListActions.searchCancelled()
                : TicketingAuthorityListActions.searchConfirmed()
            )
          )
      )
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TicketingAuthorityListActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromTicketingAuthority.getListQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(converter.toViewModels),
          map(data => TicketingAuthorityListActions.searchSuccess({ data })),
          rethrowError(() => TicketingAuthorityListActions.searchError())
        )
      )
    )
  );

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TicketingAuthorityListActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(dialogResult =>
        dialogResult === FooterButton.Apply ? of(TicketingAuthorityListActions.searchConfirmed()) : EMPTY
      ),
      rethrowError(() => TicketingAuthorityListActions.applyChangesError())
    )
  );

  public modifyAll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TicketingAuthorityListActions.modifyAll),
      switchMap(() => this.changesDialogService.modifyAllChanges()),
      switchMap(dialogResult =>
        dialogResult === FooterButton.Apply ? of(TicketingAuthorityListActions.searchConfirmed()) : EMPTY
      )
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(TicketingAuthorityListActions.applyChangesSuccess),
        tap(() => this.notificationService.showSuccess('LIST.MASTER_DATA.ticketingAuthority.applyChanges.success'))
      ),
    { dispatch: false }
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TicketingAuthorityListActions.download),
      switchMap(() => this.changesDialogService.confirmUnsavedChanges()),
      switchMap(result =>
        result !== FooterButton.Cancel ? of(TicketingAuthorityListActions.downloadConfirmed()) : EMPTY
      )
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(TicketingAuthorityListActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromTicketingAuthority.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: TicketingAuthorityService,
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private changesDialogService: ChangesDialogService,
    private notificationService: NotificationService,
    private dialogService: DialogService
  ) {}
}
