import { TicketingAuthority, TicketingAuthorityViewModel } from '../../models';
import { PagedData } from '~app/shared/models/paged-data.model';

const toViewModel = (dataModel: TicketingAuthority): TicketingAuthorityViewModel => ({
  ...dataModel,
  original: dataModel
});

const toDataModel = (viewModel: TicketingAuthorityViewModel): TicketingAuthority => {
  const { original, ...dataModel } = viewModel;

  return dataModel;
};

const toViewModels = (pagedData: PagedData<TicketingAuthority>): PagedData<TicketingAuthorityViewModel> => ({
  ...pagedData,
  records: (pagedData.records || []).map(toViewModel)
});

export const converter = { toViewModel, toDataModel, toViewModels };
