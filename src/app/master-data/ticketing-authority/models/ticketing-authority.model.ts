export interface TicketingAuthority {
  id: number;
  agent: { id: string; iataCode: string; name: string; status: string };
  airline: { id: number; iataCode: string; name: string; abrev: string };
  bsp: { id: number; isoCountryCode: string; name: string };
  ticketingAuthority: boolean;
}

export interface TicketingAuthorityViewModel extends TicketingAuthority {
  original: TicketingAuthority;
}
