/* eslint-disable @typescript-eslint/naming-convention */
export enum ViewType {
  Airline = 'airline',
  Agent = 'agent'
}
