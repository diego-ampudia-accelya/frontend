import { AgentGroup } from '~app/master-data/models/agent-group.model';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

export interface TicketingAuthorityFilter {
  bsp?: Bsp;
  airlines?: AirlineSummary[];
  agents?: AgentSummary[];
  abrev?: AirlineSummary[];
  ticketingAuthority?: boolean;
  agentStatus?: string;
  agentGroup?: AgentGroup;
  agentAddress?: string;
  agentCity?: string;
}
