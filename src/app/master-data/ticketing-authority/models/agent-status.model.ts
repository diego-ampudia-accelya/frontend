export enum AgentStatus {
  active = 'active',
  nonActive = 'nonActive',
  inProcess = 'inProcess'
}
