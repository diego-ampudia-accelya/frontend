/* eslint-disable @typescript-eslint/naming-convention */
export enum ViewTicketingAuthority {
  Enabled = 'enabled',
  Disabled = 'disabled'
}
