import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { first, map, skipWhile, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import {
  AgentStatus,
  TicketingAuthorityFilter,
  TicketingAuthorityViewModel,
  ViewAs,
  ViewTicketingAuthority
} from '../models';
import { TicketingAuthorityFormatter } from '../services/ticketing-authority-formatter.service';
import { TicketingAuthorityService } from '../services/ticketing-authority.service';
import { TicketingAuthorityListActions } from '../store/actions';
import * as fromTicketingAuthority from '../store/reducers';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { CanComponentDeactivate } from '~app/core/services';
import { AppState } from '~app/reducers';
import { ButtonDesign, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants/permissions';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-ticketing-authority',
  templateUrl: './ticketing-authority.component.html',
  styleUrls: ['./ticketing-authority.component.scss'],
  providers: [TicketingAuthorityFormatter]
})
export class TicketingAuthorityComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  public predefinedFilters: Partial<TicketingAuthorityFilter> = { ticketingAuthority: true };

  public loggedUser: User;

  public loading$ = this.store.pipe(select(fromTicketingAuthority.getListLoading));
  public data$ = this.store.pipe(select(fromTicketingAuthority.getListData));
  public query$ = this.store.pipe(select(fromTicketingAuthority.getListQuery));
  public hasData$ = this.store.pipe(select(fromTicketingAuthority.hasData));

  // Flags
  public hasLeanPermission: boolean;
  public isBspFilterLocked: boolean;
  public isSearchEnabled = false;
  public canShowDataQuery = true;
  public hasFilteringExpanded = false;
  public isUpdateAllVisible = false;

  public isAgentFilterLocked: boolean;
  public agentDropdownPlaceholder: string;
  public isAgentGroupFilterLocked: boolean;
  public agentGroupDropdownPlaceholder: string;

  public columns$: Observable<TableColumn[]>;
  public searchForm: FormGroup;

  public agents$: Observable<DropdownOption<AgentSummary>[]>;
  public airlines$: Observable<DropdownOption[]>;
  public designators$: Observable<DropdownOption[]>;
  public agentGroupOptions$: Observable<DropdownOption[]>;

  public userBspOptions: DropdownOption<Bsp>[] = [];
  public statusOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatStatus(value)
  }));
  public agentStatusOptions: DropdownOption<string>[] = [
    AgentStatus.active,
    AgentStatus.nonActive,
    AgentStatus.inProcess
  ].map(value => ({
    value,
    label: this.translationService.translate(`LIST.MASTER_DATA.ticketingAuthority.${value}`)
  }));

  public get viewAs(): ViewAs {
    return this.activatedRoute.snapshot.data.viewAs;
  }

  public get isAgentView(): boolean {
    return this.viewAs === ViewAs.Agent;
  }

  public get isAirlineView(): boolean {
    return this.viewAs === ViewAs.Airline;
  }

  public get isAgentGroupView(): boolean {
    return this.viewAs === ViewAs.AgentGroup;
  }

  public get isAgentGroupUser(): boolean {
    return this.loggedUser?.userType === UserType.AGENT_GROUP;
  }

  public get isAgentAddressColumnVisible$(): Observable<boolean> {
    return this.loggedUser$.pipe(
      map(
        user =>
          user.userType === UserType.AGENT_GROUP ||
          user.userType === UserType.AIRLINE ||
          user.userType === UserType.GDS ||
          user.userType === UserType.IATA
      )
    );
  }

  public massiveLabel = ViewTicketingAuthority.Disabled;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public ButtonDesign = ButtonDesign;

  private selectedBsp: Bsp;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(fromAuth.getUser), first());
  }

  private destroy$ = new Subject();

  constructor(
    public displayFormatter: TicketingAuthorityFormatter,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private changesDialogService: ChangesDialogService,
    private translationService: L10nTranslationService,
    private dataService: TicketingAuthorityService,
    private agentDictionaryService: AgentDictionaryService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.searchForm = this.buildForm();

    this.columns$ = this.store.pipe(
      select(fromAuth.hasPermission(Permissions.updateTicketingAuthority)),
      withLatestFrom(this.isAgentAddressColumnVisible$),
      //TODO this (canUpdateAll = false) hide temporarily enable-disable-all button for (FCA-8719) request
      tap(([canUpdateAll]) => {
        canUpdateAll = false;
        this.initializeUpdateAllVisibility(canUpdateAll);
      }),
      map(([canUpdateAll, isAgentAddressColumnVisible]) => this.getColumns(canUpdateAll, isAgentAddressColumnVisible))
    );

    this.displayFormatter.ignoredFilters = this.getIgnoredFilters();

    this.initializeFeaturesByLoggedUser();
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  public onQueryChanged(query: DataQuery<TicketingAuthorityFilter>): void {
    this.initializeSearchFlags(query);

    this.store.dispatch(TicketingAuthorityListActions.search({ query }));

    this.changeMassiveLabel(query);
  }

  public onModify(items: TicketingAuthorityViewModel[]): void {
    this.store.dispatch(TicketingAuthorityListActions.modify({ items }));
  }

  public onDownload(): void {
    this.store.dispatch(TicketingAuthorityListActions.download());
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.initializeFiltersDropdown(this.selectedBsp);
    }
  }

  public onChangeAllFilteredClicked() {
    this.store.dispatch(TicketingAuthorityListActions.modifyAll());
  }

  private initializeUpdateAllVisibility(canUpdateAll: boolean) {
    this.isUpdateAllVisible = this.isAirlineView && canUpdateAll;
  }

  private changeMassiveLabel(query: DataQuery<TicketingAuthorityFilter>) {
    const ticketingAuthority = query.filterBy.ticketingAuthority;
    this.massiveLabel = ticketingAuthority ? ViewTicketingAuthority.Disabled : ViewTicketingAuthority.Enabled;
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      bsp: [],
      airlines: [],
      abrev: [],
      agents: [],
      ticketingAuthority: [],
      agentStatus: [],
      agentGroup: [],
      agentAddress: [],
      agentCity: []
    });
  }

  private initializeFeaturesByLoggedUser(): void {
    combineLatest([this.loggedUser$, this.query$.pipe(first())]).subscribe(([loggedUser, query]) => {
      this.loggedUser = loggedUser;
      if (query) {
        if (this.hasLeanPermission) {
          this.initializeBsp(query);
          this.disableBspDependentFilters();
        }
        this.initializeSearchFlags(query);
        this.initializeFiltersDropdown(query.filterBy.bsp);
        this.initializeData();
      }
    });
  }

  private initializeData(): void {
    if (!this.hasLeanPermission || (this.hasLeanPermission && !!this.selectedBsp)) {
      // We fetch query data only if the user is not Lean or they are Lean and have already selected a BSP
      this.store.dispatch(TicketingAuthorityListActions.search({}));
    }
  }

  private initializeBsp(query: DataQuery<TicketingAuthorityFilter>): void {
    const bspFromQuery = query.filterBy.bsp;

    if (bspFromQuery) {
      this.selectedBsp = bspFromQuery;
      this.setBspPredefinedFilter(bspFromQuery);
    }

    this.initializeBspListener();
    this.initializeBspOptions();
  }

  private setBspPredefinedFilter(bsp: Bsp): void {
    // We save predefined filters in our component (to provide it to list-view) and in the state (so Search action has it available)
    this.predefinedFilters = { ...this.predefinedFilters, bsp };
    this.store.dispatch(
      TicketingAuthorityListActions.changePredefinedFilters({ predefinedFilters: this.predefinedFilters })
    );
  }

  private initializeSearchFlags(query: DataQuery<TicketingAuthorityFilter>): void {
    const isBspSelectedValue = !isEmpty(query.filterBy.bsp);
    const isSearchEnabled = !(this.hasLeanPermission && !isBspSelectedValue);

    this.canShowDataQuery = isSearchEnabled;
    this.hasFilteringExpanded = !isSearchEnabled;
    this.isSearchEnabled = isSearchEnabled;
  }

  private initializeBspListener(): void {
    FormUtil.get<TicketingAuthorityFilter>(this.searchForm, 'bsp')
      .valueChanges.pipe(
        skipWhile(value => !value),
        takeUntil(this.destroy$)
      )
      .subscribe(selectedBspValue => {
        this.selectedBsp = selectedBspValue;
        this.isSearchEnabled = !isEmpty(selectedBspValue);

        this.setBspPredefinedFilter(this.selectedBsp);
        this.enableBspDependentFilters();
      });
  }

  private initializeBspOptions(): void {
    this.userBspOptions = this.filterBspsByPermission()?.map(toValueLabelObjectBsp) as DropdownOption<Bsp>[];

    const oneBspWithPermission = this.userBspOptions?.length === 1;

    this.isBspFilterLocked = oneBspWithPermission;

    if (oneBspWithPermission) {
      const bspControl = FormUtil.get<TicketingAuthorityFilter>(this.searchForm, 'bsp');
      bspControl.patchValue(this.userBspOptions[0].value);
      this.selectBsp(this.userBspOptions[0].value);
    }
  }

  private filterBspsByPermission(): Bsp[] {
    const permission = Permissions.readTicketingAuthority;
    if (this.loggedUser) {
      const { bsps, bspPermissions } = this.loggedUser;

      return bsps.filter(bsp =>
        bspPermissions.some(bspPerm => bsp.id === bspPerm.bspId && bspPerm.permissions.includes(permission))
      );
    }
  }

  public async selectBsp(bsp: Bsp): Promise<void> {
    if (bsp) {
      this.selectedBsp = bsp;
      this.resetFieldsOnBspChange();
      this.initializeFiltersDropdown(bsp);
    }
  }

  private resetFieldsOnBspChange(): void {
    FormUtil.get<TicketingAuthorityFilter>(this.searchForm, 'agents').reset();
    FormUtil.get<TicketingAuthorityFilter>(this.searchForm, 'agentGroup').reset();
  }

  private initializeFiltersDropdown(bsp: Bsp) {
    if (bsp) {
      this.initializeFiltersDropdownForLeanUser(bsp);
    } else if (!this.hasLeanPermission) {
      this.initializeFiltersDropdownForNonLeanUser();
    }
    this.airlines$ = this.isAirlineView ? of(null) : this.dataService.getAirlines();
    this.designators$ = this.isAirlineView ? of(null) : this.dataService.getDesignators();
  }

  private initializeFiltersDropdownForNonLeanUser(): void {
    this.agents$ =
      this.isAgentGroupView || this.isAgentGroupUser
        ? this.agentDictionaryService.getAgentGroupUserDropdownOptions(Permissions.readTicketingAuthority)
        : this.dataService.getAgents();
    this.agentGroupOptions$ =
      this.isAgentView || this.isAgentGroupView || this.isAgentGroupUser
        ? of(null)
        : this.dataService.getAgentGroupOptions();
  }

  private initializeFiltersDropdownForLeanUser(bsp: Bsp): void {
    const queryFilters = { bspId: bsp.id };
    this.agents$ =
      this.isAgentGroupView || this.isAgentGroupUser
        ? this.agentDictionaryService.getAgentGroupUserDropdownOptions(Permissions.readTicketingAuthority)
        : this.agentDictionaryService.getDropdownOptions(queryFilters);
    this.agentGroupOptions$ =
      this.isAgentView || this.isAgentGroupView || this.isAgentGroupUser
        ? of(null)
        : this.dataService.getAgentGroupOptions(bsp.id).pipe(
            map(options =>
              options.map(option => ({
                ...option,
                label: `${this.selectedBsp.isoCountryCode} - ${option.label}`
              }))
            )
          );
  }

  private enableBspDependentFilters(): void {
    this.agentDropdownPlaceholder = 'LIST.MASTER_DATA.ticketingAuthority.filters.placeholders.agent';
    this.isAgentFilterLocked = false;

    this.agentGroupDropdownPlaceholder = 'LIST.MASTER_DATA.ticketingAuthority.filters.placeholders.agentGroup';
    this.isAgentGroupFilterLocked = false;
  }

  private disableBspDependentFilters(): void {
    this.agentDropdownPlaceholder = 'LIST.MASTER_DATA.ticketingAuthority.filters.placeholders.disableAgent';
    this.isAgentFilterLocked = true;

    this.agentGroupDropdownPlaceholder = 'LIST.MASTER_DATA.ticketingAuthority.filters.placeholders.disableAgentGroup';
    this.isAgentGroupFilterLocked = true;
  }

  private getColumns(canEdit: boolean, isAgentAddressColumnVisible: boolean): Array<TableColumn & { viewAs?: ViewAs }> {
    const editableStatusColumn: Partial<TableColumn> = {
      name: 'LIST.MASTER_DATA.ticketingAuthority.columns.taManagement',
      headerTemplate: 'checkboxHeaderTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl',
      cellClass: this.isChanged.bind(this)
    };
    const readOnlyStatusColumn: Partial<TableColumn> = {
      name: 'LIST.MASTER_DATA.ticketingAuthority.columns.taStatus',
      cellTemplate: 'defaultBooleanCellTmpl'
    };

    const statusColumn = canEdit ? editableStatusColumn : readOnlyStatusColumn;

    const agentAddressColumns = isAgentAddressColumnVisible
      ? [
          {
            name: 'LIST.MASTER_DATA.ticketingAuthority.columns.agentAddress',
            prop: 'agent.address',
            flexGrow: 6
          },
          {
            name: 'LIST.MASTER_DATA.ticketingAuthority.columns.agentCity',
            prop: 'agent.city',
            flexGrow: 3
          }
        ]
      : [];

    return [
      {
        name: 'LIST.MASTER_DATA.ticketingAuthority.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 3,
        viewAs: ViewAs.Airline
      },
      {
        name: 'LIST.MASTER_DATA.ticketingAuthority.columns.agentName',
        prop: 'agent.name',
        flexGrow: 8,
        viewAs: ViewAs.Airline
      },
      ...agentAddressColumns,
      {
        name: 'LIST.MASTER_DATA.ticketingAuthority.columns.agentStatus',
        prop: 'agent.status',
        flexGrow: 4,
        viewAs: ViewAs.Airline
      },
      {
        name: 'LIST.MASTER_DATA.ticketingAuthority.columns.airlineCode',
        prop: 'airline.iataCode',
        flexGrow: 3,
        viewAs: ViewAs.Agent
      },
      {
        name: 'LIST.MASTER_DATA.ticketingAuthority.columns.airlineName',
        prop: 'airline.name',
        flexGrow: 8,
        viewAs: ViewAs.Agent
      },
      {
        name: 'LIST.MASTER_DATA.ticketingAuthority.columns.designator',
        prop: 'airline.abrev',
        flexGrow: 4,
        viewAs: ViewAs.Agent
      },
      {
        ...statusColumn,
        prop: 'ticketingAuthority',
        flexGrow: 5
      }
    ].filter((column: any) => !column.viewAs || this.isAgentGroupView || column.viewAs === this.viewAs);
  }

  private getIgnoredFilters(): Array<keyof TicketingAuthorityFilter> {
    if (this.isAgentGroupView) {
      return [];
    }

    return this.isAirlineView ? ['airlines'] : ['agents'];
  }

  private isChanged({ row, column }: { row: TicketingAuthorityViewModel; column: TableColumn }): {
    [key: string]: boolean;
  } {
    const { prop } = column;

    return {
      'checkbox-control-changed': row.original[prop] !== row[prop],
      'checkbox-control': true
    };
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
