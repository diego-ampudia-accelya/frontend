import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { ViewAs } from '../models';
import { TicketingAuthorityFormatter } from '../services/ticketing-authority-formatter.service';
import { TicketingAuthorityService } from '../services/ticketing-authority.service';
import { TicketingAuthorityListActions } from '../store/actions';
import * as fromList from '../store/reducers/list.reducer';
import { TicketingAuthorityComponent } from './ticketing-authority.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('TicketingAuthorityComponent', () => {
  let component: TicketingAuthorityComponent;
  let fixture: ComponentFixture<TicketingAuthorityComponent>;
  let displayFormatterStub: SpyObject<TicketingAuthorityFormatter>;
  let changesDialogServiceStub: SpyObject<ChangesDialogService>;
  let activatedRoute;
  let ticketingAuthorityServiceStub: SpyObject<TicketingAuthorityService>;
  let agentDictionaryServiceStub: SpyObject<AgentDictionaryService>;
  let airlineDictionaryServicestub: SpyObject<AirlineDictionaryService>;

  let translationServiceSpy;
  let mockStore: MockStore;

  const airlineUser = createAirlineUser();
  const airline = { ...airlineUser.globalAirline, airlines: createAirlineUser() };
  const initialUser = { ...airlineUser, globalAirline: airline };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: []
  };

  beforeEach(waitForAsync(() => {
    displayFormatterStub = createSpyObject(TicketingAuthorityFormatter);
    activatedRoute = {
      snapshot: {
        data: {}
      }
    };
    ticketingAuthorityServiceStub = createSpyObject(TicketingAuthorityService);
    ticketingAuthorityServiceStub.getAgents.and.returnValue(of([]));
    ticketingAuthorityServiceStub.getAgentGroupOptions.and.returnValue(of([]));

    agentDictionaryServiceStub = createSpyObject(AgentDictionaryService);

    airlineDictionaryServicestub = createSpyObject(AirlineDictionaryService);

    changesDialogServiceStub = createSpyObject(ChangesDialogService);
    translationServiceSpy = createSpyObject(L10nTranslationService);

    TestBed.configureTestingModule({
      declarations: [TicketingAuthorityComponent, TranslatePipeMock],
      providers: [
        provideMockStore({
          initialState: {
            auth: {
              user: expectedUserDetails
            },
            ticketingAuthority: {
              list: fromList.initialState
            }
          },
          selectors: [
            { selector: fromAuth.hasPermission('MODIFY_TICKETING_AUTHORITY'), value: true },
            { selector: fromAuth.getUser, value: initialUser }
          ]
        }),
        FormBuilder,
        PermissionsService,
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: TicketingAuthorityService, useValue: ticketingAuthorityServiceStub },
        { provide: ChangesDialogService, useValue: changesDialogServiceStub },
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        },
        {
          provide: AgentDictionaryService,
          useValue: agentDictionaryServiceStub
        },
        {
          provide: AirlineDictionaryService,
          useValue: airlineDictionaryServicestub
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(TicketingAuthorityComponent, {
        set: {
          providers: [{ provide: TicketingAuthorityFormatter, useValue: displayFormatterStub }]
        }
      })
      .compileComponents();

    mockStore = TestBed.inject<any>(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketingAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should configure columns when viewing as Airline', () => {
    setViewAs(ViewAs.Airline);
    component.ngOnInit();

    let columns: TableColumn[];
    component.columns$.subscribe(result => (columns = result));
    expect(columns.length).toBe(6);
  });

  it('should configure columns when viewing as Agent', () => {
    setViewAs(ViewAs.Agent);
    component.ngOnInit();

    let columns: TableColumn[];
    component.columns$.subscribe(result => (columns = result));
    expect(columns.length).toBe(6);
  });

  it('should configure columns when viewing as Agent Group', () => {
    setViewAs(ViewAs.AgentGroup);
    component.ngOnInit();

    let columns: TableColumn[];
    component.columns$.subscribe(result => (columns = result));
    expect(columns.length).toBe(9);
  });

  it('should ignore airlines filter when viewing as Airline', () => {
    setViewAs(ViewAs.Airline);
    component.ngOnInit();

    expect(component.isAirlineView).toBe(true);
    expect(component.displayFormatter.ignoredFilters).toEqual(['airlines']);
  });

  it('should ignore agents filter when viewing as Agent', () => {
    setViewAs(ViewAs.Agent);
    component.ngOnInit();

    expect(component.isAgentView).toBe(true);
    expect(component.displayFormatter.ignoredFilters).toEqual(['agents']);
  });

  it('should not ignore any filter when viewing as Agent Group', () => {
    setViewAs(ViewAs.AgentGroup);
    component.ngOnInit();

    expect(component.isAgentGroupView).toBe(true);
    expect(component.displayFormatter.ignoredFilters).toEqual([]);
  });

  it('should dispatch search when query change', () => {
    spyOn(mockStore, 'dispatch');

    component.onQueryChanged(defaultQuery);

    expect(mockStore.dispatch).toHaveBeenCalledWith(TicketingAuthorityListActions.search({ query: defaultQuery }));
  });

  it('should dispatch modify', () => {
    spyOn(mockStore, 'dispatch');

    component.onModify([]);

    expect(mockStore.dispatch).toHaveBeenCalledWith(TicketingAuthorityListActions.modify({ items: [] }));
  });

  it('should dispatch download', () => {
    spyOn(mockStore, 'dispatch');

    component.onDownload();

    expect(mockStore.dispatch).toHaveBeenCalledWith(TicketingAuthorityListActions.download());
  });

  it('should set airlines and designators when it is viewAs Agent', fakeAsync(() => {
    setViewAs(ViewAs.Agent);

    ticketingAuthorityServiceStub.getAirlines.and.returnValue(of([{ value: '000', label: '000 / Test Airline' }]));
    ticketingAuthorityServiceStub.getDesignators.and.returnValue(of([{ value: 'TA', label: 'TA' }]));

    component.onFilterButtonClicked(true);

    let airlines = [];
    let designator = [];

    component.designators$.subscribe(dgn => {
      designator = dgn;
    });

    component.airlines$.subscribe(airl => {
      airlines = airl;
    });

    tick();

    expect(designator).toEqual([{ value: 'TA', label: 'TA' }]);
    expect(airlines).toEqual([{ value: '000', label: '000 / Test Airline' }]);
  }));

  it('should set agents when it is viewAs Airline', fakeAsync(() => {
    setViewAs(ViewAs.Airline);

    ticketingAuthorityServiceStub.getAgents.and.returnValue(of([{ value: '788', label: '788 / Test Agent' }]));

    component.onFilterButtonClicked(true);

    let agents = [];
    let airlines = [];

    component.agents$.subscribe(agn => {
      agents = agn;
    });

    component.airlines$.subscribe(airl => {
      airlines = airl;
    });

    tick();

    expect(agents).toEqual([{ value: '788', label: '788 / Test Agent' }]);
    expect(airlines).toEqual(null);
  }));

  it('should set agents, airlines and designators when it is viewAs Agent Group', fakeAsync(() => {
    let agents = [];
    let airlines = [];
    let designators = [];
    let agentGroups = [];

    agentDictionaryServiceStub.getAgentGroupUserDropdownOptions.and.returnValue(
      of([{ value: '788', label: '788 / Test Agent' }])
    );
    ticketingAuthorityServiceStub.getAirlines.and.returnValue(of([{ value: '000', label: '000 / Test Airline' }]));
    ticketingAuthorityServiceStub.getDesignators.and.returnValue(of([{ value: 'TA', label: 'TA' }]));

    setViewAs(ViewAs.AgentGroup);

    component.onFilterButtonClicked(true);

    component.agents$.subscribe(agn => (agents = agn));
    component.airlines$.subscribe(airl => (airlines = airl));
    component.designators$.subscribe(desig => (designators = desig));
    component.agentGroupOptions$.subscribe(agnGrp => (agentGroups = agnGrp));

    tick();

    expect(agents).toEqual([{ value: '788', label: '788 / Test Agent' }]);
    expect(airlines).toEqual([{ value: '000', label: '000 / Test Airline' }]);
    expect(designators).toEqual([{ value: 'TA', label: 'TA' }]);
    expect(agentGroups).toBeNull();
  }));

  it('should dispatch modify all when changeAll is clicked', () => {
    spyOn(mockStore, 'dispatch');

    component.onChangeAllFilteredClicked();

    expect(mockStore.dispatch).toHaveBeenCalledWith(TicketingAuthorityListActions.modifyAll());
  });

  function setViewAs(viewAs: ViewAs): void {
    component['activatedRoute'].snapshot.data.viewAs = viewAs;
  }
});
