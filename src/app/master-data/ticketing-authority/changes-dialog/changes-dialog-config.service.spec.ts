import { TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { TicketingAuthority, TicketingAuthorityFilter, TicketingAuthorityViewModel } from '../models';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { AppState } from '~app/reducers';
import { ChangesDialogConfig } from '~app/shared/components';
import { AirlineSummary } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';

describe('ChangesDialogConfigService', () => {
  let service: ChangesDialogConfigService;
  let translationStub: SpyObject<L10nTranslationService>;
  let mockStore: MockStore<AppState>;

  const bsp = { id: 1, name: 'TRAPPIST-1e', isoCountryCode: 'ES' } as Bsp;
  const agent1 = { id: '1', name: 'Agent 1', iataCode: '1000000', status: 'Non Active' };
  const agent2 = { id: '2', name: 'Agent 2', iataCode: '2000000', status: 'Active' };
  const airline = { id: 1, name: 'Airline 1', iataCode: '001', abrev: 'AA' };

  const agentWithEnabledAuthority: TicketingAuthorityViewModel = {
    id: 1,
    agent: agent1,
    airline,
    bsp,
    ticketingAuthority: true,
    original: { ticketingAuthority: false } as TicketingAuthority
  };
  const agentWithDisabledAuthority: TicketingAuthorityViewModel = {
    id: 2,
    agent: agent2,
    airline,
    bsp,
    ticketingAuthority: false,
    original: { ticketingAuthority: true } as TicketingAuthority
  };
  const requiredFilter: TicketingAuthorityFilter = {
    airlines: [{ id: 1, name: 'Airline 1' } as AirlineSummary]
  };

  beforeEach(waitForAsync(() => {
    translationStub = createSpyObject(L10nTranslationService);
    translationStub.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      providers: [
        ChangesDialogConfigService,
        { provide: L10nTranslationService, useValue: translationStub },
        provideMockStore()
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(ChangesDialogConfigService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should build configuration with formatted modifications', () => {
    const modifications: TicketingAuthorityViewModel[] = [agentWithEnabledAuthority, agentWithDisabledAuthority];
    const expectedConfiguration = jasmine.objectContaining<ChangesDialogConfig>({
      changes: [
        {
          group: translations.taGroupTitle,
          name: agent1.name,
          value: translations.enabled,
          originalValue: agentWithEnabledAuthority.ticketingAuthority
        },
        {
          group: translations.taGroupTitle,
          name: agent2.name,
          value: translations.disabled,
          originalValue: agentWithDisabledAuthority.ticketingAuthority
        }
      ]
    });

    const configuration = service.build(modifications, requiredFilter, translations.applyChanges);
    expect(configuration).toEqual(expectedConfiguration);
  });
});
