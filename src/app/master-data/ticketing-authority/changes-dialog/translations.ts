export const translations = {
  applyChanges: {
    title: 'LIST.MASTER_DATA.ticketingAuthority.applyChanges.title',
    details: 'LIST.MASTER_DATA.ticketingAuthority.applyChanges.message'
  },
  unsavedChanges: {
    title: 'LIST.MASTER_DATA.ticketingAuthority.unsavedChanges.title',
    details: 'LIST.MASTER_DATA.ticketingAuthority.unsavedChanges.message'
  },
  modifyAllChanges: {
    title: 'LIST.MASTER_DATA.ticketingAuthority.applyChanges.title',
    details: 'LIST.MASTER_DATA.ticketingAuthority.applyChanges.modifyAllMessage'
  },
  taGroupTitle: 'LIST.MASTER_DATA.ticketingAuthority.ta',
  enabled: 'LIST.MASTER_DATA.ticketingAuthority.enabled',
  disabled: 'LIST.MASTER_DATA.ticketingAuthority.disabled'
};
