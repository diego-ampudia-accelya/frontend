import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { TicketingAuthorityFilter, TicketingAuthorityViewModel } from '../models';

import { translations } from './translations';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable({ providedIn: 'root' })
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: TicketingAuthorityViewModel[],
    requiredFilter: TicketingAuthorityFilter,
    question: { title: string; details: string }
  ): ChangesDialogConfig {
    const message = this.translation.translate(
      question.details,
      this.getTranslationValues(requiredFilter, modifications)
    );

    return {
      data: {
        title: question.title,
        footerButtonsType: modifications
          ? [{ type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary }, { type: FooterButton.Apply }]
          : [{ type: FooterButton.Apply }]
      },
      changes: modifications ? this.formatChanges(modifications, requiredFilter) : null,
      message
    };
  }

  private formatChanges(
    modifications: TicketingAuthorityViewModel[],
    requiredFilter: TicketingAuthorityFilter
  ): ChangeModel[] {
    const translateStatus = (status: boolean) =>
      this.translation.translate(status ? translations.enabled : translations.disabled);
    const getName = (item: TicketingAuthorityViewModel) =>
      requiredFilter.airlines ? item.agent.name : item.airline.name;

    return modifications.map(item => ({
      group: this.translation.translate(translations.taGroupTitle),
      name: getName(item),
      value: translateStatus(item.ticketingAuthority),
      originalValue: item.ticketingAuthority
    }));
  }

  private getTranslationValues(
    requiredFilter: TicketingAuthorityFilter,
    modifications: TicketingAuthorityViewModel[]
  ): { bspName: string; name: string } {
    const { airlines, agents } = requiredFilter;
    const bsp = modifications ? modifications.map(mod => mod.bsp) : '';

    return {
      bspName: bsp ? bsp[0].name : null,
      name: airlines ? airlines[0].name : agents[0].name
    };
  }
}
