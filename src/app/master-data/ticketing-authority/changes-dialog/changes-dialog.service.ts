import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { TicketingAuthorityFilter, TicketingAuthorityViewModel } from '../models';
import { TicketingAuthorityService } from '../services/ticketing-authority.service';
import { TicketingAuthorityListActions } from '../store/actions';
import { converter } from '../store/effects/converters';
import * as fromTicketingAuthority from '../store/reducers';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: TicketingAuthorityService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public modifyAllChanges(): Observable<FooterButton> {
    return this.confirmModifyAll(translations.modifyAllChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return of(null).pipe(
      withLatestFrom(
        this.store.pipe(select(fromTicketingAuthority.getChanges)),
        this.store.pipe(select(fromTicketingAuthority.getRequiredFilter))
      ),
      switchMap(([_, modifications, requiredFilter]) =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, requiredFilter, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  public confirmModifyAll(question: { title: string; details: string }): Observable<FooterButton> {
    return of(null).pipe(
      withLatestFrom(this.store.pipe(select(fromTicketingAuthority.getRequiredFilter))),
      switchMap(([_, requiredFilter]) => defer(() => this.open(null, requiredFilter, question)))
    );
  }

  private open(
    modifications: TicketingAuthorityViewModel[],
    requiredFilter: TicketingAuthorityFilter,
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, requiredFilter, question);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = modifications ? this.apply(modifications, dialogConfig) : this.applyToAll(dialogConfig);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(TicketingAuthorityListActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private applyToAll(dialogConfig: ChangesDialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return of(null).pipe(
      withLatestFrom(this.store.pipe(select(fromTicketingAuthority.getListQuery))),
      switchMap(([_, query]) =>
        defer(() => {
          setLoading(true);

          return this.dataService.saveAll(query).pipe(
            tap(() => this.store.dispatch(TicketingAuthorityListActions.applyChangesSuccess())),
            mapTo(FooterButton.Apply),
            finalize(() => setLoading(false))
          );
        })
      )
    );
  }

  private apply(
    modifications: TicketingAuthorityViewModel[],
    dialogConfig: ChangesDialogConfig
  ): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.saveMany(modifications.map(converter.toDataModel)).pipe(
        tap(() => this.store.dispatch(TicketingAuthorityListActions.applyChangesSuccess())),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
