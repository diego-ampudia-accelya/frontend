import { PaymentStatusPipe } from './payment-status.pipe';

export const AGENTS = {
  DATA_PROPERTIES: {
    BSP: 'bsp.id',
    ACTIVE: 'active',
    AGENT_CODE: 'iataCode',
    REGISTER_DATE: 'effectiveFrom',
    EXPIRATION_DATE: 'effectiveTo',
    AIRLINE_CODE: 'airlineId',
    TA_STATUS: 'ticketingAuthority',
    LOCATION_CLASS: 'locationClass',
    LOCATION_TYPE: 'locationType',
    REMITTANCE_FREQUENCY: 'remittanceFrequency',
    VAT_NUMBER: 'vatNumber',
    CITY: 'address.city',
    STATE: 'address.state',
    ADDRESS_COUNTRY: 'address.country',
    POSTAL_CODE: 'address.postalCode',
    EMAIL: 'contact.email',
    TELEPHONE: 'contact.telephone',
    FAX: 'contact.fax',
    CASH_PAYMENT: 'cash',
    PAYMENT_CARD: 'paymentCard',
    EASY_PAY: 'easyPay',
    PARENT_IATA_CODE: 'parentIataCode',
    LOCALITY: 'address.locality'
  },
  COLUMNS: [
    {
      name: `MASTER_DATA.agent.columns.isoc`,
      prop: 'bsp.isoCountryCode',
      draggable: false,
      resizeable: false,
      width: 75
    },
    {
      prop: 'iataCode',
      name: 'MASTER_DATA.agent.columns.code',
      resizeable: false,
      draggable: false,
      maxWidth: 100
    },
    {
      prop: 'name',
      name: 'MASTER_DATA.agent.columns.name',
      resizeable: false,
      draggable: false,
      minWidth: 130,
      cellTemplate: 'commonLinkCellTmpl'
    },
    {
      prop: 'effectiveFrom',
      name: 'MASTER_DATA.agent.columns.registerDate',
      resizeable: false,
      draggable: false,
      minWidth: 130,
      cellTemplate: 'dayMonthYearCellTmpl'
    },
    {
      prop: 'effectiveTo',
      name: 'MASTER_DATA.agent.columns.expiryDate',
      resizeable: false,
      draggable: false,
      minWidth: 130,
      cellTemplate: 'dayMonthYearCellTmpl'
    },
    {
      prop: 'locationType',
      name: 'MASTER_DATA.agent.columns.locationType',
      resizeable: false,
      draggable: false,
      minWidth: 70
    },
    {
      prop: 'active',
      name: 'MASTER_DATA.agent.columns.status',
      resizeable: false,
      draggable: false,
      minWidth: 130,
      cellTemplate: 'statusTmpl',
      sortable: false
    },
    {
      prop: 'cash',
      name: 'MASTER_DATA.agent.columns.cash',
      resizeable: false,
      draggable: false,
      width: 130,
      sortable: false,
      pipe: new PaymentStatusPipe()
    },
    {
      prop: 'easyPay',
      name: 'MASTER_DATA.agent.columns.easyPay',
      resizeable: false,
      draggable: false,
      width: 130,
      sortable: false,
      pipe: new PaymentStatusPipe()
    },
    {
      prop: 'paymentCard',
      name: 'MASTER_DATA.agent.columns.paymentCard',
      resizeable: false,
      draggable: false,
      width: 130,
      sortable: false,
      pipe: new PaymentStatusPipe()
    },
    {
      prop: 'parentIataCode',
      name: 'MASTER_DATA.agent.columns.parentIataCode',
      resizeable: false,
      draggable: false
    }
  ],
  SEARCH_FORM_DEFAULT_VALUE: {
    bspId: [],
    active: [],
    registerDate: [],
    expiryDate: [],
    agentCodeName: [],
    locationClass: [],
    locationType: [],
    remittanceFrequency: [],
    vatNumber: [],
    city: [],
    state: [],
    addressCountry: [],
    postalCode: [],
    email: [],
    telephone: [],
    fax: [],
    easyPay: [],
    cash: [],
    paymentCard: [],
    parentIataCode: [],
    locality: []
  }
};
