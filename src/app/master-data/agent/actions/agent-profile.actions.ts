import { createAction, props } from '@ngrx/store';

import { Agent } from '~app/master-data/models';

export const loadAgentSuccess = createAction('[Agent Profile] Load Global Agent Success', props<{ agent: Agent }>());
