import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, takeUntil, tap } from 'rxjs/operators';

import { getUserType } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AddressMaintenanceActions, fromAddressMaintenance } from '~app/master-data/address-maintenance/store';
import {
  canApplyChanges,
  canIpSettingsApplyChanges,
  IpAddressConfigurationActions,
  MenuBuilder,
  RoutedMenuItem,
  SettingConfigurationActions
} from '~app/master-data/configuration';
import { Agent } from '~app/master-data/models/agent.model';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import * as fromTicketingAuthority from '~app/master-data/ticketing-authority/store/reducers';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { Permissions } from '~app/shared/constants/permissions';
import { SelectMode } from '~app/shared/enums';
import { UserType } from '~app/shared/models/user.model';
import { or } from '~app/shared/utils';

@Component({
  selector: 'bspl-agent-profile',
  templateUrl: './agent-profile.component.html',
  styleUrls: ['./agent-profile.component.scss']
})
export class AgentProfileComponent implements OnInit, OnDestroy {
  public agent: Agent;

  public tabs: RoutedMenuItem[];
  public isApplyChangesVisible: boolean;
  public currentSavableTab = null;
  public isApplyChangesEnabled$: Observable<boolean>;

  public btnDesign = ButtonDesign;
  public selectMode = SelectMode;

  private loggedUserType$: Observable<UserType> = this.store.pipe(select(getUserType));
  private destroyed$ = new Subject();

  private ticketingAuthorityTab = {
    url: './ticketing-authority',
    canApplyChangesSelector: fromTicketingAuthority.canApplyChanges,
    applyChangesAction: TicketingAuthorityListActions.openApplyChanges(),
    applyChangesPermission: Permissions.updateTicketingAuthority
  };

  private emailAlertsSettingsTab = {
    url: './configuration/email-alerts-settings',
    canApplyChangesSelector: canApplyChanges,
    applyChangesAction: SettingConfigurationActions.openApplyChanges(),
    applyChangesPermission: Permissions.updateEmailAlerts
  };

  private addressMaintenanceTab = {
    url: './contact-info',
    canApplyChangesSelector: fromAddressMaintenance.canApplyChanges,
    applyChangesAction: AddressMaintenanceActions.openApplyChanges(),
    applyChangesPermission: Permissions.updateAgentAddressMaintenance
  };

  private notificationsSettingsTab = {
    url: './configuration/notifications',
    canApplyChangesSelector: canApplyChanges,
    applyChangesAction: SettingConfigurationActions.openApplyChanges(),
    applyChangesPermission: Permissions.updateNotificationSettings
  };

  private ipAddressSettingsTab = {
    url: './configuration/ip-address-settings',
    canApplyChangesSelector: canIpSettingsApplyChanges,
    applyChangesAction: IpAddressConfigurationActions.openApplyChanges(),
    applyChangesPermission: Permissions.updateIpAddressSettings
  };

  private savableTabs = [
    this.ticketingAuthorityTab,
    this.emailAlertsSettingsTab,
    this.addressMaintenanceTab,
    this.notificationsSettingsTab,
    this.ipAddressSettingsTab
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private menuBuilder: MenuBuilder,
    private permissionsService: PermissionsService
  ) {
    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab === savableTab)
        )
      )
    ).pipe(map(values => or(...values)));
  }

  public ngOnInit(): void {
    this.agent = this.activatedRoute.snapshot.data.agent;
    this.setTabs();
    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        tap(activeSavableTab => {
          this.isApplyChangesVisible = false;
          if (activeSavableTab != null) {
            this.isApplyChangesVisible = this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission);
          }

          this.currentSavableTab = activeSavableTab;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public applyChanges(): void {
    if (this.currentSavableTab) {
      this.store.dispatch(this.currentSavableTab.applyChangesAction);
    }
  }

  private setTabs(): void {
    this.loggedUserType$.pipe(distinctUntilChanged(), takeUntil(this.destroyed$)).subscribe(userType => {
      this.tabs = this.getTabs(userType);
    });
  }

  private getTabs(userType: UserType): RoutedMenuItem[] {
    const originalTabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    return userType === UserType.AIRLINE
      ? originalTabs.filter(tab => tab.route !== 'ticketing-authority')
      : originalTabs;
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
