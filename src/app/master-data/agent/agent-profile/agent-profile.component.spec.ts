import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuBuilder, SettingConfigurationActions } from '~app/master-data/configuration';
import { ChangesDialogService as SettingsChangesDialogService } from '~app/master-data/configuration/changes-dialog/changes-dialog.service';
import { ChangesDialogService as TaUnsavedChangesDialogService } from '~app/master-data/ticketing-authority';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import * as fromTicketingAuthority from '~app/master-data/ticketing-authority/store/reducers';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { UserType } from '~app/shared/models/user.model';
import { AgentProfileComponent } from './agent-profile.component';

describe('AgentProfileComponent', () => {
  let fixture: ComponentFixture<AgentProfileComponent>;
  let component: AgentProfileComponent;
  let permissionsServiceSpy: SpyObject<PermissionsService>;
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const taUnsavedChangesDialogServiceMock = createSpyObject(TaUnsavedChangesDialogService);
  const stUnsavedChangesDialogServiceMock = createSpyObject(SettingsChangesDialogService);
  taUnsavedChangesDialogServiceMock.confirmApplyChanges.and.returnValue(of(FooterButton.Cancel));
  stUnsavedChangesDialogServiceMock.confirmApplyChanges.and.returnValue(of(FooterButton.Cancel));
  let mockStore: MockStore<AppState>;

  const menuBuilderMock = jasmine.createSpyObj<MenuBuilder>('MenuBuilder', {
    buildMenuItemsFrom: [
      {
        route: '/master-data/agent/12578/ticketing-authority',
        title: 'menu.masterData.ticketingAuthority.title',
        isAccessible: true
      },
      {
        route: '/master-data/agent/12578/configuration/email-alerts-settings',
        title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
        isAccessible: true
      }
    ]
  });

  const activatedRouteStub = {
    snapshot: {
      data: {
        tab: ROUTES.AGENT_PROFILE
      }
    }
  };

  const initialState = {
    auth: {
      user: createIataUser()
    },
    core: {
      menu: {
        tabs: {}
      }
    },
    agent: {
      ticketingAuthority: {
        isChanged: false
      }
    },
    configuration: {}
  };

  let routerMock;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/ticketing-authority', './ticketing-authority')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './ticketing-authority',
      createUrlTree: () => {},
      isActive: (res = false) => res,
      isChanged: true
    };

    permissionsServiceSpy = createSpyObject(PermissionsService, { permissions$: of([]) });
    permissionsServiceSpy.hasPermission.and.returnValue(false);

    TestBed.configureTestingModule({
      declarations: [AgentProfileComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        provideMockStore({
          initialState,
          selectors: [{ selector: fromTicketingAuthority.canApplyChanges, value: false }]
        }),
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy },
        { provide: TaUnsavedChangesDialogService, useValue: taUnsavedChangesDialogServiceMock },
        { provide: SettingsChangesDialogService, useValue: stUnsavedChangesDialogServiceMock },
        { provide: MenuBuilder, useValue: menuBuilderMock },
        {
          provide: Router,
          useValue: routerMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should call taUnsavedChangesDialogService when you call applyChanges method in TA', () => {
    component.currentSavableTab = {
      url: './ticketing-authority',
      route: '/master-data/agent/12578/ticketing-authority',
      title: 'Ticketing Authority',
      isAccessible: true
    };

    component.applyChanges();

    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
  });

  it('should dispatch TA save action when applyChanges method is called', () => {
    spyOn(routerMock, 'isActive').and.returnValue(true);
    component.currentSavableTab = {
      url: 'TAUrl',
      applyChangesAction: TicketingAuthorityListActions.openApplyChanges()
    };

    component.applyChanges();

    expect(mockStore.dispatch).toHaveBeenCalledWith(TicketingAuthorityListActions.openApplyChanges());
  });

  it('should dispatch EAlert save action when applyChanges method is called', () => {
    spyOn(routerMock, 'isActive').and.returnValue(true);
    component.currentSavableTab = {
      url: 'EAlertUrl',
      applyChangesAction: SettingConfigurationActions.openApplyChanges()
    };

    component.applyChanges();

    expect(mockStore.dispatch).toHaveBeenCalledWith(SettingConfigurationActions.openApplyChanges());
  });

  it('should button ApplyChanges must not be visible if PermissionsService return false', () => {
    spyOn(routerMock, 'isActive').and.returnValue(true);
    component.currentSavableTab = {
      url: 'EAlertUrl',
      applyChangesPermission: 'AnyPermissions'
    };

    expect(component.isApplyChangesVisible).toBe(false);
  });

  it('should get all the tabs when getTabs method is triggered with airline user type', () => {
    const result = component['getTabs'](UserType.AIRLINE);
    const expectedResult = [
      {
        route: '/master-data/agent/12578/ticketing-authority',
        title: 'menu.masterData.ticketingAuthority.title',
        isAccessible: true
      },
      {
        route: '/master-data/agent/12578/configuration/email-alerts-settings',
        title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
        isAccessible: true
      }
    ];

    expect(result).toEqual(expectedResult);
  });

  it('should not dispatch the store when currentSavableTab is null and applyChanges is triggered', () => {
    component.currentSavableTab = null;

    component.applyChanges();
    expect(mockStore.dispatch).not.toHaveBeenCalled();
  });
});
