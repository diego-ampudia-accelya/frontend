import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Agent } from '~app/master-data/models';

@Component({
  selector: 'bspl-agent-properties',
  templateUrl: './agent-properties.component.html',
  styleUrls: ['./agent-properties.component.scss']
})
export class AgentPropertiesComponent implements OnInit {
  public agent: Agent;
  public billingLocalAddress: string;
  public registrationLocalAddress: string;
  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.agent = this.activatedRoute.parent.snapshot.data.agent;
    if (this.agent.localAgentInformation) {
      this.billingLocalAddress = `${this.agent.localAgentInformation?.billingLocalAddress}, ${this.agent.localAgentInformation?.billingLocalAddressPostCode}`;
      this.registrationLocalAddress = `${this.agent.localAgentInformation?.registrationLocalAddress}, ${this.agent.localAgentInformation?.registrationLocalAddressPostCode}`;
    }
  }
}
