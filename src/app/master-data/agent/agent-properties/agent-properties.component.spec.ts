import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { PaymentStatusPipe } from '../payment-status.pipe';

import { AgentPropertiesComponent } from './agent-properties.component';

describe('AgentPropertiesComponent', () => {
  let component: AgentPropertiesComponent;
  let fixture: ComponentFixture<AgentPropertiesComponent>;

  const activatedRouteSpy = {
    parent: {
      snapshot: {
        data: {
          agent: {
            localAgentInformation: {
              billingLocalAddress: 'Billing Address',
              billingLocalAddressPostCode: '12345',
              registrationLocalAddress: 'Registration Address',
              registrationLocalAddressPostCode: '10600'
            }
          }
        }
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AgentPropertiesComponent, PaymentStatusPipe],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [RouterTestingModule],
      providers: [{ provide: ActivatedRoute, useValue: activatedRouteSpy }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
