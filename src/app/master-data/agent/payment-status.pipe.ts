import { Pipe, PipeTransform } from '@angular/core';

import { PaymentStatus } from '~app/shared/enums/payment-status.enum';

@Pipe({
  name: 'paymentStatus',
  pure: true
})
export class PaymentStatusPipe implements PipeTransform {
  transform(value: PaymentStatus, args?: any): any {
    if (value && value.length > 0) {
      const valueAsArray = value.toLowerCase().split('_');

      return valueAsArray.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
    }

    // TODO Use this when we get back to checkboxes (Task FCA-1539)
    // return value && value !== PaymentStatus.NON_ACTIVE;
    return value;
  }
}
