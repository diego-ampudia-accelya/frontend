import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { first } from 'rxjs/operators';

import { AgentListService } from '../services';
import { AgentsDisplayFilterFormatter } from '../services/agent-display-filter-formatter';
import { AgentListComponent } from './agent-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AgentService } from '~app/master-data/services/agent.service';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { SortOrder } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService, AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

const query: DataQuery = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
};

describe('AgentListComponent', () => {
  let component: AgentListComponent;
  let fixture: ComponentFixture<AgentListComponent>;
  let mockStore: MockStore<AppState>;

  let permissionsServiceSpy: SpyObject<PermissionsService>;
  let appConfigurationServiceSpy: SpyObject<AppConfigurationService>;
  let routerSpy: SpyObject<Router>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let agentServiceSpy: SpyObject<AgentService>;
  let airlineDictionaryServiceSpy: SpyObject<AirlineDictionaryService>;
  let agentDictionaryServiceSpy: SpyObject<AgentDictionaryService>;
  let queryableDataSourceSpy: SpyObject<QueryableDataSource<AgentListService>>;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    core: {
      menu: {
        tabs: { dashboard: { ...ROUTES.DASHBOARD, id: 'dashboard' } },
        activeTabId: 'dashboard'
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    queryableDataSourceSpy = createSpyObject(QueryableDataSource);
    queryableDataSourceSpy.hasData$ = of(true);

    queryableDataSourceSpy.appliedQuery$ = of(query);
    queryableDataSourceSpy.data$ = of([]);
    queryableDataSourceSpy.get.and.returnValue(of(query));

    appConfigurationServiceSpy = createSpyObject(AppConfigurationService);

    routerSpy = createSpyObject(Router);

    dialogServiceSpy = createSpyObject(DialogService);

    agentServiceSpy = createSpyObject(AgentService);
    agentServiceSpy.getAll.and.returnValue(of(null));

    airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
    airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([{ value: { id: 1 }, label: 'Airline 1' }]));

    agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
    agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([{ value: { code: 'code1' }, label: 'Agent 1' }]));

    permissionsServiceSpy = createSpyObject(PermissionsService);

    TestBed.configureTestingModule({
      declarations: [AgentListComponent, TranslatePipeMock],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        provideMockStore({ initialState }),
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: AgentService, useValue: agentServiceSpy },
        mockProvider(AgentsDisplayFilterFormatter),
        mockProvider(DefaultQueryStorage),
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: PermissionsService, useValue: permissionsServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        {
          provide: AppConfigurationService,
          useValue: appConfigurationServiceSpy
        },
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        }
      ]
    })
      .overrideComponent(AgentListComponent, {
        set: {
          providers: [{ provide: QueryableDataSource, useValue: queryableDataSourceSpy }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentListComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject<any>(Store);
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should set a default sorting when loadData is triggered and there is not another sorting selected', () => {
    component.loadData({} as DataQuery);
    const expected = jasmine.objectContaining({
      sortBy: [{ attribute: 'iataCode', sortType: SortOrder.Asc }]
    });

    expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expected);
  });

  it('should not set a default sorting when loadData is triggered and there is a sorting selected', () => {
    component.loadData({ ...query, sortBy: [{ attribute: 'key', sortType: SortOrder.Desc }] });
    const expected = jasmine.objectContaining({
      sortBy: [{ attribute: 'key', sortType: SortOrder.Desc }]
    });

    expect(queryableDataSourceSpy.get).toHaveBeenCalledWith(expected);
  });

  it('should set initial form state', () => {
    const initialFormState = {
      active: null,
      registerDate: null,
      expiryDate: null,
      agentCodeName: null,
      locationClass: null,
      locationType: null,
      remittanceFrequency: null,
      vatNumber: null,
      city: null,
      state: null,
      addressCountry: null,
      postalCode: null,
      email: null,
      telephone: null,
      fax: null,
      easyPay: null,
      cash: null,
      paymentCard: null,
      parentIataCode: null,
      locality: null,
      bspId: null
    };

    component.searchForm = undefined;
    component.ngOnInit();
    expect(component.searchForm.value).toEqual(initialFormState);
  });

  it('should navigate to agent profile when onViewAgent called', () => {
    component.onViewAgent(0);
    expect(routerSpy.navigate).toHaveBeenCalledWith([0], { relativeTo: component['activeRoute'] });
  });

  it('should change showAdvancedSearch to true if default value is false when toggleAdvancedSearch is called', () => {
    component.showAdvancedSearch = false;

    component.toggleAdvancedSearch();

    expect(component.showAdvancedSearch).toBe(true);
  });

  it('should call dataSource get method with the query from loadData', () => {
    const dataQuery: DataQuery = { ...defaultQuery, sortBy: [{ attribute: 'name', sortType: SortOrder.Asc }] };

    component.loadData(dataQuery);

    expect(component.dataSource.get).toHaveBeenCalledWith(dataQuery);
  });

  it('should call router navigate with "id" and "ticketing-authority", if actionType is ManageTA when onActionClick is triggered', () => {
    component.onActionClick({
      action: { actionType: GridTableActionType.ManageTA },
      row: { id: 1 },
      event: {} as MouseEvent
    });

    expect(routerSpy.navigate).toHaveBeenCalledWith([1, 'ticketing-authority'], {
      relativeTo: component['activeRoute']
    });
  });

  it('should call router navigate with "id", if actionType is View when onActionClick is triggered', () => {
    component.onActionClick({
      action: { actionType: GridTableActionType.View },
      row: { id: 1 },
      event: {} as MouseEvent
    });

    expect(routerSpy.navigate).toHaveBeenCalledWith([1], {
      relativeTo: component['activeRoute']
    });
  });

  it('should call airlineDictionaryService getDropdownOptions when getAirlines is triggered', fakeAsync(() => {
    component.getAirlines().subscribe(result => {
      const airlineDropDownOption = result[0];

      expect(airlineDropDownOption).toBeTruthy();
      expect(airlineDropDownOption.value.displayValue).toBe('Airline 1');
      expect(airlineDropDownOption.value.id).toBe(1);
      expect(airlineDropDownOption.label).toBe('Airline 1');

      expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
    });
  }));

  it('should call dialog service open when download is triggered', fakeAsync(() => {
    component.download();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  }));

  it('should apply bsp default filter to query', () => {
    const mockBsp: Bsp = { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '01/01/2000' };
    component['defaultBsp'] = mockBsp;
    expect(component['applyPredefinedFilters'](query)).toEqual({
      ...query,
      filterBy: { ...query.filterBy, bspId: [mockBsp] }
    });
  });

  it('should trigger updateAgentFilterValue and updateAgentFilterOptions when updateAgentFilter is triggered', () => {
    spyOn(component as any, 'updateAgentFilterValue');
    spyOn(component as any, 'updateAgentFilterOptions');

    component['updateAgentFilter']();

    expect((component as any).updateAgentFilterValue).toHaveBeenCalled();
    expect((component as any).updateAgentFilterOptions).toHaveBeenCalled();
  });

  it('should update agent filter value correctly', () => {
    const mockForm = {
      value: {
        agentCodeName: {
          value: ['1254', '6']
        }
      }
    };
    const selectedBsps = [
      { id: 1, name: 'BSP 1', isoCountryCode: 'ES' },
      { id: 2, name: 'BSP 2', isoCountryCode: 'US' }
    ] as Bsp[];

    spyOn(FormUtil, 'get').and.returnValue(mockForm);

    component['selectedBsps'] = selectedBsps;
    component['updateAgentFilterValue']();

    expect(FormUtil.get).toHaveBeenCalledWith(component.searchForm, 'agentCodeName');
  });

  it('should set ticketing authority filters correctly for agent user type', () => {
    const rowMock = {
      id: 1,
      iataCode: 'codeIata',
      name: 'agentName',
      bsp: 'bsp'
    };
    const expectedRequiredFilter = {
      requiredFilter: Object({ agents: [Object({ id: 1, code: 'codeIata', name: 'agentName', bsp: 'bsp' })] }),
      type: '[Ticketing Authority] Change Required Filter'
    };

    spyOn(component['loggedUser$'], 'pipe').and.returnValue(of(UserType.AGENT));
    spyOn(mockStore, 'dispatch');

    component['userType'] = UserType.AGENT;
    component['setTicketingAuthorityFilters'](rowMock);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      TicketingAuthorityListActions.changeRequiredFilter(expectedRequiredFilter)
    );
  });

  it('should geRouteToTicketingAuthority', () => {
    const rowMock = {
      id: 1,
      iataCode: 'codeIata',
      name: 'agentName',
      bsp: 'bsp'
    };
    component['userType'] = UserType.AIRLINE;
    const result = component['geRouteToTicketingAuthority'](rowMock);

    expect(result).toEqual([ROUTES.MY_AIRLINE.url + '/:my-airline-id/ticketing-authority']);
  });

  it('should set selectedBsps correctly when selectedBsps array is empty', () => {
    const selectedBsps = [];

    component['selectedBsps'] = selectedBsps;

    expect(component['bspControl'].value).toBeNull();
  });

  it('should set selectedBsps correctly when selectedBsps array has one element', () => {
    const selectedBsps = [{ id: 1, name: 'BSP 1', isoCountryCode: 5 } as unknown] as Bsp[];

    component['selectedBsps'] = selectedBsps;

    expect(component['bspControl'].value).toEqual(selectedBsps[0]);
  });

  it('should initialize Lean BSP filter correctly when initializeLeanBspFilter$ is triggered', () => {
    const mockBspList: BspDto[] = [
      { id: 1, name: 'BSP 1', isoCountryCode: 'US' },
      { id: 2, name: 'BSP 2', isoCountryCode: 'CA' },
      { id: 3, name: 'BSP 3', isoCountryCode: 'ES' }
    ];
    const mockUser = {
      id: 1,
      name: 'nameUser',
      bspPermissions: [
        { bspId: 1, permissions: ['permission1'] },
        { bspId: 2, permissions: [] },
        { bspId: 3, permissions: ['permission1'] }
      ],
      defaultIsoc: 'ES'
    } as unknown as User;

    spyOn(mockStore, 'select').and.returnValues(of(mockBspList), of(mockUser));

    const result$ = component['initializeLeanBspFilter$']();

    result$.pipe(first()).subscribe(() => {
      expect(component.isBspFilterMultiple).toBeFalsy();
      expect(component.isBspFilterLocked).toBeFalsy();
    });
  });

  it('should return the correct DataQuery when selectedBsps is not empty', done => {
    const mockSelectedBsps = [{ id: 1, name: 'bspName1' }] as Bsp[];

    spyOn<any>(component, 'initializeLeanBspFilter$').and.returnValue(of(null));
    component['selectedBsps'] = mockSelectedBsps;
    const expectedQuery = {
      filterBy: { bspId: mockSelectedBsps },
      paginateBy: Object({ page: 0, size: 20, totalElements: 0 }),
      sortBy: []
    } as DataQuery;

    const result$ = component['getQuery$']();

    result$.subscribe(data => {
      expect(data).toEqual(expectedQuery);
      done();
    });
  });

  it('should apply default filters when selectedBsps is empty and defaultBsp is set', () => {
    const mockDefaultBsp = { id: 1, name: 'BSP 1', isoCountryCode: 'ES' };
    const mockQuery = {
      filterBy: {},
      paginateBy: Object({ page: 0, size: 20, totalElements: 0 }),
      sortBy: []
    } as DataQuery;

    component['defaultBsp'] = mockDefaultBsp;
    component['selectedBsps'] = [mockDefaultBsp];

    const expectedQuery: DataQuery = {
      filterBy: { bspId: [mockDefaultBsp] },
      paginateBy: Object({ page: 0, size: 20, totalElements: 0 }),
      sortBy: []
    };

    const result = component['applyPredefinedFilters'](mockQuery);

    expect(result).toEqual(expectedQuery);
  });
});
