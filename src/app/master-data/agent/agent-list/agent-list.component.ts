import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { AGENTS } from '../agents.constants';
import { AgentListService } from '../services';
import { AgentsDisplayFilterFormatter } from '../services/agent-display-filter-formatter';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Agent, AgentFilter } from '~app/master-data/models';
import { AgentService } from '~app/master-data/services';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { countryList } from '~app/shared/constants/country-list';
import { paymentStatuses } from '~app/shared/constants/payment-statuses';
import { LocationClass, LocationType, RemittanceFrequency, SortOrder } from '~app/shared/enums';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp, toValueLabelObjectDictionary } from '~app/shared/helpers';
import { AgentSummary } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.scss'],
  providers: [
    AgentListService,
    AgentService,
    AgentsDisplayFilterFormatter,
    DefaultQueryStorage,
    { provide: QUERYABLE, useClass: AgentService },
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [AgentService]
    }
  ]
})
export class AgentListComponent implements OnInit, OnDestroy {
  public columns: TableColumn[];
  public searchForm: FormGroup;
  public showAdvancedSearch = false;
  public actions: Array<{ action: GridTableActionType; hidden?: boolean }>;

  public airlineCodesList$: Observable<DropdownOption[]>;
  public agentCodesList$: Observable<DropdownOption[]> = of([]);
  public addressCountriesList$: Observable<DropdownOption[]>;
  public paymentCardStatuses$: Observable<DropdownOption[]>;
  public cashFormStatuses$: Observable<DropdownOption[]>;
  public easyPayStatuses$: Observable<DropdownOption[]>;
  public locationClassList: DropdownOption[];
  public locationTypeList: DropdownOption[];
  public remittanceFrequencyList: DropdownOption[];
  public userBsps: DropdownOption<BspDto>[];

  public noticeMessage: string;

  public taStatusOptions = [
    {
      value: 'enabled',
      label: this.translationService.translate('MASTER_DATA.agent.taStatusOptions.enabled')
    },
    {
      value: 'disabled',
      label: this.translationService.translate('MASTER_DATA.agent.taStatusOptions.disabled')
    }
  ];
  public activeOptions: DropdownOption[] = [
    {
      value: true,
      label: this.translationService.translate('MASTER_DATA.agent.activeOptions.active')
    },
    {
      value: false,
      label: this.translationService.translate('MASTER_DATA.agent.activeOptions.inactive')
    }
  ];

  public header = this.translationService.translate('agent.viewTitle');
  public isAirlineUser$ = this.store.select(fromAuth.isAirlineUser);
  private formFactory: FormUtil;

  public isBspFilterLocked: boolean;
  public isBspFilterMultiple: boolean;
  private defaultBsp: Bsp;
  private bspControl: FormControl;
  private get selectedBsps(): Array<Bsp> {
    const bspControlValue = this.bspControl.value || [];

    return Array.isArray(bspControlValue) ? bspControlValue : [bspControlValue];
  }
  private set selectedBsps(selectedBsps: Bsp[]) {
    if (selectedBsps.length === 0) {
      return;
    }
    this.bspControl.setValue(this.isBspFilterMultiple ? selectedBsps : selectedBsps[0]);
  }
  private hasLeanPermission: boolean;
  private hasReadTicketingAuthorityPermission: boolean;

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(fromAuth.getUser), first());
  }
  private userType: UserType;

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<AgentService>,
    public filterFormatter: AgentsDisplayFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private agentListService: AgentListService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private agentService: AgentService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private agentDictionaryService: AgentDictionaryService,
    private airlineDictionaryService: AirlineDictionaryService,
    private permissionsService: PermissionsService,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.columns = this.agentListService.generateColumns();

    this.buildSearchForm();
    this.populateDropdownOptions();

    this.initializePermissions();
    this.getQuery$().subscribe(query => this.loadData(query));

    this.initializeBspControlListener();
    this.initializeNoticeMessage();
  }

  private initializeLoggedUser() {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.userType = loggedUser.userType;
      this.initializeActions();
    });
  }

  public loadData(query: DataQuery): void {
    const adaptedQuery = this.adaptQuery(query);
    this.dataSource.get(adaptedQuery);
    this.queryStorage.save(adaptedQuery);
  }

  public onViewAgent(id: number): void {
    this.router.navigate([id], { relativeTo: this.activeRoute });
  }

  private setTicketingAuthorityFilters(row: any): void {
    if (this.userType !== UserType.AGENT && this.userType !== UserType.AIRLINE) {
      return;
    }

    const agent: AgentSummary = {
      id: row.id,
      code: row.iataCode,
      name: row.name,
      bsp: row.bsp
    };
    const requiredFilter = { agents: [agent] };
    // With this actions the List query is updated with the agent filter
    this.store.dispatch(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));
  }

  public onActionClick({ action, row }: { action: any; row: any; event: MouseEvent }) {
    if (action.actionType === GridTableActionType.ManageTA || action.actionType === GridTableActionType.ViewTA) {
      this.setTicketingAuthorityFilters(row);

      const route = this.geRouteToTicketingAuthority(row);
      this.router.navigate(route, { relativeTo: this.activeRoute });
    }

    if (action.actionType === GridTableActionType.View) {
      this.onViewAgent(row.id);
    }
  }

  public toggleAdvancedSearch() {
    this.showAdvancedSearch = !this.showAdvancedSearch;
  }

  public getAirlines() {
    return this.airlineDictionaryService.getDropdownOptions().pipe(
      map(airlineSummary =>
        airlineSummary.map(airline => ({
          value: {
            displayValue: airline.label,
            id: airline.value.id
          },
          label: airline.label
        }))
      )
    );
  }

  public download() {
    const requestQuery = this.queryStorage.get();

    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: requestQuery
      },
      apiService: this.agentService
    });
  }

  private geRouteToTicketingAuthority(row: any): string[] {
    return this.userType === UserType.AIRLINE
      ? [ROUTES.MY_AIRLINE.url + '/:my-airline-id/ticketing-authority']
      : [row.id, 'ticketing-authority'];
  }

  private adaptQuery(query: DataQuery): DataQuery {
    if (isEmpty(query.sortBy)) {
      const sortBy = [{ attribute: 'iataCode', sortType: SortOrder.Asc }];
      query = { ...query, sortBy };
    }

    return this.applyPredefinedFilters(query);
  }

  private applyPredefinedFilters(query: DataQuery): DataQuery {
    let adaptedQuery = query;

    let bspFilter: Bsp[];
    if (this.selectedBsps.length) {
      bspFilter = this.selectedBsps;
    } else if (this.defaultBsp) {
      bspFilter = [this.defaultBsp];
    }

    if (bspFilter) {
      adaptedQuery = { ...query, filterBy: { ...query.filterBy, bspId: bspFilter } };
    }

    return adaptedQuery;
  }

  private buildSearchForm() {
    this.bspControl = new FormControl();

    this.searchForm = this.formFactory.createGroup<AgentFilter>({
      ...AGENTS.SEARCH_FORM_DEFAULT_VALUE,
      bspId: this.bspControl
    });
  }

  private getQuery$(): Observable<DataQuery> {
    return this.initializeLeanBspFilter$().pipe(
      map(() => {
        let query = this.queryStorage.get() || defaultQuery;

        if (this.selectedBsps?.length) {
          query = { ...query, filterBy: { ...query.filterBy, bspId: this.selectedBsps } };
        }

        return query;
      })
    );
  }

  private initializeBspControlListener(): void {
    const bspControl = FormUtil.get<AgentFilter>(this.searchForm, 'bspId');

    // We want to wait until the list is ready to start listening for changes
    this.dataSource.data$
      .pipe(
        first(),
        switchMap(() => bspControl.valueChanges.pipe(takeUntil(this.destroy$)))
      )
      .subscribe(() => {
        // If select is not multiple, it won't accept the array value coming from QueryStorage so we need to set the single Bsp
        if (!this.isBspFilterMultiple && this.selectedBsps.length) {
          bspControl.setValue(this.selectedBsps[0], { emitEvent: false });
        }

        if (this.selectedBsps.length) {
          // If there are selected bsps, we need to update both options and values for agent filter
          this.updateAgentFilter();
        } else {
          // If no bsp is selected, we just need to update the list of options for agent filter
          this.updateAgentFilterOptions();
        }
      });
  }

  private updateAgentFilter(): void {
    this.updateAgentFilterValue();
    this.updateAgentFilterOptions();
  }

  private updateAgentFilterValue(): void {
    const agentControl = FormUtil.get<AgentFilter>(this.searchForm, 'agentCodeName');
    const selectedAgents = agentControl.value as Agent[];
    if (selectedAgents?.length) {
      const filteredAgents = selectedAgents.filter(agent =>
        this.selectedBsps.map(bsp => bsp.id).includes(agent.bsp.id)
      );
      agentControl.patchValue(filteredAgents);
    }
  }

  private updateAgentFilterOptions(): void {
    const bspFilter = this.selectedBsps.length ? { bspId: this.selectedBsps.map(bsp => bsp.id) } : null;
    this.agentCodesList$ = this.agentDictionaryService
      .getDropdownOptions(bspFilter)
      .pipe(map(toValueLabelObjectDictionary));
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.hasReadTicketingAuthorityPermission = this.permissionsService.hasPermission(
      Permissions.readTicketingAuthority
    );
    this.initializeLoggedUser();
  }

  private initializeActions(): void {
    this.actions = [
      { action: GridTableActionType.View },
      {
        action: this.userType === UserType.GDS ? GridTableActionType.ViewTA : GridTableActionType.ManageTA,
        hidden:
          !this.hasReadTicketingAuthorityPermission ||
          (this.userType !== UserType.IATA && this.userType !== UserType.GDS)
      }
    ];
  }

  private initializeNoticeMessage(): void {
    this.store
      .select(fromAuth.isAirlineUser)
      .pipe(first())
      .subscribe(isAirline => (this.noticeMessage = isAirline ? 'MASTER_DATA.agent.alertMessage' : null));
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return combineLatest([this.store.select(fromAuth.getUserBsps), this.store.select(fromAuth.getUser)]).pipe(
      first(),
      map(([bspList, user]) => {
        // Filtering bsp list depending on permissions
        const requiredPermissions = [Permissions.readAgent];

        const filteredBsps = bspList.filter(bsp => {
          const bspPermissions = user.bspPermissions?.find(
            bspPermission => bspPermission.bspId === bsp.id
          )?.permissions;

          return bspPermissions && requiredPermissions.some(permission => bspPermissions.includes(permission));
        });

        return [filteredBsps, user] as [Bsp[], User];
      }),
      tap(([bspList, user]) => {
        // Selecting default Bsp
        if (bspList.length === 1) {
          this.defaultBsp = bspList[0];
        }
        if (!this.hasLeanPermission && bspList.length > 1) {
          this.defaultBsp = bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
        }

        if (this.defaultBsp) {
          this.selectedBsps = [this.defaultBsp];
        }
      }),
      map(([bspList]) => bspList.map(bsp => toValueLabelObjectBsp(bsp))),
      tap(bspList => (this.userBsps = bspList)),
      tap(bspList => (this.isBspFilterMultiple = this.hasLeanPermission && bspList.length !== 1)),
      tap(bspList => (this.isBspFilterLocked = bspList.length === 1))
    );
  }

  private populateDropdownOptions() {
    this.airlineCodesList$ = this.getAirlines();
    this.addressCountriesList$ = of(countryList.map(country => ({ value: country.name, label: country.name })));
    this.paymentCardStatuses$ = of(paymentStatuses);
    this.cashFormStatuses$ = of(paymentStatuses);
    this.easyPayStatuses$ = of(paymentStatuses);
    this.locationClassList = ArrayHelper.enumToDropdownValue(LocationClass);
    this.locationTypeList = ArrayHelper.enumToDropdownValue(LocationType);
    this.remittanceFrequencyList = ArrayHelper.enumToDropdownValue(RemittanceFrequency);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
