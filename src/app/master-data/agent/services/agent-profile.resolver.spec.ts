import { TestBed } from '@angular/core/testing';

import { AgentProfileResolver } from './agent-profile.resolver';
import { AgentService } from '~app/master-data/services/agent.service';

describe('AgentProfileResolver', () => {
  let resolver: AgentProfileResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AgentProfileResolver,
        {
          provide: AgentService,
          useValue: jasmine.createSpyObj('agentService', ['getAgentById'])
        }
      ]
    });

    resolver = TestBed.inject(AgentProfileResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
