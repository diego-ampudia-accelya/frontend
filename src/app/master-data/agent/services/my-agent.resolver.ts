import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models/agent.model';
import { AppState } from '~app/reducers';
import { AgentUser, UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class MyAgentResolver implements Resolve<Agent> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<Agent> {
    return this.store.pipe(
      select(getUser),
      filter(user => !!user),
      first(),
      map((user: AgentUser) => {
        if (user.userType !== UserType.AGENT) {
          throw new Error('Access Denied.');
        }

        return user.agent;
      })
    );
  }
}
