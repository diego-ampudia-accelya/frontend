import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

@Injectable()
export class AgentsDisplayFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translationService: L10nTranslationService) {
    super();
  }

  public format(filter: any): AppliedFilter[] {
    const { active, registerDate, expiryDate, bspId, agentCodeName, ...rest } = filter;
    const formattedFilters = super.format(rest);

    if (active != null) {
      const activeLabel = this.translationService.translate('MASTER_DATA.agent.activeOptions.active');
      const inactiveLabel = this.translationService.translate('MASTER_DATA.agent.activeOptions.inactive');
      const statusValue = active ? activeLabel : inactiveLabel;

      formattedFilters.push({
        label: `${this.translationService.translate('MASTER_DATA.agent.status')} - ${statusValue}`,
        keys: ['active']
      });
    }

    if (registerDate) {
      const registerDateLabel = this.translationService.translate('MASTER_DATA.agent.registerDate');

      formattedFilters.push({
        label: `${registerDateLabel} - ${rangeDateFilterTagMapper(registerDate)}`,
        keys: ['registerDate']
      });
    }

    if (expiryDate) {
      const expiryDateLabel = this.translationService.translate('MASTER_DATA.agent.expiryDate');

      formattedFilters.push({
        label: `${expiryDateLabel} - ${rangeDateFilterTagMapper(expiryDate)}`,
        keys: ['expiryDate']
      });
    }

    if (bspId) {
      const bspIds = Array.isArray(bspId) ? bspId : [bspId];
      const bspIdLabel = this.translationService.translate('MASTER_DATA.agent.isoc');
      const bspMapper = bsp => `${bsp.name} (${bsp.isoCountryCode})`;

      formattedFilters.push({
        label: `${bspIdLabel} - ${bspIds.map(bspMapper).join(', ')}`,
        keys: ['bspId']
      });
    }

    if (agentCodeName) {
      const agentCodeNameLabel = this.translationService.translate('MASTER_DATA.agent.agentCodeFilter');

      formattedFilters.push({
        label: `${agentCodeNameLabel} - ${agentCodeName.join(', ')}`,
        keys: ['agentCodeName']
      });
    }

    return formattedFilters;
  }
}
