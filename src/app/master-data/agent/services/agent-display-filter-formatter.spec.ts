import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { AgentsDisplayFilterFormatter } from './agent-display-filter-formatter';

describe('AgentsDisplayFilterFormatter', () => {
  let formatter: AgentsDisplayFilterFormatter;
  let translationServiceSpy: jasmine.SpyObj<L10nTranslationService>;

  beforeEach(() => {
    const translationServiceMock = jasmine.createSpyObj('L10nTranslationService', ['translate']);

    TestBed.configureTestingModule({
      providers: [AgentsDisplayFilterFormatter, { provide: L10nTranslationService, useValue: translationServiceMock }],
      schemas: [NO_ERRORS_SCHEMA]
    });

    formatter = TestBed.inject(AgentsDisplayFilterFormatter);
    translationServiceSpy = TestBed.inject(L10nTranslationService) as jasmine.SpyObj<L10nTranslationService>;
  });

  it('should create', () => {
    expect(formatter).toBeTruthy();
  });

  it('should format the filter correctly', () => {
    translationServiceSpy.translate.and.returnValue('TranslatedValueMock');

    const filter = {
      active: true,
      registerDate: '2022-01-01',
      expiryDate: '2022-12-31',
      bspId: [
        {
          name: 'bspName',
          isoCountryCode: 'ES'
        }
      ],
      agentCodeName: ['codeName1', 'codeName2']
    };

    const formattedFilters = formatter.format(filter);

    expect(formattedFilters).toEqual([
      {
        label: 'TranslatedValueMock - TranslatedValueMock',
        keys: ['active']
      },
      {
        label: 'TranslatedValueMock - 01/02/2001 - 01/01/2000',
        keys: ['registerDate']
      },
      {
        label: 'TranslatedValueMock - 01/02/2001 - 01/01/2000',
        keys: ['expiryDate']
      },
      {
        label: 'TranslatedValueMock - bspName (ES)',
        keys: ['bspId']
      },
      {
        label: 'TranslatedValueMock - codeName1, codeName2',
        keys: ['agentCodeName']
      }
    ]);
    expect(translationServiceSpy.translate).toHaveBeenCalledWith('MASTER_DATA.agent.activeOptions.active');
  });

  it('should not format the filters when there are not filters selected', () => {
    const formattedFilters = formatter.format([]);

    expect(formattedFilters).toEqual([]);
  });
});
