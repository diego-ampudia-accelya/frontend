import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

import { TicketingAuthorityResolver } from './ticketing-authority.resolver';
import { Agent } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';

describe('TicketingAuthorityResolver', () => {
  let resolver: TicketingAuthorityResolver;
  let mockStore: jasmine.SpyObj<Store<AppState>>;
  let mockAgentService: jasmine.SpyObj<AgentDictionaryService>;
  const route: ActivatedRouteSnapshot = jasmine.createSpyObj<ActivatedRouteSnapshot>('ActivatedRouteSnapshot', [
    'params',
    'root',
    'parent'
  ]);

  route.parent.data = {
    agent: {
      id: 1,
      iataCode: 'code',
      bsp: null
    } as Agent
  };

  beforeEach(() => {
    const storeSpy = jasmine.createSpyObj('Store', ['pipe', 'dispatch', 'subscribe']);
    const agentServiceSpy = jasmine.createSpyObj('AgentDictionaryService', ['summarize']);

    TestBed.configureTestingModule({
      providers: [
        TicketingAuthorityResolver,
        { provide: Store, useValue: storeSpy },
        { provide: AgentDictionaryService, useValue: agentServiceSpy }
      ]
    });

    resolver = TestBed.inject(TicketingAuthorityResolver);
    mockStore = TestBed.inject(Store) as jasmine.SpyObj<Store<AppState>>;
    mockAgentService = TestBed.inject(AgentDictionaryService) as jasmine.SpyObj<AgentDictionaryService>;
  });

  it('should create', () => {
    expect(resolver).toBeTruthy();
    expect(mockStore).toBeTruthy();
    expect(mockAgentService).toBeTruthy();
  });

  it('should triggered dispatch from Store when the resolver is triggered', () => {
    mockStore.pipe.and.returnValue(of([]));
    resolver.resolve(route);

    expect(mockStore.dispatch).toHaveBeenCalled();
  });
});
