import { Injectable } from '@angular/core';

import { Agent } from '~app/master-data/models/agent.model';
import { AgentService } from '~app/master-data/services/agent.service';
import { EntityResolverService } from '~app/shared/services/entity-resolver.service';

@Injectable({
  providedIn: 'root'
})
export class AgentProfileResolver extends EntityResolverService<Agent> {
  constructor(apiService: AgentService) {
    super(apiService);
  }
}
