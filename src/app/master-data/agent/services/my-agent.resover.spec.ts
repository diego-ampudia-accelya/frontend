import { TestBed, waitForAsync } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';

import { MyAgentResolver } from './my-agent.resolver';
import { State } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models/agent.model';
import { AgentUser, User, UserType } from '~app/shared/models/user.model';

describe('MyAgentResolver', () => {
  let mockStore: MockStore<State>;
  let resolver: MyAgentResolver;
  let initialState: State;

  beforeEach(waitForAsync(() => {
    initialState = {
      router: null,
      auth: {
        user: null
      }
    };

    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), MyAgentResolver]
    });

    mockStore = TestBed.inject<any>(Store);
    resolver = TestBed.inject(MyAgentResolver);
  }));

  it('should resolve agent when current user is an agent user', waitForAsync(() => {
    const agent = {
      name: 'Agent Smith'
    } as Agent;
    const validUser = {
      userType: UserType.AGENT,
      agent
    } as AgentUser;
    mockStore.setState({
      ...initialState,
      auth: { user: validUser }
    });

    const expected = cold('(a|)', { a: agent });
    expect(resolver.resolve()).toBeObservable(expected);
  }));

  it('should not resolve agent when user information is not available', waitForAsync(() => {
    const actual = resolver.resolve();

    const expected = cold('-');
    expect(actual).toBeObservable(expected);
  }));

  it('should throw error when user is not of type agent', () => {
    const iataUser = {
      userType: UserType.IATA
    } as User;

    mockStore.setState({
      ...initialState,
      auth: { user: iataUser }
    });

    const expected = cold('#', null, new Error('Access Denied.'));
    expect(resolver.resolve()).toBeObservable(expected);
  });
});
