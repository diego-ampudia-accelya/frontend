import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Store } from '@ngrx/store';

import { RefundAuthorityFilter, ViewAs } from '~app/master-data/refund-authority/models';
import { RefundAuthorityListActions } from '~app/master-data/refund-authority/store/actions';
import { AppState } from '~app/reducers';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class RefundAuthorityRequiredFilterResolver implements Resolve<RefundAuthorityFilter> {
  constructor(private store: Store<AppState>, private agentService: AgentDictionaryService) {}

  resolve(route: ActivatedRouteSnapshot): RefundAuthorityFilter {
    const agent = route.parent.data.agent;
    const requiredFilter = agent && {
      agents: [this.agentService.summarize(agent)],
      viewAs: ViewAs.Agent,
      bsp: agent.bsp
    };

    this.store.dispatch(RefundAuthorityListActions.changeRequiredFilter({ requiredFilter }));

    return requiredFilter;
  }
}
