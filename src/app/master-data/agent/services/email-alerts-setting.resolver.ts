import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { AgentUser } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class EmailAlertsSettingResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getUser),
      first(),
      tap((userAgent: AgentUser) => {
        this.store.dispatch(
          SettingConfigurationActions.load({
            scope: {
              scopeType: 'users',
              scopeId: `${userAgent.id}`,
              name: userAgent.agent?.name,
              service: 'user-profile-management'
            }
          })
        );
      }),
      mapTo(null)
    );
  }
}
