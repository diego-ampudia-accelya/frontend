import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { IpAddressConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { AgentUser } from '~app/shared/models/user.model';

@Injectable()
export class AgentIpAddressSettingsResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getUser),
      first(),
      tap((userAgent: AgentUser) => {
        this.store.dispatch(
          IpAddressConfigurationActions.load({
            url: `user-management/users/bsps/${userAgent.bsps[0].id}/accepted-ips`
          })
        );
      }),
      mapTo(null)
    );
  }
}
