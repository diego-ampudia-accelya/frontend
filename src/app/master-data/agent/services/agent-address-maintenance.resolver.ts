import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AddressMaintenanceResourceType } from '~app/master-data/address-maintenance/models/address-maintenance.model';
import { AddressMaintenanceService } from '~app/master-data/address-maintenance/services/address-maintenance.service';
import { AddressMaintenanceActions } from '~app/master-data/address-maintenance/store';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { AgentUser } from '~app/shared/models/user.model';
import { CustomRouterSateSerializer } from '~app/shared/utils';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class AgentAddressMaintenanceResolver implements Resolve<any> {
  constructor(
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private addressMaintenanceService: AddressMaintenanceService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const { id } = new CustomRouterSateSerializer().getParamsFrom(route);
    const resourceType = AddressMaintenanceResourceType.agent;
    const isReadonlyMode = !this.permissionsService.hasPermission(Permissions.updateAgentAddressMaintenance);

    return this.store.pipe(
      select(getUser),
      first(),
      tap((userAgent: AgentUser) => {
        const resourceId = id || userAgent.agent.id.toString();

        this.store.dispatch(AddressMaintenanceActions.load({ resourceType, resourceId, isReadonlyMode }));
      }),
      switchMap(() => this.addressMaintenanceService.get()),
      map(addressMaintenance =>
        this.store.dispatch(AddressMaintenanceActions.loadSuccess({ value: addressMaintenance }))
      ),
      rethrowError(() => AddressMaintenanceActions.loadError())
    );
  }
}
