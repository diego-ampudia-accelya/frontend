import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { EmailAlertsSettingResolver } from './email-alerts-setting.resolver';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { createAgentUser } from '~app/shared/mocks/agent-user';

describe('Email Alerts Setting Resolver', () => {
  const initialState = {
    auth: {
      user: createAgentUser()
    }
  };

  let emailAlertsSettingResolver: EmailAlertsSettingResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), EmailAlertsSettingResolver]
    });

    emailAlertsSettingResolver = TestBed.inject(EmailAlertsSettingResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(emailAlertsSettingResolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    emailAlertsSettingResolver.resolve().subscribe();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'users',
          scopeId: '10000',
          name: 'German Travel Service',
          service: 'user-profile-management'
        }
      })
    );
  }));
});
