import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { first } from 'rxjs/operators';

import { TicketingAuthorityFilter } from '~app/master-data/ticketing-authority';
import { TicketingAuthorityListActions } from '~app/master-data/ticketing-authority/store/actions';
import * as fromTicketingAuthority from '~app/master-data/ticketing-authority/store/reducers';
import { AppState } from '~app/reducers';
import { AirlineSummary } from '~app/shared/models';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class TicketingAuthorityResolver implements Resolve<TicketingAuthorityFilter> {
  constructor(private store: Store<AppState>, private agentService: AgentDictionaryService) {}

  resolve(route: ActivatedRouteSnapshot): TicketingAuthorityFilter {
    const agent = route.parent.data.agent;
    let airlines: AirlineSummary[] = [];

    this.store.pipe(first(), select(fromTicketingAuthority.getRequiredFilter)).subscribe(filter => {
      airlines = filter?.airlines;
    });

    const requiredFilter = agent && {
      agents: [this.agentService.summarize(agent)],
      airlines
    };

    this.store.dispatch(TicketingAuthorityListActions.changeRequiredFilter({ requiredFilter }));

    return requiredFilter;
  }
}
