import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { RefundAuthorityRequiredFilterResolver } from './refund-authority-required-filter.resolver';
import { ViewAs } from '~app/master-data/refund-authority/models';
import { AppState } from '~app/reducers';
import { AgentDictionaryService } from '~app/shared/services/dictionary/agent-dictionary.service';
import { AgentSummary } from '~app/shared/models/dictionary/agent-summary.model';

describe('RefundAuthorityRequiredFilterResolver', () => {
  let resolver: RefundAuthorityRequiredFilterResolver;
  let mockStore: jasmine.SpyObj<Store<AppState>>;
  let mockAgentService: jasmine.SpyObj<AgentDictionaryService>;

  beforeEach(() => {
    const storeSpy = jasmine.createSpyObj('Store', ['dispatch']);
    const agentServiceSpy = jasmine.createSpyObj('AgentDictionaryService', ['summarize']);

    TestBed.configureTestingModule({
      providers: [
        RefundAuthorityRequiredFilterResolver,
        { provide: Store, useValue: storeSpy },
        { provide: AgentDictionaryService, useValue: agentServiceSpy }
      ]
    });

    resolver = TestBed.inject(RefundAuthorityRequiredFilterResolver);
    mockStore = TestBed.inject(Store) as jasmine.SpyObj<Store<AppState>>;
    mockAgentService = TestBed.inject(AgentDictionaryService) as jasmine.SpyObj<AgentDictionaryService>;
  });

  it('should create', () => {
    expect(resolver).toBeTruthy();
    expect(mockStore).toBeTruthy();
    expect(mockAgentService).toBeTruthy();
  });

  it('should resolve the RefundAuthorityFilter with required filter when agent is available', () => {
    const routeSnapshot = {
      parent: {
        data: {
          agent: {}
        } as unknown
      }
    } as ActivatedRouteSnapshot;
    const mockAgentSummary = {
      id: '1',
      name: 'agentName',
      code: 'code1'
    } as AgentSummary;
    mockAgentService.summarize.and.returnValue(mockAgentSummary);

    const result = resolver.resolve(routeSnapshot);

    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(result).toEqual({
      agents: [mockAgentSummary],
      viewAs: ViewAs.Agent,
      bsp: undefined
    });
  });
});
