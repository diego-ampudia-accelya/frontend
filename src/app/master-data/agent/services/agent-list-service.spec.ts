import { inject, TestBed } from '@angular/core/testing';
import { TableColumn } from '@swimlane/ngx-datatable';

import { AgentListService } from './agent-list.service';
import { Agent } from '~app/master-data/models';

describe('Service: AgentList', () => {
  let service: AgentListService;

  const agentRowsMock = [
    {
      id: 1,
      iataCode: 'code',
      name: 'agentName',
      bsp: {
        id: 1,
        name: 'bspName',
        isoCountryCode: 'ES'
      }
    }
  ] as Agent[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgentListService]
    });
  });

  beforeEach(inject([AgentListService], (agentListService: AgentListService) => {
    service = agentListService;
  }));

  it('should initialize', () => {
    expect(service).toBeTruthy();
  });

  describe('formatRequestFilters', () => {
    it('should format basic filter fields', () => {
      const data = {
        active: true,
        agentCodeName: ['0206004'],
        expiryDate: [new Date('2020-08-09'), new Date('2020-09-10')],
        registerDate: [new Date('2020-09-10'), new Date('2020-10-10')]
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'active', operation: '=', attributeValue: true },
        { attribute: 'iataCode', operation: 'in', attributeValue: '0206004' },
        { attribute: 'effectiveTo', operation: '>', attributeValue: '2020-08-09' },
        { attribute: 'effectiveTo', operation: '<', attributeValue: '2020-09-10' },
        { attribute: 'effectiveFrom', operation: '>', attributeValue: '2020-09-10' },
        { attribute: 'effectiveFrom', operation: '<', attributeValue: '2020-10-10' }
      ]);
    });

    it('should generate and clone columns', () => {
      const result: Array<TableColumn> = service.generateColumns();

      expect(result).toBeDefined();
    });

    it('should generate countries when generateCountries is triggered', () => {
      const result = service.generateCountries(agentRowsMock);

      expect(result).toEqual([{ value: 'bspName', label: 'bspName' }]);
    });

    it('should populate filterss when populateFilters is triggered', () => {
      const result = service.populateFilters(agentRowsMock);

      expect(result).toEqual({
        agentCodes: [{ value: 'code', label: 'code' }],
        countries: [{ value: 'bspName', label: 'bspName' }]
      });
    });

    it('should format advanced search fields', () => {
      const data = {
        locationClass: ['A'],
        locationType: ['HE'],
        remittanceFrequency: ['W'],
        vatNumber: 'vatNumber',
        city: 'Sofia',
        state: 'Sofia',
        addressCountry: ['Bulgaria'],
        postalCode: '2300',
        email: 'agent@example.com',
        telephone: '35988742536',
        fax: '256',
        cash: ['NON_ACTIVE'],
        paymentCard: ['NON_ACTIVE'],
        easyPay: ['NON_ACTIVE'],
        locality: 'testing',
        parentIataCode: '12',
        bspId: ['1']
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        {
          attribute: 'locationClass',
          operation: 'in',
          attributeValue: 'A'
        },
        {
          attribute: 'locationType',
          operation: 'in',
          attributeValue: 'HE'
        },
        {
          attribute: 'remittanceFrequency',
          operation: 'in',
          attributeValue: 'W'
        },
        {
          attribute: 'vatNumber',
          operation: 'like',
          attributeValue: 'vatNumber'
        },
        {
          attribute: 'address.city',
          operation: 'like',
          attributeValue: 'Sofia'
        },
        {
          attribute: 'address.state',
          operation: 'like',
          attributeValue: 'Sofia'
        },
        {
          attribute: 'address.country',
          operation: 'in',
          attributeValue: 'Bulgaria'
        },
        {
          attribute: 'address.postalCode',
          operation: 'like',
          attributeValue: '2300'
        },
        {
          attribute: 'contact.email',
          operation: 'like',
          attributeValue: 'agent@example.com'
        },
        {
          attribute: 'contact.telephone',
          operation: 'like',
          attributeValue: '35988742536'
        },
        {
          attribute: 'contact.fax',
          operation: 'like',
          attributeValue: '256'
        },
        {
          attribute: 'cash',
          operation: 'in',
          attributeValue: 'NON_ACTIVE'
        },
        {
          attribute: 'paymentCard',
          operation: 'in',
          attributeValue: 'NON_ACTIVE'
        },
        {
          attribute: 'easyPay',
          operation: 'in',
          attributeValue: 'NON_ACTIVE'
        },
        {
          attribute: 'address.locality',
          operation: 'like',
          attributeValue: 'testing'
        },
        {
          attribute: 'parentIataCode',
          operation: 'like',
          attributeValue: '12'
        },
        {
          attribute: 'bsp.id',
          operation: 'in',
          attributeValue: ''
        }
      ]);
    });
  });
});
