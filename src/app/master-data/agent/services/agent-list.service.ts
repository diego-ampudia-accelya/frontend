import { Injectable } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable';
import { cloneDeep } from 'lodash';

import { AGENTS } from './../agents.constants';
import { Agent } from '~app/master-data/models/agent.model';
import { EQ, GT, IN, LIKE, LT } from '~app/shared/constants/operations';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';

const PROPERTIES = AGENTS.DATA_PROPERTIES;

const toValueLabelObject = (item: any) => ({ value: item, label: item });

@Injectable({ providedIn: 'root' })
export class AgentListService {
  populateFilters(rows: Agent[] = []) {
    const agentCodes = this.generateFromBasicData(rows, PROPERTIES.AGENT_CODE);
    const countries = this.generateCountries(rows);

    return {
      agentCodes,
      countries
    };
  }
  generateFromBasicData(rows: Agent[], property: string): any[] {
    const data = new Set();

    rows.forEach(item => {
      if (item[property]) {
        data.add(item[property]);
      }
    });

    return Array.from(data).map(toValueLabelObject);
  }

  generateCountries(rows: Agent[]): any[] {
    const data = new Set();

    rows.forEach(item => {
      if (item.bsp && item.bsp.name) {
        data.add(item.bsp.name);
      }
    });

    return Array.from(data).map(toValueLabelObject);
  }

  generateColumns(): Array<TableColumn> {
    return cloneDeep(AGENTS.COLUMNS);
  }

  formatRequestFilters(data: any): RequestQueryFilter[] {
    const formattedRequestFilters: RequestQueryFilter[] = [];

    Object.entries<any>(data).forEach(([key, value]) => {
      let attribute;
      let operation = EQ;
      let attributeValue = value;

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'registerDate') {
        attribute = PROPERTIES.REGISTER_DATE;

        if (value[1] === null) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: toShortIsoDate(value[0])
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: toShortIsoDate(value[0])
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: toShortIsoDate(value[1])
          });
        }

        return;
      }

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'expiryDate') {
        attribute = PROPERTIES.EXPIRATION_DATE;

        if (value[1] === null) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: toShortIsoDate(value[0])
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: toShortIsoDate(value[0])
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: toShortIsoDate(value[1])
          });
        }

        return;
      }

      switch (key) {
        case 'active':
          attribute = PROPERTIES.ACTIVE;
          break;
        case 'agentCodeName':
          attribute = PROPERTIES.AGENT_CODE;
          operation = IN;
          attributeValue = value.join(',');
          break;
        case 'parentIataCode':
          attribute = PROPERTIES.PARENT_IATA_CODE;
          operation = LIKE;
          break;
        case 'locationClass':
          attribute = PROPERTIES.LOCATION_CLASS;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'locationType':
          attribute = PROPERTIES.LOCATION_TYPE;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'remittanceFrequency':
          attribute = PROPERTIES.REMITTANCE_FREQUENCY;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'vatNumber':
          attribute = PROPERTIES.VAT_NUMBER;
          operation = LIKE;
          break;
        case 'city':
          attribute = PROPERTIES.CITY;
          operation = LIKE;
          break;
        case 'state':
          attribute = PROPERTIES.STATE;
          operation = LIKE;
          break;
        case 'addressCountry':
          attribute = PROPERTIES.ADDRESS_COUNTRY;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'postalCode':
          attribute = PROPERTIES.POSTAL_CODE;
          operation = LIKE;
          break;
        case 'email':
          attribute = PROPERTIES.EMAIL;
          operation = LIKE;
          break;
        case 'telephone':
          attribute = PROPERTIES.TELEPHONE;
          operation = LIKE;
          break;
        case 'fax':
          attribute = PROPERTIES.FAX;
          operation = LIKE;
          break;
        case 'cash':
          attribute = PROPERTIES.CASH_PAYMENT;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'paymentCard':
          attribute = PROPERTIES.PAYMENT_CARD;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'easyPay':
          attribute = PROPERTIES.EASY_PAY;
          attributeValue = value.join(',');
          operation = IN;
          break;
        case 'locality':
          attribute = PROPERTIES.LOCALITY;
          operation = LIKE;
          break;
        case 'bspId':
          attribute = PROPERTIES.BSP;
          attributeValue = value.map(bsp => bsp.id).join(',');
          operation = IN;
          break;
      }

      if (attribute) {
        formattedRequestFilters.push({
          attribute,
          operation,
          attributeValue
        });
      }
    });

    return formattedRequestFilters;
  }
}
