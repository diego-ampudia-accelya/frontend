import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AddressMaintenanceModule } from '../address-maintenance/address-maintenance.module';
import { AgentGroupModule } from '../agent-group/agent-group.module';
import { GdsByAgentModule } from '../gds-by-agent/gds-by-agent.module';
import { RefundAuthorityModule } from '../refund-authority/refund-authority.module';
import { TicketingAuthorityModule } from '../ticketing-authority/ticketing-authority.module';
import { VariableRemittanceModule } from '../variable-remittance/variable-remittance.module';

import { AgentListComponent } from './agent-list/agent-list.component';
import { AgentProfileComponent } from './agent-profile/agent-profile.component';
import { AgentPropertiesComponent } from './agent-properties/agent-properties.component';
import { AgentRoutingModule } from './agent-routing.module';
import { PaymentStatusPipe } from './payment-status.pipe';
import { reducers } from './reducers';
import { AgentIpAddressSettingsResolver } from './services';
import { AgentAddressMaintenanceResolver } from './services/agent-address-maintenance.resolver';
import { SharedModule } from '~app/shared/shared.module';
import { ConfigurationModule } from '~app/master-data/configuration';

@NgModule({
  declarations: [AgentListComponent, PaymentStatusPipe, AgentProfileComponent, AgentPropertiesComponent],
  imports: [
    CommonModule,
    SharedModule,
    AgentRoutingModule,
    RefundAuthorityModule,
    ConfigurationModule,
    TicketingAuthorityModule,
    AddressMaintenanceModule,
    AgentGroupModule,
    GdsByAgentModule,
    VariableRemittanceModule,
    StoreModule.forFeature('agent', reducers),
    EffectsModule.forFeature([])
  ],
  exports: [AgentListComponent],
  providers: [AgentAddressMaintenanceResolver, AgentIpAddressSettingsResolver]
})
export class AgentModule {}
