import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { AddressMaintenanceComponent } from '../address-maintenance';
import { AgentGroupSubTabContainerComponent } from '../agent-group/agent-group-sub-tab-container/agent-group-sub-tab-container.component';
import { AgentGroupResolver } from '../agent-group/services/agent-group.resolver';
import { NotificationSettingsResolver } from '../airline/services/notification-settings.resolver';
import { ConfigurationComponent } from '../configuration';
import { IpAddressSettingsComponent } from '../configuration/ip-address-settings/ip-address-settings.component';
import { SettingsViewComponent } from '../configuration/settings-view/settings-view.component';
import { UnsavedChangesGuard } from '../configuration/unsaved-changes.guard';
import { GdsByAgentComponent } from '../gds-by-agent/gds-by-agent/gds-by-agent.component';
import { GdsByAgentResolver } from '../gds/services/gds-by-agent.resolver';
import { ViewAs } from '../refund-authority/models';
import { RefundAuthorityComponent } from '../refund-authority/refund-authority/refund-authority.component';
import { TicketingAuthorityComponent } from '../ticketing-authority';
import { VariableRemittanceComponent } from '../variable-remittance/variable-remittance/variable-remittance.component';

import { EmailAlertsSettingsComponent } from '../configuration/email-alert-settings/email-alert-settings.component';
import { AgentProfileComponent } from './agent-profile/agent-profile.component';
import { AgentPropertiesComponent } from './agent-properties/agent-properties.component';
import { AgentIpAddressSettingsResolver, RefundAuthorityRequiredFilterResolver } from './services';
import { AgentAddressMaintenanceResolver } from './services/agent-address-maintenance.resolver';
import { AgentProfileResolver } from './services/agent-profile.resolver';
import { EmailAlertsSettingResolver } from './services/email-alerts-setting.resolver';
import { MyAgentResolver } from './services/my-agent.resolver';
import { TicketingAuthorityResolver } from './services/ticketing-authority.resolver';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';
import { AgentListComponent } from '~app/master-data/agent/agent-list/agent-list.component';
import { CanComponentDeactivateGuard } from '~app/core/services';

const emailAlertRoute: Route = {
  path: 'email-alerts-settings',
  resolve: {
    loadSettings: EmailAlertsSettingResolver
  },
  component: EmailAlertsSettingsComponent,

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.emailAlerts.groupName',
    title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
    viewAs: ViewAs.Agent,
    configuration: {
      title: 'menu.masterData.configuration.emailAlerts.sectionTitle'
    },
    requiredPermissions: Permissions.readEmailAlerts
  }
};

const ipAddressSettingsRoute: Route = {
  path: 'ip-address-settings',
  component: IpAddressSettingsComponent,
  resolve: {
    loadIpSettings: AgentIpAddressSettingsResolver
  },
  canDeactivate: [CanComponentDeactivateGuard],
  data: {
    group: 'menu.masterData.configuration.ipAddressGroupName',
    title: 'menu.masterData.configuration.ipAddressSettingsTitle',
    configuration: {
      title: 'menu.masterData.configuration.ipAddressSettingsTitle'
    },
    requiredPermissions: [Permissions.readIpAddressSettings]
  },
  runGuardsAndResolvers: 'always'
};

const notificationsRoute: Route = {
  path: 'notifications',
  component: SettingsViewComponent,
  resolve: {
    loadSettings: NotificationSettingsResolver
  },

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.notificationsGroupName',
    title: 'menu.masterData.configuration.notificationsSettings',
    configuration: {
      title: 'menu.masterData.configuration.notificationsSettingsTitle',
      permissions: {
        read: Permissions.readNotificationSettings,
        update: Permissions.updateNotificationSettings
      }
    },
    requiredPermissions: [Permissions.readNotificationSettings]
  },
  runGuardsAndResolvers: 'always'
};

const configurationRoute: Route = {
  path: 'configuration',
  component: ConfigurationComponent,
  data: {
    title: 'menu.masterData.configuration.title',
    requiredPermissions: [Permissions.readAgentSettings]
  },
  children: [emailAlertRoute, ipAddressSettingsRoute, notificationsRoute]
};

const propertiesRoute: Route = {
  path: 'properties',
  component: AgentPropertiesComponent,
  data: {
    title: 'menu.masterData.properties.title'
  }
};

const ticketingAuthorityRoute: Route = {
  path: 'ticketing-authority',
  canDeactivate: [CanComponentDeactivateGuard],
  component: TicketingAuthorityComponent,
  resolve: {
    requiredFilter: TicketingAuthorityResolver
  },
  data: {
    title: 'menu.masterData.ticketingAuthority.title',
    viewAs: ViewAs.Agent,
    requiredPermissions: Permissions.readTicketingAuthority
  }
};

const gdsByAgentsRoute: Route = {
  path: 'gds-by-agent',
  component: GdsByAgentComponent,
  resolve: {
    requiredFilter: GdsByAgentResolver
  },
  data: {
    title: 'menu.masterData.gdsByAgent.title',
    viewAs: ViewAs.Agent,
    requiredPermissions: Permissions.readGdsByAgent
  }
};

const refundAuthorityRoute: Route = {
  path: 'refund-authority',
  component: RefundAuthorityComponent,
  resolve: {
    requiredFilter: RefundAuthorityRequiredFilterResolver
  },
  data: {
    title: 'LIST.MASTER_DATA.refundAuthority.tabLabel',
    viewAs: ViewAs.Agent,
    requiredPermissions: [Permissions.readRAIssueAgent, Permissions.readRNIssueAgent]
  }
};

const agentGroupInvitations: Route = {
  path: 'agent-group',
  component: AgentGroupSubTabContainerComponent,
  resolve: {
    agentGroup: AgentGroupResolver
  },
  data: {
    title: 'MASTER_DATA.agentGroupInvitations.title',
    viewAs: ViewAs.Agent,
    requiredPermissions: [Permissions.readAgentGroup]
  }
};

const agentProfileChildren: Route[] = [
  propertiesRoute,
  configurationRoute,
  ticketingAuthorityRoute,
  { path: '', redirectTo: 'properties', pathMatch: 'full' }
];

const contactInfoRoute: Route = {
  path: 'contact-info',
  component: AddressMaintenanceComponent,
  canDeactivate: [CanComponentDeactivateGuard],
  resolve: {
    loadAddress: AgentAddressMaintenanceResolver
  },
  data: {
    title: 'LIST.MASTER_DATA.addressMaintenance.tabLabel',
    requiredPermissions: [Permissions.readAgentAddressMaintenance]
  }
};

const variableRemittanceRoute: Route = {
  path: 'variable-remittance',
  component: VariableRemittanceComponent,
  data: {
    title: 'LIST.MASTER_DATA.variableRemittance.tabLabel',
    viewAs: ViewAs.Agent,
    requiredPermissions: Permissions.readRemittanceFrequency
  }
};

const routes: Routes = [
  {
    path: '',
    component: AgentListComponent,
    data: {
      tab: ROUTES.AGENT
    }
  },
  {
    path: 'my',
    component: AgentProfileComponent,
    resolve: {
      agent: MyAgentResolver
    },
    data: {
      tab: ROUTES.MY_AGENT
    },
    children: [
      ...agentProfileChildren,
      refundAuthorityRoute,
      agentGroupInvitations,
      contactInfoRoute,
      gdsByAgentsRoute,
      variableRemittanceRoute
    ]
  },
  {
    path: ':id',
    component: AgentProfileComponent,
    resolve: {
      agent: AgentProfileResolver
    },
    data: {
      tab: ROUTES.AGENT_PROFILE
    },
    children: [...agentProfileChildren, contactInfoRoute, gdsByAgentsRoute]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentRoutingModule {}
