import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';

import * as fromAgentProfile from './agent-profile.reducer';
import { AppState } from '~app/reducers';

export interface AgentState {
  profile: fromAgentProfile.ProfileState;
}

export interface State extends AppState {
  agent: AgentState;
}

export function reducers(state: AgentState | undefined, action: Action) {
  return combineReducers({
    profile: fromAgentProfile.reducer
  })(state, action);
}

export const getAgentState = createFeatureSelector<State, AgentState>('agent');
