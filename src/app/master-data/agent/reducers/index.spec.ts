import { TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { reducers, State } from './index';

describe('Agent Reducer', () => {
  let store: Store<State>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          agent: reducers
        })
      ]
    });

    store = TestBed.inject(Store);
  });

  it('should create the store', () => {
    expect(store).toBeTruthy();
  });
});
