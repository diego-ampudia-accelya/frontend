import { createReducer, on } from '@ngrx/store';

import { ProfileActions } from '../actions';
import { Agent } from '~app/master-data/models';

export interface ProfileState {
  agent: Agent;
}

const initialState: ProfileState = {
  agent: null
};

export const reducer = createReducer(
  initialState,
  on(ProfileActions.loadAgentSuccess, (state, { agent }) => ({
    ...state,
    agent
  }))
);
