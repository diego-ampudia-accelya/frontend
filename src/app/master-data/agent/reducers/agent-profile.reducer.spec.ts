import { ProfileActions } from '../actions';

import { ProfileState, reducer } from './agent-profile.reducer';
import { Agent } from '~app/master-data/models';

describe('Profile Reducer', () => {
  const agent = {
    id: 1,
    name: 'Agent'
  } as Agent;

  const initialState: ProfileState = {
    agent: null
  };

  it('should return the initial state', () => {
    const action = {} as any;
    const state = reducer(undefined, action);

    expect(state).toEqual(initialState);
  });

  it('should handle loadAgentSuccess action when loadAgentSuccess is triggered', () => {
    const action = ProfileActions.loadAgentSuccess({ agent });
    const state = reducer(initialState, action);

    expect(state.agent).toEqual(agent);
  });
});
