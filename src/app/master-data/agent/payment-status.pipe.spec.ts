import { PaymentStatusPipe } from './payment-status.pipe';
import { PaymentStatus } from '~app/shared/enums/payment-status.enum';

describe('PaymentStatusPipe', () => {
  let pipe: PaymentStatusPipe;

  beforeEach(() => {
    pipe = new PaymentStatusPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return correct string when the status is not authorised', () => {
    expect(pipe.transform(PaymentStatus.NON_AUTHORIZED)).toBe('Not Authorized');
  });

  it('should return null when payment status value is null', () => {
    const result = pipe.transform(null);

    expect(result).toBe(null);
  });
});
