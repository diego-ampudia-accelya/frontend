import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { CanComponentDeactivateGuard } from '~app/core/services';
import { ConfigurationComponent } from '~app/master-data/configuration/configuration/configuration.component';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants/routes';
import { NotificationSettingsResolver } from '../airline/services/notification-settings.resolver';
import { TctpService } from '../bsp/configuration/api/tctp.service';
import { tctpConfig } from '../bsp/configuration/tctp.config';
import {
  baseDisputeReasonsConfig,
  baseIssueReasonConfig,
  baseRejectionReasonsConfig,
  DescriptionViewComponent,
  SettingsViewComponent,
  UnsavedChangesGuard
} from '../configuration';
import { ConsentSettingsComponent } from '../configuration/consent-settings/consent-settings.component';
import { EmailAlertsSettingsComponent } from '../configuration/email-alert-settings/email-alert-settings.component';
import { SftpConfigurationsResolver } from '../sftp/services/sftp-configurations-resolver.service';
import { SftpAccountsListComponent } from '../sftp/sftp-accounts-list/sftp-accounts-list.component';
import { SftpAccountsComponent } from '../sftp/sftp-accounts/sftp-accounts.component';
import { AccessPermissionsComponent } from './access-permissions/access-permissions.component';
import { AccessPermissionsGuard } from './access-permissions/guards/access-permissions.guard';
import { DisputeReasonsService } from './api/dispute-reasons.service';
import { IssueReasonsService } from './api/issue-reasons.service';
import { RaIssueReasonsService } from './api/ra-issue-reasons.service';
import { RaRejectionReasonsService } from './api/ra-rejection-reasons.service';
import { CurrencyConversionComponent } from './currency-conversion/currency-conversion/currency-conversion.component';
import { DeactivatedAgentsComponent } from './deactivated-agents/deactivated-agents.component';
import { DownloadSubUsersGuard } from './donwload-sub-users/services/download-sub-users.guard';
import { GlobalReportsSettingsComponent } from './global-reports-settings/global-reports-settings/global-reports-settings.component';
import { GlobalUserFileGuard } from './global-user-file/global-user-file-guard/global-user-file-guard';
import { HomuUserPermissionsComponent } from './homu-users/homu-user-permissions/homu-user-permissions.component';
import { HomuUserProfileComponent } from './homu-users/homu-user-profile/homu-user-profile.component';
import { HomuUserPropertiesComponent } from './homu-users/homu-user-properties/homu-user-properties.component';
import { HomuUsersHosusListComponent } from './homu-users/homu-users-hosus-list/homu-users-hosus-list.component';
import { HomuUsersListComponent } from './homu-users/homu-users-list/homu-users-list.component';
import { HomuUserProfileResolver } from './homu-users/services/homu-user-profile.resolver';
import { IataProfileComponent } from './iata-profile/iata-profile.component';
import { ReinstatedAgentsComponent } from './reinstated-agents/reinstated-agents.component';
import { AgentMaintenanceResolver } from './services/agent-maintenance.resolver';
import { BasicSettingResolver } from './services/basic-setting.resolver';
import { CurrencyConversionResolver } from './services/currency-conversion.resolver';
import { EmailAlertsSettingResolver } from './services/email-alerts-setting.resolver';
import { GeneralSettingResolver } from './services/general-setting.resolver';
import { IataReasonsResolver } from './services/iata-reasons.resolver';
import { RefundSettingResolver } from './services/refund-setting.resolver';
import { RemittanceSettingResolver } from './services/remittance-setting.resolver';
import { TaxOnCommissionTypesResolver } from './services/tax-on-commission-types.resolver';
import { SettingsChangesComponent } from './settings-changes/settings-changes.component';
import { SuspendedAirlineComponent } from './suspended-airline/suspended-airline.component';

// Tip Consents
const consentsRoute = {
  path: 'consents-settings',
  component: ConsentSettingsComponent,
  canDeactivate: [CanComponentDeactivateGuard],
  data: {
    group: 'menu.masterData.configuration.tipConsents.groupName',
    title: 'menu.masterData.configuration.tipConsents.title',
    configuration: {
      title: 'menu.masterData.configuration.tipConsents.title'
    },
    requiredPermissions: [Permissions.uTipGlobCfg]
  }
};

// Agent Maintenance
const agentMaintenance = [
  {
    path: 'agent-maintenance-settings',
    component: SettingsViewComponent,
    resolve: {
      loadSettings: AgentMaintenanceResolver
    },
    canDeactivate: [UnsavedChangesGuard],
    data: {
      group: 'menu.masterData.configuration.agentMaintenance.groupName',
      title: 'menu.masterData.configuration.agentMaintenance.title',
      configuration: {
        title: 'menu.masterData.configuration.agentMaintenance.title'
      },
      requiredPermissions: [Permissions.readIataSettings, Permissions.updateIataSettings]
    }
  }
];

// ACDM Configuration
const acdmConfiguration = [
  {
    path: 'basic-settings',
    component: SettingsViewComponent,
    resolve: {
      loadSettings: BasicSettingResolver
    },
    canDeactivate: [UnsavedChangesGuard],
    data: {
      group: 'menu.masterData.configuration.acdmSettings.groupName',
      title: 'menu.masterData.configuration.basicSettings',
      configuration: {
        title: 'menu.masterData.configuration.basicSettings'
      },
      requiredPermissions: [Permissions.readIataSettings]
    }
  },
  {
    path: 'issue-reasons',
    component: DescriptionViewComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    resolve: {
      parentEntity: IataReasonsResolver
    },
    data: {
      group: 'menu.masterData.configuration.acdmSettings.groupName',
      title: 'menu.masterData.configuration.issueReasons',
      configuration: {
        ...baseIssueReasonConfig,
        description: { ...baseIssueReasonConfig.description, canValidate: true },
        permissions: {
          read: Permissions.readIssueReason,
          create: Permissions.createIssueReason,
          update: Permissions.updateIssueReason,
          delete: Permissions.deleteIssueReason
        },
        apiService: IssueReasonsService
      },
      requiredPermissions: Permissions.readIssueReason
    }
  },
  {
    path: 'dispute-reasons',
    resolve: {
      parentEntity: IataReasonsResolver
    },
    component: DescriptionViewComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      group: 'menu.masterData.configuration.acdmSettings.groupName',
      title: 'menu.masterData.configuration.disputeReasons',
      configuration: {
        ...baseDisputeReasonsConfig,
        description: { ...baseDisputeReasonsConfig.description, canValidate: true },
        permissions: {
          read: Permissions.readDisputeReason,
          create: Permissions.createDisputeReason,
          update: Permissions.updateDisputeReason,
          delete: Permissions.deleteDisputeReason
        },
        apiService: DisputeReasonsService
      },
      requiredPermissions: Permissions.readDisputeReason
    }
  },
  {
    path: 'tax-on-comission-types',
    resolve: {
      parentEntity: TaxOnCommissionTypesResolver
    },
    component: DescriptionViewComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      group: 'menu.masterData.configuration.acdmSettings.groupName',
      title: 'menu.masterData.configuration.taxOnCommissionTypes',
      configuration: {
        ...tctpConfig,
        permissions: {
          read: Permissions.readTaxOnComissionTypes,
          create: Permissions.createTaxOnComissionTypes,
          update: Permissions.updateTaxOnComissionTypes,
          delete: Permissions.deleteTaxOnComissionTypes
        },
        apiService: TctpService
      },
      requiredPermissions: [Permissions.readTaxOnComissionTypes]
    }
  }
];

// Email Alert Configuration
const emailAlertRoute: Route = {
  path: 'email-alerts-settings',
  resolve: {
    loadSettings: EmailAlertsSettingResolver
  },
  component: EmailAlertsSettingsComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.emailAlerts.groupName',
    title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.emailAlerts.sectionTitle'
    },
    requiredPermissions: Permissions.readEmailAlerts
  }
};

// Notifications Configuration
const notificationsRoute: Route = {
  path: 'notifications',
  component: SettingsViewComponent,
  resolve: {
    loadSettings: NotificationSettingsResolver
  },

  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.notificationsGroupName',
    title: 'menu.masterData.configuration.notificationsSettings',
    configuration: {
      title: 'menu.masterData.configuration.notificationsSettingsTitle',
      permissions: {
        read: Permissions.readNotificationSettings,
        update: Permissions.updateNotificationSettings
      }
    },
    requiredPermissions: [Permissions.readNotificationSettings]
  },
  runGuardsAndResolvers: 'always'
};

// RA / RN Settings
const raRnRoute = [
  {
    path: 'refund-settings',
    resolve: {
      loadSettings: RefundSettingResolver
    },
    component: SettingsViewComponent,
    canDeactivate: [UnsavedChangesGuard],
    data: {
      group: 'menu.masterData.configuration.refundSettings.groupName',
      title: 'menu.masterData.configuration.refundSettings.sectionTitle',
      configuration: {
        title: 'menu.masterData.configuration.refundSettings.sectionTitle'
      },
      requiredPermissions: Permissions.readIataSettings
    }
  },
  {
    path: 'ra-issue-reasons',
    component: DescriptionViewComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    resolve: {
      parentEntity: IataReasonsResolver
    },
    data: {
      group: 'menu.masterData.configuration.refundSettings.groupName',
      title: 'menu.masterData.configuration.refundSettings.raIssueReasons',
      configuration: {
        ...baseIssueReasonConfig,
        title: 'menu.masterData.configuration.refundSettings.raIssueReasons',
        permissions: {
          read: Permissions.readIataRaIssueReason,
          create: Permissions.createIataRaIssueReason,
          update: Permissions.updateIataRaIssueReason,
          delete: Permissions.deleteIataRaIssueReason
        },
        description: {
          ...baseIssueReasonConfig.description,
          maxLength: 250,
          lineLimit: null,
          rowsLimit: null
        },
        label: {
          maxLength: 50
        },
        apiService: RaIssueReasonsService
      },
      requiredPermissions: Permissions.readIataRaIssueReason
    }
  },
  {
    path: 'ra-rejection-reasons',
    component: DescriptionViewComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    resolve: {
      parentEntity: IataReasonsResolver
    },
    data: {
      group: 'menu.masterData.configuration.refundSettings.groupName',
      title: 'menu.masterData.configuration.refundSettings.raRejectionReasons',
      configuration: {
        ...baseRejectionReasonsConfig,
        permissions: {
          read: Permissions.readIataRaRejectionReason,
          create: Permissions.createIataRaRejectionReason,
          update: Permissions.updateIataRaRejectionReason,
          delete: Permissions.deleteIataRaRejectionReason
        },
        apiService: RaRejectionReasonsService
      },
      requiredPermissions: Permissions.readIataRaRejectionReason
    }
  }
];

// SFTP Configuration
const sftpRoute: Route = {
  path: 'sftp',
  component: SftpAccountsComponent,
  resolve: {
    loadSettings: SftpConfigurationsResolver
  },
  data: {
    group: 'menu.masterData.configuration.sftpConfigurations.groupName',
    title: 'menu.masterData.configuration.sftpConfigurations.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.sftpConfigurations.sectionTitle'
    }
  }
};

const bspConfigurationRoute = {
  path: 'general-settings',
  resolve: {
    loadSettings: GeneralSettingResolver
  },
  component: SettingsViewComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.bspConfigurationGroupName',
    title: 'menu.masterData.configuration.generalSettings.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.generalSettings.sectionTitle'
    },
    requiredPermissions: Permissions.readIataSettings
  }
};

const remittancenRoute = {
  path: 'remittance-settings',
  resolve: {
    loadSettings: RemittanceSettingResolver
  },
  component: SettingsViewComponent,
  canDeactivate: [UnsavedChangesGuard],
  data: {
    group: 'menu.masterData.configuration.bspConfigurationGroupName',
    title: 'menu.masterData.configuration.remittanceSettings.sectionTitle',
    configuration: {
      title: 'menu.masterData.configuration.remittanceSettings.sectionTitle'
    },
    requiredPermissions: Permissions.readIataSettings
  }
};

const homuUserChildRoutes: Route[] = [
  {
    path: 'properties',
    component: HomuUserPropertiesComponent,
    data: {
      title: 'MENU.USERS_MAINTENANCE.HOMU_USERS.profile.tabNames.properties',
      requiredPermissions: Permissions.readHomu
    }
  },
  {
    path: 'permissions',
    canDeactivate: [CanComponentDeactivateGuard],
    component: HomuUserPermissionsComponent,
    data: {
      title: 'MENU.USERS_MAINTENANCE.HOMU_USERS.profile.tabNames.permissions',
      requiredPermissions: Permissions.updateHomuPermissions
    }
  },
  {
    path: 'hosus-list',
    component: HomuUsersHosusListComponent,
    data: {
      title: 'MENU.USERS_MAINTENANCE.HOMU_USERS.profile.tabNames.hosusList',
      requiredPermissions: Permissions.readHomu
    }
  },
  { path: ROUTES.HOMU_USERS_PROFILE.path, redirectTo: 'properties', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: 'my',
    component: IataProfileComponent,
    data: {
      tab: ROUTES.MY_IATA
    },
    children: [
      {
        path: 'configuration',
        component: ConfigurationComponent,
        data: {
          title: 'menu.masterData.configuration.title',
          requiredPermissions: Permissions.readSftpAccount
        },
        children: [
          ...acdmConfiguration,
          ...agentMaintenance,
          emailAlertRoute,
          notificationsRoute,
          ...raRnRoute,
          bspConfigurationRoute,
          sftpRoute,
          remittancenRoute,
          consentsRoute
        ]
      },
      {
        path: 'currency-conversion',
        component: CurrencyConversionComponent,
        resolve: {
          loadSettings: CurrencyConversionResolver
        },
        canDeactivate: [CanComponentDeactivateGuard],
        data: {
          title: 'menu.masterData.cutpDecimalConversion.title',
          requiredPermissions: Permissions.readCurrencyConversion
        }
      },
      {
        path: 'settings/global-reports-settings',
        component: GlobalReportsSettingsComponent,
        canDeactivate: [CanComponentDeactivateGuard],
        data: {
          title: 'menu.masterData.globalReportsSettings.title',
          requiredPermissions: Permissions.readTipGlobRepStt
        }
      }
    ]
  },
  {
    path: ROUTES.SETTINGS_CHANGES.path,
    component: SettingsChangesComponent,
    data: {
      tab: ROUTES.SETTINGS_CHANGES
    }
  },
  {
    path: ROUTES.SUSPENDED_AIRLINE.path,
    component: SuspendedAirlineComponent,
    data: {
      tab: ROUTES.SUSPENDED_AIRLINE
    }
  },
  {
    path: ROUTES.MAIN_USERS.path,
    loadChildren: () => import('./main-users/main-users.module').then(m => m.MainUsersModule)
  },
  {
    path: ROUTES.GLOBAL_USER_FILE.path,
    canActivate: [GlobalUserFileGuard]
  },
  {
    path: ROUTES.SFTP_ACCOUNTS.path,
    component: SftpAccountsListComponent,
    data: {
      tab: ROUTES.SFTP_ACCOUNTS
    }
  },
  {
    path: ROUTES.DEACTIVATED_AGENTS.path,
    component: DeactivatedAgentsComponent,
    data: {
      tab: ROUTES.DEACTIVATED_AGENTS
    }
  },
  {
    path: ROUTES.REINSTATED_AGENTS.path,
    component: ReinstatedAgentsComponent,
    data: {
      tab: ROUTES.REINSTATED_AGENTS
    }
  },
  {
    path: ROUTES.DOWNLOAD_SUB_USERS.path,
    canActivate: [DownloadSubUsersGuard]
  },
  {
    path: ROUTES.ACCESS_PERMISSIONS.path,
    canActivate: [AccessPermissionsGuard]
  },
  {
    path: `${ROUTES.ACCESS_PERMISSIONS.path}/list`,
    component: AccessPermissionsComponent,
    data: {
      tab: ROUTES.ACCESS_PERMISSIONS
    }
  },
  // {
  //   path: `${ROUTES.ACCESS_PERMISSIONS.path}/list`,
  //   loadChildren: () => import('./access-permissions/access-permissions.module').then(m => m.AccessPermissionsModule)
  // },
  {
    path: ROUTES.HOMU_USERS_LIST.path,
    component: HomuUsersListComponent,
    data: {
      tab: ROUTES.HOMU_USERS_LIST
    }
  },
  {
    path: ROUTES.HOMU_USERS_PROFILE.path,
    component: HomuUserProfileComponent,
    resolve: { homuUser: HomuUserProfileResolver },
    data: {
      tab: ROUTES.HOMU_USERS_PROFILE
    },
    children: homuUserChildRoutes
  },
  {
    path: ROUTES.MONITOR.path,
    loadChildren: () => import('./monitoring/monitoring.module').then(m => m.MonitoringModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IataRoutingModule {}
