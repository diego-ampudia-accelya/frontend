import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { of } from 'rxjs';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { CreateGlobalReportDialogComponent } from '../create-global-report-dialog/create-global-report-dialog.component';
import { GlobalReportsSettingsRow } from '../model/global-reports-settings.model';
import { GlobalReportsSettingsFilterFormatter } from '../services/global-reports-settings-filter-formatter';
import { GlobalReportsSettingsService } from '../services/global-reports-settings.service';
import { GlobalReportsSettingsComponent } from './global-reports-settings.component';

describe('GlobalReportsSettingsComponent', () => {
  let component: GlobalReportsSettingsComponent;
  let fixture: ComponentFixture<GlobalReportsSettingsComponent>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const notificationServiceSpy: SpyObject<NotificationService> = createSpyObject(NotificationService);
  const globalReportsSettingsServiceSpy: SpyObject<GlobalReportsSettingsService> =
    createSpyObject(GlobalReportsSettingsService);
  let queryableDataSourceSpy: SpyObject<QueryableDataSource<GlobalReportsSettingsService>>;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { globalReportsQuery: { id: 'globalReportsQuery' } },
        activeTabId: 'globalReportsQuery'
      },
      viewListsInfo: {}
    }
  };

  const query: DataQuery = {
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  };

  beforeEach(async(() => {
    queryableDataSourceSpy = createSpyObject(QueryableDataSource, { appliedQuery$: of(query) });
    queryableDataSourceSpy.get.and.returnValue(of(query));

    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      declarations: [GlobalReportsSettingsComponent],
      providers: [
        provideMockStore({ initialState }),
        MessageService,
        AppConfigurationService,
        L10nTranslationService,
        GlobalReportsSettingsFilterFormatter,
        FormBuilder,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DefaultQueryStorage, useValue: queryStorageSpy },
        { provide: GlobalReportsSettingsService, useValue: globalReportsSettingsServiceSpy }
      ]
    })
      .overrideComponent(GlobalReportsSettingsComponent, {
        set: {
          providers: [{ provide: QueryableDataSource, useValue: queryableDataSourceSpy }]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalReportsSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set rows selected when onRowsSelectionChange is triggered', () => {
    const rows: GlobalReportsSettingsRow[] = [
      {
        id: 1,
        reportDescription: 'TRNXS - ENROLLED ATMS WITH CONSENT',
        fileType: 'TIP_2A',
        format: 'CSV+PDF',
        userType: 'Agent',
        frequency: 'Daily+Monthly',
        channel: 'BSPlink',
        threshold: false,
        subscribeLomus: false,
        status: 'Deleted',
        isRowSelected: true
      },
      {
        id: 2,
        reportDescription: 'TRNXS - ENROLLED ATMS WITH CONSENT',
        fileType: 'TIP_2A',
        format: 'CSV+PDF',
        userType: 'Agent',
        frequency: 'Daily+Monthly',
        channel: 'BSPlink',
        threshold: false,
        subscribeLomus: false,
        status: 'Deleted',
        isRowSelected: false
      }
    ];
    component.onRowsSelectionChange(rows);

    expect(component['rowsSelected'].length).toEqual(1);
    component.selectedRowsCount$.subscribe(count => {
      expect(count).toEqual(component['rowsSelected'].length);
    });
  });

  it('should clear all the rows selected when clearSelectedRows method is triggered', () => {
    component.clearSelectedRows();

    component.selectedRowsCount$.subscribe(count => {
      expect(count).toEqual(null);
    });
  });

  it('should open Report Dialog when openAddReportDialog method is triggered', () => {
    component.openAddReportDialog();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(CreateGlobalReportDialogComponent, {
      data: {
        title: 'LIST.MASTER_DATA.globalReportsSettings.createDialog.title',
        hasCancelButton: true,
        isClosable: true,
        actionEmitter: component['dialogFormSubmitEmitter'],
        footerButtonsType: [{ type: FooterButton.Create, isDisabled: true }]
      }
    });
  });

  it('should show notification success and clear the selected rows when reports selected are deleted successfully', () => {
    component['rowsSelected'] = [
      {
        id: 1,
        reportDescription: 'TRNXS - ENROLLED ATMS WITH CONSENT',
        fileType: 'TIP_2A',
        format: 'CSV+PDF',
        userType: 'Agent',
        frequency: 'Daily+Monthly',
        channel: 'BSPlink',
        threshold: false,
        subscribeLomus: false,
        status: 'Deleted',
        isRowSelected: true
      }
    ];
    const idItemsToDelete: number[] = component['rowsSelected'].map(row => row.id);

    globalReportsSettingsServiceSpy.delete.and.returnValue(of([]));
    spyOn(component, 'clearSelectedRows');

    component.onDeleteGlobalReportsClick();

    globalReportsSettingsServiceSpy.delete(idItemsToDelete).subscribe(() => {
      expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
      expect(component.clearSelectedRows).toHaveBeenCalled();
    });
  });
});
