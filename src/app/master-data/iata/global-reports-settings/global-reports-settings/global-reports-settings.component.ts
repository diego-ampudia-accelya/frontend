import { DatePipe } from '@angular/common';
import { Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { MarkStatus } from '~app/shared/models/mark-status.model';
import { NotificationService } from '~app/shared/services';
import { CreateGlobalReportDialogComponent } from '../create-global-report-dialog/create-global-report-dialog.component';
import { GlobalDropdownOption, GlobalReportsSettingsFilter } from '../model/global-report-settings-filter.model';
import * as GlobalOption from '../model/global-reports-settings-options';
import { GlobalReportsSettings, GlobalReportsSettingsRow } from '../model/global-reports-settings.model';
import { GlobalReportsSettingsFilterFormatter } from '../services/global-reports-settings-filter-formatter';
import { GlobalReportsSettingsService } from '../services/global-reports-settings.service';

@Component({
  selector: 'bspl-global-reports-settings',
  templateUrl: './global-reports-settings.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: GlobalReportsSettingsService }
  ]
})
export class GlobalReportsSettingsComponent implements OnInit, OnDestroy {
  public title = this.translationService.translate('LIST.MASTER_DATA.globalReportsSettings.title');
  public customLabels = { create: 'LIST.MASTER_DATA.globalReportsSettings.buttons.addReports' };
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public formatFilterOptionList: DropdownOption<GlobalDropdownOption>[];
  public userTypeOptionList: DropdownOption<GlobalDropdownOption>[];
  public frequencyOptionList: DropdownOption<GlobalDropdownOption>[];
  public channelOptionList: DropdownOption<GlobalDropdownOption>[];
  public thresholdOptionList: DropdownOption<boolean>[];
  public subscribeLomusOptionList: DropdownOption<boolean>[];
  public statusOptionList: DropdownOption<GlobalDropdownOption>[];
  public selectedRowsCount$: Observable<number>;

  private storedQuery: DataQuery<{ [key: string]: any }>;
  private formFactory: FormUtil;
  private rowsSelected: GlobalReportsSettingsRow[];
  private destroy$ = new Subject();
  private dialogFormSubmitEmitter = new Subject();

  constructor(
    public displayFormatter: GlobalReportsSettingsFilterFormatter,
    public dataSource: QueryableDataSource<GlobalReportsSettings>,
    public notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private queryStorage: DefaultQueryStorage,
    private globalReportsSettingsService: GlobalReportsSettingsService,
    private dialogService: DialogService,
    private globalErrorHandlerService: ErrorHandler
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initColumns();
    this.searchForm = this.buildForm();
    this.loadData(this.storedQuery);
    this.populateDropdownFilters();
    this.dialogFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.loadData(this.storedQuery));
  }

  public onRowsSelectionChange(rows: GlobalReportsSettingsRow[]): void {
    this.rowsSelected = rows.filter(row => row.isRowSelected);
    this.selectedRowsCount$ = of(this.rowsSelected.length);
  }

  public rowClassesModifier = (dataItem: any, classes: string[]): void => {
    if (dataItem.markStatus === MarkStatus.Unread) {
      classes.push('row-unread');
    }
    if (dataItem.isRowSelected) {
      classes.push('selected-row');
    }
  };

  public clearSelectedRows(): void {
    this.selectedRowsCount$ = of(null);
    this.loadData(this.queryStorage.get());
  }

  public onDeleteGlobalReportsClick(): void {
    const idItemsToDelete: number[] = this.rowsSelected.map(row => row.id);
    this.globalReportsSettingsService.delete(idItemsToDelete).subscribe(
      () => {
        this.notificationService.showSuccess(
          this.translationService.translate(
            'LIST.MASTER_DATA.globalReportsSettings.createDialog.messages.deleteSuccess'
          )
        );
        this.clearSelectedRows();
      },
      error => {
        this.globalErrorHandlerService.handleError(error);
      }
    );
  }

  public openAddReportDialog(): any {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'LIST.MASTER_DATA.globalReportsSettings.createDialog.title',
        hasCancelButton: true,
        isClosable: true,
        actionEmitter: this.dialogFormSubmitEmitter,
        footerButtonsType: [{ type: FooterButton.Create, isDisabled: true }]
      }
    };

    return this.dialogService.open(CreateGlobalReportDialogComponent, dialogConfig);
  }

  public loadData(query: DataQuery<GlobalReportsSettings>): void {
    this.selectedRowsCount$ = of(null);
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private generateOptionList(enumOptions): DropdownOption<GlobalDropdownOption>[] {
    return Object.entries(enumOptions).map(([key, val]: [string, string]) => ({
      value: { translationKey: key, value: val },
      label: this.translationService.translate(`LIST.MASTER_DATA.globalReportsSettings.filters.options.labels.${key}`)
    }));
  }

  private populateDropdownFilters(): void {
    this.formatFilterOptionList = this.generateOptionList(GlobalOption.Format);
    this.userTypeOptionList = this.generateOptionList(GlobalOption.UserType);
    this.frequencyOptionList = this.generateOptionList(GlobalOption.Frequency);
    this.channelOptionList = this.generateOptionList(GlobalOption.Channel);
    this.thresholdOptionList = this.generateYesNoOptionList();
    this.subscribeLomusOptionList = this.generateYesNoOptionList();
    this.statusOptionList = this.generateOptionList(GlobalOption.Status);
  }

  private generateYesNoOptionList(): DropdownOption<boolean>[] {
    return [true, false].map(item => ({
      label: this.translateBooleanToYesNo(item),
      value: item
    }));
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<GlobalReportsSettingsFilter>({
      reportDescription: [],
      fileType: [],
      format: [],
      userType: [],
      frequency: [],
      channel: [],
      threshold: [],
      subscribeLomus: [],
      status: []
    });
  }

  private translateBooleanToYesNo(booleanValue: boolean): string {
    return this.translationService.translate(
      `LIST.MASTER_DATA.globalReportsSettings.yesNoBooleanValue.${booleanValue}`
    );
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'isRowSelected',
        maxWidth: 40,
        sortable: false,
        name: '',
        headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
        cellTemplate: 'defaultLabeledCheckboxCellTmpl'
      },
      {
        prop: 'reportDescription',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.reportDescription'
      },
      {
        prop: 'fileType',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.fileType'
      },
      {
        prop: 'format',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.format'
      },
      {
        prop: 'userType',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.userType'
      },
      {
        prop: 'frequency',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.frequency'
      },
      {
        prop: 'channel',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.channel'
      },
      {
        prop: 'threshold',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.threshold',
        pipe: {
          transform: value => this.translateBooleanToYesNo(value)
        }
      },
      {
        prop: 'subscribeLomus',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.subscribeLomus',
        pipe: {
          transform: value => this.translateBooleanToYesNo(value)
        }
      },
      {
        prop: 'status',
        name: 'LIST.MASTER_DATA.globalReportsSettings.columns.status'
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
