import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { GlobalReportsSettingsFilter } from '../model/global-report-settings-filter.model';
import { GlobalReportsSettingsFilterFormatter } from './global-reports-settings-filter-formatter';

describe('GlobalReportsSettingsFilterFormatter', () => {
  let formatter: GlobalReportsSettingsFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  const filters = {
    subscribeLomus: false,
    threshold: false,
    status: {
      translationKey: 'deleted',
      value: 'deleted'
    },
    reportDescription: 'Description test',
    fileType: 'TIP',
    channel: {
      translationKey: 'BSPlink',
      value: 'BSPlink'
    },
    frequency: [
      {
        translationKey: 'Daily',
        value: 'Daily'
      }
    ],
    format: [
      {
        translationKey: 'CSV',
        value: 'CSV'
      }
    ],
    userType: [
      {
        translationKey: 'IATA',
        value: 'IATA'
      }
    ]
  } as GlobalReportsSettingsFilter;

  const translateKey = 'LIST.MASTER_DATA.globalReportsSettings.filters.labels';
  const translateOptionKey = 'LIST.MASTER_DATA.globalReportsSettings.filters.options.labels';

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new GlobalReportsSettingsFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['subscribeLomus'],
        label: `${translateKey}.subscribeLomus - ${translateOptionKey}.false`
      },
      {
        keys: ['threshold'],
        label: `${translateKey}.threshold - ${translateOptionKey}.false`
      },
      {
        keys: ['status'],
        label: `${translateKey}.status - ${translateOptionKey}.deleted`
      },
      {
        keys: ['reportDescription'],
        label: `${translateKey}.reportDescription - Description test`
      },
      {
        keys: ['fileType'],
        label: `${translateKey}.fileType - TIP`
      },
      {
        keys: ['channel'],
        label: `${translateKey}.channel - BSPlink`
      },
      {
        keys: ['frequency'],
        label: `${translateKey}.frequency - Daily`
      },
      {
        keys: ['format'],
        label: `${translateKey}.format - CSV`
      },
      {
        keys: ['userType'],
        label: `${translateKey}.userType - IATA`
      }
    ]);
  });
});
