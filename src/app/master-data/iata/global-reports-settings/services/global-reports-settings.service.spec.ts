import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { GlobalReportsSettings, GlobalReportsSettingsCreateRequest } from '../model/global-reports-settings.model';
import { GlobalReportsSettingsService } from './global-reports-settings.service';

describe('GlobalReportsSettingsService', () => {
  let service: GlobalReportsSettingsService;
  let httpClientSpy: SpyObject<HttpClient>;

  const globalReportsRecordsMock: GlobalReportsSettings[] = [
    {
      reportDescription: 'testing 1',
      fileType: 'TIP_2',
      format: 'CSV',
      userType: 'IATA',
      frequency: 'Daily',
      channel: 'BSPlink',
      threshold: false,
      subscribeLomus: true,
      status: 'deleted'
    },
    {
      reportDescription: 'testing 2',
      fileType: 'TIP_2',
      format: 'CSV',
      userType: 'IATA',
      frequency: 'CVD',
      channel: 'BSPlink',
      threshold: false,
      subscribeLomus: true,
      status: 'deleted'
    }
  ];

  const queryCloneDeep = cloneDeep(defaultQuery);
  const dataQuery: DataQuery = {
    ...queryCloneDeep,
    filterBy: {
      format: ['CSV'],
      userType: ['Airline'],
      frequency: ['CVD'],
      channel: 'BSPlink',
      status: true,
      ...queryCloneDeep.filterBy
    }
  };

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        GlobalReportsSettingsService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(GlobalReportsSettingsService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/tip-management/global-reports-settings');
  });

  it('should trigger httpClient post method when delete method is triggered', () => {
    const itemsId = [1, 2];
    const requestBody = {
      ids: itemsId,
      status: 'Deleted'
    };

    service.delete(itemsId);

    expect(httpClientSpy.post).toHaveBeenCalledWith('/tip-management/global-reports-settings/delete', requestBody);
  });

  it('should trigger httpClient post method when create method is triggered', () => {
    const reportItem: GlobalReportsSettingsCreateRequest = {
      reportDescription: 'testing',
      fileType: 'TIP_2',
      format: 'CSV',
      userType: 'IATA',
      frequency: 'Daily',
      channel: 'BSPlink',
      threshold: false,
      subscribeLomus: true
    };

    service.create(reportItem);

    expect(httpClientSpy.post).toHaveBeenCalledWith('/tip-management/global-reports-settings', reportItem);
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let globalReportsData: PagedData<GlobalReportsSettings>;

    const globalReportsDataMock: PagedData<GlobalReportsSettings> = {
      records: globalReportsRecordsMock,
      total: globalReportsRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(globalReportsDataMock));
    service.find(query).subscribe(res => {
      globalReportsData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/tip-management/global-reports-settings?page=0&size=20');
    expect(globalReportsData).toEqual(globalReportsDataMock);
  }));

  it('should return filters formatted calling formatQuery method', () => {
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.size).toEqual(20);
    expect(formattedQuery.page).toEqual(0);
  });
});
