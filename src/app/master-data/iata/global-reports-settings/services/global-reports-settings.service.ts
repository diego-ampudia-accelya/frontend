import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import {
  GlobalReportsSettingsFilter,
  GlobalReportsSettingsFilterBE
} from '../model/global-report-settings-filter.model';
import { GlobalReportsSettings, GlobalReportsSettingsCreateRequest } from '../model/global-reports-settings.model';

@Injectable()
export class GlobalReportsSettingsService implements Queryable<GlobalReportsSettings> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/global-reports-settings`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<GlobalReportsSettingsFilter>): Observable<PagedData<GlobalReportsSettings>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<GlobalReportsSettings>>(this.baseUrl + requestQuery.getQueryString());
  }

  public delete(itemsId: number[]): Observable<GlobalReportsSettings> {
    const requestBody = {
      ids: itemsId,
      status: 'Deleted'
    };

    return this.http.post<GlobalReportsSettings>(`${this.baseUrl}/delete`, requestBody);
  }

  public create(reportItem: GlobalReportsSettingsCreateRequest): Observable<GlobalReportsSettingsFilterBE> {
    return this.http.post<GlobalReportsSettingsFilterBE>(`${this.baseUrl}`, reportItem);
  }

  private formatQuery(
    query: Partial<DataQuery<GlobalReportsSettingsFilter>>
  ): RequestQuery<GlobalReportsSettingsFilterBE> {
    const { format, userType, frequency, channel, status, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        format: format && format.map(value => value.value),
        userType: userType && userType.map(value => value.value),
        frequency: frequency && frequency.map(value => value.value),
        channel: channel && channel.value,
        status: status && status.value
      }
    });
  }
}
