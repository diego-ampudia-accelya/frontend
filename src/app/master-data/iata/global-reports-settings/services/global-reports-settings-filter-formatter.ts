import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { mapJoinMapper } from '~app/shared/helpers';
import { GlobalReportsSettingsFilter } from '../model/global-report-settings-filter.model';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };
@Injectable()
export class GlobalReportsSettingsFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translation: L10nTranslationService) {
    super();
  }

  public format(filter: GlobalReportsSettingsFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<GlobalReportsSettingsFilter>> = {
      reportDescription: reportDescription => `${this.translate('reportDescription')} - ${reportDescription}`,
      fileType: fileType => `${this.translate('fileType')} - ${fileType}`,
      format: format => `${this.translate('format')} - ${mapJoinMapper(format, 'value')}`,
      userType: userType => `${this.translate('userType')} - ${mapJoinMapper(userType, 'value')}`,
      frequency: frequency => `${this.translate('frequency')} - ${mapJoinMapper(frequency, 'value')}`,
      channel: channel => `${this.translate('channel')} - ${channel.value}`,
      threshold: threshold => `${this.translate('threshold')} - ${this.translateOption(threshold)}`,
      subscribeLomus: subscribeLomus => `${this.translate('subscribeLomus')} - ${this.translateOption(subscribeLomus)}`,
      status: status => `${this.translate('status')} - ${this.translateOption(status.value)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`LIST.MASTER_DATA.globalReportsSettings.filters.labels.${key}`);
  }

  private translateOption(key: string | boolean): string {
    return this.translation.translate(`LIST.MASTER_DATA.globalReportsSettings.filters.options.labels.${key}`);
  }
}
