import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '~app/shared/shared.module';
import { CreateGlobalReportDialogComponent } from './create-global-report-dialog/create-global-report-dialog.component';
import { GlobalReportsSettingsComponent } from './global-reports-settings/global-reports-settings.component';

@NgModule({
  imports: [CommonModule, SharedModule],
  exports: [GlobalReportsSettingsComponent],
  declarations: [GlobalReportsSettingsComponent, CreateGlobalReportDialogComponent]
})
export class GlobalReportsSettingsModule {}
