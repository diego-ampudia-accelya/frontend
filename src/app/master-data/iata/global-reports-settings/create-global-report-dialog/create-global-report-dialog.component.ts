import { AfterViewInit, Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, first, switchMap, takeUntil } from 'rxjs/operators';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { UserType } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';
import { GlobalDropdownOption, GlobalReportsSettingsFilterBE } from '../model/global-report-settings-filter.model';
import * as GlobalOption from '../model/global-reports-settings-options';
import { GlobalReportsSettingsCreateRequest } from '../model/global-reports-settings.model';
import { GlobalReportsSettingsService } from '../services/global-reports-settings.service';
import { CreateReportGlobalForm } from './model/create-report-global-form.model';

@Component({
  selector: 'bspl-create-global-report-dialog',
  templateUrl: './create-global-report-dialog.component.html'
})
export class CreateGlobalReportDialogComponent implements OnInit, OnDestroy, AfterViewInit {
  public form: FormGroup;
  public userTypeControl: FormControl;
  public fileTypeOptionList: DropdownOption<GlobalDropdownOption>[];
  public formatFilterOptionList: DropdownOption<GlobalDropdownOption>[];
  public userTypeOptionList: DropdownOption<GlobalDropdownOption>[];
  public frequencyOptionList: DropdownOption<GlobalDropdownOption>[];
  public channelOptionList: DropdownOption<GlobalDropdownOption>[];
  public thresholdOptionList: DropdownOption<boolean>[];
  public subscribeLomusOptionList: DropdownOption<boolean>[];
  public isUserTypeSelectAirline = false;

  private createButton: ModalAction;
  private formFactory: FormUtil;
  private destroy$ = new Subject();
  private actionEmitter = new Subject();
  private subscribeLomusControl: FormControl;

  constructor(
    public dialogService: DialogService,
    public config: DialogConfig,
    public translationService: L10nTranslationService,
    public notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    private globalReportsSettingsService: GlobalReportsSettingsService,
    private globalErrorHandlerService: ErrorHandler
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeCreateButton();
    this.initializeCreateButtonListener();
    this.initializeForm();
    this.populateDropdownFormOptions();
    this.actionEmitter = this.config.data.actionEmitter;
  }

  public ngAfterViewInit(): void {
    this.formStatusChanges();
    this.userTypeValueChanges();
  }

  private userTypeValueChanges(): void {
    this.userTypeControl.valueChanges.subscribe((value: GlobalDropdownOption) => {
      const selectValue: GlobalDropdownOption = value;
      this.isUserTypeSelectAirline = selectValue?.value.toLowerCase() === UserType.AIRLINE.toLowerCase();
      if (!this.isUserTypeSelectAirline) this.subscribeLomusControl.reset();
      this.removeOrSetSubscribeLomusValidator(this.isUserTypeSelectAirline);
    });
  }

  private formStatusChanges(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe((status: string) => {
      if (this.createButton) {
        this.createButton.isDisabled = status !== 'VALID';
      }
    });
  }

  private initializeCreateButton(): void {
    this.createButton = this.config.data.buttons.find(
      (button: { type: FooterButton }) => button.type === FooterButton.Create
    );

    if (this.createButton) {
      this.createButton.isDisabled = true;
    }
  }

  private removeOrSetSubscribeLomusValidator(isAirlineSelected: boolean): void {
    if (isAirlineSelected) {
      this.subscribeLomusControl.setValidators(Validators.required);
    } else {
      this.subscribeLomusControl.clearValidators();
    }
    this.subscribeLomusControl.updateValueAndValidity();
  }

  private initializeForm(): void {
    this.userTypeControl = new FormControl(null, Validators.required);
    this.subscribeLomusControl = new FormControl(null, []);

    this.form = this.formFactory.createGroup<CreateReportGlobalForm>({
      reportDescription: ['', [Validators.required, Validators.maxLength(40)]],
      fileType: [null, Validators.required],
      format: [null, Validators.required],
      userType: this.userTypeControl,
      frequency: [null, Validators.required],
      channel: [null, Validators.required],
      threshold: [null, Validators.required],
      subscribeLomus: this.subscribeLomusControl
    });
  }

  private initializeCreateButtonListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        first(),
        filter(action => action?.clickedBtn === FooterButton.Create),
        switchMap(() => this.requestForm()),
        takeUntil(this.destroy$)
      )
      .subscribe(
        () => {
          this.notificationService.showSuccess(
            this.translationService.translate(
              'LIST.MASTER_DATA.globalReportsSettings.createDialog.messages.createSuccess'
            )
          );
          this.dialogService.close();
          this.actionEmitter.next();
        },
        error => {
          this.initializeCreateButtonListener();
          this.globalErrorHandlerService.handleError(error);
        }
      );
  }

  private requestForm(): Observable<GlobalReportsSettingsFilterBE> {
    const formValue = this.form.value;
    const subscribeLomusValue = this.isUserTypeSelectAirline ? formValue.subscribeLomus : this.isUserTypeSelectAirline;

    const requestValues: GlobalReportsSettingsCreateRequest = {
      reportDescription: formValue.reportDescription,
      fileType: formValue.fileType.value,
      format: formValue.format.value,
      userType: formValue.userType.value,
      frequency: formValue.frequency.value,
      channel: formValue.channel.value,
      threshold: formValue.threshold,
      subscribeLomus: subscribeLomusValue
    };

    return this.globalReportsSettingsService.create(requestValues);
  }

  private generateOptionList(enumOptions): DropdownOption<GlobalDropdownOption>[] {
    return Object.entries(enumOptions).map(([key, val]: [string, string]) => ({
      value: { translationKey: key, value: val },
      label: this.translationService.translate(
        `LIST.MASTER_DATA.globalReportsSettings.createDialog.options.labels.${key}`
      )
    }));
  }

  private generateYesNoOptionList(): DropdownOption<boolean>[] {
    return [true, false].map(item => ({
      label: this.translateBooleanToYesNo(item),
      value: item
    }));
  }

  private translateBooleanToYesNo(booleanValue: boolean): string {
    return this.translationService.translate(
      `LIST.MASTER_DATA.globalReportsSettings.yesNoBooleanValue.${booleanValue}`
    );
  }

  private populateDropdownFormOptions(): void {
    this.fileTypeOptionList = this.generateOptionList(GlobalOption.FileType);
    this.formatFilterOptionList = this.generateOptionList(GlobalOption.Format);
    this.userTypeOptionList = this.generateOptionList(GlobalOption.UserType);
    this.frequencyOptionList = this.generateOptionList(GlobalOption.Frequency);
    this.channelOptionList = this.generateOptionList(GlobalOption.Channel);
    this.thresholdOptionList = this.generateYesNoOptionList();
    this.subscribeLomusOptionList = this.generateYesNoOptionList();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
