import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { GlobalReportsSettingsFilterBE } from '../model/global-report-settings-filter.model';
import { GlobalReportsSettingsCreateRequest } from '../model/global-reports-settings.model';
import { GlobalReportsSettingsService } from '../services/global-reports-settings.service';
import { CreateGlobalReportDialogComponent } from './create-global-report-dialog.component';

describe('CreateGlobalReportDialogComponent', () => {
  let component: CreateGlobalReportDialogComponent;
  let fixture: ComponentFixture<CreateGlobalReportDialogComponent>;
  let formBuilder: FormBuilder;

  const globalReportsSettingsServiceSpy: SpyObject<GlobalReportsSettingsService> =
    createSpyObject(GlobalReportsSettingsService);

  const dialogConfigSpy: DialogConfig = {
    data: {
      title: 'LIST.MASTER_DATA.globalReportsSettings.createDialog.title',
      hasCancelButton: true,
      isClosable: true,
      footerButtonsType: [{ type: FooterButton.Create, isDisabled: true }],
      buttons: [{ title: 'BUTTON.DEFAULT.CREATE', buttonDesign: 'primary', type: 'create', isDisabled: true }]
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [CreateGlobalReportDialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        DialogService,
        L10nTranslationService,
        NotificationService,
        FormBuilder,
        ReactiveSubject,
        ErrorHandler,
        MessageService,
        { provide: DialogConfig, UseValue: dialogConfigSpy },
        { provide: GlobalReportsSettingsService, UseValue: globalReportsSettingsServiceSpy }
      ]
    }).compileComponents();
  }));

  function createMockForm(values: GlobalReportsSettingsCreateRequest): FormGroup {
    return formBuilder.group({
      reportDescription: values.reportDescription,
      fileType: { value: values.fileType },
      format: { value: values.format },
      userType: { value: values.userType },
      frequency: { value: values.frequency },
      channel: { value: values.channel },
      threshold: values.threshold,
      subscribeLomus: values.subscribeLomus
    });
  }

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGlobalReportDialogComponent);
    component = fixture.componentInstance;
    component['config'] = dialogConfigSpy;
    formBuilder = TestBed.inject(FormBuilder);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clear Validators when removeOrSetSubscribeLomusValidator method is triggered and airline is not selected', () => {
    const setValidatorsSpy = spyOn<any>(component['subscribeLomusControl'], 'clearValidators');
    component['removeOrSetSubscribeLomusValidator'](false);

    expect(setValidatorsSpy).toHaveBeenCalled();
  });

  it('should set Validators when removeOrSetSubscribeLomusValidator method is triggered and airline is selected', () => {
    const setValidatorsSpy = spyOn<any>(component['subscribeLomusControl'], 'setValidators');
    component['removeOrSetSubscribeLomusValidator'](true);

    expect(setValidatorsSpy).toHaveBeenCalled();
  });

  it('should initialize Create Button Listener and trigger requestForm method', fakeAsync(() => {
    const requestFormMockValues: GlobalReportsSettingsFilterBE = {
      id: 1,
      reportDescription: 'TRNXS - ENROLLED ATMS WITH CONSENT',
      fileType: 'TIP_2A',
      format: ['CSV+PDF'],
      userType: ['Agent'],
      frequency: ['Daily+Monthly'],
      channel: 'BSPlink',
      threshold: false,
      subscribeLomus: false,
      status: 'Deleted'
    };
    const requestFormSpy = spyOn<any>(component, 'requestForm');
    (component as any).reactiveSubject.asObservable = new BehaviorSubject({ clickedBtn: FooterButton.Create });
    (component as any).actionEmitter = new Subject();
    requestFormSpy.and.returnValue(of(null));

    globalReportsSettingsServiceSpy.create.and.returnValue(of(requestFormMockValues));
    component['requestForm']().subscribe();
    tick();

    component['initializeCreateButtonListener']();

    expect(component['requestForm']).toHaveBeenCalled();
  }));

  it('should request the form when requestForm method is triggered with valid form values', fakeAsync(() => {
    const requestFormMockValues: GlobalReportsSettingsFilterBE = {
      id: 1,
      reportDescription: 'TRNXS - ENROLLED ATMS WITH CONSENT',
      fileType: 'TIP_2A',
      format: ['CSV+PDF'],
      userType: ['Agent'],
      frequency: ['Daily+Monthly'],
      channel: 'BSPlink',
      threshold: false,
      subscribeLomus: false,
      status: 'Deleted'
    };
    const formValues = {
      reportDescription: 'Sample Report',
      fileType: 'TIP_2A',
      format: 'CSV+PDF',
      userType: 'Agent',
      frequency: 'Daily+Monthly',
      channel: 'BSPlink',
      threshold: false,
      subscribeLomus: true
    };
    const mockForm = createMockForm(formValues);
    component.form = mockForm;
    globalReportsSettingsServiceSpy.create.and.returnValue(of(requestFormMockValues));
    component['requestForm']().subscribe(() => {
      tick();
      expect(globalReportsSettingsServiceSpy.create).toHaveBeenCalled();
    });
  }));

  it('should set isDisabled property of the createButton to False when form Status is equal to VALID', () => {
    const formValues = {
      reportDescription: 'Sample Report',
      fileType: 'TIP_2A',
      format: 'CSV+PDF',
      userType: 'Agent',
      frequency: 'Daily+Monthly',
      channel: 'BSPlink',
      threshold: false,
      subscribeLomus: true
    };
    const mockForm = createMockForm(formValues);
    component.form = mockForm;
    spyOn(component.form.statusChanges, 'pipe').and.returnValue(of('VALID'));
    component['formStatusChanges']();

    expect(component['createButton'].isDisabled).toBeFalsy();
  });

  it('should subscribe to userType ValueChanges and set the selected value', fakeAsync(() => {
    component['userTypeControl'] = new FormControl(null, Validators.required);
    spyOn(component['userTypeControl'].valueChanges, 'subscribe').and.callThrough();

    component['userTypeValueChanges']();
    component['userTypeControl'].setValue({
      translationKey: 'Airline',
      value: 'Airline'
    });
    tick();

    expect(component['userTypeControl'].value).toEqual({
      translationKey: 'Airline',
      value: 'Airline'
    });
    expect(component['userTypeControl'].valueChanges.subscribe).toHaveBeenCalled();
  }));
});
