export interface CreateReportGlobalForm {
  reportDescription: string;
  fileType: string;
  format: string;
  userType: string;
  frequency: string;
  channel: string;
  threshold: boolean;
  subscribeLomus: boolean;
}
