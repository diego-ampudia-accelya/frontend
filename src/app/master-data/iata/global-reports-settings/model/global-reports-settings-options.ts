export enum FileType {
  TIP_2A = 'TIP_2A',
  TIP_2B = 'TIP_2B',
  TIP_4 = 'TIP_4',
  TIP_7A = 'TIP_7A',
  TIP_7B = 'TIP_7B',
  TIP_8 = 'TIP_8'
}

export enum UserType {
  Agent = 'Agent',
  Airline = 'Airline',
  IATA = 'IATA'
}

export enum Format {
  CSV = 'CSV',
  PDF = 'PDF',
  CSV_PDF = 'CSV+PDF'
}

export enum Frequency {
  Daily = 'Daily',
  Monthly = 'Monthly',
  Daily_Monthly = 'Daily+Monthly'
}

export enum Channel {
  BSPlink = 'BSPlink',
  Portal = 'Portal'
}

export enum Status {
  Active = 'Active',
  Deleted = 'Deleted'
}
