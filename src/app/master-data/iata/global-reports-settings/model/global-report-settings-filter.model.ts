export interface GlobalDropdownOption {
  translationKey: string;
  value: string;
}

export interface GlobalReportsSettingsFilter {
  reportDescription: string;
  fileType: string;
  format: GlobalDropdownOption[];
  userType: GlobalDropdownOption[];
  frequency: GlobalDropdownOption[];
  channel: GlobalDropdownOption;
  threshold: boolean;
  subscribeLomus: boolean;
  status: GlobalDropdownOption;
}

export interface GlobalReportsSettingsFilterBE {
  id: number;
  reportDescription: string;
  fileType: string;
  format: string[];
  userType: string[];
  frequency: string[];
  channel: string;
  threshold: boolean;
  subscribeLomus: boolean;
  status: string;
}
