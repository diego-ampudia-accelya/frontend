export interface GlobalReportsSettings {
  reportDescription: string;
  fileType: string;
  format: string;
  userType: string;
  frequency: string;
  channel: string;
  threshold: boolean;
  subscribeLomus: boolean;
  status: string;
}
export interface GlobalReportsSettingsRow extends GlobalReportsSettings {
  id: number;
  isRowSelected: boolean;
}
export interface GlobalReportsSettingsCreateRequest {
  reportDescription: string;
  fileType: string;
  format: string;
  userType: string;
  frequency: string;
  channel: string;
  threshold: boolean;
  subscribeLomus: boolean;
}
