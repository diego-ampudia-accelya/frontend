import { AgentSummary } from '~app/shared/models';

export interface ReinstatedAgentsFilter {
  agentCode: AgentSummary[];
  reinstatementDate: Date[];
}

export interface ReinstatedAgentsFilterBE {
  agentCode: string[];
  fromReinstatementDate: string;
  toReinstatementDate: string;
}
