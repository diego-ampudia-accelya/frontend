export interface ReinstatedAgents {
  id: number;
  agentCode: string;
  name: string;
  reinstatementDate: string;
}
