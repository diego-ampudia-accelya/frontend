import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { ReinstatedAgentsFilter } from '../model/reinstated-agents-filter.model';

import { ReinstatedAgentsFilterFormatter } from './reinstated-agents-filter-formatter';

const filters: ReinstatedAgentsFilter = {
  agentCode: [
    {
      id: '1245',
      name: 'Name test',
      code: '18475'
    }
  ],
  reinstatementDate: [new Date('2022-11-30')]
};

describe('ReinstatedAgentsFilterFormatter', () => {
  let formatter: ReinstatedAgentsFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new ReinstatedAgentsFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      { keys: ['agentCode'], label: 'MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.filter.label.agentCode - 18475' },
      {
        keys: ['reinstatementDate'],
        label: 'MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.filter.label.reinstatementDate - 30/11/2022'
      }
    ]);
  });
});
