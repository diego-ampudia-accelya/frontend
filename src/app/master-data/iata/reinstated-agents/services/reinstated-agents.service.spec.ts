import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { ReinstatedAgents } from '../model/reinstated-agents.model';
import { ReinstatedAgentsService } from './reinstated-agents.service';
import { DataQuery } from '~app/shared/components/list-view';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('Service: ReinstatedAgents', () => {
  let service: ReinstatedAgentsService;
  let httpClientSpy: SpyObject<HttpClient>;

  const reinstatedAgentsMock: ReinstatedAgents[] = [
    {
      agentCode: '1000005',
      reinstatementDate: '2022-11-30',
      id: 69831000005,
      name: 'NOMBRE 1000005'
    },
    {
      agentCode: '1000008',
      reinstatementDate: '2022-11-30',
      id: 69831000008,
      name: 'NOMBRE 1000008'
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        ReinstatedAgentsService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(ReinstatedAgentsService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/user-management/reinstated-agents');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let reinstatedAgentsData: PagedData<ReinstatedAgents>;

    const reinstatedAgentsDataMock: PagedData<ReinstatedAgents> = {
      records: reinstatedAgentsMock,
      total: reinstatedAgentsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(reinstatedAgentsDataMock));
    service.find(query).subscribe(res => {
      reinstatedAgentsData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/user-management/reinstated-agents?page=0&size=20');
    expect(reinstatedAgentsData).toEqual(reinstatedAgentsDataMock);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/user-management/reinstated-agents/download?exportAs=txt';

    service.download(query, exportOptions).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
