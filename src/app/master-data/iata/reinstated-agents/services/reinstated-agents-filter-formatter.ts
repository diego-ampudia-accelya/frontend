import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ReinstatedAgentsFilter } from '../model/reinstated-agents-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class ReinstatedAgentsFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: ReinstatedAgentsFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<ReinstatedAgentsFilter>> = {
      agentCode: agentCode => `${this.translate('label.agentCode')} - ${agentCode.map(agent => agent.code).join(', ')}`,
      reinstatementDate: reinstatementDate =>
        `${this.translate('label.reinstatementDate')} - ${rangeDateFilterTagMapper(reinstatementDate)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.filter.${key}`);
  }
}
