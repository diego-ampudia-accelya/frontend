import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ReinstatedAgentsFilter, ReinstatedAgentsFilterBE } from '../model/reinstated-agents-filter.model';
import { ReinstatedAgents } from '../model/reinstated-agents.model';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class ReinstatedAgentsService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/reinstated-agents`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<ReinstatedAgentsFilter>): Observable<PagedData<ReinstatedAgents>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<ReinstatedAgents>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery<ReinstatedAgentsFilter>>): RequestQuery<ReinstatedAgentsFilterBE> {
    const { agentCode, reinstatementDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        agentCode: agentCode && agentCode.map(agent => agent.code),
        fromReinstatementDate: reinstatementDate && toShortIsoDate(reinstatementDate[0]),
        toReinstatementDate:
          reinstatementDate && toShortIsoDate(reinstatementDate[1] ? reinstatementDate[1] : reinstatementDate[0])
      }
    });
  }

  public download(
    query: DataQuery<ReinstatedAgentsFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
