import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';

import { ReinstatedAgentsFilter } from './model/reinstated-agents-filter.model';
import { ReinstatedAgents } from './model/reinstated-agents.model';
import { ReinstatedAgentsFilterFormatter } from './services/reinstated-agents-filter-formatter';
import { ReinstatedAgentsService } from './services/reinstated-agents.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-reinstated-agents',
  templateUrl: './reinstated-agents.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: ReinstatedAgentsService }
  ]
})
export class ReinstatedAgentsComponent implements OnInit {
  public title = this.translationService.translate('MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.title');
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public agentsCodeOptions$: Observable<DropdownOption<AgentSummary>[]>;

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;

  constructor(
    public displayFormatter: ReinstatedAgentsFilterFormatter,
    public dataSource: QueryableDataSource<ReinstatedAgents>,
    private formBuilder: FormBuilder,
    private queryStorage: DefaultQueryStorage,
    private reinstatedAgentsService: ReinstatedAgentsService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private agentsDictironayService: AgentDictionaryService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.loadData(this.storedQuery);
    this.initColumns();
    this.populateFilterDropdowns();
  }

  public loadData(query: DataQuery<ReinstatedAgentsFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.downloadDialog.title'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.reinstatedAgentsService
    });
  }

  private populateFilterDropdowns(): void {
    this.agentsCodeOptions$ = this.agentsDictironayService.getDropdownOptions();
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<ReinstatedAgentsFilter>({
      agentCode: [],
      reinstatementDate: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'agentCode',
        name: 'MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.columns.agentCode'
      },
      {
        prop: 'name',
        name: 'MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.columns.name'
      },
      {
        prop: 'reinstatementDate',
        name: 'MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.columns.reinstatementDate'
      }
    ];
  }
}
