import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { CurrencyConversionChangesDialogConfigService } from './changes-dialog/changes-dialog-config.service';
import { CurrencyConversionChangesDialogService } from './changes-dialog/changes-dialog.service';
import { CurrencyConversionComponent } from './currency-conversion/currency-conversion.component';
import { CurrencyConversionFilterFormatter } from './services/currency-conversion-filter-formatter';
import { CurrencyConversionService } from './services/currency-conversion.service';
import { CurrencyConversionEffects } from './store/effects/currency-conversion.effects';
import * as fromCurrencyConversion from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [CurrencyConversionComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromCurrencyConversion.currencyConversionFeatureKey, fromCurrencyConversion.reducers),
    EffectsModule.forFeature([CurrencyConversionEffects])
  ],
  exports: [CurrencyConversionComponent],
  providers: [
    CurrencyConversionService,
    CurrencyConversionFilterFormatter,
    CurrencyConversionChangesDialogService,
    CurrencyConversionChangesDialogConfigService
  ]
})
export class CurrencyConversionModule {}
