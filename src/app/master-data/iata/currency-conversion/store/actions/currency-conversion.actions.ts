import { createAction, props } from '@ngrx/store';

import { CurrencyConversionFilter, CurrencyConversionViewModel } from '../../models/currency-conversion.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const load = createAction('[Currency Conversion] Load');
export const loadSuccess = createAction(
  '[Currency Conversion] Load Success',
  props<{
    data: PagedData<CurrencyConversionViewModel>;
  }>()
);
export const loadError = createAction('[Currency Conversion] Load Error');

export const search = createAction(
  '[Currency Conversion] Search',
  props<{ query?: DataQuery<CurrencyConversionFilter> }>()
);
export const searchConfirmed = createAction('[Currency Conversion] Search Confirmed');
export const searchCancelled = createAction('[Currency Conversion] Search Cancelled');
export const searchSuccess = createAction(
  '[Currency Conversion] Search Success',
  props<{ data: PagedData<CurrencyConversionViewModel> }>()
);
export const searchError = createAction('[Currency Conversion] Search Error');

export const modify = createAction('[Currency Conversion] Modify', props<{ items: CurrencyConversionViewModel[] }>());

export const openApplyChanges = createAction('[Currency Conversion] Open Apply Changes');
export const applyChangesSuccess = createAction('Currency Conversion] Apply Success');
export const applyChangesError = createAction('[Currency Conversion] Apply Error');

export const discard = createAction('[Currency Conversion] Discard');

export const download = createAction('[Currency Conversion] Download');
export const openDownload = createAction('[Currency Conversion] Open Download');
