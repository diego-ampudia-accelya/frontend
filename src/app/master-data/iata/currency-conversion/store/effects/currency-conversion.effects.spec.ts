import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { NotificationService } from '~app/shared/services/notification.service';
import { CurrencyConversionChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { CurrencyConversionFilter } from '../../models/currency-conversion.model';
import { CurrencyConversionService } from '../../services/currency-conversion.service';
import { CurrencyConversionActions } from '../actions';
import * as fromCurrencyConversion from '../reducers';
import { CurrencyConversionEffects } from './currency-conversion.effects';

describe('CurrencyConversionEffects', () => {
  let effects: CurrencyConversionEffects;
  let actions$: Observable<Action>;
  let mockStore: MockStore<AppState>;

  const currencyConversionChangesDialogServiceSpy: SpyObject<CurrencyConversionChangesDialogService> = createSpyObject(
    CurrencyConversionChangesDialogService
  );
  const notificationServiceSpy: SpyObject<NotificationService> = createSpyObject(NotificationService);
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const initialState = {
    auth: {
      user: null
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockActions(() => actions$),
        CurrencyConversionEffects,
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromCurrencyConversion.getDownloadQuery, value: null },
            { selector: fromCurrencyConversion.getQuery, value: null }
          ]
        }),
        {
          provide: CurrencyConversionService,
          useValue: {
            find: () => of({})
          }
        },
        {
          provide: CurrencyConversionChangesDialogService,
          useValue: currencyConversionChangesDialogServiceSpy
        },
        {
          provide: NotificationService,
          useValue: notificationServiceSpy
        },
        {
          provide: DialogService,
          useValue: dialogServiceSpy
        }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(CurrencyConversionEffects);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('Download Currency Conversion Effects', () => {
    beforeEach(() => {
      actions$ = of(CurrencyConversionActions.download());
      currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges.calls.reset();
    });

    it('should trigger first confirmUnsavedChanges method, and then dispatcher searchConfirmed action when the Apply Footer Button is clicked', fakeAsync(() => {
      let dispatchedAction: Action;
      currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Apply));

      effects.download$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(CurrencyConversionActions.searchConfirmed());
    }));

    it('should trigger first confirmUnsavedChanges method, and then return empty when the Cancel Footer Button is clicked', fakeAsync(() => {
      let dispatchedAction: Action;
      currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));

      effects.download$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction).not.toEqual(CurrencyConversionActions.searchConfirmed());
      expect(dispatchedAction).not.toEqual(CurrencyConversionActions.openDownload());
    }));

    it('should trigger first confirmUnsavedChanges method, and then dispatcher openDownload action when the Discard Footer Button is clicked', fakeAsync(() => {
      let dispatchedAction: Action;
      currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Discard));

      effects.download$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(CurrencyConversionActions.openDownload());
    }));

    it('should open dialog modal when openDownload action is triggered', fakeAsync(() => {
      actions$ = of(CurrencyConversionActions.openDownload());
      mockStore.overrideSelector(fromCurrencyConversion.getDownloadQuery, defaultQuery);
      dialogServiceSpy.open.and.returnValue(of({}));

      effects.openDownload$.subscribe();
      tick();
      expect(dialogServiceSpy.open).toHaveBeenCalled();
    }));
  });

  describe('Apply Changes Currency Conversion Effects', () => {
    it('should trigger first confirmApplyChanges method, and then dispatcher searchConfirmed action when the Apply Footer Button is clicked', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(CurrencyConversionActions.openApplyChanges());
      currencyConversionChangesDialogServiceSpy.confirmApplyChanges.and.returnValue(of(FooterButton.Apply));

      effects.openApplyChanges$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(currencyConversionChangesDialogServiceSpy.confirmApplyChanges).toHaveBeenCalled();
      expect(dispatchedAction).toEqual(CurrencyConversionActions.searchConfirmed());
    }));

    it('should trigger first applyChangesSuccess and showSuccess from notification Service when applySuccess action is triggered', fakeAsync(() => {
      actions$ = of(CurrencyConversionActions.applyChangesSuccess());

      effects.applySuccess$.subscribe();
      tick();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
    }));
  });

  describe('Search Currency Conversion Effects', () => {
    it('should trigger first confirmApplyChanges method, and then dispatcher searchConfirmed action when the Apply Footer Button is clicked', fakeAsync(() => {
      let dispatchedAction: Action;
      const queryMock = defaultQuery as DataQuery<CurrencyConversionFilter>;
      actions$ = of(CurrencyConversionActions.search({ query: queryMock }));
      currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Apply));

      effects.search$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction.type).toEqual(CurrencyConversionActions.searchConfirmed.type);
    }));

    it('should trigger first confirmApplyChanges method, and then dispatcher searchCancelled action when the Cancel Footer Button is clicked', fakeAsync(() => {
      let dispatchedAction: Action;
      const queryMock = defaultQuery as DataQuery<CurrencyConversionFilter>;
      actions$ = of(CurrencyConversionActions.search({ query: queryMock }));
      currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Cancel));

      effects.search$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges).toHaveBeenCalled();
      expect(dispatchedAction.type).toEqual(CurrencyConversionActions.searchCancelled.type);
    }));

    it('should trigger first confirmApplyChanges method, and then dispatcher searchConfirmed action when the Apply Footer Button is clicked', fakeAsync(() => {
      currencyConversionChangesDialogServiceSpy.confirmUnsavedChanges.and.returnValue(of(FooterButton.Apply));
      actions$ = of(CurrencyConversionActions.search({ query: defaultQuery }));
      mockStore.overrideSelector(fromCurrencyConversion.getQuery, defaultQuery);

      let dispatchedAction: Action;
      effects.search$.subscribe(action => (dispatchedAction = action));
      tick();
      expect(dispatchedAction.type).toEqual(CurrencyConversionActions.searchConfirmed.type);
    }));

    it('should trigger searchConfirmed action, and then dispacher searchSuccess action when the data service is triggered with no errors', fakeAsync(() => {
      actions$ = of(CurrencyConversionActions.searchConfirmed());
      mockStore.overrideSelector(fromCurrencyConversion.getQuery, defaultQuery);

      let dispatchedAction: Action;
      effects.searchConfirmed$.subscribe(action => (dispatchedAction = action));
      tick();
      expect(dispatchedAction.type).toEqual(CurrencyConversionActions.searchSuccess.type);
    }));
  });

  describe('Load Currency Conversion Effects', () => {
    it('should trigger load action, and then dispacher loadSuccess action when the data service is triggered with no errors', fakeAsync(() => {
      actions$ = of(CurrencyConversionActions.load());
      mockStore.overrideSelector(fromCurrencyConversion.getQuery, defaultQuery);

      let dispatchedAction: Action;
      effects.load$.subscribe(action => (dispatchedAction = action));
      tick();
      expect(dispatchedAction.type).toEqual(CurrencyConversionActions.loadSuccess.type);
    }));
  });
});
