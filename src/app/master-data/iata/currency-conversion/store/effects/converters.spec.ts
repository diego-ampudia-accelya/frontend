import {
  CurrencyConversionBE,
  CurrencyConversionChange,
  CurrencyConversionViewModel
} from '../../models/currency-conversion.model';
import { converter } from './converters';

const mockCurrencyCoversion: CurrencyConversionBE = {
  id: 1,
  airlineCode: 'Airline',
  name: 'Currency Conversion',
  designator: 'Airline Desingnator',
  active: true,
  cutpDecimalConversion: true
};

const mockCurrencyConversionViewModel: CurrencyConversionViewModel[] = [
  {
    id: 1,
    airlineCode: 'Airline',
    name: 'Currency Conversion',
    designator: 'Airline Desingnator',
    active: true,
    cutpDecimalConversion: true,
    original: {
      id: 1,
      airlineCode: 'Airline',
      name: 'Currency Conversion',
      designator: 'Airline Desingnator',
      active: true,
      cutpDecimalConversion: true
    }
  }
];

describe('Currency Conversion Converters functions', () => {
  it('should convert data model to view model', () => {
    const result = converter.toViewModel(mockCurrencyCoversion);
    expect(result).toEqual(mockCurrencyConversionViewModel[0]);
  });

  it('should convert data model to view model', () => {
    const result = converter.toChangeDto(mockCurrencyConversionViewModel[0]);
    const expectedConversionViewModel: CurrencyConversionChange = {
      id: 1,
      cutpDecimalConversion: true
    };
    expect(result).toEqual(expectedConversionViewModel);
  });
});
