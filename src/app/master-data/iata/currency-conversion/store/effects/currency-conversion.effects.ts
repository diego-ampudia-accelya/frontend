import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { EMPTY, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { CurrencyConversionChangesDialogService } from '../../changes-dialog/changes-dialog.service';
import { CurrencyConversionService } from '../../services/currency-conversion.service';
import { CurrencyConversionActions } from '../actions';
import * as fromCurrencyConversion from '../reducers';

import { converter } from './converters';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services/notification.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class CurrencyConversionEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CurrencyConversionActions.load),
      withLatestFrom(this.store.pipe(select(fromCurrencyConversion.getQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(result =>
            CurrencyConversionActions.loadSuccess({
              data: converter.toViewModels(result)
            })
          ),
          rethrowError(() => CurrencyConversionActions.loadError())
        )
      )
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CurrencyConversionActions.search),
      switchMap(() =>
        this.changesDialogService
          .confirmUnsavedChanges()
          .pipe(
            map(dialogResult =>
              dialogResult === FooterButton.Cancel
                ? CurrencyConversionActions.searchCancelled()
                : CurrencyConversionActions.searchConfirmed()
            )
          )
      )
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CurrencyConversionActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromCurrencyConversion.getQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(result =>
            CurrencyConversionActions.searchSuccess({
              data: converter.toViewModels(result)
            })
          ),
          rethrowError(() => CurrencyConversionActions.searchError())
        )
      )
    )
  );

  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CurrencyConversionActions.openApplyChanges),
      switchMap(() => this.changesDialogService.confirmApplyChanges()),
      switchMap(dialogResult =>
        dialogResult === FooterButton.Apply ? of(CurrencyConversionActions.searchConfirmed()) : EMPTY
      ),
      rethrowError(() => CurrencyConversionActions.applyChangesError())
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(CurrencyConversionActions.applyChangesSuccess),
        tap(() => this.notificationService.showSuccess('LIST.MASTER_DATA.currencyConversion.applyChanges.success'))
      ),
    { dispatch: false }
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CurrencyConversionActions.download),
      switchMap(() => this.changesDialogService.confirmUnsavedChanges()),
      switchMap(result => {
        if (result === FooterButton.Cancel) {
          return EMPTY;
        }

        if (result === FooterButton.Apply) {
          return of(CurrencyConversionActions.searchConfirmed());
        }

        return of(CurrencyConversionActions.openDownload());
      })
    )
  );

  public openDownload$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(CurrencyConversionActions.openDownload),
        withLatestFrom(this.store.pipe(select(fromCurrencyConversion.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: CurrencyConversionService,
    private store: Store<AppState>,
    private changesDialogService: CurrencyConversionChangesDialogService,
    private notificationService: NotificationService,
    private dialogService: DialogService
  ) {}
}
