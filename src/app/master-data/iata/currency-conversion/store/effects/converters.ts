import {
  CurrencyConversionBE,
  CurrencyConversionChange,
  CurrencyConversionViewModel
} from '../../models/currency-conversion.model';
import { PagedData } from '~app/shared/models/paged-data.model';

const toViewModel = (dataModel: CurrencyConversionBE): CurrencyConversionViewModel => ({
  ...dataModel,
  original: dataModel
});

const toDataModel = (viewModel: CurrencyConversionViewModel): CurrencyConversionBE => {
  const { original, ...dataModel } = viewModel;

  return dataModel;
};

const toChangeDto = (viewModel: CurrencyConversionViewModel): CurrencyConversionChange => {
  const model = toDataModel(viewModel);

  return {
    id: model.id,
    cutpDecimalConversion: model.cutpDecimalConversion
  };
};

const toViewModels = (pagedData: PagedData<CurrencyConversionBE>): PagedData<CurrencyConversionViewModel> => ({
  ...pagedData,
  records: (pagedData.records || []).map(toViewModel)
});

export const converter = { toViewModel, toChangeDto, toViewModels };
