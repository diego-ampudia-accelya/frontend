import { cloneDeep } from 'lodash';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';
import { CurrencyConversionFilter, CurrencyConversionViewModel } from '../../models/currency-conversion.model';
import { CurrencyConversionActions } from '../actions';
import { getData, getLoading, getQuery, hasChanges, initialState, reducer, State } from './currency-conversion.reducer';

const loadData = (state: State, data: PagedData<CurrencyConversionViewModel>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (
  storedQuery: DataQuery<CurrencyConversionFilter>
): DataQuery<CurrencyConversionFilter> => {
  const query: DataQuery<CurrencyConversionFilter> = storedQuery || cloneDeep(defaultQuery);

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = {
    ...query.filterBy
  };

  return initialQuery;
};

const mockCurrencyConversionData: CurrencyConversionViewModel[] = [
  {
    id: 1,
    airlineCode: '075',
    name: 'Currency Conversion 1',
    designator: 'CC-075',
    active: true,
    cutpDecimalConversion: false,
    original: {
      id: 1,
      airlineCode: '075',
      name: 'Currency Conversion 1',
      designator: 'CC-075',
      active: true,
      cutpDecimalConversion: false
    }
  }
];

const mockPagedData: PagedData<CurrencyConversionViewModel> = {
  pageNumber: 1,
  pageSize: 10,
  total: 100,
  records: mockCurrencyConversionData
};

describe('Currency Conversion Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual(initialState);
  });

  describe('Reducers', () => {
    it('should handle Currency Conversion Actions load', () => {
      const state: State = { ...initialState };
      const action = CurrencyConversionActions.load();
      const expectedState: State = {
        ...initialState,
        loading: true,
        query: determineInitialQuery(state.query)
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Actions loadSuccess', () => {
      const state: State = { ...initialState, loading: true };
      const action = CurrencyConversionActions.loadSuccess({ data: mockPagedData });
      const expectedState: State = loadData(state, mockPagedData);
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Action loadError', () => {
      const state: State = { ...initialState, loading: true };
      const action = CurrencyConversionActions.loadError();
      const expectedState: State = {
        ...initialState,
        loading: false,
        data: []
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Action search', () => {
      const state: State = { ...initialState, loading: true };
      const action = CurrencyConversionActions.search({ query: defaultQuery });
      const expectedState: State = {
        ...initialState,
        loading: true,
        query: determineInitialQuery(state.query)
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Action searchConfirmed', () => {
      const state: State = { ...initialState, loading: true };
      const action = CurrencyConversionActions.searchConfirmed();
      const expectedState: State = {
        ...initialState,
        loading: true,
        previousQuery: null
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Actions searchCancelled', () => {
      const state: State = { ...initialState };
      const action = CurrencyConversionActions.searchCancelled();
      const expectedState: State = {
        ...state,
        previousQuery: null,
        query: state.previousQuery
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Actions searchSuccess', () => {
      const state: State = { ...initialState, loading: true };
      const action = CurrencyConversionActions.searchSuccess({ data: mockPagedData });
      const expectedState: State = loadData(state, mockPagedData);
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Actions modify', () => {
      const state: State = { ...initialState };
      const items: CurrencyConversionViewModel[] = mockCurrencyConversionData;
      const action = CurrencyConversionActions.modify({ items });
      const expectedState: State = {
        ...state,
        data: items
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Actions applyChangesSuccess', () => {
      const state: State = {
        ...initialState,
        data: mockCurrencyConversionData
      };
      const action = CurrencyConversionActions.applyChangesSuccess();
      const expectedState: State = {
        ...state,
        data: state.data.map(({ original, ...modified }) => ({ ...modified, original: modified }))
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Currency Conversion Actions discard', () => {
      const state: State = {
        ...initialState,
        data: mockCurrencyConversionData
      };
      const action = CurrencyConversionActions.discard();
      const expectedState: State = {
        ...state,
        data: state.data.map(({ original }) => ({ ...original, original }))
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });
  });

  describe('Selectors', () => {
    it('should get loading state', () => {
      const state: State = { ...initialState, loading: true };
      expect(getLoading(state)).toEqual(true);
    });

    it('should get data state', () => {
      const state: State = { ...initialState, data: mockCurrencyConversionData };
      expect(getData(state)).toEqual(mockCurrencyConversionData);
    });

    it('should get query state', () => {
      const state: State = { ...initialState, query: defaultQuery };
      expect(getQuery(state)).toEqual(defaultQuery);
    });

    it('should check if there are changes in the state', () => {
      const state: State = { ...initialState, data: mockCurrencyConversionData };
      expect(hasChanges(state)).toEqual(false);
    });
  });
});
