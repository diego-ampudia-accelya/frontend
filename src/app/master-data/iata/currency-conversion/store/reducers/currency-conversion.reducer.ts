import { createReducer, on } from '@ngrx/store';
import { cloneDeep, isEqual } from 'lodash';

import { CurrencyConversionFilter, CurrencyConversionViewModel } from '../../models/currency-conversion.model';
import { CurrencyConversionActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

export const currencyConversionListKey = 'list';

export interface State {
  query: DataQuery<CurrencyConversionFilter>;
  previousQuery: DataQuery<CurrencyConversionFilter>;
  data: CurrencyConversionViewModel[];
  loading: boolean;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false
};

const loadData = (state: State, data: PagedData<CurrencyConversionViewModel>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (
  storedQuery: DataQuery<CurrencyConversionFilter>
): DataQuery<CurrencyConversionFilter> => {
  const query: DataQuery<CurrencyConversionFilter> = storedQuery || cloneDeep(defaultQuery);

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = {
    ...query.filterBy
  };

  return initialQuery;
};

export const reducer = createReducer(
  initialState,
  on(CurrencyConversionActions.load, state => ({
    ...state,
    loading: true,
    query: determineInitialQuery(state.query)
  })),
  on(CurrencyConversionActions.loadSuccess, (state, { data }) => ({
    ...loadData(state, data)
  })),
  on(CurrencyConversionActions.search, (state, { query = state.query }) => ({
    ...state,
    query: {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    },
    previousQuery: state.query
  })),
  on(CurrencyConversionActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(CurrencyConversionActions.searchCancelled, state => ({
    ...state,
    previousQuery: null,
    query: state.previousQuery
  })),
  on(CurrencyConversionActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(CurrencyConversionActions.modify, (state, { items }) => ({
    ...state,
    data: items
  })),
  on(CurrencyConversionActions.applyChangesSuccess, state => ({
    ...state,
    data: state.data.map(({ original, ...modified }) => ({ ...modified, original: modified }))
  })),
  on(CurrencyConversionActions.discard, state => ({
    ...state,
    data: state.data.map(({ original }) => ({ ...original, original }))
  })),
  on(CurrencyConversionActions.searchError, CurrencyConversionActions.loadError, state => ({
    ...state,
    loading: false,
    data: []
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;

export const hasChanges = (state: State) => getChanges(state).length > 0;

export const getChanges = ({ data }: State) => data.filter(({ original, ...item }) => !isEqual(item, original));
