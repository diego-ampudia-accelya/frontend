import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromCurrencyConversion from './currency-conversion.reducer';
import { AppState } from '~app/reducers';

export const currencyConversionFeatureKey = 'currencyConversion';

export interface State extends AppState {
  [currencyConversionFeatureKey]: CurrencyConversionState;
}

export interface CurrencyConversionState {
  [fromCurrencyConversion.currencyConversionListKey]: fromCurrencyConversion.State;
}

export function reducers(state: CurrencyConversionState | undefined, action: Action) {
  return combineReducers({
    [fromCurrencyConversion.currencyConversionListKey]: fromCurrencyConversion.reducer
  })(state, action);
}

export const getCurrencyConversion = createFeatureSelector<State, CurrencyConversionState>(
  currencyConversionFeatureKey
);

export const getList = createSelector(
  getCurrencyConversion,
  state => state[fromCurrencyConversion.currencyConversionListKey]
);

export const getLoading = createSelector(getList, fromCurrencyConversion.getLoading);

export const getData = createSelector(getList, fromCurrencyConversion.getData);

export const hasData = createSelector(getData, data => data && data.length > 0);

export const getQuery = createSelector(getList, fromCurrencyConversion.getQuery);

export const getDownloadQuery = createSelector(
  getQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

export const getChanges = createSelector(getList, fromCurrencyConversion.getChanges);

export const hasChanges = createSelector(getList, fromCurrencyConversion.hasChanges);

export const canApplyChanges = createSelector(
  getList,
  state => fromCurrencyConversion.hasChanges(state) && !fromCurrencyConversion.getLoading(state)
);
