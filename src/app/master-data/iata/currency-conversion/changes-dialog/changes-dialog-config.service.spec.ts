import { TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';
import { CurrencyConversionViewModel } from '../models/currency-conversion.model';
import { CurrencyConversionChangesDialogConfigService } from './changes-dialog-config.service';

describe('CurrencyConversionChangesDialogConfigService', () => {
  let service: CurrencyConversionChangesDialogConfigService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        CurrencyConversionChangesDialogConfigService,
        { provide: L10nTranslationService, useValue: { translate: () => '' } }
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(CurrencyConversionChangesDialogConfigService);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should build and return the proper data when build method is triggered', () => {
    const mockModifications: CurrencyConversionViewModel[] = [
      {
        original: {
          id: 1,
          airlineCode: 'ABC',
          name: 'Currency Conversion 1',
          designator: 'CC1',
          active: true,
          cutpDecimalConversion: false
        },
        id: 1,
        airlineCode: 'ABC',
        name: 'Currency Conversion 1',
        designator: 'CC1',
        active: true,
        cutpDecimalConversion: false
      }
    ];
    const mockQuestion: { title: string; details: string } = { title: 'question', details: 'details' };
    const result = service.build(mockModifications, mockQuestion);

    expect(result.data.title).toEqual(mockQuestion.title);
  });
});
