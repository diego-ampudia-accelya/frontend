export const translations = {
  applyChanges: {
    title: 'LIST.MASTER_DATA.currencyConversion.applyChanges.title',
    details: 'LIST.MASTER_DATA.currencyConversion.applyChanges.message'
  },
  unsavedChanges: {
    title: 'LIST.MASTER_DATA.currencyConversion.unsavedChanges.title',
    details: 'LIST.MASTER_DATA.currencyConversion.unsavedChanges.message'
  },
  enabled: 'LIST.MASTER_DATA.currencyConversion.enabled',
  disabled: 'LIST.MASTER_DATA.currencyConversion.disabled',
  groupTitle: 'LIST.MASTER_DATA.currencyConversion.decimalConversion'
};
