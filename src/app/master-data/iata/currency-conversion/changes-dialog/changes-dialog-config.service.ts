import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { CurrencyConversionViewModel } from '../models/currency-conversion.model';

import { translations } from './translations';
import { ButtonDesign, ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable()
export class CurrencyConversionChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(
    modifications: CurrencyConversionViewModel[],
    question: { title: string; details: string }
  ): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: modifications
          ? [{ type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary }, { type: FooterButton.Apply }]
          : [{ type: FooterButton.Apply }]
      },
      changes: modifications ? this.formatChanges(modifications) : null,
      message
    };
  }

  private formatChanges(modifications: CurrencyConversionViewModel[]): ChangeModel[] {
    const translateStatus = (status: boolean) =>
      this.translation.translate(status ? translations.enabled : translations.disabled);

    return modifications.map(item => ({
      group: this.translation.translate(translations.groupTitle),
      name: item.name,
      value: translateStatus(item.cutpDecimalConversion),
      originalValue: item.cutpDecimalConversion
    }));
  }
}
