import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { CurrencyConversionViewModel } from '../models/currency-conversion.model';
import { CurrencyConversionService } from '../services/currency-conversion.service';
import { CurrencyConversionActions } from '../store/actions';
import { converter } from '../store/effects/converters';
import * as fromCurrencyConversion from '../store/reducers';

import { CurrencyConversionChangesDialogConfigService } from './changes-dialog-config.service';
import { translations } from './translations';
import { ChangesDialogComponent, ChangesDialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class CurrencyConversionChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: CurrencyConversionChangesDialogConfigService,
    private store: Store<AppState>,
    private dataService: CurrencyConversionService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return of(null).pipe(
      withLatestFrom(this.store.pipe(select(fromCurrencyConversion.getChanges))),
      switchMap(([_, modifications]) =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    modifications: CurrencyConversionViewModel[],
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig = this.configBuilder.build(modifications, question);

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      switchMap(result => {
        let dialogResult$ = of(result);
        if (result === FooterButton.Apply) {
          dialogResult$ = this.apply(modifications, dialogConfig);
        } else if (result === FooterButton.Discard) {
          this.store.dispatch(CurrencyConversionActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(
    modifications: CurrencyConversionViewModel[],
    dialogConfig: ChangesDialogConfig
  ): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(modifications.map(converter.toChangeDto)).pipe(
        tap(() => this.store.dispatch(CurrencyConversionActions.applyChangesSuccess())),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }
}
