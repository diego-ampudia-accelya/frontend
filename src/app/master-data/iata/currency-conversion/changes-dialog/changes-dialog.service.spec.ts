import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { AppState } from '~app/reducers';
import { ButtonDesign, DialogService, FooterButton } from '~app/shared/components';
import { CurrencyConversionViewModel } from '../models/currency-conversion.model';
import { CurrencyConversionService } from '../services/currency-conversion.service';
import { CurrencyConversionActions } from '../store/actions';
import * as fromCurrencyConversion from '../store/reducers';
import { CurrencyConversionChangesDialogConfigService } from './changes-dialog-config.service';
import { CurrencyConversionChangesDialogService } from './changes-dialog.service';
import { translations } from './translations';

describe('CurrencyConversionChangesDialogService', () => {
  let service: CurrencyConversionChangesDialogService;
  let mockStore: MockStore<AppState>;
  const isFooterButtonsType = false;
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);

  const modificationsMock: CurrencyConversionViewModel[] = [
    {
      id: 1,
      airlineCode: 'Code 1',
      name: 'Airline 1',
      designator: 'Designator 1',
      active: true,
      cutpDecimalConversion: true,
      original: {
        id: 1,
        airlineCode: 'Code 1',
        name: 'Airline 1',
        designator: 'Designator 1',
        active: true,
        cutpDecimalConversion: true
      }
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        CurrencyConversionChangesDialogService,
        {
          provide: DialogService,
          useValue: dialogServiceSpy
        },
        {
          provide: CurrencyConversionChangesDialogConfigService,
          useValue: {
            build: () => ({
              data: {
                title: 'Dialog Title',
                footerButtonsType: isFooterButtonsType
                  ? [{ type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary }, { type: FooterButton.Apply }]
                  : [{ type: FooterButton.Apply }],
                buttons: [{ title: 'BUTTON.DEFAULT.APPLYCHANGES', buttonDesign: 'primary', isDisabled: true }]
              }
            })
          }
        },
        {
          provide: CurrencyConversionService,
          useValue: {
            save: () => of({})
          }
        },
        provideMockStore({
          selectors: [{ selector: fromCurrencyConversion.getChanges, value: [] }]
        })
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(CurrencyConversionChangesDialogService);
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should confirm Apply Changes when confirmApplyChanges method is triggered', () => {
    spyOn(service, 'confirm');
    service.confirmApplyChanges();

    expect(service.confirm).toHaveBeenCalledWith(translations.applyChanges);
  });

  it('should confirm Unsaved Changes when confirmUnsavedChanges method is triggered', () => {
    spyOn(service, 'confirm');
    service.confirmUnsavedChanges();

    expect(service.confirm).toHaveBeenCalledWith(translations.unsavedChanges);
  });

  it('should confirm question title and details when confirm method is triggered', fakeAsync(() => {
    let expectedResult: FooterButton;
    service.confirm({ title: 'titleConfirm', details: 'datailConfirm' }).subscribe(result => {
      expectedResult = result;
    });
    tick();

    expect(expectedResult).toEqual(FooterButton.Discard);
  }));

  it('should trigger dialog open and apply methods when the action of the footer button is equal to Apply', () => {
    const applySpy = spyOn<any>(service, 'apply').and.callThrough();

    dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Apply }));
    service['open'](modificationsMock, { title: 'Title', details: 'Details' }).subscribe();

    expect(applySpy).toHaveBeenCalled();
  });

  it('should trigger dialog open and dispatch discard methods when the action of the footer button is equal to Discard', () => {
    dialogServiceSpy.open.and.returnValue(of({ clickedBtn: FooterButton.Discard }));
    service['open'](modificationsMock, { title: 'Title', details: 'Details' }).subscribe();

    expect(mockStore.dispatch).toHaveBeenCalledWith(CurrencyConversionActions.discard());
  });
});
