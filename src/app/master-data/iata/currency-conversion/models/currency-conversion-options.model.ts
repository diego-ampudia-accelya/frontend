/* eslint-disable @typescript-eslint/naming-convention */
export const CURRENCY_CONVERSION_STATUS = {
  Active: true,
  Inactive: false
};
export const CUTP_DECIMAL_CONVERSION = {
  Enabled: true,
  Disabled: false
};
