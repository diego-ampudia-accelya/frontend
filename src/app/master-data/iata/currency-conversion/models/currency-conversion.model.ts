import { AirlineSummary } from '~app/shared/models';

export interface CurrencyConversionBE {
  id: number;
  airlineCode: string;
  name: string;
  designator: string;
  active: boolean;
  cutpDecimalConversion: boolean;
}

export interface CurrencyConversionViewModel extends CurrencyConversionBE {
  original: CurrencyConversionBE;
}

export interface CurrencyConversionOptionForTranslate {
  translationKey: string;
  value: boolean;
}

export interface CurrencyConversionForm {
  code: string;
  name: string;
  designator: string;
  status: string;
  cutpDecimalConversion: boolean;
}

export interface CurrencyConversionFilter {
  airlines: AirlineSummary[];
  designators: string[];
  status: CurrencyConversionOptionForTranslate;
  cutpDecimalConversion: CurrencyConversionOptionForTranslate;
}

export interface CurrencyConversionFilterBE {
  airlineCode: string[];
  designator: string[];
  active: boolean;
  cutpDecimalConversion: boolean;
}

export interface CurrencyConversionChange {
  id: number;
  cutpDecimalConversion: boolean;
}
