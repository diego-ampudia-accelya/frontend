import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { GridTableComponent, SelectComponent } from '~app/shared/components';
import { AlertMessageComponent } from '~app/shared/components/alert-message/alert-message.component';
import { DataQuery, ListViewComponent } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { TranslatePipeMock } from '~app/test';
import { CurrencyConversionChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { CurrencyConversionFilter, CurrencyConversionViewModel } from '../models/currency-conversion.model';
import { CurrencyConversionFilterFormatter } from '../services/currency-conversion-filter-formatter';
import { CurrencyConversionService } from '../services/currency-conversion.service';
import { CurrencyConversionActions } from '../store/actions';
import * as fromCurrencyConversion from '../store/reducers';
import { CurrencyConversionComponent } from './currency-conversion.component';

describe('CurrencyConversionComponent', () => {
  let component: CurrencyConversionComponent;
  let fixture: ComponentFixture<CurrencyConversionComponent>;
  let mockStore: MockStore<AppState>;

  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        CurrencyConversionComponent,
        MockComponents(AlertMessageComponent, ListViewComponent, SelectComponent, GridTableComponent),
        TranslatePipeMock
      ],
      providers: [
        FormBuilder,
        { provide: CurrencyConversionFilterFormatter, useValue: {} },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: CurrencyConversionService, useValue: { getAirlines: () => of([]), getDesignators: () => of([]) } },
        { provide: CurrencyConversionChangesDialogService, useValue: { confirmUnsavedChanges: () => of({}) } },
        { provide: PermissionsService, useValue: { hasPermission: () => true } },
        provideMockStore({
          selectors: [
            { selector: fromCurrencyConversion.getLoading, value: false },
            { selector: fromCurrencyConversion.getData, value: [] },
            { selector: fromCurrencyConversion.getQuery, value: defaultQuery },
            { selector: fromCurrencyConversion.hasData, value: true }
          ]
        })
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyConversionComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject<any>(Store);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch the download action when onDownload method is triggered', () => {
    spyOn(mockStore, 'dispatch');

    component.onDownload();
    expect(mockStore.dispatch).toHaveBeenCalledWith(CurrencyConversionActions.download());
  });

  it('should dispatch the modify action with the provided items', () => {
    const items = [];
    spyOn(mockStore, 'dispatch');

    component.onModify(items);
    expect(mockStore.dispatch).toHaveBeenCalledWith(CurrencyConversionActions.modify({ items }));
  });

  it('should dispatch the modify action with the provided items', () => {
    const mockQuery: DataQuery<CurrencyConversionFilter> = {
      ...defaultQuery
    };
    spyOn(mockStore, 'dispatch');

    component.onQueryChanged(defaultQuery);
    expect(mockStore.dispatch).toHaveBeenCalledWith(CurrencyConversionActions.search({ query: mockQuery }));
  });

  it('should trigger translate method and translate currency filter when formatBoolean method is triggered', () => {
    const value = false;
    const filterName = 'filter2';
    const keysObject = {
      key1: true,
      key2: false
    };

    component['formatBoolean'](value, filterName, keysObject);
    expect(translationServiceSpy.translate).toHaveBeenCalled();
  });

  it('should check if can deactivate when canDeactivate is triggered', fakeAsync(() => {
    let expectResult: boolean;
    component['canDeactivate']().subscribe(result => {
      expectResult = result;
    });

    tick();

    expect(expectResult).toBeTruthy();
  }));

  it('should return an object with the appropriate boolean properties when isChanged method is triggered', () => {
    const row: CurrencyConversionViewModel = {
      original: {
        id: 1,
        airlineCode: 'ABC',
        name: 'Currency Conversion 1',
        designator: 'CC1',
        active: true,
        cutpDecimalConversion: false
      },
      id: 1,
      airlineCode: 'ABC',
      name: 'Currency Conversion 1',
      designator: 'CC1',
      active: true,
      cutpDecimalConversion: false
    };

    const column: TableColumn = {
      prop: 'propName'
    };

    const result = component['isChanged']({ row, column });

    expect(result).toEqual({
      'checkbox-control-changed': row.original[column.prop] !== row[column.prop],
      'checkbox-control': true
    });
  });
});
