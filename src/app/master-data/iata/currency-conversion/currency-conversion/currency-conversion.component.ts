import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import isBoolean from 'lodash/isBoolean';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CurrencyConversionChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { CURRENCY_CONVERSION_STATUS, CUTP_DECIMAL_CONVERSION } from '../models/currency-conversion-options.model';
import {
  CurrencyConversionFilter,
  CurrencyConversionOptionForTranslate,
  CurrencyConversionViewModel
} from '../models/currency-conversion.model';
import { CurrencyConversionFilterFormatter } from '../services/currency-conversion-filter-formatter';
import { CurrencyConversionService } from '../services/currency-conversion.service';
import { CurrencyConversionActions } from '../store/actions';
import * as fromCurrencyConversion from '../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { AirlineSummary, DropdownOption, GridColumn } from '~app/shared/models';

@Component({
  selector: 'bspl-currency-conversion',
  templateUrl: './currency-conversion.component.html',
  styleUrls: ['./currency-conversion.component.scss']
})
export class CurrencyConversionComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];

  public airlineOptions$: Observable<DropdownOption<AirlineSummary>[]> = this.currencyConversionService.getAirlines();
  public designatorOptions$: Observable<DropdownOption<string>[]> = this.currencyConversionService.getDesignators();
  public statusOptions = this.getEnumOptions(CURRENCY_CONVERSION_STATUS, 'status');
  public cutpDecimalConversionOptions = this.getEnumOptions(CUTP_DECIMAL_CONVERSION, 'cutpDecimalConversion');

  public loading$ = this.store.pipe(select(fromCurrencyConversion.getLoading));
  public data$ = this.store.pipe(select(fromCurrencyConversion.getData));
  public query$ = this.store.pipe(select(fromCurrencyConversion.getQuery));
  public hasData$ = this.store.pipe(select(fromCurrencyConversion.hasData));

  private formFactory: FormUtil;
  private canEdit: boolean = this.permissionsService.hasPermission(Permissions.createCurrencyConversion);

  constructor(
    public displayFormatter: CurrencyConversionFilterFormatter,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private store: Store<AppState>,
    private currencyConversionService: CurrencyConversionService,
    private changesDialogService: CurrencyConversionChangesDialogService,
    private permissionsService: PermissionsService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.initColumns();
  }

  public onQueryChanged(query: DataQuery<CurrencyConversionFilter>): void {
    this.store.dispatch(CurrencyConversionActions.search({ query }));
  }

  public onDownload(): void {
    this.store.dispatch(CurrencyConversionActions.download());
  }

  public onModify(items: CurrencyConversionViewModel[]): void {
    this.store.dispatch(CurrencyConversionActions.modify({ items }));
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<CurrencyConversionFilter>({
      airlines: [],
      designators: [],
      status: [],
      cutpDecimalConversion: []
    });
  }

  private initColumns(): void {
    const editableDecimalConversionColumn: Partial<TableColumn> = {
      headerTemplate: 'checkboxHeaderTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl',
      cellClass: this.isChanged.bind(this)
    };
    const readOnlyDecimalConversionColumn: Partial<TableColumn> = {
      pipe: {
        transform: value =>
          isBoolean(value) ? this.formatBoolean(value, 'cutpDecimalConversion', CUTP_DECIMAL_CONVERSION) : value
      }
    };
    const decimalConversionColumn = this.canEdit ? editableDecimalConversionColumn : readOnlyDecimalConversionColumn;

    this.columns = [
      {
        prop: 'airlineCode',
        name: 'LIST.MASTER_DATA.currencyConversion.columns.airlineCode'
      },
      {
        prop: 'name',
        name: 'LIST.MASTER_DATA.currencyConversion.columns.name'
      },
      {
        prop: 'designator',
        name: 'LIST.MASTER_DATA.currencyConversion.columns.designator'
      },
      {
        prop: 'active',
        name: 'LIST.MASTER_DATA.currencyConversion.columns.status',
        pipe: {
          transform: value =>
            isBoolean(value) ? this.formatBoolean(value, 'status', CURRENCY_CONVERSION_STATUS) : value
        }
      },
      {
        ...decimalConversionColumn,
        prop: 'cutpDecimalConversion',
        name: 'LIST.MASTER_DATA.currencyConversion.columns.cutpDecimalConversion'
      }
    ];
  }

  private isChanged({ row, column }: { row: CurrencyConversionViewModel; column: TableColumn }): {
    [key: string]: boolean;
  } {
    const { prop } = column;

    return {
      'checkbox-control-changed': row.original[prop] !== row[prop],
      'checkbox-control': true
    };
  }

  private formatBoolean(value: boolean, filterName: string, keysObject: any): string {
    const translationKey = Object.keys(keysObject).find(key => keysObject[key] === value);

    return this.translationService.translate(
      `LIST.MASTER_DATA.currencyConversion.filters.labels.${filterName}.options.${translationKey}`
    );
  }

  private getEnumOptions(options: any, filterName: string): DropdownOption<CurrencyConversionOptionForTranslate>[] {
    return Object.keys(options).map((key: string) => ({
      value: {
        translationKey: `${filterName}.options.${key}`,
        value: options[key]
      },
      label: this.translationService.translate(
        `LIST.MASTER_DATA.currencyConversion.filters.labels.${filterName}.options.${key}`
      )
    }));
  }
}
