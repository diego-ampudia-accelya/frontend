import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DownloadFormat } from '~app/shared/models';
import { AirlineDictionaryService, AppConfigurationService } from '~app/shared/services';
import { CurrencyConversionService } from './currency-conversion.service';

describe('CurrencyConversionService', () => {
  let service: CurrencyConversionService;
  const airlineDictionaryServiceSpy = jasmine.createSpyObj('AirlineDictionaryService', ['getDropdownOptions']);
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CurrencyConversionService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: HttpClient, useValue: httpClientSpy }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CurrencyConversionService);
    httpClientSpy.get.and.returnValue(of());
    airlineDictionaryServiceSpy.getDropdownOptions.and.returnValue(of([]));
    service['bspId'] = 1;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call endpoint with specified download format', fakeAsync(() => {
    service.download(defaultQuery, DownloadFormat.TXT).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/bsp-management/bsps/1/currency-conversion/download?exportAs=txt', {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }));

  it('should get Airlines and designators when airlines service is triggered', () => {
    service.getAirlines();
    service.getDesignators();

    expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
  });

  it('should trigger post method when save method is triggered', () => {
    service.save([]);

    expect(httpClientSpy.post).toHaveBeenCalledWith('/bsp-management/bsps/1/currency-conversion', []);
  });

  it('should trigger get method when find method is triggered', fakeAsync(() => {
    service.find(defaultQuery).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalled();
  }));

  it('should set bsp Id when setBspId method is triggered', () => {
    const mockBspId = 1;
    service.setBspId(mockBspId);

    expect(service['bspId']).toEqual(mockBspId);
  });
});
