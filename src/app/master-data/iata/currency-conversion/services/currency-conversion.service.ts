import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  CurrencyConversionBE,
  CurrencyConversionChange,
  CurrencyConversionFilter,
  CurrencyConversionFilterBE
} from '../models/currency-conversion.model';
import { DataQuery } from '~app/shared/components/list-view';
import {
  downloadRequestOptions,
  formatDownloadResponse,
  toValueLabelObjectDictionaryDesignator
} from '~app/shared/helpers';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AirlineDictionaryService, AppConfigurationService } from '~app/shared/services';

@Injectable()
export class CurrencyConversionService {
  private bspId: number;

  private get baseUrl(): string {
    return `${this.appConfiguration.baseApiPath}/bsp-management/bsps/${this.bspId}/currency-conversion`;
  }

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private airlineService: AirlineDictionaryService
  ) {}

  public find(query?: DataQuery<CurrencyConversionFilter>): Observable<PagedData<CurrencyConversionBE>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<CurrencyConversionBE>>(this.baseUrl + requestQuery.getQueryString());
  }

  public save(items: CurrencyConversionChange[]): Observable<any> {
    return this.http.post(this.baseUrl, items);
  }

  public download(
    query: DataQuery<CurrencyConversionFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public setBspId(bspId: number): void {
    this.bspId = bspId;
  }

  public getAirlines(): Observable<DropdownOption[]> {
    return this.airlineService.getDropdownOptions();
  }

  public getDesignators(): Observable<DropdownOption[]> {
    return this.getAirlines().pipe(map(airlinesSummary => toValueLabelObjectDictionaryDesignator(airlinesSummary)));
  }

  private formatQuery(query: Partial<DataQuery<CurrencyConversionFilter>>): RequestQuery<CurrencyConversionFilterBE> {
    const { status, cutpDecimalConversion, airlines, designators, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        airlineCode: airlines && airlines.map(airline => airline.code),
        designator: designators,
        active: status && status.value,
        cutpDecimalConversion: cutpDecimalConversion && Boolean(cutpDecimalConversion.value)
      }
    });
  }
}
