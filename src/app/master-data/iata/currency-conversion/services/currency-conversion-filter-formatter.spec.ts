import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { CurrencyConversionFilter } from '../models/currency-conversion.model';
import { CurrencyConversionFilterFormatter } from './currency-conversion-filter-formatter';

const filters = {
  airlines: [
    {
      id: 1,
      name: 'airline1',
      code: 'airlineCode1',
      designator: 'designator1'
    }
  ],
  designators: ['designator1'],
  status: {
    translationKey: 'status1',
    value: true
  },
  cutpDecimalConversion: {
    translationKey: 'cutpDecimalConversion1',
    value: true
  }
} as CurrencyConversionFilter;

const translateKey = 'LIST.MASTER_DATA.currencyConversion.filters.labels';

describe('CurrencyConversionFilterFormatter', () => {
  let formatter: CurrencyConversionFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new CurrencyConversionFilterFormatter(translationSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeTruthy();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['airlines'],
        label: `${translateKey}.airlineCode - airlineCode1`
      },
      {
        keys: ['designators'],
        label: `${translateKey}.designator - designator1`
      },
      {
        keys: ['status'],
        label: `${translateKey}.status.label - ${translateKey}.status1`
      },
      {
        keys: ['cutpDecimalConversion'],
        label: `${translateKey}.cutpDecimalConversion.label - ${translateKey}.cutpDecimalConversion1`
      }
    ]);
  });

  it('should not format filters when there are not filters selected', () => {
    const emptyFilters = {} as CurrencyConversionFilter;
    const result = formatter.format(emptyFilters);
    expect(result).toEqual([]);
  });
});
