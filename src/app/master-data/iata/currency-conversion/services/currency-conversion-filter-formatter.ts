import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { CurrencyConversionFilter } from '../models/currency-conversion.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { joinMapper, mapJoinMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class CurrencyConversionFilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: CurrencyConversionFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<CurrencyConversionFilter>> = {
      airlines: airline => `${this.translate('airlineCode')} - ${mapJoinMapper(airline, 'code')}`,
      designators: designator => `${this.translate('designator')} - ${joinMapper(designator)}`,
      status: isActive => `${this.translate('status.label')} - ${this.translate(isActive.translationKey)}`,
      cutpDecimalConversion: isEnabled =>
        `${this.translate('cutpDecimalConversion.label')} - ${this.translate(isEnabled.translationKey)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`LIST.MASTER_DATA.currencyConversion.filters.labels.${key}`);
  }
}
