import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { EmailAlertsSettingResolver } from './email-alerts-setting.resolver';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { IataUser, UserType } from '~app/shared/models/user.model';

describe('Email Alerts Setting Resolver', () => {
  const user: IataUser = {
    email: 'third-party-user-nfe@accelya.com',
    firstName: 'Stanley',
    lastName: 'Kubrick',
    userType: UserType.IATA,
    id: 10005,

    active: true,
    permissions: ['uEmailAlrt']
  };

  const initialState = {
    auth: {
      user
    }
  };

  let emailAlertsSettingResolver: EmailAlertsSettingResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), EmailAlertsSettingResolver]
    });

    emailAlertsSettingResolver = TestBed.inject(EmailAlertsSettingResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(emailAlertsSettingResolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    emailAlertsSettingResolver.resolve().subscribe();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'users',
          scopeId: '10005',
          name: 'Kubrick',
          service: 'user-profile-management'
        }
      })
    );
  }));
});
