import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { AgentMaintenanceResolver } from './agent-maintenance.resolver';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants/permissions';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { IataUser } from '~app/shared/models/user.model';

describe('AgentMaintenanceResolver', () => {
  const iataUser: IataUser = {
    ...createIataUser(),
    permissions: [Permissions.readIataSettings, Permissions.updateIataSettings]
  };

  const initialState = {
    auth: {
      user: iataUser
    }
  };

  let resolver: AgentMaintenanceResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), AgentMaintenanceResolver]
    });

    resolver = TestBed.inject(AgentMaintenanceResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    resolver.resolve().subscribe();
    tick();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'bsps',
          scopeId: '10026',
          name: 'Anderson',
          service: 'bsp-management',
          scopeConfigName: 'configuration-agents'
        }
      })
    );
  }));
});
