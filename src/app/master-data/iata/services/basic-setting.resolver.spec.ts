import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { BasicSettingResolver } from './basic-setting.resolver';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { IataUser, UserType } from '~app/shared/models/user.model';

describe('Basic Setting Resolver', () => {
  const user: IataUser = {
    email: 'third-party-user-nfe@accelya.com',
    firstName: 'Stanley',
    lastName: 'Kubrick',
    userType: UserType.IATA,
    id: 10005,
    bsps: [
      {
        effectiveFrom: '2000-01-01',
        id: 6983,
        isoCountryCode: 'ES',
        name: 'SPAIN'
      }
    ],
    active: true,
    permissions: ['rBasicCfg']
  };

  const initialState = {
    auth: {
      user
    }
  };

  let resolver: BasicSettingResolver;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), BasicSettingResolver]
    });

    resolver = TestBed.inject(BasicSettingResolver);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should dispatch load SettingConfiguration', fakeAsync(() => {
    spyOn(mockStore, 'dispatch');

    resolver.resolve().subscribe();

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      SettingConfigurationActions.load({
        scope: {
          scopeType: 'bsps',
          scopeId: '6983',
          name: 'Kubrick',
          service: 'bsp-management',
          scopeConfigName: 'configuration-acdm'
        }
      })
    );
  }));
});
