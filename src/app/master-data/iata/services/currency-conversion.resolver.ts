import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { CurrencyConversionService } from '../currency-conversion/services/currency-conversion.service';
import { CurrencyConversionActions } from '../currency-conversion/store/actions';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { IataUser } from '~app/shared/models/user.model';

@Injectable()
export class CurrencyConversionResolver implements Resolve<any> {
  constructor(private store: Store<AppState>, private dataService: CurrencyConversionService) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getUser),
      first(),
      tap((user: IataUser) => this.dataService.setBspId(user.bsps[0].id)),
      tap(() => this.store.dispatch(CurrencyConversionActions.load())),
      mapTo(null)
    );
  }
}
