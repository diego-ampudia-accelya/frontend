import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { TaxOnCommissionTypesResolver } from './tax-on-commission-types.resolver';
import { IataUser, UserType } from '~app/shared/models/user.model';

describe('TaxOnCommissionTypesResolver', () => {
  const user: IataUser = {
    email: 'third-party-user-nfe@accelya.com',
    firstName: 'Stanley',
    lastName: 'Kubrick',
    userType: UserType.IATA,
    id: 10005,
    active: true,
    permissions: ['uEmailAlrt'],
    bsps: [
      {
        id: 1234,
        name: 'Spain',
        isoCountryCode: 'ES',
        effectiveFrom: ''
      }
    ]
  };

  const initialState = {
    auth: {
      user
    }
  };

  let resolver: TaxOnCommissionTypesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), TaxOnCommissionTypesResolver]
    });
    resolver = TestBed.inject(TaxOnCommissionTypesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should map to the first bsp', fakeAsync(() => {
    let result;
    resolver.resolve().subscribe(data => (result = data));
    tick();

    expect(result).toEqual({
      id: 1234,
      name: 'Spain',
      isoCountryCode: 'ES',
      effectiveFrom: ''
    });
  }));
});
