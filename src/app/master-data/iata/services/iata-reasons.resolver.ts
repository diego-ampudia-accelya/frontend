import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { Bsp } from '~app/shared/models/bsp.model';
import { IataUser } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class IataReasonsResolver implements Resolve<Bsp> {
  constructor(private store: Store<AppState>) {}

  public resolve(): Observable<Bsp> {
    return this.store.pipe(
      select(getUser),
      first(),
      map((loggedUser: IataUser) => loggedUser?.bsps?.[0])
    );
  }
}
