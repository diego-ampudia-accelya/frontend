import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, mapTo, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { SettingConfigurationActions } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { IataUser } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AgentMaintenanceResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<any> {
    return this.store.pipe(
      select(getUser),
      first(),
      tap((userIata: IataUser) => {
        this.store.dispatch(
          SettingConfigurationActions.load({
            scope: {
              scopeType: 'bsps',
              scopeId: userIata.bsps[0]?.id?.toString(),
              name: userIata.lastName,
              service: 'bsp-management',
              scopeConfigName: 'configuration-agents'
            }
          })
        );
      }),
      mapTo(null)
    );
  }
}
