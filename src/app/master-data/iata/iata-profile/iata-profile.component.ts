import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, defer, merge, Observable, of, Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  canApplyChanges,
  ConsentSettingsActions,
  getCanApplyChanges,
  MenuBuilder,
  RoutedMenuItem,
  SettingConfigurationActions
} from '~app/master-data/configuration';
import { ChangesDialogService as SettingsChangesDialogService } from '~app/master-data/configuration/changes-dialog/changes-dialog.service';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { Permissions } from '~app/shared/constants/permissions';
import { or } from '~app/shared/utils';
import { CurrencyConversionActions } from '../currency-conversion/store/actions';
import * as fromCurrencyConversion from '../currency-conversion/store/reducers';

@Component({
  selector: 'bspl-iata-profile',
  templateUrl: './iata-profile.component.html',
  styleUrls: ['./iata-profile.component.scss']
})
export class IataProfileComponent implements OnInit {
  public tabs: RoutedMenuItem[];
  public isApplyChangesVisible: boolean;
  public currentSavableTab = null;
  public isApplyChangesEnabled$: Observable<boolean>;

  public bspName$ = this.store.pipe(
    select(fromAuth.getUserBsps),
    map(bsps => bsps[0]?.name)
  );

  public beforeActivateTab$: Observable<boolean> = defer(() =>
    this.unsavedChangesDialogService.confirmUnsavedChanges().pipe(map(action => action !== FooterButton.Cancel))
  );
  private destroyed$ = new Subject();

  //This is an array to prepare this component for future settings addings.
  private savableTabs = [
    {
      url: './configuration/email-alerts-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateEmailAlerts
    },
    {
      url: './configuration/notifications',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateNotificationSettings
    },
    {
      url: './configuration/refund-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './configuration/general-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './configuration/remittance-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './configuration/basic-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './configuration/agent-maintenance-settings',
      canApplyChangesSelector: canApplyChanges,
      applyChangesAction: SettingConfigurationActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateIataSettings
    },
    {
      url: './currency-conversion',
      canApplyChangesSelector: fromCurrencyConversion.hasChanges,
      applyChangesAction: CurrencyConversionActions.openApplyChanges(),
      applyChangesPermission: Permissions.createCurrencyConversion
    },
    {
      url: './configuration/consents-settings',
      canApplyChangesSelector: getCanApplyChanges,
      applyChangesAction: ConsentSettingsActions.openApplyChanges(),
      applyChangesPermission: null
    }
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private menuBuilder: MenuBuilder,
    private unsavedChangesDialogService: SettingsChangesDialogService,
    private permissionsService: PermissionsService
  ) {
    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab === savableTab)
        )
      )
    ).pipe(map(values => or(...values)));
  }

  public ngOnInit(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        tap(activeSavableTab => {
          this.isApplyChangesVisible = false;
          if (activeSavableTab != null) {
            this.isApplyChangesVisible = this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission);
          }
          this.currentSavableTab = activeSavableTab;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  public applyChanges(): void {
    if (this.currentSavableTab) {
      this.store.dispatch(this.currentSavableTab.applyChangesAction);
    }
  }
}
