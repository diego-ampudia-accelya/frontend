import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';

import { SuspendedAirlineFilter } from './model/suspended-airline-filter.model';
import { SuspendedAirline } from './model/suspended-airline.model';
import { SuspendedAirlineFilterFormatter } from './services/suspended-airline-filter-formatter';
import { SuspendedAirlineService } from './services/suspended-airline.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { AirlineSummary, GridColumn } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { AirlineDictionaryService } from '~app/shared/services/dictionary';

@Component({
  selector: 'bspl-suspended-airline',
  templateUrl: './suspended-airline.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: SuspendedAirlineService }
  ]
})
export class SuspendedAirlineComponent implements OnInit {
  public title = this.translationService.translate('MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.title');
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public airlineCodeOptions$: Observable<DropdownOption<AirlineSummary>[]>;
  public raOptionList: DropdownOption<any>[] = [];

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private dateTableFormat = 'dd/MM/yyyy';

  constructor(
    public displayFormatter: SuspendedAirlineFilterFormatter,
    public dataSource: QueryableDataSource<SuspendedAirline>,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private queryStorage: DefaultQueryStorage,
    private suspendedAirlineService: SuspendedAirlineService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private airlineDictionaryService: AirlineDictionaryService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  ngOnInit() {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.initializeDataQuery();
    this.initColumns();
    this.populateFilterDropdowns();
  }

  public loadData(query: DataQuery<SuspendedAirlineFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.downloadDialog.title'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.suspendedAirlineService
    });
  }

  private populateFilterDropdowns(): void {
    this.airlineCodeOptions$ = this.airlineDictionaryService.getDropdownOptions();
    this.raOptionList = Object.values([true, false]).map(item => ({
      label: this.translationService.translate(
        `MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.filter.yesNoBooleanValue.${item}`
      ),
      value: item
    }));
  }

  private initializeDataQuery(): void {
    this.loadData(this.storedQuery);
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<SuspendedAirlineFilter>({
      airlineCode: [],
      suspensionDate: [],
      deactivationDate: [],
      raForAgents: [],
      raForAirline: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'airlineCode',
        name: 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.columns.airlineCode'
      },
      {
        prop: 'suspensionDate',
        name: 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.columns.suspensionDate',
        pipe: {
          transform: value => this.datePipe.transform(value, this.dateTableFormat)
        }
      },
      {
        prop: 'deactivationDate',
        name: 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.columns.userDeactivationDate',
        pipe: {
          transform: value => this.datePipe.transform(value, this.dateTableFormat)
        }
      },
      {
        prop: 'raForAgents',
        name: 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.columns.raAgents',
        cellTemplate: 'yesNoTmpl'
      },
      {
        prop: 'raForAirline',
        name: 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.columns.raSuspendedAirline',
        cellTemplate: 'yesNoTmpl'
      }
    ];
  }
}
