import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';
import { SuspendedAirlineFilter } from '../model/suspended-airline-filter.model';
import { SuspendedAirlineFilterFormatter } from './suspended-airline-filter-formatter';

const filters = {
  raForAgents: true,
  raForAirline: true,
  airlineCode: [
    {
      id: 1,
      name: 'Airline 1',
      code: 'Code 1',
      designator: 'Designator 1'
    },
    {
      id: 2,
      name: 'Airline 2',
      code: 'Code 2',
      designator: 'Designator 2'
    }
  ],
  suspensionDate: [new Date('26/05/2022'), new Date('26/05/2022')],
  deactivationDate: [new Date('26/05/2022'), new Date('26/05/2022')]
} as SuspendedAirlineFilter;

const translateKey = 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.filter';

describe('SuspendedAirlineFilterFormatter', () => {
  let formatter: SuspendedAirlineFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new SuspendedAirlineFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['raForAgents'],
        label: `${translateKey}.label.raAgents - ${translateKey}.yesNoBooleanValue.${filters.raForAgents}`
      },
      {
        keys: ['raForAirline'],
        label: `${translateKey}.label.raSuspendedAirline - ${translateKey}.yesNoBooleanValue.${filters.raForAirline}`
      },
      {
        keys: ['airlineCode'],
        label: `${translateKey}.label.airlineCode - ${filters.airlineCode.map(airline => airline.code).join(', ')}`
      },
      {
        keys: ['suspensionDate'],
        label: `${translateKey}.label.suspensionDate - ${rangeDateFilterTagMapper(filters.suspensionDate)}`
      },
      {
        keys: ['deactivationDate'],
        label: `${translateKey}.label.userDeactivationDate - ${rangeDateFilterTagMapper(filters.deactivationDate)}`
      }
    ]);
  });
});
