import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SuspendedAirlineFilter } from '../model/suspended-airline-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class SuspendedAirlineFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: SuspendedAirlineFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<SuspendedAirlineFilter>> = {
      airlineCode: airlineCode =>
        `${this.translate('label.airlineCode')} - ${airlineCode.map(airline => airline.code).join(', ')}`,
      suspensionDate: suspensionDate =>
        `${this.translate('label.suspensionDate')} - ${rangeDateFilterTagMapper(suspensionDate)}`,
      deactivationDate: deactivationDate =>
        `${this.translate('label.userDeactivationDate')} - ${rangeDateFilterTagMapper(deactivationDate)}`,
      raForAgents: raForAgents =>
        `${this.translate('label.raAgents')} - ${this.translate(`yesNoBooleanValue.${raForAgents}`)}`,
      raForAirline: raForAirline =>
        `${this.translate('label.raSuspendedAirline')} - ${this.translate(`yesNoBooleanValue.${raForAirline}`)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.filter.${key}`);
  }
}
