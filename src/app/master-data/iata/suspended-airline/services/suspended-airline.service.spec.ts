import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { AirlineSummary, DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { SuspendedAirlineFilter } from '../model/suspended-airline-filter.model';
import { SuspendedAirline } from '../model/suspended-airline.model';
import { SuspendedAirlineService } from './suspended-airline.service';

describe('SuspendedAirlineService', () => {
  let service: SuspendedAirlineService;
  let httpClientSpy: SpyObject<HttpClient>;

  const suspendedAirlinesMock: SuspendedAirline[] = [
    {
      id: 6983838349,
      airlineCode: 'POP1',
      suspensionDate: '2022-10-30',
      deactivationDate: '2018-03-20',
      raForAgents: false,
      raForAirline: false
    },
    {
      id: 6983838349,
      airlineCode: 'POP2',
      suspensionDate: '2022-10-30',
      deactivationDate: '2018-03-20',
      raForAgents: false,
      raForAirline: false
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        SuspendedAirlineService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(SuspendedAirlineService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/user-management/users/suspended-airlines');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let suspendedData: PagedData<SuspendedAirline>;

    const suspendedDataMock: PagedData<SuspendedAirline> = {
      records: suspendedAirlinesMock,
      total: suspendedAirlinesMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(suspendedDataMock));
    service.find(query).subscribe(res => {
      suspendedData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/user-management/users/suspended-airlines?page=0&size=20');
    expect(suspendedData).toEqual(suspendedDataMock);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/user-management/users/suspended-airlines/download?exportAs=txt';

    service.download(query, exportOptions).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));

  it('should format the data query passed to formatQuery method and return filters formatted', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const airlineCodes: AirlineSummary[] = [
      {
        id: 1,
        name: 'Airline 1',
        code: 'Code 1',
        designator: 'Designator 1'
      },
      {
        id: 2,
        name: 'Airline 2',
        code: 'Code 2',
        designator: 'Designator 2'
      }
    ];
    const airlineCodesFormatted = ['Code 1', 'Code 2'];
    const MockSuspensionDate: Date[] = [new Date('26/05/2022'), new Date('26/05/2022')];
    const MockDeactivationDate: Date[] = [new Date('26/05/2022'), new Date('26/05/2022')];
    const dataQuery: Partial<DataQuery<SuspendedAirlineFilter>> = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy,
        airlineCode: airlineCodes,
        suspensionDate: MockSuspensionDate,
        deactivationDate: MockDeactivationDate,
        raForAgents: true,
        raForAirline: true
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['airlineCode']).toEqual(airlineCodesFormatted);
    expect(formattedQuery.filterBy['toSuspensionDate']).toEqual(toShortIsoDate(MockSuspensionDate[1]));
    expect(formattedQuery.filterBy['fromSuspensionDate']).toEqual(toShortIsoDate(MockSuspensionDate[0]));
    expect(formattedQuery.filterBy['fromDeactivationDate']).toEqual(toShortIsoDate(MockDeactivationDate[0]));
  });

  it('should format suspensionDate and deactivationDate filters when just one date is selected and formatQuery is triggered', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const MockSuspensionDate: Date[] = [new Date('26/05/2022')];
    const MockDeactivationDate: Date[] = [new Date('26/05/2022')];
    const dataQuery: Partial<DataQuery<SuspendedAirlineFilter>> = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy,
        suspensionDate: MockSuspensionDate,
        deactivationDate: MockDeactivationDate
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['fromSuspensionDate']).toEqual(toShortIsoDate(MockSuspensionDate[0]));
    expect(formattedQuery.filterBy['toDeactivationDate']).toEqual(toShortIsoDate(MockDeactivationDate[0]));
  });
});
