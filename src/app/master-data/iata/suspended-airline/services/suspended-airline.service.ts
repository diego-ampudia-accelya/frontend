import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SuspendedAirlineBE, SuspendedAirlineFilter } from '../model/suspended-airline-filter.model';
import { SuspendedAirline } from '../model/suspended-airline.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class SuspendedAirlineService implements Queryable<SuspendedAirline> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/users/suspended-airlines`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<SuspendedAirlineFilter>): Observable<PagedData<SuspendedAirline>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<SuspendedAirline>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery<SuspendedAirlineFilter>>): RequestQuery<SuspendedAirlineBE> {
    const { airlineCode, suspensionDate, deactivationDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        airlineCode: airlineCode && airlineCode.map(airline => airline.code),
        fromSuspensionDate: suspensionDate && toShortIsoDate(suspensionDate[0]),
        toSuspensionDate: suspensionDate && toShortIsoDate(suspensionDate[1] ? suspensionDate[1] : suspensionDate[0]),
        fromDeactivationDate: deactivationDate && toShortIsoDate(deactivationDate[0]),
        toDeactivationDate:
          deactivationDate && toShortIsoDate(deactivationDate[1] ? deactivationDate[1] : deactivationDate[0])
      }
    });
  }

  public download(
    query: DataQuery<SuspendedAirlineFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
