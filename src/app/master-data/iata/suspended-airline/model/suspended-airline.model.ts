export interface SuspendedAirline {
  id: number;
  airlineCode: string;
  suspensionDate: string;
  deactivationDate: string;
  raForAgents: boolean;
  raForAirline: boolean;
}
