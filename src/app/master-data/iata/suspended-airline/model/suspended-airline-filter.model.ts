import { AirlineSummary } from '~app/shared/models';

export interface SuspendedAirlineFilter {
  airlineCode: AirlineSummary[];
  suspensionDate: Date[];
  deactivationDate: Date[];
  raForAgents: boolean;
  raForAirline: boolean;
}

export interface SuspendedAirlineBE {
  airlineCode: string[];
  fromSuspensionDate: string;
  toSuspensionDate: string;
  fromDeactivationDate: string;
  toDeactivationDate: string;
  raForAgents: boolean;
  raForAirline: boolean;
}
