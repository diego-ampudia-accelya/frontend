import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { SuspendedAirlineFilterFormatter } from './services/suspended-airline-filter-formatter';
import { SuspendedAirlineService } from './services/suspended-airline.service';
import { SuspendedAirlineComponent } from './suspended-airline.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

describe('SuspendedAirlineComponent', () => {
  let component: SuspendedAirlineComponent;
  let fixture: ComponentFixture<SuspendedAirlineComponent>;
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const suspendedAirlineServiceSpy: SpyObject<SuspendedAirlineService> = createSpyObject(SuspendedAirlineService);
  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { suspendedAirlineQuery: { ...ROUTES.SUSPENDED_AIRLINE, id: 'suspendedAirlineQuery' } },
        activeTabId: 'suspendedAirlineQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SuspendedAirlineComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        SuspendedAirlineFilterFormatter,
        { provide: DialogService, useValue: dialogServiceSpy },
        FormBuilder
      ]
    })
      .overrideComponent(SuspendedAirlineComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: SuspendedAirlineService, useValue: suspendedAirlineServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuspendedAirlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');
    component.ngOnInit();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
