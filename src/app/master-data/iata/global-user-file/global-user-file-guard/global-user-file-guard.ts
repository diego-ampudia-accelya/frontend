import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

import { GlobalUserFileDialogComponent } from '../global-user-file-dialog/global-user-file-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class GlobalUserFileGuard implements CanActivate {
  constructor(private dialogService: DialogService) {}

  public canActivate(): Observable<boolean> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'MENU.USERS_MAINTENANCE.GLOBAL_USER_FILE.dialog.title',
        footerButtonsType: [{ type: FooterButton.Request }]
      }
    };

    return this.dialogService.open(GlobalUserFileDialogComponent, dialogConfig);
  }
}
