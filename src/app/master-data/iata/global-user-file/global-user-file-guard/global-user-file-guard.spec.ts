import { TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';

import { GlobalUserFileGuard } from './global-user-file-guard';
import { DialogService } from '~app/shared/components';

describe('GlobalUserFileGuard', () => {
  let globalUserFileGuard: GlobalUserFileGuard;
  let dialogService: SpyObject<DialogService>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [GlobalUserFileGuard, mockProvider(DialogService)]
    });

    globalUserFileGuard = TestBed.inject(GlobalUserFileGuard);
    dialogService = TestBed.inject<any>(DialogService);
  }));

  it('should create', () => {
    expect(globalUserFileGuard).toBeTruthy();
  });

  it('should open global user file dialog component when canActivate is called', () => {
    globalUserFileGuard.canActivate();

    expect(dialogService.open).toHaveBeenCalled();
  });
});
