import { Component, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';
import { filter, first, switchMap } from 'rxjs/operators';

import { GlobalUserFileService } from '../services/global-user-file.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AlertMessageType } from '~app/shared/enums';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-global-user-file-dialog',
  templateUrl: './global-user-file-dialog.component.html'
})
export class GlobalUserFileDialogComponent implements OnInit {
  public requestText = this.translationService.translate('MENU.USERS_MAINTENANCE.GLOBAL_USER_FILE.dialog.requestText');
  public disclaimerMsg = {
    message: this.translationService.translate('MENU.USERS_MAINTENANCE.GLOBAL_USER_FILE.dialog.disclaimer'),
    type: AlertMessageType.info
  };

  private successMessageKey = 'MENU.USERS_MAINTENANCE.GLOBAL_USER_FILE.dialog.successMessage';
  private requestButtonClick$ = this.reactiveSubject.asObservable.pipe(
    first(),
    filter(action => action?.clickedBtn === FooterButton.Request)
  );

  constructor(
    public config: DialogConfig,
    private dialogService: DialogService,
    private globalUserFileService: GlobalUserFileService,
    private notificationService: NotificationService,
    private reactiveSubject: ReactiveSubject,
    private translationService: L10nTranslationService
  ) {}

  public ngOnInit(): void {
    this.initializeListeners();
  }

  private initializeListeners(): void {
    this.requestButtonClick$.pipe(switchMap(() => this.requestGlobalUserFile())).subscribe(() => {
      this.notificationService.showSuccess(this.translationService.translate(this.successMessageKey));
    });
  }

  private requestGlobalUserFile(): Observable<any> {
    this.dialogService.close();

    return this.globalUserFileService.requestFile();
  }
}
