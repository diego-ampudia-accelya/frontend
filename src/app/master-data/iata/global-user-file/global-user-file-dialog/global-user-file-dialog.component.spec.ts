import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { GlobalUserFileService } from '../services/global-user-file.service';
import { GlobalUserFileDialogComponent } from './global-user-file-dialog.component';
import { DialogConfig } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

describe('GlobalUserFileDialogComponent', () => {
  let component: GlobalUserFileDialogComponent;
  let fixture: ComponentFixture<GlobalUserFileDialogComponent>;

  const notificationServiceSpy = createSpyObject(NotificationService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const globalUserFileServiceSpy = createSpyObject(GlobalUserFileService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GlobalUserFileDialogComponent],
      imports: [HttpClientTestingModule],
      providers: [
        ReactiveSubject,
        DialogConfig,
        {
          provide: GlobalUserFileService,
          useValue: globalUserFileServiceSpy
        },
        {
          provide: NotificationService,
          useValue: notificationServiceSpy
        },
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        },
        {
          provide: AppConfigurationService,
          useValue: { baseApiPath: '' }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalUserFileDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog when request a file service is called', fakeAsync(() => {
    const dialogSpy = spyOn(component['dialogService'], 'close');
    component['requestGlobalUserFile']();
    tick();

    expect(dialogSpy).toHaveBeenCalled();
  }));

  it('should request a file service when requestGlobalUserFile method is called', () => {
    component['requestGlobalUserFile']();

    expect(globalUserFileServiceSpy.requestFile).toHaveBeenCalled();
  });

  it('should initialize listeners when initializeListeners is called', () => {
    const requestButtonSpy = spyOn(component['requestButtonClick$'], 'subscribe');
    component['initializeListeners']();

    expect(requestButtonSpy).toHaveBeenCalled();
  });
});
