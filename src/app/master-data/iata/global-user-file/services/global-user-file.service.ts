import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class GlobalUserFileService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/global-users-file`;

  constructor(private appConfiguration: AppConfigurationService, private http: HttpClient) {}

  public requestFile(): Observable<any> {
    return this.http.post(this.baseUrl, {});
  }
}
