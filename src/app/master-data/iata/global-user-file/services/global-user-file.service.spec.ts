import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';

import { GlobalUserFileService } from './global-user-file.service';
import { AppConfigurationService } from '~app/shared/services';

const baseUrl = '/file-management/global-users-file';

describe('GlobalUserFileService', () => {
  let service: GlobalUserFileService;
  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        GlobalUserFileService,
        {
          provide: AppConfigurationService,
          useValue: { baseApiPath: '' }
        },
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(GlobalUserFileService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should call Http post method with baseUrl when requestFile method is called', fakeAsync(() => {
    service.requestFile();
    tick();

    expect(httpClientSpy.post).toHaveBeenCalledWith(baseUrl, {});
  }));
});
