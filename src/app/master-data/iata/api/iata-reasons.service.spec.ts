import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { IataReasonsService } from './iata-reasons.service';
import { EditableDescription, ReasonType } from '~app/master-data/configuration';
import { AppConfigurationService } from '~app/shared/services';

describe('IataReasonsService', () => {
  let service: IataReasonsService;
  const baseApiPath = 'test';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: AppConfigurationService, useValue: { baseApiPath } }],
      imports: [HttpClientTestingModule]
    });
    service = new IataReasonsService(
      TestBed.inject(HttpClient),
      ReasonType.Issue,
      TestBed.inject(AppConfigurationService)
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get - should make get request and return mapped description', fakeAsync(() => {
    (service as any).http.get = jasmine.createSpy().and.returnValue(
      of([
        {
          id: 1,
          bspId: 6983,
          sequenceNumber: 10,
          bspAcdmConfigId: 1,
          title: 'Title',
          description: 'Description'
        }
      ])
    );
    let res;
    const expectedRes = [
      {
        id: 1,
        sequenceNumber: 10,
        bspAcdmConfigId: 1,
        description: 'Description',
        label: 'Title',
        parentId: 6983
      }
    ];
    const url = 'test/acdm-management/bsps/6983/acdm-reason-configuration';
    const params = { reasonType: ReasonType.Issue };

    service.get(6983).subscribe(data => (res = data));
    tick();

    expect((service as any).http.get).toHaveBeenCalledWith(url, { params });
    expect(res).toEqual(expectedRes);
  }));

  it('delete - should make delete request with correct url', fakeAsync(() => {
    (service as any).http.delete = jasmine.createSpy().and.returnValue(of({}));
    const url = 'test/acdm-management/bsps/6983/acdm-reason-configuration/1';
    const param = {
      parentId: 6983,
      id: 1
    } as any;

    service.delete(param).subscribe();
    tick();

    expect((service as any).http.delete).toHaveBeenCalledWith(url);
  }));

  it('save(PUT) - should make put request and return mapped data', fakeAsync(() => {
    const param: EditableDescription = {
      id: 1,
      parentId: 6983,
      sequenceNumber: 10,
      bspAcdmConfigId: 1,
      label: 'Title',
      description: 'Description'
    } as any;
    const mappedParam = {
      sequenceNumber: 10,
      title: 'Title',
      description: 'Description'
    };
    (service as any).http.request = jasmine.createSpy().and.returnValue(
      of({
        id: 1,
        bspId: 6983,
        sequenceNumber: 10,
        bspAcdmConfigId: 1,
        title: 'Title',
        description: 'Description'
      })
    );
    let res;
    const expectedRes = {
      id: 1,
      sequenceNumber: 10,
      bspAcdmConfigId: 1,
      description: 'Description',
      label: 'Title',
      parentId: 6983
    };
    const url = 'test/acdm-management/bsps/6983/acdm-reason-configuration/1';

    service.save(param).subscribe(data => (res = data));
    tick();

    expect((service as any).http.request).toHaveBeenCalledWith('PUT', url, {
      body: mappedParam
    });
    expect(res).toEqual(expectedRes);
  }));

  it('save(POST) - should make put request and return mapped data', fakeAsync(() => {
    const param: EditableDescription = {
      parentId: 6983,
      sequenceNumber: 10,
      bspAcdmConfigId: 1,
      label: 'Title',
      description: 'Description'
    } as any;
    const mappedParam = {
      sequenceNumber: 10,
      title: 'Title',
      description: 'Description',
      type: ReasonType.Issue
    };
    (service as any).http.request = jasmine.createSpy().and.returnValue(
      of({
        id: 1,
        bspId: 6983,
        sequenceNumber: 10,
        bspAcdmConfigId: 1,
        title: 'Title',
        description: 'Description'
      })
    );
    let res;
    const expectedRes = {
      id: 1,
      sequenceNumber: 10,
      bspAcdmConfigId: 1,
      description: 'Description',
      label: 'Title',
      parentId: 6983
    };
    const url = 'test/acdm-management/bsps/6983/acdm-reason-configuration';

    service.save(param).subscribe(data => (res = data));
    tick();

    expect((service as any).http.request).toHaveBeenCalledWith('POST', url, {
      body: mappedParam
    });
    expect(res).toEqual(expectedRes);
  }));
});
