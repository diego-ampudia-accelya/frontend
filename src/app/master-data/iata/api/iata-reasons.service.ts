import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IataReason, IataReasonRequest } from '../configuration/models/iata-reason.model';
import { DescriptionService, EditableDescription, ReasonType } from '~app/master-data/configuration';
import { AppConfigurationService } from '~app/shared/services';
import { determineMethodToSave, isNew } from '~app/shared/utils';
import { buildPath } from '~app/shared/utils/path-builder';

export class IataReasonsService implements DescriptionService {
  private baseurl = `${this.appConfiguration.baseApiPath}/acdm-management/bsps`;

  constructor(private http: HttpClient, public type: ReasonType, private appConfiguration: AppConfigurationService) {}

  public get(parentId: number): Observable<EditableDescription[]> {
    return this.http
      .get<IataReason[]>(this.getUrl(parentId), {
        params: { reasonType: this.type }
      })
      .pipe(map((reasons: IataReason[]) => reasons.map(this.mapToEditableDescription.bind(this))));
  }

  public delete(model: EditableDescription): Observable<void> {
    return this.http.delete<void>(this.getUrl(model.parentId, model.id));
  }

  public save(model: EditableDescription): Observable<EditableDescription> {
    const url = this.getUrl(model.parentId, model.id);
    const method = determineMethodToSave(model);
    const reason: IataReasonRequest = this.mapToReason(model);

    return this.http
      .request<IataReason>(method, url, {
        body: reason
      })
      .pipe(map(this.mapToEditableDescription).bind(this));
  }

  private getUrl(bspId: number, modelId?: number): string {
    return buildPath(`${this.baseurl}/${bspId}`, 'acdm-reason-configuration', modelId);
  }

  private mapToReason(model: EditableDescription): IataReasonRequest {
    const { label, sequenceNumber, description } = model;
    const typeObject = isNew(model) ? { type: this.type } : null; // PUT doesn't need 'type'

    return {
      title: label,
      sequenceNumber,
      description,
      ...typeObject
    };
  }

  private mapToEditableDescription(reason: IataReason): EditableDescription {
    const { bspId, title, ...otherProps } = reason;

    return {
      ...otherProps,
      label: title,
      parentId: bspId
    };
  }
}
