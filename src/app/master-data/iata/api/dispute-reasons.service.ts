import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { IataReasonsService } from './iata-reasons.service';
import { ReasonType } from '~app/master-data/configuration';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class DisputeReasonsService extends IataReasonsService {
  constructor(http: HttpClient, appConfiguration: AppConfigurationService) {
    super(http, ReasonType.Dispute, appConfiguration);
  }
}
