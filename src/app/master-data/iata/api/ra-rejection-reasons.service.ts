import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { IataRaReasonsService } from './iata-ra-reasons.service';
import { ReasonType } from '~app/master-data/configuration';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class RaRejectionReasonsService extends IataRaReasonsService {
  constructor(http: HttpClient, appConfiguration: AppConfigurationService) {
    super(http, ReasonType.Rejection, appConfiguration);
  }
}
