import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { IataRaReasonsService } from './iata-ra-reasons.service';
import { EditableDescription, ReasonType } from '~app/master-data/configuration';
import { AppConfigurationService } from '~app/shared/services';

describe('IataRaReasonsService', () => {
  let service: IataRaReasonsService;
  const baseApiPath = 'test';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: AppConfigurationService, useValue: { baseApiPath } }],
      imports: [HttpClientTestingModule]
    });
    service = new IataRaReasonsService(
      TestBed.inject(HttpClient),
      ReasonType.Issue,
      TestBed.inject(AppConfigurationService)
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get - should make get request and return mapped description', fakeAsync(() => {
    (service as any).http.get = jasmine.createSpy().and.returnValue(
      of([
        {
          id: 1,
          bspId: 6983,
          sequenceNumber: 10,
          title: 'Title',
          description: 'Description'
        }
      ])
    );
    let res;
    const expectedRes = [
      {
        id: 1,
        sequenceNumber: 10,
        description: 'Description',
        label: 'Title',
        parentId: 6983
      }
    ];
    const url = 'test/refund-management/bsps/6983/refund-application-issue-reason-configuration';

    service.get(6983).subscribe(data => (res = data));
    tick();

    expect((service as any).http.get).toHaveBeenCalledWith(url);
    expect(res).toEqual(expectedRes);
  }));

  it('delete - should make delete request with correct url', fakeAsync(() => {
    (service as any).http.delete = jasmine.createSpy().and.returnValue(of({}));
    const url = 'test/refund-management/bsps/6983/refund-application-issue-reason-configuration/1';
    const param = {
      parentId: 6983,
      id: 1
    } as any;

    service.delete(param).subscribe();
    tick();

    expect((service as any).http.delete).toHaveBeenCalledWith(url);
  }));

  it('save(PUT) - should make put request and return mapped data', fakeAsync(() => {
    const param: EditableDescription = {
      id: 1,
      parentId: 6983,
      sequenceNumber: 10,
      label: 'Title',
      description: 'Description'
    } as any;
    const mappedParam = {
      sequenceNumber: 10,
      title: 'Title',
      description: 'Description'
    };
    (service as any).http.request = jasmine.createSpy().and.returnValue(
      of({
        id: 1,
        bspId: 6983,
        sequenceNumber: 10,
        title: 'Title',
        description: 'Description'
      })
    );
    let res;
    const expectedRes = {
      id: 1,
      sequenceNumber: 10,
      description: 'Description',
      label: 'Title',
      parentId: 6983
    };
    const url = 'test/refund-management/bsps/6983/refund-application-issue-reason-configuration/1';

    service.save(param).subscribe(data => (res = data));
    tick();

    expect((service as any).http.request).toHaveBeenCalledWith('PUT', url, {
      body: mappedParam
    });
    expect(res).toEqual(expectedRes);
  }));

  it('save(POST) - should make put request and return mapped data', fakeAsync(() => {
    const param: EditableDescription = {
      parentId: 6983,
      sequenceNumber: 10,
      label: 'Title',
      description: 'Description'
    } as any;
    const mappedParam = {
      sequenceNumber: 10,
      title: 'Title',
      description: 'Description'
    };
    (service as any).http.request = jasmine.createSpy().and.returnValue(
      of({
        id: 1,
        bspId: 6983,
        sequenceNumber: 10,
        title: 'Title',
        description: 'Description'
      })
    );
    let res;
    const expectedRes = {
      id: 1,
      sequenceNumber: 10,
      description: 'Description',
      label: 'Title',
      parentId: 6983
    };
    const url = 'test/refund-management/bsps/6983/refund-application-issue-reason-configuration';

    service.save(param).subscribe(data => (res = data));
    tick();

    expect((service as any).http.request).toHaveBeenCalledWith('POST', url, {
      body: mappedParam
    });
    expect(res).toEqual(expectedRes);
  }));

  it('should call delete with rejection url', fakeAsync(() => {
    const rejectionService = new IataRaReasonsService(
      TestBed.inject(HttpClient),
      ReasonType.Rejection,
      TestBed.inject(AppConfigurationService)
    );

    (rejectionService as any).http.delete = jasmine.createSpy().and.returnValue(of({}));
    const url = 'test/refund-management/bsps/6983/refund-application-rejection-reason-configuration/1';
    const param = {
      parentId: 6983,
      id: 1
    } as any;

    rejectionService.delete(param).subscribe();
    tick();

    expect((rejectionService as any).http.delete).toHaveBeenCalledWith(url);
  }));
});
