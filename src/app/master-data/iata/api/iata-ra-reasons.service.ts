import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IataRaReasonRequest } from '../configuration/models/iata-ra-reason.model';
import { DescriptionService, EditableDescription, ReasonType } from '~app/master-data/configuration';
import { RefundReason } from '~app/refund/models/refund-reason.model';
import { AppConfigurationService } from '~app/shared/services';
import { determineMethodToSave } from '~app/shared/utils';
import { buildPath } from '~app/shared/utils/path-builder';

const PATH_BY_TYPE = {
  [ReasonType.Issue]: 'issue',
  [ReasonType.Rejection]: 'rejection'
};

export class IataRaReasonsService implements DescriptionService {
  private baseurl = `${this.appConfiguration.baseApiPath}/refund-management/bsps`;

  constructor(private http: HttpClient, private type: ReasonType, private appConfiguration: AppConfigurationService) {}

  public get(parentId: number): Observable<EditableDescription[]> {
    return this.http
      .get<RefundReason[]>(this.getUrl(parentId))
      .pipe(map((reasons: RefundReason[]) => reasons.map(this.mapToEditableDescription.bind(this))));
  }

  public delete(model: EditableDescription): Observable<void> {
    return this.http.delete<void>(this.getUrl(model.parentId, model.id));
  }

  public save(model: EditableDescription): Observable<EditableDescription> {
    const url = this.getUrl(model.parentId, model.id);
    const method = determineMethodToSave(model);
    const reason: IataRaReasonRequest = this.mapToReason(model);

    return this.http
      .request<RefundReason>(method, url, {
        body: reason
      })
      .pipe(map(this.mapToEditableDescription).bind(this));
  }

  private getUrl(bspId: number, modelId?: number): string {
    return buildPath(
      `${this.baseurl}/${bspId}`,
      `refund-application-${PATH_BY_TYPE[this.type]}-reason-configuration`,
      modelId
    );
  }

  private mapToReason(model: EditableDescription): IataRaReasonRequest {
    const { label, sequenceNumber, description } = model;

    return {
      title: label,
      sequenceNumber,
      description
    };
  }

  private mapToEditableDescription(reason: RefundReason): EditableDescription {
    const { title, bspId, ...otherProps } = reason;

    return {
      label: title,
      ...otherProps,
      parentId: bspId
    };
  }
}
