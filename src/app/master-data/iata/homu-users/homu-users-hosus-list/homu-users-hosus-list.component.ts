import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';

import { HomuUserHosuViewModel } from '../models/homu-users.model';
import { HomuUsersHosusFilterFormatter } from '../services/homu-users-hosus-filter-formatter';
import { HomuUsersHosusActions } from '../store/actions';
import * as fromHomuUsers from '../store/reducers';
import { ActiveInactiveInUpperCase, DropdownOption, GridColumn } from '~app/shared/models';
import { FormUtil } from '~app/shared/helpers';
import { AppState } from '~app/reducers';

@Component({
  selector: 'bspl-homu-users-hosus-list',
  templateUrl: './homu-users-hosus-list.component.html',
  styleUrls: ['./homu-users-hosus-list.component.scss']
})
export class HomuUsersHosusListComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public totalItemsMessage: string;

  public loading$ = this.store.pipe(select(fromHomuUsers.getHosusLoading));
  public data$ = this.store.pipe(select(fromHomuUsers.getHosusData));
  public query$ = this.store.pipe(select(fromHomuUsers.getHosusQuery));
  public hasData$ = this.store.pipe(select(fromHomuUsers.hasHosusData));

  public statusOptions: DropdownOption<typeof ActiveInactiveInUpperCase>[] = this.getDropdownOptions(
    ActiveInactiveInUpperCase,
    'common'
  );

  public levelOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatLevel(value)
  }));

  private formFactory: FormUtil;

  constructor(
    public displayFormatter: HomuUsersHosusFilterFormatter,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.initializeDataQuery();
    this.initializeTotalItems();
    this.initColumns();
  }

  public onQueryChanged(query): void {
    this.store.dispatch(HomuUsersHosusActions.search({ query }));
  }

  public onDownload(): void {
    this.store.dispatch(HomuUsersHosusActions.download());
  }

  private initializeTotalItems() {
    this.store.pipe(select(fromHomuUsers.getHosusTotalElements)).subscribe(total => {
      this.totalItemsMessage = this.translationService.translate(
        'MENU.USERS_MAINTENANCE.HOMU_USERS.listOfHosus.totalItemsMessage',
        { total }
      );
    });
  }

  private initializeDataQuery(): void {
    this.store.dispatch(HomuUsersHosusActions.load());
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<HomuUserHosuViewModel>({
      userCode: [],
      portalEmail: [],
      mainUser: [],
      name: [],
      registerDate: [],
      expiryDate: [],
      status: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.userCode',
        prop: 'userCode'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.portalEmail',
        prop: 'portalEmail'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.mainUser',
        prop: 'mainUser',
        pipe: { transform: value => this.displayFormatter.formatLevel(value) }
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.name',
        prop: 'name'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.registerDate',
        prop: 'registerDate',
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.expiryDate',
        prop: 'expiryDate',
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.numberOfUsers',
        prop: 'numberOfUsers'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.status',
        prop: 'status',
        pipe: {
          transform: value => (value ? this.translationService.translate(`common.${value}`) : value)
        }
      }
    ];
  }

  private getDropdownOptions<T>(object: T, translationKey: string): DropdownOption<T>[] {
    return Object.keys(object).map(key => ({
      value: object[key],
      label: this.translationService.translate(`${translationKey}.${key}`)
    }));
  }
}
