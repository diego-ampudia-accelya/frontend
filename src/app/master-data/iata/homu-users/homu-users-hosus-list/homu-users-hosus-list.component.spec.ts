import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import {
  GridTableComponent,
  InputComponent,
  PropertyComponent,
  PropertyListComponent,
  SelectComponent
} from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { ListViewComponent } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { TranslatePipeMock } from '~app/test';
import { HomuUsersHosusFilterFormatter } from '../services/homu-users-hosus-filter-formatter';
import { HomuUsersHosusActions } from '../store/actions';
import * as fromHomuUsers from '../store/reducers';
import { HomuUsersHosusListComponent } from './homu-users-hosus-list.component';

describe('HomuUsersHosusListComponent', () => {
  let component: HomuUsersHosusListComponent;
  let fixture: ComponentFixture<HomuUsersHosusListComponent>;
  let mockStore = createSpyObject(Store);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        HomuUsersHosusListComponent,
        MockComponents(
          ListViewComponent,
          SelectComponent,
          InputComponent,
          DatepickerComponent,
          GridTableComponent,
          PropertyListComponent,
          PropertyComponent
        ),
        TranslatePipeMock
      ],
      providers: [
        FormBuilder,
        HomuUsersHosusFilterFormatter,
        { provide: L10nTranslationService, useValue: { translate: () => '(2 elements)' } },
        provideMockStore({
          selectors: [
            { selector: fromHomuUsers.getHosusLoading, value: false },
            { selector: fromHomuUsers.getHosusData, value: [] },
            { selector: fromHomuUsers.hasHosusData, value: true },
            { selector: fromHomuUsers.getHosusQuery, value: defaultQuery },
            { selector: fromHomuUsers.getHosusTotalElements, value: 2 },
            {
              selector: fromHomuUsers.getHomuUsersState,
              value: {
                query: defaultQuery,
                previousQuery: defaultQuery,
                data: [],
                loading: false
              }
            }
          ]
        })
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    mockStore = TestBed.inject<any>(Store);
    fixture = TestBed.createComponent(HomuUsersHosusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch download action when onDownload method is triggered', () => {
    spyOn(mockStore, 'dispatch');
    component.onDownload();

    expect(mockStore.dispatch).toHaveBeenCalledWith({
      type: HomuUsersHosusActions.download.type
    });
  });

  it('should dispatch search action when onQueryChanged method is triggered', () => {
    spyOn(mockStore, 'dispatch');
    component.onQueryChanged(defaultQuery);

    expect(mockStore.dispatch).toHaveBeenCalledWith({
      query: defaultQuery,
      type: HomuUsersHosusActions.search.type
    });
  });

  it('should initialize the total of items when initializeTotalItems method is triggered', () => {
    component['initializeTotalItems']();
    expect(component.totalItemsMessage).toEqual('(2 elements)');
  });
});
