import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';

import * as fromHomuUsers from '../store/reducers';
import { HomuUserProfileComponent } from './homu-user-profile.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuBuilder } from '~app/master-data/configuration';
import { TabLinkComponent } from '~app/shared/components/tabs/tab-navbar/tab-link/tab-link.component';
import { TabNavbarComponent } from '~app/shared/components/tabs/tab-navbar/tab-navbar.component';

describe('HomuUserProfileComponent', () => {
  let component: HomuUserProfileComponent;
  let fixture: ComponentFixture<HomuUserProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomuUserProfileComponent, MockComponents(TabNavbarComponent, TabLinkComponent)],
      imports: [RouterTestingModule],
      providers: [
        PermissionsService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {}
          }
        },
        {
          provide: MenuBuilder,
          useValue: {
            buildMenuItemsFrom: () => []
          }
        },
        provideMockStore({
          initialState: {},
          selectors: [{ selector: fromHomuUsers.getProfileHomuUserPortalEmail, value: '' }]
        })
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomuUserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
