import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { HomuUserSavableTabs } from '../models/homu-users.model';
import { HomuUsersPermissionsActions } from '../store/actions';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { or } from '~app/shared/utils';

@Component({
  selector: 'bspl-homu-user-profile',
  templateUrl: './homu-user-profile.component.html',
  styleUrls: ['./homu-user-profile.component.scss']
})
export class HomuUserProfileComponent implements OnInit, OnDestroy {
  public userPortalEmail$: Observable<string>;

  public tabs: RoutedMenuItem[];

  public isApplyChangesEnabled$: Observable<boolean>;

  public isApplyChangesVisible = false;

  public currentSavableTab = null;

  private destroyed$ = new Subject();

  private savableTabs: HomuUserSavableTabs[] = [
    {
      url: './permissions',
      canApplyChangesSelector: fromHomuUsers.canApplyPermissionsChanges,
      applyChangesAction: HomuUsersPermissionsActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateHomuPermissions
    },
    {
      url: './properties'
    },
    {
      url: './hosus-list'
    }
  ];

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    private permissionsService: PermissionsService
  ) {}

  public applyChanges(): void {
    this.store.dispatch(this.currentSavableTab.applyChangesAction);
  }

  public ngOnInit(): void {
    this.initializePortalEmail();

    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        savableTab.canApplyChangesSelector
          ? this.store.pipe(
              select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
              map(canSave => canSave && this.currentSavableTab === savableTab)
            )
          : of(false)
      )
    ).pipe(map(values => or(...values)));

    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        switchMap(activeSavableTab => {
          this.currentSavableTab = activeSavableTab;

          return activeSavableTab && activeSavableTab.canApplyChangesSelector
            ? of(this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission))
            : of(false);
        }),
        tap(applyChangesVisible => (this.isApplyChangesVisible = applyChangesVisible)),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  private initializePortalEmail() {
    this.userPortalEmail$ = this.store.pipe(
      select(fromHomuUsers.getProfileHomuUserPortalEmail),
      takeUntil(this.destroyed$)
    );
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
