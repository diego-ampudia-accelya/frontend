import { cloneDeep } from 'lodash';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';
import { HomuUserBE, HomuUserFilter } from '../../models/homu-users.model';
import { HomuUsersActions } from '../actions';
import { getData, getLoading, getQuery, initialState, reducer, State } from './homu-users-list.reducer';

const determineInitialQuery = (storedQuery: DataQuery<HomuUserFilter>): DataQuery<HomuUserFilter> => {
  const query: DataQuery<HomuUserFilter> = storedQuery || cloneDeep(defaultQuery);

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = {
    ...query.filterBy
  };

  return initialQuery;
};

const mockData: HomuUserBE = {
  id: 1,
  portalEmail: 'test@example.com',
  level: 2,
  name: 'Name 1',
  userCode: 'JD123',
  registerDate: new Date('2022-01-15'),
  expiryDate: new Date('2023-01-15'),
  numberOfUsers: 5,
  status: null,
  contactInfo: {
    organization: 'XYZ Inc.',
    telephone: '+1234567890',
    country: 'USA',
    city: 'New York',
    address: '123 Main St',
    postCode: '10001',
    email: 'test@example.com'
  },
  template: null
};

const mockPagedData: PagedData<HomuUserBE> = {
  records: [mockData],
  pageSize: 0,
  total: 0,
  totalPages: 0,
  pageNumber: 1
};

const loadData = (state: State, data: PagedData<HomuUserBE>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const queryMock: DataQuery<HomuUserFilter> = {
  ...defaultQuery,
  filterBy: {
    portalEmail: 'test@email,com'
  }
};

describe('Homu Users List Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual(initialState);
  });

  describe('Reducers', () => {
    it('should handle Homu Users list load action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersActions.load();
      const expectedState: State = {
        ...initialState,
        loading: true,
        query: determineInitialQuery(defaultQuery)
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users list load success action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersActions.loadSuccess({ data: mockPagedData });
      const expectedState: State = loadData(state, mockPagedData);
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users list search action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersActions.search({ query: queryMock });
      const expectedState: State = {
        ...state,
        query: {
          ...queryMock,
          filterBy: {
            ...queryMock.filterBy
          }
        },
        previousQuery: state.query
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users list search confirmed action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersActions.searchConfirmed();
      const expectedState: State = {
        ...state,
        loading: true,
        previousQuery: null
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users list search success action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersActions.searchSuccess({ data: mockPagedData });
      const expectedState: State = {
        ...loadData(state, mockPagedData)
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users list search error action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersActions.searchError();
      const expectedState: State = {
        ...state,
        loading: false,
        data: []
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });
  });

  describe('Selectors', () => {
    it('should get Loading from state', () => {
      const state: State = { ...initialState };
      expect(getLoading(state)).toBe(false);
    });

    it('should get getData from state', () => {
      const state: State = { ...initialState, data: [mockData] };
      expect(getData(state)).toBe(state.data);
    });

    it('should get getQuery from state', () => {
      const state: State = { ...initialState, data: [mockData] };
      expect(getQuery(state)).toBe(state.query);
    });
  });
});
