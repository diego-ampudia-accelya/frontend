import { Action, createReducer, on } from '@ngrx/store';
import { chain } from 'lodash';

import { HomuUserPermission, HomuUserTemplateForPermissisons } from '../../models/homu-users-permissions.model';
import { HomuUsersPermissionsActions } from '~app/master-data/iata/homu-users/store/actions';
import { ChangeModel } from '~app/shared/components';
import { SelectionList, SelectionListItem } from '~app/shared/components/selection-list/selection-list.models';
import { UserTemplate } from '~app/shared/models/user-template.model';

export const homuUserPermissionsFeatureKey = 'homuUserPermissions';

export interface State {
  userId: number;
  original: SelectionList[];
  modified: SelectionList[];
  loading: boolean;
  bspId: number;
}

export const initialState: State = {
  userId: null,
  original: [],
  modified: [],
  loading: false,
  bspId: null
};

const homuUserPermissionsReducer = createReducer(
  initialState,
  on(HomuUsersPermissionsActions.requestHomuUserPermissions, () => ({ ...initialState, loading: true })),
  on(
    HomuUsersPermissionsActions.requestHomuUserPermissionsSuccess,
    HomuUsersPermissionsActions.applyChangesSuccess,
    (state, { permissions, user }) => {
      const original = mapPermissionsToSelectionList(permissions, user.template);

      return {
        ...state,
        original,
        userId: user.id,
        modified: original,
        loading: false
      };
    }
  ),
  on(HomuUsersPermissionsActions.requestHomuUserPermissionsError, () => ({
    ...initialState,
    hasError: true,
    loading: false
  })),
  on(HomuUsersPermissionsActions.change, (state, { modifications }) => ({
    ...state,
    modified: modifications
  })),
  on(HomuUsersPermissionsActions.applyChanges, state => ({ ...state, loading: true })),
  on(HomuUsersPermissionsActions.applyChangesError, state => ({ ...state, loading: false })),
  on(HomuUsersPermissionsActions.discardChanges, state => ({
    ...state,
    original: [...state.original],
    modified: state.original
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return homuUserPermissionsReducer(state, action);
}

export const getOriginal = (state: State) => state.original;

export const getModified = (state: State) => state.modified;

export const getLoading = (state: State) => state.loading;

export const getBspId = (state: State) => state.bspId;

export const hasChanges = (state: State) => state.modified?.some(permissionCategory => permissionCategory.hasChanged);

export function getChanges(state: State): ChangeModel[] {
  return state.modified
    .filter(category => category.hasChanged)
    .map(category => ({ ...category, items: category.items.filter(item => item.hasChanged) }))
    .reduce((changes, item) => {
      item.items.forEach((listItem: SelectionListItem) => {
        changes.push({
          group: item.name,
          name: listItem.name,
          value: String(listItem.checked),
          originalValue: listItem.checked
        });
      });

      return changes;
    }, []);
}

export function mapSelectionListToPermissions(
  selectionListItems: SelectionList[],
  multiPermissionsEnabled = true
): HomuUserPermission[] {
  return chain(selectionListItems)
    .map(category => category.items)
    .flatten()
    .map(item => ({ ...item.value, enabled: multiPermissionsEnabled ? item.checked : !item.checked }))
    .value();
}

function mapPermissionsToSelectionList(permissions: HomuUserPermission[], userTemplate: UserTemplate): SelectionList[] {
  const isStreamlinedUser = userTemplate === UserTemplate.Streamlined;

  return chain(permissions)
    .map(permission => {
      // if user is streamlined or permission's template is basic, then the checkboxes are enabled
      const disabled = !isStreamlinedUser && permission.template !== HomuUserTemplateForPermissisons.Basic;

      return {
        name: permission.name,
        checked: permission.enabled,
        value: permission,
        hasChanged: false,
        disabled,
        tooltip: disabled ? 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.disabledTooltip' : ''
      };
    })
    .groupBy('value.category.name')
    .map((items, name) => {
      const disabled = items.every(item => item.disabled);

      return {
        name,
        disabled,
        items,
        tooltip: disabled ? 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.disabledTooltip' : ''
      };
    })
    .value();
}
