import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromHomuUserPermissions from './homu-user-permissions.reducer';
import * as fromHomuUserProfile from './homu-user-profile.reducer';
import * as fromHomuUsersHosusList from './homu-users-hosus-list.reducer';
import * as fromHomuUsersList from './homu-users-list.reducer';
import { AppState } from '~app/reducers';

export const homuUsersFeatureKey = 'homuUsers';

export interface HomuUsersState {
  [fromHomuUsersList.homuUsersListkey]: fromHomuUsersList.State;
  [fromHomuUserProfile.homuUserProfileFeatureKey]: fromHomuUserProfile.State;
  [fromHomuUsersHosusList.homuUsersHosusListkey]: fromHomuUsersHosusList.State;
  [fromHomuUserPermissions.homuUserPermissionsFeatureKey]: fromHomuUserPermissions.State;
}
export interface State extends AppState {
  [homuUsersFeatureKey]: HomuUsersState;
}

export function reducers(state: HomuUsersState | undefined, action: Action) {
  return combineReducers({
    [fromHomuUsersList.homuUsersListkey]: fromHomuUsersList.reducer,
    [fromHomuUsersHosusList.homuUsersHosusListkey]: fromHomuUsersHosusList.reducer,
    [fromHomuUserProfile.homuUserProfileFeatureKey]: fromHomuUserProfile.reducer,
    [fromHomuUserPermissions.homuUserPermissionsFeatureKey]: fromHomuUserPermissions.reducer
  })(state, action);
}

export const getHomuUsersState = createFeatureSelector<State, HomuUsersState>(homuUsersFeatureKey);

//List

export const getList = createSelector(getHomuUsersState, state => state[fromHomuUsersList.homuUsersListkey]);

export const getLoading = createSelector(getList, fromHomuUsersList.getLoading);

export const getData = createSelector(getList, fromHomuUsersList.getData);

export const hasData = createSelector(getData, data => data && data.length > 0);

export const getQuery = createSelector(getList, fromHomuUsersList.getQuery);

export const getDownloadQuery = createSelector(
  getQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

//Profile

export const getHomuUserProfileState = createSelector(
  getHomuUsersState,
  state => state[fromHomuUserProfile.homuUserProfileFeatureKey]
);

export const getProfileHomuUser = createSelector(getHomuUserProfileState, fromHomuUserProfile.getHomuUser);

export const getProfileHomuLoading = createSelector(getHomuUserProfileState, fromHomuUserProfile.getLoading);

export const getProfileHomuUserPortalEmail = createSelector(
  getHomuUserProfileState,
  fromHomuUserProfile.getHomuUserPortalEmail
);

export const getProfileHomuUserRequestError = createSelector(
  getHomuUserProfileState,
  fromHomuUserProfile.homuUserRequestError
);

//Hosus List

export const getHosusList = createSelector(
  getHomuUsersState,
  state => state[fromHomuUsersHosusList.homuUsersHosusListkey]
);

export const getHosusLoading = createSelector(getHosusList, fromHomuUsersHosusList.getLoading);

export const getHosusData = createSelector(getHosusList, fromHomuUsersHosusList.getData);

export const hasHosusData = createSelector(getHosusData, data => data && data.length > 0);

export const getHosusTotalElements = createSelector(getHosusList, fromHomuUsersHosusList.getDataTotalElements);

export const getHosusQuery = createSelector(getHosusList, fromHomuUsersHosusList.getQuery);

export const getHosusDownloadQuery = createSelector(
  getQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

//Permissions

export const getHomuUserPermissionsState = createSelector(
  getHomuUsersState,
  state => state[fromHomuUserPermissions.homuUserPermissionsFeatureKey]
);
export const getHomuUserPermissionsLoading = createSelector(
  getHomuUserPermissionsState,
  fromHomuUserPermissions.getLoading
);
export const getOriginalHomuUserPermissions = createSelector(
  getHomuUserPermissionsState,
  fromHomuUserPermissions.getOriginal
);
export const canApplyPermissionsChanges = createSelector(
  getHomuUserPermissionsState,
  fromHomuUserPermissions.hasChanges
);
export const getModifiedPermissions = createSelector(getHomuUserPermissionsState, fromHomuUserPermissions.getModified);
export const getPermissionsChanges = createSelector(getHomuUserPermissionsState, fromHomuUserPermissions.getChanges);
