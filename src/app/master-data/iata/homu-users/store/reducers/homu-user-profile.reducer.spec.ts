import { ServerError } from '~app/shared/errors';
import { ResponseErrorBE } from '~app/shared/models';
import { HomuUserBE } from '../../models/homu-users.model';
import { HomuUserProfileActions } from '../actions';
import {
  getHomuUser,
  getHomuUserPortalEmail,
  getLoading,
  homuUserRequestError,
  initialState,
  reducer,
  State
} from './homu-user-profile.reducer';

const mockHomuUser: HomuUserBE = {
  id: 1,
  portalEmail: 'test@example.com',
  level: 2,
  name: 'Name 1',
  userCode: 'JD123',
  registerDate: new Date('2022-01-15'),
  expiryDate: new Date('2023-01-15'),
  numberOfUsers: 5,
  status: null,
  contactInfo: {
    organization: 'XYZ Inc.',
    telephone: '+1234567890',
    country: 'USA',
    city: 'New York',
    address: '123 Main St',
    postCode: '10001',
    email: 'test@example.com'
  },
  template: null
};

describe('Homu User Profile Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual(initialState);
  });

  describe('Reducers', () => {
    it('should handle request Homu User action', () => {
      const state: State = { ...initialState };
      const homuUserId = 1;
      const action = HomuUserProfileActions.requestHomuUser({ id: homuUserId });
      expect(reducer(state, action)).toEqual(initialState);
    });

    it('should handle request Homu User Success action', () => {
      const state: State = { ...initialState };
      const action = HomuUserProfileActions.requestHomuUserSuccess({ homuUser: mockHomuUser });
      const expectedState: State = {
        ...state,
        loading: false,
        homuUser: mockHomuUser
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle request request HomuUser Error action', () => {
      const state: State = { ...initialState };
      const errorMock: Partial<ResponseErrorBE> = {
        errorCode: 500,
        errorMessage: 'Internal Server Error'
      };

      const serverError = new ServerError(errorMock);
      const action = HomuUserProfileActions.requestHomuUserError({ error: serverError });
      const expectedState: State = {
        ...state,
        homuUserRequestError: serverError
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });
  });

  describe('Selectors', () => {
    it('should get Loading from state', () => {
      const state: State = { ...initialState };
      expect(getLoading(state)).toBe(false);
    });

    it('should get Homu User from state', () => {
      const state: State = { ...initialState, homuUser: mockHomuUser };
      expect(getHomuUser(state)).toBe(state.homuUser);
    });

    it('should get Homu User Portal Email from state', () => {
      const state: State = { ...initialState, homuUser: mockHomuUser };
      expect(getHomuUserPortalEmail(state)).toEqual(state.homuUser.portalEmail);
    });

    it('should try to get Homu User Portal Email from state and if there is not a homu user return an empty string', () => {
      const state: State = { ...initialState };
      expect(getHomuUserPortalEmail(state)).toEqual('');
    });

    it('should get homu User Request Error from state', () => {
      const state: State = { ...initialState };
      expect(homuUserRequestError(state)).toEqual(state.homuUserRequestError);
    });
  });
});
