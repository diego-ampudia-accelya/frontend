import { ChangeModel } from '~app/shared/components/changes-dialog';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { HomuUserPermission, HomuUserTemplateForPermissisons } from '../../models/homu-users-permissions.model';
import { HomuUserBE } from '../../models/homu-users.model';
import { HomuUsersPermissionsActions } from '../actions';
import {
  getBspId,
  getChanges,
  getLoading,
  getModified,
  getOriginal,
  hasChanges,
  initialState,
  mapSelectionListToPermissions,
  reducer,
  State
} from './homu-user-permissions.reducer';

const mockPermissions: HomuUserPermission[] = [
  {
    id: 1,
    code: 'code_permission',
    name: 'name_permission',
    category: null,
    enabled: true,
    template: HomuUserTemplateForPermissisons.Basic
  }
];

const mockUser: HomuUserBE = {
  id: 1,
  portalEmail: 'test@example.com',
  level: 2,
  name: 'Name 1',
  userCode: 'JD123',
  registerDate: new Date('2022-01-15'),
  expiryDate: new Date('2023-01-15'),
  numberOfUsers: 5,
  status: null,
  contactInfo: {
    organization: 'XYZ Inc.',
    telephone: '+1234567890',
    country: 'USA',
    city: 'New York',
    address: '123 Main St',
    postCode: '10001',
    email: 'test@example.com'
  },
  template: null
};

const mockOriginal = [
  {
    name: 'undefined',
    disabled: false,
    items: [
      {
        name: 'name_permission',
        checked: true,
        value: {
          id: 1,
          code: 'code_permission',
          name: 'name_permission',
          category: null,
          enabled: true,
          template: 'BASIC'
        },
        hasChanged: false,
        disabled: false,
        tooltip: ''
      }
    ],
    tooltip: ''
  }
];
const mockModified = [
  {
    name: 'undefined',
    disabled: false,
    items: [
      {
        name: 'name_permission',
        checked: true,
        value: {
          id: 1,
          code: 'code_permission',
          name: 'name_permission',
          category: null,
          enabled: true,
          template: 'BASIC'
        },
        hasChanged: false,
        disabled: false,
        tooltip: ''
      }
    ],
    tooltip: ''
  }
];

describe('Homu User Permissions Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual(initialState);
  });

  it('should get changes from the state', () => {
    const mockModifiedChanges = [
      {
        name: 'undefined',
        disabled: false,
        items: [
          {
            name: 'name_permission',
            checked: true,
            value: {
              id: 1,
              code: 'code_permission',
              name: 'name_permission',
              category: null,
              enabled: true,
              template: 'BASIC'
            },
            hasChanged: false,
            disabled: false,
            tooltip: ''
          }
        ],
        tooltip: '',
        hasChanged: true
      }
    ];
    const state: State = { ...initialState, modified: mockModifiedChanges };
    const expectedChangesResult: ChangeModel[] = [];
    expect(getChanges(state)).toEqual(expectedChangesResult);
  });

  it('should map Selection List To Permissions', () => {
    const selectionList = [
      {
        name: 'name_permission1',
        items: [
          { value: { id: 1 }, checked: true },
          { value: { id: 2 }, checked: false }
        ],
        disabled: false
      },
      {
        name: 'name_permission2',
        items: [
          { value: { id: 3 }, checked: true },
          { value: { id: 4 }, checked: true }
        ],
        disabled: false
      }
    ] as SelectionList[];

    const result = mapSelectionListToPermissions(selectionList);
    const enabledFirstElement = result[0].enabled;
    expect(enabledFirstElement).toBeTruthy();
  });

  describe('Reducers', () => {
    it('should handle request Homu User Permissions action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersPermissionsActions.requestHomuUserPermissions();
      const expectedState: State = {
        ...state,
        loading: true
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle request Homu User Permissions action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersPermissionsActions.requestHomuUserPermissionsSuccess({
        permissions: mockPermissions,
        user: mockUser
      });

      const expectedState: State = {
        ...state,
        loading: false,
        userId: mockUser.id,
        original: mockOriginal,
        modified: mockModified
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle change Permissions action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersPermissionsActions.change({ modifications: mockModified });
      const expectedState: State = {
        ...state,
        modified: mockModified
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle apply Changes Permissions action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersPermissionsActions.applyChanges();
      const expectedState: State = {
        ...state,
        loading: true
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle apply Changes Error Permissions action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersPermissionsActions.applyChangesError();
      const expectedState: State = {
        ...state,
        loading: false
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle apply discard Changes Permissions action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersPermissionsActions.discardChanges();
      const expectedState: State = {
        ...state,
        original: [...state.original],
        modified: state.original
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });
  });

  describe('Selectors', () => {
    it('should get Original from state', () => {
      const state: State = { ...initialState };
      expect(getOriginal(state)).toBe(state.original);
    });

    it('should get Modified from state', () => {
      const state: State = { ...initialState };
      expect(getModified(state)).toBe(state.modified);
    });

    it('should get Loading from state', () => {
      const state: State = { ...initialState };
      expect(getLoading(state)).toBe(state.loading);
    });

    it('should get Bsp Id from state', () => {
      const state: State = { ...initialState };
      expect(getBspId(state)).toBe(state.bspId);
    });

    it('should get if hasChanges from state', () => {
      const state: State = { ...initialState, modified: mockModified };
      expect(hasChanges(state)).toBe(false);
    });
  });
});
