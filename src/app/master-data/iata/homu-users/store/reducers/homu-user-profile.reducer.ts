import { Action, createReducer, on } from '@ngrx/store';

import { HomuUserBE } from '../../models/homu-users.model';
import { HomuUserProfileActions } from '../actions';
import { ServerError } from '~app/shared/errors';

export const homuUserProfileFeatureKey = 'homuUserProfile';

export interface State {
  loading: boolean;
  homuUser: HomuUserBE;
  homuUserRequestError: ServerError;
}

export const initialState: State = {
  loading: false,
  homuUser: null,
  homuUserRequestError: null
};

const homuUserProfileReducer = createReducer(
  initialState,
  on(HomuUserProfileActions.requestHomuUser, () => ({
    ...initialState
  })),
  on(HomuUserProfileActions.requestHomuUserSuccess, (state, { homuUser }) => ({
    ...state,
    homuUser,
    loading: false
  })),
  on(HomuUserProfileActions.requestHomuUserError, (_state, { error }) => ({
    ...initialState,
    homuUserRequestError: error
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return homuUserProfileReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;

export const getHomuUser = (state: State): HomuUserBE => state.homuUser;

export const getHomuUserPortalEmail = (state: State): string => state.homuUser?.portalEmail || '';

export const homuUserRequestError = (state: State): ServerError => state.homuUserRequestError;
