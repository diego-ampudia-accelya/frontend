import { cloneDeep } from 'lodash';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';
import { HomuUserHosusBE, HomuUserHosusFilter } from '../../models/homu-users.model';
import { HomuUsersHosusActions } from '../actions';
import {
  getData,
  getDataTotalElements,
  getLoading,
  getQuery,
  initialState,
  reducer,
  State
} from './homu-users-hosus-list.reducer';

const mockData: HomuUserHosusBE = {
  id: 1,
  portalEmail: 'test@example.com',
  level: 2,
  name: 'Name 1',
  userCode: 'JD123',
  registerDate: new Date('2022-01-15'),
  expiryDate: new Date('2023-01-15'),
  numberOfUsers: 5,
  status: null,
  contactInfo: {
    organization: 'XYZ Inc.',
    telephone: '+1234567890',
    country: 'USA',
    city: 'New York',
    address: '123 Main St',
    postCode: '10001',
    email: 'test@example.com'
  },
  template: null,
  mainUser: true
};

const mockPagedData: PagedData<HomuUserHosusBE> = {
  records: [mockData],
  pageSize: 0,
  total: 0,
  totalPages: 0,
  pageNumber: 1
};

const queryMock: DataQuery<HomuUserHosusFilter> = {
  ...defaultQuery,
  filterBy: {
    portalEmail: 'test@email,com'
  }
};

const determineInitialQuery = (storedQuery: DataQuery<HomuUserHosusFilter>): DataQuery<HomuUserHosusFilter> => {
  const query: DataQuery<HomuUserHosusFilter> = storedQuery || cloneDeep(defaultQuery);

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = {
    ...query.filterBy
  };

  return initialQuery;
};

const loadData = (state: State, data: PagedData<HomuUserHosusBE>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

describe('Homu Users Hosus List Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual(initialState);
  });

  describe('Reducers', () => {
    it('should handle Homu Users Hosus list load action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersHosusActions.load();
      const expectedState: State = {
        ...initialState,
        loading: true,
        query: determineInitialQuery(defaultQuery)
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users Hosus list load success action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersHosusActions.loadSuccess({ data: mockPagedData });
      const expectedState: State = loadData(state, mockPagedData);
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users Hosus list search action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersHosusActions.search({ query: queryMock });
      const expectedState: State = {
        ...state,
        query: {
          ...queryMock,
          filterBy: {
            ...queryMock.filterBy
          }
        },
        previousQuery: state.query
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users Hosus list search confirmed action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersHosusActions.searchConfirmed();
      const expectedState: State = {
        ...state,
        loading: true,
        previousQuery: null
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users Hosus list search success action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersHosusActions.searchSuccess({ data: mockPagedData });
      const expectedState: State = {
        ...loadData(state, mockPagedData)
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });

    it('should handle Homu Users Hosus list search error action', () => {
      const state: State = { ...initialState };
      const action = HomuUsersHosusActions.searchError();
      const expectedState: State = {
        ...state,
        loading: false,
        data: []
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });
  });

  describe('Selectors', () => {
    it('should get Loading from state', () => {
      const state: State = { ...initialState };
      expect(getLoading(state)).toBe(false);
    });

    it('should get getData from state', () => {
      const state: State = { ...initialState, data: [mockData] };
      expect(getData(state)).toBe(state.data);
    });

    it('should get getQuery from state', () => {
      const state: State = { ...initialState, data: [mockData] };
      expect(getQuery(state)).toBe(state.query);
    });

    it('should get getQuery from state', () => {
      const state: State = { ...initialState, query: defaultQuery };
      expect(getDataTotalElements(state)).toBe(state.query.paginateBy.totalElements);
    });
  });
});
