import { Action, createReducer, on } from '@ngrx/store';
import { cloneDeep } from 'lodash';

import { HomuUserHosusBE, HomuUserHosusFilter } from '../../models/homu-users.model';
import { HomuUsersHosusActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

export const homuUsersHosusListkey = 'homuUsersHosusList';

export interface State {
  query: DataQuery<HomuUserHosusFilter>;
  previousQuery: DataQuery<HomuUserHosusFilter>;
  data: HomuUserHosusBE[];
  loading: boolean;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false
};

const loadData = (state: State, data: PagedData<HomuUserHosusBE>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (storedQuery: DataQuery<HomuUserHosusFilter>): DataQuery<HomuUserHosusFilter> => {
  const query: DataQuery<HomuUserHosusFilter> = storedQuery || cloneDeep(defaultQuery);

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = {
    ...query.filterBy
  };

  return initialQuery;
};

export const homuUsersListReducer = createReducer(
  initialState,
  on(HomuUsersHosusActions.load, state => ({
    ...state,
    loading: true,
    query: determineInitialQuery(state.query)
  })),
  on(HomuUsersHosusActions.loadSuccess, (state, { data }) => ({
    ...loadData(state, data)
  })),
  on(HomuUsersHosusActions.search, (state, { query = state.query }) => ({
    ...state,
    query: {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    },
    previousQuery: state.query
  })),
  on(HomuUsersHosusActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(HomuUsersHosusActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(HomuUsersHosusActions.searchError, HomuUsersHosusActions.loadError, state => ({
    ...state,
    loading: false,
    data: []
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return homuUsersListReducer(state, action);
}

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getDataTotalElements = (state: State) => state.query.paginateBy.totalElements;

export const getQuery = (state: State) => state.query;
