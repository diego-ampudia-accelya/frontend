import { createAction, props } from '@ngrx/store';

import { HomuUserBE, HomuUserFilter } from '../../models/homu-users.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const load = createAction('[Homu Users] Load');
export const loadSuccess = createAction('[Homu Users] Load Success', props<{ data: PagedData<HomuUserBE> }>());
export const loadError = createAction('[Homu Users] Load Error');

export const search = createAction('[Homu Users] Search', props<{ query?: DataQuery<HomuUserFilter> }>());
export const searchConfirmed = createAction('[Homu Users] Search Confirmed');
export const searchSuccess = createAction('[Homu Users] Search Success', props<{ data: PagedData<HomuUserBE> }>());
export const searchError = createAction('[Homu Users] Search Error');

export const download = createAction('[Homu Users] Download');
export const openDownload = createAction('[Homu Users] Open Download');
