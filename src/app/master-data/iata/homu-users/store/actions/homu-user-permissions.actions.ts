import { createAction, props } from '@ngrx/store';

import { HomuUserPermission } from '../../models/homu-users-permissions.model';
import { HomuUserBE } from '../../models/homu-users.model';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';

export const requestHomuUserPermissions = createAction('[HomuUsersPermissions] Homu Users Permissions');

export const requestHomuUserPermissionsSuccess = createAction(
  '[HomuUsersPermissions] Homu User Permissions Request Success',
  props<{ permissions: HomuUserPermission[]; user: HomuUserBE }>()
);

export const requestHomuUserPermissionsError = createAction(
  '[HomuUsersPermissions] Homu User Permissions Request Error'
);

export const change = createAction('[HomuUsersPermissions] Change', props<{ modifications: SelectionList[] }>());

export const openApplyChanges = createAction('[HomuUsersPermissions] Open Apply Changes');

export const applyChanges = createAction('[HomuUsersPermissions] Apply Changes');

export const applyChangesSuccess = createAction(
  '[HomuUsersPermissions]  Apply Changes Success',
  props<{ permissions: HomuUserPermission[]; user: HomuUserBE }>()
);

export const applyChangesError = createAction('[HomuUsersPermissions] Apply Changes Error');

export const discardChanges = createAction('[HomuUsersPermissions] Discard Changes');

export const cancel = createAction('[HomuUsersPermissions] Cancel');
