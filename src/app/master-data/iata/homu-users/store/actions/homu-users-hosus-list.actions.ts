import { createAction, props } from '@ngrx/store';

import { HomuUserHosusBE, HomuUserHosusFilter } from '../../models/homu-users.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const load = createAction('[Homu Users Hosu] Load');
export const loadSuccess = createAction(
  '[Homu Users Hosu] Load Success',
  props<{ data: PagedData<HomuUserHosusBE> }>()
);
export const loadError = createAction('[Homu Users Hosu] Load Error');

export const search = createAction('[Homu Users Hosu] Search', props<{ query?: DataQuery<HomuUserHosusFilter> }>());
export const searchConfirmed = createAction('[Homu Users Hosu] Search Confirmed');
export const searchSuccess = createAction(
  '[Homu Users Hosu] Search Success',
  props<{ data: PagedData<HomuUserHosusBE> }>()
);
export const searchError = createAction('[Homu Users Hosu] Search Error');

export const download = createAction('[Homu Users Hosu] Download');
export const openDownload = createAction('[Homu Users Hosu] Open Download');
