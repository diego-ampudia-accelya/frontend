import { createAction, props } from '@ngrx/store';

import { HomuUserBE } from '../../models/homu-users.model';
import { ServerError } from '~app/shared/errors';

export const requestHomuUser = createAction('[Homu User Profile] Request HomuUser', props<{ id: number }>());

export const requestHomuUserSuccess = createAction(
  '[HomuUser Profile] Request Homu User Success',
  props<{ homuUser: HomuUserBE }>()
);

export const requestHomuUserError = createAction(
  '[HomuUser Profile] Request Homu User Error',
  props<{ error: ServerError }>()
);
