import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import { HomuUsersService } from '../../services/homu-users.service';
import { HomuUserProfileActions } from '../actions';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class HomuUserProfileEffects {
  $requestSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUserProfileActions.requestHomuUser),
      switchMap(({ id }) => this.homuUsersService.getHomuById(id)),
      map(homuUser => HomuUserProfileActions.requestHomuUserSuccess({ homuUser })),
      rethrowError(error => HomuUserProfileActions.requestHomuUserError({ error }))
    )
  );

  constructor(private actions$: Actions, private homuUsersService: HomuUsersService) {}
}
