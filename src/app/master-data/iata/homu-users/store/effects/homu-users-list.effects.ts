import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';

import { HomuUsersService } from '../../services/homu-users.service';
import { HomuUsersActions } from '../actions';
import * as fromHomuUsers from '../reducers';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class HomuUsersListEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersActions.load),
      withLatestFrom(this.store.pipe(select(fromHomuUsers.getQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(data =>
            HomuUsersActions.loadSuccess({
              data
            })
          ),
          rethrowError(() => HomuUsersActions.loadError())
        )
      )
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersActions.search),
      switchMap(() => of(HomuUsersActions.searchConfirmed()))
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromHomuUsers.getQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(result =>
            HomuUsersActions.searchSuccess({
              data: result
            })
          ),
          rethrowError(() => HomuUsersActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersActions.download),
      switchMap(() => of(HomuUsersActions.openDownload()))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(HomuUsersActions.openDownload),
        withLatestFrom(this.store.pipe(select(fromHomuUsers.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: HomuUsersService,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {}
}
