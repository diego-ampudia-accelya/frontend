import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockProviders } from 'ng-mocks';
import { Observable, of } from 'rxjs';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { NotificationService } from '~app/shared/services';
import { HomuUserPermissionsChangesService } from '../../services/homu-user-permissions-changes.service';
import { HomuUsersPermissionsService } from '../../services/homu-users-permissions.service';
import { HomuUsersPermissionsActions } from '../actions';
import { State } from '../reducers/homu-user-permissions.reducer';
import { HomuUserPermissionsEffects } from './homu-user-permissions.effects';

describe('HomuUserPermissionsEffects', () => {
  let effects: HomuUserPermissionsEffects;
  let actions$: Observable<Action>;

  const homuUsersPermissionsServiceSpy: SpyObject<HomuUsersPermissionsService> =
    createSpyObject(HomuUsersPermissionsService);
  const notificationServiceSpy: SpyObject<NotificationService> = createSpyObject(NotificationService);
  const homuUserPermissionsChangesServiceSpy: SpyObject<HomuUserPermissionsChangesService> = createSpyObject(
    HomuUserPermissionsChangesService
  );

  const initialState: State = {
    userId: null,
    original: [],
    modified: [],
    loading: false,
    bspId: null
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        MockProviders(L10nTranslationService),
        provideMockActions(() => actions$),
        HomuUserPermissionsEffects,
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromHomuUsers.getModifiedPermissions, value: [] },
            { selector: fromHomuUsers.getProfileHomuUser, value: [] },
            { selector: fromHomuUsers.getProfileHomuUserPortalEmail, value: [] },
            { selector: fromHomuUsers.getHomuUsersState, value: [] },
            { selector: fromHomuUsers.getHomuUserPermissionsState, value: initialState }
          ]
        }),
        { provide: HomuUsersPermissionsService, useValue: homuUsersPermissionsServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: HomuUserPermissionsChangesService, homuUserPermissionsChangesServiceSpy }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(HomuUserPermissionsEffects);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('Apply Changes Effects', () => {
    it('should update Permissions By User Id and apply changes success when applyChanges effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersPermissionsActions.applyChanges);

      homuUsersPermissionsServiceSpy.updatePermissionsByUserId.and.returnValue(of([]));
      effects.applyChanges$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersPermissionsActions.applyChangesSuccess.type);
    }));

    it('should show notification Success when applyChangesSuccess effect is triggered', fakeAsync(() => {
      actions$ = of(HomuUsersPermissionsActions.applyChangesSuccess);

      effects.$applyChangesSuccess.subscribe();
      tick();

      expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
    }));
  });

  describe('Request Homu User Permissions Effects', () => {
    it('should get Homu Permissions by Id when requestHomuUserPermissions effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersPermissionsActions.requestHomuUserPermissions);

      homuUsersPermissionsServiceSpy.getHomuPermissionsById.and.returnValue(of([]));
      effects.requestHomuUserPermissions$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersPermissionsActions.requestHomuUserPermissionsSuccess.type);
    }));
  });
});
