import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';
import { DialogService } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { HomuUserBE } from '../../models/homu-users.model';
import { HomuUsersHosusService } from '../../services/homu-users.hosus.service';
import { HomuUsersHosusActions } from '../actions';
import * as fromHomuUsers from '../reducers';
import { HomuUsersHosusListEffects } from './homu-users-hosus-list.effects';

describe('HomuUsersHosusListEffects', () => {
  let effects: HomuUsersHosusListEffects;
  let actions$: Observable<Action>;

  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const homuUsersServiceSpy: SpyObject<HomuUsersHosusService> = createSpyObject(HomuUsersHosusService);

  const initialState = {
    auth: {
      user: null
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  const mockUser: HomuUserBE = {
    id: 1,
    portalEmail: 'test@example.com',
    level: 2,
    name: 'Name 1',
    userCode: 'JD123',
    registerDate: new Date('2022-01-15'),
    expiryDate: new Date('2023-01-15'),
    numberOfUsers: 5,
    status: null,
    contactInfo: {
      organization: 'XYZ Inc.',
      telephone: '+1234567890',
      country: 'USA',
      city: 'New York',
      address: '123 Main St',
      postCode: '10001',
      email: 'test@example.com'
    },
    template: null
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockActions(() => actions$),
        HomuUsersHosusListEffects,
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromHomuUsers.getQuery, value: defaultQuery },
            { selector: fromHomuUsers.getDownloadQuery, value: defaultQuery },
            { selector: fromHomuUsers.getHosusQuery, value: [] },
            { selector: fromHomuUsers.getProfileHomuUser, value: mockUser }
          ]
        }),
        {
          provide: HomuUsersHosusService,
          useValue: homuUsersServiceSpy
        },
        {
          provide: DialogService,
          useValue: dialogServiceSpy
        }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(HomuUsersHosusListEffects);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('Load Homu Users Hosus List Effects', () => {
    it('should load homu users list and dispatch loadSuccess action when load effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersHosusActions.load);

      homuUsersServiceSpy.find.and.returnValue(of({}));
      effects.load$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersHosusActions.loadSuccess.type);
    }));
  });

  describe('Search Homu Users Hosus List Effects', () => {
    it('should search homu users list and dispatch searchConfirmed action when search effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersHosusActions.search);

      effects.search$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction).toEqual(HomuUsersHosusActions.searchConfirmed());
    }));

    it('should confirm the search and trigger search success action when searchConfirmed action is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersHosusActions.searchConfirmed);

      homuUsersServiceSpy.find.and.returnValue(of({}));
      effects.searchConfirmed$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersHosusActions.searchSuccess.type);
    }));
  });

  describe('Download Homu Users Hosus List Effects', () => {
    it('should download homu users list and dispatch openDownload action when download effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersHosusActions.download);

      effects.download$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersHosusActions.openDownload.type);
    }));

    it('should confirm download and open method from dialog service when downloadConfirmed action is triggered', fakeAsync(() => {
      actions$ = of(HomuUsersHosusActions.openDownload);
      dialogServiceSpy.open.and.returnValue(of({}));

      effects.downloadConfirmed$.subscribe();
      tick();

      expect(dialogServiceSpy.open).toHaveBeenCalled();
    }));
  });
});
