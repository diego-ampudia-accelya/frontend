import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { HomuUserPermissionsChangesService } from '../../services/homu-user-permissions-changes.service';
import { HomuUsersPermissionsService } from '../../services/homu-users-permissions.service';
import { HomuUsersPermissionsActions } from '../actions';
import { mapSelectionListToPermissions } from '../reducers/homu-user-permissions.reducer';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { AppState } from '~app/reducers';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class HomuUserPermissionsEffects {
  requestHomuUserPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersPermissionsActions.requestHomuUserPermissions),
      withLatestFrom(this.store.pipe(select(fromHomuUsers.getProfileHomuUser))),
      switchMap(([_, user]) =>
        this.homuUsersPermissionsService.getHomuPermissionsById(user.id).pipe(
          map(permissions =>
            HomuUsersPermissionsActions.requestHomuUserPermissionsSuccess({
              permissions,
              user
            })
          )
        )
      ),
      rethrowError(() => HomuUsersPermissionsActions.requestHomuUserPermissionsError())
    )
  );

  openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersPermissionsActions.openApplyChanges),
      switchMap(() => this.changesService.applyUserPermissions$)
    )
  );

  applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersPermissionsActions.applyChanges),
      withLatestFrom(
        this.store.pipe(select(fromHomuUsers.getModifiedPermissions)),
        this.store.pipe(select(fromHomuUsers.getProfileHomuUser))
      ),
      switchMap(([_, modified, user]) =>
        this.homuUsersPermissionsService
          .updatePermissionsByUserId(user.id, mapSelectionListToPermissions(modified))
          .pipe(map(permissions => HomuUsersPermissionsActions.applyChangesSuccess({ permissions, user })))
      ),
      rethrowError(() => HomuUsersPermissionsActions.applyChangesError())
    )
  );

  $applyChangesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(HomuUsersPermissionsActions.applyChangesSuccess),
        withLatestFrom(this.store.pipe(select(fromHomuUsers.getProfileHomuUserPortalEmail))),
        tap(([_, email]) => {
          const message = this.translationService.translate('mainUsers.permissions.changesDialog.editUserSuccess', {
            email: `<span class="link">${email}</span>`
          });
          this.notificationService.showSuccess(message);
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private changesService: HomuUserPermissionsChangesService,
    private homuUsersPermissionsService: HomuUsersPermissionsService,
    private store: Store<AppState>,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}
}
