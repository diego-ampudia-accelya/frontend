import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { HomuUsersHosusService } from '../../services/homu-users.hosus.service';
import { HomuUsersHosusActions } from '../actions';
import * as fromHomuUsers from '../reducers';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class HomuUsersHosusListEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersHosusActions.load),
      switchMap(() => this.getHosusQueryAndHomuUser()),
      switchMap(([query, homuUser]) =>
        this.dataService.find(homuUser.id, query).pipe(
          map(data =>
            HomuUsersHosusActions.loadSuccess({
              data
            })
          ),
          rethrowError(() => HomuUsersHosusActions.loadError())
        )
      )
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersHosusActions.search),
      switchMap(() => of(HomuUsersHosusActions.searchConfirmed()))
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersHosusActions.searchConfirmed),
      switchMap(() => this.getHosusQueryAndHomuUser()),
      switchMap(([query, homuUser]) =>
        this.dataService.find(homuUser.id, query).pipe(
          map(result =>
            HomuUsersHosusActions.searchSuccess({
              data: result
            })
          ),
          rethrowError(() => HomuUsersHosusActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HomuUsersHosusActions.download),
      switchMap(() => of(HomuUsersHosusActions.openDownload()))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(HomuUsersHosusActions.openDownload),
        switchMap(() => this.getHosusQueryAndHomuUser()),
        switchMap(([downloadQuery, homuUser]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery,
              downloadBody: homuUser.id
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: HomuUsersHosusService,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {}

  private getHosusQueryAndHomuUser() {
    return combineLatest([
      this.store.pipe(select(fromHomuUsers.getHosusQuery)),
      this.store.pipe(select(fromHomuUsers.getProfileHomuUser))
    ]).pipe(first());
  }
}
