import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { HomuUsersService } from '../../services/homu-users.service';
import { HomuUserProfileActions } from '../actions';
import { HomuUserProfileEffects } from './homu-user-profile.effects';

describe('HomuUserProfileEffects', () => {
  let effects: HomuUserProfileEffects;
  let actions$: Observable<Action>;

  const homuUsersServiceSpy: SpyObject<HomuUsersService> = createSpyObject(HomuUsersService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockActions(() => actions$),
        HomuUserProfileEffects,
        { provide: HomuUsersService, useValue: homuUsersServiceSpy }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(HomuUserProfileEffects);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  it('should request Homu User and trigger requestHomuUserSuccess action when requestHomuUser action is triggered', fakeAsync(() => {
    let dispatchedAction: Action;
    actions$ = of(HomuUserProfileActions.requestHomuUser);

    homuUsersServiceSpy.getHomuById.and.returnValue(of({}));
    effects.$requestSubUser.subscribe(action => (dispatchedAction = action));
    tick();

    expect(dispatchedAction.type).toEqual(HomuUserProfileActions.requestHomuUserSuccess.type);
  }));
});
