import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';
import { DialogService } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { HomuUsersService } from '../../services/homu-users.service';
import { HomuUsersActions } from '../actions';
import * as fromHomuUsers from '../reducers';
import { HomuUsersListEffects } from './homu-users-list.effects';

describe('HomuUsersListEffects', () => {
  let effects: HomuUsersListEffects;
  let actions$: Observable<Action>;

  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const homuUsersServiceSpy: SpyObject<HomuUsersService> = createSpyObject(HomuUsersService);

  const initialState = {
    auth: {
      user: null
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      }
    },
    acdm: {}
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockActions(() => actions$),
        HomuUsersListEffects,
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromHomuUsers.getQuery, value: defaultQuery },
            { selector: fromHomuUsers.getDownloadQuery, value: defaultQuery }
          ]
        }),
        {
          provide: HomuUsersService,
          useValue: homuUsersServiceSpy
        },
        {
          provide: DialogService,
          useValue: dialogServiceSpy
        }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(HomuUsersListEffects);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('Load Homu Users List Effects', () => {
    it('should load homu users list and dispatch loadSuccess action when load effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersActions.load);

      homuUsersServiceSpy.find.and.returnValue(of({}));
      effects.load$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersActions.loadSuccess.type);
    }));
  });

  describe('Search Homu Users List Effects', () => {
    it('should search homu users list and dispatch searchConfirmed action when search effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersActions.search);

      effects.search$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction).toEqual(HomuUsersActions.searchConfirmed());
    }));

    it('should confirm the search and trigger search success action when searchConfirmed action is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersActions.searchConfirmed);

      homuUsersServiceSpy.find.and.returnValue(of({}));
      effects.searchConfirmed$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersActions.searchSuccess.type);
    }));
  });

  describe('Download Homu Users List Effects', () => {
    it('should download homu users list and dispatch openDownload action when download effect is triggered', fakeAsync(() => {
      let dispatchedAction: Action;
      actions$ = of(HomuUsersActions.download);

      effects.download$.subscribe(action => (dispatchedAction = action));
      tick();

      expect(dispatchedAction.type).toEqual(HomuUsersActions.openDownload.type);
    }));

    it('should confirm download and open method from dialog service when downloadConfirmed action is triggered', fakeAsync(() => {
      actions$ = of(HomuUsersActions.openDownload);
      dialogServiceSpy.open.and.returnValue(of({}));

      effects.downloadConfirmed$.subscribe();
      tick();

      expect(dialogServiceSpy.open).toHaveBeenCalled();
    }));
  });
});
