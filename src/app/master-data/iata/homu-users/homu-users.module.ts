import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { HomuUserPermissionsComponent } from './homu-user-permissions/homu-user-permissions.component';
import { HomuUserProfileComponent } from './homu-user-profile/homu-user-profile.component';
import { HomuUserPropertiesComponent } from './homu-user-properties/homu-user-properties.component';
import { HomuUsersHosusListComponent } from './homu-users-hosus-list/homu-users-hosus-list.component';
import { HomuUsersListComponent } from './homu-users-list/homu-users-list.component';
import { HomuUserPermissionsChangesService } from './services/homu-user-permissions-changes.service';
import { HomuUserProfileResolver } from './services/homu-user-profile.resolver';
import { HomuUsersFilterFormatter } from './services/homu-users-filter-formatter';
import { HomuUsersHosusFilterFormatter } from './services/homu-users-hosus-filter-formatter';
import { HomuUsersPermissionsService } from './services/homu-users-permissions.service';
import { HomuUsersHosusService } from './services/homu-users.hosus.service';
import { HomuUsersService } from './services/homu-users.service';
import { HomuUserPermissionsEffects } from './store/effects/homu-user-permissions.effects';
import { HomuUserProfileEffects } from './store/effects/homu-user-profile.effects';
import { HomuUsersHosusListEffects } from './store/effects/homu-users-hosus-list.effects';
import { HomuUsersListEffects } from './store/effects/homu-users-list.effects';
import * as fromHomuUsers from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [
    HomuUsersListComponent,
    HomuUserProfileComponent,
    HomuUsersHosusListComponent,
    HomuUserPropertiesComponent,
    HomuUserPermissionsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    StoreModule.forFeature(fromHomuUsers.homuUsersFeatureKey, fromHomuUsers.reducers),
    EffectsModule.forFeature([
      HomuUsersListEffects,
      HomuUserProfileEffects,
      HomuUsersHosusListEffects,
      HomuUserPermissionsEffects
    ])
  ],
  exports: [HomuUsersListComponent],
  providers: [
    HomuUsersService,
    HomuUsersFilterFormatter,
    HomuUserProfileResolver,
    HomuUsersHosusService,
    HomuUsersHosusFilterFormatter,
    HomuUsersPermissionsService,
    HomuUserPermissionsChangesService
  ]
})
export class HomuUsersModule {}
