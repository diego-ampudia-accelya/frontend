/* eslint-disable @typescript-eslint/naming-convention */
import { UserPermission, UserPermissionDto } from '~app/shared/models/user-permission.model';

export enum HomuUserTemplateForPermissisons {
  Basic = 'BASIC',
  Enhanced = 'ENHANCED'
}

export type HomuUserPermission = UserPermission<HomuUserTemplateForPermissisons>;
export type HomuUserPermissionDto = UserPermissionDto<HomuUserTemplateForPermissisons>;
