import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { UserTemplate } from '~app/shared/models/user-template.model';

//Homus
export interface HomuUserBE {
  id: number;
  portalEmail: string;
  level: number;
  name: string;
  userCode: string;
  registerDate: Date;
  expiryDate: Date;
  numberOfUsers: number;
  status: ActiveInactiveInUpperCase;
  contactInfo: {
    organization: string;
    telephone: string;
    country: string;
    city: string;
    address: string;
    postCode: string;
    email: string;
  };
  template: UserTemplate;
}

export interface HomuUserViewModel {
  portalEmail: string;
  name: string;
  userCode: string;
  registerDate: Date;
  expiryDate: Date;
  status: ActiveInactiveInUpperCase;
}

export interface HomuUserFilter {
  portalEmail: string;
  name: string;
  userCode: string;
  registerDate: Date[];
  expiryDate: Date[];
  status: ActiveInactiveInUpperCase;
}

export interface HomuUserFilterBE {
  userCode: string;
  portalEmail: string;
  name: string;
  fromRegisterDate: string;
  toRegisterDate: string;
  fromExpiryDate: string;
  toExpiryDate: string;
  status: ActiveInactiveInUpperCase;
}

//Properties
export interface HomuUserPropertiesViewModel {
  portalEmail: string;
  name: string;
  organization: string;
  telephone: string;
  country: string;
  city: string;
  address: string;
  postCode: string;
}

//Hosus
export interface HomuUserHosusBE extends HomuUserBE {
  mainUser: boolean;
}
export interface HomuUserHosuViewModel extends HomuUserViewModel {
  mainUser: boolean;
}

export interface HomuUserHosusFilter extends HomuUserFilter {
  mainUser: boolean;
}

export interface HomuUserSavableTabs {
  url: string;
  canApplyChangesSelector?: any;
  applyChangesAction?: any;
  applyChangesPermission?: string;
}
