import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { PermissionsService } from '~app/auth/services/permissions.service';
import {
  GridTableComponent,
  InputComponent,
  PropertyComponent,
  PropertyListComponent,
  SelectComponent
} from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { ListViewComponent } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { TranslatePipeMock } from '~app/test';
import { HomuUserBE } from '../models/homu-users.model';
import { HomuUsersFilterFormatter } from '../services/homu-users-filter-formatter';
import { HomuUsersActions } from '../store/actions';
import * as fromHomuUsers from '../store/reducers';
import { HomuUsersListComponent } from './homu-users-list.component';

describe('HomuUsersListComponent', () => {
  let component: HomuUsersListComponent;
  let fixture: ComponentFixture<HomuUsersListComponent>;
  let router = createSpyObject(Router);
  let mockStore = createSpyObject(Store);

  const mockUser: HomuUserBE = {
    id: 1,
    portalEmail: 'test@example.com',
    level: 2,
    name: 'Name 1',
    userCode: 'JD123',
    registerDate: new Date('2022-01-15'),
    expiryDate: new Date('2023-01-15'),
    numberOfUsers: 5,
    status: null,
    contactInfo: {
      organization: 'XYZ Inc.',
      telephone: '+1234567890',
      country: 'USA',
      city: 'New York',
      address: '123 Main St',
      postCode: '10001',
      email: 'test@example.com'
    },
    template: null
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        HomuUsersListComponent,
        MockComponents(
          ListViewComponent,
          SelectComponent,
          InputComponent,
          DatepickerComponent,
          GridTableComponent,
          PropertyListComponent,
          PropertyComponent
        ),
        TranslatePipeMock
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [RouterTestingModule],
      providers: [
        FormBuilder,
        PermissionsService,
        { provide: HomuUsersFilterFormatter, useValue: {} },
        { provide: L10nTranslationService, useValue: { translate: () => '' } },
        provideMockStore({
          selectors: [
            { selector: fromHomuUsers.getLoading, value: false },
            { selector: fromHomuUsers.getData, value: [] },
            { selector: fromHomuUsers.getQuery, value: defaultQuery },
            { selector: fromHomuUsers.hasData, value: true }
          ]
        })
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject<any>(Router);
    mockStore = TestBed.inject<any>(Store);
    fixture = TestBed.createComponent(HomuUsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to the list of homu users when onActionClick method is triggered with "listOfHOSUs" value parameter', () => {
    const navigateSpy = spyOn(router, 'navigate');
    component.onActionClick({
      action: { actionType: GridTableActionType.ListOfHOSUs, methodName: 'method' },
      row: mockUser
    });
    expect(navigateSpy).toHaveBeenCalledWith(['/master-data/iata/homu-users/details', mockUser.id, 'hosus-list']);
  });

  it('should navigate to the properties homu users when onActionClick method is triggered with "propertiesHOMU" value parameter', () => {
    const navigateSpy = spyOn(router, 'navigate');
    component.onActionClick({
      action: { actionType: GridTableActionType.propertiesHOMU, methodName: 'method' },
      row: mockUser
    });
    expect(navigateSpy).toHaveBeenCalledWith(['/master-data/iata/homu-users/details', mockUser.id]);
  });

  it('should navigate to permission and template of homu users when onActionClick method is triggered with "permissionAndTemplate" value parameter', () => {
    const navigateSpy = spyOn(router, 'navigate');
    component.onActionClick({
      action: { actionType: GridTableActionType.PermissionAndTemplate, methodName: 'method' },
      row: mockUser
    });
    expect(navigateSpy).toHaveBeenCalledWith(['/master-data/iata/homu-users/details', mockUser.id, 'permissions']);
  });

  it('should navigate to homu users details when onRowLinkClick is triggered and row status is active', () => {
    const navigateSpy = spyOn(router, 'navigate');
    mockUser.status = ActiveInactiveInUpperCase.Active;
    component.onRowLinkClick(mockUser);
    expect(navigateSpy).toHaveBeenCalledWith(['/master-data/iata/homu-users/details', mockUser.id]);
  });

  it('should not navigate to homu users details when onRowLinkClick is triggered and row status is Inactive', () => {
    const navigateSpy = spyOn(router, 'navigate');
    mockUser.status = ActiveInactiveInUpperCase.Inactive;
    component.onRowLinkClick(mockUser);
    expect(navigateSpy).not.toHaveBeenCalledWith(['/master-data/iata/homu-users/details', mockUser.id]);
  });

  it('should dispatch search action when onQueryChanged method is triggered', () => {
    spyOn(mockStore, 'dispatch');
    component.onQueryChanged(defaultQuery);

    expect(mockStore.dispatch).toHaveBeenCalledWith({
      query: defaultQuery,
      type: HomuUsersActions.search.type
    });
  });

  it('should dispatch download action when onDownload method is triggered', () => {
    spyOn(mockStore, 'dispatch');
    component.onDownload();

    expect(mockStore.dispatch).toHaveBeenCalledWith({
      type: HomuUsersActions.download.type
    });
  });

  it('should populate actions when onActionMenuClick method is triggered', () => {
    component.onActionMenuClick(mockUser);
    expect(component.actions).toEqual([
      { action: GridTableActionType.ListOfHOSUs, disabled: true },
      { action: GridTableActionType.propertiesHOMU, disabled: true },
      { action: GridTableActionType.PermissionAndTemplate, disabled: true }
    ]);
  });
});
