import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';

import { HomuUserBE, HomuUserViewModel } from '../models/homu-users.model';
import { HomuUsersFilterFormatter } from '../services/homu-users-filter-formatter';
import { HomuUsersActions } from '../store/actions';
import * as fromHomuUsers from '../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { Permissions, ROUTES } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { ActiveInactiveInUpperCase, DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-homu-users-list',
  templateUrl: './homu-users-list.component.html',
  providers: [DatePipe]
})
export class HomuUsersListComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];

  public loading$ = this.store.pipe(select(fromHomuUsers.getLoading));
  public query$ = this.store.pipe(select(fromHomuUsers.getQuery));
  public hasData$ = this.store.pipe(select(fromHomuUsers.hasData));
  public data$ = this.store.pipe(select(fromHomuUsers.getData));

  public actions: Array<{ action: GridTableActionType; disabled: boolean }>;

  public statusOptions: DropdownOption<typeof ActiveInactiveInUpperCase>[] = this.getDropdownOptions(
    ActiveInactiveInUpperCase,
    'common'
  );

  private formFactory: FormUtil;

  constructor(
    public displayFormatter: HomuUsersFilterFormatter,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private store: Store<AppState>,
    private router: Router,
    private permissionsService: PermissionsService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.initializeDataQuery();
    this.initColumns();
  }

  public onQueryChanged(query): void {
    this.store.dispatch(HomuUsersActions.search({ query }));
  }

  public onDownload(): void {
    this.store.dispatch(HomuUsersActions.download());
  }

  public onActionMenuClick(row: HomuUserBE): void {
    const hasListOfHosuPermission = this.permissionsService.hasPermission(Permissions.readHomu);
    const hasReadHosuPermission = this.permissionsService.hasPermission(Permissions.readHomuPerm);
    const isActiveFile = row.status === ActiveInactiveInUpperCase.Active;

    this.actions = [
      { action: GridTableActionType.ListOfHOSUs, disabled: !isActiveFile || !hasListOfHosuPermission },
      { action: GridTableActionType.propertiesHOMU, disabled: !isActiveFile || !hasListOfHosuPermission },
      { action: GridTableActionType.PermissionAndTemplate, disabled: !hasReadHosuPermission }
    ];
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: HomuUserBE }): void {
    switch (action.actionType) {
      case GridTableActionType.ListOfHOSUs:
        this.router.navigate([ROUTES.HOMU_USERS_PROFILE.url, row.id, 'hosus-list']);
        break;
      case GridTableActionType.propertiesHOMU:
        this.router.navigate([ROUTES.HOMU_USERS_PROFILE.url, row.id]);
        break;
      case GridTableActionType.PermissionAndTemplate:
        this.router.navigate([ROUTES.HOMU_USERS_PROFILE.url, row.id, 'permissions']);
        break;
    }
  }

  public onRowLinkClick(row: any) {
    if (row.status === ActiveInactiveInUpperCase.Active) {
      this.router.navigate([ROUTES.HOMU_USERS_PROFILE.url, row.id]);
    }
  }

  private initializeDataQuery(): void {
    this.store.dispatch(HomuUsersActions.load());
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<HomuUserViewModel>({
      portalEmail: [],
      name: [],
      userCode: [],
      registerDate: [],
      expiryDate: [],
      status: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.userCode',
        prop: 'userCode'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.portalEmail',
        prop: 'portalEmail',
        cellTemplate: 'linkCellTmplWithObjDisabled'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.name',
        prop: 'name'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.registerDate',
        prop: 'registerDate',
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.expiryDate',
        prop: 'expiryDate',
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.numberOfUsers',
        prop: 'numberOfUsers'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.HOMU_USERS.columns.status',
        prop: 'status',
        pipe: {
          transform: value => (value ? this.translationService.translate(`common.${value}`) : value)
        }
      }
    ];
  }

  private getDropdownOptions<T>(object: T, translationKey: string): DropdownOption<T>[] {
    return Object.keys(object).map(key => ({
      value: object[key],
      label: this.translationService.translate(`${translationKey}.${key}`)
    }));
  }
}
