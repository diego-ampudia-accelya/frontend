import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { HomuUserBE, HomuUserPropertiesViewModel } from '../models/homu-users.model';
import * as fromHomuUsers from '../store/reducers';
import { AppState } from '~app/reducers';
import { FormUtil } from '~app/shared/helpers';

@Component({
  selector: 'bspl-homu-user-properties',
  templateUrl: './homu-user-properties.component.html',
  styleUrls: ['./homu-user-properties.component.scss']
})
export class HomuUserPropertiesComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromHomuUsers.getProfileHomuLoading));
  public form: FormGroup;
  public user: HomuUserBE;

  private user$ = this.store.pipe(select(fromHomuUsers.getProfileHomuUser));
  private formUtil: FormUtil;
  private destroy$ = new Subject();

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeForm();
    this.initializeUser();
  }

  private initializeForm(): void {
    this.form = this.formUtil.createGroup<HomuUserPropertiesViewModel>({
      portalEmail: ['', [Validators.required, Validators.maxLength(200)]],
      name: ['', [Validators.required, Validators.maxLength(49)]],
      organization: ['', [Validators.required, Validators.maxLength(30)]],
      telephone: ['', [Validators.required, Validators.maxLength(15)]],
      country: ['', [Validators.required, Validators.maxLength(26)]],
      city: ['', [Validators.required, Validators.maxLength(30)]],
      address: ['', [Validators.required, Validators.maxLength(30)]],
      postCode: ['', [Validators.required, Validators.maxLength(10)]]
    });
  }

  private initializeUser(): void {
    this.user$.pipe(takeUntil(this.destroy$)).subscribe((user: HomuUserBE) => {
      this.user = user;
      this.form.reset();

      if (user) {
        this.form.patchValue(this.transformToFormValue(user));
      }
    });
  }

  private transformToFormValue(user: HomuUserBE): HomuUserPropertiesViewModel {
    return {
      portalEmail: user.portalEmail,
      name: user.name,
      organization: user.contactInfo?.organization,
      telephone: user.contactInfo?.telephone,
      country: user.contactInfo?.country,
      city: user.contactInfo?.city,
      address: user.contactInfo?.address,
      postCode: user.contactInfo?.postCode
    };
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
