import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents, MockPipe } from 'ng-mocks';

import * as fromHomuUsers from '../store/reducers';
import { HomuUserPropertiesComponent } from './homu-user-properties.component';
import { InputComponent, PropertyComponent, PropertyListComponent, SpinnerComponent } from '~app/shared/components';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { TranslatePipeMock } from '~app/test';

describe('HomuUserPropertiesComponent', () => {
  let component: HomuUserPropertiesComponent;
  let fixture: ComponentFixture<HomuUserPropertiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        HomuUserPropertiesComponent,
        MockComponents(
          SpinnerComponent,
          AccordionComponent,
          AccordionItemComponent,
          PropertyListComponent,
          PropertyComponent,
          InputComponent
        ),
        MockPipe(DateTimeFormatPipe),
        TranslatePipeMock
      ],
      providers: [
        FormBuilder,
        provideMockStore({
          initialState: {},
          selectors: [
            { selector: fromHomuUsers.getProfileHomuUser, value: {} },
            { selector: fromHomuUsers.getProfileHomuLoading, value: false }
          ]
        })
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomuUserPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
