import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { HomuUsersPermissionsActions } from '~app/master-data/iata/homu-users/store/actions';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { AppState } from '~app/reducers';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { State } from '../../main-users/store/reducers/main-user-permissions.reducer';
import { HomuUserPermissionsChangesService } from '../services/homu-user-permissions-changes.service';
import { HomuUserPermissionsComponent } from './homu-user-permissions.component';

describe('HomuUserPermissionsComponent', () => {
  let component: HomuUserPermissionsComponent;
  let fixture: ComponentFixture<HomuUserPermissionsComponent>;
  let mockStore: MockStore<AppState>;
  const translationServiceSpy: SpyObject<L10nTranslationService> = createSpyObject(L10nTranslationService);

  const initialState: State = {
    userId: null,
    original: [],
    modified: [],
    loading: false,
    bspId: null
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HomuUserPermissionsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromHomuUsers.canApplyPermissionsChanges, value: false },
            { selector: fromHomuUsers.getHomuUserPermissionsLoading, value: true },
            { selector: fromHomuUsers.getOriginalHomuUserPermissions, value: [] }
          ]
        }),
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        {
          provide: HomuUserPermissionsChangesService,
          useValue: {
            handlePermissionsChange$: of(true),
            handlePermissionsChangeAndDeactivate$: of(false)
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    mockStore = TestBed.inject<any>(Store);
    fixture = TestBed.createComponent(HomuUserPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('onPermissionChange', () => {
    it('should dispatch change action from HomuUsersPermissionsActions when onPermissionChange is triggered with the modifications', () => {
      const mockModifications: SelectionList[] = [
        {
          name: 'mod',
          disabled: true,
          items: [
            {
              name: 'item 1',
              checked: true,
              hasChanged: false,
              disabled: false
            }
          ]
        }
      ];
      spyOn(mockStore, 'dispatch');
      component.onPermissionChange(mockModifications);

      expect(mockStore.dispatch).toHaveBeenCalledWith({
        modifications: mockModifications,
        type: HomuUsersPermissionsActions.change.type
      });
    });
  });

  describe('canDeactivate', () => {
    it('should allow to deactivate when the canDeactivate method returns true', fakeAsync(() => {
      component.canDeactivate().subscribe(result => {
        expect(result).toBeTruthy();
      });
      tick();
    }));

    it('should not allow to deactivate when the canDeactivate method returns false', fakeAsync(() => {
      mockStore.overrideSelector(fromHomuUsers.canApplyPermissionsChanges, true);
      component.canDeactivate().subscribe(result => {
        expect(result).toBeFalsy();
      });
      tick();
    }));
  });
});
