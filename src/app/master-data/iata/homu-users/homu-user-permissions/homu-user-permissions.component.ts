import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { HomuUserPermissionsChangesService } from '../services/homu-user-permissions-changes.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { HomuUsersPermissionsActions } from '~app/master-data/iata/homu-users/store/actions';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { AppState } from '~app/reducers';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { Permissions } from '~app/shared/constants/permissions';

@Component({
  selector: 'bspl-homu-user-permissions',
  templateUrl: './homu-user-permissions.component.html',
  styleUrls: ['./homu-user-permissions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomuUserPermissionsComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromHomuUsers.getHomuUserPermissionsLoading));
  public permissions$ = this.store.pipe(select(fromHomuUsers.getOriginalHomuUserPermissions), map(cloneDeep));
  public canEditPermissions = this.permissionsService.hasPermission(Permissions.updateHomuPermissions);
  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private changesService: HomuUserPermissionsChangesService
  ) {}

  public ngOnInit(): void {
    this.requestHomuUserPermissions();
  }

  public onPermissionChange(modifications: SelectionList[]) {
    this.store.dispatch(HomuUsersPermissionsActions.change({ modifications: cloneDeep(modifications) }));
  }

  public canDeactivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromHomuUsers.canApplyPermissionsChanges),
      switchMap(hasChanges => (hasChanges ? this.changesService.handlePermissionsChangeAndDeactivate$ : of(true))),
      first()
    );
  }

  private requestHomuUserPermissions(): void {
    this.store.dispatch(HomuUsersPermissionsActions.requestHomuUserPermissions());
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
