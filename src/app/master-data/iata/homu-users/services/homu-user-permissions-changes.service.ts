import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';

import { HomuUsersPermissionsActions } from '../store/actions';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { AppState } from '~app/reducers';
import { ButtonDesign, ChangeModel, ChangesDialogComponent, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class HomuUserPermissionsChangesService {
  private changedPermissions$: Observable<ChangeModel[]> = this.store.pipe(
    select(fromHomuUsers.getPermissionsChanges),
    map(changes =>
      changes.map((change: ChangeModel) => ({
        ...change,
        value: this.permissionsStatusTextMap.get(change.value.toString())
      }))
    ),
    take(1)
  );
  private loading$ = this.store.pipe(select(fromHomuUsers.getHomuUserPermissionsLoading));
  private handlePermissionsChange$: Observable<boolean> = this.handlePermissionsChange('unsavedChanges').pipe(
    tap(action => this.store.dispatch(action)),
    map(
      ({ type }) =>
        type === HomuUsersPermissionsActions.applyChanges.type ||
        type === HomuUsersPermissionsActions.discardChanges.type
    )
  );

  public applyUserPermissions$: Observable<Action> = this.handlePermissionsChange();

  public handlePermissionsChangeAndDeactivate$ = combineLatest([this.handlePermissionsChange$, this.loading$]).pipe(
    filter(([_, loading]) => !loading),
    map(([shouldProceed]) => shouldProceed)
  );

  private permissionsStatusTextMap = new Map([
    [
      'true',
      this.translationService.translate('MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.checkboxStatusText.true')
    ],
    [
      'false',
      this.translationService.translate('MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.checkboxStatusText.false')
    ]
  ]);

  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {}

  private handlePermissionsChange(
    dialogConfigType: 'applyChanges' | 'unsavedChanges' = 'applyChanges'
  ): Observable<Action> {
    const dialogConfigs = {
      applyChanges: {
        data: {
          title: 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.changesDialog.title',
          footerButtonsType: [{ type: FooterButton.Apply }]
        },
        message: 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.changesDialog.descriptionPermissionsApply',
        changes: []
      },
      unsavedChanges: {
        data: {
          title: 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.changesDialog.titleUnsaved',
          footerButtonsType: [
            { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
            { type: FooterButton.Apply }
          ]
        },
        message: 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.changesDialog.descriptionUnsavedPermissions',
        changes: []
      }
    };

    return this.changedPermissions$.pipe(
      switchMap(changes =>
        this.dialogService.open(ChangesDialogComponent, { ...dialogConfigs[dialogConfigType], changes })
      ),
      filter(action => action?.clickedBtn && action.contentComponentRef),
      tap(({ contentComponentRef }) => contentComponentRef.config?.serviceRef?.close()),
      map(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Apply:
            return HomuUsersPermissionsActions.applyChanges();
          case FooterButton.Discard:
            return HomuUsersPermissionsActions.discardChanges();
          default:
            return HomuUsersPermissionsActions.cancel();
        }
      })
    );
  }
}
