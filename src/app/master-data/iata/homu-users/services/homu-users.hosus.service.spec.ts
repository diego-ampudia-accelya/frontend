import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ActiveInactiveInUpperCase, DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { HomuUserHosusFilter } from '../models/homu-users.model';
import { HomuUsersHosusService } from './homu-users.hosus.service';

describe('HomuUsersHosusService', () => {
  let service: HomuUsersHosusService;
  let httpClientSpy: SpyObject<HttpClient>;

  const hosuUsersRecordsMock: HomuUserHosusFilter[] = [
    {
      portalEmail: 'a@b',
      name: 'responsible',
      userCode: '015',
      registerDate: [new Date('2013-08-05')],
      expiryDate: [new Date('2013-08-05')],
      status: ActiveInactiveInUpperCase.Active,
      mainUser: true
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const baseurl = '/user-management/homus/';

  beforeEach(() => {
    httpClientSpy = createSpyObject(HttpClient);
    TestBed.configureTestingModule({
      providers: [
        HomuUsersHosusService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: HttpClient, useValue: httpClientSpy }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HomuUsersHosusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should trigger get HTTP method with the right endpoint url and return correct page data', fakeAsync(() => {
    const mockId = 15;
    const hosuUsersDataMock: PagedData<HomuUserHosusFilter> = {
      records: hosuUsersRecordsMock,
      total: hosuUsersRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(hosuUsersDataMock));
    service.find(mockId, query).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(`${baseurl}${mockId}/users?page=0&size=20`);
  }));

  it('should send proper request when download method is triggered', fakeAsync(() => {
    const mockId = 15;
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = `${baseurl}${mockId}/users/download?exportAs=txt`;
    httpClientSpy.get.and.returnValue(of([]));
    service.download(query, exportOptions, 15).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));

  it('should properly format all the dates when formatQuery method is triggered with the provided data query dates', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const mockDate = [new Date('2013-10-10')];
    const dataQuery: DataQuery<HomuUserHosusFilter> = {
      ...queryCloneDeep,
      filterBy: {
        registerDate: mockDate,
        expiryDate: mockDate
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);
    expect(formattedQuery.filterBy['fromRegisterDate']).toEqual('2013-10-10');
    expect(formattedQuery.filterBy['toRegisterDate']).toEqual('2013-10-10');
    expect(formattedQuery.filterBy['fromExpiryDate']).toEqual('2013-10-10');
    expect(formattedQuery.filterBy['toExpiryDate']).toEqual('2013-10-10');
  });
});
