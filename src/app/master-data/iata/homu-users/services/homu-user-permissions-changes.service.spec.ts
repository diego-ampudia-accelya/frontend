import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { AppState } from '~app/reducers';
import { DialogService, FooterButton } from '~app/shared/components';
import { HomuUsersPermissionsActions } from '../store/actions';
import { HomuUserPermissionsChangesService } from './homu-user-permissions-changes.service';

describe('HomuUserPermissionsChangesService', () => {
  let service: HomuUserPermissionsChangesService;
  let mockStore: MockStore<AppState>;
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HomuUserPermissionsChangesService,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState: {},
          selectors: [
            {
              selector: fromHomuUsers.getPermissionsChanges,
              value: [
                { group: 'CONFIGURACIÓN BÁSICA', name: 'Autorización a Terceros', value: 'false', originalValue: false }
              ]
            },
            {
              selector: fromHomuUsers.getHomuUserPermissionsState,
              value: []
            }
          ]
        }),
        {
          provide: DialogService,
          useValue: {
            open: () =>
              of({ clickedBtn: 'Apply', contentComponentRef: { config: { serviceRef: { close: () => {} } } } })
          }
        },
        { provide: L10nTranslationService, useValue: { translate: () => '' } }
      ]
    });
    service = TestBed.inject(HomuUserPermissionsChangesService);

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('handlePermissionsChange', () => {
    const closeFn = jasmine.createSpy();
    const action = {
      clickedBtn: FooterButton.Apply,
      contentComponentRef: { config: { serviceRef: { close: closeFn } } }
    };

    beforeEach(() => {
      (service as any).dialogService.open = jasmine.createSpy().and.returnValue(of(action));
      (service as any).handlePermissionsChange$ = jasmine.createSpy().and.returnValue(of(true));
    });

    it('should call dialogService.open() when it subscribes to handlePermissionsChange', fakeAsync(() => {
      (service as any).handlePermissionsChange().subscribe();
      tick();

      expect((service as any).dialogService.open).toHaveBeenCalled();
      expect((service as any).dialogService.open.calls.first().args[1]).toEqual({
        data: {
          title: 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.changesDialog.title',
          footerButtonsType: [{ type: FooterButton.Apply }]
        },
        message: 'MENU.USERS_MAINTENANCE.HOMU_USERS.permissions.changesDialog.descriptionPermissionsApply',
        changes: [{ group: 'CONFIGURACIÓN BÁSICA', name: 'Autorización a Terceros', value: '', originalValue: false }]
      });
    }));

    it('should call close() function from config when it subscribes to handlePermissionsChange', fakeAsync(() => {
      (service as any).handlePermissionsChange().subscribe();
      tick();

      expect(closeFn).toHaveBeenCalled();
    }));

    it('should return HomuUsersPermissionsActions.applyChanges() action when it subscribes to handlePermissionsChange', fakeAsync(() => {
      let res;

      (service as any).handlePermissionsChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(HomuUsersPermissionsActions.applyChanges());
    }));

    it('should return HomuUsersPermissionsActions.discardChanges() action when discard button is clicked and it subscribes to handlePermissionsChange', fakeAsync(() => {
      let res;
      (service as any).dialogService.open = jasmine
        .createSpy()
        .and.returnValue(of({ ...action, clickedBtn: FooterButton.Discard }));

      (service as any).handlePermissionsChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(HomuUsersPermissionsActions.discardChanges());
    }));

    it('should return HomuUsersPermissionsActions.cancel() actio when cancel button is clicked and it subscribes to handlePermissionsChange', fakeAsync(() => {
      let res;
      (service as any).dialogService.open = jasmine
        .createSpy()
        .and.returnValue(of({ ...action, clickedBtn: FooterButton.Cancel }));

      (service as any).handlePermissionsChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(HomuUsersPermissionsActions.cancel());
    }));

    it('should dispatch actions and return boolean when it subscribes to handlePermissionsChangeAndDeactivate$', fakeAsync(() => {
      actions$ = of(action);

      const applyUserPermissionsSpy = spyOn(service.applyUserPermissions$, 'subscribe');
      applyUserPermissionsSpy.and.returnValue(of(action));

      service.handlePermissionsChangeAndDeactivate$.subscribe(shouldProceed => {
        expect(shouldProceed).toBe(true);
      });
      tick();
    }));
  });
});
