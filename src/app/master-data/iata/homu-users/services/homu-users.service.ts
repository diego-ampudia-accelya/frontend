import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HomuUserBE, HomuUserFilter, HomuUserFilterBE } from '../models/homu-users.model';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class HomuUsersService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/homus`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<HomuUserFilter>): Observable<PagedData<HomuUserBE>> {
    const requestQuery = this.formatQuery(query);

    return this.http
      .get<PagedData<HomuUserBE>>(this.baseUrl + requestQuery.getQueryString())
      .pipe(map(this.toDisabledLinksNoActives));
  }

  private toDisabledLinksNoActives(data: PagedData<HomuUserBE>): PagedData<HomuUserBE> {
    return {
      ...data,
      records: data.records.map(file => ({
        ...file,
        disabled: file.status !== 'ACTIVE'
      }))
    };
  }

  public getHomuById(id: number): Observable<HomuUserBE> {
    const url = `${this.baseUrl}/${id}`;

    return this.http.get<HomuUserBE>(url);
  }

  public download(
    query: DataQuery<HomuUserFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<HomuUserFilter>>): RequestQuery<HomuUserFilterBE> {
    const { registerDate, expiryDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromRegisterDate: registerDate && toShortIsoDate(registerDate[0]),
        toRegisterDate: registerDate && toShortIsoDate(registerDate[1] || registerDate[0]),
        fromExpiryDate: expiryDate && toShortIsoDate(expiryDate[0]),
        toExpiryDate: expiryDate && toShortIsoDate(expiryDate[1] || expiryDate[0])
      }
    });
  }
}
