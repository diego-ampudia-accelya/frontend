import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { merge, Observable } from 'rxjs';
import { filter, first, tap } from 'rxjs/operators';

import { HomuUserBE } from '../models/homu-users.model';
import { HomuUserProfileActions } from '~app/master-data/iata/homu-users/store/actions';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { AppState } from '~app/reducers';

@Injectable({
  providedIn: 'root'
})
export class HomuUserProfileResolver implements Resolve<HomuUserBE> {
  constructor(private store: Store<AppState>, private router: Router) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<HomuUserBE> {
    this.requestHomuUser(route);

    const homuUser$ = this.store.pipe(select(fromHomuUsers.getProfileHomuUser), filter(Boolean));
    const homuUserError$ = this.store.pipe(
      select(fromHomuUsers.getProfileHomuUserRequestError),
      filter(Boolean),
      tap(() => this.handleNavigationOnError())
    );

    return merge(homuUserError$, homuUser$).pipe(first()) as Observable<HomuUserBE>;
  }

  private requestHomuUser(route: ActivatedRouteSnapshot) {
    const id = Number(route.paramMap.get('id'));

    if (id) {
      this.store.dispatch(HomuUserProfileActions.requestHomuUser({ id }));
    }
  }

  private handleNavigationOnError() {
    const lastSuccessfulNavigation = this.router.getCurrentNavigation().previousNavigation;
    if (lastSuccessfulNavigation != null) {
      this.router.navigateByUrl(lastSuccessfulNavigation.finalUrl.toString());
    } else {
      this.router.navigate(['./']);
    }
  }
}
