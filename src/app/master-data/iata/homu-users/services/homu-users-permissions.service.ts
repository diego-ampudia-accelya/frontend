import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HomuUserPermission, HomuUserPermissionDto } from '../models/homu-users-permissions.model';
import { UserPermissionForUpdate } from '~app/shared/models/user-permission.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class HomuUsersPermissionsService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/homus`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public getHomuPermissionsById(id: number): Observable<HomuUserPermission[]> {
    const url = `${this.baseUrl}/${id}/permissions`;

    return this.http.get<HomuUserPermissionDto[]>(url).pipe(map(this.toUserPermissionsFromDto));
  }

  public updatePermissionsByUserId(id: number, permissions: HomuUserPermission[]): Observable<HomuUserPermission[]> {
    const data: UserPermissionForUpdate[] = permissions.map(p => ({ id: p.id, enabled: p.enabled }));

    return this.http
      .put<HomuUserPermissionDto[]>(`${this.baseUrl}/${id}/permissions`, data)
      .pipe(map(this.toUserPermissionsFromDto));
  }

  private toUserPermissionsFromDto = (dtos: HomuUserPermissionDto[]): HomuUserPermission[] =>
    dtos.map(({ catgory, ...p }) => ({ ...p, category: catgory }));
}
