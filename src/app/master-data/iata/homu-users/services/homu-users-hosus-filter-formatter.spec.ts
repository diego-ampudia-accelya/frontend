import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { HomuUserHosusFilter } from '../models/homu-users.model';
import { HomuUsersHosusFilterFormatter } from './homu-users-hosus-filter-formatter';

const filters = {
  name: 'name1',
  userCode: 'JD123',
  portalEmail: 'email@example.com',
  status: ActiveInactiveInUpperCase.Inactive,
  registerDate: [new Date('2023-01-15'), new Date('2023-02-20')],
  expiryDate: [new Date('2023-12-31'), new Date('2024-06-30')],
  mainUser: true
} as HomuUserHosusFilter;
const translateKey = 'MENU.USERS_MAINTENANCE.HOMU_USERS.listOfHosus.filters';

describe('HomuUsersHosusFilterFormatter', () => {
  let formatter: HomuUsersHosusFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new HomuUsersHosusFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['name'],
        label: `${translateKey}.name.label - ${filters.name}`
      },
      {
        keys: ['userCode'],
        label: `${translateKey}.userCode.label - ${filters.userCode}`
      },
      {
        keys: ['portalEmail'],
        label: `${translateKey}.portalEmail.label - ${filters.portalEmail}`
      },
      {
        keys: ['status'],
        label: `${translateKey}.status.label - common.${filters.status}`
      },
      {
        keys: ['registerDate'],
        label: `${translateKey}.registerDate.label - ${rangeDateFilterTagMapper(filters.registerDate)}`
      },
      {
        keys: ['expiryDate'],
        label: `${translateKey}.expiryDate.label - ${rangeDateFilterTagMapper(filters.expiryDate)}`
      },
      {
        keys: ['mainUser'],
        label: `${translateKey}.mainUser.label - ${formatter.formatLevel(filters.mainUser)}`
      }
    ]);
  });

  it('should format the level depending of the status value when formatLevel method is triggered', () => {
    const formatLevelResult = formatter.formatLevel(false);

    expect(formatLevelResult).toEqual(jasmine.stringMatching('levelOptions.hosu'));
    expect(translationSpy.translate).toHaveBeenCalled();
  });
});
