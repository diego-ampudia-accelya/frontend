import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ActiveInactiveInUpperCase, DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';
import { HomuUserFilter } from '../models/homu-users.model';
import { HomuUsersService } from './homu-users.service';

describe('HomuUsersService', () => {
  let service: HomuUsersService;
  let httpClientSpy: SpyObject<HttpClient>;

  const homuUsersRecordsMock: HomuUserFilter[] = [
    {
      portalEmail: 'a@b',
      name: 'responsible',
      userCode: '015',
      registerDate: [new Date('2013-08-05')],
      expiryDate: [new Date('2013-08-05')],
      status: ActiveInactiveInUpperCase.Active
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    httpClientSpy = createSpyObject(HttpClient);
    TestBed.configureTestingModule({
      providers: [
        HomuUsersService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HomuUsersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should trigger get HTTP method with the right endpoint url and return correct page data', fakeAsync(() => {
    const homuUsersDataMock: PagedData<HomuUserFilter> = {
      records: homuUsersRecordsMock,
      total: homuUsersRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(homuUsersDataMock));
    service.find(query).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/user-management/homus?page=0&size=20');
  }));

  it('should trigger get HTTP method with the right endpoint url and Homu Id', fakeAsync(() => {
    const mockHomuId = 15;
    httpClientSpy.get.and.returnValue(of([]));
    service.getHomuById(mockHomuId).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(`/user-management/homus/${mockHomuId}`);
  }));

  it('should send proper request when download method is triggered', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/user-management/homus/download?exportAs=txt';
    httpClientSpy.get.and.returnValue(of([]));
    service.download(query, exportOptions).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));

  it('should properly format all the dates when formatQuery method is triggered with the provided data query dates', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const mockDate = [new Date('2013-10-10')];
    const dataQuery: DataQuery<HomuUserFilter> = {
      ...queryCloneDeep,
      filterBy: {
        registerDate: mockDate,
        expiryDate: mockDate
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);
    expect(formattedQuery.filterBy['fromRegisterDate']).toEqual('2013-10-10');
    expect(formattedQuery.filterBy['toRegisterDate']).toEqual('2013-10-10');
    expect(formattedQuery.filterBy['fromExpiryDate']).toEqual('2013-10-10');
    expect(formattedQuery.filterBy['toExpiryDate']).toEqual('2013-10-10');
  });
});
