import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import * as fromHomuUsers from '~app/master-data/iata/homu-users/store/reducers';
import { HomuUserProfileActions } from '../store/actions';
import { HomuUserProfileResolver } from './homu-user-profile.resolver';

export const initialState = {
  loading: false,
  homuUser: null,
  homuUserRequestError: null
};

describe('HomuUserProfileResolver', () => {
  let resolver: HomuUserProfileResolver;
  let storeMock: MockStore;
  const routerSpy = createSpyObject(Router);

  let activatedRouteSnapshotMock = {
    paramMap: { get: () => '6005' } as unknown
  } as ActivatedRouteSnapshot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        provideMockStore({
          initialState: {},
          selectors: [
            {
              selector: fromHomuUsers.getHomuUserProfileState,
              value: []
            },
            {
              selector: fromHomuUsers.getProfileHomuUserRequestError,
              value: Boolean(true)
            }
          ]
        }),
        mockProvider(Router),
        { provide: ActivatedRouteSnapshot, useValue: activatedRouteSnapshotMock },
        { provide: Router, useValue: routerSpy }
      ]
    });
    activatedRouteSnapshotMock = TestBed.inject<any>(ActivatedRouteSnapshot);
    resolver = TestBed.inject(HomuUserProfileResolver);
    storeMock = TestBed.inject<any>(Store);
    spyOn(storeMock, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should dispatch and request Homu User when the resolver is triggered', fakeAsync(() => {
    routerSpy.getCurrentNavigation.and.returnValue('/current');
    resolver.resolve(activatedRouteSnapshotMock).subscribe();
    tick();

    expect(storeMock.dispatch).toHaveBeenCalledWith(HomuUserProfileActions.requestHomuUser({ id: 6005 }));
  }));
});
