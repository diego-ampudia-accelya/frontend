import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { PermissionCategoryBE, UserPermission } from '~app/shared/models/user-permission.model';
import { AppConfigurationService } from '~app/shared/services';
import {
  HomuUserPermission,
  HomuUserPermissionDto,
  HomuUserTemplateForPermissisons
} from '../models/homu-users-permissions.model';
import { HomuUsersPermissionsService } from './homu-users-permissions.service';

describe('HomuUsersPermissionsService', () => {
  let service: HomuUsersPermissionsService;
  let httpClientSpy: SpyObject<HttpClient>;

  beforeEach(() => {
    httpClientSpy = createSpyObject(HttpClient);
    TestBed.configureTestingModule({
      providers: [
        HomuUsersPermissionsService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: HttpClient, useValue: httpClientSpy }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HomuUsersPermissionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should trigger get HTTP method with the right endpoint url and Homu Id', fakeAsync(() => {
    const mockHomuId = 15;
    httpClientSpy.get.and.returnValue(of([]));
    service.getHomuPermissionsById(mockHomuId).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(`/user-management/homus/${mockHomuId}/permissions`);
  }));

  it('should trigger put HTTP method with the right endpoint url, id and permissions', fakeAsync(() => {
    const id = 15;
    const mockHomuUserPermissions: HomuUserPermission[] = [
      {
        id: 1,
        code: 'code',
        name: 'permission1',
        category: null,
        enabled: true,
        template: null
      }
    ];
    const expectedResult = [{ id: 1, enabled: true }];
    httpClientSpy.put.and.returnValue(of([]));
    service.updatePermissionsByUserId(id, mockHomuUserPermissions).subscribe();
    tick();

    expect(httpClientSpy.put).toHaveBeenCalledWith(`/user-management/homus/${id}/permissions`, expectedResult);
  }));

  it('should map to user permissions from Dto when toUserPermissionsFromDto method is triggered', () => {
    const mockCategory: PermissionCategoryBE = {
      id: 1,
      code: 'CatA',
      name: 'Category A'
    };
    const mockPermission: UserPermission<HomuUserTemplateForPermissisons> = {
      id: 1,
      code: 'perm_123',
      name: 'Permission 123',
      category: mockCategory,
      enabled: true,
      template: null
    };
    const mockDtos: HomuUserPermissionDto[] = [
      {
        id: mockPermission.id,
        code: mockPermission.code,
        name: mockPermission.name,
        enabled: mockPermission.enabled,
        template: mockPermission.template,
        catgory: mockPermission.category
      }
    ];
    const expectedResult = {
      id: 1,
      code: 'perm_123',
      name: 'Permission 123',
      enabled: true,
      template: null,
      category: {
        id: 1,
        code: 'CatA',
        name: 'Category A'
      }
    };
    const result = service['toUserPermissionsFromDto'](mockDtos);
    expect(result).toEqual([expectedResult]);
  });
});
