import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HomuUserFilterBE, HomuUserHosusBE, HomuUserHosusFilter } from '../models/homu-users.model';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class HomuUsersHosusService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/homus`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(id: number, query?: DataQuery<HomuUserHosusFilter>): Observable<PagedData<HomuUserHosusBE>> {
    const url = `${this.baseUrl}/${id}/users`;
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<HomuUserHosusBE>>(url + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<HomuUserHosusFilter>,
    exportOptions: DownloadFormat,
    id: number
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };
    const url = `${this.baseUrl}/${id}/users/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<HomuUserHosusFilter>>): RequestQuery<HomuUserFilterBE> {
    const { registerDate, expiryDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromRegisterDate: registerDate && toShortIsoDate(registerDate[0]),
        toRegisterDate: registerDate && toShortIsoDate(registerDate[1] || registerDate[0]),
        fromExpiryDate: expiryDate && toShortIsoDate(expiryDate[0]),
        toExpiryDate: expiryDate && toShortIsoDate(expiryDate[1] || expiryDate[0])
      }
    });
  }
}
