import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { HomuUserFilter } from '../models/homu-users.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class HomuUsersFilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: HomuUserFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<HomuUserFilter>> = {
      userCode: userCode => `${this.translate('userCode.label')} - ${userCode}`,
      portalEmail: portalAccessEmail => `${this.translate('portalEmail.label')} - ${portalAccessEmail}`,
      name: name => `${this.translate('name.label')} - ${name}`,
      registerDate: registerDate =>
        `${this.translate('registerDate.label')} - ${rangeDateFilterTagMapper(registerDate)}`,
      expiryDate: expiryDate => `${this.translate('expiryDate.label')} - ${rangeDateFilterTagMapper(expiryDate)}`,
      status: status => {
        const translatedStatus = this.translation.translate(`common.${status}`);

        return `${this.translate('status.label')} - ${translatedStatus}`;
      }
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.USERS_MAINTENANCE.HOMU_USERS.filters.${key}`);
  }
}
