import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { HomuUserFilter } from '../models/homu-users.model';
import { HomuUsersFilterFormatter } from './homu-users-filter-formatter';

const filters = {
  name: 'name1',
  userCode: 'JD123',
  portalEmail: 'email@example.com',
  status: ActiveInactiveInUpperCase.Inactive,
  registerDate: [new Date('2023-01-15'), new Date('2023-02-20')],
  expiryDate: [new Date('2023-12-31'), new Date('2024-06-30')]
} as HomuUserFilter;
const translateKey = 'MENU.USERS_MAINTENANCE.HOMU_USERS.filters';

describe('HomuUsersFilterFormatter', () => {
  let formatter: HomuUsersFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new HomuUsersFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['name'],
        label: `${translateKey}.name.label - ${filters.name}`
      },
      {
        keys: ['userCode'],
        label: `${translateKey}.userCode.label - ${filters.userCode}`
      },
      {
        keys: ['portalEmail'],
        label: `${translateKey}.portalEmail.label - ${filters.portalEmail}`
      },
      {
        keys: ['status'],
        label: `${translateKey}.status.label - common.${filters.status}`
      },
      {
        keys: ['registerDate'],
        label: `${translateKey}.registerDate.label - ${rangeDateFilterTagMapper(filters.registerDate)}`
      },
      {
        keys: ['expiryDate'],
        label: `${translateKey}.expiryDate.label - ${rangeDateFilterTagMapper(filters.expiryDate)}`
      }
    ]);
  });
});
