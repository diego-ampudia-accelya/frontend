import { Reason } from '~app/master-data/configuration';

export type IataRaReasonRequest = Omit<Reason, 'id' | 'type'>;
