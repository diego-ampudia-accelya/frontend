import { Reason } from '~app/master-data/configuration';

export interface IataReason extends Omit<Reason, 'type'> {
  bspId: number;
  bspAcdmConfigId: number;
}

export type IataReasonRequest = Omit<Reason, 'id'>;
