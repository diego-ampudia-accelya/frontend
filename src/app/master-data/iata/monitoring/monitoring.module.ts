import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringHotDetailsModule } from './monitoring-hot-details/monitoring-hot-details.module';
import { MonitorHotDetailsIdResolver } from './monitoring-hot-details/services/monitoring-hot-details-resolvers';
import { MonitoringRoutingModule } from './monitoring-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, L10nTranslationModule, MonitoringRoutingModule, MonitoringHotDetailsModule],
  providers: [MonitorHotDetailsIdResolver]
})
export class MonitoringModule {}
