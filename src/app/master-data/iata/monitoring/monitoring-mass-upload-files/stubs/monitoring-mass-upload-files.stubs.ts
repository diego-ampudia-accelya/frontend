import { EFileType, EStatus, MonitoringMassUploadEntry } from '../models/monitoring-mass-upload-files.models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { User, UserType } from '~app/shared/models/user.model';

export const monitoringMasterDataRet: MonitoringMassUploadEntry[] = [
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESecEARS_20220418_2203_010',
    fileType: EFileType.ec,
    startDate: '2023-04-18T16:03:11',
    endDate: '2023-04-18T16:03:20',
    status: EStatus.Completed,
    loadedRecords: 1,
    errorsTotal: 1
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESpeEARS_20230415_0755_010.txt',
    fileType: EFileType.pe,
    startDate: '2023-04-18T15:30:19',
    endDate: '2023-04-18T15:30:19',
    status: EStatus.Completed,
    loadedRecords: 2,
    errorsTotal: 2
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESpeEARS_20230415_0755_005.txt',
    fileType: EFileType.pe,
    startDate: '2023-04-18T15:20:42',
    endDate: null,
    status: EStatus.Pending,
    loadedRecords: null,
    errorsTotal: null
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESpeEARS_20230415_0755_005.txt',
    fileType: EFileType.pe,
    startDate: '2023-04-18T15:20:41',
    endDate: null,
    status: EStatus.Pending,
    loadedRecords: null,
    errorsTotal: null
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESpeEARS_20230415_0755_005.txt',
    fileType: EFileType.pe,
    startDate: '2023-04-18T15:20:41',
    endDate: null,
    status: EStatus.Pending,
    loadedRecords: null,
    errorsTotal: null
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESpeEARS_20230415_0755_005.txt',
    fileType: EFileType.pe,
    startDate: '2023-04-18T13:22:48',
    endDate: '2023-04-18T13:22:49',
    status: EStatus.Completed,
    loadedRecords: 2,
    errorsTotal: 2
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESecEARS_20220418_2203_011',
    fileType: EFileType.ec,
    startDate: '2023-04-18T09:42:15',
    endDate: '2023-04-18T09:42:20',
    status: EStatus.Completed,
    loadedRecords: 2,
    errorsTotal: 1
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESecEARS_20220418_2203_001',
    fileType: EFileType.ec,
    startDate: '2023-04-18T08:59:20',
    endDate: null,
    status: EStatus.Pending,
    loadedRecords: null,
    errorsTotal: null
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESecEARS_20220418_2203_001',
    fileType: EFileType.ec,
    startDate: '2023-04-18T08:53:08',
    endDate: null,
    status: EStatus.Pending,
    loadedRecords: null,
    errorsTotal: null
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESecEARS_20220418_2203_001',
    fileType: EFileType.ec,
    startDate: '2023-04-18T08:52:25',
    endDate: null,
    status: EStatus.Pending,
    loadedRecords: null,
    errorsTotal: null
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESecEARS_20220418_2203_001',
    fileType: EFileType.ec,
    startDate: '2023-04-18T08:52:25',
    endDate: null,
    status: EStatus.Pending,
    loadedRecords: null,
    errorsTotal: null
  }
];

export const monitoringMassUploadFilesPagedData: PagedData<MonitoringMassUploadEntry> = {
  pageNumber: 0,
  pageSize: 20,
  total: 1,
  totalPages: 1,
  records: monitoringMasterDataRet
};

export const userBsps: Bsp[] = [
  {
    id: 6983,
    isoCountryCode: 'ES',
    name: 'SPAIN',
    effectiveFrom: '2000-01-01',
    effectiveTo: null,
    version: 3232,
    defaultCurrencyCode: null,
    active: true
  }
];

export const user: User = {
  email: 'martin.sautter@iata.org',
  firstName: null,
  lastName: 'User0BSPES',
  telephone: '021224836126969',
  organization: "organisation ' comilla",
  address: '19 Mayis Cad. 4',
  locality: "icity ' comillas",
  city: null,
  zip: '123',
  country: 'Turquia',
  registerDate: '2000-09-06',
  expiryDate: null,
  active: true,
  id: 69830,
  userType: UserType.IATA,
  iataCode: null,
  language: 'en',
  bspPermissions: [
    {
      bspId: 6983,
      permissions: ['rMonHot']
    }
  ],
  permissions: ['rMonHot'],
  bsps: [
    {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      version: 3232,
      defaultCurrencyCode: null,
      active: true
    }
  ],
  isImpersonating: false,
  defaultIsoc: 'ES',
  template: 'STREAMLINED',
  isSuperUser: false
};
