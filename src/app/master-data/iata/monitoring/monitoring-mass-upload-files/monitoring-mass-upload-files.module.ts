import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringMassUploadFilesListComponent } from './components/monitoring-master-data-ret-list/monitoring-mass-upload-files-list.component';
import { MonitoringMassUploadFilesEffects } from './effects/monitoring-mass-upload-files.effects';
import { MonitoringMassUploadFilesRoutingModule } from './monitoring-mass-upload-files-routing.module';
import { MonitoringMasterDataRetComponent } from './monitoring-mass-upload-files.component';
import { monitoringMassUploadFilesReducer } from './reducers/monitoring-mass-upload-files.reducer';
import { MonitoringMassUploadFilesFilterFormatterService } from './services/monitoring-mass-upload-files-filter-formatter.service';
import { MonitoringMassUploadFilesService } from './services/monitoring-mass-upload-files.service';
import { MonitoringMassUploadFilesStoreFacadeService } from './store/monitoring-mass-upload-files-store-facade.service';
import { monitoringMassUploadFilesStateFeatureKey } from './store/monitoring-mass-upload-files.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MonitoringMassUploadFilesRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(monitoringMassUploadFilesStateFeatureKey, monitoringMassUploadFilesReducer),
    EffectsModule.forFeature([MonitoringMassUploadFilesEffects])
  ],
  declarations: [MonitoringMasterDataRetComponent, MonitoringMassUploadFilesListComponent],
  providers: [
    MonitoringMassUploadFilesService,
    MonitoringMassUploadFilesStoreFacadeService,
    MonitoringMassUploadFilesFilterFormatterService
  ]
})
export class MonitoringMassUploadFilesModule {}
