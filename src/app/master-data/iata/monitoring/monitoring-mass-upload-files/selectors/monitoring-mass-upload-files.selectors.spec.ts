import { MonitoringMassUploadFilesState } from '../store/monitoring-mass-upload-files.state';

import {
  selectHasData,
  selectMonitoringMassUploadFiles,
  selectMonitoringMassUploadFilesStateFeature,
  selectMonitoringMassUploadQuery,
  selectSearchMonitoringMassUploadFilesLoading
} from './monitoring-mass-upload-files.selectors';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoring mass upload files selectors', () => {
  const mockState: MonitoringMassUploadFilesState = {
    query: defaultQuery,
    data: [],
    loading: {
      searchMonitoringMassUploadFiles: false
    },
    error: {
      searchMonitoringMassUploadFiles: null
    }
  };

  it('should return MonitoringMassUploadFilesState state', () => {
    const result = selectMonitoringMassUploadFilesStateFeature.projector(mockState);

    expect(result).toBe(mockState);
  });

  it('should select and return monitoring mass upload files data', () => {
    const result = selectMonitoringMassUploadFiles.projector(mockState);

    expect(result).toBe(mockState.data);
  });

  it('should select and return monitoring mass upload query', () => {
    const result = selectMonitoringMassUploadQuery.projector(mockState);

    expect(result).toBe(mockState.query);
  });

  it('should check if search monitoring mass upload files is loading', () => {
    const result = selectSearchMonitoringMassUploadFilesLoading.projector(mockState);

    expect(result).toBe(mockState.loading.searchMonitoringMassUploadFiles);
  });

  it('should return false if the state has not data', () => {
    const result = selectHasData.projector(mockState);

    expect(result).toBe(false);
  });
});
