import { createFeatureSelector, createSelector } from '@ngrx/store';

import {
  MonitoringMassUploadFilesState,
  monitoringMassUploadFilesStateFeatureKey
} from '../store/monitoring-mass-upload-files.state';

export const selectMonitoringMassUploadFilesStateFeature = createFeatureSelector<MonitoringMassUploadFilesState>(
  monitoringMassUploadFilesStateFeatureKey
);

export const selectMonitoringMassUploadFiles = createSelector(
  selectMonitoringMassUploadFilesStateFeature,
  state => state.data
);
export const selectMonitoringMassUploadQuery = createSelector(
  selectMonitoringMassUploadFilesStateFeature,
  state => state.query
);
export const selectSearchMonitoringMassUploadFilesLoading = createSelector(
  selectMonitoringMassUploadFilesStateFeature,
  state => state.loading.searchMonitoringMassUploadFiles
);

export const selectHasData = createSelector(selectMonitoringMassUploadFilesStateFeature, state =>
  Boolean(state.data && state.data.length)
);
