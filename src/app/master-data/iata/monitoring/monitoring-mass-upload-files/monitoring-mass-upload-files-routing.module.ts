import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringMasterDataRetComponent } from './monitoring-mass-upload-files.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: MonitoringMasterDataRetComponent,
    data: {
      tab: ROUTES.MONITOR_MASS_UPLOAD_FILES
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringMassUploadFilesRoutingModule {}
