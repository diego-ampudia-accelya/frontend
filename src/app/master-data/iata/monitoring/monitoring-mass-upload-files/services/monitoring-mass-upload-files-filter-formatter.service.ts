import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { EStatus, MonitoringMassUploadEntryFilters } from '../models/monitoring-mass-upload-files.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type MonitoringMassUploadMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class MonitoringMassUploadFilesFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: MonitoringMassUploadEntryFilters) {
    const filterMappers: Partial<MonitoringMassUploadMapper<MonitoringMassUploadEntryFilters>> = {
      isoCountryCode: isoCountryCode =>
        `${this.translate('isoCountryCode')} - ${mapJoinMapper(isoCountryCode, 'isoCountryCode')}`,
      filename: name => `${this.translate('filename')} - ${name}`,
      fileType: fileType => `${this.translate('fileType')} - ${fileType}`,
      status: status => `${this.translate('status')} - ${this.statusMapper(status)}`
    };

    const entries = Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));

    if (filter.startDate?.length) {
      entries.push({
        label: `${this.translate('startDate')} - ${rangeDateFilterTagMapper(filter.startDate)}`,
        keys: ['startDate']
      });
    }

    if (filter.endDate?.length) {
      entries.push({
        label: `${this.translate('endDate')} - ${rangeDateFilterTagMapper(filter.endDate)}`,
        keys: ['endDate']
      });
    }

    return entries;
  }

  private translate(key: string): string {
    return this.translationService.translate(`monitoring-mass-upload-files.filters.${key}.label`);
  }

  private translateStatus(status: EStatus): string {
    return this.translationService.translate(`monitoring-mass-upload-files.filters.status.option.${status}`);
  }

  private statusMapper(status: EStatus[]): string {
    return status.map(this.translateStatus.bind(this)).join(', ');
  }
}
