import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import {
  EFileType,
  EStatus,
  MonitoringMassUploadEntry,
  MonitoringMassUploadEntryFilters
} from '../models/monitoring-mass-upload-files.models';
import { MonitoringMassUploadFilesStoreFacadeService } from '../store/monitoring-mass-upload-files-store-facade.service';
import { MonitoringMassUploadFilesService } from './monitoring-mass-upload-files.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MonitoringMassUploadFilesService', () => {
  let service: MonitoringMassUploadFilesService;
  let httpClientSpy: SpyObject<HttpClient>;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_HOT_DATA_DETAILS, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  const monitoringRecordsMock: MonitoringMassUploadEntry[] = [
    {
      bsp: {
        id: 1,
        name: 'Spain',
        isoCountryCode: 'ES'
      },
      endDate: '2023-01-01',
      errorsTotal: 5,
      fileType: EFileType.e9,
      loadedRecords: 5,
      filename: 'name',
      startDate: '2023-01-01',
      status: EStatus.Completed
    }
  ];

  const monitoringFilterMock: MonitoringMassUploadEntryFilters[] = [
    {
      isoCountryCode: [
        {
          id: 1,
          name: 'SPAIN',
          isoCountryCode: 'ES'
        }
      ],
      endDate: [new Date('2023-01-01')],
      fileType: EFileType.e9,
      filename: 'name',
      startDate: [new Date('2023-01-01')],
      status: [EStatus.Completed]
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const monitoringMassUploadFilesStoreFacadeServiceSpy = {
    actions: {
      searchMonitoringMassUploadFiles: () => {}
    },
    selectors: {
      query$: of(null),
      data$: of(null),
      hasData$: of(null),
      userBsps$: of(null)
    },
    loading: {
      searchMonitoringMassUploadFiles$: of(null)
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        { provide: HttpClient, useValue: httpClientSpy },
        MonitoringMassUploadFilesService,
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        provideMockStore({ initialState }),
        { provide: MonitoringMassUploadFilesStoreFacadeService, monitoringMassUploadFilesStoreFacadeServiceSpy }
      ]
    });

    service = TestBed.inject(MonitoringMassUploadFilesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should subscribe to find method and return page data as MonitoringMassUploadEntry', fakeAsync(() => {
    let monitoringReportData: PagedData<MonitoringMassUploadEntry>;

    const auditDataMock: PagedData<MonitoringMassUploadEntry> = {
      records: monitoringRecordsMock,
      total: monitoringRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };
    const requestQuery = service['buildRequestQuery'](query);
    spyOn(requestQuery, 'getQueryString').and.callThrough();

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(result => {
      monitoringReportData = result;
    });
    tick();

    expect(monitoringReportData).toEqual(auditDataMock);
  }));

  it('should format the query filtering by the filters selected for the user, isoCountryCode', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const isoCountryCodeFormatted = ['ES'];
    const time = [new Date('01/01/2023'), new Date('01/01/2023')];
    queryCloneDeep.filterBy.startDate = time;
    queryCloneDeep.filterBy.endDate = time;

    const dataQuery: DataQuery<MonitoringMassUploadEntryFilters> = {
      ...queryCloneDeep,
      filterBy: {
        isoCountryCode: monitoringFilterMock[0].isoCountryCode,
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['buildRequestQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual(isoCountryCodeFormatted);
  });
});
