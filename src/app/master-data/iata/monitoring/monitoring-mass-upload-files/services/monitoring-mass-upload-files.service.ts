import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { clone } from 'lodash';
import { Observable } from 'rxjs';

import {
  MonitoringMassUploadEntry,
  MonitoringMassUploadEntryFilters
} from '../models/monitoring-mass-upload-files.models';
import { MonitoringMassUploadFilesStoreFacadeService } from '../store/monitoring-mass-upload-files-store-facade.service';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class MonitoringMassUploadFilesService implements Queryable<MonitoringMassUploadEntry> {
  private bspId: number | null;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private monitoringMasterDataRetStoreFacadeService: MonitoringMassUploadFilesStoreFacadeService
  ) {
    this.monitoringMasterDataRetStoreFacadeService.selectors.userBsps$.subscribe(
      bsps => (this.bspId = bsps && bsps[0] ? bsps[0].id : null)
    );
  }

  private baseUrl = `${this.appConfiguration.baseApiPath}/bsp-management/bsps`;

  public find(query: DataQuery<MonitoringMassUploadEntryFilters>): Observable<PagedData<MonitoringMassUploadEntry>> {
    const requestQuery = this.buildRequestQuery(query);

    return this.http.get<PagedData<MonitoringMassUploadEntry>>(
      `${this.baseUrl}/${this.bspId}/monitoring/massupload` + requestQuery.getQueryString()
    );
  }

  private buildRequestQuery(query: DataQuery<MonitoringMassUploadEntryFilters>) {
    const _query = JSON.parse(JSON.stringify(query));

    if (_query.filterBy.startDate?.length) {
      const [fromStartDate, toStartDate] = clone(_query.filterBy.startDate);

      delete _query.filterBy.startDate;

      _query.filterBy['fromStartDate'] = toShortIsoDate(fromStartDate);
      _query.filterBy['toStartDate'] = toShortIsoDate(toStartDate || fromStartDate);
    }

    if (_query.filterBy['isoCountryCode']) {
      const isoCountryCode = clone(_query.filterBy.isoCountryCode);

      delete _query.filterBy.isoCountryCode;

      _query.filterBy['isoCountryCode'] = isoCountryCode.map(bsp => bsp.isoCountryCode);
    }

    if (_query.filterBy.endDate?.length) {
      const [fromEndDate, toEndDate] = clone(_query.filterBy.endDate);

      delete _query.filterBy.endDate;

      _query.filterBy['fromEndDate'] = toShortIsoDate(fromEndDate);
      _query.filterBy['toEndDate'] = toShortIsoDate(toEndDate || fromEndDate);
    }

    return RequestQuery.fromDataQuery(_query);
  }
}
