import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { EFileType, EStatus, MonitoringMassUploadEntryFilters } from '../models/monitoring-mass-upload-files.models';

import { MonitoringMassUploadFilesFilterFormatterService } from './monitoring-mass-upload-files-filter-formatter.service';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

const filters = {
  isoCountryCode: [
    {
      id: 1,
      name: 'SPAIN',
      isoCountryCode: 'ES'
    }
  ],
  filename: 'filename1',
  endDate: [new Date('2023-01-01')],
  fileType: EFileType.e9,
  startDate: [new Date('2023-01-01')],
  status: [EStatus.Completed]
} as MonitoringMassUploadEntryFilters;

const translateKey = (key: string) => `monitoring-mass-upload-files.filters.${key}.label`;
const translateStatus = (status: EStatus) => `monitoring-mass-upload-files.filters.status.option.${status}`;

describe('MonitoringMassUploadFilesFilterFormatterService', () => {
  let formatter: MonitoringMassUploadFilesFilterFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new MonitoringMassUploadFilesFilterFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['isoCountryCode'],
        label: `${translateKey('isoCountryCode')} - ES`
      },
      {
        keys: ['filename'],
        label: `${translateKey('filename')} - filename1`
      },
      {
        keys: ['fileType'],
        label: `${translateKey('fileType')} - ${EFileType.e9}`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - ${translateStatus(filters.status[0])}`
      },
      {
        keys: ['startDate'],
        label: `${translateKey('startDate')} - ${rangeDateFilterTagMapper(filters.startDate)}`
      },
      {
        keys: ['endDate'],
        label: `${translateKey('endDate')} - ${rangeDateFilterTagMapper(filters.endDate)}`
      }
    ]);
  });

  it('should return empty array if no filters are passed by parameter to format method', () => {
    const emptyFilters = {} as MonitoringMassUploadEntryFilters;
    const result = formatter.format(emptyFilters);

    expect(result).toEqual([]);
  });
});
