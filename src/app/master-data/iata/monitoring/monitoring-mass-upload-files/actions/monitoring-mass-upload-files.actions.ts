import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import {
  MonitoringMassUploadEntry,
  MonitoringMassUploadEntryFilters
} from '../models/monitoring-mass-upload-files.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchMonitoringMassUploadFilesRequest = createAction(
  '[Monitoring Mass Upload Files]Search Monitoring Mass Upload Files Request',
  props<{ payload: { query?: DataQuery<MonitoringMassUploadEntryFilters> } }>()
);

export const searchMonitoringMassUploadFilesSuccess = createAction(
  '[Monitoring Mass Upload Files]Search Monitoring Mass Upload Files Success',
  props<{ payload: { pagedData: PagedData<MonitoringMassUploadEntry> } }>()
);

export const searchMonitoringMassUploadFilesFailure = createAction(
  '[Monitoring Mass Upload Files]Search Monitoring Mass Upload Files Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);
