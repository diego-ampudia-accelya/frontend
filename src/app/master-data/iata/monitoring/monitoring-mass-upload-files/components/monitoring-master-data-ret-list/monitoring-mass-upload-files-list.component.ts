import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { MONITORING_MASS_UPLOAD_FILES } from '../../constants/monitoring-mass-upload-files.constants';
import {
  EFileType,
  EStatus,
  MonitoringMassUploadEntry,
  MonitoringMassUploadEntryFilters
} from '../../models/monitoring-mass-upload-files.models';
import { MonitoringMassUploadFilesFilterFormatterService } from '../../services/monitoring-mass-upload-files-filter-formatter.service';
import { MonitoringMassUploadFilesStoreFacadeService } from '../../store/monitoring-mass-upload-files-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-monitoring-mass-upload-files-list',
  templateUrl: './monitoring-mass-upload-files-list.component.html'
})
export class MonitoringMassUploadFilesListComponent implements OnInit {
  public columns: TableColumn[] = MONITORING_MASS_UPLOAD_FILES.COLUMNS;
  public hasAllBspsPermission: boolean;
  public userBspDropdownOptions$: Observable<DropdownOption<BspDto>[]>;
  public userDefaultBsp$: Observable<Bsp>;

  public fileTypeOptions = this.initFileTypeDropdownOptions();

  public statusOptions = [
    {
      value: EStatus.Completed,
      label: this.translationService.translate('monitoring-mass-upload-files.filters.status.option.Completed')
    },
    {
      value: EStatus.Failed,
      label: this.translationService.translate('monitoring-mass-upload-files.filters.status.option.Failed')
    },
    {
      value: EStatus.Pending,
      label: this.translationService.translate('monitoring-mass-upload-files.filters.status.option.Pending')
    }
  ];

  public data$: Observable<MonitoringMassUploadEntry[]> =
    this.monitoringMassUploadFilesStoreFacadeService.selectors.data$;
  public query$: Observable<DataQuery<MonitoringMassUploadEntryFilters>> =
    this.monitoringMassUploadFilesStoreFacadeService.selectors.query$;
  public loading$: Observable<boolean> =
    this.monitoringMassUploadFilesStoreFacadeService.loading.searchMonitoringMassUploadFiles$;
  public hasData$: Observable<boolean> = this.monitoringMassUploadFilesStoreFacadeService.selectors.hasData$;

  public filterFormGroup: FormGroup;

  constructor(
    public readonly monitoringMassUploadFilesFilterFormatterService: MonitoringMassUploadFilesFilterFormatterService,
    private readonly monitoringMassUploadFilesStoreFacadeService: MonitoringMassUploadFilesStoreFacadeService,
    private readonly translationService: L10nTranslationService,
    private readonly permissionService: PermissionsService
  ) {}

  public get title$(): Observable<string> {
    return this.query$.pipe(
      map(query => query?.paginateBy?.totalElements),
      map(length => `${this.translationService.translate('monitoring-mass-upload-files.view.title')} (${length || 0})`)
    );
  }

  public get predefinedFilters$(): Observable<Partial<MonitoringMassUploadEntryFilters>> {
    return this.userDefaultBsp$.pipe(
      map(bsp => ({
        isoCountryCode: [bsp]
      }))
    );
  }

  public get isBspFilterDisabled$(): Observable<boolean> {
    return this.userBspDropdownOptions$.pipe(map(userBspDropdownOptions => Boolean(userBspDropdownOptions?.length)));
  }

  public ngOnInit(): void {
    this.initializePermissions();

    this.initializeDefaultBsp();

    this.initializeFilterFormGroup();

    this.userBspDropdownOptions$ = this.monitoringMassUploadFilesStoreFacadeService.selectors.userBsps$.pipe(
      map(bspList => bspList.map(bsp => toValueLabelObjectBsp(bsp)))
    );

    this.userDefaultBsp$.pipe(first()).subscribe(bsp => {
      this.monitoringMassUploadFilesStoreFacadeService.actions.searchMonitoringMassUploadFiles({
        ...defaultQuery,
        filterBy: {
          ...defaultQuery.filterBy,
          isoCountryCode: [bsp]
        },
        sortBy: [
          ...defaultQuery.sortBy,
          {
            attribute: 'startDate',
            sortType: SortOrder.Desc
          }
        ]
      });
    });
  }

  public onQueryChanged(query: DataQuery<MonitoringMassUploadEntryFilters>): void {
    this.monitoringMassUploadFilesStoreFacadeService.actions.searchMonitoringMassUploadFiles(query);
  }

  private initializeFilterFormGroup() {
    this.filterFormGroup = new FormGroup({
      isoCountryCode: new FormControl(null, []),
      filename: new FormControl(null, []),
      startDate: new FormControl(null, []),
      endDate: new FormControl(null, []),
      status: new FormControl(null, []),
      fileType: new FormControl(null, [])
    });
  }

  private initializeDefaultBsp() {
    this.userDefaultBsp$ = combineLatest([
      this.monitoringMassUploadFilesStoreFacadeService.selectors.userBsps$,
      this.monitoringMassUploadFilesStoreFacadeService.selectors.user$
    ]).pipe(map(([bspList, user]) => this.getDefaultBsp(bspList, user)));
  }

  private initializePermissions(): void {
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private getDefaultBsp(bspList: Bsp[], user: User): Bsp {
    return bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
  }

  private initFileTypeDropdownOptions() {
    return Object.keys(EFileType).map(key => ({
      value: key,
      label: EFileType[key]
    }));
  }
}
