import { ComponentFixture, TestBed } from '@angular/core/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { MonitoringMassUploadFilesFilterFormatterService } from '../../services/monitoring-mass-upload-files-filter-formatter.service';
import { MonitoringMassUploadFilesStoreFacadeService } from '../../store/monitoring-mass-upload-files-store-facade.service';
import * as fromStub from '../../stubs/monitoring-mass-upload-files.stubs';
import { MonitoringMassUploadFilesListComponent } from './monitoring-mass-upload-files-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { ListViewComponent } from '~app/shared/components/list-view';

describe('MonitoringMassUploadFilesListComponent', () => {
  let component: MonitoringMassUploadFilesListComponent;
  let fixture: ComponentFixture<MonitoringMassUploadFilesListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule)],
      declarations: [
        MonitoringMassUploadFilesListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: MonitoringMassUploadFilesFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: MonitoringMassUploadFilesStoreFacadeService,
          useValue: {
            actions: {
              searchMonitoringMassUploadFiles: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null),
              userBsps$: of(fromStub.userBsps),
              user$: of(fromStub.user)
            },
            loading: {
              searchMonitoringMassUploadFilesLoading$: of(null)
            }
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringMassUploadFilesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
