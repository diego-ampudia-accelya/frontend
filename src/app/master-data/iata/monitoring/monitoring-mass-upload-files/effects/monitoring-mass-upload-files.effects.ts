import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as fromActions from '../actions/monitoring-mass-upload-files.actions';
import { MonitoringMassUploadFilesService } from '../services/monitoring-mass-upload-files.service';

@Injectable()
export class MonitoringMassUploadFilesEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly monitoringHotDataService: MonitoringMassUploadFilesService
  ) {}

  public searchMonitoringMassUploadFilesEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchMonitoringMassUploadFilesRequest>>(
        fromActions.searchMonitoringMassUploadFilesRequest
      ),
      switchMap(({ payload: { query } }) =>
        this.monitoringHotDataService.find(query).pipe(
          map(data =>
            fromActions.searchMonitoringMassUploadFilesSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchMonitoringMassUploadFilesFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );
}
