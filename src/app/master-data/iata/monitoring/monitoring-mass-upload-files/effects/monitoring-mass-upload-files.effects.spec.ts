import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/monitoring-mass-upload-files.actions';
import * as fromSelectors from '../selectors/monitoring-mass-upload-files.selectors';
import { MonitoringMassUploadFilesService } from '../services/monitoring-mass-upload-files.service';
import * as fromState from '../store/monitoring-mass-upload-files.state';
import * as fromStub from '../stubs/monitoring-mass-upload-files.stubs';

import { MonitoringMassUploadFilesEffects } from './monitoring-mass-upload-files.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringMassUploadFilesEffects', () => {
  let actions$: Observable<any>;
  let effects: MonitoringMassUploadFilesEffects;
  let service: MonitoringMassUploadFilesService;

  const initialState = {
    [fromState.monitoringMassUploadFilesStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringMassUploadFilesEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectMonitoringMassUploadQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: MonitoringMassUploadFilesService,
          useValue: {
            find: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<MonitoringMassUploadFilesEffects>(MonitoringMassUploadFilesEffects);
    service = TestBed.inject<MonitoringMassUploadFilesService>(MonitoringMassUploadFilesService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchMonitoringMassUploadFilesEffect', () => {
    it('should return searchMonitoringMassUploadFilesSuccess with paged data', () => {
      const action = fromActions.searchMonitoringMassUploadFilesRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchMonitoringMassUploadFilesSuccess({
        payload: {
          pagedData: fromStub.monitoringMassUploadFilesPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.monitoringMassUploadFilesPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchMonitoringMassUploadFilesEffect$).toBeObservable(expected);
    });

    it('should return searchMonitoringMassUploadFilesFailure when monitoring hot data service find fails', () => {
      const action = fromActions.searchMonitoringMassUploadFilesRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchMonitoringMassUploadFilesFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchMonitoringMassUploadFilesEffect$).toBeObservable(expected);
    });
  });
});
