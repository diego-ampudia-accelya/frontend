/* eslint-disable @typescript-eslint/naming-convention */
import { Bsp } from '~app/shared/models/bsp.model';

export enum EStatus {
  Completed = 'Completed',
  Failed = 'Failed',
  Pending = 'Pending'
}

export enum EFileType {
  ec = 'ec - ACM/ADM mass upload v2.0 file',
  e9 = 'e9 - Refund Application Action File',
  ed = 'ed - Mass Loading files',
  pe = 'pe - Mass upload PBD file'
}

export interface MonitoringMassUploadEntryFilters {
  isoCountryCode: Bsp[];
  fileType: EFileType;
  filename: string;
  startDate: Date[];
  endDate: Date[];
  status: EStatus[];
}

export interface MonitoringMassUploadEntry {
  bsp: Bsp;
  endDate: string;
  errorsTotal: number;
  fileType: EFileType;
  loadedRecords: number;
  filename: string;
  startDate: string;
  status: EStatus;
}
