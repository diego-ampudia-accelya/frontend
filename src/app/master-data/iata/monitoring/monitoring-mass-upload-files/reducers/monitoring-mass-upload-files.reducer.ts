import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-mass-upload-files.actions';
import { initialState } from '../store/monitoring-mass-upload-files.state';

export const monitoringMassUploadFilesReducer = createReducer(
  initialState,
  on(fromActions.searchMonitoringMassUploadFilesRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchMonitoringMassUploadFiles: true
    },
    error: {
      ...state.error,
      searchMonitoringMassUploadFiles: null
    }
  })),
  on(fromActions.searchMonitoringMassUploadFilesFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchMonitoringMassUploadFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringMassUploadFiles: error
    }
  })),
  on(fromActions.searchMonitoringMassUploadFilesSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchMonitoringMassUploadFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringMassUploadFiles: null
    }
  }))
);
