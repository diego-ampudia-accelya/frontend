import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/monitoring-mass-upload-files.actions';
import * as fromState from '../store/monitoring-mass-upload-files.state';
import * as fromStub from '../stubs/monitoring-mass-upload-files.stubs';

import * as fromReducer from './monitoring-mass-upload-files.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoringMassUploadFilesReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.monitoringMassUploadFilesReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchMonitoringMassUploadFilesRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchMonitoringMassUploadFilesRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.monitoringMassUploadFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringMassUploadFiles: true
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringMassUploadFiles: null
        }
      });
    });
  });

  describe('searchMonitoringMassUploadFilesFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchMonitoringMassUploadFilesFailure({ payload: { error } });
      const result = fromReducer.monitoringMassUploadFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringMassUploadFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringMassUploadFiles: error
        }
      });
    });
  });

  describe('searchMonitoringMassUploadFilesSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchMonitoringMassUploadFilesSuccess({
        payload: { pagedData: fromStub.monitoringMassUploadFilesPagedData }
      });
      const result = fromReducer.monitoringMassUploadFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.monitoringMassUploadFilesPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.monitoringMassUploadFilesPagedData.pageNumber,
            size: fromStub.monitoringMassUploadFilesPagedData.pageSize,
            totalElements: fromStub.monitoringMassUploadFilesPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringMassUploadFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringMassUploadFiles: null
        }
      });
    });
  });
});
