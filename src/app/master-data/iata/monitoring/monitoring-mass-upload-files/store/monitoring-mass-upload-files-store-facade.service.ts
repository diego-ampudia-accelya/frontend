import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-mass-upload-files.actions';
import { MonitoringMassUploadEntryFilters } from '../models/monitoring-mass-upload-files.models';
import * as fromSelectors from '../selectors/monitoring-mass-upload-files.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class MonitoringMassUploadFilesStoreFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchMonitoringMassUploadFiles: (query?: DataQuery<MonitoringMassUploadEntryFilters>) =>
        this.store.dispatch(fromActions.searchMonitoringMassUploadFilesRequest({ payload: { query } }))
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectMonitoringMassUploadQuery),
      data$: this.store.select(fromSelectors.selectMonitoringMassUploadFiles),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser)
    };
  }

  public get loading() {
    return {
      searchMonitoringMassUploadFiles$: this.store.select(fromSelectors.selectSearchMonitoringMassUploadFilesLoading)
    };
  }
}
