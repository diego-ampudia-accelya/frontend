import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/monitoring-mass-upload-files.actions';
import * as fromSelectors from '../selectors/monitoring-mass-upload-files.selectors';

import { MonitoringMassUploadFilesStoreFacadeService } from './monitoring-mass-upload-files-store-facade.service';
import { initialState, monitoringMassUploadFilesStateFeatureKey } from './monitoring-mass-upload-files.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringMasterDataRetFacadeService', () => {
  let service: MonitoringMassUploadFilesStoreFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringMassUploadFilesStoreFacadeService,
        provideMockStore({
          initialState: {
            [monitoringMassUploadFilesStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MonitoringMassUploadFilesStoreFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('should invoke searchMonitoringMassUploadFiles and dispatch a new searchMonitoringMassUploadFilesRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchMonitoringMassUploadFiles(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchMonitoringMassUploadFilesRequest({ payload: { query: defaultQuery } })
      );
    });
  });

  describe('selectors', () => {
    it('should invoke query$ and select query', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringMassUploadQuery);
    });

    it('should invoke data$ and select data', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringMassUploadFiles);
    });

    it('should invoke hasData$ and select hasData', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('should invoke searchMonitoringMassUploadFiles$ and select selectSearchMonitoringMassUploadFilesLoading', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchMonitoringMassUploadFiles$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchMonitoringMassUploadFilesLoading);
    });
  });
});
