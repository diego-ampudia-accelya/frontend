import { HttpErrorResponse } from '@angular/common/http';

import {
  MonitoringMassUploadEntry,
  MonitoringMassUploadEntryFilters
} from '../models/monitoring-mass-upload-files.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const monitoringMassUploadFilesStateFeatureKey = 'monitoringMassUploadFilesStateFeatureKey';

export interface MonitoringMassUploadFilesState {
  query: DataQuery<MonitoringMassUploadEntryFilters>;
  data: MonitoringMassUploadEntry[];
  loading: {
    searchMonitoringMassUploadFiles: boolean;
  };
  error: {
    searchMonitoringMassUploadFiles: HttpErrorResponse | null;
  };
}

export const initialState: MonitoringMassUploadFilesState = {
  query: defaultQuery,
  data: [],
  loading: {
    searchMonitoringMassUploadFiles: false
  },
  error: {
    searchMonitoringMassUploadFiles: null
  }
};
