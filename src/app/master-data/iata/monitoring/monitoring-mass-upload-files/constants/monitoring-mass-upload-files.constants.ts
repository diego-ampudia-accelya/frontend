/* eslint-disable @typescript-eslint/naming-convention */
import moment from 'moment-mini';

import { GridColumn } from '~app/shared/models';

export const MONITORING_MASS_UPLOAD_FILES = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'bsp.isoCountryCode',
        name: 'monitoring-mass-upload-files.columns.isoCountryCode',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'fileType',
        name: 'monitoring-mass-upload-files.columns.fileType',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'filename',
        name: 'monitoring-mass-upload-files.columns.filename',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'startDate',
        name: 'monitoring-mass-upload-files.columns.startDate',
        resizeable: false,
        draggable: false,
        pipe: { transform: date => date && moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'endDate',
        name: 'monitoring-mass-upload-files.columns.endDate',
        resizeable: true,
        draggable: false,
        pipe: { transform: date => date && moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'status',
        name: 'monitoring-mass-upload-files.columns.status',
        resizeable: true,
        draggable: false,
        cellTemplate: 'statusBasicTmpl'
      },
      {
        prop: 'loadedRecords',
        name: 'monitoring-mass-upload-files.columns.loadedRecords',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'errorsTotal',
        name: 'monitoring-mass-upload-files.columns.errorsTotal',
        resizeable: true,
        draggable: false
      }
    ];
  }
};
