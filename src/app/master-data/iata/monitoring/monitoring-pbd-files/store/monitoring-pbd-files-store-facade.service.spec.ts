import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromSelectors from '../selectors/monitoring-pbd-files.selectors';

import { MonitoringPbdStoreFacadeService } from './monitoring-pbd-files-store-facade.service';
import { initialState, monitoringPbdFilesStateFeatureKey } from './monitoring-pbd-files.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringPbdStoreFacadeService', () => {
  let service: MonitoringPbdStoreFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringPbdStoreFacadeService,
        provideMockStore({
          initialState: {
            [monitoringPbdFilesStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MonitoringPbdStoreFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('should invoke searchMonitoringHotData and dispatch a new searchMonitoringHotDataRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchMonitoringPbdFiles(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchMonitoringPbdFilesRequest({ payload: { query: defaultQuery } })
      );
    });
  });

  describe('selectors', () => {
    it('should invoke query$ and select query', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectselectMonitoringPbdFilesQuery);
    });

    it('should invoke data$ and select data', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringPbdFiles);
    });

    it('should invoke hasData$ and select hasData', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('should invoke searchMonitoringPbdFiles$ and select selectSearchMonitoringPbdFilesLoading', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchMonitoringPbdFiles$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchMonitoringPbdFilesLoading);
    });
  });
});
