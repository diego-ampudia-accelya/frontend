import { HttpErrorResponse } from '@angular/common/http';

import { MonitoringPbdFileEntry, MonitoringPbdFileEntryFilters } from '../models/monitoring-pbd-files.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const monitoringPbdFilesStateFeatureKey = 'monitoringPbdFilesStateFeatureKey';

export interface MonitoringPbdFilesState {
  query: DataQuery<MonitoringPbdFileEntryFilters>;
  data: MonitoringPbdFileEntry[];
  loading: {
    searchMonitoringPbdFiles: boolean;
  };
  error: {
    searchMonitoringPbdFiles: HttpErrorResponse | null;
  };
}

export const initialState: MonitoringPbdFilesState = {
  query: defaultQuery,
  data: [],
  loading: {
    searchMonitoringPbdFiles: false
  },
  error: {
    searchMonitoringPbdFiles: null
  }
};
