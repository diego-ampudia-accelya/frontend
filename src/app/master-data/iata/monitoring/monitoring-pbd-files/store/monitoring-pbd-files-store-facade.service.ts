import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { MonitoringPbdFileEntryFilters } from '../models/monitoring-pbd-files.models';
import * as fromSelectors from '../selectors/monitoring-pbd-files.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class MonitoringPbdStoreFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchMonitoringPbdFiles: (query?: DataQuery<MonitoringPbdFileEntryFilters>) =>
        this.store.dispatch(fromActions.searchMonitoringPbdFilesRequest({ payload: { query } }))
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectselectMonitoringPbdFilesQuery),
      data$: this.store.select(fromSelectors.selectMonitoringPbdFiles),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser)
    };
  }

  public get loading() {
    return {
      searchMonitoringPbdFiles$: this.store.select(fromSelectors.selectSearchMonitoringPbdFilesLoading)
    };
  }
}
