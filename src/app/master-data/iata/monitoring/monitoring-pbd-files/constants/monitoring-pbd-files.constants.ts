/* eslint-disable @typescript-eslint/naming-convention */
import moment from 'moment-mini';

import { GridColumn } from '~app/shared/models';

export const MONITORING_PBD_FILES = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'bsp.isoCountryCode',
        name: 'monitoring-pbd-files.columns.isoCountryCode',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'filename',
        name: 'monitoring-pbd-files.columns.filename',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'startDate',
        name: 'monitoring-pbd-files.columns.startDate',
        resizeable: false,
        draggable: false,
        pipe: { transform: date => date && moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'endDate',
        name: 'monitoring-pbd-files.columns.endDate',
        resizeable: true,
        draggable: false,
        pipe: { transform: date => date && moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'status',
        name: 'monitoring-pbd-files.columns.status',
        resizeable: true,
        draggable: false,
        cellTemplate: 'statusBasicTmpl'
      }
    ];
  }
};
