/* eslint-disable @typescript-eslint/naming-convention */
import { Bsp } from '~app/shared/models/bsp.model';

export enum EStatus {
  Completed = 'Completed',
  Failed = 'Failed',
  Pending = 'Pending'
}

export interface MonitoringPbdFileEntryFilters {
  isoCountryCode: Bsp[];
  filename: string;
  startDate: Date[];
  endDate: Date[];
  status: EStatus[];
}

export interface MonitoringPbdFileEntry {
  bsp: Bsp;
  endDate: string;
  filename: string;
  startDate: string;
  status: EStatus;
}
