import { EStatus, MonitoringPbdFileEntry } from '../models/monitoring-pbd-files.models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { User, UserType } from '~app/shared/models/user.model';

export const monitoringPbdFiles: MonitoringPbdFileEntry[] = [
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESeuEARS_rejections',
    startDate: '2023-04-07T02:04:59',
    endDate: null,
    status: EStatus.Pending
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESeuEARS_rejected',
    startDate: '2023-04-06T02:05:01',
    endDate: '2023-04-06T02:05:01',
    status: EStatus.Failed
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'ESeuEARS_rejected',
    startDate: '2023-04-05T02:05:01',
    endDate: '2023-04-05T02:05:01',
    status: EStatus.Completed
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'crs_agent',
    startDate: '2023-04-05T02:00:07',
    endDate: '2023-04-05T02:00:08',
    status: EStatus.Completed
  },
  {
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    filename: 'crs_agent',
    startDate: '2023-04-04T02:00:13',
    endDate: '2023-04-04T02:00:14',
    status: EStatus.Completed
  }
];

export const monitoringPbdFilesPagedData: PagedData<MonitoringPbdFileEntry> = {
  pageNumber: 0,
  pageSize: 20,
  total: 1,
  totalPages: 1,
  records: monitoringPbdFiles
};

export const userBsps: Bsp[] = [
  {
    id: 6983,
    isoCountryCode: 'ES',
    name: 'SPAIN',
    effectiveFrom: '2000-01-01',
    effectiveTo: null,
    version: 3232,
    defaultCurrencyCode: null,
    active: true
  }
];

export const user: User = {
  email: 'martin.sautter@iata.org',
  firstName: null,
  lastName: 'User0BSPES',
  telephone: '021224836126969',
  organization: "organisation ' comilla",
  address: '19 Mayis Cad. 4',
  locality: "icity ' comillas",
  city: null,
  zip: '123',
  country: 'Turquia',
  registerDate: '2000-09-06',
  expiryDate: null,
  active: true,
  id: 69830,
  userType: UserType.IATA,
  iataCode: null,
  language: 'en',
  bspPermissions: [
    {
      bspId: 6983,
      permissions: ['rMonHot']
    }
  ],
  permissions: ['rMonHot'],
  bsps: [
    {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      version: 3232,
      defaultCurrencyCode: null,
      active: true
    }
  ],
  isImpersonating: false,
  defaultIsoc: 'ES',
  template: 'STREAMLINED',
  isSuperUser: false
};
