import { createFeatureSelector, createSelector } from '@ngrx/store';

import { MonitoringPbdFilesState, monitoringPbdFilesStateFeatureKey } from '../store/monitoring-pbd-files.state';

export const selectMonitoringPbdFilesStateFeature = createFeatureSelector<MonitoringPbdFilesState>(
  monitoringPbdFilesStateFeatureKey
);

export const selectMonitoringPbdFiles = createSelector(selectMonitoringPbdFilesStateFeature, state => state.data);
export const selectselectMonitoringPbdFilesQuery = createSelector(
  selectMonitoringPbdFilesStateFeature,
  state => state.query
);
export const selectSearchMonitoringPbdFilesLoading = createSelector(
  selectMonitoringPbdFilesStateFeature,
  state => state.loading.searchMonitoringPbdFiles
);

export const selectHasData = createSelector(selectMonitoringPbdFilesStateFeature, state =>
  Boolean(state.data && state.data.length)
);
