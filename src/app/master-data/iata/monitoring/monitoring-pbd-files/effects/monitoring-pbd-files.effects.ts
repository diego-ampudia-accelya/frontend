import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { MonitoringPbdFilesService } from '../services/monitoring-pbd-files.service';

@Injectable()
export class MonitoringPbdFilesEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly monitoringHotDataService: MonitoringPbdFilesService
  ) {}

  public searchMonitoringPbdFilesEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchMonitoringPbdFilesRequest>>(
        fromActions.searchMonitoringPbdFilesRequest
      ),
      switchMap(({ payload: { query } }) =>
        this.monitoringHotDataService.find(query).pipe(
          map(data =>
            fromActions.searchMonitoringPbdFilesSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchMonitoringPbdFilesFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );
}
