import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromSelectors from '../selectors/monitoring-pbd-files.selectors';
import { MonitoringPbdFilesService } from '../services/monitoring-pbd-files.service';
import * as fromState from '../store/monitoring-pbd-files.state';
import * as fromStub from '../stubs/monitoring-pbd-files.stubs';

import { MonitoringPbdFilesEffects } from './monitoring-pbd-files.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringMasterDataRetEffects', () => {
  let actions$: Observable<any>;
  let effects: MonitoringPbdFilesEffects;
  let service: MonitoringPbdFilesService;

  const initialState = {
    [fromState.monitoringPbdFilesStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringPbdFilesEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectselectMonitoringPbdFilesQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: MonitoringPbdFilesService,
          useValue: {
            find: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<MonitoringPbdFilesEffects>(MonitoringPbdFilesEffects);
    service = TestBed.inject<MonitoringPbdFilesService>(MonitoringPbdFilesService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchMonitoringPbdFilesEffect', () => {
    it('should return searchMonitoringPbdFilesSuccess with paged data', () => {
      const action = fromActions.searchMonitoringPbdFilesRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchMonitoringPbdFilesSuccess({
        payload: {
          pagedData: fromStub.monitoringPbdFilesPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.monitoringPbdFilesPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchMonitoringPbdFilesEffect$).toBeObservable(expected);
    });

    it('should return searchMonitoringPbdFilesFailure when monitoring hot data service find fails', () => {
      const action = fromActions.searchMonitoringPbdFilesRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchMonitoringPbdFilesFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchMonitoringPbdFilesEffect$).toBeObservable(expected);
    });
  });
});
