import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringPbdFilesComponent } from './monitoring-pbd-files.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: MonitoringPbdFilesComponent,
    data: {
      tab: ROUTES.MONITOR_PBD_FILES
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringPbdFilesRoutingModule {}
