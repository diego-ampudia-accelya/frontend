import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { EStatus, MonitoringPbdFileEntry, MonitoringPbdFileEntryFilters } from '../models/monitoring-pbd-files.models';
import { MonitoringPbdStoreFacadeService } from '../store/monitoring-pbd-files-store-facade.service';
import { MonitoringPbdFilesService } from './monitoring-pbd-files.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MonitoringPbdFilesService', () => {
  let service: MonitoringPbdFilesService;
  let httpClientSpy: SpyObject<HttpClient>;

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const monitoringRecordsMock: MonitoringPbdFileEntry[] = [
    {
      bsp: {
        id: 123,
        name: 'Spain',
        isoCountryCode: 'ES'
      },
      filename: 'ESeuDPC_20230503_remitfreq',
      startDate: '2023-01-01',
      endDate: '2023-01-01',
      status: EStatus.Completed
    },
    {
      bsp: {
        id: 123,
        name: 'Spain',
        isoCountryCode: 'ES'
      },
      filename: 'ESeuDPC_20230503_remitfreq',
      startDate: '2023-01-01',
      endDate: '2023-01-01',
      status: EStatus.Completed
    }
  ];

  const queryFilterMock: MonitoringPbdFileEntryFilters = {
    isoCountryCode: [
      {
        id: 123,
        name: 'Spain',
        isoCountryCode: 'ES'
      }
    ],
    filename: 'ESeuDPC_20230503_remitfreq',
    startDate: [new Date('2023-01-01'), new Date('2023-01-01')],
    endDate: [new Date('2023-01-01'), new Date('2023-01-01')],
    status: [EStatus.Completed]
  };

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_PBD_FILES, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MonitoringPbdFilesService,
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: ''
          }
        },
        { provide: HttpClient, useValue: httpClientSpy },
        {
          provide: MonitoringPbdStoreFacadeService,
          useValue: {
            actions: {
              searchMonitoringPbdFiles: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null),
              userBsps$: of(null)
            },
            loading: {
              searchMonitoringPbdFiles$: of(null)
            }
          }
        },
        provideMockStore({ initialState })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MonitoringPbdFilesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    service['bspId'] = 5849;
    expect(service['baseUrl']).toBe('/bsp-management/bsps');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let monitoringReportData: PagedData<MonitoringPbdFileEntry>;
    service['bspId'] = 10;

    const auditDataMock: PagedData<MonitoringPbdFileEntry> = {
      records: monitoringRecordsMock,
      total: monitoringRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      monitoringReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(
      `/bsp-management/bsps/${service['bspId']}/monitoring/pbd?page=0&size=20`
    );
    expect(monitoringReportData).toEqual(auditDataMock);
  }));

  it('should build request query filtering by the filters selected for the user, isoCountryCode, startDate, endDate', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    queryCloneDeep.filterBy['startDate'] = queryFilterMock.startDate;
    queryCloneDeep.filterBy['endDate'] = queryFilterMock.endDate;

    const dataQuery: DataQuery<MonitoringPbdFileEntryFilters> = {
      ...queryCloneDeep,
      filterBy: {
        isoCountryCode: queryFilterMock.isoCountryCode,
        startDate: queryFilterMock.startDate,
        endDate: queryFilterMock.endDate,
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['buildRequestQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual([queryFilterMock.isoCountryCode[0].isoCountryCode]);
    expect(formattedQuery.filterBy['toStartDate']).toEqual('2023-01-01');
    expect(formattedQuery.filterBy['toEndDate']).toEqual('2023-01-01');
  });
});
