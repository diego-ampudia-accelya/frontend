import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { EStatus, MonitoringPbdFileEntryFilters } from '../models/monitoring-pbd-files.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type MonitoringPbdFilesMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class MonitoringPbdFilesFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: MonitoringPbdFileEntryFilters) {
    const filterMappers: Partial<MonitoringPbdFilesMapper<MonitoringPbdFileEntryFilters>> = {
      isoCountryCode: isoCountryCode =>
        `${this.translate('isoCountryCode')} - ${mapJoinMapper(isoCountryCode, 'isoCountryCode')}`,
      filename: filename => `${this.translate('filename')} - ${filename}`,
      status: status => `${this.translate('status')} - ${this.statusMapper(status)}`
    };

    const entries = Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));

    if (filter.startDate?.length) {
      entries.push({
        label: `${this.translate('startDate')} - ${rangeDateFilterTagMapper(filter.startDate)}`,
        keys: ['startDate']
      });
    }

    if (filter.endDate?.length) {
      entries.push({
        label: `${this.translate('endDate')} - ${rangeDateFilterTagMapper(filter.endDate)}`,
        keys: ['endDate']
      });
    }

    return entries;
  }

  private translate(key: string): string {
    return this.translationService.translate(`monitoring-pbd-files.filters.${key}.label`);
  }

  private translateStatus(status: EStatus): string {
    return this.translationService.translate(`monitoring-pbd-files.filters.status.option.${status}`);
  }

  private statusMapper(status: EStatus[]): string {
    return status.map(this.translateStatus.bind(this)).join(', ');
  }
}
