import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { EStatus, MonitoringPbdFileEntryFilters } from '../models/monitoring-pbd-files.models';

import { MonitoringPbdFilesFilterFormatterService } from './monitoring-pbd-files-filter-formatter.service';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

const filters = {
  isoCountryCode: [
    {
      id: 123,
      name: 'Spain',
      isoCountryCode: 'ES'
    }
  ],
  filename: 'ESeuDPC_20230503_remitfreq',
  status: [EStatus.Completed],
  startDate: [new Date('2023-01-01'), new Date('2023-01-01')],
  endDate: [new Date('2023-01-01'), new Date('2023-01-01')]
} as MonitoringPbdFileEntryFilters;

const filtersNot = {
  isoCountryCode: [
    {
      id: 123,
      name: 'Spain',
      isoCountryCode: 'ES'
    }
  ],
  filename: 'ESeuDPC_20230503_remitfreq',
  status: [EStatus.Completed]
} as MonitoringPbdFileEntryFilters;

const translateKey = (key: string) => `monitoring-pbd-files.filters.${key}.label`;
const translateKeyStatus = (key: string) => `monitoring-pbd-files.filters.status.option.${key}`;

describe('MonitoringMasterDataRetFilterFormatterService', () => {
  let formatter: MonitoringPbdFilesFilterFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new MonitoringPbdFilesFilterFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const expectedResult = formatter.format(filters);
    const toEqualResult = [
      {
        keys: ['isoCountryCode'],
        label: `${translateKey('isoCountryCode')} - ${filters.isoCountryCode[0].isoCountryCode}`
      },
      {
        keys: ['filename'],
        label: `${translateKey('filename')} - ${filters.filename}`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - ${translateKeyStatus(filters.status[0])}`
      },
      {
        keys: ['startDate'],
        label: `${translateKey('startDate')} - ${rangeDateFilterTagMapper(filters.startDate)}`
      },
      {
        keys: ['endDate'],
        label: `${translateKey('endDate')} - ${rangeDateFilterTagMapper(filters.endDate)}`
      }
    ];
    expect(expectedResult).toEqual(toEqualResult);
  });

  it('should format all filters except starDate and endDate when these filters are not selected', () => {
    const expectedResult = formatter.format(filtersNot);
    const toEqualResult = [
      {
        keys: ['isoCountryCode'],
        label: `${translateKey('isoCountryCode')} - ${filtersNot.isoCountryCode[0].isoCountryCode}`
      },
      {
        keys: ['filename'],
        label: `${translateKey('filename')} - ${filtersNot.filename}`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - ${translateKeyStatus(filtersNot.status[0])}`
      }
    ];
    expect(expectedResult).toEqual(toEqualResult);
  });
});
