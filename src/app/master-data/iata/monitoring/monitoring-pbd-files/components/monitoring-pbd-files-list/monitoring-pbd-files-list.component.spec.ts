import { ComponentFixture, TestBed } from '@angular/core/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { MonitoringPbdFilesFilterFormatterService } from '../../services/monitoring-pbd-files-filter-formatter.service';
import { MonitoringPbdStoreFacadeService } from '../../store/monitoring-pbd-files-store-facade.service';
import * as fromStub from '../../stubs/monitoring-pbd-files.stubs';
import { MonitoringPbdFilesListComponent } from './monitoring-pbd-files-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { ListViewComponent } from '~app/shared/components/list-view';

describe('MonitoringPbdFilesListComponent', () => {
  let component: MonitoringPbdFilesListComponent;
  let fixture: ComponentFixture<MonitoringPbdFilesListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule)],
      declarations: [
        MonitoringPbdFilesListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: MonitoringPbdFilesFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: MonitoringPbdStoreFacadeService,
          useValue: {
            actions: {
              searchMonitoringPbdFiles: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null),
              userBsps$: of(fromStub.userBsps),
              user$: of(fromStub.user)
            },
            loading: {
              searchMonitoringPbdFiles$: of(null)
            }
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringPbdFilesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
