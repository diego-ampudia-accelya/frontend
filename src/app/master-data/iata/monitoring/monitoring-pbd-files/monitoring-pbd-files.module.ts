import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringPbdFilesListComponent } from './components/monitoring-pbd-files-list/monitoring-pbd-files-list.component';
import { MonitoringPbdFilesEffects } from './effects/monitoring-pbd-files.effects';
import { MonitoringPbdFilesRoutingModule } from './monitoring-pbd-files-routing.module';
import { MonitoringPbdFilesComponent } from './monitoring-pbd-files.component';
import { monitoringPbdFilesReducer } from './reducers/monitoring-pbd-files.reducer';
import { MonitoringPbdFilesFilterFormatterService } from './services/monitoring-pbd-files-filter-formatter.service';
import { MonitoringPbdFilesService } from './services/monitoring-pbd-files.service';
import { MonitoringPbdStoreFacadeService } from './store/monitoring-pbd-files-store-facade.service';
import { monitoringPbdFilesStateFeatureKey } from './store/monitoring-pbd-files.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MonitoringPbdFilesRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(monitoringPbdFilesStateFeatureKey, monitoringPbdFilesReducer),
    EffectsModule.forFeature([MonitoringPbdFilesEffects])
  ],
  declarations: [MonitoringPbdFilesComponent, MonitoringPbdFilesListComponent],
  providers: [MonitoringPbdFilesService, MonitoringPbdStoreFacadeService, MonitoringPbdFilesFilterFormatterService]
})
export class MonitoringPbdFilesModule {}
