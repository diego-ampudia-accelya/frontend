import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import { MonitoringPbdFileEntry, MonitoringPbdFileEntryFilters } from '../models/monitoring-pbd-files.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchMonitoringPbdFilesRequest = createAction(
  '[Monitoring Pbd Files]Search Monitoring Pbd Files Request',
  props<{ payload: { query?: DataQuery<MonitoringPbdFileEntryFilters> } }>()
);

export const searchMonitoringPbdFilesSuccess = createAction(
  '[Monitoring Pbd Files]Search Monitoring Pbd Files Success',
  props<{ payload: { pagedData: PagedData<MonitoringPbdFileEntry> } }>()
);

export const searchMonitoringPbdFilesFailure = createAction(
  '[Monitoring Pbd Files]Search Monitoring Pbd Files Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);
