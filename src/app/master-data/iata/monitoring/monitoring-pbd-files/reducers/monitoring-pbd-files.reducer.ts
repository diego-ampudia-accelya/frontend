import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { initialState } from '../store/monitoring-pbd-files.state';

export const monitoringPbdFilesReducer = createReducer(
  initialState,
  on(fromActions.searchMonitoringPbdFilesRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchMonitoringPbdFiles: true
    },
    error: {
      ...state.error,
      searchMonitoringPbdFiles: null
    }
  })),
  on(fromActions.searchMonitoringPbdFilesFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchMonitoringPbdFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringPbdFiles: error
    }
  })),
  on(fromActions.searchMonitoringPbdFilesSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchMonitoringPbdFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringPbdFiles: null
    }
  }))
);
