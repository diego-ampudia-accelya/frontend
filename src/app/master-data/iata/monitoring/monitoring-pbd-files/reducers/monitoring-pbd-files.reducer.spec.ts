import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromState from '../store/monitoring-pbd-files.state';
import * as fromStub from '../stubs/monitoring-pbd-files.stubs';

import * as fromReducer from './monitoring-pbd-files.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoringPbdFilesReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.monitoringPbdFilesReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchMonitoringPbdFilesRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchMonitoringPbdFilesRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.monitoringPbdFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringPbdFiles: true
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringPbdFiles: null
        }
      });
    });
  });

  describe('searchMonitoringPbdFilesFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchMonitoringPbdFilesFailure({ payload: { error } });
      const result = fromReducer.monitoringPbdFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringPbdFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringPbdFiles: error
        }
      });
    });
  });

  describe('searchMonitoringPbdFilesSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchMonitoringPbdFilesSuccess({
        payload: { pagedData: fromStub.monitoringPbdFilesPagedData }
      });
      const result = fromReducer.monitoringPbdFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.monitoringPbdFilesPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.monitoringPbdFilesPagedData.pageNumber,
            size: fromStub.monitoringPbdFilesPagedData.pageSize,
            totalElements: fromStub.monitoringPbdFilesPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringPbdFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringPbdFiles: null
        }
      });
    });
  });
});
