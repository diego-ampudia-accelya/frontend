import { HttpErrorResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromSelectors from '../selectors/monitoring-hot-data.selectors';
import { MonitoringHotDataService } from '../services/monitoring-hot-data.service';
import * as fromState from '../store/monitoring-hot-data.state';
import * as fromStub from '../stubs/monitoring-hot-data.stubs';

import { MonitoringHotDataEffects } from './monitoring-hot-data.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

describe('MonitoringHotDataEffects', () => {
  let actions$: Observable<any>;
  let effects: MonitoringHotDataEffects;
  let service: MonitoringHotDataService;
  let dialogService: DialogService;

  const initialState = {
    [fromState.monitoringHotDataStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringHotDataEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectMonitoringHotDataQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: MonitoringHotDataService,
          useValue: {
            find: () => {},
            download: () => {}
          }
        },
        {
          provide: DialogService,
          useValue: {
            open: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<MonitoringHotDataEffects>(MonitoringHotDataEffects);
    service = TestBed.inject<MonitoringHotDataService>(MonitoringHotDataService);
    dialogService = TestBed.inject<DialogService>(DialogService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchMonitoringHotDataEffect', () => {
    it('should return searchMonitoringHotDataSuccess with paged data', () => {
      const action = fromActions.searchMonitoringHotDataRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchMonitoringHotDataSuccess({
        payload: {
          pagedData: fromStub.monitoringHotDataPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.monitoringHotDataPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchMonitoringHotDataEffect$).toBeObservable(expected);
    });

    it('should return searchMonitoringHotDataFailure when monitoring hot data service find fails', () => {
      const action = fromActions.searchMonitoringHotDataRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchMonitoringHotDataFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchMonitoringHotDataEffect$).toBeObservable(expected);
    });
  });

  describe('downloadMonitoringHotDataEffect', () => {
    it('when downloadMonitoringHotDataRequest is called should open download dialog', fakeAsync(() => {
      const action = fromActions.downloadMonitoringHotDataRequest();

      spyOn(dialogService, 'open');

      actions$ = new BehaviorSubject<unknown>(action);

      effects.downloadMonitoringHotDataEffect$.subscribe();

      tick();

      expect(dialogService.open).toHaveBeenCalledWith(DownloadFileComponent, {
        data: {
          title: 'BUTTON.DEFAULT.DOWNLOAD',
          footerButtonsType: FooterButton.Download,
          downloadQuery: {
            ...defaultQuery,
            filterBy: {
              ...defaultQuery?.filterBy
            }
          }
        },
        apiService: service
      });
    }));
  });
});
