import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromSelectors from '../selectors/monitoring-hot-data.selectors';
import { MonitoringHotDataService } from '../services/monitoring-hot-data.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

@Injectable()
export class MonitoringHotDataEffects {
  public constructor(
    private readonly actions$: Actions,
    private readonly monitoringHotDataService: MonitoringHotDataService,
    private readonly store: Store<unknown>,
    private readonly dialogService: DialogService
  ) {}

  public searchMonitoringHotDataEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchMonitoringHotDataRequest>>(fromActions.searchMonitoringHotDataRequest),
      switchMap(({ payload: { query } }) =>
        this.monitoringHotDataService.find(query).pipe(
          map(data =>
            fromActions.searchMonitoringHotDataSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchMonitoringHotDataFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public downloadMonitoringHotDataEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.downloadMonitoringHotDataRequest>>(
          fromActions.downloadMonitoringHotDataRequest
        ),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectMonitoringHotDataQuery))),
        tap(([_, query]) => {
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.monitoringHotDataService
          });
        })
      ),
    { dispatch: false }
  );
}
