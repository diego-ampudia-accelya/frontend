import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { EStatus, MonitoringHotDataEntry, MonitoringHotDataEntryFilters } from '../models/monitoring-hot-data.models';
import { MonitoringHotDataFacadeService } from '../store/monitoring-hot-data-store-facade.service';
import { MonitoringHotDataService } from './monitoring-hot-data.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MonitoringHotDataService', () => {
  let service: MonitoringHotDataService;
  let httpClientSpy: SpyObject<HttpClient>;

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const monitoringRecordsMock: MonitoringHotDataEntry[] = [
    {
      isoCountryCode: 'ES',
      name: 'SPAIN',
      startProcessingTime: 'start',
      endProcessingTime: 'endtime',
      status: EStatus.COMPLETED,
      totalTransactions: 20,
      acceptedTransactions: 20,
      errors: 0,
      warnings: 45
    }
  ];

  const monitoringFilterMock: MonitoringHotDataEntryFilters[] = [
    {
      isoCountryCode: [
        {
          id: 1,
          name: 'SPAIN',
          isoCountryCode: 'ES'
        }
      ],
      name: 'SPAIN',
      startProcessingTime: [new Date('2023-01-01')],
      endProcessingTime: [new Date('2023-01-01')],
      status: [EStatus.COMPLETED],
      salesQueryProcessorStatus: ['status1']
    }
  ];

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_HOT_DATA, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  const monitoringHotDataFacadeServiceSpy = {
    actions: {
      searchMonitoringHotData: () => {},
      downloadMonitoringHotData: () => {}
    },
    selectors: {
      query$: of(null),
      data$: of(null),
      hasData$: of(null)
    },
    loading: {
      searchMonitoringHotData$: of(null)
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MonitoringHotDataService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: MonitoringHotDataFacadeService, useValue: monitoringHotDataFacadeServiceSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        provideMockStore({ initialState })
      ]
    });

    service = TestBed.inject(MonitoringHotDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call HttpClient get method when download method is called', () => {
    service.download(query, DownloadFormat.CSV);

    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should subscribe to find method and return page data as MonitoringHotDataEntry', fakeAsync(() => {
    let monitoringReportData: PagedData<MonitoringHotDataEntry>;
    const auditDataMock: PagedData<MonitoringHotDataEntry> = {
      records: monitoringRecordsMock,
      total: monitoringRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      monitoringReportData = res;
    });
    tick();

    expect(monitoringReportData).toEqual(auditDataMock);
  }));

  it('should format the query filtering by the filters selected for the user, isoCountryCode, startProcessingTime and endProcessingTime', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const isoCountryCodeFormatted = ['ES'];
    const time = [new Date('01/01/2023'), new Date('01/01/2023')];
    queryCloneDeep.filterBy.startProcessingTime = time;
    queryCloneDeep.filterBy.endProcessingTime = time;

    const dataQuery: DataQuery<MonitoringHotDataEntryFilters> = {
      ...queryCloneDeep,
      filterBy: {
        isoCountryCode: monitoringFilterMock[0].isoCountryCode,
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual(isoCountryCodeFormatted);
    expect(formattedQuery.filterBy['startProcessingDateAfter']).toEqual('2023-01-01');
    expect(formattedQuery.filterBy['endProcessingDateBefore']).toEqual('2023-01-01');
  });

  it('should startProcessingDateBefore and endProcessingDateBefore filters to be defined when the user selects start and end processing time', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const time = [new Date('01/01/2023'), new Date('01/01/2023')];
    queryCloneDeep.filterBy.startProcessingTime = time[0];
    queryCloneDeep.filterBy.endProcessingTime = time[0];

    const dataQuery: DataQuery<MonitoringHotDataEntryFilters> = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);
    expect(formattedQuery.filterBy['startProcessingDateBefore']).toBeDefined();
    expect(formattedQuery.filterBy['endProcessingDateBefore']).toBeDefined();
  });
});
