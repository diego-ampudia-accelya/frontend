import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { EStatus, MonitoringHotDataEntryFilters } from '../models/monitoring-hot-data.models';

import { MonitoringHotDataFilterFormatterService } from './monitoring-hot-data-filter-formatter.service';

const filters = {
  isoCountryCode: [
    {
      id: 1,
      name: 'SPAIN',
      isoCountryCode: 'ES'
    }
  ],
  name: 'SPAIN',
  startProcessingTime: [new Date('2023-01-01')],
  endProcessingTime: [new Date('2023-01-01')],
  salesQueryProcessorStatus: ['status1'],
  status: [EStatus.COMPLETED]
} as MonitoringHotDataEntryFilters;

const translateKey = (key: string) => `monitoring-hot-data.filters.${key}.label`;
const translateStatus = (status: EStatus) => `monitoring-hot-data.filters.status.option.${status}`;

describe('MonitoringHotDataFilterFormatterService', () => {
  let formatter: MonitoringHotDataFilterFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new MonitoringHotDataFilterFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['isoCountryCode'],
        label: `${translateKey('isoCountryCode')} - ES`
      },
      {
        keys: ['name'],
        label: `${translateKey('name')} - SPAIN`
      },
      {
        keys: ['startProcessingTime'],
        label: `${translateKey('startProcessingTime')} - 01/01/2023`
      },
      {
        keys: ['endProcessingTime'],
        label: `${translateKey('endProcessingTime')} - 01/01/2023`
      },
      {
        keys: ['salesQueryProcessorStatus'],
        label: `${translateKey('salesQueryProcessor')} - Status1`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - ${translateStatus(filters.status[0])}`
      }
    ]);
  });
});
