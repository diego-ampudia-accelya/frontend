import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  MonitoringHotDataEntry,
  MonitoringHotDataEntryFilters,
  MonitoringHotDataEntryFiltersBE
} from '../models/monitoring-hot-data.models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class MonitoringHotDataService implements Queryable<MonitoringHotDataEntry> {
  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  private baseUrl = `${this.appConfiguration.baseApiPath}/monitoring-service/hot-files`;

  public find(query: DataQuery<MonitoringHotDataEntryFilters>): Observable<PagedData<MonitoringHotDataEntry>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<MonitoringHotDataEntry>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<MonitoringHotDataEntryFilters>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<MonitoringHotDataEntryFilters>): RequestQuery<MonitoringHotDataEntryFiltersBE> {
    const { startProcessingTime, endProcessingTime, isoCountryCode, ...filterBy } = query.filterBy;

    const bspsFormatted = Array.isArray(isoCountryCode) ? isoCountryCode : [isoCountryCode];

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        isoCountryCode: isoCountryCode && bspsFormatted.map(bsp => bsp.isoCountryCode),
        startProcessingDateAfter: startProcessingTime && toShortIsoDate(startProcessingTime[0]),
        startProcessingDateBefore:
          startProcessingTime && toShortIsoDate(startProcessingTime[1] || startProcessingTime[0]),
        endProcessingDateAfter: endProcessingTime && toShortIsoDate(endProcessingTime[0]),
        endProcessingDateBefore: endProcessingTime && toShortIsoDate(endProcessingTime[1] || endProcessingTime[0])
      }
    });
  }
}
