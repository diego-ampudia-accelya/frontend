import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { capitalize } from 'lodash';

import { EStatus, MonitoringHotDataEntryFilters } from '../models/monitoring-hot-data.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type MonitoringHotDataMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class MonitoringHotDataFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: MonitoringHotDataEntryFilters) {
    const filterMappers: Partial<MonitoringHotDataMapper<MonitoringHotDataEntryFilters>> = {
      isoCountryCode: bsps => {
        const bspsFormatted = Array.isArray(bsps) ? bsps : [bsps];

        return `${this.translate('isoCountryCode')} - ${bspsFormatted.map(bsp => bsp.isoCountryCode).join(', ')}`;
      },
      name: name => `${this.translate('name')} - ${name}`,
      status: status => `${this.translate('status')} - ${status.map(s => this.translateStatus(s)).join(', ')}`,
      startProcessingTime: startProcessingTime =>
        `${this.translate('startProcessingTime')} - ${rangeDateFilterTagMapper(startProcessingTime)}`,
      endProcessingTime: endProcessingTime =>
        `${this.translate('endProcessingTime')} - ${rangeDateFilterTagMapper(endProcessingTime)}`,
      salesQueryProcessorStatus: salesQueryProcessorStatus =>
        `${this.translate('salesQueryProcessor')} - ${salesQueryProcessorStatus.map(s => capitalize(s)).join(', ')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`monitoring-hot-data.filters.${key}.label`);
  }

  private translateStatus(status: EStatus): string {
    return this.translationService.translate(`monitoring-hot-data.filters.status.option.${status}`);
  }
}
