import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { combineLatest, Observable } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';

import { MONITORING_HOT_DATA } from '../../constants/monitoring-hot-data.constants';
import {
  EStatus,
  MonitoringHotDataEntry,
  MonitoringHotDataEntryFilters,
  salesQueryProcessorFilterOptions
} from '../../models/monitoring-hot-data.models';
import { MonitoringHotDataFilterFormatterService } from '../../services/monitoring-hot-data-filter-formatter.service';
import { MonitoringHotDataService } from '../../services/monitoring-hot-data.service';
import { MonitoringHotDataFacadeService } from '../../store/monitoring-hot-data-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions, ROUTES } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-monitoring-hot-data-list',
  templateUrl: './monitoring-hot-data-list.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: MonitoringHotDataService }
  ]
})
export class MonitoringHotDataListComponent implements OnInit {
  public columns: TableColumn[] = MONITORING_HOT_DATA.COLUMNS;
  public isBspFilterMultiple: boolean;
  public userBsps: DropdownOption<BspDto>[];
  public isBspFilterLocked: boolean;

  public salesQueryProcessorOptions = Object.entries(salesQueryProcessorFilterOptions).map(
    ([key, value]: [string, string]) => ({
      value,
      label: this.translationService.translate(`monitoring-hot-data.filters.salesQueryProcessor.option.${key}`)
    })
  );
  public statusOptions = Object.entries(EStatus).map(([key, value]: [string, string]) => ({
    value,
    label: this.translationService.translate(`monitoring-hot-data.filters.status.option.${key}`)
  }));

  public listActions: Array<{ action: GridTableActionType; disabled?: boolean; group?: string }>;
  public filterFormGroup: FormGroup;
  public predefinedFilters: {
    isoCountryCode: Bsp;
  };

  private bspControl: FormControl;
  private hasAllBspsPermission: boolean;
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private defaultBsp: Bsp;
  private loggedUser: User;
  private get selectedBsp(): Bsp {
    const bspControl = FormUtil.get<MonitoringHotDataEntryFilters>(this.filterFormGroup, 'isoCountryCode');

    return bspControl.value;
  }
  private set selectedBsp(selectedBsp: Bsp) {
    const bspControl = FormUtil.get<MonitoringHotDataEntryFilters>(this.filterFormGroup, 'isoCountryCode');
    const selectedBspAdapted = this.isBspFilterMultiple ? [selectedBsp] : selectedBsp;

    bspControl.setValue(selectedBspAdapted);
  }

  constructor(
    public readonly dataSource: QueryableDataSource<MonitoringHotDataEntry>,
    public readonly monitoringHotDataFilterFormatterService: MonitoringHotDataFilterFormatterService,
    private readonly monitoringHotDataStoreFacadeService: MonitoringHotDataFacadeService,
    private readonly translationService: L10nTranslationService,
    private readonly permissionService: PermissionsService,
    private readonly bspsDictionaryService: BspsDictionaryService,
    private readonly queryStorage: DefaultQueryStorage,
    protected router: Router
  ) {}

  public get title$(): Observable<string> {
    return this.dataSource.data$.pipe(
      map(rows => `${this.translationService.translate('monitoring-hot-data.view.title')} (${rows.length || 0})`)
    );
  }

  public ngOnInit(): void {
    this.initializeLoggedUser();
    this.initializePermissions();
    this.initializeFilterFormGroup();

    this.initializeBspFilterAndLoadData();
  }

  public onGetActionList(hotDataEntry: MonitoringHotDataEntry): void {
    this.listActions =
      hotDataEntry.warnings > 0 || hotDataEntry.errors > 0
        ? [{ action: GridTableActionType.HotDetails }]
        : [{ action: GridTableActionType.HotDetails, disabled: true }];
  }

  public onActionClick({ action, row }): void {
    if (action.actionType === GridTableActionType.HotDetails) {
      this.onDetails(row.name);
    }
  }

  public loadData(query: DataQuery<MonitoringHotDataEntryFilters>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    this.monitoringHotDataStoreFacadeService.actions.downloadMonitoringHotData();
  }

  private initializeLoggedUser() {
    this.monitoringHotDataStoreFacadeService.selectors.user$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });
  }

  private initializeBspFilterAndLoadData(): void {
    this.initializeLeanBspFilterAndDefaultBsp$().subscribe(() => {
      this.storedQuery = this.queryStorage.get();
      this.loadData(this.storedQuery);
    });
  }

  private initializeLeanBspFilterAndDefaultBsp$(): Observable<DropdownOption<BspDto>[]> {
    if (this.loggedUser.userType.toUpperCase() === UserType.IATA.toUpperCase() && this.hasAllBspsPermission) {
      return this.initializeMcIataBspFilterAndDefaultBsp$();
    } else {
      return combineLatest([
        this.monitoringHotDataStoreFacadeService.selectors.userBsps$,
        this.monitoringHotDataStoreFacadeService.selectors.user$
      ]).pipe(
        first(),
        tap(() => (this.isBspFilterMultiple = this.hasAllBspsPermission)),
        map(([bspList, user]) => {
          this.setDefaultBsp(bspList, user);
          if (!this.isBspFilterMultiple || bspList.length === 1) {
            this.setPredefinedFilters();
          }

          return [bspList] as [Bsp[]];
        }),
        map(([bspList]) => bspList.map(bsp => toValueLabelObjectBsp(bsp))),
        tap(bspList => (this.userBsps = bspList)),
        tap(bspList => (this.isBspFilterLocked = this.setBspFilterLockedCases(bspList)))
      );
    }
  }

  private initializeMcIataBspFilterAndDefaultBsp$(): Observable<DropdownOption<BspDto>[]> {
    return combineLatest([
      this.bspsDictionaryService.getAllBspDropdownOptions(),
      this.monitoringHotDataStoreFacadeService.selectors.user$
    ]).pipe(
      first(),
      tap(() => (this.isBspFilterMultiple = this.hasAllBspsPermission)),
      map(([bspList, user]) => {
        this.getDefaultAndSetSelectedBspForMcIata(bspList, user);

        if (!this.isBspFilterMultiple || bspList.length === 1) {
          this.setPredefinedFilters();
        }

        return bspList;
      }),
      tap(bspList => (this.userBsps = bspList)),
      tap(bspList => (this.isBspFilterLocked = this.setBspFilterLockedCases(bspList)))
    );
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      isoCountryCode: this.selectedBsp || this.defaultBsp
    };
  }

  private getDefaultAndSetSelectedBspForMcIata(bspList: DropdownOption<BspDto>[], user: User): void {
    this.defaultBsp = user.bsps.find(bspUser =>
      bspList.some(bsp => bsp.value.isoCountryCode === bspUser.isoCountryCode)
    );
    this.selectedBsp = this.defaultBsp;
  }

  private setBspFilterLockedCases(bspList: DropdownOption<BspDto>[]): boolean {
    return bspList.length === 1 || !this.hasAllBspsPermission;
  }

  private onDetails(file: string): void {
    this.router.navigate([ROUTES.MONITOR_HOT_DATA_DETAILS.url, file]);
  }

  private initializePermissions(): void {
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private initializeFilterFormGroup(): void {
    this.bspControl = new FormControl();

    this.filterFormGroup = new FormGroup({
      isoCountryCode: this.bspControl,
      name: new FormControl(null, []),
      status: new FormControl(null, []),
      startProcessingTime: new FormControl(null, []),
      endProcessingTime: new FormControl(null, []),
      salesQueryProcessorStatus: new FormControl(null, [])
    });
  }

  private setDefaultBsp(bspList: Bsp[], user: User): void {
    this.defaultBsp = bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
    this.selectedBsp = this.defaultBsp;
  }
}
