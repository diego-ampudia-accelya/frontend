import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { MonitoringHotDataEntry, MonitoringHotDataEntryFilters } from '../../models/monitoring-hot-data.models';
import { MonitoringHotDataFilterFormatterService } from '../../services/monitoring-hot-data-filter-formatter.service';
import { MonitoringHotDataService } from '../../services/monitoring-hot-data.service';
import { MonitoringHotDataFacadeService } from '../../store/monitoring-hot-data-store-facade.service';
import * as fromStub from '../../stubs/monitoring-hot-data.stubs';
import { MonitoringHotDataListComponent } from './monitoring-hot-data-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { FormUtil } from '~app/shared/helpers';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { AppConfigurationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('MonitoringHotDataListComponent', () => {
  let component: MonitoringHotDataListComponent;
  let fixture: ComponentFixture<MonitoringHotDataListComponent>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const monitoringHotDataServiceSpy: SpyObject<MonitoringHotDataService> = createSpyObject(MonitoringHotDataService);
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  const bspsDictionaryServiceSpy: SpyObject<BspsDictionaryService> = createSpyObject(BspsDictionaryService);
  const routerSpy: SpyObject<Router> = createSpyObject(Router);

  const monitoringHotDataFacadeServiceSpy = {
    actions: {
      searchMonitoringHotData: () => {},
      downloadMonitoringHotData: () => {}
    },
    selectors: {
      query$: of(null),
      data$: of(null),
      hasData$: of(null),
      userBsps$: of(fromStub.userBsps),
      user$: of(fromStub.user)
    },
    loading: {
      searchMonitoringHotData$: of(null)
    }
  };

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_HOT_DATA, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true),
    data$: of(defaultQuery)
  });

  const bspListMock = [
    { value: { id: 7376, isoCountryCode: 'IL', name: 'ISRAEL' }, label: 'IL - ISRAEL' },
    { value: { id: 6554, isoCountryCode: 'A6', name: 'AUSTRALIA' }, label: 'A6 - AUSTRALIA' },
    { value: { id: 6671, isoCountryCode: 'BG', name: 'Bulgaria' }, label: 'BG - Bulgaria' },
    { value: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' }, label: 'ES - SPAIN' },
    { value: { id: 7784, isoCountryCode: 'MT', name: 'MALTA' }, label: 'MT - MALTA' }
  ] as DropdownOption<BspDto>[];

  const bspList = [
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      version: 3212
    },
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6984,
      isoCountryCode: 'MT',
      name: 'SPAIN',
      version: 3212
    }
  ] as Bsp[];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MonitoringHotDataListComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockStore({ initialState }),
        AppConfigurationService,
        L10nTranslationService,
        FormBuilder,
        MonitoringHotDataFilterFormatterService
      ]
    })
      .overrideComponent(MonitoringHotDataListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: MonitoringHotDataService, useValue: monitoringHotDataServiceSpy },
            { provide: PermissionsService, useValue: permissionsServiceSpy },
            { provide: Router, useValue: routerSpy },
            { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
            { provide: MonitoringHotDataFacadeService, useValue: monitoringHotDataFacadeServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringHotDataListComponent);
    component = fixture.componentInstance;
    permissionsServiceSpy.hasPermission.and.returnValue(true);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call monitoringHotDataFacadeService downloadMonitoringHotData action when onDownload method is called', () => {
    const downloadMonitoringHotDataSpy = spyOn(monitoringHotDataFacadeServiceSpy.actions, 'downloadMonitoringHotData');
    component.onDownload();

    expect(downloadMonitoringHotDataSpy).toHaveBeenCalled();
  });

  it('should set disabled property of listActions to true if errors or warnings are not > 0', () => {
    const hotDataEntryMock = {
      errors: 0,
      warnings: 0
    } as unknown as MonitoringHotDataEntry;
    component.onGetActionList(hotDataEntryMock);

    component.listActions =
      hotDataEntryMock.warnings > 0 || hotDataEntryMock.errors > 0
        ? [{ action: GridTableActionType.HotDetails }]
        : [{ action: GridTableActionType.HotDetails, disabled: true }];

    expect(component.listActions).toEqual([{ action: GridTableActionType.HotDetails, disabled: true }]);
  });

  it('should not set disabled property of listActions if errors or warnings are > 0', () => {
    const hotDataEntryMock = {
      errors: 0,
      warnings: 1163
    } as unknown as MonitoringHotDataEntry;
    component.onGetActionList(hotDataEntryMock);

    component.listActions =
      hotDataEntryMock.warnings > 0 || hotDataEntryMock.errors > 0
        ? [{ action: GridTableActionType.HotDetails }]
        : [{ action: GridTableActionType.HotDetails, disabled: true }];

    expect(component.listActions).toEqual([{ action: GridTableActionType.HotDetails }]);
  });

  it('should call the onDetails method when onActionClick is called with actionType as "hotDetails"', () => {
    const onDetailsSpy = spyOn<any>(component, 'onDetails');
    component.onActionClick({ action: { actionType: GridTableActionType.HotDetails }, row: { name: 'rowName' } });

    expect(onDetailsSpy).toHaveBeenCalled();
  });

  it('should not call the onDetails method when onActionClick is called with an actionType value different from "hotDetails"', () => {
    const onDetailsSpy = spyOn<any>(component, 'onDetails');
    component.onActionClick({ action: { actionType: GridTableActionType.View }, row: { name: 'rowName' } });

    expect(onDetailsSpy).not.toHaveBeenCalled();
  });

  it('should call naviage method from router with a given url when onDetails is called', () => {
    component['onDetails']('file');

    expect(routerSpy.navigate).toHaveBeenCalledWith([`${ROUTES.MONITOR_HOT_DATA_DETAILS.url}`, 'file']);
  });

  it('should get Default And Set Selected Bsp For Iata Multicountry', () => {
    component['getDefaultAndSetSelectedBspForMcIata'](bspListMock, initialState.auth.user);

    expect(component['defaultBsp']).toEqual({
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    });
  });

  it('should check if Bsp filter is Locked', () => {
    component['hasAllBspsPermission'] = false;
    component['hasLeanPermission'] = false;
    const isBspFilterLocked = component['setBspFilterLockedCases'](bspListMock);

    expect(isBspFilterLocked).toBe(true);
  });

  it('should get default and set selected Bsp For Iata Multicountry', () => {
    component['getDefaultAndSetSelectedBspForMcIata'](bspListMock, initialState.auth.user);

    expect(component['defaultBsp']).toEqual({
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    });
  });

  it('should set default BSP by calling setDefaultBsp method', () => {
    component['setDefaultBsp'](bspList, initialState.auth.user);

    expect(component['defaultBsp']).toEqual(bspList[0]);
  });

  it('should call loadData method when the user susbcribes to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    const loadDataSpy = spyOn(component, 'loadData');
    spyOn<any>(component, 'initializeLeanBspFilterAndDefaultBsp$').and.returnValue(of({}));

    component['initializeBspFilterAndLoadData']();
    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(loadDataSpy).toHaveBeenCalled();
  }));

  it('should not set predefined filters when isBspFilterMultiple is not true and the user suscribed to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    const setPredefinedFiltersSpy = spyOn<any>(component, 'setPredefinedFilters');
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListMock]);

    spyOn<any>(component, 'getDefaultAndSetSelectedBspForMcIata').and.stub();

    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(setPredefinedFiltersSpy).not.toHaveBeenCalled();
  }));

  it('should set predefined filters when the user has not hasAllBspsPermission and it suscribed to initializeLeanBspFilterAndDefaultBsp$', fakeAsync(() => {
    component.isBspFilterMultiple = true;
    component['hasAllBspsPermission'] = false;
    const setPredefinedFiltersSpy = spyOn<any>(component, 'setPredefinedFilters');
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListMock]);

    spyOn<any>(component, 'getDefaultAndSetSelectedBspForMcIata').and.stub();

    component['initializeLeanBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(setPredefinedFiltersSpy).toHaveBeenCalled();
  }));

  it('should select the correct BSP after patching the value', () => {
    const expectedSelectedBsp: Bsp = {
      id: 1,
      name: 'SPAIN',
      isoCountryCode: 'ES'
    };
    FormUtil.get<MonitoringHotDataEntryFilters>(component.filterFormGroup, 'isoCountryCode').patchValue(
      expectedSelectedBsp
    );
    const result = component['selectedBsp'];

    expect(result).toBe(expectedSelectedBsp);
  });

  it('should defined predefinedFilters when setPredefinedFilters is called', () => {
    component['setPredefinedFilters']();

    expect(component.predefinedFilters).toBeDefined();
  });
});
