import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromState from '../store/monitoring-hot-data.state';
import * as fromStub from '../stubs/monitoring-hot-data.stubs';

import * as fromReducer from './monitoring-hot-data.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoringHotDataReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.monitoringHotDataReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchMonitoringHotDataRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchMonitoringHotDataRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.monitoringHotDataReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringHotData: true
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringHotData: null
        }
      });
    });
  });

  describe('searchMonitoringHotDataFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchMonitoringHotDataFailure({ payload: { error } });
      const result = fromReducer.monitoringHotDataReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringHotData: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringHotData: error
        }
      });
    });
  });

  describe('searchMonitoringHotDataSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchMonitoringHotDataSuccess({
        payload: { pagedData: fromStub.monitoringHotDataPagedData }
      });
      const result = fromReducer.monitoringHotDataReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.monitoringHotDataPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.monitoringHotDataPagedData.pageNumber,
            size: fromStub.monitoringHotDataPagedData.pageSize,
            totalElements: fromStub.monitoringHotDataPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringHotData: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringHotData: null
        }
      });
    });
  });
});
