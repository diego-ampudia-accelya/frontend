import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { initialState } from '../store/monitoring-hot-data.state';

export const monitoringHotDataReducer = createReducer(
  initialState,
  on(fromActions.searchMonitoringHotDataRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchMonitoringHotData: true
    },
    error: {
      ...state.error,
      searchMonitoringHotData: null
    }
  })),
  on(fromActions.searchMonitoringHotDataFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchMonitoringHotData: false
    },
    error: {
      ...state.error,
      searchMonitoringHotData: error
    }
  })),
  on(fromActions.searchMonitoringHotDataSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchMonitoringHotData: false
    },
    error: {
      ...state.error,
      searchMonitoringHotData: null
    }
  }))
);
