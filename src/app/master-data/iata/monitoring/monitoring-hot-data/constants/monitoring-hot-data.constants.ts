/* eslint-disable @typescript-eslint/naming-convention */
import { capitalize } from 'lodash';
import moment from 'moment-mini';

import { GridColumn } from '~app/shared/models';

export const MONITORING_HOT_DATA = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'isoCountryCode',
        name: 'monitoring-hot-data.columns.isoCountryCode',
        resizeable: false,
        draggable: false,
        width: 60
      },
      {
        prop: 'name',
        name: 'monitoring-hot-data.columns.name',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'startProcessingTime',
        name: 'monitoring-hot-data.columns.startProcessingTime',
        resizeable: false,
        draggable: false,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'endProcessingTime',
        name: 'monitoring-hot-data.columns.endProcessingTime',
        resizeable: true,
        draggable: false,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'status',
        name: 'monitoring-hot-data.columns.status',
        resizeable: true,
        draggable: false,
        cellTemplate: 'statusExtendedTmpl'
      },
      {
        prop: 'totalTransactions',
        name: 'monitoring-hot-data.columns.totalTransactions',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'acceptedTransactions',
        name: 'monitoring-hot-data.columns.acceptedTransactions',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'errors',
        name: 'monitoring-hot-data.columns.errors',
        resizeable: true,
        draggable: false,
        width: 80
      },
      {
        prop: 'warnings',
        name: 'monitoring-hot-data.columns.warnings',
        resizeable: true,
        draggable: false,
        width: 80
      },
      {
        prop: 'salesQueryProcessorStatus',
        name: 'monitoring-hot-data.columns.salesQueryProcessor',
        resizeable: true,
        draggable: false,
        pipe: {
          transform: sales => capitalize(sales)
        }
      }
    ];
  }
};
