import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringHotDataComponent } from './monitoring-hot-data.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: MonitoringHotDataComponent,
    data: {
      tab: ROUTES.MONITOR_HOT_DATA
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringHotDataRoutingModule {}
