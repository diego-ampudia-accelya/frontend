import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringHotDataListComponent } from './components/monitoring-hot-data-list/monitoring-hot-data-list.component';
import { MonitoringHotDataEffects } from './effects/monitoring-hot-data.effects';
import { MonitoringHotDataRoutingModule } from './monitoring-hot-data-routing.module';
import { MonitoringHotDataComponent } from './monitoring-hot-data.component';
import { monitoringHotDataReducer } from './reducers/monitoring-hot-data.reducer';
import { MonitoringHotDataFilterFormatterService } from './services/monitoring-hot-data-filter-formatter.service';
import { MonitoringHotDataService } from './services/monitoring-hot-data.service';
import { MonitoringHotDataFacadeService } from './store/monitoring-hot-data-store-facade.service';
import { monitoringHotDataStateFeatureKey } from './store/monitoring-hot-data.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MonitoringHotDataRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(monitoringHotDataStateFeatureKey, monitoringHotDataReducer),
    EffectsModule.forFeature([MonitoringHotDataEffects])
  ],
  declarations: [MonitoringHotDataComponent, MonitoringHotDataListComponent],
  providers: [MonitoringHotDataService, MonitoringHotDataFacadeService, MonitoringHotDataFilterFormatterService]
})
export class MonitoringHotDataModule {}
