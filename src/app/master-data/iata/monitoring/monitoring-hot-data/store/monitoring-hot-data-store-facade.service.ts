import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { MonitoringHotDataEntryFilters } from '../models/monitoring-hot-data.models';
import * as fromSelectors from '../selectors/monitoring-hot-data.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class MonitoringHotDataFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchMonitoringHotData: (query?: DataQuery<MonitoringHotDataEntryFilters>) =>
        this.store.dispatch(fromActions.searchMonitoringHotDataRequest({ payload: { query } })),
      downloadMonitoringHotData: () => this.store.dispatch(fromActions.downloadMonitoringHotDataRequest())
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectMonitoringHotDataQuery),
      data$: this.store.select(fromSelectors.selectMonitoringHotData),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser)
    };
  }

  public get loading() {
    return {
      searchMonitoringHotData: this.store.select(fromSelectors.selectSearchMonitoringHotDataLoading)
    };
  }
}
