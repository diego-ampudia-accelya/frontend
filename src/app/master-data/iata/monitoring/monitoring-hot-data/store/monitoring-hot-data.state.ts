import { HttpErrorResponse } from '@angular/common/http';

import { MonitoringHotDataEntry, MonitoringHotDataEntryFilters } from '../models/monitoring-hot-data.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const monitoringHotDataStateFeatureKey = 'monitoringHotDataStateFeatureKey';

export interface MonitoringHotDataState {
  query: DataQuery<MonitoringHotDataEntryFilters>;
  data: MonitoringHotDataEntry[];
  loading: {
    searchMonitoringHotData: boolean;
  };
  error: {
    searchMonitoringHotData: HttpErrorResponse | null;
  };
}

export const initialState: MonitoringHotDataState = {
  query: defaultQuery,
  data: [],
  loading: {
    searchMonitoringHotData: false
  },
  error: {
    searchMonitoringHotData: null
  }
};
