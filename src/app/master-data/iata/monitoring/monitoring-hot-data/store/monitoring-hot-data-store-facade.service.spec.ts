import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromSelectors from '../selectors/monitoring-hot-data.selectors';

import { MonitoringHotDataFacadeService } from './monitoring-hot-data-store-facade.service';
import { initialState, monitoringHotDataStateFeatureKey } from './monitoring-hot-data.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringHotDataFacadeService', () => {
  let service: MonitoringHotDataFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringHotDataFacadeService,
        provideMockStore({
          initialState: {
            [monitoringHotDataStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MonitoringHotDataFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('searchMonitoringHotData should dispatch a new searchMonitoringHotDataRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchMonitoringHotData(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchMonitoringHotDataRequest({ payload: { query: defaultQuery } })
      );
    });

    it('downloadMonitoringHotData should dispatch a new downloadMonitoringHotDataRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.downloadMonitoringHotData();

      expect(store.dispatch).toHaveBeenCalledWith(fromActions.downloadMonitoringHotDataRequest());
    });
  });

  describe('selectors', () => {
    it('query$ should select query', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringHotDataQuery);
    });

    it('data$ should select data', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringHotData);
    });

    it('hasData$ should select hasData', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('searchMonitoringHotData$ should invoke selectsearchMonitoringHotDataLoading', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchMonitoringHotData; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchMonitoringHotDataLoading);
    });
  });
});
