/* eslint-disable @typescript-eslint/naming-convention */
import { Bsp } from '~app/shared/models/bsp.model';

export enum EStatus {
  COMPLETED = 'COMPLETED',
  FAILED = 'FAILED',
  PROCESSING = 'PROCESSING',
  REJECTING = 'REJECTING',
  REJECTED = 'REJECTED'
}

export enum salesQueryProcessorFilterOptions {
  COMPLETED = 'COMPLETED',
  FAILED = 'FAILED',
  PROCESSING = 'PROCESSING',
  PENDING = 'PENDING'
}

export interface MonitoringHotDataEntryFilters {
  isoCountryCode: Bsp[];
  name: string;
  startProcessingTime: Date[];
  endProcessingTime: Date[];
  status: EStatus[];
  salesQueryProcessorStatus: string[];
}

export interface MonitoringHotDataEntryFiltersBE {
  isoCountryCode: string[];
  name: string;
  startProcessingDateAfter: string;
  startProcessingDateBefore: string;
  endProcessingDateBefore: string;
  endProcessingDateAfter: string;
  status: EStatus[];
}

export interface MonitoringHotDataEntry {
  isoCountryCode: string;
  name: string;
  startProcessingTime: string;
  endProcessingTime: string;
  status: EStatus;
  totalTransactions: number;
  acceptedTransactions: number;
  errors: number;
  warnings: number;
}

export interface MonitoringPredefinedFilters {
  isoCountryCode: Bsp;
}
