import { createFeatureSelector, createSelector } from '@ngrx/store';

import { MonitoringHotDataState, monitoringHotDataStateFeatureKey } from '../store/monitoring-hot-data.state';

export const selectMonitoringHotDataStateFeature = createFeatureSelector<MonitoringHotDataState>(
  monitoringHotDataStateFeatureKey
);

export const selectMonitoringHotData = createSelector(selectMonitoringHotDataStateFeature, state => state.data);
export const selectMonitoringHotDataQuery = createSelector(selectMonitoringHotDataStateFeature, state => state.query);
export const selectSearchMonitoringHotDataLoading = createSelector(
  selectMonitoringHotDataStateFeature,
  state => state.loading.searchMonitoringHotData
);

export const selectHasData = createSelector(selectMonitoringHotDataStateFeature, state =>
  Boolean(state.data && state.data.length)
);
