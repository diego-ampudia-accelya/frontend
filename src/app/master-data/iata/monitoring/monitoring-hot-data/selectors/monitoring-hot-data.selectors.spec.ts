import { MonitoringHotDataState } from '../store/monitoring-hot-data.state';

import {
  selectHasData,
  selectMonitoringHotData,
  selectMonitoringHotDataQuery,
  selectMonitoringHotDataStateFeature,
  selectSearchMonitoringHotDataLoading
} from './monitoring-hot-data.selectors';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoring hot data selectors', () => {
  const mockState: MonitoringHotDataState = {
    query: defaultQuery,
    data: [],
    loading: {
      searchMonitoringHotData: false
    },
    error: {
      searchMonitoringHotData: null
    }
  };

  it('should return MonitoringHotDataState state', () => {
    const result = selectMonitoringHotDataStateFeature.projector(mockState);

    expect(result).toBe(mockState);
  });

  it('should select and return monitoring hot data', () => {
    const result = selectMonitoringHotData.projector(mockState);

    expect(result).toBe(mockState.data);
  });

  it('should select and return monitoring hot data query', () => {
    const result = selectMonitoringHotDataQuery.projector(mockState);

    expect(result).toBe(mockState.query);
  });

  it('should check if search monitoring hot data is loading', () => {
    const result = selectSearchMonitoringHotDataLoading.projector(mockState);

    expect(result).toBe(mockState.loading.searchMonitoringHotData);
  });

  it('should return false if the state has not data', () => {
    const result = selectHasData.projector(mockState);

    expect(result).toBe(false);
  });
});
