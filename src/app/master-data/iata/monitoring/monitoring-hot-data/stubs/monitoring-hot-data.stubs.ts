import { EStatus, MonitoringHotDataEntry } from '../models/monitoring-hot-data.models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { User, UserType } from '~app/shared/models/user.model';

export const monitoringHotData: MonitoringHotDataEntry[] = [
  {
    warnings: 1,
    errors: 0,
    status: EStatus.COMPLETED,
    totalTransactions: 1,
    acceptedTransactions: 1,
    endProcessingTime: '2023-03-09T09:00:46.203',
    isoCountryCode: 'ES',
    startProcessingTime: '2023-03-09T09:00:45.293',
    name: 'ESel2203_20210817_210817_B_01.220.SINGLE.zip'
  }
];

export const monitoringHotDataPagedData: PagedData<MonitoringHotDataEntry> = {
  pageNumber: 0,
  pageSize: 20,
  total: 1,
  totalPages: 1,
  records: monitoringHotData
};

export const userBsps: Bsp[] = [
  {
    id: 6983,
    isoCountryCode: 'ES',
    name: 'SPAIN',
    effectiveFrom: '2000-01-01',
    effectiveTo: null,
    version: 3232,
    defaultCurrencyCode: null,
    active: true
  }
];

export const user: User = {
  email: 'martin.sautter@iata.org',
  firstName: null,
  lastName: 'User0BSPES',
  telephone: '021224836126969',
  organization: "organisation ' comilla",
  address: '19 Mayis Cad. 4',
  locality: "icity ' comillas",
  city: null,
  zip: '123',
  country: 'Turquia',
  registerDate: '2000-09-06',
  expiryDate: null,
  active: true,
  id: 69830,
  userType: UserType.IATA,
  iataCode: null,
  language: 'en',
  bspPermissions: [
    {
      bspId: 6983,
      permissions: ['rMonHot']
    }
  ],
  permissions: ['rMonHot'],
  bsps: [
    {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      version: 3232,
      defaultCurrencyCode: null,
      active: true
    }
  ],
  isImpersonating: false,
  defaultIsoc: 'ES',
  template: 'STREAMLINED',
  isSuperUser: false
};
