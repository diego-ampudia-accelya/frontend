import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import { MonitoringHotDataEntry, MonitoringHotDataEntryFilters } from '../models/monitoring-hot-data.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchMonitoringHotDataRequest = createAction(
  '[Monitoring Hot Data]Search Monitoring Hot Data Request',
  props<{ payload: { query?: DataQuery<MonitoringHotDataEntryFilters> } }>()
);

export const searchMonitoringHotDataSuccess = createAction(
  '[Monitoring Hot Data]Search Monitoring Hot Data Success',
  props<{ payload: { pagedData: PagedData<MonitoringHotDataEntry> } }>()
);

export const searchMonitoringHotDataFailure = createAction(
  '[Monitoring Hot Data]Search Monitoring Hot Data Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const downloadMonitoringHotDataRequest = createAction(
  '[Monitoring Hot Data]Download Monitoring Hot Data Request'
);
