import { HttpErrorResponse } from '@angular/common/http';

import {
  MonitoringMasterDataRetEntry,
  MonitoringMasterDataRetEntryFilters
} from '../models/monitoring-master-data-ret.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const monitoringMasterDataRetStateFeatureKey = 'monitoringMasterDataRetStateFeatureKey';

export interface MonitoringMasterDataRetState {
  query: DataQuery<MonitoringMasterDataRetEntryFilters>;
  data: MonitoringMasterDataRetEntry[];
  loading: {
    searchMonitoringMasterDataRet: boolean;
  };
  error: {
    searchMonitoringMasterDataRet: HttpErrorResponse | null;
  };
}

export const initialState: MonitoringMasterDataRetState = {
  query: defaultQuery,
  data: [],
  loading: {
    searchMonitoringMasterDataRet: false
  },
  error: {
    searchMonitoringMasterDataRet: null
  }
};
