import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromSelectors from '../selectors/monitoring-master-data-ret.selectors';

import { MonitoringMasterDataRetFacadeService } from './monitoring-master-data-ret-store-facade.service';
import { initialState, monitoringMasterDataRetStateFeatureKey } from './monitoring-master-data-ret.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringMasterDataRetFacadeService', () => {
  let service: MonitoringMasterDataRetFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringMasterDataRetFacadeService,
        provideMockStore({
          initialState: {
            [monitoringMasterDataRetStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MonitoringMasterDataRetFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('should when invoke searchMonitoringHotData dispatch a new searchMonitoringHotDataRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchMonitoringMasterDataRet(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchMonitoringMasterDataRetRequest({ payload: { query: defaultQuery } })
      );
    });
  });

  describe('selectors', () => {
    it('should when invoke query$ select query', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringMasterDataRetQuery);
    });

    it('should when invoke data$ select data', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringMasterDataRet);
    });

    it('should when invoke hasData$ select hasData', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('should when invoke searchMonitoringHotData$ select selectsearchMonitoringHotDataLoading', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchMonitoringMasterDataRet$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchMonitoringMasterDataRetLoading);
    });
  });
});
