import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { MonitoringMasterDataRetEntryFilters } from '../models/monitoring-master-data-ret.models';
import * as fromSelectors from '../selectors/monitoring-master-data-ret.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class MonitoringMasterDataRetFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchMonitoringMasterDataRet: (query?: DataQuery<MonitoringMasterDataRetEntryFilters>) =>
        this.store.dispatch(fromActions.searchMonitoringMasterDataRetRequest({ payload: { query } }))
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectMonitoringMasterDataRetQuery),
      data$: this.store.select(fromSelectors.selectMonitoringMasterDataRet),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser)
    };
  }

  public get loading() {
    return {
      searchMonitoringMasterDataRet$: this.store.select(fromSelectors.selectSearchMonitoringMasterDataRetLoading)
    };
  }
}
