import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import {
  MonitoringMasterDataRetEntry,
  MonitoringMasterDataRetEntryFilters
} from '../models/monitoring-master-data-ret.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchMonitoringMasterDataRetRequest = createAction(
  '[Monitoring Master Data Ret]Search Monitoring Master Data Ret Request',
  props<{ payload: { query?: DataQuery<MonitoringMasterDataRetEntryFilters> } }>()
);

export const searchMonitoringMasterDataRetSuccess = createAction(
  '[Monitoring Master Data Ret]Search Monitoring Master Data Ret Success',
  props<{ payload: { pagedData: PagedData<MonitoringMasterDataRetEntry> } }>()
);

export const searchMonitoringMasterDataRetFailure = createAction(
  '[Monitoring Master Data Ret]Search Monitoring Master Data Ret Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const downloadsearchMonitoringMasterDataRetRequest = createAction(
  '[Monitoring Master Data Ret]Download Monitoring Master Data Ret Request'
);
