import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromState from '../store/monitoring-master-data-ret.state';
import * as fromStub from '../stubs/monitoring-master-data-ret.stubs';

import * as fromReducer from './monitoring-master-data-ret.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoringMasterDataRetReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.monitoringMasterDataRetReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchMonitoringMasterDataRetRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchMonitoringMasterDataRetRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.monitoringMasterDataRetReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringMasterDataRet: true
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringMasterDataRet: null
        }
      });
    });
  });

  describe('searchMonitoringMasterDataRetFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchMonitoringMasterDataRetFailure({ payload: { error } });
      const result = fromReducer.monitoringMasterDataRetReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringMasterDataRet: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringMasterDataRet: error
        }
      });
    });
  });

  describe('searchMonitoringMasterDataRetSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchMonitoringMasterDataRetSuccess({
        payload: { pagedData: fromStub.monitoringMasterDataRetPagedData }
      });
      const result = fromReducer.monitoringMasterDataRetReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.monitoringMasterDataRetPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.monitoringMasterDataRetPagedData.pageNumber,
            size: fromStub.monitoringMasterDataRetPagedData.pageSize,
            totalElements: fromStub.monitoringMasterDataRetPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringMasterDataRet: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringMasterDataRet: null
        }
      });
    });
  });
});
