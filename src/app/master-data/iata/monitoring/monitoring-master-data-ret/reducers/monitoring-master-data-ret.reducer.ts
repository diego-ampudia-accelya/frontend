import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { initialState } from '../store/monitoring-master-data-ret.state';

export const monitoringMasterDataRetReducer = createReducer(
  initialState,
  on(fromActions.searchMonitoringMasterDataRetRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchMonitoringMasterDataRet: true
    },
    error: {
      ...state.error,
      searchMonitoringMasterDataRet: null
    }
  })),
  on(fromActions.searchMonitoringMasterDataRetFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchMonitoringMasterDataRet: false
    },
    error: {
      ...state.error,
      searchMonitoringMasterDataRet: error
    }
  })),
  on(fromActions.searchMonitoringMasterDataRetSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchMonitoringMasterDataRet: false
    },
    error: {
      ...state.error,
      searchMonitoringMasterDataRet: null
    }
  }))
);
