import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { MONITORING_MASTER_DATA_RET } from '../../constants/monitoring-master-data-ret.constants';
import {
  EInputOutput,
  EStatus,
  MonitoringMasterDataRetEntry,
  MonitoringMasterDataRetEntryFilters
} from '../../models/monitoring-master-data-ret.models';
import { MonitoringMasterDataRetFilterFormatterService } from '../../services/monitoring-master-data-ret-filter-formatter.service';
import { MonitoringMasterDataRetFacadeService } from '../../store/monitoring-master-data-ret-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-monitoring-master-data-ret-list',
  templateUrl: './monitoring-master-data-ret-list.component.html'
})
export class MonitoringMasterDataRetListComponent implements OnInit {
  public columns: TableColumn[] = MONITORING_MASTER_DATA_RET.COLUMNS;
  public hasAllBspsPermission: boolean;
  public userBspDropdownOptions$: Observable<DropdownOption<BspDto>[]>;
  public userDefaultBsp$: Observable<Bsp>;
  public inputOutputOptions = [
    {
      value: EInputOutput.Input,
      label: this.translationService.translate('monitoring-master-data-ret.filters.inputOutput.option.Input')
    },
    {
      value: EInputOutput.Output,
      label: this.translationService.translate('monitoring-master-data-ret.filters.inputOutput.option.Output')
    }
  ];

  public statusOptions = [
    {
      value: EStatus.Completed,
      label: this.translationService.translate('monitoring-master-data-ret.filters.status.option.Completed')
    },
    {
      value: EStatus.Failed,
      label: this.translationService.translate('monitoring-master-data-ret.filters.status.option.Failed')
    },
    {
      value: EStatus.Pending,
      label: this.translationService.translate('monitoring-master-data-ret.filters.status.option.Pending')
    }
  ];

  public data$: Observable<MonitoringMasterDataRetEntry[]> =
    this.monitoringMasterDataRetStoreFacadeService.selectors.data$;
  public query$: Observable<DataQuery<MonitoringMasterDataRetEntryFilters>> =
    this.monitoringMasterDataRetStoreFacadeService.selectors.query$;
  public loading$: Observable<boolean> =
    this.monitoringMasterDataRetStoreFacadeService.loading.searchMonitoringMasterDataRet$;
  public hasData$: Observable<boolean> = this.monitoringMasterDataRetStoreFacadeService.selectors.hasData$;

  public filterFormGroup: FormGroup;

  constructor(
    public readonly monitoringMasterDataRetFilterFormatterService: MonitoringMasterDataRetFilterFormatterService,
    private readonly monitoringMasterDataRetStoreFacadeService: MonitoringMasterDataRetFacadeService,
    private readonly translationService: L10nTranslationService,
    private readonly permissionService: PermissionsService
  ) {}

  public get title$(): Observable<string> {
    return this.query$.pipe(
      map(query => query?.paginateBy?.totalElements),
      map(length => `${this.translationService.translate('monitoring-master-data-ret.view.title')} (${length || 0})`)
    );
  }

  public get predefinedFilters$(): Observable<Partial<MonitoringMasterDataRetEntryFilters>> {
    return this.userDefaultBsp$.pipe(
      map(bsp => ({
        isoCountryCode: [bsp]
      }))
    );
  }

  public get isBspFilterDisabled$(): Observable<boolean> {
    return this.userBspDropdownOptions$.pipe(map(userBspDropdownOptions => Boolean(userBspDropdownOptions?.length)));
  }

  public ngOnInit(): void {
    this.initializePermissions();

    this.initializeDefaultBsp();

    this.initializeFilterFormGroup();

    this.userBspDropdownOptions$ = this.monitoringMasterDataRetStoreFacadeService.selectors.userBsps$.pipe(
      map(bspList => bspList.map(bsp => toValueLabelObjectBsp(bsp)))
    );

    this.userDefaultBsp$.pipe(first()).subscribe(bsp => {
      this.monitoringMasterDataRetStoreFacadeService.actions.searchMonitoringMasterDataRet({
        ...defaultQuery,
        filterBy: {
          ...defaultQuery.filterBy,
          isoCountryCode: [bsp]
        },
        sortBy: [
          ...defaultQuery.sortBy,
          {
            attribute: 'startDate',
            sortType: SortOrder.Desc
          }
        ]
      });
    });
  }

  public onQueryChanged(query: DataQuery<MonitoringMasterDataRetEntryFilters>): void {
    this.monitoringMasterDataRetStoreFacadeService.actions.searchMonitoringMasterDataRet(query);
  }

  private initializeFilterFormGroup() {
    this.filterFormGroup = new FormGroup({
      isoCountryCode: new FormControl(null, []),
      filename: new FormControl(null, []),
      startDate: new FormControl(null, []),
      endDate: new FormControl(null, []),
      status: new FormControl(null, []),
      inputOutput: new FormControl(null, [])
    });
  }

  private initializeDefaultBsp() {
    this.userDefaultBsp$ = combineLatest([
      this.monitoringMasterDataRetStoreFacadeService.selectors.userBsps$,
      this.monitoringMasterDataRetStoreFacadeService.selectors.user$
    ]).pipe(map(([bspList, user]) => this.getDefaultBsp(bspList, user)));
  }

  private initializePermissions(): void {
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private getDefaultBsp(bspList: Bsp[], user: User): Bsp {
    return bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
  }
}
