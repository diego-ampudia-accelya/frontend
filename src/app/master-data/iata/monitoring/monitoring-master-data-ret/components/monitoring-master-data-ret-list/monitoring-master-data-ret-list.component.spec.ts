import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { MonitoringMasterDataRetEntryFilters } from '../../models/monitoring-master-data-ret.models';
import { MonitoringMasterDataRetFilterFormatterService } from '../../services/monitoring-master-data-ret-filter-formatter.service';
import { MonitoringMasterDataRetFacadeService } from '../../store/monitoring-master-data-ret-store-facade.service';
import * as fromStub from '../../stubs/monitoring-master-data-ret.stubs';
import { MonitoringMasterDataRetListComponent } from './monitoring-master-data-ret-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { DataQuery, ListViewComponent } from '~app/shared/components/list-view';

describe('MonitoringMasterDataRetListComponent', () => {
  let component: MonitoringMasterDataRetListComponent;
  let fixture: ComponentFixture<MonitoringMasterDataRetListComponent>;

  const monitoringMasterDataRetFacadeServiceSpy = {
    actions: {
      searchMonitoringMasterDataRet: () => {}
    },
    selectors: {
      query$: of(null),
      data$: of(null),
      hasData$: of(null),
      userBsps$: of(fromStub.userBsps),
      user$: of(fromStub.user)
    },
    loading: {
      searchMonitoringMasterDataRet$: of(null)
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule)],
      declarations: [
        MonitoringMasterDataRetListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: MonitoringMasterDataRetFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: MonitoringMasterDataRetFacadeService,
          useValue: monitoringMasterDataRetFacadeServiceSpy
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringMasterDataRetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call searchMonitoringMasterDataRet method when onQueryChanged is called', () => {
    const searchMonitoringMasterDataRetSpy = spyOn(
      monitoringMasterDataRetFacadeServiceSpy.actions,
      'searchMonitoringMasterDataRet'
    );
    const query: DataQuery<MonitoringMasterDataRetEntryFilters> = {
      paginateBy: { page: 0, size: 20, totalElements: 33256 },
      sortBy: null,
      filterBy: {}
    };

    component.onQueryChanged(query);

    expect(searchMonitoringMasterDataRetSpy).toHaveBeenCalled();
  });

  it('should return the title string when it is subscribed to title$', fakeAsync(() => {
    let titleResult: string;
    component['query$'] = of({
      paginateBy: { page: 0, size: 20, totalElements: 33256 },
      sortBy: null,
      filterBy: {}
    });

    component['title$'].subscribe(title => (titleResult = title));
    tick();

    expect(titleResult).toBeDefined();
  }));

  it('should return false when there are not user Bsp Dropdown Options', fakeAsync(() => {
    component['userBspDropdownOptions$'] = of(null);
    let result: boolean;

    component['isBspFilterDisabled$'].subscribe(isBspFilterDisabled => (result = isBspFilterDisabled));
    tick();

    expect(result).toBe(false);
  }));
});
