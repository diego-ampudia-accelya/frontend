import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringMasterDataRetListComponent } from './components/monitoring-master-data-ret-list/monitoring-master-data-ret-list.component';
import { MonitoringMasterDataRetEffects } from './effects/monitoring-master-data-ret.effects';
import { MonitoringMasterDataRetRoutingModule } from './monitoring-master-data-ret-routing.module';
import { MonitoringMasterDataRetComponent } from './monitoring-master-data-ret.component';
import { monitoringMasterDataRetReducer } from './reducers/monitoring-master-data-ret.reducer';
import { MonitoringMasterDataRetFilterFormatterService } from './services/monitoring-master-data-ret-filter-formatter.service';
import { MonitoringMasterDataRetService } from './services/monitoring-master-data-ret.service';
import { MonitoringMasterDataRetFacadeService } from './store/monitoring-master-data-ret-store-facade.service';
import { monitoringMasterDataRetStateFeatureKey } from './store/monitoring-master-data-ret.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MonitoringMasterDataRetRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(monitoringMasterDataRetStateFeatureKey, monitoringMasterDataRetReducer),
    EffectsModule.forFeature([MonitoringMasterDataRetEffects])
  ],
  declarations: [MonitoringMasterDataRetComponent, MonitoringMasterDataRetListComponent],
  providers: [
    MonitoringMasterDataRetService,
    MonitoringMasterDataRetFacadeService,
    MonitoringMasterDataRetFilterFormatterService
  ]
})
export class MonitoringMasterDataRetModule {}
