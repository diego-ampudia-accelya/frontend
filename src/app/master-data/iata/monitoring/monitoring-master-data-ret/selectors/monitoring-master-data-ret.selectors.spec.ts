import { EStatus } from '../models/monitoring-master-data-ret.models';
import { MonitoringMasterDataRetState } from '../store/monitoring-master-data-ret.state';

import {
  selectHasData,
  selectMonitoringMasterDataRet,
  selectMonitoringMasterDataRetQuery,
  selectMonitoringMasterDataRetStateFeature,
  selectSearchMonitoringMasterDataRetLoading
} from './monitoring-master-data-ret.selectors';

describe('monitoring global ta fop files selectors', () => {
  const mockState: MonitoringMasterDataRetState = {
    data: [
      {
        bsp: null,
        endDate: '2023-01-01',
        filename: 'ESeuDPC_20230503_remitfreq',
        inputOutput: null,
        startDate: '2023-01-01',
        status: EStatus.Completed
      }
    ],
    query: {
      filterBy: {},
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    },
    loading: {
      searchMonitoringMasterDataRet: true
    },
    error: null
  };

  describe('selectMonitoringMasterDataRetStateFeature', () => {
    it('should return the MonitoringMasterDataRet state feature', () => {
      const result = selectMonitoringMasterDataRetStateFeature.projector(mockState);
      expect(result).toBe(mockState);
    });
  });

  describe('selectMonitoringMasterDataRet', () => {
    it('should return the data from the MonitoringMasterDataRet state', () => {
      const result = selectMonitoringMasterDataRet.projector(mockState);
      expect(result).toBe(mockState.data);
    });
  });

  describe('selectMonitoringMasterDataRetQuery', () => {
    it('should return the query from the MonitoringMasterDataRet state', () => {
      const result = selectMonitoringMasterDataRetQuery.projector(mockState);
      expect(result).toBe(mockState.query);
    });
  });

  describe('selectSearchMonitoringMasterDataRetLoading', () => {
    it('should return the SearchMonitoringMasterDataRet loading value from the MonitoringMasterDataRet state', () => {
      const result = selectSearchMonitoringMasterDataRetLoading.projector(mockState);
      expect(result).toBe(mockState.loading.searchMonitoringMasterDataRet);
    });
  });

  describe('selectHasData', () => {
    it('should return true if the MonitoringMasterDataRet data array exists and has a length greater than 0', () => {
      const result = selectHasData.projector(mockState);
      expect(result).toBe(true);
    });

    it('should return false if the MonitoringMasterDataRet data array is empty or undefined', () => {
      const emptyState = {
        data: [],
        query: [],
        loading: {
          searchMonitoringMasterDataRet: []
        }
      };
      const result = selectHasData.projector(emptyState);
      expect(result).toBe(false);
    });
  });
});
