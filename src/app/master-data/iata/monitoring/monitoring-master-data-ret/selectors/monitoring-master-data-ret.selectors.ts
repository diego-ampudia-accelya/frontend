import { createFeatureSelector, createSelector } from '@ngrx/store';

import {
  MonitoringMasterDataRetState,
  monitoringMasterDataRetStateFeatureKey
} from '../store/monitoring-master-data-ret.state';

export const selectMonitoringMasterDataRetStateFeature = createFeatureSelector<MonitoringMasterDataRetState>(
  monitoringMasterDataRetStateFeatureKey
);

export const selectMonitoringMasterDataRet = createSelector(
  selectMonitoringMasterDataRetStateFeature,
  state => state.data
);
export const selectMonitoringMasterDataRetQuery = createSelector(
  selectMonitoringMasterDataRetStateFeature,
  state => state.query
);
export const selectSearchMonitoringMasterDataRetLoading = createSelector(
  selectMonitoringMasterDataRetStateFeature,
  state => state.loading.searchMonitoringMasterDataRet
);

export const selectHasData = createSelector(selectMonitoringMasterDataRetStateFeature, state =>
  Boolean(state.data && state.data.length)
);
