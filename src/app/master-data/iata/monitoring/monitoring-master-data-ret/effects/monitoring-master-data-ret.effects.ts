import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import { MonitoringMasterDataRetService } from '../services/monitoring-master-data-ret.service';

@Injectable()
export class MonitoringMasterDataRetEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly monitoringHotDataService: MonitoringMasterDataRetService
  ) {}

  public searchMonitoringMasterDataRetEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchMonitoringMasterDataRetRequest>>(
        fromActions.searchMonitoringMasterDataRetRequest
      ),
      switchMap(({ payload: { query } }) =>
        this.monitoringHotDataService.find(query).pipe(
          map(data =>
            fromActions.searchMonitoringMasterDataRetSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchMonitoringMasterDataRetFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );
}
