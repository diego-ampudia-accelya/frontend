import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/monitoring-hot-data.actions';
import * as fromSelectors from '../selectors/monitoring-master-data-ret.selectors';
import { MonitoringMasterDataRetService } from '../services/monitoring-master-data-ret.service';
import * as fromState from '../store/monitoring-master-data-ret.state';
import * as fromStub from '../stubs/monitoring-master-data-ret.stubs';

import { MonitoringMasterDataRetEffects } from './monitoring-master-data-ret.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringMasterDataRetEffects', () => {
  let actions$: Observable<any>;
  let effects: MonitoringMasterDataRetEffects;
  let service: MonitoringMasterDataRetService;

  const initialState = {
    [fromState.monitoringMasterDataRetStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringMasterDataRetEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectMonitoringMasterDataRetQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: MonitoringMasterDataRetService,
          useValue: {
            find: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<MonitoringMasterDataRetEffects>(MonitoringMasterDataRetEffects);
    service = TestBed.inject<MonitoringMasterDataRetService>(MonitoringMasterDataRetService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchMonitoringMasterDataRetEffect', () => {
    it('should return searchMonitoringMasterDataRetSuccess with paged data', () => {
      const action = fromActions.searchMonitoringMasterDataRetRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchMonitoringMasterDataRetSuccess({
        payload: {
          pagedData: fromStub.monitoringMasterDataRetPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.monitoringMasterDataRetPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchMonitoringMasterDataRetEffect$).toBeObservable(expected);
    });

    it('should return searchMonitoringMasterDataRetFailure when monitoring hot data service find fails', () => {
      const action = fromActions.searchMonitoringMasterDataRetRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchMonitoringMasterDataRetFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchMonitoringMasterDataRetEffect$).toBeObservable(expected);
    });
  });
});
