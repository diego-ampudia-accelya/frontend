/* eslint-disable @typescript-eslint/naming-convention */
import moment from 'moment-mini';

import { GridColumn } from '~app/shared/models';

export const MONITORING_MASTER_DATA_RET = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'bsp.isoCountryCode',
        name: 'monitoring-master-data-ret.columns.isoCountryCode',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'filename',
        name: 'monitoring-master-data-ret.columns.filename',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'startDate',
        name: 'monitoring-master-data-ret.columns.startDate',
        resizeable: false,
        draggable: false,
        pipe: { transform: date => date && moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'endDate',
        name: 'monitoring-master-data-ret.columns.endDate',
        resizeable: true,
        draggable: false,
        pipe: { transform: date => date && moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'status',
        name: 'monitoring-master-data-ret.columns.status',
        resizeable: true,
        draggable: false,
        cellTemplate: 'statusBasicTmpl'
      },
      {
        prop: 'inputOutput',
        name: 'monitoring-master-data-ret.columns.inputOutput',
        resizeable: true,
        draggable: false,
        cellTemplate: 'inputOutputTmpl'
      }
    ];
  }
};
