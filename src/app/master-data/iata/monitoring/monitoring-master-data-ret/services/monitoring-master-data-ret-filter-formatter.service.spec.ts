import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import {
  EInputOutput,
  EStatus,
  MonitoringMasterDataRetEntryFilters
} from '../models/monitoring-master-data-ret.models';

import { MonitoringMasterDataRetFilterFormatterService } from './monitoring-master-data-ret-filter-formatter.service';

const filters = {
  filename: 'ESeuDPC_20230503_remitfreq',
  status: [EStatus.Completed],
  isoCountryCode: [
    {
      id: 1,
      name: 'Spain',
      isoCountryCode: 'ES'
    }
  ],
  inputOutput: EInputOutput.Input,
  startDate: [new Date('2022-02-10'), new Date('2022-02-12')],
  endDate: [new Date('2022-02-10'), new Date('2022-02-12')]
} as MonitoringMasterDataRetEntryFilters;

const translateKey = (key: string) => `monitoring-master-data-ret.filters.${key}.label`;
const translateStatus = (status: EStatus) => `monitoring-master-data-ret.filters.status.option.${status}`;
const translateInputOutput = (inputOutput: EInputOutput) =>
  `monitoring-master-data-ret.filters.inputOutput.option.${inputOutput}`;

describe('MonitoringMasterDataRetFilterFormatterService', () => {
  let formatter: MonitoringMasterDataRetFilterFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new MonitoringMasterDataRetFilterFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['filename'],
        label: `${translateKey('filename')} - ESeuDPC_20230503_remitfreq`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - ${translateStatus(filters.status[0])}`
      },
      {
        keys: ['isoCountryCode'],
        label: `${translateKey('isoCountryCode')} - ES`
      },
      {
        keys: ['inputOutput'],
        label: `${translateKey('inputOutput')} - ${translateInputOutput(filters.inputOutput)}`
      },
      {
        keys: ['startDate'],
        label: `${translateKey('startDate')} - 10/02/2022 - 12/02/2022`
      },
      {
        keys: ['endDate'],
        label: `${translateKey('endDate')} - 10/02/2022 - 12/02/2022`
      }
    ]);
  });

  it('should return empty array if no filters are passed by parameter to format method', () => {
    const emptyFilters = {
      filename: null,
      status: null,
      isoCountryCode: null,
      inputOutput: null,
      startDate: null,
      endDate: null
    } as MonitoringMasterDataRetEntryFilters;
    const result = formatter.format(emptyFilters);

    expect(result).toEqual([]);
  });
});
