import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import {
  EInputOutput,
  EStatus,
  MonitoringMasterDataRetEntry,
  MonitoringMasterDataRetEntryFilters
} from '../models/monitoring-master-data-ret.models';
import { MonitoringMasterDataRetFacadeService } from '../store/monitoring-master-data-ret-store-facade.service';
import { MonitoringMasterDataRetService } from './monitoring-master-data-ret.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MonitoringMasterDataRetService', () => {
  let service: MonitoringMasterDataRetService;
  let httpClientSpy: SpyObject<HttpClient>;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_MASTER_DATA_RET, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  const monitoringRecordsMock: MonitoringMasterDataRetEntry[] = [
    {
      bsp: {
        id: 1,
        isoCountryCode: 'ES',
        name: 'Spain'
      },
      inputOutput: EInputOutput.Input,
      filename: 'ESeuDPC_20230503_remitfreq',
      startDate: '2023-01-01',
      endDate: '2023-01-01',
      status: EStatus.Completed
    }
  ];

  const monitoringFilterMock: MonitoringMasterDataRetEntryFilters[] = [
    {
      isoCountryCode: [
        {
          id: 1,
          name: 'Spain',
          isoCountryCode: 'ES'
        }
      ],
      inputOutput: EInputOutput.Input,
      filename: 'ESeuDPC_20230503_remitfreq',
      startDate: [new Date('2023-01-01'), new Date('2023-01-01')],
      endDate: [new Date('2023-01-01'), new Date('2023-01-01')],
      status: [EStatus.Completed]
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const monitoringMasterDataRetFacadeServiceSpy = {
    actions: {
      searchMonitoringMasterDataRet: () => {}
    },
    selectors: {
      query$: of(null),
      data$: of(null),
      hasData$: of(null),
      userBsps$: of([
        {
          id: 1,
          isoCountryCode: 'ES',
          name: 'Spain'
        },
        {
          id: 2,
          isoCountryCode: 'MT',
          name: 'Malta'
        }
      ])
    },
    loading: {
      searchMonitoringHotData$: of(null)
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        MonitoringMasterDataRetService,
        { provide: HttpClient, useValue: httpClientSpy },
        {
          provide: MonitoringMasterDataRetFacadeService,
          useValue: monitoringMasterDataRetFacadeServiceSpy
        },
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        provideMockStore({ initialState })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(MonitoringMasterDataRetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let monitoringReportData: PagedData<MonitoringMasterDataRetEntry>;
    service['bspId'] = 11;

    const auditDataMock: PagedData<MonitoringMasterDataRetEntry> = {
      records: monitoringRecordsMock,
      total: monitoringRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      monitoringReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/bsp-management/bsps/11/monitoring/process-centre?page=0&size=20');
    expect(monitoringReportData).toEqual(auditDataMock);
  }));

  it('should build request query filtering by the filters selected for the user, isoCountryCode, toStartDate and toEndDate', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    queryCloneDeep.filterBy['startDate'] = monitoringFilterMock[0].startDate;
    queryCloneDeep.filterBy['endDate'] = monitoringFilterMock[0].endDate;

    const dataQuery: DataQuery<MonitoringMasterDataRetEntryFilters> = {
      ...queryCloneDeep,
      filterBy: {
        isoCountryCode: monitoringFilterMock[0].isoCountryCode,
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['buildRequestQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual([
      monitoringFilterMock[0].isoCountryCode[0].isoCountryCode
    ]);
    expect(formattedQuery.filterBy['toStartDate']).toEqual('2023-01-01');
    expect(formattedQuery.filterBy['toEndDate']).toEqual('2023-01-01');
  });

  it('should build request query filtering by the filters selected for the user, startDate and toEndDate', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const time = [new Date('01/01/2023')];
    queryCloneDeep.filterBy.startDate = time;
    queryCloneDeep.filterBy.endDate = time;

    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['buildRequestQuery'](dataQuery);

    expect(formattedQuery.filterBy['toStartDate']).toEqual('2023-01-01');
    expect(formattedQuery.filterBy['toEndDate']).toEqual('2023-01-01');
  });
});
