import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringMasterDataRetComponent } from './monitoring-master-data-ret.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: MonitoringMasterDataRetComponent,
    data: {
      tab: ROUTES.MONITOR_MASTER_DATA_RET
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringMasterDataRetRoutingModule {}
