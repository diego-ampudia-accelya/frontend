/* eslint-disable @typescript-eslint/naming-convention */
import { Bsp } from '~app/shared/models/bsp.model';

export enum EStatus {
  Completed = 'Completed',
  Failed = 'Failed',
  Pending = 'Pending'
}

export enum EInputOutput {
  Input = 'Input',
  Output = 'Output'
}

export interface MonitoringMasterDataRetEntryFilters {
  isoCountryCode: Bsp[];
  filename: string;
  startDate: Date[];
  endDate: Date[];
  status: EStatus[];
  inputOutput: EInputOutput;
}

export interface MonitoringMasterDataRetEntry {
  bsp: Bsp;
  endDate: string;
  filename: string;
  inputOutput: EInputOutput;
  startDate: string;
  status: EStatus;
}
