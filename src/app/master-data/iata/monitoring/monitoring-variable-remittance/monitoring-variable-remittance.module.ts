import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringVariableRemittanceComponent } from './monitoring-variable-remittance.component';
import { MonitoringVariableRemittanceRoutingModule } from './monitoring-variable-remittance.routing.module';
import { MonitoringVariableRemittanceFilterFormatter } from './services/monitoring-variable-remittance-filter-formatter';
import { MonitoringVariableRemittanceService } from './services/monitoring-variable-remittance.service';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, MonitoringVariableRemittanceRoutingModule, SharedModule, L10nTranslationModule],
  declarations: [MonitoringVariableRemittanceComponent],
  providers: [MonitoringVariableRemittanceService, MonitoringVariableRemittanceFilterFormatter]
})
export class MonitoringVariableRemittanceModule {}
