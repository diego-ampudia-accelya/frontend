import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { MonitoringVariableRemittance } from '../models/monitoring-variable-remittance.model';
import { MonitoringVariableRemittanceService } from './monitoring-variable-remittance.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MonitoringVariableRemittance', () => {
  let service: MonitoringVariableRemittanceService;
  let httpClientSpy: SpyObject<HttpClient>;

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const monitoringRecordsMock: MonitoringVariableRemittance[] = [
    {
      isoCountryCode: [
        {
          id: 6983,
          isoCountryCode: 'ES',
          name: 'SPAIN'
        }
      ],
      filename: 'ESeuDPC_20230503_remitfreq',
      startDate: [new Date()],
      endDate: [new Date()],
      status: 'Completed'
    },
    {
      isoCountryCode: [
        {
          id: 6984,
          isoCountryCode: 'ES',
          name: 'SPAIN'
        }
      ],
      filename: 'ESeuDPC_20230503_remitfreq2',
      startDate: [new Date()],
      endDate: [new Date()],
      status: 'Completed'
    }
  ];

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_VARIABLE_REMITTANCE, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        MonitoringVariableRemittanceService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        provideMockStore({ initialState })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(MonitoringVariableRemittanceService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    service['bspId'] = 5849;
    expect(service['baseUrl']).toBe('/bsp-management/bsps/5849/monitoring/variable-remittance');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let monitoringReportData: PagedData<MonitoringVariableRemittance>;

    const auditDataMock: PagedData<MonitoringVariableRemittance> = {
      records: monitoringRecordsMock,
      total: monitoringRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      monitoringReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/bsp-management/bsps/10026/monitoring/variable-remittance?page=0&size=20'
    );
    expect(monitoringReportData).toEqual(auditDataMock);
  }));

  it('should return isoCountryCode formatted calling formatQuery method', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const isoCountryCodeFormatted = ['ES'];
    const time = [new Date(), new Date()];
    const status = [
      {
        translationKey: 'status1',
        value: 'status1'
      }
    ];
    queryCloneDeep.filterBy.startDate = time;
    queryCloneDeep.filterBy.endDate = time;

    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        isoCountryCode: [{ isoCountryCode: 'ES' }],
        status,
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['isoCountryCode']).toEqual(isoCountryCodeFormatted);
  });

  it('should return toStartDate and toEndDate properly when there is not the second date selected for each one', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const time = [new Date('01/01/2023')];
    queryCloneDeep.filterBy.startDate = time;
    queryCloneDeep.filterBy.endDate = time;

    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['toStartDate']).toEqual('2023-01-01');
    expect(formattedQuery.filterBy['toEndDate']).toEqual('2023-01-01');
  });
});
