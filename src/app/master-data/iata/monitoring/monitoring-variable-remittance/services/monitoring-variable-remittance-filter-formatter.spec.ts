import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { MonitoringVariableRemittanceFilter } from '../models/monitoring-variable-remittance-filter.model';

import { MonitoringVariableRemittanceFilterFormatter } from './monitoring-variable-remittance-filter-formatter';

const filters = {
  isoCountryCode: [
    {
      active: true,
      defaultCurrencyCode: null,
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      version: 3127
    }
  ],
  filename: 'remitfreq-override',
  startDate: [new Date('2022-02-10'), new Date('2022-02-12')],
  endDate: [new Date('2022-02-10'), new Date('2022-02-12')],
  status: [
    {
      translationKey: 'Completed',
      value: 'Completed'
    }
  ]
} as MonitoringVariableRemittanceFilter;

const translateKey = (key: string) => `monitoring-variable-remittance.filter.${key}.label`;

describe('MonitoringVariableRemittanceFilterFormatter', () => {
  let formatter: MonitoringVariableRemittanceFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new MonitoringVariableRemittanceFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['isoCountryCode'],
        label: `${translateKey('isoCountryCode')} - ES`
      },
      {
        keys: ['filename'],
        label: `${translateKey('filename')} - remitfreq-override`
      },
      {
        keys: ['startDate'],
        label: `${translateKey('startDate')} - 10/02/2022 - 12/02/2022`
      },
      {
        keys: ['endDate'],
        label: `${translateKey('endDate')} - 10/02/2022 - 12/02/2022`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - Completed`
      }
    ]);
  });
});
