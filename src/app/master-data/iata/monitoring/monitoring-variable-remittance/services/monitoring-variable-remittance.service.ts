import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { MonitoringVariableRemittanceFilter } from '../models/monitoring-variable-remittance-filter.model';
import {
  MonitoringVariableRemittance,
  MonitoringVariableRemittanceBE
} from '../models/monitoring-variable-remittance.model';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class MonitoringVariableRemittanceService implements Queryable<MonitoringVariableRemittance> {
  private bspId: number;
  private get baseUrl(): string {
    return `${this.appConfiguration.baseApiPath}/bsp-management/bsps/${this.bspId}/monitoring/variable-remittance`;
  }

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {}

  public find(
    query?: DataQuery<MonitoringVariableRemittanceFilter>
  ): Observable<PagedData<MonitoringVariableRemittance>> {
    this.store.pipe(select(fromAuth.getUserBsps), first()).subscribe(bsps => {
      if (bsps && bsps[0]) {
        this.setBspId(bsps[0].id);
      }
    });
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<MonitoringVariableRemittance>>(this.baseUrl + requestQuery.getQueryString());
  }

  private setBspId(bspId: number): void {
    this.bspId = bspId;
  }

  private formatQuery(
    query: Partial<DataQuery<MonitoringVariableRemittanceFilter>>
  ): RequestQuery<MonitoringVariableRemittanceBE> {
    const { isoCountryCode, startDate, endDate, status, ...filterBy } = query.filterBy;

    const bspsFormatted = Array.isArray(isoCountryCode) ? isoCountryCode : [isoCountryCode];

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        isoCountryCode: isoCountryCode && bspsFormatted.map(bsp => bsp.isoCountryCode),
        fromStartDate: startDate && toShortIsoDate(startDate[0]),
        toStartDate: startDate && toShortIsoDate(startDate[1] ? startDate[1] : startDate[0]),
        fromEndDate: endDate && toShortIsoDate(endDate[0]),
        toEndDate: endDate && toShortIsoDate(endDate[1] ? endDate[1] : endDate[0]),
        status: status && status.map(st => st.value)
      }
    });
  }
}
