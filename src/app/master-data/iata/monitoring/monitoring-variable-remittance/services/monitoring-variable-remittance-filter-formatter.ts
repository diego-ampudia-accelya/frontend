import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { MonitoringVariableRemittanceFilter } from '../models/monitoring-variable-remittance-filter.model';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { mapJoinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };
@Injectable()
export class MonitoringVariableRemittanceFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translation: L10nTranslationService) {
    super();
  }

  public format(filter: MonitoringVariableRemittanceFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<MonitoringVariableRemittanceFilter>> = {
      isoCountryCode: bsps => {
        const bspsFormatted = Array.isArray(bsps) ? bsps : [bsps];

        return `${this.translate('isoCountryCode')} - ${bspsFormatted.map(bsp => bsp.isoCountryCode).join(', ')}`;
      },
      filename: file => `${this.translate('filename')} - ${file}`,
      startDate: startDate => `${this.translate('startDate')} - ${rangeDateFilterTagMapper(startDate)}`,
      endDate: endDate => `${this.translate('endDate')} - ${rangeDateFilterTagMapper(endDate)}`,
      status: status => `${this.translate('status')} - ${mapJoinMapper(status, 'value')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`monitoring-variable-remittance.filter.${key}.label`);
  }
}
