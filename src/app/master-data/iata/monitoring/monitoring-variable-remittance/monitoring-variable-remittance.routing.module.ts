import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringVariableRemittanceComponent } from './monitoring-variable-remittance.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: MonitoringVariableRemittanceComponent,
    data: {
      tab: ROUTES.MONITOR_VARIABLE_REMITTANCE
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringVariableRemittanceRoutingModule {}
