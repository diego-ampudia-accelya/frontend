import { Bsp } from '~app/shared/models/bsp.model';

export interface MonitoringVariableRemittance {
  isoCountryCode: Bsp[];
  filename: string;
  startDate: Date[];
  endDate: Date[];
  status: string;
}

export interface MonitoringVariableRemittanceBE {
  isoCountryCode: string[];
  filename: string;
  fromStartDate: string;
  toStartDate: string;
  fromEndDate: string;
  toEndDate: string;
  status: string[];
}
