import { Bsp } from '~app/shared/models/bsp.model';

export interface MonitoringVariableRemittanceFilter {
  isoCountryCode: Bsp[];
  filename: string;
  startDate: Date[];
  endDate: Date[];
  status: MonitoringVRDropdownOption[];
}

export interface MonitoringVRDropdownOption {
  translationKey: string;
  value: string;
}
