/* eslint-disable @typescript-eslint/naming-convention */
export enum StatusFilesOptions {
  Completed = 'Completed',
  Pending = 'Pending',
  Failed = 'Failed'
}
