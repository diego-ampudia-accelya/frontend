import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { MonitoringVariableRemittanceComponent } from './monitoring-variable-remittance.component';
import { MonitoringVariableRemittanceFilterFormatter } from './services/monitoring-variable-remittance-filter-formatter';
import { MonitoringVariableRemittanceService } from './services/monitoring-variable-remittance.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('MonitoringVariableRemittanceComponent', () => {
  let component: MonitoringVariableRemittanceComponent;
  let fixture: ComponentFixture<MonitoringVariableRemittanceComponent>;

  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);
  const monitoringVariableRemittanceServiceSpy: SpyObject<MonitoringVariableRemittanceService> = createSpyObject(
    MonitoringVariableRemittanceService
  );
  const permissionsServiceSpy: SpyObject<PermissionsService> = createSpyObject(PermissionsService);
  const bspsDictionaryServiceSpy: SpyObject<BspsDictionaryService> = createSpyObject(BspsDictionaryService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_VARIABLE_REMITTANCE, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  const bspListMock = [
    { value: { id: 7376, isoCountryCode: 'IL', name: 'ISRAEL' }, label: 'IL - ISRAEL' },
    { value: { id: 6554, isoCountryCode: 'A6', name: 'AUSTRALIA' }, label: 'A6 - AUSTRALIA' },
    { value: { id: 6671, isoCountryCode: 'BG', name: 'Bulgaria' }, label: 'BG - Bulgaria' },
    { value: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' }, label: 'ES - SPAIN' },
    { value: { id: 7784, isoCountryCode: 'MT', name: 'MALTA' }, label: 'MT - MALTA' }
  ] as DropdownOption<BspDto>[];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MonitoringVariableRemittanceComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AppConfigurationService,
        L10nTranslationService,
        MonitoringVariableRemittanceFilterFormatter,
        FormBuilder
      ]
    })
      .overrideComponent(MonitoringVariableRemittanceComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: MonitoringVariableRemittanceService, useValue: monitoringVariableRemittanceServiceSpy },
            { provide: PermissionsService, useValue: permissionsServiceSpy },
            { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringVariableRemittanceComponent);
    component = fixture.componentInstance;
    permissionsServiceSpy.hasPermission.and.returnValue(true);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return a true when the user type checked is IATA user type', () => {
    const isAgentLogged = component['loggedUserIsOfType'](UserType.IATA);
    expect(isAgentLogged).toBe(true);
  });

  it('should get Default And Set Selected Bsp For Iata Multicountry', () => {
    component['getDefaultAndSetSelectedBspForMcIata'](bspListMock, initialState.auth.user);

    expect(component['defaultBsp']).toEqual({
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    });
  });

  it('should call loadData method when susbcribe to initializeBspFilterAndDefaultBsp$', fakeAsync(() => {
    const loadDataSpy = spyOn(component, 'loadData');
    spyOn<any>(component, 'initializeBspFilterAndDefaultBsp$').and.returnValue(of({}));

    component['initializeBspFilterAndLoadData']();
    component['initializeBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(loadDataSpy).toHaveBeenCalled();
  }));

  it('should set Predefined Filters when initializeBspFilterAndDefaultBsp$ has been subscribed', fakeAsync(() => {
    const setPredefinedFiltersSpy = spyOn<any>(component, 'setPredefinedFilters');
    bspsDictionaryServiceSpy.getAllBspDropdownOptions.and.returnValue([bspListMock]);

    spyOn<any>(component, 'getDefaultAndSetSelectedBspForMcIata').and.stub();

    component['initializeBspFilterAndDefaultBsp$']().subscribe();

    tick();

    expect(setPredefinedFiltersSpy).toHaveBeenCalled();
  }));
});
