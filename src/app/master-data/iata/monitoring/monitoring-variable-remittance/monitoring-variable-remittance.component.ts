import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEmpty } from 'lodash';
import { combineLatest, Observable, Subject } from 'rxjs';
import { first, map, takeUntil, tap } from 'rxjs/operators';

import {
  MonitoringVariableRemittanceFilter,
  MonitoringVRDropdownOption
} from './models/monitoring-variable-remittance-filter.model';
import { MonitoringVariableRemittance } from './models/monitoring-variable-remittance.model';
import { StatusFilesOptions } from './models/status-files-options';
import { MonitoringVariableRemittanceFilterFormatter } from './services/monitoring-variable-remittance-filter-formatter';
import { MonitoringVariableRemittanceService } from './services/monitoring-variable-remittance.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

const DEFAULT_SORTBY = 'startDate';

@Component({
  selector: 'bspl-monitoring-variable-remittance',
  templateUrl: './monitoring-variable-remittance.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: MonitoringVariableRemittanceService }
  ]
})
export class MonitoringVariableRemittanceComponent implements OnInit, OnDestroy {
  public title = this.translationService.translate('monitoring-variable-remittance.view.title');
  public columns: GridColumn[];
  public searchForm: FormGroup;
  public userBsps: DropdownOption<BspDto>[];
  public statusOptions: DropdownOption<MonitoringVRDropdownOption>[] =
    this.generateStatusOptionList(StatusFilesOptions);
  public isBspFilterLocked: boolean;
  public isBspFilterMultiple: boolean;
  public predefinedFilters: {
    isoCountryCode: Bsp;
  };

  private formFactory: FormUtil;
  private dateTimeFormat = 'dd/MM/yyyy HH:mm:ss';
  private defaultBsp: Bsp;
  private bspControl: FormControl;
  private destroy$ = new Subject();
  private storedQuery: DataQuery<{ [key: string]: any }>;
  private loggedUser: User;
  private get selectedBsp(): Bsp {
    const bspControl = FormUtil.get<MonitoringVariableRemittanceFilter>(this.searchForm, 'isoCountryCode');

    return bspControl.value;
  }
  private set selectedBsp(selectedBsp: Bsp) {
    const bspControl = FormUtil.get<MonitoringVariableRemittanceFilter>(this.searchForm, 'isoCountryCode');
    const selectedBspAdapted = this.isBspFilterMultiple ? [selectedBsp] : selectedBsp;

    bspControl.setValue(selectedBspAdapted);
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(fromAuth.getUser), first());
  }

  constructor(
    public displayFormatter: MonitoringVariableRemittanceFilterFormatter,
    public dataSource: QueryableDataSource<MonitoringVariableRemittance>,
    private datePipe: DatePipe,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private queryStorage: DefaultQueryStorage,
    private store: Store<AppState>,
    private bspsDictionaryService: BspsDictionaryService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit() {
    this.initializeLoggedUser();
    this.initColumns();
    this.searchForm = this.buildForm();

    this.initializeBspFilterAndLoadData();
  }

  public loggedUserIsOfType(userType: string): boolean {
    return userType.toUpperCase() === this.loggedUser.userType.toUpperCase();
  }

  public loadData(query: DataQuery<MonitoringVariableRemittanceFilter>): void {
    query = query || cloneDeep(defaultQuery);
    let dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...this.predefinedFilters,
        ...query.filterBy
      }
    };

    if (isEmpty(dataQuery.sortBy)) {
      const sortBy = [{ attribute: DEFAULT_SORTBY, sortType: SortOrder.Desc }];
      dataQuery = { ...dataQuery, sortBy };
    }

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private generateStatusOptionList(options: any): DropdownOption<MonitoringVRDropdownOption>[] {
    return Object.entries(options).map(([key, val]: [string, string]) => ({
      value: { translationKey: key, value: val },
      label: this.translationService.translate(`monitoring-variable-remittance.filter.status.option.${key}`)
    }));
  }

  private buildForm(): FormGroup {
    this.bspControl = new FormControl();

    return this.formFactory.createGroup<MonitoringVariableRemittanceFilter>({
      isoCountryCode: this.bspControl,
      filename: [],
      startDate: [],
      endDate: [],
      status: []
    });
  }

  private setPredefinedFilters(): void {
    this.predefinedFilters = {
      isoCountryCode: this.selectedBsp || this.defaultBsp
    };
  }

  private initializeLoggedUser() {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });
  }

  private initializeBspFilterAndLoadData(): void {
    this.initializeBspFilterAndDefaultBsp$().subscribe(() => {
      this.storedQuery = this.queryStorage.get();
      this.loadData(this.storedQuery);
    });
  }

  private initializeBspFilterAndDefaultBsp$(): Observable<DropdownOption<BspDto>[]> {
    return combineLatest([
      this.bspsDictionaryService.getAllBspDropdownOptions(),
      this.store.select(fromAuth.getUser)
    ]).pipe(
      first(),
      map(([bspList, user]) => {
        this.getDefaultAndSetSelectedBspForMcIata(bspList, user);
        this.setPredefinedFilters();

        return bspList;
      }),

      tap(bspList => (this.userBsps = bspList)),
      tap(() => (this.isBspFilterLocked = true))
    );
  }

  private getDefaultAndSetSelectedBspForMcIata(bspList: DropdownOption<BspDto>[], user: User): void {
    this.defaultBsp = user.bsps.find(bspUser =>
      bspList.some(bsp => bsp.value.isoCountryCode === bspUser.isoCountryCode)
    );
    this.selectedBsp = this.defaultBsp;
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'bsp.isoCountryCode',
        name: 'monitoring-variable-remittance.columns.bsp',
        maxWidth: 80
      },
      {
        prop: 'filename',
        name: 'monitoring-variable-remittance.columns.filename'
      },
      {
        prop: 'startDate',
        name: 'monitoring-variable-remittance.columns.startDate',
        pipe: {
          transform: value => this.datePipe.transform(value, this.dateTimeFormat)
        }
      },
      {
        prop: 'endDate',
        name: 'monitoring-variable-remittance.columns.endDate',
        pipe: {
          transform: value => this.datePipe.transform(value, this.dateTimeFormat)
        }
      },
      {
        prop: 'status',
        name: 'monitoring-variable-remittance.columns.status'
      }
    ];
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
