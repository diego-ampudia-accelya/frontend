import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringHotDetailsComponent } from './monitoring-hot-details/monitoring-hot-details.component';
import { MonitorHotDetailsIdResolver } from './monitoring-hot-details/services/monitoring-hot-details-resolvers';
import { ROUTES } from '~app/shared/constants/routes';

export function getHotDataTabLabel(tabConfig: any): string {
  const hotFileName = tabConfig.id;

  return `${hotFileName}`;
}

const routes: Routes = [
  {
    path: ROUTES.MONITOR_PBD_FILES.path,
    loadChildren: () =>
      import('./monitoring-pbd-files/monitoring-pbd-files.module').then(m => m.MonitoringPbdFilesModule)
  },
  {
    path: ROUTES.MONITOR_MASS_UPLOAD_FILES.path,
    loadChildren: () =>
      import('./monitoring-mass-upload-files/monitoring-mass-upload-files.module').then(
        m => m.MonitoringMassUploadFilesModule
      )
  },
  {
    path: ROUTES.MONITOR_HOT_DATA.path,
    loadChildren: () => import('./monitoring-hot-data/monitoring-hot-data.module').then(m => m.MonitoringHotDataModule)
  },
  {
    path: ROUTES.MONITOR_GLOBAL_TA_AND_FOP_FILES.path,
    loadChildren: () =>
      import('./monitoring-global-ta-fop-files/monitoring-global-ta-fop-files.module').then(
        m => m.MonitoringGlobalTaFopFilesModule
      )
  },
  {
    path: ROUTES.MONITOR_VARIABLE_REMITTANCE.path,
    loadChildren: () =>
      import('./monitoring-variable-remittance/monitoring-variable-remittance.module').then(
        m => m.MonitoringVariableRemittanceModule
      )
  },
  {
    path: ROUTES.MONITOR_MASTER_DATA_RET.path,
    loadChildren: () =>
      import('./monitoring-master-data-ret/monitoring-master-data-ret.module').then(
        m => m.MonitoringMasterDataRetModule
      )
  },
  {
    path: ROUTES.MONITOR_HOT_DATA_DETAILS.path,
    resolve: { id: MonitorHotDetailsIdResolver },
    data: {
      tab: {
        ...ROUTES.MONITOR_HOT_DATA_DETAILS,
        getTabLabel: getHotDataTabLabel
      }
    },
    component: MonitoringHotDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringRoutingModule {}
