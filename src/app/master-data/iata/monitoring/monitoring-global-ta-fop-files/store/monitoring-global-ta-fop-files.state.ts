import { HttpErrorResponse } from '@angular/common/http';

import {
  MonitoringGlobalTaFopFileEntry,
  MonitoringTaFopFileEntryFilters
} from '../models/monitoring-global-ta-fop-files.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const monitoringGlobalTaFopFilesStateFeatureKey = 'monitoringGlobalTaFopFilesStateFeatureKey';

export interface MonitoringGlobalTaFopFilesState {
  query: DataQuery<MonitoringTaFopFileEntryFilters>;
  data: MonitoringGlobalTaFopFileEntry[];
  loading: {
    searchMonitoringGlobalTaFopFiles: boolean;
  };
  error: {
    searchMonitoringGlobalTaFopFiles: HttpErrorResponse | null;
  };
}

export const initialState: MonitoringGlobalTaFopFilesState = {
  query: defaultQuery,
  data: [],
  loading: {
    searchMonitoringGlobalTaFopFiles: false
  },
  error: {
    searchMonitoringGlobalTaFopFiles: null
  }
};
