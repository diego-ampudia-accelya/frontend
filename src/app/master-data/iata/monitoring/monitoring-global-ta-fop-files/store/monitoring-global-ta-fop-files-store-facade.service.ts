import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-global-ta-fop-files.actions';
import { MonitoringTaFopFileEntryFilters } from '../models/monitoring-global-ta-fop-files.models';
import * as fromSelectors from '../selectors/monitoring-global-ta-fop-files.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class MonitoringGlobalTaFopFilesFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchMonitoringGlobalTaFopFiles: (query?: DataQuery<MonitoringTaFopFileEntryFilters>) =>
        this.store.dispatch(fromActions.searchMonitoringGlobalTaFopFilesRequest({ payload: { query } })),
      downloadMonitoringGlobalTaFopFiles: () =>
        this.store.dispatch(fromActions.downloadMonitoringGlobalTaFopFilesRequest())
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectMonitoringGlobalTaFopFilesQuery),
      data$: this.store.select(fromSelectors.selectMonitoringGlobalTaFopFiles),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser)
    };
  }

  public get loading() {
    return {
      searchMonitoringGlobalTaFopFiles: this.store.select(fromSelectors.selectSearchMonitoringGlobalTaFopFilesLoading)
    };
  }
}
