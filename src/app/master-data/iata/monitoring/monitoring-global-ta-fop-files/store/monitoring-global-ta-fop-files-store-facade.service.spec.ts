import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/monitoring-global-ta-fop-files.actions';
import * as fromSelectors from '../selectors/monitoring-global-ta-fop-files.selectors';

import { MonitoringGlobalTaFopFilesFacadeService } from './monitoring-global-ta-fop-files-store-facade.service';
import { initialState, monitoringGlobalTaFopFilesStateFeatureKey } from './monitoring-global-ta-fop-files.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringGlobalTaFopFilesFacadeService', () => {
  let service: MonitoringGlobalTaFopFilesFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringGlobalTaFopFilesFacadeService,
        provideMockStore({
          initialState: {
            [monitoringGlobalTaFopFilesStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MonitoringGlobalTaFopFilesFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('should dispatch a new searchMonitoringGlobalTaFopFilesRequest action when searchMonitoringGlobalTaFopFiles is called', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchMonitoringGlobalTaFopFiles(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchMonitoringGlobalTaFopFilesRequest({ payload: { query: defaultQuery } })
      );
    });

    it('should dispatch a new downloadMonitoringGlobalTaFopFilesRequest action when downloadMonitoringGlobalTaFopFiles is called', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.downloadMonitoringGlobalTaFopFiles();

      expect(store.dispatch).toHaveBeenCalledWith(fromActions.downloadMonitoringGlobalTaFopFilesRequest());
    });
  });

  describe('selectors', () => {
    it('should select query when selecting query$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringGlobalTaFopFilesQuery);
    });

    it('should select data when selecting data$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringGlobalTaFopFiles);
    });

    it('should select hasData when selecting hasData$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('should invoke selectSearchMonitoringGlobalTaFopFilesLoading when calling searchMonitoringGlobalTaFopFiles$', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchMonitoringGlobalTaFopFiles; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchMonitoringGlobalTaFopFilesLoading);
    });
  });
});
