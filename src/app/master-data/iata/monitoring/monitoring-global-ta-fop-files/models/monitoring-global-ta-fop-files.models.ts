/* eslint-disable @typescript-eslint/naming-convention */
import { GdsSummary } from '~app/shared/models';

export enum EStatus {
  COMPLETED = 'Completed',
  FAILED = 'Failed',
  PENDING = 'Pending'
}

export enum FileTypes {
  TA = 'TA',
  FOP = 'FOP'
}

export interface MonitoringTaFopFileEntryFilters {
  gdsName: GdsSummary[];
  filename: string;
  startDate: Date[];
  endDate: Date[];
  status: EStatus[];
  fileType: string;
}

export interface MonitoringTaFopFileEntryFiltersBE {
  gdsId: number[];
  filename: string;
  fileType: string;
  fromStartDate: string;
  toStartDate: string;
  fromEndDate: string;
  toEndDate: string;
  status: string[];
}

export interface MonitoringGlobalTaFopFileEntry {
  gdsName: string;
  gdsCode: string;
  gdsId: number;
  startDate: string;
  endDate: string;
  fileType: string;
  filename: string;
  status: EStatus;
}
