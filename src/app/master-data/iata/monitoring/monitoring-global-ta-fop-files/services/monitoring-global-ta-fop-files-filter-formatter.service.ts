import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { EStatus, MonitoringTaFopFileEntryFilters } from '../models/monitoring-global-ta-fop-files.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type MonitoringGlobalTaFopFileMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class MonitoringGlobalTaFopFilesFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: MonitoringTaFopFileEntryFilters) {
    const filterMappers: Partial<MonitoringGlobalTaFopFileMapper<MonitoringTaFopFileEntryFilters>> = {
      gdsName: gdsName => `${this.translate('gdsName')} - ${gdsName.map(gds => gds.name)}`,
      filename: filename => `${this.translate('filename')} - ${filename}`,
      status: status => `${this.translate('status')} - ${status.map(s => this.translateStatus(s)).join(', ')}`,
      fileType: fileType => `${this.translate('fileType')} - ${fileType}`,
      startDate: startDate => `${this.translate('startDate')} - ${rangeDateFilterTagMapper(startDate)}`,
      endDate: endDate => `${this.translate('endDate')} - ${rangeDateFilterTagMapper(endDate)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`monitoring-global-ta-fop-files.filters.${key}.label`);
  }

  private translateStatus(status: EStatus): string {
    return this.translationService.translate(`monitoring-global-ta-fop-files.filters.status.option.${status}`);
  }
}
