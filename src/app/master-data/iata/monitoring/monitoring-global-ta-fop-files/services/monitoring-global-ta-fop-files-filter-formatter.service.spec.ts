import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { EStatus, MonitoringTaFopFileEntryFilters } from '../models/monitoring-global-ta-fop-files.models';

import { MonitoringGlobalTaFopFilesFilterFormatterService } from './monitoring-global-ta-fop-files-filter-formatter.service';

const filters = {
  gdsName: [
    {
      id: 1,
      gdsCode: 'code',
      name: 'gsd',
      version: 2
    }
  ],
  filename: 'ESeuDPC_20230503_remitfreq',
  startDate: [new Date('2022-02-10'), new Date('2022-02-12')],
  endDate: [new Date('2022-02-10'), new Date('2022-02-12')],
  fileType: 'ta-top',
  status: [EStatus.COMPLETED]
} as MonitoringTaFopFileEntryFilters;

const translateKey = (key: string) => `monitoring-global-ta-fop-files.filters.${key}.label`;
const translateStatus = (status: EStatus) => `monitoring-global-ta-fop-files.filters.status.option.${status}`;

describe('MonitoringGlobalTaFopFilesFilterFormatterService', () => {
  let formatter: MonitoringGlobalTaFopFilesFilterFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new MonitoringGlobalTaFopFilesFilterFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['gdsName'],
        label: `${translateKey('gdsName')} - gsd`
      },
      {
        keys: ['filename'],
        label: `${translateKey('filename')} - ESeuDPC_20230503_remitfreq`
      },
      {
        keys: ['startDate'],
        label: `${translateKey('startDate')} - 10/02/2022 - 12/02/2022`
      },
      {
        keys: ['endDate'],
        label: `${translateKey('endDate')} - 10/02/2022 - 12/02/2022`
      },
      {
        keys: ['fileType'],
        label: `${translateKey('fileType')} - ta-top`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - ${translateStatus(filters.status[0])}`
      }
    ]);
  });

  it('should return empty array if no filters are passed by parameter to format method', () => {
    const result = formatter.format(null);

    expect(result).toEqual([]);
  });
});
