import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  MonitoringGlobalTaFopFileEntry,
  MonitoringTaFopFileEntryFilters,
  MonitoringTaFopFileEntryFiltersBE
} from '../models/monitoring-global-ta-fop-files.models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class MonitoringGlobalTaFopFilesService implements Queryable<MonitoringGlobalTaFopFileEntry> {
  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}
  private baseUrl = `${this.appConfiguration.baseApiPath}`;

  public find(
    query: DataQuery<MonitoringTaFopFileEntryFilters>
  ): Observable<PagedData<MonitoringGlobalTaFopFileEntry>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<MonitoringGlobalTaFopFileEntry>>(
      this.baseUrl + `/bsp-management/monitoring/global-ta-fop` + requestQuery.getQueryString()
    );
  }

  public download(
    query: DataQuery<MonitoringTaFopFileEntryFilters>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(
    query: DataQuery<MonitoringTaFopFileEntryFilters>
  ): RequestQuery<MonitoringTaFopFileEntryFiltersBE> {
    const { gdsName, startDate, endDate, status, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        gdsId: gdsName && gdsName.map(gds => gds.id),
        fromStartDate: startDate && toShortIsoDate(startDate[0]),
        toStartDate: startDate && toShortIsoDate(startDate[1] || startDate[0]),
        fromEndDate: endDate && toShortIsoDate(endDate[0]),
        toEndDate: endDate && toShortIsoDate(endDate[1] || endDate[0]),
        status
      }
    });
  }
}
