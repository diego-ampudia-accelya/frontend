import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { EStatus, MonitoringGlobalTaFopFileEntry } from '../models/monitoring-global-ta-fop-files.models';
import { MonitoringGlobalTaFopFilesService } from './monitoring-global-ta-fop-files.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MonitoringGlobalTaFopFilesService', () => {
  let service: MonitoringGlobalTaFopFilesService;
  let httpClientSpy: SpyObject<HttpClient>;

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_GLOBAL_TA_AND_FOP_FILES, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  const monitoringRecordsMock: MonitoringGlobalTaFopFileEntry[] = [
    {
      filename: 'ESeuDPC_20230503_remitfreq',
      gdsName: '',
      gdsCode: '',
      gdsId: 0,
      startDate: '',
      endDate: '',
      fileType: '',
      status: EStatus.COMPLETED
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        MonitoringGlobalTaFopFilesService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        provideMockStore({ initialState })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(MonitoringGlobalTaFopFilesService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let monitoringReportData: PagedData<MonitoringGlobalTaFopFileEntry>;

    const auditDataMock: PagedData<MonitoringGlobalTaFopFileEntry> = {
      records: monitoringRecordsMock,
      total: monitoringRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      monitoringReportData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/bsp-management/monitoring/global-ta-fop?page=0&size=20');
    expect(monitoringReportData).toEqual(auditDataMock);
  }));

  it('should return status formatted after calling formatQuery method', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const gdsName = [1, 2];
    const time = [new Date(), new Date()];
    const status = ['status1', 'status2'];

    queryCloneDeep.filterBy.startDate = time;
    queryCloneDeep.filterBy.endDate = time;
    queryCloneDeep.filterBy.gdsName = gdsName;

    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        status,
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['status']).toEqual(status);
  });

  it('should return toStartDate and toEndDate properly when there is not the second date selected for each one', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const time = [new Date('01/01/2023')];
    queryCloneDeep.filterBy.startDate = time;
    queryCloneDeep.filterBy.endDate = time;

    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['formatQuery'](dataQuery);

    expect(formattedQuery.filterBy['toStartDate']).toEqual('2023-01-01');
    expect(formattedQuery.filterBy['toEndDate']).toEqual('2023-01-01');
  });

  it('should call HttpClient get method when download method is called', () => {
    service.download(query, DownloadFormat.CSV);

    expect(httpClientSpy.get).toHaveBeenCalled();
  });
});
