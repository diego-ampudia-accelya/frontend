import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringGlobalTaFopFilesComponent } from './monitoring-global-ta-fop-files.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: '',
    component: MonitoringGlobalTaFopFilesComponent,
    data: {
      tab: ROUTES.MONITOR_GLOBAL_TA_AND_FOP_FILES
    },
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringGlobalTaFopFilesRoutingModule {}
