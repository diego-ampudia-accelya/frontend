import { HttpErrorResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/monitoring-global-ta-fop-files.actions';
import * as fromSelectors from '../selectors/monitoring-global-ta-fop-files.selectors';
import { MonitoringGlobalTaFopFilesService } from '../services/monitoring-global-ta-fop-files.service';
import * as fromState from '../store/monitoring-global-ta-fop-files.state';
import * as fromStub from '../stubs/monitoring-global-ta-fop-files.stubs';

import { MonitoringGlobalTaFopFilesEffects } from './monitoring-global-ta-fop-files.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

describe('MonitoringGlobalTaFopFilesEffects', () => {
  let actions$: Observable<any>;
  let effects: MonitoringGlobalTaFopFilesEffects;
  let service: MonitoringGlobalTaFopFilesService;
  let dialogService: DialogService;

  const initialState = {
    [fromState.monitoringGlobalTaFopFilesStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringGlobalTaFopFilesEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectMonitoringGlobalTaFopFilesQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: MonitoringGlobalTaFopFilesService,
          useValue: {
            find: () => {},
            download: () => {}
          }
        },
        {
          provide: DialogService,
          useValue: {
            open: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<MonitoringGlobalTaFopFilesEffects>(MonitoringGlobalTaFopFilesEffects);
    service = TestBed.inject<MonitoringGlobalTaFopFilesService>(MonitoringGlobalTaFopFilesService);
    dialogService = TestBed.inject<DialogService>(DialogService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchMonitoringGlobalTaFopFilesEffect', () => {
    it('should return searchMonitoringGlobalTaFopFilesSuccess with paged data', () => {
      const action = fromActions.searchMonitoringGlobalTaFopFilesRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchMonitoringGlobalTaFopFilesSuccess({
        payload: {
          pagedData: fromStub.monitoringGlobalTaFopFilesPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.monitoringGlobalTaFopFilesPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchMonitoringGlobalTaFopFilesEffect$).toBeObservable(expected);
    });

    it('should return searchMonitoringGlobalTaFopFilesRequest when monitoring global ta fop files service find fails', () => {
      const action = fromActions.searchMonitoringGlobalTaFopFilesRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchMonitoringGlobalTaFopFilesFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchMonitoringGlobalTaFopFilesEffect$).toBeObservable(expected);
    });
  });

  describe('downloadMonitoringGlobalTaFopFilesRequest', () => {
    it('should call downloadMonitoringGlobalTaFopFilesRequest when opening download dialog', fakeAsync(() => {
      const action = fromActions.downloadMonitoringGlobalTaFopFilesRequest();

      spyOn(dialogService, 'open');

      actions$ = new BehaviorSubject<unknown>(action);

      effects.downloadMonitoringGlobalTaFopFilesEffect$.subscribe();

      tick();

      expect(dialogService.open).toHaveBeenCalledWith(DownloadFileComponent, {
        data: {
          title: 'BUTTON.DEFAULT.DOWNLOAD',
          footerButtonsType: FooterButton.Download,
          downloadQuery: {
            ...defaultQuery,
            filterBy: {
              ...defaultQuery?.filterBy
            }
          }
        },
        apiService: service
      });
    }));
  });
});
