import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as fromActions from '../actions/monitoring-global-ta-fop-files.actions';
import * as fromSelectors from '../selectors/monitoring-global-ta-fop-files.selectors';
import { MonitoringGlobalTaFopFilesService } from '../services/monitoring-global-ta-fop-files.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

@Injectable()
export class MonitoringGlobalTaFopFilesEffects {
  public constructor(
    private readonly actions$: Actions,
    private readonly monitoringGlobalTaFopFilesService: MonitoringGlobalTaFopFilesService,
    private readonly store: Store<unknown>,
    private readonly dialogService: DialogService
  ) {}

  public searchMonitoringGlobalTaFopFilesEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchMonitoringGlobalTaFopFilesRequest>>(
        fromActions.searchMonitoringGlobalTaFopFilesRequest
      ),
      switchMap(({ payload: { query } }) =>
        this.monitoringGlobalTaFopFilesService.find(query).pipe(
          map(data =>
            fromActions.searchMonitoringGlobalTaFopFilesSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchMonitoringGlobalTaFopFilesFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public downloadMonitoringGlobalTaFopFilesEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.downloadMonitoringGlobalTaFopFilesRequest>>(
          fromActions.downloadMonitoringGlobalTaFopFilesRequest
        ),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectMonitoringGlobalTaFopFilesQuery))),
        tap(([_, query]) => {
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.monitoringGlobalTaFopFilesService
          });
        })
      ),
    { dispatch: false }
  );
}
