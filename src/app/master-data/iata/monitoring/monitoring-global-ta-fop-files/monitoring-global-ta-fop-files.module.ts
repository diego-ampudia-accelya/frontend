import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringGlobalTaFopFilesListComponent } from './components/monitoring-global-ta-fop-files-list/monitoring-global-ta-fop-files-list.component';
import { MonitoringGlobalTaFopFilesEffects } from './effects/monitoring-global-ta-fop-files.effects';
import { MonitoringGlobalTaFopFilesRoutingModule } from './monitoring-global-ta-fop-files-routing.module';
import { MonitoringGlobalTaFopFilesComponent } from './monitoring-global-ta-fop-files.component';
import { monitoringGlobalTaFopFilesReducer } from './reducers/monitoring-global-ta-fop-files.reducer';
import { MonitoringGlobalTaFopFilesFilterFormatterService } from './services/monitoring-global-ta-fop-files-filter-formatter.service';
import { MonitoringGlobalTaFopFilesService } from './services/monitoring-global-ta-fop-files.service';
import { MonitoringGlobalTaFopFilesFacadeService } from './store/monitoring-global-ta-fop-files-store-facade.service';
import { monitoringGlobalTaFopFilesStateFeatureKey } from './store/monitoring-global-ta-fop-files.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MonitoringGlobalTaFopFilesRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(monitoringGlobalTaFopFilesStateFeatureKey, monitoringGlobalTaFopFilesReducer),
    EffectsModule.forFeature([MonitoringGlobalTaFopFilesEffects])
  ],
  declarations: [MonitoringGlobalTaFopFilesComponent, MonitoringGlobalTaFopFilesListComponent],
  providers: [
    MonitoringGlobalTaFopFilesService,
    MonitoringGlobalTaFopFilesFacadeService,
    MonitoringGlobalTaFopFilesFilterFormatterService
  ]
})
export class MonitoringGlobalTaFopFilesModule {}
