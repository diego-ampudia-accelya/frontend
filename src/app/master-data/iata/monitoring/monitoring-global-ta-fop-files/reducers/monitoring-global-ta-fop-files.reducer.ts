import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-global-ta-fop-files.actions';
import { initialState } from '../store/monitoring-global-ta-fop-files.state';

export const monitoringGlobalTaFopFilesReducer = createReducer(
  initialState,
  on(fromActions.searchMonitoringGlobalTaFopFilesRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchMonitoringGlobalTaFopFiles: true
    },
    error: {
      ...state.error,
      searchMonitoringGlobalTaFopFiles: null
    }
  })),
  on(fromActions.searchMonitoringGlobalTaFopFilesFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchMonitoringGlobalTaFopFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringGlobalTaFopFiles: error
    }
  })),
  on(fromActions.searchMonitoringGlobalTaFopFilesSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchMonitoringGlobalTaFopFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringGlobalTaFopFiles: null
    }
  }))
);
