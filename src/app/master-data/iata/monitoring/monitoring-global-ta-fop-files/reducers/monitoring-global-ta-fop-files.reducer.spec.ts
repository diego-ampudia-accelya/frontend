import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/monitoring-global-ta-fop-files.actions';
import * as fromState from '../store/monitoring-global-ta-fop-files.state';
import * as fromStub from '../stubs/monitoring-global-ta-fop-files.stubs';

import * as fromReducer from './monitoring-global-ta-fop-files.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoringGlobalTaFopFilesReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.monitoringGlobalTaFopFilesReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchMonitoringGlobalTaFopFilesRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchMonitoringGlobalTaFopFilesRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.monitoringGlobalTaFopFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringGlobalTaFopFiles: true
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringGlobalTaFopFiles: null
        }
      });
    });
  });

  describe('searchMonitoringGlobalTaFopFilesFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchMonitoringGlobalTaFopFilesFailure({ payload: { error } });
      const result = fromReducer.monitoringGlobalTaFopFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringGlobalTaFopFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringGlobalTaFopFiles: error
        }
      });
    });
  });

  describe('searchMonitoringGlobalTaFopFilesSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchMonitoringGlobalTaFopFilesSuccess({
        payload: { pagedData: fromStub.monitoringGlobalTaFopFilesPagedData }
      });
      const result = fromReducer.monitoringGlobalTaFopFilesReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.monitoringGlobalTaFopFilesPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.monitoringGlobalTaFopFilesPagedData.pageNumber,
            size: fromStub.monitoringGlobalTaFopFilesPagedData.pageSize,
            totalElements: fromStub.monitoringGlobalTaFopFilesPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringGlobalTaFopFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringGlobalTaFopFiles: null
        }
      });
    });
  });
});
