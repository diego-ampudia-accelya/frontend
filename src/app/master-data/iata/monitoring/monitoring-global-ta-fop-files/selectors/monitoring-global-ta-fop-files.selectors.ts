import { createFeatureSelector, createSelector } from '@ngrx/store';

import {
  MonitoringGlobalTaFopFilesState,
  monitoringGlobalTaFopFilesStateFeatureKey
} from '../store/monitoring-global-ta-fop-files.state';

export const selectMonitoringGlobalTaFopFilesStateFeature = createFeatureSelector<MonitoringGlobalTaFopFilesState>(
  monitoringGlobalTaFopFilesStateFeatureKey
);

export const selectMonitoringGlobalTaFopFiles = createSelector(
  selectMonitoringGlobalTaFopFilesStateFeature,
  state => state.data
);
export const selectMonitoringGlobalTaFopFilesQuery = createSelector(
  selectMonitoringGlobalTaFopFilesStateFeature,
  state => state.query
);
export const selectSearchMonitoringGlobalTaFopFilesLoading = createSelector(
  selectMonitoringGlobalTaFopFilesStateFeature,
  state => state.loading.searchMonitoringGlobalTaFopFiles
);

export const selectHasData = createSelector(selectMonitoringGlobalTaFopFilesStateFeature, state =>
  Boolean(state.data && state.data.length)
);
