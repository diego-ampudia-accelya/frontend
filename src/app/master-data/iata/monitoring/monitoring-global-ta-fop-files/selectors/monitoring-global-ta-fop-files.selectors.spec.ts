import { EStatus } from '../models/monitoring-global-ta-fop-files.models';
import { MonitoringGlobalTaFopFilesState } from '../store/monitoring-global-ta-fop-files.state';

import {
  selectHasData,
  selectMonitoringGlobalTaFopFiles,
  selectMonitoringGlobalTaFopFilesQuery,
  selectMonitoringGlobalTaFopFilesStateFeature,
  selectSearchMonitoringGlobalTaFopFilesLoading
} from './monitoring-global-ta-fop-files.selectors';

describe('monitoring global ta fop files selectors', () => {
  const mockState: MonitoringGlobalTaFopFilesState = {
    data: [
      {
        filename: 'ESeuDPC_20230503_remitfreq',
        gdsName: 'gdsName',
        gdsCode: 'code',
        gdsId: 0,
        startDate: '2023-01-01',
        endDate: '2023-01-01',
        fileType: 'type',
        status: EStatus.COMPLETED
      },
      {
        filename: 'ESeuDPC_20230503_remitfreq',
        gdsName: 'gdsName',
        gdsCode: 'code',
        gdsId: 0,
        startDate: '2023-01-01',
        endDate: '2023-01-01',
        fileType: 'type',
        status: EStatus.COMPLETED
      }
    ],
    query: {
      filterBy: {},
      paginateBy: { size: 20, page: 0 },
      sortBy: []
    },
    loading: {
      searchMonitoringGlobalTaFopFiles: true
    },
    error: null
  };

  describe('selectMonitoringGlobalTaFopFilesStateFeature', () => {
    it('should return the monitoringGlobalTaFopFiles state feature', () => {
      const result = selectMonitoringGlobalTaFopFilesStateFeature.projector(mockState);
      expect(result).toBe(mockState);
    });
  });

  describe('selectMonitoringGlobalTaFopFiles', () => {
    it('should return the data from the monitoringGlobalTaFopFiles state', () => {
      const result = selectMonitoringGlobalTaFopFiles.projector(mockState);
      expect(result).toBe(mockState.data);
    });
  });

  describe('selectMonitoringGlobalTaFopFilesQuery', () => {
    it('should return the query from the monitoringGlobalTaFopFiles state', () => {
      const result = selectMonitoringGlobalTaFopFilesQuery.projector(mockState);
      expect(result).toBe(mockState.query);
    });
  });

  describe('selectSearchMonitoringGlobalTaFopFilesLoading', () => {
    it('should return the searchMonitoringGlobalTaFopFiles loading value from the monitoringGlobalTaFopFiles state', () => {
      const result = selectSearchMonitoringGlobalTaFopFilesLoading.projector(mockState);
      expect(result).toBe(mockState.loading.searchMonitoringGlobalTaFopFiles);
    });
  });

  describe('selectHasData', () => {
    it('should return true if the monitoringGlobalTaFopFiles data array exists and has a length greater than 0', () => {
      const result = selectHasData.projector(mockState);
      expect(result).toBe(true);
    });

    it('should return false if the monitoringGlobalTaFopFiles data array is empty or undefined', () => {
      const emptyState = {
        data: [],
        query: [],
        loading: {
          searchMonitoringGlobalTaFopFiles: []
        }
      };
      const result = selectHasData.projector(emptyState);
      expect(result).toBe(false);
    });
  });
});
