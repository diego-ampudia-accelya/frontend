import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  EStatus,
  FileTypes,
  MonitoringGlobalTaFopFileEntry,
  MonitoringTaFopFileEntryFilters
} from '../../models/monitoring-global-ta-fop-files.models';
import { MonitoringGlobalTaFopFilesFilterFormatterService } from '../../services/monitoring-global-ta-fop-files-filter-formatter.service';
import { MonitoringGlobalTaFopFilesFacadeService } from '../../store/monitoring-global-ta-fop-files-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';

@Component({
  selector: 'bspl-monitoring-global-ta-fop-files-list',
  templateUrl: './monitoring-global-ta-fop-files-list.component.html'
})
export class MonitoringGlobalTaFopFilesListComponent implements OnInit {
  public columns: Array<GridColumn>;
  public hasAllBspsPermission: boolean;
  public userBspDropdownOptions$: Observable<DropdownOption<BspDto>[]>;
  public gdsDropdownOptions$ = this.gdsDictionaryService.getDropdownOptionsForFiltering();

  public statusOptions = [
    {
      value: EStatus.COMPLETED,
      label: this.translationService.translate('monitoring-global-ta-fop-files.filters.status.option.Completed')
    },
    {
      value: EStatus.FAILED,
      label: this.translationService.translate('monitoring-global-ta-fop-files.filters.status.option.Failed')
    },
    {
      value: EStatus.PENDING,
      label: this.translationService.translate('monitoring-global-ta-fop-files.filters.status.option.Pending')
    }
  ];

  public fileTypeOptions = [
    {
      value: FileTypes.TA,
      label: this.translationService.translate('monitoring-global-ta-fop-files.filters.fileType.option.TA')
    },
    {
      value: FileTypes.FOP,
      label: this.translationService.translate('monitoring-global-ta-fop-files.filters.fileType.option.FOP')
    }
  ];

  public data$: Observable<MonitoringGlobalTaFopFileEntry[]> =
    this.monitoringGlobalTaFopFilesFacadeService.selectors.data$;
  public query$: Observable<DataQuery<MonitoringTaFopFileEntryFilters>> =
    this.monitoringGlobalTaFopFilesFacadeService.selectors.query$;
  public loading$: Observable<boolean> =
    this.monitoringGlobalTaFopFilesFacadeService.loading.searchMonitoringGlobalTaFopFiles;
  public hasData$: Observable<boolean> = this.monitoringGlobalTaFopFilesFacadeService.selectors.hasData$;

  public filterFormGroup: FormGroup;

  public get title$(): Observable<string> {
    return this.query$.pipe(
      map(query => query?.paginateBy?.totalElements),
      map(
        length => `${this.translationService.translate('monitoring-global-ta-fop-files.view.title')} (${length || 0})`
      )
    );
  }

  public get isBspFilterDisabled$(): Observable<boolean> {
    return this.userBspDropdownOptions$.pipe(map(userBspDropdownOptions => Boolean(userBspDropdownOptions?.length)));
  }

  constructor(
    public readonly monitoringGlobalTaFopFilesFilterFormatterService: MonitoringGlobalTaFopFilesFilterFormatterService,
    private readonly permissionService: PermissionsService,
    private readonly translationService: L10nTranslationService,
    private readonly monitoringGlobalTaFopFilesFacadeService: MonitoringGlobalTaFopFilesFacadeService,
    private readonly gdsDictionaryService: GdsDictionaryService
  ) {}

  public ngOnInit(): void {
    this.columns = this.buildColumns();
    this.initializePermissions();
    this.initializeFormGroup();
    this.initializeBspAndDropdownOptions();
  }

  public onQueryChanged(query: DataQuery<MonitoringTaFopFileEntryFilters>): void {
    this.monitoringGlobalTaFopFilesFacadeService.actions.searchMonitoringGlobalTaFopFiles(query);
  }

  public onDownload(): void {
    this.monitoringGlobalTaFopFilesFacadeService.actions.downloadMonitoringGlobalTaFopFiles();
  }

  private getTranslatedFileType(fileType: FileTypes): string {
    return this.translationService.translate(`monitoring-global-ta-fop-files.filters.fileType.option.${fileType}`);
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'gdsName',
        name: 'monitoring-global-ta-fop-files.columns.gdsName',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'filename',
        name: 'monitoring-global-ta-fop-files.columns.filename',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'startDate',
        name: 'monitoring-global-ta-fop-files.columns.startDate',
        resizeable: false,
        draggable: false,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'endDate',
        name: 'monitoring-global-ta-fop-files.columns.endDate',
        resizeable: true,
        draggable: false,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY HH:mm:ss') }
      },
      {
        prop: 'status',
        name: 'monitoring-global-ta-fop-files.columns.status',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'fileType',
        name: 'monitoring-global-ta-fop-files.columns.fileType',
        resizeable: true,
        draggable: false,
        pipe: { transform: fileType => this.getTranslatedFileType(fileType) }
      }
    ];
  }

  private initializePermissions(): void {
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private initializeFormGroup(): void {
    this.filterFormGroup = new FormGroup({
      gdsName: new FormControl(null, []),
      filename: new FormControl(null, []),
      status: new FormControl(null, []),
      startDate: new FormControl(null, []),
      endDate: new FormControl(null, []),
      fileType: new FormControl(null, [])
    });
  }

  private initializeBspAndDropdownOptions(): void {
    this.userBspDropdownOptions$ = this.monitoringGlobalTaFopFilesFacadeService.selectors.userBsps$.pipe(
      map(bspList => bspList.map(bsp => toValueLabelObjectBsp(bsp)))
    );

    this.monitoringGlobalTaFopFilesFacadeService.actions.searchMonitoringGlobalTaFopFiles({
      ...defaultQuery,
      filterBy: {
        ...defaultQuery.filterBy
      },
      sortBy: [{ attribute: 'startDate', sortType: SortOrder.Desc }]
    });
  }
}
