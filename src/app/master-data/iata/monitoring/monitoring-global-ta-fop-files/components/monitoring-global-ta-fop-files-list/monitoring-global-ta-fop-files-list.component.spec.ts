import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration } from 'ng-mocks';
import { of } from 'rxjs';

import { FileTypes, MonitoringTaFopFileEntryFilters } from '../../models/monitoring-global-ta-fop-files.models';
import { MonitoringGlobalTaFopFilesFilterFormatterService } from '../../services/monitoring-global-ta-fop-files-filter-formatter.service';
import { MonitoringGlobalTaFopFilesFacadeService } from '../../store/monitoring-global-ta-fop-files-store-facade.service';
import * as fromStub from '../../stubs/monitoring-global-ta-fop-files.stubs';
import { MonitoringGlobalTaFopFilesListComponent } from './monitoring-global-ta-fop-files-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { DataQuery, ListViewComponent } from '~app/shared/components/list-view';

describe('MonitoringGlobalTaFopFilesListComponent', () => {
  let component: MonitoringGlobalTaFopFilesListComponent;
  let fixture: ComponentFixture<MonitoringGlobalTaFopFilesListComponent>;
  let monitoringGlobalTaFopFilesFacadeService: MonitoringGlobalTaFopFilesFacadeService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      declarations: [
        MonitoringGlobalTaFopFilesListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        { provide: L10nTranslationService, useValue: translationSpy },
        {
          provide: MonitoringGlobalTaFopFilesFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: MonitoringGlobalTaFopFilesFacadeService,
          useValue: {
            actions: {
              searchMonitoringGlobalTaFopFiles: () => {},
              downloadMonitoringGlobalTaFopFiles: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null),
              userBsps$: of(fromStub.userBsps),
              user$: of(fromStub.user)
            },
            loading: {
              searchMonitoringGlobalTaFopFiles$: of(null)
            }
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        },
        {
          provide: GdsDictionaryService,
          useValue: {
            getDropdownOptionsForFiltering: () => {}
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringGlobalTaFopFilesListComponent);
    component = fixture.componentInstance;
    monitoringGlobalTaFopFilesFacadeService = TestBed.inject<MonitoringGlobalTaFopFilesFacadeService>(
      MonitoringGlobalTaFopFilesFacadeService
    );
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get translated file type when the value is passed to transform pipe for Filetype column', () => {
    const testValue = 'FOP';
    const expectedResultValue = 'Global FOP File';
    spyOn<any>(component, 'getTranslatedFileType').and.returnValue(expectedResultValue);
    const columns = component['buildColumns']();

    const fileTypeColumn = columns.find(column => column.prop === 'fileType');

    component['buildColumns']();

    expect(fileTypeColumn.pipe.transform(testValue)).toBe(expectedResultValue);
  });

  it('should return and translate a key when it is passed by parameter to getTranslatedFileType method', () => {
    const testValue = FileTypes.TA;
    const expectedTranslationResult = 'Global TA File';
    translationSpy.translate.and.returnValue(expectedTranslationResult);

    const result = component['getTranslatedFileType'](testValue);

    expect(result).toBe(expectedTranslationResult);
  });

  it('should transform dates values using moment function format for startDate and endDate column properties', () => {
    const testValue = '01/01/2023';
    const expectedTransformedValue = '01/01/2023 00:00:00';
    const startDateColumn = component.columns.find(column => column.prop === 'startDate');
    const endDateColumn = component.columns.find(column => column.prop === 'endDate');

    component['buildColumns']();

    expect(startDateColumn.pipe.transform(testValue)).toBe(expectedTransformedValue);
    expect(endDateColumn.pipe.transform(testValue)).toBe(expectedTransformedValue);
  });

  it('should call downloadMonitoringGlobalTaFopFiles method when onDownload is called', () => {
    const downloadMonitoringGlobalTaFopFilesSpy = spyOn(
      monitoringGlobalTaFopFilesFacadeService.actions,
      'downloadMonitoringGlobalTaFopFiles'
    );

    component.onDownload();

    expect(downloadMonitoringGlobalTaFopFilesSpy).toHaveBeenCalled();
  });

  it('should call searchMonitoringGlobalTaFopFiles method when onQueryChanged is called', () => {
    const query: DataQuery<MonitoringTaFopFileEntryFilters> = {
      paginateBy: { page: 0, size: 20, totalElements: 33256 },
      sortBy: null,
      filterBy: { gdsName: [{ id: 82828282, gdsCode: 'RRRR', name: 'RRRRRR', version: 94 }] }
    };
    const searchMonitoringGlobalTaFopFilesSpy = spyOn(
      monitoringGlobalTaFopFilesFacadeService.actions,
      'searchMonitoringGlobalTaFopFiles'
    );

    component.onQueryChanged(query);

    expect(searchMonitoringGlobalTaFopFilesSpy).toHaveBeenCalled();
  });

  it('should return true when there are user Bsp Dropdown Options', fakeAsync(() => {
    let result: boolean;
    component['isBspFilterDisabled$'].subscribe(isBspFilterDisabled => (result = isBspFilterDisabled));
    tick();

    expect(result).toBe(true);
  }));

  it('should return false when there are not user Bsp Dropdown Options', fakeAsync(() => {
    component['userBspDropdownOptions$'] = of(null);
    let result: boolean;

    component['isBspFilterDisabled$'].subscribe(isBspFilterDisabled => (result = isBspFilterDisabled));
    tick();

    expect(result).toBe(false);
  }));

  it('should return the title string when it is subscribed to title$', fakeAsync(() => {
    let titleResult: string;
    component['query$'] = of({
      paginateBy: { page: 0, size: 20, totalElements: 33256 },
      sortBy: null,
      filterBy: { gdsName: [{ id: 82828282, gdsCode: 'RRRR', name: 'RRRRRR', version: 94 }] }
    });

    component['title$'].subscribe(title => (titleResult = title));
    tick();

    expect(translationSpy.translate).toHaveBeenCalled();
    expect(titleResult).toBeDefined();
  }));
});
