import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import {
  MonitoringGlobalTaFopFileEntry,
  MonitoringTaFopFileEntryFilters
} from '../models/monitoring-global-ta-fop-files.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchMonitoringGlobalTaFopFilesRequest = createAction(
  '[Monitoring Global Ta Fop Files]Search Monitoring Global Ta Fop Files Request',
  props<{ payload: { query?: DataQuery<MonitoringTaFopFileEntryFilters> } }>()
);

export const searchMonitoringGlobalTaFopFilesSuccess = createAction(
  '[Monitoring Global Ta Fop Files]Search Monitoring Global Ta Fop Files Success',
  props<{ payload: { pagedData: PagedData<MonitoringGlobalTaFopFileEntry> } }>()
);

export const searchMonitoringGlobalTaFopFilesFailure = createAction(
  '[Monitoring Global Ta Fop Files]Search Monitoring Global Ta Fop Files Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const downloadMonitoringGlobalTaFopFilesRequest = createAction(
  '[Monitoring Global Ta Fop Files]Download Monitoring Global Ta Fop Files Request'
);
