import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonitoringHotDetailsComponent } from './monitoring-hot-details.component';
import { ROUTES } from '~app/shared/constants';

const routes: Routes = [
  {
    path: ROUTES.MONITOR_HOT_DATA_DETAILS.path,
    component: MonitoringHotDetailsComponent,
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringHotDetailsFilesRoutingModule {}
