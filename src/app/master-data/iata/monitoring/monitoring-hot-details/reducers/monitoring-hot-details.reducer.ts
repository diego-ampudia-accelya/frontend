import { createReducer, on } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-details.actions';
import { initialState } from '../store/monitoring-hot-details.state';

export const monitoringHotDetailsReducer = createReducer(
  initialState,
  on(fromActions.searchMonitoringHotDetailsFilesRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchMonitoringHotDetailsFiles: true
    },
    error: {
      ...state.error,
      searchMonitoringHotDetailsFiles: null
    }
  })),
  on(fromActions.searchMonitoringHotDetailsFilesFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchMonitoringHotDetailsFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringHotDetailsFiles: error
    }
  })),
  on(fromActions.searchMonitoringHotDetailsFilesSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchMonitoringHotDetailsFiles: false
    },
    error: {
      ...state.error,
      searchMonitoringHotDetailsFiles: null
    }
  }))
);
