import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/monitoring-hot-details.actions';
import * as fromState from '../store/monitoring-hot-details.state';
import * as fromStub from '../stubs/monitoring-hot-details.stubs';

import * as fromReducer from './monitoring-hot-details.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoringHotDetailsReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.monitoringHotDetailsReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchMonitoringHotDetailsFilesRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchMonitoringHotDetailsFilesRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.monitoringHotDetailsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringHotDetailsFiles: true
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringHotDetailsFiles: null
        }
      });
    });
  });

  describe('searchMonitoringHotDetailsFilesFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchMonitoringHotDetailsFilesFailure({ payload: { error } });
      const result = fromReducer.monitoringHotDetailsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringHotDetailsFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringHotDetailsFiles: error
        }
      });
    });
  });

  describe('searchMonitoringHotDetailsFilesSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchMonitoringHotDetailsFilesSuccess({
        payload: { pagedData: fromStub.monitoringHotDetailsPagedData }
      });
      const result = fromReducer.monitoringHotDetailsReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.monitoringHotDetailsPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.monitoringHotDetailsPagedData.pageNumber,
            size: fromStub.monitoringHotDetailsPagedData.pageSize,
            totalElements: fromStub.monitoringHotDetailsPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchMonitoringHotDetailsFiles: false
        },
        error: {
          ...fromState.initialState.error,
          searchMonitoringHotDetailsFiles: null
        }
      });
    });
  });
});
