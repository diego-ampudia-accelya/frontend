import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { EStatus, MonitoringHotDetailsEntryFilters } from '../models/monitoring-hot-details.models';

import { MonitoringHotDetailsFilesFilterFormatterService } from './monitoring-hot-details-filter-formatter.service';

const filters = {
  documentNumber: '1',
  agentCode: ['006'],
  airlineCode: ['102'],
  fieldContent: 'field content test',
  fieldElement: 'field element',
  message: 'test message',
  transactionCode: ['TRC'],
  status: [EStatus.ERROR]
} as MonitoringHotDetailsEntryFilters;

const translateKey = (key: string) => `monitoring-hot-details.filters.${key}.label`;
const translateStatus = (status: EStatus) => `monitoring-hot-details.filters.status.option.${status}`;

describe('MonitoringHotDetailsFilesFilterFormatterService', () => {
  let formatter: MonitoringHotDetailsFilesFilterFormatterService;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new MonitoringHotDetailsFilesFilterFormatterService(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['documentNumber'],
        label: `${translateKey('documentNumber')} - 1`
      },
      {
        keys: ['agentCode'],
        label: `${translateKey('agentCode')} - 006`
      },
      {
        keys: ['airlineCode'],
        label: `${translateKey('airlineCode')} - 102`
      },
      {
        keys: ['fieldContent'],
        label: `${translateKey('fieldContent')} - field content test`
      },
      {
        keys: ['fieldElement'],
        label: `${translateKey('fieldElement')} - field element`
      },
      {
        keys: ['message'],
        label: `${translateKey('message')} - test message`
      },
      {
        keys: ['transactionCode'],
        label: `${translateKey('transactionCode')} - TRC`
      },
      {
        keys: ['status'],
        label: `${translateKey('status')} - ${translateStatus(filters.status[0])}`
      }
    ]);
  });

  it('should return empty array if no filters are passed by parameter to format method', () => {
    const result = formatter.format(null);

    expect(result).toEqual([]);
  });
});
