import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { EStatus, MonitoringHotDetailsEntryFilters } from '../models/monitoring-hot-details.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';

type MonitoringHotDetailsFileMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class MonitoringHotDetailsFilesFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: MonitoringHotDetailsEntryFilters) {
    const filterMappers: Partial<MonitoringHotDetailsFileMapper<MonitoringHotDetailsEntryFilters>> = {
      documentNumber: documentNumber => `${this.translate('documentNumber')} - ${documentNumber}`,
      airlineCode: airlineCode => `${this.translate('airlineCode')} - ${airlineCode.map(airline => airline)}`,
      agentCode: agentCode => `${this.translate('agentCode')} - ${agentCode.map(agent => agent)}`,
      status: status => `${this.translate('status')} - ${status.map(s => this.translateStatus(s)).join(', ')}`,
      transactionCode: transactionCode =>
        `${this.translate('transactionCode')} - ${transactionCode.map(transaction => transaction)}`,
      fieldElement: fieldElement => `${this.translate('fieldElement')} - ${fieldElement}`,
      fieldContent: fieldContent => `${this.translate('fieldContent')} - ${fieldContent}`,
      message: message => `${this.translate('message')} - ${message}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`monitoring-hot-details.filters.${key}.label`);
  }

  private translateStatus(status: EStatus): string {
    return this.translationService.translate(`monitoring-hot-details.filters.status.option.${status}`);
  }
}
