import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';

import { MonitorHotDetailsIdResolver } from './monitoring-hot-details-resolvers';

describe('MonitorHotDetailsIdResolver', () => {
  let resolver: MonitorHotDetailsIdResolver;

  const routerMock = {
    params: { id: '123' }
  } as unknown as ActivatedRouteSnapshot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MonitorHotDetailsIdResolver]
    });

    resolver = TestBed.inject(MonitorHotDetailsIdResolver);
  });

  it('should resolve the ID from ActivatedRouteSnapshot', () => {
    const resolvedId = resolver.resolve(routerMock);

    expect(resolvedId).toBe(routerMock.params.id);
  });
});
