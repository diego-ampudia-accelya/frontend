import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import {
  EStatus,
  MonitoringHotDetailsEntryFilters,
  MonitoringHotDetailsFileEntry
} from '../models/monitoring-hot-details.models';
import { MonitoringHotDetailsFilesFacadeService } from '../store/monitoring-hot-details-store-facade.service';
import { MonitoringHotDetailsFilesService } from './monitoring-hot-details.service';
import { MenuService } from '~app/core/services';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MonitoringHotDetailsFilesService', () => {
  let service: MonitoringHotDetailsFilesService;
  let httpClientSpy: SpyObject<HttpClient>;
  const menuServiceSpy = createSpyObject(MenuService);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { monitoringQuery: { ...ROUTES.MONITOR_HOT_DATA_DETAILS, id: 'monitoringQuery' } },
        activeTabId: 'monitoringQuery'
      },
      viewListsInfo: {}
    }
  };

  const activatedRouteStub = {
    snapshot: {
      data: {
        tableTitle: 'Title',
        type: 'title',
        tab: ROUTES.MONITOR_HOT_DATA_DETAILS
      }
    }
  };

  const monitoringRecordsMock: MonitoringHotDetailsFileEntry[] = [
    {
      agentCode: '006',
      airlineCode: '102',
      documentNumber: '1',
      fieldContent: 'field content test',
      fieldElement: 'field element',
      message: 'test message',
      status: EStatus.ERROR,
      transactionCode: 'TRC'
    }
  ];

  const monitoringFilterMock: MonitoringHotDetailsEntryFilters[] = [
    {
      agentCode: ['006'],
      airlineCode: ['102'],
      documentNumber: '1',
      fieldContent: 'field content test',
      fieldElement: 'field element',
      message: 'test message',
      status: [EStatus.ERROR],
      transactionCode: ['TRC']
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  const monitoringHotDetailsFilesFacadeServiceSpy = {
    actions: {
      searchMonitoringHotDetailsFiles: () => {},
      downloadMonitoringHotDetailsFiles: () => {}
    },
    selectors: {
      query$: of(null),
      data$: of(null),
      hasData$: of(null)
    },
    loading: {
      searchMonitoringHotDetailsFiles$: of(null)
    }
  };
  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        MonitoringHotDetailsFilesService,
        { provide: HttpClient, useValue: httpClientSpy },
        {
          provide: MonitoringHotDetailsFilesFacadeService,
          useValue: monitoringHotDetailsFilesFacadeServiceSpy
        },
        {
          provide: MenuService,
          useValue: menuServiceSpy
        },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        provideMockStore({ initialState })
      ],
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    service = TestBed.inject(MonitoringHotDetailsFilesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should build request query filtering by the filters selected for the user, airlineCode, agentCode and transactionCode', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    const expectedAirlineCodeResult = monitoringFilterMock[0].airlineCode;
    const expectedAgentCodeResult = monitoringFilterMock[0].agentCode;
    const expectedTransactionCodeResult = monitoringFilterMock[0].transactionCode;

    queryCloneDeep.filterBy.airlineCode = monitoringFilterMock[0].airlineCode;
    queryCloneDeep.filterBy.agentCode = monitoringFilterMock[0].agentCode;
    queryCloneDeep.filterBy.transactionCode = monitoringFilterMock[0].transactionCode;

    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['buildRequestQuery'](dataQuery);

    expect(formattedQuery.filterBy['airlineCode']).toEqual(expectedAirlineCodeResult);
    expect(formattedQuery.filterBy['agentCode']).toEqual(expectedAgentCodeResult);
    expect(formattedQuery.filterBy['transactionCode']).toEqual(expectedTransactionCodeResult);
  });

  it('should not format filters like airlineCode, agentCode and transactionCode when their values are null or undefined', () => {
    const queryCloneDeep = cloneDeep(defaultQuery);
    queryCloneDeep.filterBy.airlineCode = null;
    queryCloneDeep.filterBy.agentCode = null;
    queryCloneDeep.filterBy.transactionCode = null;

    const dataQuery: DataQuery = {
      ...queryCloneDeep,
      filterBy: {
        ...queryCloneDeep.filterBy
      }
    };
    const formattedQuery = service['buildRequestQuery'](dataQuery);

    expect(formattedQuery.filterBy['airlineCode']).toEqual(null);
    expect(formattedQuery.filterBy['agentCode']).toEqual(null);
    expect(formattedQuery.filterBy['transactionCode']).toEqual(null);
  });

  it('should call HttpClient get method when download method is called', () => {
    service.download(query, DownloadFormat.CSV);

    expect(httpClientSpy.get).toHaveBeenCalled();
  });

  it('should subscribe to find method and return page data as MonitoringHotDetailsFileEntry', fakeAsync(() => {
    let monitoringReportData: PagedData<MonitoringHotDetailsFileEntry>;
    menuServiceSpy.findActiveTabConfig.and.returnValue('MENU.MASTER_DATA.MONITOR_GLOBAL_TA_AND_FOP_FILES.LBL');

    const auditDataMock: PagedData<MonitoringHotDetailsFileEntry> = {
      records: monitoringRecordsMock,
      total: monitoringRecordsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(auditDataMock));
    service.find(query).subscribe(res => {
      monitoringReportData = res;
    });
    tick();

    expect(monitoringReportData).toEqual(auditDataMock);
  }));
});
