import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { clone } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  MonitoringHotDetailsEntryFilters,
  MonitoringHotDetailsFileEntry
} from '../models/monitoring-hot-details.models';
import { MenuService } from '~app/core/services';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class MonitoringHotDetailsFilesService implements Queryable<MonitoringHotDetailsFileEntry> {
  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private menuService: MenuService,
    private router: Router
  ) {}
  private baseUrl = `${this.appConfiguration.baseApiPath}`;

  public find(
    query: DataQuery<MonitoringHotDetailsEntryFilters>
  ): Observable<PagedData<MonitoringHotDetailsFileEntry>> {
    const requestQuery = this.buildRequestQuery(query);
    const hotFileName = this.menuService.findActiveTabConfig(this.router.routerState.snapshot).tabLabel;

    return this.http.get<PagedData<MonitoringHotDetailsFileEntry>>(
      this.baseUrl + `/monitoring-service/hot-files/` + hotFileName + requestQuery.getQueryString()
    );
  }

  private buildRequestQuery(query: DataQuery<MonitoringHotDetailsEntryFilters>) {
    const _query = JSON.parse(JSON.stringify(query));

    if (_query.filterBy.airlineCode) {
      const airlineCode = clone(_query.filterBy.airlineCode);
      delete _query.filterBy.airlineCode;

      _query.filterBy['airlineCode'] = airlineCode.map(airline => airline);
    }

    if (_query.filterBy.agentCode) {
      const agentCode = clone(_query.filterBy.agentCode);

      delete _query.filterBy.agentCode;
      _query.filterBy['agentCode'] = agentCode.map(agent => agent);
    }

    if (_query.filterBy.transactionCode) {
      const transactionCode = clone(_query.filterBy.transactionCode);

      delete _query.filterBy.transactionCode;
      _query.filterBy['transactionCode'] = transactionCode.map(transaction => transaction);
    }

    return RequestQuery.fromDataQuery(_query);
  }

  public download(
    query: DataQuery<MonitoringHotDetailsEntryFilters>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.buildRequestQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
