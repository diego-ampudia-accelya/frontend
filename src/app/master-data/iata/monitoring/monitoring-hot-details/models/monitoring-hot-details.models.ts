/* eslint-disable @typescript-eslint/naming-convention */
export enum EStatus {
  ERROR = 'error',
  WARNING = 'warning'
}

export interface DropdownItem {
  value: string;
  label: string;
}

export interface MonitoringHotDetailsEntryFilters {
  agentCode: string[];
  airlineCode: string[];
  documentNumber: string;
  fieldContent: string;
  fieldElement: string;
  message: string;
  status: EStatus[];
  transactionCode: string[];
}

export interface MonitoringHotDetailsFileEntry {
  agentCode: string;
  airlineCode: string;
  documentNumber: string;
  fieldContent: string;
  fieldElement: string;
  message: string;
  status: EStatus;
  transactionCode: string;
}
