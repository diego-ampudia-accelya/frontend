import { createFeatureSelector, createSelector } from '@ngrx/store';

import {
  MonitoringHotDetailsFilesState,
  monitoringHotDetailsStateFeatureKey
} from '../store/monitoring-hot-details.state';

export const selectMonitoringHotDetailsFilesStateFeature = createFeatureSelector<MonitoringHotDetailsFilesState>(
  monitoringHotDetailsStateFeatureKey
);

export const selectMonitoringHotDetailsFiles = createSelector(
  selectMonitoringHotDetailsFilesStateFeature,
  state => state.data
);
export const selectMonitoringHotDetailsFilesQuery = createSelector(
  selectMonitoringHotDetailsFilesStateFeature,
  state => state.query
);
export const selectSearchMonitoringHotDetailsFilesLoading = createSelector(
  selectMonitoringHotDetailsFilesStateFeature,
  state => state.loading.searchMonitoringHotDetailsFiles
);

export const selectHasData = createSelector(selectMonitoringHotDetailsFilesStateFeature, state =>
  Boolean(state.data && state.data.length)
);
