import { MonitoringHotDetailsFilesState } from '../store/monitoring-hot-details.state';

import {
  selectHasData,
  selectMonitoringHotDetailsFiles,
  selectMonitoringHotDetailsFilesQuery,
  selectMonitoringHotDetailsFilesStateFeature,
  selectSearchMonitoringHotDetailsFilesLoading
} from './monitoring-hot-details.selectors';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('monitoring hot details files selectors', () => {
  const mockState: MonitoringHotDetailsFilesState = {
    query: defaultQuery,
    data: [],
    loading: {
      searchMonitoringHotDetailsFiles: false
    },
    error: {
      searchMonitoringHotDetailsFiles: null
    }
  };

  it('should return MonitoringHotDetailsFilesState state', () => {
    const result = selectMonitoringHotDetailsFilesStateFeature.projector(mockState);

    expect(result).toBe(mockState);
  });

  it('should select and return monitoring hot details files data', () => {
    const result = selectMonitoringHotDetailsFiles.projector(mockState);

    expect(result).toBe(mockState.data);
  });

  it('should select and return monitoring hot details files query', () => {
    const result = selectMonitoringHotDetailsFilesQuery.projector(mockState);

    expect(result).toBe(mockState.query);
  });

  it('should check if search monitoring hot details files is loading', () => {
    const result = selectSearchMonitoringHotDetailsFilesLoading.projector(mockState);

    expect(result).toBe(mockState.loading.searchMonitoringHotDetailsFiles);
  });

  it('should return false if the state has not data', () => {
    const result = selectHasData.projector(mockState);

    expect(result).toBe(false);
  });
});
