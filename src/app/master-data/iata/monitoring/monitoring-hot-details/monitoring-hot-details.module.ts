import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { MonitoringHotDetailsListComponent } from './components/monitoring-hot-details-list/monitoring-hot-details-list.component';
import { MonitoringHotDetailsFilesEffects } from './effects/monitoring-hot-details.effects';
import { MonitoringHotDetailsFilesRoutingModule } from './monitoring-hot-details-routing.module';
import { MonitoringHotDetailsComponent } from './monitoring-hot-details.component';
import { monitoringHotDetailsReducer } from './reducers/monitoring-hot-details.reducer';
import { MonitoringHotDetailsFilesFilterFormatterService } from './services/monitoring-hot-details-filter-formatter.service';
import { MonitorHotDetailsIdResolver } from './services/monitoring-hot-details-resolvers';
import { MonitoringHotDetailsFilesService } from './services/monitoring-hot-details.service';
import { MonitoringHotDetailsFilesFacadeService } from './store/monitoring-hot-details-store-facade.service';
import { monitoringHotDetailsStateFeatureKey } from './store/monitoring-hot-details.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MonitoringHotDetailsFilesRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(monitoringHotDetailsStateFeatureKey, monitoringHotDetailsReducer),
    EffectsModule.forFeature([MonitoringHotDetailsFilesEffects])
  ],
  declarations: [MonitoringHotDetailsComponent, MonitoringHotDetailsListComponent],
  providers: [
    MonitoringHotDetailsFilesService,
    MonitoringHotDetailsFilesFacadeService,
    MonitoringHotDetailsFilesFilterFormatterService,
    MonitorHotDetailsIdResolver
  ]
})
export class MonitoringHotDetailsModule {}
