import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as fromActions from '../actions/monitoring-hot-details.actions';
import * as fromSelectors from '../selectors/monitoring-hot-details.selectors';
import { MonitoringHotDetailsFilesService } from '../services/monitoring-hot-details.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

@Injectable()
export class MonitoringHotDetailsFilesEffects {
  public constructor(
    private readonly actions$: Actions,
    private readonly monitoringHotDetailsService: MonitoringHotDetailsFilesService,
    private readonly store: Store<unknown>,
    private readonly dialogService: DialogService
  ) {}

  public searchMonitoringHotDetailsFilesEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchMonitoringHotDetailsFilesRequest>>(
        fromActions.searchMonitoringHotDetailsFilesRequest
      ),
      switchMap(({ payload: { query } }) =>
        this.monitoringHotDetailsService.find(query).pipe(
          map(data =>
            fromActions.searchMonitoringHotDetailsFilesSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchMonitoringHotDetailsFilesFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public downloadMonitoringHotDetailsFilesEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.downloadMonitoringHotDetailsFilesRequest>>(
          fromActions.downloadMonitoringHotDetailsFilesRequest
        ),
        withLatestFrom(this.store.pipe(select(fromSelectors.selectMonitoringHotDetailsFilesQuery))),
        tap(([_, query]) => {
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.monitoringHotDetailsService
          });
        })
      ),
    { dispatch: false }
  );
}
