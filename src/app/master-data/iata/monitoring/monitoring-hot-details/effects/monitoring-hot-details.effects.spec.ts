import { HttpErrorResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/monitoring-hot-details.actions';
import * as fromSelectors from '../selectors/monitoring-hot-details.selectors';
import { MonitoringHotDetailsFilesService } from '../services/monitoring-hot-details.service';
import * as fromState from '../store/monitoring-hot-details.state';
import * as fromStub from '../stubs/monitoring-hot-details.stubs';

import { MonitoringHotDetailsFilesEffects } from './monitoring-hot-details.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

describe('MonitoringHotDetailsFilesEffects', () => {
  let actions$: Observable<any>;
  let effects: MonitoringHotDetailsFilesEffects;
  let service: MonitoringHotDetailsFilesService;
  let dialogService: DialogService;

  const initialState = {
    [fromState.monitoringHotDetailsStateFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringHotDetailsFilesEffects,
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectMonitoringHotDetailsFilesQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: MonitoringHotDetailsFilesService,
          useValue: {
            find: () => {},
            download: () => {}
          }
        },
        {
          provide: DialogService,
          useValue: {
            open: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<MonitoringHotDetailsFilesEffects>(MonitoringHotDetailsFilesEffects);
    service = TestBed.inject<MonitoringHotDetailsFilesService>(MonitoringHotDetailsFilesService);
    dialogService = TestBed.inject<DialogService>(DialogService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchMonitoringHotDetailsFilesEffect', () => {
    it('should return searchMonitoringHotDetailsFilesSuccess with paged data', () => {
      const action = fromActions.searchMonitoringHotDetailsFilesRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchMonitoringHotDetailsFilesSuccess({
        payload: {
          pagedData: fromStub.monitoringHotDetailsPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.monitoringHotDetailsPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchMonitoringHotDetailsFilesEffect$).toBeObservable(expected);
    });

    it('should return searchMonitoringHotDetailsFilesRequest when monitoring global ta fop files service find fails', () => {
      const action = fromActions.searchMonitoringHotDetailsFilesRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchMonitoringHotDetailsFilesFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchMonitoringHotDetailsFilesEffect$).toBeObservable(expected);
    });
  });

  describe('downloadMonitoringHotDetailsFilesRequest', () => {
    it('should call downloadMonitoringHotDetailsFilesRequest when opening download dialog', fakeAsync(() => {
      const action = fromActions.downloadMonitoringHotDetailsFilesRequest();

      spyOn(dialogService, 'open');

      actions$ = new BehaviorSubject<unknown>(action);

      effects.downloadMonitoringHotDetailsFilesEffect$.subscribe();

      tick();

      expect(dialogService.open).toHaveBeenCalledWith(DownloadFileComponent, {
        data: {
          title: 'BUTTON.DEFAULT.DOWNLOAD',
          footerButtonsType: FooterButton.Download,
          downloadQuery: {
            ...defaultQuery,
            filterBy: {
              ...defaultQuery?.filterBy
            }
          }
        },
        apiService: service
      });
    }));
  });
});
