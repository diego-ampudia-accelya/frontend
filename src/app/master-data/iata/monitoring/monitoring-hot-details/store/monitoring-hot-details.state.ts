import { HttpErrorResponse } from '@angular/common/http';

import {
  MonitoringHotDetailsEntryFilters,
  MonitoringHotDetailsFileEntry
} from '../models/monitoring-hot-details.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

export const monitoringHotDetailsStateFeatureKey = 'monitoringHotDetailsStateFeatureKey';

export interface MonitoringHotDetailsFilesState {
  query: DataQuery<MonitoringHotDetailsEntryFilters>;
  data: MonitoringHotDetailsFileEntry[];
  loading: {
    searchMonitoringHotDetailsFiles: boolean;
  };
  error: {
    searchMonitoringHotDetailsFiles: HttpErrorResponse | null;
  };
}

export const initialState: MonitoringHotDetailsFilesState = {
  query: defaultQuery,
  data: [],
  loading: {
    searchMonitoringHotDetailsFiles: false
  },
  error: {
    searchMonitoringHotDetailsFiles: null
  }
};
