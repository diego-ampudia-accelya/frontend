import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/monitoring-hot-details.actions';
import * as fromSelectors from '../selectors/monitoring-hot-details.selectors';

import { MonitoringHotDetailsFilesFacadeService } from './monitoring-hot-details-store-facade.service';
import { initialState, monitoringHotDetailsStateFeatureKey } from './monitoring-hot-details.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('MonitoringHotDetailsFilesFacadeService', () => {
  let service: MonitoringHotDetailsFilesFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MonitoringHotDetailsFilesFacadeService,
        provideMockStore({
          initialState: {
            [monitoringHotDetailsStateFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(MonitoringHotDetailsFilesFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('should dispatch a new searchMonitoringHotDetailsFilesRequest action when searchMonitoringHotDetailsFiles is called', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchMonitoringHotDetailsFiles(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchMonitoringHotDetailsFilesRequest({ payload: { query: defaultQuery } })
      );
    });

    it('should dispatch a new downloadMonitoringHotDetailsFilesRequest action when downloadMonitoringHotDetailsFiles is called', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.downloadMonitoringHotDetailsFiles();

      expect(store.dispatch).toHaveBeenCalledWith(fromActions.downloadMonitoringHotDetailsFilesRequest());
    });
  });

  describe('selectors', () => {
    it('should select query when selecting query$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringHotDetailsFilesQuery);
    });

    it('should select data when selecting data$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectMonitoringHotDetailsFiles);
    });

    it('should select hasData when selecting hasData$', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('should invoke selectSearchMonitoringHotDetailsFilesLoading when calling searchMonitoringHotDetailsFiles$', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchMonitoringHotDetailsFiles; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchMonitoringHotDetailsFilesLoading);
    });
  });
});
