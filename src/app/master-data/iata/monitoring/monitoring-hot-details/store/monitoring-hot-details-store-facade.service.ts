import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/monitoring-hot-details.actions';
import { MonitoringHotDetailsEntryFilters } from '../models/monitoring-hot-details.models';
import * as fromSelectors from '../selectors/monitoring-hot-details.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import * as fromAuth from '~app/auth/selectors/auth.selectors';

@Injectable()
export class MonitoringHotDetailsFilesFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchMonitoringHotDetailsFiles: (query?: DataQuery<MonitoringHotDetailsEntryFilters>) =>
        this.store.dispatch(fromActions.searchMonitoringHotDetailsFilesRequest({ payload: { query } })),
      downloadMonitoringHotDetailsFiles: () =>
        this.store.dispatch(fromActions.downloadMonitoringHotDetailsFilesRequest())
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectMonitoringHotDetailsFilesQuery),
      data$: this.store.select(fromSelectors.selectMonitoringHotDetailsFiles),
      hasData$: this.store.select(fromSelectors.selectHasData),
      userBsps$: this.store.select(fromAuth.getUserBsps),
      user$: this.store.select(fromAuth.getUser)
    };
  }

  public get loading() {
    return {
      searchMonitoringHotDetailsFiles: this.store.select(fromSelectors.selectSearchMonitoringHotDetailsFilesLoading)
    };
  }
}
