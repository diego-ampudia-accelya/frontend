import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';

import {
  EStatus,
  MonitoringHotDetailsEntryFilters,
  MonitoringHotDetailsFileEntry
} from '../../models/monitoring-hot-details.models';
import { MonitoringHotDetailsFilesFilterFormatterService } from '../../services/monitoring-hot-details-filter-formatter.service';
import { MonitoringHotDetailsFilesFacadeService } from '../../store/monitoring-hot-details-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { DocumentType, SortOrder } from '~app/shared/enums';
import { ArrayHelper, toValueLabelObjectDictionary } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-monitoring-hot-details-list',
  templateUrl: './monitoring-hot-details-list.component.html'
})
export class MonitoringHotDetailsListComponent implements OnInit {
  public columns = this.buildColumns();
  public hasAllBspsPermission: boolean;
  public userBspDropdownOptions$: Observable<DropdownOption<BspDto>[]>;
  public userDefaultBsp$: Observable<Bsp>;
  public gdsDropdownOptions$ = this.gdsDictionaryService.getDropdownOptionsForFiltering();

  public statusOptions = [
    {
      value: EStatus.WARNING,
      label: this.translationService.translate('monitoring-hot-details.filters.status.option.warning')
    },
    {
      value: EStatus.ERROR,
      label: this.translationService.translate('monitoring-hot-details.filters.status.option.error')
    }
  ];

  public transactionCodeOptions = ArrayHelper.toDropdownOptions(Object.values(DocumentType));
  public agentCodesList$: Observable<DropdownOption[]> = of([]);
  public airlineCodesList$: Observable<DropdownOption[]> = of([]);

  public data$: Observable<MonitoringHotDetailsFileEntry[]> = this.monitoringHotDetailsFacadeService.selectors.data$;
  public query$: Observable<DataQuery<MonitoringHotDetailsEntryFilters>> =
    this.monitoringHotDetailsFacadeService.selectors.query$;
  public loading$: Observable<boolean> = this.monitoringHotDetailsFacadeService.loading.searchMonitoringHotDetailsFiles;
  public hasData$: Observable<boolean> = this.monitoringHotDetailsFacadeService.selectors.hasData$;

  public filterFormGroup: FormGroup;

  constructor(
    public readonly monitoringHotDetailsFilterFormatterService: MonitoringHotDetailsFilesFilterFormatterService,
    private readonly permissionService: PermissionsService,
    private readonly translationService: L10nTranslationService,
    private readonly monitoringHotDetailsFacadeService: MonitoringHotDetailsFilesFacadeService,
    private readonly gdsDictionaryService: GdsDictionaryService,
    private readonly agentDictionaryService: AgentDictionaryService,
    private readonly airlineDictionaryService: AirlineDictionaryService
  ) {}

  public get title$(): Observable<string> {
    return this.query$.pipe(
      map(query => query?.paginateBy?.totalElements),
      map(length => `${this.translationService.translate('monitoring-hot-details.view.title')} (${length || 0})`)
    );
  }

  public get isBspFilterDisabled$(): Observable<boolean> {
    return this.userBspDropdownOptions$?.pipe(map(userBspDropdownOptions => Boolean(userBspDropdownOptions?.length)));
  }

  public ngOnInit(): void {
    this.initializePermissions();
    this.initializeFormGroup();
    this.initializeBspAndDropdownOptions();
  }

  public onQueryChanged(query: DataQuery<MonitoringHotDetailsEntryFilters>): void {
    this.monitoringHotDetailsFacadeService.actions.searchMonitoringHotDetailsFiles(query);
  }

  public onDownload(): void {
    this.monitoringHotDetailsFacadeService.actions.downloadMonitoringHotDetailsFiles();
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'documentNumber',
        name: 'monitoring-hot-details.columns.documentNumber',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'airlineCode',
        name: 'monitoring-hot-details.columns.airlineCode',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'agentCode',
        name: 'monitoring-hot-details.columns.agentCode',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'transactionCode',
        name: 'monitoring-hot-details.columns.transactionCode',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'status',
        name: 'monitoring-hot-details.columns.status',
        resizeable: true,
        draggable: false,
        pipe: { transform: status => this.getTranslatedStatus(status) }
      },
      {
        prop: 'fieldElement',
        name: 'monitoring-hot-details.columns.fieldElement',
        resizeable: false,
        draggable: false
      },
      {
        prop: 'fieldContent',
        name: 'monitoring-hot-details.columns.fieldContent',
        resizeable: false,
        draggable: false,
        sortable: false
      },
      {
        prop: 'message',
        name: 'monitoring-hot-details.columns.message',
        resizeable: false,
        draggable: false,
        sortable: false
      }
    ];
  }

  private getTranslatedStatus(status: EStatus): string {
    return this.translationService.translate(`monitoring-hot-details.filters.status.option.${status}`);
  }

  private initializePermissions(): void {
    this.hasAllBspsPermission = this.permissionService.hasPermission(Permissions.readAllBsps);
  }

  private initializeFormGroup(): void {
    this.filterFormGroup = new FormGroup({
      agentCode: new FormControl(null, []),
      airlineCode: new FormControl(null, []),
      documentNumber: new FormControl(null, []),
      fieldContent: new FormControl(null, []),
      fieldElement: new FormControl(null, []),
      message: new FormControl(null, []),
      status: new FormControl(null, []),
      transactionCode: new FormControl(null, [])
    });
  }

  private initializeBspAndDropdownOptions(): void {
    this.userDefaultBsp$ = combineLatest([
      this.monitoringHotDetailsFacadeService.selectors.userBsps$,
      this.monitoringHotDetailsFacadeService.selectors.user$
    ]).pipe(map(([bspList, user]) => this.getDefaultBsp(bspList, user)));

    this.userDefaultBsp$
      .pipe(
        first(),
        tap(bsp => {
          this.initializeAgentCodesList(bsp);
          this.initializeAirlineCodesList(bsp);
        })
      )
      .subscribe(bsp =>
        this.monitoringHotDetailsFacadeService.actions.searchMonitoringHotDetailsFiles({
          ...defaultQuery,
          filterBy: {
            ...defaultQuery.filterBy
          },
          sortBy: [{ attribute: 'documentNumber', sortType: SortOrder.Asc }]
        })
      );
  }

  private initializeAgentCodesList(bsp: Bsp): void {
    this.agentCodesList$ = this.agentDictionaryService
      .getDropdownOptions({ bspId: bsp.id })
      .pipe(map(agents => toValueLabelObjectDictionary(agents)));
  }

  private initializeAirlineCodesList(bsp: Bsp): void {
    this.airlineCodesList$ = this.airlineDictionaryService
      .getDropdownOptions(bsp.id)
      .pipe(map(airlines => toValueLabelObjectDictionary(airlines)));
  }

  private getDefaultBsp(bspList: Bsp[], user: User): Bsp {
    return bspList.find(bsp => bsp.isoCountryCode === user.defaultIsoc);
  }
}
