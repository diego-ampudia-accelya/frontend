import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { EStatus, MonitoringHotDetailsEntryFilters } from '../../models/monitoring-hot-details.models';
import { MonitoringHotDetailsFilesFilterFormatterService } from '../../services/monitoring-hot-details-filter-formatter.service';
import { MonitoringHotDetailsFilesFacadeService } from '../../store/monitoring-hot-details-store-facade.service';
import * as fromStub from '../../stubs/monitoring-hot-details.stubs';
import { MonitoringHotDetailsListComponent } from './monitoring-hot-details-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { DataQuery, ListViewComponent } from '~app/shared/components/list-view';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';

describe('MonitoringHotDetailsFilesListComponent', () => {
  let component: MonitoringHotDetailsListComponent;
  let fixture: ComponentFixture<MonitoringHotDetailsListComponent>;
  const translationSpy = createSpyObject(L10nTranslationService);

  const monitoringHotDetailsFilesFacadeServiceSpy = {
    actions: {
      searchMonitoringHotDetailsFiles: () => {},
      downloadMonitoringHotDetailsFiles: () => {}
    },
    selectors: {
      query$: of(null),
      data$: of(null),
      hasData$: of(null),
      userBsps$: of(fromStub.userBsps),
      user$: of(fromStub.user)
    },
    loading: {
      searchMonitoringHotDetailsFiles$: of(false)
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule)],
      declarations: [
        MonitoringHotDetailsListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: translationSpy
        },
        {
          provide: MonitoringHotDetailsFilesFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: MonitoringHotDetailsFilesFacadeService,
          useValue: monitoringHotDetailsFilesFacadeServiceSpy
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        },
        {
          provide: GdsDictionaryService,
          useValue: {
            getDropdownOptionsForFiltering: () => of([])
          }
        },
        {
          provide: AgentDictionaryService,
          useValue: {
            getDropdownOptions: () => of([])
          }
        },
        {
          provide: AirlineDictionaryService,
          useValue: {
            getDropdownOptions: () => of([])
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringHotDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call searchMonitoringMasterDataRet method when onQueryChanged is called', () => {
    const searchMonitoringMasterDataRetSpy = spyOn(
      monitoringHotDetailsFilesFacadeServiceSpy.actions,
      'searchMonitoringHotDetailsFiles'
    );
    const query: DataQuery<MonitoringHotDetailsEntryFilters> = {
      paginateBy: { page: 0, size: 20, totalElements: 33256 },
      sortBy: null,
      filterBy: {}
    };

    component.onQueryChanged(query);

    expect(searchMonitoringMasterDataRetSpy).toHaveBeenCalled();
  });

  it('should return the title string when it is subscribed to title$', fakeAsync(() => {
    let titleResult: string;
    component['query$'] = of({
      paginateBy: { page: 0, size: 20, totalElements: 33256 },
      sortBy: null,
      filterBy: {}
    });

    component['title$'].subscribe(title => (titleResult = title));
    tick();

    expect(titleResult).toBeDefined();
  }));

  it('should return true when there are user Bsp Dropdown Options', fakeAsync(() => {
    let result: boolean;
    component.userBspDropdownOptions$ = of([
      {
        label: 'ES',
        value: {
          id: 1,
          isoCountryCode: 'ES',
          name: 'Spain'
        }
      }
    ]);
    component['isBspFilterDisabled$'].subscribe(isBspFilterDisabled => (result = isBspFilterDisabled));
    tick();

    expect(result).toBe(true);
  }));

  it('should return false when there are not user Bsp Dropdown Options', fakeAsync(() => {
    let result: boolean;
    component.userBspDropdownOptions$ = of(null);
    component['isBspFilterDisabled$'].subscribe(isBspFilterDisabled => (result = isBspFilterDisabled));
    tick();

    expect(result).toBe(false);
  }));

  it('should return false when there are not user Bsp Dropdown Options', fakeAsync(() => {
    component['userBspDropdownOptions$'] = of(null);
    let result: boolean;

    component['isBspFilterDisabled$'].subscribe(isBspFilterDisabled => (result = isBspFilterDisabled));
    tick();

    expect(result).toBe(false);
  }));

  it('should call downloadMonitoringHotDetailsFiles action when onDownload is called', () => {
    const downloadMonitoringHotDetailsFilesSpy = spyOn(
      monitoringHotDetailsFilesFacadeServiceSpy.actions,
      'downloadMonitoringHotDetailsFiles'
    );
    component.onDownload();

    expect(downloadMonitoringHotDetailsFilesSpy).toHaveBeenCalled();
  });

  it('should call translate method from L10nTranslationService and return status translated when getTranslatedStatus method is called', () => {
    component['getTranslatedStatus'](EStatus.ERROR);

    expect(translationSpy.translate).toHaveBeenCalled();
  });

  it('should get translated status when the value is passed to transform pipe for status column', () => {
    const testValue = EStatus.ERROR;
    spyOn<any>(component, 'getTranslatedStatus').and.returnValue(EStatus.ERROR);
    const columns = component['buildColumns']();

    const statusColumn = columns.find(column => column.prop === 'status');

    component['buildColumns']();

    expect(statusColumn.pipe.transform(testValue)).toBe(EStatus.ERROR);
  });
});
