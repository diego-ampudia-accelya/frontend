import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import {
  MonitoringHotDetailsEntryFilters,
  MonitoringHotDetailsFileEntry
} from '../models/monitoring-hot-details.models';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchMonitoringHotDetailsFilesRequest = createAction(
  '[Monitoring Global Ta Fop Files]Search Monitoring Global Ta Fop Files Request',
  props<{ payload: { query?: DataQuery<MonitoringHotDetailsEntryFilters> } }>()
);

export const searchMonitoringHotDetailsFilesSuccess = createAction(
  '[Monitoring Global Ta Fop Files]Search Monitoring Global Ta Fop Files Success',
  props<{ payload: { pagedData: PagedData<MonitoringHotDetailsFileEntry> } }>()
);

export const searchMonitoringHotDetailsFilesFailure = createAction(
  '[Monitoring Global Ta Fop Files]Search Monitoring Global Ta Fop Files Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const downloadMonitoringHotDetailsFilesRequest = createAction(
  '[Monitoring Global Ta Fop Files]Download Monitoring Global Ta Fop Files Request'
);
