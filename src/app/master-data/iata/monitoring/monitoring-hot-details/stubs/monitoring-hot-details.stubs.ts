import { EStatus, MonitoringHotDetailsFileEntry } from '../models/monitoring-hot-details.models';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { User, UserType } from '~app/shared/models/user.model';

export const monitoringHotDetails: MonitoringHotDetailsFileEntry[] = [
  {
    agentCode: '7824824',
    airlineCode: '001',
    documentNumber: '0011020756685',
    fieldContent: '7824824',
    fieldElement: 'Agent Code',
    message: 'Unknown Agent Code',
    transactionCode: 'RFND',
    status: EStatus.WARNING
  }
];

export const monitoringHotDetailsPagedData: PagedData<MonitoringHotDetailsFileEntry> = {
  pageNumber: 0,
  pageSize: 20,
  total: 1,
  totalPages: 1,
  records: monitoringHotDetails
};

export const userBsps: Bsp[] = [
  {
    id: 6983,
    isoCountryCode: 'ES',
    name: 'SPAIN',
    effectiveFrom: '2000-01-01',
    effectiveTo: null,
    version: 3232,
    defaultCurrencyCode: null,
    active: true
  }
];

export const user: User = {
  email: 'martin.sautter@iata.org',
  firstName: null,
  lastName: 'User0BSPES',
  telephone: '021224836126969',
  organization: "organisation ' comilla",
  address: '19 Mayis Cad. 4',
  locality: "icity ' comillas",
  city: null,
  zip: '123',
  country: 'Turquia',
  registerDate: '2000-09-06',
  expiryDate: null,
  active: true,
  id: 69830,
  userType: UserType.IATA,
  iataCode: null,
  language: 'en',
  bspPermissions: [
    {
      bspId: 6983,
      permissions: ['rMonHot']
    }
  ],
  permissions: ['rMonHot'],
  bsps: [
    {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      version: 3232,
      defaultCurrencyCode: null,
      active: true
    }
  ],
  isImpersonating: false,
  defaultIsoc: 'ES',
  template: 'STREAMLINED',
  isSuperUser: false
};
