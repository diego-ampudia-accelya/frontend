import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import isEqual from 'lodash/isEqual';
import { Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, filter, first, switchMap, takeUntil } from 'rxjs/operators';

import { MainUserDetails } from '../../models/main-user-details.model';
import { MainUserType } from '../../models/main-user-type.model';
import { MainUserBE } from '../../models/main-users.model';
import { MainUserPropertiesChangesService } from '../../services/main-user-properties-changes.service';
import { MainUserActivationDialogActions, MainUserPropertiesActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import { ActiveInactiveInUpperCase, ResponseErrorBE } from '~app/shared/models';
import { FormUtil } from '~app/shared/helpers';
import { Permissions } from '~app/shared/constants';
import { AppState } from '~app/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';

@Component({
  selector: 'bspl-main-user-properties',
  templateUrl: './main-user-properties.component.html',
  styleUrls: ['./main-user-properties.component.scss']
})
export class MainUserPropertiesComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromMainUsers.getUserPropertiesLoading));
  public form: FormGroup;
  public user: MainUserBE;
  public isUserActive = false;
  public isAgentUser = false;
  public inactiveOrHasNoPermission = true;
  public hasUpdatePermission = this.permissionsService.hasPermission(Permissions.updateMainUsers);

  public get inactiveOrNoPermissionOrAgentGroup(): boolean {
    return this.inactiveOrHasNoPermission || this.isAgentUser;
  }

  private formUtil: FormUtil;
  private user$ = this.store.pipe(select(fromMainUsers.getMainUser));
  private mainUserRequestError$ = this.store.pipe(
    select(fromMainUsers.getUserUpdatePropertiesErrors),
    filter(error => !!error)
  );
  private hasChanges$ = this.store.pipe(select(fromMainUsers.hasMainUserPropertiesChanges));

  private reactivateOrDeactivateSubmitEmitter = new Subject();
  private destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private mainUserPropertiesChangesService: MainUserPropertiesChangesService
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeForm();
    this.initializeUser();
    this.listenToReactivateOrDeactivateSubmit();
    this.listenToFormChanges();
    this.listenToErrors();
  }

  public deactivateUser(): void {
    this.store.dispatch(
      MainUserActivationDialogActions.openDeactivationDialog({
        user: this.user,
        actionEmitter: () => this.reactivateOrDeactivateSubmitEmitter
      })
    );
  }

  public reactivateUser(): void {
    this.store.dispatch(
      MainUserActivationDialogActions.openReactivationDialog({
        user: this.user,
        actionEmitter: () => this.reactivateOrDeactivateSubmitEmitter
      })
    );
  }

  public canDeactivate(): Observable<boolean> {
    this.store.dispatch(MainUserPropertiesActions.propertiesDeactivationRequest());

    return this.hasChanges$.pipe(
      switchMap(hasChanges => (hasChanges ? this.mainUserPropertiesChangesService.getDeactivateAction() : of(true))),
      first()
    );
  }

  private initializeForm(): void {
    this.form = this.formUtil.createGroup<MainUserDetails>({
      portalEmail: ['', [Validators.required, Validators.maxLength(200)]],
      name: ['', [Validators.required, Validators.maxLength(49)]],
      organization: ['', [Validators.required, Validators.maxLength(30)]],
      telephone: ['', [Validators.required, Validators.maxLength(15)]],
      country: ['', [Validators.required, Validators.maxLength(26)]],
      city: ['', [Validators.required, Validators.maxLength(30)]],
      address: ['', [Validators.required, Validators.maxLength(30)]],
      postCode: ['', [Validators.required, Validators.maxLength(10)]]
    });
  }

  private initializeUser(): void {
    this.user$
      .pipe(
        distinctUntilChanged((prev, current) => isEqual(prev, current)),
        takeUntil(this.destroy$)
      )
      .subscribe((user: MainUserBE) => {
        this.user = user;
        this.form.reset();

        if (user) {
          this.isUserActive = user.status === ActiveInactiveInUpperCase.Active;
          this.isAgentUser = user.userType === MainUserType.AgentGroup;
          this.inactiveOrHasNoPermission = !this.hasUpdatePermission || !this.isUserActive;

          this.form.patchValue(this.transformToFormValue(user));
        }
      });
  }

  private transformToFormValue(user: MainUserBE): MainUserDetails {
    return {
      portalEmail: user.portalEmail,
      name: user.name,
      organization: user.contactInfo?.organization,
      telephone: user.contactInfo?.telephone,
      country: user.contactInfo?.country,
      city: user.contactInfo?.city,
      address: user.contactInfo?.address,
      postCode: user.contactInfo?.postCode
    };
  }

  private listenToReactivateOrDeactivateSubmit(): void {
    this.reactivateOrDeactivateSubmitEmitter
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.store.dispatch(MainUserPropertiesActions.requestUser({ id: this.user.id })));
  }

  private listenToFormChanges(): void {
    if (this.isUserActive) {
      this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => this.dispatchValueChange());
    }
  }

  private dispatchValueChange(): void {
    this.store.dispatch(
      MainUserPropertiesActions.propertiesFormValueChange({ formValue: this.form.value, isFormValid: this.form.valid })
    );
  }

  private listenToErrors(): void {
    this.mainUserRequestError$.pipe(takeUntil(this.destroy$)).subscribe(({ error }) => this.setFieldErrors(error));
  }

  private setFieldErrors(error: ResponseErrorBE): void {
    if (error.errorCode !== 400 || !error.messages?.length) {
      return;
    }

    // Validation error
    error.messages.forEach(({ message, messageParams }) => {
      const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
      const invalidControl = this.form.get(controlName);

      if (invalidControl) {
        invalidControl.setErrors({ requestError: { message } });
        FormUtil.showControlState(invalidControl);
      }
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
