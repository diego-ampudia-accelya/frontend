import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockComponents, MockPipe } from 'ng-mocks';
import { of } from 'rxjs';

import { MainUserType } from '../../models/main-user-type.model';
import { MainUserPropertiesChangesService } from '../../services/main-user-properties-changes.service';
import { MainUserActivationDialogActions, MainUserPropertiesActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import { MainUserPropertiesComponent } from './main-user-properties.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { InputComponent, PropertyComponent, PropertyListComponent, SpinnerComponent } from '~app/shared/components';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { TranslatePipeMock } from '~app/test';

describe('MainUserPropertiesComponent', () => {
  let component: MainUserPropertiesComponent;
  let fixture: ComponentFixture<MainUserPropertiesComponent>;
  let mockStore: MockStore<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        MainUserPropertiesComponent,
        MockComponents(
          SpinnerComponent,
          AccordionComponent,
          AccordionItemComponent,
          PropertyListComponent,
          PropertyComponent,
          InputComponent
        ),
        MockPipe(DateTimeFormatPipe),
        TranslatePipeMock
      ],
      providers: [
        FormBuilder,
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {}
            }
          },
          selectors: [
            { selector: fromMainUsers.getUserPropertiesLoading, value: false },
            { selector: fromMainUsers.getMainUser, value: {} },
            {
              selector: fromMainUsers.getUserUpdatePropertiesErrors,
              value: {
                error: {
                  errorCode: 500
                }
              }
            },
            { selector: fromMainUsers.hasMainUserPropertiesChanges, value: false }
          ]
        }),
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        },
        {
          provide: MainUserPropertiesChangesService,
          useValue: {
            getDeactivateAction: () => of(true)
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUserPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('deactivateUser', () => {
    it('should dispatch MainUserActivationDialogActions.openDeactivationDialog', () => {
      component.user = { id: 10 } as any;

      component.deactivateUser();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserActivationDialogActions.openDeactivationDialog({
          user: { id: 10 } as any,
          actionEmitter: jasmine.anything()
        })
      );
    });
  });

  describe('reactivateUser', () => {
    it('should dispatch MainUserActivationDialogActions.openReactivationDialog', () => {
      component.user = { id: 15 } as any;

      component.reactivateUser();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserActivationDialogActions.openReactivationDialog({
          user: { id: 15 } as any,
          actionEmitter: jasmine.anything()
        })
      );
    });
  });

  describe('canDeactivate', () => {
    it('should dispatch MainUserPropertiesActions.propertiesDeactivationRequest', fakeAsync(() => {
      component.canDeactivate().subscribe();
      tick();

      expect(mockStore.dispatch).toHaveBeenCalledWith(MainUserPropertiesActions.propertiesDeactivationRequest());
    }));

    it('should return true', fakeAsync(() => {
      let res;

      component.canDeactivate().subscribe(r => (res = r));
      tick();

      expect(res).toBe(true);
    }));

    it('should return handlePermissionsChangeAndDeactivate$ as false', fakeAsync(() => {
      let res;
      (component as any).mainUserPropertiesChangesService.getDeactivateAction = jasmine
        .createSpy()
        .and.returnValue(of(false));
      mockStore.overrideSelector(fromMainUsers.hasMainUserPropertiesChanges, true);

      component.canDeactivate().subscribe(r => (res = r));
      tick();

      expect(res).toBe(false);
    }));
  });

  describe('initializeForm', () => {
    it('should set form with corresponding value', () => {
      component.form = null;
      const expectedRes = {
        portalEmail: '',
        name: '',
        organization: '',
        country: '',
        city: '',
        address: '',
        telephone: '',
        postCode: ''
      };

      (component as any).initializeForm();

      expect(component.form.value).toEqual(expectedRes);
    });
  });

  describe('initializeUser', () => {
    beforeEach(() => {
      (component as any).transformToFormValue = jasmine.createSpy().and.returnValue({});
    });

    it('should update user', fakeAsync(() => {
      component.user = { id: 10 } as any;
      mockStore.overrideSelector(fromMainUsers.getMainUser, null);

      (component as any).initializeUser();
      tick();

      expect(component.user).toBe(null);
    }));

    it('should reset form', fakeAsync(() => {
      component.form.patchValue({ portalEmail: 'a@b.com', name: 'test' });
      mockStore.overrideSelector(fromMainUsers.getMainUser, null);

      (component as any).initializeUser();
      tick();

      expect(component.form.value).toEqual({
        portalEmail: null,
        name: null,
        organization: null,
        country: null,
        city: null,
        address: null,
        telephone: null,
        postCode: null
      });
    }));

    it('should set isUserActive and isAgentUser flags', fakeAsync(() => {
      component.isUserActive = false;
      component.isAgentUser = false;
      mockStore.overrideSelector(fromMainUsers.getMainUser, {
        id: 15,
        status: ActiveInactiveInUpperCase.Active,
        userType: MainUserType.AgentGroup
      } as any);

      (component as any).initializeUser();
      tick();

      expect(component.isUserActive).toBe(true);
      expect(component.isAgentUser).toBe(true);
    }));

    it('should set true to inactiveOrHasNoPermission flag when hasUpdatePermission is false', fakeAsync(() => {
      component.inactiveOrHasNoPermission = false;
      component.hasUpdatePermission = false;
      mockStore.overrideSelector(fromMainUsers.getMainUser, {
        id: 15,
        status: ActiveInactiveInUpperCase.Active,
        userType: MainUserType.AgentGroup
      } as any);

      (component as any).initializeUser();
      tick();

      expect(component.inactiveOrHasNoPermission).toBe(true);
    }));

    it('should set false to inactiveOrHasNoPermission flag when hasUpdatePermission is true and user is active', fakeAsync(() => {
      component.inactiveOrHasNoPermission = true;
      component.hasUpdatePermission = true;
      mockStore.overrideSelector(fromMainUsers.getMainUser, {
        id: 15,
        status: ActiveInactiveInUpperCase.Active,
        userType: MainUserType.AgentGroup
      } as any);

      (component as any).initializeUser();
      tick();

      expect(component.inactiveOrHasNoPermission).toBe(false);
    }));

    it('should call transformToFormValue with user', fakeAsync(() => {
      component.inactiveOrHasNoPermission = true;
      component.hasUpdatePermission = true;
      mockStore.overrideSelector(fromMainUsers.getMainUser, {
        id: 15,
        status: ActiveInactiveInUpperCase.Active,
        userType: MainUserType.AgentGroup
      } as any);

      (component as any).initializeUser();
      tick();

      expect((component as any).transformToFormValue).toHaveBeenCalledWith({
        id: 15,
        status: ActiveInactiveInUpperCase.Active,
        userType: MainUserType.AgentGroup
      });
    }));
  });

  describe('transformToFormValue', () => {
    it('should return mapped ', () => {
      const res = (component as any).transformToFormValue({
        portalEmail: 'aa@b.com',
        name: 'test-name',
        contactInfo: {
          organization: 'test-org',
          telephone: '1111111',
          country: 'test-country',
          city: 'test-city',
          address: 'test-address',
          postCode: '123456'
        }
      });

      expect(res).toEqual({
        portalEmail: 'aa@b.com',
        name: 'test-name',
        organization: 'test-org',
        country: 'test-country',
        city: 'test-city',
        address: 'test-address',
        telephone: '1111111',
        postCode: '123456'
      });
    });
  });

  describe('listenToReactivateOrDeactivateSubmit', () => {
    it('should dispatch MainUserPropertiesActions.requestUser', fakeAsync(() => {
      component.user = { id: 40 } as any;

      (component as any).listenToReactivateOrDeactivateSubmit();
      (component as any).reactivateOrDeactivateSubmitEmitter.next();
      tick();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserPropertiesActions.requestUser({
          id: 40
        })
      );
    }));
  });

  describe('listenToFormChanges', () => {
    beforeEach(() => {
      (component as any).dispatchValueChange = jasmine.createSpy();
    });

    it('should call dispatchValueChange() when isUserActive is true', fakeAsync(() => {
      component.isUserActive = true;

      (component as any).listenToFormChanges();
      component.form.patchValue({ name: 'test' });
      tick();

      expect((component as any).dispatchValueChange).toHaveBeenCalled();
    }));

    it('should NOT call dispatchValueChange() when isUserActive is false', fakeAsync(() => {
      component.isUserActive = false;

      (component as any).listenToFormChanges();
      component.form.patchValue({ name: 'test' });
      tick();

      expect((component as any).dispatchValueChange).not.toHaveBeenCalled();
    }));
  });

  describe('dispatchValueChange', () => {
    it('should dispatch MainUserPropertiesActions.propertiesFormValueChange', () => {
      component.form.setValue({
        portalEmail: 'aa@b.com',
        name: 'test-name',
        organization: 'test-org',
        country: 'test-country',
        city: 'test-city',
        address: 'test-address',
        telephone: '1111111',
        postCode: '123456'
      });

      (component as any).dispatchValueChange();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserPropertiesActions.propertiesFormValueChange({
          formValue: {
            portalEmail: 'aa@b.com',
            name: 'test-name',
            organization: 'test-org',
            country: 'test-country',
            city: 'test-city',
            address: 'test-address',
            telephone: '1111111',
            postCode: '123456'
          },
          isFormValid: true
        })
      );
    });
  });

  describe('listenToErrors', () => {
    it('should call setFieldErrors with error', fakeAsync(() => {
      (component as any).setFieldErrors = jasmine.createSpy();

      (component as any).listenToErrors();
      tick();

      expect((component as any).setFieldErrors).toHaveBeenCalledWith({ errorCode: 500 });
    }));
  });

  describe('setFieldErrors', () => {
    it('should set errors to portalEmail', () => {
      const error = {
        errorCode: 400,
        messages: [{ message: 'Already exist', messageParams: [{ name: 'fieldName', value: 'portalEmail' }] }]
      };
      (component as any).initializeForm();
      expect(component.form.get('portalEmail').getError('requestError')).not.toBeDefined();

      (component as any).setFieldErrors(error);

      expect(component.form.get('portalEmail').getError('requestError')).toEqual({ message: 'Already exist' });
    });
  });
});
