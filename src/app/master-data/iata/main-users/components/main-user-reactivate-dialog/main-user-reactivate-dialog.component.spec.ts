import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { of, Subject } from 'rxjs';

import { MainUserActivationDialogActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import * as fromMainListOfUsers from '../../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../../store/reducers/main-users.reducer';

import { MainUserReactivateDialogComponent } from './main-user-reactivate-dialog.component';
import { TranslatePipeMock } from '~app/test';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import {
  DialogConfig,
  FooterButton,
  InputComponent,
  PropertyComponent,
  PropertyListComponent
} from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('MainUserReactivateDialogComponent', () => {
  let component: MainUserReactivateDialogComponent;
  let fixture: ComponentFixture<MainUserReactivateDialogComponent>;
  let mockStore: MockStore<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [
        MainUserReactivateDialogComponent,
        TranslatePipeMock,
        MockComponents(PropertyListComponent, PropertyComponent, InputComponent)
      ],
      providers: [
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [{ selector: fromMainUsers.getMainUserActivationDialogLoading, value: false }]
        }),
        { provide: ReactiveSubject, useValue: { asObservable: of(false) } },
        { provide: DialogConfig, useValue: { data: { buttons: [], user: { id: 111 } } } }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUserReactivateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('initializeSaveButton', () => {
    beforeEach(() => {
      (component as any).setIsDisabledForSaveButton = jasmine.createSpy();
    });

    it('should set reactivateButton', () => {
      (component as any).reactivateButton = null;
      (component as any).config = { data: { buttons: [{ type: FooterButton.Reactivate, isDisabled: false }] } };

      (component as any).initializeSaveButton();

      expect((component as any).reactivateButton).toEqual({ type: FooterButton.Reactivate, isDisabled: false });
    });

    it('should call setIsDisabledForSaveButton() when emailContrl is invalid', () => {
      component.emailControl.setValue('');

      (component as any).initializeSaveButton();

      expect((component as any).setIsDisabledForSaveButton).toHaveBeenCalledWith(true);
    });

    it('should NOT call setIsDisabledForSaveButton() when emailContrl is valid', () => {
      component.emailControl.setValue('a@b.com');

      (component as any).initializeSaveButton();

      expect((component as any).setIsDisabledForSaveButton).not.toHaveBeenCalledWith(true);
    });
  });

  describe('subscribeToLoading', () => {
    it('should call setLoading with loading', fakeAsync(() => {
      (component as any).setLoading = jasmine.createSpy();

      (component as any).subscribeToLoading();

      expect((component as any).setLoading).toHaveBeenCalledWith(false);
    }));
  });

  describe('subscribeToButtonClick', () => {
    it('should dispatch MainUserActivationDialogActions.reactivate', () => {
      (component as any).reactivateButtonClick$ = new Subject();
      component.user = { id: 10 } as any;
      component.emailControl.setValue('a@b.com');
      (component as any).user = { id: 10 };
      (component as any).config.data = { actionEmitter: { emit: {} } };

      (component as any).subscribeToButtonClick();
      (component as any).reactivateButtonClick$.next();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserActivationDialogActions.reactivate({
          userId: 10,
          portalEmail: 'a@b.com',
          actionEmitter: { emit: {} } as any
        })
      );
    });
  });

  describe('subscribeToControlStatus', () => {
    beforeEach(() => {
      (component as any).setIsDisabledForSaveButton = jasmine.createSpy();
    });

    it('should call setIsDisabledForSaveButton with false', () => {
      component.emailControl.setValue('');

      (component as any).subscribeToControlStatus();
      component.emailControl.setValue('a@b.com');

      expect((component as any).setIsDisabledForSaveButton).toHaveBeenCalledWith(false);
    });
  });

  describe('setLoading', () => {
    it('should set isLoading for reactivateButton as true', () => {
      (component as any).reactivateButton = { isLoading: false };

      (component as any).setLoading(true);

      expect((component as any).reactivateButton.isLoading).toBe(true);
    });
  });
});
