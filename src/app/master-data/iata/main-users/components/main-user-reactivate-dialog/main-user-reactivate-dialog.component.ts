import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { MainUserBE } from '../../models/main-users.model';
import { MainUserActivationDialogActions } from '../../store/actions';
import * as fromMainUserActivationDialog from '../../store/reducers';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-main-user-reactivate-dialog',
  templateUrl: './main-user-reactivate-dialog.component.html',
  styleUrls: ['./main-user-reactivate-dialog.component.scss']
})
export class MainUserReactivateDialogComponent implements OnInit, OnDestroy {
  public user: MainUserBE = this.config.data.user;
  public emailControl: FormControl = new FormControl(this.user.portalEmail, [Validators.required]);

  private reactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter((res: any) => res?.clickedBtn === FooterButton.Reactivate)
  );
  private reactivateButton: ModalAction;
  private destroy$ = new Subject();

  constructor(private config: DialogConfig, private reactiveSubject: ReactiveSubject, private store: Store<AppState>) {}

  public ngOnInit(): void {
    this.initializeSaveButton();
    this.subscribeToLoading();
    this.subscribeToButtonClick();
    this.subscribeToControlStatus();
  }

  private initializeSaveButton(): void {
    this.reactivateButton = this.config.data.buttons.find(
      (button: ModalAction) => button.type === FooterButton.Reactivate
    );

    if (this.emailControl.invalid) {
      this.setIsDisabledForSaveButton(true);
    }
  }

  private subscribeToLoading(): void {
    this.store
      .pipe(
        select(fromMainUserActivationDialog.getMainUserActivationDialogLoading),
        tap(loading => this.setLoading(loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private subscribeToButtonClick(): void {
    this.reactivateButtonClick$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(
        MainUserActivationDialogActions.reactivate({
          userId: this.user.id,
          portalEmail: this.emailControl.value,
          actionEmitter: this.config.data?.actionEmitter
        })
      );
    });
  }

  private subscribeToControlStatus(): void {
    this.emailControl.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      this.setIsDisabledForSaveButton(status !== 'VALID');
    });
  }

  private setLoading(loading: boolean): void {
    if (this.reactivateButton) {
      this.reactivateButton.isLoading = loading;
    }
  }

  private setIsDisabledForSaveButton(isDisabled: boolean): void {
    if (this.reactivateButton) {
      this.reactivateButton.isDisabled = isDisabled;
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
