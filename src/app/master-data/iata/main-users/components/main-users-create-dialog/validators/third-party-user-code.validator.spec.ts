import { FormControl } from '@angular/forms';

import { thirdPartyUserCodeValidator } from './third-party-user-code.validator';

describe('thirdPartyUserCodeValidator', () => {
  it('should return null if user is NOT third party', () => {
    const control = new FormControl(
      '10',
      thirdPartyUserCodeValidator(() => false)
    );

    expect(control.errors).toBeNull();
  });

  it('should return null if user is third party and control value is empty', () => {
    const control = new FormControl(
      null,
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toBeNull();
  });

  it('should return whitespace error when control value value has whitespaces', () => {
    const control = new FormControl(
      '1 1',
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toEqual({ whitespace: { value: '1 1' } });
  });

  it('should return minLetters error when control value has no letters', () => {
    const control = new FormControl(
      '11',
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toEqual({ minLetters: { value: '11', minLetters: 1 } });
  });

  it('should return minDigits error when control value has no digits', () => {
    const control = new FormControl(
      'aa',
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toEqual({ minDigits: { value: 'aa', minDigits: 1 } });
  });

  it('should return specialCharacters error when control value has special characters', () => {
    const control = new FormControl(
      'a1<',
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toEqual({
      specialCharacters: {
        key: 'MENU.USERS_MAINTENANCE.MAIN_USERS.createDialog.specialCharactersError'
      }
    });
  });

  it('should return minlength error when control value has less characters than min', () => {
    const control = new FormControl(
      'a1',
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toEqual({
      minlength: {
        requiredLength: 6
      }
    });
  });

  it('should return maxlength error when control value has more characters than max', () => {
    const control = new FormControl(
      'a1234567890',
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toEqual({
      maxlength: {
        requiredLength: 10
      }
    });
  });

  it('should return null when control value is correct', () => {
    const control = new FormControl(
      'a12345678',
      thirdPartyUserCodeValidator(() => true)
    );

    expect(control.errors).toBeNull();
  });
});
