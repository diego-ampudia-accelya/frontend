import { FormControl } from '@angular/forms';

import { templateRequiredValidator } from './template-required.validator';

describe('templateRequiredValidator', () => {
  it('should return null if user is NOT agent', () => {
    const control = new FormControl(
      10,
      templateRequiredValidator(() => false)
    );

    expect(control.errors).toBeNull();
  });

  it('should return null if user is agent and control value is NOT empty', () => {
    const control = new FormControl(
      10,
      templateRequiredValidator(() => true)
    );

    expect(control.errors).toBeNull();
  });

  it('should return required error if user is agent and control value is empty', () => {
    const control = new FormControl(
      null,
      templateRequiredValidator(() => true)
    );

    expect(control.errors).toEqual({ required: true });
  });
});
