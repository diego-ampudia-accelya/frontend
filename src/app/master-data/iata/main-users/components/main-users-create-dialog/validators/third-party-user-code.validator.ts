import { AbstractControl, ValidatorFn } from '@angular/forms';

function abstractControlChecker(control: AbstractControl): { [key: string]: any } | null {
  const value: string = control.value;

  const space = ' ';
  const tab = ' ';
  const hasWhitespace = value.includes(space) || value.includes(tab);
  if (hasWhitespace) {
    return { whitespace: { value } };
  }

  const actualLetters = (value.match(/[a-zA-Z]/g) || []).length;
  if (actualLetters < 1) {
    return { minLetters: { value, minLetters: 1 } };
  }

  const actualDigits = (value.match(/[0-9]/g) || []).length;
  if (actualDigits < 1) {
    return { minDigits: { value, minDigits: 1 } };
  }

  // eslint-disable-next-line no-useless-escape
  if (!/^[^.\/:*?\\"<>|_']*$/.test(value)) {
    return {
      specialCharacters: {
        key: 'MENU.USERS_MAINTENANCE.MAIN_USERS.createDialog.specialCharactersError'
      }
    };
  }

  if (value.length < 6) {
    return {
      minlength: {
        requiredLength: 6
      }
    };
  }

  if (value.length > 10) {
    return {
      maxlength: {
        requiredLength: 10
      }
    };
  }

  return null;
}

export function thirdPartyUserCodeValidator(isThirdPartyUserFn: () => boolean): ValidatorFn {
  return function (control: AbstractControl): { [key: string]: any } | null {
    const isThirdPartyUser = isThirdPartyUserFn();

    if (isThirdPartyUser && control.value) {
      return abstractControlChecker(control);
    }

    return null;
  };
}
