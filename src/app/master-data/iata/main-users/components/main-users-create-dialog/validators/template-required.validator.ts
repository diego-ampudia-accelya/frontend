import { AbstractControl, ValidatorFn } from '@angular/forms';

export function templateRequiredValidator(isAgentUserFn: () => boolean): ValidatorFn {
  return function (control: AbstractControl): { [key: string]: boolean } | null {
    const isAgentUser = isAgentUserFn();

    if (!isAgentUser || !!control?.value) {
      return null;
    }

    return { required: true };
  };
}
