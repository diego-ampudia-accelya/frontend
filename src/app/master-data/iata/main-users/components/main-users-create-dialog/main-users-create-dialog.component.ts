import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import isEqual from 'lodash/isEqual';
import { BehaviorSubject, combineLatest, Observable, of, Subject, throwError } from 'rxjs';
import { catchError, distinctUntilChanged, filter, finalize, first, takeUntil } from 'rxjs/operators';

import { MainUserCreateModelOmitted } from '../../models/create-main-user.model';
import { MainUserTypeForCreate } from '../../models/main-user-type.model';
import { MainUsersService } from '../../services/main-users.service';
import { MainUserCreateDialogActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import { templateRequiredValidator } from './validators/template-required.validator';
import { thirdPartyUserCodeValidator } from './validators/third-party-user-code.validator';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { Agent, Airline } from '~app/master-data/models';
import { AppState } from '~app/reducers';
import { DialogConfig, DialogConfigData, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, DropdownOption, GdsSummary, ResponseErrorBE } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { UserTemplate } from '~app/shared/models/user-template.model';
import { User } from '~app/shared/models/user.model';

const FORM_VALUE_BY_USER = {
  [MainUserTypeForCreate.Agent]: (user: Agent) => ({
    portalEmail: user.contact?.email,
    name: user.name,
    telephone: user.contact?.telephone,
    country: user.address?.country,
    city: user.address?.city,
    address: user.address?.street,
    postCode: user.address?.postalCode
  }),
  [MainUserTypeForCreate.Airline]: (user: Airline) => ({
    portalEmail: user.contact?.email,
    name: user.localName,
    telephone: user.contact?.telephone,
    country: user.address?.country,
    city: user.address?.city,
    address: `${user.address?.address1 || ''} ${user.address?.address2 || ''}`.substring(0, 30),
    postCode: user.address?.postalCode
  })
};

@Component({
  selector: 'bspl-main-users-create-dialog',
  templateUrl: './main-users-create-dialog.component.html',
  styleUrls: ['./main-users-create-dialog.component.scss']
})
export class MainUsersCreateDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;
  public userTypeControl: FormControl;
  public userCodeControl: FormControl;

  public isThirdPartyUser = false;
  public isAgentUser = false;
  public isUserTypeNotSelected = true;
  public areFieldsLoading = false;

  public userTypeOptions: DropdownOption<typeof MainUserTypeForCreate>[] = this.getDropdownOptions(
    MainUserTypeForCreate,
    'MENU.USERS_MAINTENANCE.MAIN_USERS.filters.userType.options'
  );
  public userTemplateOptions: DropdownOption<typeof UserTemplate>[] = this.getDropdownOptions(
    UserTemplate,
    'MENU.USERS_MAINTENANCE.MAIN_USERS.filters.userTemplate.options'
  );
  public userCodeOptions$ = new BehaviorSubject<DropdownOption[]>([]);
  public loading$ = this.store.pipe(select(fromMainUsers.getCreateDialogLoading));

  public get areFieldsLocked(): boolean {
    return !this.userWasReceivedByUserCode && !this.isThirdPartyUser && !this.isGdsUser;
  }

  private createButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Create)
  );
  private createMainUserError$ = this.store.pipe(
    select(fromMainUsers.getCreateError),
    filter(error => !!error)
  );

  private isGdsUser = false;
  private userWasReceivedByUserCode = false;
  private dialogData: DialogConfigData = this.config.data;
  private createButton: ModalAction;
  private formFactory: FormUtil;

  private userCodeOptionsByUserType = {};
  private userCodeByUserType = {
    [MainUserTypeForCreate.Agent]: () => this.userCodeControl.value.code,
    [MainUserTypeForCreate.Airline]: () => this.userCodeControl.value.code,
    [MainUserTypeForCreate.GDS]: () => this.userCodeControl.value.gdsCode,
    [MainUserTypeForCreate.ThirdParty]: () => this.userCodeControl.value
  };
  private userByIdMap = {
    [MainUserTypeForCreate.Agent]: (id: number) => this.dataService.getAgent(id),
    [MainUserTypeForCreate.Airline]: (id: number) => this.dataService.getAirline(id)
  };

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    private translationService: L10nTranslationService,
    private dataService: MainUsersService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initiateData();
  }

  public ngAfterViewInit(): void {
    this.setIsDisabledStates();
  }

  private initiateData(): void {
    this.store.pipe(select(getUser), first()).subscribe(user => {
      this.setUserCodeOptionsByUserType(user);
      this.setInitialFormData();
      this.initializeCreateButton();
      this.initializeListeners();
      this.subscribeToUserTypeControl();
      this.subscribeToUserCodeControl();
    });
  }

  private setUserCodeOptionsByUserType(user: User): void {
    this.userCodeOptionsByUserType = {
      [MainUserTypeForCreate.Agent]: this.dataService.getAgents(),
      [MainUserTypeForCreate.Airline]: this.dataService.getAirlines(),
      [MainUserTypeForCreate.ThirdParty]: of([]),
      [MainUserTypeForCreate.GDS]: this.dataService.getGds(user.bsps?.[0]?.id)
    };
  }

  private setIsDisabledStates(): void {
    combineLatest([this.form.statusChanges, this.userCodeControl.statusChanges, this.userTypeControl.statusChanges])
      .pipe(
        distinctUntilChanged((previous, current) => isEqual(previous, current)),
        takeUntil(this.destroy$)
      )
      .subscribe(statuses => {
        this.createButton.isDisabled = statuses.some(status => status !== 'VALID');
      });
  }

  private setInitialFormData(): void {
    this.userTypeControl = new FormControl(null, Validators.required);
    this.userCodeControl = new FormControl(null, [
      Validators.required,
      thirdPartyUserCodeValidator(() => this.isThirdPartyUser)
    ]);

    this.form = this.formFactory.createGroup<MainUserCreateModelOmitted>({
      template: [null, [templateRequiredValidator(() => this.isAgentUser)]],
      portalEmail: [null, [Validators.required, Validators.maxLength(200)]],
      name: [null, [Validators.required, Validators.maxLength(49)]],
      organization: [null, [Validators.required, Validators.maxLength(30)]],
      telephone: [null, [Validators.required, Validators.maxLength(15)]],
      country: [null, [Validators.required, Validators.maxLength(26)]],
      city: [null, [Validators.required, Validators.maxLength(30)]],
      address: [null, [Validators.required, Validators.maxLength(30)]],
      postCode: [null, [Validators.required, Validators.maxLength(10)]]
    });
  }

  private initializeCreateButton(): void {
    this.createButton = this.dialogData.buttons.find(button => button.type === FooterButton.Create);
    if (this.createButton) {
      this.createButton.isDisabled = true;
      this.loading$.pipe(takeUntil(this.destroy$)).subscribe(loading => (this.createButton.isLoading = loading));
    }
  }

  private initializeListeners(): void {
    this.createButtonClick$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(
        MainUserCreateDialogActions.createMainUser({
          user: {
            ...this.form.value,
            userType: this.userTypeControl.value,
            userCode: this.userCodeByUserType[this.userTypeControl.value]()
          }
        })
      );
    });

    this.createMainUserError$.pipe(takeUntil(this.destroy$)).subscribe(({ error }) => this.setFieldErrors(error));
  }

  private subscribeToUserTypeControl(): void {
    this.userTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((userType: MainUserTypeForCreate) => {
      this.isThirdPartyUser = userType === MainUserTypeForCreate.ThirdParty;
      this.isAgentUser = userType === MainUserTypeForCreate.Agent;
      this.isGdsUser = userType === MainUserTypeForCreate.GDS;
      this.isUserTypeNotSelected = !userType;

      this.userCodeControl.reset();
      this.userCodeOptions$ = this.userCodeOptionsByUserType[userType];

      if (this.isThirdPartyUser) {
        this.form.reset();
      }
    });
  }

  private subscribeToUserCodeControl(): void {
    this.userCodeControl.valueChanges
      .pipe(
        filter(() => !this.isThirdPartyUser),
        takeUntil(this.destroy$)
      )
      .subscribe((userCode: AgentSummary | AirlineSummary | GdsSummary) => {
        this.form.reset();

        this.userWasReceivedByUserCode = false;

        if (userCode) {
          this.populateFieldsByUserCode(+userCode.id);
        }
      });
  }

  private populateFieldsByUserCode(userId: number): void {
    const getUserById: (userId: number) => Observable<Agent | Airline> = this.userByIdMap[this.userTypeControl.value];

    if (!getUserById) {
      return;
    }

    this.areFieldsLoading = true;

    getUserById(userId)
      .pipe(
        first(),
        finalize(() => {
          this.areFieldsLoading = false;
          this.userWasReceivedByUserCode = true;
        }),
        catchError(err => (err.error.errorCode === 403 ? of(null) : throwError(err)))
      )
      .subscribe((user: Agent | Airline) => {
        if (user) {
          this.form.patchValue(FORM_VALUE_BY_USER[this.userTypeControl.value](user));
        }
      });
  }

  private setFieldErrors(error: ResponseErrorBE): void {
    if (error.errorCode !== 400 || !error.messages?.length) {
      return;
    }

    // Validation error
    error.messages.forEach(({ message, messageParams }) => {
      const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
      const invalidControl = this.getInvalidControl(controlName);

      if (invalidControl) {
        invalidControl.setErrors({ requestError: { message } });
        FormUtil.showControlState(invalidControl);
      }
    });
  }

  private getInvalidControl(controlName: string): AbstractControl {
    if (controlName === 'userCode') {
      return this.userCodeControl;
    }

    if (controlName === 'userType') {
      return this.userTypeControl;
    }

    return this.form.get(controlName);
  }

  private getDropdownOptions<T>(object: T, translationKey: string): DropdownOption<T>[] {
    return Object.keys(object).map(key => ({
      value: object[key],
      label: this.translationService.translate(`${translationKey}.${key}`)
    }));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
