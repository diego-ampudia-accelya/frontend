import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { of, Subject } from 'rxjs';

import { MainUserCreateModelOmitted } from '../../models/create-main-user.model';
import { MainUserTypeForCreate } from '../../models/main-user-type.model';
import { MainUsersService } from '../../services/main-users.service';
import { MainUserCreateDialogActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import * as fromMainListOfUsers from '../../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../../store/reducers/main-users.reducer';

import { MainUsersCreateDialogComponent } from './main-users-create-dialog.component';
import { TranslatePipeMock } from '~app/test';
import { UserTemplate } from '~app/shared/models/user-template.model';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DialogConfig, FooterButton, InputComponent, SelectComponent } from '~app/shared/components';
import { AppState } from '~app/reducers';
import { getUser } from '~app/auth/selectors/auth.selectors';

describe('MainUsersCreateDialogComponent', () => {
  let component: MainUsersCreateDialogComponent;
  let fixture: ComponentFixture<MainUsersCreateDialogComponent>;
  let mockStore: MockStore<AppState>;

  const validFormValue: MainUserCreateModelOmitted = {
    template: UserTemplate.Efficient,
    portalEmail: 'test@email.com',
    name: 'test-name',
    organization: 'test-org',
    telephone: '123456789',
    country: 'Spain',
    city: 'Madrid',
    address: 'test-address',
    postCode: '111111'
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [
        MainUsersCreateDialogComponent,
        MockComponents(SelectComponent, InputComponent),
        TranslatePipeMock
      ],
      providers: [
        {
          provide: DialogConfig,
          useValue: {
            data: {
              buttons: []
            }
          }
        },
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [
            { selector: getUser, value: { bsps: [{ id: 1111 }] } },
            { selector: fromMainUsers.getCreateDialogLoading, value: of(false) },
            { selector: fromMainUsers.getCreateError, value: null }
          ]
        }),
        FormBuilder,
        { provide: ReactiveSubject, useValue: { asObservable: of({}) } },
        { provide: L10nTranslationService, useValue: { translate: () => '' } },
        {
          provide: MainUsersService,
          useValue: {
            getAgents: () => of([]),
            getAirlines: () => of([{ id: 10 }]),
            getGds: () => of([]),
            getAgent: () => of({}),
            getAirline: () => of({})
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUsersCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('initiateData', () => {
    beforeEach(() => {
      (component as any).setInitialFormData = jasmine.createSpy();
      (component as any).initializeCreateButton = jasmine.createSpy();
      (component as any).initializeListeners = jasmine.createSpy();
      (component as any).subscribeToUserTypeControl = jasmine.createSpy();
      (component as any).subscribeToUserCodeControl = jasmine.createSpy();
    });

    it('should set userCodeOptionsByUserType', fakeAsync(() => {
      (component as any).userCodeOptionsByUserType = null;

      (component as any).initiateData();
      tick();

      expect((component as any).userCodeOptionsByUserType).not.toBeNull();
    }));

    it('should call setInitialFormData()', fakeAsync(() => {
      (component as any).initiateData();
      tick();

      expect((component as any).setInitialFormData).toHaveBeenCalled();
    }));

    it('should call initializeCreateButton()', fakeAsync(() => {
      (component as any).initiateData();
      tick();

      expect((component as any).initializeCreateButton).toHaveBeenCalled();
    }));

    it('should call initializeListeners()', fakeAsync(() => {
      (component as any).initiateData();
      tick();

      expect((component as any).initializeListeners).toHaveBeenCalled();
    }));

    it('should call subscribeToUserTypeControl()', fakeAsync(() => {
      (component as any).initiateData();
      tick();

      expect((component as any).subscribeToUserTypeControl).toHaveBeenCalled();
    }));

    it('should call subscribeToUserCodeControl()', fakeAsync(() => {
      (component as any).initiateData();
      tick();

      expect((component as any).subscribeToUserCodeControl).toHaveBeenCalled();
    }));
  });

  describe('setIsDisabledStates', () => {
    beforeEach(() => {
      (component as any).setInitialFormData();
      (component as any).createButton = {};
    });

    it('should set isDisable state for createButton as false when form and controls are valid', fakeAsync(() => {
      (component as any).setIsDisabledStates();
      tick();

      (component as any).createButton.isDisabled = true;
      component.form.setValue(validFormValue);
      component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      component.userCodeControl.setValue('test-code');

      expect((component as any).createButton.isDisabled).toBe(false);
    }));

    it('should set isDisable state for createButton as true when form is invalid', fakeAsync(() => {
      (component as any).setIsDisabledStates();
      tick();

      (component as any).createButton.isDisabled = false;
      component.form.setValue({ ...validFormValue, postCode: '' });
      component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      component.userCodeControl.setValue('test-code');

      expect((component as any).createButton.isDisabled).toBe(true);
    }));

    it('should set isDisable state for createButton as false when userTypeControl is invalid', fakeAsync(() => {
      (component as any).setIsDisabledStates();
      tick();

      (component as any).createButton.isDisabled = false;
      component.form.setValue(validFormValue);
      component.userTypeControl.setValue('');
      component.userCodeControl.setValue('test-code');

      expect((component as any).createButton.isDisabled).toBe(true);
    }));

    it('should set isDisable state for createButton as false when userCodeControl is invalid', fakeAsync(() => {
      (component as any).setIsDisabledStates();
      tick();

      (component as any).createButton.isDisabled = false;
      component.form.setValue(validFormValue);
      component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      component.userCodeControl.setValue('');

      expect((component as any).createButton.isDisabled).toBe(true);
    }));
  });

  describe('setInitialFormData', () => {
    it('should set userTypeControl', () => {
      (component as any).userTypeControl = null;

      (component as any).setInitialFormData();

      expect((component as any).userTypeControl).not.toBeNull();
    });

    it('should set userCodeControl', () => {
      (component as any).userCodeControl = null;

      (component as any).setInitialFormData();

      expect((component as any).userCodeControl).not.toBeNull();
    });

    it('should set form', () => {
      component.form = null;

      (component as any).setInitialFormData();

      expect(component.form).not.toBeNull();
      expect(component.form.value).toEqual({
        template: null,
        portalEmail: null,
        name: null,
        organization: null,
        telephone: null,
        country: null,
        city: null,
        address: null,
        postCode: null
      });
    });
  });

  describe('initializeCreateButton', () => {
    it('should set createButton', () => {
      (component as any).createButton = null;
      (component as any).loading$ = new Subject();
      (component as any).dialogData.buttons = [{ type: FooterButton.Create, isLoading: true }];

      (component as any).initializeCreateButton();
      (component as any).loading$.next(false);

      expect((component as any).createButton).toEqual({
        type: FooterButton.Create,
        isDisabled: true,
        isLoading: false
      });
    });

    it('should set isDisabled state for createButton', fakeAsync(() => {
      (component as any).createButton = null;
      (component as any).loading$ = new Subject();
      (component as any).dialogData.buttons = [{ type: FooterButton.Create }];

      (component as any).initializeCreateButton();
      tick();

      expect((component as any).createButton.isDisabled).toBe(true);
    }));

    it('should set isLoading state for createButton', fakeAsync(() => {
      (component as any).createButton = null;
      (component as any).loading$ = new Subject();
      (component as any).dialogData.buttons = [{ type: FooterButton.Create, isLoading: false }];

      (component as any).initializeCreateButton();
      (component as any).loading$.next(true);
      tick();

      expect((component as any).createButton.isLoading).toBe(true);
    }));
  });

  describe('initializeListeners with user', () => {
    beforeEach(() => {
      (component as any).setInitialFormData();
    });

    it('should dispatch MainUserCreateDialogActions.createMainUser', fakeAsync(() => {
      (component as any).createButtonClick$ = new Subject();
      component.form.setValue(validFormValue);
      component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      component.userCodeControl.setValue({ code: 'test-code' });

      (component as any).initializeListeners();
      (component as any).createButtonClick$.next();
      tick();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserCreateDialogActions.createMainUser({
          user: {
            ...validFormValue,
            userType: MainUserTypeForCreate.Airline,
            userCode: 'test-code'
          }
        })
      );
    }));

    it('should call setFieldErrors with error', fakeAsync(() => {
      (component as any).createMainUserError$ = new Subject();
      (component as any).setFieldErrors = jasmine.createSpy();
      // component.form.setValue(validFormValue);
      // component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      // component.userCodeControl.setValue({ code: 'test-code' });

      (component as any).initializeListeners();
      (component as any).createMainUserError$.next({ error: { errorCode: 500, message: 'Server Error' } });
      tick();

      expect((component as any).setFieldErrors).toHaveBeenCalledWith({ errorCode: 500, message: 'Server Error' });
    }));
  });

  describe('subscribeToUserTypeControl', () => {
    beforeEach(() => {
      (component as any).setInitialFormData();
    });

    it('should set user type flags', fakeAsync(() => {
      component.isThirdPartyUser = undefined;
      component.isAgentUser = undefined;
      component.isUserTypeNotSelected = undefined;
      (component as any).isGdsUser = undefined;

      (component as any).subscribeToUserTypeControl();
      (component as any).userTypeControl.setValue(MainUserTypeForCreate.Agent);

      expect(component.isThirdPartyUser).toBe(false);
      expect(component.isAgentUser).toBe(true);
      expect(component.isUserTypeNotSelected).toBe(false);
      expect((component as any).isGdsUser).toBe(false);
    }));

    it('should reset userCodeControl', fakeAsync(() => {
      component.userCodeControl.setValue('test-code');

      (component as any).subscribeToUserTypeControl();
      (component as any).userTypeControl.setValue(MainUserTypeForCreate.Agent);

      expect(component.userCodeControl.value).toBe(null);
    }));

    it('should set userCodeOptions', fakeAsync(() => {
      component.userCodeOptions$ = null;
      (component as any).setUserCodeOptionsByUserType({});
      (component as any).dataService.getAirlines = jasmine.createSpy().and.returnValue(of([]));

      (component as any).subscribeToUserTypeControl();
      (component as any).userTypeControl.setValue(MainUserTypeForCreate.Agent);

      expect(component.userCodeOptions$.value).not.toBe(null);
    }));

    it('should reset form', fakeAsync(() => {
      component.form.setValue(validFormValue);

      (component as any).subscribeToUserTypeControl();
      (component as any).userTypeControl.setValue(MainUserTypeForCreate.ThirdParty);

      expect(component.form.value).toEqual({
        template: null,
        portalEmail: null,
        name: null,
        organization: null,
        telephone: null,
        country: null,
        city: null,
        address: null,
        postCode: null
      });
    }));
  });

  describe('populateFieldsByUserCode', () => {
    beforeEach(() => {
      (component as any).setInitialFormData();
    });

    it('should not set userWasReceivedByUserCode to true when userType is not Airline or Agent', () => {
      component.userTypeControl.setValue(MainUserTypeForCreate.GDS);
      (component as any).userWasReceivedByUserCode = false;

      (component as any).populateFieldsByUserCode();

      expect((component as any).userWasReceivedByUserCode).toBe(false);
    });

    it('should set userWasReceivedByUserCode to true when userType is Airline', fakeAsync(() => {
      component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      (component as any).userWasReceivedByUserCode = false;

      (component as any).populateFieldsByUserCode();
      tick();

      expect((component as any).userWasReceivedByUserCode).toBe(true);
    }));

    it('should call dataService.getAirline() with user id when userType is Airline', fakeAsync(() => {
      component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      (component as any).dataService.getAirline = jasmine.createSpy().and.returnValue(
        of({
          id: 10,
          contact: {
            email: 'test-email',
            telephone: '1111111'
          },
          address: {
            country: 'Spain',
            city: 'Madrid',
            address1: 'test-test-address1',
            address2: 'test-test-address2',
            postalCode: 'post-code'
          },
          localName: 'test-name'
        })
      );

      (component as any).populateFieldsByUserCode(20);
      tick();

      expect((component as any).dataService.getAirline).toHaveBeenCalledWith(20);
    }));

    it('should set form value by airline user data when userType is Airline', fakeAsync(() => {
      component.form.reset();
      component.userTypeControl.setValue(MainUserTypeForCreate.Airline);
      (component as any).dataService.getAirline = jasmine.createSpy().and.returnValue(
        of({
          id: 10,
          contact: {
            email: 'test-email',
            telephone: '1111111'
          },
          address: {
            country: 'Spain',
            city: 'Madrid',
            address1: 'test-test-address1',
            address2: 'test-test-address2',
            postalCode: 'post-code'
          },
          localName: 'test-name'
        })
      );

      (component as any).populateFieldsByUserCode(20);
      tick();

      expect(component.form.value).toEqual({
        portalEmail: 'test-email',
        telephone: '1111111',
        country: 'Spain',
        city: 'Madrid',
        address: 'test-test-address1 test-test-a',
        postCode: 'post-code',
        name: 'test-name',
        organization: null,
        template: null
      });
    }));
  });

  describe('setFieldErrors', () => {
    it('should set errors to portalEmail', () => {
      const error = {
        errorCode: 400,
        messages: [{ message: 'Already exist', messageParams: [{ name: 'fieldName', value: 'portalEmail' }] }]
      };
      (component as any).setInitialFormData();
      expect(component.form.get('portalEmail').getError('requestError')).not.toBeDefined();

      (component as any).setFieldErrors(error);

      expect(component.form.get('portalEmail').getError('requestError')).toEqual({ message: 'Already exist' });
    });
  });

  describe('getInvalidControl', () => {
    it('should return userCodeControl', () => {
      const res = (component as any).getInvalidControl('userCode');

      expect(res).toEqual(component.userCodeControl);
    });

    it('should return userTypeControl', () => {
      const res = (component as any).getInvalidControl('userType');

      expect(res).toEqual(component.userTypeControl);
    });

    it('should return portalEmail control from form', () => {
      const res = (component as any).getInvalidControl('portalEmail');

      expect(res).toEqual(component.form.controls.portalEmail);
    });
  });

  describe('getDropdownOptions', () => {
    beforeEach(() => {
      (component as any).translationService.translate = jasmine
        .createSpy()
        .and.callFake(key => (key === 'common.Active' ? 'test-active' : 'test-inactive'));
    });

    it('should return mapped options', () => {
      const expectedRes = [
        { value: 'ACTIVE', label: 'test-active' },
        { value: 'INACTIVE', label: 'test-inactive' }
      ];
      const res = (component as any).getDropdownOptions(ActiveInactiveInUpperCase, 'common');

      expect(res).toEqual(expectedRes);
    });

    it('should call translationService.translate', () => {
      (component as any).getDropdownOptions(ActiveInactiveInUpperCase, 'common');

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.first().args).toEqual(['common.Active']);
      expect((component as any).translationService.translate.calls.mostRecent().args).toEqual(['common.Inactive']);
    });
  });
});
