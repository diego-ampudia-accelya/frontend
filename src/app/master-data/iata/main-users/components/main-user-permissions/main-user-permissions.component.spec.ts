import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';

import { MainUserPermissionsChangesService } from '../../services/main-user-permissions-changes.service';
import { MainUserPermissionsActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import * as fromMainListOfUsers from '../../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../../store/reducers/main-users.reducer';

import { MainUserPermissionsComponent } from './main-user-permissions.component';
import { SelectionListComponent } from '~app/shared/components/selection-list/selection-list.component';
import { SpinnerComponent } from '~app/shared/components';
import { AppState } from '~app/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';

describe('MainUserPermissionsComponent', () => {
  let component: MainUserPermissionsComponent;
  let fixture: ComponentFixture<MainUserPermissionsComponent>;
  let mockStore: MockStore<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MainUserPermissionsComponent, MockComponents(SelectionListComponent, SpinnerComponent)],
      providers: [
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [
            { selector: fromMainUsers.canApplyPermissionsChanges, value: false },
            { selector: fromMainUsers.getMainUserPermissionsLoading, value: false },
            { selector: fromMainUsers.getOriginalMainUserPermissions, value: [] }
          ]
        }),
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        },
        {
          provide: MainUserPermissionsChangesService,
          useValue: {
            handlePermissionsChangeAndDeactivate$: of(true)
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUserPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onPermissionChange', () => {
    it('should dispatch MainUserPermissionsActions.change', () => {
      (component as any).onPermissionChange([{ name: 'perm1' }, { name: 'perm2' }]);

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserPermissionsActions.change({ modifications: [{ name: 'perm1' }, { name: 'perm2' }] as any })
      );
    });
  });

  describe('canDeactivate', () => {
    it('should return true', fakeAsync(() => {
      let res;

      component.canDeactivate().subscribe(r => (res = r));
      tick();

      expect(res).toBe(true);
    }));

    it('should return handlePermissionsChangeAndDeactivate$ as false', fakeAsync(() => {
      let res;
      (component as any).changesService.handlePermissionsChangeAndDeactivate$ = of(false);
      mockStore.overrideSelector(fromMainUsers.canApplyPermissionsChanges, true);

      component.canDeactivate().subscribe(r => (res = r));
      tick();

      expect(res).toBe(false);
    }));
  });

  describe('initializeUserBsps', () => {
    it('should dispatch MainUserPermissionsActions.requestPermissions', () => {
      (component as any).initializeUserBsps();

      expect(mockStore.dispatch).toHaveBeenCalledWith(MainUserPermissionsActions.requestPermissions());
    });
  });
});
