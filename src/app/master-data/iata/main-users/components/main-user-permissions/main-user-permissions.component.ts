import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { MainUserPermissionsChangesService } from '../../services/main-user-permissions-changes.service';
import { MainUserPermissionsActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { Permissions } from '~app/shared/constants';

@Component({
  selector: 'bspl-main-user-permissions',
  templateUrl: './main-user-permissions.component.html',
  styleUrls: ['./main-user-permissions.component.scss']
})
export class MainUserPermissionsComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromMainUsers.getMainUserPermissionsLoading));
  // Use defensive cloning to force the SelectionListComponent to work with the store
  public permissions$ = this.store.pipe(select(fromMainUsers.getOriginalMainUserPermissions), map(cloneDeep));

  public canEdit = this.permissionsService.hasPermission(Permissions.updateMainUserPermissions);

  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private changesService: MainUserPermissionsChangesService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.initializeUserBsps();
  }

  public onPermissionChange(modifications: SelectionList[]): void {
    // Use defensive cloning to force the SelectionListComponent to work with the store
    this.store.dispatch(MainUserPermissionsActions.change({ modifications: cloneDeep(modifications) }));
  }

  public canDeactivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromMainUsers.canApplyPermissionsChanges),
      switchMap(hasChanges => (hasChanges ? this.changesService.handlePermissionsChangeAndDeactivate$ : of(true))),
      first()
    );
  }

  private initializeUserBsps(): void {
    this.store.dispatch(MainUserPermissionsActions.requestPermissions());
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
