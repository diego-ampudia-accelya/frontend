import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MainUserType } from '../../models/main-user-type.model';
import { MainUserBE, MainUsersFilter } from '../../models/main-users.model';
import { MainUsersFilterFormatter } from '../../services/main-users-filter-formatter';
import { MainUserActivationDialogActions, MainUserCreateDialogActions, MainUsersActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions, ROUTES } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { ActiveInactiveInUpperCase, DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserTemplate } from '~app/shared/models/user-template.model';

const MAIN_USERS_ACTIONS = [
  {
    action: GridTableActionType.ListOfUsers,
    requiredPermission: Permissions.readMainUsers
  },
  {
    action: GridTableActionType.UserProperties,
    requiredPermission: Permissions.readMainUsers
  },
  {
    action: GridTableActionType.PermissionAndTemplate,
    disabled: (row: MainUserBE) => row.lean || row.userType === MainUserType.IATA,
    requiredPermission: Permissions.readMainUserPermissions
  },
  {
    action: GridTableActionType.DeactivateUser,
    hidden: (row: MainUserBE) => row.status === ActiveInactiveInUpperCase.Inactive,
    requiredPermission: Permissions.updateMainUsers
  },
  {
    action: GridTableActionType.ReactivateUser,
    hidden: (row: MainUserBE) => row.status === ActiveInactiveInUpperCase.Active,
    requiredPermission: Permissions.updateMainUsers
  }
];

@Component({
  selector: 'bspl-main-users',
  templateUrl: './main-users.component.html',
  providers: [DatePipe]
})
export class MainUsersComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public hasCreating: boolean = this.permissionsService.hasPermission(Permissions.createMainUsers);
  public primaryActions: Array<{ action: GridTableActionType }> = MAIN_USERS_ACTIONS.filter(({ requiredPermission }) =>
    this.permissionsService.hasPermission(requiredPermission)
  );

  public loading$ = this.store.pipe(select(fromMainUsers.getLoading));
  public data$ = this.store.pipe(select(fromMainUsers.getData));
  public query$ = this.store.pipe(select(fromMainUsers.getQuery));
  public hasData$ = this.store.pipe(select(fromMainUsers.hasData));

  public userTypeOptions: DropdownOption<typeof MainUserType>[] = this.getDropdownOptions(
    MainUserType,
    'MENU.USERS_MAINTENANCE.MAIN_USERS.filters.userType.options'
  );
  public userTemplateOptions: DropdownOption<typeof UserTemplate>[] = this.getDropdownOptions(
    UserTemplate,
    'MENU.USERS_MAINTENANCE.MAIN_USERS.filters.userTemplate.options'
  );
  public statusOptions: DropdownOption<typeof ActiveInactiveInUpperCase>[] = this.getDropdownOptions(
    ActiveInactiveInUpperCase,
    'common'
  );

  public customLabels = { create: 'MENU.USERS_MAINTENANCE.MAIN_USERS.createMainUserLabel' };

  private formFactory: FormUtil;
  private reactivateOrDeactivateSubmitEmitter = new Subject();
  private destroy$ = new Subject();

  constructor(
    public displayFormatter: MainUsersFilterFormatter,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private datePipe: DatePipe,
    private store: Store<AppState>,
    private permissionsService: PermissionsService,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.initializeDataQuery();
    this.initColumns();
  }

  public onQueryChanged(query: DataQuery<MainUsersFilter>): void {
    this.store.dispatch(MainUsersActions.search({ query }));
  }

  public onDownload(): void {
    this.store.dispatch(MainUsersActions.download());
  }

  public create(): void {
    this.store.dispatch(MainUserCreateDialogActions.openMainUserCreateDialog());
  }

  public onRowLinkClick({ id }: MainUserBE): void {
    this.router.navigate([ROUTES.MAIN_USER_DETAILS.url, id]);
  }

  public onGridActionClick({ action, row }: { action: GridTableAction; row: MainUserBE }): void {
    switch (action.actionType) {
      case GridTableActionType.DeactivateUser:
        // TODO: component can't destroy after opening activation dialog,
        // when reactivateOrDeactivateSubmitEmitter is used by itself, without a function
        this.store.dispatch(
          MainUserActivationDialogActions.openDeactivationDialog({
            user: row,
            actionEmitter: () => this.reactivateOrDeactivateSubmitEmitter
          })
        );
        break;
      case GridTableActionType.ReactivateUser:
        this.store.dispatch(
          MainUserActivationDialogActions.openReactivationDialog({
            user: row,
            actionEmitter: () => this.reactivateOrDeactivateSubmitEmitter
          })
        );
        break;
      case GridTableActionType.UserProperties:
        this.router.navigate([ROUTES.MAIN_USER_DETAILS.url, row.id, 'properties']);
        break;
      case GridTableActionType.PermissionAndTemplate:
        this.router.navigate([ROUTES.MAIN_USER_DETAILS.url, row.id, 'permissions']);
        break;
      case GridTableActionType.ListOfUsers:
        this.router.navigate([ROUTES.MAIN_USER_DETAILS.url, row.id, 'users-list']);
        break;
    }
  }

  private initializeDataQuery(): void {
    this.store.dispatch(MainUsersActions.load());

    this.reactivateOrDeactivateSubmitEmitter
      .asObservable()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.store.dispatch(MainUsersActions.load()));
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<MainUsersFilter>({
      userType: [],
      userCode: [],
      userTemplate: [],
      portalAccessEmail: [],
      name: [],
      registerDate: [],
      expiryDate: [],
      status: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.userType',
        prop: 'userType'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.userCode',
        prop: 'userCode'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.userTemplate',
        prop: 'template',
        pipe: {
          transform: value =>
            value
              ? this.translationService.translate(
                  `MENU.USERS_MAINTENANCE.MAIN_USERS.filters.userTemplate.options.${value}`
                )
              : value
        }
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.portalAccessEmail',
        prop: 'portalEmail',
        cellTemplate: 'commonLinkFromObjCellTmpl'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.name',
        prop: 'name'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.registerDate',
        prop: 'registerDate',
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy')
        }
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.expiryDate',
        prop: 'expiryDate',
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy')
        }
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.numberOfUsers',
        prop: 'numberOfUsers'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.status',
        prop: 'status',
        pipe: {
          transform: value => (value ? this.translationService.translate(`common.${value}`) : value)
        }
      }
    ];
  }

  private getDropdownOptions<T>(object: T, translationKey: string): DropdownOption<T>[] {
    return Object.keys(object).map(key => ({
      value: object[key],
      label: this.translationService.translate(`${translationKey}.${key}`)
    }));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
