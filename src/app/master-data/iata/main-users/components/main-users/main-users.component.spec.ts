import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';

import { MainUsersFilterFormatter } from '../../services/main-users-filter-formatter';
import { MainUserActivationDialogActions, MainUserCreateDialogActions, MainUsersActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import * as fromMainListOfUsers from '../../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../../store/reducers/main-users.reducer';
import { MainUsersComponent } from './main-users.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import {
  GridTableComponent,
  InputComponent,
  PropertyComponent,
  PropertyListComponent,
  SelectComponent
} from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { ListViewComponent } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { TranslatePipeMock } from '~app/test';

describe('MainUsersComponent', () => {
  let component: MainUsersComponent;
  let fixture: ComponentFixture<MainUsersComponent>;
  let mockStore: MockStore<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule],
      declarations: [
        MainUsersComponent,
        MockComponents(
          ListViewComponent,
          SelectComponent,
          InputComponent,
          DatepickerComponent,
          GridTableComponent,
          PropertyListComponent,
          PropertyComponent
        ),
        TranslatePipeMock
      ],
      providers: [
        FormBuilder,
        { provide: MainUsersFilterFormatter, useValue: {} },
        { provide: L10nTranslationService, useValue: { translate: () => '' } },
        { provide: PermissionsService, useValue: { hasPermission: () => true } },
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [
            { selector: fromMainUsers.getLoading, value: false },
            { selector: fromMainUsers.getData, value: [] },
            { selector: fromMainUsers.getQuery, value: defaultQuery },
            { selector: fromMainUsers.hasData, value: true }
          ]
        })
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onQueryChanged', () => {
    it('should dispatch MainUsersActions.search', () => {
      component.onQueryChanged({ userCode: 'test-user-code' } as any);

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUsersActions.search({ query: { userCode: 'test-user-code' } } as any)
      );
    });
  });

  describe('onDownload', () => {
    it('should dispatch MainUsersActions.download', () => {
      component.onDownload();

      expect(mockStore.dispatch).toHaveBeenCalledWith(MainUsersActions.download());
    });
  });

  describe('create', () => {
    it('should dispatch MainUserCreateDialogActions.openMainUserCreateDialog', () => {
      component.create();

      expect(mockStore.dispatch).toHaveBeenCalledWith(MainUserCreateDialogActions.openMainUserCreateDialog());
    });
  });

  describe('onRowLinkClick', () => {
    it('should navigate to user details with id', () => {
      (component as any).router.navigate = jasmine.createSpy().and.callThrough();

      component.onRowLinkClick({ id: 10 } as any);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.MAIN_USER_DETAILS.url, 10]);
    });
  });

  describe('onGridActionClick', () => {
    it('should dispatch MainUserActivationDialogActions.openDeactivationDialog when action is DeactivateUser', () => {
      component.onGridActionClick({
        action: { actionType: GridTableActionType.DeactivateUser },
        row: { id: 10 }
      } as any);

      expect((mockStore.dispatch as any).calls.first().args).toEqual([
        MainUserActivationDialogActions.openDeactivationDialog({
          user: { id: 10 } as any,
          actionEmitter: jasmine.any(Function)
        })
      ]);
    });

    it('should dispatch MainUserActivationDialogActions.openReactivationDialog when action is ReactivateUser', () => {
      component.onGridActionClick({
        action: { actionType: GridTableActionType.ReactivateUser },
        row: { id: 10 }
      } as any);

      expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
      expect((mockStore.dispatch as any).calls.first().args).toEqual([
        MainUserActivationDialogActions.openReactivationDialog({
          user: { id: 10 } as any,
          actionEmitter: jasmine.any(Function)
        })
      ]);
    });

    it('should navigate to main user properties when action is UserProperties', () => {
      (component as any).router.navigate = jasmine.createSpy().and.callThrough();
      component.onGridActionClick({
        action: { actionType: GridTableActionType.UserProperties },
        row: { id: 10 }
      } as any);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.MAIN_USER_DETAILS.url, 10, 'properties']);
    });

    it('should navigate to main user permissions when action is PermissionAndTemplate', () => {
      (component as any).router.navigate = jasmine.createSpy().and.callThrough();
      component.onGridActionClick({
        action: { actionType: GridTableActionType.PermissionAndTemplate },
        row: { id: 10 }
      } as any);

      expect((component as any).router.navigate).toHaveBeenCalledWith([
        ROUTES.MAIN_USER_DETAILS.url,
        10,
        'permissions'
      ]);
    });

    it('should navigate to main users-list when action is ListOfUsers', () => {
      (component as any).router.navigate = jasmine.createSpy().and.callThrough();
      component.onGridActionClick({
        action: { actionType: GridTableActionType.ListOfUsers },
        row: { id: 10 }
      } as any);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.MAIN_USER_DETAILS.url, 10, 'users-list']);
    });
  });

  describe('initializeDataQuery', () => {
    it('should dispatch MainUsersActions.load 1 time', () => {
      (component as any).initializeDataQuery();

      expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
      expect(mockStore.dispatch).toHaveBeenCalledWith(MainUsersActions.load());
    });

    it('should dispatch MainUsersActions.load 2 times when subject emits', () => {
      (component as any).initializeDataQuery();
      (component as any).reactivateOrDeactivateSubmitEmitter.next();

      expect(mockStore.dispatch).toHaveBeenCalledTimes(3);
      expect(mockStore.dispatch).toHaveBeenCalledWith(MainUsersActions.load());
    });
  });

  describe('buildForm', () => {
    it('should return form with corresponding value', () => {
      const expectedRes = {
        userType: null,
        userCode: null,
        userTemplate: null,
        portalAccessEmail: null,
        name: null,
        registerDate: null,
        expiryDate: null,
        status: null
      };

      const form = (component as any).buildForm();

      expect(form.value).toEqual(expectedRes);
    });
  });

  describe('initColumns', () => {
    it('should set columns', () => {
      component.columns = null;

      (component as any).initColumns();

      expect(component.columns).not.toBeNull();
    });
  });

  describe('getDropdownOptions', () => {
    beforeEach(() => {
      (component as any).translationService.translate = jasmine
        .createSpy()
        .and.callFake(key => (key === 'common.Active' ? 'test-active' : 'test-inactive'));
    });

    it('should return mapped options', () => {
      const expectedRes = [
        { value: 'ACTIVE', label: 'test-active' },
        { value: 'INACTIVE', label: 'test-inactive' }
      ];
      const res = (component as any).getDropdownOptions(ActiveInactiveInUpperCase, 'common');

      expect(res).toEqual(expectedRes);
    });

    it('should call translationService.translate', () => {
      (component as any).getDropdownOptions(ActiveInactiveInUpperCase, 'common');

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.first().args).toEqual(['common.Active']);
      expect((component as any).translationService.translate.calls.mostRecent().args).toEqual(['common.Inactive']);
    });
  });
});
