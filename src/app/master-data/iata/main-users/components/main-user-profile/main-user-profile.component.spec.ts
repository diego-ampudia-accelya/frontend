import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';

import * as fromMainUsers from '../../store/reducers';
import * as fromMainListOfUsers from '../../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../../store/reducers/main-users.reducer';

import { MainUserProfileComponent } from './main-user-profile.component';
import { TranslatePipeMock } from '~app/test';
import { TabNavbarComponent } from '~app/shared/components/tabs/tab-navbar/tab-navbar.component';
import { TabLinkComponent } from '~app/shared/components/tabs/tab-navbar/tab-link/tab-link.component';
import { ButtonComponent } from '~app/shared/components';
import { MenuBuilder } from '~app/master-data/configuration';
import { PermissionsService } from '~app/auth/services/permissions.service';

describe('MainUserProfileComponent', () => {
  let component: MainUserProfileComponent;
  let fixture: ComponentFixture<MainUserProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        MainUserProfileComponent,
        MockComponents(TabNavbarComponent, TabLinkComponent, ButtonComponent),
        TranslatePipeMock
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {}
          }
        },
        {
          provide: MenuBuilder,
          useValue: {
            buildMenuItemsFrom: () => []
          }
        },
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [{ selector: fromMainUsers.getProfileMainUserPortalEmail, value: '' }]
        }),
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
