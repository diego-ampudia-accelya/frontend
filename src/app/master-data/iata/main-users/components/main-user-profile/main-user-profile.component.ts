import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, takeUntil, tap } from 'rxjs/operators';

import { MainUserType } from '../../models/main-user-type.model';
import { MainUserPermissionsActions, MainUserPropertiesActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { MENU } from '~app/master-data/configuration/menu.constants';
import { AppState } from '~app/reducers';
import { Permissions } from '~app/shared/constants';
import { or } from '~app/shared/utils';

@Component({
  selector: 'bspl-main-user-profile',
  templateUrl: './main-user-profile.component.html',
  styleUrls: ['./main-user-profile.component.scss']
})
export class MainUserProfileComponent implements OnInit, OnDestroy {
  public userPortalEmail$: Observable<string> = this.store.pipe(select(fromMainUsers.getProfileMainUserPortalEmail));
  public mainUserType$: Observable<MainUserType> = this.store.pipe(
    select(fromMainUsers.getMainUser),
    map(user => user?.userType)
  );
  public tabs: RoutedMenuItem[];
  public isApplyChangesEnabled$: Observable<boolean>;
  public currentSavableTab = null;
  public isApplyChangesVisible = false;

  private savableTabs = [
    {
      url: './properties',
      canApplyChangesSelector: fromMainUsers.canApplyPropertyChanges,
      applyChangesAction: MainUserPropertiesActions.applyPropertyChanges(),
      applyChangesPermission: Permissions.updateMainUsers
    },
    {
      url: './permissions',
      canApplyChangesSelector: fromMainUsers.canApplyPermissionsChanges,
      applyChangesAction: MainUserPermissionsActions.openApplyChanges(),
      applyChangesPermission: Permissions.updateMainUserPermissions
    }
  ];

  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    private router: Router,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.setTabs();
    this.isApplyChangesEnabled$ = this.getIsApplyChanges();

    this.listenToCurrentTab();
  }

  public applyChanges(): void {
    this.store.dispatch(this.currentSavableTab.applyChangesAction);
  }

  public trackByIndex(index: number): number {
    return index;
  }

  private setTabs(): void {
    this.mainUserType$.pipe(distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(userType => {
      this.tabs = this.getTabs(userType);
    });
  }

  private getTabs(userType: MainUserType): RoutedMenuItem[] {
    const originalTabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    return userType === MainUserType.IATA
      ? originalTabs.filter(tab => tab.route !== MENU.TABS.PERMISSIONS)
      : originalTabs;
  }

  private getIsApplyChanges(): Observable<boolean> {
    return combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab.url === savableTab.url)
        )
      )
    ).pipe(map(values => or(...values)));
  }

  private listenToCurrentTab(): void {
    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        map(activeSavableTab => {
          this.currentSavableTab = activeSavableTab;

          return activeSavableTab && activeSavableTab.canApplyChangesSelector
            ? this.permissionsService.hasPermission(activeSavableTab.applyChangesPermission)
            : false;
        }),
        tap(applyChangesVisible => (this.isApplyChangesVisible = applyChangesVisible)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
