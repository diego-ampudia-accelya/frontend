import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { MainUserBE } from '../../models/main-users.model';
import { MainUserActivationDialogActions } from '../../store/actions';
import * as fromMainUserActivationDialog from '../../store/reducers';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-main-user-deactivate-dialog',
  templateUrl: './main-user-deactivate-dialog.component.html',
  styleUrls: ['./main-user-deactivate-dialog.component.scss']
})
export class MainUserDeactivateDialogComponent implements OnInit, OnDestroy {
  public user: MainUserBE = this.config.data.user;
  public expiryDateControl: FormControl = new FormControl(new Date(), [Validators.required]);
  public minimumExpiryDate = new Date();

  private deactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter((res: any) => res?.clickedBtn === FooterButton.Deactivate)
  );
  private deactivateButton: ModalAction;
  private destroy$ = new Subject();

  constructor(private config: DialogConfig, private reactiveSubject: ReactiveSubject, private store: Store<AppState>) {}

  public ngOnInit(): void {
    this.initializeSaveButton();
    this.subscribeToLoading();
    this.subscribeToButtonClick();
  }

  private initializeSaveButton(): void {
    this.deactivateButton = this.config.data.buttons.find(
      (button: ModalAction) => button.type === FooterButton.Deactivate
    );
  }

  private subscribeToLoading(): void {
    this.store
      .pipe(
        select(fromMainUserActivationDialog.getMainUserActivationDialogLoading),
        tap(loading => this.setLoading(loading))
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  private subscribeToButtonClick(): void {
    this.deactivateButtonClick$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(
        MainUserActivationDialogActions.deactivate({
          userId: this.user.id,
          expiryDate: this.expiryDateControl.value,
          actionEmitter: this.config.data?.actionEmitter
        })
      );
    });
  }

  private setLoading(loading: boolean): void {
    if (this.deactivateButton) {
      this.deactivateButton.isLoading = loading;
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
