import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { of, Subject } from 'rxjs';

import { MainUserActivationDialogActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import * as fromMainListOfUsers from '../../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../../store/reducers/main-users.reducer';

import { MainUserDeactivateDialogComponent } from './main-user-deactivate-dialog.component';
import { TranslatePipeMock } from '~app/test';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { DialogConfig, FooterButton, PropertyComponent, PropertyListComponent } from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('MainUserDeactivateDialogComponent', () => {
  let component: MainUserDeactivateDialogComponent;
  let fixture: ComponentFixture<MainUserDeactivateDialogComponent>;
  let mockStore: MockStore<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        MainUserDeactivateDialogComponent,
        TranslatePipeMock,
        MockComponents(PropertyListComponent, PropertyComponent, DatepickerComponent)
      ],
      providers: [
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [{ selector: fromMainUsers.getMainUserActivationDialogLoading, value: false }]
        }),
        { provide: ReactiveSubject, useValue: { asObservable: of(false) } },
        { provide: DialogConfig, useValue: { data: { buttons: [], user: { id: 111 } } } }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUserDeactivateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('initializeSaveButton', () => {
    it('should set deactivateButton', () => {
      (component as any).deactivateButton = null;
      (component as any).config = { data: { buttons: [{ type: FooterButton.Deactivate, isDisabled: false }] } };

      (component as any).initializeSaveButton();

      expect((component as any).deactivateButton).toEqual({ type: FooterButton.Deactivate, isDisabled: false });
    });
  });

  describe('subscribeToLoading', () => {
    it('should call setLoading with loading', fakeAsync(() => {
      (component as any).setLoading = jasmine.createSpy();

      (component as any).subscribeToLoading();

      expect((component as any).setLoading).toHaveBeenCalledWith(false);
    }));
  });

  describe('subscribeToButtonClick', () => {
    it('should dispatch MainUserActivationDialogActions.deactivate', () => {
      (component as any).deactivateButtonClick$ = new Subject();
      component.user = { id: 10 } as any;
      component.expiryDateControl.setValue(new Date('10/10/20'));
      (component as any).user = { id: 10 };
      (component as any).config.data = { actionEmitter: { emit: {} } };

      (component as any).subscribeToButtonClick();
      (component as any).deactivateButtonClick$.next();

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainUserActivationDialogActions.deactivate({
          userId: 10,
          expiryDate: new Date('10/10/20'),
          actionEmitter: { emit: {} } as any
        })
      );
    });
  });

  describe('setLoading', () => {
    it('should set isLoading for deactivateButton as true', () => {
      (component as any).deactivateButton = { isLoading: false };

      (component as any).setLoading(true);

      expect((component as any).deactivateButton.isLoading).toBe(true);
    });
  });
});
