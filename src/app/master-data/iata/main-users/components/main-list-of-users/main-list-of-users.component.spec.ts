import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';

import { MainListOfUsersFilterFormatter } from '../../services/main-list-of-users-filter-formatter';
import { MainListOfUsersActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import * as fromMainListOfUsers from '../../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../../store/reducers/main-users.reducer';

import { MainListOfUsersComponent } from './main-list-of-users.component';
import { TranslatePipeMock } from '~app/test';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ListViewComponent } from '~app/shared/components/list-view';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import {
  GridTableComponent,
  InputComponent,
  PropertyComponent,
  PropertyListComponent,
  SelectComponent
} from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('MainListOfUsersComponent', () => {
  let component: MainListOfUsersComponent;
  let fixture: ComponentFixture<MainListOfUsersComponent>;
  let mockStore: MockStore<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        MainListOfUsersComponent,
        MockComponents(
          ListViewComponent,
          SelectComponent,
          InputComponent,
          DatepickerComponent,
          GridTableComponent,
          PropertyListComponent,
          PropertyComponent
        ),
        TranslatePipeMock
      ],
      providers: [
        FormBuilder,
        { provide: MainListOfUsersFilterFormatter, useValue: { formatMainUser: () => '' } },
        { provide: L10nTranslationService, useValue: { translate: () => '' } },
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [
            { selector: fromMainUsers.getListOfUsersLoading, value: false },
            { selector: fromMainUsers.getListOfUsersTotalElements, value: 15 },
            { selector: fromMainUsers.getListOfUsersData, value: [] },
            { selector: fromMainUsers.getListOfUsersQuery, value: defaultQuery },
            { selector: fromMainUsers.hasListOfUsersData, value: true }
          ]
        })
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainListOfUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onQueryChanged', () => {
    it('should dispatch MainListOfUsersActions.search', () => {
      component.onQueryChanged({ userCode: 'test-user-code' } as any);

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        MainListOfUsersActions.search({ query: { userCode: 'test-user-code' } } as any)
      );
    });
  });

  describe('onDownload', () => {
    it('should dispatch MainListOfUsersActions.download', () => {
      component.onDownload();

      expect(mockStore.dispatch).toHaveBeenCalledWith(MainListOfUsersActions.download());
    });
  });

  describe('initializeTotalItems', () => {
    it('should call translate with total number', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('Total items: 15');

      (component as any).initializeTotalItems();

      expect((component as any).translationService.translate).toHaveBeenCalledWith(
        'MENU.USERS_MAINTENANCE.MAIN_USERS.listOfUsers.totalItemsMessage',
        { total: 15 }
      );
    });

    it('should set totalItemsMessage', () => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('Total items: 15');
      (component as any).totalItemsMessage = 0;

      (component as any).initializeTotalItems();

      expect((component as any).totalItemsMessage).toBe('Total items: 15');
    });
  });

  describe('initializeDataQuery', () => {
    it('should dispatch MainListOfUsersActions.load', () => {
      (component as any).initializeDataQuery();

      expect(mockStore.dispatch).toHaveBeenCalledWith(MainListOfUsersActions.load());
    });
  });

  describe('buildForm', () => {
    it('should return form with corresponding value', () => {
      const expectedRes = {
        portalAccessEmail: null,
        level: null,
        name: null,
        userTemplate: null,
        registerDate: null,
        expiryDate: null,
        status: null
      };

      const form = (component as any).buildForm();

      expect(form.value).toEqual(expectedRes);
    });
  });

  describe('initColumns', () => {
    it('should set columns', () => {
      component.columns = null;

      (component as any).initColumns();

      expect(component.columns).not.toBeNull();
    });
  });

  describe('getDropdownOptions', () => {
    beforeEach(() => {
      (component as any).translationService.translate = jasmine
        .createSpy()
        .and.callFake(key => (key === 'common.Active' ? 'test-active' : 'test-inactive'));
    });

    it('should return mapped options', () => {
      const expectedRes = [
        { value: 'ACTIVE', label: 'test-active' },
        { value: 'INACTIVE', label: 'test-inactive' }
      ];
      const res = (component as any).getDropdownOptions(ActiveInactiveInUpperCase, 'common');

      expect(res).toEqual(expectedRes);
    });

    it('should call translationService.translate', () => {
      (component as any).getDropdownOptions(ActiveInactiveInUpperCase, 'common');

      expect((component as any).translationService.translate).toHaveBeenCalledTimes(2);
      expect((component as any).translationService.translate.calls.first().args).toEqual(['common.Active']);
      expect((component as any).translationService.translate.calls.mostRecent().args).toEqual(['common.Inactive']);
    });
  });
});
