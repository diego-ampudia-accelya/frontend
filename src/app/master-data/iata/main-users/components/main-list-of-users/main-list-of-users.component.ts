import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';

import { MainListOfUsersFilter } from '../../models/main-list-of-users.model';
import { MainListOfUsersFilterFormatter } from '../../services/main-list-of-users-filter-formatter';
import { MainListOfUsersActions } from '../../store/actions';
import * as fromMainUsers from '../../store/reducers';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { FormUtil } from '~app/shared/helpers';
import { ActiveInactiveInUpperCase, DropdownOption, GridColumn } from '~app/shared/models';
import { UserTemplate } from '~app/shared/models/user-template.model';

@Component({
  selector: 'bspl-main-list-of-users',
  templateUrl: './main-list-of-users.component.html'
})
export class MainListOfUsersComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public totalItemsMessage: string;

  public loading$ = this.store.pipe(select(fromMainUsers.getListOfUsersLoading));
  public data$ = this.store.pipe(select(fromMainUsers.getListOfUsersData));
  public query$ = this.store.pipe(select(fromMainUsers.getListOfUsersQuery));
  public hasData$ = this.store.pipe(select(fromMainUsers.hasListOfUsersData));

  public levelOptions: DropdownOption<boolean>[] = [true, false].map(value => ({
    value,
    label: this.displayFormatter.formatMainUser(`options.${value}`)
  }));
  public userTemplateOptions: DropdownOption<typeof UserTemplate>[] = this.getDropdownOptions(
    UserTemplate,
    'MENU.USERS_MAINTENANCE.MAIN_USERS.filters.userTemplate.options'
  );
  public statusOptions: DropdownOption<typeof ActiveInactiveInUpperCase>[] = this.getDropdownOptions(
    ActiveInactiveInUpperCase,
    'common'
  );

  private formFactory: FormUtil;

  constructor(
    public displayFormatter: MainListOfUsersFilterFormatter,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private store: Store<AppState>
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.initializeDataQuery();
    this.initializeTotalItems();
    this.initColumns();
  }

  public onQueryChanged(query: DataQuery<MainListOfUsersFilter>): void {
    this.store.dispatch(MainListOfUsersActions.search({ query }));
  }

  public onDownload(): void {
    this.store.dispatch(MainListOfUsersActions.download());
  }

  private initializeTotalItems(): void {
    this.store.pipe(select(fromMainUsers.getListOfUsersTotalElements)).subscribe(total => {
      this.totalItemsMessage = this.translationService.translate(
        'MENU.USERS_MAINTENANCE.MAIN_USERS.listOfUsers.totalItemsMessage',
        { total }
      );
    });
  }

  private initializeDataQuery(): void {
    this.store.dispatch(MainListOfUsersActions.load());
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<MainListOfUsersFilter>({
      portalAccessEmail: [],
      level: [],
      name: [],
      userTemplate: [],
      registerDate: [],
      expiryDate: [],
      status: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.userType',
        prop: 'userType'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.userCode',
        prop: 'userCode'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.userTemplate',
        prop: 'template',
        pipe: {
          transform: value =>
            value
              ? this.translationService.translate(
                  `MENU.USERS_MAINTENANCE.MAIN_USERS.filters.userTemplate.options.${value}`
                )
              : value
        }
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.portalAccessEmail',
        prop: 'portalEmail'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.listOfUsers.level.label',
        prop: 'mainUser',
        pipe: {
          transform: value => this.displayFormatter.formatMainUser(`options.${value}`)
        }
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.name',
        prop: 'name'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.registerDate',
        prop: 'registerDate',
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.expiryDate',
        prop: 'expiryDate',
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.numberOfUsers',
        prop: 'numberOfUsers'
      },
      {
        name: 'MENU.USERS_MAINTENANCE.MAIN_USERS.columns.status',
        prop: 'status',
        pipe: {
          transform: value => (value ? this.translationService.translate(`common.${value}`) : value)
        }
      }
    ];
  }

  private getDropdownOptions<T>(object: T, translationKey: string): DropdownOption<T>[] {
    return Object.keys(object).map(key => ({
      value: object[key],
      label: this.translationService.translate(`${translationKey}.${key.toString()}`)
    }));
  }
}
