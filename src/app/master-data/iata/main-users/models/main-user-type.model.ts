/* eslint-disable @typescript-eslint/naming-convention */
export enum MainUserType {
  Airline = 'Airline',
  Agent = 'Agent',
  IATA = 'IATA',
  DPC = 'DPC',
  GDS = 'GDS',
  ThirdParty = 'Third Party',
  AgentGroup = 'Agent Group'
}

export enum MainUserTypeForCreate {
  Agent = 'Agent',
  Airline = 'Airline',
  GDS = 'GDS',
  ThirdParty = 'Third Party'
}
