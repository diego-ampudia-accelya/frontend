/* eslint-disable @typescript-eslint/naming-convention */
import { UserPermission, UserPermissionDto } from '~app/shared/models/user-permission.model';

export enum MainUserTemplateForPermissisons {
  Basic = 'BASIC',
  Enhanced = 'ENHANCED'
}

export type MainUserPermission = UserPermission<MainUserTemplateForPermissisons>;
export type MainUserPermissionDto = UserPermissionDto<MainUserTemplateForPermissisons>;
