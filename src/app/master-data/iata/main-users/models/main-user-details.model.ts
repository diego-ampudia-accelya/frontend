export interface MainUserDetails {
  portalEmail: string;
  name: string;
  organization: string;
  telephone: string;
  country: string;
  city: string;
  address: string;
  postCode: string;
}
