import { MainUserBE, MainUsersFilter, MainUsersFilterBE } from './main-users.model';

export interface UserFromMainListOfUsersBE extends MainUserBE {
  mainUser: boolean;
}

export type MainListOfUsersFilter = Omit<MainUsersFilter, 'userType' | 'userCode'> & {
  level: boolean;
};

export type MainListOfUsersFilterBE = Omit<MainUsersFilterBE, 'userType' | 'userCode'> & {
  mainUser: boolean;
};
