import { MainUserTypeForCreate } from './main-user-type.model';
import { UserTemplate } from '~app/shared/models/user-template.model';

export interface MainUserCreateModelBE {
  portalEmail: string;
  name: string;
  userCode: string;
  template?: UserTemplate; // only for Agent
  userType: MainUserTypeForCreate;
  organization: string;
  telephone: string;
  country: string;
  city: string;
  address: string;
  postCode: string;
}

export type MainUserCreateModelOmitted = Omit<MainUserCreateModelBE, 'userType' | 'userCode'>;
