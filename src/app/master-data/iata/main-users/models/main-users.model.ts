import { MainUserType } from './main-user-type.model';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { UserTemplate } from '~app/shared/models/user-template.model';

export interface MainUserBE {
  id: number;
  portalEmail: string;
  level: number;
  name: string;
  userCode: string;
  template?: UserTemplate;
  registerDate: Date;
  expiryDate: Date;
  numberOfUsers: number;
  status: ActiveInactiveInUpperCase;
  userType: MainUserType;
  lean: boolean;
  contactInfo: {
    organization: string;
    telephone: string;
    country: string;
    city: string;
    address: string;
    postCode: string;
    email: string;
  };
}

export interface MainUsersFilter {
  userType: MainUserType[];
  userCode: string;
  userTemplate: UserTemplate;
  portalAccessEmail: string;
  name: string;
  registerDate: Date[];
  expiryDate: Date[];
  status: ActiveInactiveInUpperCase;
}

export interface MainUsersFilterBE {
  userType: MainUserType[];
  userCode: string;
  template: UserTemplate;
  portalEmail: string;
  name: string;
  fromRegisterDate: string;
  toRegisterDate: string;
  fromExpiryDate: string;
  toExpiryDate: string;
  status: ActiveInactiveInUpperCase;
}
