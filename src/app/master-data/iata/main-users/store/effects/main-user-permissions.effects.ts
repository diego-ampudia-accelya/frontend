import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { MainUserPermissionsChangesService } from '../../services/main-user-permissions-changes.service';
import { MainUsersService } from '../../services/main-users.service';
import { MainUserPermissionsActions } from '../actions';
import * as fromMainUsers from '../reducers';
import { mapSelectionListToPermissions } from '../reducers/main-user-permissions.reducer';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { AppState } from '~app/reducers';

@Injectable()
export class MainUserPermissionsEffects {
  requestMainUserPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserPermissionsActions.requestPermissions),
      withLatestFrom(this.store.pipe(select(fromMainUsers.getMainUser))),
      switchMap(([_, user]) =>
        this.dataService.getPermissionsByUserId(user.id).pipe(
          map(permissions =>
            MainUserPermissionsActions.requestPermissionsSuccess({
              permissions,
              user
            })
          )
        )
      ),
      rethrowError(() => MainUserPermissionsActions.requestPermissionsError())
    )
  );

  openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserPermissionsActions.openApplyChanges),
      switchMap(() => this.changesService.applyUserPermissions$)
    )
  );

  applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserPermissionsActions.applyChanges),
      withLatestFrom(
        this.store.pipe(select(fromMainUsers.getModifiedPermissions)),
        this.store.pipe(select(fromMainUsers.getMainUser))
      ),
      switchMap(([_, modified, user]) =>
        this.dataService
          .updatePermissionsByUserId(user.id, mapSelectionListToPermissions(modified))
          .pipe(map(permissions => MainUserPermissionsActions.applyChangesSuccess({ permissions, user })))
      ),
      rethrowError(() => MainUserPermissionsActions.applyChangesError())
    )
  );

  $applyChangesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUserPermissionsActions.applyChangesSuccess),
        withLatestFrom(this.store.pipe(select(fromMainUsers.getProfileMainUserPortalEmail))),
        tap(([_, email]) => {
          const message = this.translationService.translate('mainUsers.permissions.changesDialog.editUserSuccess', {
            email: email ? `<span class="link">${email}</span>` : ''
          });
          this.notificationService.showSuccess(message);
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private dataService: MainUsersService,
    private changesService: MainUserPermissionsChangesService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}
}
