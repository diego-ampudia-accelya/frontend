import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { MainUsersCreateDialogComponent } from '../../components/main-users-create-dialog/main-users-create-dialog.component';
import { MainUsersService } from '../../services/main-users.service';
import { MainUserCreateDialogActions, MainUsersActions } from '../actions';
import { DialogService, FooterButton } from '~app/shared/components';
import { NotificationService } from '~app/shared/services/notification.service';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class MainUserCreateDialogEffects {
  $openMainUserCreateDialog = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUserCreateDialogActions.openMainUserCreateDialog),
        switchMap(() => this.openCreateDialog())
      ),
    { dispatch: false }
  );

  $createMainUser = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserCreateDialogActions.createMainUser),
      switchMap(({ user }) => this.dataService.create(user)),
      map(user => MainUserCreateDialogActions.createMainUserSuccess({ user })),
      rethrowError(error => MainUserCreateDialogActions.createMainUserError({ error }))
    )
  );

  $createMainUserSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserCreateDialogActions.createMainUserSuccess),
      tap(({ user }) => {
        this.dialogService.close();

        this.notificationService.showSuccess(
          this.translationService.translate('MENU.USERS_MAINTENANCE.MAIN_USERS.createDialog.successMessage', {
            email: `<span class="link">${user.portalEmail}</span>`
          })
        );
      }),
      map(() => MainUsersActions.load())
    )
  );

  constructor(
    private actions$: Actions,
    private dataService: MainUsersService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService
  ) {}

  private openCreateDialog(): Observable<any> {
    return this.dialogService.open(MainUsersCreateDialogComponent, {
      data: {
        title: this.translationService.translate('MENU.USERS_MAINTENANCE.MAIN_USERS.createDialog.title'),
        hasCancelButton: true,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Create }]
      }
    });
  }
}
