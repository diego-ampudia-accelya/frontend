import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { MainUserDeactivateDialogComponent } from '../../components/main-user-deactivate-dialog/main-user-deactivate-dialog.component';
import { MainUserReactivateDialogComponent } from '../../components/main-user-reactivate-dialog/main-user-reactivate-dialog.component';
import { MainUserBE } from '../../models/main-users.model';
import { MainUsersService } from '../../services/main-users.service';
import { MainUserActivationDialogActions } from '../actions';
import { ButtonDesign, DialogService, FooterButton } from '~app/shared/components';
import { dateFilterTagMapper } from '~app/shared/helpers';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class MainUserActivationDialogEffects {
  // deactiavte
  $openDeactivationDialog = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUserActivationDialogActions.openDeactivationDialog),
        tap(({ user, actionEmitter }) => this.openMainUserStatusDialog(user, false, actionEmitter))
      ),
    { dispatch: false }
  );

  $deactivate = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserActivationDialogActions.deactivate),
      switchMap(({ userId, expiryDate, actionEmitter }) =>
        this.dataService
          .deactivateUser(userId, expiryDate)
          .pipe(map(() => MainUserActivationDialogActions.deactivateSuccess({ expiryDate, actionEmitter })))
      ),
      rethrowError(err => MainUserActivationDialogActions.deactivateError(err))
    )
  );

  $deactivateSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUserActivationDialogActions.deactivateSuccess),
        tap(({ expiryDate, actionEmitter }) => {
          const msg = this.translationService.translate(
            'MENU.USERS_MAINTENANCE.MAIN_USERS.activationDialogs.deactivateSuccess',
            {
              expiryDate: dateFilterTagMapper(new Date(expiryDate))
            }
          );

          this.notificationService.showSuccess(msg);
          this.dialogService.close();

          if (actionEmitter) {
            actionEmitter().next();
          }
        })
      ),
    { dispatch: false }
  );

  // reactivate
  $openReactivationDialog = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUserActivationDialogActions.openReactivationDialog),
        tap(({ user, actionEmitter }) => this.openMainUserStatusDialog(user, true, actionEmitter))
      ),
    { dispatch: false }
  );

  $reactivate = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserActivationDialogActions.reactivate),
      switchMap(({ userId, portalEmail, actionEmitter }) =>
        this.dataService
          .reactivateUser(userId, portalEmail)
          .pipe(map(() => MainUserActivationDialogActions.reactivateSuccess({ actionEmitter })))
      ),

      rethrowError(err => MainUserActivationDialogActions.reactivateError(err))
    )
  );

  $reactivateSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUserActivationDialogActions.reactivateSuccess),
        tap(({ actionEmitter }) => {
          const msg = this.translationService.translate(
            'MENU.USERS_MAINTENANCE.MAIN_USERS.activationDialogs.reactivateSuccess'
          );

          this.notificationService.showSuccess(msg);
          this.dialogService.close();

          if (actionEmitter) {
            actionEmitter().next();
          }
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: MainUsersService,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  private openMainUserStatusDialog(user: MainUserBE, isReactivation: boolean, actionEmitter: () => Subject<any>) {
    let title = '';
    let footerButtonsType: { type: FooterButton; buttonDesign?: ButtonDesign }[];
    let component: any;

    if (!isReactivation && user.status === ActiveInactiveInUpperCase.Active) {
      title = 'MENU.USERS_MAINTENANCE.MAIN_USERS.activationDialogs.deactivateUserTitle';
      footerButtonsType = [{ type: FooterButton.Deactivate }];
      component = MainUserDeactivateDialogComponent;
    }

    if (isReactivation && user.status === ActiveInactiveInUpperCase.Inactive) {
      title = 'MENU.USERS_MAINTENANCE.MAIN_USERS.activationDialogs.reactivateUserTitle';
      footerButtonsType = [{ type: FooterButton.Reactivate }];
      component = MainUserReactivateDialogComponent;
    }

    return this.dialogService.open(component, {
      data: {
        title,
        isClosable: true,
        footerButtonsType,
        user,
        actionEmitter
      }
    });
  }
}
