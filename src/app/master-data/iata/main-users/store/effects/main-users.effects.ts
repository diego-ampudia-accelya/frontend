import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';

import { MainUsersService } from '../../services/main-users.service';
import { MainUsersActions } from '../actions';
import * as fromMainUsers from '../reducers';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class MainUsersEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUsersActions.load),
      withLatestFrom(this.store.pipe(select(fromMainUsers.getQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(data =>
            MainUsersActions.loadSuccess({
              data
            })
          ),
          rethrowError(() => MainUsersActions.loadError())
        )
      )
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUsersActions.search),
      switchMap(() => of(MainUsersActions.searchConfirmed()))
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUsersActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromMainUsers.getQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(result =>
            MainUsersActions.searchSuccess({
              data: result
            })
          ),
          rethrowError(() => MainUsersActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUsersActions.download),
      switchMap(() => of(MainUsersActions.openDownload()))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUsersActions.openDownload),
        withLatestFrom(this.store.pipe(select(fromMainUsers.getDownloadQuery))),
        switchMap(([_, downloadQuery]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: MainUsersService,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {}
}
