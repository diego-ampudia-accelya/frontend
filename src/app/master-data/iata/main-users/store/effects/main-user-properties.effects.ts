import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';
import { map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

import { MainUserDetails } from '../../models/main-user-details.model';
import { MainUserPropertiesChangesService } from '../../services/main-user-properties-changes.service';
import { MainUsersService } from '../../services/main-users.service';
import { MainUserPropertiesActions } from '../actions';
import * as fromMainUsers from '../reducers';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { AppState } from '~app/reducers';

@Injectable()
export class MainUserPropertiesEffects {
  $requestUser = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserPropertiesActions.requestUser),
      switchMap(({ id }) => this.dataService.getOne(id)),
      map(user => MainUserPropertiesActions.requestUserSuccess({ user })),
      rethrowError(error => MainUserPropertiesActions.requestUserError({ error }))
    )
  );

  $openPropertiesChangesDialog = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserPropertiesActions.applyPropertyChanges),
      switchMap(() => this.getMainUserUpdateData()),
      map(data =>
        data
          ? MainUserPropertiesActions.updateMainUserProperties({ data })
          : MainUserPropertiesActions.cancelPropertiesChanges()
      )
    )
  );

  $updateMainUserProperties = createEffect(() =>
    this.actions$.pipe(
      ofType(MainUserPropertiesActions.updateMainUserProperties),
      withLatestFrom(this.store.pipe(select(fromMainUsers.getMainUser))),
      switchMap(([{ data }, { id }]) => this.dataService.update(id, data)),
      map(user => MainUserPropertiesActions.updateMainUserPropertiesSuccess({ user })),
      rethrowError(error => MainUserPropertiesActions.updateMainUserPropertiesError({ error }))
    )
  );

  $updateMainUserPropertiesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainUserPropertiesActions.updateMainUserPropertiesSuccess),
        tap(({ user }) => {
          const successMessage = this.translationService.translate(
            'mainUsers.userProperties.changesDialog.changePropertiesSuccess',
            {
              email: `<span class="link">${user.portalEmail}</span>`
            }
          );

          this.notificationService.showSuccess(successMessage);
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: MainUsersService,
    private store: Store<AppState>,
    private mainUserPropertiesChangesService: MainUserPropertiesChangesService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  private getMainUserUpdateData(): Observable<MainUserDetails> {
    return this.mainUserPropertiesChangesService
      .confirmApplyPropertiesChanges()
      .pipe(
        switchMap(confirmed =>
          confirmed ? this.store.pipe(select(fromMainUsers.getPropertiesFormValue), take(1)) : of(null)
        )
      );
  }
}
