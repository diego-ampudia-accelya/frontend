import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { MainListOfUsersService } from '../../services/main-list-of-users.service';
import { MainListOfUsersActions } from '../actions';
import * as fromMainUsers from '../reducers';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Injectable()
export class MainListOfUsersEffects {
  public load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainListOfUsersActions.load),
      switchMap(() => this.getUsersQueryAndMainUser()),
      switchMap(([query, user]) =>
        this.dataService.find(user.id, query).pipe(
          map(data =>
            MainListOfUsersActions.loadSuccess({
              data
            })
          ),
          rethrowError(() => MainListOfUsersActions.loadError())
        )
      )
    )
  );

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainListOfUsersActions.search),
      switchMap(() => of(MainListOfUsersActions.searchConfirmed()))
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainListOfUsersActions.searchConfirmed),
      switchMap(() => this.getUsersQueryAndMainUser()),
      switchMap(([query, user]) =>
        this.dataService.find(user.id, query).pipe(
          map(result =>
            MainListOfUsersActions.searchSuccess({
              data: result
            })
          ),
          rethrowError(() => MainListOfUsersActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainListOfUsersActions.download),
      switchMap(() => of(MainListOfUsersActions.openDownload()))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MainListOfUsersActions.openDownload),
        switchMap(() =>
          combineLatest([
            this.store.pipe(select(fromMainUsers.getListOfUsersQuery)),
            this.store.pipe(select(fromMainUsers.getMainUser))
          ]).pipe(first())
        ),
        switchMap(([downloadQuery, user]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery,
              downloadBody: user.id
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private dataService: MainListOfUsersService,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {}

  private getUsersQueryAndMainUser() {
    return combineLatest([
      this.store.pipe(select(fromMainUsers.getListOfUsersQuery)),
      this.store.pipe(select(fromMainUsers.getMainUser))
    ]).pipe(first());
  }
}
