import { Action, createReducer, on } from '@ngrx/store';
import { chain } from 'lodash';

import { MainUserType } from '../../models/main-user-type.model';
import { MainUserPermission, MainUserTemplateForPermissisons } from '../../models/main-users-permissions.model';
import { MainUserPermissionsActions } from '../actions';
import { ChangeModel } from '~app/shared/components';
import { SelectionList, SelectionListItem } from '~app/shared/components/selection-list/selection-list.models';
import { UserTemplate } from '~app/shared/models/user-template.model';

export const mainUserPermissionsKey = 'mainUserPermissions';

export interface State {
  userId: number;
  original: SelectionList[];
  modified: SelectionList[];
  loading: boolean;
  bspId: number;
}

export const initialState: State = {
  userId: null,
  original: [],
  modified: [],
  loading: false,
  bspId: null
};

const mainUserPermissionsReducer = createReducer(
  initialState,
  on(MainUserPermissionsActions.requestPermissions, () => ({ ...initialState, loading: true })),
  on(
    MainUserPermissionsActions.requestPermissionsSuccess,
    MainUserPermissionsActions.applyChangesSuccess,
    (state, { permissions, user }) => {
      const original = mapPermissionsToSelectionList(permissions, user.template, user.userType);

      return {
        ...state,
        original,
        userId: user.id,
        modified: original,
        loading: false
      };
    }
  ),
  on(MainUserPermissionsActions.requestPermissionsError, () => ({
    ...initialState,
    hasError: true,
    loading: false
  })),
  on(MainUserPermissionsActions.change, (state, { modifications }) => ({
    ...state,
    modified: modifications
  })),
  on(MainUserPermissionsActions.applyChanges, state => ({ ...state, loading: true })),
  on(MainUserPermissionsActions.applyChangesError, state => ({ ...state, loading: false })),
  on(MainUserPermissionsActions.discardChanges, state => ({
    ...state,
    original: [...state.original],
    modified: state.original
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return mainUserPermissionsReducer(state, action);
}

export const getOriginal = (state: State) => state.original;

export const getModified = (state: State) => state.modified;

export const getLoading = (state: State) => state.loading;

export const getBspId = (state: State) => state.bspId;

export const hasChanges = (state: State) => state.modified?.some(permissionCategory => permissionCategory.hasChanged);

export function getChanges(state: State): ChangeModel[] {
  return state.modified
    .filter(category => category.hasChanged)
    .map(category => ({ ...category, items: category.items.filter(item => item.hasChanged) }))
    .reduce((changes, item) => {
      item.items.forEach((listItem: SelectionListItem) => {
        changes.push({
          group: item.name,
          name: listItem.name,
          value: String(listItem.checked),
          originalValue: listItem.checked
        });
      });

      return changes;
    }, []);
}

export function mapSelectionListToPermissions(
  selectionListItems: SelectionList[],
  multiPermissionsEnabled = true
): MainUserPermission[] {
  return chain(selectionListItems)
    .map(category => category.items)
    .flatten()
    .map(item => ({ ...item.value, enabled: multiPermissionsEnabled ? item.checked : !item.checked }))
    .value();
}

function mapPermissionsToSelectionList(
  permissions: MainUserPermission[],
  userTemplate: UserTemplate,
  userType: MainUserType
): SelectionList[] {
  const shouldCheckDisabledState =
    (userType === MainUserType.Agent || userType === MainUserType.Airline) && userTemplate === UserTemplate.Efficient;

  return chain(permissions)
    .map(permission => {
      // if user is streamlined or permission's template is basic, then the checkboxes are enabled
      const disabled = shouldCheckDisabledState && permission.template === MainUserTemplateForPermissisons.Enhanced;

      return {
        name: permission.name,
        checked: permission.enabled,
        value: permission,
        hasChanged: false,
        disabled,
        tooltip: disabled ? 'mainUsers.permissions.disabledTooltip' : ''
      };
    })
    .groupBy('value.category.name')
    .map((items, name) => {
      const disabled = items.every(item => item.disabled);

      return {
        name,
        disabled,
        items,
        tooltip: disabled ? 'mainUsers.permissions.disabledTooltip' : ''
      };
    })
    .value();
}
