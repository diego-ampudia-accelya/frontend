import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromMainListOfUsers from './main-list-of-users.reducer';
import * as fromMainUserActivationDialog from './main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from './main-user-permissions.reducer';
import * as fromMainUserProperties from './main-user-properties.reducer';
import * as fromCreateMainUser from './main-users-create-dialog.reducer';
import * as fromMainUsers from './main-users.reducer';
import { AppState } from '~app/reducers';

export const mainUsersFeatureKey = 'mainUsers';

export interface State extends AppState {
  [mainUsersFeatureKey]: MainUsersState;
}

export interface MainUsersState {
  [fromMainUsers.key]: fromMainUsers.State;
  [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: fromMainUserActivationDialog.State;
  [fromCreateMainUser.createMainUserFeatureKey]: fromCreateMainUser.State;
  [fromMainUserProperties.mainUserPropertiesFeatureKey]: fromMainUserProperties.State;
  [fromMainUserPermissions.mainUserPermissionsKey]: fromMainUserPermissions.State;
  [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.State;
}

export function reducers(state: MainUsersState | undefined, action: Action) {
  return combineReducers({
    [fromMainUsers.key]: fromMainUsers.reducer,
    [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: fromMainUserActivationDialog.reducer,
    [fromCreateMainUser.createMainUserFeatureKey]: fromCreateMainUser.reducer,
    [fromMainUserProperties.mainUserPropertiesFeatureKey]: fromMainUserProperties.reducer,
    [fromMainUserPermissions.mainUserPermissionsKey]: fromMainUserPermissions.reducer,
    [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
  })(state, action);
}

export const getMainUsers = createFeatureSelector<State, MainUsersState>(mainUsersFeatureKey);

export const getList = createSelector(getMainUsers, state => state[fromMainUsers.key]);

export const getLoading = createSelector(getList, fromMainUsers.getLoading);

export const getData = createSelector(getList, fromMainUsers.getData);

export const hasData = createSelector(getData, data => data && data.length > 0);

export const getQuery = createSelector(getList, fromMainUsers.getQuery);

export const getDownloadQuery = createSelector(
  getQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);

// Deactivete / Reactive
export const getMainUsersActivationDialog = createSelector(
  getMainUsers,
  state => state[fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]
);

export const getMainUserActivationDialogLoading = createSelector(
  getMainUsersActivationDialog,
  fromMainUserActivationDialog.getLoading
);

// Create
export const getMainUserCreateDialogState = createSelector(
  getMainUsers,
  state => state[fromCreateMainUser.createMainUserFeatureKey]
);

export const getCreateDialogLoading = createSelector(getMainUserCreateDialogState, fromCreateMainUser.getLoading);

export const getCreateError = createSelector(getMainUserCreateDialogState, fromCreateMainUser.getRequestError);

// User Properties
export const getMainUserProperties = createSelector(
  getMainUsers,
  state => state[fromMainUserProperties.mainUserPropertiesFeatureKey]
);
export const getMainUser = createSelector(getMainUserProperties, fromMainUserProperties.getUser);
export const getUserPropertiesLoading = createSelector(getMainUserProperties, fromMainUserProperties.getLoading);
export const getUserPropertiesRequestError = createSelector(
  getMainUserProperties,
  fromMainUserProperties.getUserPropertiesRequestError
);
export const getProfileMainUserPortalEmail = createSelector(
  getMainUserProperties,
  fromMainUserProperties.getUserProfileEmail
);

export const canApplyPropertyChanges = createSelector(
  getMainUserProperties,
  fromMainUserProperties.canApplyPropertyChanges
);
export const getPropertiesFormValue = createSelector(getMainUserProperties, state => state.propertiesFormValue);
export const getPropertiesChanges = createSelector(getMainUserProperties, fromMainUserProperties.getPropertiesChanges);
export const hasPropertiesValidationErrors = createSelector(
  getMainUserProperties,
  fromMainUserProperties.hasPropertiesValidationErrors
);
export const getUserUpdatePropertiesErrors = createSelector(
  getMainUserProperties,
  fromMainUserProperties.getUserUpdatePropertiesError
);
export const hasMainUserPropertiesChanges = createSelector(getMainUserProperties, fromMainUserProperties.hasChanges);

// Permissions
export const getMainUserPermissionsState = createSelector(
  getMainUsers,
  state => state[fromMainUserPermissions.mainUserPermissionsKey]
);
export const getMainUserPermissionsLoading = createSelector(
  getMainUserPermissionsState,
  fromMainUserPermissions.getLoading
);
export const getOriginalMainUserPermissions = createSelector(
  getMainUserPermissionsState,
  fromMainUserPermissions.getOriginal
);
export const canApplyPermissionsChanges = createSelector(
  getMainUserPermissionsState,
  fromMainUserPermissions.hasChanges
);
export const getModifiedPermissions = createSelector(getMainUserPermissionsState, fromMainUserPermissions.getModified);
export const getPermissionsChanges = createSelector(getMainUserPermissionsState, fromMainUserPermissions.getChanges);

// List of Users
export const getListOfUsers = createSelector(getMainUsers, state => state[fromMainListOfUsers.mainListOfUsersKey]);
export const getListOfUsersLoading = createSelector(getListOfUsers, fromMainListOfUsers.getLoading);
export const getListOfUsersData = createSelector(getListOfUsers, fromMainListOfUsers.getData);
export const hasListOfUsersData = createSelector(getListOfUsersData, data => data && data.length > 0);
export const getListOfUsersTotalElements = createSelector(getListOfUsers, fromMainListOfUsers.getDataTotalElements);
export const getListOfUsersQuery = createSelector(getListOfUsers, fromMainListOfUsers.getQuery);
export const getListOfUsersDownloadQuery = createSelector(
  getQuery,
  query => query && { filterBy: query.filterBy, sortBy: query.sortBy }
);
