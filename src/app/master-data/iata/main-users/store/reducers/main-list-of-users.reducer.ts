import { Action, createReducer, on } from '@ngrx/store';
import { cloneDeep } from 'lodash';

import { MainListOfUsersFilter, UserFromMainListOfUsersBE } from '../../models/main-list-of-users.model';
import { MainListOfUsersActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

export const mainListOfUsersKey = 'mainListOfUsers';

export interface State {
  query: DataQuery<MainListOfUsersFilter>;
  previousQuery: DataQuery<MainListOfUsersFilter>;
  data: UserFromMainListOfUsersBE[];
  loading: boolean;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false
};

const loadData = (state: State, data: PagedData<UserFromMainListOfUsersBE>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (storedQuery: DataQuery<MainListOfUsersFilter>): DataQuery<MainListOfUsersFilter> => {
  const query: DataQuery<MainListOfUsersFilter> = storedQuery || cloneDeep(defaultQuery);

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = {
    ...query.filterBy
  };

  return initialQuery;
};

export const mainListOfUsersReducer = createReducer(
  initialState,
  on(MainListOfUsersActions.load, state => ({
    ...state,
    loading: true,
    query: determineInitialQuery(state.query)
  })),
  on(MainListOfUsersActions.loadSuccess, (state, { data }) => ({
    ...loadData(state, data)
  })),
  on(MainListOfUsersActions.search, (state, { query = state.query }) => ({
    ...state,
    query: {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    },
    previousQuery: state.query
  })),
  on(MainListOfUsersActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(MainListOfUsersActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(MainListOfUsersActions.searchError, MainListOfUsersActions.loadError, state => ({
    ...state,
    loading: false,
    data: []
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return mainListOfUsersReducer(state, action);
}

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getDataTotalElements = (state: State) => state.query.paginateBy.totalElements;

export const getQuery = (state: State) => state.query;
