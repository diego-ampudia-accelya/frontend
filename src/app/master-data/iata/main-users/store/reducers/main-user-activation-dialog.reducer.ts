import { Action, createReducer, on } from '@ngrx/store';

import { MainUserActivationDialogActions } from '../actions';

export const mainUserActivationDialogFeatureKey = 'mainUserActivationDialog';

export interface State {
  loading: boolean;
}

export const initialState: State = {
  loading: false
};

const mainUserACtivationDialogReducer = createReducer(
  initialState,
  on(
    MainUserActivationDialogActions.openDeactivationDialog,
    MainUserActivationDialogActions.openReactivationDialog,
    () => initialState
  ),
  on(MainUserActivationDialogActions.deactivate, MainUserActivationDialogActions.reactivate, state => ({
    ...state,
    loading: true
  })),
  on(
    MainUserActivationDialogActions.deactivateSuccess,
    MainUserActivationDialogActions.deactivateError,
    MainUserActivationDialogActions.reactivateSuccess,
    MainUserActivationDialogActions.reactivateError,
    state => ({
      ...state,
      loading: false
    })
  )
);

export function reducer(state: State | undefined, action: Action) {
  return mainUserACtivationDialogReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;
