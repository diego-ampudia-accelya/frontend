import { createReducer, on } from '@ngrx/store';
import isEqual from 'lodash/isEqual';

import { toMainUserUpdateModel } from '../../helpers/user.helper';
import { MainUserDetails } from '../../models/main-user-details.model';
import { MainUserBE } from '../../models/main-users.model';
import { MainUserPropertiesActions } from '../actions';
import { ServerError } from '~app/shared/errors';
import { ChangeModel } from '~app/shared/components';

export const mainUserPropertiesFeatureKey = 'mainUserProperties';

const propertyCategories = {
  portalEmail: 'contactDetails',
  name: 'contactDetails',
  organization: 'contactDetails',
  telephone: 'contactDetails',
  country: 'addressDetails',
  city: 'addressDetails',
  address: 'addressDetails',
  postCode: 'addressDetails'
};

export interface State {
  loading: boolean;
  user: MainUserBE;
  mainUserRequestError: ServerError;
  propertiesFormValue: MainUserDetails;
  hasPropertyValidationErrors: boolean;
  mainUserUpdatePropertiesError: ServerError;
}

export const initialState: State = {
  loading: false,
  user: null,
  mainUserRequestError: null,
  propertiesFormValue: null,
  hasPropertyValidationErrors: false,
  mainUserUpdatePropertiesError: null
};

export const reducer = createReducer(
  initialState,
  on(MainUserPropertiesActions.requestUser, state => ({
    ...state,
    user: null,
    loading: true
  })),
  on(MainUserPropertiesActions.requestUserSuccess, (state, { user }) => ({
    ...state,
    user,
    propertiesFormValue: toMainUserUpdateModel(user),
    mainUserRequestError: null,
    loading: false
  })),
  on(MainUserPropertiesActions.requestUserError, (state, { error }) => ({
    ...state,
    mainUserRequestError: error,
    loading: false
  })),
  on(MainUserPropertiesActions.propertiesFormValueChange, (state, { isFormValid, formValue: propertiesFormValue }) => ({
    ...state,
    propertiesFormValue,
    hasPropertyValidationErrors: !isFormValid
  })),
  on(MainUserPropertiesActions.updateMainUserProperties, state => ({
    ...state,
    loading: true
  })),
  on(MainUserPropertiesActions.updateMainUserPropertiesSuccess, (state, { user }) => ({
    ...state,
    loading: false,
    mainUserUpdatePropertiesError: null,
    user
  })),
  on(MainUserPropertiesActions.updateMainUserPropertiesError, (state, { error }) => ({
    ...state,
    loading: false,
    mainUserUpdatePropertiesError: error
  }))
);

export const getPropertiesChanges = (state: State): ChangeModel[] => {
  const mainUserDetails = toMainUserUpdateModel(state.user);
  const { propertiesFormValue } = state;

  return Object.entries(propertiesFormValue || {}).reduce((changes, [name, value]) => {
    const originalValue = mainUserDetails[name];

    if (value !== originalValue) {
      changes = [
        ...changes,
        {
          group: propertyCategories[name],
          name,
          value
        }
      ];
    }

    return changes;
  }, []);
};

export const hasChanges = (state: State): boolean => {
  const mainUserDetails = toMainUserUpdateModel(state.user);
  const { propertiesFormValue } = state;

  return propertiesFormValue && !isEqual(mainUserDetails, propertiesFormValue);
};

export const getLoading = (state: State): boolean => state.loading;
export const getUser = (state: State): MainUserBE => state.user;
export const getUserProfileEmail = (state: State): string => state.user?.portalEmail;
export const getUserPropertiesRequestError = (state: State): ServerError => state.mainUserRequestError;

export const hasPropertiesValidationErrors = (state: State): boolean => state.hasPropertyValidationErrors;
export const canApplyPropertyChanges = (state: State): boolean =>
  hasChanges(state) && !state.hasPropertyValidationErrors;
export const getUserUpdatePropertiesError = (state: State): ServerError => state.mainUserUpdatePropertiesError;
