import { Action, createReducer, on } from '@ngrx/store';

import { MainUserCreateDialogActions } from '../actions';
import { ServerError } from '~app/shared/errors';

export const createMainUserFeatureKey = 'createMainUser';

export interface State {
  loading: boolean;
  requestError: ServerError;
}

export const initialState: State = {
  loading: false,
  requestError: null
};

const createMainUserDialogReducer = createReducer(
  initialState,
  on(
    MainUserCreateDialogActions.openMainUserCreateDialog,
    MainUserCreateDialogActions.createMainUserSuccess,
    () => initialState
  ),
  on(MainUserCreateDialogActions.createMainUserError, (state, { error }) => ({
    ...state,
    loading: false,
    requestError: error
  })),
  on(MainUserCreateDialogActions.createMainUser, state => ({ ...state, loading: true }))
);

export function reducer(state: State | undefined, action: Action) {
  return createMainUserDialogReducer(state, action);
}

export const getLoading = (state: State): boolean => state.loading;
export const getRequestError = (state: State): ServerError => state.requestError;
