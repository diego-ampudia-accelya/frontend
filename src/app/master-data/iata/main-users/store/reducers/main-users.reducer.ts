import { createReducer, on } from '@ngrx/store';
import cloneDeep from 'lodash/cloneDeep';

import { MainUserBE, MainUsersFilter } from '../../models/main-users.model';
import { MainUsersActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<MainUsersFilter>;
  previousQuery: DataQuery<MainUsersFilter>;
  data: MainUserBE[];
  loading: boolean;
}

export const initialState: State = {
  previousQuery: null,
  query: null,
  data: [],
  loading: false
};

const loadData = (state: State, data: PagedData<MainUserBE>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

const determineInitialQuery = (storedQuery: DataQuery<MainUsersFilter>): DataQuery<MainUsersFilter> => {
  const query: DataQuery<MainUsersFilter> = storedQuery || cloneDeep(defaultQuery);

  const initialQuery = cloneDeep(query);
  initialQuery.filterBy = {
    ...query.filterBy
  };

  return initialQuery;
};

export const reducer = createReducer(
  initialState,
  on(MainUsersActions.load, state => ({
    ...state,
    loading: true,
    query: determineInitialQuery(state.query)
  })),
  on(MainUsersActions.loadSuccess, (state, { data }) => ({
    ...loadData(state, data)
  })),
  on(MainUsersActions.search, (state, { query = state.query }) => ({
    ...state,
    query: {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    },
    previousQuery: state.query
  })),
  on(MainUsersActions.searchConfirmed, state => ({
    ...state,
    loading: true,
    previousQuery: null
  })),
  on(MainUsersActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(MainUsersActions.searchError, MainUsersActions.loadError, state => ({
    ...state,
    loading: false,
    data: []
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;
