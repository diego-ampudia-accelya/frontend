import { createAction, props } from '@ngrx/store';
import { Subject } from 'rxjs';

import { MainUserBE } from '../../models/main-users.model';
import { ServerError } from '~app/shared/errors';

// deactivate
export const openDeactivationDialog = createAction(
  '[Main User Activation Dialog] Open Deactivation Dialog',
  props<{ user: MainUserBE; actionEmitter: () => Subject<any> }>()
);
export const deactivate = createAction(
  '[Main User Activation Dialog] Deactivate User',
  props<{ userId: number; expiryDate?: Date; actionEmitter: () => Subject<any> }>()
);
export const deactivateSuccess = createAction(
  '[Main User Activation Dialog] Deactivate User Success',
  props<{ expiryDate: Date; actionEmitter: () => Subject<any> }>()
);
export const deactivateError = createAction(
  '[Main User Activation Dialog] Deactivate User Error',
  props<{ error: ServerError }>()
);

// reactivate
export const openReactivationDialog = createAction(
  '[Main User Activation Dialog] Open Reactivation Dialog',
  props<{ user: MainUserBE; actionEmitter: () => Subject<any> }>()
);
export const reactivate = createAction(
  '[Main User Activation Dialog] Reactivate User',
  props<{ userId: number; portalEmail: string; actionEmitter: () => Subject<any> }>()
);
export const reactivateSuccess = createAction(
  '[Main User Activation Dialog] Reactivate User Success',
  props<{ actionEmitter: () => Subject<any> }>()
);
export const reactivateError = createAction(
  '[Main User Activation Dialog] Reactivate User Error',
  props<{ error: ServerError }>()
);
