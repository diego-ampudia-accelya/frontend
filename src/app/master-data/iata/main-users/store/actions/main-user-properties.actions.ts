import { createAction, props } from '@ngrx/store';

import { MainUserDetails } from '../../models/main-user-details.model';
import { MainUserBE } from '../../models/main-users.model';
import { ServerError } from '~app/shared/errors';

export const requestUser = createAction('[Main User Properties] Request User', props<{ id: number }>());
export const requestUserSuccess = createAction(
  '[Main User Properties] Request User Success',
  props<{ user: MainUserBE }>()
);
export const requestUserError = createAction(
  '[Main User Properties] Request User Error',
  props<{ error: ServerError }>()
);

export const openApplyPropertyChangesDialog = createAction(
  '[Main User Properties] Open Apply Properties Changes Dialog'
);
export const applyPropertyChanges = createAction('[Main User Properties] Apply Properties Changes');
export const propertiesFormValueChange = createAction(
  '[Main User Properties] Properties Form Value Change',
  props<{ formValue: MainUserDetails; isFormValid: boolean }>()
);
export const discardPropertyChanges = createAction('[Main User Properties] Discard Properties Changes');

export const updateMainUserProperties = createAction(
  '[Main User Properties] Update Main User Properties',
  props<{ data: MainUserDetails }>()
);
export const cancelPropertiesChanges = createAction('[Main User Properties] Cancel Properties Changes');
export const updateMainUserPropertiesSuccess = createAction(
  '[Main User Properties] Update Main User Properties Success',
  props<{ user: MainUserBE }>()
);
export const updateMainUserPropertiesError = createAction(
  '[Main User Properties] Update Main User Properties Error',
  props<{ error: ServerError }>()
);
export const propertiesDeactivationRequest = createAction(
  '[Main User Properties] Properties Component Deactivation Request'
);
