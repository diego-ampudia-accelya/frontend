import { createAction, props } from '@ngrx/store';

import { MainUserBE, MainUsersFilter } from '../../models/main-users.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const load = createAction('[Main Users] Load');
export const loadSuccess = createAction('[Main Users] Load Success', props<{ data: PagedData<MainUserBE> }>());
export const loadError = createAction('[Main Users] Load Error');

export const search = createAction('[Main Users] Search', props<{ query?: DataQuery<MainUsersFilter> }>());
export const searchConfirmed = createAction('[Main Users] Search Confirmed');
export const searchSuccess = createAction('[Main Users] Search Success', props<{ data: PagedData<MainUserBE> }>());
export const searchError = createAction('[Main Users] Search Error');

export const download = createAction('[Main Users] Download');
export const openDownload = createAction('[Main Users] Open Download');
