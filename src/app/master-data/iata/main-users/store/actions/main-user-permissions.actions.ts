import { createAction, props } from '@ngrx/store';

import { MainUserPermission } from '../../models/main-users-permissions.model';
import { MainUserBE } from '../../models/main-users.model';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';

export const requestPermissions = createAction('[Main User Permissions] Request');

export const requestPermissionsSuccess = createAction(
  '[Main User Permissions] Request Success',
  props<{ permissions: MainUserPermission[]; user: MainUserBE }>()
);
export const requestPermissionsError = createAction('[Main User Permissions] Request Error');

export const change = createAction('[Main User Permissions] Change', props<{ modifications: SelectionList[] }>());

export const openApplyChanges = createAction('[Main User Permissions] Open Apply Changes');

export const applyChanges = createAction('[Main User Permissions] Apply Changes');

export const applyChangesSuccess = createAction(
  '[Main User Permissions] Apply Changes Success',
  props<{ permissions: MainUserPermission[]; user: MainUserBE }>()
);
export const applyChangesError = createAction('[Main User Permissions] Apply Changes Error');

export const discardChanges = createAction('[Main User Permissions] Discard Changes');

export const cancel = createAction('[Main User Permissions] Cancel');
