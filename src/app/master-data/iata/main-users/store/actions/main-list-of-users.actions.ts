import { createAction, props } from '@ngrx/store';

import { MainListOfUsersFilter, UserFromMainListOfUsersBE } from '../../models/main-list-of-users.model';
import { DataQuery } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';

export const load = createAction('[Main List of Users] Load');
export const loadSuccess = createAction(
  '[Main List of Users] Load Success',
  props<{ data: PagedData<UserFromMainListOfUsersBE> }>()
);
export const loadError = createAction('[Main List of Users] Load Error');

export const search = createAction(
  '[Main List of Users] Search',
  props<{ query?: DataQuery<MainListOfUsersFilter> }>()
);
export const searchConfirmed = createAction('[Main List of Users] Search Confirmed');
export const searchSuccess = createAction(
  '[Main List of Users] Search Success',
  props<{ data: PagedData<UserFromMainListOfUsersBE> }>()
);
export const searchError = createAction('[Main List of Users] Search Error');

export const download = createAction('[Main List of Users] Download');
export const openDownload = createAction('[Main List of Users] Open Download');
