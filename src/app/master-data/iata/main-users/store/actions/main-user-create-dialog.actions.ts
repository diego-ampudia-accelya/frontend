import { createAction, props } from '@ngrx/store';

import { MainUserCreateModelBE } from '../../models/create-main-user.model';
import { MainUserBE } from '../../models/main-users.model';
import { ServerError } from '~app/shared/errors';

export const openMainUserCreateDialog = createAction('[Main Users Create Dialog] Open Create Dialog');

export const createMainUser = createAction(
  '[Main Users Create Dialog] Create Main User',
  props<{ user: MainUserCreateModelBE }>()
);

export const createMainUserSuccess = createAction(
  '[Main Users Create Dialog] Create Main User Success',
  props<{ user: MainUserBE }>()
);

export const createMainUserError = createAction(
  '[Main Users Create Dialog] Create Main User Error',
  props<{ error: ServerError }>()
);
