import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  MainListOfUsersFilter,
  MainListOfUsersFilterBE,
  UserFromMainListOfUsersBE
} from '../models/main-list-of-users.model';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class MainListOfUsersService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/main-users`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(id: number, query?: DataQuery<MainListOfUsersFilter>): Observable<PagedData<UserFromMainListOfUsersBE>> {
    const url = `${this.baseUrl}/${id}/users`;
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<UserFromMainListOfUsersBE>>(url + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<MainListOfUsersFilter>,
    exportOptions: DownloadFormat,
    id: number
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };
    const url = `${this.baseUrl}/${id}/users/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<MainListOfUsersFilter>>): RequestQuery<MainListOfUsersFilterBE> {
    const { portalAccessEmail, userTemplate, level, registerDate, expiryDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        portalEmail: portalAccessEmail,
        template: userTemplate,
        mainUser: level,
        fromRegisterDate: registerDate && toShortIsoDate(registerDate[0]),
        toRegisterDate: registerDate && toShortIsoDate(registerDate[1] || registerDate[0]),
        fromExpiryDate: expiryDate && toShortIsoDate(expiryDate[0]),
        toExpiryDate: expiryDate && toShortIsoDate(expiryDate[1] || expiryDate[0])
      }
    });
  }
}
