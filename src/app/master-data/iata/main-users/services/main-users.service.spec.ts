import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import moment from 'moment-mini';
import { of } from 'rxjs';

import { MainUserType } from '../models/main-user-type.model';
import { MainUsersFilter } from '../models/main-users.model';
import { MainUsersService } from './main-users.service';
import { AgentService, AirlineService } from '~app/master-data/services';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { DataQuery } from '~app/shared/components/list-view';
import { GLOBALS } from '~app/shared/constants';
import { downloadRequestOptions } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { ActiveInactiveInUpperCase, DownloadFormat } from '~app/shared/models';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { UserTemplate } from '~app/shared/models/user-template.model';
import { AgentDictionaryService, AirlineDictionaryService, AppConfigurationService } from '~app/shared/services';

describe('MainUsersService', () => {
  let service: MainUsersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MainUsersService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: AirlineDictionaryService, useValue: { getDropdownOptions: () => of([]) } },
        { provide: AgentDictionaryService, useValue: { getDropdownOptions: () => of([]) } },
        { provide: GdsDictionaryService, useValue: { getDropdownOptions: () => of([]) } },
        { provide: AirlineService, useValue: { getOne: () => of({}) } },
        { provide: AgentService, useValue: { getOne: () => of({}) } }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MainUsersService);

    (service as any).http.get = jasmine.createSpy().and.returnValue(of({}));
    (service as any).http.put = jasmine.createSpy().and.returnValue(of({}));
    (service as any).http.post = jasmine.createSpy().and.returnValue(of({}));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('find', () => {
    const baseQuery: DataQuery<MainUsersFilter> = {
      filterBy: {
        userType: [MainUserType.Agent],
        userCode: 'test-code',
        userTemplate: UserTemplate.Efficient,
        portalAccessEmail: 'a@b.com',
        name: 'name',
        status: ActiveInactiveInUpperCase.Active
      },
      sortBy: [],
      paginateBy: { size: 20, page: 1 }
    };

    it('should call main users endpoint', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(jasmine.stringMatching('/user-management/main-users'));
    }));

    it('should include pagination parameters in main users request', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(jasmine.stringMatching('page=1&size=20'));
    }));

    it('should include user code parameter in main users request', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(jasmine.stringMatching('userCode=test-code'));
    }));
  });

  describe('download', () => {
    const baseQuery: DataQuery<MainUsersFilter> = {
      filterBy: {
        userType: [MainUserType.Agent],
        userCode: 'test-code',
        userTemplate: UserTemplate.Efficient,
        portalAccessEmail: 'a@b.com',
        name: 'name',
        status: ActiveInactiveInUpperCase.Active
      },
      sortBy: [],
      paginateBy: { size: 20, page: 1 }
    };
    const exportOptions = DownloadFormat.CSV;

    it('should call users download endpoint', fakeAsync(() => {
      service.download(baseQuery, exportOptions).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users/download'),
        downloadRequestOptions
      );
    }));

    it('should exclude pagination parameters in users download request', fakeAsync(() => {
      service.download(baseQuery, exportOptions).subscribe();
      tick();

      expect((service as any).http.get.calls.first().args[0]).not.toContain('page=1&size=20');
    }));

    it('should include user code parameter in users download request', fakeAsync(() => {
      service.download(baseQuery, exportOptions).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(
        jasmine.stringMatching('userCode=test-code'),
        downloadRequestOptions
      );
    }));
    it('should include exportAs parameter in users download request', fakeAsync(() => {
      service.download(baseQuery, exportOptions).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(
        jasmine.stringMatching('exportAs=csv'),
        downloadRequestOptions
      );
    }));
  });

  describe('deactivateUser', () => {
    it('should call user status change endpoint with user id', fakeAsync(() => {
      service.deactivateUser(25, new Date('1/10/22')).subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users/25/status'),
        jasmine.anything()
      );
    }));

    it('should include expiryDate and status parameters in user status change request', fakeAsync(() => {
      const expiryDate = moment(new Date('1/10/22')).format(GLOBALS.DATE_FORMAT.SHORT_ISO);

      service.deactivateUser(25, new Date('1/10/22')).subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(jasmine.any(String), {
        expiryDate,
        status: ActiveInactiveInUpperCase.Inactive
      });
    }));
  });

  describe('reactivateUser', () => {
    it('should call user status change endpoint with user id', fakeAsync(() => {
      service.reactivateUser(5, 'a@b.com').subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users/5/status'),
        jasmine.anything()
      );
    }));

    it('should include portalEmail and status parameters in user status change request', fakeAsync(() => {
      service.reactivateUser(5, 'a@b.com').subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(jasmine.any(String), {
        portalEmail: 'a@b.com',
        status: ActiveInactiveInUpperCase.Active
      });
    }));
  });

  describe('getPermissionsByUserId', () => {
    beforeEach(() => {
      (service as any).toUserPermissionsFromDto = jasmine.createSpy().and.returnValue([]);
    });

    it('should call permissions endpoint with user id', fakeAsync(() => {
      service.getPermissionsByUserId(15).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users/15/permissions')
      );
    }));
  });

  describe('updatePermissionsByUserId', () => {
    beforeEach(() => {
      (service as any).toUserPermissionsFromDto = jasmine.createSpy().and.returnValue([]);
    });

    it('should call update permissions endpoint with user id', fakeAsync(() => {
      service.updatePermissionsByUserId(10, []).subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users/10/permissions'),
        []
      );
    }));

    it('should include permissions list parameter in update permissions request', fakeAsync(() => {
      const paramData = [
        { id: 1, name: 'test-1', enabled: true },
        { id: 2, name: 'test-2', enabled: false }
      ];
      const expectedData = [
        { id: 1, enabled: true },
        { id: 2, enabled: false }
      ];

      service.updatePermissionsByUserId(5, paramData as any).subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(jasmine.any(String), expectedData);
    }));
  });

  describe('create', () => {
    it('should call create endpoint', fakeAsync(() => {
      service.create({} as any).subscribe();
      tick();

      expect((service as any).http.post).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users'),
        {}
      );
    }));

    it('should include data in create request', fakeAsync(() => {
      service.create({ portalEmail: 'a@b.com', name: 'test-name' } as any).subscribe();
      tick();

      expect((service as any).http.post).toHaveBeenCalledWith(jasmine.any(String), {
        portalEmail: 'a@b.com',
        name: 'test-name'
      });
    }));
  });

  describe('update', () => {
    it('should call update endpoint with user id', fakeAsync(() => {
      service.update(4, {} as any).subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users/4'),
        {}
      );
    }));

    it('should include data in update request', fakeAsync(() => {
      service.update(4, { portalEmail: 'a@b.com', name: 'test-name' } as any).subscribe();
      tick();

      expect((service as any).http.put).toHaveBeenCalledWith(jasmine.any(String), {
        portalEmail: 'a@b.com',
        name: 'test-name'
      });
    }));
  });
  describe('getOne', () => {
    it('should call getOne endpoint with user id', fakeAsync(() => {
      service.getOne(3).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(jasmine.stringMatching('/user-management/main-users/3'));
    }));
  });

  describe('getAirlines', () => {
    beforeEach(() => {
      (service as any).airlineDictionaryService.getDropdownOptions = jasmine
        .createSpy()
        .and.returnValue(of([{ id: 2, name: 'test-name-2' }]));
    });

    it('should call airlineDictionaryService.getDropdownOptions()', fakeAsync(() => {
      service.getAirlines().subscribe();
      tick();

      expect((service as any).airlineDictionaryService.getDropdownOptions).toHaveBeenCalled();
    }));

    it('should return result from airlineDictionaryService.getDropdownOptions()', fakeAsync(() => {
      let res;

      service.getAirlines().subscribe(r => (res = r));
      tick();

      expect(res).toEqual([{ id: 2, name: 'test-name-2' }]);
    }));
  });

  describe('getAgents', () => {
    beforeEach(() => {
      (service as any).agentDictionaryService.getDropdownOptions = jasmine
        .createSpy()
        .and.returnValue(of([{ id: 3, name: 'test-name-3' }]));
    });

    it('should call agentDictionaryService.getDropdownOptions()', fakeAsync(() => {
      service.getAgents().subscribe();
      tick();

      expect((service as any).agentDictionaryService.getDropdownOptions).toHaveBeenCalled();
    }));

    it('should return result from agentDictionaryService.getDropdownOptions()', fakeAsync(() => {
      let res;

      service.getAgents().subscribe(r => (res = r));
      tick();

      expect(res).toEqual([{ id: 3, name: 'test-name-3' }]);
    }));
  });

  describe('getGds', () => {
    beforeEach(() => {
      (service as any).gdsService.getDropdownOptions = jasmine
        .createSpy()
        .and.returnValue(of([{ id: 4, name: 'test-name-4' }]));
    });

    it('should call gdsService.getDropdownOptions() with bsp id', fakeAsync(() => {
      service.getGds(2).subscribe();
      tick();

      expect((service as any).gdsService.getDropdownOptions).toHaveBeenCalledWith([2]);
    }));

    it('should return result from gdsService.getDropdownOptions()', fakeAsync(() => {
      let res;

      service.getGds(2).subscribe(r => (res = r));
      tick();

      expect(res).toEqual([{ id: 4, name: 'test-name-4' }]);
    }));
  });

  describe('getAirline', () => {
    beforeEach(() => {
      (service as any).airlineService.getOne = jasmine.createSpy().and.returnValue(of({ id: 6, name: 'test-name-6' }));
    });

    it('should call airlineService.getOne() with agent id', fakeAsync(() => {
      service.getAirline(5).subscribe();
      tick();

      expect((service as any).airlineService.getOne).toHaveBeenCalledWith(5);
    }));

    it('should return result from airlineService.getOne()', fakeAsync(() => {
      let res;

      service.getAirline(5).subscribe(r => (res = r));
      tick();

      expect(res).toEqual({ id: 6, name: 'test-name-6' });
    }));
  });

  describe('getAgent', () => {
    beforeEach(() => {
      (service as any).agentService.getOne = jasmine.createSpy().and.returnValue(of({ id: 7, name: 'test-name-7' }));
    });

    it('should call agentService.getOne() with agent id', fakeAsync(() => {
      service.getAgent(6).subscribe();
      tick();

      expect((service as any).agentService.getOne).toHaveBeenCalledWith(6);
    }));

    it('should return result from agentService.getOne()', fakeAsync(() => {
      let res;

      service.getAgent(6).subscribe(r => (res = r));
      tick();

      expect(res).toEqual({ id: 7, name: 'test-name-7' });
    }));
  });

  describe('toUserPermissionsFromDto', () => {
    it('should return mapped permissions', () => {
      const params = [
        { name: 'test-1', catgory: { id: 1, name: 'cat-1' } },
        { name: 'test-2', catgory: { id: 2, name: 'cat-2' } }
      ];
      const expectedRes = [
        { name: 'test-1', category: { id: 1, name: 'cat-1' } },
        { name: 'test-2', category: { id: 2, name: 'cat-2' } }
      ];

      const res = (service as any).toUserPermissionsFromDto(params);

      expect(res).toEqual(expectedRes);
    });
  });

  describe('formatQuery', () => {
    it('should return mapped request query', () => {
      const query = {
        filterBy: {
          userTemplate: UserTemplate.Efficient,
          portalAccessEmail: 'a@b.com',
          registerDate: [new Date('02/01/22'), new Date('02/02/22')],
          expiryDate: [new Date('11/01/22')]
        }
      };
      const expectedRes = RequestQuery.fromDataQuery({
        filterBy: {
          template: UserTemplate.Efficient,
          portalEmail: 'a@b.com',
          fromRegisterDate: toShortIsoDate(new Date('02/01/22')),
          toRegisterDate: toShortIsoDate(new Date('02/02/22')),
          fromExpiryDate: toShortIsoDate(new Date('11/01/22')),
          toExpiryDate: toShortIsoDate(new Date('11/01/22'))
        }
      });

      const res = (service as any).formatQuery(query);

      expect(res).toEqual(expectedRes);
    });
  });
});
