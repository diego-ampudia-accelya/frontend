import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { forkJoin, Observable } from 'rxjs';
import { filter, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

import { MainUserPropertiesActions } from '../store/actions';
import * as fromMainUsers from '../store/reducers';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ChangeModel,
  ChangesDialogComponent,
  ChangesDialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';

@Injectable()
export class MainUserPropertiesChangesService {
  private changedProperties$: Observable<ChangeModel[]> = this.store.pipe(
    select(fromMainUsers.getPropertiesChanges),
    take(1)
  );
  private hasValidationErrors$: Observable<boolean> = this.store.pipe(
    select(fromMainUsers.hasPropertiesValidationErrors),
    take(1)
  );
  private propertiesFormValue$ = this.store.pipe(select(fromMainUsers.getPropertiesFormValue), take(1));

  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {}

  public confirmApplyPropertiesChanges(): Observable<boolean> {
    return this.openPropertyChangesDialog().pipe(map(clickedBtn => clickedBtn === FooterButton.Apply));
  }

  public getDeactivateAction(): Observable<boolean> {
    return this.handleDeactivationChange().pipe(
      tap(action => this.store.dispatch(action)),
      map(({ type }) => type !== MainUserPropertiesActions.cancelPropertiesChanges.type)
    );
  }

  private handleDeactivationChange(): Observable<Action> {
    return this.openPropertyChangesDialog('unsavedChanges').pipe(
      withLatestFrom(this.propertiesFormValue$),
      map(([clickedBtn, data]) => {
        switch (clickedBtn) {
          case FooterButton.Apply:
            return MainUserPropertiesActions.updateMainUserProperties({ data });
          case FooterButton.Discard:
            return MainUserPropertiesActions.discardPropertyChanges();
          default:
            return MainUserPropertiesActions.cancelPropertiesChanges();
        }
      })
    );
  }

  private openPropertyChangesDialog(
    configType: 'applyChanges' | 'unsavedChanges' = 'applyChanges'
  ): Observable<FooterButton> {
    return forkJoin([this.changedProperties$, this.hasValidationErrors$]).pipe(
      switchMap(([changesNoFormat, hasInvalidChanges]) => {
        const dialogConfig = this.getDialogConfig(changesNoFormat, hasInvalidChanges, configType);

        return this.dialogService.open(ChangesDialogComponent, dialogConfig);
      }),
      filter(action => action?.clickedBtn && action.contentComponentRef),
      tap(({ contentComponentRef }) => contentComponentRef.config?.serviceRef?.close()),
      map(({ clickedBtn }) => clickedBtn)
    );
  }

  private getDialogConfig(
    notFormattedChanges: ChangeModel[],
    hasInvalidChanges: boolean,
    configType: 'applyChanges' | 'unsavedChanges'
  ): ChangesDialogConfig {
    const configs = {
      applyChanges: {
        data: {
          title: 'mainUsers.userProperties.changesDialog.title',
          footerButtonsType: [{ type: FooterButton.Apply }]
        },
        message: 'mainUsers.userProperties.changesDialog.message',
        usePillsForChanges: true,
        changes: this.formatChanges(notFormattedChanges),
        hasInvalidChanges
      },
      unsavedChanges: {
        data: {
          title: 'mainUsers.userProperties.changesDialog.titleUnsaved',
          footerButtonsType: [
            { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
            { type: FooterButton.Apply }
          ]
        },
        message: 'mainUsers.userProperties.changesDialog.descriptionUnsavedProperties',
        invalidChangesMessage: 'mainUsers.userProperties.changesDialog.invalidWarning',
        usePillsForChanges: true,
        changes: this.formatChanges(notFormattedChanges),
        hasInvalidChanges
      }
    };

    return configs[configType];
  }

  private formatChanges(modifications: ChangeModel[]): ChangeModel[] {
    return modifications.map(item => ({
      group: this.formatLabel(item.group),
      name: this.formatItem(item),
      value: item.value,
      tooltip: this.formatItem(item)
    }));
  }

  private formatLabel(value: string): string {
    return this.translationService.translate(`mainUsers.userProperties.labels.${value}`);
  }

  private formatItem(item: ChangeModel): string {
    return `${this.formatLabel(item.name)}:  ${item.value}`;
  }
}
