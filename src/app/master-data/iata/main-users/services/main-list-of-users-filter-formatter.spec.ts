import { TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { MainListOfUsersFilterFormatter } from './main-list-of-users-filter-formatter';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';
import { ActiveInactiveInUpperCase } from '~app/shared/models';
import { UserTemplate } from '~app/shared/models/user-template.model';

describe('MainListOfUsersFilterFormatter', () => {
  let service: MainListOfUsersFilterFormatter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MainListOfUsersFilterFormatter,
        { provide: L10nTranslationService, useValue: { translate: () => '' } }
      ]
    });
    service = TestBed.inject(MainListOfUsersFilterFormatter);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('format', () => {
    const filterArgument = {
      userTemplate: UserTemplate.Efficient,
      portalAccessEmail: 'a@b.com',
      name: 'test-name',
      level: true,
      registerDate: [new Date('01/02/22'), new Date('02/02/22')],
      expiryDate: [new Date('01/02/22'), new Date('02/02/22')],
      status: ActiveInactiveInUpperCase.Active
    };

    beforeEach(() => {
      service.formatMainUser = jasmine.createSpy().and.callFake(val => val);
      (service as any).translate = jasmine.createSpy().and.callFake(val => val);
      (service as any).translation.translate = jasmine.createSpy().and.callFake(val => val);
    });

    it('should return formatted filter', () => {
      const expectedRes = [
        { keys: ['userTemplate'], label: 'userTemplate.label - userTemplate.options.Efficient' },
        { keys: ['portalAccessEmail'], label: 'portalAccessEmail.label - a@b.com' },
        { keys: ['name'], label: 'name.label - test-name' },
        { keys: ['level'], label: 'label - options.true' },
        {
          keys: ['registerDate'],
          label: `registerDate.label - ${rangeDateFilterTagMapper([new Date('01/02/22'), new Date('02/02/22')])}`
        },
        {
          keys: ['expiryDate'],
          label: `expiryDate.label - ${rangeDateFilterTagMapper([new Date('01/02/22'), new Date('02/02/22')])}`
        },
        { keys: ['status'], label: 'status.label - common.ACTIVE' }
      ];

      const res = service.format(filterArgument as any);

      expect(res).toEqual(expectedRes as any);
    });
  });

  describe('formatMainUser', () => {
    beforeEach(() => {
      (service as any).translation.translate = jasmine.createSpy().and.returnValue('test-1');
    });

    it('should call translation.translate', () => {
      service.formatMainUser('true');

      expect((service as any).translation.translate).toHaveBeenCalledWith(
        'MENU.USERS_MAINTENANCE.MAIN_USERS.listOfUsers.level.true'
      );
    });

    it('should return result from translate function', () => {
      const res = service.formatMainUser('true');

      expect(res).toBe('test-1');
    });
  });

  describe('translate', () => {
    beforeEach(() => {
      (service as any).translation.translate = jasmine.createSpy().and.returnValue('test-2');
    });

    it('should call translation.translate', () => {
      (service as any).translate('template.label');

      expect((service as any).translation.translate).toHaveBeenCalledWith(
        'MENU.USERS_MAINTENANCE.MAIN_USERS.filters.template.label'
      );
    });

    it('should return result from translate function', () => {
      const res = (service as any).translate('template.label');

      expect(res).toBe('test-2');
    });
  });
});
