import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import moment from 'moment-mini';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { MainUserCreateModelBE } from '../models/create-main-user.model';
import { MainUserDetails } from '../models/main-user-details.model';
import { MainUserPermission, MainUserPermissionDto } from '../models/main-users-permissions.model';
import { MainUserBE, MainUsersFilter, MainUsersFilterBE } from '../models/main-users.model';
import { Agent, Airline } from '~app/master-data/models';
import { AgentService, AirlineService } from '~app/master-data/services';
import { GdsDictionaryService } from '~app/master-data/services/gds-dictionary.service';
import { DataQuery } from '~app/shared/components/list-view';
import { GLOBALS } from '~app/shared/constants';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import {
  ActiveInactiveInUpperCase,
  AgentSummary,
  AirlineSummary,
  DownloadFormat,
  DropdownOption,
  GdsSummary
} from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { UserPermissionForUpdate } from '~app/shared/models/user-permission.model';
import { AgentDictionaryService, AirlineDictionaryService, AppConfigurationService } from '~app/shared/services';

@Injectable()
export class MainUsersService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/main-users`;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private airlineDictionaryService: AirlineDictionaryService,
    private agentDictionaryService: AgentDictionaryService,
    private airlineService: AirlineService,
    private agentService: AgentService,
    private gdsService: GdsDictionaryService
  ) {}

  public find(query?: DataQuery<MainUsersFilter>): Observable<PagedData<MainUserBE>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<MainUserBE>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<MainUsersFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public deactivateUser(userId: number, expiryDate: Date): Observable<any> {
    return this.http.put(`${this.baseUrl}/${userId}/status`, {
      expiryDate: expiryDate && moment(expiryDate).format(GLOBALS.DATE_FORMAT.SHORT_ISO),
      status: ActiveInactiveInUpperCase.Inactive
    });
  }

  public reactivateUser(userId: number, portalEmail: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/${userId}/status`, {
      portalEmail,
      status: ActiveInactiveInUpperCase.Active
    });
  }

  public getPermissionsByUserId(userId: number): Observable<MainUserPermission[]> {
    return this.http
      .get<MainUserPermissionDto[]>(`${this.baseUrl}/${userId}/permissions`)
      .pipe(map(this.toUserPermissionsFromDto));
  }

  public updatePermissionsByUserId(id: number, permissions: MainUserPermission[]): Observable<MainUserPermission[]> {
    const data: UserPermissionForUpdate[] = permissions.map(p => ({ id: p.id, enabled: p.enabled }));

    return this.http
      .put<MainUserPermissionDto[]>(`${this.baseUrl}/${id}/permissions`, data)
      .pipe(map(this.toUserPermissionsFromDto));
  }

  public create(data: MainUserCreateModelBE): Observable<MainUserBE> {
    return this.http.post<MainUserBE>(this.baseUrl, data);
  }

  public update(userId: number, data: MainUserDetails): Observable<MainUserBE> {
    return this.http.put<MainUserBE>(`${this.baseUrl}/${userId}`, data);
  }

  public getOne(id: number): Observable<MainUserBE> {
    return this.http.get<MainUserBE>(`${this.baseUrl}/${id}`);
  }

  public getAirlines(): Observable<DropdownOption<AirlineSummary>[]> {
    return this.airlineDictionaryService.getDropdownOptions();
  }

  public getAgents(): Observable<DropdownOption<AgentSummary>[]> {
    return this.agentDictionaryService.getDropdownOptions();
  }

  public getGds(bspId: number): Observable<DropdownOption<GdsSummary>[]> {
    return this.gdsService.getDropdownOptions([bspId]);
  }

  public getAirline(airlineId: number): Observable<Airline> {
    return this.airlineService.getOne(airlineId);
  }

  public getAgent(agentId: number): Observable<Agent> {
    return this.agentService.getOne(agentId);
  }

  private toUserPermissionsFromDto = (dtos: MainUserPermissionDto[]): MainUserPermission[] =>
    dtos.map(({ catgory, ...p }) => ({ ...p, category: catgory }));

  private formatQuery(query: Partial<DataQuery<MainUsersFilter>>): RequestQuery<MainUsersFilterBE> {
    const { userTemplate, portalAccessEmail, registerDate, expiryDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        template: userTemplate,
        portalEmail: portalAccessEmail,
        fromRegisterDate: registerDate && toShortIsoDate(registerDate[0]),
        toRegisterDate: registerDate && toShortIsoDate(registerDate[1] || registerDate[0]),
        fromExpiryDate: expiryDate && toShortIsoDate(expiryDate[0]),
        toExpiryDate: expiryDate && toShortIsoDate(expiryDate[1] || expiryDate[0])
      }
    });
  }
}
