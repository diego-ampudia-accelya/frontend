import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';

import { MainUserPermissionsActions } from '../store/actions';
import * as fromMainUsers from '../store/reducers';
import { AppState } from '~app/reducers';
import { ButtonDesign, ChangeModel, ChangesDialogComponent, DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class MainUserPermissionsChangesService {
  private changedPermissions$: Observable<ChangeModel[]> = this.store.pipe(
    select(fromMainUsers.getPermissionsChanges),
    map(changes =>
      changes.map((change: ChangeModel) => ({
        ...change,
        value: this.permissionsStatusTextMap.get(change.value.toString())
      }))
    ),
    take(1)
  );
  private loading$ = this.store.pipe(select(fromMainUsers.getMainUserPermissionsLoading));

  public applyUserPermissions$: Observable<Action> = this.handlePermissionsChange();

  public handlePermissionsChangeAndDeactivate$ = combineLatest([this.handlePermissionsChanges$(), this.loading$]).pipe(
    filter(([_, loading]) => !loading),
    map(([shouldProceed]) => shouldProceed)
  );

  private permissionsStatusTextMap = new Map([
    ['true', this.translationService.translate('mainUsers.permissions.checkboxStatusText.true')],
    ['false', this.translationService.translate('mainUsers.permissions.checkboxStatusText.false')]
  ]);

  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {}

  private handlePermissionsChanges$(): Observable<boolean> {
    return this.handlePermissionsChange('unsavedChanges').pipe(
      tap(action => this.store.dispatch(action)),
      map(
        ({ type }) =>
          type === MainUserPermissionsActions.applyChanges.type ||
          type === MainUserPermissionsActions.discardChanges.type
      )
    );
  }

  private handlePermissionsChange(
    dialogConfigType: 'applyChanges' | 'unsavedChanges' = 'applyChanges'
  ): Observable<Action> {
    const dialogConfigs = {
      applyChanges: {
        data: {
          title: 'mainUsers.userProperties.changesDialog.title',
          footerButtonsType: [{ type: FooterButton.Apply }]
        },
        message: 'mainUsers.permissions.changesDialog.descriptionPermissionsApply',
        changes: []
      },
      unsavedChanges: {
        data: {
          title: 'mainUsers.userProperties.changesDialog.titleUnsaved',
          footerButtonsType: [
            { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
            { type: FooterButton.Apply }
          ]
        },
        message: 'mainUsers.permissions.changesDialog.descriptionUnsavedPermissions',
        changes: []
      }
    };

    return this.changedPermissions$.pipe(
      switchMap(changes =>
        this.dialogService.open(ChangesDialogComponent, { ...dialogConfigs[dialogConfigType], changes })
      ),
      filter(action => action?.clickedBtn && action.contentComponentRef),
      tap(({ contentComponentRef }) => contentComponentRef.config?.serviceRef?.close()),
      map(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Apply:
            return MainUserPermissionsActions.applyChanges();
          case FooterButton.Discard:
            return MainUserPermissionsActions.discardChanges();
          default:
            return MainUserPermissionsActions.cancel();
        }
      })
    );
  }
}
