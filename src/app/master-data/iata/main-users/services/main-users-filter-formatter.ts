import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { MainUsersFilter } from '../models/main-users.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { joinMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class MainUsersFilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: MainUsersFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<MainUsersFilter>> = {
      userType: userTypes => {
        const translateUserType = userType => this.translate(`userType.options.${userType.replace(/ /g, '')}`);

        return `${this.translate('userType.label')} - ${joinMapper(userTypes.map(translateUserType))}`;
      },
      userCode: userCode => `${this.translate('userCode.label')} - ${userCode}`,
      userTemplate: userTemplate => {
        const translatedTemplate = this.translate(`userTemplate.options.${userTemplate}`);

        return `${this.translate('userTemplate.label')} - ${translatedTemplate}`;
      },
      portalAccessEmail: portalAccessEmail => `${this.translate('portalAccessEmail.label')} - ${portalAccessEmail}`,
      name: name => `${this.translate('name.label')} - ${name}`,
      registerDate: registerDate =>
        `${this.translate('registerDate.label')} - ${rangeDateFilterTagMapper(registerDate)}`,
      expiryDate: expiryDate => `${this.translate('expiryDate.label')} - ${rangeDateFilterTagMapper(expiryDate)}`,
      status: status => {
        const translatedStatus = this.translation.translate(`common.${status}`);

        return `${this.translate('status.label')} - ${translatedStatus}`;
      }
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.USERS_MAINTENANCE.MAIN_USERS.filters.${key}`);
  }
}
