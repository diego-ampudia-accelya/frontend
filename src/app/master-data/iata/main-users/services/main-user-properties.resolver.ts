import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { merge, Observable } from 'rxjs';
import { filter, first, tap } from 'rxjs/operators';

import { MainUserBE } from '../models/main-users.model';
import { MainUserPropertiesActions } from '../store/actions';
import * as fromMainUsers from '../store/reducers';
import { AppState } from '~app/reducers';

@Injectable()
export class MainUserPropertiesResolver implements Resolve<MainUserBE> {
  constructor(private store: Store<AppState>, private router: Router) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<MainUserBE> {
    this.requestUser(route);

    const user$ = this.store.pipe(select(fromMainUsers.getMainUser), filter(Boolean));
    const userError$ = this.store.pipe(
      select(fromMainUsers.getUserPropertiesRequestError),
      filter(Boolean),
      tap(() => this.handleNavigationOnError())
    );

    return merge(userError$, user$).pipe(first()) as Observable<MainUserBE>;
  }

  private requestUser(route: ActivatedRouteSnapshot) {
    const id = Number(route.paramMap.get('id'));

    if (id) {
      this.store.dispatch(MainUserPropertiesActions.requestUser({ id }));
    }
  }

  private handleNavigationOnError(): void {
    const lastSuccessfulNavigation = this.router.getCurrentNavigation().previousNavigation;
    if (lastSuccessfulNavigation != null) {
      this.router.navigateByUrl(lastSuccessfulNavigation.finalUrl.toString());
    } else {
      this.router.navigate(['./']);
    }
  }
}
