import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { MainListOfUsersFilter } from '../models/main-list-of-users.model';
import { MainListOfUsersService } from './main-list-of-users.service';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { ActiveInactiveInUpperCase, DownloadFormat } from '~app/shared/models';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { UserTemplate } from '~app/shared/models/user-template.model';
import { AppConfigurationService } from '~app/shared/services';

describe('MainListOfUsersService', () => {
  let service: MainListOfUsersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MainListOfUsersService, { provide: AppConfigurationService, useValue: { baseApiPath: '' } }]
    });
    service = TestBed.inject(MainListOfUsersService);

    (service as any).http.get = jasmine.createSpy().and.returnValue(of({}));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('find', () => {
    const baseQuery: DataQuery<MainListOfUsersFilter> = {
      filterBy: {
        userTemplate: UserTemplate.Efficient,
        portalAccessEmail: 'a@b.com',
        name: 'name',
        status: ActiveInactiveInUpperCase.Active,
        level: false
      },
      sortBy: [],
      paginateBy: { size: 20, page: 1 }
    };

    it('should call main users endpoint', fakeAsync(() => {
      service.find(5, baseQuery).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(jasmine.stringMatching('/user-management/main-users'));
    }));

    it('should include pagination parameters in main users request', fakeAsync(() => {
      service.find(5, baseQuery).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(jasmine.stringMatching('page=1&size=20'));
    }));

    it('should include user id parameter in main users request', fakeAsync(() => {
      service.find(5, baseQuery).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(jasmine.stringMatching('/5/users'));
    }));
  });

  describe('download', () => {
    const baseQuery: DataQuery<MainListOfUsersFilter> = {
      filterBy: {
        userTemplate: UserTemplate.Efficient,
        portalAccessEmail: 'a@b.com',
        name: 'name',
        status: ActiveInactiveInUpperCase.Active,
        level: false
      },
      sortBy: [],
      paginateBy: { size: 20, page: 1 }
    };
    const exportOptions = DownloadFormat.CSV;
    const id = 5;

    it('should call users download endpoint', fakeAsync(() => {
      service.download(baseQuery, exportOptions, id).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(
        jasmine.stringMatching('/users/download'),
        downloadRequestOptions
      );
    }));

    it('should exclude pagination parameters in users download request', fakeAsync(() => {
      service.download(baseQuery, exportOptions, id).subscribe();
      tick();

      expect((service as any).http.get.calls.first().args[0]).not.toContain('page=1&size=20');
    }));

    it('should include user id parameter in users download request', fakeAsync(() => {
      service.download(baseQuery, exportOptions, id).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(
        jasmine.stringMatching('/user-management/main-users/5'),
        downloadRequestOptions
      );
    }));
  });

  describe('formatQuery', () => {
    it('should return mapped request query', () => {
      const query = {
        filterBy: {
          portalAccessEmail: 'a@b.com',
          userTemplate: UserTemplate.Efficient,
          level: true,
          registerDate: [new Date('02/01/22'), new Date('02/02/22')],
          expiryDate: [new Date('11/01/22')]
        }
      };
      const expectedRes = RequestQuery.fromDataQuery({
        filterBy: {
          portalEmail: 'a@b.com',
          template: UserTemplate.Efficient,
          mainUser: true,
          fromRegisterDate: toShortIsoDate(new Date('02/01/22')),
          toRegisterDate: toShortIsoDate(new Date('02/02/22')),
          fromExpiryDate: toShortIsoDate(new Date('11/01/22')),
          toExpiryDate: toShortIsoDate(new Date('11/01/22'))
        }
      });

      const res = (service as any).formatQuery(query);

      expect(res).toEqual(expectedRes);
    });
  });
});
