import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { MainListOfUsersFilter } from '../models/main-list-of-users.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class MainListOfUsersFilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: MainListOfUsersFilter): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<MainListOfUsersFilter>> = {
      userTemplate: userTemplate => {
        const translatedTemplate = this.translate(`userTemplate.options.${userTemplate}`);

        return `${this.translate('userTemplate.label')} - ${translatedTemplate}`;
      },
      portalAccessEmail: portalAccessEmail => `${this.translate('portalAccessEmail.label')} - ${portalAccessEmail}`,
      level: level => `${this.formatMainUser('label')} - ${this.formatMainUser(`options.${level}`)}`,
      name: name => `${this.translate('name.label')} - ${name}`,
      registerDate: registerDate =>
        `${this.translate('registerDate.label')} - ${rangeDateFilterTagMapper(registerDate)}`,
      expiryDate: expiryDate => `${this.translate('expiryDate.label')} - ${rangeDateFilterTagMapper(expiryDate)}`,
      status: status => {
        const translatedStatus = this.translation.translate(`common.${status}`);

        return `${this.translate('status.label')} - ${translatedStatus}`;
      }
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public formatMainUser(status: string): string {
    return this.translation.translate(`MENU.USERS_MAINTENANCE.MAIN_USERS.listOfUsers.level.${status}`);
  }

  private translate(key: string): string {
    return this.translation.translate(`MENU.USERS_MAINTENANCE.MAIN_USERS.filters.${key}`);
  }
}
