import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, of } from 'rxjs';

import { MainUserPermissionsActions } from '../store/actions';
import * as fromMainUsers from '../store/reducers';
import * as fromMainListOfUsers from '../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../store/reducers/main-users.reducer';

import { MainUserPermissionsChangesService } from './main-user-permissions-changes.service';
import { DialogService, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

describe('MainUserPermissionsChangesService', () => {
  let service: MainUserPermissionsChangesService;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MainUserPermissionsChangesService,
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [
            { selector: fromMainUsers.getPermissionsChanges, value: [{ name: 'test-name-1', value: true }] },
            { selector: fromMainUsers.getMainUserPermissionsLoading, value: false }
          ]
        }),
        { provide: DialogService, useValue: { open: () => of({}) } },
        { provide: L10nTranslationService, useValue: { translate: () => 'test-val-1' } }
      ]
    });
    service = TestBed.inject(MainUserPermissionsChangesService);

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('handlePermissionsChanges$', () => {
    const subject = new BehaviorSubject(MainUserPermissionsActions.applyChanges());

    beforeEach(() => {
      (service as any).handlePermissionsChange = jasmine.createSpy().and.returnValue(subject);
    });

    it('should call handlePermissionsChange()', fakeAsync(() => {
      (service as any).handlePermissionsChanges$().subscribe();
      subject.next(MainUserPermissionsActions.applyChanges());
      tick();

      expect((service as any).handlePermissionsChange).toHaveBeenCalledWith('unsavedChanges');
    }));

    it('should dispatch MainUserPermissionsActions.applyChanges', fakeAsync(() => {
      (service as any).handlePermissionsChanges$().subscribe();
      subject.next(MainUserPermissionsActions.applyChanges());
      tick();

      expect(mockStore.dispatch).toHaveBeenCalledWith(MainUserPermissionsActions.applyChanges());
    }));

    it('should return true when action is applyChanges', fakeAsync(() => {
      let res;

      (service as any).handlePermissionsChanges$().subscribe(r => (res = r));
      subject.next(MainUserPermissionsActions.applyChanges());
      tick();

      expect(res).toBe(true);
    }));
  });

  describe('handlePermissionsChange', () => {
    const closeFn = jasmine.createSpy();
    const action = {
      clickedBtn: FooterButton.Apply,
      contentComponentRef: { config: { serviceRef: { close: closeFn } } }
    };

    beforeEach(() => {
      (service as any).dialogService.open = jasmine.createSpy().and.returnValue(of(action));
      (service as any).handlePermissionsChanges$ = jasmine.createSpy().and.returnValue(of(true));
    });

    it('should call dialogService.open()', fakeAsync(() => {
      (service as any).handlePermissionsChange().subscribe();
      tick();

      expect((service as any).dialogService.open).toHaveBeenCalled();
      expect((service as any).dialogService.open.calls.first().args[1]).toEqual({
        data: {
          title: 'mainUsers.userProperties.changesDialog.title',
          footerButtonsType: [{ type: FooterButton.Apply }]
        },
        message: 'mainUsers.permissions.changesDialog.descriptionPermissionsApply',
        changes: [{ name: 'test-name-1', value: 'test-val-1' }]
      });
    }));

    it('should call close() function from config', fakeAsync(() => {
      (service as any).handlePermissionsChange().subscribe();
      tick();

      expect(closeFn).toHaveBeenCalled();
    }));

    it('should return MainUserPermissionsActions.applyChanges() action', fakeAsync(() => {
      let res;

      (service as any).handlePermissionsChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(MainUserPermissionsActions.applyChanges());
    }));

    it('should return MainUserPermissionsActions.discardChanges() action', fakeAsync(() => {
      let res;
      (service as any).dialogService.open = jasmine
        .createSpy()
        .and.returnValue(of({ ...action, clickedBtn: FooterButton.Discard }));

      (service as any).handlePermissionsChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(MainUserPermissionsActions.discardChanges());
    }));

    it('should return MainUserPermissionsActions.cancel() action', fakeAsync(() => {
      let res;
      (service as any).dialogService.open = jasmine
        .createSpy()
        .and.returnValue(of({ ...action, clickedBtn: FooterButton.Cancel }));

      (service as any).handlePermissionsChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(MainUserPermissionsActions.cancel());
    }));
  });
});
