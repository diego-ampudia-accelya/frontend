import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { MainUserPropertiesActions } from '../store/actions';
import * as fromMainUsers from '../store/reducers';
import * as fromMainListOfUsers from '../store/reducers/main-list-of-users.reducer';
import * as fromMainUserActivationDialog from '../store/reducers/main-user-activation-dialog.reducer';
import * as fromMainUserPermissions from '../store/reducers/main-user-permissions.reducer';
import * as fromMainUserProperties from '../store/reducers/main-user-properties.reducer';
import * as fromCreateMainUser from '../store/reducers/main-users-create-dialog.reducer';
import * as fromMainUsersList from '../store/reducers/main-users.reducer';

import { MainUserPropertiesChangesService } from './main-user-properties-changes.service';
import { ButtonDesign, DialogService, FooterButton } from '~app/shared/components';

describe('MainUserPropertiesChangesService', () => {
  let service: MainUserPropertiesChangesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MainUserPropertiesChangesService,
        provideMockStore({
          initialState: {
            [fromMainUsers.mainUsersFeatureKey]: {
              [fromMainUsersList.key]: {},
              [fromMainUserActivationDialog.mainUserActivationDialogFeatureKey]: {},
              [fromCreateMainUser.createMainUserFeatureKey]: {},
              [fromMainUserProperties.mainUserPropertiesFeatureKey]: {},
              [fromMainUserPermissions.mainUserPermissionsKey]: {},
              [fromMainListOfUsers.mainListOfUsersKey]: fromMainListOfUsers.reducer
            }
          },
          selectors: [
            {
              selector: fromMainUsers.getPropertiesChanges,
              value: [{ group: 'test-group', name: 'test-value', tooltip: 'test-value', value: 'test-value' }]
            },
            { selector: fromMainUsers.hasPropertiesValidationErrors, value: false },
            {
              selector: fromMainUsers.getPropertiesFormValue,
              value: { portalEmail: 'a@b.com', name: 'test-name' }
            }
          ]
        }),
        { provide: DialogService, useValue: { open: () => of({}) } },
        { provide: L10nTranslationService, useValue: { translate: () => '' } }
      ]
    });
    service = TestBed.inject(MainUserPropertiesChangesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('confirmApplyPropertiesChanges', () => {
    beforeEach(() => {
      (service as any).openPropertyChangesDialog = jasmine.createSpy().and.returnValue(of(FooterButton.Apply));
    });

    it('should call openPropertyChangesDialog()', fakeAsync(() => {
      service.confirmApplyPropertiesChanges().subscribe();
      tick();

      expect((service as any).openPropertyChangesDialog).toHaveBeenCalled();
    }));

    it('should return true when clickedBtn is Apply', fakeAsync(() => {
      let res;

      service.confirmApplyPropertiesChanges().subscribe(r => (res = r));
      tick();

      expect(res).toBe(true);
    }));

    it('should return false when clickedBtn is Cancel', fakeAsync(() => {
      let res;
      (service as any).openPropertyChangesDialog = jasmine.createSpy().and.returnValue(of(FooterButton.Cancel));

      service.confirmApplyPropertiesChanges().subscribe(r => (res = r));
      tick();

      expect(res).toBe(false);
    }));
  });

  describe('getDeactivateAction', () => {
    beforeEach(() => {
      (service as any).handleDeactivationChange = jasmine
        .createSpy()
        .and.returnValue(of(MainUserPropertiesActions.discardPropertyChanges()));
    });

    it('should call handleDeactivationChange()', fakeAsync(() => {
      service.getDeactivateAction().subscribe();
      tick();

      expect((service as any).handleDeactivationChange).toHaveBeenCalled();
    }));

    it('should return true when returned action is not cancelPropertiesChanges', fakeAsync(() => {
      let res;

      service.getDeactivateAction().subscribe(r => (res = r));
      tick();

      expect(res).toBe(true);
    }));

    it('should return false when returned action is cancelPropertiesChanges', fakeAsync(() => {
      let res;
      (service as any).handleDeactivationChange = jasmine
        .createSpy()
        .and.returnValue(of(MainUserPropertiesActions.cancelPropertiesChanges()));

      service.getDeactivateAction().subscribe(r => (res = r));
      tick();

      expect(res).toBe(false);
    }));
  });

  describe('handleDeactivationChange', () => {
    beforeEach(() => {
      (service as any).openPropertyChangesDialog = jasmine.createSpy().and.returnValue(of(FooterButton.Apply));
    });

    it('should call openPropertyChangesDialog()', fakeAsync(() => {
      (service as any).handleDeactivationChange().subscribe();
      tick();

      expect((service as any).openPropertyChangesDialog).toHaveBeenCalledWith('unsavedChanges');
    }));

    it('should return MainUserPropertiesActions.updateMainUserProperties when clicked button is Apply', fakeAsync(() => {
      let res;

      (service as any).handleDeactivationChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(
        MainUserPropertiesActions.updateMainUserProperties({
          data: { portalEmail: 'a@b.com', name: 'test-name' } as any
        })
      );
    }));

    it('should return MainUserPropertiesActions.discardPropertyChanges when clicked button is Discard', fakeAsync(() => {
      let res;
      (service as any).openPropertyChangesDialog = jasmine.createSpy().and.returnValue(of(FooterButton.Discard));

      (service as any).handleDeactivationChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(MainUserPropertiesActions.discardPropertyChanges());
    }));

    it('should return MainUserPropertiesActions.cancelPropertiesChanges when clicked button is Cancel', fakeAsync(() => {
      let res;
      (service as any).openPropertyChangesDialog = jasmine.createSpy().and.returnValue(of(FooterButton.Cancel));

      (service as any).handleDeactivationChange().subscribe(r => (res = r));
      tick();

      expect(res).toEqual(MainUserPropertiesActions.cancelPropertiesChanges());
    }));
  });

  describe('openPropertyChangesDialog', () => {
    const closeFn = jasmine.createSpy();

    beforeEach(() => {
      (service as any).getDialogConfig = jasmine
        .createSpy()
        .and.returnValue({ title: 'test-title', message: 'test-message' });
      (service as any).dialogService.open = jasmine.createSpy().and.returnValue(
        of({
          clickedBtn: FooterButton.Apply,
          contentComponentRef: { config: { serviceRef: { close: closeFn } } }
        })
      );
    });

    it('should call getDialogConfig()', fakeAsync(() => {
      (service as any).openPropertyChangesDialog('unsavedChanges').subscribe();
      tick();

      expect((service as any).getDialogConfig).toHaveBeenCalledWith(
        [{ group: 'test-group', name: 'test-value', tooltip: 'test-value', value: 'test-value' }],
        false,
        'unsavedChanges'
      );
    }));

    it('should call dialogService.open()', fakeAsync(() => {
      (service as any).openPropertyChangesDialog('unsavedChanges').subscribe();
      tick();

      expect((service as any).dialogService.open).toHaveBeenCalledWith(jasmine.anything(), {
        title: 'test-title',
        message: 'test-message'
      });
    }));

    it('should call close() function from config', fakeAsync(() => {
      (service as any).openPropertyChangesDialog('unsavedChanges').subscribe();
      tick();

      expect(closeFn).toHaveBeenCalled();
    }));

    it('should return Apply clicked button', fakeAsync(() => {
      let res;

      (service as any).openPropertyChangesDialog('unsavedChanges').subscribe(r => (res = r));
      tick();

      expect(res).toBe(FooterButton.Apply);
    }));
  });

  describe('getDialogConfig', () => {
    const notFormattedChanges = [{ name: 'test-name', value: 'test-value' }];

    beforeEach(() => {
      (service as any).formatChanges = jasmine
        .createSpy()
        .and.returnValue([
          { group: 'formatted-group', name: 'formatted-value', tooltip: 'formatted-value', value: 'test-value' }
        ]);
    });

    it('should call formatChanges() with not formatted changes', () => {
      (service as any).getDialogConfig(notFormattedChanges, false, 'applyChanges');

      expect((service as any).formatChanges).toHaveBeenCalledWith(notFormattedChanges);
    });

    it('should return applyChanges config', () => {
      const expectedRes = {
        data: {
          title: 'mainUsers.userProperties.changesDialog.title',
          footerButtonsType: [{ type: FooterButton.Apply }]
        },
        message: 'mainUsers.userProperties.changesDialog.message',
        usePillsForChanges: true,
        changes: [
          { group: 'formatted-group', name: 'formatted-value', tooltip: 'formatted-value', value: 'test-value' }
        ],
        hasInvalidChanges: false
      };

      const res = (service as any).getDialogConfig(notFormattedChanges, false, 'applyChanges');

      expect(res).toEqual(expectedRes);
    });

    it('should return unsavedChanges config', () => {
      const expectedRes = {
        data: {
          title: 'mainUsers.userProperties.changesDialog.titleUnsaved',
          footerButtonsType: [
            { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
            { type: FooterButton.Apply }
          ]
        },
        message: 'mainUsers.userProperties.changesDialog.descriptionUnsavedProperties',
        invalidChangesMessage: 'mainUsers.userProperties.changesDialog.invalidWarning',
        usePillsForChanges: true,
        changes: [
          { group: 'formatted-group', name: 'formatted-value', tooltip: 'formatted-value', value: 'test-value' }
        ],
        hasInvalidChanges: true
      };

      const res = (service as any).getDialogConfig(notFormattedChanges, true, 'unsavedChanges');

      expect(res).toEqual(expectedRes);
    });
  });

  describe('formatChanges', () => {
    const params = [{ group: 'test-group', value: 'test-value', name: 'test-name' }];

    beforeEach(() => {
      (service as any).formatLabel = jasmine.createSpy().and.returnValue('formatted-group');
      (service as any).formatItem = jasmine.createSpy().and.returnValue('formatted-value');
    });

    it('should call formatLabel() with item group', () => {
      (service as any).formatChanges(params);

      expect((service as any).formatLabel).toHaveBeenCalledWith('test-group');
    });

    it('should call formatItem() twice with item', () => {
      (service as any).formatChanges(params);

      expect((service as any).formatItem).toHaveBeenCalledTimes(2);
      expect((service as any).formatItem.calls.first().args).toEqual([
        { group: 'test-group', value: 'test-value', name: 'test-name' }
      ]);
    });

    it('should return mapped nodifications', () => {
      const res = (service as any).formatChanges(params);
      const expectedRes = [
        { group: 'formatted-group', name: 'formatted-value', tooltip: 'formatted-value', value: 'test-value' }
      ];

      expect(res).toEqual(expectedRes);
    });
  });

  describe('formatLabel', () => {
    beforeEach(() => {
      (service as any).translationService.translate = jasmine.createSpy().and.returnValue('translated-label');
    });

    it('should call translationService.translate', () => {
      (service as any).formatLabel('label-name');

      expect((service as any).translationService.translate).toHaveBeenCalledWith(
        'mainUsers.userProperties.labels.label-name'
      );
    });

    it('should return translated label', () => {
      const res = (service as any).formatLabel('label-name');

      expect(res).toBe('translated-label');
    });
  });

  describe('formatItem', () => {
    beforeEach(() => {
      (service as any).formatLabel = jasmine.createSpy().and.returnValue('formatted-name');
    });

    it('should call formatLabel() with item name', () => {
      (service as any).formatItem({ name: 'test-name', value: 'test-value' });

      expect((service as any).formatLabel).toHaveBeenCalledWith('test-name');
    });

    it('should return formatted to string item', () => {
      const res = (service as any).formatItem({ name: 'test-name', value: 'test-value' });

      expect(res).toBe('formatted-name:  test-value');
    });
  });
});
