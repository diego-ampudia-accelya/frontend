import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { MainListOfUsersComponent } from './components/main-list-of-users/main-list-of-users.component';
import { MainUserDeactivateDialogComponent } from './components/main-user-deactivate-dialog/main-user-deactivate-dialog.component';
import { MainUserPermissionsComponent } from './components/main-user-permissions/main-user-permissions.component';
import { MainUserProfileComponent } from './components/main-user-profile/main-user-profile.component';
import { MainUserPropertiesComponent } from './components/main-user-properties/main-user-properties.component';
import { MainUserReactivateDialogComponent } from './components/main-user-reactivate-dialog/main-user-reactivate-dialog.component';
import { MainUsersCreateDialogComponent } from './components/main-users-create-dialog/main-users-create-dialog.component';
import { MainUsersComponent } from './components/main-users/main-users.component';
import { MainUsersRoutingModule } from './main-users-routing.module';
import { MainListOfUsersFilterFormatter } from './services/main-list-of-users-filter-formatter';
import { MainListOfUsersService } from './services/main-list-of-users.service';
import { MainUserPermissionsChangesService } from './services/main-user-permissions-changes.service';
import { MainUserPropertiesChangesService } from './services/main-user-properties-changes.service';
import { MainUserPropertiesResolver } from './services/main-user-properties.resolver';
import { MainUsersFilterFormatter } from './services/main-users-filter-formatter';
import { MainUsersService } from './services/main-users.service';
import { MainListOfUsersEffects } from './store/effects/main-list-of-users.effects';
import { MainUserActivationDialogEffects } from './store/effects/main-user-activation-dialog.effects';
import { MainUserCreateDialogEffects } from './store/effects/main-user-create-dialog.effects';
import { MainUserPermissionsEffects } from './store/effects/main-user-permissions.effects';
import { MainUserPropertiesEffects } from './store/effects/main-user-properties.effects';
import { MainUsersEffects } from './store/effects/main-users.effects';
import * as fromMainUsers from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [
    MainUsersComponent,
    MainUserDeactivateDialogComponent,
    MainUserReactivateDialogComponent,
    MainUsersCreateDialogComponent,
    MainUserProfileComponent,
    MainUserPropertiesComponent,
    MainUserPermissionsComponent,
    MainListOfUsersComponent
  ],
  imports: [
    CommonModule,
    MainUsersRoutingModule,
    SharedModule,
    StoreModule.forFeature(fromMainUsers.mainUsersFeatureKey, fromMainUsers.reducers),
    EffectsModule.forFeature([
      MainUsersEffects,
      MainUserActivationDialogEffects,
      MainUserCreateDialogEffects,
      MainUserPropertiesEffects,
      MainUserPermissionsEffects,
      MainListOfUsersEffects
    ])
  ],
  providers: [
    MainUsersService,
    MainUsersFilterFormatter,
    MainUserPropertiesResolver,
    MainUserPropertiesChangesService,
    MainUserPermissionsChangesService,
    MainListOfUsersService,
    MainListOfUsersFilterFormatter
  ]
})
export class MainUsersModule {}
