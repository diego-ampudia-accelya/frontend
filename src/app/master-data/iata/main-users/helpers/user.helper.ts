import { MainUserDetails } from '../models/main-user-details.model';
import { MainUserBE } from '../models/main-users.model';

export const toMainUserUpdateModel = (user: MainUserBE): MainUserDetails => {
  const {
    portalEmail,
    name,
    contactInfo: { organization, telephone, country, city, address, postCode }
  } = user || { contactInfo: {} };

  return {
    portalEmail,
    name,
    organization,
    telephone,
    country,
    city,
    address,
    postCode
  };
};
