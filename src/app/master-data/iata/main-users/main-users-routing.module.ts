import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainListOfUsersComponent } from './components/main-list-of-users/main-list-of-users.component';
import { MainUserPermissionsComponent } from './components/main-user-permissions/main-user-permissions.component';
import { MainUserProfileComponent } from './components/main-user-profile/main-user-profile.component';
import { MainUserPropertiesComponent } from './components/main-user-properties/main-user-properties.component';
import { MainUsersComponent } from './components/main-users/main-users.component';
import { MainUserPropertiesResolver } from './services/main-user-properties.resolver';
import { Permissions, ROUTES } from '~app/shared/constants';
import { CanComponentDeactivateGuard } from '~app/core/services';

const mainUsersTabs = [
  {
    path: 'properties',
    canDeactivate: [CanComponentDeactivateGuard],
    component: MainUserPropertiesComponent,
    data: {
      title: 'MENU.USERS_MAINTENANCE.MAIN_USERS.TABS.USER_PROPERTIES',
      requiredPermissions: Permissions.readMainUsers
    }
  },
  {
    path: 'permissions',
    canDeactivate: [CanComponentDeactivateGuard],
    component: MainUserPermissionsComponent,
    data: {
      title: 'MENU.USERS_MAINTENANCE.MAIN_USERS.TABS.PERMISSIONS',
      requiredPermissions: Permissions.readMainUserPermissions
    }
  },
  {
    path: 'users-list',
    component: MainListOfUsersComponent,
    data: {
      title: 'MENU.USERS_MAINTENANCE.MAIN_USERS.TABS.LIST_OF_USERS',
      requiredPermissions: Permissions.readMainUsers
    }
  },
  { path: '', redirectTo: 'properties', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: '',
    component: MainUsersComponent,
    data: {
      tab: ROUTES.MAIN_USERS
    }
  },
  {
    path: 'details/:id',
    component: MainUserProfileComponent,
    resolve: { mainUser: MainUserPropertiesResolver },
    data: {
      tab: ROUTES.MAIN_USER_DETAILS
    },
    children: mainUsersTabs
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainUsersRoutingModule {}
