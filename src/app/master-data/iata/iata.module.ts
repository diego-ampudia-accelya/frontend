import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '~app/shared/shared.module';
import { ConfigurationModule } from '../configuration';
import { SftpModule } from '../sftp/sftp.module';
import { AccessPermissionsModule } from './access-permissions/access-permissions.module';
import { CurrencyConversionModule } from './currency-conversion/currency-convertion.module';
import { DeactivatedAgentsComponent } from './deactivated-agents/deactivated-agents.component';
import { DeactivatedAgentsFilterFormatter } from './deactivated-agents/services/deactivated-agents-filter-formatter';
import { DownloadSubUsersDialogComponent } from './donwload-sub-users/dialog/download-sub-users-dialog.component';
import { DownloadSubUsersGuard } from './donwload-sub-users/services/download-sub-users.guard';
import { DowmnloadSubUsersService } from './donwload-sub-users/services/download-sub-users.service';
import { GlobalReportsSettingsModule } from './global-reports-settings/global-reports-settings.module';
import { GlobalReportsSettingsFilterFormatter } from './global-reports-settings/services/global-reports-settings-filter-formatter';
import { GlobalReportsSettingsService } from './global-reports-settings/services/global-reports-settings.service';
import { GlobalUserFileDialogComponent } from './global-user-file/global-user-file-dialog/global-user-file-dialog.component';
import { GlobalUserFileGuard } from './global-user-file/global-user-file-guard/global-user-file-guard';
import { GlobalUserFileService } from './global-user-file/services/global-user-file.service';
import { HomuUsersModule } from './homu-users/homu-users.module';
import { IataProfileComponent } from './iata-profile/iata-profile.component';
import { IataRoutingModule } from './iata-routing.module';
import { MonitoringModule } from './monitoring/monitoring.module';
import { ReinstatedAgentsComponent } from './reinstated-agents/reinstated-agents.component';
import { ReinstatedAgentsFilterFormatter } from './reinstated-agents/services/reinstated-agents-filter-formatter';
import { CurrencyConversionResolver } from './services/currency-conversion.resolver';
import { SettingsChangesFilterFormatter } from './settings-changes/services/settings-changes-filter-formatter';
import { SettingsChangesService } from './settings-changes/services/settings-changes.service';
import { SettingsChangesComponent } from './settings-changes/settings-changes.component';
import { SuspendedAirlineFilterFormatter } from './suspended-airline/services/suspended-airline-filter-formatter';
import { SuspendedAirlineComponent } from './suspended-airline/suspended-airline.component';

@NgModule({
  providers: [
    SettingsChangesService,
    SettingsChangesFilterFormatter,
    SuspendedAirlineFilterFormatter,
    GlobalUserFileGuard,
    GlobalUserFileService,
    DeactivatedAgentsFilterFormatter,
    ReinstatedAgentsFilterFormatter,
    DownloadSubUsersGuard,
    DowmnloadSubUsersService,
    CurrencyConversionResolver,
    GlobalReportsSettingsFilterFormatter,
    GlobalReportsSettingsService
  ],
  declarations: [
    IataProfileComponent,
    SettingsChangesComponent,
    SuspendedAirlineComponent,
    GlobalUserFileDialogComponent,
    DeactivatedAgentsComponent,
    ReinstatedAgentsComponent,
    DownloadSubUsersDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IataRoutingModule,
    SftpModule,
    ConfigurationModule,
    CurrencyConversionModule,
    HomuUsersModule,
    MonitoringModule,
    AccessPermissionsModule,
    GlobalReportsSettingsModule
  ]
})
export class IataModule {}
