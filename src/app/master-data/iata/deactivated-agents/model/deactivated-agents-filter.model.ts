import { AgentSummary } from '~app/shared/models';

export interface DeactivatedAgentsFilter {
  agentCode: AgentSummary[];
  deactivationDate: Date[];
  expiryDate: Date[];
}

export interface DeactivatedAgentsFilterBE {
  agentCode: string[];
  fromDeactivationDate: string;
  toDeactivationDate: string;
  fromExpiryDate: string;
  toExpiryDate: string;
}
