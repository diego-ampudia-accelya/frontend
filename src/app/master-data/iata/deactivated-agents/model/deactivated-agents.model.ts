export interface DeactivatedAgents {
  id: number;
  agentCode: string;
  name: string;
  deactivationDate: string;
  expiryDate: string;
}
