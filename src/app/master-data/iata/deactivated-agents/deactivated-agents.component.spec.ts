import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { DeactivatedAgentsComponent } from './deactivated-agents.component';
import { DeactivatedAgentsFilterFormatter } from './services/deactivated-agents-filter-formatter';
import { DeactivatedAgentsService } from './services/deactivated-agents.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ROUTES } from '~app/shared/constants/routes';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { AgentDictionaryService, AppConfigurationService, NotificationService } from '~app/shared/services';

describe('DeactivatedAgentsComponent', () => {
  let component: DeactivatedAgentsComponent;
  let fixture: ComponentFixture<DeactivatedAgentsComponent>;
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const deactivatedAgentsServiceSpy: SpyObject<DeactivatedAgentsService> = createSpyObject(DeactivatedAgentsService);
  const queryStorageSpy: SpyObject<DefaultQueryStorage> = createSpyObject(DefaultQueryStorage);

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: { deactivatedAgentsQuery: { ...ROUTES.DEACTIVATED_AGENTS, id: 'deactivatedAgentsQuery' } },
        activeTabId: 'deactivatedAgentsQuery'
      },
      viewListsInfo: {}
    }
  };

  const queryableDataSourceSpy = createSpyObject(QueryableDataSource, {
    appliedQuery$: of(defaultQuery),
    hasData$: of(true)
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeactivatedAgentsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        NotificationService,
        AgentDictionaryService,
        AppConfigurationService,
        L10nTranslationService,
        PermissionsService,
        DeactivatedAgentsFilterFormatter,
        { provide: DialogService, useValue: dialogServiceSpy },
        FormBuilder
      ]
    })
      .overrideComponent(DeactivatedAgentsComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: queryableDataSourceSpy },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy },
            { provide: DeactivatedAgentsService, useValue: deactivatedAgentsServiceSpy },
            DatePipe
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeactivatedAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');
    component.ngOnInit();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should open download dialog correctly on download', () => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
