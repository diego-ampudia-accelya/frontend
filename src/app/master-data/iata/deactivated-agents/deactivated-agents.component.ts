import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';

import { DeactivatedAgentsFilter } from './model/deactivated-agents-filter.model';
import { DeactivatedAgents } from './model/deactivated-agents.model';
import { DeactivatedAgentsFilterFormatter } from './services/deactivated-agents-filter-formatter';
import { DeactivatedAgentsService } from './services/deactivated-agents.service';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { AgentDictionaryService } from '~app/shared/services';

@Component({
  selector: 'bspl-deactivated-agents',
  templateUrl: './deactivated-agents.component.html',
  providers: [
    DatePipe,
    QueryableDataSource,
    DefaultQueryStorage,
    { provide: QUERYABLE, useExisting: DeactivatedAgentsService }
  ]
})
export class DeactivatedAgentsComponent implements OnInit {
  public title = this.translationService.translate('MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.title');
  public searchForm: FormGroup;
  public columns: GridColumn[];
  public agentsCodeOptions$: Observable<DropdownOption<AgentSummary>[]>;

  private formFactory: FormUtil;
  private storedQuery: DataQuery<{ [key: string]: any }>;

  constructor(
    public displayFormatter: DeactivatedAgentsFilterFormatter,
    public dataSource: QueryableDataSource<DeactivatedAgents>,
    private formBuilder: FormBuilder,
    private queryStorage: DefaultQueryStorage,
    private deactivatedAgentsService: DeactivatedAgentsService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private agentsDictironayService: AgentDictionaryService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.storedQuery = this.queryStorage.get();
    this.loadData(this.storedQuery);
    this.initColumns();
    this.populateFilterDropdowns();
  }

  public loadData(query: DataQuery<DeactivatedAgentsFilter>): void {
    query = query || cloneDeep(defaultQuery);
    const dataQuery: DataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.downloadDialog.title'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.deactivatedAgentsService
    });
  }

  private populateFilterDropdowns(): void {
    this.agentsCodeOptions$ = this.agentsDictironayService.getDropdownOptions();
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<DeactivatedAgentsFilter>({
      agentCode: [],
      deactivationDate: [],
      expiryDate: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'agentCode',
        name: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.columns.agentCode'
      },
      {
        prop: 'name',
        name: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.columns.name'
      },
      {
        prop: 'deactivationDate',
        name: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.columns.deactivationDate'
      },
      {
        prop: 'expiryDate',
        name: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.columns.expiryDate'
      }
    ];
  }
}
