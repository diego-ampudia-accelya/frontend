import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DeactivatedAgentsFilter, DeactivatedAgentsFilterBE } from '../model/deactivated-agents-filter.model';
import { DeactivatedAgents } from '../model/deactivated-agents.model';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class DeactivatedAgentsService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/deactivated-agents`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query?: DataQuery<DeactivatedAgentsFilter>): Observable<PagedData<DeactivatedAgents>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<DeactivatedAgents>>(this.baseUrl + requestQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery<DeactivatedAgentsFilter>>): RequestQuery<DeactivatedAgentsFilterBE> {
    const { agentCode, deactivationDate, expiryDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        agentCode: agentCode && agentCode.map(agent => agent.code),
        fromDeactivationDate: deactivationDate && toShortIsoDate(deactivationDate[0]),
        toDeactivationDate:
          deactivationDate && toShortIsoDate(deactivationDate[1] ? deactivationDate[1] : deactivationDate[0]),
        fromExpiryDate: expiryDate && toShortIsoDate(expiryDate[0]),
        toExpiryDate: expiryDate && toShortIsoDate(expiryDate[1] ? expiryDate[1] : expiryDate[0])
      }
    });
  }

  public download(
    query: DataQuery<DeactivatedAgentsFilter>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
