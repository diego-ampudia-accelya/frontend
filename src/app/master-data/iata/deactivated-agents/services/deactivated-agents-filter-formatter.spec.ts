import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { DeactivatedAgentsFilter } from '../model/deactivated-agents-filter.model';

import { DeactivatedAgentsFilterFormatter } from './deactivated-agents-filter-formatter';

const filters: DeactivatedAgentsFilter = {
  agentCode: [
    {
      id: '1245',
      name: 'Name test',
      code: '18475'
    }
  ],
  deactivationDate: [new Date('2022-11-30')],
  expiryDate: [new Date('2022-11-30')]
};

describe('DeactivatedAgentsFilterFormatter', () => {
  let formatter: DeactivatedAgentsFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new DeactivatedAgentsFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      { keys: ['agentCode'], label: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.filter.label.agentCode - 18475' },
      {
        keys: ['deactivationDate'],
        label: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.filter.label.deactivationDate - 30/11/2022'
      },
      { keys: ['expiryDate'], label: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.filter.label.expiryDate - 30/11/2022' }
    ]);
  });
});
