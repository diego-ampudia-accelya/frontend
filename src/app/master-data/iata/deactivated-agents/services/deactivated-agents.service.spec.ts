import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { DeactivatedAgents } from '../model/deactivated-agents.model';
import { DeactivatedAgentsService } from './deactivated-agents.service';
import { DataQuery } from '~app/shared/components/list-view';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('DeactivatedAgentsService', () => {
  let service: DeactivatedAgentsService;
  let httpClientSpy: SpyObject<HttpClient>;

  const deactivatedAgentsMock: DeactivatedAgents[] = [
    {
      agentCode: '1000005',
      deactivationDate: '2022-11-30',
      expiryDate: '2023-01-26',
      id: 69831000005,
      name: 'NOMBRE 1000005'
    },
    {
      agentCode: '1000008',
      deactivationDate: '2022-11-30',
      expiryDate: '2023-01-26',
      id: 69831000008,
      name: 'NOMBRE 1000008'
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [
        DeactivatedAgentsService,
        { provide: HttpClient, useValue: httpClientSpy },
        mockProvider(AppConfigurationService, { baseApiPath: '' })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(DeactivatedAgentsService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/user-management/deactivated-agents');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let deactivatedAgentsData: PagedData<DeactivatedAgents>;

    const deactivatedAgentsDataMock: PagedData<DeactivatedAgents> = {
      records: deactivatedAgentsMock,
      total: deactivatedAgentsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(deactivatedAgentsDataMock));
    service.find(query).subscribe(res => {
      deactivatedAgentsData = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/user-management/deactivated-agents?page=0&size=20');
    expect(deactivatedAgentsData).toEqual(deactivatedAgentsDataMock);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/user-management/deactivated-agents/download?exportAs=txt';

    service.download(query, exportOptions).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
