import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { EUserType, IPermission, PermissionInputModel } from '../models/access-permissions.models';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class AccessPermissionsService {
  private baseUrl = (userType: EUserType) =>
    `${this.appConfiguration.baseApiPath}/user-management/user-types/${userType
      ?.split(' ')
      .join('_')
      .toLocaleLowerCase()}/permissions`;

  constructor(private httpClient: HttpClient, private appConfiguration: AppConfigurationService) {}

  public searchAccessPermissions(userType: EUserType): Observable<IPermission[]> {
    return this.httpClient.get<IPermission[]>(this.baseUrl(userType));
  }

  public updateAccessPermission(userType: EUserType, permissions: PermissionInputModel[]): Observable<IPermission[]> {
    return this.httpClient.put<IPermission[]>(this.baseUrl(userType), permissions);
  }
}
