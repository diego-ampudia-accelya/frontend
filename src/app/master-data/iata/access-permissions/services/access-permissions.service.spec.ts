/* eslint-disable @typescript-eslint/no-unused-vars */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AccessPermissionsService } from './access-permissions.service';
import { AppConfigurationService } from '~app/shared/services';

describe('AccessPermissionsService', () => {
  let service: AccessPermissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AccessPermissionsService,
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: ''
          }
        }
      ]
    });

    service = TestBed.inject(AccessPermissionsService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
