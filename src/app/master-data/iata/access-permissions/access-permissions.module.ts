import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AccessPermissionsComponent } from './access-permissions.component';
import { AccessPermissionsDialogComponent } from './components/access-permissions-dialog/access-permissions-dialog.component';
import { AccessPermissionsListComponent } from './components/access-permissions-list/access-permissions-list.component';
import { AccessPermissionsEffects } from './effects/access-permissions.effects';
import { AccessPermissionsGuard } from './guards/access-permissions.guard';
import { accessPermissionsReducer } from './reducers/access-permissions.reducer';
import { AccessPermissionsService } from './services/access-permissions.service';
import { AccessPermissionsStoreFacadeService } from './store/access-permissions-store-facade.service';
import { accessPermissionsStateFeatureKey } from './store/access-permissions.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(accessPermissionsStateFeatureKey, accessPermissionsReducer),
    EffectsModule.forFeature([AccessPermissionsEffects])
  ],
  declarations: [AccessPermissionsComponent, AccessPermissionsDialogComponent, AccessPermissionsListComponent],
  providers: [AccessPermissionsGuard, AccessPermissionsStoreFacadeService, AccessPermissionsService],
  exports: [AccessPermissionsComponent]
})
export class AccessPermissionsModule {}
