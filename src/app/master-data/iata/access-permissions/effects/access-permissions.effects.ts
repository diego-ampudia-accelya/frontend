import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as fromActions from '../actions/access-permissions.actions';
import { AccessPermissionsService } from '../services/access-permissions.service';
import { AccessPermissionsStoreFacadeService } from '../store/access-permissions-store-facade.service';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class AccessPermissionsEffects {
  public constructor(
    private readonly actions$: Actions,
    private readonly accessPermissionsService: AccessPermissionsService,
    private readonly accessPermissionsStoreFacadeService: AccessPermissionsStoreFacadeService,
    private readonly notificationService: NotificationService,
    private readonly translationService: L10nTranslationService
  ) {}

  public searchAccessPermissionsEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchAccessPermissionsRequest>>(fromActions.searchAccessPermissionsRequest),
      withLatestFrom(this.accessPermissionsStoreFacadeService.selectors.userType$),
      switchMap(([, userType]) =>
        this.accessPermissionsService.searchAccessPermissions(userType).pipe(
          map(permissions =>
            fromActions.searchAccessPermissionsSuccess({
              payload: { permissions }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchAccessPermissionsFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public updateAccessPermissionsEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.updateAccessPermissionsRequest>>(fromActions.updateAccessPermissionsRequest),
      withLatestFrom(this.accessPermissionsStoreFacadeService.selectors.userType$),
      switchMap(
        ([
          {
            payload: { permissions }
          },
          userType
        ]) =>
          this.accessPermissionsService.updateAccessPermission(userType, permissions).pipe(
            map(payload =>
              fromActions.updateAccessPermissionsSuccess({
                payload: { permissions: payload }
              })
            ),
            tap(() => {
              const successMessage = this.translationService.translate('access-permissions.update.message.success');
              this.notificationService.showSuccess(successMessage);
            }),
            catchError((error: HttpErrorResponse) =>
              of(
                fromActions.updateAccessPermissionsFailure({
                  payload: { error }
                })
              )
            )
          )
      )
    )
  );
}
