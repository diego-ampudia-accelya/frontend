import { createFeatureSelector, createSelector } from '@ngrx/store';
import { cloneDeep } from 'lodash';

import { mapPermissionsToSelectionList } from '../helpers/helpers';
import { AccessPermissionsState, accessPermissionsStateFeatureKey } from '../store/access-permissions.state';

export const selectAccessPermissionsFeature = createFeatureSelector<AccessPermissionsState>(
  accessPermissionsStateFeatureKey
);

export const selectUserType = createSelector(selectAccessPermissionsFeature, state => state.userType);
export const selectPermissions = createSelector(selectAccessPermissionsFeature, state => state.permissions);
export const selectSearchPermissionsLoading = createSelector(
  selectAccessPermissionsFeature,
  state => state.loading.searchPermissions
);
export const selectSearchPermissionsError = createSelector(
  selectAccessPermissionsFeature,
  state => state.error.searchPermissions
);

export const selectCategories = createSelector(selectAccessPermissionsFeature, state => state.categories);

export const selectAccessPermissionsList = createSelector(selectAccessPermissionsFeature, state =>
  state.permissions ? mapPermissionsToSelectionList(state.permissions.map(cloneDeep)) : null
);
