import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import { EUserType, IPermission, PermissionInputModel } from '../models/access-permissions.models';

export const searchAccessPermissionsRequest = createAction('[Access Permissions] Search Access Permissions Request');

export const searchAccessPermissionsSuccess = createAction(
  '[Access Permissions] Search Access Permissions Success',
  props<{ payload: { permissions: IPermission[] } }>()
);

export const searchAccessPermissionsFailure = createAction(
  '[Access Permissions] Search Access Permissions Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const setAccessPermissionsUserType = createAction(
  '[Access Permissions] Set Access Permissions User Type',
  props<{ payload: { userType: EUserType } }>()
);

export const reset = createAction('[Access Permissions] Reset');

export const updateAccessPermissionsRequest = createAction(
  '[Access Permissions] Update Access Permissions Request',
  props<{ payload: { permissions: PermissionInputModel[] } }>()
);

export const updateAccessPermissionsSuccess = createAction(
  '[Access Permissions] Update Access Permissions Success',
  props<{ payload: { permissions: IPermission[] } }>()
);

export const updateAccessPermissionsFailure = createAction(
  '[Access Permissions] Update Access Permissions Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);
