import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { AccessPermissionsDialogComponent } from '../components/access-permissions-dialog/access-permissions-dialog.component';
import { DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants';

@Injectable()
export class AccessPermissionsGuard implements CanActivate {
  constructor(
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private router: Router
  ) {}
  public canActivate(): Observable<boolean> {
    const title = this.translationService.translate('access-permissions.dialog.title');

    return this.dialogService
      .open(AccessPermissionsDialogComponent, {
        data: {
          title,
          isClosable: true,
          hasCancelButton: true,
          mainButtonType: FooterButton.Search,
          footerButtonsType: [
            {
              type: FooterButton.Search,
              isDisabled: false
            }
          ]
        }
      })
      .pipe(
        switchMap(searchAction => {
          if (searchAction.clickedBtn === FooterButton.Cancel) {
            return of(false);
          }

          if (searchAction.clickedBtn === FooterButton.Search) {
            this.router.navigateByUrl(`${ROUTES.ACCESS_PERMISSIONS.url}/list`);

            this.dialogService.close();
          }

          return EMPTY;
        })
      );
  }
}
