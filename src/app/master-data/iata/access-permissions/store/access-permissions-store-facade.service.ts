import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActions from '../actions/access-permissions.actions';
import { EUserType, PermissionInputModel } from '../models/access-permissions.models';
import * as fromSelectors from '../selectors/access-permissions.selectors';
import { AppState } from '~app/reducers';
@Injectable()
export class AccessPermissionsStoreFacadeService {
  constructor(private store: Store<AppState>) {}

  public get actions() {
    return {
      reset: () => this.store.dispatch(fromActions.reset()),
      searchAccessPermissions: () => this.store.dispatch(fromActions.searchAccessPermissionsRequest()),
      updateAccessPermissions: (permissions: PermissionInputModel[]) =>
        this.store.dispatch(
          fromActions.updateAccessPermissionsRequest({
            payload: {
              permissions
            }
          })
        ),
      setAccessPermissionsUserType: (userType: EUserType) =>
        this.store.dispatch(
          fromActions.setAccessPermissionsUserType({
            payload: {
              userType
            }
          })
        )
    };
  }

  public get selectors() {
    return {
      userType$: this.store.select(fromSelectors.selectUserType),
      permissions$: this.store.select(fromSelectors.selectPermissions),
      permissionsList$: this.store.select(fromSelectors.selectAccessPermissionsList),
      categories$: this.store.select(fromSelectors.selectCategories)
    };
  }

  public get loading() {
    return {
      searchPermissionsLoading$: this.store.select(fromSelectors.selectSearchPermissionsLoading)
    };
  }

  public get error() {
    return {
      searchPermissionsError$: this.store.select(fromSelectors.selectSearchPermissionsError)
    };
  }
}
