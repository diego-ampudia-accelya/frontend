/* eslint-disable @typescript-eslint/no-unused-vars */

import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { AccessPermissionsStoreFacadeService } from './access-permissions-store-facade.service';
import { accessPermissionsStateFeatureKey, initialState } from './access-permissions.state';

describe('AccessPermissionsStoreFacadeService', () => {
  let service: AccessPermissionsStoreFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AccessPermissionsStoreFacadeService,
        provideMockStore({
          initialState: {
            [accessPermissionsStateFeatureKey]: initialState
          }
        })
      ]
    });

    service = TestBed.inject(AccessPermissionsStoreFacadeService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
