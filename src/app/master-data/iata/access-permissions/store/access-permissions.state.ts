import { HttpErrorResponse } from '@angular/common/http';

import { EUserType, IPermission } from '../models/access-permissions.models';

export const accessPermissionsStateFeatureKey = 'accessPermissions';

export interface AccessPermissionsState {
  permissions: IPermission[];
  categories: { [key: string]: IPermission[] };
  userType: EUserType | null;
  loading: {
    searchPermissions: boolean;
    updatePermissions: boolean;
  };
  error: {
    searchPermissions: HttpErrorResponse | null;
    updatePermissions: HttpErrorResponse | null;
  };
}

export const initialState: AccessPermissionsState = {
  permissions: null,
  categories: null,
  userType: null,
  loading: {
    searchPermissions: false,
    updatePermissions: false
  },
  error: {
    searchPermissions: null,
    updatePermissions: null
  }
};
