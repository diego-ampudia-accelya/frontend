import { chain } from 'lodash';

import { IPermission } from '../models/access-permissions.models';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';

export const mapPermissionsToSelectionList = (permissions: IPermission[]): SelectionList[] =>
  chain(permissions)
    .map(permission => ({
      id: permission.id,
      name: permission.name,
      checked: permission.enabled,
      value: permission,
      hasChanged: false,
      tooltip: '',
      disabled: false
    }))
    .groupBy('value.category.name')
    .map((items, name) => ({
      name,
      items,
      tooltip: '',
      disabled: false
    }))
    .value();

export const mapSelectionListToPermissions = (selectionListItems: SelectionList[]): IPermission[] =>
  chain(selectionListItems)
    .map(catgory => catgory.items)
    .flatten()
    .map(item => ({ ...item.value, enabled: item.checked }))
    .value();
