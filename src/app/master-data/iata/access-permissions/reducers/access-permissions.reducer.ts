import { createReducer, on } from '@ngrx/store';
// import { groupBy } from '../helpers/helpers';
import { groupBy } from 'lodash';

import * as fromActions from '../actions/access-permissions.actions';
import { IPermission } from '../models/access-permissions.models';
import { initialState } from '../store/access-permissions.state';

export const accessPermissionsReducer = createReducer(
  initialState,
  on(fromActions.reset, state => ({
    ...state,
    userType: null,
    permissions: null
  })),
  on(fromActions.searchAccessPermissionsRequest, state => ({
    ...state,
    permissions: null,
    categories: null,
    loading: {
      ...state.loading,
      searchPermissions: true
    },
    error: {
      ...state.error,
      searchPermissions: null
    }
  })),
  on(fromActions.searchAccessPermissionsSuccess, (state, { payload: { permissions } }) => {
    const updatedPermissions = permissions.map(value => ({
      ...value,
      category: value['catgory']
    }));

    const categories = groupBy(updatedPermissions, p => p.category.name);

    return {
      ...state,
      permissions: updatedPermissions,
      categories: categories as unknown as { [key: string]: IPermission[] },
      loading: {
        ...state.loading,
        searchPermissions: false
      },
      error: {
        ...state.error,
        searchPermissions: null
      }
    };
  }),
  on(fromActions.searchAccessPermissionsFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchPermissions: false
    },
    error: {
      ...state.error,
      searchPermissions: error
    }
  })),
  on(fromActions.setAccessPermissionsUserType, (state, { payload: { userType } }) => ({
    ...state,
    userType
  })),
  on(fromActions.updateAccessPermissionsRequest, state => ({
    ...state,
    loading: {
      ...state.loading,
      updatePermissions: true
    },
    error: {
      ...state.error,
      updatePermissions: null
    }
  })),
  on(fromActions.updateAccessPermissionsSuccess, (state, { payload: { permissions } }) => {
    const updatedPermissions = permissions.map(value => ({
      ...value,
      category: value['catgory']
    }));

    const categories = groupBy(updatedPermissions, p => p.category.name);

    return {
      ...state,
      permissions: updatedPermissions,
      categories: categories as unknown as { [key: string]: IPermission[] },
      loading: {
        ...state.loading,
        updatePermissions: false
      },
      error: {
        ...state.error,
        updatePermissions: null
      }
    };
  }),
  on(fromActions.updateAccessPermissionsFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      updatePermissions: false
    },
    error: {
      ...state.error,
      updatePermissions: error
    }
  }))
);
