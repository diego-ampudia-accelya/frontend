import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { EUserType } from '../../models/access-permissions.models';
import { AccessPermissionsStoreFacadeService } from '../../store/access-permissions-store-facade.service';
import { DialogConfig } from '~app/shared/components';
import { ArrayHelper } from '~app/shared/helpers';

@Component({
  selector: 'bspl-access-permissions-dialog',
  templateUrl: './access-permissions-dialog.component.html',
  styleUrls: ['./access-permissions-dialog.component.scss']
})
export class AccessPermissionsDialogComponent implements OnInit, OnDestroy {
  constructor(
    public config: DialogConfig,
    private accessPermissionsStoreFacadeService: AccessPermissionsStoreFacadeService
  ) {}
  private subscription = new Subscription();

  public options = ArrayHelper.enumToDropdownOptions(EUserType);

  public formGroup: FormGroup;

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public ngOnInit() {
    this.formGroup = new FormGroup({
      userType: new FormControl(null, [Validators.required])
    });

    this.subscription.add(
      this.formGroup.controls.userType.valueChanges.subscribe(
        this.accessPermissionsStoreFacadeService.actions.setAccessPermissionsUserType
      )
    );
  }
}
