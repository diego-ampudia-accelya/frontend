/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AccessPermissionsStoreFacadeService } from '../../store/access-permissions-store-facade.service';
import { AccessPermissionsDialogComponent } from './access-permissions-dialog.component';
import { DialogConfig } from '~app/shared/components';
import { UserTypeForFilter } from '~app/shared/models/user-type-for-filter.model';
import { TranslatePipeMock } from '~app/test';

describe('AccessPermissionsDialogComponent', () => {
  let component: AccessPermissionsDialogComponent;
  let fixture: ComponentFixture<AccessPermissionsDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccessPermissionsDialogComponent, TranslatePipeMock],
      providers: [
        { provide: DialogConfig, useValue: {} },
        {
          provide: AccessPermissionsStoreFacadeService,
          useValue: {
            actions: {
              searchAccessPermissions: () => {},
              updateAccessPermissions: () => {},
              setAccessPermissionsUserType: () => {}
            },
            selectors: {
              userType$: of(UserTypeForFilter.Agent),
              permissions$: of([])
            },
            error: {
              searchPermissionsError$: of(null)
            },
            loading: {
              searchPermissionsLoading$: of(false)
            }
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessPermissionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
