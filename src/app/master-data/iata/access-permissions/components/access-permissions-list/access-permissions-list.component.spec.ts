/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { AccessPermissionsStoreFacadeService } from '../../store/access-permissions-store-facade.service';
import { AccessPermissionsListComponent } from './access-permissions-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { UserTypeForFilter } from '~app/shared/models/user-type-for-filter.model';
import { TranslatePipeMock } from '~app/test';

describe('AccessPermissionsListComponent', () => {
  let component: AccessPermissionsListComponent;
  let fixture: ComponentFixture<AccessPermissionsListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccessPermissionsListComponent, TranslatePipeMock],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => ''
          }
        },
        {
          provide: PermissionsService,
          useValue: {
            hasPermission: () => true
          }
        },
        {
          provide: AccessPermissionsStoreFacadeService,
          useValue: {
            actions: {
              searchAccessPermissions: () => {},
              updateAccessPermissions: () => {},
              setAccessPermissionsUserType: () => {}
            },
            selectors: {
              userType$: of(UserTypeForFilter.Agent),
              permissions$: of([]),
              categories$: of([]),
              permissionsList$: of([])
            },
            error: {
              searchPermissionsError$: of(null)
            },
            loading: {
              searchPermissionsLoading$: of(false)
            }
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessPermissionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
