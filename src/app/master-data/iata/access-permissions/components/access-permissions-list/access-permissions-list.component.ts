/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEqual } from 'lodash';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { filter, first, map, tap } from 'rxjs/operators';
import { Permissions } from 'src/app/shared/constants';

import { mapSelectionListToPermissions } from '../../helpers/helpers';
import {
  EUserType,
  IPermission,
  IPermissionFilters,
  PermissionInputModel
} from '../../models/access-permissions.models';
import { AccessPermissionsStoreFacadeService } from '../../store/access-permissions-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { DataQuery } from '~app/shared/components/list-view';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { ArrayHelper } from '~app/shared/helpers';

enum EPermissionStatus {
  Enabled = 'Enabled',
  Disabled = 'Disabled',
  Template = 'Template'
}

@Component({
  selector: 'bspl-access-permissions-list',
  templateUrl: './access-permissions-list.component.html',
  styleUrls: ['./access-permissions-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccessPermissionsListComponent implements OnInit, OnDestroy {
  public searchPermissionsLoading$: Observable<boolean>;

  constructor(
    private translationService: L10nTranslationService,
    private accessPermissionsStoreFacadeService: AccessPermissionsStoreFacadeService,
    private permissionsService: PermissionsService
  ) {}

  public get userType$(): Observable<EUserType> {
    return this.accessPermissionsStoreFacadeService.selectors.userType$;
  }

  public get hasDetailedUpdate$(): Observable<boolean> {
    return this.userType$.pipe(
      filter(Boolean),
      map((userType: EUserType) => userType === EUserType.Agent || userType === EUserType.Airline)
    );
  }

  public get title(): string {
    return this.translationService.translate(`access-permissions.list.title`);
  }

  public get accessPermissionsList$(): Observable<SelectionList[] | IPermission[]> {
    return this.accessPermissionsStoreFacadeService.selectors.permissionsList$;
  }

  public get accessPermissions$(): Observable<IPermission[]> {
    return this.accessPermissionsStoreFacadeService.selectors.permissions$.pipe(
      filter(Boolean),
      map(cloneDeep)
    ) as Observable<IPermission[]>;
  }

  public get hasChanges(): boolean {
    return (
      !!Object.keys(this.permissionsStatus).length && !isEqual(this.permissionsStatus, this.permissionsStatusDefault)
    );
  }

  public permissionsStatus = {};
  public permissionsStatusDefault = {};

  public canEdit: boolean;
  public isFilterExpanded: boolean;
  private subscription: Subscription = new Subscription();

  public query: DataQuery<IPermissionFilters>;
  public predefinedFilters: IPermissionFilters;

  public filterFormGroup: FormGroup;

  public userTypeOptions = ArrayHelper.enumToDropdownOptions(EUserType);
  public permissionStatusOptions = ArrayHelper.enumToDropdownOptions(EPermissionStatus);

  public handleCategoryClick(event: { items: IPermission[] }, option: EUserType) {
    event.items.forEach(({ id }) => (this.permissionsStatus[id] = option));
  }

  public onPermissionChange(modifications: SelectionList[]) {
    const permissions = mapSelectionListToPermissions(modifications);

    this.permissionsStatus = {};
    permissions.forEach(permission => {
      this.permissionsStatus[permission.id] = this.convertEnabledToPermissionStatus(permission.enabled);
    });
  }

  public onResetFilter() {
    this.accessPermissionsStoreFacadeService.actions.reset();
  }

  public convertEnabledToPermissionStatus(enabled: boolean | null): EPermissionStatus {
    if (enabled === null) {
      return EPermissionStatus.Template;
    } else if (enabled) {
      return EPermissionStatus.Enabled;
    } else {
      return EPermissionStatus.Disabled;
    }
  }

  public convertPermissionStatusToEnabled(status: EPermissionStatus): boolean | null {
    if (status === EPermissionStatus.Template) {
      return null;
    } else if (status === EPermissionStatus.Enabled) {
      return true;
    } else {
      return false;
    }
  }

  public handleClick(permission: IPermission, value: EPermissionStatus) {
    this.permissionsStatus[permission.id] = value;
  }

  public toggleFilter(): void {
    this.isFilterExpanded = !this.isFilterExpanded;
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public onFilter(): void {
    this.accessPermissionsStoreFacadeService.actions.setAccessPermissionsUserType(
      this.filterFormGroup.controls.userType.value
    );
    this.accessPermissionsStoreFacadeService.actions.searchAccessPermissions();
    this.isFilterExpanded = false;
  }

  public makeFilterTag(userType: EUserType): string {
    return `${this.translationService.translate(
      'access-permissions.list.filters.tag.user-type.label'
    )} - ${this.translationService.translate(`access-permissions.list.filters.tag.user-type.option.${userType}`)}`;
  }

  public onApplyChanges() {
    const permissionsInputModel: PermissionInputModel[] = Object.keys(this.permissionsStatus)
      .filter(Number)
      .map(permissionId => ({
        id: Number(permissionId),
        enabled: this.convertPermissionStatusToEnabled(this.permissionsStatus[permissionId])
      }));

    this.accessPermissionsStoreFacadeService.actions.updateAccessPermissions(permissionsInputModel);
  }

  public ngOnInit() {
    this.subscription.add(
      combineLatest([
        this.accessPermissionsStoreFacadeService.selectors.permissions$,
        this.accessPermissionsStoreFacadeService.selectors.categories$
      ])
        .pipe(
          filter(([permisisons, categories]) => Boolean(permisisons) && Boolean(categories)),
          tap(([permisisons, categories]) => {
            this.permissionsStatus = {};
            permisisons.forEach(permission => {
              this.permissionsStatus[permission.id] = this.convertEnabledToPermissionStatus(permission.enabled);
            });

            Object.keys(categories).forEach(key => {
              const permissionsByCategory = categories[key].map(p => this.permissionsStatus[p.id]);
              const allPermissionsAreSame = permissionsByCategory.reduce(function (a, b) {
                return a === b ? a : NaN;
              });

              this.permissionsStatus[key] = allPermissionsAreSame ? permissionsByCategory.pop() : null;
            });

            this.permissionsStatusDefault = cloneDeep(this.permissionsStatus);
          })
        )
        .subscribe()
    );

    this.searchPermissionsLoading$ = this.accessPermissionsStoreFacadeService.loading.searchPermissionsLoading$;

    this.canEdit = this.permissionsService.hasPermission(Permissions.updateMainUserPermissions);

    this.filterFormGroup = new FormGroup({
      userType: new FormControl(null, [])
    });

    this.subscription.add(
      this.filterFormGroup.controls.userType.valueChanges
        .pipe(first(Boolean))
        .subscribe(this.accessPermissionsStoreFacadeService.actions.setAccessPermissionsUserType)
    );

    this.userType$.pipe(first()).subscribe((predefinedUserType: EUserType) => {
      if (predefinedUserType) {
        this.filterFormGroup.controls.userType.patchValue(predefinedUserType);

        this.accessPermissionsStoreFacadeService.actions.searchAccessPermissions();
      }
    });
  }
}
