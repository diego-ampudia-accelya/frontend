/* eslint-disable @typescript-eslint/naming-convention */
import { UserTypeForFilter } from '~app/shared/models/user-type-for-filter.model';

export interface IPermission {
  id: number;
  code: string;
  name: string;
  category: ICategory;
  enabled: boolean | null;
  template: ETemplateType;
}

export enum ETemplateType {
  ENHANCED = 'ENHANCED',
  BASIC = 'BASIC'
}

export interface ICategory {
  id: number;
  code: string;
  name: string;
}

export interface IPermissionFilters {
  userType: UserTypeForFilter;
}

export type PermissionInputModel = Pick<IPermission, 'id' | 'enabled'>;

export enum EUserType {
  Airline = 'Airline',
  Agent = 'Agent',
  AgentGroup = 'Agent Group',
  ThirdParty = 'Third Party',
  IATA = 'IATA',
  DPC = 'DPC',
  GDS = 'GDS'
}
