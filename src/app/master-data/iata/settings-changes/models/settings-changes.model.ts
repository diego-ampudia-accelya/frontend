export interface SettingsChanges {
  module: string;
  parameter: string;
  dateTime: string;
  oldValue: string;
  newValue: string;
  userName: string;
  email: string;
}

export interface SettingsChangesFilter {
  userName: string;
  module: string;
  parameter: string;
}
