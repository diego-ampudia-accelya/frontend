import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { AppliedFilter } from '~app/shared/components/list-view';
import { SettingsChangesFilter } from '../models/settings-changes.model';
import { SettingsChangesFilterFormatter } from './settings-changes-filter-formatter';

type DisplayFilterConfig<T> = {
  [P in keyof T]?: {
    label?: string;
    format?(value: T[P]): string | string[];
  };
};

const filters = {
  userName: 'User Name',
  module: 'Module',
  parameter: 'Parameter'
} as SettingsChangesFilter;

const configuration: DisplayFilterConfig<SettingsChangesFilter> = {
  module: { label: 'menu.masterData.settingsChanges.filters.labels.module' },
  parameter: { label: 'menu.masterData.settingsChanges.filters.labels.parameter' },
  userName: { label: 'menu.masterData.settingsChanges.filters.labels.userName' }
};

describe('SettingsChangesFilterFormatter', () => {
  let formatter: SettingsChangesFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new SettingsChangesFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const formattedFilters = formatter.format(filters);
    const expectedFilters: AppliedFilter[] = [
      {
        label: `${configuration.module.label} - ${filters.module}`,
        keys: ['module']
      },
      {
        label: `${configuration.parameter.label} - ${filters.parameter}`,
        keys: ['parameter']
      },
      {
        label: `${configuration.userName.label} - ${filters.userName}`,
        keys: ['userName']
      }
    ];
    expect(formattedFilters).toEqual(expectedFilters);
  });
});
