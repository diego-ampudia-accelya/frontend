import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AppConfigurationService } from '~app/shared/services';
import { SettingsChangesService } from './settings-changes.service';

describe('SettingsChangesService', () => {
  const appConfigurationService = createSpyObject(AppConfigurationService);
  const mockBsp = { id: 1 };

  let service: SettingsChangesService;
  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        SettingsChangesService,
        { provide: AppConfigurationService, useValue: appConfigurationService },
        provideMockStore({ initialState: {}, selectors: [{ selector: getUserDefaultBsp, value: mockBsp }] }),
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });
    service = TestBed.inject(SettingsChangesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize baseUrl correctly', () => {
    expect(service['baseUrl']).toBe(
      `${appConfigurationService.baseApiPath}/bsp-management/bsps/${mockBsp.id}/configuration/history`
    );
  });

  it('should trigger find method and return http get request method', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(of([]));
    service.find(defaultQuery).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalled();
  }));
});
