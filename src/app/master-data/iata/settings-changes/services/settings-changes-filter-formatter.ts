import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { SettingsChangesFilter } from '../models/settings-changes.model';
import { AppliedFilter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';

type DisplayFilterConfig<T> = {
  [P in keyof T]?: {
    label?: string;
    format?(value: T[P]): string | string[];
  };
};

@Injectable()
export class SettingsChangesFilterFormatter {
  private configuration: DisplayFilterConfig<SettingsChangesFilter> = {
    module: { label: 'menu.masterData.settingsChanges.filters.labels.module' },
    parameter: { label: 'menu.masterData.settingsChanges.filters.labels.parameter' },
    userName: { label: 'menu.masterData.settingsChanges.filters.labels.userName' }
  };

  constructor(private translation: L10nTranslationService) {}

  public format(filters: SettingsChangesFilter): AppliedFilter[] {
    return Object.entries(this.configuration)
      .filter(([filterKey]) => filters && !isEmpty(filters[filterKey]))
      .map(([filterKey, config]) => {
        const formatValue = config.format || identity;
        let formattedValue = formatValue(filters[filterKey]);
        if (Array.isArray(formattedValue)) {
          formattedValue = formattedValue.join(', ');
        }

        let label = formattedValue;
        if (config.label) {
          label = `${this.translation.translate(config.label)} - ${formattedValue}`;
        }

        return { label, keys: [filterKey] };
      });
  }
}
