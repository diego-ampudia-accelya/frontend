import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { SettingsChanges, SettingsChangesFilter } from '../models/settings-changes.model';
import { getUserDefaultBsp } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class SettingsChangesService implements Queryable<SettingsChanges> {
  private defaultIsoc: number;

  private get baseUrl(): string {
    return `${this.appConfiguration.baseApiPath}/bsp-management/bsps/${this.defaultIsoc}/configuration/history`;
  }

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {
    this.initializeDefaultBsp();
  }

  public find(query: DataQuery<SettingsChangesFilter>): Observable<PagedData<SettingsChanges>> {
    const requestQuery = RequestQuery.fromDataQuery(query);

    return this.http.get<PagedData<SettingsChanges>>(this.baseUrl + requestQuery.getQueryString());
  }

  private initializeDefaultBsp(): void {
    this.store
      .select(getUserDefaultBsp)
      .pipe(first())
      .subscribe(defaultBsp => (this.defaultIsoc = defaultBsp.id));
  }
}
