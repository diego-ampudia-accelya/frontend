import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { cloneDeep, isEmpty } from 'lodash';

import { SettingsChanges, SettingsChangesFilter } from './models/settings-changes.model';
import { SettingsChangesFilterFormatter } from './services/settings-changes-filter-formatter';
import { SettingsChangesService } from './services/settings-changes.service';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { GridColumn } from '~app/shared/models';

@Component({
  selector: 'bspl-settings-changes',
  templateUrl: './settings-changes.component.html',
  providers: [DefaultQueryStorage, QueryableDataSource, { provide: QUERYABLE, useExisting: SettingsChangesService }]
})
export class SettingsChangesComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: Array<GridColumn>;

  private formFactory: FormUtil;

  constructor(
    public dataSource: QueryableDataSource<SettingsChanges>,
    public displayFilterFormatter: SettingsChangesFilterFormatter,
    private datePipe: DatePipe,
    private queryStorage: DefaultQueryStorage,
    private formBuilder: FormBuilder
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildSearchForm();
    this.columns = this.buildColumns();

    const storedQuery = this.queryStorage.get();
    this.loadData(storedQuery);
  }

  public loadData(query?: DataQuery): void {
    query = query || cloneDeep(defaultQuery);

    let dataQuery: DataQuery = {
      ...query
    };

    if (isEmpty(dataQuery.sortBy)) {
      const sortBy = [{ attribute: 'dateTime', sortType: SortOrder.Desc }];
      dataQuery = { ...dataQuery, sortBy };
    }

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<SettingsChangesFilter>({
      userName: [],
      module: [],
      parameter: []
    });
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: 'menu.masterData.settingsChanges.columns.module',
        prop: 'module',
        width: 50,
        sortable: true
      },
      {
        name: 'menu.masterData.settingsChanges.columns.parameter',
        prop: 'parameter',
        width: 50,
        sortable: true
      },
      {
        name: 'menu.masterData.settingsChanges.columns.dateTime',
        prop: 'dateTime',
        width: 50,
        sortable: true,
        pipe: {
          transform: value => this.datePipe.transform(value, 'dd/MM/yyyy HH:mm:ss')
        }
      },
      {
        name: 'menu.masterData.settingsChanges.columns.oldValue',
        prop: 'oldValue',
        width: 50,
        sortable: false
      },
      {
        name: 'menu.masterData.settingsChanges.columns.newValue',
        prop: 'newValue',
        width: 50,
        sortable: false
      },
      {
        name: 'menu.masterData.settingsChanges.columns.userName',
        prop: 'userName',
        width: 50,
        sortable: true
      },
      {
        name: 'menu.masterData.settingsChanges.columns.email',
        prop: 'email',
        width: 50,
        sortable: false
      }
    ];
  }
}
