import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';

import { SettingsChangesFilterFormatter } from './services/settings-changes-filter-formatter';
import { SettingsChangesService } from './services/settings-changes.service';
import { SettingsChangesComponent } from './settings-changes.component';
import { DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';

describe('SettingsChangesComponent', () => {
  const settingsChangesService = createSpyObject(SettingsChangesService);
  const settingsChangesFilterFormatter = createSpyObject(SettingsChangesFilterFormatter);
  const queryableDataSource = createSpyObject(QueryableDataSource);
  const defaultQueryStorage = createSpyObject(DefaultQueryStorage);

  let component: SettingsChangesComponent;
  let fixture: ComponentFixture<SettingsChangesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SettingsChangesComponent, MockPipe(L10nTranslatePipe)],
      providers: [
        { provide: SettingsChangesService, useValue: settingsChangesService },
        { provide: SettingsChangesFilterFormatter, useValue: settingsChangesFilterFormatter },
        mockProvider(FormBuilder),
        mockProvider(DatePipe),
        provideMockStore({})
      ]
    })
      .overrideComponent(SettingsChangesComponent, {
        set: {
          providers: [
            { provide: DefaultQueryStorage, useValue: defaultQueryStorage },
            { provide: QueryableDataSource, useValue: queryableDataSource },
            { provide: QUERYABLE, useValue: settingsChangesService }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load search query', () => {
    const loadDataSpy = spyOn(component, 'loadData');

    component.ngOnInit();

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should sort by dateTime DESC', () => {
    queryableDataSource.get.and.returnValue(null);

    component.loadData();

    expect(defaultQueryStorage.save).toHaveBeenCalledWith({
      ...defaultQuery,
      sortBy: [{ attribute: 'dateTime', sortType: SortOrder.Desc }]
    });
  });
});
