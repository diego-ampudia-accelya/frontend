export interface DownloadSubUsersViewModel {
  queryByUsersType: string;
  exportAs: string;
  requestDate: string;
}
export interface DownloadSubUsers {
  queryByUsersType: string;
  requestDate: string;
}
