import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';

import { DownloadSubUsersDialogComponent } from '../dialog/download-sub-users-dialog.component';
import { DialogService, FooterButton } from '~app/shared/components';

@Injectable()
export class DownloadSubUsersGuard implements CanActivate {
  constructor(private dialogService: DialogService, private translationService: L10nTranslationService) {}

  canActivate(): Observable<boolean> {
    return this.dialogService.open(DownloadSubUsersDialogComponent, {
      data: {
        title: this.translationService.translate('MENU.USERS_MAINTENANCE.DOWNLOAD_SUB_USERS.TAB'),
        footerButtonsType: FooterButton.Download
      }
    });
  }
}
