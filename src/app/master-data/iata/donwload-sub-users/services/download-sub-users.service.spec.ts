import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { DownloadSubUsers } from '../model/download-sub-users.model';
import { DowmnloadSubUsersService } from './download-sub-users.service';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

const filterByQuery: DownloadSubUsers = {
  queryByUsersType: 'activeUsers',
  requestDate: '2020-02'
};

describe('DowmnloadSubUsersService', () => {
  let dowmnloadSubUsersService: DowmnloadSubUsersService;
  let httpSpy: SpyObject<HttpClient>;
  let appConfigurationSpy: SpyObject<AppConfigurationService>;

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpSpy = createSpyObject(HttpClient);
    httpSpy.post.and.returnValue(of(httpResponse));
    appConfigurationSpy = createSpyObject(AppConfigurationService, { baseApiPath: '' });

    TestBed.configureTestingModule({
      providers: [
        DowmnloadSubUsersService,
        { provide: HttpClient, useValue: httpSpy },
        { provide: AppConfigurationService, useValue: appConfigurationSpy }
      ]
    });

    dowmnloadSubUsersService = TestBed.inject(DowmnloadSubUsersService);
  });

  it('should create', () => {
    expect(dowmnloadSubUsersService).toBeDefined();
  });

  it('should send proper request when download() is called', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/file-management/subusers-file/download?exportAs=txt';

    dowmnloadSubUsersService.download(filterByQuery, exportOptions).subscribe();

    expect(httpSpy.post).toHaveBeenCalledWith(expectedUrl, filterByQuery, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
