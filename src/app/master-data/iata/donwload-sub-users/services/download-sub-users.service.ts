import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DownloadSubUsers } from '../model/download-sub-users.model';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class DowmnloadSubUsersService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/subusers-file/download`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public download(
    filters: DownloadSubUsers,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}?exportAs=${downloadFormat}`;

    return this.http
      .post<HttpResponse<ArrayBuffer>>(url, filters, downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }
}
