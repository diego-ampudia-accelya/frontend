/* eslint-disable @typescript-eslint/naming-convention */
export const DOWNLOAD_SUB_USERS_FORM = {
  DOWNLOAD_OPTIONS: [
    {
      value: 'txt',
      label: 'TXT'
    },
    {
      value: 'csv',
      label: 'CSV'
    }
  ],
  ACTIVE_ALL_OPTIONS: [
    {
      value: 'activeUsers',
      label: 'Active Users'
    },
    {
      value: 'allUsers',
      label: 'All Users'
    }
  ]
};
