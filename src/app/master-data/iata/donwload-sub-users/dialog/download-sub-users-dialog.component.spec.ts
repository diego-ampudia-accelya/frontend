import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, flush, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { DowmnloadSubUsersService } from '../services/download-sub-users.service';
import { DownloadSubUsersDialogComponent } from './download-sub-users-dialog.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { TranslatePipeMock } from '~app/test';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Download,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel'
      },
      {
        title: 'BUTTON.DEFAULT.DOWNLOAD',
        buttonDesign: 'primary',
        type: 'download',
        isDisabled: true
      }
    ]
  }
} as DialogConfig;

describe('DownloadSubUsersDialogComponent', () => {
  let component: DownloadSubUsersDialogComponent;
  let fixture: ComponentFixture<DownloadSubUsersDialogComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let dowmnloadSubUsersServiceSpy: SpyObject<DowmnloadSubUsersService>;
  const notificationServiceSpy = createSpyObject(NotificationService);

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    dowmnloadSubUsersServiceSpy = createSpyObject(DowmnloadSubUsersService);

    TestBed.configureTestingModule({
      declarations: [DownloadSubUsersDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        mockProvider(L10nTranslationService),
        FormBuilder,
        ReactiveSubject,
        LocalBspTimeService,
        AppConfigurationService,
        HttpClient,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: DowmnloadSubUsersService, useValue: dowmnloadSubUsersServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadSubUsersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should disable downloadButton when form is invalid', fakeAsync(() => {
    const queryByUsersTypeControl = component['queryByUsersTypeControl'];

    component.ngAfterViewInit();

    tick();

    queryByUsersTypeControl.setValue(null);
    expect(component['downloadButton'].isDisabled).toBeTruthy();
  }));

  it('should initialize Button Listeners', fakeAsync(() => {
    spyOn<any>(component, 'requestDownloadFile').and.returnValue(of({ blob: new Blob(), fileName: 'test.txt' }));

    component['downloadButtonClick$'] = of({});
    component['downloadButtonClick$'].pipe(take(1)).subscribe();

    component['initializeButtonListeners']();
    flush();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
  }));

  it('should call download when requestDownloadFile is called', () => {
    component['requestDownloadFile']();

    expect(dowmnloadSubUsersServiceSpy.download).toHaveBeenCalled();
  });
});
