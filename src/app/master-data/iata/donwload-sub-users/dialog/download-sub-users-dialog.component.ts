import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import * as FileSaver from 'file-saver';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';

import { DownloadSubUsers, DownloadSubUsersViewModel } from '../model/download-sub-users.model';
import { DowmnloadSubUsersService } from '../services/download-sub-users.service';
import { DOWNLOAD_SUB_USERS_FORM } from './download-sub-users-dialog.constants';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AlertMessageType } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { toShotIsoYearMonth } from '~app/shared/helpers/datesHelper';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { RadioButton } from '~app/shared/models/radio-button.model';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-download-sub-users-dialog',
  templateUrl: './download-sub-users-dialog.component.html',
  styleUrls: ['./download-sub-users-dialog.component.scss']
})
export class DownloadSubUsersDialogComponent implements AfterViewInit, OnInit, OnDestroy {
  public form: FormGroup;
  public queryByUsersTypeOptions: RadioButton[] = DOWNLOAD_SUB_USERS_FORM.ACTIVE_ALL_OPTIONS;
  public exportAsOptions: RadioButton[] = DOWNLOAD_SUB_USERS_FORM.DOWNLOAD_OPTIONS;
  public disclaimerType = AlertMessageType.info;

  private formFactory: FormUtil;
  private downloadButton: ModalAction;

  private queryByUsersTypeControl: AbstractControl;

  private destroy$ = new Subject();

  private downloadButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Download),
    takeUntil(this.destroy$)
  );

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private fb: FormBuilder,
    private dowmnloadSubUsersService: DowmnloadSubUsersService,
    private localBspTimeService: LocalBspTimeService
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.buildForm();
    this.initializeFormFeatures();
    this.initializeRadioButtonsValue();
    this.initializeCurrentMonthValue();
    this.initializeButtons();
    this.initializeButtonListeners();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const isFormValid = status === 'VALID';

      if (this.downloadButton) {
        this.downloadButton.isDisabled = !isFormValid;
      }
    });
  }

  private buildForm(): void {
    this.form = this.formFactory.createGroup<DownloadSubUsersViewModel>({
      queryByUsersType: [null, Validators.required],
      exportAs: [null, Validators.required],
      requestDate: [null, Validators.required]
    });
  }

  private initializeFormFeatures(): void {
    this.queryByUsersTypeControl = FormUtil.get<DownloadSubUsersViewModel>(this.form, 'queryByUsersType');
  }

  private initializeRadioButtonsValue(): void {
    const formControlValue = this.queryByUsersTypeOptions[0].value;
    this.queryByUsersTypeControl.patchValue(formControlValue);
  }

  private initializeCurrentMonthValue(): void {
    const currentDate = this.localBspTimeService.getDate();
    const requestDateControl = FormUtil.get<DownloadSubUsersViewModel>(this.form, 'requestDate');

    requestDateControl.patchValue(toShotIsoYearMonth(currentDate));
  }

  private initializeButtons(): void {
    this.downloadButton = this.config.data.buttons.find(button => button.type === FooterButton.Download);
    if (this.downloadButton) {
      this.downloadButton.isDisabled = true;
    }
  }

  private initializeButtonListeners(): void {
    this.downloadButtonClick$
      .pipe(
        switchMap(() => this.requestDownloadFile()),
        takeUntil(this.destroy$)
      )
      .subscribe(fileInfo => {
        FileSaver.saveAs(fileInfo.blob, fileInfo.fileName);
        this.dialogService.close();
        this.notificationService.showSuccess(
          this.translationService.translate('MENU.USERS_MAINTENANCE.DOWNLOAD_SUB_USERS.successMessage')
        );
      });
  }

  private requestDownloadFile(): Observable<{ blob: Blob; fileName: string }> {
    this.notificationService.showInformation('downloadDialog.inProcess');
    this.downloadButton.isLoading = true;

    const exportAsControl = FormUtil.get<DownloadSubUsersViewModel>(this.form, 'exportAs');
    const requestDateControl = FormUtil.get<DownloadSubUsersViewModel>(this.form, 'requestDate');

    const paramsToRequest: DownloadSubUsers = {
      queryByUsersType: this.queryByUsersTypeControl.value,
      requestDate: toShotIsoYearMonth(requestDateControl.value)
    };

    return this.dowmnloadSubUsersService.download(paramsToRequest, exportAsControl.value);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
