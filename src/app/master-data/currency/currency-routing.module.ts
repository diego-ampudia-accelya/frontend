import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CurrencyListComponent } from '~app/master-data/currency/currency-list/currency-list.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: CurrencyListComponent,
    data: {
      tab: ROUTES.CURRENCY
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyRoutingModule {}
