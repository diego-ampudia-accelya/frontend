import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CurrencyAddFormComponent } from '~app/master-data/currency/currency-dialog/currency-add-form/currency-add-form.component';
import { CurrencyDeleteFormComponent } from '~app/master-data/currency/currency-dialog/currency-delete-form/currency-delete-form.component';
import { CurrencyListComponent } from '~app/master-data/currency/currency-list/currency-list.component';
import { CurrencyRoutingModule } from '~app/master-data/currency/currency-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [CurrencyListComponent, CurrencyAddFormComponent, CurrencyDeleteFormComponent],
  imports: [CommonModule, CurrencyRoutingModule, SharedModule]
})
export class CurrencyModule {}
