import { Component, ViewChild } from '@angular/core';

import { CurrencyService } from '~app/master-data/services/currency.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ViewListComponent } from '~app/shared/components';
import { Currency } from '~app/shared/models/currency.model';
import { DataSource } from '~app/shared/services/data-source.service';

@Component({
  selector: 'bspl-currency-list',
  templateUrl: './currency-list.component.html'
})
export class CurrencyListComponent {
  @ViewChild('currencyList', { static: true }) currencyList: ViewListComponent;

  public dataSource = new DataSource<Currency>(this.currencyService);
  public currentItem;
  public masterDataType = MasterDataType.CURRENCY;

  constructor(public currencyService: CurrencyService) {}
}
