import { MasterDataType, MasterTableConfig } from '~app/master-shared/models/master.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

export const CURRENCY_LIST: MasterTableConfig = {
  get: {
    get [MasterDataType.CURRENCY]() {
      return {
        COLUMNS: [
          {
            prop: 'code',
            name: 'LIST.MASTER_DATA.CURRENCY.COLUMNS.CODE',
            resizeable: false,
            draggable: false,
            minWidth: 100
          },
          {
            prop: 'decimals',
            name: 'LIST.MASTER_DATA.CURRENCY.COLUMNS.DECIMALS',
            resizeable: false,
            draggable: false
          }
        ],
        ACTIONS: [GridTableActionType.Delete],
        PRIMARY_ACTIONS: [{ action: GridTableActionType.Edit }]
      };
    }
  }
};
