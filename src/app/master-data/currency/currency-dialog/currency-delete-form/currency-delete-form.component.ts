import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { CurrencyService } from '~app/master-data/services/currency.service';
import { ErrorMessageService } from '~app/master-data/services/error-message.service';
import { MasterDialogForm } from '~app/master-shared/master-dialog-form/master-dialog-form.class';
import { MasterDataForRequest } from '~app/master-shared/models/master.model';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-currency-delete-form',
  templateUrl: './currency-delete-form.component.html',
  styleUrls: ['./currency-delete-form.component.scss']
})
export class CurrencyDeleteFormComponent extends MasterDialogForm implements OnInit {
  public form: FormGroup; // Not used. Only to satisfy parent class
  protected formConstants; // Not used. Only to satisfy parent class

  // Template variables
  public question;
  public code;

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public currencyService: CurrencyService,
    public dialogService: DialogService,
    public notificationService: NotificationService,
    public errorMessageService: ErrorMessageService,
    public translationService: L10nTranslationService
  ) {
    super(
      config,
      currencyService,
      reactiveSubject,
      dialogService,
      notificationService,
      errorMessageService,
      translationService
    );
  }

  ngOnInit() {
    super.setDialogClickCallback();
    this.question = this.dialogInjectedData.question;
    this.code = this.dialogInjectedData.rowTableContent.code;
  }

  public generateDataForRequestByAction(action?: any): MasterDataForRequest {
    return { id: this.dialogInjectedData.rowTableContent.id };
  }
}
