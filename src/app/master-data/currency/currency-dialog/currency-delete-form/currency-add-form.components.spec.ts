import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { CurrencyDeleteFormComponent } from './currency-delete-form.component';
import { AuthState } from '~app/auth/reducers/auth.reducer';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';

const dialogInjectedDataMock = {
  question: 'Are you sure?',
  rowTableContent: { code: 'BGN', id: 2345 },
  gridTable: null,
  title: 'Title',
  footerButtonsType: FooterButton.Update,
  actionType: GridTableActionType.Edit,
  dataType: null,
  rowSourceTableContent: null
};

const dialogConfig: DialogConfig = {
  data: dialogInjectedDataMock
};

describe('CurrencyDeleteFormComponent', () => {
  let component: CurrencyDeleteFormComponent;
  let fixture: ComponentFixture<CurrencyDeleteFormComponent>;
  const notificationServiceSpy = createSpyObject(NotificationService);
  const initialState: { auth: AuthState } = {
    auth: {
      user: {} as User
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CurrencyDeleteFormComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        L10nTranslationModule.forRoot(l10nConfig),
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [
        DialogService,
        {
          provide: DialogConfig,
          useValue: dialogConfig
        },
        { provide: NotificationService, useValue: notificationServiceSpy },
        provideMockStore({ initialState }),
        ReactiveSubject,
        L10nTranslationService,
        FormBuilder
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyDeleteFormComponent);
    component = fixture.componentInstance;
    component['dialogInjectedData'] = dialogInjectedDataMock;
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should set correct component data from injected dialog data', () => {
    component.ngOnInit();
    expect(component.question).toBe('Are you sure?');
    expect(component.code).toBe('BGN');
  });

  it('should generate data for request', () => {
    const dataForRequest = component.generateDataForRequestByAction({ clickedBtn: 'update' });

    expect(dataForRequest).toEqual({
      id: 2345
    });
  });
});
