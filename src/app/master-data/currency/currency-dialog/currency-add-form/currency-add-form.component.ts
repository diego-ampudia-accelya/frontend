import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { CURRENCY_FORM } from '~app/master-data/currency/currency-dialog/constants/currency-form.constants';
import { CurrencyService } from '~app/master-data/services/currency.service';
import { ErrorMessageService } from '~app/master-data/services/error-message.service';
import { MasterDialogForm } from '~app/master-shared/master-dialog-form/master-dialog-form.class';
import { MasterDataForRequest } from '~app/master-shared/models/master.model';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { InputComponent } from '~app/shared/components/input/input.component';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-currency-add-form',
  templateUrl: './currency-add-form.component.html',
  styleUrls: ['./currency-add-form.component.scss']
})
export class CurrencyAddFormComponent extends MasterDialogForm implements OnInit {
  @ViewChildren(InputComponent) bsplInputs: QueryList<InputComponent>;
  protected formConstants;
  public form: FormGroup;

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public errorMessageService: ErrorMessageService,
    public currencyService: CurrencyService,
    public dialogService: DialogService,
    public notificationService: NotificationService,
    public translationService: L10nTranslationService,
    private formBuilder: FormBuilder
  ) {
    super(
      config,
      currencyService,
      reactiveSubject,
      dialogService,
      notificationService,
      errorMessageService,
      translationService
    );
    this.formConstants = CURRENCY_FORM;
  }

  ngOnInit() {
    this.createForm();
    super.setDialogClickCallback();
  }

  createForm() {
    this.form = this.formBuilder.group({
      code: [this.dialogInjectedData.rowTableContent.code, [Validators.required, Validators.pattern('[A-Z]{3}')]],
      decimals: [this.dialogInjectedData.rowTableContent.decimals, [Validators.required, Validators.pattern('[0-9]')]]
    });
  }

  //  TODO This method should be generic (moved to MasterDialogForm)
  //  TODO It should simply send the form?
  //  TODO action param should have type
  public generateDataForRequestByAction(action?: any): MasterDataForRequest {
    const dataForRequest = { ...this.form.value };

    if (action.clickedBtn === FooterButton.Update) {
      dataForRequest.id = this.dialogInjectedData.rowTableContent.id;
    }

    return dataForRequest;
  }
}
