import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { CurrencyAddFormComponent } from './currency-add-form.component';
import { AuthState } from '~app/auth/reducers/auth.reducer';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';

const dialogInjectedDataMock = {
  rowTableContent: {
    id: 2345,
    code: 'BGN',
    devimals: 3
  },
  gridTable: null,
  title: 'Title',
  question: '',
  footerButtonsType: FooterButton.Update,
  actionType: GridTableActionType.Edit,
  dataType: null,
  rowSourceTableContent: null
};

const dialogConfig: DialogConfig = {
  data: dialogInjectedDataMock
};

describe('CurrencyAddFormComponent', () => {
  let component: CurrencyAddFormComponent;
  let fixture: ComponentFixture<CurrencyAddFormComponent>;
  const notificationServiceSpy = createSpyObject(NotificationService);
  const initialState: { auth: AuthState } = {
    auth: {
      user: {} as User
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CurrencyAddFormComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        L10nTranslationModule.forRoot(l10nConfig),
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [
        DialogService,
        {
          provide: DialogConfig,
          useValue: dialogConfig
        },
        { provide: NotificationService, useValue: notificationServiceSpy },
        provideMockStore({ initialState }),
        ReactiveSubject,
        L10nTranslationService,
        FormBuilder
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyAddFormComponent);
    component = fixture.componentInstance;
    component['dialogInjectedData'] = dialogInjectedDataMock;
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize form on createForm', () => {
    expect(component.form).not.toBeDefined();
    component.createForm();
    expect(component.form).toBeDefined();
  });

  it('should generate data for request when clicking update action', () => {
    component.createForm();
    component.form.get('code').setValue('BGN');
    component.form.get('decimals').setValue(4);

    const dataForRequest = component.generateDataForRequestByAction({ clickedBtn: 'update' });

    expect(dataForRequest).toEqual({
      id: 2345,
      code: 'BGN',
      decimals: 4
    });
  });
});
