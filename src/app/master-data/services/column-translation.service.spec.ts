import { waitForAsync } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';
import { ReplaySubject } from 'rxjs';

import { ColumnTranslationService } from './column-translation.service';

describe('ColumnTranslationService', () => {
  let service: ColumnTranslationService;
  let translationStub: jasmine.SpyObj<L10nTranslationService>;
  let language$: ReplaySubject<string>;

  beforeEach(() => {
    language$ = new ReplaySubject(1);
    translationStub = jasmine.createSpyObj('L10nTranslationService', ['translate', 'onChange']);
    translationStub.onChange.and.returnValue(language$.asObservable());
    translationStub.translate.and.returnValue('translated');

    service = new ColumnTranslationService(translationStub);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should translate column headers when translation changes', waitForAsync(() => {
    service.translate([{ name: 'test' }]).subscribe();

    language$.next('en');
    language$.next('de');

    expect(translationStub.translate).toHaveBeenCalledTimes(2);
  }));

  it('should translate column headers with existing translation', waitForAsync(() => {
    language$.next('en');

    service.translate([{ name: 'test' }]).subscribe();

    expect(translationStub.translate).toHaveBeenCalledTimes(1);
  }));

  it('should ignore new language when it is same as previous language', waitForAsync(() => {
    language$.next('en');
    service.translate([{ name: 'test' }]).subscribe();

    language$.next('en');

    expect(translationStub.translate).toHaveBeenCalledTimes(1);
  }));
});
