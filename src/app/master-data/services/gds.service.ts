import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { chain } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Gds } from '~app/master-data/models/gds.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { ApiService } from '~app/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class GdsService extends ApiService<Gds> {
  constructor(http: HttpClient) {
    super(http, 'gds');
  }

  public getCodeNameDropdownOptions(valueField: string): Observable<DropdownOption[]> {
    return this.getAllItems().pipe(
      map(gdsItems =>
        chain(gdsItems)
          .orderBy('gdsCode')
          .map(gds => ({
            value: gds[valueField],
            label: `${gds.gdsCode} / ${gds.name}`
          }))
          .value()
      )
    );
  }

  public getByAgentCodeNameDropdownOptions(): Observable<DropdownOption[]> {
    return this.getAllItems().pipe(
      map(gdsItems =>
        chain(gdsItems)
          .orderBy('gdsCode')
          .map(gds => ({
            value: gds,
            label: `${gds.gdsCode} / ${gds.name}`
          }))
          .value()
      )
    );
  }
}
