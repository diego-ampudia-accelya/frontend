import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { GlobalAirlineFilter } from '../global-airline/models/global-airline-list.model';
import { AirlineService } from './airline.service';
import { GlobalAirline, GlobalAirlineTemplate } from '~app/master-data/models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { RequestMethod } from '~app/shared/enums/request-method.enum';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalAirlineService extends ApiService<GlobalAirline> implements Queryable<GlobalAirline> {
  constructor(public http: HttpClient, private airlineService: AirlineService) {
    super(http, '');
  }

  public get baseUrl(): string {
    return `${appConfiguration.baseApiPath}/airline-management/global-airlines`;
  }

  public find(query: DataQuery): Observable<PagedData<GlobalAirline>> {
    const requestQuery = this.formatFilter(query);

    return this.http.get<any>(this.baseUrl + requestQuery.getQueryString());
  }

  // TODO: Move/refactor common logic from getOne and getBy methods.
  public getOne(id: number, includeAirlines = false): Observable<GlobalAirline> {
    if (includeAirlines) {
      return super.getOne(id).pipe(
        switchMap(globalAirline => this.airlineService.getBy(globalAirline.id)),
        map(airlines => ({ ...airlines[0].globalAirline, airlines } as GlobalAirline))
      );
    }

    return super.getOne(id);
  }

  @Cacheable()
  public getBy(localAirlineId: number, includeAirlines = false): Observable<GlobalAirline> {
    const globalAirline$ = this.airlineService.getOne(localAirlineId).pipe(map(airline => airline.globalAirline));

    if (includeAirlines) {
      return globalAirline$.pipe(
        switchMap(globalAirline => this.airlineService.getBy(globalAirline.id)),
        map(airlines => ({ ...airlines[0].globalAirline, airlines }))
      );
    }

    return globalAirline$;
  }

  public getAll(requestQuery: RequestQuery): Observable<PagedData<GlobalAirline>> {
    requestQuery.reduceFilterBy = true;

    return super.getAll(requestQuery);
  }

  @Cacheable()
  public getDropDownOptions(): Observable<DropdownOption[]> {
    return this.getAllItems().pipe(
      map(globalAirlines =>
        globalAirlines.map(globalAirline => ({
          value: globalAirline.id,
          label: `${globalAirline.iataCode} / ${globalAirline.globalName}`
        }))
      )
    );
  }

  @Cacheable()
  public getIataCodeDropdownOptions(): Observable<DropdownOption[]> {
    return this.getAllItems().pipe(
      map(globalAirlines =>
        globalAirlines.map(globalAirline => ({
          value: globalAirline.iataCode,
          label: `${globalAirline.iataCode} / ${globalAirline.globalName}`
        }))
      )
    );
  }

  public updateTemplate(globalAirlineId: number, template: GlobalAirlineTemplate): Observable<GlobalAirline> {
    return this.http.put<GlobalAirline>(`${this.baseUrl}/${globalAirlineId}`, { template });
  }

  public download(query: any, format: string): Observable<{ blob: Blob; fileName: string }> {
    const queryString = this.formatFilter(query).getQueryString();

    return super.download({}, `/download${queryString}&type=${format}`, RequestMethod.Get);
  }

  public formatFilter(query: DataQuery<GlobalAirlineFilter>): RequestQuery {
    const { iataCode, ...filterBy } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        iataCode: iataCode && iataCode.map(globalAirlineSummary => globalAirlineSummary.iataCode)
      }
    });
  }
}
