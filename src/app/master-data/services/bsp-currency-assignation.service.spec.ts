import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { BspCurrencyRequest } from '../models/bsp-currency-request.model';

import { BspCurrencyAssignationService } from './bsp-currency-assignation.service';

describe('BspCurrencyAssignationService', () => {
  let service: BspCurrencyAssignationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BspCurrencyAssignationService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(BspCurrencyAssignationService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should create a currency with a POST request', fakeAsync(() => {
    spyOn(service.http, 'post').and.returnValue(of({}));

    const requestBody: BspCurrencyRequest = {
      currency: {
        code: 'AAA',
        decimals: 1,
        effectiveTo: new Date()
      },
      effectiveTo: '2012-12-12'
    };
    const bspId = 12;

    service.create(bspId, requestBody).subscribe(result => {
      expect(result).toBeTruthy();
      tick();
    });

    expect(service.http.post).toHaveBeenCalled();
  }));

  it('should update a currency with a PUT request', fakeAsync(() => {
    spyOn(service.http, 'put').and.returnValue(of({}));

    const requestBody: BspCurrencyRequest = {
      currency: {
        code: 'AAA',
        decimals: 1,
        effectiveTo: new Date()
      },
      effectiveTo: '2012-12-12'
    };
    const bspId = 12;
    const currencyId = 1;

    service.update(bspId, currencyId, requestBody).subscribe(result => {
      expect(result).toBeTruthy();
      tick();
    });

    expect(service.http.put).toHaveBeenCalled();
  }));
});
