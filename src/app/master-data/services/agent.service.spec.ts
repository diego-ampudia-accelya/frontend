import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AgentService } from './agent.service';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { testDownloadRequestQueryBodyNoReduceFilterBy, testQuery } from '~app/shared/utils/test-utils';

const httpResponseMock = {
  pageNumber: 1,
  pageSize: 20,
  total: 32924,
  records: [
    {
      id: 69837822607,
      version: 5,
      iataCode: '7822607',
      name: 'John Smith',
      parentIataCode: null,
      topParentIataCode: null,
      cash: 'ACTIVE',
      easyPay: 'ACTIVE',
      paymentCard: 'ACTIVE',
      locationClass: null,
      locationType: 'HO',
      remittanceFrequency: null,
      taxId: null,
      tradeName: null,
      vatNumber: 'B96313168',
      logo: 'I7822607',
      effectiveFrom: '2012-12-03',
      effectiveTo: null,
      active: true,
      address: {
        street: 'Bake',
        city: 'VALENCIA',
        state: 'KAKA',
        country: 'Spain',
        postalCode: '46010'
      },
      contact: {
        telephone: '963699554',
        email: 'angelines.garcia@accelya.com',
        fax: '963611666'
      },
      bsp: {
        id: 6983,
        version: 30,
        isoCountryCode: 'ES',
        name: 'SPAIN',
        effectiveFrom: '2000-01-01',
        effectiveTo: null,
        easyPayAvailable: true,
        timeZoneConfiguration: {
          utcOffset: '+03:00',
          dstStart: '2020-03-29',
          dstEnd: '2020-10-28'
        }
      }
    }
  ]
};

describe('AgentService', () => {
  let service: AgentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgentService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(AgentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('find should execute POST request, and return PagedData ', () => {
    spyOn(service.http, 'post').and.returnValue(of(httpResponseMock));

    service.find(testQuery).subscribe(result => {
      expect(result.records.length).toBe(1);
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('getAll should execute POST request, and return PagedData', () => {
    spyOn(service.http, 'post').and.returnValue(of(httpResponseMock));

    service.getAll(new RequestQuery()).subscribe(result => {
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('getDropdownOptions should return value label pairs', () => {
    spyOn(service.http, 'post').and.returnValue(of(httpResponseMock));

    service.getDropdownOptions().subscribe(result => {
      expect(result.length).toBe(1);
      const { value, label } = result[0];
      const expectedValue = httpResponseMock.records[0].id;
      const expectedLabel = `${httpResponseMock.records[0].iataCode} / ${httpResponseMock.records[0].name}`;

      expect(value).toBe(expectedValue);
      expect(label).toBe(expectedLabel);
    });
  });

  it('getIataCodeDropdownOptions should return value label pairs', () => {
    spyOn(service.http, 'post').and.returnValue(of(httpResponseMock));

    service.getIataCodeDropdownOptions().subscribe(result => {
      expect(result.length).toBe(1);
      const { iataCode, name } = httpResponseMock.records[0];
      const { value, label } = result[0];
      const expectedValue = iataCode;
      const expectedLabel = `${iataCode} / ${name}`;

      expect(value).toBe(expectedValue);
      expect(label).toBe(expectedLabel);
    });
  });

  it('download should execute a POST request', () => {
    spyOn(service.http, 'post').and.returnValue(of());
    service.download(testDownloadRequestQueryBodyNoReduceFilterBy, 'txt').subscribe();

    expect(service.http.post).toHaveBeenCalled();
  });
});
