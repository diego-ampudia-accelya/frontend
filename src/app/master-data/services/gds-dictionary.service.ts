import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { IN } from '~app/shared/constants/operations';
import { DropdownOption } from '~app/shared/models';
import { GdsSummary } from '~app/shared/models/dictionary/gds-summary.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class GdsDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/bsp-management/bsps/gds`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public get(bspId?: number[]): Observable<GdsSummary[]> {
    const query = new RequestQuery(null, null, null, null);
    if (bspId?.length) {
      query.filterBy = [
        {
          attribute: 'bsp.id',
          attributeValue: bspId,
          operation: IN
        }
      ];
    }

    query.reduceFilterBy = true;

    return this.http.get<GdsSummary[]>(`${this.baseUrl}${query.getQueryString()}`);
  }

  public getDropdownOptions(bspId: number[]): Observable<DropdownOption<GdsSummary>[]> {
    return this.get(bspId).pipe(
      map(records =>
        records.map((gds: GdsSummary) => ({
          value: gds,
          label: `${gds.gdsCode} / ${gds.name}`
        }))
      ),
      catchError(() => of([]))
    );
  }

  public getDropdownOptionsForFiltering(bspId?: number[]): Observable<DropdownOption<GdsSummary>[]> {
    return this.get(bspId).pipe(
      map(records =>
        records
          .filter((gds: GdsSummary) => gds.gdsCode !== 'EARS' && gds.gdsCode !== 'MASS')
          .map((gds: GdsSummary) => ({
            value: gds,
            label: `${gds.name}`
          }))
      ),
      catchError(() => of([]))
    );
  }
}
