import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { BspCurrencyAssignationFilter } from '../models/bsp-currency-assignation-filter.model';
import { BspCurrencyRequest } from '../models/bsp-currency-request.model';
import { BspCurrencyAssignation } from '~app/master-data/models/bsp-currency-assignation.model';
import { DataQuery } from '~app/shared/components/list-view';
import { EQ } from '~app/shared/constants/operations';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class BspCurrencyAssignationService extends ApiService<BspCurrencyAssignation> {
  constructor(public http: HttpClient) {
    super(http, 'bsp');
  }

  public get baseUrl(): string {
    return `${appConfiguration.baseApiPath}/bsp-management/bsps`;
  }

  public find(query?: DataQuery<BspCurrencyAssignationFilter>): Observable<PagedData<BspCurrencyAssignation>> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = this.formatFilter(query.filterBy);

    requestQuery.reduceFilterBy = true;

    return this.http.get<PagedData<BspCurrencyAssignation>>(
      this.baseUrl + '/currency-assignments' + requestQuery.getQueryString()
    );
  }

  // TODO BE model when is ready
  private formatQuery(query: Partial<DataQuery<BspCurrencyAssignationFilter>>): RequestQuery<any> {
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...query.filterBy
      }
    });
  }

  public create(bspId: number, requestBody: BspCurrencyRequest): Observable<BspCurrencyAssignation> {
    const urlRequest = `${this.baseUrl}/${bspId}/currencies`;

    return this.http.post<BspCurrencyAssignation>(urlRequest, requestBody);
  }

  public update(bspId: number, currencyId: number, requestBody: unknown): Observable<BspCurrencyAssignation> {
    const urlRequest = `${this.baseUrl}/${bspId}/currencies/${currencyId}`;

    return this.http.put<BspCurrencyAssignation>(urlRequest, requestBody);
  }

  public formatFilter(filter: any): RequestQueryFilter[] {
    return Object.entries<any>(filter).map(value => ({
      attribute: value[0],
      operation: EQ,
      attributeValue: value[0]
    }));
  }
}
