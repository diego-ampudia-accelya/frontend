import { fakeAsync } from '@angular/core/testing';
import { of } from 'rxjs';

import { GdsService } from './gds.service';

const httpMock = jasmine.createSpyObj('HttpClient', ['get']);
httpMock.get.and.returnValue(
  of({
    pageNumber: 0,
    pageSize: 2000,
    records: [
      {
        gdsCode: 'AAA',
        id: 1,
        name: 'GDS AAA',
        version: 2
      },
      {
        gdsCode: 'BBB',
        id: 2,
        name: 'GDS BBB',
        version: 18
      }
    ]
  })
);

describe('GdsService', () => {
  let service: GdsService;

  beforeEach(() => {
    service = new GdsService(httpMock);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getCodeNameDropdownOptions should call http GET and return result', fakeAsync(() => {
    service.getCodeNameDropdownOptions('id').subscribe(result => {
      expect(result.length).toBe(2);
      expect(result[0].value).toBe(1);
      expect(result[1].value).toBe(2);
    });

    expect(httpMock.get).toHaveBeenCalled();
  }));
});
