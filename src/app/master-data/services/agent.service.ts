import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AgentListService } from '~app/master-data/agent/services/agent-list.service';
import { Agent } from '~app/master-data/models/agent.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { RequestMethod } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { appConfiguration } from '~app/shared/services';
import { ApiService } from '~app/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class AgentService extends ApiService<Agent> implements Queryable<Agent> {
  constructor(public http: HttpClient, private agentListService: AgentListService) {
    super(http, 'agent');
  }

  public find(query: DataQuery): Observable<PagedData<Agent>> {
    const dataQuery = {
      ...query,
      filterBy: this.formatFilter(query.filterBy)
    };

    const url = `${appConfiguration.baseApiPath}/agent-management/agents/browse`;

    return this.http.post<any>(url, dataQuery);
  }

  public getAll(requestQuery: RequestQuery): Observable<PagedData<Agent>> {
    return super.getAll(requestQuery, '/browse', RequestMethod.Post);
  }

  public getDropdownOptions(): Observable<DropdownOption[]> {
    return this.getAllItems().pipe(
      map(agents =>
        agents.map(agent => ({
          value: agent.id,
          label: `${agent.iataCode} / ${agent.name}`
        }))
      )
    );
  }

  public getIataCodeDropdownOptions(): Observable<DropdownOption[]> {
    return this.getAllItems().pipe(
      map(agents =>
        agents.map(agent => ({
          value: agent.iataCode,
          label: `${agent.iataCode} / ${agent.name}`
        }))
      )
    );
  }

  public download(requestQueryBody: any, downloadFormat: string) {
    const requestQuery = {
      ...requestQueryBody,
      filterBy: this.formatFilter(requestQueryBody.filterBy)
    };

    return super.download(requestQuery, `/download?type=${downloadFormat}`, RequestMethod.Post);
  }

  public formatFilter(filter: any): RequestQueryFilter[] {
    return this.agentListService.formatRequestFilters(filter);
  }
}
