import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AirlinesListService } from '../airline/services/airline-list.service';
import { Airline } from '~app/master-data/models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { IN } from '~app/shared/constants/operations';
import { RequestMethod } from '~app/shared/enums/request-method.enum';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class AirlineService extends ApiService<Airline> implements Queryable<Airline> {
  constructor(public http: HttpClient, private airlinesListService: AirlinesListService) {
    super(http, 'airline');
  }

  public find(query: DataQuery): Observable<PagedData<Airline>> {
    const dataQuery = {
      ...query,
      filterBy: this.formatFilter(query.filterBy)
    };
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery(dataQuery);
    requestQuery.reduceFilterBy = true;

    const url = `${appConfiguration.baseApiPath}/airline-management/airlines` + requestQuery.getQueryString();

    return this.http.get<any>(url);
  }

  public getAll(requestQuery: RequestQuery): Observable<PagedData<Airline>> {
    requestQuery.reduceFilterBy = true;

    return super.getAll(requestQuery);
  }

  public getBy(globalAirlineId: number, bspNames: Array<string> = []): Observable<Airline[]> {
    const requestQuery = new RequestQuery(PAGINATION.FIRST_PAGE, PAGINATION.MAX_PAGE_SIZE);
    requestQuery.reduceFilterBy = true;
    requestQuery.filterBy = [new RequestQueryFilter('globalAirline.id', globalAirlineId)];

    if (Array.isArray(requestQuery.filterBy) && bspNames.length > 0) {
      requestQuery.filterBy.push(new RequestQueryFilter('bsp.name', bspNames.join(','), IN));
    }

    return this.getAll(requestQuery).pipe(map(result => result.records));
  }

  public getIataCodeDropdownOptions(): Observable<DropdownOption[]> {
    return this.getAllItems().pipe(
      map(airlines =>
        airlines.map(airline => ({
          value: airline.globalAirline.iataCode,
          label: `${airline.globalAirline.iataCode} / ${airline.localName}`
        }))
      )
    );
  }

  // TODO: Refactor.
  public formatFilter(filter: any): RequestQueryFilter[] {
    return this.airlinesListService.formatRequestFilters(filter);
  }

  public download(query: any, format: string): Observable<{ blob: Blob; fileName: string }> {
    query = {
      ...query,
      filterBy: this.formatFilter(query.filterBy)
    };

    const queryString = this.toRequestQuery(query).getQueryString();

    return super.download({}, `/download${queryString}&type=${format}`, RequestMethod.Get);
  }

  private toRequestQuery(query: any) {
    const sortBy = (query.sortBy && query.sortBy[0]) || {};
    const { attribute: sortAttribute, sortType } = sortBy;
    const { page, size } = query.paginateBy;

    return new RequestQuery(page, size, sortAttribute, sortType, query.filterBy, true);
  }
}
