import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AirlineService } from './airline.service';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { testDownloadRequestQueryBody, testQuery } from '~app/shared/utils/test-utils';

const httpResponseMock = {
  pageNumber: 1,
  pageSize: 5,
  total: 352,
  records: [
    {
      id: 6983484954,
      designator: 'LM',
      localName: 'AIRLINE NAME 016',
      vatNumber: 'NEW VAT 016',
      active: true,
      easyPayStatus: 'OPTIN',
      easyPayEffectiveFrom: '2019-06-16',
      effectiveFrom: '2016-10-11',
      address: {
        address1: 'NO:229/6  Known',
        address2: 'NO:229/6  Known',
        city: 'ISTANBUL',
        country: 'TURKEY',
        postalCode: '28020'
      },
      contact: {
        lastName: 'John Smith',
        telephone: '0/212/2319321'
      },
      version: 4,
      bsp: {
        id: 6983,
        isoCountryCode: 'ES',
        name: 'SPAIN',
        effectiveFrom: '2000-01-01',
        active: true,
        version: 30
      },
      globalAirline: {
        id: 484954,
        iataCode: '016',
        globalName: 'United Airlines, Inc.',
        logo: 'l016',
        version: 1
      }
    }
  ]
};

describe('AirlineService', () => {
  let service: AirlineService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AirlineService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(AirlineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('find should execute GET request, and return PagedData ', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.find(testQuery).subscribe(result => {
      expect(result.records.length).toBe(1);
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('getAll should execute GET request, and return PagedData', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.getAll(new RequestQuery()).subscribe(result => {
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('getBy should execute GET request, and return array', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.getBy(484954).subscribe(result => {
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toBe(1);
    });
  });

  it('getBy should execute GET request, and return empty array, if bsps do not match', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));
    spyOn(service, 'getAll').and.returnValue(of({ ...httpResponseMock, records: [] }));

    service.getBy(484954, ['YYY']).subscribe(result => {
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toBe(0);
    });
  });

  it('getIataCodeDropdownOptions should return value label pairs', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.getIataCodeDropdownOptions().subscribe(result => {
      expect(result.length).toBe(1);
      const { globalAirline, localName: name } = httpResponseMock.records[0];
      const { iataCode } = globalAirline;
      const { value, label } = result[0];
      const expectedValue = iataCode;
      const expectedLabel = `${iataCode} / ${name}`;

      expect(value).toBe(expectedValue);
      expect(label).toBe(expectedLabel);
    });
  });

  it('download should execute a GET request', () => {
    spyOn(service.http, 'get').and.returnValue(of());
    service.download(testDownloadRequestQueryBody, 'txt').subscribe();

    expect(service.http.get).toHaveBeenCalled();
  });

  describe('formatFilter', () => {
    it('should return correct data', () => {
      const inputData = {
        airlines: [{ code: '057', name: 'Name' }]
      };
      const result = service.formatFilter(inputData);

      expect(result).toEqual([{ attribute: 'globalAirline.iataCode', operation: 'in', attributeValue: '057' }]);
    });
  });
});
