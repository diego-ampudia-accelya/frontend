import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { NotificationService } from '~app/shared/services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessageService {
  constructor(private notificationService: NotificationService, private translationService: L10nTranslationService) {}

  public processResponseErrors(errors, tableColumnMap) {
    const errorBE = errors.error;
    let errorMessages = '';

    if (!errorBE) {
      // HTTP Error, no JSON messages
      // TODO To process HTTP errors we should probably have an interceptor
      this.notificationService.showError(this.translationService.translate('GENERIC_HTTP_ERROR'));
    } else {
      if (!errorBE.messages.length || !tableColumnMap) {
        // TODO BE should send a errorMessage as a key to be translated for FE
        this.notificationService.showError(this.translationService.translate(errorBE.errorMessage));
      } else {
        errorBE.messages.forEach(error => {
          const label =
            tableColumnMap[error.messageParams[0].value] === 'isoCountryCode'
              ? 'Country Code'
              : tableColumnMap[error.messageParams[0].value];
          const errorMessageTranslation = this.translationService.translate(error.message);

          // TODO BE should send an array of messages with keys to be translated for FE
          const messageParts: Array<string> = [];
          if (label) {
            messageParts.push(label);
            messageParts.push(': ');
          }

          messageParts.push(errorMessageTranslation);
          messageParts.push('\n');

          errorMessages += messageParts.join('');
        });
        this.notificationService.showError(errorMessages);
      }
    }
  }

  public processFormErrors(form, tableColumnMap) {
    const controls = Object.keys(form.controls);
    let messages = '';

    controls.forEach(control => {
      const errors = form.controls[control].errors;
      const errorType = errors ? Object.keys(errors)[0] : '';

      if (errorType) {
        const errorTypeTranslation = this.translationService.translate(`FORM_CONTROL_VALIDATORS.${errorType}`);
        messages = `${tableColumnMap[control]}: ${errorTypeTranslation}`;
        this.notificationService.showError(messages);
      }
    });
  }
}
