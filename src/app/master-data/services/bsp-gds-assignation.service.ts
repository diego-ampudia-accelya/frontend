import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { BspGdsListService } from '../bsp-gds-assignation/shared/bsp-gds-list.service';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import {
  BspGdsAssignation,
  BspGdsAssignationCreateModel,
  BspGdsAssignationEditModel
} from '~app/master-data/models/bsp-gds-assignation.model';
import { AppState } from '~app/reducers';
import { DataQuery } from '~app/shared/components/list-view';
import { Queryable } from '~app/shared/components/list-view/queryable';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class BspGdsAssignationService extends ApiService<BspGdsAssignation> implements Queryable<BspGdsAssignation> {
  private bspId: number;

  constructor(public http: HttpClient, private bspGdsListService: BspGdsListService, private store: Store<AppState>) {
    super(http, 'bsp');
    this.store
      .pipe(select(fromAuth.getUserBsps), first())
      .subscribe(bsps => (this.bspId = bsps && bsps[0] ? bsps[0].id : null));
  }

  public create(model: BspGdsAssignationCreateModel): Observable<void> {
    return this.http.post<void>(`${this.baseUrl}/${this.bspId}/gds-assignments`, model);
  }

  public update(model: BspGdsAssignationEditModel, gdsAssignationId: number): Observable<void> {
    return this.http.put<void>(`${this.baseUrl}/${this.bspId}/gds-assignments/${gdsAssignationId}`, model);
  }

  public delete(gdsAssignationId: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${this.bspId}/gds-assignments/${gdsAssignationId}`);
  }

  public find(dataQuery: DataQuery): Observable<PagedData<BspGdsAssignation>> {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery(dataQuery);
    requestQuery.filterBy = this.formatFilter(dataQuery.filterBy);

    return this.getAll(requestQuery);
  }

  public getAll(requestQuery: RequestQuery): Observable<PagedData<BspGdsAssignation>> {
    requestQuery.reduceFilterBy = true;

    return super.getAll(requestQuery, '/gds-assignments');
  }

  public save(data?: any): Observable<any> {
    let method = 'post';
    let url = this.baseUrl;

    data.refundsEnabled = !!data.refundsEnabled;

    url = url + (!data.id ? `/${data.bspId}/gds-assignments` : `/gds-assignments`);

    if (data.id) {
      method = 'put';
      url += '/' + data.id;
    }

    return this.http[method](url, data);
  }

  public formatFilter(filter: any): RequestQueryFilter[] {
    return this.bspGdsListService.formatRequestFilters(filter);
  }

  public download(
    query: DataQuery<BspGdsAssignation>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = RequestQuery.fromDataQuery(query);

    requestQuery.filterBy = this.formatFilter(query.filterBy);

    requestQuery.filterBy = { ...query.filterBy, exportAs: downloadFormat.toUpperCase() };

    const url = `${this.baseUrl}/gds-assignments/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
