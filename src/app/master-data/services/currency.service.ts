import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Currency } from '~app/shared/models/currency.model';
import { ApiService } from '~app/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService extends ApiService<Currency> {
  constructor(http: HttpClient) {
    super(http, 'currency');
  }
}
