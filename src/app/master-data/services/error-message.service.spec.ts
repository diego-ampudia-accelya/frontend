import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { ReplaySubject } from 'rxjs';

import { ErrorMessageService } from './error-message.service';
import { NotificationService } from '~app/shared/services';

describe('ErrorMessageService', () => {
  let service: ErrorMessageService;
  let translationStub: SpyObject<L10nTranslationService>;
  let language$: ReplaySubject<string>;
  let notificationServiceStub: SpyObject<NotificationService>;

  beforeEach(() => {
    language$ = new ReplaySubject(1);
    translationStub = createSpyObject(L10nTranslationService);
    translationStub.onChange.and.returnValue(language$.asObservable());
    translationStub.translate.and.returnValue('translated');

    notificationServiceStub = createSpyObject(NotificationService);

    service = new ErrorMessageService(notificationServiceStub, translationStub);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('processResponseErrors should call notify with generic message, if empty error', () => {
    service.processResponseErrors({}, null);
    expect(translationStub.translate).toHaveBeenCalledWith('GENERIC_HTTP_ERROR');
    expect(notificationServiceStub.showError).toHaveBeenCalled();
  });

  it('processResponseErrors should call notify with translated error message, if no error messages', () => {
    service.processResponseErrors({ error: { errorMessage: 'Test error message', messages: [] } }, null);
    expect(translationStub.translate).toHaveBeenCalledWith('Test error message');
    expect(notificationServiceStub.showError).toHaveBeenCalled();
  });

  it('processResponseErrors should call notify with translated error message, if no has messages, but no tableColumnMap', () => {
    service.processResponseErrors({ error: { errorMessage: 'Test error message', messages: [{}] } }, null);
    expect(translationStub.translate).toHaveBeenCalledWith('Test error message');
    expect(notificationServiceStub.showError).toHaveBeenCalled();
  });

  it('processResponseErrors should call notify with translated error messages message, and message params value is isoCountryCode', () => {
    service.processResponseErrors(
      {
        error: {
          errorMessage: 'Test error message',
          messages: [
            {
              message: 'Test message',
              messageParams: [
                {
                  value: 'country'
                }
              ]
            }
          ]
        }
      },
      { country: 'isoCountryCode' }
    );
    expect(translationStub.translate).toHaveBeenCalledWith('Test message');
    expect(notificationServiceStub.showError).toHaveBeenCalled();
  });

  it('processResponseErrors should call notify with translated error messages message, and message params value is not isoCountryCode', () => {
    service.processResponseErrors(
      {
        error: {
          errorMessage: 'Test error message',
          messages: [
            {
              message: 'Test message',
              messageParams: [
                {
                  value: 'country'
                }
              ]
            }
          ]
        }
      },
      { country: '' }
    );
    expect(translationStub.translate).toHaveBeenCalledWith('Test message');
    expect(notificationServiceStub.showError).toHaveBeenCalled();
  });

  it('processFormErrors should call notification service for each error control', () => {
    service.processFormErrors(
      {
        controls: {
          name: {
            errors: [{ type: 'empty' }]
          }
        }
      },
      { name: '' }
    );
    expect(notificationServiceStub.showError).toHaveBeenCalled();
  });
});
