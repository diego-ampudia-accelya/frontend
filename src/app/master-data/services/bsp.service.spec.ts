import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { BspService } from './bsp.service';
import { Bsp } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';

const httpResponseMock: PagedData<Bsp> = {
  pageNumber: 1,
  pageSize: 20,
  total: 2,
  totalPages: 1,
  records: [
    {
      id: 1,
      name: 'Spain',
      isoCountryCode: 'ES',
      effectiveFrom: '2000/05/31',
      active: true
    },
    {
      id: 2,
      name: 'Australia',
      isoCountryCode: 'A6',
      effectiveFrom: '2000/05/31',
      effectiveTo: '2001/05/31',
      active: false
    }
  ]
};

describe('BspService', () => {
  let httpClientSpy: SpyObject<HttpClient>;
  let service: BspService;

  beforeEach(() => {
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponseMock));

    TestBed.configureTestingModule({
      providers: [BspService, { provide: HttpClient, useValue: httpClientSpy }]
    });

    service = TestBed.inject(BspService);
    spyOn(service, 'appConfiguration').and.returnValue({ baseApiPath: '' });
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAll should execute GET request, and return PagedData', fakeAsync(() => {
    service.getAll(new RequestQuery()).subscribe(result => {
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });

    expect(httpClientSpy.get).toHaveBeenCalled();
  }));

  it('getAllItems should execute GET request, and return Bsp[]', fakeAsync(() => {
    service.getAllItems().subscribe(result => {
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toBe(2);
    });
    expect(httpClientSpy.get).toHaveBeenCalled();
  }));

  it('getAllItems should execute GET request, and return filtered Bsp[]', fakeAsync(() => {
    service.getAllActiveItems().subscribe(result => {
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toBe(1);
      expect(result[0].id).toBe(1);
    });
    expect(httpClientSpy.get).toHaveBeenCalled();
  }));

  it('getDropdownOptions should execute GET request, and return DropdownOption[]', fakeAsync(() => {
    service.getDropDownOptions().subscribe(result => {
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toBe(2);
      expect(result[0].value).toBe(1);
      expect(result[0].label).toBe('Spain');
    });
    expect(httpClientSpy.get).toHaveBeenCalled();
  }));

  it('getActiveBspsDropDownOptions should execute GET request, and return filtered DropdownOption[]', fakeAsync(() => {
    service.getActiveBspsDropDownOptions().subscribe(result => {
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toBe(1);
      expect(result[0].value).toBe(1);
      expect(result[0].label).toBe('Spain');
    });
    expect(httpClientSpy.get).toHaveBeenCalled();
  }));
});
