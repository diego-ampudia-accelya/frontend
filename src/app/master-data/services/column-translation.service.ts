import { Injectable } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ColumnTranslationService {
  constructor(private translationService: L10nTranslationService) {}

  translate(columns: TableColumn[]): Observable<TableColumn[]> {
    return this.translationService.onChange().pipe(
      distinctUntilChanged(),
      map(() =>
        columns.map(column => ({
          ...column,
          name: this.translationService.translate(column.name)
        }))
      )
    );
  }
}
