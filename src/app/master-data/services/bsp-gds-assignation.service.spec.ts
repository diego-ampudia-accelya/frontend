import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { BspGdsAssignationService } from './bsp-gds-assignation.service';
import { initialState } from '~app/auth/reducers/auth.reducer';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { testQuery } from '~app/shared/utils/test-utils';

const httpResponseMock = {
  pageNumber: 1,
  pageSize: 20,
  total: 1,
  records: []
};

describe('BspGdsAssignationService', () => {
  let service: BspGdsAssignationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BspGdsAssignationService,
        {
          provide: ReactiveSubject,
          useValue: {
            asObservable: of({})
          }
        },
        provideMockStore({
          initialState: {
            auth: initialState
          },
          selectors: [{ selector: getUserBsps, value: of(null) }]
        })
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(BspGdsAssignationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAll should execute get request, and return PagedData', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.getAll(new RequestQuery()).subscribe(result => {
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('find should execute GET request, and return PagedData ', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.find(testQuery).subscribe(result => {
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('save should execute POST request', () => {
    spyOn(service.http, 'post').and.returnValue(of({}));

    service.save({}).subscribe(result => {
      expect(result).toBeTruthy();
    });

    expect(service.http.post).toHaveBeenCalled();
  });

  it('save should execute PUT request, if data id is provided', () => {
    spyOn(service.http, 'put').and.returnValue(of({}));

    service.save({ id: 12 }).subscribe(result => {
      expect(result).toBeTruthy();
    });

    expect(service.http.put).toHaveBeenCalled();
  });
});
