/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { chain, cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { ArrayHelper } from '~app/shared/helpers';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class BspService extends ApiService<Bsp> {
  public appConfiguration = appConfiguration;

  public get baseUrl() {
    return `${this.appConfiguration.baseApiPath || ''}/bsp-management/bsps`;
  }

  private ALL_ITEMS_QUERY = new RequestQuery(PAGINATION.FIRST_PAGE, PAGINATION.MAX_PAGE_SIZE);

  constructor(public http: HttpClient) {
    super(http, 'bsp');
  }

  public getAll(query?: RequestQuery) {
    query = query || cloneDeep(this.ALL_ITEMS_QUERY);
    query.reduceFilterBy = true;

    return super.getAll(query);
  }

  public getAllItems(): Observable<Bsp[]> {
    const query = cloneDeep(this.ALL_ITEMS_QUERY);
    query.reduceFilterBy = true;
    const url = this.baseUrl + query.getQueryString();

    return this.http.get<PagedData<Bsp>>(url).pipe(map(result => result.records));
  }

  public getAllActiveItems(): Observable<Bsp[]> {
    return this.getAllItems().pipe(
      map((bsps: Bsp[]) =>
        chain(bsps)
          .filter(bsp => bsp.active)
          .orderBy('name')
          .value()
      )
    );
  }

  public getDropDownOptions() {
    return this.getAllItems().pipe(map(ArrayHelper.toDropdownOptions));
  }

  public getActiveBspsDropDownOptions(): Observable<DropdownOption[]> {
    return this.getAllActiveItems().pipe(map(ArrayHelper.toDropdownOptions));
  }
}
