import { HttpClient } from '@angular/common/http';
import { fakeAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of, throwError } from 'rxjs';

import { GdsDictionaryService } from './gds-dictionary.service';
import { GdsSummary } from '~app/shared/models/dictionary/gds-summary.model';
import { appConfiguration } from '~app/shared/services';

const responseGdsItems: GdsSummary[] = [
  {
    id: 1,
    version: 1,
    gdsCode: 'GDS1',
    name: 'GDS 1'
  },
  {
    id: 2,
    version: 2,
    gdsCode: 'GDS2',
    name: 'GDS 2'
  }
];

// We need to make sure that every time we call httpClient get we provide different filter.
// This will ensure that get request is executed
// TODO find better way to invalidate cache.

describe('GdsDictionaryService', () => {
  const httpClientSpy = createSpyObject(HttpClient);
  httpClientSpy.get.and.returnValue(of(responseGdsItems));

  let service: GdsDictionaryService;
  beforeEach(() => {
    service = new GdsDictionaryService(httpClientSpy, appConfiguration);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get should call http get and provide Array, if no bspId is provided', fakeAsync(() => {
    service.get().subscribe(items => {
      expect(items.length).toBe(2);
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/bsp-management/bsps/gds'));
    });
  }));

  it('get should call http get and provide Array, when bspId is provided', fakeAsync(() => {
    service.get([1]).subscribe(items => {
      expect(items.length).toBe(2);
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/bsp-management/bsps/gds'));
    });
  }));

  it('getDropdownOptions should provide array of DropdownOption', fakeAsync(() => {
    service.getDropdownOptions([2]).subscribe(([firstOption]) => {
      const firstResponseItem = responseGdsItems[0];
      expect(firstOption.value).toEqual(firstResponseItem);
      expect(firstOption.label).toEqual(`${firstResponseItem.gdsCode} / ${firstResponseItem.name}`);
    });
  }));
});

describe('GdsDictionaryService Error', () => {
  const httpClientErrorSpy = createSpyObject(HttpClient);
  httpClientErrorSpy.get.and.returnValue(throwError(new Error()));

  it('getDropdownOptions should provide empty array, when there is an error', fakeAsync(() => {
    const serviceThrowingError = new GdsDictionaryService(httpClientErrorSpy, appConfiguration);

    serviceThrowingError.getDropdownOptions([3]).subscribe(items => {
      expect(items).toEqual([]);
    });
  }));
});
