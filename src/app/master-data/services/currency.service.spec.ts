import { of } from 'rxjs';

import { CurrencyService } from './currency.service';

const httpMock = jasmine.createSpyObj('HttpClient', ['get']);
httpMock.get.and.returnValue(
  of({
    pageNumber: 0,
    pageSize: 20,
    records: [
      {
        code: 'AUD',
        decimals: 2,
        id: 658568,
        version: 101
      }
    ]
  })
);

describe('CurrencyService', () => {
  let service: CurrencyService;

  beforeEach(() => {
    service = new CurrencyService(httpMock);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
