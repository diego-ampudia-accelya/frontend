import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AirlineService } from './airline.service';
import { GlobalAirlineService } from './global-airline.service';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { testDownloadRequestQueryBody } from '~app/shared/utils/test-utils';

const airlineServiceSpy = jasmine.createSpyObj('AirlineService', ['getBy', 'getOne']);

airlineServiceSpy.getBy.and.returnValue(
  of([
    {
      id: 123,
      globalAirline: {
        id: 484954,
        iataCode: '016',
        globalName: 'United Airlines, Inc.',
        logo: 'l016',
        version: 1
      }
    },
    {
      globalAirline: {
        id: 484954,
        iataCode: '016',
        globalName: 'United Airlines, Inc.',
        logo: 'l016',
        version: 1
      }
    }
  ])
);
airlineServiceSpy.getOne.and.returnValue(
  of({
    id: 123,
    globalAirline: {
      id: 484954,
      iataCode: '016',
      globalName: 'United Airlines, Inc.',
      logo: 'l016',
      version: 1
    }
  })
);

const httpResponseMock = {
  pageNumber: 1,
  pageSize: 5,
  total: 352,
  records: [
    {
      id: 484954,
      iataCode: '016',
      globalName: 'United Airlines, Inc.',
      logo: 'l016',
      version: 1
    },
    {
      id: 485250,
      iataCode: '042',
      globalName: 'Air Excursion, LLC',
      logo: 'l042',
      version: 1
    }
  ]
};

describe('GlobalAirlineService', () => {
  let service: GlobalAirlineService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GlobalAirlineService,
        {
          provide: AirlineService,
          useValue: airlineServiceSpy
        }
      ],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(GlobalAirlineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAll should execute GET request, and return PagedData', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.getAll(new RequestQuery()).subscribe(result => {
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('getOne should execute GET request, and return one global airline', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock.records[0]));

    service.getOne(484954).subscribe(result => {
      expect(result.id).toBe(484954);
    });

    expect(service.http.get).toHaveBeenCalled();
  });

  it('getOne, called with includeAirlines true, should execute GET request, and return one global airline with airlines', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock.records[0]));

    service.getOne(484954, true).subscribe(result => {
      expect(result.id).toBe(484954);
      expect(result.airlines.length).toBe(2);
    });

    expect(service.http.get).toHaveBeenCalled();
  });

  it('getBy airlineId should return one global airline', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock.records[0]));

    service.getBy(123).subscribe(result => {
      expect(result.id).toBe(484954);
    });
  });

  it('getBy airlineId, includeAirlines true, should execute GET request, and return one global airline, with airlines', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock.records[0]));

    service.getBy(123, true).subscribe(result => {
      expect(result.id).toBe(484954);

      expect(result.airlines.length).toBe(2);
    });
  });

  it('getDropdownOptions should return value label pairs', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.getDropDownOptions().subscribe(result => {
      const { iataCode, globalName: name } = httpResponseMock.records[0];
      const { value, label } = result[0];
      const expectedValue = httpResponseMock.records[0].id;
      const expectedLabel = `${iataCode} / ${name}`;

      expect(result.length).toBe(2);
      expect(value).toBe(expectedValue);
      expect(label).toBe(expectedLabel);
    });
  });

  it('getIataCodeDropdownOptions should return value label pairs', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.getIataCodeDropdownOptions().subscribe(result => {
      const { iataCode, globalName: name } = httpResponseMock.records[0];
      const { value, label } = result[0];
      const expectedValue = iataCode;
      const expectedLabel = `${iataCode} / ${name}`;

      expect(result.length).toBe(2);
      expect(value).toBe(expectedValue);
      expect(label).toBe(expectedLabel);
    });
  });

  it('download should execute a GET request', () => {
    spyOn(service.http, 'get').and.returnValue(of());
    service.download(testDownloadRequestQueryBody, 'txt').subscribe();

    expect(service.http.get).toHaveBeenCalled();
  });
});
