/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

import { SftpAccountPublicKey, SftpAccountViewModel } from '~app/master-data/sftp/models';
import { sftpAccountTypeUrlMap } from '~app/master-data/sftp/services/sftp-account.map.constants';
import { SftpAccountManagePublicKeysDialogActions as PublicKeysDialogActions } from '~app/master-data/sftp/store/actions';
import * as fromSftp from '~app/master-data/sftp/store/reducers';
import { AppState } from '~app/reducers';
import { DialogConfig } from '~app/shared/components';
import { FileValidator, FileValidators } from '~app/shared/components/upload/file-validators';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { AppConfigurationService } from '~app/shared/services';

const translationManagePublicKeysDialogNs = 'menu.masterData.configuration.sftpConfigurations.managePublicKeysDialog';

@Component({
  selector: 'bspl-sftp-account-manage-public-keys-dialog',
  templateUrl: './sftp-account-manage-public-keys-dialog.component.html',
  styleUrls: ['./sftp-account-manage-public-keys-dialog.component.scss']
})
export class SftpAccountManagePublicKeysDialogComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();

  public sftpAccount: SftpAccountViewModel;

  public UploadModes = UploadModes;

  public validationRules: FileValidator[] = [
    FileValidators.maxFileSize(),
    FileValidators.filenamePattern(/^(.*)\.pub$/, `${translationManagePublicKeysDialogNs}.errors.publicKeysFileFormat`)
  ];

  public uploadPath = '';

  public publicKeys$: Observable<SftpAccountPublicKey[]>;

  public hasSelectedPublicKeys$ = this.store.pipe(select(fromSftp.getHasSelectedPublicKeys));

  public hasDeletedPublicKeys$ = this.store.pipe(select(fromSftp.getHasDeletedPublicKeys));

  public deletedPublicKeysErrorMessage$ = this.store.pipe(select(fromSftp.getDeletedPublicKeysErrorMessage));

  public hasFilesToUpload$ = this.store.pipe(select(fromSftp.getHasFilesToUpload));

  public isUploadInProgress$ = this.store.pipe(select(fromSftp.getIsUploadInProgress));

  public allFilesAreCompleted$ = this.store.pipe(select(fromSftp.getAllFilesAreCompleted));

  public failedFileNames$ = this.store.pipe(select(fromSftp.getFailedFilesNames));

  @ViewChild(UploadComponent, { static: true }) uploadComponent: UploadComponent;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private config: DialogConfig,
    private store: Store<AppState>,
    private appConfiguration: AppConfigurationService
  ) {}

  public ngOnInit(): void {
    const sftpAccount = this.config.data.sftpAccount as SftpAccountViewModel;
    this.sftpAccount = sftpAccount;

    this.publicKeys$ = this.store.pipe(
      select(fromSftp.getSftpAccountPublicKeys, { sftpAccount }),
      takeUntil(this.destroy$)
    );

    this.uploadPath = `${
      this.appConfiguration.baseUploadPath
    }/sftp-management/sftp-accounts/public-keys/${sftpAccountTypeUrlMap.get(sftpAccount.sftpAccountType)}`;
  }

  public ngOnDestroy() {
    this.destroy$.next();
    this.store.dispatch(PublicKeysDialogActions.sftpAccountManagePubicKeysDialogDestroy());
  }

  public onFilesChange() {
    const hasFilesToUpload = this.uploadComponent.hasFilesInPreviewMode;

    this.store.dispatch(PublicKeysDialogActions.filesToUploadChanged({ hasFilesToUpload }));
  }

  public onUploadClick() {
    this.store.dispatch(PublicKeysDialogActions.publicKeysUploadStart());

    this.uploadComponent
      .uploadFiles()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        const { sftpAccountType } = this.sftpAccount;
        const uploadedFiles = [...this.uploadComponent.filesToUpload];

        this.uploadComponent.reset();

        this.store.dispatch(PublicKeysDialogActions.publicKeysUploadEnd({ sftpAccountType, uploadedFiles }));
      });
  }

  public onDeleteClick() {
    this.store
      .pipe(select(fromSftp.getSftpAccountSelectedPublicKeys), takeUntil(this.destroy$), take(1))
      .subscribe(publicKeys => {
        this.store.dispatch(PublicKeysDialogActions.deletePublicKeys({ sftpAccount: this.sftpAccount, publicKeys }));
      });
  }

  public onPublicKeyStateChange(checked: boolean, publicKey: SftpAccountPublicKey) {
    const action = checked
      ? PublicKeysDialogActions.selectPublicKey({ publicKey })
      : PublicKeysDialogActions.deselectPublicKey({ publicKey });

    this.store.dispatch(action);
  }
}
