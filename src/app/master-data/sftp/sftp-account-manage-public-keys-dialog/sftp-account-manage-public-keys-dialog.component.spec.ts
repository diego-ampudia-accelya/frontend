import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { SftpAccountManagePublicKeysDialogComponent } from './sftp-account-manage-public-keys-dialog.component';
import { SftpAccountManagePublicKeysDialogActions as PublicKeysDialogActions } from '~app/master-data/sftp/store/actions';
import {
  getInitialState,
  getTestSftpAccount,
  getTestSftpAccountPublicKey,
  getTestUploadFile
} from '~app/master-data/sftp/test/sftp.test.mocks';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { ButtonDesign, DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('SftpAccountManagePublicKeysDialogComponent', () => {
  let component: SftpAccountManagePublicKeysDialogComponent;
  let fixture: ComponentFixture<SftpAccountManagePublicKeysDialogComponent>;
  let store: MockStore;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        hasCancelButton: false,
        isClosable: true,
        footerButtonsType: [
          {
            type: FooterButton.Cancel,
            buttonDesign: ButtonDesign.Tertiary,
            title: 'BUTTON.DEFAULT.CLOSE'
          }
        ],
        sftpAccount: getTestSftpAccount(),
        deletedPublicKeys: []
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      declarations: [SftpAccountManagePublicKeysDialogComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        provideMockStore({ initialState: getInitialState() }),
        mockProvider(AppConfigurationService, { baseApiPath: 'api', baseUploadPath: 'upload/api' }),
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SftpAccountManagePublicKeysDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.inject(MockStore);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onFilesChange should dispatch filesToUploadChanged', () => {
    component.uploadComponent = { hasFilesInPreviewMode: false } as UploadComponent;

    component.onFilesChange();

    expect(store.dispatch).toHaveBeenCalledWith(
      PublicKeysDialogActions.filesToUploadChanged({ hasFilesToUpload: false })
    );
  });

  it('onUploadClick should dispatch publicKeysUploadStart', fakeAsync(() => {
    component.uploadComponent = {
      uploadFiles: () => of(true),
      reset: () => {},
      filesToUpload: [getTestUploadFile('test1.pub', null)]
    } as UploadComponent;

    component.onUploadClick();

    expect(store.dispatch).toHaveBeenCalledWith(PublicKeysDialogActions.publicKeysUploadStart());
  }));

  it('onDeleteClick should dispatch deletePublicKeys', fakeAsync(() => {
    const testSftpAccount = getTestSftpAccount();

    component.sftpAccount = testSftpAccount;

    component.onDeleteClick();

    expect(store.dispatch).toHaveBeenCalledWith(
      PublicKeysDialogActions.deletePublicKeys({ sftpAccount: testSftpAccount, publicKeys: [] })
    );
  }));

  it('onPublicKeyStateChange should dispatch selectPublicKey, if checked is true', () => {
    const publicKey = getTestSftpAccountPublicKey();

    component.onPublicKeyStateChange(true, publicKey);

    expect(store.dispatch).toHaveBeenCalledWith(PublicKeysDialogActions.selectPublicKey({ publicKey }));
  });

  it('onPublicKeyStateChange should dispatch deselectPublicKey, if checked is false', () => {
    const publicKey = getTestSftpAccountPublicKey();

    component.onPublicKeyStateChange(false, publicKey);

    expect(store.dispatch).toHaveBeenCalledWith(PublicKeysDialogActions.deselectPublicKey({ publicKey }));
  });
});
