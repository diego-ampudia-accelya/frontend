import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslateAsyncPipe, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration } from 'ng-mocks';

import { SftpAccountsComponent } from './sftp-accounts.component';
import { SftpAccountViewModel } from '~app/master-data/sftp/models';
import { SftpAccountsActions } from '~app/master-data/sftp/store/actions';
import { getInitialState, getTestSftpAccount } from '~app/master-data/sftp/test/sftp.test.mocks';

describe('SftpAccountsComponent', () => {
  let component: SftpAccountsComponent;
  let fixture: ComponentFixture<SftpAccountsComponent>;
  let store: MockStore;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SftpAccountsComponent, MockDeclaration(L10nTranslateAsyncPipe)],
      providers: [provideMockStore({ initialState: getInitialState() }), mockProvider(L10nTranslationService)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SftpAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    store = TestBed.inject(MockStore);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize columns', () => {
    const expectedColumns: any[] = [
      jasmine.objectContaining({
        header: 'menu.masterData.configuration.sftpConfigurations.labels.sftpAccountType',
        field: 'sftpAccountTypeText',
        customStyle: 'font-weight-bold'
      }),
      jasmine.objectContaining({
        header: 'menu.masterData.configuration.sftpConfigurations.labels.login',
        field: 'login'
      }),
      jasmine.objectContaining({
        header: 'menu.masterData.configuration.sftpConfigurations.labels.status',
        field: 'stateText'
      }),
      jasmine.objectContaining({
        header: 'menu.masterData.configuration.sftpConfigurations.labels.action',
        field: 'actionText',
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        header: 'menu.masterData.configuration.sftpConfigurations.labels.publicKeys',
        field: 'publicKeys',
        cellTemplate: jasmine.anything()
      }),
      jasmine.objectContaining({
        header: 'menu.masterData.configuration.sftpConfigurations.labels.owner',
        field: 'ownerText',
        tooltip: jasmine.anything()
      })
    ];

    expect(component.columns).toEqual(expectedColumns);
  });

  describe('onActionClick', () => {
    const spyEvent = createSpyObject(MouseEvent);
    let testSftpAccount: SftpAccountViewModel;

    beforeEach(() => {
      testSftpAccount = getTestSftpAccount();
    });

    it('should dispatch editSftpAccount', () => {
      component.onActionClick(spyEvent, testSftpAccount);

      expect(store.dispatch).toHaveBeenCalledWith(
        SftpAccountsActions.sftpAccountComponentEditAccount({ sftpAccount: testSftpAccount })
      );
    });

    it('should not dispatch editSftpAccount if hasUpdatePermission is false', () => {
      component.onActionClick(spyEvent, { ...testSftpAccount, hasUpdatePermission: false });
      expect(store.dispatch).not.toHaveBeenCalled();
    });
  });

  describe('onManageClickPublicKeys', () => {
    const spyEvent = createSpyObject(MouseEvent);
    let testSftpAccount: SftpAccountViewModel;

    beforeEach(() => {
      testSftpAccount = getTestSftpAccount();
    });

    it('should dispatch sftpAccountComponentManageAccountPublicKeys', () => {
      component.onManagePublicKeysClick(spyEvent, testSftpAccount);

      expect(store.dispatch).toHaveBeenCalledWith(
        SftpAccountsActions.sftpAccountComponentManageAccountPublicKeys({ sftpAccount: testSftpAccount })
      );
    });

    it('should not dispatch sftpAccountComponentManageAccountPublicKeys if hasUpdatePermission is false', () => {
      component.onManagePublicKeysClick(spyEvent, { ...testSftpAccount, hasUpdatePermission: false });
      expect(store.dispatch).not.toHaveBeenCalled();
    });
  });
});
