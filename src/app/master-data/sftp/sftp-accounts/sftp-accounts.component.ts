/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';

import { SftpAccountOwner, SftpAccountState, SftpAccountViewModel } from '~app/master-data/sftp/models';
import { SftpAccountsActions } from '~app/master-data/sftp/store/actions';
import * as fromSftp from '~app/master-data/sftp/store/reducers';
import { AppState } from '~app/reducers';
import { SimpleTableColumn } from '~app/shared/components';
import { AlertMessageType } from '~app/shared/enums';

const translationLabelsNs = 'menu.masterData.configuration.sftpConfigurations.labels';

@Component({
  selector: 'bspl-sftp-accounts',
  templateUrl: './sftp-accounts.component.html',
  styleUrls: ['./sftp-accounts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SftpAccountsComponent implements OnInit {
  public SftpAccountState = SftpAccountState;
  public SftpAccountOwner = SftpAccountOwner;

  public AlertMessageType = AlertMessageType;

  public isLoading$ = this.store.pipe(select(fromSftp.getSftpAccountsLoading));

  public data$ = this.store.pipe(select(fromSftp.getSftpAccountsData));

  public columns: SimpleTableColumn<any>[];

  @ViewChild('actionTemplate', { static: true })
  private actionTemplate: TemplateRef<any>;

  @ViewChild('publicKeysTemplate', { static: true })
  private publicKeysTemplate: TemplateRef<any>;

  constructor(private store: Store<AppState>, private translationService: L10nTranslationService) {}

  ngOnInit(): void {
    this.initColumns();
  }

  private initColumns() {
    this.columns = [
      {
        header: `${translationLabelsNs}.sftpAccountType`,
        field: 'sftpAccountTypeText',
        customStyle: 'font-weight-bold'
      },
      {
        header: `${translationLabelsNs}.login`,
        field: 'login'
      },
      {
        header: `${translationLabelsNs}.status`,
        field: 'stateText'
      },
      {
        header: `${translationLabelsNs}.action`,
        field: 'actionText',
        cellTemplate: this.actionTemplate
      },
      {
        header: `${translationLabelsNs}.publicKeys`,
        field: 'publicKeys',
        cellTemplate: this.publicKeysTemplate
      },
      {
        header: `${translationLabelsNs}.owner`,
        field: 'ownerText',
        tooltip: (account: SftpAccountViewModel) =>
          account.owner
            ? this.translationService.translate(
                `menu.masterData.configuration.sftpConfigurations.accountOwner.tooltip.${account.owner}`
              )
            : null
      }
    ];
  }

  public onActionClick(event: MouseEvent, sftpAccount: SftpAccountViewModel) {
    event.preventDefault();

    if (sftpAccount.hasUpdatePermission) {
      this.store.dispatch(SftpAccountsActions.sftpAccountComponentEditAccount({ sftpAccount }));
    }
  }

  public onManagePublicKeysClick(event: MouseEvent, sftpAccount: SftpAccountViewModel) {
    event.preventDefault();

    if (sftpAccount.hasUpdatePermission) {
      this.store.dispatch(SftpAccountsActions.sftpAccountComponentManageAccountPublicKeys({ sftpAccount }));
    }
  }

  public canManagePublicKeys(sftpAccount: SftpAccountViewModel): boolean {
    return sftpAccount.state === SftpAccountState.Active && sftpAccount.owner === SftpAccountOwner.Yes;
  }
}
