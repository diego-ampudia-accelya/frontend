import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { SftpAccountsService } from '../services/sftp-accounts-list/sftp-accounts.service';
import { SftpAccountsListComponent } from './sftp-accounts-list.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { Permissions } from '~app/shared/constants';
import { User, UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { SharedModule } from '~app/shared/shared.module';
import { TranslatePipeMock } from '~app/test';

const query = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery;

const query$ = of(query);

describe('SftpAccountsListComponent', () => {
  let component: SftpAccountsListComponent;
  let fixture: ComponentFixture<SftpAccountsListComponent>;

  const sftpAccountsServiceSpy = createSpyObject(SftpAccountsService);
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);

  const expectedUserDetails: Partial<User> = {
    id: 10126,
    email: 'agent@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.IATA,
    permissions: [Permissions.readSftpAccounts],
    bspPermissions: [
      { bspId: 1, permissions: [Permissions.readSftpAccounts] },
      { bspId: 2, permissions: [] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: {
      user: expectedUserDetails
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SftpAccountsListComponent, TranslatePipeMock],
      imports: [SharedModule, L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockStore({
          initialState,
          selectors: [{ selector: getUser, value: expectedUserDetails }]
        }),
        { provide: SftpAccountsService, useValue: sftpAccountsServiceSpy },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: QueryableDataSource, useValue: { hasData$: of(true), appliedQuery$: query$, get: () => {} } },
        FormBuilder,
        HttpClient,
        LocalBspTimeService
      ]
    })
      .overrideComponent(SftpAccountsListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: { hasData$: of(true), appliedQuery$: query$, get: () => {} } },
            { provide: DefaultQueryStorage, useValue: queryStorageSpy }
          ]
        }
      })
      .compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(SftpAccountsListComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('totalItemsMessage$', () => {
    it('should call translate with total elements', fakeAsync(() => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('10 items');

      component.totalItemsMessage$.subscribe();
      tick();

      expect((component as any).translationService.translate).toHaveBeenCalledWith('SFTP_ACCOUNTS.totalItemsMessage', {
        total: 10
      });
    }));

    it('should retun string with total elements', fakeAsync(() => {
      (component as any).translationService.translate = jasmine.createSpy().and.returnValue('10 items');
      let res;

      component.totalItemsMessage$.subscribe(str => (res = str));
      tick();

      expect(res).toBe('10 items');
    }));
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      (component as any).initializeDropdowns = jasmine.createSpy();
      (component as any).initializeColumns = jasmine.createSpy();
      (component as any).buildSearchForm = jasmine
        .createSpy()
        .and.returnValue(new FormGroup({ status: new FormControl() }));
      (component as any).queryStorage.get = jasmine.createSpy().and.returnValue(query);
      component.loadData = jasmine.createSpy();
    });

    it('should call initializeColumns', () => {
      component.ngOnInit();

      expect((component as any).initializeColumns).toHaveBeenCalled();
    });

    it('should call buildSearchForm', () => {
      component.ngOnInit();

      expect((component as any).buildSearchForm).toHaveBeenCalled();
    });

    it('should set searchForm from buildSearchForm', () => {
      component.searchForm = new FormGroup({});

      component.ngOnInit();

      expect(component.searchForm.value).toEqual(new FormGroup({ status: new FormControl() }).value);
    });

    it('should call queryStorage.get', () => {
      component.ngOnInit();

      expect((component as any).queryStorage.get).toHaveBeenCalled();
    });

    it('should call loadData', () => {
      component.ngOnInit();

      expect(component.loadData).toHaveBeenCalledWith(query);
    });
  });

  describe('loadData', () => {
    it('should call dataSource.get with query', () => {
      (component as any).dataSource.get = jasmine.createSpy();

      component.loadData(query);

      expect((component as any).dataSource.get).toHaveBeenCalledWith(query);
    });

    it('should call queryStorage.save with query', () => {
      (component as any).queryStorage.save = jasmine.createSpy();

      component.loadData(query);

      expect((component as any).queryStorage.save).toHaveBeenCalledWith(query);
    });
    it('should call dataSource.get with default query', () => {
      (component as any).dataSource.get = jasmine.createSpy();

      component.loadData(null);

      expect((component as any).dataSource.get).toHaveBeenCalledWith(defaultQuery);
    });

    it('should call queryStorage.save with default  query', () => {
      (component as any).queryStorage.save = jasmine.createSpy();

      component.loadData(null);

      expect((component as any).queryStorage.save).toHaveBeenCalledWith(defaultQuery);
    });
  });

  describe('buildSearchForm', () => {
    it('should call createGroup', () => {
      (component as any).formFactory.createGroup = jasmine
        .createSpy()
        .and.returnValue(new FormGroup({ status: new FormControl() }));

      (component as any).buildSearchForm();

      expect((component as any).formFactory.createGroup).toHaveBeenCalledWith({
        login: [],
        creationDate: []
      });
    });

    it('should return group', () => {
      (component as any).formFactory.createGroup = jasmine
        .createSpy()
        .and.returnValue(new FormGroup({ status: new FormControl() }));

      const res = (component as any).buildSearchForm();

      expect(res.value).toEqual(new FormGroup({ status: new FormControl() }).value);
    });
  });

  describe('initializeColumns', () => {
    it('should call buildColumns', () => {
      (component as any).buildColumns = jasmine.createSpy().and.returnValue([]);

      (component as any).initializeColumns();

      expect((component as any).buildColumns).toHaveBeenCalled();
    });
  });

  describe('buildColumns', () => {
    it('should return proper columns', () => {
      const res = (component as any).buildColumns();

      expect(res[0].prop).toBe('login');
      expect(res[1].prop).toBe('creationDate');
    });
  });
});
