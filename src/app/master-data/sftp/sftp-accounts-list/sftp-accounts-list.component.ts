import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SftpAccountsFilterFormatter } from '../services/sftp-accounts-list/sftp-accounts-formatter';
import { SftpAccountsService } from '../services/sftp-accounts-list/sftp-accounts.service';
import { SftpAccountFilters } from '~app/master-data/sftp/models';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';

@Component({
  selector: 'bspl-sftp-accounts-list',
  templateUrl: './sftp-accounts-list.component.html',
  styleUrls: ['./sftp-accounts-list.component.scss'],
  providers: [DefaultQueryStorage, QueryableDataSource, { provide: QUERYABLE, useExisting: SftpAccountsService }]
})
export class SftpAccountsListComponent implements OnInit {
  public columns: GridColumn[] = [];
  public searchForm: FormGroup;

  public statusOptions: DropdownOption[] = [];

  public totalItemsMessage$: Observable<string> = this.dataSource.appliedQuery$.pipe(
    map(query => query.paginateBy.totalElements),
    map(total =>
      this.translationService.translate('SFTP_ACCOUNTS.totalItemsMessage', {
        total
      })
    )
  );

  private formFactory: FormUtil;

  constructor(
    public displayFilterFormatter: SftpAccountsFilterFormatter,
    public dataSource: QueryableDataSource<SftpAccountFilters>,
    private queryStorage: DefaultQueryStorage,
    private sftpAccountsService: SftpAccountsService,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private formBuilder: FormBuilder
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeColumns();
    this.searchForm = this.buildSearchForm();
    const storedQuery = this.queryStorage.get();
    this.loadData(storedQuery);
  }

  public loadData(query?: DataQuery): void {
    query = query || cloneDeep(defaultQuery);
    this.dataSource.get(query);
    this.queryStorage.save(query);
  }

  private buildSearchForm(): FormGroup {
    return this.formFactory.createGroup<SftpAccountFilters>({
      login: [],
      creationDate: []
    });
  }

  private initializeColumns(): void {
    this.columns = this.buildColumns();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.downloadDialog.title'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.queryStorage.get()
      },
      apiService: this.sftpAccountsService
    });
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        prop: 'login',
        name: 'SFTP_ACCOUNTS.columns.login',
        width: 160,
        resizeable: true,
        draggable: false
      },

      {
        prop: 'creationDate',
        name: 'SFTP_ACCOUNTS.columns.creationDate',
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 145,
        resizeable: true,
        draggable: false
      }
    ];
  }
}
