import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { SftpAccountEditDialogComponent } from './sftp-account-edit-dialog.component';
import { SftpAccountEditDialogActions } from '~app/master-data/sftp/store/actions';
import { getInitialState, getTestSftpAccount } from '~app/master-data/sftp/test/sftp.test.mocks';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TranslatePipeMock } from '~app/test';

const testSftpAccount = getTestSftpAccount();

describe('SftpAccountEditDialogComponent', () => {
  let component: SftpAccountEditDialogComponent;
  let fixture: ComponentFixture<SftpAccountEditDialogComponent>;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Create,
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Create
          }
        ],
        sftpAccount: { ...testSftpAccount }
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      declarations: [SftpAccountEditDialogComponent, TranslatePipeMock],
      providers: [
        provideMockStore({ initialState: getInitialState() }),
        { provide: DialogConfig, useValue: mockConfig },
        FormBuilder,
        ReactiveSubject
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SftpAccountEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('save button click', () => {
    it('and form is valid, should dispatch saveSftpAccountPassword', fakeAsync(() => {
      component.sftpAccount = { ...testSftpAccount };
      const store = TestBed.inject(MockStore);
      spyOn(store, 'dispatch').and.callThrough();
      component.form.setValue({ password: 'test4test', confirmPassword: 'test4test' });
      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit({
        clickedBtn: FooterButton.Create
      });

      expect(store.dispatch).toHaveBeenCalledWith(
        SftpAccountEditDialogActions.sftpAccountEditDialogFormValidStateChange({
          isValid: true
        })
      );

      expect(store.dispatch).toHaveBeenCalledWith(
        SftpAccountEditDialogActions.saveSftpAccountPassword({
          sftpAccount: { ...testSftpAccount },
          password: 'test4test'
        })
      );
    }));

    it('and form is not valid, should not dispatch saveSftpAccountPassword', fakeAsync(() => {
      component.sftpAccount = { ...testSftpAccount };
      const store = TestBed.inject(MockStore);
      spyOn(store, 'dispatch').and.callThrough();
      component.form.setValue({ password: 'te', confirmPassword: 'te' });
      const reactiveSubject = TestBed.inject(ReactiveSubject);
      reactiveSubject.emit({
        clickedBtn: FooterButton.Create
      });

      expect(store.dispatch).toHaveBeenCalledWith(
        SftpAccountEditDialogActions.sftpAccountEditDialogFormValidStateChange({
          isValid: false
        })
      );

      expect(store.dispatch).not.toHaveBeenCalledWith(
        SftpAccountEditDialogActions.saveSftpAccountPassword({
          sftpAccount: { ...testSftpAccount },
          password: 'te'
        })
      );
    }));
  });

  describe('form validity', () => {
    it('should be valid when password and confirmPassword are specified with proper complexity', () => {
      component.form.setValue({
        password: 'Passw0rd',
        confirmPassword: 'Passw0rd'
      });

      expect(component.form.valid).toBe(true);
    });

    it('should be invalid when both password and confirm password are not specified', () => {
      const { password, confirmPassword } = component.form.controls;

      expect(component.form.valid).toBe(false);
      expect(password.errors).toEqual({ required: true });
      expect(confirmPassword.errors).toEqual({ required: true });
    });

    it('should be invalid when password does not satisfy complexity requirements', () => {
      const { password, confirmPassword } = component.form.controls;

      component.form.setValue({
        password: '123',
        confirmPassword: null
      });

      expect(component.form.valid).toBe(false);
      // eslint-disable-next-line @typescript-eslint/naming-convention
      expect(password.errors).toEqual({ sftp_password_length: true });
      expect(confirmPassword.errors).toEqual({ required: true, passwordsMustMatch: true });
    });

    it('should be invalid when password and confirm password do not match', () => {
      const { password, confirmPassword } = component.form.controls;

      component.form.setValue({
        password: 'Passw0rd',
        confirmPassword: 'no-match'
      });

      expect(component.form.valid).toBe(false);
      expect(password.errors).toEqual(null);
      expect(confirmPassword.errors).toEqual({ passwordsMustMatch: true });
    });

    it('should mark confirm password as invalid when password is changed so they no longer match', () => {
      const { password, confirmPassword } = component.form.controls;

      component.form.setValue({
        password: 'Passw0rd',
        confirmPassword: 'Passw0rd'
      });

      password.setValue('Passw0r');

      expect(component.form.valid).toBe(false);
      expect(password.valid).toBe(true);
      expect(confirmPassword.errors).toEqual({ passwordsMustMatch: true });
    });
  });
});
