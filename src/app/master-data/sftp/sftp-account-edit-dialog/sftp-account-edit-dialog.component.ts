import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';

import { SftpAccountViewModel } from '~app/master-data/sftp/models';
import { SftpAccountEditDialogActions } from '~app/master-data/sftp/store/actions';
import * as fromSftp from '~app/master-data/sftp/store/reducers';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileValidator, FileValidators } from '~app/shared/components/upload/file-validators';
import { FormUtil } from '~app/shared/helpers';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { confirmPasswordValidator } from '~app/shared/validators/confirm-password.validator';
import { sftpPasswordValidator } from '~app/shared/validators/sftp-password.validator';

const translationEditDialogNs = 'menu.masterData.configuration.sftpConfigurations.editDialog';
const saveButtonTypes = [FooterButton.Create, FooterButton.Modify, FooterButton.Reactivate];

@Component({
  selector: 'bspl-sftp-account-edit-dialog',
  templateUrl: './sftp-account-edit-dialog.component.html',
  styleUrls: ['./sftp-account-edit-dialog.component.scss']
})
export class SftpAccountEditDialogComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();
  public sftpAccount: SftpAccountViewModel;
  public form: FormGroup;

  public loading$: Observable<boolean> = this.store.pipe(
    select(fromSftp.getSftpAccountEditLoading),
    tap(loading => this.setLoading(loading)),
    takeUntil(this.destroy$)
  );

  public saveButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter((res: any) => saveButtonTypes.includes(res.clickedBtn)),
    takeUntil(this.destroy$)
  );

  public validationRules: FileValidator[] = [
    FileValidators.maxFileSize(),
    FileValidators.filenamePattern(/^(.*)\.pub/, `${translationEditDialogNs}.errors.publicKeysFileFormat`)
  ];

  private saveButton: ModalAction;

  constructor(
    private config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private formBuilder: FormBuilder,
    private store: Store<AppState>
  ) {}

  public ngOnInit(): void {
    this.sftpAccount = this.config.data.sftpAccount as SftpAccountViewModel;
    this.createForm();
    this.initializeSaveButton();
    this.initializeSaveButtonSubscription();
  }

  private createForm() {
    this.form = this.formBuilder.group({
      password: ['', [Validators.required, sftpPasswordValidator]],
      confirmPassword: ['', [Validators.required, confirmPasswordValidator]]
    });

    const password = this.form.get('password');
    const confirmPassword = this.form.get('confirmPassword');

    password.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      confirmPassword.updateValueAndValidity();
    });

    this.form.statusChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(
        SftpAccountEditDialogActions.sftpAccountEditDialogFormValidStateChange({
          isValid: this.form.valid
        })
      );
    });
  }

  private initializeSaveButton() {
    this.saveButton = this.config.data.buttons.find((button: ModalAction) =>
      saveButtonTypes.includes(button.type as FooterButton)
    );

    this.store
      .pipe(select(fromSftp.getSftpAccountEditIsSaveButtonDisabled), takeUntil(this.destroy$))
      .subscribe(isSaveButtonDisabled => (this.saveButton.isDisabled = isSaveButtonDisabled));

    this.loading$.subscribe();
  }

  private initializeSaveButtonSubscription() {
    this.saveButtonClick$.subscribe(() => {
      FormUtil.showControlState(this.form);
      this.savePassword();
    });
  }

  private savePassword() {
    if (this.form.valid) {
      this.store.dispatch(
        SftpAccountEditDialogActions.saveSftpAccountPassword({
          sftpAccount: this.sftpAccount,
          password: this.form.value.password
        })
      );
    }
  }

  private setLoading(loading: boolean) {
    this.saveButton.isLoading = loading;
  }

  public ngOnDestroy() {
    this.destroy$.next();
    this.store.dispatch(SftpAccountEditDialogActions.sftpAccountEditDialogDestroy());
  }
}
