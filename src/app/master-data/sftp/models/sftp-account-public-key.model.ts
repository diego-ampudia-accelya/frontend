export interface SftpAccountPublicKey {
  login: string;
  filename: string;
}
