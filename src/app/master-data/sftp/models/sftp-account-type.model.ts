/* eslint-disable @typescript-eslint/naming-convention */
export enum SftpAccountType {
  Download = 'download',
  Upload = 'upload',
  AdditionalUpload = 'additionalUpload',
  MassDownload = 'massDownload',
  Global = 'global'
}
