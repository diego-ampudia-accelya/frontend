export interface SftpAccountList {
  login?: string;
  creationDate?: Date[];
}

export interface SftpAccountFilters {
  login?: string;
  creationDate?: Date[];
}
export interface SftpAccountFiltersBE {
  login?: string;
  fromCreationDate?: string;
  toCreationDate?: string;
}
