import { SftpAccount } from './sftp-account.model';

export interface SftpAccountViewModel extends SftpAccount {
  sftpAccountTypeText: string;
  stateText: string;
  actionText: string;
  ownerText: string;
  hasUpdatePermission: boolean;
}
