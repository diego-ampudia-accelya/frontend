/* eslint-disable @typescript-eslint/naming-convention */
export enum SftpAccountActionType {
  Create = 'create',
  Modify = 'modify',
  Reactivate = 'reactivate'
}
