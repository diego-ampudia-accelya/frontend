import { SftpAccountOwner } from './sftp-account-owner.model';
import { SftpAccountPublicKey } from './sftp-account-public-key.model';
import { SftpAccountState } from './sftp-account-state.model';
import { SftpAccountType } from './sftp-account-type.model';

export interface SftpAccount {
  sftpAccountType: SftpAccountType;
  login: string;
  state: SftpAccountState;
  publicKeys: SftpAccountPublicKey[];
  owner: SftpAccountOwner;
}
