/* eslint-disable @typescript-eslint/naming-convention */
export enum SftpAccountState {
  Active = 'active',
  Pending = 'pending',
  Expired = 'expired',
  NonExisting = 'nonExisting'
}
