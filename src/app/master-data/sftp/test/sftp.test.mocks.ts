import { cloneDeep } from 'lodash';

import {
  SftpAccountOwner,
  SftpAccountPublicKey,
  SftpAccountState,
  SftpAccountType,
  SftpAccountViewModel
} from '~app/master-data/sftp/models';
import * as fromSftpAccountDeletePublicKeysConfirmation from '~app/master-data/sftp/store/reducers/sftp-account-delete-public-keys-confirmation.reducer';
import * as fromSftpAccountEditState from '~app/master-data/sftp/store/reducers/sftp-account-edit.reducer';
import * as fromSftpAccountManagePublicKeys from '~app/master-data/sftp/store/reducers/sftp-account-manage-public-keys.reducer';
import * as fromSftpAccountsState from '~app/master-data/sftp/store/reducers/sftp-accounts.reducer';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { UploadStatus } from '~app/shared/components/upload/upload-mode.enum';
import { createAirlineUser } from '~app/shared/mocks/airline-user';

export const sftpAccountBEMock = {
  login: 'A6081',
  sftpAccountType: 'Download',
  state: 'Non existing',
  owner: 'Yes'
};

export const testSftpAccount = {
  login: 'A6081',
  sftpAccountType: SftpAccountType.Download,
  sftpAccountTypeText: 'Download',
  state: null,
  actionText: 'Modify',
  stateText: 'Active',
  hasUpdatePermission: true,
  publicKeys: [],
  owner: SftpAccountOwner.Yes,
  ownerText: 'Yes'
};

export function getTestSftpAccount(state = SftpAccountState.Active, hasUpdatePermission = true): SftpAccountViewModel {
  return {
    ...testSftpAccount,
    hasUpdatePermission,
    state
  };
}

export function getTestSftpAccountPublicKey(login = 'A6081', filename = 'test1.pub'): SftpAccountPublicKey {
  return { login, filename };
}

export function getTestSftpAccountPublicKeys(): SftpAccountPublicKey[] {
  return [
    {
      login: 'A6081',
      filename: 'test1.pub'
    },
    {
      login: 'A6081',
      filename: 'test2.pub'
    }
  ];
}

export function getTestSftpAccountWithPublicKeys(sftpAccountType = SftpAccountType.Download) {
  return {
    ...getTestSftpAccount(),
    sftpAccountType,
    publicKeys: getTestSftpAccountPublicKeys()
  };
}

export function getInitialState() {
  return {
    auth: {
      user: createAirlineUser()
    },
    sftp: {
      [fromSftpAccountsState.sftpAccountsFeatureKey]: cloneDeep(fromSftpAccountsState.initialState),
      [fromSftpAccountEditState.sftpAccountEditFeatureKey]: cloneDeep(fromSftpAccountEditState.initialState),
      [fromSftpAccountManagePublicKeys.sftpAccountManagePublicKeysFeatureKey]: cloneDeep(
        fromSftpAccountManagePublicKeys.initialState
      ),
      [fromSftpAccountDeletePublicKeysConfirmation.sftpAccountDeletePublicKeysConfirmationFeatureKey]: cloneDeep(
        fromSftpAccountDeletePublicKeysConfirmation.initialState
      )
    }
  };
}

export function getTestUploadFile(name: string, status: UploadStatus): FileUpload {
  return {
    name,
    status,
    progress: 100,
    size: 300,
    fileInstance: null,
    isValid: true,
    error: null,
    cancelled: null,
    finished: null
  };
}
