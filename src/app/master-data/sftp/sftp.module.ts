import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SftpAccountDeletePublicKeysConfirmationDialogComponent } from './sftp-account-delete-public-keys-confirmation-dialog/sftp-account-delete-public-keys-confirmation-dialog.component';
import { SftpAccountEditDialogComponent } from './sftp-account-edit-dialog/sftp-account-edit-dialog.component';
import { SftpAccountManagePublicKeysDialogComponent } from './sftp-account-manage-public-keys-dialog/sftp-account-manage-public-keys-dialog.component';
import { SftpAccountsListComponent } from './sftp-accounts-list/sftp-accounts-list.component';
import { SftpAccountsComponent } from './sftp-accounts/sftp-accounts.component';
import { SftpAccountsEffects } from './store/effects/sftp-accounts.effects';
import { SharedModule } from '~app/shared/shared.module';
import * as fromSftp from '~app/master-data/sftp/store/reducers';

@NgModule({
  declarations: [
    SftpAccountsComponent,
    SftpAccountsListComponent,
    SftpAccountEditDialogComponent,
    SftpAccountManagePublicKeysDialogComponent,
    SftpAccountDeletePublicKeysConfirmationDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromSftp.sftpFeatureKey, fromSftp.reducers),
    EffectsModule.forFeature([SftpAccountsEffects])
  ],
  exports: [],
  providers: []
})
export class SftpModule {}
