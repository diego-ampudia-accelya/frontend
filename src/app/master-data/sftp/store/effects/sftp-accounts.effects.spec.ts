import { fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { catchError, skip, take } from 'rxjs/operators';

import { SftpAccountType } from '~app/master-data/sftp/models';
import { SftpAccountsService } from '~app/master-data/sftp/services/sftp-accounts.service';
import {
  SftpAccountDeletePublicKeysConfirmationDialogActions as DeleteKeysConfirmDialogActions,
  SftpAccountEditDialogActions,
  SftpAccountManagePublicKeysDialogActions,
  SftpAccountsActions
} from '~app/master-data/sftp/store/actions';
import { SftpAccountsEffects } from '~app/master-data/sftp/store/effects/sftp-accounts.effects';
import {
  getInitialState,
  getTestSftpAccount,
  getTestSftpAccountPublicKeys,
  getTestUploadFile
} from '~app/master-data/sftp/test/sftp.test.mocks';
import { DialogService } from '~app/shared/components';
import { UploadStatus } from '~app/shared/components/upload/upload-mode.enum';

describe('SftpAccountsEffects', () => {
  let effects: SftpAccountsEffects;
  let actions$: Observable<Action>;
  let sftpAccountsServiceSpy: SpyObject<SftpAccountsService>;
  let dialogServiceSpy: SpyObject<DialogService>;

  beforeEach(waitForAsync(() => {
    sftpAccountsServiceSpy = createSpyObject(SftpAccountsService);
    sftpAccountsServiceSpy.getSftpAccounts.and.returnValue(of([]));
    dialogServiceSpy = createSpyObject(DialogService);

    TestBed.configureTestingModule({
      providers: [
        SftpAccountsEffects,
        provideMockStore({ initialState: getInitialState() }),
        provideMockActions(() => actions$),
        { provide: SftpAccountsService, useValue: sftpAccountsServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    });
  }));

  beforeEach(() => {
    effects = TestBed.inject(SftpAccountsEffects);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('$requestSubUserDetails', () => {
    it('should dispatch loadSftpAccountsSuccess', fakeAsync(() => {
      actions$ = of(SftpAccountsActions.loadSftpAccounts);

      effects.$requestSubUserDetails.subscribe(action => {
        expect(action.type).toBe(SftpAccountsActions.loadSftpAccountsComplete.type);
      });
    }));
  });

  describe('$sftpAccountComponentEditAccount', () => {
    it('should call open dialog', fakeAsync(() => {
      actions$ = of(SftpAccountsActions.sftpAccountComponentEditAccount({ sftpAccount: getTestSftpAccount() }));

      dialogServiceSpy.open.and.returnValue(of({}));

      effects.$sftpAccountComponentEditAccount.subscribe(() => {
        expect(dialogServiceSpy.open).toHaveBeenCalled();
      });
    }));
  });

  describe('$saveSftpAccountPassword', () => {
    it('should call sftpAccountsService save and return saveSftpAccountPasswordSuccess on success', fakeAsync(() => {
      sftpAccountsServiceSpy.save.and.returnValue(of({}));

      actions$ = of(
        SftpAccountEditDialogActions.saveSftpAccountPassword({
          sftpAccount: getTestSftpAccount(),
          password: 'password'
        })
      );

      effects.$saveSftpAccountPassword.subscribe(action => {
        expect(action.type).toBe(SftpAccountEditDialogActions.saveSftpAccountPasswordSuccess.type);
      });
    }));

    it('should call sftpAccountsService save and return saveSftpAccountPasswordError on error', fakeAsync(() => {
      sftpAccountsServiceSpy.save.and.returnValue(throwError(new Error('text')));

      actions$ = of(
        SftpAccountEditDialogActions.saveSftpAccountPassword({
          sftpAccount: getTestSftpAccount(),
          password: 'password'
        })
      );

      effects.$saveSftpAccountPassword.pipe(take(1)).subscribe(action => {
        expect(action.type).toBe(SftpAccountEditDialogActions.saveSftpAccountPasswordError.type);
      });
    }));
  });

  describe('$updateSftpAccountSuccess', () => {
    it('should return SftpAccountsActions.loadSftpAccounts and call close dialog on saveSftpAccountPasswordSuccess', fakeAsync(() => {
      dialogServiceSpy.close.and.callThrough();
      actions$ = of(SftpAccountEditDialogActions.saveSftpAccountPasswordSuccess);

      effects.$updateSftpAccountSuccess.subscribe(action => {
        expect(dialogServiceSpy.close).toHaveBeenCalled();
        expect(action.type).toBe(SftpAccountsActions.loadSftpAccounts.type);
      });
    }));

    it('should return SftpAccountsActions.loadSftpAccounts and call close dialog on sftpAccountEditDialogUploadSuccess', fakeAsync(() => {
      dialogServiceSpy.close.and.callThrough();
      actions$ = of(SftpAccountEditDialogActions.sftpAccountEditDialogUploadSuccess);

      effects.$updateSftpAccountSuccess.subscribe(action => {
        expect(dialogServiceSpy.close).toHaveBeenCalled();
        expect(action.type).toBe(SftpAccountsActions.loadSftpAccounts.type);
      });
    }));
  });

  describe('$publicKeysUploadEnd', () => {
    it('should return loadSftpAccountComplete action with sftpAccount and uploadedFiles', fakeAsync(() => {
      const testSftpAccount = getTestSftpAccount();
      sftpAccountsServiceSpy.getSftpAccount.and.returnValue(of(testSftpAccount));
      const uploadedFiles = [getTestUploadFile('test1.pub', UploadStatus.completed)];

      actions$ = of(
        SftpAccountManagePublicKeysDialogActions.publicKeysUploadEnd({
          sftpAccountType: SftpAccountType.Download,
          uploadedFiles
        })
      );

      effects.$publicKeysUploadEnd.subscribe(action => {
        expect(action.type).toBe(SftpAccountManagePublicKeysDialogActions.loadSftpAccountComplete.type);
        expect(action.sftpAccount).toEqual(testSftpAccount);
        expect(action.uploadedFiles).toEqual(uploadedFiles);
      });
    }));
  });

  describe('$requestSftpAccountPublicKeysDelete', () => {
    it('should return deleteAccountPublicKeysSuccess and call dialogService close, when there are errors', fakeAsync(() => {
      const testSftpAccount = getTestSftpAccount();
      const testPublicKeys = getTestSftpAccountPublicKeys();

      sftpAccountsServiceSpy.deletePublicKeys.and.returnValue(
        of({
          sftpAccount: testSftpAccount,
          publicKeys: testPublicKeys,
          errorMessage: ''
        })
      );

      actions$ = of(
        DeleteKeysConfirmDialogActions.requestDeleteSftpAccountPublicKeys({
          sftpAccount: testSftpAccount,
          publicKeys: testPublicKeys,
          bspId: 100
        })
      );

      dialogServiceSpy.close.and.callThrough();

      effects.$requestSftpAccountPublicKeysDelete.subscribe((action: any) => {
        expect(action.type).toBe(DeleteKeysConfirmDialogActions.deleteAccountPublicKeysSuccess.type);
        expect(action.sftpAccount).toEqual(testSftpAccount);
        expect(action.publicKeys).toEqual(testPublicKeys);
        expect(action.errorMessage).toEqual('');
        expect(dialogServiceSpy.close).toHaveBeenCalled();
      });
    }));

    it('should return deleteAccountPublicKeysError on errors', fakeAsync(() => {
      const error = new Error('error message');
      sftpAccountsServiceSpy.deletePublicKeys.and.returnValue(throwError(error));

      actions$ = of(
        DeleteKeysConfirmDialogActions.requestDeleteSftpAccountPublicKeys({
          sftpAccount: getTestSftpAccount(),
          publicKeys: getTestSftpAccountPublicKeys(),
          bspId: 100
        })
      );

      effects.$requestSftpAccountPublicKeysDelete.pipe(take(1)).subscribe((action: any) => {
        expect(action.type).toBe(DeleteKeysConfirmDialogActions.deleteAccountPublicKeysError.type);
        expect(action.error).toEqual(error);
      });
    }));

    it('should throw error', fakeAsync(() => {
      const error = new Error('error message');
      sftpAccountsServiceSpy.deletePublicKeys.and.returnValue(throwError(error));

      actions$ = of(
        DeleteKeysConfirmDialogActions.requestDeleteSftpAccountPublicKeys({
          sftpAccount: getTestSftpAccount(),
          publicKeys: getTestSftpAccountPublicKeys(),
          bspId: 100
        })
      );

      effects.$requestSftpAccountPublicKeysDelete
        .pipe(
          skip(1),
          take(1),
          catchError(err => of(err))
        )
        .subscribe(err => expect(err).toEqual(error));
    }));
  });

  describe('$sftpAccountComponentManageAccountPublicKeys', () => {
    [
      SftpAccountsActions.sftpAccountComponentManageAccountPublicKeys,
      SftpAccountsActions.sftpAccountComponentManageAccountPublicKeys
    ].forEach(action => {
      it(`${action.type} should open manage sftp account public keys dialog`, fakeAsync(() => {
        actions$ = of(action);

        effects.$sftpAccountComponentManageAccountPublicKeys.subscribe(() => {
          expect(dialogServiceSpy.open).toHaveBeenCalled();
        });
      }));
    });
  });

  describe('$deleteAccountPublicKeysSuccess', () => {
    it('deleteAccountPublicKeysSuccess should open ', fakeAsync(() => {
      actions$ = of(
        DeleteKeysConfirmDialogActions.deleteAccountPublicKeysSuccess({
          sftpAccount: getTestSftpAccount(),
          publicKeys: getTestSftpAccountPublicKeys(),
          errorMessage: ''
        })
      );

      effects.$deleteAccountPublicKeysSuccess.subscribe(() => {
        expect(dialogServiceSpy.open).toHaveBeenCalled();
      });
    }));
  });

  describe('$deletePublicKeys', () => {
    it('deletePublicKeys should call dialog service close and open ', fakeAsync(() => {
      actions$ = of(
        SftpAccountManagePublicKeysDialogActions.deletePublicKeys({
          sftpAccount: getTestSftpAccount(),
          publicKeys: getTestSftpAccountPublicKeys()
        })
      );

      effects.$deletePublicKeys.subscribe(() => {
        expect(dialogServiceSpy.close).toHaveBeenCalled();
        expect(dialogServiceSpy.open).toHaveBeenCalled();
      });
    }));
  });
});
