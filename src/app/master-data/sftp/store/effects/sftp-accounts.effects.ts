import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { merge, of, throwError } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { SftpAccountPublicKey, SftpAccountViewModel } from '~app/master-data/sftp/models';
import {
  sftpAccountActionTypeFooterButtonMap,
  sftpAccountStateActionTypesMap
} from '~app/master-data/sftp/services/sftp-account.map.constants';
import { SftpAccountsService } from '~app/master-data/sftp/services/sftp-accounts.service';
import { SftpAccountDeletePublicKeysConfirmationDialogComponent } from '~app/master-data/sftp/sftp-account-delete-public-keys-confirmation-dialog/sftp-account-delete-public-keys-confirmation-dialog.component';
import { SftpAccountEditDialogComponent } from '~app/master-data/sftp/sftp-account-edit-dialog/sftp-account-edit-dialog.component';
import { SftpAccountManagePublicKeysDialogComponent } from '~app/master-data/sftp/sftp-account-manage-public-keys-dialog/sftp-account-manage-public-keys-dialog.component';
import {
  SftpAccountDeletePublicKeysConfirmationDialogActions as DeleteKeysConfirmDialogActions,
  SftpAccountEditDialogActions,
  SftpAccountManagePublicKeysDialogActions,
  SftpAccountsActions
} from '~app/master-data/sftp/store/actions';
import { ButtonDesign, DialogService, FooterButton } from '~app/shared/components';

const translationEditDialogNs = 'menu.masterData.configuration.sftpConfigurations.editDialog';
const translationManagePublicKeysDialogNs = 'menu.masterData.configuration.sftpConfigurations.managePublicKeysDialog';
const translationDeletePublicKeysConfirmationDialogNs =
  'menu.masterData.configuration.sftpConfigurations.deletePublicKeysConfirmationDialog';

@Injectable()
export class SftpAccountsEffects {
  $requestSubUserDetails = createEffect(() =>
    this.actions$.pipe(
      ofType(SftpAccountsActions.loadSftpAccounts),
      switchMap(() => this.sftAccountsService.getSftpAccounts()),
      map(data => SftpAccountsActions.loadSftpAccountsComplete({ data }))
    )
  );

  $sftpAccountComponentEditAccount = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SftpAccountsActions.sftpAccountComponentEditAccount),
        tap(({ sftpAccount }) => {
          const actionType = sftpAccountStateActionTypesMap.get(sftpAccount.state);

          this.dialogService.open(SftpAccountEditDialogComponent, {
            data: {
              title: `${translationEditDialogNs}.title.${actionType}Account`,
              hasCancelButton: true,
              isClosable: true,
              footerButtonsType: [{ type: sftpAccountActionTypeFooterButtonMap.get(actionType) }],
              sftpAccount
            }
          });
        })
      ),
    {
      dispatch: false
    }
  );

  $saveSftpAccountPassword = createEffect(() =>
    this.actions$.pipe(
      ofType(SftpAccountEditDialogActions.saveSftpAccountPassword),
      switchMap(({ sftpAccount, password }) => this.sftAccountsService.save(sftpAccount, password)),
      map((data: SftpAccountViewModel) =>
        SftpAccountEditDialogActions.saveSftpAccountPasswordSuccess({ sftpAccount: data })
      ),
      catchError(error =>
        merge(of(SftpAccountEditDialogActions.saveSftpAccountPasswordError({ error })), throwError(error))
      )
    )
  );

  $updateSftpAccountSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(
        SftpAccountEditDialogActions.saveSftpAccountPasswordSuccess,
        SftpAccountEditDialogActions.sftpAccountEditDialogUploadSuccess
      ),
      tap(() => {
        this.dialogService.close();
      }),
      map(() => SftpAccountsActions.loadSftpAccounts())
    )
  );

  $publicKeysUploadEnd = createEffect(() =>
    this.actions$.pipe(
      ofType(SftpAccountManagePublicKeysDialogActions.publicKeysUploadEnd),
      switchMap(({ sftpAccountType, uploadedFiles }) =>
        this.sftAccountsService
          .getSftpAccount(sftpAccountType)
          .pipe(
            map(sftpAccount =>
              SftpAccountManagePublicKeysDialogActions.loadSftpAccountComplete({ sftpAccount, uploadedFiles })
            )
          )
      )
    )
  );

  $requestSftpAccountPublicKeysDelete = createEffect(() =>
    this.actions$.pipe(
      ofType(DeleteKeysConfirmDialogActions.requestDeleteSftpAccountPublicKeys),
      switchMap(({ sftpAccount, publicKeys, bspId }) =>
        this.sftAccountsService.deletePublicKeys(sftpAccount.sftpAccountType, publicKeys, bspId)
      ),
      map(({ sftpAccount, publicKeys, errorMessage }) =>
        DeleteKeysConfirmDialogActions.deleteAccountPublicKeysSuccess({
          sftpAccount,
          publicKeys,
          errorMessage
        })
      ),
      tap(() => {
        this.dialogService.close();
      }),
      catchError(error =>
        merge(of(DeleteKeysConfirmDialogActions.deleteAccountPublicKeysError({ error })), throwError(error))
      )
    )
  );

  $sftpAccountComponentManageAccountPublicKeys = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          SftpAccountsActions.sftpAccountComponentManageAccountPublicKeys,
          DeleteKeysConfirmDialogActions.cancelSftpAccountPublicKeysDelete
        ),
        tap(({ sftpAccount }) => this.openSftpAccountManagePublicKeysDialog(sftpAccount))
      ),
    {
      dispatch: false
    }
  );

  $deleteAccountPublicKeysSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(DeleteKeysConfirmDialogActions.deleteAccountPublicKeysSuccess),
        tap(({ sftpAccount, publicKeys: deletedPublicKeys }) =>
          this.openSftpAccountManagePublicKeysDialog(sftpAccount, deletedPublicKeys)
        )
      ),
    {
      dispatch: false
    }
  );

  $deletePublicKeys = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SftpAccountManagePublicKeysDialogActions.deletePublicKeys),
        tap(({ sftpAccount, publicKeys }) => {
          this.dialogService.close();
          this.dialogService.open(SftpAccountDeletePublicKeysConfirmationDialogComponent, {
            data: {
              title: `${translationDeletePublicKeysConfirmationDialogNs}.title`,
              hasCancelButton: true,
              isClosable: true,
              footerButtonsType: [FooterButton.Delete],
              sftpAccount,
              publicKeys
            }
          });
        })
      ),
    {
      dispatch: false
    }
  );

  constructor(
    private actions$: Actions,
    private sftAccountsService: SftpAccountsService,
    private dialogService: DialogService
  ) {}

  private openSftpAccountManagePublicKeysDialog(
    sftpAccount: SftpAccountViewModel,
    deletedPublicKeys?: SftpAccountPublicKey[]
  ) {
    this.dialogService.open(SftpAccountManagePublicKeysDialogComponent, {
      data: {
        title: `${translationManagePublicKeysDialogNs}.title.managePublicKeys`,
        hasCancelButton: false,
        isClosable: true,
        footerButtonsType: [
          {
            type: FooterButton.Cancel,
            buttonDesign: ButtonDesign.Tertiary,
            title: 'BUTTON.DEFAULT.CLOSE'
          }
        ],
        sftpAccount,
        deletedPublicKeys
      }
    });
  }
}
