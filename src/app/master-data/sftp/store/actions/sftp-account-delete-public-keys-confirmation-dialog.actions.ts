import { createAction, props } from '@ngrx/store';

import { SftpAccountPublicKey, SftpAccountViewModel } from '~app/master-data/sftp/models';
import { ServerError } from '~app/shared/errors';

export const sftpAccountDeletePubicKeysConfirmationDialogDestroy = createAction(
  '[Sftp Account Delete Public Keys Confirmation Dialog] Destroy'
);

export const requestDeleteSftpAccountPublicKeys = createAction(
  '[Sftp Account Delete Public Keys Confirmation Dialog] Request Public Keys Delete',
  props<{ sftpAccount: SftpAccountViewModel; publicKeys: SftpAccountPublicKey[]; bspId: number }>()
);

export const deleteAccountPublicKeysSuccess = createAction(
  '[Sftp Account Delete Public Keys Confirmation Dialog] Delete Public Keys Success',
  props<{ sftpAccount: SftpAccountViewModel; publicKeys: SftpAccountPublicKey[]; errorMessage: string }>()
);

export const deleteAccountPublicKeysError = createAction(
  '[Sftp Account Delete Public Keys Confirmation Dialog] Delete Public Keys Error',
  props<{ error: ServerError }>()
);

export const cancelSftpAccountPublicKeysDelete = createAction(
  '[Sftp Account Delete Public Keys Confirmation Dialog] Cancel Public Keys Delete',
  props<{ sftpAccount: SftpAccountViewModel }>()
);
