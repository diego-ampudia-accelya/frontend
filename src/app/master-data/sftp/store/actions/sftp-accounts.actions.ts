import { createAction, props } from '@ngrx/store';

import { SftpAccountViewModel } from '~app/master-data/sftp/models';

export const loadSftpAccounts = createAction('[Sftp Accounts Resolve] Load Sftp Accounts');

export const loadSftpAccountsComplete = createAction(
  '[Sftp Accounts Resolve] Load Sftp Accounts Complete',
  props<{ data: SftpAccountViewModel[] }>()
);

export const sftpAccountComponentEditAccount = createAction(
  '[Sftp Accounts Component] Edit Account',
  props<{ sftpAccount: SftpAccountViewModel }>()
);

export const sftpAccountComponentManageAccountPublicKeys = createAction(
  '[Sftp Accounts Component] Manage Account Public Keys',
  props<{ sftpAccount: SftpAccountViewModel }>()
);
