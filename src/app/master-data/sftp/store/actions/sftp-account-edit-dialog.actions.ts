import { createAction, props } from '@ngrx/store';

import { SftpAccountViewModel } from '~app/master-data/sftp/models';
import { ServerError } from '~app/shared/errors';

export const sftpAccountEditDialogDestroy = createAction('[Sftp Account Edit Dialog] Destroy');

export const sftpAccountEditDialogFormValidStateChange = createAction(
  '[Sftp Account Edit Dialog] Form Valid State Change',
  props<{ isValid: boolean }>()
);

export const saveSftpAccountPassword = createAction(
  '[Sftp Account Edit Dialog] Save Sftp Account Password',
  props<{ sftpAccount: SftpAccountViewModel; password: string }>()
);

export const saveSftpAccountPasswordSuccess = createAction(
  '[Sftp Account Edit Dialog] Save Sftp Account Success',
  props<{ sftpAccount: SftpAccountViewModel }>()
);

export const saveSftpAccountPasswordError = createAction(
  '[Sftp Account Edit Dialog] Save Sftp Account Error',
  props<{ error: ServerError }>()
);

export const sftpAccountEditDialogUpload = createAction('[Sftp Account Edit Dialog] Upload');

export const sftpAccountEditDialogUploadSuccess = createAction('[Sftp Account Edit Dialog] Upload Success');

export const sftpAccountEditDialogUploadFailure = createAction('[Sftp Account Edit Dialog] Upload Failure');
