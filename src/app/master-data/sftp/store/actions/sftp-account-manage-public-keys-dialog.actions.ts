import { createAction, props } from '@ngrx/store';

import { SftpAccountPublicKey, SftpAccountType, SftpAccountViewModel } from '~app/master-data/sftp/models';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';

export const sftpAccountManagePubicKeysDialogDestroy = createAction('[Sftp Account Manage Public Keys Dialog] Destroy');

export const selectPublicKey = createAction(
  '[Sftp Account Manage Public Keys Dialog] Select Public Key',
  props<{ publicKey: SftpAccountPublicKey }>()
);

export const filesToUploadChanged = createAction(
  '[Sftp Account Manage Public Keys Dialog] Files To Upload Changed',
  props<{ hasFilesToUpload: boolean }>()
);

export const publicKeysUploadStart = createAction('[Sftp Account Manage Public Keys Dialog] Public Keys Upload Start');

export const publicKeysUploadEnd = createAction(
  '[Sftp Account Manage Public Keys Dialog] Public Keys Upload End',
  props<{ sftpAccountType: SftpAccountType; uploadedFiles: FileUpload[] }>()
);

export const loadSftpAccountComplete = createAction(
  '[Sftp Account Manage Public Keys Dialog] Load Sftp Account Complete',
  props<{ sftpAccount: SftpAccountViewModel; uploadedFiles: FileUpload[] }>()
);

export const deselectPublicKey = createAction(
  '[Sftp Account Manage Public Keys Dialog] Deselect Public Key',
  props<{ publicKey: SftpAccountPublicKey }>()
);

export const deletePublicKeys = createAction(
  '[Sftp Account Manage Public Keys Dialog] Delete Public Keys',
  props<{ sftpAccount: SftpAccountViewModel; publicKeys: SftpAccountPublicKey[] }>()
);
