import * as fromSftpDeleteKeyConfirmationReducer from './sftp-account-delete-public-keys-confirmation.reducer';
import { SftpAccountDeletePublicKeysConfirmationDialogActions } from '~app/master-data/sftp/store/actions';

describe('sftp account delete public keys confirmation reducer', () => {
  it('on requestDeleteSftpAccountPublicKeys loading is set to true', () => {
    const action = SftpAccountDeletePublicKeysConfirmationDialogActions.requestDeleteSftpAccountPublicKeys;

    const newState = fromSftpDeleteKeyConfirmationReducer.reducer(
      {
        ...fromSftpDeleteKeyConfirmationReducer.initialState,
        loading: false
      },
      action
    );

    expect(newState.loading).toBeTruthy(true);
  });

  it('on sftpAccountDeletePubicKeysConfirmationDialogDestroy shuld reset state', () => {
    const action =
      SftpAccountDeletePublicKeysConfirmationDialogActions.sftpAccountDeletePubicKeysConfirmationDialogDestroy;

    const newState = fromSftpDeleteKeyConfirmationReducer.reducer(
      {
        ...fromSftpDeleteKeyConfirmationReducer.initialState,
        loading: true
      },
      action
    );

    expect(newState).toEqual(fromSftpDeleteKeyConfirmationReducer.initialState);
  });
});
