import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromSftpAccountDeletePublicKeysConfirmation from '~app/master-data/sftp/store/reducers/sftp-account-delete-public-keys-confirmation.reducer';
import * as fromSftpAccountEditState from '~app/master-data/sftp/store/reducers/sftp-account-edit.reducer';
import * as fromSftpAccountManagePublicKeys from '~app/master-data/sftp/store/reducers/sftp-account-manage-public-keys.reducer';
import * as fromSftpAccountsState from '~app/master-data/sftp/store/reducers/sftp-accounts.reducer';
import { AppState } from '~app/reducers';

export const sftpFeatureKey = 'sftp';

export interface SftpState {
  [fromSftpAccountsState.sftpAccountsFeatureKey]: fromSftpAccountsState.State;
  [fromSftpAccountEditState.sftpAccountEditFeatureKey]: fromSftpAccountEditState.State;
  [fromSftpAccountManagePublicKeys.sftpAccountManagePublicKeysFeatureKey]: fromSftpAccountManagePublicKeys.State;
  [fromSftpAccountDeletePublicKeysConfirmation.sftpAccountDeletePublicKeysConfirmationFeatureKey]: fromSftpAccountDeletePublicKeysConfirmation.State;
}

export interface State extends AppState {
  [sftpFeatureKey]: SftpState;
}

export function reducers(state: SftpState | undefined, action: Action) {
  return combineReducers({
    [fromSftpAccountsState.sftpAccountsFeatureKey]: fromSftpAccountsState.reducer,
    [fromSftpAccountEditState.sftpAccountEditFeatureKey]: fromSftpAccountEditState.reducer,
    [fromSftpAccountManagePublicKeys.sftpAccountManagePublicKeysFeatureKey]: fromSftpAccountManagePublicKeys.reducer,
    [fromSftpAccountDeletePublicKeysConfirmation.sftpAccountDeletePublicKeysConfirmationFeatureKey]:
      fromSftpAccountDeletePublicKeysConfirmation.reducer
  })(state, action);
}

export const getSftpState = createFeatureSelector<State, SftpState>(sftpFeatureKey);

export const getSftpAccountsState = createSelector(
  getSftpState,
  state => state[fromSftpAccountsState.sftpAccountsFeatureKey]
);

export const getSftpAccountsLoading = createSelector(getSftpAccountsState, fromSftpAccountsState.getLoading);

export const getSftpAccountsData = createSelector(getSftpAccountsState, fromSftpAccountsState.getData);

export const getSftpAccountPublicKeys = createSelector(
  getSftpAccountsState,
  fromSftpAccountsState.getSftpAccountPublicKeys
);

export const getSftpAccountEditState = createSelector(
  getSftpState,
  state => state[fromSftpAccountEditState.sftpAccountEditFeatureKey]
);

export const getSftpAccountEditLoading = createSelector(getSftpAccountEditState, fromSftpAccountEditState.getLoading);

export const getSftpAccountEditIsSaveButtonDisabled = createSelector(
  getSftpAccountEditState,
  fromSftpAccountEditState.getIsSaveButtonDisabled
);

export const getSftpAccountManagePublicKeysState = createSelector(
  getSftpState,
  state => state[fromSftpAccountManagePublicKeys.sftpAccountManagePublicKeysFeatureKey]
);

export const getSftpAccountSelectedPublicKeys = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getSelectedPublicKeys
);

export const getHasSelectedPublicKeys = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getHasSelectedPublicKeys
);

export const getHasDeletedPublicKeys = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getHasDeletedPublicKeys
);

export const getDeletedPublicKeysErrorMessage = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getDeletedPublicKeysErrorMessage
);

export const getHasFilesToUpload = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getHasFilesToUpload
);

export const getAllFilesAreCompleted = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getAllFilesAreCompleted
);

export const getFailedFilesNames = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getFailedFilesNames
);

export const getIsUploadInProgress = createSelector(
  getSftpAccountManagePublicKeysState,
  fromSftpAccountManagePublicKeys.getIsUploadInProgress
);

export const getDeletedPublicKeysConfirmationState = createSelector(
  getSftpState,
  state => state[fromSftpAccountDeletePublicKeysConfirmation.sftpAccountDeletePublicKeysConfirmationFeatureKey]
);

export const getDeletePublicKeysConfirmationLoading = createSelector(
  getDeletedPublicKeysConfirmationState,
  fromSftpAccountDeletePublicKeysConfirmation.getLoading
);
