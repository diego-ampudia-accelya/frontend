import * as fromSftpAccountEditState from './sftp-account-edit.reducer';
import { SftpAccountEditDialogActions } from '~app/master-data/sftp/store/actions';
import { getTestSftpAccount } from '~app/master-data/sftp/test/sftp.test.mocks';
import { ServerError } from '~app/shared/errors';

const serverError = new ServerError({
  timestamp: null,
  errorCode: 500,
  errorMajorCode: '001',
  errorMessage: 'Something went wrong.',
  messages: [
    {
      message: '',
      messageCode: '301',
      messageParams: [],
      textParams: []
    }
  ]
});

describe('sftp account edit reducer', () => {
  it('on sftpAccountEditDialogFormValidStateChange isSaveButtonDisabled to false, if isValid is true', () => {
    const action = SftpAccountEditDialogActions.sftpAccountEditDialogFormValidStateChange({ isValid: true });

    const newState = fromSftpAccountEditState.reducer(
      {
        ...fromSftpAccountEditState.initialState,
        isSaveButtonDisabled: true
      },
      action
    );

    expect(newState.isSaveButtonDisabled).toBe(false);
  });

  it('on sftpAccountEditDialogFormValidStateChange isSaveButtonDisabled to true, if isValid is false', () => {
    const action = SftpAccountEditDialogActions.sftpAccountEditDialogFormValidStateChange({ isValid: false });

    const newState = fromSftpAccountEditState.reducer(
      {
        ...fromSftpAccountEditState.initialState,
        isSaveButtonDisabled: false
      },
      action
    );

    expect(newState.isSaveButtonDisabled).toBe(true);
  });

  [
    SftpAccountEditDialogActions.saveSftpAccountPassword({ sftpAccount: getTestSftpAccount(), password: 'a' }),
    SftpAccountEditDialogActions.sftpAccountEditDialogUpload()
  ].forEach(action => {
    it(`on ${action.type}, login should be true and isSaveButtonDisabled false`, () => {
      const newState = fromSftpAccountEditState.reducer(
        {
          ...fromSftpAccountEditState.initialState,
          loading: false,
          isSaveButtonDisabled: true
        },
        action
      );

      expect(newState.loading).toBe(true);
      expect(newState.isSaveButtonDisabled).toBe(false);
    });
  });

  [
    SftpAccountEditDialogActions.saveSftpAccountPasswordSuccess({ sftpAccount: getTestSftpAccount() }),
    SftpAccountEditDialogActions.saveSftpAccountPasswordError({ error: serverError }),
    SftpAccountEditDialogActions.sftpAccountEditDialogUploadSuccess(),
    SftpAccountEditDialogActions.sftpAccountEditDialogUploadFailure()
  ].forEach(action => {
    it(`on ${action.type}, login should be false and isSaveButtonDisabled true`, () => {
      const newState = fromSftpAccountEditState.reducer(
        {
          ...fromSftpAccountEditState.initialState,

          loading: true,
          isSaveButtonDisabled: false
        },
        action
      );

      expect(newState.loading).toBe(false);
      expect(newState.isSaveButtonDisabled).toBe(true);
    });
  });

  it('on sftpAccountEditDialogDestroy should reset state to initialState', () => {
    const action = SftpAccountEditDialogActions.sftpAccountEditDialogDestroy();

    const newState = fromSftpAccountEditState.reducer(
      {
        loading: null,
        isSaveButtonDisabled: null
      },
      action
    );

    expect(newState).toEqual(fromSftpAccountEditState.initialState);
  });
});
