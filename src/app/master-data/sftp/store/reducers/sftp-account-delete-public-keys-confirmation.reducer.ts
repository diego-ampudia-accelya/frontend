import { createReducer, on } from '@ngrx/store';

import { SftpAccountDeletePublicKeysConfirmationDialogActions } from '~app/master-data/sftp/store/actions';

export const sftpAccountDeletePublicKeysConfirmationFeatureKey = 'sftpAccountDeletePubicKeysConfirmation';

export interface State {
  loading: boolean;
}

export const initialState: State = {
  loading: false
};

export const reducer = createReducer(
  initialState,
  on(SftpAccountDeletePublicKeysConfirmationDialogActions.requestDeleteSftpAccountPublicKeys, state => ({
    ...state,
    loading: true
  })),
  on(SftpAccountDeletePublicKeysConfirmationDialogActions.sftpAccountDeletePubicKeysConfirmationDialogDestroy, () => ({
    ...initialState
  }))
);

export const getLoading = (state: State): boolean => state.loading;
