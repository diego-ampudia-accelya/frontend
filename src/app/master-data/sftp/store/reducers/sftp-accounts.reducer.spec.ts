import * as fromSftpAccountsState from './sftp-accounts.reducer';
import { getSftpAccountPublicKeys } from './sftp-accounts.reducer';
import { SftpAccountType, SftpAccountViewModel } from '~app/master-data/sftp/models';
import {
  SftpAccountDeletePublicKeysConfirmationDialogActions,
  SftpAccountManagePublicKeysDialogActions,
  SftpAccountsActions
} from '~app/master-data/sftp/store/actions';
import {
  getTestSftpAccount,
  getTestSftpAccountPublicKeys,
  getTestSftpAccountWithPublicKeys
} from '~app/master-data/sftp/test/sftp.test.mocks';

describe('sftp accounts reducer', () => {
  it('on loadSftpAccounts should clear date and set loading to true', () => {
    const action = SftpAccountsActions.loadSftpAccounts;

    const newState = fromSftpAccountsState.reducer(
      {
        ...fromSftpAccountsState.initialState,
        data: [{}]
      },
      action
    );

    expect(newState.data).toEqual([]);
    expect(newState.loading).toBeTruthy(true);
  });

  it('on loadSftpAccountsComplete should set date and set loading to false', () => {
    const testData: SftpAccountViewModel[] = [getTestSftpAccount()];

    const action = SftpAccountsActions.loadSftpAccountsComplete({
      data: testData
    });

    const newState = fromSftpAccountsState.reducer(
      {
        ...fromSftpAccountsState.initialState
      },
      action
    );

    expect(newState.data).toEqual(testData);
    expect(newState.loading).toBeFalsy();
  });

  it('on loadSftpAccountComplete should set the correct sftpAccount and set loading to false', () => {
    const action = SftpAccountManagePublicKeysDialogActions.loadSftpAccountComplete({
      sftpAccount: getTestSftpAccount(),
      uploadedFiles: []
    });

    const newState = fromSftpAccountsState.reducer(
      {
        ...fromSftpAccountsState.initialState,
        data: [getTestSftpAccountWithPublicKeys(), getTestSftpAccountWithPublicKeys(SftpAccountType.Upload)],
        loading: true
      },
      action
    );

    expect(newState.data[0].publicKeys).toEqual([]);
    expect(newState.data[1].publicKeys).toEqual(getTestSftpAccountPublicKeys());
  });

  it('on deleteAccountPublicKeysSuccess should set the correct sftpAccount and set loading to false', () => {
    const action = SftpAccountDeletePublicKeysConfirmationDialogActions.deleteAccountPublicKeysSuccess({
      sftpAccount: getTestSftpAccount(),
      publicKeys: [],
      errorMessage: ''
    });

    const newState = fromSftpAccountsState.reducer(
      {
        ...fromSftpAccountsState.initialState,
        data: [getTestSftpAccountWithPublicKeys(), getTestSftpAccountWithPublicKeys(SftpAccountType.Upload)],
        loading: true
      },
      action
    );

    expect(newState.data[0].publicKeys).toEqual([]);
    expect(newState.data[1].publicKeys).toEqual(getTestSftpAccountPublicKeys());
  });
});

describe('selectors', () => {
  it('getSftpAccountPublicKeys should return the public keys of an account', () => {
    const props = { sftpAccount: getTestSftpAccount() };
    const state = {
      data: [getTestSftpAccountWithPublicKeys()],
      loading: false
    };

    const result = getSftpAccountPublicKeys(state, props);

    expect(result).toEqual(getTestSftpAccountPublicKeys());
  });
});
