import { createReducer, on } from '@ngrx/store';

import { SftpAccountPublicKey, SftpAccountViewModel } from '~app/master-data/sftp/models';
import {
  SftpAccountDeletePublicKeysConfirmationDialogActions,
  SftpAccountManagePublicKeysDialogActions,
  SftpAccountsActions
} from '~app/master-data/sftp/store/actions';

export const sftpAccountsFeatureKey = 'sftpAccounts';

export interface State {
  data: SftpAccountViewModel[];
  loading: boolean;
}

export const initialState: State = {
  data: [],
  loading: false
};

export const reducer = createReducer(
  initialState,
  on(SftpAccountsActions.loadSftpAccounts, state => ({
    ...state,
    data: [],
    loading: true
  })),
  on(SftpAccountsActions.loadSftpAccountsComplete, (state, { data }) => ({
    ...state,
    data,
    loading: false
  })),
  on(
    SftpAccountManagePublicKeysDialogActions.loadSftpAccountComplete,
    SftpAccountDeletePublicKeysConfirmationDialogActions.deleteAccountPublicKeysSuccess,
    (state, { sftpAccount }) => ({
      ...state,
      data: state.data.map(account =>
        account.sftpAccountType === sftpAccount.sftpAccountType ? sftpAccount : account
      ),
      loading: false
    })
  )
);

export const getData = (state: State): SftpAccountViewModel[] => state.data;

export const getSftpAccountPublicKeys = (
  state: State,
  props: { sftpAccount: SftpAccountViewModel }
): SftpAccountPublicKey[] =>
  state.data.find(sftpAccount => sftpAccount.sftpAccountType === props.sftpAccount.sftpAccountType)?.publicKeys || [];

export const getLoading = (state: State): boolean => state.loading;
