import * as fromSftpAccountManagePublicKeys from './sftp-account-manage-public-keys.reducer';
import {
  SftpAccountDeletePublicKeysConfirmationDialogActions as DeleteKeysConfirmDialogActions,
  SftpAccountManagePublicKeysDialogActions
} from '~app/master-data/sftp/store/actions';
import {
  getTestSftpAccount,
  getTestSftpAccountPublicKeys,
  getTestUploadFile
} from '~app/master-data/sftp/test/sftp.test.mocks';
import { UploadStatus } from '~app/shared/components/upload/upload-mode.enum';

describe('sftp account delete public keys confirmation reducer', () => {
  let initialState: fromSftpAccountManagePublicKeys.State;

  beforeEach(() => {
    initialState = fromSftpAccountManagePublicKeys.initialState;
  });

  it('on selectPublicKey should add a key to selectedPublicKeys', () => {
    const publicKey = { login: 'A6081', filename: 'test1.pub' };
    const action = SftpAccountManagePublicKeysDialogActions.selectPublicKey({ publicKey });
    const newState = fromSftpAccountManagePublicKeys.reducer(initialState, action);

    expect(newState.selectedPublicKeys).toEqual([publicKey]);
  });

  it('on deselectPublicKey should remove a key form selectedPublicKeys', () => {
    const publicKey = { login: 'A6081', filename: 'test1.pub' };
    const state = { ...initialState, selectedPublicKeys: [publicKey] };
    const action = SftpAccountManagePublicKeysDialogActions.deselectPublicKey({ publicKey });
    const newState = fromSftpAccountManagePublicKeys.reducer(state, action);

    expect(newState.selectedPublicKeys).toEqual([]);
  });

  it('on filesToUploadChanged should set hasFilesToUpload', () => {
    const action = SftpAccountManagePublicKeysDialogActions.filesToUploadChanged({ hasFilesToUpload: true });
    const newState = fromSftpAccountManagePublicKeys.reducer(fromSftpAccountManagePublicKeys.initialState, action);

    expect(newState.hasFilesToUpload).toBe(true);
  });

  it('on publicKeysUploadStart should set isUploadInProgress to true', () => {
    const action = SftpAccountManagePublicKeysDialogActions.publicKeysUploadStart();
    const newState = fromSftpAccountManagePublicKeys.reducer(fromSftpAccountManagePublicKeys.initialState, action);

    expect(newState.isUploadInProgress).toBe(true);
  });

  it('on loadSftpAccountComplete should set isUploadInProgress to false', () => {
    const action = SftpAccountManagePublicKeysDialogActions.loadSftpAccountComplete({
      sftpAccount: getTestSftpAccount(),
      uploadedFiles: [null]
    });
    const newState = fromSftpAccountManagePublicKeys.reducer(fromSftpAccountManagePublicKeys.initialState, action);

    expect(newState.isUploadInProgress).toBe(false);
    expect(newState.uploadedFiles).toEqual([null]);
  });

  it('on deleteAccountPublicKeysSuccess should update deletedPublicKeys', () => {
    const action = DeleteKeysConfirmDialogActions.deleteAccountPublicKeysSuccess({
      sftpAccount: getTestSftpAccount(),
      publicKeys: [null],
      errorMessage: ''
    });
    const newState = fromSftpAccountManagePublicKeys.reducer(fromSftpAccountManagePublicKeys.initialState, action);

    expect(newState.deletedPublicKeysErrorMessage).toBe('');
    expect(newState.deletedPublicKeys).toEqual([null]);
  });

  it('on deleteAccountPublicKeysSuccess should clears deletedPublicKeys, if has error message', () => {
    const action = DeleteKeysConfirmDialogActions.deleteAccountPublicKeysSuccess({
      sftpAccount: getTestSftpAccount(),
      publicKeys: [null],
      errorMessage: 'test error message'
    });
    const newState = fromSftpAccountManagePublicKeys.reducer(fromSftpAccountManagePublicKeys.initialState, action);

    expect(newState.deletedPublicKeysErrorMessage).toBe('test error message');
    expect(newState.deletedPublicKeys).toEqual([]);
  });

  it('on sftpAccountManagePubicKeysDialogDestroy restore initial state', () => {
    const action = SftpAccountManagePublicKeysDialogActions.sftpAccountManagePubicKeysDialogDestroy();
    const newState = fromSftpAccountManagePublicKeys.reducer(
      {
        selectedPublicKeys: [null],
        deletedPublicKeys: [null],
        deletedPublicKeysErrorMessage: 'test error message',
        uploadedFiles: [null],
        hasFilesToUpload: true,
        isUploadInProgress: true
      },
      action
    );

    expect(newState).toEqual(fromSftpAccountManagePublicKeys.initialState);
  });

  it('getSelectedPublicKeys should get the selectedPublicKeys', () => {
    const testPublicKeys = getTestSftpAccountPublicKeys();
    const selectedPublicKeys = fromSftpAccountManagePublicKeys.getSelectedPublicKeys({
      ...fromSftpAccountManagePublicKeys.initialState,
      selectedPublicKeys: testPublicKeys
    });

    expect(selectedPublicKeys).toEqual(testPublicKeys);
  });

  it('getAllFilesAreCompleted should return false if no uploadedFiles', () => {
    const result = fromSftpAccountManagePublicKeys.getAllFilesAreCompleted({
      ...fromSftpAccountManagePublicKeys.initialState,
      uploadedFiles: []
    });

    expect(result).toBeFalsy();
  });

  it('getAllFilesAreCompleted should return false if on file is failed', () => {
    const result = fromSftpAccountManagePublicKeys.getAllFilesAreCompleted({
      ...fromSftpAccountManagePublicKeys.initialState,
      uploadedFiles: [
        getTestUploadFile('test1.pub', UploadStatus.completed),
        getTestUploadFile('test2.pub', UploadStatus.failed)
      ]
    });

    expect(result).toBeFalsy();
  });

  it('getAllFilesAreCompleted should return true if all files are completed', () => {
    const result = fromSftpAccountManagePublicKeys.getAllFilesAreCompleted({
      ...fromSftpAccountManagePublicKeys.initialState,
      uploadedFiles: [
        getTestUploadFile('test1.pub', UploadStatus.completed),
        getTestUploadFile('test2.pub', UploadStatus.completed)
      ]
    });

    expect(result).toBeTruthy();
  });

  it('getFailedFilesNames should get commas separated list with failed files names', () => {
    const fileNames = fromSftpAccountManagePublicKeys.getFailedFilesNames({
      ...fromSftpAccountManagePublicKeys.initialState,
      uploadedFiles: [
        getTestUploadFile('test1.pub', UploadStatus.completed),
        getTestUploadFile('test2.pub', UploadStatus.failed),
        getTestUploadFile('test3.pub', UploadStatus.failed)
      ]
    });

    expect(fileNames).toBe('test2.pub, test3.pub');
  });
});
