import { createReducer, on } from '@ngrx/store';

import { SftpAccountEditDialogActions } from '~app/master-data/sftp/store/actions';

export const sftpAccountEditFeatureKey = 'sftpAccountEdit';

export interface State {
  loading: boolean;
  isSaveButtonDisabled: boolean;
}

export const initialState: State = {
  loading: false,
  isSaveButtonDisabled: true
};

export const reducer = createReducer(
  initialState,
  on(
    SftpAccountEditDialogActions.saveSftpAccountPassword,
    SftpAccountEditDialogActions.sftpAccountEditDialogUpload,
    state => ({
      ...state,
      loading: true,
      isSaveButtonDisabled: false
    })
  ),
  on(SftpAccountEditDialogActions.sftpAccountEditDialogFormValidStateChange, (state, { isValid }) => ({
    ...state,
    isSaveButtonDisabled: !isValid
  })),
  on(
    SftpAccountEditDialogActions.saveSftpAccountPasswordSuccess,
    SftpAccountEditDialogActions.saveSftpAccountPasswordError,
    SftpAccountEditDialogActions.sftpAccountEditDialogUploadSuccess,
    SftpAccountEditDialogActions.sftpAccountEditDialogUploadFailure,
    state => ({
      ...state,
      loading: false,
      isSaveButtonDisabled: true
    })
  ),
  on(SftpAccountEditDialogActions.sftpAccountEditDialogDestroy, () => initialState)
);

export const getLoading = (state: State): boolean => state.loading;

export const getIsSaveButtonDisabled = (state: State): boolean => state.isSaveButtonDisabled;
