import { createReducer, on } from '@ngrx/store';

import { SftpAccountPublicKey } from '~app/master-data/sftp/models';
import {
  SftpAccountDeletePublicKeysConfirmationDialogActions as DeleteKeysConfirmDialogActions,
  SftpAccountManagePublicKeysDialogActions
} from '~app/master-data/sftp/store/actions';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { UploadStatus } from '~app/shared/components/upload/upload-mode.enum';

export const sftpAccountManagePublicKeysFeatureKey = 'sftpAccountManagePubicKeys';

export interface State {
  selectedPublicKeys: SftpAccountPublicKey[];
  deletedPublicKeys: SftpAccountPublicKey[];
  deletedPublicKeysErrorMessage: string;
  uploadedFiles: FileUpload[];
  hasFilesToUpload: boolean;
  isUploadInProgress: boolean;
}

export const initialState: State = {
  selectedPublicKeys: [],
  deletedPublicKeys: [],
  deletedPublicKeysErrorMessage: '',
  uploadedFiles: [],
  hasFilesToUpload: false,
  isUploadInProgress: false
};

export const reducer = createReducer(
  initialState,
  on(SftpAccountManagePublicKeysDialogActions.selectPublicKey, (state, { publicKey }) => ({
    ...state,
    deletedPublicKeys: [],
    deletedPublicKeysErrorMessage: '',
    uploadedFiles: [],
    selectedPublicKeys: [...state.selectedPublicKeys, publicKey]
  })),
  on(SftpAccountManagePublicKeysDialogActions.deselectPublicKey, (state, { publicKey }) => ({
    ...state,
    selectedPublicKeys: state.selectedPublicKeys.filter(key => key.filename !== publicKey.filename)
  })),
  on(SftpAccountManagePublicKeysDialogActions.filesToUploadChanged, (state, { hasFilesToUpload }) => ({
    ...state,
    deletedPublicKeys: [],
    deletedPublicKeysErrorMessage: '',
    uploadedFiles: [],
    hasFilesToUpload
  })),
  on(SftpAccountManagePublicKeysDialogActions.publicKeysUploadStart, state => ({
    ...state,
    isUploadInProgress: true
  })),
  on(SftpAccountManagePublicKeysDialogActions.loadSftpAccountComplete, (state, { uploadedFiles }) => ({
    ...state,
    uploadedFiles,
    isUploadInProgress: false
  })),
  on(
    DeleteKeysConfirmDialogActions.deleteAccountPublicKeysSuccess,
    (state, { publicKeys: deletedPublicKeys, errorMessage: deletedPublicKeysErrorMessage }) => ({
      ...state,
      deletedPublicKeys: deletedPublicKeysErrorMessage ? [] : deletedPublicKeys,
      deletedPublicKeysErrorMessage
    })
  ),
  on(SftpAccountManagePublicKeysDialogActions.sftpAccountManagePubicKeysDialogDestroy, () => ({ ...initialState }))
);

export const getSelectedPublicKeys = (state: State): SftpAccountPublicKey[] => state.selectedPublicKeys;

export const getHasSelectedPublicKeys = (state: State): boolean => state.selectedPublicKeys.length > 0;

export const getHasDeletedPublicKeys = (state: State): boolean => state.deletedPublicKeys.length > 0;

export const getDeletedPublicKeysErrorMessage = (state: State): string => state.deletedPublicKeysErrorMessage;

export const getHasFilesToUpload = (state: State): boolean => state.hasFilesToUpload || state.isUploadInProgress;

export const getIsUploadInProgress = (state: State): boolean => state.isUploadInProgress;

export const getAllFilesAreCompleted = (state: State): boolean => {
  const uploadedFiles = state.uploadedFiles;

  return uploadedFiles.length > 0 && uploadedFiles.every(file => file.status === UploadStatus.completed);
};

export const getFailedFilesNames = (state: State): string =>
  state.uploadedFiles
    .filter(file => file.status === UploadStatus.failed)
    .map(file => file.name)
    .join(', ');
