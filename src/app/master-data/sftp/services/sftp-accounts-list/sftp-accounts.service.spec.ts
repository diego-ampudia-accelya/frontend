import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { SftpAccountsService } from './sftp-accounts.service';
import { SftpAccountList } from '~app/master-data/sftp/models';
import { DataQuery } from '~app/shared/components/list-view';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { AppConfigurationService } from '~app/shared/services';

describe('SftpAccountsService', () => {
  let service: SftpAccountsService;
  let httpClientSpy: SpyObject<HttpClient>;

  const sftpAccountsMock: SftpAccountList[] = [
    {
      login: 'test1',
      creationDate: []
    },
    {
      login: 'test2',
      creationDate: []
    }
  ];

  const query: DataQuery = {
    filterBy: {},
    paginateBy: { size: 20, page: 0 },
    sortBy: []
  };

  beforeEach(() => {
    const httpResponse = new HttpResponse({ headers: new HttpHeaders() });
    httpClientSpy = createSpyObject(HttpClient);
    httpClientSpy.get.and.returnValue(of(httpResponse));

    TestBed.configureTestingModule({
      providers: [SftpAccountsService, AppConfigurationService, { provide: HttpClient, useValue: httpClientSpy }],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(SftpAccountsService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set correct HTTP request base url', () => {
    expect(service['baseUrl']).toBe('/file-management/sftp-accounts');
  });

  it('should call HTTP method with proper url and return correct data list', fakeAsync(() => {
    let data: PagedData<SftpAccountList>;

    const dataMock: PagedData<SftpAccountList> = {
      records: sftpAccountsMock,
      total: sftpAccountsMock.length,
      totalPages: 1,
      pageNumber: 0,
      pageSize: 20
    };

    httpClientSpy.get.and.returnValue(of(dataMock));
    service.find(query).subscribe(res => {
      data = res;
    });
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/file-management/sftp-accounts?page=0&size=20');
    expect(data).toEqual(dataMock);
  }));

  it('should send proper request when download() is called', fakeAsync(() => {
    const exportOptions: DownloadFormat = DownloadFormat.TXT;
    const expectedUrl = '/file-management/sftp-accounts/download?exportAs=txt';

    service.download(query, exportOptions).subscribe();
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(expectedUrl, {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    });
  }));
});
