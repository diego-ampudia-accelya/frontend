import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SftpAccountFilters } from '~app/master-data/sftp/models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type DisplayFilterConfig<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable({ providedIn: 'root' })
export class SftpAccountsFilterFormatter implements FilterFormatter {
  constructor(private translation: L10nTranslationService) {}

  public format(filter: SftpAccountFilters): AppliedFilter[] {
    const filterMappers: Partial<DisplayFilterConfig<SftpAccountFilters>> = {
      login: login => `${this.translate('login')} - ${login}`,
      creationDate: creationDate => `${this.translate('creationDate')} - ${rangeDateFilterTagMapper(creationDate)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value != null && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translation.translate(`SFTP_ACCOUNTS.filters.${key}`);
  }
}
