import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SftpAccount, SftpAccountFilters, SftpAccountFiltersBE } from '~app/master-data/sftp/models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class SftpAccountsService implements Queryable<SftpAccount> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/sftp-accounts`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<SftpAccountFilters>): Observable<PagedData<SftpAccount>> {
    const formattedQuery = this.formatQuery(query);

    return this.http.get<PagedData<SftpAccount>>(this.baseUrl + formattedQuery.getQueryString());
  }

  private formatQuery(query: Partial<DataQuery<SftpAccountFilters>>): RequestQuery<SftpAccountFiltersBE> {
    const { login, creationDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        login,
        fromCreationDate: creationDate && toShortIsoDate(creationDate[0]),
        toCreationDate: creationDate && toShortIsoDate(creationDate[1] ? creationDate[1] : creationDate[0])
      }
    });
  }

  public download(
    query: DataQuery<SftpAccountFilters>,
    exportOptions: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);
    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: exportOptions };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString({ omitPaging: true });

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }
}
