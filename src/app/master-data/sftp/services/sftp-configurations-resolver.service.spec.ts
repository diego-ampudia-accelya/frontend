import { fakeAsync, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';

import { SftpConfigurationsResolver } from './sftp-configurations-resolver.service';
import { SftpAccountsActions } from '~app/master-data/sftp/store/actions';
import { getInitialState } from '~app/master-data/sftp/test/sftp.test.mocks';

describe('SftpConfigurationsResolverService', () => {
  let service: SftpConfigurationsResolver;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState: getInitialState() })]
    });
    service = TestBed.inject(SftpConfigurationsResolver);
    store = TestBed.inject<any>(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('resolve should return null', () => {
    const expected = cold('(a|)', { a: null });

    expect(service.resolve(null)).toBeObservable(expected);
  });

  it('resolve should call store dispatch', fakeAsync(() => {
    service.resolve(null).subscribe(() => {
      expect(store.dispatch).toHaveBeenCalledWith(SftpAccountsActions.loadSftpAccounts());
    });
  }));
});
