import { SftpAccountActionType, SftpAccountState, SftpAccountType } from '~app/master-data/sftp/models';
import { FooterButton } from '~app/shared/components';
import { Permissions } from '~app/shared/constants/permissions';

export const sftpAccountActionTypeFooterButtonMap = new Map([
  [SftpAccountActionType.Create, FooterButton.Create],
  [SftpAccountActionType.Create, FooterButton.Create],
  [SftpAccountActionType.Modify, FooterButton.Modify],
  [SftpAccountActionType.Reactivate, FooterButton.Reactivate]
]);

export const sftpAccountStateActionTypesMap = new Map([
  [SftpAccountState.Active, SftpAccountActionType.Modify],
  [SftpAccountState.Pending, SftpAccountActionType.Modify],
  [SftpAccountState.Expired, SftpAccountActionType.Reactivate],
  [SftpAccountState.NonExisting, SftpAccountActionType.Create]
]);

export const sftpAccountTypeUrlMap = new Map([
  [SftpAccountType.Download, 'download'],
  [SftpAccountType.Upload, 'upload'],
  [SftpAccountType.AdditionalUpload, 'additional-upload'],
  [SftpAccountType.MassDownload, 'mass-download'],
  [SftpAccountType.Global, 'global']
]);
export const sftpAccountTypeSaveModelMap = new Map([
  [SftpAccountType.Download, 'Download'],
  [SftpAccountType.Upload, 'Upload'],
  [SftpAccountType.AdditionalUpload, 'Additional Upload'],
  [SftpAccountType.MassDownload, 'Mass Download'],
  [SftpAccountType.Global, 'Global']
]);

export const sftpAccountTypeUpdatePermissionsMap = new Map([
  [SftpAccountType.Download, Permissions.updateSftpAccount],
  [SftpAccountType.Upload, Permissions.updateSftpAccount],
  [SftpAccountType.AdditionalUpload, Permissions.updateAdditionalUploadSftpAccount],
  [SftpAccountType.MassDownload, Permissions.updateMassDownloadSftpAccount],
  [SftpAccountType.Global, Permissions.updateGlobalSftpAccount]
]);

export const sftpAccountTypeReadPermissionsMap = new Map([
  [SftpAccountType.Download, Permissions.readSftpAccount],
  [SftpAccountType.Upload, Permissions.readSftpAccount],
  [SftpAccountType.AdditionalUpload, Permissions.readAdditionalUploadSftpAccount],
  [SftpAccountType.MassDownload, Permissions.readMassDownloadSftpAccount],
  [SftpAccountType.Global, Permissions.readGlobalSftpAccount]
]);
