import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { SftpAccountsActions } from '~app/master-data/sftp/store/actions';
import { AppState } from '~app/reducers';

@Injectable({ providedIn: 'root' })
export class SftpConfigurationsResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) {}

  resolve(_route: ActivatedRouteSnapshot): Observable<any> {
    this.store.dispatch(SftpAccountsActions.loadSftpAccounts());

    return of(null);
  }
}
