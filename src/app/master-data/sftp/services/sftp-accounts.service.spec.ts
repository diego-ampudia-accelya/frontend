import { HttpClient } from '@angular/common/http';
import { ErrorHandler } from '@angular/core';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of, throwError } from 'rxjs';

import { SftpAccountsService } from './sftp-accounts.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { getConfigurationAirline } from '~app/master-data/airline/reducers';
import { Airline } from '~app/master-data/models';
import { SftpAccountState, SftpAccountType, SftpAccountViewModel } from '~app/master-data/sftp/models';
import {
  getTestSftpAccount,
  getTestSftpAccountPublicKeys,
  sftpAccountBEMock
} from '~app/master-data/sftp/test/sftp.test.mocks';
import { getInitialState } from '~app/master-data/sftp/test/sftp.test.mocks';
import { AppState } from '~app/reducers';
import { AppConfigurationService } from '~app/shared/services';

describe('SftpAccountsService', () => {
  let service: SftpAccountsService;
  let httpClientSpy: SpyObject<HttpClient>;
  let globalErrorHandlerServiceSpy: SpyObject<ErrorHandler>;
  let permissionsServiceSpy: SpyObject<PermissionsService>;
  let mockStore: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        mockProvider(AppConfigurationService, { baseApiPath: 'api' }),
        mockProvider(HttpClient),
        mockProvider(L10nTranslationService, {
          translate: () => 'translation'
        }),
        mockProvider(ErrorHandler),
        mockProvider(PermissionsService),
        provideMockStore({ initialState: getInitialState() })
      ]
    });

    service = TestBed.inject(SftpAccountsService);
    mockStore = TestBed.inject<any>(Store);
    mockStore.overrideSelector(getConfigurationAirline, {} as Airline);

    httpClientSpy = TestBed.inject<any>(HttpClient);
    httpClientSpy.get.and.returnValue(of([sftpAccountBEMock]));
    httpClientSpy.post.and.returnValue(of(null));
    httpClientSpy.put.and.returnValue(of(null));
    httpClientSpy.delete.and.returnValue(of(null));

    globalErrorHandlerServiceSpy = TestBed.inject<any>(ErrorHandler);
    globalErrorHandlerServiceSpy.handleError.and.callThrough();

    permissionsServiceSpy = TestBed.inject<any>(PermissionsService);
    permissionsServiceSpy.hasPermission.and.returnValue(true);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getSftpAccounts should return normalized data', fakeAsync(() => {
    service.getSftpAccounts().subscribe(result => {
      expect(result.length).toBe(5);
      expect(result[0].sftpAccountType).toBe('download');
      expect(result[0].state).toBe('nonExisting');
      expect(result[0].publicKeys.length).toBe(0);
      expect(result[0].owner).toBe('yes');
    });
  }));

  it('getSftpAccounts should return empty array if no permissions', fakeAsync(() => {
    permissionsServiceSpy.hasPermission.and.returnValue(false);
    let actual: SftpAccountViewModel[];

    service.getSftpAccounts().subscribe(result => (actual = result));

    expect(actual.length).toBe(0);
  }));

  it('getSftpAccounts should call globalErrorHandlerService handleError and return empty array', fakeAsync(() => {
    globalErrorHandlerServiceSpy.handleError.and.callThrough();
    httpClientSpy.get.and.callFake((url: string) => {
      if (url.indexOf('mass-download') > -1) {
        return throwError(new Error('testError'));
      } else {
        return of([sftpAccountBEMock]);
      }
    });

    service.getSftpAccounts().subscribe(result => {
      expect(result.length).toBe(4);
      expect(globalErrorHandlerServiceSpy.handleError).toHaveBeenCalledTimes(1);
    });
  }));

  describe('getSftpAccountsUploadKeys', () => {
    it('getSftpAccounts should return normalized data with public keys, when state is Active', fakeAsync(() => {
      httpClientSpy.get.and.callFake((url: string) => {
        if (url.indexOf('public-keys') === -1) {
          return of([{ ...sftpAccountBEMock, state: 'Active' }]);
        } else {
          return of([
            { login: 'MassES016', filename: 'test1.pub' },
            { login: 'MassES016', filename: 'test2.pub' }
          ]);
        }
      });
      service.getSftpAccounts().subscribe(result => {
        expect(result.length).toBe(5);
        expect(result[0].publicKeys.length).toBe(2);
      });
    }));

    it('getSftpAccounts should return normalized data with empty public keys, when public keys request throws error', fakeAsync(() => {
      httpClientSpy.get.and.callFake((url: string) => {
        if (url.indexOf('public-keys') === -1) {
          return of([{ ...sftpAccountBEMock, state: 'Active' }]);
        } else {
          return throwError(new Error('testError'));
        }
      });
      service.getSftpAccounts().subscribe(result => {
        expect(result.length).toBe(5);
        expect(result[0].publicKeys).toEqual([]);
      });
    }));

    [SftpAccountState.NonExisting, SftpAccountState.Expired, SftpAccountState.Pending].forEach(state => {
      it(`getSftpAccounts should return normalized data with empty public keys, when state is ${state}`, fakeAsync(() => {
        httpClientSpy.get.and.callFake((url: string) => {
          if (url.indexOf('public-keys') === -1) {
            return of([{ ...sftpAccountBEMock, state }]);
          } else {
            return of([
              { login: 'MassES016', filename: 'test1.pub' },
              { login: 'MassES016', filename: 'test2.pub' }
            ]);
          }
        });
        service.getSftpAccounts().subscribe(result => {
          expect(result.length).toBe(5);
          expect(result[0].publicKeys).toEqual([]);
        });
      }));
    });
  });

  describe('save should', () => {
    it('call post if action type is Create', fakeAsync(() => {
      const sftpAccount = getTestSftpAccount(SftpAccountState.NonExisting);

      service.save(sftpAccount, 'password');
      tick();

      expect(httpClientSpy.request).toHaveBeenCalledWith('POST', 'api/sftp-management/sftp-accounts/download', {
        body: {
          sftpAccountType: 'Download',
          login: 'A6081',
          passwd: 'password'
        }
      });
    }));

    it('call put if action type is Modify', fakeAsync(() => {
      const sftpAccount = getTestSftpAccount(SftpAccountState.Active);

      service.save(sftpAccount, 'password');
      tick();

      expect(httpClientSpy.request).toHaveBeenCalledWith('PUT', 'api/sftp-management/sftp-accounts/download', {
        body: {
          sftpAccountType: 'Download',
          login: 'A6081',
          passwd: 'password'
        }
      });
    }));

    it('call put if action type is Reactivate', fakeAsync(() => {
      const sftpAccount = getTestSftpAccount(SftpAccountState.Expired);

      service.save(sftpAccount, 'password');
      tick();

      expect(httpClientSpy.request).toHaveBeenCalledWith('PUT', 'api/sftp-management/sftp-accounts/download', {
        body: {
          sftpAccountType: 'Download',
          login: 'A6081',
          passwd: 'password'
        }
      });
    }));
  });

  describe('deletePublicKey', () => {
    it('should call httpClient delete and return sftpAccount and deletedPublicKeys', fakeAsync(() => {
      service.deletePublicKeys(SftpAccountType.Download, getTestSftpAccountPublicKeys(), 100).subscribe(result => {
        expect(httpClientSpy.delete).toHaveBeenCalledWith(
          'api/sftp-management/sftp-accounts/public-keys/download?bspId=100&filename=test1.pub'
        );
        expect(httpClientSpy.delete).toHaveBeenCalledWith(
          'api/sftp-management/sftp-accounts/public-keys/download?bspId=100&filename=test2.pub'
        );
        expect(result.sftpAccount).toBeTruthy();
        expect(result.publicKeys.length).toBe(2);
        expect(result.errorMessage).toEqual('');
      });
    }));

    it('should return errorMessage when error', fakeAsync(() => {
      httpClientSpy.delete.and.callFake(url => {
        if (url.indexOf('test2') > -1) {
          return throwError(new Error('testError'));
        }

        return of(null);
      });

      service.deletePublicKeys(SftpAccountType.Download, getTestSftpAccountPublicKeys(), 100).subscribe(result => {
        expect(result.sftpAccount).toBeTruthy();
        expect(result.publicKeys.length).toBe(1);
        expect(result.errorMessage).toEqual('translation');
      });
    }));
  });
});
