import { HttpClient } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { camelCase, isEmpty } from 'lodash';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { getConfigurationAirline } from '~app/master-data/airline/reducers';
import {
  SftpAccount,
  SftpAccountOwner,
  SftpAccountPublicKey,
  SftpAccountState,
  SftpAccountType,
  SftpAccountViewModel
} from '~app/master-data/sftp/models';
import {
  sftpAccountStateActionTypesMap,
  sftpAccountTypeReadPermissionsMap,
  sftpAccountTypeSaveModelMap,
  sftpAccountTypeUpdatePermissionsMap,
  sftpAccountTypeUrlMap
} from '~app/master-data/sftp/services/sftp-account.map.constants';
import { AppState } from '~app/reducers';
import { AppConfigurationService } from '~app/shared/services';

const translationNs = 'menu.masterData.configuration.sftpConfigurations';

@Injectable({ providedIn: 'root' })
export class SftpAccountsService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/sftp-management/sftp-accounts/`;
  private airlineId: number;

  constructor(
    private httpClient: HttpClient,
    private appConfiguration: AppConfigurationService,
    private globalErrorHandlerService: ErrorHandler,
    private permissionsService: PermissionsService,
    private translationService: L10nTranslationService,
    private store: Store<AppState>
  ) {
    this.getAirlineConfiguration();
  }

  private getAirlineConfiguration() {
    this.store.pipe(select(getConfigurationAirline)).subscribe(airline => {
      this.airlineId = airline && !isEmpty(airline) ? airline.id : null;
    });
  }

  public getSftpAccounts(): Observable<SftpAccountViewModel[]> {
    const sftpAccountGets: Observable<SftpAccountViewModel>[] = Object.values(SftpAccountType)
      .filter((accountType: SftpAccountType) => this.canRead(accountType))
      .map((accountType: SftpAccountType) => this.getSftpAccount(accountType));

    // https://github.com/ReactiveX/rxjs/issues/2816
    if (isEmpty(sftpAccountGets)) {
      return of([]);
    }

    return forkJoin(sftpAccountGets).pipe(map(response => response.filter(Boolean)));
  }

  private canRead(sftpAccountType: SftpAccountType): boolean {
    return this.permissionsService.hasPermission(sftpAccountTypeReadPermissionsMap.get(sftpAccountType));
  }

  public getSftpAccount(sftpAccountType: SftpAccountType): Observable<SftpAccountViewModel> {
    const sftpAccountTypeUrl = sftpAccountTypeUrlMap.get(sftpAccountType);
    const url =
      this.airlineId && sftpAccountTypeUrl !== SftpAccountType.Global
        ? `${this.baseUrl}airlines/${this.airlineId}/${sftpAccountTypeUrl}`
        : `${this.baseUrl}${sftpAccountTypeUrl}`;

    return this.httpClient.get<SftpAccount[]>(url).pipe(
      switchMap(([accountInfo]) => {
        const sftpAccount = this.normalizeSftpAccount(accountInfo);

        if (sftpAccount.state === SftpAccountState.Active && sftpAccount.owner === SftpAccountOwner.Yes) {
          return this.getSftpAccountPublicKeys(sftpAccountType).pipe(
            map(publicKeys => ({ ...sftpAccount, publicKeys }))
          );
        }

        return of({ ...sftpAccount, publicKeys: [] });
      }),
      catchError(error => {
        this.globalErrorHandlerService.handleError(error);

        return of(null);
      })
    );
  }

  private getSftpAccountPublicKeys(sftpAccountType: SftpAccountType): Observable<SftpAccountPublicKey[]> {
    const sftpAccountTypeUrl = sftpAccountTypeUrlMap.get(sftpAccountType);
    const url = this.airlineId
      ? `${this.baseUrl}public-keys/airlines/${this.airlineId}/${sftpAccountTypeUrl}`
      : `${this.baseUrl}public-keys/${sftpAccountTypeUrl}`;

    return this.httpClient.get<SftpAccountPublicKey[]>(url).pipe(
      catchError(error => {
        this.globalErrorHandlerService.handleError(error);

        return of([]);
      })
    );
  }

  private normalizeSftpAccount(sftpAccount: any): SftpAccountViewModel {
    const sftpAccountType = camelCase(sftpAccount.sftpAccountType) as SftpAccountType;
    const state = camelCase(sftpAccount.state) as SftpAccountState;
    const owner = camelCase(sftpAccount.owner) as SftpAccountOwner;
    const actionType = sftpAccountStateActionTypesMap.get(state);

    return {
      ...sftpAccount,
      sftpAccountType,
      state,
      owner,
      sftpAccountTypeText: this.translationService.translate(`${translationNs}.accountType.${sftpAccountType}`),
      actionText: this.translationService.translate(`${translationNs}.accountAction.${actionType}`),
      stateText: this.translationService.translate(`${translationNs}.accountState.${state}`),
      ownerText: owner ? this.translationService.translate(`${translationNs}.accountOwner.${owner}`) : null,
      hasUpdatePermission: this.hasUpdatePermission(sftpAccountType)
    };
  }

  private hasUpdatePermission(sftpAccountType: SftpAccountType): boolean {
    const permission = sftpAccountTypeUpdatePermissionsMap.get(sftpAccountType);

    return permission ? this.permissionsService.hasPermission(permission) : false;
  }

  public save(sftpAccount: SftpAccountViewModel, passwd: string): Observable<SftpAccount> {
    const { login, sftpAccountType } = sftpAccount;

    const sftpAccountTypeUrl = sftpAccountTypeUrlMap.get(sftpAccountType);
    const url = this.airlineId
      ? `${this.baseUrl}airlines/${this.airlineId}/${sftpAccountTypeUrl}`
      : `${this.baseUrl}${sftpAccountTypeUrl}`;

    const requestBody = { sftpAccountType: sftpAccountTypeSaveModelMap.get(sftpAccountType), login, passwd };
    const requestMethod = sftpAccount.state === SftpAccountState.NonExisting ? 'POST' : 'PUT';

    return this.httpClient.request<SftpAccount>(requestMethod, url, { body: requestBody });
  }

  public deletePublicKeys(
    sftpAccountType: SftpAccountType,
    publicKeys: SftpAccountPublicKey[],
    bspId: number
  ): Observable<{ sftpAccount: SftpAccountViewModel; publicKeys: SftpAccountPublicKey[]; errorMessage: string }> {
    const deleteRequests = publicKeys.map(publicKey => this.deletePublicKey(sftpAccountType, publicKey, bspId));

    return forkJoin(deleteRequests).pipe(
      switchMap((res: any[]) => {
        const deletedPublicKeys = publicKeys.filter((_publicKey, index) => typeof res[index] !== 'string');

        const fileNames = res.filter(item => typeof item === 'string').join(', ');

        const errorMessage = fileNames
          ? this.translationService.translate(`${translationNs}.managePublicKeysDialog.errors.publicKeyDeleteError`, {
              fileNames
            })
          : '';

        return this.getSftpAccount(sftpAccountType).pipe(
          map(sftpAccount => ({ sftpAccount, publicKeys: deletedPublicKeys, errorMessage }))
        );
      })
    );
  }

  private deletePublicKey(
    sftpAccountType: SftpAccountType,
    publicKey: SftpAccountPublicKey,
    bspId: number
  ): Observable<SftpAccount | string> {
    const { filename } = publicKey;

    const sftpAccountTypeUrl = sftpAccountTypeUrlMap.get(sftpAccountType);
    const url = this.airlineId
      ? `${this.baseUrl}public-keys/airlines/${this.airlineId}/${sftpAccountTypeUrl}?bspId=${bspId}&filename=${filename}`
      : `${this.baseUrl}public-keys/${sftpAccountTypeUrl}?bspId=${bspId}&filename=${filename}`;

    return this.httpClient.delete<null | string>(url).pipe(catchError(() => of(filename)));
  }
}
