import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { SftpAccountDeletePublicKeysConfirmationDialogComponent } from './sftp-account-delete-public-keys-confirmation-dialog.component';
import { SftpAccountDeletePublicKeysConfirmationDialogActions as DialogActions } from '~app/master-data/sftp/store/actions';
import {
  getInitialState,
  getTestSftpAccount,
  getTestSftpAccountPublicKeys
} from '~app/master-data/sftp/test/sftp.test.mocks';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TranslatePipeMock } from '~app/test';

describe('SftpAccountDeletePublicKeysConfirmationDialogComponent', () => {
  let component: SftpAccountDeletePublicKeysConfirmationDialogComponent;
  let fixture: ComponentFixture<SftpAccountDeletePublicKeysConfirmationDialogComponent>;
  let store: MockStore;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Delete
          }
        ],
        footerButtonsType: [FooterButton.Delete],
        sftpAccount: { ...getTestSftpAccount() },
        publicKeys: getTestSftpAccountPublicKeys()
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      declarations: [SftpAccountDeletePublicKeysConfirmationDialogComponent, TranslatePipeMock],
      providers: [
        provideMockStore({ initialState: getInitialState() }),
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SftpAccountDeletePublicKeysConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.inject(MockStore);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('keysToBeDeletedFileNames should be comma-separated string of file names', () => {
    expect(component.keysToBeDeletedFileNames).toBe('test1.pub, test2.pub');
  });

  it('Cancel should dispatch cancelSftpAccountPublicKeysDelete', fakeAsync(() => {
    const reactiveSubject = TestBed.inject(ReactiveSubject);
    reactiveSubject.emit({
      contentComponentRef: {},
      clickedBtn: FooterButton.Cancel
    });

    tick(5);

    expect(store.dispatch).toHaveBeenCalledWith(
      DialogActions.cancelSftpAccountPublicKeysDelete({ sftpAccount: getTestSftpAccount() })
    );
  }));

  it('Delete should dispatch requestDeleteSftpAccountPublicKeys', fakeAsync(() => {
    const reactiveSubject = TestBed.inject(ReactiveSubject);
    reactiveSubject.emit({
      contentComponentRef: {},
      clickedBtn: FooterButton.Delete
    });

    tick(5);

    expect(store.dispatch).toHaveBeenCalledWith(
      DialogActions.requestDeleteSftpAccountPublicKeys({
        sftpAccount: getTestSftpAccount(),
        publicKeys: getTestSftpAccountPublicKeys(),
        bspId: 10000
      })
    );
  }));
});
