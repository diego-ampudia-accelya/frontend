import { Component, OnDestroy, OnInit } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { iif, Observable, of, Subject } from 'rxjs';
import { delay, filter, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { SftpAccountPublicKey, SftpAccountViewModel } from '~app/master-data/sftp/models';
import { SftpAccountDeletePublicKeysConfirmationDialogActions as DialogActions } from '~app/master-data/sftp/store/actions';
import * as fromSftp from '~app/master-data/sftp/store/reducers';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-sftp-account-delete-public-keys-confirmation-dialog',
  templateUrl: './sftp-account-delete-public-keys-confirmation-dialog.component.html',
  styleUrls: ['./sftp-account-delete-public-keys-confirmation-dialog.component.scss']
})
export class SftpAccountDeletePublicKeysConfirmationDialogComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();

  public sftpAccount: SftpAccountViewModel;

  public publicKeys: SftpAccountPublicKey[];

  public keysToBeDeletedFileNames = '';

  public loading$: Observable<boolean> = this.store.pipe(
    select(fromSftp.getDeletePublicKeysConfirmationLoading),
    tap(loading => this.setLoading(loading)),
    takeUntil(this.destroy$)
  );

  private deleteButton: ModalAction;

  private userBspId$ = this.store.pipe(
    select(fromAuth.getUserBsps),
    map(([bsp]) => bsp.id),
    take(1)
  );

  constructor(private config: DialogConfig, private reactiveSubject: ReactiveSubject, private store: Store<AppState>) {}

  public ngOnInit(): void {
    this.sftpAccount = this.config.data.sftpAccount;
    this.publicKeys = this.config.data.publicKeys;
    this.keysToBeDeletedFileNames = this.publicKeys.map(key => key.filename).join(', ');
    this.initializeDeleteButton();
    this.initializeDialogActionsSubscription();
  }

  private initializeDeleteButton() {
    this.deleteButton = this.config.data.buttons.find((button: ModalAction) => button.type === FooterButton.Delete);
    this.loading$.subscribe();
  }

  private setLoading(loading: boolean) {
    this.deleteButton.isLoading = loading;
  }

  private initializeDialogActionsSubscription() {
    this.reactiveSubject.asObservable
      .pipe(
        filter(res => Boolean(res.contentComponentRef)),
        switchMap(res => {
          const sftpAccount = this.sftpAccount;
          const publicKeys = this.publicKeys;

          return iif(
            () => res.clickedBtn !== FooterButton.Delete,
            of(DialogActions.cancelSftpAccountPublicKeysDelete({ sftpAccount })),
            this.userBspId$.pipe(
              map(bspId => DialogActions.requestDeleteSftpAccountPublicKeys({ sftpAccount, publicKeys, bspId }))
            )
          );
        }),
        delay(1),
        takeUntil(this.destroy$)
      )
      .subscribe((action: Action) => {
        this.store.dispatch(action);
      });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.store.dispatch(DialogActions.sftpAccountDeletePubicKeysConfirmationDialogDestroy());
  }
}
