export interface ThirdParty {
  id: number;
  code: string;
  name: string;
  active: boolean;
  organization: string;
}
