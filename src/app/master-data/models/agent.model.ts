import { AgentLocalInformation } from './agent-local-information';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AgentContact } from '~app/master-data/models/agent-contact.model';
import { LocationClass } from '~app/shared/enums/location-class.enum';
import { LocationType } from '~app/shared/enums/location-type.enum';
import { PaymentStatus } from '~app/shared/enums/payment-status.enum';
import { RemittanceFrequency } from '~app/shared/enums/remittance-frequency.enum';
import { Bsp } from '~app/shared/models/bsp.model';

export interface Agent {
  id: number;
  iataCode: string;
  parentIataCode?: string;
  name: string;
  cash?: PaymentStatus;
  easyPay?: PaymentStatus;
  paymentCard?: PaymentStatus;
  locationClass?: LocationClass;
  locationType?: LocationType;
  remittanceFrequency?: RemittanceFrequency;
  taxId?: string;
  tradeName?: string;
  vatNumber?: string;
  logo?: string;
  effectiveFrom: string;
  effectiveTo?: string;
  active?: boolean;
  version?: number;
  address?: AgentAddress;
  contact?: AgentContact;
  bsp: Bsp;
  localAgentInformation?: AgentLocalInformation;
}

export interface AgentInfo {
  agentName: string;
  agentCode: string;
}

export interface AgentFilter {
  bspId: Bsp[] | Bsp;
  active: string;
  registerDate: Date;
  expiryDate: Date;
  agentCodeName: string;
  locationClass: LocationClass[];
  locationType: LocationType[];
  remittanceFrequency: RemittanceFrequency[];
  vatNumber: string;
  city: string;
  state: string;
  addressCountry: string;
  postalCode: string;
  email: string;
  telephone: string;
  fax: string;
  easyPay: PaymentStatus[];
  cash: PaymentStatus[];
  paymentCard: PaymentStatus[];
  parentIataCode: string;
  locality: string;
}
