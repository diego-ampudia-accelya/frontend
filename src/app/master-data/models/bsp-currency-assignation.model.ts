import { Bsp } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface BspCurrencyAssignation {
  id: number;
  default: boolean;
  effectiveFrom: string;
  effectiveTo: string;
  active: boolean;
  bsp: Bsp;
  currency: Currency;
  decimals: number;
}
