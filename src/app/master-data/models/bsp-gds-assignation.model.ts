/* eslint-disable @typescript-eslint/naming-convention */
import { Gds } from '~app/master-data/models/gds.model';
import { Bsp } from '~app/shared/models/bsp.model';

export interface BspGdsAssignation {
  id: number;
  bsp: Bsp;
  gds: Gds;
  refundsEnabled: boolean;
  effectiveFrom: string;
  effectiveTo: string;
  active: boolean;
}

export interface BspGdsAssignationCreateModel {
  refundEnabled: boolean;
  gds: {
    gdsCode: string;
    name: string;
  };
}

export interface BspGdsAssignationEditModel {
  refundEnabled: boolean;
  gds: {
    name: string;
  };
}

export enum BspGdsAssignationModifyDialogType {
  Edit = 'edit',
  Delete = 'delete',
  Create = 'create'
}
