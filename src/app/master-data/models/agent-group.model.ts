import { AgentGroupIndividualAccess, AgentGroupMembership } from '../enums';
import { BspDto } from '~app/shared/models/bsp.model';

export interface AgentGroup {
  id: number;
  iataCode: string;
  lastName: string;
  active: boolean;
  organization: string;
  bsp?: BspDto;
}

export interface AgentGroupCreateForm {
  iataCode: string;
  portalEmail: string;
  name: string;
  organization: string;
  telephone: string;
  address: string;
  postCode: string;
  city: string;
  country: string;
}

export interface MasterDataAgentGroup {
  id: number;
  user: {
    bsp: BspDto;

    status: string;
    userTypeDetails: {
      iataCode: string;
      name: string;
    };
    contactInfo: {
      organization: string;
      telephone: string;
      city: string;
      address: string;
      postCode: string;
      email: string;
    };
  };
  creator: string;
  members: number;
}

export interface MasterDataAgentGroupMember {
  id: string;
  agentName: string;
  user: {
    bsp: BspDto;
    userTypeDetails: {
      iataCode: string;
      name: string;
    };
    contactInfo: {
      organization: string;
      telephone: string;
      city: string;
      address: string;
      postCode: string;
      email: string;
    };
  };
  membership: AgentGroupMembership;
  individualAccess: AgentGroupIndividualAccess;
}

export interface AgentGroupInvitation {
  id: string;
  individualAccess: AgentGroupIndividualAccess;
  group: MasterDataAgentGroup;
}

export interface MainUserAgentGroup {
  id: number;
  userCode: string;
  name: string;
  organization: string;
}
