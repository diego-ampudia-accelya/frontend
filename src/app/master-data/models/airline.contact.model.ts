export interface AirlineContact {
  email?: string;
  firstName?: string;
  lastName?: string;
  telephone?: string;
}
