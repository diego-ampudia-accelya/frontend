import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';

export interface BulkUpdateResponse<T = any> {
  data: T;
  error?: ResponseErrorBE;
  status?: number;
}
