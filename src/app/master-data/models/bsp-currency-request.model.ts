import { BspCurrency } from './bsp-currency.model';

export interface BspCurrencyRequest {
  effectiveTo: string;
  currency: BspCurrency;
}
