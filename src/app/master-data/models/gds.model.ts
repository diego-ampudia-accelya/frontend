export interface Gds {
  id: number;
  name: string;
  gdsCode: string;
  version?: number;
}
