import { AirlineAddress } from '../airline-address.model';
import { AirlineContact } from '../airline.contact.model';

export interface LocalAndGlobalAirline {
  id: number;
  designator: string;
  iataCode: string;
  localName: string;
  globalName: string;
  bsp: { id: number; isoCountryCode: string; name: string };
  vatNumber: string;
  active: true;
  address: AirlineAddress;
  contact: AirlineContact;
}
