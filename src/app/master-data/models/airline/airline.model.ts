import { AirlineAddress } from '../airline-address.model';
import { AirlineContact } from '../airline.contact.model';

import { GlobalAirline } from './global-airline.model';
import { Bsp } from '~app/shared/models/bsp.model';
import { EasyPayStatus } from '~app/shared/enums/easypay-status.enum';

// ToDo: Airline models should be grouped together in the same `airline` directory

export interface Airline {
  id: number;
  localName: string;
  bsp: Bsp;
  designator: string;
  vatNumber?: string;
  easyPayStatus?: EasyPayStatus;
  easyPayEffectiveFrom?: string;
  effectiveFrom: string;
  effectiveTo?: string;
  contact?: AirlineContact;
  address?: AirlineAddress;
  active: boolean;
  globalAirline: GlobalAirline;
  version?: number;
}
