/* eslint-disable @typescript-eslint/naming-convention */
import { Airline } from './airline.model';

export interface GlobalAirline {
  id: number;
  globalName: string;
  iataCode: string;
  designator?: string;
  template?: GlobalAirlineTemplate;
  logo?: string;
  airlines?: Airline[];
  version?: number;
}

export enum GlobalAirlineTemplate {
  Efficient = 'EFFICIENT',
  Streamlined = 'STREAMLINED',
  Lean = 'LEAN'
}
