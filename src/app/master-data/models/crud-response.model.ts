export interface CrudResponse {
  pageNumber: number;
  pageSize: number;
  total: number;
  records: [];
}
