export interface AgentLocalInformation {
  agentLocalName: string;
  billingLocalAddress: string;
  billingLocalAddressPostCode: string;
  registrationLocalAddress: string;
  registrationLocalAddressPostCode: string;
  enterpriseLegalPerson: string;
}
