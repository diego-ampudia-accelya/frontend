export interface BspCurrency {
  code?: string;
  decimals: number;
  effectiveTo?: Date;
}
