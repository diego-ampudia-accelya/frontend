export interface AirlineAddress {
  address1?: string;
  address2?: string;
  city?: string;
  country?: string;
  postalCode?: string;
  state?: string;
  telephone?: string;
}
