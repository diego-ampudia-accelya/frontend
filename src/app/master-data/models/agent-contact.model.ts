export interface AgentContact {
  email?: string;
  fax?: string;
  telephone?: string;
}
