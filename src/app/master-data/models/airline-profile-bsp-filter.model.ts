import { Bsp } from '~app/shared/models/bsp.model';

export interface AirlineProfileBspFilter {
  bsp: Bsp;
}
