export interface AgentAddress {
  city?: string;
  country?: string;
  postalCode?: string;
  state?: string;
  street?: string;
  telephone?: string;
  locality?: string;
}
