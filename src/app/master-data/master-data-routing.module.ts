import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompanyCalendarGuard } from './company-calendar/company-calendar.guard';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ROUTES.BSP.path,
        loadChildren: () => import('./bsp/bsp.module').then(m => m.BspModule)
      },
      {
        path: ROUTES.BSP_CURRENCY_ASSIGNATION.path,
        loadChildren: () =>
          import('./bsp-currency-assignation/bsp-currency-assignation.module').then(m => m.BspCurrencyAssignationModule)
      },
      {
        path: ROUTES.BSP_GDS_ASSIGNATION.path,
        loadChildren: () =>
          import('./bsp-gds-assignation/bsp-gds-assignation.module').then(m => m.BspGdsAssignationModule)
      },
      {
        path: ROUTES.PERIODS.path,
        loadChildren: () => import('./periods/periods.module').then(m => m.PeriodsModule)
      },
      {
        path: ROUTES.CURRENCY.path,
        loadChildren: () => import('./currency/currency.module').then(m => m.CurrencyModule)
      },
      {
        path: ROUTES.VARIABLE_REMITTANCE.path,
        loadChildren: () =>
          import('./variable-remittance/variable-remittance.module').then(m => m.VariableRemittanceModule)
      },
      {
        path: ROUTES.GDS.path,
        loadChildren: () => import('./gds/gds.module').then(m => m.GdsModule)
      },
      {
        path: ROUTES.AIRLINE.path,
        loadChildren: () => import('./airline/airline.module').then(m => m.AirlineModule)
      },
      {
        path: ROUTES.CREDIT_CARDS.path,
        loadChildren: () => import('./credit-card/credit-card.module').then(m => m.CreditCardModule)
      },
      {
        path: ROUTES.GLOBAL_AIRLINE.path,
        loadChildren: () => import('./global-airline/global-airline.module').then(m => m.GlobalAirlineModule)
      },
      {
        path: ROUTES.AGENT.path,
        loadChildren: () => import('./agent/agent.module').then(m => m.AgentModule)
      },
      {
        path: ROUTES.THIRD_PARTY.path,
        loadChildren: () => import('./third-party/third-party.module').then(m => m.ThirdPartyModule)
      },
      {
        path: ROUTES.AGENT_GROUP.path,
        loadChildren: () => import('./agent-group/agent-group.module').then(m => m.AgentGroupModule)
      },
      {
        path: ROUTES.IATA.path,
        loadChildren: () => import('./iata/iata.module').then(m => m.IataModule)
      },

      {
        path: ROUTES.DPC.path,
        loadChildren: () => import('./dpc/dpc.module').then(m => m.DpcModule)
      },
      {
        path: ROUTES.USERS.path,
        loadChildren: () => import('~app/sub-users/sub-users.module').then(m => m.SubUsersModule)
      },
      {
        path: ROUTES.HOMC_SUB_USER.path,
        loadChildren: () =>
          import('~app/master-data/head-office-sub-users/head-office-sub-users.module').then(
            m => m.HeadOfficeSubUsersModule
          )
      },
      {
        path: ROUTES.TICKETING_AUTHORITY_HISTORY.path,
        loadChildren: () =>
          import('./ticketing-authority-history/ticketing-authority-history.module').then(
            m => m.TicketingAuthorityHistoryModule
          )
      },
      {
        path: ROUTES.FILE_DESCRIPTORS.path,
        loadChildren: () => import('./file-descriptors/file-descriptors.module').then(m => m.FileDescriptorsModule)
      },
      {
        path: ROUTES.AUDIT.path,
        loadChildren: () => import('./audit/audit.module').then(m => m.AuditModule)
      },
      {
        path: ROUTES.SAF_TYPES.path,
        loadChildren: () => import('./saf-types/saf-types.module').then(m => m.SafTypesModule)
      },
      {
        path: ROUTES.SAF_SERIES.path,
        loadChildren: () => import('./saf-series/saf-series.module').then(m => m.SafSeriesModule)
      },
      {
        path: ROUTES.COMPANY_CALENDAR.path,
        canActivate: [CompanyCalendarGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterDataRoutingModule {}
