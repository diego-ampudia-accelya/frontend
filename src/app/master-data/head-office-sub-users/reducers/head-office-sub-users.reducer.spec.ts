import { HttpErrorResponse } from '@angular/common/http';

import * as fromActions from '../actions/head-office-sub-users.actions';
import * as fromState from '../store/head-office-sub-users.state';
import * as fromStub from '../stubs/head-office-sub-users.stubs';

import * as fromReducer from './head-office-sub-users.reducer';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('headOfficeSubUserReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = fromReducer.headOfficeSubUserReducer(fromState.initialState, action);

      expect(result).toBe(fromState.initialState);
    });
  });

  describe('searchHeadOfficeSubUserRequest', () => {
    it('should set loading and reset error', () => {
      const action = fromActions.searchHeadOfficeSubUserRequest({ payload: { query: defaultQuery } });
      const result = fromReducer.headOfficeSubUserReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        query: defaultQuery,
        loading: {
          ...fromState.initialState.loading,
          searchHeadOfficeSubUser: true
        },
        error: {
          ...fromState.initialState.error,
          searchHeadOfficeSubUser: null
        }
      });
    });
  });

  describe('searchHeadOfficeSubUserFailure', () => {
    it('should reset loading and set error', () => {
      const error = new HttpErrorResponse({ error: 'mock error' });
      const action = fromActions.searchHeadOfficeSubUserFailure({ payload: { error } });
      const result = fromReducer.headOfficeSubUserReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        loading: {
          ...fromState.initialState.loading,
          searchHeadOfficeSubUser: false
        },
        error: {
          ...fromState.initialState.error,
          searchHeadOfficeSubUser: error
        }
      });
    });
  });

  describe('searchHeadOfficeSubUserSuccess', () => {
    it('should reset loading and set data', () => {
      const action = fromActions.searchHeadOfficeSubUserSuccess({
        payload: { pagedData: fromStub.headOfficeSubUsersPagedData }
      });
      const result = fromReducer.headOfficeSubUserReducer(fromState.initialState, action);

      expect(result).toEqual({
        ...fromState.initialState,
        data: fromStub.headOfficeSubUsersPagedData.records,
        query: {
          ...fromState.initialState.query,
          paginateBy: {
            page: fromStub.headOfficeSubUsersPagedData.pageNumber,
            size: fromStub.headOfficeSubUsersPagedData.pageSize,
            totalElements: fromStub.headOfficeSubUsersPagedData.total
          }
        },
        loading: {
          ...fromState.initialState.loading,
          searchHeadOfficeSubUser: false
        },
        error: {
          ...fromState.initialState.error,
          searchHeadOfficeSubUser: null
        }
      });
    });
  });
});
