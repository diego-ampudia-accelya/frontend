import { createReducer, on } from '@ngrx/store';
import { chain, isEmpty } from 'lodash';

import * as fromPermissionActions from '../actions/head-office-sub-users-permissions.actions';
import * as fromActions from '../actions/head-office-sub-users.actions';
import { HeadOfficeSubUserState, initialState, propertyCategories } from '../store/head-office-sub-users.state';
import { ChangeModel } from '~app/shared/components';
import { SelectionList, SelectionListItem } from '~app/shared/components/selection-list/selection-list.models';
import { ServerError } from '~app/shared/errors';
import { toSubUserHomcUpdateModel } from '~app/shared/helpers/user-homc.helper';
import { UserPermission } from '~app/sub-users/shared/permissions.models';
import { SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

export const headOfficeSubUserReducer = createReducer(
  initialState,
  on(fromActions.searchHeadOfficeSubUserRequest, (state, { payload: { query } }) => ({
    ...state,
    query,
    loading: {
      ...state.loading,
      searchHeadOfficeSubUser: true
    },
    error: {
      ...state.error,
      searchHeadOfficeSubUser: null
    }
  })),
  on(fromActions.searchHeadOfficeSubUserFailure, (state, { payload: { error } }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchHeadOfficeSubUser: false
    },
    error: {
      ...state.error,
      searchHeadOfficeSubUser: error
    }
  })),
  on(fromActions.searchHeadOfficeSubUserSuccess, (state, { payload: { pagedData } }) => ({
    ...state,
    data: pagedData.records,
    query: {
      ...state.query,
      paginateBy: {
        page: pagedData.pageNumber,
        size: pagedData.pageSize,
        totalElements: pagedData.total
      }
    },
    loading: {
      ...state.loading,
      searchHeadOfficeSubUser: false
    },
    error: {
      ...state.error,
      searchHeadOfficeSubUser: null
    }
  })),
  on(fromActions.openAdd, state => ({ ...state })),
  on(fromActions.add, state => ({
    ...state,
    loading: {
      ...state.loading,
      searchHeadOfficeSubUser: true
    }
  })),
  on(fromActions.addSuccess, state => ({
    ...state,
    loading: {
      ...state.loading,
      searchHeadOfficeSubUser: false
    }
  })),
  on(fromActions.addError, (state, { error }) => ({
    ...state,
    loading: {
      ...state.loading,
      searchHeadOfficeSubUser: false
    },
    errors: error
  })),
  on(fromActions.requestSubUser, fromActions.updateSubUserProperties, state => ({
    ...state,
    loading: {
      ...state.loading,
      subUserRequest: true
    }
  })),
  on(fromActions.requestSubUserSuccess, (state, { subUser }) => ({
    ...state,
    subUser,
    loading: {
      ...state.loading,
      subUserRequest: false
    }
  })),
  on(fromActions.requestSubUserError, (state, { error }) => ({
    ...state,
    subUserRequestError: error,
    loading: {
      ...state.loading,
      subUserRequest: false
    }
  })),
  on(fromActions.propertiesFormValueChange, (state, { isFormValid, formValue: propertiesFormValue }) => ({
    ...state,
    propertiesFormValue,
    hasPropertyValidationErrors: !isFormValid
  })),
  on(fromActions.updateSubUserPropertiesSuccess, (state, { subUser }) => ({
    ...state,
    loading: {
      ...state.loading,
      subUserRequest: false
    },
    propertiesFormValue: null,
    subUserPropertiesError: null,
    subUser
  })),
  on(fromActions.updateSubUserPropertiesError, (state, { error }) => ({
    ...state,
    loading: {
      ...state.loading,
      subUserRequest: false
    },
    subUserPropertiesError: error
  })),
  on(fromActions.discardPropertyChanges, state => ({
    ...state,
    propertiesFormValue: null
  })),
  on(fromActions.cancelPropertyChanges, state => ({
    ...state
  })),
  on(fromActions.changeSubUserStatus, (state, { status }) => ({
    ...state,
    subUser: {
      ...state.subUser,
      status
    }
  })),
  on(fromPermissionActions.requestPermissions, fromPermissionActions.applyChanges, state => ({
    ...state,
    permissions: {
      ...state.permissions,
      loading: true
    }
  })),
  on(
    fromPermissionActions.requestPermissionsSuccess,
    fromPermissionActions.applyChangesSuccess,
    (state, { permissions, subUser }) => {
      const original = mapPermissionsToSelectionList(permissions, subUser.template);

      return {
        ...state,
        permissions: {
          ...state.permissions,
          original,
          userId: subUser.id,
          modified: original,
          loading: false
        }
      };
    }
  ),
  on(fromPermissionActions.requestPermissionsError, state => ({
    ...state,
    permissions: {
      ...state.permissions,
      hasError: true,
      loading: false
    }
  })),
  on(fromPermissionActions.change, (state, { modifications }) => ({
    ...state,
    permissions: {
      ...state.permissions,
      modified: modifications
    }
  })),
  on(fromPermissionActions.applyChangesError, state => ({
    ...state,
    permissions: {
      ...state.permissions,
      loading: false
    }
  })),
  on(fromPermissionActions.discardChanges, state => ({
    ...state,
    permissions: {
      ...state.permissions,
      original: [...state.permissions.original],
      modified: state.permissions.original
    }
  }))
);

export const getPropertyChanges = (state: HeadOfficeSubUserState): ChangeModel[] => {
  const subUserDetails = toSubUserHomcUpdateModel(state.subUser);
  const { propertiesFormValue } = state;

  return Object.entries(propertiesFormValue || {}).reduce((changes, [name, value]) => {
    const originalValue = subUserDetails[name];

    if (value !== originalValue && Array.isArray(value)) {
      const changeToPush = {
        group: propertyCategories[name],
        name
      };

      // The elements that will be disabled are those that were already in originalValues but now are not in values.
      const elementsToDisabled = value.filter(val => !val.access && originalValue.some(org => org.id === val.id));

      // The elements that will be enabled are all those that are now in values and were not previously in originalValues.
      const elementsToEnabled = value.filter(val => val.access && originalValue.every(org => org.id !== val.id));

      const valueToPush = [...elementsToDisabled, ...elementsToEnabled];

      if (valueToPush.length) {
        changes = valueToPush.reduce((acc, val) => [...acc, { ...changeToPush, value: val }], changes);
      }
    }

    if (value !== originalValue && (typeof value === 'boolean' || typeof value !== 'object')) {
      changes = [
        ...changes,
        {
          group: propertyCategories[name],
          name,
          value
        }
      ];
    }

    return changes;
  }, []);
};

export const getHasPropertyValidationErrors = (state: HeadOfficeSubUserState) => state.hasPropertyValidationErrors;

export const canApplyPropertyChanges = (state: HeadOfficeSubUserState): boolean =>
  !isEmpty(getPropertyChanges(state)) && !state.hasPropertyValidationErrors;

export const getSubUserPropertiesError = (state: HeadOfficeSubUserState): ServerError => state.subUserPropertiesError;

export const getSubUserPortalEmail = (state: HeadOfficeSubUserState) => state.subUser?.portalEmail;

export const getOriginalPermissions = (state: HeadOfficeSubUserState) => state.permissions.original;

export const getModifiedPermissions = (state: HeadOfficeSubUserState) => state.permissions.modified;

export const getLoadingPermissions = (state: HeadOfficeSubUserState) => state.permissions.loading;

export const getBspId = (state: HeadOfficeSubUserState) => state.permissions.bspId;

export const hasChangesPermissions = (state: HeadOfficeSubUserState) =>
  state.permissions.modified.some(permissionCategory => permissionCategory.hasChanged);

export function getChangesPermissions(state: HeadOfficeSubUserState): ChangeModel[] {
  return state.permissions.modified
    .filter(category => category.hasChanged)
    .map(category => ({ ...category, items: category.items.filter(item => item.hasChanged) }))
    .reduce((changes, item) => {
      item.items.forEach((listItem: SelectionListItem) => {
        changes.push({
          group: item.name,
          name: listItem.name,
          value: String(listItem.checked),
          originalValue: listItem.checked
        });
      });

      return changes;
    }, []);
}

function mapPermissionsToSelectionList(permissions: UserPermission[], userTemplate?: SubUserTemplate): SelectionList[] {
  const isEfficientUser = userTemplate === SubUserTemplate.Efficient;

  return chain(permissions)
    .map(permission => {
      const disabled = isEfficientUser && permission.template === SubUserTemplate.Streamlined;

      return {
        name: permission.name,
        checked: !disabled && permission.enabled,
        value: permission,
        hasChanged: false,
        disabled,
        tooltip: disabled ? 'subUsers.profile.permissionsTab.disabledTooltip' : ''
      };
    })
    .groupBy('value.category.name')
    .map((items, name) => {
      const disabled = items.every(item => item.disabled);

      return {
        name,
        disabled,
        items,
        tooltip: disabled ? 'subUsers.profile.permissionsTab.disabledTooltip' : ''
      };
    })
    .value();
}

export function mapSelectionListToPermissions(
  selectionListItems: SelectionList[],
  multiPermissionsEnabled = true
): UserPermission[] {
  return chain(selectionListItems)
    .map(category => category.items)
    .flatten()
    .map(item => ({ ...item.value, enabled: multiPermissionsEnabled ? item.checked : !item.checked }))
    .value();
}
