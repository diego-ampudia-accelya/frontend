import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { SubUserHomcPermissionsComponent } from './containers/sub-user-homc-profile-permissions/sub-user-homc-profile-permissions.component';
import { SubUserHomcProfileComponent } from './containers/sub-user-homc-profile/sub-user-homc-profile.component';
import { SubUserHomcPropertiesComponent } from './containers/sub-user-homc-properties/sub-user-homc-properties.component';
import { HeadOfficeSubUsersComponent } from './head-office-sub-users.component';
import { SubUserHomcProfileResolver } from './shared/sub-user-homc-profile.resolver';
import { Permissions } from '~app/shared/constants/permissions';
import { ROUTES } from '~app/shared/constants';
import { CanComponentDeactivateGuard } from '~app/core/services';

const subUserRoutes: Route[] = [
  {
    path: 'properties',
    component: SubUserHomcPropertiesComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      title: 'userProperties',
      requiredPermissions: Permissions.readHOMCSU
    }
  },
  {
    path: 'permissions',
    component: SubUserHomcPermissionsComponent,
    canDeactivate: [CanComponentDeactivateGuard],
    data: {
      title: 'subUsers.profile.tabNames.permissions',
      requiredPermissions: Permissions.readHOMCSUPermissions
    }
  },
  { path: ROUTES.HOMC_SUB_USER_DETAILS.path, redirectTo: 'properties', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: '',
    component: HeadOfficeSubUsersComponent,
    data: {
      tab: ROUTES.HOMC_SUB_USER
    }
  },
  {
    path: ROUTES.HOMC_SUB_USER_DETAILS.path,
    component: SubUserHomcProfileComponent,
    resolve: { subUser: SubUserHomcProfileResolver },
    data: {
      tab: ROUTES.HOMC_SUB_USER_DETAILS
    },
    children: subUserRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeadOfficeSubUsersRoutingModule {}
