import { inject, TestBed } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { HeadOfficeSubUsersFilterFormatterService } from './head-office-sub-users-filter-formatter.service';

describe('Service: HeadOfficeSubUsersFilterFormatterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HeadOfficeSubUsersFilterFormatterService,
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        }
      ]
    });
  });

  it('should be created', inject(
    [HeadOfficeSubUsersFilterFormatterService],
    (service: HeadOfficeSubUsersFilterFormatterService) => {
      expect(service).toBeTruthy();
    }
  ));
});
