import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  EStatus,
  HeadOfficeSubUser,
  HeadOfficeSubUserCreateRequest,
  HeadOfficeSubUserFilters,
  HeadOfficeSubUserFiltersBE,
  HeadOfficeSubUserUpdateModel
} from '../models/head-office-sub-users.models';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';
import { UserPermission, UserPermissionDto, UserPermissionUpdate } from '~app/sub-users/shared/permissions.models';

@Injectable()
export class HeadOfficeSubUsersService implements Queryable<HeadOfficeSubUser> {
  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  private baseUrl = `${this.appConfiguration.baseApiPath}/user-management/homcsus`;

  public find(query: DataQuery<HeadOfficeSubUserFilters>): Observable<PagedData<HeadOfficeSubUser>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<HeadOfficeSubUser>>(this.baseUrl + requestQuery.getQueryString());
  }

  public create(item: Partial<HeadOfficeSubUser>): Observable<HeadOfficeSubUser> {
    const request: HeadOfficeSubUserCreateRequest = {
      portalEmail: item.portalEmail,
      name: item.name,
      organization: item.contactInfo.organization,
      telephone: item.contactInfo.telephone,
      country: item.contactInfo.country,
      city: item.contactInfo.city,
      address: item.contactInfo.address,
      postCode: item.contactInfo.postCode,
      access: item.access.toString()
    };

    return this.http.post<HeadOfficeSubUser>(this.baseUrl, request);
  }

  public updateSubUserStatus(id: number, status: EStatus, portalEmail?: string): Observable<HeadOfficeSubUser> {
    const url = `${this.baseUrl}/${id}/status`;
    const request = {
      status,
      portalEmail
    };

    return this.http.put<HeadOfficeSubUser>(url, request);
  }

  public getBy(id: number): Observable<HeadOfficeSubUser> {
    return this.http.get<HeadOfficeSubUser>(`${this.baseUrl}/${id}`);
  }

  public getPermissionsBy(userId: number): Observable<UserPermission[]> {
    return this.http
      .get<UserPermissionDto[]>(`${this.baseUrl}/${userId}/permissions`)
      .pipe(map(this.toUserPermissions));
  }

  public updatePermissionsByUserId(id: number, permissions: UserPermission[]) {
    const data: UserPermissionUpdate[] = permissions.map(p => ({ id: p.id, enabled: p.enabled }));

    return this.http
      .put<UserPermissionDto[]>(`${this.baseUrl}/${id}/permissions`, data)
      .pipe(map(this.toUserPermissions));
  }

  public update(data: Partial<HeadOfficeSubUserUpdateModel>): Observable<HeadOfficeSubUser> {
    const formatedData = data;

    return this.http.put<HeadOfficeSubUser>(`${this.baseUrl}/${data.id}`, formatedData);
  }

  public download(
    query: DataQuery<HeadOfficeSubUserFilters>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };

    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<HeadOfficeSubUserFilters>): RequestQuery<HeadOfficeSubUserFiltersBE> {
    const { registerDate, expiryDate, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromRegisterDate: registerDate && toShortIsoDate(registerDate[0]),
        toRegisterDate: registerDate && toShortIsoDate(registerDate[1] || registerDate[0]),
        fromExpiryDate: expiryDate && toShortIsoDate(expiryDate[0]),
        toExpiryDate: expiryDate && toShortIsoDate(expiryDate[1] || expiryDate[0])
      }
    });
  }

  private toUserPermissions = (dtos: UserPermissionDto[]): UserPermission[] =>
    dtos.map(({ catgory, ...p }) => ({ ...p, category: catgory }));
}
