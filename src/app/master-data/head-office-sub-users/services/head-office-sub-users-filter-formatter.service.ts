import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { HeadOfficeSubUserFilters } from '../models/head-office-sub-users.models';
import { FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type HeadOfficeSubUsersMapper<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class HeadOfficeSubUsersFilterFormatterService implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: HeadOfficeSubUserFilters) {
    const filterMappers: Partial<HeadOfficeSubUsersMapper<HeadOfficeSubUserFilters>> = {
      portalEmail: portalEmail => `${this.translate('portalEmail')} - ${portalEmail}`,
      name: name => `${this.translate('name')} - ${name}`,
      parentName: parentName => `${this.translate('parentName')} - ${parentName}`,
      status: status => `${this.translate('status')} - ${status}`,
      access: access => `${this.translate('access')} - ${access}`,
      registerDate: registerDate => `${this.translate('registerDate')} - ${rangeDateFilterTagMapper(registerDate)}`,
      expiryDate: expiryDate => `${this.translate('expiryDate')} - ${rangeDateFilterTagMapper(expiryDate)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`head-office-sub-users.filters.${key}.label`);
  }
}
