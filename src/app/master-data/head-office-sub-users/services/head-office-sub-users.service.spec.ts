import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { HeadOfficeSubUsersStoreFacadeService } from '../store/head-office-sub-users-store-facade.service';

import { HeadOfficeSubUsersService } from './head-office-sub-users.service';
import { AppConfigurationService } from '~app/shared/services';

describe('Service: HeadOfficeSubUsersService', () => {
  let service: HeadOfficeSubUsersService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        HeadOfficeSubUsersService,
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: ''
          }
        },
        {
          provide: HeadOfficeSubUsersStoreFacadeService,
          useValue: {
            actions: {
              searchHeadOfficeSubUser: () => {},
              downloadHeadOfficeSubUser: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null)
            },
            loading: {
              searchHeadOfficeSubUser$: of(null)
            }
          }
        }
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(HeadOfficeSubUsersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
