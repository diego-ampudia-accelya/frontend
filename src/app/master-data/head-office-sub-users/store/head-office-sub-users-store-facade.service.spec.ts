import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';

import * as fromActions from '../actions/head-office-sub-users.actions';
import * as fromSelectors from '../selectors/head-office-sub-users.selectors';

import { HeadOfficeSubUsersStoreFacadeService } from './head-office-sub-users-store-facade.service';
import { headOfficeSubUserFeatureKey, initialState } from './head-office-sub-users.state';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

describe('Service: HeadOfficeSubUsersStoreFacadeService', () => {
  let service: HeadOfficeSubUsersStoreFacadeService;
  let store: Store<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HeadOfficeSubUsersStoreFacadeService,
        provideMockStore({
          initialState: {
            [headOfficeSubUserFeatureKey]: initialState
          }
        })
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(HeadOfficeSubUsersStoreFacadeService);
    store = TestBed.inject<Store<unknown>>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('actions', () => {
    it('searchHeadOfficeSubUser should dispatch a new searchHeadOfficeSubUserRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.searchHeadOfficeSubUser(defaultQuery);

      expect(store.dispatch).toHaveBeenCalledWith(
        fromActions.searchHeadOfficeSubUserRequest({ payload: { query: defaultQuery } })
      );
    });

    it('downloadHeadOfficeSubUser should dispatch a new downloadHeadOfficeSubUserRequest action', () => {
      spyOn(store, 'dispatch').and.callThrough();

      service.actions.downloadHeadOfficeSubUser();

      expect(store.dispatch).toHaveBeenCalledWith(fromActions.downloadHeadOfficeSubUserRequest());
    });
  });

  describe('selectors', () => {
    it('query$ should select query', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.query$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHeadOfficeSubUserQuery);
    });

    it('data$ should select data', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.data$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHeadOfficeSubUserData);
    });

    it('hasData$ should select hasData', () => {
      spyOn(store, 'select').and.callThrough();

      service.selectors.hasData$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectHasData);
    });
  });

  describe('loading', () => {
    it('searchHeadOfficeSubUser$ should invoke selectSearchHeadOfficeSubUserLoading', () => {
      spyOn(store, 'select').and.callThrough();

      service.loading.searchHeadOfficeSubUser$; // eslint-disable-line

      expect(store.select).toHaveBeenCalledWith(fromSelectors.selectSearchHeadOfficeSubUserLoading);
    });
  });
});
