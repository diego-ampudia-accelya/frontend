import { HttpErrorResponse } from '@angular/common/http';

import { HeadOfficeSubUser, HeadOfficeSubUserFilters } from '../models/head-office-sub-users.models';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { ServerError } from '~app/shared/errors';

export const headOfficeSubUserFeatureKey = 'headOfficeSubUserFeatureKey';

export const propertyCategories = {
  id: 'id',
  access: 'access',
  status: 'status',
  registerDate: 'registerDate',
  expiryDate: 'expiryDate',
  portalEmail: 'portalEmail',
  name: 'name',
  organization: 'organization',
  telephone: 'telephone',
  country: 'country',
  city: 'city',
  address: 'address',
  postCode: 'postCode'
};

export interface HeadOfficeSubUserState {
  query: DataQuery<HeadOfficeSubUserFilters>;
  data: HeadOfficeSubUser[];
  subUser: HeadOfficeSubUser;
  subUserRequestError: ServerError;
  propertiesFormValue: Partial<HeadOfficeSubUser>;
  hasPropertyValidationErrors: boolean;
  subUserPropertiesError: ServerError;
  permissions: {
    userId: number;
    original: SelectionList[];
    modified: SelectionList[];
    loading: boolean;
    bspId: number;
  };
  loading: {
    searchHeadOfficeSubUser: boolean;
    subUserRequest: boolean;
  };
  error: {
    searchHeadOfficeSubUser: HttpErrorResponse | null;
  };
}

export const initialState: HeadOfficeSubUserState = {
  query: defaultQuery,
  data: [],
  subUser: null,
  subUserRequestError: null,
  propertiesFormValue: null,
  hasPropertyValidationErrors: false,
  subUserPropertiesError: null,
  permissions: {
    userId: null,
    original: [],
    modified: [],
    loading: false,
    bspId: null
  },
  loading: {
    searchHeadOfficeSubUser: false,
    subUserRequest: false
  },
  error: {
    searchHeadOfficeSubUser: null
  }
};
