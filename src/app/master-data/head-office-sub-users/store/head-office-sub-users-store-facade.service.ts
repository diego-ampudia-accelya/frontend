import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromActionsPermissions from '../actions/head-office-sub-users-permissions.actions';
import * as fromActions from '../actions/head-office-sub-users.actions';
import { EStatus, HeadOfficeSubUser, HeadOfficeSubUserFilters } from '../models/head-office-sub-users.models';
import * as fromSelectors from '../selectors/head-office-sub-users.selectors';
import { DataQuery } from '~app/shared/components/list-view';

@Injectable()
export class HeadOfficeSubUsersStoreFacadeService {
  constructor(private store: Store<unknown>) {}

  public get actions() {
    return {
      searchHeadOfficeSubUser: (query?: DataQuery<HeadOfficeSubUserFilters>) =>
        this.store.dispatch(fromActions.searchHeadOfficeSubUserRequest({ payload: { query } })),
      downloadHeadOfficeSubUser: () => this.store.dispatch(fromActions.downloadHeadOfficeSubUserRequest()),

      openCreateHeadOfficeSubUserDialog: () => this.store.dispatch(fromActions.openAdd()),
      add: (payload: Partial<HeadOfficeSubUser>) => this.store.dispatch(fromActions.add({ value: payload })),
      requestSubUser: (id: number) => this.store.dispatch(fromActions.requestSubUser({ id })),
      propertiesFormValueChange: (formValue: Partial<HeadOfficeSubUser>, isFormValid: boolean) =>
        this.store.dispatch(fromActions.propertiesFormValueChange({ formValue, isFormValid })),
      applyPropertyChanges: () => this.store.dispatch(fromActions.applyPropertyChanges()),
      changeSubUserStatus: (status: EStatus) => this.store.dispatch(fromActions.changeSubUserStatus({ status })),
      openApplyChanges: () => this.store.dispatch(fromActionsPermissions.openApplyChanges())
    };
  }

  public get selectors() {
    return {
      query$: this.store.select(fromSelectors.selectHeadOfficeSubUserQuery),
      data$: this.store.select(fromSelectors.selectHeadOfficeSubUserData),
      hasData$: this.store.select(fromSelectors.selectHasData),
      getProfileHeadOfficeSubUser$: this.store.select(fromSelectors.getProfileHeadOfficeSubUser),
      getProfileHeadOfficeSubUserError$: this.store.select(fromSelectors.getProfileHeadOfficeSubUserError),
      getProfileSubUserPropertiesError$: this.store.select(fromSelectors.getProfileSubUserPropertiesError),
      getProfileHeadOfficeSubUserLoading$: this.store.select(fromSelectors.getProfileHeadOfficeSubUserRequestLoading),
      getPropertiesFromValue$: this.store.select(fromSelectors.getPropertiesFormValue)
    };
  }

  public get loading() {
    return {
      searchHeadOfficeSubUser$: this.store.select(fromSelectors.selectSearchHeadOfficeSubUserLoading)
    };
  }
}
