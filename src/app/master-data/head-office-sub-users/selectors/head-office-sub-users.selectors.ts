import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromSubUserProfile from '../reducers/head-office-sub-users.reducer';
import { headOfficeSubUserFeatureKey, HeadOfficeSubUserState } from '../store/head-office-sub-users.state';

export const selectHeadOfficeSubUserStateFeature =
  createFeatureSelector<HeadOfficeSubUserState>(headOfficeSubUserFeatureKey);

export const selectHeadOfficeSubUserData = createSelector(selectHeadOfficeSubUserStateFeature, state => state.data);
export const selectHeadOfficeSubUserQuery = createSelector(selectHeadOfficeSubUserStateFeature, state => state.query);
export const selectSearchHeadOfficeSubUserLoading = createSelector(
  selectHeadOfficeSubUserStateFeature,
  state => state.loading.searchHeadOfficeSubUser
);

export const selectHasData = createSelector(selectHeadOfficeSubUserStateFeature, state =>
  Boolean(state.data && state.data.length)
);

export const getProfileHeadOfficeSubUser = createSelector(selectHeadOfficeSubUserStateFeature, state => state.subUser);

export const getProfileHeadOfficeSubUserError = createSelector(
  selectHeadOfficeSubUserStateFeature,
  state => state.subUserRequestError
);

export const getProfileSubUserPropertiesError = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.getSubUserPropertiesError
);

export const getProfileHeadOfficeSubUserRequestLoading = createSelector(
  selectHeadOfficeSubUserStateFeature,
  state => state.loading.subUserRequest
);

export const getPropertiesFormValue = createSelector(
  selectHeadOfficeSubUserStateFeature,
  state => state.propertiesFormValue
);

export const getProfilePropertyChanges = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.getPropertyChanges
);

export const canApplyPropertyChanges = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.canApplyPropertyChanges
);

export const hasPropertyValidationErrors = createSelector(selectHeadOfficeSubUserStateFeature, state =>
  Boolean(state.hasPropertyValidationErrors)
);

//Permissions
export const getOriginalProfilePermissions = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.getOriginalPermissions
);

export const getModifiedProfilePermissions = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.getModifiedPermissions
);

export const areProfilePermissionsLoading = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.getLoadingPermissions
);

export const canApplyProfilePermissionsChanges = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.hasChangesPermissions
);

export const getProfilePermissionsChanges = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.getChangesPermissions
);

export const getProfileSubUserPortalEmail = createSelector(
  selectHeadOfficeSubUserStateFeature,
  fromSubUserProfile.getSubUserPortalEmail
);
