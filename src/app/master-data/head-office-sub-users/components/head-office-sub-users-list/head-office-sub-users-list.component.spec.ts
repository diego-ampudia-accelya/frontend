import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration, MockModule } from 'ng-mocks';
import { of } from 'rxjs';

import { HeadOfficeSubUsersFilterFormatterService } from '../../services/head-office-sub-users-filter-formatter.service';
import { HeadOfficeSubUsersStoreFacadeService } from '../../store/head-office-sub-users-store-facade.service';
import { HeadOfficeSubUsersListComponent } from './head-office-sub-users-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { GridTableComponent, InputComponent } from '~app/shared/components';
import { DatepickerComponent } from '~app/shared/components/datepicker/datepicker.component';
import { InputSelectComponent } from '~app/shared/components/input-select/input-select.component';
import { ListViewComponent } from '~app/shared/components/list-view';

describe('HeadOfficeSubUsersListComponent', () => {
  let component: HeadOfficeSubUsersListComponent;
  let fixture: ComponentFixture<HeadOfficeSubUsersListComponent>;

  const permissionServiceMock = jasmine.createSpyObj<PermissionsService>('PermissionsService', [
    'hasPermission',
    'hasUserType'
  ]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MockModule(L10nTranslationModule), RouterTestingModule],
      declarations: [
        HeadOfficeSubUsersListComponent,
        MockDeclaration(ListViewComponent),
        MockDeclaration(GridTableComponent),
        MockDeclaration(InputComponent),
        MockDeclaration(InputSelectComponent),
        MockDeclaration(DatepickerComponent)
      ],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => {}
          }
        },
        {
          provide: HeadOfficeSubUsersFilterFormatterService,
          useValue: {
            format: () => {}
          }
        },
        {
          provide: HeadOfficeSubUsersStoreFacadeService,
          useValue: {
            actions: {
              searchHeadOfficeSubUser: () => {},
              downloadHeadOfficeSubUser: () => {}
            },
            selectors: {
              query$: of(null),
              data$: of(null),
              hasData$: of(null)
            },
            loading: {
              searchHeadOfficeSubUser$: of(null)
            }
          }
        },
        { provide: PermissionsService, useValue: permissionServiceMock }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadOfficeSubUsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
