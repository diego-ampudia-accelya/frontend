import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { HEAD_OFFICE_SUB_USERS } from '../../constants/head-office-sub-users.constants';
import { HeadOfficeSubUsersReactivateDeactivateDialogComponent } from '../../dialogs/head-office-sub-users-reactivate-deactivate-dialog/head-office-sub-users-reactivate-deactivate-dialog.component';
import {
  EAccess,
  EStatus,
  HeadOfficeSubUser,
  HeadOfficeSubUserFilters
} from '../../models/head-office-sub-users.models';
import { HeadOfficeSubUsersFilterFormatterService } from '../../services/head-office-sub-users-filter-formatter.service';
import { HeadOfficeSubUsersStoreFacadeService } from '../../store/head-office-sub-users-store-facade.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions, ROUTES } from '~app/shared/constants';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-head-office-sub-users-list',
  templateUrl: './head-office-sub-users-list.component.html'
})
export class HeadOfficeSubUsersListComponent implements OnInit, OnDestroy {
  public columns: TableColumn[] = HEAD_OFFICE_SUB_USERS.COLUMNS;

  public statusOptions = [
    {
      value: EStatus.ACTIVE,
      label: this.translationService.translate('head-office-sub-users.filters.status.option.ACTIVE')
    },
    {
      value: EStatus.INACTIVE,
      label: this.translationService.translate('head-office-sub-users.filters.status.option.INACTIVE')
    }
  ];

  public accessOptions = [
    {
      value: EAccess.R,
      label: this.translationService.translate('head-office-sub-users.filters.access.option.R')
    },
    {
      value: EAccess.RW,
      label: this.translationService.translate('head-office-sub-users.filters.access.option.RW')
    }
  ];

  public customLabels = { create: 'head-office-sub-users.createButtonLabel' };

  public data$: Observable<HeadOfficeSubUser[]> = this.headOfficeSubUsersStoreFacadeService.selectors.data$;
  public query$: Observable<DataQuery<HeadOfficeSubUserFilters>> =
    this.headOfficeSubUsersStoreFacadeService.selectors.query$;
  public loading$: Observable<boolean> = this.headOfficeSubUsersStoreFacadeService.loading.searchHeadOfficeSubUser$;
  public hasData$: Observable<boolean> = this.headOfficeSubUsersStoreFacadeService.selectors.hasData$;

  public canCreateHeadOfficeSubUser = this.permissionsService.hasPermission(Permissions.createHOMCSU);
  public actions: Array<{ action: GridTableActionType; disabled?: boolean; hidden?: boolean }>;

  public filterFormGroup: FormGroup;
  public dialogQueryFormSubmitEmitter = new Subject();

  private query: DataQuery<HeadOfficeSubUserFilters>;
  private destroy$ = new Subject();

  constructor(
    public readonly headOfficeSubUsersFilterFormatterService: HeadOfficeSubUsersFilterFormatterService,
    private readonly headOfficeSubUsersStoreFacadeService: HeadOfficeSubUsersStoreFacadeService,
    private readonly translationService: L10nTranslationService,
    private permissionsService: PermissionsService,
    private dialogService: DialogService,
    private router: Router
  ) {}

  public get title$(): Observable<string> {
    return this.query$.pipe(
      map(x => x?.paginateBy?.totalElements),
      map(length => `${this.translationService.translate('head-office-sub-users.view.title')} (${length || 0})`)
    );
  }

  public ngOnInit(): void {
    this.filterFormGroup = new FormGroup({
      portalEmail: new FormControl(null, []),
      name: new FormControl(null, []),
      parentName: new FormControl(null, []),
      registerDate: new FormControl(null, []),
      expiryDate: new FormControl(null, []),
      status: new FormControl(null, []),
      access: new FormControl(null, [])
    });

    this.initializeFormSubmitter();
  }

  public onQueryChanged(query: DataQuery<HeadOfficeSubUserFilters>): void {
    this.headOfficeSubUsersStoreFacadeService.actions.searchHeadOfficeSubUser(query);
  }

  public onDownload(): void {
    this.headOfficeSubUsersStoreFacadeService.actions.downloadHeadOfficeSubUser();
  }

  public createHeadOfficeSubUser(): void {
    this.headOfficeSubUsersStoreFacadeService.actions.openCreateHeadOfficeSubUserDialog();
  }
  public onActionMenuClick(subUser: HeadOfficeSubUser): void {
    const hasUpdatePermission = this.permissionsService.hasPermission(Permissions.updateHOMCSU);
    const hasReadPermission = this.permissionsService.hasPermission(Permissions.readHOMCSU);
    const hasReadPermissionsPermission = this.permissionsService.hasPermission(Permissions.readHOMCSUPermissions);
    const isCreatedByMainUser = subUser.level === 1;

    this.actions = [
      {
        action: GridTableActionType.DeactivateUser,
        hidden: subUser.status === EStatus.INACTIVE,
        disabled: !hasUpdatePermission || !isCreatedByMainUser
      },
      {
        action: GridTableActionType.ReactivateUser,
        hidden: subUser.status === EStatus.ACTIVE,
        disabled: !hasUpdatePermission || !isCreatedByMainUser
      },
      {
        action: GridTableActionType.UserProperties,
        disabled: !hasReadPermission || !isCreatedByMainUser
      },
      {
        action: GridTableActionType.PermissionAndTemplate,
        disabled: !hasReadPermissionsPermission || !isCreatedByMainUser
      }
    ];
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: HeadOfficeSubUser }): void {
    switch (action.actionType) {
      case GridTableActionType.DeactivateUser:
        this.openDeactivateReactivateDialog(this.getConfigDeactivateDialog(row));
        break;
      case GridTableActionType.ReactivateUser:
        this.openDeactivateReactivateDialog(this.getConfigReactivateDialog(row));
        break;
      case GridTableActionType.UserProperties:
        this.router.navigate([ROUTES.HOMC_SUB_USER_DETAILS.url, row.id]);
        break;
      case GridTableActionType.PermissionAndTemplate:
        this.router.navigate([ROUTES.HOMC_SUB_USER_DETAILS.url, row.id, 'permissions']);
        break;
    }
  }

  private getConfigDeactivateDialog(item: HeadOfficeSubUser): DialogConfig {
    return {
      data: {
        actionEmitter: this.dialogQueryFormSubmitEmitter,
        item,
        title: this.translationService.translate('head-office-sub-users.deactivateTitle'),
        footerButtonsType: FooterButton.Deactivate,
        isDeactivateMode: true
      }
    };
  }

  private openDeactivateReactivateDialog(config: DialogConfig) {
    this.dialogService.open(HeadOfficeSubUsersReactivateDeactivateDialogComponent, config);
  }

  private getConfigReactivateDialog(item: HeadOfficeSubUser): DialogConfig {
    return {
      data: {
        actionEmitter: this.dialogQueryFormSubmitEmitter,
        item,
        title: this.translationService.translate('head-office-sub-users.reactivateTitle'),
        footerButtonsType: FooterButton.Reactivate,
        isDeactivateMode: false
      }
    };
  }

  private initializeFormSubmitter(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.query = query));
    this.dialogQueryFormSubmitEmitter
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.headOfficeSubUsersStoreFacadeService.actions.searchHeadOfficeSubUser(this.query));
  }

  public onRowLinkClick(event: any) {
    if (event.level === 1) {
      this.router.navigate([ROUTES.HOMC_SUB_USER_DETAILS.url, event.id]);
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
