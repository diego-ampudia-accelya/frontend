import { EAccess, EStatus, HeadOfficeSubUser } from '../models/head-office-sub-users.models';
import { PagedData } from '~app/shared/models/paged-data.model';

export const headOfficeSubUsers: HeadOfficeSubUser[] = [
  {
    id: 99997,
    portalEmail: 'mux@iata.org',
    level: 1,
    name: 'otra0324',
    parentName: '22=MCMUALL',
    registerDate: '2013-01-03',
    expiryDate: '2022-07-29',
    numberOfUsers: 2,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'telefono',
      country: 'country',
      city: '',
      address: 'direccion',
      postCode: 'direccion',
      email: 'mux@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 999961,
    portalEmail: null,
    level: 2,
    name: 'sdf',
    parentName: 'otra0324',
    registerDate: '2017-11-02',
    expiryDate: '2017-12-12',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'asdf',
      country: 'Antigua y Barbuda aa',
      city: '',
      address: 'asdfsdfdsf',
      postCode: 'asdfasd',
      email: 'a@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 99999,
    portalEmail: 'flups@iata.org',
    level: 1,
    name: '=CHX655090',
    parentName: '22=MCMUALL',
    registerDate: '2013-01-14',
    expiryDate: '2022-07-29',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'postcode',
      country: 'SOLO LECTURA',
      city: '',
      address: 'direccion',
      postCode: 'postcode',
      email: 'flups@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 999913,
    portalEmail: 'mux@iata.org',
    level: 1,
    name: '=ESX892793',
    parentName: '22=MCMUALL',
    registerDate: '2013-03-01',
    expiryDate: '2023-02-13',
    numberOfUsers: 2,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'HIJOMCMU',
      country: 'HIJOMCMU',
      city: '',
      address: 'HIJOMCMU',
      postCode: 'v',
      email: 'mux@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 999941,
    portalEmail: null,
    level: 2,
    name: '=ESX541602',
    parentName: '=ESX892793',
    registerDate: '2013-07-31',
    expiryDate: '2017-09-12',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'HIJOESX892793',
      country: 'HIJOESX892793',
      city: '',
      address: 'HIJOESX892793',
      postCode: 'HIJOESX892',
      email: 'a@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 999935,
    portalEmail: null,
    level: 1,
    name: '=ESX864751',
    parentName: '22=MCMUALL',
    registerDate: '2013-07-30',
    expiryDate: '2023-01-02',
    numberOfUsers: 2,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'HIJO ESX150323',
      country: 'HIJO ESX150323',
      city: '',
      address: 'HIJO ESX150323',
      postCode: 'HIJO ESX15',
      email: 'a@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 9999157,
    portalEmail: 'x@iata.org',
    level: 2,
    name: 'xs',
    parentName: '=ESX864751',
    registerDate: '2022-12-13',
    expiryDate: '2023-01-02',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'xs',
      country: 'xs',
      city: '',
      address: 'xs',
      postCode: 'xs',
      email: 'x@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 999957,
    portalEmail: 'u@iata.org',
    level: 1,
    name: 'MCMUALLh3',
    parentName: '22=MCMUALL',
    registerDate: '2016-02-08',
    expiryDate: '2022-07-29',
    numberOfUsers: 2,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'HIJO3',
      country: 'HIJO3',
      city: '',
      address: 'HIJO3',
      postCode: 'HIJO3',
      email: 'u@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 999960,
    portalEmail: 'a@b',
    level: 2,
    name: 'hijoMCMU',
    parentName: 'MCMUALLh3',
    registerDate: '2017-07-12',
    expiryDate: '2017-09-12',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'v',
      country: 'hijoMCMU',
      city: '',
      address: 'hijoMCMU',
      postCode: 'hijoMCMU',
      email: 'a@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 9999138,
    portalEmail: 'mux@iata.org',
    level: 1,
    name: 'Martin Sautter',
    parentName: '22=MCMUALL',
    registerDate: '2022-06-17',
    expiryDate: '2022-07-18',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: '3653465',
      country: 'Turqu�a',
      city: '',
      address: '534534',
      postCode: '28031',
      email: 'mux@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 9999143,
    portalEmail: 'xq@iata.org',
    level: 1,
    name: 'xq',
    parentName: '22=MCMUALL',
    registerDate: '2022-06-24',
    expiryDate: '2022-11-28',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'x',
      country: 'x',
      city: '',
      address: 'x',
      postCode: 'x',
      email: 'xq@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 9999146,
    portalEmail: 'xxs@iata.org',
    level: 1,
    name: 'Martin Sautter',
    parentName: '22=MCMUALL',
    registerDate: '2022-06-27',
    expiryDate: '2022-07-29',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: '5',
      country: 'x',
      city: '',
      address: 'gf',
      postCode: '28031',
      email: 'xxs@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 9999150,
    portalEmail: 'mcx@iata.org',
    level: 1,
    name: 'mcx',
    parentName: '22=MCMUALL',
    registerDate: '2022-09-05',
    expiryDate: '2022-11-15',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'x',
      country: 'x',
      city: '',
      address: 'x',
      postCode: 'x',
      email: 'mcx@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 9999151,
    portalEmail: 'mcz@iata.org',
    level: 1,
    name: 'mcz',
    parentName: '22=MCMUALL',
    registerDate: '2022-09-05',
    expiryDate: '2022-11-15',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'z',
      country: 'z',
      city: '',
      address: 'z',
      postCode: 'z',
      email: 'mcz@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 9999155,
    portalEmail: 'x@iata.org',
    level: 1,
    name: 'x',
    parentName: '22=MCMUALL',
    registerDate: '2022-12-13',
    expiryDate: '2023-02-13',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'x',
      country: 'x',
      city: '',
      address: 'x',
      postCode: 'x',
      email: 'x@iata.org'
    },
    access: EAccess.R
  },
  {
    id: 9999156,
    portalEmail: 'x@iata.org',
    level: 1,
    name: 'x2',
    parentName: '22=MCMUALL',
    registerDate: '2022-12-13',
    expiryDate: '2023-02-13',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: 'x2',
      country: 'x2',
      city: '',
      address: 'x2',
      postCode: 'x2',
      email: 'x@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 9999162,
    portalEmail: 'test-nfe@iata.org',
    level: 1,
    name: '623Airline',
    parentName: '22=MCMUALL',
    registerDate: '2023-01-05',
    expiryDate: '2023-02-13',
    numberOfUsers: 1,
    status: EStatus.INACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: '12345789',
      country: 'ES',
      city: '',
      address: 'address',
      postCode: '1234',
      email: 'test-nfe@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 9999164,
    portalEmail: 'nfe-test@iata.org',
    level: 1,
    name: 'MCSKY123',
    parentName: '22=MCMUALL',
    registerDate: '2023-01-18',
    expiryDate: null,
    numberOfUsers: 1,
    status: EStatus.ACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'IATA',
      telephone: '2657',
      country: 'MT',
      city: '',
      address: 'MT',
      postCode: '8895',
      email: 'nfe-test@iata.org'
    },
    access: EAccess.RW
  },
  {
    id: 9999166,
    portalEmail: 'nfe-test@iata.org',
    level: 1,
    name: 'MTSKYMULTY',
    parentName: '22=MCMUALL',
    registerDate: '2023-01-19',
    expiryDate: null,
    numberOfUsers: 1,
    status: EStatus.ACTIVE,
    userType: 'IATA',
    contactInfo: {
      organization: 'Skywalkers',
      telephone: '123456789',
      country: 'MT',
      city: '',
      address: 'address',
      postCode: '8895',
      email: 'nfe-test@iata.org'
    },
    access: EAccess.RW
  }
];

export const headOfficeSubUsersPagedData: PagedData<HeadOfficeSubUser> = {
  pageNumber: 0,
  pageSize: 20,
  total: 83,
  totalPages: 5,
  records: headOfficeSubUsers
};
