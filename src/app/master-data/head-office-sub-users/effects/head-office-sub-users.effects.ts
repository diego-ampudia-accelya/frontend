import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

import * as fromPermissionActions from '../actions/head-office-sub-users-permissions.actions';
import * as fromActions from '../actions/head-office-sub-users.actions';
import { ChangesDialogService } from '../dialogs/head-office-sub-users-changes-dialog/services/changes-dialog.service';
import { HeadOfficeSubUserUpdateModel } from '../models/head-office-sub-users.models';
import { mapSelectionListToPermissions } from '../reducers/head-office-sub-users.reducer';
import * as fromSubUsers from '../selectors/head-office-sub-users.selectors';
import { HeadOfficeSubUsersService } from '../services/head-office-sub-users.service';
import { SubUserHomcChangesService } from '../shared/sub-user-homc-changes.service';
import { SubUserHomcPropertyChangesService } from '../shared/sub-user-homc-property-changes.service';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';

@Injectable()
export class HeadOfficeSubUserEffects {
  public searchHeadOfficeSubUserEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.searchHeadOfficeSubUserRequest>>(fromActions.searchHeadOfficeSubUserRequest),
      switchMap(({ payload: { query } }) =>
        this.headOfficeSubUsersService.find(query).pipe(
          map(data =>
            fromActions.searchHeadOfficeSubUserSuccess({
              payload: { pagedData: data }
            })
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromActions.searchHeadOfficeSubUserFailure({
                payload: { error }
              })
            )
          )
        )
      )
    )
  );

  public downloadHeadOfficeSubUserEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.downloadHeadOfficeSubUserRequest>>(
          fromActions.downloadHeadOfficeSubUserRequest
        ),
        withLatestFrom(this.store.pipe(select(fromSubUsers.selectHeadOfficeSubUserQuery))),
        tap(([_, query]) => {
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.headOfficeSubUsersService
          });
        })
      ),
    { dispatch: false }
  );

  public $openAdd = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.openAdd>>(fromActions.openAdd),
        switchMap(() => this.changesDialogService.open())
      ),
    { dispatch: false }
  );

  public add$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.add>>(fromActions.add),
      switchMap(({ value }) =>
        this.changesDialogService.create(value).pipe(
          map(() => fromActions.addSuccess()),
          rethrowError(error => fromActions.addError(error))
        )
      )
    )
  );

  public addSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.addSuccess>>(fromActions.addSuccess),
        tap(() => {
          this.dialogService.close();
          const successMessage = this.translationService.translate('head-office-sub-users.createMessage');
          this.notificationService.showSuccess(successMessage);
        })
      ),
    { dispatch: false }
  );

  public $requestSubUser = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.requestSubUser>>(fromActions.requestSubUser),
      switchMap(({ id }) => this.headOfficeSubUsersService.getBy(id)),
      map(subUser => fromActions.requestSubUserSuccess({ subUser })),
      rethrowError(error => fromActions.requestSubUserError({ error }))
    )
  );

  $openPropertyChangesDialog = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.applyPropertyChanges>>(fromActions.applyPropertyChanges),
      switchMap(() => this.getSubUserUpdateData()),
      map(data => (data ? fromActions.updateSubUserProperties({ data }) : fromActions.cancelPropertyChanges()))
    )
  );

  $updateSubUserProperties = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromActions.updateSubUserProperties>>(fromActions.updateSubUserProperties),
      switchMap(({ data }) => this.headOfficeSubUsersService.update(data)),
      map(subUser => fromActions.updateSubUserPropertiesSuccess({ subUser })),
      rethrowError(error => fromActions.updateSubUserPropertiesError({ error }))
    )
  );

  $updateSubUserPropertiesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromActions.updateSubUserPropertiesSuccess>>(
          fromActions.updateSubUserPropertiesSuccess
        ),
        tap(({ subUser }) => {
          const successMessage = this.translationService.translate('subUsers.editUser.editUserSuccess', {
            email: `<span class="link">${subUser.portalEmail}</span>`
          });

          this.notificationService.showSuccess(successMessage);
        })
      ),
    { dispatch: false }
  );

  requestProfilePermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromPermissionActions.requestPermissions>>(fromPermissionActions.requestPermissions),
      withLatestFrom(this.store.pipe(select(fromSubUsers.getProfileHeadOfficeSubUser))),
      switchMap(([_, subUser]) =>
        this.headOfficeSubUsersService.getPermissionsBy(subUser.id).pipe(
          map(permissions =>
            fromPermissionActions.requestPermissionsSuccess({
              permissions,
              subUser
            })
          )
        )
      ),
      rethrowError(() => fromPermissionActions.requestPermissionsError())
    )
  );

  openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromPermissionActions.openApplyChanges>>(fromPermissionActions.openApplyChanges),
      switchMap(() => this.changesService.applyUserPermissions$)
    )
  );

  applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType<ReturnType<typeof fromPermissionActions.applyChanges>>(fromPermissionActions.applyChanges),
      withLatestFrom(
        this.store.pipe(select(fromSubUsers.getModifiedProfilePermissions)),
        this.store.pipe(select(fromSubUsers.getProfileHeadOfficeSubUser))
      ),
      switchMap(([_, modified, subUser]) =>
        this.headOfficeSubUsersService
          .updatePermissionsByUserId(subUser.id, mapSelectionListToPermissions(modified))
          .pipe(map(permissions => fromPermissionActions.applyChangesSuccess({ permissions, subUser })))
      ),
      rethrowError(() => fromPermissionActions.applyChangesError())
    )
  );

  $applyChangesSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType<ReturnType<typeof fromPermissionActions.applyChangesSuccess>>(fromPermissionActions.applyChangesSuccess),
        withLatestFrom(this.store.pipe(select(fromSubUsers.getProfileSubUserPortalEmail))),
        tap(([_, email]) => {
          const message = this.translationService.translate('subUsers.editUser.editUserSuccess', {
            email: `<span class="link">${email}</span>`
          });
          this.notificationService.showSuccess(message);
        })
      ),
    {
      dispatch: false
    }
  );

  public constructor(
    private readonly actions$: Actions,
    private readonly headOfficeSubUsersService: HeadOfficeSubUsersService,
    private readonly store: Store<unknown>,
    private readonly dialogService: DialogService,
    private changesDialogService: ChangesDialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private subUserHomcPropertyChangesService: SubUserHomcPropertyChangesService,
    private changesService: SubUserHomcChangesService
  ) {}

  private getSubUserUpdateData(): Observable<HeadOfficeSubUserUpdateModel> {
    return this.subUserHomcPropertyChangesService
      .confirmApplyPropertyChanges()
      .pipe(
        switchMap(confirmed =>
          confirmed ? this.store.pipe(select(fromSubUsers.getPropertiesFormValue), take(1)) : of(null)
        )
      );
  }
}
