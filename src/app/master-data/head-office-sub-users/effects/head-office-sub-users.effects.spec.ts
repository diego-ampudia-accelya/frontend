import { HttpErrorResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { cold, hot } from 'jasmine-marbles';
import { MessageService } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

import * as fromActions from '../actions/head-office-sub-users.actions';
import { ChangesDialogConfigService } from '../dialogs/head-office-sub-users-changes-dialog/services/changes-dialog-config.service';
import { ChangesDialogService } from '../dialogs/head-office-sub-users-changes-dialog/services/changes-dialog.service';
import * as fromSelectors from '../selectors/head-office-sub-users.selectors';
import { HeadOfficeSubUsersService } from '../services/head-office-sub-users.service';
import * as fromState from '../store/head-office-sub-users.state';
import * as fromStub from '../stubs/head-office-sub-users.stubs';

import { HeadOfficeSubUserEffects } from './head-office-sub-users.effects';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('HeadOfficeSubUserEffects', () => {
  let actions$: Observable<any>;
  let effects: HeadOfficeSubUserEffects;
  let service: HeadOfficeSubUsersService;
  let dialogService: DialogService;

  const initialState = {
    [fromState.headOfficeSubUserFeatureKey]: fromState.initialState
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        ChangesDialogService,
        ChangesDialogConfigService,
        HeadOfficeSubUserEffects,
        L10nTranslationService,
        mockProvider(MessageService),
        provideMockActions(() => actions$),
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: fromSelectors.selectHeadOfficeSubUserQuery,
              value: defaultQuery
            }
          ]
        }),
        {
          provide: HeadOfficeSubUsersService,
          useValue: {
            find: () => {},
            download: () => {}
          }
        },
        {
          provide: DialogService,
          useValue: {
            open: () => {}
          }
        }
      ]
    });

    effects = TestBed.inject<HeadOfficeSubUserEffects>(HeadOfficeSubUserEffects);
    service = TestBed.inject<HeadOfficeSubUsersService>(HeadOfficeSubUsersService);
    dialogService = TestBed.inject<DialogService>(DialogService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('searchHeadOfficeSubUserEffect', () => {
    it('should return searchHeadOfficeSubUserSuccess with paged data', () => {
      const action = fromActions.searchHeadOfficeSubUserRequest({ payload: { query: defaultQuery } });

      const actionSuccess = fromActions.searchHeadOfficeSubUserSuccess({
        payload: {
          pagedData: fromStub.headOfficeSubUsersPagedData
        }
      });

      actions$ = hot('-a', { a: action });

      const response = cold('-a|', { a: fromStub.headOfficeSubUsersPagedData });
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionSuccess });
      expect(effects.searchHeadOfficeSubUserEffect$).toBeObservable(expected);
    });

    it('should return searchHeadOfficeSubUserFailure when credit card service find fails', () => {
      const action = fromActions.searchHeadOfficeSubUserRequest({ payload: { query: defaultQuery } });
      const error = new HttpErrorResponse({ error: 'mock error' });
      const actionError = fromActions.searchHeadOfficeSubUserFailure({ payload: { error } });

      actions$ = hot('-a', { a: action });

      const response = cold('-#|', {}, error);
      service.find = jasmine.createSpy().and.returnValue(response);

      const expected = cold('--b', { b: actionError });
      expect(effects.searchHeadOfficeSubUserEffect$).toBeObservable(expected);
    });
  });

  describe('downloadHeadOfficeSubUserEffect', () => {
    it('when downloadHeadOfficeSubUserRequest is called should open download dialog', fakeAsync(() => {
      const action = fromActions.downloadHeadOfficeSubUserRequest();

      spyOn(dialogService, 'open');

      actions$ = new BehaviorSubject<unknown>(action);

      effects.downloadHeadOfficeSubUserEffect$.subscribe();

      tick();

      expect(dialogService.open).toHaveBeenCalledWith(DownloadFileComponent, {
        data: {
          title: 'BUTTON.DEFAULT.DOWNLOAD',
          footerButtonsType: FooterButton.Download,
          downloadQuery: {
            ...defaultQuery,
            filterBy: {
              ...defaultQuery?.filterBy
            }
          }
        },
        apiService: service
      });
    }));
  });
});
