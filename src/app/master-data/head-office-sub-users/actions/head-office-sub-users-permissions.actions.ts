import { createAction, props } from '@ngrx/store';

import { HeadOfficeSubUser } from '../models/head-office-sub-users.models';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { UserPermission } from '~app/sub-users/shared/permissions.models';

export const requestPermissions = createAction('[HeadOfficeSubUser Profile Permissions] Request');

export const requestPermissionsSuccess = createAction(
  '[HeadOfficeSubUser Profile Permissions] Request Success',
  props<{ permissions: UserPermission[]; subUser: HeadOfficeSubUser }>()
);
export const requestPermissionsError = createAction('[HeadOfficeSubUser Profile Permissions] Request Error');

export const change = createAction(
  '[HeadOfficeSubUser Profile Permissions] Change',
  props<{ modifications: SelectionList[] }>()
);

export const openApplyChanges = createAction('[HeadOfficeSubUser Profile Permissions] Open Apply Changes');

export const applyChanges = createAction('[HeadOfficeSubUser Profile Permissions] Apply Changes');

export const applyChangesSuccess = createAction(
  '[HeadOfficeSubUser Profile Permissions] Apply Changes Success',
  props<{ permissions: UserPermission[]; subUser: HeadOfficeSubUser }>()
);
export const applyChangesError = createAction('[HeadOfficeSubUser Profile Permissions] Apply Changes Error');

export const discardChanges = createAction('[HeadOfficeSubUser Profile Permissions] Discard Changes');

export const cancel = createAction('[HeadOfficeSubUser Profile Permissions] Cancel');
