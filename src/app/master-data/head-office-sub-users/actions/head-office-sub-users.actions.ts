import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

import {
  EStatus,
  HeadOfficeSubUser,
  HeadOfficeSubUserFilters,
  HeadOfficeSubUserUpdateModel
} from '../models/head-office-sub-users.models';
import { DataQuery } from '~app/shared/components/list-view';
import { ServerError } from '~app/shared/errors';
import { PagedData } from '~app/shared/models/paged-data.model';

export const searchHeadOfficeSubUserRequest = createAction(
  '[HeadOfficeSubUsers] Search Head Office Sub Users Request',
  props<{ payload: { query?: DataQuery<HeadOfficeSubUserFilters> } }>()
);

export const searchHeadOfficeSubUserSuccess = createAction(
  '[HeadOfficeSubUsers] Search Head Office Sub Users Success',
  props<{ payload: { pagedData: PagedData<HeadOfficeSubUser> } }>()
);

export const searchHeadOfficeSubUserFailure = createAction(
  '[HeadOfficeSubUsers] Search Head Office Sub Users Failure',
  props<{ payload: { error: HttpErrorResponse } }>()
);

export const downloadHeadOfficeSubUserRequest = createAction(
  '[HeadOfficeSubUsers] Download Head Office Sub Users Request'
);

export const openAdd = createAction('[[HeadOfficeSubUsers] Open Add');

export const add = createAction('[HeadOfficeSubUsers] Add', props<{ value: Partial<HeadOfficeSubUser> }>());

export const addSuccess = createAction('[HeadOfficeSubUsers] Add Success');

export const addError = createAction('[HeadOfficeSubUsers] Add Error', props<{ error }>());

export const requestSubUser = createAction(
  '[HeadOfficeSubUsers] Request Head Office Sub User',
  props<{ id: number }>()
);

export const requestSubUserSuccess = createAction(
  '[HeadOfficeSubUsers] Request Head Office Sub User Success',
  props<{ subUser: HeadOfficeSubUser }>()
);

export const requestSubUserError = createAction(
  '[HeadOfficeSubUsers] Request Head Office Sub User Error',
  props<{ error: ServerError }>()
);

export const propertiesFormValueChange = createAction(
  '[HeadOfficeSubUsers] Properties Form Value Change',
  props<{ formValue: Partial<HeadOfficeSubUserUpdateModel>; isFormValid: boolean }>()
);

export const applyPropertyChanges = createAction('[HeadOfficeSubUsers] Apply Property Changes');

export const discardPropertyChanges = createAction('[HeadOfficeSubUsers] Discard Property Changes');

export const cancelPropertyChanges = createAction('[HeadOfficeSubUsers] Cancel Property Changes');

export const updateSubUserProperties = createAction(
  '[HeadOfficeSubUsers] Update Head Office Sub User Properties',
  props<{ data: Partial<HeadOfficeSubUserUpdateModel> }>()
);

export const updateSubUserPropertiesSuccess = createAction(
  '[HeadOfficeSubUsers] Update Head Office Sub User Properties Success',
  props<{ subUser: HeadOfficeSubUser }>()
);

export const updateSubUserPropertiesError = createAction(
  '[HeadOfficeSubUsers] Update Head Office Sub User Properties Error',
  props<{ error: ServerError }>()
);

export const changeSubUserStatus = createAction(
  '[HeadOfficeSubUsers] Change Head Office Sub User Status',
  props<{ status: EStatus }>()
);
