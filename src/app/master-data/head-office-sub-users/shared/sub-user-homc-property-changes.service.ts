import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { forkJoin, Observable } from 'rxjs';
import { filter, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

import * as fromSubUsers from '../selectors/head-office-sub-users.selectors';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ChangeModel,
  ChangesDialogComponent,
  ChangesDialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import { SubUsersProfileActions } from '~app/sub-users/actions';

@Injectable({
  providedIn: 'root'
})
export class SubUserHomcPropertyChangesService {
  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {}

  private changedProperties$: Observable<ChangeModel[]> = this.store.pipe(
    select(fromSubUsers.getProfilePropertyChanges),
    take(1)
  );

  private hasValidationErrors$: Observable<boolean> = this.store.pipe(
    select(fromSubUsers.hasPropertyValidationErrors),
    take(1)
  );

  private propertiesFormValue$ = this.store.pipe(select(fromSubUsers.getPropertiesFormValue), take(1));

  private openPropertyChangesDialog(dialogConfig: ChangesDialogConfig): Observable<FooterButton> {
    return forkJoin([this.changedProperties$, this.hasValidationErrors$]).pipe(
      switchMap(([changesNoFormat, hasInvalidChanges]) => {
        const changes = this.formatChanges(changesNoFormat);

        return this.dialogService.open(ChangesDialogComponent, { ...dialogConfig, changes, hasInvalidChanges });
      }),
      filter(action => action?.clickedBtn && action.contentComponentRef),
      tap(({ contentComponentRef }) => contentComponentRef.config?.serviceRef?.close()),
      map(({ clickedBtn }) => clickedBtn)
    );
  }

  public confirmApplyPropertyChanges(): Observable<boolean> {
    const footerButtonsType = FooterButton.Apply;
    const dialogConfig = {
      data: {
        title: 'subUsers.editUser.titleApply',
        footerButtonsType: [{ type: footerButtonsType }]
      },
      message: 'subUsers.editUser.descriptionFormApply',
      changes: [],
      usePillsForChanges: true
    };

    return this.openPropertyChangesDialog(dialogConfig).pipe(map(clickedBtn => clickedBtn === FooterButton.Apply));
  }

  public getDeactivateAction(): Observable<boolean> {
    const dialogConfig = {
      data: {
        title: 'subUsers.editUser.titleUnsaved',
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      message: 'subUsers.editUser.descriptionUnsavedProperties',
      invalidChangesMessage: 'subUsers.editUser.invalidWarning',
      changes: [],
      usePillsForChanges: true
    };

    return this.handleDeactivationChange(dialogConfig).pipe(
      tap(action => this.store.dispatch(action)),
      map(({ type }) => type !== SubUsersProfileActions.cancelPropertyChanges.type)
    );
  }

  private handleDeactivationChange(dialogConfig: ChangesDialogConfig): Observable<Action> {
    return this.openPropertyChangesDialog(dialogConfig).pipe(
      withLatestFrom(this.propertiesFormValue$),
      map(([clickedBtn, data]) => {
        switch (clickedBtn) {
          case FooterButton.Apply:
            return SubUsersProfileActions.updateSubUserProperties({ data });
          case FooterButton.Discard:
            return SubUsersProfileActions.discardPropertyChanges();
          default:
            return SubUsersProfileActions.cancelPropertyChanges();
        }
      })
    );
  }

  private formatChanges(modifications: ChangeModel[]): ChangeModel[] {
    return modifications.map(item => ({
      group: this.formatLabel(item.group),
      name: this.formatItem(item),
      value: item.value,
      tooltip: this.formatItem(item)
    }));
  }

  private formatLabel(value): string {
    return this.translationService.translate(`subUsers.profile.propertiesTab.labels.${value}`);
  }

  private formatBoolean(value): string {
    const setBooleanValue = value ? 'enabled' : 'disabled';

    return this.translationService.translate(`subUsers.editUser.${setBooleanValue}`);
  }

  private formatItem(item): string {
    let result;

    if (typeof item.value === 'string') {
      result = `${this.formatLabel(item.name)}:  ${item.value}`;
    }

    if (typeof item.value === 'boolean') {
      result = `${this.formatLabel(item.name)}:  ${this.formatBoolean(item.value)}`;
    }

    if (typeof item.value === 'object' && item.value) {
      result = `${item.value.isoCountryCode}: ${item.value.name}, ${this.formatBoolean(item.value.access)}`;
    }

    return result;
  }
}
