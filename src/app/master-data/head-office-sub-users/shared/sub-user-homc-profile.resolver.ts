import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { merge, Observable } from 'rxjs';
import { filter, first, tap } from 'rxjs/operators';

import { HeadOfficeSubUser } from '../models/head-office-sub-users.models';
import { HeadOfficeSubUsersStoreFacadeService } from '../store/head-office-sub-users-store-facade.service';

@Injectable({
  providedIn: 'root'
})
export class SubUserHomcProfileResolver implements Resolve<HeadOfficeSubUser> {
  constructor(
    private router: Router,
    private readonly headOfficeSubUsersStoreFacadeService: HeadOfficeSubUsersStoreFacadeService
  ) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<HeadOfficeSubUser> {
    this.requestSubUser(route);

    const subUser$ = this.headOfficeSubUsersStoreFacadeService.selectors.getProfileHeadOfficeSubUser$.pipe(
      filter(Boolean)
    );
    const subUserError$ = this.headOfficeSubUsersStoreFacadeService.selectors.getProfileHeadOfficeSubUserError$.pipe(
      filter(Boolean),
      tap(() => this.handleNavigationOnError())
    );

    return merge(subUserError$, subUser$).pipe(first()) as Observable<HeadOfficeSubUser>;
  }

  private requestSubUser(route: ActivatedRouteSnapshot) {
    const id = Number(route.paramMap.get('id'));

    if (id) {
      this.headOfficeSubUsersStoreFacadeService.actions.requestSubUser(id);
    }
  }

  private handleNavigationOnError() {
    const lastSuccessfulNavigation = this.router.getCurrentNavigation().previousNavigation;
    if (lastSuccessfulNavigation != null) {
      this.router.navigateByUrl(lastSuccessfulNavigation.finalUrl.toString());
    } else {
      this.router.navigate(['./']);
    }
  }
}
