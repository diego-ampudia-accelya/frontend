/* eslint-disable @typescript-eslint/naming-convention */
import { GridColumn } from '~app/shared/models';

export const HEAD_OFFICE_SUB_USERS = {
  get COLUMNS(): Array<GridColumn> {
    return [
      {
        prop: 'portalEmail',
        name: 'head-office-sub-users.columns.portalEmail',
        resizeable: false,
        draggable: false,
        cellTemplate: 'commonLinkFromObjCellTmpl'
      },
      {
        prop: 'level',
        name: 'head-office-sub-users.columns.level',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'name',
        name: 'head-office-sub-users.columns.name',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'parentName',
        name: 'head-office-sub-users.columns.parentName',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'registerDate',
        name: 'head-office-sub-users.columns.registerDate',
        resizeable: false,
        draggable: false,
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        prop: 'expiryDate',
        name: 'head-office-sub-users.columns.expiryDate',
        resizeable: true,
        draggable: false,
        cellTemplate: 'dayMonthYearCellTmpl'
      },
      {
        prop: 'numberOfUsers',
        name: 'head-office-sub-users.columns.numberOfUsers',
        resizeable: true,
        draggable: false
      },
      {
        prop: 'status',
        name: 'head-office-sub-users.columns.status',
        resizeable: true,
        draggable: false,
        cellTemplate: 'statusStringTmpl'
      },
      {
        prop: 'access',
        name: 'head-office-sub-users.columns.access',
        resizeable: true,
        draggable: false,
        cellTemplate: 'readWriteTmpl'
      }
    ];
  }
};
