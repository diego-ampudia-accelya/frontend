import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { HeadOfficeSubUsersService } from '../../services/head-office-sub-users.service';
import { HeadOfficeSubUsersStoreFacadeService } from '../../store/head-office-sub-users-store-facade.service';
import { initialState } from '../../store/head-office-sub-users.state';
import { HeadOfficeSubUsersReactivateDeactivateDialogComponent } from './head-office-sub-users-reactivate-deactivate-dialog.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { TranslatePipeMock } from '~app/test';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Deactivate,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel'
      },
      {
        title: 'BUTTON.DEFAULT.DEACTIVATE',
        buttonDesign: 'primary',
        type: 'deactivate',
        isDisabled: true
      }
    ],
    item: {
      primaryReason: 'primary Reason',
      primaryReasonId: 'id',
      subReason: 'sub Reason',
      active: true
    }
  }
} as DialogConfig;

describe('HeadOfficeSubUsersReactivateDeactivateDialogComponent', () => {
  let component: HeadOfficeSubUsersReactivateDeactivateDialogComponent;
  let fixture: ComponentFixture<HeadOfficeSubUsersReactivateDeactivateDialogComponent>;
  let dialogServiceSpy: SpyObject<DialogService>;
  let headOfficeSubUsersServiceSpy: SpyObject<HeadOfficeSubUsersService>;
  const notificationServiceSpy = createSpyObject(NotificationService);

  beforeEach(waitForAsync(() => {
    dialogServiceSpy = createSpyObject(DialogService);
    headOfficeSubUsersServiceSpy = createSpyObject(HeadOfficeSubUsersService);

    TestBed.configureTestingModule({
      declarations: [HeadOfficeSubUsersReactivateDeactivateDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        mockProvider(L10nTranslationService),
        mockProvider(LocalBspTimeService),
        FormBuilder,
        ReactiveSubject,
        HeadOfficeSubUsersStoreFacadeService,
        HttpClient,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: HeadOfficeSubUsersService, useValue: headOfficeSubUsersServiceSpy },
        provideMockStore({ initialState })
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadOfficeSubUsersReactivateDeactivateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should call correct functions on Init', () => {
    const initializeConfigFeaturesSpy = spyOn<any>(component, 'initializeConfigFeatures');
    const buildFormSpy = spyOn<any>(component, 'buildForm');
    const initializeFormFeaturesSpy = spyOn<any>(component, 'initializeFormFeatures');
    const initializeButtonsSpy = spyOn<any>(component, 'initializeButtons');
    const initializeButtonsListenersSpy = spyOn<any>(component, 'initializeButtonsListeners');

    component.ngOnInit();

    expect(initializeConfigFeaturesSpy).toHaveBeenCalled();
    expect(buildFormSpy).toHaveBeenCalled();
    expect(initializeFormFeaturesSpy).toHaveBeenCalled();
    expect(initializeButtonsSpy).toHaveBeenCalled();
    expect(initializeButtonsListenersSpy).toHaveBeenCalled();
  });
});
