import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';

import { EStatus, HeadOfficeSubUser } from '../../models/head-office-sub-users.models';
import { HeadOfficeSubUsersService } from '../../services/head-office-sub-users.service';
import { HeadOfficeSubUsersStoreFacadeService } from '../../store/head-office-sub-users-store-facade.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { NotificationService } from '~app/shared/services/notification.service';

@Component({
  selector: 'bspl-head-office-sub-users-reactivate-deactivate-dialog-dialog',
  templateUrl: './head-office-sub-users-reactivate-deactivate-dialog.component.html',
  styleUrls: ['./head-office-sub-users-reactivate-deactivate-dialog.component.scss']
})
export class HeadOfficeSubUsersReactivateDeactivateDialogComponent implements AfterViewInit, OnInit, OnDestroy {
  public form: FormGroup;
  public isDeactivateMode: boolean;
  public item: HeadOfficeSubUser;

  private portalEmailControl: AbstractControl;
  private nameControl: AbstractControl;
  private expiryDateControl: AbstractControl;

  private formFactory: FormUtil;
  private deactivateButton: ModalAction;
  private reactivateButton: ModalAction;

  private actionEmitter = new Subject();
  private destroy$ = new Subject();

  private deactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Deactivate),
    takeUntil(this.destroy$)
  );

  private reactivateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Reactivate),
    takeUntil(this.destroy$)
  );

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private headOfficeSubUsersService: HeadOfficeSubUsersService,
    private fb: FormBuilder,
    private localBspTimeService: LocalBspTimeService,
    private readonly headOfficeSubUsersStoreFacadeService: HeadOfficeSubUsersStoreFacadeService
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.initializeConfigFeatures();
    this.buildForm();
    this.initializeFormFeatures();

    this.initializeButtons();
    this.initializeButtonsListeners();
  }

  public ngAfterViewInit(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (this.deactivateButton) {
        this.deactivateButton.isDisabled = status !== 'VALID';
      }
    });
  }

  private initializeButtons(): void {
    this.deactivateButton = this.config.data.buttons.find(button => button.type === FooterButton.Deactivate);
    if (this.deactivateButton) {
      this.deactivateButton.isDisabled = false;
    }

    this.reactivateButton = this.config.data.buttons.find(button => button.type === FooterButton.Reactivate);
    if (this.reactivateButton) {
      this.reactivateButton.isDisabled = false;
    }
  }

  private initializeFormFeatures(): void {
    this.portalEmailControl = FormUtil.get<HeadOfficeSubUser>(this.form, 'portalEmail');
    this.nameControl = FormUtil.get<HeadOfficeSubUser>(this.form, 'name');
    this.expiryDateControl = FormUtil.get<HeadOfficeSubUser>(this.form, 'expiryDate');

    this.portalEmailControl.patchValue(this.item.portalEmail);
    this.nameControl.patchValue(this.item.name);
    const currentDate = this.localBspTimeService.getDate();
    this.expiryDateControl.patchValue(currentDate);
  }

  private initializeConfigFeatures(): void {
    this.item = this.config.data.item;
    this.isDeactivateMode = this.config.data.isDeactivateMode;
    this.actionEmitter = this.config.data.actionEmitter;
  }

  private initializeButtonsListeners(): void {
    this.deactivateButtonListener();
    this.reactivateButtonlistener();
  }

  private deactivateButtonListener(): void {
    this.deactivateButtonClick$
      .pipe(
        switchMap(() => this.requestDeactivate()),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.actionEmitter.next();
        this.dialogService.close();
        const translateMessage = 'head-office-sub-users.deactivateSuccessMessage';
        this.headOfficeSubUsersStoreFacadeService.actions.changeSubUserStatus(
          this.item.status === EStatus.ACTIVE ? EStatus.INACTIVE : EStatus.ACTIVE
        );
        this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      });
  }

  private reactivateButtonlistener(): void {
    this.reactivateButtonClick$
      .pipe(
        switchMap(() => this.requestReactivate()),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.actionEmitter.next();
        this.dialogService.close();
        const translateMessage = 'head-office-sub-users.reactivateSuccessMessage';
        this.headOfficeSubUsersStoreFacadeService.actions.changeSubUserStatus(
          this.item.status === EStatus.ACTIVE ? EStatus.INACTIVE : EStatus.ACTIVE
        );

        this.notificationService.showSuccess(this.translationService.translate(translateMessage));
      });
  }

  private requestDeactivate(): Observable<HeadOfficeSubUser> {
    return this.headOfficeSubUsersService.updateSubUserStatus(this.item.id, EStatus.INACTIVE);
  }

  private requestReactivate(): Observable<HeadOfficeSubUser> {
    return this.headOfficeSubUsersService.updateSubUserStatus(
      this.item.id,
      EStatus.ACTIVE,
      this.portalEmailControl.value
    );
  }

  private buildForm(): void {
    this.form = this.formFactory.createGroup<Partial<HeadOfficeSubUser>>({
      portalEmail: [],
      name: [],
      expiryDate: []
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
