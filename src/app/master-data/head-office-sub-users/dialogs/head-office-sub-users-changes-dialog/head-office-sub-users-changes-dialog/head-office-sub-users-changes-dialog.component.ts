import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { EAccess, HeadOfficeSubUser } from '../../../models/head-office-sub-users.models';
import { HeadOfficeSubUsersStoreFacadeService } from '../../../store/head-office-sub-users-store-facade.service';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-head-office-sub-users-changes-dialog',
  templateUrl: './head-office-sub-users-changes-dialog.component.html',
  styleUrls: ['./head-office-sub-users-changes-dialog.component.scss']
})
export class HeadOfficeSubUsersChangesDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;

  public portalEmailControl: FormControl;
  public nameControl: FormControl;
  public organizationControl: FormControl;
  public telephoneControl: FormControl;
  public countryControl: FormControl;
  public cityControl: FormControl;
  public addressControl: FormControl;
  public postCodeControl: FormControl;
  public accessControl: FormControl;

  public accessOptions = [
    {
      value: EAccess.R,
      label: this.translationService.translate('head-office-sub-users.filters.access.option.R')
    },
    {
      value: EAccess.RW,
      label: this.translationService.translate('head-office-sub-users.filters.access.option.RW')
    }
  ];

  private createButton: ModalAction;

  private destroy$ = new Subject();

  private createButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Create),
    takeUntil(this.destroy$)
  );

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    private readonly headOfficeSubUsersStoreFacadeService: HeadOfficeSubUsersStoreFacadeService,
    private readonly translationService: L10nTranslationService
  ) {}

  public ngOnInit(): void {
    this.buildForm();
    this.initializeButtons();
    this.initializeFormListeners();
  }

  public ngAfterViewInit(): void {
    this.subscribeToButtonStatus();
  }

  private subscribeToButtonStatus(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const isFormInvalid = status !== 'VALID';
      this.setIsDisabledForSaveButton(isFormInvalid);
    });
  }

  private setIsDisabledForSaveButton(isDisabled: boolean): void {
    if (this.createButton) {
      this.createButton.isDisabled = isDisabled;
    }
  }

  private buildForm(): void {
    this.portalEmailControl = new FormControl(null, Validators.required);
    this.nameControl = new FormControl(null, Validators.required);
    this.organizationControl = new FormControl(null, Validators.required);
    this.telephoneControl = new FormControl(null, Validators.required);
    this.countryControl = new FormControl(null, Validators.required);
    this.cityControl = new FormControl(null, Validators.required);
    this.addressControl = new FormControl(null, Validators.required);
    this.postCodeControl = new FormControl(null, Validators.required);
    this.accessControl = new FormControl(null, Validators.required);

    this.organizationControl.patchValue('IATA');

    this.form = this.formBuilder.group({
      portalEmail: this.portalEmailControl,
      name: this.nameControl,
      organization: this.organizationControl,
      telephone: this.telephoneControl,
      country: this.countryControl,
      city: this.cityControl,
      address: this.addressControl,
      postCode: this.postCodeControl,
      access: this.accessControl
    });
  }

  private initializeButtons(): void {
    this.createButton = this.config.data.buttons.find(button => button.type === FooterButton.Create);

    if (this.createButton) {
      this.createButton.isDisabled = true;
    }
  }

  private initializeFormListeners(): void {
    this.createButtonClick$.subscribe(() => {
      const valueToDispatch: Partial<HeadOfficeSubUser> = {
        portalEmail: this.portalEmailControl.value,
        name: this.nameControl.value,
        contactInfo: {
          organization: this.organizationControl.value,
          telephone: this.telephoneControl.value,
          country: this.countryControl.value,
          city: this.cityControl.value,
          address: this.addressControl.value,
          postCode: this.postCodeControl.value,
          email: this.portalEmailControl.value
        },
        access: this.accessControl.value
      };
      this.headOfficeSubUsersStoreFacadeService.actions.add(valueToDispatch);
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
