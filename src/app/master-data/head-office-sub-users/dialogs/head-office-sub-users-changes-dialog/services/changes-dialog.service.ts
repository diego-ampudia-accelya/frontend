import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { translations } from '../../translations';
import { HeadOfficeSubUsersChangesDialogComponent } from '../head-office-sub-users-changes-dialog/head-office-sub-users-changes-dialog.component';
import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { HeadOfficeSubUser } from '~app/master-data/head-office-sub-users/models/head-office-sub-users.models';
import { HeadOfficeSubUsersService } from '~app/master-data/head-office-sub-users/services/head-office-sub-users.service';
import { DialogService } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private dataService: HeadOfficeSubUsersService
  ) {}

  public open(): Observable<any> {
    const buildTranslations = translations.create;
    const dialogConfig = this.configBuilder.build(buildTranslations);

    return this.dialogService.open(HeadOfficeSubUsersChangesDialogComponent, dialogConfig);
  }

  public create(item: Partial<HeadOfficeSubUser>): Observable<HeadOfficeSubUser> {
    return this.dataService.create(item);
  }
}
