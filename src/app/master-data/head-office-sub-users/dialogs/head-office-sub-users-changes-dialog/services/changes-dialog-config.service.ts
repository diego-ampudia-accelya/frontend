import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(question: { title: string; details: string }): any {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Create }]
      },
      message
    };
  }
}
