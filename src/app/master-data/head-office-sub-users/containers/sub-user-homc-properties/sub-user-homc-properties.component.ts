import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { filter, takeUntil, withLatestFrom } from 'rxjs/operators';

import { HeadOfficeSubUsersReactivateDeactivateDialogComponent } from '../../dialogs/head-office-sub-users-reactivate-deactivate-dialog/head-office-sub-users-reactivate-deactivate-dialog.component';
import {
  EAccess,
  EStatus,
  HeadOfficeSubUser,
  HeadOfficeSubUserUpdateModel
} from '../../models/head-office-sub-users.models';
import { HeadOfficeSubUsersStoreFacadeService } from '../../store/head-office-sub-users-store-facade.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { FormUtil } from '~app/shared/helpers';
import { toSubUserHomcUpdateModel } from '~app/shared/helpers/user-homc.helper';
import { ResponseErrorBE } from '~app/shared/models';

@Component({
  selector: 'bspl-sub-user-homc-properties',
  templateUrl: './sub-user-homc-properties.component.html',
  styleUrls: ['./sub-user-homc-properties.component.scss']
})
export class SubUserHomcPropertiesComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public subUserHomc: HeadOfficeSubUser;
  public statusLabel: string;

  public loading$ = this.headOfficeSubUsersStoreFacadeService.selectors.getProfileHeadOfficeSubUserLoading$;

  public accessOptions = [
    {
      value: EAccess.R,
      label: this.translationService.translate('head-office-sub-users.filters.access.option.R')
    },
    {
      value: EAccess.RW,
      label: this.translationService.translate('head-office-sub-users.filters.access.option.RW')
    }
  ];

  public isUserActive: boolean;

  private formUtil: FormUtil;
  private dialogQueryFormSubmitEmitter = new Subject();
  private subUserHomc$ = this.headOfficeSubUsersStoreFacadeService.selectors.getProfileHeadOfficeSubUser$;
  private subUserRequestError$ =
    this.headOfficeSubUsersStoreFacadeService.selectors.getProfileSubUserPropertiesError$.pipe(
      filter(error => !!error)
    );
  private storedFormValue$ = this.headOfficeSubUsersStoreFacadeService.selectors.getPropertiesFromValue$;

  private destroy$ = new Subject();

  constructor(
    private readonly headOfficeSubUsersStoreFacadeService: HeadOfficeSubUsersStoreFacadeService,
    private formBuilder: FormBuilder,
    private readonly translationService: L10nTranslationService,
    private dialogService: DialogService
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.initializeSubUserForm();
  }

  public changeUserStatus(): void {
    if (this.subUserHomc.status === EStatus.ACTIVE) {
      this.openDeactivateReactivateDialog(this.getConfigDeactivateDialog(this.subUserHomc));
    } else {
      this.openDeactivateReactivateDialog(this.getConfigReactivateDialog(this.subUserHomc));
    }
  }

  private initializeSubUserForm(): void {
    this.subUserHomc$
      .pipe(withLatestFrom(this.storedFormValue$), takeUntil(this.destroy$))
      .subscribe(([subUserHomc, storedForm]) => {
        this.subUserHomc = subUserHomc;
        this.isUserActive = this.subUserHomc.status === EStatus.ACTIVE;
        this.initializeSubUser();
        this.initializeForm();

        if (this.form) {
          this.setFormValue(storedForm);
        }
      });
  }

  private initializeForm(): void {
    this.form = this.formUtil.createGroup<Partial<HeadOfficeSubUserUpdateModel>>(
      {
        id: [''],
        access: ['', [Validators.required]],
        status: [''],
        registerDate: [''],
        expiryDate: [''],
        portalEmail: ['', [Validators.required, Validators.maxLength(200)]],
        name: ['', [Validators.required, Validators.maxLength(49)]],
        organization: ['', [Validators.required, Validators.maxLength(30)]],
        telephone: ['', [Validators.required, Validators.maxLength(15)]],
        country: ['', [Validators.required, Validators.maxLength(26)]],
        city: ['', [Validators.required, Validators.maxLength(30)]],
        address: ['', [Validators.required, Validators.maxLength(30)]],
        postCode: ['', [Validators.required, Validators.maxLength(10)]]
      },
      { updateOn: 'change' }
    );

    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => this.dispatchValueChange());

    this.subUserRequestError$.pipe(takeUntil(this.destroy$)).subscribe(({ error }) => this.setFieldErrors(error));
  }

  private dispatchValueChange(): void {
    const isFormValid = this.form.valid;
    const formValue = this.form.value;

    this.headOfficeSubUsersStoreFacadeService.actions.propertiesFormValueChange(formValue, isFormValid);
  }

  private setFieldErrors(error: ResponseErrorBE): void {
    if (error.errorCode !== 400 || !error.messages?.length) {
      return;
    }

    error.messages.forEach(({ message, messageParams }) => {
      const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
      const invalidControl = this.form.get(controlName);

      if (invalidControl) {
        invalidControl.setErrors({ requestError: { message } });
        FormUtil.showControlState(invalidControl);
      }
    });
  }

  private getConfigDeactivateDialog(item: HeadOfficeSubUser): DialogConfig {
    return {
      data: {
        actionEmitter: this.dialogQueryFormSubmitEmitter,
        item,
        title: this.translationService.translate('head-office-sub-users.deactivateTitle'),
        footerButtonsType: FooterButton.Deactivate,
        isDeactivateMode: true
      }
    };
  }

  private openDeactivateReactivateDialog(config: DialogConfig): void {
    this.dialogService.open(HeadOfficeSubUsersReactivateDeactivateDialogComponent, config);
  }

  private getConfigReactivateDialog(item: HeadOfficeSubUser): DialogConfig {
    return {
      data: {
        actionEmitter: this.dialogQueryFormSubmitEmitter,
        item,
        title: this.translationService.translate('head-office-sub-users.reactivateTitle'),
        footerButtonsType: FooterButton.Reactivate,
        isDeactivateMode: false
      }
    };
  }

  private setFormValue(formValue: Partial<HeadOfficeSubUser>): void {
    const formToPatch = formValue || toSubUserHomcUpdateModel(this.subUserHomc);
    this.form.reset(undefined, { emitEvent: false });
    this.form.patchValue(formToPatch, { emitEvent: false });
    FormUtil.showControlState(this.form);
  }

  private initializeSubUser(): void {
    if (this.subUserHomc) {
      this.statusLabel = `head-office-sub-users.filters.status.option.${this.subUserHomc.status.toUpperCase()}`;
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
