import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';

import * as SubUsersProfilePermissionsActions from '../../actions/head-office-sub-users-permissions.actions';
import * as fromSubUsers from '../../selectors/head-office-sub-users.selectors';
import { SubUserHomcChangesService } from '../../shared/sub-user-homc-changes.service';

import { SubUserHomcPermissionsComponent } from './sub-user-homc-profile-permissions.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';

describe('SubUserHomcPermissionsComponent', () => {
  let component: SubUserHomcPermissionsComponent;
  let fixture: ComponentFixture<SubUserHomcPermissionsComponent>;
  let mockStore: MockStore;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [SubUserHomcPermissionsComponent, MockPipe(HasPermissionPipe)],
      providers: [
        provideMockStore({
          selectors: [
            { selector: fromSubUsers.getOriginalProfilePermissions, value: [] },
            { selector: fromSubUsers.areProfilePermissionsLoading, value: false },
            { selector: fromSubUsers.getProfileHeadOfficeSubUser, value: {} },
            { selector: fromSubUsers.canApplyProfilePermissionsChanges, value: false }
          ]
        }),
        mockProvider(L10nTranslationService, {
          translate: (key: string) => key
        }),
        mockProvider(SubUserHomcChangesService),
        FormBuilder,
        { provide: PermissionsService, useValue: { hasPermission: () => true } }
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUserHomcPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onPermissionChange should dispatch change action', () => {
    const dispatchSpy = spyOn(mockStore, 'dispatch').and.callThrough();

    component.onPermissionChange([]);

    expect(dispatchSpy).toHaveBeenCalledWith(SubUsersProfilePermissionsActions.change({ modifications: [] }));
  });
});
