/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as subUserPermissionsActions from '../../actions/head-office-sub-users-permissions.actions';
import * as fromSubUsers from '../../selectors/head-office-sub-users.selectors';
import { SubUserHomcChangesService } from '../../shared/sub-user-homc-changes.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { SelectionList } from '~app/shared/components/selection-list/selection-list.models';
import { Permissions } from '~app/shared/constants/permissions';

@Component({
  selector: 'bspl-sub-user-homc-profile-permissions',
  templateUrl: './sub-user-homc-profile-permissions.component.html',
  styleUrls: ['./sub-user-homc-profile-permissions.component.scss']
})
export class SubUserHomcPermissionsComponent implements OnInit, OnDestroy {
  public loading$ = this.store.pipe(select(fromSubUsers.areProfilePermissionsLoading));
  // Use defensive cloning to force the SelectionListComponent to work with the store
  public permissions$ = this.store.pipe(select(fromSubUsers.getOriginalProfilePermissions), map(cloneDeep));
  public Permissions = Permissions;
  public buttonDesign = ButtonDesign;

  public isFilterExpanded = true;
  public canEditPermissions = this.permissionsService.hasPermission(Permissions.updateHOMCSUPermissions);

  private iconLabels = {
    clear: 'mi md-18 clear',
    filter: 'mi md-18 filter_list'
  };

  public filterIcon: string = this.iconLabels.clear;

  private subUser$ = this.store.pipe(select(fromSubUsers.getProfileHeadOfficeSubUser));

  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private changesService: SubUserHomcChangesService,
    private permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.getHeadOfficeSubUserAndPermissions();
  }

  public onPermissionChange(modifications: SelectionList[]) {
    this.store.dispatch(subUserPermissionsActions.change({ modifications: cloneDeep(modifications) }));
  }

  public toggleFilter(): void {
    this.isFilterExpanded = !this.isFilterExpanded;
    this.filterIcon = !this.isFilterExpanded ? this.iconLabels.filter : this.iconLabels.clear;
  }

  public viewFilter(): void {
    this.isFilterExpanded = true;
    this.filterIcon = this.iconLabels.clear;
  }

  public canDeactivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromSubUsers.canApplyProfilePermissionsChanges),
      switchMap(hasChanges => (hasChanges ? this.changesService.handlePermissionsChangeAndDeactivate$ : of(true))),
      first()
    );
  }

  private getHeadOfficeSubUserAndPermissions(): void {
    this.subUser$
      .pipe(
        withLatestFrom(),
        tap(() => {
          this.store.dispatch(subUserPermissionsActions.requestPermissions());
        })
      )
      .subscribe();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
