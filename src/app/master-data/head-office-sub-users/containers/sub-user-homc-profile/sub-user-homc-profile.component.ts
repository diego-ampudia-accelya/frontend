import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

import * as subUserPermissionsActions from '../../actions/head-office-sub-users-permissions.actions';
import * as subUserActions from '../../actions/head-office-sub-users.actions';
import * as fromSubUsers from '../../selectors/head-office-sub-users.selectors';
import { HeadOfficeSubUsersStoreFacadeService } from '../../store/head-office-sub-users-store-facade.service';
import { or } from '~app/shared/utils';
import { AppState } from '~app/reducers';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';

@Component({
  selector: 'bspl-sub-user-homc-profile',
  templateUrl: './sub-user-homc-profile.component.html',
  styleUrls: ['./sub-user-homc-profile.component.scss']
})
export class SubUserHomcProfileComponent implements OnInit, OnDestroy {
  public userPortalEmail$: Observable<any>;

  public tabs: RoutedMenuItem[];

  public isApplyChangesEnabled$: Observable<boolean>;

  public currentSavableTab = null;

  public buttonLabel: string;

  private destroyed$ = new Subject();

  private savableTabs = [
    {
      url: './properties',
      canApplyChangesSelector: fromSubUsers.canApplyPropertyChanges,
      applyChangesAction: subUserActions.applyPropertyChanges()
    },
    {
      url: './permissions',
      canApplyChangesSelector: fromSubUsers.canApplyProfilePermissionsChanges,
      applyChangesAction: subUserPermissionsActions.openApplyChanges()
    }
  ];

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    private readonly headOfficeSubUsersStoreFacadeService: HeadOfficeSubUsersStoreFacadeService
  ) {}

  public applyChanges() {
    this.store.dispatch(this.currentSavableTab.applyChangesAction);
  }

  public ngOnInit(): void {
    this.initializePortalEmail();
    this.buttonLabel = 'common.applyChanges';

    this.isApplyChangesEnabled$ = combineLatest(
      this.savableTabs.map(savableTab =>
        this.store.pipe(
          select(savableTab.canApplyChangesSelector as MemoizedSelector<AppState, boolean>),
          map(canSave => canSave && this.currentSavableTab.url === savableTab.url)
        )
      )
    ).pipe(map(values => or(...values)));
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);

    const navigated$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));
    merge(navigated$, of(null))
      .pipe(
        map(() =>
          this.savableTabs.find(savable => {
            const url = this.router.createUrlTree([savable.url], {
              relativeTo: this.activatedRoute
            });

            return this.router.isActive(url, false);
          })
        ),
        tap(activeSavableTab => {
          this.currentSavableTab = activeSavableTab;
        }),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  private initializePortalEmail() {
    this.userPortalEmail$ = this.headOfficeSubUsersStoreFacadeService.selectors.getProfileHeadOfficeSubUser$.pipe(
      map(subUserHomc => subUserHomc?.portalEmail),
      takeUntil(this.destroyed$)
    );
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
