import { Component, OnInit } from '@angular/core';

import { HeadOfficeSubUsersStoreFacadeService } from './store/head-office-sub-users-store-facade.service';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';

@Component({
  selector: 'bspl-head-office-sub-users',
  templateUrl: './head-office-sub-users.component.html'
})
export class HeadOfficeSubUsersComponent implements OnInit {
  constructor(private headOfficeSubUsersStoreFacadeService: HeadOfficeSubUsersStoreFacadeService) {}

  public ngOnInit(): void {
    this.headOfficeSubUsersStoreFacadeService.actions.searchHeadOfficeSubUser(defaultQuery);
  }
}
