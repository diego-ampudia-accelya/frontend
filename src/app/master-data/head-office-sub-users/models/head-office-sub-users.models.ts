import { SubUserTemplate } from '~app/sub-users/shared/sub-users.models';

// TODO: replace to ActiveInactiveInUpperCase
export enum EStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}

export enum EAccess {
  R = 'R',
  RW = 'RW'
}

export interface ContactInfo {
  organization: string;
  telephone: string;
  country: string;
  city: string;
  address: string;
  postCode: string;
  email: string;
}

export interface HeadOfficeSubUser {
  id: number;
  portalEmail: string;
  level: number;
  name: string;
  parentName: string;
  registerDate: string;
  expiryDate: string;
  numberOfUsers: number;
  status: EStatus;
  userType: string;
  contactInfo: ContactInfo;
  access: EAccess;
  template?: SubUserTemplate;
}

export interface HeadOfficeSubUserUpdateModel {
  id: number;
  access: EAccess;
  status: EStatus;
  registerDate: string;
  expiryDate: string;
  portalEmail: string;
  name: string;
  organization: string;
  telephone: string;
  country: string;
  city: string;
  address: string;
  postCode: string;
}

export interface HeadOfficeSubUserCreateRequest {
  portalEmail: string;
  name: string;
  organization: string;
  telephone: string;
  country: string;
  city: string;
  address: string;
  postCode: string;
  access: string;
}

export interface HeadOfficeSubUserFilters {
  portalEmail: string;
  name: string;
  parentName: string;
  registerDate: Date[];
  expiryDate: Date[];
  status: EStatus;
  access: EAccess;
}

export interface HeadOfficeSubUserFiltersBE {
  portalEmail: string;
  name: string;
  parentName: string;
  fromRegisterDate: string;
  toRegisterDate: string;
  fromExpiryDate: string;
  toExpiryDate: string;
  status: EStatus;
  access: EAccess;
}
