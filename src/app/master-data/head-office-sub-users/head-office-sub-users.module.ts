import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { HeadOfficeSubUsersListComponent } from './components/head-office-sub-users-list/head-office-sub-users-list.component';
import { SubUserHomcPermissionsComponent } from './containers/sub-user-homc-profile-permissions/sub-user-homc-profile-permissions.component';
import { SubUserHomcProfileComponent } from './containers/sub-user-homc-profile/sub-user-homc-profile.component';
import { SubUserHomcPropertiesComponent } from './containers/sub-user-homc-properties/sub-user-homc-properties.component';
import { HeadOfficeSubUsersChangesDialogComponent } from './dialogs/head-office-sub-users-changes-dialog/head-office-sub-users-changes-dialog/head-office-sub-users-changes-dialog.component';
import { ChangesDialogConfigService } from './dialogs/head-office-sub-users-changes-dialog/services/changes-dialog-config.service';
import { ChangesDialogService } from './dialogs/head-office-sub-users-changes-dialog/services/changes-dialog.service';
import { HeadOfficeSubUsersReactivateDeactivateDialogComponent } from './dialogs/head-office-sub-users-reactivate-deactivate-dialog/head-office-sub-users-reactivate-deactivate-dialog.component';
import { HeadOfficeSubUserEffects } from './effects/head-office-sub-users.effects';
import { HeadOfficeSubUsersRoutingModule } from './head-office-sub-users-routing.module';
import { HeadOfficeSubUsersComponent } from './head-office-sub-users.component';
import { headOfficeSubUserReducer } from './reducers/head-office-sub-users.reducer';
import { HeadOfficeSubUsersFilterFormatterService } from './services/head-office-sub-users-filter-formatter.service';
import { HeadOfficeSubUsersService } from './services/head-office-sub-users.service';
import { SubUserHomcProfileResolver } from './shared/sub-user-homc-profile.resolver';
import { HeadOfficeSubUsersStoreFacadeService } from './store/head-office-sub-users-store-facade.service';
import { headOfficeSubUserFeatureKey } from './store/head-office-sub-users.state';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    HeadOfficeSubUsersRoutingModule,
    SharedModule,
    L10nTranslationModule,
    StoreModule.forFeature(headOfficeSubUserFeatureKey, headOfficeSubUserReducer),
    EffectsModule.forFeature([HeadOfficeSubUserEffects])
  ],
  declarations: [
    HeadOfficeSubUsersComponent,
    HeadOfficeSubUsersListComponent,
    HeadOfficeSubUsersChangesDialogComponent,
    HeadOfficeSubUsersReactivateDeactivateDialogComponent,
    SubUserHomcProfileComponent,
    SubUserHomcPropertiesComponent,
    SubUserHomcPermissionsComponent
  ],
  providers: [
    HeadOfficeSubUsersService,
    HeadOfficeSubUsersStoreFacadeService,
    HeadOfficeSubUsersFilterFormatterService,
    ChangesDialogService,
    ChangesDialogConfigService,
    SubUserHomcProfileResolver
  ]
})
export class HeadOfficeSubUsersModule {}
