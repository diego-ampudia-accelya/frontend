import { createAction, props } from '@ngrx/store';

import { SafTypes, SafTypesFilter } from '../../shared/saf-types.models';
import { DataQuery } from '~app/shared/components/list-view';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { PagedData } from '~app/shared/models/paged-data.model';

export const search = createAction('[Saf Types] Search', props<{ query?: DataQuery<SafTypesFilter> }>());
export const searchConfirmed = createAction('[Saf Types] Search Confirmed');
export const searchCancelled = createAction('[Saf Types] Search Cancelled');
export const searchSuccess = createAction('[Saf Types] Search Success', props<{ data: PagedData<SafTypes> }>());
export const searchError = createAction('[Saf Types] Search Error');

export const download = createAction('[Saf Types] Download');
export const downloadConfirmed = createAction('[Saf Types] Download Confirmed');

export const openApplyChanges = createAction('[Saf Types] Open Apply Changes', props<{ value?: SafTypes }>());
export const applyChanges = createAction('[Saf Types] Apply Changes', props<{ value: SafTypes }>());
export const applyChangesSuccess = createAction('[Saf Types] Apply Changes Success');
export const applyChangesError = createAction('[Saf Types] Apply Changes Error', props<{ error }>());

export const remove = createAction('[Saf Types] Remove', props<{ value: SafTypes }>());
export const removeSuccess = createAction('[Saf Types] Remove Success', props<{ value: SafTypes }>());
export const removeError = createAction('[Saf Types] Remove Error', props<{ error: ResponseErrorBE }>());

export const openAdd = createAction('[Saf Types] Open Add');
export const add = createAction('[Saf Types] Add', props<{ value: SafTypesFilter }>());
export const addSuccess = createAction('[Saf Types] Add Success');
export const addError = createAction('[Saf Types] Add Error', props<{ error }>());
