import { createReducer, on } from '@ngrx/store';

import { SafTypes, SafTypesFilter } from '../../shared/saf-types.models';
import { SafTypesActions } from '../actions';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { ResponseErrorBE } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';

export const key = 'list';

export interface State {
  query: DataQuery<SafTypesFilter>;
  previousQuery: DataQuery<SafTypesFilter>;
  data: SafTypes[];
  loading: boolean;
  errors: { error: ResponseErrorBE };
}

export const initialState: State = {
  query: defaultQuery,
  previousQuery: null,
  data: [],
  loading: false,
  errors: null
};

const loadData = (state: State, data: PagedData<SafTypes>) => ({
  ...state,
  loading: false,
  data: data.records,
  query: {
    ...state.query,
    paginateBy: {
      page: data.pageNumber,
      size: data.pageSize,
      totalElements: data.total
    }
  }
});

export const reducer = createReducer(
  initialState,
  on(SafTypesActions.openApplyChanges, state => ({ ...state })),
  on(SafTypesActions.openAdd, state => ({ ...state })),
  on(SafTypesActions.search, (state, { query = state.query }) => ({
    ...state,
    previousQuery: state.query,
    query
  })),
  on(SafTypesActions.searchConfirmed, state => ({
    ...state,
    previousQuery: null,
    loading: true
  })),
  on(SafTypesActions.searchCancelled, state => ({
    ...state,
    query: state.previousQuery,
    previousQuery: null
  })),
  on(SafTypesActions.searchSuccess, (state, { data }) => loadData(state, data)),
  on(SafTypesActions.searchError, state => ({
    ...state,
    loading: false
  })),
  on(SafTypesActions.add, state => ({ ...state, loading: true })),
  on(SafTypesActions.addSuccess, state => ({ ...state, loading: false })),
  on(SafTypesActions.applyChangesError, SafTypesActions.addError, (state, { error }) => ({
    ...state,
    loading: false,
    errors: error
  }))
);

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;

export const getQuery = (state: State) => state.query;
