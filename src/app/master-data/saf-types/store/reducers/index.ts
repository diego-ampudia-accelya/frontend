import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromList from './saf-types.reducer';
import { AppState } from '~app/reducers';

export const safTypesFeatureKey = 'safTypes';

export interface State extends AppState {
  [safTypesFeatureKey]: SafTypesState;
}

export interface SafTypesState {
  [fromList.key]: fromList.State;
}

export function reducers(state: SafTypesState | undefined, action: Action) {
  return combineReducers({
    [fromList.key]: fromList.reducer
  })(state, action);
}

export const getSafTypes = createFeatureSelector<State, SafTypesState>(safTypesFeatureKey);

export const getList = createSelector(getSafTypes, state => state[fromList.key]);

export const getListLoading = createSelector(getList, fromList.getLoading);

export const getListData = createSelector(getList, fromList.getData);

export const hasData = createSelector(getListData, data => data && data.length > 0);

export const getListQuery = createSelector(getList, fromList.getQuery);
