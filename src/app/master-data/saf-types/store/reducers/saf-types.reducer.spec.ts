import { SafType, SafTypes, SafTypesFilter, StatisticalCode } from '../../shared/saf-types.models';
import { SafTypesActions } from '../actions';
import * as fromSafTypes from './saf-types.reducer';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { PagedData } from '~app/shared/models/paged-data.model';

describe('Saf Types Reducer', () => {
  const query: DataQuery<SafTypesFilter> = {
    ...defaultQuery,
    filterBy: {
      description: [
        {
          id: 698353,
          type: SafType.adm,
          description: 'ACM 53'
        }
      ]
    }
  };

  const safTypes: SafTypes[] = [
    {
      id: 999,
      type: [
        {
          translationKey: 'test',
          value: SafType.adm
        }
      ],
      description: [
        {
          id: 698353,
          type: SafType.adm,
          description: 'ACM 53'
        }
      ],
      statisticalCode: [
        {
          translationKey: 'test',
          value: StatisticalCode.int
        }
      ]
    }
  ];

  it('should update state correctly on SEARCH action', () => {
    const action = SafTypesActions.search({ query });
    const state = fromSafTypes.reducer(fromSafTypes.initialState, action);
    const geQuery = fromSafTypes.getQuery(state);

    expect(state.query).toEqual(query);
    expect(state.previousQuery).toEqual(defaultQuery);
    expect(geQuery).toEqual(state.query);
  });

  it('should update state correctly on SEARCH CONFIRMED action', () => {
    const initialState: fromSafTypes.State = {
      ...fromSafTypes.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = SafTypesActions.searchConfirmed();
    const state = fromSafTypes.reducer(initialState, action);

    expect(state.previousQuery).toBeNull();
    expect(state.loading).toBe(true);
  });

  it('should update state correctly on SEARCH CANCELLED action', () => {
    const initialState: fromSafTypes.State = {
      ...fromSafTypes.initialState,
      query,
      previousQuery: defaultQuery
    };

    const action = SafTypesActions.searchCancelled();
    const state = fromSafTypes.reducer(initialState, action);

    expect(state.query).toEqual(defaultQuery);
    expect(state.previousQuery).toBeNull();
  });

  it('should update state correctly on SEARCH SUCCESS action', () => {
    const data: PagedData<SafTypes> = {
      records: safTypes,
      pageSize: 20,
      total: safTypes.length,
      totalPages: 1,
      pageNumber: 0
    };

    const action = SafTypesActions.searchSuccess({ data });
    const state = fromSafTypes.reducer(fromSafTypes.initialState, action);
    const getData = fromSafTypes.getData(state);

    expect(state.loading).toBeFalsy();
    expect(state.data).toEqual(safTypes);
    expect(state.query).toEqual(
      jasmine.objectContaining({ paginateBy: { page: 0, size: 20, totalElements: safTypes.length } })
    );
    expect(getData).toEqual(state.data);
  });

  it('should set loading false on SEARCH ERROR action', () => {
    const action = SafTypesActions.searchError();
    const state = fromSafTypes.reducer(fromSafTypes.initialState, action);

    expect(state.loading).toBeFalsy();
  });

  it('should set loading true on APPLY CHANGE action', () => {
    const action = SafTypesActions.applyChanges({ value: safTypes[0] });
    const state = fromSafTypes.reducer(fromSafTypes.initialState, action);
    const getLoading = fromSafTypes.getLoading(state);

    expect(state.loading).toBeFalsy();
    expect(getLoading).toEqual(state.loading);
  });

  it('should set loading false on APPLY CHANGE SUCCESS action', () => {
    const action = SafTypesActions.applyChangesSuccess();
    const state = fromSafTypes.reducer(fromSafTypes.initialState, action);

    expect(state.loading).toBeFalsy();
  });

  it('should not change state on OPEN APPLY CHANGE action', () => {
    const action = SafTypesActions.openApplyChanges({ value: safTypes[0] });
    const state = fromSafTypes.reducer(fromSafTypes.initialState, action);

    expect(state).toEqual(state);
  });

  it('should set error on OPEN APPLY CHANGE ERROR action', () => {
    const action = SafTypesActions.applyChangesError({ error: 'error' });
    const state = fromSafTypes.reducer(fromSafTypes.initialState, action);

    expect(state.errors).toEqual('error');
  });
});
