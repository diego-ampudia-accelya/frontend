import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { EMPTY, of } from 'rxjs';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { DeleteDialogService } from '../../dialogs/delete-dialog/delete-dialog.service';
import { ChangesDialogService } from '../../dialogs/saf-types-changes-dialog/services/changes-dialog.service';
import { SafTypesListService } from '../../shared/saf-types-list.service';
import { SafTypesActions } from '../actions';
import * as fromSafTypes from '../reducers';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';
import { NotificationService } from '~app/shared/services';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AppState } from '~app/reducers';

@Injectable()
export class SafTypesEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private dataService: SafTypesListService,
    private dialogService: DialogService,
    private changesDialogService: ChangesDialogService,
    private removeDialogService: DeleteDialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  public search$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafTypesActions.search),
      switchMap(() => of(SafTypesActions.searchConfirmed()))
    )
  );

  public searchConfirmed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafTypesActions.searchConfirmed),
      withLatestFrom(this.store.pipe(select(fromSafTypes.getListQuery))),
      switchMap(([_, query]) =>
        this.dataService.find(query).pipe(
          map(data => SafTypesActions.searchSuccess({ data })),
          rethrowError(() => SafTypesActions.searchError())
        )
      )
    )
  );

  public download$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafTypesActions.download),
      switchMap(result => (result ? of(SafTypesActions.downloadConfirmed()) : EMPTY))
    )
  );

  public downloadConfirmed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafTypesActions.downloadConfirmed),
        withLatestFrom(this.store.pipe(select(fromSafTypes.getListQuery))),
        switchMap(([_, query]) =>
          this.dialogService.open(DownloadFileComponent, {
            data: {
              title: 'BUTTON.DEFAULT.DOWNLOAD',
              footerButtonsType: FooterButton.Download,
              downloadQuery: query
            },
            apiService: this.dataService
          })
        )
      ),
    { dispatch: false }
  );

  public remove$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafTypesActions.remove),
        switchMap(({ value }) => this.removeDialogService.open(value))
      ),
    { dispatch: false }
  );

  public removeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafTypesActions.removeSuccess),
        tap(({ value }) => {
          const deleteMessage = this.translationService.translate(
            'LIST.MASTER_DATA.safTypes.dialog.delete.deleteMessage',
            {
              item: value.description
            }
          );
          this.notificationService.showSuccess(deleteMessage);

          return this.store.dispatch(SafTypesActions.searchConfirmed());
        })
      ),

    { dispatch: false }
  );

  public $openApplyChanges = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafTypesActions.openApplyChanges),
        switchMap(({ value }) => this.changesDialogService.open(value))
      ),
    { dispatch: false }
  );

  public applyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafTypesActions.applyChanges),
      switchMap(({ value }) =>
        this.changesDialogService.edit(value).pipe(
          map(() => SafTypesActions.applyChangesSuccess()),
          rethrowError(error => SafTypesActions.applyChangesError(error))
        )
      )
    )
  );

  public applyChangesSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafTypesActions.applyChangesSuccess),
        tap(() => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(
            'LIST.MASTER_DATA.safTypes.dialog.edit.successMessage'
          );
          this.notificationService.showSuccess(successMessage);

          return this.store.dispatch(SafTypesActions.searchConfirmed());
        })
      ),
    { dispatch: false }
  );

  public $openAdd = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafTypesActions.openAdd),
        switchMap(() => this.changesDialogService.open())
      ),
    { dispatch: false }
  );

  public add$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SafTypesActions.add),
      switchMap(({ value }) =>
        this.changesDialogService.create(value).pipe(
          map(() => SafTypesActions.addSuccess()),
          rethrowError(error => SafTypesActions.addError(error))
        )
      )
    )
  );

  public addSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SafTypesActions.addSuccess),
        tap(() => {
          this.dialogService.close();
          const successMessage = this.translationService.translate(
            'LIST.MASTER_DATA.safTypes.dialog.create.createMessage'
          );
          this.notificationService.showSuccess(successMessage);

          return this.store.dispatch(SafTypesActions.searchConfirmed());
        })
      ),
    { dispatch: false }
  );
}
