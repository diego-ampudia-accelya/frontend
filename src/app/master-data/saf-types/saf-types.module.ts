import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { DeleteDialogConfigService } from './dialogs/delete-dialog/delete-dialog-config.service';
import { DeleteDialogService } from './dialogs/delete-dialog/delete-dialog.service';
import { SafTypesChangeDialogComponent } from './dialogs/saf-types-changes-dialog/saf-types-changes-dialog/saf-types-changes-dialog.component';
import { ChangesDialogConfigService } from './dialogs/saf-types-changes-dialog/services/changes-dialog-config.service';
import { ChangesDialogService } from './dialogs/saf-types-changes-dialog/services/changes-dialog.service';
import { SafTypesListComponent } from './saf-types-list/saf-types-list.component';
import { SafTypesRoutingModule } from './saf-types-routing.module';
import { SafTypesFilterFormatter } from './shared/saf-types-filter-formatter.service';
import { SafTypesListService } from './shared/saf-types-list.service';
import { SafTypesEffects } from './store/effects/saf-types.effects';
import * as fromSafTypes from './store/reducers';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [SafTypesListComponent, SafTypesChangeDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    SafTypesRoutingModule,
    L10nTranslationModule,
    StoreModule.forFeature(fromSafTypes.safTypesFeatureKey, fromSafTypes.reducers),
    EffectsModule.forFeature([SafTypesEffects])
  ],
  providers: [
    SafTypesListService,
    SafTypesFilterFormatter,
    DeleteDialogService,
    DeleteDialogConfigService,
    ChangesDialogService,
    ChangesDialogConfigService
  ]
})
export class SafTypesModule {}
