import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SafTypesListComponent } from './saf-types-list/saf-types-list.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: SafTypesListComponent,
    data: {
      tab: ROUTES.SAF_TYPES
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SafTypesRoutingModule {}
