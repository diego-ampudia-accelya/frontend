import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { SafTypesListService } from './saf-types-list.service';
import { SafType, StatisticalCode } from './saf-types.models';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import { downloadRequestOptions } from '~app/shared/helpers';
import { DownloadFormat } from '~app/shared/models';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

const query = {
  sortBy: [],
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 10 }
} as DataQuery;

describe('SafTypesListService', () => {
  let service: SafTypesListService;

  const usersBsps = [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }];

  const initialState = {
    auth: {
      user: {
        bsps: usersBsps
      }
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SafTypesListService,
        { provide: AppConfigurationService, useValue: { baseApiPath: 'test' } },
        provideMockStore({ initialState, selectors: [{ selector: getUserBsps, value: usersBsps }] })
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SafTypesListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('find', () => {
    let formattedQuery;

    beforeEach(() => {
      formattedQuery = RequestQuery.fromDataQuery({ filterBy: {} });
      (service as any).formatQuery = jasmine.createSpy().and.returnValue(formattedQuery);
      (service as any).http.get = jasmine.createSpy().and.returnValue(of({ records: [] }));
    });

    it('should call formatQuery', fakeAsync(() => {
      service.find(query).pipe(take(1)).subscribe();
      tick();

      expect((service as any).formatQuery).toHaveBeenCalledWith(query);
    }));

    it('should call http get with corresponding query', fakeAsync(() => {
      (service as any).baseUrl = 'base-url';
      const param = `base-url${formattedQuery.getQueryString()}`;

      service.find(query).pipe(take(1)).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(param);
    }));
  });

  describe('download', () => {
    let formattedQuery;

    beforeEach(() => {
      formattedQuery = RequestQuery.fromDataQuery({ filterBy: {} });
      (service as any).formatQuery = jasmine.createSpy().and.returnValue(formattedQuery);
      (service as any).http.get = jasmine.createSpy().and.returnValue(of({ records: [] }));
    });

    it('should call formatQuery', fakeAsync(() => {
      service.download(query, DownloadFormat.TXT).pipe(take(1)).subscribe();
      tick();

      expect((service as any).formatQuery).toHaveBeenCalledWith(query);
    }));

    it('should call http get with download options', fakeAsync(() => {
      (service as any).baseUrl = 'base-url';
      formattedQuery.filterBy = { exportAs: DownloadFormat.TXT };
      const param = `base-url/download${formattedQuery.getQueryString({ omitPaging: true })}`;

      service.download(query, DownloadFormat.TXT).pipe(take(1)).subscribe();
      tick();

      expect((service as any).http.get).toHaveBeenCalledWith(param, downloadRequestOptions);
    }));
  });

  it('formatQuery - should return mapped query', () => {
    const param = {
      filterBy: {
        type: [
          {
            translationKey: 'test',
            value: SafType.adm
          }
        ],
        description: [
          {
            id: 698353,
            type: 'ACM',
            description: 'ACM 53'
          }
        ],
        statisticalCode: [
          {
            translationKey: 'test',
            value: StatisticalCode.int
          }
        ]
      }
    };

    const expectedRes = RequestQuery.fromDataQuery({
      filterBy: {
        type: [SafType.adm],
        description: ['ACM 53'],
        statisticalCode: [StatisticalCode.int]
      }
    });

    const res = (service as any).formatQuery(param);

    expect(res).toEqual(expectedRes);
  });
});
