export interface SafTypeOptionForTranslate {
  translationKey: string;
  value: string;
}

export enum SafType {
  adm = 'ADM',
  acm = 'ACM',
  ra = 'RA',
  rn = 'RN'
}

export enum StatisticalCode {
  int = 'INT',
  dom = 'DOM',
  na = 'NA'
}

export interface SafTypeDescription {
  id: number;
  type: string;
  description: string;
}

export interface SafTypes {
  id: number;
  type: SafTypeOptionForTranslate[];
  description: SafTypeDescription[];
  statisticalCode: SafTypeOptionForTranslate[];
}

export interface SafTypesFilter {
  type?: SafTypeOptionForTranslate[];
  description?: SafTypeDescription[];
  statisticalCode?: SafTypeOptionForTranslate[];
}

export interface SafTypesFilterBE {
  type?: string[];
  description?: string[];
  statisticalCode?: string[];
}
