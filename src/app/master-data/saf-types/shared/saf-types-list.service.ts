import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, first, map } from 'rxjs/operators';

import { SafTypeDescription, SafTypes, SafTypesFilter, SafTypesFilterBE } from './saf-types.models';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class SafTypesListService implements Queryable<SafTypes> {
  private bspId: number;
  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(fromAuth.getUserBsps), first()).subscribe(bsps => {
      this.bspId = bsps && bsps[0] ? bsps[0].id : null;
      this.baseUrl = `${this.appConfiguration.baseApiPath}/bsp-management/bsps/${this.bspId}/saf-types`;
    });
  }

  public find(query: DataQuery<SafTypesFilter>): Observable<PagedData<SafTypes>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<SafTypes>>(this.baseUrl + requestQuery.getQueryString());
  }

  public save(item: SafTypes): Observable<SafTypes> {
    const url = `${this.baseUrl}/${item.id}`;

    return this.http.put<SafTypes>(url, item);
  }

  public create(item: SafTypesFilter): Observable<SafTypes> {
    return this.http.post<SafTypes>(this.baseUrl, item);
  }

  public delete(item: SafTypes): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${item.id}`);
  }

  public get(): Observable<SafTypeDescription[]> {
    const query = new RequestQuery(null, null, null, null);

    query.reduceFilterBy = true;

    return this.http.get<SafTypeDescription[]>(`${this.baseUrl}-dictionary`);
  }

  public getDropdownOptions(): Observable<DropdownOption<SafTypeDescription>[]> {
    return this.get().pipe(
      map(records =>
        records.map(safType => ({
          value: safType,
          label: `${safType.description}`
        }))
      ),
      catchError(() => of([]))
    );
  }

  public download(
    query: DataQuery<SafTypesFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat };
    const url = `${this.baseUrl}/download` + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: Partial<DataQuery<SafTypesFilter>>): RequestQuery<SafTypesFilterBE> {
    const { type, description, statisticalCode, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        type: type && type.map(v => v.value),
        description: description && description.map(d => d.description),
        statisticalCode: statisticalCode && statisticalCode.map(v => v.value)
      }
    });
  }
}
