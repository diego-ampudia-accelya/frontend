import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SafTypesFilter } from './saf-types.models';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { joinMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class SafTypesFilterFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: SafTypesFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<SafTypesFilter>> = {
      type: type =>
        `${this.translate('type.label')} - ${joinMapper(type.map(safType => this.translate(safType.translationKey)))}`,
      description: description =>
        `${this.translate('description.label')} - ${joinMapper(
          description.map(safDescription => safDescription.description)
        )}`,
      statisticalCode: statisticalCode =>
        `${this.translate('statisticalCode.label')} - ${joinMapper(
          statisticalCode.map(code => this.translate(code.translationKey))
        )}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  public translate(key: string): string {
    return this.translationService.translate(`LIST.MASTER_DATA.safTypes.filters.${key}`);
  }
}
