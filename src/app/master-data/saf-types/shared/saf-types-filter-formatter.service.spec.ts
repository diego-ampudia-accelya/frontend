import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { SafTypesFilterFormatter } from './saf-types-filter-formatter.service';

describe('SafTypesFormatter', () => {
  let formatter: SafTypesFilterFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new SafTypesFilterFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
