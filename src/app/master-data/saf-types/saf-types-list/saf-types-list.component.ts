import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { debounceTime, first, switchMap } from 'rxjs/operators';

import { SafTypesFilterFormatter } from '../shared/saf-types-filter-formatter.service';
import { SafTypesListService } from '../shared/saf-types-list.service';
import {
  SafType,
  SafTypeDescription,
  SafTypeOptionForTranslate,
  SafTypes,
  SafTypesFilter,
  StatisticalCode
} from '../shared/saf-types.models';
import { SafTypesActions } from '../store/actions';
import * as fromSafTypes from '../store/reducers';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DataQuery, QUERYABLE } from '~app/shared/components/list-view';
import { Permissions, ROUTES } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-saf-types',
  templateUrl: './saf-types-list.component.html',
  providers: [DatePipe, { provide: QUERYABLE, useExisting: SafTypesListService }]
})
export class SafTypesListComponent implements OnInit {
  public searchForm: FormGroup;
  public columns: GridColumn[];

  public safTypesOptions = this.getEnumOptions(SafType, 'type');
  public statisticalCodeOptions = this.getEnumOptions(StatisticalCode, 'statisticalCode');
  public actions: Array<{ action: GridTableActionType; disabled: boolean }>;
  public safTypeDescriptions: DropdownOption<SafTypeDescription>[];
  public canCreateSafType =
    this.permissionsService.hasPermission(Permissions.createSafTypes) ||
    this.permissionsService.hasPermission(Permissions.createDocumentTypes);
  public customLabels = { create: 'LIST.MASTER_DATA.safTypes.createButtonLabel' };

  public data$ = this.store.pipe(select(fromSafTypes.getListData));
  public loading$ = this.store.pipe(select(fromSafTypes.getListLoading));
  public hasData$ = this.store.pipe(select(fromSafTypes.hasData));
  public query$ = this.store.pipe(select(fromSafTypes.getListQuery));

  private safDescriptionsSubject = new Subject<void>();
  private formFactory: FormUtil;

  constructor(
    public displayFormatter: SafTypesFilterFormatter,
    private formBuilder: FormBuilder,
    private permissionsService: PermissionsService,
    private safTypesListService: SafTypesListService,
    protected store: Store<AppState>,
    private router: Router
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.searchForm = this.buildForm();
    this.checkPermissionAndLoadData();
    this.initColumns();
    this.initializeSafTypeDescriptions();
  }

  public onQueryChanged(query: DataQuery<SafTypesFilter>): void {
    this.store.dispatch(SafTypesActions.search({ query }));
  }

  public onActionMenuClick(): void {
    const hasUpdatePermission = this.permissionsService.hasPermission(
      Permissions.updateSafTypes || Permissions.updateDocumentTypes
    );
    const hasRemovePermission = this.permissionsService.hasPermission(
      Permissions.deleteSafTypes || Permissions.deleteDocumentTypes
    );

    this.actions = [
      { action: GridTableActionType.SeeSafSeries, disabled: false },
      { action: GridTableActionType.Modify, disabled: !hasUpdatePermission },
      { action: GridTableActionType.Delete, disabled: !hasRemovePermission }
    ];
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: SafTypes }) {
    switch (action.actionType) {
      case GridTableActionType.Modify:
        this.store.dispatch(SafTypesActions.openApplyChanges({ value: row }));
        break;
      case GridTableActionType.Delete:
        this.store.dispatch(SafTypesActions.remove({ value: row }));
        break;
      case GridTableActionType.SeeSafSeries:
        this.navigateToSeriesWithFilters(action, row);
        break;
      default:
        break;
    }
  }

  public onDownload(): void {
    this.store.dispatch(SafTypesActions.download());
  }

  public createSafType(): void {
    this.store.dispatch(SafTypesActions.openAdd());
  }

  public getSafTypeDescriptions(): void {
    this.safDescriptionsSubject.next();
  }

  private checkPermissionAndLoadData(): void {
    if (
      this.permissionsService.hasPermission([
        Permissions.readSafTypes,
        Permissions.readDocumentTypes,
        Permissions.readDocumentSeries
      ])
    ) {
      this.dispatchSearch();
    }
  }

  private dispatchSearch(): void {
    this.query$.pipe(first()).subscribe(query => {
      this.store.dispatch(SafTypesActions.search({ query }));
    });
  }

  private navigateToSeriesWithFilters(action: GridTableAction, row: SafTypes): void {
    if (action.actionType === GridTableActionType.SeeSafSeries) {
      this.router.navigate([ROUTES.SAF_SERIES.url], {
        queryParams: {
          safType: row.type.toString().trim(),
          description: row.description
        }
      });
    }
  }

  private initializeSafTypeDescriptions(): void {
    // Subscribe to the Subject and use debounceTime to delay the API call
    this.safDescriptionsSubject
      .pipe(
        debounceTime(500), // Wait for 500ms before making the API call
        switchMap(() => this.safTypesListService.getDropdownOptions())
      )
      .subscribe(descriptions => {
        this.safTypeDescriptions = descriptions;
      });
  }

  private getEnumOptions(options: any, filterName: string): DropdownOption<SafTypeOptionForTranslate>[] {
    return Object.keys(options).map((key: string) => ({
      value: { translationKey: `${filterName}.options.${key}`, value: options[key] },
      label: this.displayFormatter.translate(`${filterName}.options.${key}`)
    }));
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<SafTypesFilter>({
      type: [],
      description: [],
      statisticalCode: []
    });
  }

  private initColumns(): void {
    this.columns = [
      {
        prop: 'type',
        name: 'LIST.MASTER_DATA.safTypes.columns.type',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'description',
        name: 'LIST.MASTER_DATA.safTypes.columns.description',
        width: 140,
        resizeable: true,
        draggable: false
      },
      {
        prop: 'statisticalCode',
        name: 'LIST.MASTER_DATA.safTypes.columns.statisticalCode',
        width: 140,
        resizeable: true,
        draggable: false
      }
    ];
  }
}
