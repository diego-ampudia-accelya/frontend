import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';

import { SafTypesFilterFormatter } from '../shared/saf-types-filter-formatter.service';
import { SafTypesListService } from '../shared/saf-types-list.service';
import * as fromSafTypes from '../store/reducers';
import * as fromSafTypesList from '../store/reducers/saf-types.reducer';

import { SafTypesListComponent } from './saf-types-list.component';
import { TranslatePipeMock } from '~app/test';
import { ListViewComponent } from '~app/shared/components/list-view';
import { GridTableComponent, InputComponent, SelectComponent } from '~app/shared/components';
import { PermissionsService } from '~app/auth/services/permissions.service';

describe('SafTypesListComponent', () => {
  let component: SafTypesListComponent;
  let fixture: ComponentFixture<SafTypesListComponent>;
  let mockStore: MockStore;
  const safTypesServiceSpy = createSpyObject(SafTypesListService);

  const permissionServiceMock = jasmine.createSpyObj<PermissionsService>('PermissionsService', ['hasPermission']);

  permissionServiceMock.hasPermission.and.returnValue(true);

  const translationSpy = createSpyObject(L10nTranslationService);

  const mockSafTypeDropdownOptions = [
    {
      value: {
        id: 698360,
        type: 'ACM',
        description: 'ACM'
      },
      label: 'ACM'
    },
    {
      value: {
        id: 698353,
        type: 'ACM',
        description: 'ACM 53'
      },
      label: 'ACM 53'
    }
  ];

  const initialState = fromSafTypesList.initialState;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SafTypesListComponent,
        TranslatePipeMock,
        MockComponents(InputComponent, ListViewComponent, GridTableComponent, SelectComponent)
      ],
      providers: [
        SafTypesFilterFormatter,
        FormBuilder,
        { provide: PermissionsService, useValue: permissionServiceMock },
        { provide: SafTypesListService, useValue: safTypesServiceSpy },
        { provide: L10nTranslationService, useValue: translationSpy },
        provideMockStore({
          selectors: [
            { selector: fromSafTypes.getListQuery, value: initialState.query },
            { selector: fromSafTypes.getListData, value: initialState.data },
            { selector: fromSafTypes.getListLoading, value: initialState.loading }
          ]
        })
      ],
      imports: [ReactiveFormsModule, RouterTestingModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SafTypesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');

    safTypesServiceSpy.getDropdownOptions.and.returnValue(of(mockSafTypeDropdownOptions));
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
