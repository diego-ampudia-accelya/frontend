import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SafTypes } from '~app/master-data/saf-types/shared/saf-types.models';
import { FooterButton } from '~app/shared/components';

@Injectable()
export class ChangesDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(question: { title: string; details: string }, item?: SafTypes): any {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: item ? [{ type: FooterButton.Modify }] : [{ type: FooterButton.Create }]
      },
      item,
      message
    };
  }
}
