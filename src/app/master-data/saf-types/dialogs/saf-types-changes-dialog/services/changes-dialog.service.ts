import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { SafTypesListService } from '../../../shared/saf-types-list.service';
import { SafTypes, SafTypesFilter } from '../../../shared/saf-types.models';
import { translations } from '../../translations';
import { SafTypesChangeDialogComponent } from '../saf-types-changes-dialog/saf-types-changes-dialog.component';

import { ChangesDialogConfigService } from './changes-dialog-config.service';
import { DialogService } from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private configBuilder: ChangesDialogConfigService,
    private dataService: SafTypesListService
  ) {}

  public open(item?: SafTypes): Observable<any> {
    const buildTranslations = item ? translations.edit : translations.create;
    const dialogConfig = this.configBuilder.build(buildTranslations, item);

    return this.dialogService.open(SafTypesChangeDialogComponent, dialogConfig);
  }

  public edit(item: SafTypes): Observable<SafTypes> {
    return this.dataService.save(item);
  }

  public create(item: SafTypesFilter): Observable<SafTypes> {
    return this.dataService.create(item);
  }
}
