import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { isEqual, omit } from 'lodash';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import {
  SafType,
  SafTypeOptionForTranslate,
  SafTypes,
  SafTypesFilter,
  StatisticalCode
} from '../../../shared/saf-types.models';
import { SafTypesFilterFormatter } from '~app/master-data/saf-types/shared/saf-types-filter-formatter.service';
import { SafTypesActions } from '~app/master-data/saf-types/store/actions';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-saf-types-changes-dialog',
  templateUrl: './saf-types-changes-dialog.component.html',
  styleUrls: ['./saf-types-changes-dialog.component.scss']
})
export class SafTypesChangeDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;
  public typeControl: FormControl;
  public descriptionControl: FormControl;
  public statisticalCodeControl: FormControl;
  public isEditMode = false;
  public safTypesOptions = this.getEnumOptions(SafType, 'type');
  public statisticalCodeOptions = this.getEnumOptions(StatisticalCode, 'statisticalCode');

  private updateButton: ModalAction;
  private createButton: ModalAction;
  private item: SafTypes = this.config.item;

  private destroy$ = new Subject();

  private updateButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Modify),
    takeUntil(this.destroy$)
  );

  private createButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Create),
    takeUntil(this.destroy$)
  );

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private reactiveSubject: ReactiveSubject,
    public store: Store<AppState>,
    public displayFormatter: SafTypesFilterFormatter
  ) {}

  public ngOnInit(): void {
    this.buildForm();
    this.initializeButtons();
    this.setItemsFeatures();
    this.initializeFormListeners();
  }

  public ngAfterViewInit(): void {
    this.subscribeToButtonStatus();
  }

  private subscribeToButtonStatus(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const isFormInvalid = status !== 'VALID';

      if (!this.isEditMode) {
        this.setIsDisabledForSaveButton(isFormInvalid);
      } else {
        const formValue = {
          type: this.form.value.type?.value,
          description: this.form.value.description,
          statisticalCode: this.form.value.statisticalCode?.value
        };

        const hasNoChangesInForm = isEqual(formValue, omit(this.item, 'id'));
        this.setIsDisabledForSaveButton(isFormInvalid || hasNoChangesInForm);
      }
    });
  }

  private setIsDisabledForSaveButton(isDisabled: boolean): void {
    if (this.updateButton) {
      this.updateButton.isDisabled = isDisabled;
    }

    if (this.createButton) {
      this.createButton.isDisabled = isDisabled;
    }
  }

  private buildForm(): void {
    this.typeControl = new FormControl(null, Validators.required);
    this.descriptionControl = new FormControl(null, Validators.required);
    this.statisticalCodeControl = new FormControl(null, Validators.required);

    this.form = this.formBuilder.group({
      type: this.typeControl,
      description: this.descriptionControl,
      statisticalCode: this.statisticalCodeControl
    });
  }

  private initializeButtons(): void {
    this.updateButton = this.config.data.buttons.find(button => button.type === FooterButton.Modify);
    this.createButton = this.config.data.buttons.find(button => button.type === FooterButton.Create);

    if (this.updateButton) {
      this.updateButton.isDisabled = true;
    }
    if (this.createButton) {
      this.createButton.isDisabled = true;
    }
  }

  private initializeFormListeners(): void {
    this.updateButtonClick$.subscribe(() => {
      const isDescriptionChanged = this.item.description !== this.descriptionControl.value;
      const isStatisticalCodeChanged = this.item.statisticalCode !== this.statisticalCodeControl.value?.value;

      if (isDescriptionChanged || isStatisticalCodeChanged) {
        const valueToDispatch: SafTypes = {
          ...this.item,
          description: this.descriptionControl.value,
          statisticalCode: this.statisticalCodeControl.value?.value
        };
        this.store.dispatch(SafTypesActions.applyChanges({ value: valueToDispatch }));
      }
    });

    this.createButtonClick$.subscribe(() => {
      const valueToDispatch: SafTypesFilter = {
        ...this.item,
        type: this.typeControl.value?.value,
        description: this.descriptionControl.value,
        statisticalCode: this.statisticalCodeControl.value?.value
      };
      this.store.dispatch(SafTypesActions.add({ value: valueToDispatch }));
    });
  }

  private setItemsFeatures() {
    this.isEditMode = !!this.item;

    if (this.item) {
      const safType = this.getDropdownValue('type', this.item.type.toString());
      const statisticalCode = this.getDropdownValue('statisticalCode', this.item.statisticalCode.toString());

      this.typeControl.patchValue(safType);
      this.descriptionControl.patchValue(this.item.description);
      this.statisticalCodeControl.patchValue(statisticalCode);
    }
  }

  private getDropdownValue(key: string, value: string): SafTypeOptionForTranslate {
    const translationKey = `${key}.options.${value.toLowerCase()}`;

    return { translationKey, value };
  }

  private getEnumOptions(options: any, filterName: string): DropdownOption<SafTypeOptionForTranslate>[] {
    return Object.keys(options).map((key: string) => ({
      value: { translationKey: `${filterName}.options.${key}`, value: options[key] },
      label: this.displayFormatter.translate(`${filterName}.options.${key}`)
    }));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
