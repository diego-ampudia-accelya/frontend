export const translations = {
  delete: {
    title: 'LIST.MASTER_DATA.safTypes.dialog.delete.title',
    details: 'LIST.MASTER_DATA.safTypes.dialog.delete.message'
  },
  edit: {
    title: 'LIST.MASTER_DATA.safTypes.dialog.edit.title',
    details: 'LIST.MASTER_DATA.safTypes.dialog.edit.message'
  },
  create: {
    title: 'LIST.MASTER_DATA.safTypes.dialog.create.title',
    details: 'LIST.MASTER_DATA.safTypes.dialog.create.message'
  }
};
