import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { SafTypes } from '../../shared/saf-types.models';
import { ChangeModel, ChangesDialogConfig, FooterButton } from '~app/shared/components';

@Injectable()
export class DeleteDialogConfigService {
  constructor(private translation: L10nTranslationService) {}

  public build(item: SafTypes, question: { title: string; details: string }): ChangesDialogConfig {
    const message = this.translation.translate(question.details);

    return {
      data: {
        title: question.title,
        footerButtonsType: [{ type: FooterButton.Delete }]
      },
      changes: this.formatChanges(item),
      message
    };
  }

  private formatChanges(item: SafTypes): ChangeModel[] {
    return [
      {
        group: '',
        name: this.translation.translate('LIST.MASTER_DATA.safTypes.dialog.edit.type.label'),
        value: `${item.type}`
      },
      {
        group: '',
        name: this.translation.translate('LIST.MASTER_DATA.safTypes.dialog.edit.description.label'),
        value: `${item.description}`
      },
      {
        group: '',
        name: this.translation.translate('LIST.MASTER_DATA.safTypes.dialog.edit.statisticalCode.label'),
        value: `${item.statisticalCode}`
      }
    ];
  }
}
