import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'rxjs';

import { SafType, SafTypes, StatisticalCode } from '../../shared/saf-types.models';

import { DeleteDialogConfigService } from './delete-dialog-config.service';
import { FooterButton } from '~app/shared/components';

describe('DeleteDialogConfigService', () => {
  let service: DeleteDialogConfigService;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  const type = {
    translationKey: 'test',
    value: SafType.adm
  };

  const description = {
    id: 698353,
    type: SafType.adm,
    description: 'ACM 53'
  };

  const statisticalCode = {
    translationKey: 'test',
    value: StatisticalCode.int
  };

  const item: SafTypes = {
    id: 999,
    type: [type],
    description: [description],
    statisticalCode: [statisticalCode]
  };

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    service = new DeleteDialogConfigService(translationServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should format item changes correctly', () => {
    const format = service['formatChanges'](item);

    service['formatChanges'](item);

    expect(format).toEqual([
      {
        group: '',
        name: 'LIST.MASTER_DATA.safTypes.dialog.edit.type.label',
        value: `${type}`
      },
      {
        group: '',
        name: 'LIST.MASTER_DATA.safTypes.dialog.edit.description.label',
        value: `${description}`
      },
      {
        group: '',
        name: 'LIST.MASTER_DATA.safTypes.dialog.edit.statisticalCode.label',
        value: `${statisticalCode}`
      }
    ]);
  });

  it('should build changes dialog configuration', () => {
    const question = { title: 'title', details: 'details' };

    service.build(item, question);

    expect(service.build(item, question)).toEqual({
      data: { title: question.title, footerButtonsType: [{ type: FooterButton.Delete }] },
      changes: [
        {
          group: '',
          name: 'LIST.MASTER_DATA.safTypes.dialog.edit.type.label',
          value: `${type}`
        },
        {
          group: '',
          name: 'LIST.MASTER_DATA.safTypes.dialog.edit.description.label',
          value: `${description}`
        },
        {
          group: '',
          name: 'LIST.MASTER_DATA.safTypes.dialog.edit.statisticalCode.label',
          value: `${statisticalCode}`
        }
      ],
      message: question.details
    });
  });
});
