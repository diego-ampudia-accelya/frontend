import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { ADDRESS_MAINTENANCE_ENDPOINTS } from '../endpoints/address-maintenance-endpoints.config';
import {
  AddressGroupModel,
  AddressMaintenanceModel,
  AddressMaintenanceResourceType
} from '../models/address-maintenance.model';
import { AddressMaintenanceState } from '../store/reducers/address-maintenance.reducer';

import { AddressMaintenanceService } from './address-maintenance.service';
import { AppConfigurationService } from '~app/shared/services';
import * as fromAddressMaintenance from '~app/master-data/address-maintenance/store/reducers/address-maintenance.reducer';

const mockAddress: AddressGroupModel = {
  address1: '',
  address2: '',
  city: '',
  comment: '',
  contactName: '',
  country: '',
  email: '',
  locality: '',
  phoneFaxNumber: '',
  postCode: '',
  state: '',
  telephone: ''
};

const mockAddressMaintenance: AddressMaintenanceModel = {
  id: null,
  iataCode: '',
  name: '',
  sameAddressForAllCategories: false,
  generic: mockAddress,
  acdm: mockAddress,
  refund: mockAddress
};

const initialState: { addressMaintenance: AddressMaintenanceState } = {
  addressMaintenance: {
    resourceId: '',
    resourceType: null,
    isReadonlyMode: false,
    originalValue: mockAddressMaintenance,
    value: mockAddressMaintenance,
    isChanged: false,
    isLoading: false,
    isInvalid: false,
    errors: null
  }
};

describe('AddressMaintenanceService', () => {
  let service: AddressMaintenanceService;
  let store: MockStore;
  // We can also use the HttpTestController but in this case it is easier to use a test double
  let httpStub: SpyObject<HttpClient>;
  // let mockResourceUrlSelector;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AddressMaintenanceService,
        AppConfigurationService,
        mockProvider(HttpClient),
        provideMockStore({
          // If we are mocking all selectors it is no longer necessary to have an initial state
          initialState
        })
      ]
    });

    store = TestBed.inject(MockStore);
    service = TestBed.inject(AddressMaintenanceService);
    httpStub = TestBed.inject<any>(HttpClient);
    httpStub.get.and.returnValue(of());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should build resourceUrl from Store when calling get', fakeAsync(() => {
    // Ultimately we want to make sure that the get method uses the correct base url
    // To test this it is enough to mock the result of getResourceId and getResourceType selector and call the get method
    // In this way, we are interacting only with the public interface of the service without touching internal implementation details.
    const resourceType = AddressMaintenanceResourceType.airline;
    const resourceId = '1';
    const mockResourceUrl = ADDRESS_MAINTENANCE_ENDPOINTS[resourceType]({ userId: resourceId });
    store.overrideSelector(fromAddressMaintenance.getResourceId, resourceId);
    store.overrideSelector(fromAddressMaintenance.getResourceType, resourceType);
    store.refreshState();

    service.get().subscribe();
    tick();
    expect(httpStub.get).toHaveBeenCalledWith(mockResourceUrl);
  }));
});
