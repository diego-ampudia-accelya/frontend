import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { skipWhile, withLatestFrom } from 'rxjs/operators';
import { CacheBuster } from 'ts-cacheable';

import { ADDRESS_MAINTENANCE_ENDPOINTS } from '../endpoints/address-maintenance-endpoints.config';
import { AddressMaintenanceModel } from '~app/master-data/address-maintenance/models/address-maintenance.model';
import * as fromAddressMaintenance from '~app/master-data/address-maintenance/store/reducers/address-maintenance.reducer';
import { AppState } from '~app/reducers';
import { contactInfoCacheBuster$ } from '~app/shared/helpers/cacheable-helper';

@Injectable()
export class AddressMaintenanceService {
  private baseUrl: string;

  constructor(private http: HttpClient, private store: Store<AppState>) {
    this.buildBaseUrl();
  }

  private buildBaseUrl() {
    this.store
      .pipe(
        select(fromAddressMaintenance.getResourceId),
        withLatestFrom(this.store.pipe(select(fromAddressMaintenance.getResourceType))),
        skipWhile(values => values.includes(null) || values.includes(undefined))
      )
      .subscribe(
        ([resourceId, resourceType]) =>
          (this.baseUrl = ADDRESS_MAINTENANCE_ENDPOINTS[resourceType]({ userId: resourceId }))
      );
  }

  public get(): Observable<AddressMaintenanceModel> {
    return this.http.get<AddressMaintenanceModel>(this.baseUrl);
  }

  @CacheBuster({ cacheBusterNotifier: contactInfoCacheBuster$ })
  public save(addressModel: AddressMaintenanceModel): Observable<AddressMaintenanceModel> {
    return this.http.put<AddressMaintenanceModel>(this.baseUrl, addressModel);
  }
}
