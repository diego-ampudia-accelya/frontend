import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ChangesDialogService } from './changes-dialog/changes-dialog.service';
import { AddressMaintenanceService } from './services/address-maintenance.service';
import { fromAddressMaintenance } from './store';
import { AddressMaintenanceComponent } from '~app/master-data/address-maintenance/address-maintenance/address-maintenance.component';
import { AddressMaintenanceEffects } from '~app/master-data/address-maintenance/store/effects/address-maintenance.effects';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [AddressMaintenanceComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAddressMaintenance.addressMaintenanceKey, fromAddressMaintenance.reducer),
    EffectsModule.forFeature([AddressMaintenanceEffects]),
    SharedModule
  ],
  providers: [AddressMaintenanceService, ChangesDialogService],
  exports: [AddressMaintenanceComponent]
})
export class AddressMaintenanceModule {}
