import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';

import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { AddressGroupModel, AddressMaintenanceModel } from '../models/address-maintenance.model';
import { fromAddressMaintenance } from '../store';
import { AddressMaintenanceComponent } from './address-maintenance.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { CheckboxComponent, InputComponent, TextareaComponent } from '~app/shared/components';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { AlertMessageComponent } from '~app/shared/components/alert-message/alert-message.component';
import { FormUtil } from '~app/shared/helpers';
import { UserType } from '~app/shared/models/user.model';
import { TranslatePipeMock } from '~app/test';

const mockAddress: AddressGroupModel = {
  address1: '',
  address2: '',
  city: '',
  comment: '',
  contactName: '',
  country: '',
  email: '',
  locality: '',
  phoneFaxNumber: '',
  postCode: '',
  state: '',
  telephone: ''
};

const mockAddressMaintenance: AddressMaintenanceModel = {
  id: null,
  iataCode: '',
  name: '',
  sameAddressForAllCategories: false,
  generic: mockAddress,
  acdm: mockAddress,
  refund: mockAddress
};

const initialState = {
  addressMaintenance: {
    id: null,
    resourceType: null,
    isReadonlyMode: false,
    originalValue: mockAddressMaintenance,
    value: mockAddressMaintenance,
    isChanged: false,
    isLoading: false,
    isInvalid: false,
    errors: null
  }
};

const expectedUserDetails = {
  id: 10126,
  email: 'iata@example.com',
  firstName: 'firstName',
  lastName: 'lastName',
  userType: UserType.AIRLINE,
  permissions: [],
  bspPermissions: [],
  bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
};

describe('AddressMaintenanceComponent', () => {
  let component: AddressMaintenanceComponent;
  let fixture: ComponentFixture<AddressMaintenanceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AddressMaintenanceComponent,
        TranslatePipeMock,
        MockComponents(
          AlertMessageComponent,
          AccordionComponent,
          AccordionItemComponent,
          CheckboxComponent,
          InputComponent,
          TextareaComponent
        )
      ],
      providers: [
        FormBuilder,
        {
          provide: ChangesDialogService,
          useValue: {
            confirmUnsavedChanges: () => of({})
          }
        },
        provideMockStore({
          initialState,
          selectors: [{ selector: fromAuth.getUser, value: expectedUserDetails }]
        })
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should propagate changes in generic section when sameAddressForAllCategories', fakeAsync(() => {
    const testAddress = 'Testing address replication';
    const acdmAddress1 = component.form.get('acdm.address1');
    const refundAddress1 = component.form.get('refund.address1');

    component.form.get('generic.address1').setValue(testAddress, { emitEvent: false });
    component.form.get('sameAddressForAllCategories').setValue(true);
    tick();

    expect(acdmAddress1.value).toBe(testAddress);
    expect(refundAddress1.value).toBe(testAddress);
  }));

  it('should not propagate changes in generic section when sameAddressForAllCategories is false', fakeAsync(() => {
    component.form.get('sameAddressForAllCategories').setValue(false);
    tick();

    const testAddress = 'Testing address replication';
    const acdmAddress1 = component.form.get('acdm.address1');
    const refundAddress1 = component.form.get('refund.address1');

    const acdmAddress1Value = acdmAddress1.value;
    const refundAddress1Value = refundAddress1.value;

    component.form.get('generic.address1').setValue(testAddress, { emitEvent: false });

    expect(acdmAddress1.value).toBe(acdmAddress1Value);
    expect(refundAddress1.value).toBe(refundAddress1Value);
  }));

  it('should keep only generic section enabled when sameAddressForAllCategories', fakeAsync(() => {
    const acdmSection = component.form.get('acdm');
    const refundSection = component.form.get('refund');

    component.form.get('sameAddressForAllCategories').setValue(true);
    tick();

    expect(acdmSection.enabled).toBe(false);
    expect(refundSection.enabled).toBe(false);
  }));

  it('should enable all sections when sameAddressForAllCategories is false', fakeAsync(() => {
    const acdmSection = component.form.get('acdm');
    const refundSection = component.form.get('refund');

    component.form.get('sameAddressForAllCategories').setValue(true);
    tick();
    expect(acdmSection.enabled).toBe(false);
    expect(refundSection.enabled).toBe(false);

    component.form.get('sameAddressForAllCategories').setValue(false);
    tick();
    expect(acdmSection.enabled).toBe(true);
    expect(refundSection.enabled).toBe(true);
  }));

  it('should discard changes', fakeAsync(() => {
    const store = TestBed.inject(MockStore);
    const testEmail = 'test@test.com';

    const sameAddressControl = component.form.get('sameAddressForAllCategories');
    const acdmSectionEmail = component.form.get('acdm.email');
    const refundSectionEmail = component.form.get('refund.email');

    acdmSectionEmail.setValue(testEmail);
    refundSectionEmail.setValue(testEmail);
    sameAddressControl.setValue(true);
    store.overrideSelector(fromAddressMaintenance.getAddressMaintenance, component.form.value);
    store.refreshState();

    expect(acdmSectionEmail.value).toBe(mockAddress.email);
    expect(refundSectionEmail.value).toBe(mockAddress.email);

    //* Simulating discard action
    store.overrideSelector(fromAddressMaintenance.getAddressMaintenance, initialState.addressMaintenance.originalValue);
    store.refreshState();

    expect(acdmSectionEmail.value).toBe(initialState.addressMaintenance.originalValue.acdm.email);
    expect(refundSectionEmail.value).toBe(initialState.addressMaintenance.originalValue.refund.email);
    expect(sameAddressControl.value).toBe(initialState.addressMaintenance.originalValue.sameAddressForAllCategories);
  }));

  describe('handleInvalidFields', () => {
    let store;

    beforeEach(() => {
      (component as any).scrollToFirstInvalidSection = jasmine.createSpy();
      FormUtil.processErrorsOnForm = jasmine.createSpy();
      FormUtil.processErrorsOnForm = jasmine.createSpy();
      spyOn(FormUtil, 'showControlState');
      store = TestBed.inject(MockStore);

      (component as any).handleInvalidFields();
    });

    it('should NOT call scrollToFirstInvalidSection when error is null', fakeAsync(() => {
      store.overrideSelector(fromAddressMaintenance.getErrors, null);
      store.refreshState();
      tick();

      expect((component as any).scrollToFirstInvalidSection).not.toHaveBeenCalled();
    }));

    it('should NOT call processErrorsOnForm when error is null', fakeAsync(() => {
      store.overrideSelector(fromAddressMaintenance.getErrors, null);
      store.refreshState();
      tick();

      expect(FormUtil.processErrorsOnForm).not.toHaveBeenCalled();
    }));

    it('should NOT call showControlState when error is null', fakeAsync(() => {
      store.overrideSelector(fromAddressMaintenance.getErrors, null);
      store.refreshState();
      tick();

      expect(FormUtil.showControlState).not.toHaveBeenCalled();
    }));

    it('should call scrollToFirstInvalidSection when error is NOT null', fakeAsync(() => {
      store.overrideSelector(fromAddressMaintenance.getErrors, { error: {} });
      store.refreshState();
      tick();

      expect((component as any).scrollToFirstInvalidSection).toHaveBeenCalled();
    }));

    it('should call processErrorsOnForm when error is NOT null', fakeAsync(() => {
      component.form = {
        value: 'test'
      } as any;
      store.overrideSelector(fromAddressMaintenance.getErrors, { error: { code: 500 } });
      store.refreshState();
      tick();

      expect(FormUtil.processErrorsOnForm).toHaveBeenCalledWith({ code: 500 }, { value: 'test' });
    }));
  });
});
