import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, first, map, takeUntil } from 'rxjs/operators';

import { ChangesDialogService } from '../changes-dialog/changes-dialog.service';
import { AddressMaintenanceActions } from '../store';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { CanComponentDeactivate } from '~app/core/services';
import {
  AddressGroupModel,
  AddressMaintenanceModel,
  AddressMaintenanceResourceType
} from '~app/master-data/address-maintenance/models/address-maintenance.model';
import {
  getAddressMaintenance,
  getErrors,
  getIsReadonlyMode,
  getResourceType
} from '~app/master-data/address-maintenance/store/reducers/address-maintenance.reducer';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { AlertMessageType } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { User, UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-address-maintenance',
  templateUrl: './address-maintenance.component.html',
  styleUrls: ['./address-maintenance.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddressMaintenanceComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  @ViewChild(AccordionComponent, { static: true }) accordion: AccordionComponent;
  @ViewChildren(AccordionItemComponent) accordionSections: QueryList<AccordionItemComponent>;

  public form: FormGroup;
  public contactInfoData: AddressMaintenanceModel;
  public sameAddressControl: FormControl;
  public isReadonlyMode: boolean;
  public isFormReady = false;

  public sectionsMapper: { [key in keyof Partial<AddressMaintenanceModel>]: string } = {
    generic: 'generic',
    acdm: 'acdm',
    refund: 'refund'
  };

  public genericSectionGroupName: keyof AddressMaintenanceModel;
  public acdmSectionGroupName: keyof AddressMaintenanceModel;
  public refundsSectionGroupName: keyof AddressMaintenanceModel;

  public resourceName: string;
  public resourceLabel: string;
  public iataCode: string;

  public infoMessage: { message: string; type: AlertMessageType };
  public airlineInfoMessage: { message: string; type: AlertMessageType };
  public agentInfoMessage: { message: string; type: AlertMessageType };

  public isAirlineUser: boolean;
  public isAgentUser: boolean;

  public get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private $destroy = new Subject();
  private formUtil: FormUtil;

  constructor(
    private fb: FormBuilder,
    private changesDialogService: ChangesDialogService,
    private store: Store<AppState>,
    private cd: ChangeDetectorRef
  ) {
    this.formUtil = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.initializeSectionNames();

    this.store.pipe(select(getIsReadonlyMode), first()).subscribe(isReadonlyMode => {
      this.isReadonlyMode = isReadonlyMode;

      if (this.isReadonlyMode) {
        this.initializeViewData();
      } else {
        this.initializeAlertInfoMessage();
        this.buildForm();
        this.initializeFormData();
        this.initializeFormChangesEmitter();
        this.initializeSameAddressListener();
      }

      this.initializeResourceValues();
      this.initializeIataCode();
    });

    this.handleInvalidFields();
    this.initializeLoggedUser();
  }

  public canDeactivate(): Observable<boolean> {
    return this.changesDialogService.confirmUnsavedChanges().pipe(map(result => result !== FooterButton.Cancel));
  }

  public isSectionInvalid(sectionId: string): boolean {
    const formSection = Object.keys(this.sectionsMapper).find(id => this.sectionsMapper[id] === sectionId);

    return this.isFormReady && this.form.get(formSection as keyof AddressMaintenanceModel)?.invalid;
  }

  public initializeLoggedUser(): void {
    this.loggedUser$.pipe(takeUntil(this.$destroy)).subscribe(loggedUser => {
      this.isAirlineUser = loggedUser.userType === UserType.AIRLINE;
      this.isAgentUser = loggedUser.userType === UserType.AGENT;
    });
  }

  private initializeIataCode() {
    this.iataCode = this.isReadonlyMode
      ? this.contactInfoData.iataCode
      : FormUtil.get<AddressMaintenanceModel>(this.form, 'iataCode').value;
  }

  private initializeResourceValues() {
    if (this.isReadonlyMode) {
      this.resourceName = this.contactInfoData.name;
    } else {
      this.resourceName = FormUtil.get<AddressMaintenanceModel>(this.form, 'name').value;
    }

    this.store.pipe(select(getResourceType), first()).subscribe(resourceType => {
      if (resourceType === AddressMaintenanceResourceType.agent) {
        this.resourceLabel = 'LIST.MASTER_DATA.addressMaintenance.sections.labels.agentName';
      }

      if (resourceType === AddressMaintenanceResourceType.airline) {
        this.resourceLabel = 'LIST.MASTER_DATA.addressMaintenance.sections.labels.airlineName';
      }
    });
  }

  private buildForm() {
    this.sameAddressControl = this.fb.control(false);

    this.form = this.formUtil.createGroup<AddressMaintenanceModel>({
      id: new FormControl(),
      name: new FormControl(),
      iataCode: new FormControl(),
      sameAddressForAllCategories: this.sameAddressControl,
      generic: this.getAddressFormGroup(),
      acdm: this.getAddressFormGroup(),
      refund: this.getAddressFormGroup()
    });
  }

  private initializeFormData() {
    this.isFormReady = false;
    this.getContactInfo$()
      .pipe(distinctUntilChanged(), takeUntil(this.$destroy))
      .subscribe(value => {
        this.updateForm(value);
        this.isFormReady = true;
        this.cd.markForCheck();
      });
  }

  private initializeViewData() {
    this.getContactInfo$()
      .pipe(first())
      .subscribe(value => (this.contactInfoData = value));
  }

  private getContactInfo$(): Observable<AddressMaintenanceModel> {
    return this.store.select(getAddressMaintenance);
  }

  private enableSecondarySections() {
    FormUtil.get<AddressMaintenanceModel>(this.form, 'acdm').enable({ emitEvent: false });
    FormUtil.get<AddressMaintenanceModel>(this.form, 'refund').enable({ emitEvent: false });
  }

  private initializeFormChangesEmitter() {
    this.form.valueChanges
      .pipe(takeUntil(this.$destroy))
      .subscribe(value => this.store.dispatch(AddressMaintenanceActions.modify({ value })));

    // To change other forms on generic form change when same address field is checked
    FormUtil.get<AddressMaintenanceModel>(this.form, 'generic')
      .valueChanges.pipe(
        filter(() => this.sameAddressControl.value),
        takeUntil(this.$destroy)
      )
      .subscribe(() => this.toggleGenericAddressSection(true));
  }

  private initializeSameAddressListener() {
    this.sameAddressControl.valueChanges
      .pipe(takeUntil(this.$destroy))
      .subscribe(sameAddress => this.toggleGenericAddressSection(sameAddress));
  }

  private initializeSectionNames() {
    this.genericSectionGroupName = 'generic';
    this.acdmSectionGroupName = 'acdm';
    this.refundsSectionGroupName = 'refund';
  }

  private initializeAlertInfoMessage() {
    this.infoMessage = {
      message: 'LIST.MASTER_DATA.addressMaintenance.alertInfo',
      type: AlertMessageType.info
    };
    this.airlineInfoMessage = {
      message: 'LIST.MASTER_DATA.addressMaintenance.airlineAlertInfo',
      type: AlertMessageType.info
    };
    this.agentInfoMessage = {
      message: 'LIST.MASTER_DATA.addressMaintenance.agentAlertInfo',
      type: AlertMessageType.info
    };
  }

  private updateForm(value: AddressMaintenanceModel) {
    this.enableSecondarySections();
    this.form.patchValue(value, { emitEvent: false });
    this.toggleGenericAddressSection(value.sameAddressForAllCategories, false);
  }

  private toggleGenericAddressSection(useGenericAddress: boolean, emitEvent = true) {
    const genericGroup = FormUtil.get<AddressMaintenanceModel>(this.form, 'generic');
    const acdmGroup = FormUtil.get<AddressMaintenanceModel>(this.form, 'acdm');
    const refundGroup = FormUtil.get<AddressMaintenanceModel>(this.form, 'refund');

    // We always enable groups to be able to patch them
    acdmGroup.enable({ emitEvent: false });
    refundGroup.enable({ emitEvent: false });

    if (useGenericAddress) {
      acdmGroup.patchValue(genericGroup.value, { emitEvent: false });
      refundGroup.patchValue(genericGroup.value, { emitEvent: false });

      acdmGroup.disable({ emitEvent: false });
      refundGroup.disable({ emitEvent: false });
    }

    if (emitEvent) {
      this.form.updateValueAndValidity();
    }
  }

  private getAddressFormGroup(): FormGroup {
    return this.formUtil.createGroup<AddressGroupModel>({
      address1: '',
      address2: '',
      locality: '',
      city: '',
      state: '',
      country: '',
      postCode: '',
      telephone: '',
      contactName: '',
      phoneFaxNumber: '',
      email: '',
      comment: ''
    });
  }

  private handleInvalidFields(): void {
    this.store
      .pipe(select(getErrors))
      .pipe(
        filter(errors => !!errors?.error),
        takeUntil(this.$destroy)
      )
      .subscribe(errors => {
        FormUtil.processErrorsOnForm(errors.error, this.form);
        this.scrollToFirstInvalidSection();
        this.cd.markForCheck();
      });
  }

  private scrollToFirstInvalidSection(): void {
    const sectionWithError = this.accordionSections.find(section => {
      const id = section.nativeElement.id;

      return Object.keys(this.sectionsMapper).some(
        formMapped => this.sectionsMapper[formMapped] === id && this.form.get(formMapped as any).invalid
      );
    });

    if (sectionWithError) {
      this.accordion.goToItem(sectionWithError);
    }
  }

  public ngOnDestroy() {
    this.$destroy.next();
  }
}
