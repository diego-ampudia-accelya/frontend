import { createAction, props } from '@ngrx/store';

import {
  AddressMaintenanceModel,
  AddressMaintenanceResourceType
} from '~app/master-data/address-maintenance/models/address-maintenance.model';

export const modify = createAction('[Address Maintenance] Modify', props<{ value: AddressMaintenanceModel }>());

export const load = createAction(
  '[Address Maintenance] Load',
  props<{ resourceType: AddressMaintenanceResourceType; resourceId: string; isReadonlyMode: boolean }>()
);

export const loadSuccess = createAction(
  '[Address Maintenance] Load Success',
  props<{ value: AddressMaintenanceModel }>()
);

export const loadError = createAction('[Address Maintenance] Load Error');

export const invalid = createAction('[Address Maintenance] Invalid');

export const openApplyChanges = createAction('[Address Maintenance] Open Apply Changes');

export const applyChangesSuccess = createAction(
  '[Address Maintenance] Apply Changes Success',
  props<{ value: AddressMaintenanceModel }>()
);

export const applyChangesError = createAction('[Address Maintenance] Apply Changes Error', props<{ error }>());

export const discard = createAction('[Address Maintenance] Discard');
