import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { isEqual } from 'lodash';

import {
  AddressMaintenanceModel,
  AddressMaintenanceResourceType
} from '~app/master-data/address-maintenance/models/address-maintenance.model';
import * as AddressMaintenanceActions from '~app/master-data/address-maintenance/store/actions/address-maintenance.actions';
import { AppState } from '~app/reducers';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';

export const addressMaintenanceKey = 'addressMaintenance';

export interface State extends AppState {
  addressMaintenance: AddressMaintenanceState;
}

export interface AddressMaintenanceState {
  resourceType: AddressMaintenanceResourceType;
  resourceId: string;
  originalValue: AddressMaintenanceModel;
  value: AddressMaintenanceModel;
  isReadonlyMode: boolean;
  isChanged: boolean;
  isLoading: boolean;
  isInvalid: boolean;
  errors: { error: ResponseErrorBE };
}

const initialState: AddressMaintenanceState = {
  resourceType: null,
  resourceId: null,
  originalValue: null,
  value: null,
  isReadonlyMode: false,
  isChanged: false,
  isLoading: false,
  isInvalid: false,
  errors: null
};

export const reducer = createReducer(
  initialState,
  on(AddressMaintenanceActions.load, (state, { resourceType, resourceId, isReadonlyMode }) => ({
    ...state,
    isReadonlyMode,
    resourceType,
    resourceId,
    isLoading: true
  })),
  on(AddressMaintenanceActions.loadSuccess, (state, { value }) => ({
    ...state,
    isLoading: false,
    value,
    originalValue: value
  })),
  on(AddressMaintenanceActions.loadError, state => ({
    ...state,
    originalValue: null,
    value: null,
    isChanged: false,
    isLoading: false,
    isInvalid: false,
    errors: null
  })),
  on(AddressMaintenanceActions.modify, (state, { value }) => ({
    ...state,
    value: changeValue(state.value, value),
    isChanged: !isEqual(state.originalValue, { ...state.value, ...value }),
    isInvalid: false,
    errors: null
  })),
  on(AddressMaintenanceActions.invalid, state => ({
    ...state,
    isInvalid: true
  })),
  on(AddressMaintenanceActions.discard, state => ({
    ...state,
    isChanged: false,
    value: state.originalValue
  })),
  on(AddressMaintenanceActions.applyChangesSuccess, (state, { value }) => ({
    ...state,
    value,
    originalValue: value,
    isChanged: false,
    isInvalid: false,
    errors: null
  })),
  on(AddressMaintenanceActions.applyChangesError, (state, { error }) => ({
    ...state,
    isChanged: !error,
    isInvalid: !!error,
    errors: error
  }))
);

export const getAddressMaintenanceState = createFeatureSelector<State, AddressMaintenanceState>(addressMaintenanceKey);
export const getAddressMaintenance = createSelector(getAddressMaintenanceState, state => state.value);
export const getResourceType = createSelector(getAddressMaintenanceState, state => state.resourceType);
export const getResourceId = createSelector(getAddressMaintenanceState, state => state.resourceId);
export const getIsReadonlyMode = createSelector(getAddressMaintenanceState, state => state.isReadonlyMode);
export const getErrors = createSelector(getAddressMaintenanceState, state => state.errors);

const getFormChanges = ({ value, originalValue }: AddressMaintenanceState) => {
  const modifications: Array<keyof AddressMaintenanceModel> = [];
  const editableSections: Array<keyof AddressMaintenanceModel> = [
    'acdm',
    'generic',
    'refund',
    'sameAddressForAllCategories'
  ];

  if (value && originalValue) {
    Object.keys(value)
      .filter(sectionKey => {
        if (editableSections.includes(sectionKey as keyof AddressMaintenanceModel)) {
          // sameAddressForAllCategories is not an object so this check is to control this case (and future ones)
          return typeof value[sectionKey] === 'object'
            ? Object.keys(value[sectionKey]).some(
                property => value[sectionKey][property] !== originalValue[sectionKey][property]
              )
            : value[sectionKey] !== originalValue[sectionKey];
        }

        return false;
      })
      .forEach(modifiedKey => {
        modifications.push(modifiedKey as keyof AddressMaintenanceModel);
      });
  }

  return modifications;
};

export const getChanges = createSelector(getAddressMaintenanceState, getFormChanges);

export const changeValue = (currentValue: AddressMaintenanceModel, newValue: AddressMaintenanceModel) => ({
  ...currentValue,
  ...newValue
});

export const canApplyChanges = createSelector(
  getAddressMaintenanceState,
  state => state && !state.isInvalid && state.isChanged
);
