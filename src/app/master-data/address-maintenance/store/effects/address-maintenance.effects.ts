import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of, throwError } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';

import { ChangesDialogService } from '~app/master-data/address-maintenance/changes-dialog/changes-dialog.service';
import { AddressMaintenanceActions } from '~app/master-data/address-maintenance/store';
import { NotificationService } from '~app/shared/services';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

const VALIDATION_ERROR_CODE = 400;

@Injectable()
export class AddressMaintenanceEffects {
  public openApplyChanges$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AddressMaintenanceActions.openApplyChanges),
      switchMap(() =>
        this.changesDialogService.confirmApplyChanges().pipe(
          switchMap(() => EMPTY),
          catchError(error => {
            // It is necessary to hide the toast message on validation error, because the error will be displayed under the field
            if (error?.status === VALIDATION_ERROR_CODE) {
              return of(AddressMaintenanceActions.applyChangesError({ error }));
            }

            return throwError(error);
          })
        )
      ),
      rethrowError(error => AddressMaintenanceActions.applyChangesError({ error }))
    )
  );

  public applySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AddressMaintenanceActions.applyChangesSuccess),
        tap(() =>
          this.notification.showSuccess('LIST.MASTER_DATA.addressMaintenance.changesDialog.applyChanges.success')
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private changesDialogService: ChangesDialogService,
    private notification: NotificationService
  ) {}
}
