import * as AddressMaintenanceActions from '~app/master-data/address-maintenance/store/actions/address-maintenance.actions';
import * as fromAddressMaintenance from '~app/master-data/address-maintenance/store/reducers/address-maintenance.reducer';

export { fromAddressMaintenance, AddressMaintenanceActions };
