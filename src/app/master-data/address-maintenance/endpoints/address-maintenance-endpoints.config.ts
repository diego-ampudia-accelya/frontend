import { AddressMaintenanceResourceType } from '../models/address-maintenance.model';
import { appConfiguration } from '~app/shared/services';

//* A function that from an Array of tokens, builds a path
type DynamicEndpointPath = (tokens: { [key: string]: string }) => string;

export const ADDRESS_MAINTENANCE_ENDPOINTS: {
  [resourceName in AddressMaintenanceResourceType]: DynamicEndpointPath;
} = {
  [AddressMaintenanceResourceType.agent]: tokens =>
    `${appConfiguration.baseApiPath}/agent-management/agents/${tokens.userId}/address`,
  [AddressMaintenanceResourceType.airline]: tokens =>
    `${appConfiguration.baseApiPath}/airline-management/airlines/${tokens.userId}/address`
};
