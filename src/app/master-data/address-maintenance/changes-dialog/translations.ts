export const translations = {
  applyChanges: {
    title: 'LIST.MASTER_DATA.addressMaintenance.changesDialog.applyChanges.title',
    details: 'LIST.MASTER_DATA.addressMaintenance.changesDialog.applyChanges.message'
  },
  unsavedChanges: {
    title: 'LIST.MASTER_DATA.addressMaintenance.changesDialog.unsavedChanges.title',
    details: 'LIST.MASTER_DATA.addressMaintenance.changesDialog.unsavedChanges.message'
  }
};
