import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { defer, iif, Observable, of } from 'rxjs';
import { finalize, first, map, mapTo, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AddressMaintenanceService } from '../services/address-maintenance.service';
import { AddressMaintenanceActions } from '../store';
import { translations } from './translations';
import { AddressMaintenanceModel } from '~app/master-data/address-maintenance/models/address-maintenance.model';
import * as fromAddressMaintenance from '~app/master-data/address-maintenance/store/reducers/address-maintenance.reducer';
import { AppState } from '~app/reducers';
import {
  ButtonDesign,
  ChangeModel,
  ChangesDialogComponent,
  DialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';

@Injectable()
export class ChangesDialogService {
  constructor(
    private dialogService: DialogService,
    private store: Store<AppState>,
    private dataService: AddressMaintenanceService,
    private translationService: L10nTranslationService
  ) {}

  public confirmApplyChanges(): Observable<FooterButton> {
    return this.confirm(translations.applyChanges);
  }

  public confirmUnsavedChanges(): Observable<FooterButton> {
    return this.confirm(translations.unsavedChanges);
  }

  public confirm(question: { title: string; details: string }): Observable<FooterButton> {
    return this.store.pipe(
      select(fromAddressMaintenance.getChanges),
      first(),
      switchMap(modifications =>
        iif(
          () => modifications.length > 0,
          defer(() => this.open(modifications, question)),
          of(FooterButton.Discard)
        )
      )
    );
  }

  private open(
    modifications: Array<keyof AddressMaintenanceModel>,
    question: { title: string; details: string }
  ): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: question.title,
        footerButtonsType: [
          { type: FooterButton.Discard, buttonDesign: ButtonDesign.Secondary },
          { type: FooterButton.Apply }
        ]
      },
      message: question.details,
      hideValues: true,
      changes: this.formatChanges(modifications)
    };

    return this.dialogService.open(ChangesDialogComponent, dialogConfig).pipe(
      map(action => action.clickedBtn),
      withLatestFrom(this.store.pipe(select(fromAddressMaintenance.getAddressMaintenance))),
      switchMap(([action, value]) => {
        let dialogResult$ = of(action);
        if (action === FooterButton.Apply) {
          dialogResult$ = this.apply(value, dialogConfig);
        } else if (action === FooterButton.Discard) {
          this.store.dispatch(AddressMaintenanceActions.discard());
        }

        return dialogResult$;
      }),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(value: AddressMaintenanceModel, dialogConfig: DialogConfig): Observable<FooterButton> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.save(value).pipe(
        tap(() => this.store.dispatch(AddressMaintenanceActions.applyChangesSuccess({ value }))),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }

  private formatChanges(modifications: Array<keyof AddressMaintenanceModel>): ChangeModel[] {
    return modifications.map(item => ({
      group: this.translationService.translate('LIST.MASTER_DATA.addressMaintenance.changesDialog.name'),
      name: this.translationService.translate(`LIST.MASTER_DATA.addressMaintenance.changesDialog.sections.${item}`),
      value: null
    }));
  }
}
