export interface AddressMaintenanceModel {
  id: number;
  name: string;
  iataCode: string;
  sameAddressForAllCategories: boolean;
  generic: AddressGroupModel;
  acdm: AddressGroupModel;
  refund: AddressGroupModel;
}

export interface AddressGroupModel {
  address1: string;
  address2: string;
  locality: string;
  city: string;
  state: string;
  country: string;
  postCode: string;
  telephone: string;
  contactName: string;
  phoneFaxNumber: string;
  email: string;
  comment: string;
}

export enum AddressMaintenanceResourceType {
  airline,
  agent
}
