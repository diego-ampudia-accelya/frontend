import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { PeriodListService } from './period-list.service';
import { Period } from '~app/master-data/periods/shared/period.models';
import { DataQuery } from '~app/shared/components/list-view';
import { Queryable } from '~app/shared/components/list-view/queryable';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { RequestMethod } from '~app/shared/enums/request-method.enum';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class PeriodService extends ApiService<Period> implements Queryable<Period> {
  private _bspId: number;

  public set bspId(id: number) {
    this._bspId = id;
    this.baseUrl = `${appConfiguration.baseApiPath}/period-management/bsps/${this.bspId}`;
  }

  public get bspId(): number {
    return this._bspId;
  }

  constructor(public http: HttpClient, private periodListService: PeriodListService) {
    super(http, 'period');
  }

  public find(dataQuery: DataQuery): Observable<PagedData<Period>> {
    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    const requestQuery = RequestQuery.fromDataQuery(dataQuery);
    requestQuery.filterBy = this.formatFilter(dataQuery.filterBy);

    requestQuery.reduceFilterBy = true;

    return super.getAll(requestQuery, '/periods');
  }

  public download(dataQuery: DataQuery, downloadFormat: string) {
    const requestQuery = dataQuery;
    if (!Array.isArray(requestQuery.filterBy)) {
      requestQuery.filterBy = this.formatFilter(dataQuery.filterBy);
    }

    return super.download(requestQuery, `/periods/download?type=${downloadFormat}`, RequestMethod.Post);
  }

  @Cacheable()
  public getByBsp(bspId: number): Observable<Period[]> {
    this.bspId = bspId;

    return this.find({
      sortBy: [],
      filterBy: {},
      paginateBy: { size: PAGINATION.MAX_PAGE_SIZE, page: PAGINATION.FIRST_PAGE }
    }).pipe(
      map(({ records = [] }) => records),
      catchError(() => of([]))
    );
  }

  public formatFilter(filter: any): RequestQueryFilter[] {
    return this.periodListService.formatRequestFilters(filter);
  }
}
