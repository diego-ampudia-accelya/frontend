/* eslint-disable @typescript-eslint/naming-convention */
import { TableColumn } from '@swimlane/ngx-datatable';

export const PERIOD_PROPERTIES = {
  PERIOD: 'period',
  DATE_FROM: 'dateFrom',
  DATE_TO: 'dateTo'
};

export const periodTableConfig: TableColumn[] = [
  {
    prop: 'period',
    name: 'MASTER_DATA.periods.list.period'
  },
  {
    prop: 'dateFrom',
    name: 'MASTER_DATA.periods.list.dateFrom',
    cellTemplate: 'dayMonthYearFromMonthDayCellTmpl'
  },
  {
    prop: 'dateTo',
    name: 'MASTER_DATA.periods.list.dateTo',
    cellTemplate: 'dayMonthYearFromMonthDayCellTmpl'
  }
];
