export interface Period {
  id: number;
  period: string;
  dateFrom: string;
  dateTo: string;
  isoCountryCode: string;
}

export interface PeriodSearchForm {
  period: string;
  dateRangeFrom: Date[];
  dateRangeTo: Date[];
}
