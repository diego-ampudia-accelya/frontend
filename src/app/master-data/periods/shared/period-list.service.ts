import { Injectable } from '@angular/core';

import { PERIOD_PROPERTIES } from './period-list.config';
import { EQ, GT, LIKE, LT } from '~app/shared/constants/operations';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';

const PROPERTIES = PERIOD_PROPERTIES;

@Injectable({ providedIn: 'root' })
export class PeriodListService {
  formatRequestFilters(data: any): RequestQueryFilter[] {
    const formattedRequestFilters: RequestQueryFilter[] = [];

    Object.entries<any>(data).forEach(([key, value]) => {
      let attribute;
      let operation = EQ;
      const attributeValue = value;

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'dateRangeFrom') {
        attribute = PROPERTIES.DATE_FROM;

        if (!value[1]) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: toShortIsoDate(value[0])
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: toShortIsoDate(value[0])
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: toShortIsoDate(value[1])
          });
        }

        return;
      }

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'dateRangeTo') {
        attribute = PROPERTIES.DATE_TO;

        if (!value[1]) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: toShortIsoDate(value[0])
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: toShortIsoDate(value[0])
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: toShortIsoDate(value[1])
          });
        }

        return;
      }

      if (key === 'period') {
        attribute = PROPERTIES.PERIOD;
        operation = LIKE;
      }

      formattedRequestFilters.push({
        attribute,
        operation,
        attributeValue
      });
    });

    return formattedRequestFilters;
  }
}
