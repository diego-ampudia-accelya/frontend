import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { PeriodService } from './period.service';
import { testDownloadRequestQueryBody, testQuery } from '~app/shared/utils/test-utils';

const httpResponseMock = {
  pageNumber: 1,
  pageSize: 20,
  total: 1110,
  records: [
    {
      dateFrom: '01/01/1997',
      dateTo: '01/15/1997',
      id: 69831997011,
      isoCountryCode: 'ES',
      period: '1997011'
    },
    {
      dateFrom: '01/16/1997',
      dateTo: '01/31/1997',
      id: 69831997012,
      isoCountryCode: 'ES',
      period: '1997012'
    }
  ]
};

describe('PeriodService', () => {
  let service: PeriodService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PeriodService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(PeriodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('find should execute GET request, and return PagedData ', () => {
    spyOn(service.http, 'get').and.returnValue(of(httpResponseMock));

    service.find(testQuery).subscribe(result => {
      expect(result.records.length).toBe(2);
      const { pageNumber, pageSize, total, records } = result;

      expect(pageNumber).toBeTruthy();
      expect(pageSize).toBeTruthy();
      expect(total).toBeTruthy();
      expect(records).toBeTruthy();
    });
  });

  it('download should execute a POST request', () => {
    spyOn(service.http, 'post').and.returnValue(of());
    service.download(testDownloadRequestQueryBody, 'txt').subscribe();

    expect(service.http.post).toHaveBeenCalled();
  });
});
