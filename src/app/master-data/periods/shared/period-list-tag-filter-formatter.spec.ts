import { inject, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import moment from 'moment-mini';

import { PeriodListTagFilterFormatter } from './period-list-tag-filter-formatter';
import { AppliedFilter } from '~app/shared/components/list-view';

const translationServiceSpy = createSpyObject(L10nTranslationService);
translationServiceSpy.translate.andCallFake(identity);

describe('Service: PeriodListTagFilterFormatter', () => {
  let service: PeriodListTagFilterFormatter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PeriodListTagFilterFormatter,
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        }
      ]
    });
  });

  beforeEach(inject([PeriodListTagFilterFormatter], (periodListTagFilterFormatter: PeriodListTagFilterFormatter) => {
    service = periodListTagFilterFormatter;
  }));

  it('should initialize', () => {
    expect(service).toBeTruthy();
  });

  it('should format filter tags properly', () => {
    // rangepicker from, start date
    const dateFromStart = new Date();
    const dateFromStartString = moment(dateFromStart).format('DD/MM/YYYY');

    // rangepicker from, end date
    const dateFromEnd = new Date(dateFromStart.getDate() + 1);
    const dateFromEndString = moment(dateFromEnd).format('DD/MM/YYYY');

    // rangepicker to, start date
    const dateToStart = new Date(dateFromStart.getDate() + 2);
    const dateToStartString = moment(dateToStart).format('DD/MM/YYYY');

    // rangepicker to, end date
    const dateToEnd = new Date(dateFromStart.getDate() + 3);
    const dateToEndString = moment(dateToEnd).format('DD/MM/YYYY');

    const initialFilter = {
      period: '2020',
      dateRangeFrom: [dateFromStart, dateFromEnd],
      dateRangeTo: [dateToStart, dateToEnd]
    };
    const actualResult: AppliedFilter[] = service.format(initialFilter);
    const expectedResult = [
      {
        label: 'MASTER_DATA.periods.list.period: 2020',
        closable: true,
        keys: ['period']
      },
      {
        label: `MASTER_DATA.periods.list.dateFrom - ${dateFromStartString} - ${dateFromEndString}`,
        closable: true,
        keys: ['dateRangeFrom']
      },
      {
        label: `MASTER_DATA.periods.list.dateTo - ${dateToStartString} - ${dateToEndString}`,
        closable: true,
        keys: ['dateRangeTo']
      }
    ];

    expect(actualResult).toEqual(expectedResult);
  });

  it('should format dateRangeFrom filter properly when single date is selected', () => {
    const dateFromStart = new Date();
    const dateFromStartString = moment(dateFromStart).format('DD/MM/YYYY');
    const initialFilter = {
      dateRangeFrom: [dateFromStart]
    };
    const actualResult: AppliedFilter[] = service.format(initialFilter);
    const expectedResult = [
      {
        label: `MASTER_DATA.periods.list.dateFrom - ${dateFromStartString}`,
        closable: true,
        keys: ['dateRangeFrom']
      }
    ];

    expect(actualResult).toEqual(expectedResult);
  });

  it('should format dateRangeTo filter properly when single date is selected', () => {
    const dateToStart = new Date();
    const dateToStartString = moment(dateToStart).format('DD/MM/YYYY');
    const initialFilter = {
      dateRangeTo: [dateToStart]
    };
    const actualResult: AppliedFilter[] = service.format(initialFilter);
    const expectedResult = [
      {
        label: `MASTER_DATA.periods.list.dateTo - ${dateToStartString}`,
        closable: true,
        keys: ['dateRangeTo']
      }
    ];

    expect(actualResult).toEqual(expectedResult);
  });
});
