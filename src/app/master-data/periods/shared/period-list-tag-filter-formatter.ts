import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { PeriodSearchForm } from '~app/master-data/periods/shared/period.models';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

@Injectable()
export class PeriodListTagFilterFormatter extends DefaultDisplayFilterFormatter {
  constructor(private translationService: L10nTranslationService) {
    super();
  }

  public format(filter: Partial<PeriodSearchForm>): AppliedFilter[] {
    const { dateRangeFrom, dateRangeTo, period, ...rest } = filter;
    const formattedFilters = super.format(rest);

    if (period) {
      formattedFilters.push({
        label: `${this.translationService.translate('MASTER_DATA.periods.list.period')}: ${period}`,
        keys: ['period'],
        closable: true
      });
    }

    if (dateRangeFrom) {
      const dateRangeFromLabel = this.translationService.translate('MASTER_DATA.periods.list.dateFrom');

      formattedFilters.push({
        label: `${dateRangeFromLabel} - ${rangeDateFilterTagMapper(dateRangeFrom)}`,
        keys: ['dateRangeFrom'],
        closable: true
      });
    }

    if (dateRangeTo) {
      const dateRangeToLabel = this.translationService.translate('MASTER_DATA.periods.list.dateTo');

      formattedFilters.push({
        label: `${dateRangeToLabel} - ${rangeDateFilterTagMapper(dateRangeTo)}`,
        keys: ['dateRangeTo'],
        closable: true
      });
    }

    return formattedFilters;
  }
}
