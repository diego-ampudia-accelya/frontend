import { of } from 'rxjs';

import { DataQuery } from '../../../shared/components/list-view';
import { Period } from '~app/master-data/periods/shared/period.models';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { PagedData } from '~app/shared/models/paged-data.model';

export const periods = [
  {
    period: '2019011',
    id: 1,
    dateFrom: '2019-01-01',
    dateTo: '2019-01-11'
  },
  {
    period: '2019012',
    id: 2,
    dateFrom: '2019-01-21',
    dateTo: '2019-01-31'
  },
  {
    period: '2019051',
    id: 3,
    dateFrom: '2019-05-01',
    dateTo: '2019-05-28'
  },
  {
    period: '2018011',
    id: 4,
    dateFrom: '2018-01-01',
    dateTo: '2018-01-11'
  },
  {
    period: '2018012',
    id: 5,
    dateFrom: '2018-01-21',
    dateTo: '2018-01-31'
  },
  {
    period: '2018051',
    id: 6,
    dateFrom: '2018-05-01',
    dateTo: '2018-05-28'
  },
  {
    period: '2017011',
    id: 7,
    dateFrom: '2017-01-01',
    dateTo: '2017-01-11'
  },
  {
    period: '2017012',
    id: 8,
    dateFrom: '2017-01-21',
    dateTo: '2017-01-31'
  },
  {
    period: '2017051',
    id: 9,
    dateFrom: '2017-05-01',
    dateTo: '2017-05-28'
  }
];

export const bsps = [
  {
    id: 1,
    name: 'BSP 1',
    isoCountryCode: '',
    effectiveFrom: ''
  },
  {
    id: 2,
    name: 'BSP 2',
    isoCountryCode: '',
    effectiveFrom: ''
  },
  {
    id: 3,
    name: 'BSP 3',
    isoCountryCode: '',
    effectiveFrom: ''
  }
];

export const initialState = {
  auth: {
    user: createIataUser()
  },
  router: null,
  core: {
    menu: {
      tabs: {}
    },
    viewListsInfo: {}
  }
};

export class QueryableDataSourceStub {
  query$ = of({
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  } as DataQuery);

  appliedQuery$ = of({
    sortBy: [],
    filterBy: {},
    paginateBy: { page: 0, size: 20, totalElements: 10 }
  } as DataQuery);

  data$ = of({
    records: periods,
    pageSize: 20,
    total: 9,
    totalPages: 1,
    pageNumber: 1
  } as PagedData<Period>);

  get() {
    return of([]);
  }
}

export class PeriodServiceStub {
  private _bspId: number;
  baseUrl: string;

  set bspId(id: number) {
    this._bspId = id;
    this.baseUrl = `api/period-management/bsps/${this.bspId}`;
  }

  get bspId(): number {
    return this._bspId;
  }

  find() {
    return of({
      records: periods,
      pageSize: 20,
      total: 9,
      totalPages: 1,
      pageNumber: 1
    });
  }
}

export const queryMock = {
  filterBy: { period: String(new Date().getFullYear()) },
  sortBy: [{ attribute: 'period', sortType: SortOrder.Asc }],
  paginateBy: {
    size: 20,
    totalElements: 1,
    page: 1
  }
} as DataQuery<Period>;
