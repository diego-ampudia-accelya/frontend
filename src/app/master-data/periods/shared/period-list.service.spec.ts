import { inject, TestBed } from '@angular/core/testing';

import { PeriodListService } from './period-list.service';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';

describe('Service: PeriodListService', () => {
  let service: PeriodListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PeriodListService]
    });
  });

  beforeEach(inject([PeriodListService], (periodListService: PeriodListService) => {
    service = periodListService;
  }));

  it('should initialize', () => {
    expect(service).toBeTruthy();
  });

  describe('formatRequestFilters', () => {
    // rangepicker from, start date
    const dateFromStart = new Date();
    const dateFromStartString = toShortIsoDate(dateFromStart);

    // rangepicker from, end date
    const dateFromEnd = new Date(dateFromStart.getDate() + 1);
    const dateFromEndString = toShortIsoDate(dateFromEnd);

    // rangepicker to, start date
    const dateToStart = new Date(dateFromStart.getDate() + 2);
    const dateToStartString = toShortIsoDate(dateToStart);

    // rangepicker to, end date
    const dateToEnd = new Date(dateFromStart.getDate() + 3);
    const dateToEndString = toShortIsoDate(dateToEnd);
    it('should format filter fields', () => {
      const data = {
        period: '2020',
        dateRangeFrom: [dateFromStart, null],
        dateRangeTo: [dateToStart, dateToEnd]
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'period', operation: 'like', attributeValue: '2020' },
        { attribute: 'dateFrom', operation: '=', attributeValue: dateFromStartString },
        { attribute: 'dateTo', operation: '>', attributeValue: dateToStartString },
        { attribute: 'dateTo', operation: '<', attributeValue: dateToEndString }
      ]);
    });

    it('should format dates correctly when only one date is selected in the picker', () => {
      const data = {
        dateRangeFrom: [dateFromStart],
        dateRangeTo: [dateToStart]
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'dateFrom', operation: '=', attributeValue: dateFromStartString },
        { attribute: 'dateTo', operation: '=', attributeValue: dateToStartString }
      ]);
    });

    it('should format dates correctly when multiple values are selected in the rangepicker', () => {
      const data = {
        dateRangeFrom: [dateFromStart, dateFromEnd],
        dateRangeTo: [dateToStart, dateToEnd]
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'dateFrom', operation: '>', attributeValue: dateFromStartString },
        { attribute: 'dateFrom', operation: '<', attributeValue: dateFromEndString },
        { attribute: 'dateTo', operation: '>', attributeValue: dateToStartString },
        { attribute: 'dateTo', operation: '<', attributeValue: dateToEndString }
      ]);
    });
  });
});
