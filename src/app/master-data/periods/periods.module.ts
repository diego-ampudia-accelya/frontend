import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';

import { PeriodListComponent } from './period-list/period-list.component';
import { PeriodsRoutingModule } from './periods-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [PeriodListComponent],
  imports: [CommonModule, SharedModule, PeriodsRoutingModule, L10nTranslationModule],
  providers: []
})
export class PeriodsModule {}
