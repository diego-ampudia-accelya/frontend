import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { PeriodListTagFilterFormatter } from '~app/master-data/periods/shared/period-list-tag-filter-formatter';
import { periodTableConfig } from '~app/master-data/periods/shared/period-list.config';
import { Period, PeriodSearchForm } from '~app/master-data/periods/shared/period.models';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { SelectMode } from '~app/shared/enums';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Component({
  selector: 'bspl-period-list',
  templateUrl: './period-list.component.html',
  styleUrls: ['./period-list.component.scss'],
  providers: [
    DefaultQueryStorage,
    PeriodListTagFilterFormatter,
    PeriodService,
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [PeriodService]
    }
  ]
})
export class PeriodListComponent implements OnInit, OnDestroy {
  public columns = periodTableConfig;
  public filterForm: FormGroup;
  public query: Partial<DataQuery<Partial<PeriodSearchForm>>>;
  public userBspOptions: DropdownOption[] = [];
  public selectedBsp: number;
  public selectMode = SelectMode;
  public downloadQuery: Partial<DataQuery>;
  public periodValidation = {
    pattern: /^(\d{0,7})$/,
    maxLength: 7
  };
  public companyCalendarMsg = 'MASTER_DATA.companyCalendar.disclaimer';

  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<Period>,
    public periodListTagFilterFormatter: PeriodListTagFilterFormatter,
    private queryStorage: DefaultQueryStorage,
    private formBuilder: FormBuilder,
    private periodService: PeriodService,
    public dialogService: DialogService,
    public translationService: L10nTranslationService,
    protected store: Store<AppState>
  ) {}

  public ngOnInit() {
    this.initFilterForm();
    this.setUserBspOptions();
  }

  public initFilterForm() {
    this.filterForm = this.formBuilder.group({
      period: ['', [Validators.pattern(this.periodValidation.pattern)]],
      dateRangeFrom: [''],
      dateRangeTo: ['']
    });
  }

  protected setUserBspOptions() {
    this.store
      .pipe(
        select(getUserBsps),
        map(res =>
          res.map(bsp => ({
            value: bsp.id,
            label: bsp.name
          }))
        ),
        takeUntil(this.destroy$)
      )
      .subscribe(res => {
        if (res && res.length > 0) {
          this.userBspOptions = res;
          this.selectBsp(res[0].value);
        }
      });
  }

  public selectBsp(id: number) {
    if (id != null) {
      this.selectedBsp = id;
      this.periodService.bspId = id;
      this.setQuery();
      this.loadData();
    }
  }

  setQuery() {
    const storedQuery = this.queryStorage.get();

    if (storedQuery && !isEmpty(storedQuery.filterBy)) {
      this.query = storedQuery;
    } else {
      const currYear = String(new Date().getFullYear());

      this.query = {
        filterBy: { period: currYear },
        sortBy: [{ attribute: 'period', sortType: SortOrder.Asc }],
        paginateBy: storedQuery ? storedQuery.paginateBy : {}
      };
    }
  }

  public loadData(query = this.query) {
    const dataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.downloadQuery = dataQuery;
    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public search(query: DataQuery<Period>): void {
    this.query = query;
    this.loadData(query);
  }

  public download() {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('BUTTON.DEFAULT.DOWNLOAD'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery
      },
      apiService: this.periodService
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
}
