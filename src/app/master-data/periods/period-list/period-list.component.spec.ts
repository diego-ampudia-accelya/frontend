import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { bsps, initialState, PeriodServiceStub, QueryableDataSourceStub, queryMock } from '../shared/periods-mocks';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { PeriodListComponent } from '~app/master-data/periods/period-list/period-list.component';
import { PeriodListTagFilterFormatter } from '~app/master-data/periods/shared/period-list-tag-filter-formatter';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { AppState } from '~app/reducers';
import { DialogService } from '~app/shared/components';
import { DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('PeriodListComponent', () => {
  let component: PeriodListComponent;
  let fixture: ComponentFixture<PeriodListComponent>;
  let store: MockStore<AppState>;

  const queryStorageMock = createSpyObject(DefaultQueryStorage);
  const dataSource = createSpyObject(QueryableDataSourceStub);
  const dialogService = createSpyObject(DialogService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PeriodListComponent, TranslatePipeMock],
      providers: [
        provideMockStore({ initialState }),
        FormBuilder,
        PeriodListTagFilterFormatter,
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(PeriodListComponent, {
        set: {
          providers: [
            { provide: QueryableDataSource, useValue: dataSource },
            { provide: PeriodService, useValue: PeriodServiceStub },
            { provide: DefaultQueryStorage, useValue: queryStorageMock },
            { provide: DialogService, useValue: dialogService }
          ]
        }
      })
      .compileComponents();

    fixture = TestBed.createComponent(PeriodListComponent);
    component = fixture.componentInstance;

    store = TestBed.inject<any>(Store);
    store.overrideSelector(getUserBsps, bsps);
    queryStorageMock.get.and.returnValue(queryMock);

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change a selected bsp and load a new data', () => {
    spyOn(component, 'setQuery');
    spyOn(component, 'loadData');
    const id = 2;

    component.selectBsp(id);

    expect(component.selectedBsp).toBe(2);
    expect(component.setQuery).toHaveBeenCalled();
    expect(component.loadData).toHaveBeenCalled();
  });

  it('should set query', () => {
    component.setQuery();

    expect(component.query).toEqual(queryMock);
  });

  it('should load periods data', () => {
    component.query = queryMock;

    component.loadData();

    expect(component.downloadQuery).toEqual(component.query);
    expect(dataSource.get).toHaveBeenCalled();
    expect(queryStorageMock.save).toHaveBeenCalled();
  });

  it('should search periods', () => {
    spyOn(component, 'loadData');

    component.search(queryMock);

    expect(component.query).toEqual(queryMock);
    expect(component.loadData).toHaveBeenCalled();
  });

  it('should open download dialog', () => {
    component.downloadQuery = queryMock;
    component.download();

    expect(component.downloadQuery).toEqual(queryMock);
    expect(dialogService.open).toHaveBeenCalled();
  });
});
