import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PeriodListComponent } from './period-list/period-list.component';
import { ROUTES } from '~app/shared/constants/routes';

const routes: Routes = [
  {
    path: '',
    component: PeriodListComponent,
    data: {
      tab: ROUTES.PERIODS
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeriodsRoutingModule {}
