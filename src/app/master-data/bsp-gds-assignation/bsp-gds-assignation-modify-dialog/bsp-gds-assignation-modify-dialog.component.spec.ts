import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { BspGdsAssignationModifyDialogComponent } from './bsp-gds-assignation-modify-dialog.component';
import { BspGdsAssignationModifyDialogType } from '~app/master-data/models/bsp-gds-assignation.model';
import { BspGdsAssignationService } from '~app/master-data/services';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('BspGdsAssignationModifyDialogComponent', () => {
  let component: BspGdsAssignationModifyDialogComponent;
  let fixture: ComponentFixture<BspGdsAssignationModifyDialogComponent>;

  const actionEmitterMock = jasmine.createSpyObj('actionEmitter', ['next']);
  const reactiveSubjectSpy = {
    asObservable: of(true),
    asEmitter: jasmine.createSpy().and.returnValue({}),
    emit: jasmine.createSpy()
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BspGdsAssignationModifyDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ReactiveSubject,
          useValue: reactiveSubjectSpy
        },
        {
          provide: DialogService,
          useValue: {
            close: () => {}
          }
        },
        {
          provide: DialogConfig,
          useValue: {
            data: {
              itemData: {
                model: {
                  gds: {
                    gdsCode: 1,
                    name: 'gdsName'
                  },
                  id: 1
                },
                type: BspGdsAssignationModifyDialogType.Create
              },
              buttons: [
                {
                  type: FooterButton.Cancel
                },
                {
                  type: FooterButton.Save
                }
              ]
            }
          }
        },
        {
          provide: L10nTranslationService,
          useValue: {
            translate: () => ''
          }
        },
        {
          provide: BspGdsAssignationService,
          useValue: {
            update: () => of(null),
            delete: () => of(null),
            create: () => of(null)
          }
        },
        {
          provide: NotificationService,
          useValue: {
            showSuccess: () => {}
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BspGdsAssignationModifyDialogComponent);
    component = fixture.componentInstance;
    component['actionEmitter'] = actionEmitterMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize several component methods and subscribe to reactiveSubject when ngOnInit is triggered', () => {
    spyOn(component as any, 'initFormGroup');
    spyOn(component as any, 'initializeSaveButton');
    spyOn((component as any).reactiveSubject.asObservable, 'pipe').and.returnValue(of({}));
    spyOn(component as any, 'onCreate');
    spyOn(component as any, 'onEdit');
    spyOn(component as any, 'onDelete');

    component.ngOnInit();

    expect((component as any).initFormGroup).toHaveBeenCalledWith(component.config.data.itemData);
    expect((component as any).initializeSaveButton).toHaveBeenCalled();
    expect((component as any).onCreate).not.toHaveBeenCalled();
    expect((component as any).onEdit).not.toHaveBeenCalled();
    expect((component as any).onDelete).not.toHaveBeenCalled();
  });

  it('should initialize saveButton and subscribe to formGroup statusChanges when initializeSaveButton is triggered', () => {
    component['type'] = BspGdsAssignationModifyDialogType.Create;
    const saveButton = {
      type: FooterButton.Create
    };
    const buttons = [
      {
        type: FooterButton.Cancel
      },
      saveButton
    ];
    component.config.data.buttons = buttons;
    spyOn(component['loading$'], 'pipe').and.returnValue(of(false));
    spyOn(component.formGroup.statusChanges, 'pipe').and.returnValue(of('VALID'));

    component['initializeSaveButton']();

    expect(component['saveButton']).toEqual(saveButton);
    expect(component['saveButton'].isDisabled).toBeFalsy();
  });

  it('should update the BspGdsAssignation when onEdit method is triggered', () => {
    const itemData = {
      model: {
        id: 1,
        gds: {
          gdsCode: 'ABCD',
          name: 'gdsName'
        },
        refundsEnabled: true
      }
    };
    component.config.data.itemData = itemData;
    spyOn((component as any).notificationService, 'showSuccess');
    spyOn((component as any).bspGdsAssignationService, 'update').and.returnValue(of(null));
    spyOn((component as any).dialogService, 'close');

    component['onEdit']({ name: 'gdsName update', refundEnabled: false });
    expect(component['dialogService'].close).toHaveBeenCalled();
    expect(component['actionEmitter'].next).toHaveBeenCalled();
  });

  it('should delete the BspGdsAssignation when onDelete method is triggered', () => {
    const itemData = {
      model: {
        id: 1
      }
    };
    component.config.data.itemData = itemData;
    spyOn((component as any).bspGdsAssignationService, 'delete').and.returnValue(of(null));
    spyOn((component as any).notificationService, 'showSuccess');
    spyOn((component as any).dialogService, 'close');

    component['onDelete']();

    expect((component as any).bspGdsAssignationService.delete).toHaveBeenCalledWith(1);
    expect((component as any).dialogService.close).toHaveBeenCalled();
    expect((component as any).actionEmitter.next).toHaveBeenCalled();
  });

  it('should create a new BspGdsAssignation when onCreate method is triggered', () => {
    spyOn((component as any).bspGdsAssignationService, 'create').and.returnValue(of(null));
    spyOn((component as any).notificationService, 'showSuccess');
    spyOn((component as any).dialogService, 'close');

    component['onCreate']({ gdsCode: 'ABCD', name: 'gdsName', refundEnabled: true });

    expect(component['bspGdsAssignationService'].create).toHaveBeenCalledWith({
      refundEnabled: true,
      gds: { gdsCode: 'ABCD', name: 'gdsName' }
    });
    expect((component as any).dialogService.close).toHaveBeenCalled();
    expect((component as any).actionEmitter.next).toHaveBeenCalled();
  });

  it('should trigger onCreate method when ngOnInit is triggered and the action is equal to create', () => {
    const action = { clickedBtn: FooterButton.Create };

    spyOnProperty(component, 'isDelete', 'get').and.returnValue(true);
    spyOn(reactiveSubjectSpy.asObservable, 'pipe').and.returnValue(of(action));
    const onCreateSpy = spyOn(component as any, 'onCreate');

    component.ngOnInit();

    expect(onCreateSpy).toHaveBeenCalled();
  });

  it('should trigger onEdit method when ngOnInit is triggered and the action is equal to modify', () => {
    const action = { clickedBtn: FooterButton.Modify };

    spyOnProperty(component, 'isDelete', 'get').and.returnValue(true);
    spyOn(reactiveSubjectSpy.asObservable, 'pipe').and.returnValue(of(action));
    const onEditSpy = spyOn(component as any, 'onEdit');

    component.ngOnInit();

    expect(onEditSpy).toHaveBeenCalled();
  });

  it('should trigger onDelete method when ngOnInit is triggered and the action is equal to delete', () => {
    const action = { clickedBtn: FooterButton.Delete };

    spyOnProperty(component, 'isDelete', 'get').and.returnValue(true);
    spyOn(reactiveSubjectSpy.asObservable, 'pipe').and.returnValue(of(action));
    const onDeleteSpy = spyOn(component as any, 'onDelete');

    component.ngOnInit();

    expect(onDeleteSpy).toHaveBeenCalled();
  });

  it('should not trigger any method when ngOnInit is triggered and the action is not equal to delete, modify or create', () => {
    const action = { clickedBtn: FooterButton.Ok };

    spyOnProperty(component, 'isDelete', 'get').and.returnValue(true);
    spyOn(reactiveSubjectSpy.asObservable, 'pipe').and.returnValue(of(action));
    const onDeleteSpy = spyOn(component as any, 'onDelete');
    const onEditSpy = spyOn(component as any, 'onEdit');
    const onCreateSpy = spyOn(component as any, 'onCreate');

    component.ngOnInit();

    expect(onDeleteSpy).not.toHaveBeenCalled();
    expect(onEditSpy).not.toHaveBeenCalled();
    expect(onCreateSpy).not.toHaveBeenCalled();
  });
});
