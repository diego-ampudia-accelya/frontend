import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';

import { YesNoOptions } from '~app/files/model/yes-no.enum';
import { stringToBoolean } from '~app/master-data/configuration/tip/tip-global-airline/services/tip-global.airline.converter';
import { BspGdsAssignationModifyDialogType } from '~app/master-data/models/bsp-gds-assignation.model';
import { BspGdsAssignationService } from '~app/master-data/services';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { GLOBALS } from '~app/shared/constants';
import { DropdownOption } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-bsp-gds-assignation-modify-dialog',
  templateUrl: './bsp-gds-assignation-modify-dialog.component.html',
  styleUrls: ['./bsp-gds-assignation-modify-dialog.component.scss']
})
export class BspGdsAssignationModifyDialogComponent implements OnInit, OnDestroy {
  public formGroup: FormGroup;
  public refundsEnabledOptions: DropdownOption[];

  public get isEdit(): boolean {
    return this.type === BspGdsAssignationModifyDialogType.Edit;
  }

  public get isDelete(): boolean {
    return this.type === BspGdsAssignationModifyDialogType.Delete;
  }

  public get subTitle(): string {
    return `MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.${this.type.toLocaleUpperCase()}.subTitle`;
  }

  private type: BspGdsAssignationModifyDialogType = this.config.data?.itemData?.type;
  private saveButton: ModalAction;

  private actionEmitter = this.config.data.actionEmitter;
  private loading$ = new BehaviorSubject(false);
  private destroy$ = new Subject();

  private saveButtonMap = {
    [BspGdsAssignationModifyDialogType.Create]: FooterButton.Create,
    [BspGdsAssignationModifyDialogType.Edit]: FooterButton.Modify,
    [BspGdsAssignationModifyDialogType.Delete]: FooterButton.Delete
  };

  constructor(
    public reactiveSubject: ReactiveSubject,
    public config: DialogConfig,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private bspGdsAssignationService: BspGdsAssignationService,
    private notificationService: NotificationService
  ) {}

  public ngOnInit(): void {
    this.initFormGroup(this.config.data.itemData);
    this.initializeSaveButton();

    this.refundsEnabledOptions = Object.values(YesNoOptions).map((item: string) => ({
      value: stringToBoolean(item),
      label: this.translationService.translate(`MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.CREATE.options.${item.toString()}`)
    }));

    this.reactiveSubject.asObservable.pipe(takeUntil(this.destroy$)).subscribe(action => {
      this.formGroup.markAllAsTouched();

      if (this.formGroup.valid || this.isDelete) {
        switch (action?.clickedBtn) {
          case FooterButton.Create:
            this.onCreate(this.formGroup.value);
            break;
          case FooterButton.Modify:
            this.onEdit(this.formGroup.value);
            break;
          case FooterButton.Delete:
            this.onDelete();
            break;
          default:
            break;
        }
      }
    });
  }

  private initFormGroup(itemData: any): void {
    this.formGroup = new FormGroup({
      gdsCode: new FormControl(
        {
          value: this.isEdit || this.isDelete ? itemData?.model?.gds?.gdsCode : null,
          disabled: this.isEdit || this.isDelete
        },
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(4),
          Validators.pattern(GLOBALS.HTML_PATTERN.ALPHANUMERIC_LOWERCASE)
        ]
      ),
      name: new FormControl(
        { value: this.isEdit || this.isDelete ? itemData?.model?.gds?.name : null, disabled: this.isDelete },
        [Validators.required, Validators.maxLength(20)]
      ),
      refundEnabled: new FormControl(
        { value: this.isEdit || this.isDelete ? itemData?.model?.refundsEnabled : null, disabled: this.isDelete },
        [Validators.required]
      )
    });
  }

  private initializeSaveButton(): void {
    const buttonType = this.saveButtonMap[this.type];
    this.saveButton = this.config.data.buttons.find(button => button.type === buttonType);

    if (this.saveButton) {
      this.loading$.pipe(takeUntil(this.destroy$)).subscribe(loading => (this.saveButton.isLoading = loading));

      if (!this.isDelete) {
        this.formGroup.statusChanges
          .pipe(takeUntil(this.destroy$))
          .subscribe(status => (this.saveButton.isDisabled = status !== 'VALID'));
      }
    }
  }

  private onEdit({ name, refundEnabled }: { name: string; refundEnabled: boolean }): void {
    const gdsAssignationId = this.config.data.itemData?.model?.id;
    this.loading$.next(true);

    this.bspGdsAssignationService
      .update({ refundEnabled, gds: { name } }, gdsAssignationId)
      .pipe(
        tap(() => {
          this.notificationService.showSuccess(
            this.translationService.translate('MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.EDIT.success')
          );
          this.dialogService.close();
        }),
        tap(() => this.actionEmitter.next()),
        finalize(() => this.loading$.next(false))
      )
      .subscribe();
  }

  private onDelete(): void {
    const gdsAssignationId = this.config.data.itemData?.model?.id;
    this.loading$.next(true);

    this.bspGdsAssignationService
      .delete(gdsAssignationId)
      .pipe(
        tap(() => {
          this.notificationService.showSuccess(
            this.translationService.translate('MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.DELETE.success')
          );

          this.dialogService.close();
        }),
        tap(() => this.actionEmitter.next()),
        finalize(() => this.loading$.next(false))
      )
      .subscribe();
  }

  private onCreate({ gdsCode, name, refundEnabled }: { gdsCode: string; name: string; refundEnabled: boolean }): void {
    this.loading$.next(true);

    this.bspGdsAssignationService
      .create({ refundEnabled, gds: { gdsCode, name } })
      .pipe(
        tap(() => {
          this.notificationService.showSuccess(
            this.translationService.translate('MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.CREATE.success')
          );

          this.dialogService.close();
        }),
        tap(() => this.actionEmitter.next()),
        finalize(() => this.loading$.next(false))
      )
      .subscribe();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
