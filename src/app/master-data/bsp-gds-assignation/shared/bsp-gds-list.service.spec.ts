import { inject, TestBed } from '@angular/core/testing';
import moment from 'moment-mini';

import { BspGdsListService } from './bsp-gds-list.service';

describe('BspGdsListService', () => {
  let service: BspGdsListService;

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [BspGdsListService]
    })
  );

  beforeEach(inject([BspGdsListService], (bspGdsListService: BspGdsListService) => {
    service = bspGdsListService;
  }));

  it('should initialize', () => {
    expect(service).toBeTruthy();
  });

  describe('formatRequestFilters', () => {
    const registerDateStart = new Date();
    const registerDateStartString = moment(registerDateStart).format('YYYY-MM-DD');

    const registerDateEnd = new Date(registerDateStart.getDate() + 1);
    const registerDateEndString = moment(registerDateEnd).format('YYYY-MM-DD');

    const expiryDateStart = new Date(registerDateStart.getDate() + 2);
    const expiryDateStartString = moment(expiryDateStart).format('YYYY-MM-DD');

    const expiryDateEnd = new Date(registerDateStart.getDate() + 3);
    const expiryDateEndString = moment(expiryDateEnd).format('YYYY-MM-DD');

    it('should format filter fields', () => {
      const data = {
        registerDate: [registerDateStart, null],
        expiryDate: [expiryDateStart, expiryDateEnd]
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'effectiveFrom', operation: '=', attributeValue: registerDateStartString },
        { attribute: 'effectiveTo', operation: '>', attributeValue: expiryDateStartString },
        { attribute: 'effectiveTo', operation: '<', attributeValue: expiryDateEndString }
      ]);
    });

    it('should format dates when one date is selected', () => {
      const data = {
        registerDate: [registerDateStart],
        expiryDate: [expiryDateStart]
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'effectiveFrom', operation: '=', attributeValue: registerDateStartString },
        { attribute: 'effectiveTo', operation: '=', attributeValue: expiryDateStartString }
      ]);
    });

    it('should format dates when multiple dates are selected', () => {
      const data = {
        registerDate: [registerDateStart, registerDateEnd],
        expiryDate: [expiryDateStart, expiryDateEnd]
      };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'effectiveFrom', operation: '>', attributeValue: registerDateStartString },
        { attribute: 'effectiveFrom', operation: '<', attributeValue: registerDateEndString },
        { attribute: 'effectiveTo', operation: '>', attributeValue: expiryDateStartString },
        { attribute: 'effectiveTo', operation: '<', attributeValue: expiryDateEndString }
      ]);
    });

    it('should format status', () => {
      const status = 'active';
      const data = { status };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([{ attribute: 'active', operation: '=', attributeValue: 'active' }]);
    });

    it('should format refunds enabled', () => {
      const refundsEnabled = 'Yes';
      const data = { refundsEnabled };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([{ attribute: 'refundsEnabled', operation: '=', attributeValue: 'Yes' }]);
    });

    it('should format gdsNames', () => {
      const gdsNames = ['A082 / SELECT PRO', 'ACDM / SELECT PRO 2'];
      const data = { gdsNames };

      const result = service.formatRequestFilters(data);

      expect(result).toEqual([
        { attribute: 'gds.name', operation: 'in', attributeValue: 'A082 / SELECT PRO,ACDM / SELECT PRO 2' }
      ]);
    });
  });
});
