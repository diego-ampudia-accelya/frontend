import { Injectable } from '@angular/core';
import moment from 'moment-mini';

import { BSP_GDS_PROPERTIES } from '../bsp-gds-assignation-list/bsp-gds-assignation-list.constants';
import { EQ, GT, IN, LT } from '~app/shared/constants/operations';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';

const PROPERTIES = BSP_GDS_PROPERTIES;

@Injectable({ providedIn: 'root' })
export class BspGdsListService {
  formatRequestFilters(data: any): RequestQueryFilter[] {
    const formattedRequestFilters: RequestQueryFilter[] = [];

    Object.entries<any>(data).forEach(([key, value]) => {
      let attribute = '';
      let operation = EQ;
      let attributeValue = value;

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'registerDate') {
        attribute = PROPERTIES.EFFECTIVE_FROM;

        if (!value[1]) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: moment(value[0]).format('YYYY-MM-DD')
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: moment(value[0]).format('YYYY-MM-DD')
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: moment(value[1]).format('YYYY-MM-DD')
          });
        }

        return;
      }

      // register date and expiry date are more special, because they use date range picker
      // their value is stored in one form control, but must be separated into two values
      if (key === 'expiryDate') {
        attribute = PROPERTIES.EFFECTIVE_TO;

        if (!value[1]) {
          formattedRequestFilters.push({
            attribute,
            operation: EQ,
            attributeValue: moment(value[0]).format('YYYY-MM-DD')
          });
        } else {
          formattedRequestFilters.push({
            attribute,
            operation: GT,
            attributeValue: moment(value[0]).format('YYYY-MM-DD')
          });

          formattedRequestFilters.push({
            attribute,
            operation: LT,
            attributeValue: moment(value[1]).format('YYYY-MM-DD')
          });
        }

        return;
      }

      switch (key) {
        case 'gdsNames':
          attribute = PROPERTIES.GDS_NAME;
          operation = IN;
          attributeValue = value.join(',');
          break;
        case 'refundsEnabled':
          attribute = PROPERTIES.REFUNDS_ENABLED;
          operation = EQ;
          break;
        case 'status':
          attribute = PROPERTIES.STATUS;
          operation = EQ;
          break;
      }

      formattedRequestFilters.push({
        attribute,
        operation,
        attributeValue
      });
    });

    return formattedRequestFilters;
  }
}
