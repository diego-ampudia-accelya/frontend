import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { L10nTranslationModule } from 'angular-l10n';

import { GdsAgentListEffects } from '../gds/gds-agent-list/store/effects/agent-by-gds.effects';
import * as fromGdsAgentList from '../gds/gds-agent-list/store/reducers';
import { GdsModule } from '../gds/gds.module';

import { BspGdsAssignationDialogComponent } from './bsp-gds-assignation-dialog/bsp-gds-assignation-dialog.component';
import { BspGdsAssignationListComponent } from './bsp-gds-assignation-list/bsp-gds-assignation-list.component';
import { BspGdsAssignationModifyDialogComponent } from './bsp-gds-assignation-modify-dialog/bsp-gds-assignation-modify-dialog.component';
import { BspGdsAssignationRoutingModule } from './bsp-gds-assignation-routing.module';
import { SharedModule } from '~app/shared/shared.module';
import { EditDialogService } from '~app/shared/components/list-view/edit-dialog.service';

@NgModule({
  declarations: [
    BspGdsAssignationListComponent,
    BspGdsAssignationDialogComponent,
    BspGdsAssignationModifyDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BspGdsAssignationRoutingModule,
    GdsModule,
    L10nTranslationModule,
    StoreModule.forFeature(fromGdsAgentList.GdsAgentListFeatureKey, fromGdsAgentList.reducers),
    EffectsModule.forFeature([GdsAgentListEffects])
  ],
  providers: [EditDialogService]
})
export class BspGdsAssignationModule {}
