import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { BspGdsAssignationListComponent } from './bsp-gds-assignation-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { BspGdsAssignation } from '~app/master-data/models/bsp-gds-assignation.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogComponent, DialogService } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage, QueryableDataSource } from '~app/shared/components/list-view';
import { EditDialogService } from '~app/shared/components/list-view/edit-dialog.service';
import { ROUTES } from '~app/shared/constants';
import { SortOrder } from '~app/shared/enums';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';

describe('BspGdsAssignationListComponent', () => {
  let component: BspGdsAssignationListComponent;
  let fixture: ComponentFixture<BspGdsAssignationListComponent>;

  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);
  const editDialogServiceSpy = createSpyObject(EditDialogService);
  const queryableDataSourceSpy = createSpyObject(QueryableDataSource);
  const storedQuery: DataQuery<BspGdsAssignation> = {
    filterBy: { expiryDateTo: new Date().getDate() + 1 },
    sortBy: [{ attribute: 'expiryDate', sortType: SortOrder.Asc }],
    paginateBy: {
      size: 20,
      totalElements: 1,
      page: 0
    }
  } as DataQuery<BspGdsAssignation>;
  const queryStorageSpy = createSpyObject(DefaultQueryStorage);

  queryableDataSourceSpy.hasData$ = of(true);
  queryableDataSourceSpy.appliedQuery$ = of(storedQuery);
  queryStorageSpy.get.and.returnValue(of(storedQuery));

  const initialState = {
    auth: {
      user: {} as User
    },
    router: null,
    core: {
      menu: {
        tabs: { gdsQuery: { ...ROUTES.BSP_GDS_ASSIGNATION, id: 'gdsQuery' } },
        activeTabId: 'gdsQuery'
      },
      viewListsInfo: {}
    }
  };

  const activatedRouteSpy = {
    snapshot: {
      data: {
        item: {}
      }
    }
  };

  const queryMock = {
    filterBy: { expiryDateFrom: new Date().getDate(), expiryDateTo: new Date().getDate() + 1 },
    sortBy: [{ attribute: 'expiryDate', sortType: SortOrder.Asc }],
    paginateBy: {
      size: 20,
      totalElements: 1,
      page: 0
    }
  } as DataQuery<BspGdsAssignation>;

  const bspGdsAssignationMock: BspGdsAssignation = {
    id: 698382828282,
    effectiveFrom: '2000-01-01',
    effectiveTo: '2000-01-01',
    refundsEnabled: true,
    gds: { id: 82828282, gdsCode: 'RRRR', name: 'RRRRRR', version: 94 },
    active: true,
    bsp: {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      active: true,
      version: 4492,
      defaultCurrencyCode: 'EUR'
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BspGdsAssignationListComponent, DialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: EditDialogService, useValue: editDialogServiceSpy },
        { provide: DefaultQueryStorage, useValue: queryStorageSpy },
        provideMockStore({ initialState }),
        FormBuilder,
        L10nTranslationService,
        PermissionsService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BspGdsAssignationListComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should populate query with DataQuery when search method is triggered', () => {
    spyOn(component, 'loadData');

    component.search(queryMock);
    expect((component as any).query).toEqual(queryMock);
    expect(component.loadData).toHaveBeenCalled();
  });

  it('should trigger open method from dialogService when onDownload method is triggered', () => {
    component['onDownload']();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should trigger navigate method from router when onViewGds method is triggered', () => {
    component['onViewGds'](bspGdsAssignationMock);

    expect(routerSpy.navigate).toHaveBeenCalledWith([698382828282], {
      relativeTo: { snapshot: { data: { item: {} } } },
      state: { gds: { id: 82828282, gdsCode: 'RRRR', name: 'RRRRRR', version: 94 } }
    });
  });

  it('should trigger open method from editDialogService when onDeleted, onEdit and onCreate methods are triggered', () => {
    component['onDelete'](bspGdsAssignationMock);
    component['onEdit'](bspGdsAssignationMock);
    component['onCreate']();

    expect(editDialogServiceSpy.open).toHaveBeenCalled();
  });

  it('should trigger onEdit method when the action type is equal to edit', () => {
    const actionMock = { action: { actionType: GridTableActionType.Edit }, row: bspGdsAssignationMock };
    const onEditSpy = spyOn<any>(component, 'onEdit');
    component.onActionClick(actionMock);

    expect(onEditSpy).toHaveBeenCalledWith(actionMock.row);
  });

  it('should trigger onDelete method when the action type is equal to delete', () => {
    const actionMock = { action: { actionType: GridTableActionType.Delete }, row: bspGdsAssignationMock };
    const onDeleteSpy = spyOn<any>(component, 'onDelete');
    component.onActionClick(actionMock);

    expect(onDeleteSpy).toHaveBeenCalledWith(actionMock.row);
  });

  it('should not trigger onDelete and onEdit methods when the action type is different to delete and edit', () => {
    const actionMock = { action: { actionType: GridTableActionType.Exception }, row: bspGdsAssignationMock };
    const onDeleteSpy = spyOn<any>(component, 'onDelete');
    const onEditSpy = spyOn<any>(component, 'onEdit');
    component.onActionClick(actionMock);

    expect(onDeleteSpy).not.toHaveBeenCalledWith(actionMock.row);
    expect(onEditSpy).not.toHaveBeenCalledWith(actionMock.row);
  });

  it('should return false when data is not available', fakeAsync(() => {
    spyOn(component.dataSource.data$, 'pipe').and.returnValue(of(Boolean(false)));

    component.hasData$.subscribe(hasData => {
      expect(hasData).toBe(false);
    });

    tick();
  }));

  it('should trigger loadData method when search method is triggered', () => {
    component['query'] = storedQuery;
    const queryMockOneFilter = {
      filterBy: [],
      sortBy: [{ attribute: 'expiryDate', sortType: SortOrder.Asc }],
      paginateBy: {
        size: 20,
        totalElements: 1,
        page: 0
      }
    } as DataQuery<BspGdsAssignation>;
    const loadDataSpy = spyOn<any>(component, 'loadData');
    component.search(queryMockOneFilter);

    expect(loadDataSpy).toHaveBeenCalled();
  });

  it('should initialize several component methods and subscribe to reactiveSubject when ngOnInit is triggered', () => {
    spyOn(component as any, 'initFilterForm');
    spyOn(component as any, 'initGridActions');
    spyOn(component as any, 'loadData');
    spyOn((component as any).dialogQueryFormSubmitEmitter, 'pipe').and.returnValue(of({}));
    spyOn((component as any).queryStorage, 'get').and.returnValue(storedQuery);

    component.ngOnInit();

    expect((component as any).initFilterForm).toHaveBeenCalled();
    expect((component as any).initGridActions).toHaveBeenCalled();
    expect((component as any).loadData).toHaveBeenCalled();
  });

  it('should check if dataSource hasData when it subscribes to hasData$ observable', () => {
    spyOn(component.dataSource.data$, 'pipe').and.returnValue(of(Boolean(true)));
    let result: boolean;
    component.hasData$.subscribe(value => {
      result = value;
    });

    expect(result).toBe(true);
  });

  it('should trigger loadData when search method is triggered', () => {
    component['query'] = queryMock;
    spyOn(component as any, 'loadData');

    component['search'](queryMock);

    expect(component.loadData).toHaveBeenCalledWith(queryMock);
  });
});
