import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { BspGdsAssignationDisplayFilterFormatter } from './bsp-gds-assignation-display-filter-formatter';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

const filters = {
  registerDate: [new Date('08/06/2023')],
  expiryDate: [new Date('08/06/2023')],
  refundsEnabled: 'yes',
  status: 'active'
};

const translate = (key: string) => `MASTER_DATA.bspGdsAssignation.list.${key}`;

describe('BspGdsAssignationDisplayFilterFormatter', () => {
  let formatter: BspGdsAssignationDisplayFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationSpy.translate.and.callFake(identity);
    formatter = new BspGdsAssignationDisplayFilterFormatter(translationSpy);
  });

  it('should create', () => {
    expect(formatter).toBeDefined();
  });

  it('should format all filters', () => {
    const result = formatter.format(filters);
    expect(result).toEqual([
      {
        keys: ['registerDate'],
        label: `${translate('registerDate')} - ${rangeDateFilterTagMapper(filters.registerDate)}`
      },
      {
        keys: ['expiryDate'],
        label: `${translate('expiryDate')} - ${rangeDateFilterTagMapper(filters.expiryDate)}`
      },
      {
        keys: ['refundsEnabled'],
        label: `${translate('refundsEnabled')} - ${translate('refundsStatus.yes')}`
      },
      {
        keys: ['status'],
        label: `${translate('status')} - ${translate('statusType.active')}`
      }
    ]);
  });

  it('should not format the filters when there are not filters selected', () => {
    const result = formatter.format([]);

    expect(result).toEqual([]);
  });
});
