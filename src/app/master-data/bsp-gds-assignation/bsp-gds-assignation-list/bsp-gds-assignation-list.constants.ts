import { TableColumn } from '@swimlane/ngx-datatable';

export const tableColumnsConfig: TableColumn[] = [
  {
    prop: 'gds.gdsCode',
    name: 'MASTER_DATA.bspGdsAssignation.list.gdsCode'
  },
  {
    prop: 'gds.name',
    name: 'MASTER_DATA.bspGdsAssignation.list.gdsName',
    cellTemplate: 'linkCellTmplWithObj'
  },
  {
    prop: 'refundsEnabled',
    name: 'MASTER_DATA.bspGdsAssignation.list.refundsEnabled',
    cellTemplate: 'yesNoTmpl'
  }
];

export const BSP_GDS_PROPERTIES = {
  GDS_NAME: 'gds.name',
  REFUNDS_ENABLED: 'refundsEnabled',
  STATUS: 'active',
  EFFECTIVE_FROM: 'effectiveFrom',
  EFFECTIVE_TO: 'effectiveTo'
};
