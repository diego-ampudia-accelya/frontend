import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep, isEqual } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { BspGdsAssignationModifyDialogComponent } from '../bsp-gds-assignation-modify-dialog/bsp-gds-assignation-modify-dialog.component';
import { BspGdsAssignationDisplayFilterFormatter } from './bsp-gds-assignation-display-filter-formatter';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { tableColumnsConfig } from '~app/master-data/bsp-gds-assignation/bsp-gds-assignation-list/bsp-gds-assignation-list.constants';
import {
  BspGdsAssignation,
  BspGdsAssignationModifyDialogType
} from '~app/master-data/models/bsp-gds-assignation.model';
import { BspGdsAssignationService } from '~app/master-data/services/bsp-gds-assignation.service';
import { GdsService } from '~app/master-data/services/gds.service';
import { AppState } from '~app/reducers';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery, DefaultQueryStorage } from '~app/shared/components/list-view';
import { EditDialogService } from '~app/shared/components/list-view/edit-dialog.service';
import { QueryableDataSource } from '~app/shared/components/list-view/queryable-data-source';
import { Permissions } from '~app/shared/constants';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

@Component({
  selector: 'bspl-bsp-gds-assignation-list',
  templateUrl: './bsp-gds-assignation-list.component.html',
  styleUrls: ['./bsp-gds-assignation-list.component.scss'],
  providers: [
    BspGdsAssignationDisplayFilterFormatter,
    DefaultQueryStorage,
    {
      provide: QueryableDataSource,
      useClass: QueryableDataSource,
      deps: [BspGdsAssignationService]
    }
  ]
})
export class BspGdsAssignationListComponent implements OnInit, OnDestroy {
  public columns = tableColumnsConfig;
  public filterForm: FormGroup;
  public gdsFilterOptions$: Observable<DropdownOption[]> = this.gdsService.getCodeNameDropdownOptions('name');
  public refundsEnabledFilterOptions = [
    { value: true, label: 'Yes' },
    { value: false, label: 'No' }
  ];
  public header = this.translationService.translate('MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.LBL');
  public userBsps$ = this.store.select(fromAuth.getAllBspNamesJoined);
  public customLabels = { create: 'MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.CREATE.btn' };
  public actions: {
    action: GridTableActionType;
    disabled: boolean;
  }[];
  public hasCreateGdsAssignationPermission: boolean;

  public get hasData$(): Observable<boolean> {
    return this.dataSource.data$.pipe(map(data => Boolean(data?.length)));
  }

  private query: DataQuery<BspGdsAssignation> = { filterBy: {}, paginateBy: {}, sortBy: [] };

  private dialogQueryFormSubmitEmitter = new Subject();
  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<BspGdsAssignation>,
    public bspGdsAssignationDisplayFilterFormatter: BspGdsAssignationDisplayFilterFormatter,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private editDialogService: EditDialogService,
    private gdsService: GdsService,
    private queryStorage: DefaultQueryStorage,
    private store: Store<AppState>,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly bspGdsAssignationService: BspGdsAssignationService,
    private readonly permissionsService: PermissionsService
  ) {}

  public ngOnInit(): void {
    this.initFilterForm();

    this.initGridActions();

    this.hasCreateGdsAssignationPermission = this.permissionsService.hasPermission(Permissions.createGds);

    const storedQuery = this.queryStorage.get();
    if (storedQuery) {
      this.query = storedQuery;
    }

    this.loadData();
    this.dialogQueryFormSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.loadData();
    });
  }

  private initGridActions(): void {
    this.actions = [
      { action: GridTableActionType.Edit, disabled: !this.permissionsService.hasPermission(Permissions.updateGds) },
      { action: GridTableActionType.Delete, disabled: !this.permissionsService.hasPermission(Permissions.deleteGds) }
    ];
  }

  public initFilterForm(): void {
    this.filterForm = this.formBuilder.group({
      gdsNames: [],
      refundsEnabled: [''],
      registerDate: ['']
    });
  }

  public onCreate(): void {
    this.editDialogService.open(
      BspGdsAssignationModifyDialogComponent,
      {
        data: {
          title: 'MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.CREATE.title',
          footerButtonsType: FooterButton.Create,
          hasCancelButton: true,
          isClosable: true,
          actionEmitter: this.dialogQueryFormSubmitEmitter
        }
      },
      { type: BspGdsAssignationModifyDialogType.Create }
    );
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: 'BUTTON.DEFAULT.DOWNLOAD',
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.query
      },
      apiService: this.bspGdsAssignationService
    });
  }

  public search(query: DataQuery<BspGdsAssignation>): void {
    if (!isEqual(this.query.filterBy, query.filterBy)) {
      query = cloneDeep(query);
      query.paginateBy.page = 0;
    }

    this.query = query;
    this.loadData(query);
  }

  public loadData(query = this.query): void {
    const dataQuery = {
      ...query,
      filterBy: {
        ...query.filterBy
      }
    };

    this.dataSource.get(dataQuery);
    this.queryStorage.save(dataQuery);
  }

  public onActionClick(event: { action: { actionType: GridTableActionType }; row: BspGdsAssignation }): void {
    switch (event.action.actionType) {
      case GridTableActionType.Edit:
        this.onEdit(event.row);
        break;
      case GridTableActionType.Delete:
        this.onDelete(event.row);
        break;
      default:
        break;
    }
  }

  public onViewGds(row: any): void {
    this.router.navigate([row.id], { relativeTo: this.activeRoute, state: { gds: row.gds } });
  }

  private onEdit(model: BspGdsAssignation): void {
    this.editDialogService.open(
      BspGdsAssignationModifyDialogComponent,
      {
        data: {
          title: 'MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.EDIT.title',
          footerButtonsType: FooterButton.Modify,
          hasCancelButton: true,
          isClosable: true,
          actionEmitter: this.dialogQueryFormSubmitEmitter
        }
      },
      {
        model,
        type: BspGdsAssignationModifyDialogType.Edit
      }
    );
  }

  private onDelete(model: BspGdsAssignation): void {
    this.editDialogService.open(
      BspGdsAssignationModifyDialogComponent,
      {
        data: {
          title: 'MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.DELETE.title',
          footerButtonsType: FooterButton.Delete,
          hasCancelButton: true,
          isClosable: true,
          actionEmitter: this.dialogQueryFormSubmitEmitter
        }
      },
      {
        model,
        type: BspGdsAssignationModifyDialogType.Delete
      }
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
