import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { tableColumnsConfig } from './bsp-gds-assignation-list.constants';
import { AppliedFilter, DefaultDisplayFilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

@Injectable({
  providedIn: 'root'
})
export class BspGdsAssignationDisplayFilterFormatter extends DefaultDisplayFilterFormatter {
  public tableColumnsConfig = tableColumnsConfig;

  constructor(private translationService: L10nTranslationService) {
    super();
  }

  format(filters: { [key: string]: any }): AppliedFilter[] {
    const { registerDate, expiryDate, refundsEnabled, status, ...rest } = filters;
    const formattedFilters = super.format(rest);

    if (registerDate) {
      const registerDateLabel = this.translationService.translate('MASTER_DATA.bspGdsAssignation.list.registerDate');

      formattedFilters.push({
        label: `${registerDateLabel} - ${rangeDateFilterTagMapper(registerDate)}`,
        keys: ['registerDate']
      });
    }

    if (expiryDate) {
      const expiryDateLabel = this.translationService.translate('MASTER_DATA.bspGdsAssignation.list.expiryDate');

      formattedFilters.push({
        label: `${expiryDateLabel} - ${rangeDateFilterTagMapper(expiryDate)}`,
        keys: ['expiryDate']
      });
    }

    if (refundsEnabled != null) {
      const refundsEnabledLabel = this.translationService.translate(
        'MASTER_DATA.bspGdsAssignation.list.refundsEnabled'
      );
      const refundsYes = this.translationService.translate('MASTER_DATA.bspGdsAssignation.list.refundsStatus.yes');
      const refundsNo = this.translationService.translate('MASTER_DATA.bspGdsAssignation.list.refundsStatus.no');
      formattedFilters.push({
        label: `${refundsEnabledLabel} - ${refundsEnabled ? refundsYes : refundsNo}`,
        keys: ['refundsEnabled']
      });
    }

    if (status != null) {
      const statusEnabledLabel = this.translationService.translate('MASTER_DATA.bspGdsAssignation.list.status');
      const isActive = this.translationService.translate('MASTER_DATA.bspGdsAssignation.list.statusType.active');
      const isInactive = this.translationService.translate('MASTER_DATA.bspGdsAssignation.list.statusType.inactive');
      formattedFilters.push({
        label: `${statusEnabledLabel} - ${status ? isActive : isInactive}`,
        keys: ['status']
      });
    }

    return formattedFilters;
  }
}
