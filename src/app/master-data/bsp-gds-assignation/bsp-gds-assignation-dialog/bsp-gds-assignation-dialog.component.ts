import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { camelCase, cloneDeep, get, mapValues, trimEnd } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { catchError, filter, finalize, takeUntil } from 'rxjs/operators';

import { BspGdsAssignation } from '~app/master-data/models/bsp-gds-assignation.model';
import { Gds } from '~app/master-data/models/gds.model';
import { BspGdsAssignationService } from '~app/master-data/services/bsp-gds-assignation.service';
import { BspService } from '~app/master-data/services/bsp.service';
import { GdsService } from '~app/master-data/services/gds.service';
import { DialogService, FooterButton, SelectComponent } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { SuccessEventType } from '~app/shared/components/list-view/edit-dialog.service';
import { FormUtil } from '~app/shared/helpers';
import { Bsp } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { NotificationService } from '~app/shared/services/notification.service';

const formControlValueMapper = (value: any) => {
  if (value instanceof Date) {
    return value.toISOString().split('T')[0];
  }

  return value;
};

@Component({
  selector: 'bspl-bsp-gds-assignation-dialog',
  templateUrl: './bsp-gds-assignation-dialog.component.html',
  styleUrls: ['./bsp-gds-assignation-dialog.component.scss']
})
export class BspGdsAssignationDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public gdsOptions$: Observable<DropdownOption[]>;
  public bspOptions$: Observable<DropdownOption[]>;
  public isEdit = false;
  public effectiveFromMinDate = new Date();
  public effectiveToMinDate = new Date();
  public isLoading = false;

  @ViewChild('gdsSelect', { static: true }) public gdsSelectComponent: SelectComponent;
  @ViewChild('bspSelect', { static: true }) public bspSelectComponent: SelectComponent;

  private get gdsSelectSelectedOption(): DropdownOption {
    return this.gdsSelectComponent.options.find(opt => opt.value === this.form.get('gdsId').value);
  }

  private get bspSelectSelectedOption(): DropdownOption {
    return this.bspSelectComponent.options.find(opt => opt.value === this.form.get('bspId').value);
  }

  private successEventType = SuccessEventType.itemCreated;

  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    private dialog: DialogService,
    public bspGdsAssignationService: BspGdsAssignationService,
    public gdsService: GdsService,
    public bspService: BspService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  ngOnInit() {
    const configItemData: BspGdsAssignation = this.config.data.itemData;
    if (configItemData) {
      this.isEdit = true;
      this.successEventType = SuccessEventType.itemModified;

      if (configItemData.gds) {
        this.getGdsOptionsFromConfigData(configItemData.gds);
      }

      if (configItemData.bsp) {
        this.getBspOptionsFromConfigData(configItemData.bsp);
      }
    } else {
      this.getBspAndGdsOptionsData();
    }

    this.createForm(configItemData);

    this.reactiveSubject.asObservable
      .pipe(
        filter(action => this.isCreateOrUpdate(action)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        if (this.form.valid) {
          this.saveBspGdsAssignation();
        } else {
          FormUtil.showControlState(this.form);
        }
      });
  }

  public getBspAndGdsOptionsData() {
    this.gdsOptions$ = this.gdsService.getCodeNameDropdownOptions('id');
    this.bspOptions$ = this.bspService.getActiveBspsDropDownOptions();
  }

  public getGdsOptionsFromConfigData(gds: Gds) {
    this.gdsOptions$ = of([{ value: gds.id, label: `${gds.gdsCode} / ${gds.name}` }]);
  }

  public getBspOptionsFromConfigData(bsp: Bsp) {
    this.bspOptions$ = of([{ value: bsp.id, label: bsp.name }]);
  }

  public saveBspGdsAssignation() {
    const postData = mapValues(this.form.value, formControlValueMapper);
    this.isLoading = true;

    return this.bspGdsAssignationService
      .save(cloneDeep(postData))
      .pipe(
        catchError(errors => {
          this.notifyAssignatinAlreadyExists(errors);

          return of(null);
        }),
        finalize(() => (this.isLoading = false))
      )
      .subscribe(itemData => {
        if (itemData) {
          this.notifyModificationSuccess(itemData);

          this.reactiveSubject.emit({
            dialog: this.dialog,
            successEventType: this.successEventType,
            itemData
          });
        }
      });
  }

  notifyAssignatinAlreadyExists(errors: any) {
    const errorMessage = trimEnd(camelCase(get(errors, 'error.errorMessage') || ''), '.');
    if (errorMessage === 'assignationAlreadyExists') {
      const gdsName = get(this.gdsSelectSelectedOption, 'label');
      const bspName = get(this.bspSelectSelectedOption, 'label');
      const errorMessageKey =
        gdsName && bspName
          ? 'MASTER_DATA.bspGdsAssignation.edit.assignationAlreadyExists'
          : 'MASTER_DATA.bspGdsAssignation.edit.assignationAlreadyExistsPlain';

      this.notificationService.showError(this.translationService.translate(errorMessageKey, { gdsName, bspName }));
    }
  }

  notifyModificationSuccess(itemData: any) {
    const gdsName = get(itemData, 'gds.name');
    const bspName = get(itemData, 'bsp.name');
    const successMessage =
      this.successEventType === SuccessEventType.itemCreated
        ? 'MASTER_DATA.bspGdsAssignation.edit.assignationCreationSuccess'
        : 'MASTER_DATA.bspGdsAssignation.edit.assignationUpdateSuccess';

    this.notificationService.showSuccess(this.translationService.translate(successMessage, { gdsName, bspName }));
  }

  isCreateOrUpdate(action: any) {
    const btn = action.clickedBtn;

    return btn && (btn === FooterButton.Assign || btn === FooterButton.Update);
  }

  createForm(itemData: any) {
    const formData = {
      id: get(itemData, 'id') || '',
      bspId: get(itemData, 'bsp.id') || '',
      gdsId: get(itemData, 'gds.id') || '',
      refundsEnabled: !!get(itemData, 'refundsEnabled'),
      effectiveFrom: get(itemData, 'effectiveFrom') || '',
      effectiveTo: get(itemData, 'effectiveTo') || ''
    };

    this.form = this.formBuilder.group({
      id: [formData.id],
      gdsId: [formData.gdsId, [Validators.required]],
      bspId: [formData.bspId, [Validators.required]],
      effectiveFrom: [formData.effectiveFrom, [Validators.required]],
      effectiveTo: [formData.effectiveTo],
      refundsEnabled: [formData.refundsEnabled]
    });

    if (this.isEdit) {
      this.form.get('gdsId').disable();
      this.form.get('bspId').disable();
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
}
