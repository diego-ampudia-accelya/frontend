import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslatePipe, L10nTranslationService } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';
import { of, Subject, throwError } from 'rxjs';
import { BspGdsAssignation } from '~app/master-data/models/bsp-gds-assignation.model';
import { Gds } from '~app/master-data/models/gds.model';
import { BspGdsAssignationService, BspService, GdsService } from '~app/master-data/services';
import { DialogService, FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { SuccessEventType } from '~app/shared/components/list-view/edit-dialog.service';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { Bsp } from '~app/shared/models/bsp.model';
import { NotificationService } from '~app/shared/services';
import { BspGdsAssignationDialogComponent } from './bsp-gds-assignation-dialog.component';

describe('BspGdsAssignationDialogComponent', () => {
  let component: BspGdsAssignationDialogComponent;
  let fixture: ComponentFixture<BspGdsAssignationDialogComponent>;

  const createSpyObj = jasmine.createSpyObj;
  const bspServiceSpy = createSpyObj('BspService', ['getActiveBspsDropDownOptions']);
  const gdsServiceSpy = createSpyObj('GdsService', ['getCodeNameDropdownOptions']);
  bspServiceSpy.getActiveBspsDropDownOptions.and.returnValue(new Subject());
  gdsServiceSpy.getCodeNameDropdownOptions.and.returnValue(new Subject());

  const initialState = {
    auth: {
      user: createIataUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };
  const notificationServiceStub = createSpyObject(NotificationService);
  const translationServiceStub = createSpyObject(L10nTranslationService);
  const dialogStub = createSpyObject(DialogService);
  const bspGdsAssignationServiceStub = createSpyObject(BspGdsAssignationService);
  const gdsServiceStub = createSpyObject(GdsService);
  const bspServiceStub = createSpyObject(BspService);

  const mockConfig = {
    data: {
      title: 'test title',
      footerButtonsType: FooterButton.Apply,
      texts: {
        description: '',
        header: ''
      },
      hasCancelButton: true,
      isClosable: true,
      buttons: [
        {
          type: FooterButton.Cancel
        },
        {
          type: FooterButton.Create
        }
      ],
      itemData: null
    }
  } as DialogConfig;

  const bspMock = {
    id: 6983,
    isoCountryCode: 'ES',
    name: 'SPAIN',
    effectiveFrom: '2000-01-01',
    active: true,
    version: 4492,
    defaultCurrencyCode: 'EUR'
  } as Bsp;

  const gdsMock = {
    id: 82828282,
    gdsCode: 'RRRR',
    name: 'RRRRRR',
    version: 94
  } as Gds;

  const itemDataMock = {
    id: 698382828282,
    effectiveFrom: '2000-01-01',
    effectiveTo: '2000-01-01',
    refundsEnabled: true,
    gds: gdsMock,
    active: true,
    bsp: bspMock
  } as BspGdsAssignation;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BspGdsAssignationDialogComponent, MockPipe(L10nTranslatePipe)],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({ initialState }),
        FormBuilder,
        ReactiveSubject,
        { provide: BspService, useValue: bspServiceSpy },
        { provide: GdsService, useValue: gdsServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: DialogService, useValue: dialogStub },
        { provide: BspGdsAssignationService, useValue: bspGdsAssignationServiceStub },
        { provide: GdsService, useValue: gdsServiceStub },
        { provide: BspService, useValue: bspServiceStub },
        { provide: L10nTranslationService, useValue: translationServiceStub },
        { provide: NotificationService, useValue: notificationServiceStub }
      ]
    }).compileComponents();
  }));

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(BspGdsAssignationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return true value if clicked button is Assign', () => {
    const result = component.isCreateOrUpdate({ clickedBtn: FooterButton.Assign });
    expect(result).toBe(true);
  });

  it('should return true value if clicked button is Update', () => {
    const result = component.isCreateOrUpdate({ clickedBtn: FooterButton.Update });
    expect(result).toBe(true);
  });

  it('should return false value if clicked button is NOT Assign or Update', () => {
    const result = component.isCreateOrUpdate({ clickedBtn: FooterButton.Cancel });
    expect(result).toBe(false);
  });

  it('should invoke gds and bsp services', () => {
    component.getBspAndGdsOptionsData();
    expect(component.gdsService.getCodeNameDropdownOptions).toHaveBeenCalled();
    expect(component.bspService.getActiveBspsDropDownOptions).toHaveBeenCalled();
  });

  it('should set correct gds data from configuration', () => {
    component.getGdsOptionsFromConfigData({ id: 1, name: 'GDS 1', gdsCode: '123' });
    component.gdsOptions$.subscribe(options => {
      expect(options).toEqual([{ value: 1, label: '123 / GDS 1' }]);
    });
  });

  it('should set Bsp options from config data when getBspOptionsFromConfigData is triggered', fakeAsync(() => {
    let bspOptionsResult;
    component.getBspOptionsFromConfigData(bspMock);
    component.bspOptions$.subscribe(data => {
      bspOptionsResult = data;
    });
    tick();

    expect(bspOptionsResult).toEqual([{ value: bspMock.id, label: bspMock.name }]);
  }));

  it('should create a form when ngOnInit life cycle is triggered', () => {
    const createFormSpy = spyOn(component, 'createForm');
    mockConfig.data.itemData = itemDataMock;
    component.ngOnInit();

    expect(createFormSpy).toHaveBeenCalledWith(mockConfig.data.itemData);
  });

  it('should return the selected option from gdsSelectComponent', () => {
    const gdsIdFormControlValue = 'selectedGdsIdValue';
    component.form = {
      get: jasmine.createSpy().and.returnValue({ value: gdsIdFormControlValue })
    } as any;

    const gdsSelectOptions = [
      { value: 'option1', label: 'Option 1' },
      { value: 'option2', label: 'Option 2' },
      { value: gdsIdFormControlValue, label: 'Selected Option' }
    ];
    component.gdsSelectComponent = {
      options: gdsSelectOptions
    } as any;
    const result = component['gdsSelectSelectedOption'];

    expect(result).toEqual(gdsSelectOptions[2]);
  });

  it('should return the selected option from bspSelectComponent', () => {
    const bspIdFormControlValue = 'selectedbspIdValue';
    component.form = {
      get: jasmine.createSpy().and.returnValue({ value: bspIdFormControlValue })
    } as any;

    const bspSelectOptions = [
      { value: 'option1', label: 'Option 1' },
      { value: 'option2', label: 'Option 2' },
      { value: bspIdFormControlValue, label: 'Selected Option' }
    ];
    component.bspSelectComponent = {
      options: bspSelectOptions
    } as any;
    const result = component['bspSelectSelectedOption'];

    expect(result).toEqual(bspSelectOptions[2]);
  });

  it('should notify creation success of itemCreated', () => {
    component['successEventType'] = SuccessEventType.itemCreated;
    component.notifyModificationSuccess(itemDataMock);

    expect(translationServiceStub.translate).toHaveBeenCalledWith(
      'MASTER_DATA.bspGdsAssignation.edit.assignationCreationSuccess',
      {
        gdsName: 'RRRRRR',
        bspName: 'SPAIN'
      }
    );
    expect(notificationServiceStub.showSuccess).toHaveBeenCalled();
  });

  it('should notify modification success of itemModified', () => {
    component['successEventType'] = SuccessEventType.itemModified;
    component.notifyModificationSuccess(itemDataMock);

    expect(translationServiceStub.translate).toHaveBeenCalledWith(
      'MASTER_DATA.bspGdsAssignation.edit.assignationUpdateSuccess',
      {
        gdsName: 'RRRRRR',
        bspName: 'SPAIN'
      }
    );
    expect(notificationServiceStub.showSuccess).toHaveBeenCalled();
  });

  it('should notify assignation already exists when there is an errorMessage', () => {
    const errors = { error: { errorMessage: 'assignationAlreadyExists' } };
    const selectOptions = [
      { value: 'option1', label: 'Option 1' },
      { value: 'option2', label: 'Option 2' }
    ];
    component.gdsSelectComponent = {
      options: selectOptions
    } as any;
    component.bspSelectComponent = {
      options: selectOptions
    } as any;
    component.notifyAssignatinAlreadyExists(errors);

    expect(translationServiceStub.translate).toHaveBeenCalled();
  });

  it('should save the itemData and notify modification success when saveBspGdsAssignation is triggered', fakeAsync(() => {
    const mockFormValue = {
      id: '123',
      gdsId: '456',
      bspId: '789',
      effectiveFrom: '2023-06-09',
      effectiveTo: '2023-06-30',
      refundsEnabled: true
    };

    component.form = { value: mockFormValue } as any;
    bspGdsAssignationServiceStub.save.and.returnValue(of(mockFormValue));
    component.saveBspGdsAssignation();

    bspGdsAssignationServiceStub.save().subscribe(() => {
      expect(bspGdsAssignationServiceStub.save).toHaveBeenCalledWith(mockFormValue);
    });
    tick();

    expect(component.isLoading).toBe(false);
  }));

  it('should catchError when saveBspGdsAssignation is triggered and assignation already exists', () => {
    spyOn(component, 'notifyAssignatinAlreadyExists');
    bspGdsAssignationServiceStub.save.and.callFake(() => throwError('Already Exists'));
    component.saveBspGdsAssignation();

    expect(component.notifyAssignatinAlreadyExists).toHaveBeenCalled();
  });
});
