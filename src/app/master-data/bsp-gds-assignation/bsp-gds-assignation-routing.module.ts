import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { ViewAs } from '../gds/gds-agent-list//models';
import { GdsAgentListComponent } from '../gds/gds-agent-list/gds-agent-list/gds-agent-list.component';
import { GdsProfileComponent } from '../gds/gds-profile/gds-profile.component';
import { GdsAgentListResolver } from '../gds/services/gds-agent-list.resolver';
import { GdsProfileResolver } from '../gds/services/gds-profile.resolver';

import { BspGdsAssignationListComponent } from './bsp-gds-assignation-list/bsp-gds-assignation-list.component';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';

const gdsAgentListRoute: Route = {
  path: 'gds-agent-list',
  component: GdsAgentListComponent,
  resolve: {
    requiredFilter: GdsAgentListResolver
  },
  data: {
    title: 'menu.masterData.gdsAgentList.title',
    viewAs: ViewAs.Gds,
    requiredPermissions: Permissions.readAgentGds
  }
};

const routes: Routes = [
  {
    path: '',
    component: BspGdsAssignationListComponent,
    data: {
      tab: ROUTES.BSP_GDS_ASSIGNATION
    }
  },
  {
    path: ':id',
    component: GdsProfileComponent,
    resolve: {
      gds: GdsProfileResolver
    },
    data: {
      tab: ROUTES.GDS_AGENTS_LIST,
      viewAs: ViewAs.Iata
    },
    children: [gdsAgentListRoute]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BspGdsAssignationRoutingModule {}
