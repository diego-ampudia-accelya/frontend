import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { AdmAcmAuthorityComponent, ViewAs } from '../adm-acm-authority';
import { NotificationSettingsResolver } from '../airline/services/notification-settings.resolver';
import { ConfigurationComponent } from '../configuration';
import { SettingsViewComponent } from '../configuration/settings-view/settings-view.component';
import { UnsavedChangesGuard } from '../configuration/unsaved-changes.guard';
import { SftpConfigurationsResolver } from '../sftp/services/sftp-configurations-resolver.service';
import { SftpAccountsComponent } from '../sftp/sftp-accounts/sftp-accounts.component';

import { EmailAlertsSettingsComponent } from '../configuration/email-alert-settings/email-alert-settings.component';
import { EmailAlertsSettingResolver } from './services/email-alerts-setting.resolver';
import { ThirdPartyProfileComponent } from './third-party-profile/third-party-profile.component';
import { UserType } from '~app/shared/models/user.model';
import { ROUTES } from '~app/shared/constants/routes';
import { Permissions } from '~app/shared/constants/permissions';

const configurationRoute: Route = {
  path: 'configuration',
  component: ConfigurationComponent,
  data: {
    title: 'menu.masterData.configuration.title',
    requiredPermissions: [Permissions.readThirdPartySettings]
  },
  children: [
    {
      path: 'email-alerts-settings',
      resolve: {
        loadSettings: EmailAlertsSettingResolver
      },
      component: EmailAlertsSettingsComponent,

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.emailAlerts.groupName',
        title: 'menu.masterData.configuration.emailAlerts.sectionTitle',
        viewAs: ViewAs.ThirdParty,
        configuration: {
          title: 'menu.masterData.configuration.emailAlerts.sectionTitle'
        },
        requiredPermissions: Permissions.readEmailAlerts
      }
    },
    {
      path: 'notifications',
      component: SettingsViewComponent,
      resolve: {
        loadSettings: NotificationSettingsResolver
      },

      canDeactivate: [UnsavedChangesGuard],
      data: {
        group: 'menu.masterData.configuration.notificationsGroupName',
        title: 'menu.masterData.configuration.notificationsSettings',
        configuration: {
          title: 'menu.masterData.configuration.notificationsSettingsTitle',
          permissions: {
            read: Permissions.readNotificationSettings,
            update: Permissions.updateNotificationSettings
          }
        },
        requiredPermissions: [Permissions.readNotificationSettings]
      },
      runGuardsAndResolvers: 'always'
    },
    {
      path: 'sftp-settings',
      resolve: {
        loadSettings: SftpConfigurationsResolver
      },
      component: SftpAccountsComponent,
      data: {
        group: 'menu.masterData.configuration.sftpConfigurations.groupName',
        title: 'menu.masterData.configuration.sftpConfigurations.sectionTitle',
        requiredPermissions: [Permissions.readSftpAccount]
      }
    }
  ]
};

const thirdPartyProfileChildren: Route[] = [
  configurationRoute,
  {
    path: 'adm-acm-authority',
    component: AdmAcmAuthorityComponent,
    data: {
      title: 'LIST.MASTER_DATA.admAcmAuthority.tabLabel',
      allowedUserTypes: [UserType.THIRD_PARTY],
      viewAs: ViewAs.ThirdParty,
      requiredPermissions: [Permissions.readACDMAuthorityThirdParty]
    }
  }
];

const routes: Routes = [
  { path: '', redirectTo: 'my', pathMatch: 'full' },
  {
    path: 'my',
    component: ThirdPartyProfileComponent,
    data: {
      tab: ROUTES.MY_THIRD_PARTY,
      allowedUserTypes: [UserType.THIRD_PARTY]
    },
    children: thirdPartyProfileChildren
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThirdPartyRoutingModule {}
