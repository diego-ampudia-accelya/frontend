import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdmAcmAuthorityModule } from '../adm-acm-authority/adm-acm-authority.module';
import { ConfigurationModule } from '../configuration';
import { SftpModule } from '../sftp/sftp.module';

import { ThirdPartyProfileComponent } from './third-party-profile/third-party-profile.component';
import { ThirdPartyRoutingModule } from './third-party-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ThirdPartyRoutingModule,
    SharedModule,
    AdmAcmAuthorityModule,
    ConfigurationModule,
    SftpModule
  ],
  declarations: [ThirdPartyProfileComponent, ThirdPartyProfileComponent],
  providers: [],
  exports: []
})
export class ThirdPartyModule {}
