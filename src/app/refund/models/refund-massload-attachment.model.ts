export interface RefundMassloadAttachment {
  id: number;
  filename: string;
  documentNumber: string;
  documentId: number;
  documentTrnc: string;
  uploadDate: Date;
  userName: string;
  email: string;
}

export interface RefundUploadMassloadDialogPayload {
  id: string;
  attachmentIds: string[];
}

export enum UploadDialogAction {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  Reset = 'Reset'
}
