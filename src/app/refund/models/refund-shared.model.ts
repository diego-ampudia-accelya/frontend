/* eslint-disable @typescript-eslint/naming-convention */
export interface RelatedTicketDocument {
  coupon1: boolean;
  coupon2: boolean;
  coupon3: boolean;
  coupon4: boolean;
  documentNumber: string;
  documentId?: string;
}

export interface OriginalIssue {
  airlineCode: string;
  agentCode: string;
  documentNumber: string;
  issueDate: string;
  locationCode: string;
}

export interface Commission {
  amount: number;
  rate: number;
}

export interface TaxMiscellaneousFee {
  type: string;
  amount: number;
}

export interface CardPayment {
  amount: number;
  card: string;
  cardMask?: string;
  payment: string;
  type: string;
}

export enum RefundOptionType {
  Loading = 'LOADING',
  Read = 'READ',
  Unread = 'UNREAD',
  Archive = 'ARCHIVE',
  Authorize = 'AUTHORIZE',
  Reject = 'REJECT',
  Delete = 'DELETE',
  Comment = 'COMMENT',
  Supervise = 'SUPERVISE',
  DownloadAttachments = 'DOWNLOAD_ATTACHMENTS',
  InternalComment = 'INTERNAL_COMMENT'
}
