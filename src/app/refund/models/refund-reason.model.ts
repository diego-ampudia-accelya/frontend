import { Reason } from '~app/master-data/configuration';

export interface RefundReason extends Omit<Reason, 'type'> {
  bspId: number;
}
