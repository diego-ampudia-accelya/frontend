/* eslint-disable @typescript-eslint/naming-convention */
import { Observable } from 'rxjs';

import { AgentDto, AirlineDto, Contact, CurrencyDto, RefundStatus, SendToDpcType } from './refund-be-aux.model';
import {
  CardPayment,
  Commission,
  OriginalIssue,
  RelatedTicketDocument,
  TaxMiscellaneousFee
} from './refund-shared.model';
import {
  AmountTaxViewModel,
  BasicInformationViewModel,
  DetailViewModel,
  FormOfPaymentViewModel,
  StatusHeaderViewModel
} from './refund-view-aux.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { AgentAddress } from '~app/master-data/models/agent-address.model';

export interface RefundBE {
  id?: number;
  agent: AgentDto;
  agentAddress: AgentAddress;
  agentContact: Contact;
  agentVatNumber?: string;
  airline: AirlineDto;
  airlineAddress: AirlineAddress;
  airlineCodeRelatedDocument: string;
  airlineContact: Contact;
  airlineVatNumber?: string;
  airlineRemark?: string;
  attachmentIds?: string[];
  cancellationPenalty?: number;
  cardPayments: CardPayment[];
  cashPaymentAmount?: number;
  changedByAgent?: string;
  commission?: Commission;
  commissionForCpAndMf?: Commission;
  currency: CurrencyDto;
  customerFileReference?: string;
  dateOfApplication?: string;
  dateOfAuthorization?: string;
  dateOfIssue?: string;
  dateOfIssueRelatedDocument: string;
  dateOfRejection?: string;
  deletionReason?: string;
  grossFare?: number;
  finalType?: RefundFinal;
  finalDaysLeft?: number;
  finalProcessStopped?: number;
  internalComment?: string;
  lessGrossFareUsed?: number;
  miscellaneousCashPaymentAmount?: number;
  miscellaneousFee?: number;
  netReporting: boolean;
  originalIssue: OriginalIssue;
  partialRefund: boolean;
  passenger: string;
  period?: string;
  reasonForRefund: string;
  rejectionReason?: string;
  relatedTicketConjunctions: RelatedTicketDocument[];
  relatedTicketDocument: RelatedTicketDocument;
  relatedTicketExchange: boolean;
  resubmissionRemark: string;
  sentToAirline?: string;
  sentToDpc: SendToDpcType;
  settlementAuthorisationCode?: string;
  statisticalCode?: string;
  status: RefundStatus;
  supplementaryCommission?: number;
  taxMiscellaneousFees: TaxMiscellaneousFee[];
  taxOnCancellationPenalty?: number;
  taxOnMiscellaneousFee?: number;
  ticketDocumentNumber?: string;
  totalAmount?: number;
  totalGrossFare?: number;
  totalPaymentAmount?: number;
  totalPaymentCardAmount?: number;
  tourCode?: string;
  type?: string;
  version?: number;
  waiverCode?: string;
  formsOfPayment?: RefundFormOfPayment[];
  predefinedReason?: string; // Property to use the value of dropdown in reason detail
  watching?: boolean;
}

export interface RefundViewModel {
  id?: number;
  attachmentIds?: string[];
  statusHeader: StatusHeaderViewModel;
  basicInformation: BasicInformationViewModel;
  details: DetailViewModel;
  formPayment: FormOfPaymentViewModel;
  amounts: AmountTaxViewModel;
}

export enum RefundActionEmitterType {
  totalAmount = 'totalAmount',
  agent = 'agent',
  airline = 'airline',
  currency = 'currency',
  bsp = 'bsp',
  partialRefund = 'partialRefund'
}

export enum PaymentMethodsType {
  card1 = 'CARD1',
  card2 = 'CARD2',
  easypay = 'EASYPAY',
  miscellaneous = 'MISCELLANEOUS',
  cash = 'cash',
  miscCash = 'miscCash'
}

export enum RefundActionType {
  issue = 'issue',
  edit = 'edit',
  supervise = 'supervise',
  applyChanges = 'applyChanges',
  investigate = 'investigate',
  authorize = 'authorize',
  reject = 'reject',
  delete = 'delete',
  issuePendingSupervision = 'issuePendingSupervision',
  startResubmission = 'startResubmission',
  resubmit = 'resubmit',
  comment = 'comment'
}

export interface RefundConfiguration {
  freeStat: boolean;
  defaultStat: string;
  airlineVatnumber: boolean;
  agentVatNumber: boolean;
  withoutCouponsCapture: boolean;
  nrRefundsPermitted: boolean;
  fareAdjustmentAmount: boolean;
  raFinal: boolean;
  mfFeeAmount: number;
}

export interface RefundNoticeConfiguration {
  id?: number;
  originalIssueDetailsEnabled: boolean;
  partialRefundEnabled: boolean;
  discountAmountEnabled: boolean;
  secondPaymentCardEnabled: boolean;
  vatnumberEnabled: boolean;
  contactEnabled: boolean;
  settlementAuthorisationCodeEnabled: boolean;
  customerFileReferenceEnabled: boolean;
  taxOnMfCpEnabled: boolean;
}

export interface RefundAirlineConfiguration {
  id: number;
  descriptorId: number;
  scopeType: string;
  scopeId: string;
  service: string;
  parameters: AirlineConfigParameters[];
}

export interface AirlineConfigParameters {
  id: number;
  descriptorId: number;
  type: string;
  name: AirlineConfigParamName;
  value: string;
  readonly: boolean;
}

export enum AirlineConfigParamName {
  RfndNoticeIssue = 'rfnd_notice_issue',
  RfndAplIssue = 'rfnd_apl_issue',
  handlingFee = 'HANDLING_FEE',
  penaltyCharge = 'PENALTY_CHARGE',
  comissionOnCp = 'COMMISSION_ON_CP'
}

export interface RefundHeaderBtnModel {
  label: string;
  design: string;
  hidden: Observable<boolean>;
  disabled: Observable<boolean>;
  actionType: RefundActionType;
}

export enum RefundFormOfPayment {
  CA = 'CA',
  CC = 'CC',
  EP = 'EP',
  MSCA = 'MSCA',
  MSCC = 'MSCC'
}

export enum RefundFinal {
  Final = 'FINAL',
  Exception = 'EXCEPTION'
}

export enum RefundType {
  RefundApplication = 'REFUND_APPLICATION',
  RefundNotice = 'REFUND_NOTICE'
}
