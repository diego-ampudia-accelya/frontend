import { FileValidator } from '~app/shared/components/upload/file-validators';

export interface RefundUploadMassloadDialogConfig {
  uploadingTitle?: string;
  uploadedTitle?: string;
  uploadPath?: string;
  filesToMass: File[];
  filesToAttach: File[];
  validationRules?: FileValidator[];
  secondaryTittleMass?: string;
  secondaryTittleAttach?: string;
}
