export interface RefundMassloadAttachmentFilter {
  filename: string;
  documentNumber: string;
  uploadDate: Date[];
  userName: string;
  email: string;
}
