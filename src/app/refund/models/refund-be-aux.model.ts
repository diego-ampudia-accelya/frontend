import { Agent } from '~app/master-data/models';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { LocalAndGlobalAirline } from '~app/master-data/models/airline/local-and-global-airline.model';

export interface Contact {
  contactName: string;
  email: string;
  phoneFaxNumber: string;
}

export interface BasicAgentInfo {
  agent: Agent;
  agentVatNumber: string;
  agentAddress: AgentAddress;
  agentContact: Contact;
}

export interface BasicAirlineInfo {
  airline: LocalAndGlobalAirline;
  airlineVatNumber: string;
  airlineAddress: AirlineAddress;
  airlineContact: Contact;
}

export type AgentDto = DTO;
export type AirlineDto = DTO;
export type CurrencyDto = DTO;

export interface DTO {
  id: number;
}

export enum RefundStatus {
  issued = 'ISSUED',
  pending = 'PENDING',
  authorized = 'AUTHORIZED',
  rejected = 'REJECTED',
  investigated = 'UNDER_INVESTIGATION',
  pendingSupervision = 'PENDING_SUPERVISION',
  deleted = 'DELETED',
  supervisionDeleted = 'SUPERVISION_DELETED',
  resubmitted = 'RESUBMITTED'
}

export enum SendToDpcType {
  sent = 'SENT',
  pending = 'PENDING',
  na = 'N/A', //* Value for getById endpoint response
  // eslint-disable-next-line @typescript-eslint/naming-convention
  n_a = 'N_A' //* Value for getAll endpoint response
}

export enum YesNoShortType {
  s = 'S',
  n = 'N'
}

export enum StatisticalCodeOptions {
  international = 'I',
  domestic = 'D'
}

export interface BspTimeModel {
  id: number;
  localTime: Date;
}
