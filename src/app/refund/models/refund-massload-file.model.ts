/* eslint-disable @typescript-eslint/naming-convention */
export interface RefundMassloadFileModel {
  id: number;
  originalFilename: string;
  filename: string;
  uploadDate: Date;
  userName: string;
  email: string;
  authorizedRefunds?: number;
  rejectedRefunds?: number;
  acceptedTransactions?: number;
  transactions?: number;
  status: RefundMassloadFileStatus;
  evaluationFileId?: string;
}

export interface RefundMassloadFileTotals {
  authorizedRefunds: number;
  rejectedRefunds: number;
  acceptedTransactions: number;
  transactions: number;
}

export enum RefundMassloadFileStatus {
  Processed = 'PROCESSED',
  Rejected = 'REJECTED',
  Pending = 'PENDING'
}
