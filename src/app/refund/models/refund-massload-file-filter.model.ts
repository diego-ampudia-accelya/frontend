import { RefundMassloadFileStatus } from './refund-massload-file.model';

export interface RefundMassloadFileFilter {
  filename: string;
  originalFileName: string;
  uploadDate: Date[];
  userName: string;
  email: string;
  status: RefundMassloadFileStatus[];
}
