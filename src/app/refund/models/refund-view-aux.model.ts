import { Contact, RefundStatus, SendToDpcType } from './refund-be-aux.model';
import {
  CardPayment,
  Commission,
  OriginalIssue,
  RelatedTicketDocument,
  TaxMiscellaneousFee
} from './refund-shared.model';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

export interface BasicInformationViewModel {
  airline: AirlineViewModel;
  agent: AgentViewModel;
  airlineContact: ContactViewModel;
  agentContact: ContactViewModel;
  bspName?: string;
}

export interface AirlineViewModel {
  id: number;
  iataCode: string;
  localName: string;
  globalName: string;
  vatNumber: string;
  bsp: BspDto;
  contact: Contact;
  address: AirlineAddress;
  rfndVatNumber?: string;
}

export interface ContactViewModel {
  contactName: string;
  email: string;
  phoneFaxNumber: string;
}

export interface AgentViewModel {
  id: number;
  iataCode: string;
  name: string;
  vatNumber: string;
  bsp: BspDto;
  contact: Contact;
  address: AgentAddress;
  rfndVatNumber?: string;
}

export interface StatusHeaderViewModel {
  docNumber: string;
  status: StatusViewModel;
  bspName: string;
  airlCode: string; //* To be connected with AirlineViewModel.iataCode
  agentCode: string; //* To be connected with AgentViewModel.iataCode
  appDate: string;
  amount: string; //* String because will include already the currency (related with totalAmount)
  finalType: string;
  finalDaysLeft: number;
  finalProcessStopped: number;
}

export interface StatusViewModel {
  status: string;
  sentToDpc: SendToDpcType;
}

export interface CurrencyViewModel {
  code: string;
  decimals: number;
  id: number;
  isDefault: boolean;
}

export interface FormOfPaymentViewModel {
  currency: CurrencyViewModel;
  tourCode: number;
  customerReference: string;
  eTicketAuthCode: string;
  cardPayments: CardPayment[];
  cashPaymentAmount: number;
  miscellaneousCashAmount: number;
  cardTotal: number;
  cashTotal: number;
  totalAmount: number;
}

export interface DetailViewModel {
  statisticalCode: string; //* INT (label: International), DOM (label: Domestic)
  reasonForRefund: string;
  airlineRemark?: string;
  passenger: string;
  dateOfIssueRelatedDocument: string;
  waiverCode: string;
  airlineCodeRelatedDocument: string;
  rejectedDocumentId: string;
  rejectionReason?: string;
  relatedTicketDocument: RelatedTicketDocument;
  relatedTicketConjunctions: RelatedTicketDocument[];
  relatedTicketExchange: boolean;
  originalIssue: OriginalIssue;
  resubmissionRemark: string;
  predefinedReason?: string;
}

export interface AmountTaxViewModel {
  partialRefund: boolean;
  netReporting: boolean;
  grossFare: number;
  lessGrossFareUsed: number;
  totalGrossFare: number;
  commission: Commission;
  commissionForCpAndMf: Commission;
  supplementaryCommission: number;
  cancellationPenalty: number;
  taxOnCancellationPenalty: number;
  miscellaneousFee: number;
  taxOnMiscellaneousFee: number;
  taxMiscellaneousFees: TaxMiscellaneousFee[];
  totalAmount: number;
}

export interface RejectViewModel {
  id: number;
  status: RefundStatus;
  reason?: string;
  rejectionReason: string;
  attachmentIds?: string[];
  airlineRemark?: string;
}

export interface SecondaryHeaderActionsViewModel {
  label: string;
  action: GridTableActionType;
}
