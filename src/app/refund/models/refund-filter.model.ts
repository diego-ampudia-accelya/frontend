import { RefundStatus, SendToDpcType, StatisticalCodeOptions, YesNoShortType } from './refund-be-aux.model';
import { RefundFinal, RefundFormOfPayment } from './refund.model';
import { PeriodOption } from '~app/shared/components';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';

export interface RefundFilter {
  ticketDocumentNumber?: string;
  bsp?: BspDto[] | BspDto;
  airlineIataCode?: AirlineSummary[];
  agentId?: AgentSummary[];
  status?: RefundStatus[] | string[];
  dateOfApplication?: Date[];
  dateOfIssue?: Date[];
  dateOfRejection?: Date[];
  dateOfLatestAction?: Date[];
  reportingDate?: Date[];
  currencyId?: Currency[];
  amountRange?: number[];
  relatedTicketDocument?: string;
  passenger?: string;
  period?: PeriodOption[];
  sentToDpc?: SendToDpcType[];
  sentToAirline?: YesNoShortType;
  changedByAgent?: YesNoShortType;
  hasInternalComment?: boolean;
  markStatus?: string[];
  formsOfPayment?: RefundFormOfPayment[];
  finalType?: RefundFinal[];
  finalDaysLeftRange?: number[];
  finalProcessStoppedRange?: number[];
  statisticalCode?: StatisticalCodeOptions;
}
