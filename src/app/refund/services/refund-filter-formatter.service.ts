import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';

import { RefundFilter } from '../models/refund-filter.model';
import { RefundFinal } from '../models/refund.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { rangeDateFilterTagMapper, rangeFilterTagMapper } from '~app/shared/helpers';
import { EMPTY_VALUE } from '~app/shared/pipes/empty.pipe';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class RefundFilterFormatter implements FilterFormatter {
  public ignoredFilters: Array<keyof RefundFilter> = [];

  private refundType: MasterDataType;

  constructor(
    private translationService: L10nTranslationService,
    private activatedRoute: ActivatedRoute,
    private periodPipe: PeriodPipe
  ) {
    this.refundType = this.activatedRoute.snapshot.data.refundType;
  }

  public format(filter: RefundFilter): AppliedFilter[] {
    const filterMappers: Partial<FilterMappers<RefundFilter>> = {
      ticketDocumentNumber: docNumber => `${this.translate('commons', 'docNumber')} - ${docNumber}`,
      agentId: agents => `${this.translate('commons', 'agentTag')} - ${agents.map(agt => agt.code).join(', ')}`,
      airlineIataCode: airlines =>
        `${this.translate('commons', 'airlineTag')} - ${airlines.map(airl => airl.code).join(', ')}`,
      bsp: bsps => {
        const bspsArray = !Array.isArray(bsps) ? [bsps] : bsps;
        const bspMapper = bsp => `${bsp.name} (${bsp.isoCountryCode})`;

        return `${this.translate('commons', 'countryTag')} - ${bspsArray.map(bspMapper).join(', ')}`;
      },
      currencyId: currencies =>
        `${this.translate('commons', 'currency')} - ${currencies.map(currency => currency.code).join(', ')}`,
      sentToDpc: toDpcList => {
        const dpcListMapper = sent => this.translationService.translate(`REFUNDS.sentToDpcStatus.${sent}`);

        return `${this.translate('commons', 'sentToDpc')} - ${toDpcList.map(dpcListMapper).join(', ')}`;
      },
      status: (statusList: string[]) => {
        const statusListMapper = status => this.translationService.translate(`REFUNDS.status.${status}`);

        return `${this.translate('commons', 'status')} - ${statusList.map(statusListMapper).join(', ')}`;
      },
      passenger: passenger => `${this.translate('commons', 'passenger')} - ${passenger}`,
      relatedTicketDocument: rtdn => `${this.translate('commons', 'rtdnNumber')} - ${rtdn}`,
      reportingDate: date => `${this.translate(this.refundType, 'reportingDate')} - ${rangeDateFilterTagMapper(date)}`,
      dateOfApplication: date =>
        `${this.translate('refund-application', 'appDate')} - ${rangeDateFilterTagMapper(date)}`,
      dateOfRejection: date =>
        `${this.translate('refund-application', 'rejAccDate')} - ${rangeDateFilterTagMapper(date)}`,
      dateOfIssue: date => `${this.translate('refund-notice', 'issueDate')} - ${rangeDateFilterTagMapper(date)}`,
      dateOfLatestAction: date =>
        `${this.translate('refund-notice', 'latestActDate')} - ${rangeDateFilterTagMapper(date)}`,
      period: periods => this.periodPipe.transform(periods),
      sentToAirline: sentToAirl => {
        const sendToAirlTranslation = this.translationService.translate(
          `REFUNDS.query.commons.yesNoShortValue.${sentToAirl}`
        );

        return `${this.translate('refund-application', 'sentToAirline')} - ${sendToAirlTranslation}`;
      },
      changedByAgent: changedByAgt => {
        const changedByAgtTranslation = this.translationService.translate(
          `REFUNDS.query.commons.yesNoShortValue.${changedByAgt}`
        );

        return `${this.translate('refund-application', 'changedByAgent')} - ${changedByAgtTranslation}`;
      },
      hasInternalComment: hasInternalComment => {
        const hasInternalCommentTranslation = this.translationService.translate(
          `REFUNDS.query.commons.yesNoBooleanValue.${hasInternalComment}`
        );

        return `${this.translate('commons', 'internalComment')} - ${hasInternalCommentTranslation}`;
      },
      formsOfPayment: payments => {
        const paymentMapper = payment => this.translationService.translate(`REFUNDS.formsOfPayment.${payment}`);

        return `${this.translate('commons', 'formsOfPayment')} - ${payments.map(paymentMapper).join(', ')}`;
      },
      markStatus: markStatus => {
        const markStatusTranslation = this.translationService.translate(`REFUNDS.markStatus.${markStatus}`);

        return `${this.translate('refund-application', 'markStatus')}  - ${markStatusTranslation}`;
      },
      finalType: finalTypes =>
        `${this.translate('refund-application', 'final')} - ${finalTypes
          .map(finalType => this.translateFinalType(finalType))
          .join(', ')}`,
      finalDaysLeftRange: days => `${this.translate('refund-application', 'daysLeft')}: ${rangeFilterTagMapper(days)}`,
      finalProcessStoppedRange: days =>
        `${this.translate('refund-application', 'processStopped')}: ${rangeFilterTagMapper(days)}`,
      amountRange: amounts =>
        `${this.translate('refund-application', 'amountRange')}: ${rangeFilterTagMapper(amounts)}`,
      statisticalCode: code =>
        `${this.translate('refund-application', 'statisticalCode')} - ${this.translationService.translate(
          `REFUNDS.query.commons.StatisticalCodeOptions.${code}`
        )}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => item.value !== null && item.mapper && !this.ignoredFilters.includes(item.key as any))
      .map(item => ({ keys: [item.key], label: item.mapper(item.value) }));
  }

  private translate(typeKey: string, key: string): string {
    return this.translationService.translate('REFUNDS.query.' + typeKey + '.filters.labels.' + key);
  }

  private translateFinalType(finalType: RefundFinal): string {
    return finalType === null ? EMPTY_VALUE : this.translationService.translate(`REFUNDS.finalType.${finalType}`);
  }
}
