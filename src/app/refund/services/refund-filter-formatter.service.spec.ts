import { TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { SendToDpcType, YesNoShortType } from '../models/refund-be-aux.model';
import { RefundFilter } from '../models/refund-filter.model';
import { RefundFilterFormatter } from './refund-filter-formatter.service';
import { AdmAcmStatus } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';

describe('RefundFilterFormatterService', () => {
  let formatter: RefundFilterFormatter;
  const translationSpy = createSpyObject(L10nTranslationService);
  const periodPipeSpy = createSpyObject(PeriodPipe);
  const activatedRouteStub = {
    snapshot: {
      data: {
        refundType: MasterDataType.RefundApp
      }
    }
  };

  const mockPeriodPipeResult = 'MOCK PERIODS';

  beforeEach(waitForAsync(() => {
    translationSpy.translate.and.callFake(identity);
    periodPipeSpy.transform.and.callFake(() => mockPeriodPipeResult);

    TestBed.configureTestingModule({
      providers: [
        RefundFilterFormatter,
        { provide: L10nTranslationService, useValue: translationSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: PeriodPipe, useValue: periodPipeSpy }
      ]
    });
  }));

  beforeEach(() => {
    formatter = TestBed.inject(RefundFilterFormatter);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format when document number is defined', () => {
    const filter: RefundFilter = {
      ticketDocumentNumber: '1234567'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['ticketDocumentNumber'],
        label: `REFUNDS.query.commons.filters.labels.docNumber - 1234567`
      }
    ]);
  });

  it('should format when agents are defined', () => {
    const filter: RefundFilter = {
      agentId: [
        { id: '1', code: '1111', name: 'agent1' },
        { id: '2', code: '2222', name: 'agent2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agentId'],
        label: `REFUNDS.query.commons.filters.labels.agentTag - 1111, 2222`
      }
    ]);
  });

  it('should format when airlines are defined', () => {
    const filter: RefundFilter = {
      airlineIataCode: [
        { id: 1, code: '111', name: 'airline1', designator: 'QA' },
        { id: 2, code: '222', name: 'airline2', designator: 'LA' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airlineIataCode'],
        label: `REFUNDS.query.commons.filters.labels.airlineTag - 111, 222`
      }
    ]);
  });

  it('should format when bsps are defined', () => {
    const filter: RefundFilter = {
      bsp: [
        { id: 1, isoCountryCode: 'ES', name: 'bsp1' },
        { id: 2, isoCountryCode: 'TE', name: 'bsp2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['bsp'],
        label: `REFUNDS.query.commons.filters.labels.countryTag - bsp1 (ES), bsp2 (TE)`
      }
    ]);
  });

  it('should format when currencies are defined', () => {
    const filter: RefundFilter = {
      currencyId: [
        { id: 1, code: 'ESP', decimals: 0 },
        { id: 2, code: 'EUR', decimals: 2 }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['currencyId'],
        label: `REFUNDS.query.commons.filters.labels.currency - ESP, EUR`
      }
    ]);
  });

  it('should format when sentToDpc is defined', () => {
    const filter: RefundFilter = {
      sentToDpc: [SendToDpcType.sent]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['sentToDpc'],
        label: `REFUNDS.query.commons.filters.labels.sentToDpc - REFUNDS.sentToDpcStatus.SENT`
      }
    ]);
  });

  it('should format when status are defined', () => {
    const filter: RefundFilter = {
      status: [AdmAcmStatus.pending, AdmAcmStatus.deactivated]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['status'],
        label: `REFUNDS.query.commons.filters.labels.status - REFUNDS.status.PENDING, REFUNDS.status.DEACTIVATED`
      }
    ]);
  });

  it('should format when passenger is defined', () => {
    const filter: RefundFilter = {
      passenger: '1234567'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['passenger'],
        label: `REFUNDS.query.commons.filters.labels.passenger - 1234567`
      }
    ]);
  });

  it('should format when relatedTicketDocument is defined', () => {
    const filter: RefundFilter = {
      relatedTicketDocument: '1234567'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['relatedTicketDocument'],
        label: `REFUNDS.query.commons.filters.labels.rtdnNumber - 1234567`
      }
    ]);
  });

  it('should format when reportingDate is defined', () => {
    const filter: RefundFilter = {
      reportingDate: [new Date('2020/06/29'), new Date('2020/07/02')]
    };

    const rfndType = activatedRouteStub.snapshot.data.refundType;

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['reportingDate'],
        label: `REFUNDS.query.${rfndType}.filters.labels.reportingDate - 29/06/2020 - 02/07/2020`
      }
    ]);
  });

  it('should format when dateOfApplication is defined', () => {
    const filter: RefundFilter = {
      dateOfApplication: [new Date('2020/06/29'), new Date('2020/07/02')]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['dateOfApplication'],
        label: `REFUNDS.query.refund-application.filters.labels.appDate - 29/06/2020 - 02/07/2020`
      }
    ]);
  });

  it('should format when dateOfRejection is defined', () => {
    const filter: RefundFilter = {
      dateOfRejection: [new Date('2020/06/29'), new Date('2020/07/02')]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['dateOfRejection'],
        label: `REFUNDS.query.refund-application.filters.labels.rejAccDate - 29/06/2020 - 02/07/2020`
      }
    ]);
  });

  it('should format when dateOfIssue is defined', () => {
    const filter: RefundFilter = {
      dateOfIssue: [new Date('2020/06/29'), new Date('2020/07/02')]
    };

    activatedRouteStub.snapshot.data.refundType = MasterDataType.RefundNotice;

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['dateOfIssue'],
        label: `REFUNDS.query.refund-notice.filters.labels.issueDate - 29/06/2020 - 02/07/2020`
      }
    ]);
  });

  it('should format when dateOfLatestAction is defined', () => {
    const filter: RefundFilter = {
      dateOfLatestAction: [new Date('2020/06/29'), new Date('2020/07/02')]
    };

    activatedRouteStub.snapshot.data.refundType = MasterDataType.RefundNotice;

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['dateOfLatestAction'],
        label: `REFUNDS.query.refund-notice.filters.labels.latestActDate - 29/06/2020 - 02/07/2020`
      }
    ]);
  });

  it('should format when period is defined', () => {
    const filter: RefundFilter = {
      period: [
        { period: '2020011', dateFrom: '01/01/2020', dateTo: '01/07/2020' },
        { period: '2020014', dateFrom: '01/24/2020', dateTo: '01/31/2020' }
      ]
    };

    activatedRouteStub.snapshot.data.refundType = MasterDataType.RefundApp;

    expect(formatter.format(filter)).toEqual([{ keys: ['period'], label: mockPeriodPipeResult }]);
  });

  it('should format when sentToAirline are defined', () => {
    const filter: RefundFilter = {
      sentToAirline: YesNoShortType.n
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['sentToAirline'],
        label: `REFUNDS.query.refund-application.filters.labels.sentToAirline - REFUNDS.query.commons.yesNoShortValue.N`
      }
    ]);
  });

  it('should format when changedByAgent are defined', () => {
    const filter: RefundFilter = {
      changedByAgent: YesNoShortType.s
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['changedByAgent'],
        label: `REFUNDS.query.refund-application.filters.labels.changedByAgent - REFUNDS.query.commons.yesNoShortValue.S`
      }
    ]);
  });

  it('should format when hasInternalComment are defined', () => {
    const filter: RefundFilter = {
      hasInternalComment: true
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['hasInternalComment'],
        label: `REFUNDS.query.commons.filters.labels.internalComment - REFUNDS.query.commons.yesNoBooleanValue.true`
      }
    ]);
  });

  it('should omit ignored filters', () => {
    const filter: RefundFilter = {
      status: [AdmAcmStatus.pending, AdmAcmStatus.deactivated]
    };

    formatter.ignoredFilters = ['status'];

    expect(formatter.format(filter).length).toEqual(0);
  });

  it('should return empty array when the filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });

  it('should ignore null values', () => {
    const filter: RefundFilter = {
      status: null,
      agentId: null
    };
    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      unknownProp: 'test'
    };
    expect(formatter.format(filter)).toEqual([]);
  });
});
