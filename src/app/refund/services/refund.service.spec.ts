/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { of } from 'rxjs';

import { BasicAgentInfo, BasicAirlineInfo, BspTimeModel } from '../models/refund-be-aux.model';
import { RefundFilter } from '../models/refund-filter.model';
import { CurrencyViewModel } from '../models/refund-view-aux.model';
import { RefundService } from './refund.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { PagedData } from '~app/shared/models/paged-data.model';

const activatedRouteStub = {
  snapshot: {
    data: {
      item: {},
      refundType: MasterDataType.RefundApp
    }
  }
};

describe('RefundService', () => {
  const httpClientSpy = createSpyObject(HttpClient);

  let service: RefundService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        RefundService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        mockProvider(NotificationsHeaderService)
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(RefundService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should call with proper url and return bsp list', fakeAsync(() => {
    let bspList: PagedData<BspDto>;
    const bspListMock: PagedData<BspDto> = {
      records: [{ id: 1, isoCountryCode: 'ES', name: 'Spain' }],
      pageNumber: 0,
      pageSize: 1,
      total: 1,
      totalPages: 1
    };
    httpClientSpy.get.and.returnValue(of(bspListMock));

    service.getBspsList(true).subscribe(results => (bspList = results));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/bsps', { params: { active: true } });
    expect(bspList).toBe(bspListMock);
  }));

  it('should call with proper url and return bsp currencies list', fakeAsync(() => {
    let currencyList: CurrencyViewModel[];
    const currencyListMock: CurrencyViewModel[] = [{ id: 1, code: 'EUR', decimals: 2, isDefault: true }];
    httpClientSpy.get.and.returnValue(of(currencyListMock));

    service.getBspCurrencies(1, true).subscribe(results => (currencyList = results));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/bsps/1/currencies', {
      params: { active: 'true' }
    });
    expect(currencyList).toBe(currencyListMock);
  }));

  it('should call with proper url and return agent info', fakeAsync(() => {
    let agentInfo: BasicAgentInfo;
    const agentInfoMock: BasicAgentInfo = {
      agent: { id: 1, name: 'agent test', bsp: null, effectiveFrom: '2020/06/29', iataCode: '1234567' },
      agentContact: { contactName: 'contact test', email: 'email.agent@test.com', phoneFaxNumber: '1111111' },
      agentAddress: null,
      agentVatNumber: '1234567'
    };
    httpClientSpy.get.and.returnValue(of(agentInfoMock));

    service.getAgentInfo(1).subscribe(results => (agentInfo = results));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/agent-info/1');
    expect(agentInfo).toBe(agentInfoMock);
  }));

  it('should call with proper url and return airline info', fakeAsync(() => {
    let airlineInfo: BasicAirlineInfo;
    const airlineInfoMock: BasicAirlineInfo = {
      airline: {
        id: 1,
        bsp: null,
        iataCode: '123',
        active: true,
        address: null,
        contact: null,
        designator: 'DES',
        globalName: 'Airline test',
        localName: 'Airline test local',
        vatNumber: '1234567'
      },
      airlineAddress: null,
      airlineContact: { contactName: 'Airline Contact', email: 'email.airline@test.com', phoneFaxNumber: '123422321' },
      airlineVatNumber: '1234567'
    };
    httpClientSpy.get.and.returnValue(of(airlineInfoMock));

    service.getAirlineInfo(1).subscribe(results => (airlineInfo = results));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/airline-info/1');
    expect(airlineInfo).toBe(airlineInfoMock);
  }));

  it('should call with proper url and return RA reject reasons', fakeAsync(() => {
    let reasons: DropdownOption[];
    const reasonsMock: DropdownOption[] = [
      { label: 'reason1', value: { title: 'reason1' } },
      { label: 'reason2', value: { title: 'reason2' } }
    ];

    httpClientSpy.get.and.returnValue(of([]));
    service.getRejectRAReasons(1).subscribe(results => (reasons = results));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/refund-management/bsps/1/refund-application-rejection-reason-configuration'
    );
    expect(reasons).toEqual([]);

    httpClientSpy.get.and.returnValue(of([{ title: 'reason1' }, { title: 'reason2' }]));
    service.getRejectRAReasons(1).subscribe(results => (reasons = results));
    tick();
    expect(reasons).toEqual(reasonsMock);
  }));

  it('should call with proper url and return BSP time', fakeAsync(() => {
    let time: BspTimeModel;
    const bspTimeMock: BspTimeModel = {
      id: 1,
      localTime: new Date('2020/06/20')
    };
    httpClientSpy.get.and.returnValue(of(bspTimeMock));
    service.getBspTime(1).subscribe(results => (time = results));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/bsps/1/time');
    expect(time).toEqual(bspTimeMock);
  }));

  it('should call with proper url and return Refund action list', fakeAsync(() => {
    let actionList: { action: GridTableActionType; disabled?: boolean }[];
    const actionsBEMock: { id: GridTableActionType; enabled: boolean }[] = [
      { id: GridTableActionType.Edit, enabled: true }
    ];

    const expectedResult: { action: GridTableActionType; disabled?: boolean }[] = [
      { action: GridTableActionType.Edit, disabled: false }
    ];
    httpClientSpy.get.and.returnValue(of(actionsBEMock));
    service.getRefundActionList(1).subscribe(results => (actionList = results));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/refund-applications/1/actions');
    expect(actionList).toEqual(expectedResult);
  }));

  it('should call with proper url and post Refund comment', fakeAsync(() => {
    httpClientSpy.post.and.returnValue(of());
    service.addComment({ reason: ['test'], attachmentIds: ['1'] }, 1).subscribe();
    tick();

    expect(httpClientSpy.post).toHaveBeenCalledWith('/refund-management/refund-applications/1/comments', {
      reason: ['test'],
      attachmentIds: ['1']
    });
  }));

  it('should adapt number data', () => {
    const data = {
      totalAmount: '12',
      currency: {
        decimals: 2
      }
    };

    service['dataAdapter'](data);

    expect(data.totalAmount).toEqual('12.00');
  });

  it('should call with proper url with detailed download', waitForAsync(() => {
    const fileName = 'filename=list-to-Download.txt';
    let refundQueryDownload: { blob: Blob; fileName: string };

    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({
        'Some-Header': 'some header',
        'Content-Disposition': `filename="${fileName}"`
      })
    });

    const query: DataQuery<RefundFilter> = {
      filterBy: {
        ticketDocumentNumber: '1234'
      },
      sortBy: [{ attribute: 'dateOfApplication', sortType: SortOrder.Desc }],
      paginateBy: { page: 0, size: 20 }
    };

    const refundQueryDownloadMock: { blob: Blob; fileName: string } = {
      blob: new Blob([null], { type: 'application/zip' }),
      fileName
    };

    httpClientSpy.get.and.returnValue(of(httpResponse));

    service.download(query, DownloadFormat.TXT, {}, true).subscribe(result => (refundQueryDownload = result));

    expect(httpClientSpy.get).toHaveBeenCalledWith(
      '/refund-management/refund-applications?exportAs=TXT&exportDetail=true&?page=0&size=20&sort=dateOfApplication,DESC&ticketDocumentNumber=1234',
      jasmine.anything()
    );
    expect(refundQueryDownload).toEqual(refundQueryDownloadMock);
  }));
});
