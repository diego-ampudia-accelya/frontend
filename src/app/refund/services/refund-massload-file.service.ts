import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { RefundUploadMassloadDialogPayload } from '../models/refund-massload-attachment.model';
import { RefundMassloadFileFilter } from '../models/refund-massload-file-filter.model';
import { RefundMassloadFileModel, RefundMassloadFileTotals } from '../models/refund-massload-file.model';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class RefundMassloadFileService implements Queryable<RefundMassloadFileModel> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/refund-management/massload-files`;

  private bspId: number;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {
    this.store
      .pipe(select(fromAuth.getUserBsps), first())
      .subscribe(bsps => (this.bspId = bsps && bsps[0] ? bsps[0].id : null));
  }

  public find(query: DataQuery<RefundMassloadFileFilter>): Observable<PagedData<RefundMassloadFileModel>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<RefundMassloadFileModel>>(this.baseUrl + requestQuery.getQueryString());
  }

  public findTotals(query: DataQuery<RefundMassloadFileFilter>): Observable<RefundMassloadFileTotals> {
    const url = `${this.baseUrl}/totals`;
    const requestQuery = this.formatQuery(query);

    return this.http.get<RefundMassloadFileTotals>(url + requestQuery.getQueryString());
  }

  public downloadEvaluationFile(id: string): Observable<{ blob: Blob; fileName: string }> {
    const downloadUrl = `${this.appConfiguration.baseApiPath}/refund-management/massload-evaluation-files/${id}`;

    return this.http
      .get<HttpResponse<ArrayBuffer>>(downloadUrl, downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  public createMassUploadFile(requestBody: RefundUploadMassloadDialogPayload): Observable<any> {
    const baseUrl = `${this.appConfiguration.baseApiPath}/refund-management/massupload`;

    return this.http.post<any>(baseUrl, requestBody);
  }

  private formatQuery(query: DataQuery<RefundMassloadFileFilter>): RequestQuery {
    const { uploadDate, ...filterBy } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        fromUploadDate: uploadDate && toShortIsoDate(uploadDate[0]),
        toUploadDate: uploadDate && toShortIsoDate(uploadDate[1] ? uploadDate[1] : uploadDate[0]),
        bspId: this.bspId
      }
    });
  }
}
