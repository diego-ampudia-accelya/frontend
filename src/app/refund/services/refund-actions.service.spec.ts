import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';

import { RefundActionType, RefundBE } from '../models/refund.model';
import { RefundActionsService } from './refund-actions.service';
import { RefundDialogService } from './refund-dialog.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundRejectDialogComponent } from '~app/refund/components/dialogs/refund-reject-dialog/refund-reject-dialog.component';
import { DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { TabService } from '~app/shared/services';

describe('RefundActionsService', () => {
  let service: RefundActionsService;

  const tabServiceSpy = createSpyObject(TabService);
  const refundDialogServiceSpy = createSpyObject(RefundDialogService);
  const dialogServiceSpy = createSpyObject(DialogService);

  const partialRfnd: Partial<RefundBE> = { id: 12345 };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: RefundDialogService, useValue: refundDialogServiceSpy },
        { provide: TabService, useValue: tabServiceSpy }
      ]
    });

    service = TestBed.inject(RefundActionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('EDIT action', () => {
    it('should close and navigate with the rigth url on RA edit', () => {
      service.getActionCommand(GridTableActionType.Edit, partialRfnd as RefundBE, MasterDataType.RefundApp)();
      expect(tabServiceSpy.closeCurrentTabAndNavigate).toHaveBeenCalledWith(
        `${ROUTES.REFUNDS_APP_EDIT.url}/${partialRfnd.id}`
      );
    });

    it('should close and navigate with the rigth url on RN edit', () => {
      service.getActionCommand(GridTableActionType.Edit, partialRfnd as RefundBE, MasterDataType.RefundNotice)();
      expect(tabServiceSpy.closeCurrentTabAndNavigate).toHaveBeenCalledWith(
        `${ROUTES.REFUNDS_NOTICE_EDIT.url}/${partialRfnd.id}`
      );
    });
  });

  describe('DELETE action', () => {
    it('should open dialog', () => {
      service.getActionCommand(GridTableActionType.Delete, partialRfnd as RefundBE, MasterDataType.RefundApp)();

      expect(refundDialogServiceSpy.openDialog).toHaveBeenCalledWith(
        RefundActionType.delete,
        partialRfnd,
        MasterDataType.RefundApp,
        jasmine.anything()
      );
    });
  });

  describe('INVESTIGATE action', () => {
    it('should close and navigate with the rigth url on RA investigate', () => {
      service.getActionCommand(GridTableActionType.Investigate, partialRfnd as RefundBE, MasterDataType.RefundApp)();
      expect(tabServiceSpy.closeCurrentTabAndNavigate).toHaveBeenCalledWith(
        `${ROUTES.REFUNDS_APP_INVESTIGATE.url}/${partialRfnd.id}`
      );
    });
  });

  describe('REJECT action', () => {
    it('should open dialog', () => {
      service.getActionCommand(GridTableActionType.Reject, partialRfnd as RefundBE, MasterDataType.RefundApp)();

      expect(dialogServiceSpy.open).toHaveBeenCalledWith(RefundRejectDialogComponent, {
        data: {
          activatedRoute: {
            data: { refundType: MasterDataType.RefundApp }
          },
          footerButtonsType: FooterButton.Reject,
          rowTableContent: partialRfnd
        }
      });
    });
  });

  describe('AUTHORIZE action', () => {
    it('should close and navigate with the rigth url on RA authorize', () => {
      service.getActionCommand(GridTableActionType.Authorize, partialRfnd as RefundBE, MasterDataType.RefundApp)();
      expect(tabServiceSpy.closeCurrentTabAndNavigate).toHaveBeenCalledWith(
        `${ROUTES.REFUNDS_APP_AUTHORIZE.url}/${partialRfnd.id}`
      );
    });
  });

  describe('SUPERVISE action', () => {
    it('should close and navigate with the rigth url on RA supervise', () => {
      service.getActionCommand(GridTableActionType.Supervise, partialRfnd as RefundBE, MasterDataType.RefundApp)();
      expect(tabServiceSpy.closeCurrentTabAndNavigate).toHaveBeenCalledWith(
        `${ROUTES.REFUNDS_APP_SUPERVISE.url}/${partialRfnd.id}`
      );
    });

    it('should close and navigate with the rigth url on RN supervise', () => {
      service.getActionCommand(GridTableActionType.Supervise, partialRfnd as RefundBE, MasterDataType.RefundNotice)();
      expect(tabServiceSpy.closeCurrentTabAndNavigate).toHaveBeenCalledWith(
        `${ROUTES.REFUNDS_NOTICE_SUPERVISE.url}/${partialRfnd.id}`
      );
    });
  });

  describe('RESUBMIT action', () => {
    it('should open dialog', () => {
      service.getActionCommand(GridTableActionType.Resubmit, partialRfnd as RefundBE, MasterDataType.RefundApp)();

      expect(refundDialogServiceSpy.openDialog).toHaveBeenCalledWith(
        RefundActionType.startResubmission,
        partialRfnd,
        MasterDataType.RefundApp,
        jasmine.anything()
      );
    });
  });
});
