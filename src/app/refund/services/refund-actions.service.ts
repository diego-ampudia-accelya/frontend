import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { RefundActionType, RefundBE } from '../models/refund.model';
import { RefundDialogService } from './refund-dialog.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundRejectDialogComponent } from '~app/refund/components/dialogs/refund-reject-dialog/refund-reject-dialog.component';
import { DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { TabService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class RefundActionsService {
  constructor(
    private dialog: DialogService,
    private tabService: TabService,
    private refundDialogService: RefundDialogService
  ) {}

  public getActionCommand(action: GridTableActionType, rfnd: RefundBE, refundType: MasterDataType) {
    let actionCommand = () => {
      //This is intentional
    };
    switch (action) {
      case GridTableActionType.Edit:
        actionCommand = () => {
          const queryUrl = this.isRa(refundType) ? ROUTES.REFUNDS_APP_EDIT.url : ROUTES.REFUNDS_NOTICE_EDIT.url;

          const url = `${queryUrl}/${rfnd.id}`;
          this.tabService.closeCurrentTabAndNavigate(url);
        };
        break;
      case GridTableActionType.Delete:
        actionCommand = () => {
          this.refundDialogService.openDialog(RefundActionType.delete, rfnd, refundType, new FormGroup({}));
        };
        break;
      case GridTableActionType.Investigate:
      case GridTableActionType.Exception:
        actionCommand = () => {
          const queryUrl = ROUTES.REFUNDS_APP_INVESTIGATE.url;

          const url = `${queryUrl}/${rfnd.id}`;
          this.tabService.closeCurrentTabAndNavigate(url);
        };
        break;
      case GridTableActionType.Reject:
        // TODO Use confirmation dialog
        actionCommand = () => {
          this.openDialog(RefundRejectDialogComponent, FooterButton.Reject, rfnd, refundType);
        };
        break;
      case GridTableActionType.Authorize:
        actionCommand = () => {
          const queryUrl = ROUTES.REFUNDS_APP_AUTHORIZE.url;

          const url = `${queryUrl}/${rfnd.id}`;
          this.tabService.closeCurrentTabAndNavigate(url);
        };
        break;
      case GridTableActionType.Supervise:
        actionCommand = () => {
          const queryUrl = this.isRa(refundType)
            ? ROUTES.REFUNDS_APP_SUPERVISE.url
            : ROUTES.REFUNDS_NOTICE_SUPERVISE.url;
          const url = `${queryUrl}/${rfnd.id}`;

          this.tabService.closeCurrentTabAndNavigate(url);
        };
        break;
      case GridTableActionType.Resubmit:
        actionCommand = () => {
          this.refundDialogService.openDialog(RefundActionType.startResubmission, rfnd, refundType, new FormGroup({}));
        };
        break;
    }

    return actionCommand;
  }

  private openDialog(dialogComponent, footerBtnDialog: FooterButton, rfnd: RefundBE, refundType: MasterDataType) {
    this.dialog.open(dialogComponent, {
      data: {
        activatedRoute: {
          data: { refundType }
        },
        footerButtonsType: footerBtnDialog,
        rowTableContent: rfnd
      }
    });
  }

  private isRa(refundType: MasterDataType): boolean {
    return refundType === MasterDataType.RefundApp;
  }
}
