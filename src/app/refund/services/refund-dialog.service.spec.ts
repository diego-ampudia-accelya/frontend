import { TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule } from 'angular-l10n';

import { RefundActionType, RefundBE } from '../models/refund.model';
import { RefundDialogService } from './refund-dialog.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';

describe('RefundDialogService', () => {
  let service: RefundDialogService;
  let dialogServiceStub: SpyObject<DialogService>;

  beforeEach(waitForAsync(() => {
    dialogServiceStub = createSpyObject(DialogService);
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [{ provide: DialogService, useValue: dialogServiceStub }]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(RefundDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open dialog', () => {
    service.openDialog(RefundActionType.issue, null, MasterDataType.RefundApp, null);

    expect(dialogServiceStub.open).toHaveBeenCalled();
  });

  it('should open multipleDialog', () => {
    service.openMultipleDialog(RefundActionType.issue, MasterDataType.RefundApp, null);

    expect(dialogServiceStub.open).toHaveBeenCalled();
  });

  it('should open ActivityHistoryDialog', () => {
    const refundItem: RefundBE = {
      agent: null,
      agentAddress: null,
      agentContact: null,
      airline: null,
      airlineAddress: null,
      airlineCodeRelatedDocument: '',
      airlineContact: null,
      cardPayments: [],
      currency: null,
      dateOfIssueRelatedDocument: '',
      netReporting: false,
      originalIssue: null,
      partialRefund: false,
      passenger: '',
      reasonForRefund: '',
      relatedTicketConjunctions: [],
      relatedTicketDocument: null,
      relatedTicketExchange: false,
      resubmissionRemark: '',
      sentToDpc: null,
      status: null,
      taxMiscellaneousFees: [],
      ticketDocumentNumber: '1234'
    };
    service.openActivityHistoryDialog(refundItem, MasterDataType.RefundApp);

    expect(dialogServiceStub.open).toHaveBeenCalled();
  });
});
