import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { RefundHistoryService } from './refund-history.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';

describe('RefundHistoryService', () => {
  let service: RefundHistoryService;
  const httpClientSpy = createSpyObject(HttpClient);
  const activitiesMock: Activity[] = [
    {
      author: {
        contact: { name: 'BU name surname', telephone: '948223456', email: 'tomina.petrova@accelya.com' },
        type: 'Agent',
        code: '5555555'
      },
      creationDate: '2020-11-18T12:16:11' as any,
      status: 'MODIFIED',
      type: '',
      attachments: [],
      params: []
    },
    {
      author: {
        contact: { name: 'BU name surname', telephone: '948223456', email: 'tomina.petrova@accelya.com' },
        type: 'Agent',
        code: '5555555'
      },
      creationDate: '2020-11-03T12:00:42' as any,
      status: 'RESUBMITTED',
      type: ' Original rejected refund:',
      attachments: [],
      params: [
        { name: 'rejectedRefundNumber', value: '51007527' },
        { name: 'rejectedRefundId', value: '698300000030458' }
      ]
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpClientSpy }, RefundHistoryService]
    });
    service = TestBed.inject(RefundHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call with proper url', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(of(activitiesMock));

    service.getActivities(1, MasterDataType.RefundApp).subscribe();
    tick();
    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/refund-applications/1/history');

    service.getActivities(1, MasterDataType.RefundNotice).subscribe();
    tick();
    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/refund-notices/1/history');
  }));

  it('should process type attribute when activity refers to rejected document', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(of(activitiesMock));
    let activityToTest: Activity;
    service
      .getActivities(1234, MasterDataType.RefundApp)
      .subscribe(
        activities =>
          (activityToTest = activities.find(item => item.params.find(param => param.name === 'rejectedRefundId')))
      );

    tick();

    expect(activityToTest.type.includes('href')).toBe(true);
  }));
});
