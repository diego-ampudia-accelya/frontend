import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { MenuItem } from 'primeng/api';
import { of, Subscription } from 'rxjs';
import { skip } from 'rxjs/operators';

import * as fromAuth from '../../auth/selectors/auth.selectors';
import { RefundStatus, SendToDpcType } from '../models/refund-be-aux.model';
import { AmountTaxViewModel, StatusHeaderViewModel } from '../models/refund-view-aux.model';
import {
  AirlineConfigParamName,
  PaymentMethodsType,
  RefundActionEmitterType,
  RefundActionType,
  RefundAirlineConfiguration,
  RefundBE,
  RefundConfiguration,
  RefundNoticeConfiguration
} from '../models/refund.model';
import { createRefundPermission, modifyRefundPermission } from '../shared/helpers/refund-permissions.config';
import { getRefundForm } from '../store/reducers';
import { RefundFormState } from '../store/reducers/refund-form.reducer';

import { RefundConfigService } from './refund-config.service';
import { RefundDataService } from './refund-data.service';
import { RefundDialogService } from './refund-dialog.service';
import { TabService } from '~app/shared/services';
import { AgentUser, AirlineUser, UserType } from '~app/shared/models/user.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { DropdownOption } from '~app/shared/models';
import { AccessType } from '~app/shared/enums';
import { GLOBALS } from '~app/shared/constants/globals';
import { DialogService } from '~app/shared/components';
import { MasterDataType } from '~app/master-shared/models/master.model';

const airlineMock: AirlineUser = {
  userType: UserType.AIRLINE,
  globalAirline: {
    globalName: 'global',
    iataCode: '000',
    id: 1
  },
  email: 'email@airline.com',
  firstName: 'airline',
  lastName: 'airline'
};

const mockAgent: AgentUser = {
  userType: UserType.AGENT,
  agent: {
    id: 1,
    bsp: {
      id: 1,
      effectiveFrom: '2020/06/29',
      isoCountryCode: 'ES',
      name: 'Spain'
    },
    effectiveFrom: '2020/06/29',
    iataCode: '1234567',
    name: 'Agent'
  },
  email: 'email@agent.com',
  firstName: 'agent',
  lastName: 'agent',
  permissions: []
};

const initialState = {
  auth: {
    user: mockAgent
  },
  core: {
    menu: {
      tabs: {}
    }
  },
  refund: {}
};

const initialFormState: RefundFormState = {
  originalValue: null,
  value: null,
  hasChanges: false
};

const rfndMock: RefundBE = {
  id: 12345,
  agent: { id: 1 },
  agentAddress: null,
  agentContact: null,
  airline: { id: 1 },
  airlineAddress: null,
  airlineCodeRelatedDocument: '001',
  airlineContact: null,
  cardPayments: [],
  currency: { id: 1 },
  dateOfIssueRelatedDocument: '2020/06/29',
  finalType: null,
  finalDaysLeft: null,
  finalProcessStopped: null,
  netReporting: false,
  originalIssue: { airlineCode: null, agentCode: null, documentNumber: null, issueDate: null, locationCode: null },
  partialRefund: false,
  passenger: 'test',
  reasonForRefund: 'test reason',
  relatedTicketConjunctions: [],
  relatedTicketDocument: {
    documentNumber: '0011231231231',
    coupon1: false,
    coupon2: false,
    coupon3: false,
    coupon4: false
  },
  relatedTicketExchange: false,
  resubmissionRemark: '',
  sentToDpc: SendToDpcType.pending,
  status: RefundStatus.pending,
  taxMiscellaneousFees: []
};

describe('RefundConfigService', () => {
  let service: RefundConfigService;
  let mockStore: MockStore;
  const translationSpy = createSpyObject(L10nTranslationService);
  const httpClientSpy = createSpyObject(HttpClient);
  const dialogServiceStub = createSpyObject(DialogService);

  beforeEach(waitForAsync(() => {
    translationSpy.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        RefundConfigService,
        RefundDataService,
        { provide: L10nTranslationService, useValue: translationSpy },
        { provide: HttpClient, useValue: httpClientSpy },
        provideMockStore({ selectors: [{ selector: fromAuth.getUser, value: initialState.auth }] }),
        provideMockStore({ selectors: [{ selector: getRefundForm, value: initialFormState }] }),
        TabService,
        { provide: DialogService, useValue: dialogServiceStub },
        RefundDialogService,
        FormBuilder
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(RefundConfigService);
    mockStore = TestBed.inject<any>(Store);
    service['formData'].getSubject(RefundActionEmitterType.bsp).next({ id: 1234, name: 'Spain', isoCountryCode: 'ES' });
    service['formData'].getSubject(RefundActionEmitterType.airline).next({
      id: 111,
      designator: 'DES',
      iataCode: '111',
      localName: 'Test Airline',
      globalName: 'Global Test Airline',
      bsp: { id: 1234, isoCountryCode: 'ES', name: 'Spain' },
      vatNumber: '01234',
      active: true,
      address: null,
      contact: null
    });
    service.refundType = MasterDataType.RefundApp;

    httpClientSpy.get.and.returnValue(
      of([
        {
          freeStat: false,
          defaultStat: 'D',
          airlineVatnumber: true,
          agentVatNumber: true,
          fareAdjustmentAmount: true,
          nrRefundsPermitted: true,
          withoutCouponsCapture: true,
          raFinal: true,
          mfFeeAmount: 1
        }
      ])
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return access mode', () => {
    service.accessType = AccessType.create;
    expect(service.isReadonlyMode).toBeFalsy();
    expect(service.isEditMode).toBeFalsy();
    expect(service.isEditTicketMode).toBeFalsy();

    service.accessType = AccessType.read;
    expect(service.isReadonlyMode).toBeTruthy();
    expect(service.isEditMode).toBeFalsy();
    expect(service.isEditTicketMode).toBeFalsy();

    service.accessType = AccessType.edit;
    expect(service.isReadonlyMode).toBeFalsy();
    expect(service.isEditMode).toBeTruthy();
    expect(service.isEditTicketMode).toBeFalsy();

    service.accessType = AccessType.editTicket;
    expect(service.isReadonlyMode).toBeFalsy();
    expect(service.isEditMode).toBeFalsy();
    expect(service.isEditTicketMode).toBeTruthy();
  });

  it('should return bspRefundConfiguration and call http.get with the proper url', fakeAsync(() => {
    let rfndConf: RefundConfiguration;

    service.bspRefundConfiguration.subscribe(res => (rfndConf = res));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/bsps/1234/refund-configuration');
    expect(rfndConf).toEqual({
      freeStat: false,
      defaultStat: 'D',
      airlineVatnumber: true,
      agentVatNumber: true,
      fareAdjustmentAmount: true,
      nrRefundsPermitted: true,
      withoutCouponsCapture: true,
      raFinal: true,
      mfFeeAmount: 1
    });
  }));

  it('should return bspRefundNoticeConfiguration and call http.get with the proper url', fakeAsync(() => {
    let rfndNoticeConf: RefundNoticeConfiguration;
    httpClientSpy.get.and.returnValue(
      of({
        originalIssueDetailsEnabled: false,
        partialRefundEnabled: false,
        discountAmountEnabled: false,
        secondPaymentCardEnabled: false,
        vatnumberEnabled: false,
        contactEnabled: false,
        settlementAuthorisationCodeEnabled: false,
        customerFileReferenceEnabled: false,
        taxOnMfCpEnabled: false
      })
    );
    service.refundType = MasterDataType.RefundNotice;

    service.bspRefundNoticeConfiguration.subscribe(res => (rfndNoticeConf = res));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/bsps/1234/refund-notice-configuration');
    expect(rfndNoticeConf).toEqual({
      originalIssueDetailsEnabled: false,
      partialRefundEnabled: false,
      discountAmountEnabled: false,
      secondPaymentCardEnabled: false,
      vatnumberEnabled: false,
      contactEnabled: false,
      settlementAuthorisationCodeEnabled: false,
      customerFileReferenceEnabled: false,
      taxOnMfCpEnabled: false
    });
  }));

  it('should return airlineRefundConfiguration and call http.get with the proper url', fakeAsync(() => {
    let rfndConf: RefundAirlineConfiguration;
    service.refundType = MasterDataType.RefundNotice;

    httpClientSpy.get.and.returnValue(
      of({
        id: 111,
        descriptorId: 77844852551100,
        scopeId: '7784485255',
        scopeType: 'airlines',
        service: 'refund-management',
        parameters: [
          {
            descriptorId: 77844852551106,
            id: 77844852551106,
            name: 'HANDLING_FEE',
            readonly: false,
            type: 'BOOLEAN',
            value: 'true'
          },
          {
            descriptorId: 77844852551107,
            id: 77844852551107,
            name: 'PENALTY_CHARGE',
            readonly: false,
            type: 'BOOLEAN',
            value: 'true'
          },
          {
            descriptorId: 77844852551108,
            id: 77844852551108,
            name: 'COMMISSION_ON_CP',
            readonly: false,
            type: 'BOOLEAN',
            value: 'false'
          }
        ]
      })
    );

    service.airlineRefundConfiguration.subscribe(res => (rfndConf = res));
    tick();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/refund-management/airlines/111/configuration');
    expect(rfndConf).toEqual({
      id: 111,
      descriptorId: 77844852551100,
      scopeId: '7784485255',
      scopeType: 'airlines',
      service: 'refund-management',
      parameters: [
        {
          descriptorId: 77844852551106,
          id: 77844852551106,
          name: AirlineConfigParamName.handlingFee,
          readonly: false,
          type: 'BOOLEAN',
          value: 'true'
        },
        {
          descriptorId: 77844852551107,
          id: 77844852551107,
          name: AirlineConfigParamName.penaltyCharge,
          readonly: false,
          type: 'BOOLEAN',
          value: 'true'
        },
        {
          descriptorId: 77844852551108,
          id: 77844852551108,
          name: AirlineConfigParamName.comissionOnCp,
          readonly: false,
          type: 'BOOLEAN',
          value: 'false'
        }
      ]
    });
  }));

  it('should return default stats', fakeAsync(() => {
    let stats: Array<DropdownOption & { selected: boolean }> | string;
    httpClientSpy.get.and.returnValue(
      of([
        {
          freeStat: false,
          defaultStat: 'D',
          airlineVatnumber: true,
          agentVatNumber: true
        }
      ])
    );

    service.getStat().subscribe(res => (stats = res));
    tick();
    expect(stats).toEqual([
      { value: 'I', label: 'REFUNDS.basicInfo.statValues.international', selected: false },
      { value: 'D', label: 'REFUNDS.basicInfo.statValues.domestic', selected: true }
    ]);
  }));

  it('should return free stat', fakeAsync(() => {
    let stats: Array<DropdownOption & { selected: boolean }> | string;

    httpClientSpy.get.and.returnValue(
      of([
        {
          freeStat: true,
          defaultStat: 'I',
          airlineVatnumber: true,
          agentVatNumber: true
        }
      ])
    );

    service.getStat().subscribe(res => (stats = res));
    tick();
    expect(stats).toEqual('I');
  }));

  it('should return header status fields', () => {
    const expectedResult: Array<keyof StatusHeaderViewModel> = [
      'docNumber',
      'status',
      'bspName',
      'airlCode',
      'agentCode',
      'appDate',
      'amount'
    ];

    expect(service.getHeaderStatusFields()).toEqual(expectedResult);
  });

  it('should return decimals pattern', () => {
    expect(service.getDecimalsPattern()).toEqual(GLOBALS.PATTERNS.DECIMALS_PATTERN);
  });

  it('should return rates pattern', () => {
    expect(service.getRatesPattern()).toEqual(GLOBALS.PATTERNS.RATE);
  });

  it('should return Payment Main Button Action', () => {
    expect(service.getPaymentMainBtnAction()).toEqual({ label: 'REFUNDS.formOfPayment.actionsBtn.main' });
  });

  it('should get Payment Method Options', fakeAsync(() => {
    let paymentOptions: ButtonMenuOption[];

    service['_bspRefundNoticeConfiguration'].next({
      originalIssueDetailsEnabled: false,
      partialRefundEnabled: false,
      discountAmountEnabled: false,
      secondPaymentCardEnabled: false,
      vatnumberEnabled: false,
      contactEnabled: false,
      settlementAuthorisationCodeEnabled: false,
      customerFileReferenceEnabled: false,
      taxOnMfCpEnabled: false
    });
    service.refundType = MasterDataType.RefundNotice;

    service.getPaymentMethodOptions().subscribe(res => (paymentOptions = res));
    tick();
    expect(paymentOptions).toEqual([
      {
        id: PaymentMethodsType.card1,
        label: 'REFUNDS.formOfPayment.actionsBtn.CARD1',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.easypay,
        label: 'REFUNDS.formOfPayment.actionsBtn.EASYPAY',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.miscellaneous,
        label: 'REFUNDS.formOfPayment.actionsBtn.MISCELLANEOUS',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.cash,
        label: 'REFUNDS.formOfPayment.actionsBtn.cash',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'cash'
      },
      {
        id: PaymentMethodsType.miscCash,
        label: 'REFUNDS.formOfPayment.actionsBtn.miscCash',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'cash'
      }
    ]);

    service['_bspRefundNoticeConfiguration'].next({
      originalIssueDetailsEnabled: false,
      partialRefundEnabled: false,
      discountAmountEnabled: false,
      secondPaymentCardEnabled: true,
      vatnumberEnabled: false,
      contactEnabled: false,
      settlementAuthorisationCodeEnabled: false,
      customerFileReferenceEnabled: false,
      taxOnMfCpEnabled: false
    });

    service.getPaymentMethodOptions().subscribe(res => (paymentOptions = res));
    tick();
    expect(paymentOptions).toEqual([
      {
        id: PaymentMethodsType.card1,
        label: 'REFUNDS.formOfPayment.actionsBtn.CARD1',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.card2,
        label: 'REFUNDS.formOfPayment.actionsBtn.CARD2',
        command: jasmine.any(Function),
        hidden: true,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.easypay,
        label: 'REFUNDS.formOfPayment.actionsBtn.EASYPAY',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.miscellaneous,
        label: 'REFUNDS.formOfPayment.actionsBtn.MISCELLANEOUS',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.cash,
        label: 'REFUNDS.formOfPayment.actionsBtn.cash',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'cash'
      },
      {
        id: PaymentMethodsType.miscCash,
        label: 'REFUNDS.formOfPayment.actionsBtn.miscCash',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'cash'
      }
    ]);

    service.refundType = MasterDataType.RefundApp;
    service.getPaymentMethodOptions().subscribe(res => (paymentOptions = res));
    tick();
    expect(paymentOptions).toEqual([
      {
        id: PaymentMethodsType.card1,
        label: 'REFUNDS.formOfPayment.actionsBtn.CARD1',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.card2,
        label: 'REFUNDS.formOfPayment.actionsBtn.CARD2',
        command: jasmine.any(Function),
        hidden: true,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.easypay,
        label: 'REFUNDS.formOfPayment.actionsBtn.EASYPAY',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.miscellaneous,
        label: 'REFUNDS.formOfPayment.actionsBtn.MISCELLANEOUS',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'card'
      },
      {
        id: PaymentMethodsType.cash,
        label: 'REFUNDS.formOfPayment.actionsBtn.cash',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'cash'
      },
      {
        id: PaymentMethodsType.miscCash,
        label: 'REFUNDS.formOfPayment.actionsBtn.miscCash',
        command: jasmine.any(Function),
        hidden: false,
        isDisabled: false,
        group: 'cash'
      }
    ]);
  }));

  it('should return amount fields', () => {
    const expectedResult: Array<keyof AmountTaxViewModel> = [
      'grossFare',
      'lessGrossFareUsed',
      'totalGrossFare',
      'supplementaryCommission',
      'cancellationPenalty',
      'taxOnCancellationPenalty',
      'miscellaneousFee',
      'taxOnMiscellaneousFee',
      'totalAmount'
    ];

    expect(service.getAmountFieldKeys()).toEqual(expectedResult);
  });

  it('should return header discard button', () => {
    let headerDiscardButton;

    service.isAuthorize = false;
    service.isInvestigate = false;
    headerDiscardButton = service.getHeaderDiscardButton();
    expect(headerDiscardButton.label).toEqual('BUTTON.DEFAULT.DISCARD');
    expect(headerDiscardButton.actionType).toEqual(RefundActionType.applyChanges);

    service.isAuthorize = false;
    service.isInvestigate = true;
    headerDiscardButton = service.getHeaderDiscardButton();
    expect(headerDiscardButton.label).toEqual('BUTTON.DEFAULT.DISCARD');
    expect(headerDiscardButton.actionType).toEqual(RefundActionType.investigate);

    service.isAuthorize = true;
    service.isInvestigate = false;
    headerDiscardButton = service.getHeaderDiscardButton();
    expect(headerDiscardButton.label).toEqual('BUTTON.DEFAULT.DISCARD');
    expect(headerDiscardButton.actionType).toEqual(RefundActionType.authorize);
  });

  it('should return formatted header options', () => {
    service.refundType = MasterDataType.RefundApp;
    const mockActions: { action: GridTableActionType; disabled?: boolean }[] = [
      { action: GridTableActionType.Details, disabled: false },
      { action: GridTableActionType.Edit, disabled: false },
      { action: GridTableActionType.Investigate, disabled: true }
    ];
    const expectedResult: (ButtonMenuOption & MenuItem)[] = [
      {
        label: 'edit',
        isDisabled: false,
        disabled: false,
        command: jasmine.any(Function)
      },
      {
        label: 'investigateEdit',
        isDisabled: true,
        disabled: true,
        command: jasmine.any(Function)
      }
    ];

    expect(service.formatHeaderBtnOptions(mockActions, rfndMock)).toEqual(expectedResult);
  });

  describe('isAgentVatFieldVisible', () => {
    beforeEach(() => {
      mockStore.overrideSelector(fromAuth.getUser, mockAgent);
      (service as any).bspConfig$ = new Subscription();
    });

    describe('RA (agent vatNumber = true)', () => {
      beforeEach(() => {
        //* BSP Refund Configuration for RA (agent vatNumber = true)
        service['_bspRefundConfiguration'].next({
          freeStat: false,
          defaultStat: 'D',
          airlineVatnumber: true,
          agentVatNumber: true,
          fareAdjustmentAmount: true,
          nrRefundsPermitted: true,
          withoutCouponsCapture: true,
          raFinal: true,
          mfFeeAmount: 1
        });

        //* Differents access types RA
        service.refundType = MasterDataType.RefundApp;
      });

      it('should return true when isEditMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.edit;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isEditMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.edit;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isCreateMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.create;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isCreateMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.create;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isEditTicketMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.editTicket;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isEditTicketMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.editTicket;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isReadonlyMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isReadonlyMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));
    });

    describe('RA (agent vatNumber = false)', () => {
      beforeEach(() => {
        //* BSP Refund Configuration for RA (agent vatNumber = false)
        service['_bspRefundConfiguration'].next({
          freeStat: false,
          defaultStat: 'D',
          airlineVatnumber: true,
          agentVatNumber: false,
          fareAdjustmentAmount: true,
          nrRefundsPermitted: true,
          withoutCouponsCapture: true,
          raFinal: true,
          mfFeeAmount: 1
        });

        //* Differents access types RA
        service.refundType = MasterDataType.RefundApp;
      });

      it('should return true when isEditMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.edit;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return false when isEditMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.edit;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return false when isCreateMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.create;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return false when isCreateMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.create;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return false when isEditTicketMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.editTicket;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return false when isEditTicketMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.editTicket;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return true when isReadonlyMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return false when isReadonlyMode is true and agentVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));
    });

    describe('RN', () => {
      beforeEach(() => {
        (service as any).bspRnConfig$ = new Subscription();
        //* Differents access types RN
        service.refundType = MasterDataType.RefundNotice;
      });

      describe('(agent vatNumber = false, agent notice vatNumber = false)', () => {
        beforeEach(() => {
          //* BSP Refund Configuration (agent vatNumber = false)
          service['_bspRefundConfiguration'].next({
            freeStat: false,
            defaultStat: 'D',
            airlineVatnumber: false,
            agentVatNumber: false,
            fareAdjustmentAmount: true,
            nrRefundsPermitted: true,
            withoutCouponsCapture: true,
            raFinal: true,
            mfFeeAmount: 1
          });

          //* BSP Refund Notice Configuration (vatNumberEnabled = false)
          service['_bspRefundNoticeConfiguration'].next({
            originalIssueDetailsEnabled: false,
            partialRefundEnabled: false,
            discountAmountEnabled: false,
            secondPaymentCardEnabled: false,
            vatnumberEnabled: false,
            contactEnabled: false,
            settlementAuthorisationCodeEnabled: false,
            customerFileReferenceEnabled: false,
            taxOnMfCpEnabled: false
          });
        });

        it('should return false when isEditMode and isSupervise are true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isEditMode and isSupervise are true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return true when isReadonlyMode is true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return false when isReadonlyMode is true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return true when isEditMode is true,isSupervise is false and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return false when isEditMode is true,isSupervise is false and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isCreateMode is true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isCreateMode is true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isEditTicketMode is true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isEditTicketMode is true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));
      });

      describe('(agent vatNumber = true, agent notice vatNumber = true)', () => {
        beforeEach(() => {
          //* BSP Refund Configuration (agent vatNumber = false)
          service['_bspRefundConfiguration'].next({
            freeStat: false,
            defaultStat: 'D',
            airlineVatnumber: true,
            agentVatNumber: true,
            fareAdjustmentAmount: true,
            nrRefundsPermitted: true,
            withoutCouponsCapture: true,
            raFinal: true,
            mfFeeAmount: 1
          });

          //* BSP Refund Notice Configuration (vatNumberEnabled = false)
          service['_bspRefundNoticeConfiguration'].next({
            originalIssueDetailsEnabled: false,
            partialRefundEnabled: false,
            discountAmountEnabled: false,
            secondPaymentCardEnabled: false,
            vatnumberEnabled: true,
            contactEnabled: false,
            settlementAuthorisationCodeEnabled: false,
            customerFileReferenceEnabled: false,
            taxOnMfCpEnabled: false
          });
        });

        it('should return true when isEditMode and isSupervise are true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return false when isEditMode and isSupervise are true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return true when isReadonlyMode is true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isReadonlyMode is true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditMode is true,isSupervise is false and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditMode is true,isSupervise is false and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isCreateMode is true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isCreateMode is true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditTicketMode is true and agentVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAgentVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditTicketMode is true and agentVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAgentVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));
      });
    });
  });

  describe('isAirlineVatFieldVisible', () => {
    beforeEach(() => {
      mockStore.overrideSelector(fromAuth.getUser, airlineMock);
      (service as any).bspConfig$ = new Subscription();
    });

    describe('RA (airline vatNumber = true)', () => {
      beforeEach(() => {
        //* BSP Refund Configuration for RA (airline vatNumber = true)
        service['_bspRefundConfiguration'].next({
          freeStat: false,
          defaultStat: 'D',
          airlineVatnumber: true,
          agentVatNumber: true,
          fareAdjustmentAmount: true,
          nrRefundsPermitted: true,
          withoutCouponsCapture: true,
          raFinal: true,
          mfFeeAmount: 1
        });

        //* Differents access types RA
        service.refundType = MasterDataType.RefundApp;
      });

      it('should return true when isReadonlyMode is true and airlineVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isReadonlyMode is true and airlineVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isInvestigate is true and airlineVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = true;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isInvestigate is true and airlineVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = true;

        service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isInvestigate is false, isAuthorize is true and airlineVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = false;
        service.isAuthorize = true;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return true when isInvestigate is false, isAuthorize is true and airlineVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = false;
        service.isAuthorize = true;

        service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));
    });

    describe('RA (airline vatNumber = false)', () => {
      beforeEach(() => {
        //* BSP Refund Configuration for RA (airline vatNumber = false)
        service['_bspRefundConfiguration'].next({
          freeStat: false,
          defaultStat: 'D',
          airlineVatnumber: false,
          agentVatNumber: true,
          fareAdjustmentAmount: true,
          nrRefundsPermitted: true,
          withoutCouponsCapture: true,
          raFinal: true,
          mfFeeAmount: 1
        });

        //* Differents access types RA
        service.refundType = MasterDataType.RefundApp;
      });

      it('should return true when isReadonlyMode is true and agentVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return false when isReadonlyMode is true and airlineVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.accessType = AccessType.read;

        service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return true when isInvestigate is true and airlineVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = true;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return false when isInvestigate is true and airlineVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = true;

        service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return true when isInvestigate is false, isAuthorize is true and airlineVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = false;
        service.isAuthorize = true;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return false when isInvestigate is false, isAuthorize is true and airlineVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = false;
        service.isAuthorize = true;

        service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return true when isReadonlyMode is true, previous conditions are false and airlineVatNumber is not null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = false;
        service.isAuthorize = false;
        service.accessType = AccessType.read;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeTruthy();
      }));

      it('should return false when isReadonlyMode is true, previous conditions are false and airlineVatNumber is null', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = false;
        service.isAuthorize = false;
        service.accessType = AccessType.read;

        service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));

      it('should return false when all conditions are false', fakeAsync(() => {
        let vatNumberVisible: boolean;
        service.isInvestigate = false;
        service.isAuthorize = false;
        service.accessType = AccessType.create;

        service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
        tick();

        expect(vatNumberVisible).toBeFalsy();
      }));
    });

    describe('RN', () => {
      beforeEach(() => {
        (service as any).bspRnConfig$ = new Subscription();
        //* Differents access types RN
        service.refundType = MasterDataType.RefundNotice;
      });

      describe('(airline vatNumber = false, airline notice vatNumber = false)', () => {
        beforeEach(() => {
          //* BSP Refund Configuration (airline vatNumber = false)
          service['_bspRefundConfiguration'].next({
            freeStat: false,
            defaultStat: 'D',
            airlineVatnumber: false,
            agentVatNumber: true,
            fareAdjustmentAmount: true,
            nrRefundsPermitted: true,
            withoutCouponsCapture: true,
            raFinal: true,
            mfFeeAmount: 1
          });

          //* BSP Refund Notice Configuration (vatNumberEnabled = false)
          service['_bspRefundNoticeConfiguration'].next({
            originalIssueDetailsEnabled: false,
            partialRefundEnabled: false,
            discountAmountEnabled: false,
            secondPaymentCardEnabled: false,
            vatnumberEnabled: false,
            contactEnabled: false,
            settlementAuthorisationCodeEnabled: false,
            customerFileReferenceEnabled: false,
            taxOnMfCpEnabled: false
          });
        });

        it('should return false when isEditMode and isSupervise are true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isEditMode and isSupervise are true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return true when isReadonlyMode is true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return false when isReadonlyMode is true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return true when isEditMode is true,isSupervise is false and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return false when isEditMode is true,isSupervise is false and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isCreateMode is true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isCreateMode is true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isEditTicketMode is true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return false when isEditTicketMode is true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));
      });

      describe('(airline vatNumber = true, airline notice vatNumber = true)', () => {
        beforeEach(() => {
          //* BSP Refund Configuration (airline vatNumber = false)
          service['_bspRefundConfiguration'].next({
            freeStat: false,
            defaultStat: 'D',
            airlineVatnumber: true,
            agentVatNumber: true,
            fareAdjustmentAmount: true,
            nrRefundsPermitted: true,
            withoutCouponsCapture: true,
            raFinal: true,
            mfFeeAmount: 1
          });

          //* BSP Refund Notice Configuration (vatNumberEnabled = false)
          service['_bspRefundNoticeConfiguration'].next({
            originalIssueDetailsEnabled: false,
            partialRefundEnabled: false,
            discountAmountEnabled: false,
            secondPaymentCardEnabled: false,
            vatnumberEnabled: true,
            contactEnabled: false,
            settlementAuthorisationCodeEnabled: false,
            customerFileReferenceEnabled: false,
            taxOnMfCpEnabled: false
          });
        });

        it('should return true when isEditMode and isSupervise are true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return false when isEditMode and isSupervise are true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = true;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeFalsy();
        }));

        it('should return true when isReadonlyMode is true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isReadonlyMode is true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.read;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditMode is true,isSupervise is false and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditMode is true,isSupervise is false and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.edit;
          service.isSupervise = false;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isCreateMode is true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isCreateMode is true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.create;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditTicketMode is true and airlineVatNumber  is not null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));

        it('should return true when isEditTicketMode is true and airlineVatNumber is null', fakeAsync(() => {
          let vatNumberVisible: boolean;
          service.accessType = AccessType.editTicket;

          service.isAirlineVatFieldVisible(null).subscribe(res => (vatNumberVisible = res));
          tick();

          expect(vatNumberVisible).toBeTruthy();
        }));
      });
    });

    it('should return false when not RA and not RN', fakeAsync(() => {
      let vatNumberVisible: boolean;
      //* Differents access types not RN, not RA
      service.refundType = MasterDataType.AGENT_TA;

      service.isAirlineVatFieldVisible('1234567').subscribe(res => (vatNumberVisible = res));
      tick();

      expect(vatNumberVisible).toBeFalsy();
    }));
  });

  it('should return contact field visibility', fakeAsync(() => {
    let contactVisible = false;

    //* Refund Application contact field visibility
    service.refundType = MasterDataType.RefundApp;
    service.isContactFieldVisible(null).subscribe(res => expect(res).toBeTruthy());
    tick();

    //* Refund Notice contact field visibility
    service.refundType = MasterDataType.RefundNotice;

    //* BSP Refund Notice Configuration (contactEnabled = true)
    service['_bspRefundNoticeConfiguration'].next({
      originalIssueDetailsEnabled: false,
      partialRefundEnabled: false,
      discountAmountEnabled: false,
      secondPaymentCardEnabled: false,
      vatnumberEnabled: true,
      contactEnabled: true,
      settlementAuthorisationCodeEnabled: false,
      customerFileReferenceEnabled: false,
      taxOnMfCpEnabled: false
    });

    //* Access type = read
    service.accessType = AccessType.read;
    service.isContactFieldVisible(null).subscribe(res => (contactVisible = res));
    expect(contactVisible).toBeFalsy();
    tick();

    //* BSP Refund Notice Configuration (contactEnabled = false)
    service['_bspRefundNoticeConfiguration'].next({
      originalIssueDetailsEnabled: false,
      partialRefundEnabled: false,
      discountAmountEnabled: false,
      secondPaymentCardEnabled: false,
      vatnumberEnabled: true,
      contactEnabled: false,
      settlementAuthorisationCodeEnabled: false,
      customerFileReferenceEnabled: false,
      taxOnMfCpEnabled: false
    });

    //* Access type = read
    service.accessType = AccessType.read;
    service.isContactFieldVisible(null).subscribe(res => (contactVisible = res));
    tick();
    expect(contactVisible).toBeFalsy();
    service
      .isContactFieldVisible({ contactName: 'contact test', email: 'contact@email.com', phoneFaxNumber: '1234567' })
      .subscribe(res => (contactVisible = res));
    tick();
    expect(contactVisible).toBeFalsy();

    //* Access type != read
    service.accessType = AccessType.create;
    service.isContactFieldVisible(null).subscribe(res => (contactVisible = res));
    tick();
    expect(contactVisible).toBeFalsy();
  }));

  it('return logged user button permission', fakeAsync(() => {
    //* RA
    mockStore.overrideSelector(fromAuth.getUser, { ...mockAgent, permissions: [] });
    service['hasUserBtnPermission'](RefundActionType.issue).subscribe(res => expect(res).toBeTruthy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.edit).subscribe(res => expect(res).toBeTruthy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.applyChanges).subscribe(res => expect(res).toBeTruthy());
    tick();
    mockStore.overrideSelector(fromAuth.getUser, {
      ...mockAgent,
      permissions: [createRefundPermission.raIssue, modifyRefundPermission.raModify]
    });
    service['hasUserBtnPermission'](RefundActionType.issue).subscribe(res => expect(res).toBeFalsy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.edit).subscribe(res => expect(res).toBeFalsy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.applyChanges).subscribe(res => expect(res).toBeFalsy());
    tick();

    //* RN
    service.refundType = MasterDataType.RefundNotice;
    mockStore.overrideSelector(fromAuth.getUser, { ...mockAgent, permissions: [] });
    service['hasUserBtnPermission'](RefundActionType.issue).subscribe(res => expect(res).toBeTruthy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.edit).subscribe(res => expect(res).toBeTruthy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.applyChanges).subscribe(res => expect(res).toBeTruthy());
    tick();
    mockStore.overrideSelector(fromAuth.getUser, {
      ...mockAgent,
      permissions: [createRefundPermission.rnIssue, modifyRefundPermission.rnModify]
    });
    service['hasUserBtnPermission'](RefundActionType.issue).subscribe(res => expect(res).toBeFalsy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.edit).subscribe(res => expect(res).toBeFalsy());
    tick();
    service['hasUserBtnPermission'](RefundActionType.applyChanges).subscribe(res => expect(res).toBeFalsy());
    tick();
  }));

  it('return header button disabled value', () => {
    service['isHeaderBtnDisabled'](RefundActionType.issue).subscribe(res => expect(res).toBeFalsy());
    service['isHeaderBtnDisabled'](RefundActionType.applyChanges).subscribe(res => expect(res).toBeTruthy());
    mockStore.overrideSelector(getRefundForm, {
      ...initialFormState,
      hasChanges: true
    });
    service['isHeaderBtnDisabled'](RefundActionType.applyChanges).subscribe(res => expect(res).toBeFalsy());
  });

  it('should return customer reference field visibility', fakeAsync(() => {
    //* RA
    service.isCustomerReferenceVisible(null).subscribe(res => expect(res).toBeTruthy());
    tick();
    service.isCustomerReferenceVisible('1234').subscribe(res => expect(res).toBeTruthy());
    tick();

    //* RN
    service.refundType = MasterDataType.RefundNotice;
    //* BSP Refund Notice Configuration (customerFileReferenceEnabled = true)
    httpClientSpy.get.and.returnValue(
      of({
        originalIssueDetailsEnabled: false,
        partialRefundEnabled: false,
        discountAmountEnabled: false,
        secondPaymentCardEnabled: false,
        vatnumberEnabled: true,
        contactEnabled: true,
        settlementAuthorisationCodeEnabled: false,
        customerFileReferenceEnabled: true,
        taxOnMfCpEnabled: false
      })
    );
    service.accessType = AccessType.create;
    service.isCustomerReferenceVisible(null).subscribe(res => expect(res).toBeTruthy());
    tick();
    service.accessType = AccessType.editTicket;
    service.isCustomerReferenceVisible(null).subscribe(res => expect(res).toBeTruthy());
    tick();
    service.accessType = AccessType.edit;
    service.isCustomerReferenceVisible(null).subscribe(res => expect(res).toBeTruthy());
    tick();
    service.isCustomerReferenceVisible('1234').subscribe(res => expect(res).toBeTruthy());
    tick();
    service.accessType = AccessType.read;
    service.isCustomerReferenceVisible(null).subscribe(res => expect(res).toBeTruthy());
    tick();
    service.isCustomerReferenceVisible('1234').subscribe(res => expect(res).toBeTruthy());
    tick();
  }));

  it('should return e-Ticket Code field visibility', fakeAsync(() => {
    //* RA
    service.iseTicketCodeVisible(null).subscribe(res => expect(res).toBeTruthy());
    tick();
    service.iseTicketCodeVisible('1234').subscribe(res => expect(res).toBeTruthy());
    tick();

    //* BSP Refund Notice Configuration (customerFileReferenceEnabled = true)
    httpClientSpy.get.and.returnValue(
      of({
        originalIssueDetailsEnabled: false,
        partialRefundEnabled: false,
        discountAmountEnabled: false,
        secondPaymentCardEnabled: false,
        vatnumberEnabled: true,
        contactEnabled: true,
        settlementAuthorisationCodeEnabled: false,
        customerFileReferenceEnabled: true,
        taxOnMfCpEnabled: false
      })
    );
    //* RN
    service.refundType = MasterDataType.RefundNotice;
    service.accessType = AccessType.create;
    service.iseTicketCodeVisible(null).subscribe(res => expect(res).toBeFalsy());
    tick();
    service.accessType = AccessType.editTicket;
    service.iseTicketCodeVisible(null).subscribe(res => expect(res).toBeFalsy());
    tick();
    service.accessType = AccessType.edit;
    service.iseTicketCodeVisible(null).subscribe(res => expect(res).toBeFalsy());
    tick();
    service.iseTicketCodeVisible('1234').subscribe(res => expect(res).toBeTruthy());
    tick();
    service.accessType = AccessType.read;
    service.iseTicketCodeVisible(null).subscribe(res => expect(res).toBeFalsy());
    tick();
    service.iseTicketCodeVisible('1234').subscribe(res => expect(res).toBeTruthy());
    tick();
  }));

  it('should return Penalty Amount field visibility', fakeAsync(() => {
    //* Airline Configuration (customerFileReferenceEnabled = true)
    httpClientSpy.get.and.returnValue(
      of({
        id: 111,
        descriptorId: 77844852551100,
        scopeId: '7784485255',
        scopeType: 'airlines',
        service: 'refund-management',
        parameters: [
          {
            descriptorId: 77844852551106,
            id: 77844852551106,
            name: 'HANDLING_FEE',
            readonly: false,
            type: 'BOOLEAN',
            value: 'true'
          },
          {
            descriptorId: 77844852551107,
            id: 77844852551107,
            name: 'PENALTY_CHARGE',
            readonly: false,
            type: 'BOOLEAN',
            value: 'true'
          },
          {
            descriptorId: 77844852551108,
            id: 77844852551108,
            name: 'COMMISSION_ON_CP',
            readonly: false,
            type: 'BOOLEAN',
            value: 'false'
          }
        ]
      })
    );

    service.accessType = AccessType.create;
    service
      .isPenaltyAmountFieldVisible('cancellationPenalty')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnCancellationPenalty')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('miscellaneousFee')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnMiscellaneousFee')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('commissionForCpAndMf')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeFalsy());
    tick();

    service.accessType = AccessType.editTicket;
    service
      .isPenaltyAmountFieldVisible('cancellationPenalty')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnCancellationPenalty')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('miscellaneousFee')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnMiscellaneousFee')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('commissionForCpAndMf')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeFalsy());
    tick();

    service.accessType = AccessType.edit;
    service
      .isPenaltyAmountFieldVisible('cancellationPenalty', '0')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('cancellationPenalty', '1')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnCancellationPenalty', '0')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnCancellationPenalty', '1')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('miscellaneousFee', '0')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('miscellaneousFee', '1')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnMiscellaneousFee', '0')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('taxOnMiscellaneousFee', '1')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
    service
      .isPenaltyAmountFieldVisible('commissionForCpAndMf', '0')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeFalsy());
    tick();
    service
      .isPenaltyAmountFieldVisible('commissionForCpAndMf', '1')
      .pipe(skip(1))
      .subscribe(res => expect(res).toBeTruthy());
    tick();
  }));
});
