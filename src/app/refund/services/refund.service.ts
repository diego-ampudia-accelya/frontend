import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { first, map, startWith, switchMap, tap } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { RefundFilter } from '../models/refund-filter.model';
import { RefundReason } from '../models/refund-reason.model';
import { RefundOptionType } from '../models/refund-shared.model';
import { getUserBsps } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models';
import { Period } from '~app/master-data/periods/shared/period.models';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { AppState } from '~app/reducers';
import { BasicAgentInfo, BasicAirlineInfo, BspTimeModel } from '~app/refund/models/refund-be-aux.model';
import { CurrencyViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundBE, RefundFinal } from '~app/refund/models/refund.model';
import { PeriodUtils } from '~app/shared/components';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { DataQuery } from '~app/shared/components/list-view';
import { RequestMethod } from '~app/shared/enums/request-method.enum';
import { NumberHelper } from '~app/shared/helpers';
import { contactInfoCacheBuster$ } from '~app/shared/helpers/cacheable-helper';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus, MarkStatusUpdateModel } from '~app/shared/models/mark-status.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { WatcherDocType, WatcherModel } from '~app/shared/models/watcher.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { DynamicEndpointClass } from '~app/shared/services/endpoints.config';
import { buildEndpointPath } from '~app/shared/services/endpoints.helper';

@Injectable()
export class RefundService extends ApiService<RefundBE> {
  private periods: Period[];

  constructor(
    protected http: HttpClient,
    private activatedRoute: ActivatedRoute,
    @Optional() private activatedRouteSnapshot?: ActivatedRouteSnapshot,
    @Optional() private notificationHeaderService?: NotificationsHeaderService,
    @Optional() private store?: Store<AppState>,
    @Optional() private periodService?: PeriodService
  ) {
    super(http, '');

    const routeSnapshotData = this.activatedRouteSnapshot
      ? this.activatedRouteSnapshot.data
      : this.activatedRoute.snapshot.data;

    this.baseUrl = buildEndpointPath(DynamicEndpointClass.RefundService, routeSnapshotData);

    if (this.store && this.periodService) {
      this.store
        .pipe(
          select(getUserBsps),
          switchMap(bsps => (bsps.length ? this.periodService.getByBsp(bsps[0].id) : of([]))),
          first()
        )
        .subscribe(periods => (this.periods = periods));
    }
  }

  public getBspsList(active?: boolean): Observable<PagedData<BspDto>> {
    const params = { ...(active && { active }) };

    return this.getAllIssues('bsps', params);
  }

  public getAll(requestQuery: RequestQuery): Observable<PagedData<any>> {
    return super.getAll(requestQuery).pipe(tap(result => result.records.forEach(record => this.dataAdapter(record))));
  }

  public find(dataQuery: DataQuery): Observable<PagedData<RefundBE>> {
    const reqQuery = this.formatQuery(dataQuery);

    return this.getAll(reqQuery);
  }

  public setMarkStatus(items: RefundBE[], markStatus: MarkStatus): Observable<MarkStatusUpdateModel[]> {
    const url = `${this.baseUrl}/mark-status`;
    const payload = items.map(item => ({
      id: item.id,
      markStatus
    }));

    return this.http.post<MarkStatusUpdateModel[]>(url, payload);
  }

  public getBspCurrencies(bspId, activeParam = true): Observable<CurrencyViewModel[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/bsps/${bspId}/currencies`;
    const paramQuery = {
      active: activeParam.toString()
    };

    return this.http.get(baseUrl, { params: paramQuery }).pipe(map((info: any) => info));
  }

  @Cacheable({ cacheBusterObserver: contactInfoCacheBuster$ })
  public getAgentInfo(agentId: number): Observable<BasicAgentInfo> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/agent-info/${agentId}`;

    return this.http.get<BasicAgentInfo>(baseUrl);
  }

  @Cacheable({ cacheBusterObserver: contactInfoCacheBuster$ })
  public getAirlineInfo(airlineId: number): Observable<BasicAirlineInfo> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/airline-info/${airlineId}`;

    return this.http.get<BasicAirlineInfo>(baseUrl);
  }

  @Cacheable()
  public getRAReasons(bspId: number): Observable<DropdownOption<RefundReason>[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/bsps/${bspId}/refund-application-issue-reason-configuration`;

    return this.http
      .get<RefundReason[]>(baseUrl)
      .pipe(map(item => item.map(value => ({ value, label: value?.title }))));
  }

  public getRejectRAReasons(bspId: number): Observable<DropdownOption<RefundReason>[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/bsps/${bspId}/refund-application-rejection-reason-configuration`;

    return this.http.get<RefundReason[]>(baseUrl).pipe(map(item => item.map(value => ({ value, label: value.title }))));
  }

  public getBspTime(bspId: number): Observable<BspTimeModel> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/bsps/${bspId}/time`;

    return this.http.get<BspTimeModel>(baseUrl).pipe(map(time => ({ ...time, localTime: new Date(time.localTime) })));
  }

  public download(requestQueryBody, downloadFormat: string, _downloadBody: any = {}, detailedDownload?: boolean) {
    const query = this.formatQuery(requestQueryBody).getQueryString();
    const urlPath = detailedDownload
      ? `?exportAs=${downloadFormat.toUpperCase()}&exportDetail=true&${query}`
      : `?exportAs=${downloadFormat.toUpperCase()}&${query}`;

    return super.download({}, urlPath, RequestMethod.Get);
  }

  public getRefundActionList(refundId: number): Observable<{ action: GridTableActionType; disabled?: boolean }[]> {
    const baseUrl = `${this.baseUrl}/${refundId}/actions`;

    return this.http.get<{ id: GridTableActionType; enabled: boolean }[]>(baseUrl).pipe(
      startWith([{ id: GridTableActionType.Loading, enabled: false }]),
      map(actionList =>
        actionList
          .map(element => ({
            action: this.fromBEActionToGridTableAction(element.id),
            disabled: !element.enabled
          }))
          .filter(element => element.action)
      )
    );
  }

  public getRefundMenuOptions(isArchivedSubTab?: boolean): Observable<ButtonMenuOption[]> {
    const baseUrl = isArchivedSubTab
      ? `${this.baseUrl}/actions?markStatus=${MarkStatus.Archive}`
      : `${this.baseUrl}/actions`;

    return this.http.get<{ id: RefundOptionType; enabled: boolean }[]>(baseUrl).pipe(
      startWith([{ id: RefundOptionType.Loading, enabled: false }]),
      map(options => {
        const refundOptionTypeValues = Object.values(RefundOptionType);

        return options
          .filter(option => refundOptionTypeValues.includes(option.id))
          .map(({ id, enabled }) => ({
            id,
            label: id,
            isDisabled: !enabled
          }));
      })
    );
  }

  public addComment(requestBody: { reason: string[]; attachmentIds?: string[] }, id: number): Observable<Activity[]> {
    const baseUrl = `${this.baseUrl}/${id}/comments`;

    return this.http.post<Activity[]>(baseUrl, requestBody);
  }

  public addInternalComment(id: number, body: { internalComment: string }): Observable<any> {
    const baseUrl = `${this.baseUrl}/${id}/internal-comment`;

    return this.http.post(baseUrl, body);
  }

  public editInternalComment(id: number, body: { internalComment: string }): Observable<any> {
    const baseUrl = `${this.baseUrl}/${id}/internal-comment`;

    return this.http.put(baseUrl, body);
  }

  public deleteInternalComment(id: number): Observable<any> {
    const baseUrl = `${this.baseUrl}/${id}/internal-comment`;

    return this.http.delete(baseUrl);
  }

  public bulkAuthorize(
    items: RefundBE[],
    body: { airlineRemark: string; attachmentIds: string[] },
    bspId: number
  ): Observable<any> {
    const baseUrl = `${this.baseUrl}/authorize`;
    const ids = items.map(item => item.id);

    return this.http.post(baseUrl, { ...body, ids }, { params: { bspId: '' + bspId } });
  }

  public bulkReject(
    items: RefundBE[],
    body: { rejectionReason: string; attachmentIds: string[] },
    bspId: number
  ): Observable<any> {
    const baseUrl = `${this.baseUrl}/reject`;
    const ids = items.map(item => item.id);

    return this.http.post(baseUrl, { ...body, ids }, { params: { bspId: '' + bspId } });
  }

  public bulkDelete(items: RefundBE[], body: { deletionReason: string }, bspId: number): Observable<any> {
    const baseUrl = `${this.baseUrl}/delete`;
    const ids = items.map(item => item.id);

    return this.http.post(baseUrl, { ...body, ids }, { params: { bspId: '' + bspId } });
  }

  public bulkComment(
    items: RefundBE[],
    body: { reason: string; attachmentIds: string[] },
    bspId: number
  ): Observable<any> {
    const baseUrl = `${this.baseUrl}/comments`;
    const ids = items.map(item => item.id);

    return this.http.post(baseUrl, { ...body, ids }, { params: { bspId: '' + bspId } });
  }

  public bulkSupervise(
    items: RefundBE[],
    body: { reasonForRefund: string; attachmentIds: string[] },
    bspId: number
  ): Observable<any> {
    const baseUrl = `${this.baseUrl}/supervise`;
    const ids = items.map(item => item.id);

    return this.http.post(baseUrl, { ...body, ids }, { params: { bspId: '' + bspId } });
  }

  // Bulk actions can only be performed, at least for now, when filtering by one BSP, so we can obtain that BSP from the filtered items
  // In the future, bulk actions will be supported for multiple BSPs, so this functionality and `bspId` won't be necessary
  public bulkDownloadAttachments(items: RefundBE[]): Observable<{ blob: Blob; fileName: string }> {
    const urlPath = `/download-attachments`;
    const ids = items.map(item => item.id);

    return super.download({ ids }, urlPath);
  }

  public updateWatchers(items: RefundBE[], watching: boolean): Observable<WatcherModel[]> {
    return forkJoin(
      items.map(item =>
        this.notificationHeaderService.updateWatcher(
          {
            documentId: item.id,
            documentType: WatcherDocType.REFUND,
            watching
          },
          (item.agent as Agent).bsp.id
        )
      )
    );
  }

  private getAllIssues(type: 'agents' | 'airlines' | 'bsps', paramQuery): Observable<PagedData<any>> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/${type}`;

    return this.http
      .get<PagedData<any>>(baseUrl, { params: paramQuery })
      .pipe(map((pagedData: PagedData<any>) => pagedData));
  }

  private dataAdapter(data): void {
    data.totalAmount = NumberHelper.roundString(data.totalAmount, data.currency.decimals) as any;
  }

  private formatQuery(query: DataQuery<RefundFilter>): RequestQuery {
    const {
      bsp,
      airlineIataCode,
      agentId,
      dateOfRejection,
      dateOfIssue,
      dateOfApplication,
      dateOfLatestAction,
      reportingDate,
      currencyId,
      period,
      finalType,
      amountRange,
      finalDaysLeftRange,
      finalProcessStoppedRange,
      ...filterBy
    } = query.filterBy;

    let bsps: BspDto[];
    if (bsp) {
      bsps = !Array.isArray(bsp) ? [bsp] : bsp;
    }

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        bspId: bsps && bsps.map(({ id }) => id),
        airlineIataCode: airlineIataCode && airlineIataCode.map(airl => airl.code),
        agentId: agentId && agentId.map(agt => agt.id),
        fromDateOfApplication: dateOfApplication && toShortIsoDate(dateOfApplication[0]),
        toDateOfApplication:
          dateOfApplication && toShortIsoDate(dateOfApplication[1] ? dateOfApplication[1] : dateOfApplication[0]),
        fromDateOfIssue: dateOfIssue && toShortIsoDate(dateOfIssue[0]),
        toDateOfIssue: dateOfIssue && toShortIsoDate(dateOfIssue[1] ? dateOfIssue[1] : dateOfIssue[0]),
        fromDateOfRejection: dateOfRejection && toShortIsoDate(dateOfRejection[0]),
        toDateOfRejection:
          dateOfRejection && toShortIsoDate(dateOfRejection[1] ? dateOfRejection[1] : dateOfRejection[0]),
        fromDateOfLatestAction: dateOfLatestAction && toShortIsoDate(dateOfLatestAction[0]),
        toDateOfLatestAction:
          dateOfLatestAction && toShortIsoDate(dateOfLatestAction[1] ? dateOfLatestAction[1] : dateOfLatestAction[0]),
        fromReportingDate: reportingDate && toShortIsoDate(reportingDate[0]),
        toReportingDate: reportingDate && toShortIsoDate(reportingDate[1] ? reportingDate[1] : reportingDate[0]),
        currencyId: currencyId && currencyId.map(curr => curr.id),
        fromTotalAmount: amountRange && amountRange[0],
        toTotalAmount: amountRange && amountRange[1],
        period:
          period &&
          PeriodUtils.getPeriodsBetweenDates(this.periods, period[0].dateFrom, period[period.length - 1].dateTo).map(
            p => p.period
          ),
        finalType: finalType && this.formatFinalTypeFilter(finalType),
        fromFinalDaysLeft: finalDaysLeftRange && finalDaysLeftRange[0],
        toFinalDaysLeft: finalDaysLeftRange && finalDaysLeftRange[1],
        fromFinalProcessStopped: finalProcessStoppedRange && finalProcessStoppedRange[0],
        toFinalProcessStopped: finalProcessStoppedRange && finalProcessStoppedRange[1]
      }
    });
  }

  //* Needed to map BE values to GridTableActionType ones
  // TODO Provisional. Actions logic needs full refactor in RFND. ACDMs style is desirable.
  private fromBEActionToGridTableAction(actionBE: RefundOptionType | any): GridTableActionType {
    const mapper: { [key in RefundOptionType]?: GridTableActionType } = {
      [RefundOptionType.InternalComment]: GridTableActionType.InternalComment
    };
    const gridTableObject = Object.keys(GridTableActionType);
    //* We need to access GridTableActionType case insensitively since element.id does not match its keys
    const gridTableAction =
      GridTableActionType[gridTableObject.find(action => action.toUpperCase() === actionBE.toUpperCase())];

    return mapper[actionBE] || gridTableAction;
  }

  // TODO: This functionality will be changed in FCA-9527 when BE functionality is ready
  private formatFinalTypeFilter(finalType: string[]): string[] {
    return finalType.includes(null) && finalType.length === 1
      ? [`!${RefundFinal.Final}`, RefundFinal.Exception]
      : finalType;
  }
}
