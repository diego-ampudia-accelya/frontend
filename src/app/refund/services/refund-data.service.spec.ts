import { fakeAsync, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { RefundStatus } from '../models/refund-be-aux.model';
import {
  AmountTaxViewModel,
  BasicInformationViewModel,
  DetailViewModel,
  FormOfPaymentViewModel,
  StatusHeaderViewModel,
  StatusViewModel
} from '../models/refund-view-aux.model';
import { RefundViewModel } from '../models/refund.model';

import { RefundDataService } from './refund-data.service';
import { FormUtil } from '~app/shared/helpers';

describe('RefundDataService', () => {
  let service: RefundDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RefundDataService, FormBuilder]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(RefundDataService);

    const builder = TestBed.inject(FormBuilder);
    const formUtil = new FormUtil(builder);

    const status = formUtil.createGroup<StatusViewModel>({
      status: RefundStatus.pending,
      sentToDpc: null
    });

    const statusHeader = formUtil.createGroup<StatusHeaderViewModel>({
      agentCode: '',
      airlCode: '',
      status,
      amount: '',
      appDate: '',
      bspName: '',
      docNumber: '',
      finalType: null,
      finalDaysLeft: null,
      finalProcessStopped: null
    });

    const basicInformation = formUtil.createGroup<BasicInformationViewModel>({
      agent: null,
      airline: null,
      agentContact: null,
      airlineContact: null
    });

    const details = formUtil.createGroup<DetailViewModel>({
      airlineCodeRelatedDocument: '',
      dateOfIssueRelatedDocument: '',
      originalIssue: null,
      passenger: '',
      reasonForRefund: '',
      relatedTicketConjunctions: null,
      relatedTicketDocument: null,
      relatedTicketExchange: null,
      rejectedDocumentId: '',
      resubmissionRemark: '',
      statisticalCode: '',
      waiverCode: ''
    });

    const formPayment = formUtil.createGroup<FormOfPaymentViewModel>({
      cardPayments: null,
      cardTotal: '',
      cashPaymentAmount: '',
      cashTotal: '',
      currency: null,
      customerReference: '',
      eTicketAuthCode: '',
      miscellaneousCashAmount: '',
      totalAmount: '',
      tourCode: ''
    });

    const amounts = formUtil.createGroup<AmountTaxViewModel>({
      cancellationPenalty: null,
      commission: null,
      commissionForCpAndMf: null,
      grossFare: null,
      lessGrossFareUsed: null,
      miscellaneousFee: null,
      netReporting: null,
      partialRefund: null,
      supplementaryCommission: null,
      taxMiscellaneousFees: null,
      taxOnCancellationPenalty: null,
      taxOnMiscellaneousFee: null,
      totalAmount: null,
      totalGrossFare: null
    });

    service['form'] = formUtil.createGroup<RefundViewModel>({
      statusHeader,
      basicInformation,
      details,
      formPayment,
      amounts
    });

    service.isFormReady.next(true);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should return correct form issue status', fakeAsync(() => {
    let formStatus: string;
    service.getIssueStatus().subscribe(statusView => (formStatus = statusView.status));

    expect(formStatus).toBe(RefundStatus.pending);
  }));
});
