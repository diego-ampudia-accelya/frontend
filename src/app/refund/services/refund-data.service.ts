import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { CurrencyViewModel, StatusHeaderViewModel, StatusViewModel } from '../models/refund-view-aux.model';
import { RefundActionEmitterType, RefundBE, RefundViewModel } from '../models/refund.model';
import { adapterBEFunctionsMapper, fieldMapperRefund } from '../shared/helpers/view-be-mapper.config';
import { Agent } from '~app/master-data/models';
import { LocalAndGlobalAirline } from '~app/master-data/models/airline/local-and-global-airline.model';
import { FormUtil } from '~app/shared/helpers';
import { BspDto } from '~app/shared/models/bsp.model';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { FormDataService } from '~app/shared/services/form-data.service';

@Injectable()
export class RefundDataService extends FormDataService<RefundBE, RefundViewModel> {
  protected get initialForm() {
    return { id: null, attachmentIds: null };
  }

  protected get initialSubjects(): { [key: string]: Subject<any> } {
    return {
      currency: new BehaviorSubject<CurrencyViewModel>(null),
      agent: new BehaviorSubject<Agent>(null),
      airline: new BehaviorSubject<LocalAndGlobalAirline>(null),
      totalAmount: new BehaviorSubject<string>(null),
      bsp: new BehaviorSubject<BspDto>(null), // `Bsp` and `BspDto` values are emitted here, but BspDto is the more restrictive between the two
      partialRefund: new BehaviorSubject<boolean>(false)
    };
  }

  constructor(protected fb: FormBuilder) {
    super(fb);
  }

  public getSubject(name: RefundActionEmitterType) {
    return super.getSubject(name);
  }

  public getDataForSubmit(): RefundBE {
    return super.getDataForSubmit(fieldMapperRefund, adapterBEFunctionsMapper);
  }

  public processErrorsOnSubmit(errorResponse: ResponseErrorBE): void {
    super.processErrorsOnSubmit(errorResponse, fieldMapperRefund);
  }

  public getIssueStatus(): Observable<StatusViewModel> {
    return this.whenFormIsReady().pipe(
      map(() => {
        const statusGroup = this.getSectionFormGroup('statusHeader');

        return FormUtil.get<StatusHeaderViewModel>(statusGroup, 'status').value;
      })
    );
  }
}
