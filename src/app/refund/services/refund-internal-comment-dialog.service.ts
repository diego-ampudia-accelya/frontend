import { Injectable } from '@angular/core';

import { RefundBE } from '../models/refund.model';
import {
  getRefundCreateInternalCommentPermission,
  getRefundDeleteInternalCommentPermission,
  getRefundUpdateInternalCommentPermission
} from '../shared/helpers/refund-permissions.config';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundInternalCommentDialogComponent } from '~app/refund/components/dialogs/refund-internal-comment-dialog/refund-internal-comment-dialog.component';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Injectable()
export class RefundInternalCommentDialogService {
  private hasDeleteInternalCommentPermission: boolean;
  private hasCreateInternalCommentPermission: boolean;
  private hasUpdateInternalCommentPermission: boolean;

  constructor(private dialogService: DialogService, private permissionsService: PermissionsService) {}

  public open(refund: RefundBE, refundType: MasterDataType): void {
    // Permissions
    this.hasDeleteInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundDeleteInternalCommentPermission(refundType)
    );
    this.hasCreateInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundCreateInternalCommentPermission(refundType)
    );
    this.hasUpdateInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundUpdateInternalCommentPermission(refundType)
    );

    const dialogConfig: DialogConfig = {
      data: {
        refund,
        refundType,
        footerButtonsType: this.buildInternalCommentDialogButtons(refund, refundType),
        hasCancelButton: false,
        isReadonly: !this.isEditable(refund)
      }
    };

    this.dialogService.open(RefundInternalCommentDialogComponent, dialogConfig);
  }

  private isEditable(refund: RefundBE): boolean {
    return (
      (!refund.internalComment && this.hasUpdateInternalCommentPermission) ||
      (!!refund.internalComment && this.hasCreateInternalCommentPermission)
    );
  }

  private buildInternalCommentDialogButtons(refund: RefundBE, refundType: MasterDataType): ModalAction[] {
    // Buttons
    const deleteButton: ModalAction = {
      type: FooterButton.Delete,
      isDisabled: !refund.internalComment,
      buttonDesign: ButtonDesign.Secondary
    };
    const postButton: ModalAction = { type: FooterButton.Post, isDisabled: true };
    const cancelButton = { type: FooterButton.Cancel, buttonDesign: ButtonDesign.Tertiary };
    const doneButton = { type: FooterButton.Done, buttonDesign: ButtonDesign.Tertiary };

    const buttons = [
      ...this.insertIf(this.hasDeleteInternalCommentPermission, deleteButton),
      ...this.insertIf(this.hasCreateInternalCommentPermission && !refund.internalComment, postButton),
      ...this.insertIf(this.hasUpdateInternalCommentPermission && !!refund.internalComment, postButton)
    ];

    return buttons.length ? [cancelButton, ...buttons] : [doneButton];
  }

  private insertIf(condition: boolean, button: ModalAction): ModalAction[] {
    return condition ? [button] : [];
  }
}
