import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundConfirmationDialogComponent } from '~app/refund/components/dialogs/refund-confirmation-dialog/refund-confirmation-dialog.component';
import { RefundMultipleConfirmationDialogComponent } from '~app/refund/components/dialogs/refund-multiple-confirmation-dialog/refund-multiple-confirmation-dialog.component';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { RefundActivityHistoryDialogComponent } from '../components/dialogs/refund-activity-history-dialog/refund-activity-history-dialog.component';
import { RefundNotFoundDialogComponent } from '../components/dialogs/refund-not-found-dialog/refund-not-found-dialog.component';
import { RefundActionType, RefundBE } from '../models/refund.model';

@Injectable({
  providedIn: 'root'
})
export class RefundDialogService {
  private footerButtons: { [key in RefundActionType]?: FooterButton } = {
    [RefundActionType.delete]: FooterButton.Delete,
    [RefundActionType.authorize]: FooterButton.Authorize,
    [RefundActionType.reject]: FooterButton.Reject,
    [RefundActionType.investigate]: FooterButton.Investigate,
    [RefundActionType.issue]: FooterButton.Issue,
    [RefundActionType.applyChanges]: FooterButton.ApplyChanges,
    [RefundActionType.issuePendingSupervision]: FooterButton.Issue,
    [RefundActionType.startResubmission]: FooterButton.StartResubmission,
    [RefundActionType.resubmit]: FooterButton.Resubmit,
    [RefundActionType.comment]: FooterButton.Comment,
    [RefundActionType.supervise]: FooterButton.Supervise
  };

  constructor(private dialog: DialogService, private translationService: L10nTranslationService) {}

  public openDialog(
    refundAction: RefundActionType,
    rfndItem: RefundBE,
    refundType: MasterDataType,
    dialogForm: FormGroup,
    extraConfig?: any
  ): Observable<any> {
    return this.dialog.open(RefundConfirmationDialogComponent, {
      data: {
        refundType,
        footerButtonsType: this.footerButtons[refundAction],
        rowTableContent: rfndItem,
        action: refundAction,
        dialogForm,
        ...extraConfig
      }
    });
  }

  public openMultipleDialog(
    refundAction: RefundActionType,
    refundType: MasterDataType,
    refundItems: RefundBE[]
  ): Observable<any> {
    return this.dialog.open(RefundMultipleConfirmationDialogComponent, {
      data: {
        footerButtonsType: { type: this.footerButtons[refundAction], isDisabled: true },
        refundType,
        action: refundAction,
        items: refundItems
      }
    });
  }

  public openActivityHistoryDialog(refundItem: RefundBE, refundType: MasterDataType): Observable<any> {
    const dialogConfig: DialogConfig = {
      data: {
        refundItem,
        refundType,
        hasCancelButton: false,
        footerButtonsType: { type: FooterButton.Close, buttonDesign: ButtonDesign.Primary },
        title: `${this.translateTitle(refundType)} ${refundItem.ticketDocumentNumber}`
      }
    };

    return this.dialog.open(RefundActivityHistoryDialogComponent, dialogConfig);
  }

  public openDialogNotFoundDialogComponent(): Observable<any> {
    return this.dialog.open(RefundNotFoundDialogComponent, {
      data: {
        footerButtonsType: FooterButton.Ok,
        hasCancelButton: false,
        isClosable: false
      }
    });
  }

  private translateTitle(refundType: MasterDataType): string {
    return this.translationService.translate(`REFUNDS.query.${refundType}.historyDialogLabel`);
  }
}
