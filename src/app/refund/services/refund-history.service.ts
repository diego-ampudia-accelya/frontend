import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { MasterDataType } from '~app/master-shared/models/master.model';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { ROUTES } from '~app/shared/constants/routes';
import { appConfiguration } from '~app/shared/services';

@Injectable()
export class RefundHistoryService {
  constructor(protected http: HttpClient) {}

  public getActivities(refundId: number, refundType: MasterDataType): Observable<Activity[]> {
    let result: Observable<Activity[]> = of([]);

    if (refundType === MasterDataType.RefundApp) {
      result = this.getActivitiesForRefundApp(refundId);
    }

    if (refundType === MasterDataType.RefundNotice) {
      result = this.getActivitiesForRefundNotice(refundId);
    }

    return result.pipe(map(activities => activities.map(activity => this.processActivity(activity))));
  }

  private getActivitiesForRefundApp(refundId: number): Observable<Activity[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/refund-applications/${refundId}/history`;

    return this.http.get<Activity[]>(baseUrl);
  }

  private getActivitiesForRefundNotice(refundId: number): Observable<Activity[]> {
    const baseUrl = `${appConfiguration.baseApiPath}/refund-management/refund-notices/${refundId}/history`;

    return this.http.get<Activity[]>(baseUrl);
  }

  private processActivity(activity: Activity): Activity {
    const processedActivity = cloneDeep(activity);

    processedActivity.creationDate = new Date(processedActivity.creationDate);

    this.processOriginalRefundDocument(processedActivity);

    return processedActivity;
  }

  private processOriginalRefundDocument(activity: Activity): Activity {
    const documentIdParam = activity.params.find(param => param.name === 'rejectedRefundId');
    const documentNumberParam = activity.params.find(param => param.name === 'rejectedRefundNumber');

    if (documentIdParam && documentNumberParam) {
      const routeToDocument = `${ROUTES.REFUNDS_APP_VIEW.url}/${documentIdParam.value}`;
      activity.type = activity.type + ` <a href="${routeToDocument}">${documentNumberParam.value}</a>`;
    }

    return activity;
  }
}
