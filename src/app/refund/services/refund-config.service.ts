/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, OnDestroy } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { MenuItem } from 'primeng/api';
import { combineLatest, Observable, of, ReplaySubject, Subject, Subscription } from 'rxjs';
import { filter, first, map, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';

import { CardPayment } from '../models/refund-shared.model';
import { AmountTaxViewModel, StatusHeaderViewModel } from '../models/refund-view-aux.model';
import {
  AirlineConfigParamName,
  PaymentMethodsType,
  RefundActionEmitterType,
  RefundActionType,
  RefundAirlineConfiguration,
  RefundBE,
  RefundConfiguration,
  RefundHeaderBtnModel,
  RefundNoticeConfiguration
} from '../models/refund.model';
import { createRefundPermission, modifyRefundPermission } from '../shared/helpers/refund-permissions.config';
import { getRefundForm } from '../store/reducers';
import { RefundFormState } from '../store/reducers/refund-form.reducer';
import { RefundActionsService } from './refund-actions.service';
import { RefundDataService } from './refund-data.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { GLOBALS } from '~app/shared/constants/globals';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { FormUtil } from '~app/shared/helpers';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable()
export class RefundConfigService implements OnDestroy {
  public paymentMethod = new EventEmitter<string>();
  public refundType: MasterDataType;
  public accessType: AccessType;
  public isInvestigate: boolean;
  public isAuthorize: boolean;
  public isSupervise: boolean;
  public isResubmission: boolean;
  public issuingPendingSupervision: boolean;

  public get isRa(): boolean {
    return this.refundType === MasterDataType.RefundApp;
  }

  public get isRn(): boolean {
    return this.refundType === MasterDataType.RefundNotice;
  }

  public get isReadonlyMode(): boolean {
    return this.accessType === AccessType.read;
  }

  public get isCreateMode(): boolean {
    return this.accessType === AccessType.create;
  }

  public get isEditMode(): boolean {
    return this.accessType === AccessType.edit;
  }

  public get isEditTicketMode(): boolean {
    return this.accessType === AccessType.editTicket;
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private get refundForm$(): Observable<RefundFormState> {
    return this.store.pipe(select(getRefundForm));
  }

  private _bspRefundConfiguration = new ReplaySubject<RefundConfiguration>(1);
  public get bspRefundConfiguration(): Observable<RefundConfiguration> {
    if (!this.bspConfig$ || this.bspConfig$.closed) {
      this.fetchAndEmitBspConfig();
    }

    return this._bspRefundConfiguration.asObservable();
  }

  private _bspRefundNoticeConfiguration = new ReplaySubject<RefundNoticeConfiguration>(1);
  public get bspRefundNoticeConfiguration(): Observable<RefundNoticeConfiguration> {
    if (!this.bspRnConfig$ || this.bspRnConfig$.closed) {
      this.fetchAndEmitBspRNConfig();
    }

    return this._bspRefundNoticeConfiguration.asObservable();
  }

  private _airlineRefundConfiguration = new ReplaySubject<RefundAirlineConfiguration>(1);
  public get airlineRefundConfiguration(): Observable<RefundAirlineConfiguration> {
    if (!this.airlineConfig$ || this.airlineConfig$.closed) {
      this.fetchAndEmitAirlineConfig();
    }

    return this._airlineRefundConfiguration.asObservable();
  }

  private destroy$ = new Subject();
  private bspConfig$: Subscription;
  private bspRnConfig$: Subscription;
  private airlineConfig$: Subscription;
  private paymentMethodTypesMapped = {
    CARD1: 'card',
    CARD2: 'card',
    EASYPAY: 'card',
    MISCELLANEOUS: 'card',
    cash: 'cash',
    miscCash: 'cash'
  };

  private penaltyFieldsParams: { [key in keyof AmountTaxViewModel]?: AirlineConfigParamName } = {
    cancellationPenalty: AirlineConfigParamName.penaltyCharge,
    taxOnCancellationPenalty: AirlineConfigParamName.penaltyCharge,
    miscellaneousFee: AirlineConfigParamName.handlingFee,
    taxOnMiscellaneousFee: AirlineConfigParamName.handlingFee,
    commissionForCpAndMf: AirlineConfigParamName.comissionOnCp
  };

  constructor(
    private formData: RefundDataService,
    private translationService: L10nTranslationService,
    private http: HttpClient,
    private store: Store<AppState>,
    private refundActionsService: RefundActionsService
  ) {}

  public getStat(): Observable<Array<DropdownOption & { selected: boolean }> | string> {
    return this.bspRefundConfiguration.pipe(
      map(bspConfig => {
        if (bspConfig.freeStat) {
          //* If freeStat, defaultStat will be returned to populate the input
          return bspConfig.defaultStat;
        } else {
          const defaultStats = this.getDefaultStats() as Array<DropdownOption & { selected: boolean }>;

          //* If not freeStat, we set the selected property on every option before returning the stats
          defaultStats.forEach(stat => (stat.selected = bspConfig.defaultStat === stat.value));

          return defaultStats;
        }
      })
    );
  }

  public getHeaderStatusFields(): Array<keyof StatusHeaderViewModel> {
    return ['docNumber', 'status', 'bspName', 'airlCode', 'agentCode', 'appDate', 'amount'];
  }

  public getDecimalsPattern(): RegExp {
    return GLOBALS.PATTERNS.DECIMALS_PATTERN;
  }

  public getRatesPattern(): RegExp {
    return GLOBALS.PATTERNS.RATE;
  }

  public getPaymentMainBtnAction(): MenuItem {
    return {
      label: this.translationService.translate('REFUNDS.formOfPayment.actionsBtn.main')
    };
  }

  public getDefaultStats(): Array<DropdownOption> {
    return [
      {
        value: 'I',
        label: this.translationService.translate('REFUNDS.basicInfo.statValues.international')
      },
      {
        value: 'D',
        label: this.translationService.translate('REFUNDS.basicInfo.statValues.domestic')
      }
    ];
  }

  public getPaymentMethodOptions(): Observable<ButtonMenuOption[]> {
    const paymentMethodsObject = Object.keys(this.paymentMethodTypesMapped);
    let paymentOptions$: Observable<ButtonMenuOption[]>;
    const paymentOptions: ButtonMenuOption[] = paymentMethodsObject.map(method => this.generateMethodOption(method));

    if (this.isRn) {
      paymentOptions$ = this.bspRefundNoticeConfiguration.pipe(
        map(config => {
          let filteredOptions = paymentOptions;

          if (!config.secondPaymentCardEnabled) {
            filteredOptions = paymentOptions.filter(method => method.id !== PaymentMethodsType.card2);
          }

          return filteredOptions;
        })
      );
    } else {
      paymentOptions$ = of(paymentOptions);
    }

    return paymentOptions$;
  }

  public getHeaderDiscardButton() {
    let btn = {
      label: 'BUTTON.DEFAULT.DISCARD',
      design: ButtonDesign.Tertiary,
      hidden: this.hasUserBtnPermission(RefundActionType.applyChanges),
      disabled: this.isHeaderBtnDisabled(RefundActionType.applyChanges),
      actionType: RefundActionType.applyChanges
    };
    if (this.isInvestigate) {
      btn = {
        label: 'BUTTON.DEFAULT.DISCARD',
        design: ButtonDesign.Tertiary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.investigate
      };
    }
    if (this.isAuthorize) {
      btn = {
        label: 'BUTTON.DEFAULT.DISCARD',
        design: ButtonDesign.Tertiary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.authorize
      };
    }
    if (this.isSupervise) {
      btn = {
        label: 'BUTTON.DEFAULT.DISCARD',
        design: ButtonDesign.Tertiary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.supervise
      };
    }

    return btn;
  }

  public getHeaderBtn(): RefundHeaderBtnModel {
    let btn = {
      label: 'BUTTON.DEFAULT.ISSUE',
      design: ButtonDesign.Primary,
      hidden: this.hasUserBtnPermission(RefundActionType.issue),
      disabled: this.isHeaderBtnDisabled(RefundActionType.issue),
      actionType: RefundActionType.issue
    };
    if (this.isEditMode) {
      btn = {
        label: 'BUTTON.DEFAULT.APPLYCHANGES',
        design: ButtonDesign.Primary,
        hidden: this.hasUserBtnPermission(RefundActionType.applyChanges),
        disabled: this.isHeaderBtnDisabled(RefundActionType.applyChanges),
        actionType: RefundActionType.applyChanges
      };
    }
    if (this.isInvestigate) {
      btn = {
        label: 'BUTTON.DEFAULT.INVESTIGATE',
        design: ButtonDesign.Primary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.investigate
      };
    }
    if (this.isAuthorize) {
      btn = {
        label: 'BUTTON.DEFAULT.AUTHORIZE',
        design: ButtonDesign.Primary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.authorize
      };
    }
    if (this.isSupervise) {
      btn = {
        label: 'BUTTON.DEFAULT.SUPERVISE',
        design: ButtonDesign.Primary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.supervise
      };
    }
    if (this.issuingPendingSupervision) {
      btn = {
        label: 'BUTTON.DEFAULT.ISSUE_PENDING',
        design: ButtonDesign.Primary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.issuePendingSupervision
      };
    }
    if (this.isResubmission) {
      btn = {
        label: 'BUTTON.DEFAULT.RESUBMIT',
        design: ButtonDesign.Primary,
        hidden: of(false),
        disabled: of(false),
        actionType: RefundActionType.resubmit
      };
    }

    return btn;
  }

  public formatHeaderBtnOptions(
    actions: { action: GridTableActionType; disabled?: boolean }[],
    rfnd: RefundBE
  ): ButtonMenuOption[] & MenuItem[] {
    return actions
      .filter(
        action => action.action !== GridTableActionType.Details && action.action !== GridTableActionType.InternalComment
      )
      .map(act => ({
        label: this.translationService.translate(act.action.toString()),
        isDisabled: act.disabled,
        disabled: act.disabled,
        command: this.refundActionsService.getActionCommand(act.action, rfnd, this.refundType)
      }));
  }

  public getAmountFieldKeys(): Array<keyof AmountTaxViewModel> {
    return [
      'grossFare',
      'lessGrossFareUsed',
      'totalGrossFare',
      'supplementaryCommission',
      'cancellationPenalty',
      'taxOnCancellationPenalty',
      'miscellaneousFee',
      'taxOnMiscellaneousFee',
      'totalAmount'
    ];
  }

  public get sortCardPayment() {
    const cardPaymentMethodsOrdered = [
      PaymentMethodsType.card1,
      PaymentMethodsType.card2,
      PaymentMethodsType.easypay,
      PaymentMethodsType.miscellaneous
    ];
    const getPaymentType = (paymentGroup: FormGroup) =>
      FormUtil.get<CardPayment>(paymentGroup, 'payment').value as PaymentMethodsType;

    return (a: AbstractControl, b: AbstractControl) =>
      cardPaymentMethodsOrdered.indexOf(getPaymentType(a as FormGroup)) -
      cardPaymentMethodsOrdered.indexOf(getPaymentType(b as FormGroup));
  }

  public isAgentVatFieldVisible(agentVatNumber: string): Observable<boolean> {
    return this.areVatFieldsVisible(agentVatNumber, null).pipe(map(vatFields => vatFields[0]));
  }

  public isAirlineVatFieldVisible(airlineVatNumber: string): Observable<boolean> {
    return this.areVatFieldsVisible(null, airlineVatNumber).pipe(map(vatFields => vatFields[1]));
  }

  public isContactFieldVisible(contactValue): Observable<boolean> {
    let isContactFieldVisible = !this.isRn;

    let visible$ = of(isContactFieldVisible);

    if (this.isRn) {
      visible$ = this.bspRefundNoticeConfiguration.pipe(
        map(config => {
          if (this.isReadonlyMode) {
            isContactFieldVisible = config.contactEnabled && !!contactValue;
          } else {
            isContactFieldVisible = config.contactEnabled;
          }

          return isContactFieldVisible;
        })
      );
    }

    return visible$;
  }

  public isCustomerReferenceVisible(customerReference): Observable<boolean> {
    let isCustomerRefVisible = !this.isRn;
    let visible$ = of(isCustomerRefVisible);

    if (this.isRn) {
      visible$ = this.bspRefundNoticeConfiguration.pipe(
        map(config => {
          if (this.isReadonlyMode || this.isEditMode) {
            isCustomerRefVisible = !!customerReference || config.customerFileReferenceEnabled;
          }
          if (this.isCreateMode || this.isEditTicketMode) {
            isCustomerRefVisible = config.customerFileReferenceEnabled;
          }

          return isCustomerRefVisible;
        })
      );
    }

    return visible$;
  }

  public iseTicketCodeVisible(eTicketCode?): Observable<boolean> {
    let iseTicketCodeVisible = !this.isRn;

    let visible$ = of(iseTicketCodeVisible);

    if (this.isRn) {
      visible$ = this.bspRefundNoticeConfiguration.pipe(
        map(config => {
          if (this.isReadonlyMode || this.isEditMode) {
            iseTicketCodeVisible = !!eTicketCode || config.settlementAuthorisationCodeEnabled;
          }
          if (this.isCreateMode || this.isEditTicketMode) {
            iseTicketCodeVisible = config.settlementAuthorisationCodeEnabled;
          }

          return iseTicketCodeVisible;
        })
      );
    }

    return visible$;
  }

  public getFareAdjustmentAmount(): Observable<boolean> {
    return this.bspRefundConfiguration.pipe(
      first(),
      map(config => config.fareAdjustmentAmount)
    );
  }

  public getMfFeeAmount(): Observable<number> {
    return this.bspRefundConfiguration.pipe(
      first(),
      map(config => config.mfFeeAmount)
    );
  }

  public getNRRefundsPermitted(): Observable<boolean> {
    return this.bspRefundConfiguration.pipe(
      first(),
      map(config => config.nrRefundsPermitted)
    );
  }

  public resetData(): void {
    this.destroy$.next();
    this.initializeDataSubjects();
  }

  public isPenaltyAmountFieldVisible(field: keyof AmountTaxViewModel, amount?: string): Observable<boolean> {
    return this.airlineRefundConfiguration.pipe(
      startWith(false),
      map((conf: RefundAirlineConfiguration) => {
        if (conf && conf.parameters && !!this.penaltyFieldsParams[field]) {
          const showValue =
            conf.parameters.find(param => param.name === this.penaltyFieldsParams[field]).value === 'true';
          if (this.isCreateMode || this.isEditTicketMode) {
            return showValue;
          } else {
            return showValue || parseInt(amount, 10) !== 0;
          }
        }

        return !this.penaltyFieldsParams[field];
      })
    );
  }

  private generateMethodOption(method: string): ButtonMenuOption {
    return {
      id: method,
      label: this.translationService.translate('REFUNDS.formOfPayment.actionsBtn.' + method),
      command: () => {
        this.paymentMethod.emit(method);
      },
      hidden: method === PaymentMethodsType.card2,
      isDisabled: false,
      group: this.paymentMethodTypesMapped[method]
    };
  }

  // TODO Check if logic for both VatNumbers will always be the same, to simplify this method
  private areVatFieldsVisible(
    agentVatNumber: string | null,
    airlineVatNumber: string | null
  ): Observable<Array<boolean>> {
    if (this.isRa) {
      return this.getVisibleForRa(agentVatNumber, airlineVatNumber);
    }
    if (this.isRn) {
      return this.getVisibleForRn(agentVatNumber, airlineVatNumber);
    }

    return of([false, false]);
  }

  private getVisibleForRa(agentVatNumber: string | null, airlineVatNumber: string | null): Observable<boolean[]> {
    let userType: UserType;

    return this.loggedUser$.pipe(
      tap(user => (userType = user.userType)),
      switchMap(() => this.bspRefundConfiguration),
      map(bspConfig => {
        const agentVatNumOrBspAgentNum = !!agentVatNumber || bspConfig.agentVatNumber;
        const airlineVatNumOrBspAgentNum = !!airlineVatNumber || bspConfig.airlineVatnumber;

        if (
          (userType === UserType.AIRLINE && (this.isInvestigate || this.isAuthorize)) ||
          (userType === UserType.AGENT && this.isEditMode)
        ) {
          return [agentVatNumOrBspAgentNum, airlineVatNumOrBspAgentNum];
        }

        if (userType === UserType.AGENT && (this.isCreateMode || this.isEditTicketMode)) {
          return [bspConfig.agentVatNumber, bspConfig.airlineVatnumber];
        }

        if ((userType === UserType.AGENT || userType === UserType.AIRLINE) && this.isReadonlyMode) {
          // TODO Check if fields are supposed to be shown in blank
          return [agentVatNumOrBspAgentNum, airlineVatNumOrBspAgentNum];
        }

        return [false, false];
      })
    );
  }

  private getVisibleForRn(agentVatNumber: string | null, airlineVatNumber: string | null): Observable<boolean[]> {
    return combineLatest([this.bspRefundConfiguration, this.bspRefundNoticeConfiguration]).pipe(
      map(([refundConfiguration, refundNoticeConfiguration]) => {
        const bspConfAgentVat = refundConfiguration.agentVatNumber;
        const bspConfAirlineVat = refundConfiguration.airlineVatnumber;
        const rnConfVatNumber = refundNoticeConfiguration.vatnumberEnabled;

        const bspAndRnConfAgentVat = bspConfAgentVat && rnConfVatNumber;
        const bspAndRnConfAirlineVat = bspConfAirlineVat && rnConfVatNumber;

        if (this.isEditMode && this.isSupervise) {
          return [bspAndRnConfAgentVat && !!agentVatNumber, bspAndRnConfAirlineVat && !!airlineVatNumber];
        }

        if (this.isReadonlyMode || this.isEditMode) {
          return [!!agentVatNumber || bspAndRnConfAgentVat, !!airlineVatNumber || bspAndRnConfAirlineVat];
        }

        if (this.isCreateMode || this.isEditTicketMode) {
          return [bspAndRnConfAgentVat, bspAndRnConfAirlineVat];
        }

        return [false, false];
      })
    );
  }

  private initializeDataSubjects(): void {
    this._bspRefundConfiguration = new ReplaySubject<RefundConfiguration>(1);
    this._bspRefundNoticeConfiguration = new ReplaySubject<RefundNoticeConfiguration>(1);
  }

  private fetchAndEmitBspConfig(): void {
    this.bspConfig$ = this.formData
      .getSubject(RefundActionEmitterType.bsp)
      .pipe(
        filter(bsp => bsp),
        switchMap(bsp => {
          const baseUrl = `${appConfiguration.baseApiPath}/refund-management/bsps/${bsp.id}/refund-configuration`;

          return this.http.get<Array<RefundConfiguration>>(baseUrl).pipe(map(configArray => configArray[0]));
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(config => this._bspRefundConfiguration.next(config));
  }

  private fetchAndEmitBspRNConfig(): void {
    this.bspRnConfig$ = this.formData
      .getSubject(RefundActionEmitterType.bsp)
      .pipe(
        filter(bsp => bsp && this.isRn),
        switchMap(bsp => {
          const baseUrl = `${appConfiguration.baseApiPath}/refund-management/bsps/${bsp.id}/refund-notice-configuration`;

          return this.http.get<RefundNoticeConfiguration>(baseUrl);
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(config => this._bspRefundNoticeConfiguration.next(config));
  }

  private fetchAndEmitAirlineConfig() {
    this.airlineConfig$ = this.formData
      .getSubject(RefundActionEmitterType.airline)
      .pipe(
        filter(airline => !!airline),
        switchMap(airline => {
          const baseUrl = `${appConfiguration.baseApiPath}/refund-management/airlines/${airline.id}/configuration`;

          return this.http.get<RefundAirlineConfiguration>(baseUrl);
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(config => this._airlineRefundConfiguration.next(config));
  }

  private hasUserBtnPermission(action: RefundActionType) {
    return this.loggedUser$.pipe(
      map(user => {
        switch (action) {
          case RefundActionType.issue:
            return (
              (this.isRa && !user.permissions.includes(createRefundPermission.raIssue)) ||
              (this.isRn && !user.permissions.includes(createRefundPermission.rnIssue))
            );

          case RefundActionType.edit:
          case RefundActionType.applyChanges:
            return (
              (this.isRa && !user.permissions.includes(modifyRefundPermission.raModify)) ||
              (this.isRn && !user.permissions.includes(modifyRefundPermission.rnModify))
            );
        }
      })
    );
  }

  private isHeaderBtnDisabled(action: RefundActionType) {
    return this.refundForm$.pipe(map(form => action === RefundActionType.applyChanges && !form.hasChanges));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
