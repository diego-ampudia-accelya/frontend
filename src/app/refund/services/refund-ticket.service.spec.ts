import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { of } from 'rxjs';

import { CardPayment, OriginalIssue, RelatedTicketDocument } from '../models/refund-shared.model';
import {
  AmountTaxViewModel,
  BasicInformationViewModel,
  DetailViewModel,
  FormOfPaymentViewModel,
  StatusHeaderViewModel,
  StatusViewModel
} from '../models/refund-view-aux.model';
import { PaymentMethodsType, RefundViewModel } from '../models/refund.model';
import { RefundConfigService } from './refund-config.service';
import { RefundDataService } from './refund-data.service';
import { RefundTicketService } from './refund-ticket.service';
import { TicketNetReportingIndicator } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { FormOfPaymentType } from '~app/document/enums';
import { SummaryTicket } from '~app/document/models/summary-ticket.model';
import { DocumentService } from '~app/document/services/document.service';
import { FormUtil } from '~app/shared/helpers';
import { TabService } from '~app/shared/services';

const mockAgent = {};

const initialState = {
  auth: {
    user: mockAgent
  },
  core: {
    menu: {
      tabs: {}
    }
  },
  refund: {}
};

const mockSummary: SummaryTicket = {
  id: '1234567890-2025-SUxfTUlTWUlOREhPVF8yMDIwMDIxNS56aXA=',
  bsp: { id: 1234, isoCountryCode: 'ES', name: 'SPAIN' },
  airline: {
    id: 1234987654,
    localName: 'Airline',
    vatNumber: '560010779',
    globalAirline: { id: 987654, iataCode: '001', globalName: 'Airlines, Inc.' },
    bsp: {
      id: 1234,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    }
  },
  agent: { id: 12341234567, iataCode: '1234567', name: 'Agent', vatNumber: '557611977' },
  currency: null,
  documentNumber: '1234567890',
  fullDocumentNumber: '0011234567890',
  transactionCode: 'TKTT',
  dateOfIssue: '2020-02-15',
  couponUseIndicator: 'FSFF',
  passengerName: 'XXXXXXXXXXXXX',
  ticketAmount: 998.61,
  taxes: [
    { type: 'IL', amount: 27.26 },
    { type: 'FR', amount: 5.1 },
    { type: 'QX', amount: 16.5 },
    { type: 'AY', amount: 11.2 },
    { type: 'US', amount: 37.8 },
    { type: 'XA', amount: 3.96 },
    { type: 'XY', amount: 7.0 },
    { type: 'YC', amount: 5.89 },
    { type: 'YR', amount: 269.49 },
    { type: 'XF', amount: 613.79 },
    { type: 'XFJFK4.5', amount: 0.0 }
  ],
  totalPaymentAmount: 998.61,
  sumCommissionAmount: -43.97,
  sumSupplementaryAmount: 0.0,
  vatGstAmount: 0.0,
  formOfPayment: [{ type: 'CA', amount: 998.61 }],
  statisticalCode: 'I',
  conjunctionTickets: [
    { documentNumber: '1234567891', fullDocumentNumber: '0011234567891', couponUseIndicator: 'SFVV' }
  ]
};

describe('RefundTicketService', () => {
  let service: RefundTicketService;

  const spyDocumentService: SpyObject<DocumentService> = createSpyObject(DocumentService);
  const translationSpy = createSpyObject(L10nTranslationService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        RefundTicketService,
        { provide: DocumentService, useValue: spyDocumentService },
        { provide: L10nTranslationService, useValue: translationSpy },
        RefundDataService,
        RefundConfigService,
        FormBuilder,
        HttpTestingController,
        HttpClient,
        provideMockStore({ initialState }),
        TabService
      ]
    });
  }));

  beforeEach(done => {
    service = TestBed.inject(RefundTicketService);

    service['formData'].whenFormIsReady().subscribe(done);

    const builder = TestBed.inject(FormBuilder);
    const formUtil = new FormUtil(builder);

    const statusHeaderStatus = formUtil.createGroup<StatusViewModel>({
      status: '',
      sentToDpc: null
    });

    const statusHeader = formUtil.createGroup<StatusHeaderViewModel>({
      agentCode: '',
      airlCode: '',
      status: statusHeaderStatus,
      amount: '',
      appDate: '',
      bspName: '',
      docNumber: '',
      finalType: null,
      finalDaysLeft: null,
      finalProcessStopped: null
    });

    const basicInformation = formUtil.createGroup<BasicInformationViewModel>({
      agent: null,
      airline: null,
      agentContact: null,
      airlineContact: null
    });

    const details = formUtil.createGroup<DetailViewModel>({
      airlineCodeRelatedDocument: '',
      dateOfIssueRelatedDocument: '',
      originalIssue: null,
      passenger: '',
      reasonForRefund: '',
      relatedTicketConjunctions: null,
      relatedTicketDocument: null,
      relatedTicketExchange: null,
      rejectedDocumentId: '',
      resubmissionRemark: '',
      statisticalCode: '',
      waiverCode: ''
    });

    const formPayment = formUtil.createGroup<FormOfPaymentViewModel>({
      cardPayments: null,
      cardTotal: '',
      cashPaymentAmount: '',
      cashTotal: '',
      currency: null,
      customerReference: '',
      eTicketAuthCode: '',
      miscellaneousCashAmount: '',
      totalAmount: '',
      tourCode: ''
    });

    const amounts = formUtil.createGroup<AmountTaxViewModel>({
      cancellationPenalty: null,
      commission: null,
      commissionForCpAndMf: null,
      grossFare: null,
      lessGrossFareUsed: null,
      miscellaneousFee: null,
      netReporting: null,
      partialRefund: null,
      supplementaryCommission: null,
      taxMiscellaneousFees: null,
      taxOnCancellationPenalty: null,
      taxOnMiscellaneousFee: null,
      totalAmount: null,
      totalGrossFare: null
    });

    service['formData']['form'] = formUtil.createGroup<RefundViewModel>({
      statusHeader,
      basicInformation,
      details,
      formPayment,
      amounts
    });

    service['formData'].isFormReady.next(true);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should updateSummaryForm', () => {
    spyDocumentService.getDocumentSummary.and.returnValue(of(mockSummary));
    service.updateSummaryForm(mockSummary.id);
    expect(service.ticketSummary).toEqual(mockSummary);
  });

  it('should return airlineCode of RelatedDocumentTicket', () => {
    service.ticketSummary = cloneDeep(mockSummary);
    expect(service.airlineCodeRelatedDocumentTicket).toEqual(mockSummary.airline.globalAirline.iataCode);
  });

  it('should return empty array if there are not conjunctionTickets', () => {
    service.ticketSummary = { ...cloneDeep(mockSummary), conjunctionTickets: null };
    expect(service.conjunctionDocumentTicket).toEqual([]);
  });

  it('should return formatted documents if conjunctionTickets are defined', () => {
    const expectedResult: RelatedTicketDocument[] = [
      {
        documentNumber: mockSummary.conjunctionTickets[0].documentNumber,
        documentId: null,
        coupon1: mockSummary.conjunctionTickets[0].couponUseIndicator.substring(0, 1) === 'F',
        coupon2: mockSummary.conjunctionTickets[0].couponUseIndicator.substring(1, 2) === 'F',
        coupon3: mockSummary.conjunctionTickets[0].couponUseIndicator.substring(2, 3) === 'F',
        coupon4: mockSummary.conjunctionTickets[0].couponUseIndicator.substring(3, 4) === 'F'
      }
    ];
    service.ticketSummary = cloneDeep(mockSummary);

    expect(service.conjunctionDocumentTicket).toEqual(expectedResult);
  });

  it('should return documentCouponsTicket', () => {
    const expectedResult: RelatedTicketDocument = {
      documentNumber: mockSummary.documentNumber,
      documentId: mockSummary.id,
      coupon1: mockSummary.couponUseIndicator.substring(0, 1) === 'F',
      coupon2: mockSummary.couponUseIndicator.substring(1, 2) === 'F',
      coupon3: mockSummary.couponUseIndicator.substring(2, 3) === 'F',
      coupon4: mockSummary.couponUseIndicator.substring(3, 4) === 'F'
    };
    service.ticketSummary = cloneDeep(mockSummary);

    expect(service.documentCouponsTicket).toEqual(expectedResult);
  });

  it('should return false if summary is not a exchange ticket', () => {
    service.ticketSummary = cloneDeep(mockSummary);

    expect(service.exchangeTicket).toBeFalsy();
  });

  it('should return true if summary is a exchange ticket', () => {
    service.ticketSummary = { ...cloneDeep(mockSummary), formOfPayment: [{ type: 'EX', amount: 998.61 }] };

    expect(service.exchangeTicket).toBeTruthy();
  });

  it('should return originalIssueTicket', () => {
    const expectedResult: OriginalIssue = {
      agentCode: '1234567',
      locationCode: 'ES',
      issueDate: '12-03-2020',
      documentNumber: '1231231231',
      airlineCode: '001'
    };
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      originalIssueDetails: {
        originalIssueAgentCode: '1234567',
        originalIssueCityCode: 'ES',
        originalIssueDate: '12-03-2020',
        originalIssueDocumentNumber: '0011231231231',
        originalIssueAirlineCode: '001'
      }
    };

    expect(service.originalIssueTicket).toEqual(expectedResult);
  });

  it('should return originalIssueTicket if airline code is not defined', () => {
    const expectedResult: OriginalIssue = {
      agentCode: '1234567',
      locationCode: 'ES',
      issueDate: '12-03-2020',
      documentNumber: '1231231231',
      airlineCode: '001'
    };
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      originalIssueDetails: {
        originalIssueAgentCode: '1234567',
        originalIssueCityCode: 'ES',
        originalIssueDate: '12-03-2020',
        originalIssueDocumentNumber: '0011231231231'
      }
    };

    expect(service.originalIssueTicket).toEqual(expectedResult);
  });

  it('should return currency when it is defined', () => {
    const expectedResult = { id: 858368, code: 'USD', decimals: 2, isDefault: true };
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      currency: { id: 858368, code: 'USD', decimals: 2, isDefault: true }
    };

    expect(service.currencyTicket).toEqual(expectedResult);
  });

  it('should return null when currency is not defined', () => {
    service.ticketSummary = cloneDeep(mockSummary);

    expect(service.currencyTicket).toBeNull();
  });

  it('should return empty string when no CC or MSCC form of payments are defined', () => {
    service.ticketSummary = cloneDeep(mockSummary);

    expect(service.customerReferenceTicket).toEqual('');
  });

  it('should return empty string when CC or MSCC form of payments are defined and no customerReferenceTicket is defined', () => {
    service.ticketSummary = { ...cloneDeep(mockSummary), formOfPayment: [{ type: 'CC', amount: 20 }] };

    expect(service.customerReferenceTicket).toBeUndefined();
  });

  it('should return customerReferenceTicket when CC or MSCC form of payments and customerReferenceTicket are defined', () => {
    const expectedResult = 'testRef';
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      formOfPayment: [{ type: 'CC', amount: 20 }],
      customerFileReference: 'testRef'
    };

    expect(service.customerReferenceTicket).toEqual(expectedResult);
  });

  it('should return array of null when card payment is not defined', () => {
    service.ticketSummary = cloneDeep(mockSummary);
    expect(service.creditCardPaymentTicket).toEqual([null, null]);
  });

  it('should return creditCardPaymentTicket when CC payment is defined', () => {
    const expectedResult: CardPayment[] = [
      {
        type: FormOfPaymentType.CC,
        amount: 12,
        card: '123131',
        payment: PaymentMethodsType.card1,
        cardMask: '1231231'
      },
      null,
      null
    ];
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      formOfPayment: [
        {
          type: FormOfPaymentType.CC,
          creditCardEntity: 'CC',
          creditCardNumber: '123131',
          amount: 12,
          maskedCreditCardNumber: '1231231'
        }
      ]
    };
    expect(service.creditCardPaymentTicket).toEqual(expectedResult);
  });

  it('should return creditCardPaymentTicket when two CC payments are defined', () => {
    const expectedResult: CardPayment[] = [
      {
        type: FormOfPaymentType.CC,
        amount: 12,
        card: '123131',
        payment: PaymentMethodsType.card1,
        cardMask: '1231231'
      },
      {
        type: FormOfPaymentType.CC,
        amount: 45,
        card: '456456',
        payment: PaymentMethodsType.card2,
        cardMask: '4654544'
      },
      null,
      null
    ];
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      formOfPayment: [
        {
          type: FormOfPaymentType.CC,
          creditCardEntity: 'CC',
          creditCardNumber: '123131',
          amount: 12,
          maskedCreditCardNumber: '1231231'
        },
        {
          type: FormOfPaymentType.CC,
          creditCardEntity: 'CC',
          creditCardNumber: '456456',
          amount: 45,
          maskedCreditCardNumber: '4654544'
        }
      ]
    };
    expect(service.creditCardPaymentTicket).toEqual(expectedResult);
  });

  it('should return creditCardPaymentTicket when MSCC payment is defined', () => {
    const expectedResult: CardPayment[] = [
      { type: 'MSCC', amount: 12, card: '123131', payment: PaymentMethodsType.miscellaneous, cardMask: '1231231' },
      null
    ];
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      formOfPayment: [
        {
          type: FormOfPaymentType.MSCC,
          creditCardEntity: FormOfPaymentType.MSCC,
          creditCardNumber: '123131',
          amount: 12,
          maskedCreditCardNumber: '1231231'
        }
      ]
    };
    expect(service.creditCardPaymentTicket).toEqual(expectedResult);
  });

  it('should return creditCardPaymentTicket when Easy Pay payment is defined', () => {
    const expectedResult: CardPayment[] = [
      null,
      { type: 'EP', amount: 12, card: '123131', payment: PaymentMethodsType.easypay, cardMask: '1231231' }
    ];
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      formOfPayment: [
        {
          type: FormOfPaymentType.EP,
          creditCardEntity: FormOfPaymentType.EP,
          creditCardNumber: '123131',
          amount: 12,
          maskedCreditCardNumber: '1231231'
        }
      ]
    };
    expect(service.creditCardPaymentTicket).toEqual(expectedResult);
  });

  it('should return cashPaymentTicket amount when cash payment is defined', () => {
    service.ticketSummary = cloneDeep(mockSummary);
    expect(service.cashPaymentTicket).toEqual(998.61);
  });

  it('should cashPaymentTicket 0 when cash payment is not defined', () => {
    service.ticketSummary = { ...cloneDeep(mockSummary), formOfPayment: [] };
    expect(service.cashPaymentTicket).toEqual(0);
  });

  it('should miscCashPaymentTicket amount when miscellaneous cash payment is defined', () => {
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      formOfPayment: [
        {
          type: FormOfPaymentType.MSCA,
          amount: 12
        }
      ]
    };
    expect(service.miscCashPaymentTicket).toEqual(12);
  });

  it('should miscCashPaymentTicket 0 when miscellaneous cash payment is not defined', () => {
    service.ticketSummary = { ...cloneDeep(mockSummary), formOfPayment: [] };
    expect(service.miscCashPaymentTicket).toEqual(0);
  });

  it('should netReportingTicket to be true if summary has net reporting indicator and sum supplementary amount', () => {
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      netReportingIndicator: TicketNetReportingIndicator.NR,
      sumSupplementaryAmount: 12
    };
    expect(service.netReportingTicket).toBeTruthy();
  });

  it('should netReportingTicket to be true if summary has net reporting indicator but not sum supplementary amount', () => {
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      netReportingIndicator: TicketNetReportingIndicator.NR
    };
    expect(service.netReportingTicket).toBeTruthy();
  });

  it('should netReportingTicket to be true if summary has sum supplementary amount net but not reporting indicator', () => {
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      sumSupplementaryAmount: 32
    };
    expect(service.netReportingTicket).toBeTruthy();
  });

  it('should netReportingTicket to be false if summary has not sum supplementary amount net nor reporting indicator', () => {
    service.ticketSummary = cloneDeep(mockSummary);
    expect(service.netReportingTicket).toBeFalsy();
  });

  it('should return grossfare ', () => {
    service.ticketSummary = cloneDeep(mockSummary);
    expect(service.grossFareTicket).toEqual(0.62);
  });

  it('should return commissionAmountTicket when sumCommissionAmount is different to 0', () => {
    service.ticketSummary = cloneDeep(mockSummary);
    expect(service.commissionAmountTicket).toEqual(43.97);
  });

  it('should return commissionAmountTicket null when sumCommissionAmount is 0', () => {
    service.ticketSummary = { ...cloneDeep(mockSummary), sumCommissionAmount: 0.0 };
    expect(service.commissionAmountTicket).toBeNull();
  });

  it('should return discAmountTicket when sumSupplementaryAmount is different to 0', () => {
    service.ticketSummary = {
      ...cloneDeep(mockSummary),
      sumSupplementaryAmount: 32
    };
    expect(service.discAmountTicket).toEqual(32);
  });

  it('should return discAmountTicket null when sumSupplementaryAmount is 0', () => {
    service.ticketSummary = cloneDeep(mockSummary);
    expect(service.discAmountTicket).toBeNull();
  });
});
