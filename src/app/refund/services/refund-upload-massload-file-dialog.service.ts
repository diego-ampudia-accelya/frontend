import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';

import { RefundUploadMassloadDialogConfig } from '../models/refund-upload-massload-file-dialog-config';
import { RefundUploadMassloadDialogComponent } from '~app/refund/components/refund-files/refund-massload-file-list/refund-upload-massload-file-dialog/refund-upload-massload-file-dialog';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class RefundUploadMassloadFileDialogService {
  constructor(
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private appConfig: AppConfigurationService
  ) {}

  public openMassDeleteFileDialog(actionEmitter: Subject<any>): Observable<any> {
    const title = this.translationService.translate('REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.title');
    const secondaryTittleMass = this.translationService.translate(
      'REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.secondaryTittleMass'
    );
    const secondaryTittleAttach = this.translationService.translate(
      'REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.secondaryTittleAttach'
    );
    const uploadingTitle = this.translationService.translate('MENU.files.uploadFiles.uploading');
    const uploadedTitle = this.translationService.translate('MENU.files.uploadFiles.uploaded');
    const dialogConfig: DialogConfig = {
      data: {
        title,
        actionEmitter,
        footerButtonsType: [{ type: FooterButton.Upload, isDisabled: true }],
        uploadingTitle,
        uploadedTitle,
        filesToMass: [],
        filesToAttach: [],
        secondaryTittleMass,
        secondaryTittleAttach,
        uploadPath: `${this.appConfig.baseUploadPath}/refund-management/files`,
        isInWideMode: true
      } as RefundUploadMassloadDialogConfig
    };

    return this.dialogService.open(RefundUploadMassloadDialogComponent, dialogConfig);
  }
}
