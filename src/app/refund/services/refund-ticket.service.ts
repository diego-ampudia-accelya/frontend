import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

import { CardPayment, OriginalIssue, RelatedTicketDocument } from '../models/refund-shared.model';
import { CurrencyViewModel } from '../models/refund-view-aux.model';
import { PaymentMethodsType, RefundViewModel } from '../models/refund.model';
import { adapterViewTicketFunctionsMapper, fieldMapperRefundTicket } from '../shared/helpers/view-be-mapper.config';
import { RefundDataService } from './refund-data.service';
import { TicketNetReportingIndicator } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { FormOfPaymentType } from '~app/document/enums';
import { FormPaymentTicketModel, SummaryTicket } from '~app/document/models/summary-ticket.model';
import { DocumentService } from '~app/document/services/document.service';
import { buildViewFromBEObject } from '~app/shared/helpers';

@Injectable()
export class RefundTicketService {
  public ticketSummary: SummaryTicket;

  constructor(private docService: DocumentService, private formData: RefundDataService) {}

  public updateSummaryForm(documentId: string) {
    this.formData.isFormReady.next(false);
    this.docService
      .getDocumentSummary(documentId)
      .pipe(take(1))
      .subscribe((summary: SummaryTicket) => {
        this.ticketSummary = summary;

        buildViewFromBEObject<Partial<SummaryTicket>, RefundViewModel>(
          summary,
          this.formData.getForm(),
          false,
          fieldMapperRefundTicket,
          adapterViewTicketFunctionsMapper
        );
        this.formData.isFormReady.next(true);
      });
  }

  public get airlineCodeRelatedDocumentTicket(): string {
    return this.ticketSummary.fullDocumentNumber.slice(0, 3);
  }

  public get conjunctionDocumentTicket(): RelatedTicketDocument[] {
    if (this.ticketSummary.conjunctionTickets) {
      return this.ticketSummary.conjunctionTickets.map(ticket => ({
        ...this.getDocumentsFormatted(ticket.fullDocumentNumber, ticket.couponUseIndicator)
      }));
    }

    return [];
  }

  public get documentCouponsTicket(): RelatedTicketDocument {
    return {
      ...this.getDocumentsFormatted(
        this.ticketSummary.fullDocumentNumber,
        this.ticketSummary.couponUseIndicator,
        this.ticketSummary.id
      )
    };
  }

  public get exchangeTicket(): boolean {
    return !!this.ticketSummary.formOfPayment.find(form => form.type === FormOfPaymentType.EX);
  }

  public get originalIssueTicket(): OriginalIssue {
    return {
      agentCode: this.ticketSummary.originalIssueDetails.originalIssueAgentCode,
      airlineCode:
        this.ticketSummary.originalIssueDetails.originalIssueAirlineCode ||
        this.ticketSummary.originalIssueDetails.originalIssueDocumentNumber.slice(0, 3),
      documentNumber: this.ticketSummary.originalIssueDetails.originalIssueDocumentNumber.slice(3),
      issueDate: this.ticketSummary.originalIssueDetails.originalIssueDate,
      locationCode: this.ticketSummary.originalIssueDetails.originalIssueCityCode
    };
  }

  public get currencyTicket(): CurrencyViewModel {
    return this.ticketSummary ? this.ticketSummary.currency : null;
  }

  public get customerReferenceTicket(): string {
    let customerRefValue = '';

    const cardPayment = this.ticketSummary.formOfPayment.find(
      formPayment =>
        formPayment.type.startsWith(FormOfPaymentType.CC) || formPayment.type.startsWith(FormOfPaymentType.MSCC)
    );
    if (cardPayment) {
      customerRefValue = this.ticketSummary.customerFileReference;
    }

    return customerRefValue;
  }

  public get creditCardPaymentTicket(): CardPayment[] {
    const creditCardTicketList: CardPayment[] = [];
    const creditCardFiltered = this.ticketSummary.formOfPayment.filter(payment =>
      payment.type.startsWith(FormOfPaymentType.CC)
    );
    const miscCard = this.ticketSummary.formOfPayment.find(payment => payment.type.startsWith(FormOfPaymentType.MSCC));
    const easyCard = this.ticketSummary.formOfPayment.find(payment => payment.type.startsWith(FormOfPaymentType.EP));

    creditCardFiltered.forEach((card, i) => {
      creditCardTicketList.push(
        this.createPaymentObject(card, i === 0 ? PaymentMethodsType.card1 : PaymentMethodsType.card2)
      );
    });
    creditCardTicketList.push(this.createPaymentObject(miscCard, PaymentMethodsType.miscellaneous));
    creditCardTicketList.push(this.createPaymentObject(easyCard, PaymentMethodsType.easypay));

    return creditCardTicketList;
  }

  public get cashPaymentTicket(): number {
    const payment = this.ticketSummary.formOfPayment.find(pay => pay.type.startsWith(FormOfPaymentType.CA));

    return payment ? payment.amount : 0;
  }

  public get miscCashPaymentTicket(): number {
    const payment = this.ticketSummary.formOfPayment.find(pay => pay.type.startsWith(FormOfPaymentType.MSCA));

    return payment ? payment.amount : 0;
  }

  public get netReportingTicket(): boolean {
    return (
      this.ticketSummary.netReportingIndicator === TicketNetReportingIndicator.NR ||
      this.ticketSummary.sumSupplementaryAmount > 0
    );
  }

  public get grossFareTicket(): number {
    const decimal = this.ticketSummary.currency?.decimals || 2;
    let result = this.ticketSummary.ticketAmount;
    this.ticketSummary.taxes.forEach(tax => {
      result = +result.toFixed(decimal) - tax.amount;
    });

    return +result.toFixed(decimal) || null;
  }

  public get commissionAmountTicket(): number {
    return this.ticketSummary.sumCommissionAmount !== 0 ? Math.abs(this.ticketSummary.sumCommissionAmount) : null;
  }

  public get discAmountTicket(): number {
    return this.ticketSummary.sumSupplementaryAmount !== 0 ? Math.abs(this.ticketSummary.sumSupplementaryAmount) : null;
  }

  private createPaymentObject(payment: FormPaymentTicketModel, paymentLbl: PaymentMethodsType): CardPayment {
    return payment
      ? {
          amount: payment.amount,
          type: payment.creditCardEntity,
          card: payment.creditCardNumber,
          cardMask: payment.maskedCreditCardNumber,
          payment: paymentLbl
        }
      : null;
  }

  private getDocumentsFormatted(docNum: string, couponUseIndicator = '', docId?: string): RelatedTicketDocument {
    return {
      documentNumber: docNum.slice(3),
      documentId: docId ? docId : null,
      coupon1: couponUseIndicator.substring(0, 1) === 'F',
      coupon2: couponUseIndicator.substring(1, 2) === 'F',
      coupon3: couponUseIndicator.substring(2, 3) === 'F',
      coupon4: couponUseIndicator.substring(3, 4) === 'F'
    };
  }
}
