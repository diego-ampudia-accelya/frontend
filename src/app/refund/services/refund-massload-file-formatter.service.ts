import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { RefundMassloadFileFilter } from '../models/refund-massload-file-filter.model';
import { RefundMassloadFileStatus } from '../models/refund-massload-file.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { rangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class RefundMassloadFileFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: Partial<RefundMassloadFileFilter>): AppliedFilter[] {
    const filterMappers: FilterMappers<RefundMassloadFileFilter> = {
      originalFileName: originalFileName => `${this.translate('originalFileName')} - ${originalFileName}`,
      filename: filename => `${this.translate('filename')} - ${filename}`,
      userName: userName => `${this.translate('userName')} - ${userName}`,
      email: email => `${this.translate('email')} - ${email}`,
      status: status => `${this.translate('status')} - ${this.statusMapper(status)}`,
      uploadDate: date => `${this.translate('uploadDate')} - ${rangeDateFilterTagMapper(date)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`REFUNDS.files.massload.filters.${key}.label`);
  }

  private statusMapper(statuses: RefundMassloadFileStatus[]): string {
    return statuses
      .map(status => this.translationService.translate(`REFUNDS.files.massload.status.${status.toLowerCase()}`))
      .join(', ');
  }
}
