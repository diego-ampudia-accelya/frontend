import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';
import { getMenuTabs } from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { TabService } from '~app/shared/services';
import { RefundStatus } from '../models/refund-be-aux.model';
import { RefundBE } from '../models/refund.model';
import { RefundDialogService } from '../services/refund-dialog.service';
import { RefundService } from '../services/refund.service';
import { getRefundDeleteQueryUrl } from '../shared/helpers/refund.helper';

@Injectable()
export class RefundItemResolver implements Resolve<RefundBE> {
  constructor(
    private http: HttpClient,
    private tabService: TabService,
    private refundDialogService: RefundDialogService,
    private store: Store<AppState>,
    private router: Router
  ) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<RefundBE> {
    const refundService = new RefundService(this.http, null, activatedRouteSnapshot);
    const refundType = activatedRouteSnapshot.data.refundType;

    return refundService.getOne(activatedRouteSnapshot.params['id']).pipe(
      switchMap(item => combineLatest([of(item), this.store.pipe(select(getMenuTabs), first())])),
      tap(([item, tabs]) => {
        // We check here if the Refund document is deleted
        if (item.status === RefundStatus.deleted) {
          const tabToCl = tabs.find(tab => tab.label === activatedRouteSnapshot.data.tab.label);
          if (tabToCl) {
            this.tabService.closeTab(tabToCl);
          }
          const url = getRefundDeleteQueryUrl(refundType);
          this.router.navigateByUrl(url);
          this.refundDialogService.openDialogNotFoundDialogComponent();
        }
      }),
      map(([item, _]) => item)
    );
  }
}
