import { createAction, props } from '@ngrx/store';

import { ContactViewModel } from '~app/refund/models/refund-view-aux.model';

export const updateAgentContactData = createAction(
  '[Refund] Update Agent Contact Data',
  props<{ payload: ContactViewModel }>()
);

export const resetAgentContactData = createAction('[Refund] Reset Agent Contact Data');

export const updateAirlineContactData = createAction(
  '[Refund] Update Airline Contact Data',
  props<{ payload: ContactViewModel }>()
);

export const resetAirlineContactData = createAction('[Refund] Reset Airline Contact Data');
