import { createAction, props } from '@ngrx/store';

export const saveResubmissionRemark = createAction(
  '[Refund Resubmission] Save Resubmission Remark',
  props<{ refundNumber: string; resubmissionRemark: string }>()
);
