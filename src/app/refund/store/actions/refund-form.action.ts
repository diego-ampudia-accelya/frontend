/* eslint-disable @typescript-eslint/no-shadow */
import { Action } from '@ngrx/store';

import { RefundViewModel } from '~app/refund/models/refund.model';

export enum Types {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  ModifyRefund = '[Refund] Modify'
}

export class ModifyRefund implements Action {
  readonly type = Types.ModifyRefund;
  constructor(public payload: RefundViewModel) {}
}

export type Actions = ModifyRefund;
