import { Injectable } from '@angular/core';
import { createEffect } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { RefundContactActions } from '../actions';
import { contactInfoCacheBuster$ } from '~app/shared/helpers/cacheable-helper';

@Injectable()
export class RefundContactEffects {
  public resetContactInfo$ = createEffect(() =>
    contactInfoCacheBuster$.pipe(
      switchMap(() => [RefundContactActions.resetAgentContactData(), RefundContactActions.resetAirlineContactData()])
    )
  );
}
