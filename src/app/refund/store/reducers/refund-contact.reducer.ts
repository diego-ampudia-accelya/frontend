import { createReducer, on } from '@ngrx/store';

import { RefundContactActions } from '../actions';
import { ContactViewModel } from '~app/refund/models/refund-view-aux.model';

const initialState: ContactViewModel = null;

export const reducerAgentContact = createReducer(
  initialState,
  on(RefundContactActions.resetAgentContactData, state => ({ ...initialState })),
  on(RefundContactActions.updateAgentContactData, (state, { payload }) => ({ ...payload }))
);

export const reducerAirlineContact = createReducer(
  initialState,
  on(RefundContactActions.resetAirlineContactData, state => ({ ...initialState })),
  on(RefundContactActions.updateAirlineContactData, (state, { payload }) => ({ ...payload }))
);
