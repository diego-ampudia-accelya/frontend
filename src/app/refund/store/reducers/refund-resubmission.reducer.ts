import { createReducer, on } from '@ngrx/store';

import * as RefundResubmissionActions from '../actions/refund-resubmission.actions';

export interface ResubmissionRemark {
  refundNumber: string;
  resubmissionRemark: string;
}

export interface RefundResubmissionState {
  resubmissionRemarks: Array<ResubmissionRemark>;
}

const initialState = {
  resubmissionRemarks: []
};

export const reducer = createReducer(
  initialState,
  on(RefundResubmissionActions.saveResubmissionRemark, (state, { refundNumber, resubmissionRemark }) => {
    const filteredRecords = state.resubmissionRemarks.filter(record => record.refundNumber !== refundNumber);

    return { resubmissionRemarks: [...filteredRecords, { refundNumber, resubmissionRemark }] };
  })
);
