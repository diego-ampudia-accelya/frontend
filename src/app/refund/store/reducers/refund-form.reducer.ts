import { isEqual } from 'lodash';

import { RefundFormActions } from '../actions';
import { RefundViewModel } from '~app/refund/models/refund.model';

export interface RefundFormState {
  originalValue: RefundViewModel;
  value: RefundViewModel;
  hasChanges: boolean;
}

const initialState: RefundFormState = {
  originalValue: null,
  value: null,
  hasChanges: false
};

export function reducer(state = initialState, action: RefundFormActions.Actions): RefundFormState {
  if (action.type === RefundFormActions.Types.ModifyRefund) {
    return {
      ...state,
      originalValue: isEqual(state.originalValue, null) ? { ...action.payload } : { ...state.originalValue },
      value: { ...action.payload },
      hasChanges: !isEqual(state.originalValue, { ...action.payload })
    };
  }

  return state;
}
