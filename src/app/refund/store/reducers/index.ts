import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromRefundContact from './refund-contact.reducer';
import * as fromRefundForm from './refund-form.reducer';
import * as fromRefundResubmission from './refund-resubmission.reducer';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { RefundFilter } from '~app/refund/models/refund-filter.model';
import { RefundMassloadAttachmentFilter } from '~app/refund/models/refund-massload-attachment-filter.model';
import { RefundMassloadAttachment } from '~app/refund/models/refund-massload-attachment.model';
import { RefundMassloadFileFilter } from '~app/refund/models/refund-massload-file-filter.model';
import { RefundMassloadFileModel } from '~app/refund/models/refund-massload-file.model';
import { ContactViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundBE } from '~app/refund/models/refund.model';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';

export const refundFeatureKey = 'refund';

//* Massload file tab key
export const massloadFileKey = 'massload-file-list';

//* Massload attachment tab key
export const massloadAttachmentKey = 'massload-attachment-list';

//* Active tab keys
const activeTabId = 'active';
export const activeKeys = {
  [MasterDataType.RefundApp]: fromListSubtabs.getKey(MasterDataType.RefundApp, activeTabId),
  [MasterDataType.RefundNotice]: fromListSubtabs.getKey(MasterDataType.RefundNotice, activeTabId)
};

//* Archived tab keys
const archiveTabId = 'archived';
export const archiveKeys = {
  [MasterDataType.RefundApp]: fromListSubtabs.getKey(MasterDataType.RefundApp, archiveTabId),
  [MasterDataType.RefundNotice]: fromListSubtabs.getKey(MasterDataType.RefundNotice, archiveTabId)
};

//* Deleted tab keys
const deletedTabId = 'deleted';
export const deletedKeys = {
  [MasterDataType.RefundApp]: fromListSubtabs.getKey(MasterDataType.RefundApp, deletedTabId),
  [MasterDataType.RefundNotice]: fromListSubtabs.getKey(MasterDataType.RefundNotice, deletedTabId)
};

export interface RefundState {
  agentContactData: ContactViewModel;
  airlineContactData: ContactViewModel;
  refundForm: fromRefundForm.RefundFormState;
  refundResubmission: fromRefundResubmission.RefundResubmissionState;

  // RA massload files state
  [massloadFileKey]: fromListSubtabs.ListSubtabsState<RefundMassloadFileModel, RefundMassloadFileFilter>;
  [massloadAttachmentKey]: fromListSubtabs.ListSubtabsState<RefundMassloadAttachment, RefundMassloadAttachmentFilter>;
  [key: string]: any;
}

export interface State extends AppState {
  [refundFeatureKey]: RefundState;
}

export function reducers(state: RefundState | undefined, action: Action) {
  return combineReducers({
    agentContactData: fromRefundContact.reducerAgentContact,
    airlineContactData: fromRefundContact.reducerAirlineContact,

    // RA massload files reducers
    [massloadFileKey]: fromListSubtabs.reducer<RefundMassloadFileModel, RefundMassloadFileFilter>(massloadFileKey),
    [massloadAttachmentKey]: fromListSubtabs.reducer<RefundMassloadAttachment, RefundMassloadAttachmentFilter>(
      massloadAttachmentKey
    ),

    refundForm: fromRefundForm.reducer,
    refundResubmission: fromRefundResubmission.reducer,
    [activeKeys[MasterDataType.RefundApp]]: fromListSubtabs.reducer<RefundBE, RefundFilter>(
      activeKeys[MasterDataType.RefundApp]
    ),
    [activeKeys[MasterDataType.RefundNotice]]: fromListSubtabs.reducer<RefundBE, RefundFilter>(
      activeKeys[MasterDataType.RefundNotice]
    ),
    [archiveKeys[MasterDataType.RefundApp]]: fromListSubtabs.reducer<RefundBE, RefundFilter>(
      archiveKeys[MasterDataType.RefundApp]
    ),
    [archiveKeys[MasterDataType.RefundNotice]]: fromListSubtabs.reducer<RefundBE, RefundFilter>(
      archiveKeys[MasterDataType.RefundNotice]
    ),
    [deletedKeys[MasterDataType.RefundApp]]: fromListSubtabs.reducer<RefundBE, RefundFilter>(
      deletedKeys[MasterDataType.RefundApp]
    ),
    [deletedKeys[MasterDataType.RefundNotice]]: fromListSubtabs.reducer<RefundBE, RefundFilter>(
      deletedKeys[MasterDataType.RefundNotice]
    )
  })(state, action);
}

export const getRefundState = createFeatureSelector<State, RefundState>(refundFeatureKey);

export const getAgentContactData = createSelector(getRefundState, state => state.agentContactData);

export const getAirlineContactData = createSelector(getRefundState, state => state.airlineContactData);

export const getRefundForm = createSelector(getRefundState, state => state.refundForm);

export const getRefundResubmissionRemark = (refundNumber: string) =>
  createSelector(
    getRefundState,
    state =>
      state.refundResubmission.resubmissionRemarks.find(record => record.refundNumber === refundNumber)
        ?.resubmissionRemark
  );

export const massloadFileSelector = createSelector(getRefundState, state => state[massloadFileKey]);
export const massloadAttachmentSelector = createSelector(getRefundState, state => state[massloadAttachmentKey]);

export const activeSelectors = {
  [MasterDataType.RefundApp]: createSelector(getRefundState, state => state[activeKeys[MasterDataType.RefundApp]]),
  [MasterDataType.RefundNotice]: createSelector(getRefundState, state => state[activeKeys[MasterDataType.RefundNotice]])
};

export const archivedSelectors = {
  [MasterDataType.RefundApp]: createSelector(getRefundState, state => state[archiveKeys[MasterDataType.RefundApp]]),
  [MasterDataType.RefundNotice]: createSelector(
    getRefundState,
    state => state[archiveKeys[MasterDataType.RefundNotice]]
  )
};

export const deletedSelectors = {
  [MasterDataType.RefundApp]: createSelector(getRefundState, state => state[deletedKeys[MasterDataType.RefundApp]]),
  [MasterDataType.RefundNotice]: createSelector(
    getRefundState,
    state => state[deletedKeys[MasterDataType.RefundNotice]]
  )
};
