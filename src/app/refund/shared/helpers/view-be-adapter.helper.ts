/* eslint-disable @typescript-eslint/ban-types */
import { Contact } from '~app/adm-acm/models/adm-acm-issue-be-aux.model';
import { CardPayment, Commission, OriginalIssue, TaxMiscellaneousFee } from '~app/refund/models/refund-shared.model';
import { RefundFinal } from '~app/refund/models/refund.model';
import {
  getDefaultCalculationFieldValueBE,
  getDefaultCalculationFieldValueView,
  uppercaseAdapter
} from '~app/shared/helpers/form-helpers/adapter/view-be-adapter.helper';

export function commissionViewAdapter(commission: Commission): Commission {
  return commissionsAdapter(commission, getDefaultCalculationFieldValueView);
}

export function commissionBEAdapter(commission: Commission): Commission {
  return commissionsAdapter(commission, getDefaultCalculationFieldValueBE);
}

export function commissionsAdapter(commission: Commission, adapter: Function): Commission {
  commission.amount = commission.amount ? adapter(commission.amount) : null;
  commission.rate = commission.rate ? adapter(commission.rate) : null;

  return commission;
}

export function cardPaymentsViewAdapter(cardPayments: CardPayment[]): CardPayment[] {
  return cardPaymentsAdapter(cardPayments, getDefaultCalculationFieldValueView);
}

export function cardPaymentsBEAdapter(cardPayments: CardPayment[]): CardPayment[] {
  return cardPaymentsAdapter(cardPayments, getDefaultCalculationFieldValueBE);
}

function cardPaymentsAdapter(cardPayments: CardPayment[], adapter: Function): CardPayment[] {
  //* Default result is empty array
  if (!cardPayments) {
    return [];
  }

  return cardPayments
    .filter(line => line.type)
    .map(line => {
      line.type = uppercaseAdapter(line.type);
      line.amount = adapter(line.amount);

      return line;
    });
}

export function taxFeesBEAdapter(taxFees: TaxMiscellaneousFee[]): TaxMiscellaneousFee[] {
  return taxFeesAdapter(taxFees, getDefaultCalculationFieldValueBE);
}

export function taxFeeViewAdapter(taxFee: TaxMiscellaneousFee[]): TaxMiscellaneousFee[] {
  return taxFeesAdapter(taxFee, getDefaultCalculationFieldValueView);
}

function taxFeesAdapter(taxFees: TaxMiscellaneousFee[], adapter: Function): TaxMiscellaneousFee[] {
  return (
    taxFees
      //* We remove empty lines from payload
      .filter(line => line.type)
      //* We set default values for calculations
      .map(line => {
        line.type = uppercaseAdapter(line.type);
        line.amount = adapter(line.amount);

        return line;
      })
  );
}

// We update REFUND form id to null, id retrieved from ticket is refered to document id
export function idTicketViewAdapter(): any {
  return null;
}

export function getDefaultContactFieldValue(contact: Contact): Contact {
  let result = contact;

  if (contact) {
    result = {
      contactName: contact.contactName ? contact.contactName : '',
      phoneFaxNumber: contact.phoneFaxNumber ? contact.phoneFaxNumber : '',
      email: contact.email
    };
  }

  return result;
}

export function originalIssueBEAdapter(originalIssue: OriginalIssue): OriginalIssue {
  return (
    originalIssue && {
      ...originalIssue,
      airlineCode: originalIssue.airlineCode.toUpperCase(),
      locationCode: originalIssue.locationCode.toUpperCase()
    }
  );
}

export function finalFlagAdapter(final: boolean): RefundFinal {
  return final ? RefundFinal.Final : null;
}
