import { MasterDataType } from '~app/master-shared/models/master.model';

export const createRefundPermission = {
  raIssue: 'cRa',
  rnIssue: 'cRn'
};

export const createRefundPermissionUnderSupervision = {
  raIssue: 'cRaSupv',
  rnIssue: 'cRnSupv'
};

export const viewRefundPermission = {
  raView: 'rRa',
  raBillView: 'rRaBill',
  rnView: 'rRn',
  rnBillView: 'rRnBill',
  raViewQuery: 'rRaQ',
  rnViewQuery: 'rRnQ'
};

export const modifyRefundPermission = {
  raModify: 'uRaMod',
  rnModify: 'uRnMod'
};

export const deleteRefundPermission = {
  raDelete: 'uRaDel',
  rnDelete: 'uRnDel'
};

export const updateRefundPermission = {
  raUpdate: 'uRaState'
};

export const supervisionRefundPermission = {
  raSupervision: 'rRaSupv',
  rnSupervision: 'rRnSupv'
};

export const refundFilesPermission = {
  raMassRead: 'rRaMass',
  raMassCreate: 'cRaMass',
  raAttRead: 'rRaAtt'
};

export const readRefundInternalCommentPermission = {
  raReadInternalComment: 'rRaIntCom',
  rnReadInternalComment: 'rRnIntCom'
};

export const createRefundInternalCommentPermission = {
  raCreateInternalComment: 'cRaIntCom',
  rnCreateInternalComment: 'cRnIntCom'
};

export const updateRefundInternalCommentPermission = {
  raUpdateInternalComment: 'uRaIntCom',
  rnUpdateInternalComment: 'uRnIntCom'
};

export const deleteRefundInternalCommentPermission = {
  raDeleteInternalComment: 'dRaIntCom',
  rnDeleteInternalComment: 'dRnIntCom'
};

export function getSupervisionRefundPermissions(): Array<string> {
  return [supervisionRefundPermission.raSupervision, supervisionRefundPermission.rnSupervision];
}

export function getRefundViewPermission(refundType: MasterDataType): string {
  return refundType === MasterDataType.RefundApp ? viewRefundPermission.raViewQuery : viewRefundPermission.rnViewQuery;
}

export function getRefundCreatePermission(refundType: MasterDataType): string {
  return refundType === MasterDataType.RefundApp ? createRefundPermission.raIssue : createRefundPermission.rnIssue;
}

export function getIssueRefundPendingSupervisionPermission(refundType: MasterDataType): string {
  return refundType === MasterDataType.RefundApp
    ? createRefundPermissionUnderSupervision.raIssue
    : createRefundPermissionUnderSupervision.rnIssue;
}

export function getRaRnMenuItemPermissions(refundType: MasterDataType): string[] {
  const raMenuItemPermissions = [
    createRefundPermission.raIssue,
    createRefundPermissionUnderSupervision.raIssue,
    viewRefundPermission.raViewQuery
  ];

  const rnMenuItemPermissions = [
    createRefundPermission.rnIssue,
    createRefundPermissionUnderSupervision.rnIssue,
    viewRefundPermission.rnViewQuery
  ];

  return refundType === MasterDataType.RefundApp ? raMenuItemPermissions : rnMenuItemPermissions;
}

export function getRefundsFilesPermissions(): Array<string> {
  return [refundFilesPermission.raMassRead, refundFilesPermission.raMassCreate, refundFilesPermission.raAttRead];
}

export function getRefundReadInternalCommentPermission(refundType: MasterDataType): string {
  return refundType === MasterDataType.RefundApp
    ? readRefundInternalCommentPermission.raReadInternalComment
    : readRefundInternalCommentPermission.rnReadInternalComment;
}

export function getRefundCreateInternalCommentPermission(refundType: MasterDataType): string {
  return refundType === MasterDataType.RefundApp
    ? createRefundInternalCommentPermission.raCreateInternalComment
    : createRefundInternalCommentPermission.rnCreateInternalComment;
}

export function getRefundUpdateInternalCommentPermission(refundType: MasterDataType): string {
  return refundType === MasterDataType.RefundApp
    ? updateRefundInternalCommentPermission.raUpdateInternalComment
    : updateRefundInternalCommentPermission.rnUpdateInternalComment;
}

export function getRefundDeleteInternalCommentPermission(refundType: MasterDataType): string {
  return refundType === MasterDataType.RefundApp
    ? deleteRefundInternalCommentPermission.raDeleteInternalComment
    : deleteRefundInternalCommentPermission.rnDeleteInternalComment;
}

export function getRefundEditInternalCommentPermission(refundType: MasterDataType): string[] {
  return [
    getRefundCreateInternalCommentPermission(refundType),
    getRefundUpdateInternalCommentPermission(refundType),
    getRefundDeleteInternalCommentPermission(refundType)
  ];
}

export function getBspFilterRequiredPermissions(refundType: MasterDataType): string[] {
  const requiredPermissions = {
    [MasterDataType.RefundApp]: [
      viewRefundPermission.raView,
      viewRefundPermission.raBillView,
      supervisionRefundPermission.raSupervision
    ],
    [MasterDataType.RefundNotice]: [
      viewRefundPermission.rnView,
      viewRefundPermission.rnBillView,
      supervisionRefundPermission.rnSupervision
    ]
  };

  return requiredPermissions[refundType];
}
