import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';

import { DocumentService } from '~app/document/services/document.service';
import { AppState } from '~app/reducers';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundTicketService } from '~app/refund/services/refund-ticket.service';

export function createRefundConfigService(
  formData: RefundDataService,
  translationService: L10nTranslationService,
  http: HttpClient,
  store: Store<AppState>,
  refundActionsService: RefundActionsService
): RefundConfigService {
  return new RefundConfigService(formData, translationService, http, store, refundActionsService);
}

export function createRefundTicketService(docService: DocumentService, formData: RefundDataService) {
  return new RefundTicketService(docService, formData);
}
