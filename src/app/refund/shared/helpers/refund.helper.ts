import { HttpClient } from '@angular/common/http';
import { capitalize } from 'lodash';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AgentViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundService } from '~app/refund/services/refund.service';
import { ROUTES } from '~app/shared/constants/routes';
import { Bsp } from '~app/shared/models/bsp.model';
import { MenuTab, MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';

export function createRefundService(http: HttpClient, refundType: MasterDataType) {
  const activatedRouteSnapshot = { data: { refundType } } as any;

  return new RefundService(http, null, activatedRouteSnapshot);
}

export function getUserDetails(address: AgentAddress | AirlineAddress) {
  let details = {};

  if (address) {
    const { address1, address2, street, city, country, postalCode, telephone } = address as AgentAddress &
      AirlineAddress;

    details = {
      telephone,
      address: !address2 ? address1 : `${address1} ${address2}`,
      street,
      city,
      country,
      postalCode
    };
  }

  return details;
}

export function getBspNameFromAgent(agent: AgentViewModel): string {
  return `${capitalize(agent.bsp.name)} (${agent.bsp.isoCountryCode})`;
}

export function getBspName(bsp: Bsp): string {
  return `${capitalize(bsp.name)} (${bsp.isoCountryCode})`;
}

export function getRefundTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;

  return { label: tab.tabLabel as string, params: { refundNumber: routeData.item.ticketDocumentNumber } };
}

export function getRefundDeleteQueryUrl(refundType: MasterDataType): string {
  return `${urlList[refundType]}/deleted`;
}

const urlList = {
  'refund-application': ROUTES.REFUNDS_APP_QUERY.url,
  'refund-notice': ROUTES.REFUNDS_NOTICE_QUERY.url
};
