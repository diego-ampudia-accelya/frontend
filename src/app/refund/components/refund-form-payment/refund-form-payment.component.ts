import { ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { MenuItem } from 'primeng/api';
import { combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import { CardPayment } from '~app/refund/models/refund-shared.model';
import { CurrencyViewModel, FormOfPaymentViewModel } from '~app/refund/models/refund-view-aux.model';
import { PaymentMethodsType, RefundActionEmitterType } from '~app/refund/models/refund.model';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundTicketService } from '~app/refund/services/refund-ticket.service';
import { RefundService } from '~app/refund/services/refund.service';
import { ButtonDesign } from '~app/shared/components';
import { GLOBALS } from '~app/shared/constants/globals';
import { toValueLabelCurrencyId } from '~app/shared/helpers';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { NumberHelper } from '~app/shared/helpers/numberHelper';
import { Bsp } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { maxLengthRoundedValidator } from '~app/shared/validators/max-length-rounded.validator';
import { positiveNumberValidator } from '~app/shared/validators/positive-number.validator';
import { paymentCardRequiredValidator } from './validators/payment-card-required.validator';

@Component({
  selector: 'bspl-refund-form-payment',
  templateUrl: './refund-form-payment.component.html',
  styleUrls: ['./refund-form-payment.component.scss']
})
export class RefundFormPaymentComponent implements OnInit, OnDestroy {
  @Input() formData: RefundDataService;
  @Input() formConfig: RefundConfigService;
  @Input() refundTicket: RefundTicketService;
  @Input() refundService: RefundService;

  @Output() totalAmountChange = new EventEmitter();

  public amountPlaceholder = '';

  public btnDesign = ButtonDesign;

  public form: FormGroup;
  public currencyForm: FormGroup;
  public cardArrayControl: FormArray;
  public customerReference: FormControl;
  public cashPaymentAmountControl: FormControl;
  public miscellaneousCashAmountControl: FormControl;

  public currencyList: Array<CurrencyViewModel> = [];
  public currencyOptions: DropdownOption[] = [];
  public currencySelected: CurrencyViewModel;

  public paymentMethodOptions: ButtonMenuOption[] = [];
  public mainBtnOption: MenuItem;

  public showCash = false;
  public showMiscCash = false;

  public issueTicketMode: boolean;
  public paymentMethodType = PaymentMethodsType;

  public isCustomerRefFieldVisible$: Observable<boolean>;
  public iseTicketAuthCodeFieldVisible$: Observable<boolean>;

  private totalAmount: FormControl;
  private formFactory: FormUtil;

  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private fb: FormBuilder,
    public translationService: L10nTranslationService,
    private cd: ChangeDetectorRef
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.setConfig();
    this.buildForm();

    if (this.formConfig.isEditMode || this.formConfig.isResubmission) {
      // This card line is added so it can be populated (if needed) with data from BE
      // If not needed, it will be deleted later on in initializePaymentsForBEData method
      this.addPaymentCard(null);
      this.cardArrayControl.controls[0].disable({ emitEvent: false });
    }

    this.initializeChangesListener();

    this.formData.whenFormIsReady().subscribe(() => {
      this.initializeCustomerReferenceField();
      this.initializeETicketAuthCodeField();
    });
  }

  public removeCardRow(index: number): void {
    const paymentMethod: PaymentMethodsType = FormUtil.get<CardPayment>(
      this.cardArrayControl.controls[index] as FormGroup,
      'payment'
    ).value;
    this.cardArrayControl.removeAt(index);
    this.resetBtnOptions(paymentMethod);

    if (!this.isCustomerReferenceEnabled()) {
      this.customerReference.reset(null, { onlySelf: true });
      this.customerReference.disable({ emitEvent: false });
    }
  }

  public removeCash(value): void {
    const cashPaymentsControl =
      value === PaymentMethodsType.cash ? this.cashPaymentAmountControl : this.miscellaneousCashAmountControl;
    cashPaymentsControl.reset(null);
    if (value === PaymentMethodsType.cash) {
      this.showCash = false;
    } else {
      this.showMiscCash = false;
    }
    this.resetBtnOptions(value);
  }

  public isBtnDisabled(): boolean {
    return this.paymentMethodOptions.every(option => option.isDisabled);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeCustomerReferenceField(): void {
    const customerReferenceCtrl = FormUtil.get<FormOfPaymentViewModel>(this.form, 'customerReference');

    this.isCustomerRefFieldVisible$ = this.formConfig.isCustomerReferenceVisible(customerReferenceCtrl.value).pipe(
      tap(() => {
        if (this.formConfig.isEditTicketMode) {
          customerReferenceCtrl.patchValue(this.refundTicket.customerReferenceTicket, { emitEvent: false });
        }

        this.toggleControlDisable(customerReferenceCtrl, !this.isCustomerReferenceEnabled());
      })
    );
  }

  private initializeETicketAuthCodeField(): void {
    const eTicketAuthCodeCtrl = FormUtil.get<FormOfPaymentViewModel>(this.form, 'eTicketAuthCode');

    this.iseTicketAuthCodeFieldVisible$ = this.formConfig
      .iseTicketCodeVisible(eTicketAuthCodeCtrl.value)
      .pipe(tap(fieldVisible => this.toggleControlDisable(eTicketAuthCodeCtrl, !fieldVisible)));
  }

  private setConfig(): void {
    this.mainBtnOption = this.formConfig.getPaymentMainBtnAction();
    this.issueTicketMode = this.formConfig.isEditTicketMode;

    this.formData
      .whenFormIsReady()
      .pipe(
        switchMap(() => this.formConfig.getPaymentMethodOptions()),
        takeUntil(this.destroy$)
      )
      .subscribe(options => this.initializePaymentOptions(options));
  }

  private initializePaymentOptions(options: ButtonMenuOption[]): void {
    this.paymentMethodOptions = options;

    if (this.formConfig.isEditMode || this.formConfig.isResubmission) {
      // We need the paymentMethodOptions populated before calling this method
      this.initializePaymentsForBEData();
      this.cd.detectChanges();
    }

    // Make sure the row for the second card is not visible if second card option is not available
    const isCard2Available = this.paymentMethodOptions.find(option => option.id === PaymentMethodsType.card2);
    if (!isCard2Available) {
      this.removeCardRowByMethod(PaymentMethodsType.card2);
    }

    if (this.issueTicketMode) {
      this.populateCardArrayTicket();
      this.populateCashAmountsTicket();
    }
  }

  private buildForm(): void {
    this.cardArrayControl = this.fb.array([], { validators: paymentCardRequiredValidator(), updateOn: 'blur' });

    this.customerReference = new FormControl(null, [
      Validators.maxLength(27),
      Validators.pattern(GLOBALS.PATTERNS.REFUNDS.CUSTOMER_REF)
    ]);

    this.currencyForm = this.formFactory.createGroup<CurrencyViewModel>({
      id: ['', Validators.required],
      code: '',
      decimals: '',
      isDefault: ''
    });

    this.cashPaymentAmountControl = this.fb.control(null, this.getMonetaryAmountValidators());
    this.miscellaneousCashAmountControl = this.fb.control(null, this.getMonetaryAmountValidators());

    this.totalAmount = new FormControl(0);

    this.form = this.formFactory.createGroup<FormOfPaymentViewModel>({
      currency: this.currencyForm,
      tourCode: [null, [Validators.maxLength(15), Validators.pattern(GLOBALS.PATTERNS.REFUNDS.TOUR_CODE)]],
      customerReference: this.customerReference,
      eTicketAuthCode: [null, [Validators.maxLength(14), Validators.pattern(GLOBALS.PATTERNS.REFUNDS.E_TICKET)]],
      cardPayments: this.cardArrayControl,
      cashPaymentAmount: this.cashPaymentAmountControl,
      miscellaneousCashAmount: this.miscellaneousCashAmountControl,
      cardTotal: 0,
      cashTotal: 0,
      totalAmount: this.totalAmount
    });

    this.formData.setSectionFormGroup('formPayment', this.form);
  }

  private initializeCardListener(): void {
    this.formData
      .whenFormIsReady()
      .pipe(
        switchMap(() => FormUtil.get<FormOfPaymentViewModel>(this.form, 'cardPayments').valueChanges),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.cardArrayControl.controls.forEach(cardPaymentGroup => {
          const { card, cardMask } = cardPaymentGroup.value;
          const cardControl = FormUtil.get<CardPayment>(cardPaymentGroup as FormGroup, 'card');
          const cardMaskControl = FormUtil.get<CardPayment>(cardPaymentGroup as FormGroup, 'cardMask');
          let conditionForIssue = false;
          let conditionForTicketSummary = false;

          if (this.refundTicket.ticketSummary) {
            const ticketCards = this.refundTicket.ticketSummary.formOfPayment;
            const conditionForTypeCC = ticketCards.some(
              cardTicketCC => cardTicketCC.maskedCreditCardNumber && cardTicketCC.maskedCreditCardNumber !== cardMask
            );
            conditionForTicketSummary =
              !cardMaskControl.pristine && ((cardMask && card !== cardMask) || conditionForTypeCC);
          } else {
            conditionForIssue = cardMask && card !== cardMask;
          }

          if (conditionForIssue || conditionForTicketSummary) {
            cardControl.patchValue(cardMask, { emitEvent: false });
          }
        });
      });
  }

  private initializePaymentsForBEData(): void {
    const cashAmountValue = this.cashPaymentAmountControl.value;
    const miscCashAmountValue = this.miscellaneousCashAmountControl.value;

    if (this.cardArrayControl.at(0).disabled) {
      this.cardArrayControl.removeAt(0); //* This disabled control is only used for population from BE data but does not contain any value
    }

    this.cardArrayControl.controls.forEach(cardGroup => {
      const cardPaymentValue = FormUtil.get<CardPayment>(cardGroup as FormGroup, 'payment').value;
      const cardAmountValue = FormUtil.get<CardPayment>(cardGroup as FormGroup, 'amount').value;
      const cardMaskNumber = FormUtil.get<CardPayment>(cardGroup as FormGroup, 'cardMask');
      const cardNumber = FormUtil.get<CardPayment>(cardGroup as FormGroup, 'card');

      if (cardNumber.value && !cardMaskNumber.value) {
        cardMaskNumber.patchValue(cardNumber.value);
      }

      if (cardAmountValue && Number(cardAmountValue) > 0) {
        this.addPaymentMethodRow(cardPaymentValue);
      }
    });

    if (cashAmountValue != null) {
      this.addPaymentMethodRow(PaymentMethodsType.cash);
    }
    if (miscCashAmountValue != null) {
      this.addPaymentMethodRow(PaymentMethodsType.miscCash);
    }
  }

  private initializeChangesListener(): void {
    this.currencyForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(currency => {
      this.onCurrencyChange(currency.id);
    });

    this.formConfig.paymentMethod
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => this.addPaymentMethodRow(value as PaymentMethodsType));

    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => this.updateForm());

    this.formData
      .getSubject(RefundActionEmitterType.bsp)
      .pipe(
        filter(bsp => bsp),
        takeUntil(this.destroy$)
      )
      .subscribe(bsp => this.initializeCurrenciesFromBsp(bsp));

    this.totalAmount.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$)).subscribe(value => {
      this.totalAmountChange.emit(value);
    });

    this.initializeCardListener();
  }

  private populateCardArrayTicket(): void {
    const ticketCards = this.refundTicket.creditCardPaymentTicket.filter(el => el !== null);
    let formG: FormGroup;

    ticketCards.forEach((card, index) => {
      this.addPaymentMethodRow(card.payment as PaymentMethodsType);
      formG = this.fb.group({
        card: card.card,
        cardMask: card.cardMask,
        payment: card.payment,
        type: card.type,
        amount: card.amount
      });
      this.cardArrayControl.setControl(index, formG);
    });
  }

  private populateCashAmountsTicket(): void {
    const cashTicket = this.refundTicket.cashPaymentTicket;
    const miscCashTicket = this.refundTicket.miscCashPaymentTicket;
    if (cashTicket) {
      this.addPaymentMethodRow(PaymentMethodsType.cash);
      this.cashPaymentAmountControl.patchValue(cashTicket);
    }
    if (miscCashTicket) {
      this.addPaymentMethodRow(PaymentMethodsType.miscCash);
      this.miscellaneousCashAmountControl.patchValue(miscCashTicket);
    }
  }

  private onCurrencyChange(id: number): void {
    this.currencySelected = cloneDeep(this.currencyList.find(curr => curr.id === id)) || null;
    this.currencyForm.reset(this.currencySelected || {}, { emitEvent: false });

    // Update form in order to update amounts and totals
    this.updateForm();

    this.formData.getSubject(RefundActionEmitterType.currency).next(this.currencySelected);
  }

  private setDefaultCurrency(): void {
    if (this.issueTicketMode || this.formConfig.isEditMode) {
      const currencyId = FormUtil.get<CurrencyViewModel>(this.currencyForm, 'id').value;
      const currencyTicket = this.currencyList.find(currency => currency.id === currencyId);

      this.onCurrencyChange(currencyTicket ? currencyTicket.id : null);
    } else {
      this.currencyList.forEach(currency => {
        if (this.currencyList.length === 1 || currency.isDefault) {
          this.onCurrencyChange(currency.id);
        }
      });
    }
  }

  private initializeCurrenciesFromBsp(bsp: Bsp): void {
    combineLatest([this.formData.whenFormIsReady(), this.getCurrenciesFromBspObservable(bsp.id)]).subscribe(
      ([_, currencies]) => {
        this.currencyList = currencies;
        this.currencyOptions = this.currencyList.map(toValueLabelCurrencyId);

        this.setDefaultCurrency();
      }
    );
  }

  private getCurrenciesFromBspObservable(bspId: number): Observable<CurrencyViewModel[]> {
    return this.refundService.getBspCurrencies(bspId);
  }

  private updateForm(): void {
    this.updateControls();
    this.setTotals();
  }

  private getCardPaymentForm(value): FormGroup {
    return this.formFactory.createGroup<CardPayment>({
      amount: [null, this.getMonetaryAmountValidators()],
      card: [null],
      cardMask: [null, [Validators.maxLength(19), Validators.pattern(GLOBALS.PATTERNS.REFUNDS.CARD_MASK)]],
      payment: value,
      type: ['', [Validators.pattern(GLOBALS.PATTERNS.REFUNDS.CARD_TYPE)]]
    });
  }

  private addPaymentMethodRow(paymentMethod: PaymentMethodsType): void {
    const paymentOption = this.paymentMethodOptions.find(item => item.id === paymentMethod);

    if (paymentOption) {
      // Always disable the clicked option in the dropdown
      paymentOption.isDisabled = true;

      // Forced trigger of change detection
      this.paymentMethodOptions = this.paymentMethodOptions.slice(0);

      switch (paymentMethod) {
        case PaymentMethodsType.cash:
        case PaymentMethodsType.miscCash:
          this.addPaymentCash(paymentMethod);
          break;
        case PaymentMethodsType.card1:
        case PaymentMethodsType.card2:
        case PaymentMethodsType.easypay:
        case PaymentMethodsType.miscellaneous: {
          if (!this.isPaymentCardAdded(paymentMethod)) {
            this.addPaymentCard(paymentMethod);
          }

          const isCard2Available = this.paymentMethodOptions.find(option => option.id === PaymentMethodsType.card2);

          if (paymentOption.id === PaymentMethodsType.card1 && isCard2Available) {
            this.switchCardOptions();
          }

          break;
        }
      }

      //* Exception ChangeAfterCheck is thrown when added rows on issuing
      this.cd.detectChanges();
    }
  }

  private addPaymentCash(paymentMethod: PaymentMethodsType): void {
    if (paymentMethod === PaymentMethodsType.cash) {
      this.showCash = true;
      if (!this.cashPaymentAmountControl.value) {
        this.cashPaymentAmountControl.patchValue('', { emitEvent: false });
      }
    }
    if (paymentMethod === PaymentMethodsType.miscCash) {
      this.showMiscCash = true;
      if (!this.miscellaneousCashAmountControl.value) {
        this.miscellaneousCashAmountControl.patchValue('', { emitEvent: false });
      }
    }
  }

  private isPaymentCardAdded(paymentMethod: PaymentMethodsType): boolean {
    let isAdded = false;

    if (this.cardArrayControl) {
      isAdded =
        this.cardArrayControl.controls.findIndex(cardGroup => {
          const typeValue: PaymentMethodsType = FormUtil.get<CardPayment>(cardGroup as FormGroup, 'payment').value;

          return typeValue === paymentMethod;
        }) !== -1;
    }

    return isAdded;
  }

  private addPaymentCard(paymentCard: PaymentMethodsType): void {
    this.cardArrayControl.push(this.getCardPaymentForm(paymentCard));
    this.cardArrayControl.controls.sort(this.formConfig.sortCardPayment);

    if (this.isCustomerReferenceEnabled()) {
      this.customerReference.enable({ emitEvent: true });
    }
  }

  private isCustomerReferenceEnabled(): boolean {
    return (
      this.isPaymentCardAdded(PaymentMethodsType.card1) ||
      this.isPaymentCardAdded(PaymentMethodsType.card2) ||
      this.isPaymentCardAdded(PaymentMethodsType.miscellaneous)
    );
  }

  private updateControls(): void {
    this.amountPlaceholder = Number(0).toFixed(this.decimals);

    this.updateNumberControl(this.cashPaymentAmountControl);
    this.updateNumberControl(this.miscellaneousCashAmountControl);

    this.cardArrayControl.controls.forEach(group => {
      this.updateNumberControl(FormUtil.get<CardPayment>(group as FormGroup, 'amount'));
    });

    const tourCodeCtrl = FormUtil.get<FormOfPaymentViewModel>(this.form, 'tourCode');
    tourCodeCtrl.setValue(tourCodeCtrl.value && tourCodeCtrl.value.toUpperCase(), { emitEvent: false });
  }

  private setTotals(): void {
    this.setCashTotals();
    this.setCardTotals();

    const cashTotalValue = parseFloat(FormUtil.get<FormOfPaymentViewModel>(this.form, 'cashTotal').value);
    const cardTotalValue = parseFloat(FormUtil.get<FormOfPaymentViewModel>(this.form, 'cardTotal').value);

    this.totalAmount.setValue(this.round(cashTotalValue + cardTotalValue), { onlySelf: true });
  }

  private setCashTotals(): void {
    const cashPaymentsControl = this.cashPaymentAmountControl;
    const cashMiscPaymentsControl = this.miscellaneousCashAmountControl;

    const cashValue = parseFloat(
      cashPaymentsControl.valid && cashPaymentsControl.value ? cashPaymentsControl.value : 0
    );
    const cashMiscValue = parseFloat(
      cashMiscPaymentsControl.valid && cashMiscPaymentsControl.value ? cashMiscPaymentsControl.value : 0
    );

    this.form.get('cashTotal').setValue(this.round(cashValue + cashMiscValue), { onlySelf: true });
  }

  private setCardTotals(): void {
    let sum = 0;
    const cardTotalControl = FormUtil.get<FormOfPaymentViewModel>(this.form, 'cardTotal');

    this.cardArrayControl.controls.forEach(group => {
      const amount = FormUtil.get<CardPayment>(group as FormGroup, 'amount');
      sum += parseFloat(amount.valid && amount.value ? amount.value : 0);
    });

    cardTotalControl.setValue(this.round(sum), { onlySelf: true });
  }

  private updateNumberControl(control: AbstractControl): void {
    FormUtil.regularizeCalculationControls(control);

    if (control.valid && control.value) {
      control.setValue(this.round(control.value), { emitEvent: false });
    }
  }

  private removeCardRowByMethod(method: PaymentMethodsType): void {
    if (this.cardArrayControl) {
      const methodIndex = this.cardArrayControl.controls.findIndex(
        control => FormUtil.get<CardPayment>(control as FormGroup, 'payment').value === method
      );

      if (methodIndex !== -1) {
        this.removeCardRow(methodIndex);
      }
    }
  }

  private toggleControlDisable(control: AbstractControl, isDisabled: boolean): void {
    if (isDisabled) {
      control.disable();
    } else {
      control.enable();
    }
  }

  public get currencyCodeLabel(): string {
    return this.currencySelected ? `, ${this.currencySelected.code.toUpperCase()}` : '';
  }

  private resetBtnOptions(removedRow: PaymentMethodsType): void {
    // Always enable the option in the dropdown, corresponding to the removed row
    this.enableMethodOption(removedRow);
    const isCard2Available = this.paymentMethodOptions.find(option => option.id === PaymentMethodsType.card2);

    if (removedRow === PaymentMethodsType.card1 && isCard2Available) {
      this.switchCardOptions();
    }
  }

  private toggleCardOption(id: PaymentMethodsType): void {
    const option = this.paymentMethodOptions.find(item => item.id === id);
    if (option) {
      option.hidden = !option.hidden;
    }
  }

  private enableMethodOption(id: PaymentMethodsType): void {
    const option = this.paymentMethodOptions.find(item => item.id === id);

    if (option) {
      option.isDisabled = false;

      // Forced trigger of change detection
      this.paymentMethodOptions = this.paymentMethodOptions.slice(0);
    }
  }

  /**
   * Controls which card option is shown in the dropdown menu.
   */
  private switchCardOptions(): void {
    const currentCardOption = this.paymentMethodOptions.find(option => !option.hidden);
    const isCard2Available = this.paymentMethodOptions.find(option => option.id === PaymentMethodsType.card2);

    if (currentCardOption.id === PaymentMethodsType.card1) {
      // When the first card option is selected, hide it and show the second card option
      this.toggleCardOption(PaymentMethodsType.card1);
      this.toggleCardOption(PaymentMethodsType.card2);
    }

    if (isCard2Available && currentCardOption.id === PaymentMethodsType.card2) {
      // When the second card option is selected, hide it and show the first card option
      this.toggleCardOption(PaymentMethodsType.card2);
      this.toggleCardOption(PaymentMethodsType.card1);
    }
  }

  private get decimals(): number {
    return this.currencySelected ? this.currencySelected.decimals : 2;
  }

  private round(value): string {
    const rounded = NumberHelper.roundString(value, this.decimals);

    return isNaN(rounded as any) ? value : rounded;
  }

  private getMonetaryAmountValidators(): ValidatorFn[] {
    return [
      positiveNumberValidator,
      Validators.pattern(this.formConfig.getDecimalsPattern()),
      maxLengthRoundedValidator(this.decimals, NumberHelper.getMonetaryAmountMaxLength(this.decimals))
    ];
  }
}
