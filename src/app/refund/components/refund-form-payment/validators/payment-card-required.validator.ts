import { FormArray, ValidatorFn } from '@angular/forms';

import { CardPayment } from '~app/refund/models/refund-shared.model';

/**
 * Validates if a card of payment form is valid together with its fields. Condition is:
 * ```
 * a line is partially filled, all other fields are required.
 ```
 *
 * @returns null since errors are set in child controls
 */
export function paymentCardRequiredValidator(): ValidatorFn {
  return (formArray: FormArray): { [key: string]: boolean } => {
    if (formArray.value) {
      const fieldsToCheck: Array<keyof CardPayment> = ['type', 'card', 'amount'];

      formArray.controls.forEach(group => {
        const isAnyFieldFilled = fieldsToCheck.some(key => group.get(key).value);

        fieldsToCheck.forEach(field => {
          const fieldControl = group.get(field);

          if (!fieldControl.value && isAnyFieldFilled) {
            fieldControl.setErrors({ required: true });
          } else if (fieldControl.hasError('required')) {
            fieldControl.setErrors(null);
          }
        });
      });
    }

    return null;
  };
}
