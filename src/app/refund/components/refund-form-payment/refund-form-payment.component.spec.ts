import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundFormPaymentComponent } from './refund-form-payment.component';
import { CurrencyViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { PaymentMethodsType, RefundActionEmitterType } from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundConfigService } from '~app/refund/shared/helpers/refund.factory';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { Bsp } from '~app/shared/models/bsp.model';

const mockAgent = {};

const activatedRouteStub = { snapshot: { data: { refundType: MasterDataType.RefundApp } } };

const initialState = {
  auth: {
    user: mockAgent
  },
  core: {
    menu: {
      tabs: {}
    }
  }
};

describe('RefundFormPaymentComponent', () => {
  let component: RefundFormPaymentComponent;
  let fixture: ComponentFixture<RefundFormPaymentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundFormPaymentComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule,
        NoopAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        provideMockStore({ initialState }),
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        HttpTestingController,
        FormBuilder,
        L10nTranslationService,
        HttpClient,
        RefundActionsService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundFormPaymentComponent);
    component = fixture.componentInstance;

    component.formData = new RefundDataService(TestBed.inject(FormBuilder));
    component.formConfig = createRefundConfigService(
      component.formData,
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Store),
      TestBed.inject(RefundActionsService)
    );
    component.refundService = jasmine.createSpyObj<RefundService>('RefundService', ['getBspCurrencies']);

    component.paymentMethodOptions = Object.values(PaymentMethodsType).map(method => ({ id: method, label: method }));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit currency when changed', done => {
    const currencySubject = component.formData.getSubject(RefundActionEmitterType.currency);
    spyOn(currencySubject, 'next');

    const mockCurrency: CurrencyViewModel = { id: 1, decimals: 2, code: 'EUR', isDefault: true };
    component.currencyList.push(mockCurrency);
    component.currencyForm.setValue(mockCurrency);

    fixture.whenStable().then(() => {
      expect(currencySubject.next).toHaveBeenCalledWith(mockCurrency);
      done();
    });
  });

  it('should set currency as default if there is only one', () => {
    const currencyId = 1;
    const mockCurrency: CurrencyViewModel = { id: currencyId, decimals: 2, code: 'EUR', isDefault: false };

    component.refundService.getBspCurrencies = jasmine.createSpy().and.returnValue(of([mockCurrency]));
    spyOn<any>(component, 'onCurrencyChange');
    spyOn<any>(component, 'setDefaultCurrency').and.callThrough();

    component['formData'].isFormReady.next(true);
    component['initializeCurrenciesFromBsp']({ id: 1 } as Bsp);
    expect(component['setDefaultCurrency']).toHaveBeenCalled();
    expect(component['onCurrencyChange']).toHaveBeenCalledWith(currencyId);
  });

  it('should add a payment card method if not present already', () => {
    expect(component.cardArrayControl.length).toBe(0);
    component['addPaymentCard'](PaymentMethodsType.card1);
    expect(component.cardArrayControl.length).toBe(1);
  });

  it('should sort payment card methods', () => {
    component['addPaymentCard'](PaymentMethodsType.card2);
    component['addPaymentCard'](PaymentMethodsType.card1);
    expect(component.cardArrayControl.controls[0].get('payment').value).toBe(PaymentMethodsType.card1);
  });

  it('should add a payment cash method', () => {
    expect(component.showCash).toBe(false);
    component['addPaymentMethodRow'](PaymentMethodsType.cash);
    expect(component.showCash).toBe(true);
  });

  it('should add a payment misc cash method', () => {
    expect(component.showMiscCash).toBe(false);
    component['addPaymentMethodRow'](PaymentMethodsType.miscCash);
    expect(component.showMiscCash).toBe(true);
  });

  it('should enable customer reference if credit or misc card payments have been added', () => {
    component['addPaymentCard'](PaymentMethodsType.miscellaneous);
    expect(component.customerReference.enabled).toBeTruthy();

    component['removeCardRow'](0);
    component['addPaymentCard'](PaymentMethodsType.easypay);
    expect(component.customerReference.enabled).toBeFalsy();
  });

  it('should calculate totals correctly', done => {
    component['currency'] = { decimals: 2 } as any;
    component['addPaymentCard'](PaymentMethodsType.card1);
    component['addPaymentMethodRow'](PaymentMethodsType.cash);

    component.cardArrayControl.controls[0].patchValue({ amount: 5 }, { emitEvent: false });
    component.form.patchValue({ cashPaymentAmount: 5 });

    fixture.whenStable().then(() => {
      expect(component.form.get('cardTotal').value).toBe('5.00');
      expect(component.form.get('cashTotal').value).toBe('5.00');
      expect(component.form.get('totalAmount').value).toBe('10.00');
      done();
    });
  });

  it('should remove card rows', () => {
    component['addPaymentCard'](PaymentMethodsType.card1);
    expect(component.cardArrayControl.length).toBe(1);
    component['removeCardRow'](0);
    expect(component.cardArrayControl.length).toBe(0);
  });

  it('should reset and disable customer reference if there are not credit or misc cards', () => {
    component['addPaymentCard'](PaymentMethodsType.card1);
    component['addPaymentCard'](PaymentMethodsType.easypay);
    expect(component.customerReference.enabled).toBeTruthy();

    component['removeCardRow'](0);
    expect(component.customerReference.enabled).toBeFalsy();
  });

  it('should remove cash rows', () => {
    component['addPaymentMethodRow'](PaymentMethodsType.cash);
    expect(component.showCash).toBeTruthy();
    component.removeCash(PaymentMethodsType.cash);
    expect(component.showCash).toBeFalsy();
  });

  it('should display tour code on upper case', done => {
    component.form.get('tourCode').setValue('aa-2');

    fixture.whenStable().then(() => {
      expect(component.form.get('tourCode').value).toBe('AA-2');
      done();
    });
  });
});
