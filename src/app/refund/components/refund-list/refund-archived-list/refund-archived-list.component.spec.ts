import { ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundArchivedListComponent } from './refund-archived-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDialogService } from '~app/refund/services/refund-dialog.service';
import { RefundFilterFormatter } from '~app/refund/services/refund-filter-formatter.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('RefundArchivedListComponent', () => {
  let fixture: ComponentFixture<RefundArchivedListComponent>;
  let component: RefundArchivedListComponent;

  const refundServiceSpy = createSpyObject(RefundService);

  const activatedRouteStub = {
    snapshot: {
      data: {
        title: 'REFUNDS.query.commons.deletedTabTitle',
        refundType: MasterDataType.RefundNotice
      }
    }
  };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: ['rRnQ', 'rRnIntCom'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          'MENU.REFUNDS.QUERY.notice': {},
          activeTabId: 'MENU.REFUNDS.QUERY.notice'
        }
      },
      viewListsInfo: {}
    },
    refund: { 'refund-notice-archived-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundArchivedListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({ initialState }),
        FormBuilder,
        PermissionsService,
        RefundDialogService,
        ErrorHandler,
        RefundInternalCommentDialogService,
        mockProvider(AgentDictionaryService),
        mockProvider(AirlineDictionaryService),
        mockProvider(CurrencyDictionaryService),
        mockProvider(PeriodService),
        mockProvider(DialogService),
        mockProvider(NotificationService),
        mockProvider(RefundActionsService),
        mockProvider(LocalBspTimeService),
        mockProvider(ActivatedRoute, activatedRouteStub)
      ]
    })
      .overrideComponent(RefundArchivedListComponent, {
        set: {
          providers: [
            RefundFilterFormatter,
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: RefundService, useValue: refundServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    refundServiceSpy.getRefundMenuOptions.and.returnValue(of([]));

    // Initialize component
    fixture = TestBed.createComponent(RefundArchivedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: include this test with maybe a ListSubtabs mock (`RangeError: Maximum call stack size exceeded`)
  xit('should create component', () => {
    expect(component).toBeDefined();
  });
});
