import { Component, ErrorHandler } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { map } from 'rxjs/operators';

import { RefundActiveListComponent } from '../refund-active-list/refund-active-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { AppState } from '~app/reducers';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { RefundFilter } from '~app/refund/models/refund-filter.model';
import { RefundBE } from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDialogService } from '~app/refund/services/refund-dialog.service';
import { RefundFilterFormatter } from '~app/refund/services/refund-filter-formatter.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import { archivedSelectors, archiveKeys, State } from '~app/refund/store/reducers';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus } from '~app/shared/models/mark-status.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-refund-archived-list',
  templateUrl: '../refund-active-list/refund-active-list.component.html',
  styleUrls: ['../refund-active-list/refund-active-list.component.scss'],
  providers: [RefundFilterFormatter, RefundService]
})
export class RefundArchivedListComponent extends RefundActiveListComponent {
  public isArchivedSubTab = true;

  constructor(
    protected store: Store<AppState>,
    protected refundService: RefundService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    public displayFormatter: RefundFilterFormatter,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected fb: FormBuilder,
    protected agentDictionaryService: AgentDictionaryService,
    protected airlineDictionaryService: AirlineDictionaryService,
    protected currencyDictionaryService: CurrencyDictionaryService,
    protected periodService: PeriodService,
    protected dialogService: DialogService,
    protected refundDialogService: RefundDialogService,
    protected notificationService: NotificationService,
    protected permissionsService: PermissionsService,
    protected refundActionsService: RefundActionsService,
    protected localBspTimeService: LocalBspTimeService,
    protected globalErrorHandlerService: ErrorHandler,
    protected refundInternalCommentDialogService: RefundInternalCommentDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(
      store,
      refundService,
      actions$,
      translationService,
      displayFormatter,
      activatedRoute,
      router,
      fb,
      agentDictionaryService,
      airlineDictionaryService,
      currencyDictionaryService,
      periodService,
      dialogService,
      refundDialogService,
      notificationService,
      permissionsService,
      refundActionsService,
      localBspTimeService,
      globalErrorHandlerService,
      refundInternalCommentDialogService,
      bspsDictionaryService
    );
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<RefundBE, RefundFilter>,
    DefaultProjectorFn<ListSubtabsState<RefundBE, RefundFilter>>
  > {
    return archivedSelectors[this.refundType];
  }

  protected getListKey(): string {
    return archiveKeys[this.refundType];
  }

  protected adaptSearchQuery(query: DataQuery<RefundFilter>): DataQuery<RefundFilter> {
    return {
      ...query,
      filterBy: {
        ...query.filterBy,
        // With "!" at the first status we are negating the following status for the API call
        status: query.filterBy.status || [`!${RefundStatus.deleted}`, RefundStatus.supervisionDeleted],
        markStatus: [`${MarkStatus.Archive}`]
      }
    };
  }

  public onGetActionList(refund: RefundBE & { markStatus: string }) {
    // TODO Addapt to RN when is needed
    this.listActions$ = this.refundService.getRefundActionList(refund.id).pipe(
      map(actions => {
        let modifiedActions: any = actions;
        const hasLoadingAction = actions.some(action => action.action === GridTableActionType.Loading);

        const watchingAction = refund.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;

        if (!hasLoadingAction && this.hasViewPermission) {
          modifiedActions = [
            ...modifiedActions,
            { action: GridTableActionType.MoveToActive, group: 'markStatus' },
            { action: watchingAction, group: 'markStatus' }
          ];
        }

        return this.groupActions(modifiedActions);
      })
    );
  }
}
