/* eslint-disable @typescript-eslint/naming-convention */
import { RefundFilter } from '~app/refund/models/refund-filter.model';
import { GridColumn } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';

const translationPrefix = 'REFUNDS.tableColumns';

export const SHARED_RA = {
  get ACTIVE_COLUMNS(): Array<GridColumn> {
    return [
      {
        name: `${translationPrefix}.isoc`,
        prop: 'agent.bsp.isoCountryCode',
        draggable: false,
        resizeable: false,
        sortable: false,
        width: 60
      },
      {
        name: `${translationPrefix}.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: false,
        cellTemplate: 'commonLinkCellTmpl',
        width: 160
      },
      {
        name: `${translationPrefix}.airlCode`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true,
        width: 80
      },
      {
        name: `${translationPrefix}.agentCode`,
        prop: 'agent.iataCode',
        draggable: false,
        resizeable: true,
        width: 90
      },
      {
        name: `${translationPrefix}.status`,
        prop: 'status',
        draggable: false,
        badgeInfo: null,
        image: null,
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        width: 160
      },
      {
        name: `${translationPrefix}.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: false,
        width: 95
      },
      {
        name: `${translationPrefix}.final`,
        prop: 'finalType',
        draggable: false,
        resizeable: true,
        width: 90
      },
      {
        name: `${translationPrefix}.daysLeft`,
        prop: 'finalDaysLeft',
        draggable: false,
        resizeable: true,
        width: 70
      },
      {
        name: `${translationPrefix}.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: 'amountCellTmpl'
      },
      {
        name: `${translationPrefix}.formsOfPayment`,
        prop: 'formsOfPayment',
        draggable: false,
        resizeable: true,
        sortable: false,
        width: 130,
        cellTemplate: 'commonPipeSeparatorCellTmpl'
      },
      {
        name: `${translationPrefix}.rtdnNumber`,
        prop: 'relatedTicketDocument.documentNumber',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.applicationDate`,
        prop: 'dateOfApplication',
        draggable: false,
        resizeable: true,
        width: 130,
        pipe: { transform: value => new DateTimeFormatPipe().transform(value, 'date') }
      },
      {
        name: `${translationPrefix}.authorizationDate`,
        prop: 'dateOfAuthorization',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 120
      },
      {
        name: `${translationPrefix}.rejectionDate`,
        prop: 'dateOfRejection',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 120
      },
      {
        name: `${translationPrefix}.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 80
      }
    ];
  },
  get DELETED_COLUMNS(): Array<GridColumn> {
    return [
      {
        name: `${translationPrefix}.isoc`,
        prop: 'agent.bsp.isoCountryCode',
        draggable: false,
        resizeable: false,
        sortable: false,
        width: 75
      },
      {
        name: `${translationPrefix}.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: false,
        width: 120
      },
      {
        name: `${translationPrefix}.airlCode`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true,
        width: 80
      },
      {
        name: `${translationPrefix}.agentCode`,
        prop: 'agent.iataCode',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.status`,
        prop: 'status',
        draggable: false,
        width: 160
      },
      {
        name: `${translationPrefix}.final`,
        prop: 'finalType',
        draggable: false,
        resizeable: true,
        width: 90
      },
      {
        name: `${translationPrefix}.daysLeft`,
        prop: 'finalDaysLeft',
        draggable: false,
        resizeable: true,
        width: 70
      },
      {
        name: `${translationPrefix}.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: false,
        width: 95
      },
      {
        name: `${translationPrefix}.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: 'amountCellTmpl'
      },
      {
        name: `${translationPrefix}.formsOfPayment`,
        prop: 'formsOfPayment',
        draggable: false,
        resizeable: true,
        sortable: false,
        width: 145,
        cellTemplate: 'commonPipeSeparatorCellTmpl'
      },
      {
        name: `${translationPrefix}.rtdnNumber`,
        prop: 'relatedTicketDocument.documentNumber',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.applicationDate`,
        prop: 'dateOfApplication',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 120
      },
      {
        name: `${translationPrefix}.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 80
      }
    ];
  },
  get PRIMARY_ACTIONS() {
    return [{ action: GridTableActionType.Details }];
  },
  get ACTIONS() {
    return [GridTableActionType.Add];
  }
};

export const SHARED_RN = {
  get ACTIVE_COLUMNS(): Array<GridColumn> {
    return [
      {
        name: `${translationPrefix}.isoc`,
        prop: 'agent.bsp.isoCountryCode',
        draggable: false,
        resizeable: false,
        sortable: false,
        width: 55
      },
      {
        name: `${translationPrefix}.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: false,
        cellTemplate: 'commonLinkCellTmpl',
        width: 140
      },
      {
        name: `${translationPrefix}.airlCode`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true,
        width: 80
      },
      {
        name: `${translationPrefix}.agentCode`,
        prop: 'agent.iataCode',
        draggable: false,
        resizeable: true,
        width: 90
      },
      {
        name: `${translationPrefix}.status`,
        prop: 'status',
        draggable: false,
        badgeInfo: null,
        cellTemplate: 'textWithBadgeInfoCellTmpl',
        width: 160
      },
      {
        name: `${translationPrefix}.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: false,
        width: 70
      },
      {
        name: `${translationPrefix}.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: 'amountCellTmpl'
      },
      {
        name: `${translationPrefix}.formsOfPayment`,
        prop: 'formsOfPayment',
        draggable: false,
        resizeable: true,
        sortable: false,
        width: 145,
        cellTemplate: 'commonPipeSeparatorCellTmpl'
      },
      {
        name: `${translationPrefix}.rtdnNumber`,
        prop: 'relatedTicketDocument.documentNumber',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.issueDate`,
        prop: 'dateOfIssue',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 120
      },
      {
        name: `${translationPrefix}.repDate`,
        prop: 'reportingDate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 120
      },
      {
        name: `${translationPrefix}.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 80
      }
    ];
  },
  get DELETED_COLUMNS(): Array<GridColumn> {
    return [
      {
        name: `${translationPrefix}.isoc`,
        prop: 'agent.bsp.isoCountryCode',
        draggable: false,
        resizeable: false,
        sortable: false,
        width: 55
      },
      {
        name: `${translationPrefix}.docNumber`,
        prop: 'ticketDocumentNumber',
        draggable: false,
        resizeable: false,
        width: 120
      },
      {
        name: `${translationPrefix}.airlCode`,
        prop: 'airline.iataCode',
        draggable: false,
        resizeable: true,
        width: 80
      },
      {
        name: `${translationPrefix}.agentCode`,
        prop: 'agent.iataCode',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.status`,
        prop: 'status',
        draggable: false,
        width: 160
      },
      {
        name: `${translationPrefix}.currency`,
        prop: 'currency.code',
        draggable: false,
        resizeable: false,
        width: 70
      },
      {
        name: `${translationPrefix}.amount`,
        prop: 'totalAmount',
        draggable: false,
        resizeable: true,
        width: 110,
        cellClass: 'text-right',
        cellTemplate: 'amountCellTmpl'
      },
      {
        name: `${translationPrefix}.formsOfPayment`,
        prop: 'formsOfPayment',
        draggable: false,
        resizeable: true,
        sortable: false,
        width: 145,
        cellTemplate: 'commonPipeSeparatorCellTmpl'
      },
      {
        name: `${translationPrefix}.rtdnNumber`,
        prop: 'relatedTicketDocument.documentNumber',
        draggable: false,
        resizeable: true,
        width: 110
      },
      {
        name: `${translationPrefix}.issueDate`,
        prop: 'dateOfIssue',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 120
      },
      {
        name: `${translationPrefix}.repDate`,
        prop: 'reportingDate',
        draggable: false,
        resizeable: true,
        cellTemplate: 'dayMonthYearCellTmpl',
        width: 120
      },
      {
        name: `${translationPrefix}.period`,
        prop: 'period',
        draggable: false,
        resizeable: true,
        width: 80
      }
    ];
  },
  get PRIMARY_ACTIONS() {
    return [{ action: GridTableActionType.Details }];
  },
  get ACTIONS() {
    return [GridTableActionType.Add];
  }
};

export const SEARCH_FORM_DEFAULT_VALUE: RefundFilter = {
  ticketDocumentNumber: '',
  bsp: null,
  airlineIataCode: null,
  agentId: null,
  status: null,
  dateOfApplication: null,
  dateOfIssue: null,
  dateOfRejection: null,
  dateOfLatestAction: null,
  reportingDate: null,
  currencyId: null,
  amountRange: null,
  relatedTicketDocument: null,
  passenger: null,
  period: null,
  sentToDpc: null,
  formsOfPayment: null,
  sentToAirline: null,
  changedByAgent: null,
  hasInternalComment: null,
  markStatus: null,
  finalType: null,
  finalDaysLeftRange: null,
  finalProcessStoppedRange: null,
  statisticalCode: null
};
