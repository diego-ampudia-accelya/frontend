import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, discardPeriodicTasks, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { MockProviders } from 'ng-mocks';
import { Observable, of, throwError } from 'rxjs';

import { RefundActiveListComponent } from './refund-active-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundStatus, SendToDpcType } from '~app/refund/models/refund-be-aux.model';
import { RefundOptionType } from '~app/refund/models/refund-shared.model';
import { RefundActionType, RefundBE } from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDialogService } from '~app/refund/services/refund-dialog.service';
import { RefundFilterFormatter } from '~app/refund/services/refund-filter-formatter.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus } from '~app/shared/models/mark-status.model';
import { User, UserType } from '~app/shared/models/user.model';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('RefundActiveListComponent', () => {
  let fixture: ComponentFixture<RefundActiveListComponent>;
  let component: RefundActiveListComponent;

  const refundServiceSpy = createSpyObject(RefundService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const currencyDictionaryServiceSpy = createSpyObject(CurrencyDictionaryService);
  const periodServiceSpy = createSpyObject(PeriodService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const refundDialogServiceSpy = createSpyObject(RefundDialogService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);
  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const refundInternalCommentDialogServiceSpy = createSpyObject(RefundInternalCommentDialogService);
  const refundActionsServiceSpy = createSpyObject(RefundActionsService);

  const activatedRouteStub = {
    snapshot: {
      data: {
        title: 'REFUNDS.query.commons.deletedTabTitle',
        refundType: MasterDataType.RefundApp
      }
    }
  };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: ['rRaQ', 'rLean'],
    defaultIsoc: 'MT',
    bspPermissions: [
      { bspId: 1, permissions: ['rRaQ'] },
      { bspId: 2, permissions: ['rRaSupv'] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          'MENU.REFUNDS.QUERY.app': {},
          activeTabId: 'MENU.REFUNDS.QUERY.app'
        }
      },
      viewListsInfo: {}
    },
    refund: { 'refund-application-active-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundActiveListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, RouterTestingModule],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({ initialState }),
        FormBuilder,
        PermissionsService,
        ErrorHandler,
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        MockProviders(LocalBspTimeService),
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionaryServiceSpy },
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: RefundDialogService, useValue: refundDialogServiceSpy },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: RefundInternalCommentDialogService, useValue: refundInternalCommentDialogServiceSpy },
        { provide: RefundActionsService, useValue: refundActionsServiceSpy }
      ]
    })
      .overrideComponent(RefundActiveListComponent, {
        set: {
          providers: [
            RefundFilterFormatter,
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: RefundService, useValue: refundServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    // Initializing spies
    refundServiceSpy.getRefundMenuOptions.and.returnValue(of([]));
    bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
      of([
        { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
        { id: 2, name: 'MALTA', isoCountryCode: 'MT' }
      ])
    );

    // Initialize component
    fixture = TestBed.createComponent(RefundActiveListComponent);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  describe('setting data from route', () => {
    it('should set refund type on init', () => {
      expect(component.refundType).toBe(MasterDataType.RefundApp);
      expect(component.isRa).toBe(true);
      expect(component.isRn).toBe(false);
    });

    it('should set grid table variables on init', () => {
      expect(component.tableTitle).toBe('REFUNDS.query.refund-application.listTitle');
      expect(component['initialSortAttribute']).toBe('dateOfApplication');
    });

    describe('for refund-notice type', () => {
      afterAll(() => {
        activatedRouteStub.snapshot.data = { refundType: MasterDataType.RefundApp } as any;
      });

      it('should set refund type on init', () => {
        component['activatedRoute'].snapshot.data = { refundType: MasterDataType.RefundNotice } as any;

        (component as any).setDataFromRoute();

        expect(component.refundType).toBe(MasterDataType.RefundNotice);
        expect(component.isRa).toBe(false);
        expect(component.isRn).toBe(true);
      });

      it('should set grid table variables on init', () => {
        activatedRouteStub.snapshot.data = { refundType: MasterDataType.RefundNotice } as any;

        (component as any).setDataFromRoute();

        expect(component.tableTitle).toBe('REFUNDS.query.refund-notice.listTitle');
        expect(component['initialSortAttribute']).toBe('dateOfIssue');
      });
    });
  });

  it('should set view permissions depending on refund type', () => {
    // RA case
    expect(component['viewPermission']).toEqual(['rRa', 'rRaBill', 'rRaSupv']);
  });

  describe('initialize logged user', () => {
    beforeEach(() => fixture.detectChanges());

    it('should initialize user type', () => {
      // Airline user case
      expect(component['userType']).toBe(UserType.AIRLINE);
      expect(component.isUserAgent).toBe(false);
      expect(component.isUserAirline).toBe(true);
    });

    it('should initialize multi-country user flag', () => {
      // Multi-country user case
      expect(component['isMultiCountryUser']).toBe(true);
    });
  });

  describe('initialize LEAN BSP filter', () => {
    describe('BSP filter flags', () => {
      it('should NOT make BSP filter multiple if not LEAN and multiple BSPs', () => {
        const mockUser = { ...expectedUserDetails, permissions: ['rRaQ'] };
        setStateUser(mockUser);

        fixture.detectChanges();
        expect(component.isBspFilterMultiple).toBe(false);
      });

      it('should NOT make BSP filter multiple if LEAN and unique BSP', () => {
        bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
          of([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }])
        );

        fixture.detectChanges();
        expect(component.isBspFilterMultiple).toBe(false);
      });

      it('should make BSP filter multiple if LEAN and multiple BSPs', () => {
        fixture.detectChanges();
        expect(component.isBspFilterMultiple).toBe(true);
      });

      it('should make BSP filter locked if unique BSP', () => {
        bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
          of([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }])
        );

        fixture.detectChanges();
        expect(component.isBspFilterLocked).toBe(true);
      });

      it('should NOT make BSP filter locked if multiple BSPs', () => {
        // Multiple BSPs case
        fixture.detectChanges();
        expect(component.isBspFilterLocked).toBe(false);
      });
    });

    describe('predefined filter', () => {
      it('should NOT update it with BSP filter value if LEAN and multiple BSPs', () => {
        // BSP filter value in this case is the default BSP
        fixture.detectChanges();

        expect(component.predefinedFilters).not.toEqual(
          jasmine.objectContaining({ bsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT' } })
        );
      });

      it('should update it with BSP filter value if NOT LEAN and multiple BSPs', () => {
        const mockUser = { ...expectedUserDetails, permissions: ['rRaQ'] };
        setStateUser(mockUser);

        // BSP filter value in this case is the default BSP
        fixture.detectChanges();

        expect(component.predefinedFilters).toEqual(
          jasmine.objectContaining({ bsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT' } })
        );
      });

      it('should update it with BSP filter value if LEAN and unique BSP', () => {
        bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
          of([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }])
        );

        fixture.detectChanges();

        expect(component.predefinedFilters).toEqual(
          jasmine.objectContaining({ bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } })
        );
      });

      it('should NOT update it with BSP filter value if unique BSP and AGENT user', () => {
        const mockUser = { ...expectedUserDetails, userType: UserType.AGENT };
        setStateUser(mockUser);

        bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
          of([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }])
        );

        fixture.detectChanges();

        expect(component.predefinedFilters).not.toEqual(
          jasmine.objectContaining({ bsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT' } })
        );
      });

      it('should NOT update it with BSP filter value if multiple BSPs and AGENT user', () => {
        const mockUser = { ...expectedUserDetails, userType: UserType.AGENT };
        setStateUser(mockUser);

        fixture.detectChanges();

        expect(component.predefinedFilters).not.toEqual(
          jasmine.objectContaining({ bsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT' } })
        );
      });
    });
  });

  describe('initialize columns', () => {
    it('should NOT have checkbox column if user has not view permissions', () => {
      const mockUser = { ...expectedUserDetails, permissions: [] };
      setStateUser(mockUser);

      fixture.detectChanges();
      expect(component.columns.map(col => col.prop)).not.toContain('isRowSelected');
    });

    it('should have checkbox column if user has view permissions', () => {
      const mockUser = { ...expectedUserDetails, permissions: ['rRa', 'rRaBill', 'rRaSupv'] };
      setStateUser(mockUser);

      fixture.detectChanges();
      expect(component.columns.map(col => col.prop)).toContain('isRowSelected');
    });

    it('should NOT have agent related columns if user is an AGENT', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.AGENT };
      setStateUser(mockUser);

      fixture.detectChanges();

      expect(component.columns.map(col => col.prop)).not.toContain('agent.bsp.name');
      expect(component.columns.map(col => col.prop)).not.toContain('agent.iataCode');
    });

    it('should have agent related columns if user is NOT an AGENT', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.AGENT_GROUP };
      setStateUser(mockUser);

      fixture.detectChanges();

      expect(component.columns.map(col => col.prop)).toContain('agent.iataCode');
    });

    it('should NOT have airline related columns if user is an AIRLINE', () => {
      // Airline user case
      fixture.detectChanges();

      expect(component.columns.map(col => col.prop)).not.toContain('airline.iataCode');
    });

    it('should have airline related columns if user is NOT an AIRLINE', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.AGENT };
      setStateUser(mockUser);

      fixture.detectChanges();
      expect(component.columns.map(col => col.prop)).toContain('airline.iataCode');
    });

    describe('initialize status badge info', () => {
      it('should set status column translation if that column exists', () => {
        const expectedColumns: any = jasmine.arrayContaining([
          jasmine.objectContaining({ prop: 'status', pipe: jasmine.anything() })
        ]);

        fixture.detectChanges();
        expect(component.columns).toEqual(expectedColumns);
      });

      describe('RA - badge info', () => {
        beforeEach(() => fixture.detectChanges());

        it('should be hidden if refund status is NOT authorized or rejected', () => {
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;

          expect(badgeInfo.hidden({ status: RefundStatus.pending })).toBe(true);
          expect(badgeInfo.hidden({ status: RefundStatus.investigated })).toBe(true);
        });

        it('should be visible if refund status is authorized or rejected', () => {
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;

          expect(badgeInfo.hidden({ status: RefundStatus.authorized })).toBe(false);
          expect(badgeInfo.hidden({ status: RefundStatus.rejected })).toBe(false);
        });

        it('should return `null` type if refund status is NOT authorized or rejected', () => {
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;

          expect(badgeInfo.type({ status: RefundStatus.pending })).toBe(null);
          expect(badgeInfo.type({ status: RefundStatus.investigated })).toBe(null);
        });

        it('should return `sendToDpc` type if refund status is authorized or rejected', () => {
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;

          expect(badgeInfo.type({ status: RefundStatus.authorized, sentToDpc: SendToDpcType.pending })).toBe(
            BadgeInfoType.info
          );
          expect(badgeInfo.type({ status: RefundStatus.rejected, sentToDpc: SendToDpcType.sent })).toBe(
            BadgeInfoType.success
          );
          expect(badgeInfo.type({ status: RefundStatus.authorized, sentToDpc: SendToDpcType.n_a })).toBe(
            BadgeInfoType.regular
          );
          expect(badgeInfo.type({ status: RefundStatus.authorized, sentToDpc: SendToDpcType.na })).toBe(
            BadgeInfoType.regular
          );
        });

        it('should set tooltip label if `sentToDpc` is available and refund status is authorized or rejected', () => {
          translationServiceSpy.translate = jasmine.createSpy().and.returnValue('test') as any;
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;

          expect(badgeInfo.tooltipLabel({ status: RefundStatus.authorized })).toBe('');
          expect(badgeInfo.tooltipLabel({ status: RefundStatus.rejected, sentToDpc: SendToDpcType.sent })).not.toEqual(
            ''
          );
          expect(badgeInfo.tooltipLabel({ status: RefundStatus.pending })).toBe('');
        });
      });

      it('should set RA status column image if `changedByAgent` has `S` value', () => {
        fixture.detectChanges();

        const image = component.columns.find(({ prop }) => prop === 'status').image;

        expect(image({ changedByAgent: 'S' })).not.toBe(null);
        expect(image({ changedByAgent: 'N' })).toBe(null);
        expect(image({})).toBe(null);
      });

      describe('RN - badge info', () => {
        beforeEach(() => {
          component.isRa = false;
          component.isRn = true;

          fixture.detectChanges();
        });

        it('should always be visible', () => {
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;
          expect(badgeInfo.hidden({})).toBe(false);
        });

        it('should return `sendToDpc` type', () => {
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;

          expect(badgeInfo.type({ sentToDpc: SendToDpcType.pending })).toBe(BadgeInfoType.info);
          expect(badgeInfo.type({ sentToDpc: SendToDpcType.sent })).toBe(BadgeInfoType.success);
          expect(badgeInfo.type({ sentToDpc: SendToDpcType.na })).toBe(BadgeInfoType.regular);
        });

        it('should always set tooltip label', () => {
          translationServiceSpy.translate = jasmine.createSpy().and.returnValue('test') as any;
          const badgeInfo = component.columns.find(({ prop }) => prop === 'status').badgeInfo;

          expect(badgeInfo.tooltipLabel({ sentToDpc: SendToDpcType.na })).toBeDefined();
        });
      });
    });

    it('should set final type column translation if that column exists', () => {
      const expectedColumns: any = jasmine.arrayContaining([
        jasmine.objectContaining({ prop: 'finalType', pipe: jasmine.anything() })
      ]);

      fixture.detectChanges();
      expect(component.columns).toEqual(expectedColumns);
    });

    it('should set final days left column icon if that column exists', () => {
      const expectedColumns: any = jasmine.arrayContaining([
        jasmine.objectContaining({ prop: 'finalDaysLeft', icon: jasmine.anything() })
      ]);

      fixture.detectChanges();
      expect(component.columns).toEqual(expectedColumns);
    });

    it('should set application date column icon if that column exists', () => {
      const expectedColumns: any = jasmine.arrayContaining([
        jasmine.objectContaining({ prop: 'dateOfApplication', icon: jasmine.anything() })
      ]);

      fixture.detectChanges();
      expect(component.columns).toEqual(expectedColumns);
    });

    // TODO: test all application date column icon methods related

    it('should set BSP column sortable property if user is LEAN', () => {
      const expectedColumns: any = jasmine.arrayContaining([
        jasmine.objectContaining({ prop: 'agent.bsp.isoCountryCode', sortable: true })
      ]);

      fixture.detectChanges();
      expect(component.columns).toEqual(expectedColumns);
    });

    it('should set document number column image if user has internal comment permission', () => {
      const expectedColumns: any = jasmine.arrayContaining([
        jasmine.objectContaining({ prop: 'ticketDocumentNumber', image: jasmine.anything() })
      ]);

      const mockUser = { ...expectedUserDetails, permissions: ['rRaIntCom'] };
      setStateUser(mockUser);

      fixture.detectChanges();
      expect(component.columns).toEqual(expectedColumns);
    });
  });

  describe('period control disability', () => {
    it('should disable period control ON INIT if zero or multiple BSPs have been selected', () => {
      // Multiple BSPs available and zero selected
      fixture.detectChanges();

      expect(component.isPeriodDisabled).toBe(true);
    });

    it('should NOT disable period control ON INIT if unique BSP is available', () => {
      bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
        of([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }])
      );

      // Unique BSP available
      fixture.detectChanges();

      expect(component.isPeriodDisabled).toBe(false);
    });

    it('should disable period control when 1 BSP is selected', fakeAsync(() => {
      // Initializing spies due to filter button click simulation
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));

      fixture.detectChanges();

      // Multiple BSPs available and one selected
      component.onFilterButtonClicked(true);
      component.bspControl.patchValue([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }]);

      discardPeriodicTasks();

      expect(component.isPeriodDisabled).toBe(false);
    }));
  });

  describe('period filter options', () => {
    beforeEach(() => periodServiceSpy.getByBsp.calls.reset());

    it('should update period filter options ON INIT when only 1 BSP is selected', () => {
      bspsDictionaryServiceSpy.getAllBspsByPermissions.and.returnValue(
        of([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }])
      );

      // Unique BSP available
      fixture.detectChanges();

      expect(periodServiceSpy.getByBsp).toHaveBeenCalledWith(1);
    });

    it('should NOT update period filter options ON INIT without any BSP selected', () => {
      // Multiple BSPs available and zero selected
      fixture.detectChanges();

      expect(periodServiceSpy.getByBsp).not.toHaveBeenCalled();
    });

    it('should update period filter options when 1 BSP is selected after open filter section', fakeAsync(() => {
      // Initializing spies due to filter button click simulation
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of([]));

      fixture.detectChanges();

      // Multiple BSPs available and one selected
      component.onFilterButtonClicked(true);
      component.bspControl.patchValue([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }]);

      discardPeriodicTasks();

      expect(periodServiceSpy.getByBsp).toHaveBeenCalledTimes(1);
      expect(periodServiceSpy.getByBsp).toHaveBeenCalledWith(1);
    }));
  });

  describe('initialize button menu options', () => {
    it('should initialize button menu options on init', fakeAsync(() => {
      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Read, label: 'read' }]));
      spyOn<any>(component, 'getTranslatedButtonMenuOptionLabel').and.callThrough();
      spyOn<any>(component, 'setButtonMenuOptionGroup').and.callThrough();
      spyOn<any>(component, 'setButtonMenuOptionCommand').and.callThrough();

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe();

      discardPeriodicTasks();

      expect(refundServiceSpy.getRefundMenuOptions).toHaveBeenCalledWith(false);
      expect(component['getTranslatedButtonMenuOptionLabel']).toHaveBeenCalledWith({
        id: RefundOptionType.Read,
        label: 'read'
      });
      expect(component['setButtonMenuOptionGroup']).toHaveBeenCalledWith({ id: RefundOptionType.Read, label: 'read' });
      expect(component['setButtonMenuOptionCommand']).toHaveBeenCalledWith({
        id: RefundOptionType.Read,
        label: 'read'
      });
    }));

    it('should translate button menu option label with active path', fakeAsync(() => {
      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Read, label: 'read' }]));

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe();

      discardPeriodicTasks();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(
        'REFUNDS.query.tableSelectionToolbar.buttonMenuOptions.read'
      );
    }));

    it('should set button menu option group on `read` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Read, label: 'read' }]));

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('markAs');
    }));

    it('should set button menu option group on `unread` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Unread, label: 'unread' }]));

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('markAs');
    }));

    it('should set button menu option group on `archive` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Archive, label: 'archive' }]));

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('markAs');
    }));

    it('should set button menu option group on `authorize` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(
        of([{ id: RefundOptionType.Authorize, label: 'authorize' }])
      );

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('update');
    }));

    it('should set button menu option group on `reject` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Reject, label: 'reject' }]));

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('update');
    }));

    it('should set button menu option group on `supervise` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(
        of([{ id: RefundOptionType.Supervise, label: 'supervise' }])
      );

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('update');
    }));

    it('should set button menu option group on `delete` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Delete, label: 'delete' }]));

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('update');
    }));

    it('should set button menu option group on `comment` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Comment, label: 'comment' }]));

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('update');
    }));

    it('should set button menu option group on `downloadAttachments` option', fakeAsync(() => {
      let buttonMenuOptionGroup: string;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(
        of([{ id: RefundOptionType.DownloadAttachments, label: 'downloadAttachments' }])
      );

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionGroup = options[0].group));

      discardPeriodicTasks();

      expect(buttonMenuOptionGroup).toBe('download');
    }));

    it('should set button menu option `setMarkStatus - read` command on `read` option', fakeAsync(() => {
      let buttonMenuOptionCommand: () => void;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Read, label: 'read' }]));
      spyOn<any>(component, 'setMarkStatus').and.callThrough();

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionCommand = options[0].command));
      buttonMenuOptionCommand();

      discardPeriodicTasks();

      expect(component['setMarkStatus']).toHaveBeenCalledWith('READ');
    }));

    it('should set button menu option `mark as archive` command on `archive` option', fakeAsync(() => {
      let buttonMenuOptionCommand: () => void;

      refundServiceSpy.getRefundMenuOptions.and.returnValue(of([{ id: RefundOptionType.Archive, label: 'archive' }]));
      spyOn<any>(component, 'markAsArchive').and.callThrough();

      fixture.detectChanges();
      component.buttonMenuOptions$.subscribe(options => (buttonMenuOptionCommand = options[0].command));
      buttonMenuOptionCommand();

      discardPeriodicTasks();

      expect(component['markAsArchive']).toHaveBeenCalled();
    }));
  });

  describe('initialize filter dropdowns', () => {
    it('should not include `na` type in `sentToDpc` list on RA', () => {
      const expectedOptions: any = jasmine.arrayContaining([{ value: SendToDpcType.na, label: jasmine.anything() }]);

      fixture.detectChanges();
      expect(component.sentToDpcList).not.toEqual(expectedOptions);
    });

    it('should not include `na` or `n_a` type in `sentToDpc` list on RN', () => {
      const expectedOptions: any = jasmine.arrayContaining([
        { value: SendToDpcType.na, label: jasmine.anything() },
        { value: SendToDpcType.n_a, label: jasmine.anything() }
      ]);

      component.isRa = false;
      component.isRn = true;

      fixture.detectChanges();
      expect(component.sentToDpcList).not.toEqual(expectedOptions);
    });

    it('should not include `issued` status in `status` list on RA', () => {
      const expectedOptions: any = jasmine.arrayContaining([{ value: RefundStatus.issued, label: jasmine.anything() }]);

      fixture.detectChanges();
      expect(component.statusList).not.toEqual(expectedOptions);
    });

    it('should not include `resubmitted` status in `status` list on RN', () => {
      const expectedOptions: any = jasmine.arrayContaining([
        { value: RefundStatus.resubmitted, label: jasmine.anything() }
      ]);

      component.isRa = false;
      component.isRn = true;

      fixture.detectChanges();
      expect(component.statusList).not.toEqual(expectedOptions);
    });

    it('should not include `pendingSupervision` status in `status` list if user has not supervision permission', () => {
      const expectedOptions: any = jasmine.arrayContaining([
        { value: RefundStatus.pendingSupervision, label: jasmine.anything() }
      ]);

      fixture.detectChanges();
      expect(component.statusList).not.toEqual(expectedOptions);
    });

    it('should initialize `changedByAgent` list on RA', () => {
      fixture.detectChanges();
      expect(component.changedByAgentList).toBeDefined();
    });

    it('should NOT initialize `changedByAgent` list on RN', () => {
      component.isRa = false;
      component.isRn = true;

      fixture.detectChanges();
      expect(component.changedByAgentList).toEqual([]);
    });

    it('should initialize `sentToAirl` list on RA', () => {
      fixture.detectChanges();
      expect(component.sentToAirlList).toBeDefined();
    });

    it('should NOT initialize `sentToAirl` list on RN', () => {
      component.isRa = false;
      component.isRn = true;

      fixture.detectChanges();
      expect(component.sentToAirlList).toEqual([]);
    });

    it('should initialize `finalType` list on RA', () => {
      fixture.detectChanges();
      expect(component.finalTypeList).toBeDefined();
    });

    it('should NOT initialize `finalType` list on RN', () => {
      component.isRa = false;
      component.isRn = true;

      fixture.detectChanges();
      expect(component.finalTypeList).toEqual([]);
    });

    it('should initialize `hasInternalComment` list if user has read internal comment permission', () => {
      component.hasReadInternalCommentPermission = true;

      fixture.detectChanges();
      expect(component.hasInternalCommentList).toBeDefined();
    });

    it('should NOT initialize `hasInternalComment` list if user has NOT read internal comment permission', () => {
      fixture.detectChanges();
      expect(component.hasInternalCommentList).toEqual([]);
    });
  });

  describe('set period picker year back', () => {
    it('should set 5 years back for LEAN users', () => {
      fixture.detectChanges();
      expect(component.periodPickerYearsBack).toBe(5);
    });

    it('should set 2 years back for NOT LEAN users', () => {
      const mockUser = { ...expectedUserDetails, permissions: ['rRaQ'] };
      setStateUser(mockUser);

      fixture.detectChanges();
      expect(component.periodPickerYearsBack).toBe(2);
    });
  });

  describe('`onFilterButtonClicked` - initialize filter dropdowns', () => {
    beforeEach(() => {
      // Initialize necessary spy methods
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of());

      // Reset spy calls
      airlineDictionaryServiceSpy.getDropdownOptions.calls.reset();
      agentDictionaryServiceSpy.getDropdownOptions.calls.reset();
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.calls.reset();
    });

    it('should initialize airline filter dropdown if user is NOT an AIRLINE', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.AGENT };
      setStateUser(mockUser);

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.airlineCodesList$).toBeDefined();
      expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
    });

    it('should NOT initialize airline filter dropdown if user is an AIRLINE', () => {
      // Airline user case
      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.airlineCodesList$).toBeUndefined();
      expect(airlineDictionaryServiceSpy.getDropdownOptions).not.toHaveBeenCalled();
    });

    it('should initialize agent filter dropdown if user is NOT an AGENT', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.IATA };
      setStateUser(mockUser);

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.agentCodesList$).toBeDefined();
      expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
    });

    it('should NOT initialize agent filter dropdown if user is an AGENT', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.AGENT };
      setStateUser(mockUser);

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.agentCodesList$).toBeUndefined();
      expect(agentDictionaryServiceSpy.getDropdownOptions).not.toHaveBeenCalled();
    });

    it('should initialize again agent filter options when 1 BSP is selected after open filter section', fakeAsync(() => {
      const initializeAgentFilterDropdownSpy = spyOn<any>(component, 'initializeAgentFilterDropdown').and.callThrough();

      fixture.detectChanges();

      // Multiple BSPs available and one selected
      component.onFilterButtonClicked(true);
      initializeAgentFilterDropdownSpy.calls.reset(); // Reset calls to catch the second access after updating BSP
      component.bspControl.patchValue([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }]);

      discardPeriodicTasks();

      expect(component['initializeAgentFilterDropdown']).toHaveBeenCalled();
    }));

    it('should update agent list control value if user is multi-country', () => {
      spyOn<any>(component, 'updateAgentListControlValue').and.callThrough();

      // Multicountry user case
      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component['updateAgentListControlValue']).toHaveBeenCalled();
    });

    it('should NOT update agent list control value if user is NOT multi-country', () => {
      const mockUser = {
        ...expectedUserDetails,
        bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
      };
      setStateUser(mockUser);

      spyOn<any>(component, 'updateAgentListControlValue').and.callThrough();

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component['updateAgentListControlValue']).not.toHaveBeenCalled();
    });

    it('should initialize agent group filter dropdown if user is an AGENT GROUP with permissions', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.AGENT_GROUP };
      setStateUser(mockUser);

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.agentCodesList$).toBeDefined();
      expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith({ permission: 'rRaQ' });
    });

    it('should NOT initialize agent group filter dropdown if user is an AGENT GROUP without permissions', () => {
      const mockUser = {
        ...expectedUserDetails,
        userType: UserType.AGENT_GROUP,
        permissions: ['rRnQ']
      };
      setStateUser(mockUser);

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.agentCodesList$).toBeDefined(); // Defined as null observable
      expect(agentDictionaryServiceSpy.getDropdownOptions).not.toHaveBeenCalled();
    });

    it('should NOT initialize agent group filter dropdown if user is NOT an AGENT GROUP', () => {
      const mockUser = { ...expectedUserDetails, userType: UserType.AGENT };
      setStateUser(mockUser);

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.agentCodesList$).toBeUndefined();
      expect(agentDictionaryServiceSpy.getDropdownOptions).not.toHaveBeenCalled();
    });

    it('should initialize currency filter dropdown', () => {
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(
        of([{ value: { id: 1, code: 'EUR', decimals: 2 }, label: 'EUR' }])
      );

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component.currencyList).toEqual([{ value: { id: 1, code: 'EUR', decimals: 2 }, label: 'EUR' }]);
      expect(currencyDictionaryServiceSpy.getFilteredDropdownOptions).toHaveBeenCalled();
    });

    it('should initialize again currency filter options when 1 BSP is selected after open filter section', fakeAsync(() => {
      const initializeCurrencyFilterDropdownSpy = spyOn<any>(
        component,
        'initializeCurrencyFilterDropdown'
      ).and.callThrough();

      fixture.detectChanges();

      // Multiple BSPs available and one selected
      component.onFilterButtonClicked(true);
      initializeCurrencyFilterDropdownSpy.calls.reset(); // Reset calls to catch the second access after updating BSP
      component.bspControl.patchValue([{ id: 1, name: 'SPAIN', isoCountryCode: 'ES' }]);

      discardPeriodicTasks();

      expect(component['initializeCurrencyFilterDropdown']).toHaveBeenCalled();
    }));

    it('should call initializeBspCountriesListener', () => {
      (component as any).initializeBspCountriesListener = jasmine.createSpy();
      (component as any).initializeAgentFilterDropdown = jasmine.createSpy();
      (component as any).initializeCurrencyFilterDropdown = jasmine.createSpy();

      component.onFilterButtonClicked(true);

      expect((component as any).initializeBspCountriesListener).toHaveBeenCalled();
    });

    it('should call initializeAirlineFilterDropdown when userType is agent', () => {
      (component as any).initializeCurrencyFilterDropdown = jasmine.createSpy();
      (component as any).initializeBspCountriesListener = jasmine.createSpy();
      (component as any).initializeAirlineFilterDropdown = jasmine.createSpy();
      (component as any).userType = UserType.AGENT;

      component.onFilterButtonClicked(true);

      expect((component as any).initializeAirlineFilterDropdown).toHaveBeenCalled();
    });

    it('should call initializeAirlineFilterDropdown when userType is agent group', () => {
      (component as any).initializeCurrencyFilterDropdown = jasmine.createSpy();
      (component as any).initializeBspCountriesListener = jasmine.createSpy();
      (component as any).initializeAirlineFilterDropdown = jasmine.createSpy();
      (component as any).initializeAgentGroupFilterDropdown = jasmine.createSpy();
      (component as any).userType = UserType.AGENT_GROUP;

      component.onFilterButtonClicked(true);

      expect((component as any).initializeAgentGroupFilterDropdown).toHaveBeenCalled();
      expect((component as any).initializeAirlineFilterDropdown).toHaveBeenCalled();
    });

    it('should NOT call initializeAirlineFilterDropdown when userType is airline', () => {
      (component as any).initializeCurrencyFilterDropdown = jasmine.createSpy();
      (component as any).initializeBspCountriesListener = jasmine.createSpy();
      (component as any).initializeAirlineFilterDropdown = jasmine.createSpy();
      (component as any).userType = UserType.AIRLINE;

      component.onFilterButtonClicked(true);

      expect((component as any).initializeAirlineFilterDropdown).not.toHaveBeenCalled();
    });

    it('should update currency list control value if user is multi-country', () => {
      spyOn<any>(component, 'updateCurrencyListControlValue').and.callThrough();

      // Multicountry user case
      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component['updateCurrencyListControlValue']).toHaveBeenCalled();
    });

    it('should NOT update currency list control value if user is NOT multi-country', () => {
      const mockUser = {
        ...expectedUserDetails,
        bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
      };
      setStateUser(mockUser);

      spyOn<any>(component, 'updateCurrencyListControlValue').and.callThrough();

      fixture.detectChanges();
      component.onFilterButtonClicked(true);

      expect(component['updateCurrencyListControlValue']).not.toHaveBeenCalled();
    });
  });

  it('onDownload - shold call download', () => {
    (component as any).download = jasmine.createSpy();

    component.onDownload();

    expect((component as any).download).toHaveBeenCalled();
  });

  it('onDetailedDownload = should call download with true', () => {
    (component as any).download = jasmine.createSpy();

    component.onDetailedDownload();

    expect((component as any).download).toHaveBeenCalledWith(true);
  });

  describe('updateAgentListControlValue', () => {
    beforeEach(() => {
      component.agentListControl = new FormControl();
    });

    it('should update agentListControl value', () => {
      (component as any).bspSelectedIds = [1, 2, 3];
      component.agentListControl.patchValue([{ bsp: { id: 1 } }, { bsp: { id: 4 } }]);

      (component as any).updateAgentListControlValue();

      expect(component.agentListControl.value).not.toContain({ bsp: { id: 4 } });
    });

    it('should NOT update agentListControl value when empty bsp selected ids', () => {
      (component as any).bspSelectedIds = [];
      component.agentListControl.patchValue([{ bsp: { id: 1 } }, { bsp: { id: 4 } }]);
      component.agentListControl.patchValue = jasmine.createSpy();

      (component as any).updateAgentListControlValue();

      expect(component.agentListControl.value).toEqual([{ bsp: { id: 1 } }, { bsp: { id: 4 } }]);
      expect(component.agentListControl.patchValue).not.toHaveBeenCalled();
    });

    it('should NOT update agentListControl value when agent list is empty', () => {
      (component as any).bspSelectedIds = [1, 2];
      component.agentListControl.patchValue([]);
      component.agentListControl.patchValue = jasmine.createSpy();

      (component as any).updateAgentListControlValue();

      expect(component.agentListControl.value).toEqual([]);
      expect(component.agentListControl.patchValue).not.toHaveBeenCalled();
    });
  });

  describe('updateCurrencyListControlValue', () => {
    beforeEach(() => {
      component.currencyListControl = new FormControl();
    });

    it('should update currencyListControl value', () => {
      (component as any).bspSelectedIds = [1, 2, 3];
      (component as any).currencyList = [{ value: { id: 1 } }, { value: { id: 2 } }, { value: { id: 3 } }];
      component.currencyListControl.patchValue([{ id: 1 }, { id: 4 }]);

      (component as any).updateCurrencyListControlValue();

      expect(component.currencyListControl.value).not.toContain({ value: { id: 4 } });
    });

    it('should NOT update currencyListControl value when empty bsp selected ids', () => {
      (component as any).bspSelectedIds = [];
      (component as any).currencyList = [];
      component.currencyListControl.patchValue([{ id: 1 }, { id: 4 }]);
      component.currencyListControl.patchValue = jasmine.createSpy();

      (component as any).updateCurrencyListControlValue();

      expect(component.currencyListControl.value).toEqual([{ id: 1 }, { id: 4 }]);
      expect(component.currencyListControl.patchValue).not.toHaveBeenCalled();
    });

    it('should NOT update currencyListControl value when agent list is empty', () => {
      (component as any).bspSelectedIds = [1, 2];
      (component as any).currencyList = [{ value: { id: 1 } }, { value: { id: 2 } }];
      component.currencyListControl.patchValue([]);
      component.currencyListControl.patchValue = jasmine.createSpy();

      (component as any).updateCurrencyListControlValue();

      expect(component.currencyListControl.value).toEqual([]);
      expect(component.currencyListControl.patchValue).not.toHaveBeenCalled();
    });
  });

  describe('initialize BSP countries listener', () => {
    beforeEach(() => {
      // Initialize necessary spy methods
      currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of());
    });

    it('should populate bsp selected ids with BSPs selected', fakeAsync(() => {
      fixture.detectChanges();

      // Multiple BSPs available and one selected
      component.onFilterButtonClicked(true);

      // BSP filter single: single BSP selected
      component.bspControl.patchValue({ id: 1, name: 'SPAIN', isoCountryCode: 'ES' });

      discardPeriodicTasks();

      expect(component['bspSelectedIds']).toEqual([1]);

      // BSP filter multiple: array of BSPs selected
      component.bspControl.patchValue([{ id: 2, name: 'MALTA', isoCountryCode: 'MT' }]);

      discardPeriodicTasks();

      expect(component['bspSelectedIds']).toEqual([2]);
    }));

    it('should populate bsp selected ids as empty with no BSPs selected', fakeAsync(() => {
      fixture.detectChanges();

      // Multiple BSPs available and one selected
      component.onFilterButtonClicked(true);

      // No BSPs selected
      component.bspControl.patchValue([]);

      discardPeriodicTasks();

      expect(component['bspSelectedIds']).toEqual([]);
    }));

    it('should populate bsp selected ids as empty with null BSP', fakeAsync(() => {
      fixture.detectChanges();

      // Multiple BSPs available and one selected
      component.onFilterButtonClicked(true);

      // No BSPs selected
      component.bspControl.patchValue(null);

      discardPeriodicTasks();

      expect(component['bspSelectedIds']).toEqual([]);
    }));
  });

  it('`bulkUpdate` - should open multiple dialog with the corresponding selected rows', () => {
    fixture.detectChanges();
    component.selectedRows$ = of([{ id: 1 }]) as Observable<RefundBE[]>;
    component['bulkUpdate'](RefundActionType.delete);

    expect(refundDialogServiceSpy.openMultipleDialog).toHaveBeenCalledWith(
      RefundActionType.delete,
      'refund-application',
      [{ id: 1 }]
    );
  });

  it('`bulkDownloadAttachments` - should show a notification error when 404 occurs', () => {
    fixture.detectChanges();
    component.selectedRows$ = throwError({ status: 404 });
    component['bulkDownloadAttachments']();

    expect(notificationServiceSpy.showError).toHaveBeenCalled();
  });

  it('`bulkDownloadAttachments` - should handle error with a global behaviour when other than 404 occurs', () => {
    spyOn(component['globalErrorHandlerService'], 'handleError').and.callFake(() => {});

    fixture.detectChanges();
    component.selectedRows$ = throwError({ status: 401 });
    component['bulkDownloadAttachments']();

    expect(component['globalErrorHandlerService'].handleError).toHaveBeenCalled();
  });

  it('should translate `sentToAirline` label with `yes` value', () => {
    fixture.detectChanges();
    component.getSentToAirlineLabel('S');

    expect(translationServiceSpy.translate).toHaveBeenCalledWith(
      'REFUNDS.query.refund-application.rowDetails.yesValue'
    );
  });

  it('should translate `sentToAirline` label with `no` value', () => {
    fixture.detectChanges();
    component.getSentToAirlineLabel('N');

    expect(translationServiceSpy.translate).toHaveBeenCalledWith('REFUNDS.query.refund-application.rowDetails.noValue');
  });

  describe('onViewRefund', () => {
    beforeEach(() => {
      (component as any).router.navigate = jasmine.createSpy();
    });

    it('should navigate to refunds app view when ra is true', () => {
      component.isRa = true;

      (component as any).onViewRefund(2);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.REFUNDS_APP_VIEW.url, 2]);
    });

    it('should navigate to refunds notice view when ra is false', () => {
      component.isRa = false;

      (component as any).onViewRefund(2);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.REFUNDS_NOTICE_VIEW.url, 2]);
    });
  });

  describe('isUrgencyIconHidden', () => {
    it('should return true when isRefundPendingFromAirline false', () => {
      (component as any).isRefundPendingFromAirline = jasmine.createSpy().and.returnValue(false);

      const res = (component as any).isUrgencyIconHidden();

      expect(res).toBe(true);
    });

    it('should return true when isRefundPendingFromAirline is true and getApplicationDateDifference is 30', () => {
      (component as any).isRefundPendingFromAirline = jasmine.createSpy().and.returnValue(true);
      (component as any).getApplicationDateDifference = jasmine.createSpy().and.returnValue(30);

      const res = (component as any).isUrgencyIconHidden();

      expect(res).toBe(true);
    });

    it('should return true when isRefundPendingFromAirline is true and getApplicationDateDifference is less than 30', () => {
      (component as any).isRefundPendingFromAirline = jasmine.createSpy().and.returnValue(true);
      (component as any).getApplicationDateDifference = jasmine.createSpy().and.returnValue(10);

      const res = (component as any).isUrgencyIconHidden();

      expect(res).toBe(true);
    });

    it('should return true when isRefundPendingFromAirline is true and getApplicationDateDifference is more than 30', () => {
      (component as any).isRefundPendingFromAirline = jasmine.createSpy().and.returnValue(true);
      (component as any).getApplicationDateDifference = jasmine.createSpy().and.returnValue(50);

      const res = (component as any).isUrgencyIconHidden();

      expect(res).toBe(false);
    });
  });

  describe('isRefundUrgent', () => {
    it('should return true when getApplicationDateDifference more than 45', () => {
      (component as any).getApplicationDateDifference = jasmine.createSpy().and.returnValue(100);

      const res = (component as any).isRefundUrgent();

      expect(res).toBe(true);
    });

    it('should return false when getApplicationDateDifference is 45', () => {
      (component as any).getApplicationDateDifference = jasmine.createSpy().and.returnValue(45);

      const res = (component as any).isRefundUrgent();

      expect(res).toBe(false);
    });

    it('should return false when getApplicationDateDifference less than 45', () => {
      (component as any).getApplicationDateDifference = jasmine.createSpy().and.returnValue(30);

      const res = (component as any).isRefundUrgent();

      expect(res).toBe(false);
    });
  });

  describe('isRefundPendingFromAirline', () => {
    it('return true when pending', () => {
      const res = (component as any).isRefundPendingFromAirline({ status: RefundStatus.pending });

      expect(res).toBe(true);
    });

    it('return true when resubmitted', () => {
      const res = (component as any).isRefundPendingFromAirline({ status: RefundStatus.resubmitted });

      expect(res).toBe(true);
    });

    it('return true when investigated', () => {
      const res = (component as any).isRefundPendingFromAirline({ status: RefundStatus.investigated });

      expect(res).toBe(true);
    });

    it('return false when not pending/resubmitted/investigated', () => {
      const res = (component as any).isRefundPendingFromAirline({ status: RefundStatus.issued });

      expect(res).toBe(false);
    });
  });

  describe('getTranslatedUrgencyIconTooltip', () => {
    it('should call translate with appDateUrgencyIconTooltip', () => {
      (component as any).isRefundUrgent = jasmine.createSpy().and.returnValue(true);
      translationServiceSpy.translate = jasmine.createSpy().and.returnValue('') as any;

      (component as any).getTranslatedUrgencyIconTooltip();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(
        'REFUNDS.query.refund-application.appDateUrgencyIconTooltip'
      );
    });

    it('should call translate with appDateSemiUrgencyIconTooltip', () => {
      (component as any).isRefundUrgent = jasmine.createSpy().and.returnValue(false);
      translationServiceSpy.translate = jasmine.createSpy().and.returnValue('') as any;

      (component as any).getTranslatedUrgencyIconTooltip();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(
        'REFUNDS.query.refund-application.appDateSemiUrgencyIconTooltip'
      );
    });
  });

  it('groupActions - should return mapped actions', () => {
    const res = (component as any).groupActions([
      { action: GridTableActionType.InternalComment },
      { action: GridTableActionType.Activity }
    ]);

    expect(res).toEqual([
      { action: GridTableActionType.InternalComment, group: 'comment' },
      { action: GridTableActionType.Activity }
    ]);
  });

  describe('download', () => {
    it('should open dialog with detailedDownloadTitle when detailed ', () => {
      dialogServiceSpy.open = jasmine.createSpy() as any;
      translationServiceSpy.translate = jasmine.createSpy() as any;
      component.downloadQuery = { paginateBy: {} };

      (component as any).download(true);

      expect(translationServiceSpy.translate).toHaveBeenCalledWith(
        'REFUNDS.query.refund-application.detailedDownloadTitle'
      );
      expect(dialogServiceSpy.open).toHaveBeenCalled();
    });

    it('should open dialog with downloadTitle when NOT detailed ', () => {
      dialogServiceSpy.open = jasmine.createSpy() as any;
      translationServiceSpy.translate = jasmine.createSpy() as any;
      component.downloadQuery = { paginateBy: {} };

      (component as any).download();

      expect(translationServiceSpy.translate).toHaveBeenCalledWith('REFUNDS.query.refund-application.downloadTitle');
      expect(dialogServiceSpy.open).toHaveBeenCalled();
    });
  });

  describe('onEditRefund', () => {
    beforeEach(() => {
      (component as any).router.navigate = jasmine.createSpy();
    });

    it('should navigate to refunds app edit when ra is true', () => {
      component.isRa = true;

      (component as any).onEditRefund(3);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.REFUNDS_APP_EDIT.url, 3]);
    });

    it('should navigate to refunds notice edit when ra is false', () => {
      component.isRa = false;

      (component as any).onEditRefund(3);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.REFUNDS_NOTICE_EDIT.url, 3]);
    });
  });

  describe('onSuperviseRefund', () => {
    beforeEach(() => {
      (component as any).router.navigate = jasmine.createSpy();
    });

    it('should navigate to refunds app when ra is true', () => {
      component.isRa = true;

      (component as any).onSuperviseRefund(4);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.REFUNDS_APP_SUPERVISE.url, 4]);
    });

    it('should navigate to refunds notice when ra is false', () => {
      component.isRa = false;

      (component as any).onSuperviseRefund(4);

      expect((component as any).router.navigate).toHaveBeenCalledWith([ROUTES.REFUNDS_NOTICE_SUPERVISE.url, 4]);
    });
  });

  describe('onActionClick', () => {
    it('should call onEditRefund with item id when GridTableActionType.Edit', () => {
      (component as any).onEditRefund = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Edit }, row: { id: 5 } });

      expect((component as any).onEditRefund).toHaveBeenCalledWith(5);
    });

    it('should call onSuperviseRefund with item id when GridTableActionType.Supervise', () => {
      (component as any).onSuperviseRefund = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Supervise }, row: { id: 5 } });

      expect((component as any).onSuperviseRefund).toHaveBeenCalledWith(5);
    });

    it('should call onViewRefund with item id when GridTableActionType.Details', () => {
      (component as any).onViewRefund = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Details }, row: { id: 5 } });

      expect((component as any).onViewRefund).toHaveBeenCalledWith(5);
    });

    it('should open dialog when GridTableActionType.Delete', () => {
      refundDialogServiceSpy.openDialog = jasmine.createSpy() as any;

      component.onActionClick({ action: { actionType: GridTableActionType.Delete }, row: { id: 5 } });

      const args = refundDialogServiceSpy.openDialog.calls.first().args;

      expect(refundDialogServiceSpy.openDialog).toHaveBeenCalledTimes(1);
      expect(args[0]).toEqual(RefundActionType.delete);
      expect(args[1]).toEqual({ id: 5 });
      expect(args[2]).toEqual(MasterDataType.RefundApp);
    });

    it('should navigate to auth with item id when GridTableActionType.Authorize', () => {
      (component as any).router.navigate = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Authorize }, row: { id: 5 } });

      expect((component as any).router.navigate).toHaveBeenCalledWith([`${ROUTES.REFUNDS_APP_AUTHORIZE.url}/5`]);
    });

    it('should navigate to auth with item id when GridTableActionType.Investigate', () => {
      (component as any).router.navigate = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Investigate }, row: { id: 5 } });

      expect((component as any).router.navigate).toHaveBeenCalledWith([`${ROUTES.REFUNDS_APP_INVESTIGATE.url}/5`]);
    });

    it('should navigate to refunds with item id when GridTableActionType.Exception', () => {
      (component as any).router.navigate = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Exception }, row: { id: 5 } });

      expect((component as any).router.navigate).toHaveBeenCalledWith([`${ROUTES.REFUNDS_APP_INVESTIGATE.url}/5`]);
    });

    it('should navigate to refunds with item id when GridTableActionType.Investigate', () => {
      (component as any).router.navigate = jasmine.createSpy();

      component.onActionClick({ action: { actionType: GridTableActionType.Investigate }, row: { id: 5 } });

      expect((component as any).router.navigate).toHaveBeenCalledWith([`${ROUTES.REFUNDS_APP_INVESTIGATE.url}/5`]);
    });

    it('should open dialog when GridTableActionType.Reject', () => {
      dialogServiceSpy.open = jasmine.createSpy() as any;

      component.onActionClick({ action: { actionType: GridTableActionType.Reject }, row: { id: 5 } });

      const args = dialogServiceSpy.open.calls.first().args;
      const secondArg = args[1];

      expect(dialogServiceSpy.open).toHaveBeenCalledTimes(1);
      expect(secondArg.data.activatedRoute.data.refundType).toEqual(MasterDataType.RefundApp);
      expect(secondArg.data.rowTableContent).toEqual({ id: 5 });
    });

    it('should dispatch when GridTableActionType.Archive', () => {
      (component as any).getListKey = jasmine.createSpy().and.returnValue('test-list');
      (component as any).store.dispatch = jasmine.createSpy() as any;

      component.onActionClick({ action: { actionType: GridTableActionType.Archive }, row: { id: 5 } });

      expect((component as any).store.dispatch).toHaveBeenCalledWith({
        data: [{ id: 5 }],
        markStatus: MarkStatus.Archive,
        type: '[test-list] Mark as'
      });
    });

    it('should dispatch when GridTableActionType.MoveToActive', () => {
      (component as any).getListKey = jasmine.createSpy().and.returnValue('test-list');
      (component as any).store.dispatch = jasmine.createSpy() as any;

      component.onActionClick({ action: { actionType: GridTableActionType.MoveToActive }, row: { id: 5 } });

      expect((component as any).store.dispatch).toHaveBeenCalledWith({
        data: [{ id: 5 }],
        markStatus: MarkStatus.Read,
        type: '[test-list] Mark as'
      });
    });

    it('should dispatch when GridTableActionType.MarkAsRead', () => {
      (component as any).getListKey = jasmine.createSpy().and.returnValue('test-list');
      (component as any).store.dispatch = jasmine.createSpy() as any;

      component.onActionClick({ action: { actionType: GridTableActionType.MarkAsRead }, row: { id: 5 } });

      expect((component as any).store.dispatch).toHaveBeenCalledWith({
        data: [{ id: 5 }],
        markStatus: MarkStatus.Read,
        type: '[test-list] Mark Read/Unread Status'
      });
    });

    it('should dispatch when GridTableActionType.MarkAsUnread', () => {
      (component as any).getListKey = jasmine.createSpy().and.returnValue('test-list');
      (component as any).store.dispatch = jasmine.createSpy() as any;

      component.onActionClick({ action: { actionType: GridTableActionType.MarkAsUnread }, row: { id: 5 } });

      expect((component as any).store.dispatch).toHaveBeenCalledWith({
        data: [{ id: 5 }],
        markStatus: MarkStatus.Unread,
        type: '[test-list] Mark Read/Unread Status'
      });
    });

    it('should open dialog when GridTableActionType.InternalComment', () => {
      refundInternalCommentDialogServiceSpy.open = jasmine.createSpy() as any;

      component.onActionClick({ action: { actionType: GridTableActionType.InternalComment }, row: { id: 5 } });

      expect(refundInternalCommentDialogServiceSpy.open).toHaveBeenCalledWith({ id: 5 }, MasterDataType.RefundApp);
    });

    it('should get action command when default', () => {
      refundActionsServiceSpy.getActionCommand = jasmine.createSpy().and.returnValue(() => {}) as any;

      component.onActionClick({ action: { actionType: GridTableActionType.Loading }, row: { id: 5 } });

      expect(refundActionsServiceSpy.getActionCommand).toHaveBeenCalledWith(
        GridTableActionType.Loading,
        { id: 5 },
        MasterDataType.RefundApp
      );
    });
  });

  it('onImageClick', () => {
    refundInternalCommentDialogServiceSpy.open = jasmine.createSpy() as any;

    component.onImageClick({ row: 'row' } as any);

    expect(refundInternalCommentDialogServiceSpy.open).toHaveBeenCalledWith('row', MasterDataType.RefundApp);
  });

  // Auxiliar functions
  const setStateUser = (user: Partial<User>) => {
    const mockStore = TestBed.inject(MockStore);

    mockStore.setState({
      ...initialState,
      auth: { user }
    });
    mockStore.refreshState();
  };
});
