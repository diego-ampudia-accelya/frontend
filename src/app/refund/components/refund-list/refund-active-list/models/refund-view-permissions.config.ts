import {
  supervisionRefundPermission,
  viewRefundPermission
} from '~app/refund/shared/helpers/refund-permissions.config';

export const RA_VIEW_PERMISSIONS = [
  viewRefundPermission.raView,
  viewRefundPermission.raBillView,
  supervisionRefundPermission.raSupervision
];

export const RN_VIEW_PERMISSIONS = [
  viewRefundPermission.rnView,
  viewRefundPermission.rnBillView,
  supervisionRefundPermission.rnSupervision
];
