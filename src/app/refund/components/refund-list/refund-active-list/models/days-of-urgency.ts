export const MIN_DAYS_TO_MARK_REFUND_AS_URGENT = 45; // the minimum number of days passed since the refund date to marked it as urgent
export const MAX_DAYS_TO_HIDE_URGENT_ICON = 30; // the maximum number of days that have passed since the return date to hide the urgency icon
