import { Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { mapValues } from 'lodash';
import moment from 'moment-mini';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { SEARCH_FORM_DEFAULT_VALUE, SHARED_RA, SHARED_RN } from '../refund-list.constants';
import { MAX_DAYS_TO_HIDE_URGENT_ICON, MIN_DAYS_TO_MARK_REFUND_AS_URGENT } from './models/days-of-urgency';
import { LEAN_USERS_YEARS, NON_LEAN_USERS_YEARS } from './models/period-picker-users-years';
import { RA_VIEW_PERMISSIONS, RN_VIEW_PERMISSIONS } from './models/refund-view-permissions.config';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { RefundRejectDialogComponent } from '~app/refund/components/dialogs/refund-reject-dialog/refund-reject-dialog.component';
import {
  RefundStatus,
  SendToDpcType,
  StatisticalCodeOptions,
  YesNoShortType
} from '~app/refund/models/refund-be-aux.model';
import { RefundFilter } from '~app/refund/models/refund-filter.model';
import { RefundOptionType } from '~app/refund/models/refund-shared.model';
import { RefundActionType, RefundBE, RefundFinal, RefundFormOfPayment } from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDialogService } from '~app/refund/services/refund-dialog.service';
import { RefundFilterFormatter } from '~app/refund/services/refund-filter-formatter.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import {
  getBspFilterRequiredPermissions,
  getRefundReadInternalCommentPermission,
  getRefundViewPermission,
  getSupervisionRefundPermissions
} from '~app/refund/shared/helpers/refund-permissions.config';
import { activeKeys, activeSelectors, State } from '~app/refund/store/reducers';
import { ListSubtabsActions } from '~app/shared/base/list-subtabs/actions';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ButtonDesign, DialogService, DownloadFileComponent, FooterButton, PeriodOption } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { RangeDropdownUnit } from '~app/shared/components/range-dropdown/models/range-dropdown.model';
import { Permissions } from '~app/shared/constants';
import { ROUTES } from '~app/shared/constants/routes';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { ServerError } from '~app/shared/errors';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, BadgeInfo, GridColumn, IconType } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { MarkStatus, MarkStatusFilter } from '~app/shared/models/mark-status.model';
import { User, UserType } from '~app/shared/models/user.model';
import { EMPTY_VALUE } from '~app/shared/pipes/empty.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  CurrencyDictionaryService,
  NotificationService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-refund-active-list',
  templateUrl: './refund-active-list.component.html',
  styleUrls: ['./refund-active-list.component.scss'],
  providers: [RefundFilterFormatter, RefundService]
})
export class RefundActiveListComponent extends ListSubtabs<RefundBE, RefundFilter> implements OnInit, OnDestroy {
  public refundType: MasterDataType;

  public isRa: boolean;
  public isRn: boolean;

  public masterDataTypes = MasterDataType;
  public userTypes = UserType;
  public btnDesign = ButtonDesign;
  public rangeDropdownUnit = RangeDropdownUnit;

  public tableTitle: string;

  public isArchivedSubTab = false;
  public isUserAgent: boolean;
  public isUserAirline: boolean;

  public searchForm: FormGroup;
  public bspControl: AbstractControl;
  public periodControl: AbstractControl;
  public agentListControl: AbstractControl;
  public currencyListControl: AbstractControl;

  public predefinedFilters: Partial<RefundFilter>;
  public buttonMenuOptions$: Observable<ButtonMenuOption[]>;
  public listActions$: Observable<{ action: GridTableActionType; disabled?: boolean }[]>;
  public columns: Array<GridColumn> = [];
  public downloadQuery: Partial<DataQuery>;

  public airlineCodesList$: Observable<DropdownOption<AirlineSummary>[]>;
  public agentCodesList$: Observable<DropdownOption<AgentSummary>[]>;
  public currencyList: DropdownOption<Currency>[] = [];
  public bspCountriesList: DropdownOption<BspDto>[] = [];
  public periodOptions$: Observable<PeriodOption[]>;
  public statusList: DropdownOption<RefundStatus>[] = [];
  public sentToDpcList: DropdownOption<SendToDpcType>[] = [];
  public sentToAirlList: DropdownOption<YesNoShortType>[] = [];
  public changedByAgentList: DropdownOption<YesNoShortType>[] = [];
  public hasInternalCommentList: DropdownOption<boolean>[] = [];
  public formsOfPaymentList: DropdownOption<RefundFormOfPayment>[] = [];
  public markStatusList: DropdownOption<MarkStatusFilter>[] = [];
  public finalTypeList: DropdownOption<RefundFinal>[] = [];
  public statisticalCodeList: DropdownOption<StatisticalCodeOptions>[] = [];

  public hasViewPermission = false;

  public hasReadInternalCommentPermission = false;

  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;
  public isPeriodDisabled = true;
  public isCurrencyControlDisabled: boolean;
  public periodPickerYearsBack: number;

  private hasLeanPermission: boolean;
  private viewPermission: string[];

  private bspSelectedIds: number[] = [];

  private loggedUser: User;
  private userType: string;
  private isMultiCountryUser: boolean;

  private get checkboxColumn(): GridColumn {
    return {
      prop: 'isRowSelected',
      maxWidth: 40,
      sortable: false,
      name: '',
      headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
      cellTemplate: 'defaultLabeledCheckboxCellTmpl'
    };
  }

  constructor(
    protected store: Store<AppState>,
    protected refundService: RefundService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    public displayFormatter: RefundFilterFormatter,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected fb: FormBuilder,
    protected agentDictionaryService: AgentDictionaryService,
    protected airlineDictionaryService: AirlineDictionaryService,
    protected currencyDictionaryService: CurrencyDictionaryService,
    protected periodService: PeriodService,
    protected dialogService: DialogService,
    protected refundDialogService: RefundDialogService,
    protected notificationService: NotificationService,
    protected permissionsService: PermissionsService,
    protected refundActionsService: RefundActionsService,
    protected localBspTimeService: LocalBspTimeService,
    protected globalErrorHandlerService: ErrorHandler,
    protected refundInternalCommentDialogService: RefundInternalCommentDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(store, refundService, actions$, translationService, notificationService);

    this.setDataFromRoute();
    this.setViewPermissions();
  }

  private setDataFromRoute(): void {
    const data = this.activatedRoute.snapshot.data;

    // Refund type initialization
    this.refundType = data.refundType;
    this.isRa = this.refundType === MasterDataType.RefundApp;
    this.isRn = this.refundType === MasterDataType.RefundNotice;

    // Grid table variables initialization
    this.tableTitle = `REFUNDS.query.${this.refundType}.listTitle`;
    this.initialSortAttribute = this.isRa ? 'dateOfApplication' : 'dateOfIssue';
  }

  private setViewPermissions(): void {
    this.viewPermission = this.isRa ? RA_VIEW_PERMISSIONS : RN_VIEW_PERMISSIONS;
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<RefundBE, RefundFilter>,
    DefaultProjectorFn<ListSubtabsState<RefundBE, RefundFilter>>
  > {
    return activeSelectors[this.refundType];
  }

  protected getListKey(): string {
    return activeKeys[this.refundType];
  }

  protected adaptSearchQuery(query: DataQuery<RefundFilter>): DataQuery<RefundFilter> {
    return {
      ...query,
      filterBy: {
        ...query.filterBy,
        // With "!" at the first status we are negating the following status for the API call
        status: query.filterBy.status || [`!${RefundStatus.deleted}`, RefundStatus.supervisionDeleted],
        markStatus: query.filterBy.markStatus || [`${MarkStatus.Read}, ${MarkStatus.Unread}`]
      }
    };
  }

  public ngOnInit(): void {
    this.setInitialFilterValues();

    this.initializePermissions();

    // Initialize grid table view
    this.loading$ = of(true);
    this.initializeColumns();

    this.initializeLoggedUser$()
      .pipe(
        switchMap(() => this.initializeLeanBspFilter$()),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        super.ngOnInit();

        this.initializeQueryListener();

        // Initialize columns again with user info
        this.initializeColumns();

        // Initialize period control
        this.updatePeriodControlDisability();
        this.updatePeriodFilterOptions();
      });

    // Initialize list view
    this.initializeButtonMenuOptions();
    this.initializeFilterDropdowns();

    this.setPeriodPickerYearsBack();
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      switch (this.userType) {
        case UserType.AGENT:
          this.initializeAirlineFilterDropdown();
          break;
        case UserType.AIRLINE:
          this.initializeAgentFilterDropdown();
          break;
        case UserType.AGENT_GROUP:
          this.initializeAgentGroupFilterDropdown();
          this.initializeAirlineFilterDropdown();
          break;
        default:
          this.initializeAirlineFilterDropdown();
          this.initializeAgentFilterDropdown();
      }

      this.initializeCurrencyFilterDropdown();
      this.initializeBspCountriesListener();
    }
  }

  public onDownload(): void {
    this.download();
  }

  public onDetailedDownload(): void {
    this.download(true);
  }

  private initializeBspCountriesListener(): void {
    this.bspControl.valueChanges
      .pipe(
        tap(bsps => this.updateBspSelectedIds(bsps)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.initializeAgentFilterDropdown();
        this.initializeCurrencyFilterDropdown();

        this.updatePeriodControlDisability();
        this.updatePeriodFilterOptions();
      });
  }

  private updateBspSelectedIds(bsps: Bsp): void {
    if (bsps) {
      this.bspSelectedIds = Array.isArray(bsps) ? bsps.map(bsp => bsp.id) : [bsps.id];
    } else {
      this.bspSelectedIds = [];
    }
  }

  private initializeAgentFilterDropdown(): void {
    this.initializeAgentList();

    if (this.isMultiCountryUser) {
      // If multicountry user, agent list should changed on BSP selection
      this.updateAgentListControlValue();
    }
  }

  private initializeAgentList(): void {
    const param = { ...(this.bspSelectedIds.length && { bspId: this.bspSelectedIds }) };

    this.agentCodesList$ = this.agentDictionaryService.getDropdownOptions(param);
  }

  private updateAgentListControlValue(): void {
    const agentsSelected: AgentSummary[] = this.agentListControl.value;

    if (this.bspSelectedIds.length && agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.bspSelectedIds.some(bspId => agent.bsp?.id === bspId));
      this.agentListControl.patchValue(agentsToPatch);
    }
  }

  private initializeCurrencyFilterDropdown(): void {
    this.initializeCurrencyList();

    if (this.isMultiCountryUser) {
      // If multicountry user, currency list should changed on BSP selection
      this.updateCurrencyListControlValue();
    }
  }

  private initializeCurrencyList(): void {
    const param = { ...(this.bspSelectedIds.length && { bspId: this.bspSelectedIds }) };

    this.currencyDictionaryService
      .getFilteredDropdownOptions(param)
      .subscribe(currencyList => (this.currencyList = currencyList));
  }

  private updateCurrencyListControlValue(): void {
    const currenciesSelected: Currency[] = this.currencyListControl.value;

    if (this.bspSelectedIds.length && currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyList.some(cur => cur.value?.id === currency.id)
      );
      this.currencyListControl.patchValue(currenciesToPatch);
    }
  }

  private updatePeriodControlDisability(): void {
    this.isPeriodDisabled = this.isBspFilterMultiple && this.bspSelectedIds.length !== 1;
    if (this.isPeriodDisabled) {
      this.periodControl.reset();
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? LEAN_USERS_YEARS : NON_LEAN_USERS_YEARS;
  }

  private initializePermissions(): void {
    this.hasViewPermission = this.permissionsService.hasPermission(this.viewPermission);
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    // Internal comment permissions
    this.hasReadInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundReadInternalCommentPermission(this.refundType)
    );
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, getBspFilterRequiredPermissions(this.refundType))
      .pipe(
        tap(bspList => {
          // Update BSP selected ids and predefined filters with the default BSP
          const onlyOneBsp = bspList.length === 1;

          if (bspList.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);

            const firstListBsp = bspList[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelectedIds = [filterValue.id];

            // Agent users have not predefined filters
            if (!this.isUserAgent) {
              this.predefinedFilters = {
                ...this.predefinedFilters,
                bsp: filterValue
              };
            }
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspList => (this.isBspFilterMultiple = this.hasLeanPermission && bspList.length !== 1)),
        tap(bspList => (this.isBspFilterLocked = bspList.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  private initializeAirlineFilterDropdown(): void {
    this.airlineCodesList$ = this.airlineDictionaryService.getDropdownOptions();
  }

  private initializeAgentGroupFilterDropdown(): void {
    const aGroupUserPermission = getRefundViewPermission(this.refundType);
    const aGroupUserHasPermission = this.loggedUser.permissions.some(perm => aGroupUserPermission === perm);
    this.agentCodesList$ = aGroupUserHasPermission
      ? this.agentDictionaryService.getDropdownOptions({ permission: aGroupUserPermission })
      : of(null);
  }

  private updatePeriodFilterOptions(): void {
    if (this.bspSelectedIds.length === 1) {
      this.periodOptions$ = this.periodService.getByBsp(this.bspSelectedIds[0]);
    }
  }

  private initializeColumns(): void {
    this.columns = this.isRa ? SHARED_RA.ACTIVE_COLUMNS : SHARED_RN.ACTIVE_COLUMNS;

    if (this.hasViewPermission) {
      this.columns = [this.checkboxColumn, ...this.columns];
    }

    if (this.isUserAgent) {
      this.columns = this.columns.filter(
        column => column.prop !== 'agent.bsp.name' && column.prop !== 'agent.iataCode'
      );
    }

    if (this.isUserAirline) {
      this.columns = this.columns.filter(column => column.prop !== 'airline.iataCode');
    }

    this.initializeStatusBadgeInfo();
    this.setFinalTypeColumn();
    this.setFinalDaysLeftColumn();
    this.setApplicationDateColumn();
    this.setBspColumn();

    if (this.hasReadInternalCommentPermission) {
      this.setDocumentNumberColumn();
    }
  }

  private initializeLoggedUser$(): Observable<any> {
    return this.store.pipe(select(getUser), first()).pipe(
      tap(user => {
        this.loggedUser = user;
        this.userType = user.userType;
        this.isUserAgent = this.userType === UserType.AGENT;
        this.isUserAirline = this.userType === UserType.AIRLINE;
        this.isMultiCountryUser = this.loggedUser.bsps?.length > 1;
      })
    );
  }

  private initializeQueryListener(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.downloadQuery = this.adaptSearchQuery(query)));
  }

  private setInitialFilterValues(): void {
    this.searchForm = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));

    this.bspControl = FormUtil.get<RefundFilter>(this.searchForm, 'bsp');
    this.periodControl = FormUtil.get<RefundFilter>(this.searchForm, 'period');
    this.agentListControl = FormUtil.get<RefundFilter>(this.searchForm, 'agentId');
    this.currencyListControl = FormUtil.get<RefundFilter>(this.searchForm, 'currencyId');
  }

  private initializeFilterDropdowns(): void {
    this.sentToDpcList = Object.values(SendToDpcType)
      .filter(sentStatus =>
        this.isRa
          ? sentStatus !== SendToDpcType.na
          : sentStatus !== SendToDpcType.na && sentStatus !== SendToDpcType.n_a
      )
      .map(item => ({
        value: item,
        label: this.translationService.translate(`REFUNDS.sentToDpcStatus.${item}`)
      }));

    this.formsOfPaymentList = Object.values(RefundFormOfPayment).map(item => ({
      value: item,
      label: this.translationService.translate(`REFUNDS.formsOfPayment.${item}`)
    }));

    this.statusList = Object.values(RefundStatus)
      .filter(
        status =>
          status !== RefundStatus.deleted &&
          status !== RefundStatus.supervisionDeleted &&
          this.filterStatusByTypePermission(status)
      )
      .map(item => ({
        value: item,
        label: this.translationService.translate(`REFUNDS.status.${item}`)
      }));

    this.markStatusList = Object.values(MarkStatusFilter).map(item => ({
      value: item,
      label: this.translationService.translate(`REFUNDS.markStatus.${item}`)
    }));

    if (this.isRa) {
      this.changedByAgentList = Object.values(YesNoShortType).map(item => ({
        label: this.translationService.translate(`REFUNDS.query.commons.yesNoShortValue.${item}`),
        value: item
      }));

      this.sentToAirlList = Object.values(YesNoShortType).map(item => ({
        label: this.translationService.translate(`REFUNDS.query.commons.yesNoShortValue.${item}`),
        value: item
      }));

      this.finalTypeList = Object.values(RefundFinal).map(item => ({
        value: item,
        label: this.translationService.translate(`REFUNDS.finalType.${item}`)
      }));
      this.finalTypeList.unshift({ value: null, label: EMPTY_VALUE });

      this.statisticalCodeList = Object.values(StatisticalCodeOptions).map(item => ({
        label: this.translationService.translate(`REFUNDS.query.commons.StatisticalCodeOptions.${item}`),
        value: item
      }));
    }

    if (this.hasReadInternalCommentPermission) {
      this.hasInternalCommentList = [true, false].map(item => ({
        label: this.translationService.translate(`REFUNDS.query.commons.yesNoBooleanValue.${item}`),
        value: item
      }));
    }
  }

  private filterStatusByTypePermission(status: RefundStatus): boolean {
    let result: boolean;

    switch (status) {
      case RefundStatus.authorized:
      case RefundStatus.investigated:
      case RefundStatus.pending:
      case RefundStatus.rejected:
      case RefundStatus.resubmitted:
        result = this.isRa;
        break;

      case RefundStatus.issued:
        result = this.isRn;
        break;

      case RefundStatus.pendingSupervision:
        result = this.loggedUser.permissions.some(perm => getSupervisionRefundPermissions().includes(perm));
        break;
    }

    return result;
  }

  private initializeStatusBadgeInfo(): void {
    const statusColumn = this.columns.find(column => column.prop === 'status');

    if (!statusColumn) {
      return;
    }

    statusColumn.pipe = {
      transform: value => this.translationService.translate(`REFUNDS.status.${value}`)
    };

    const shortName: string = this.translationService.translate(`REFUNDS.shortName.${this.refundType}`);

    if (this.isRa) {
      statusColumn.badgeInfo = this.getBagdeInfoIfRa(shortName);

      statusColumn.image = (value: RefundBE) => {
        let img = null;
        if (value.changedByAgent === 'S') {
          img = {
            src: 'assets/images/edit-doc.svg',
            tooltipLabel: this.translationService.translate(`REFUNDS.query.${this.refundType}.changedByAgentTooltip`)
          };
        }

        return img;
      };
    }

    if (this.isRn) {
      statusColumn.badgeInfo = this.getBadgeInfoIfRn(shortName);
    }
  }

  private getBagdeInfoIfRa(shortName: string): BadgeInfo {
    return {
      hidden: (value: RefundBE) => !this.isStatusRejectedOrAuthorized(value.status),
      value: this.translationService.translate(`REFUNDS.badgeInfo.label`),
      type: (value: RefundBE) => {
        if (this.isStatusRejectedOrAuthorized(value.status)) {
          return this.getBadgeInfoType(value.sentToDpc);
        }

        return null;
      },
      tooltipLabel: (value: RefundBE) => {
        if (value.sentToDpc && this.isStatusRejectedOrAuthorized(value.status)) {
          return this.translationService.translate(`REFUNDS.badgeInfo.leyend.${value.sentToDpc.toUpperCase()}`, {
            refundType: shortName
          });
        }

        return '';
      },
      showIconType: true
    };
  }

  private isStatusRejectedOrAuthorized(status: RefundStatus): boolean {
    return status === RefundStatus.authorized || status === RefundStatus.rejected;
  }

  private getBadgeInfoIfRn(shortName: string): BadgeInfo {
    return {
      hidden: () => false,
      value: this.translationService.translate(`REFUNDS.badgeInfo.label`),
      type: (value: RefundBE) => this.getBadgeInfoType(value.sentToDpc),
      tooltipLabel: (value: RefundBE) =>
        this.translationService.translate(`REFUNDS.badgeInfo.leyend.${value.sentToDpc.toUpperCase()}`, {
          refundType: shortName
        }),
      showIconType: true
    };
  }

  private getBadgeInfoType(sendToDpcType: SendToDpcType): BadgeInfoType {
    switch (sendToDpcType) {
      case SendToDpcType.pending:
        return BadgeInfoType.info;

      case SendToDpcType.sent:
        return BadgeInfoType.success;

      case SendToDpcType.na:
      case SendToDpcType.n_a:
      default:
        return BadgeInfoType.regular;
    }
  }

  private setBspColumn(): void {
    const bspColumn = this.columns.find(column => column.prop === 'agent.bsp.isoCountryCode');

    if (bspColumn) {
      bspColumn.sortable = this.hasLeanPermission;
    }
  }

  private setFinalTypeColumn(): void {
    const finalTypeColumn = this.columns.find(column => column.prop === 'finalType');

    if (finalTypeColumn) {
      finalTypeColumn.pipe = {
        transform: value => (value ? this.translationService.translate(`REFUNDS.finalType.${value}`) : null)
      };
    }
  }

  private setFinalDaysLeftColumn(): void {
    const finalDaysLeftColumn = this.columns.find(column => column.prop === 'finalDaysLeft');

    if (finalDaysLeftColumn) {
      finalDaysLeftColumn.icon = refund => ({
        name: 'check_circle',
        type: IconType.Info,
        hidden: refund.finalProcessStopped !== 0,
        tooltip: this.translationService.translate(`REFUNDS.query.${this.refundType}.daysLeftIconTooltip`)
      });
    }
  }

  private setApplicationDateColumn(): void {
    const applicationDateColumn = this.columns.find(column => column.prop === 'dateOfApplication');

    if (applicationDateColumn) {
      applicationDateColumn.icon = refund => ({
        name: this.isRefundUrgent(refund) ? 'error' : 'warning',
        type: this.isRefundUrgent(refund) ? IconType.Error : IconType.Warning,
        tooltip: this.getTranslatedUrgencyIconTooltip(refund),
        hidden: this.isUrgencyIconHidden(refund)
      });
    }
  }

  private setDocumentNumberColumn(): void {
    const documentNumberColumn = this.columns.find(column => column.prop === 'ticketDocumentNumber');

    if (documentNumberColumn) {
      documentNumberColumn.image = (refund: RefundBE) =>
        refund.internalComment
          ? {
              src: '/assets/images/utils/discuss.svg',
              tooltipLabel: this.translationService.translate(`REFUNDS.query.commons.internalCommentTooltip`)
            }
          : null;

      documentNumberColumn.icon = (refund: RefundBE) => ({
        class: 'material-icons-outlined',
        name: 'remove_red_eye',
        tooltip: this.translationService.translate(`watching`),
        hidden: !refund.watching
      });
    }
  }

  private initializeButtonMenuOptions(): void {
    this.buttonMenuOptions$ = this.refundService.getRefundMenuOptions(this.isArchivedSubTab).pipe(
      map(options =>
        options.map(option => ({
          ...option,
          label: this.getTranslatedButtonMenuOptionLabel(option),
          group: this.setButtonMenuOptionGroup(option),
          command: this.setButtonMenuOptionCommand(option)
        }))
      )
    );
  }

  private getTranslatedButtonMenuOptionLabel(option: ButtonMenuOption): string {
    const pathTranslation = this.isArchivedSubTab
      ? `REFUNDS.query.tableSelectionToolbar.buttonMenuOptionsArchive.${option.label}`
      : `REFUNDS.query.tableSelectionToolbar.buttonMenuOptions.${option.label}`;

    return this.translationService.translate(pathTranslation);
  }

  private setButtonMenuOptionGroup(option: ButtonMenuOption): string {
    let group: string;

    switch (option.id) {
      case RefundOptionType.Read:
      case RefundOptionType.Unread:
      case RefundOptionType.Archive:
        group = 'markAs';
        break;
      case RefundOptionType.Authorize:
      case RefundOptionType.Reject:
      case RefundOptionType.Supervise:
      case RefundOptionType.Delete:
      case RefundOptionType.Comment:
        group = 'update';
        break;
      case RefundOptionType.DownloadAttachments:
        group = 'download';
        break;
    }

    return group;
  }

  private setButtonMenuOptionCommand(option: ButtonMenuOption): () => void {
    let command: () => void;

    switch (option.id) {
      case RefundOptionType.Read:
        if (this.isArchivedSubTab) {
          command = () => this.moveToActive();
        } else {
          command = () => this.setMarkStatus(MarkStatus.Read);
        }
        break;
      case RefundOptionType.Unread:
        command = () => this.setMarkStatus(MarkStatus.Unread);
        break;
      case RefundOptionType.Archive:
        command = () => this.markAsArchive();
        break;
      case RefundOptionType.Authorize:
        command = () => this.bulkUpdate(RefundActionType.authorize);
        break;
      case RefundOptionType.Reject:
        command = () => this.bulkUpdate(RefundActionType.reject);
        break;
      case RefundOptionType.Supervise:
        command = () => this.bulkUpdate(RefundActionType.supervise);
        break;
      case RefundOptionType.Delete:
        command = () => this.bulkUpdate(RefundActionType.delete);
        break;
      case RefundOptionType.Comment:
        command = () => this.bulkUpdate(RefundActionType.comment);
        break;
      case RefundOptionType.DownloadAttachments:
        command = () => this.bulkDownloadAttachments();
        break;
    }

    return command;
  }

  private bulkUpdate(actionType: RefundActionType): void {
    this.selectedRows$
      .pipe(first())
      .subscribe(rows => this.refundDialogService.openMultipleDialog(actionType, this.refundType, rows));
  }

  private bulkDownloadAttachments(): void {
    this.selectedRows$
      .pipe(
        first(),
        switchMap(rows => combineLatest([of(rows), this.refundService.bulkDownloadAttachments(rows)]))
      )
      .subscribe(
        ([rows, file]) => this.onDownloadSuccess(rows, file),
        error => this.onDownloadError(error)
      );
  }

  private onDownloadSuccess(rows: RefundBE[], file: { blob: Blob; fileName: string }): void {
    FileSaver.saveAs(file.blob, file.fileName);
    this.notificationService.showSuccess(
      this.translationService.translate(`REFUNDS.success_msg.${this.refundType}.downloadAttachments`, {
        items: rows.length
      })
    );
  }

  private onDownloadError(error: ServerError): void {
    if (error.status === 404) {
      this.notificationService.showError(this.translationService.translate(`REFUNDS.error_msg.downloadAttachments`));
    } else {
      this.globalErrorHandlerService.handleError(error);
    }
  }

  private isRefundUrgent(refund: RefundBE): boolean {
    return this.getApplicationDateDifference(refund) > MIN_DAYS_TO_MARK_REFUND_AS_URGENT;
  }

  /** Get difference between BSP date and refund application date in days */
  private getApplicationDateDifference(refund: RefundBE): number {
    return moment(this.localBspTimeService.getDate()).diff(refund.dateOfApplication, 'days');
  }

  private isUrgencyIconHidden(refund: RefundBE): boolean {
    return (
      !this.isRefundPendingFromAirline(refund) ||
      this.getApplicationDateDifference(refund) <= MAX_DAYS_TO_HIDE_URGENT_ICON
    );
  }

  private isRefundPendingFromAirline(refund: RefundBE): boolean {
    return (
      refund.status === RefundStatus.pending ||
      refund.status === RefundStatus.resubmitted ||
      refund.status === RefundStatus.investigated
    );
  }

  private getTranslatedUrgencyIconTooltip(refund: RefundBE): string {
    const suffix = this.isRefundUrgent(refund) ? 'appDateUrgencyIconTooltip' : 'appDateSemiUrgencyIconTooltip';

    return this.translationService.translate(`REFUNDS.query.${this.refundType}.${suffix}`);
  }

  public getSentToAirlineLabel(sentToAirl: string): string {
    return sentToAirl === 'S'
      ? this.translationService.translate(`REFUNDS.query.${this.refundType}.rowDetails.yesValue`)
      : this.translationService.translate(`REFUNDS.query.${this.refundType}.rowDetails.noValue`);
  }

  public onGetActionList(refund: RefundBE & { markStatus: string }): void {
    // TODO Adapt to RN when is needed
    this.listActions$ = this.refundService.getRefundActionList(refund.id).pipe(
      map(actions => {
        let modifiedActions: any = actions;
        const hasLoadingAction = actions.some(action => action.action === GridTableActionType.Loading);

        if (!hasLoadingAction && this.hasViewPermission) {
          const readUnreadAction =
            refund.markStatus === MarkStatus.Read ? GridTableActionType.MarkAsUnread : GridTableActionType.MarkAsRead;

          const watchingAction = refund.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;
          const isAirlineOrIataUserType = [this.userTypes.AIRLINE, this.userTypes.IATA].includes(
            this.userType as UserType
          );

          modifiedActions = [
            ...modifiedActions,
            { action: readUnreadAction, group: 'markStatus' },
            { action: GridTableActionType.Archive, group: 'markStatus' }
          ];

          if (!isAirlineOrIataUserType) {
            modifiedActions.push({ action: watchingAction, group: 'markStatus' });
          }
        }

        return this.groupActions(modifiedActions);
      })
    );
  }

  public onViewRefund(id: number): void {
    const url = this.isRa ? ROUTES.REFUNDS_APP_VIEW.url : ROUTES.REFUNDS_NOTICE_VIEW.url;

    this.router.navigate([url, id]);
  }

  protected groupActions(
    actions: { action: GridTableActionType; disabled?: boolean; group?: string }[]
  ): { action: GridTableActionType; disabled?: boolean; group?: string }[] {
    // For now, we just need to group `internal comment` action
    return actions.map(action =>
      action.action === GridTableActionType.InternalComment ? { ...action, group: 'comment' } : action
    );
  }

  private download(detailed?: boolean): void {
    const modalTitle = detailed
      ? `REFUNDS.query.${this.refundType}.detailedDownloadTitle`
      : `REFUNDS.query.${this.refundType}.downloadTitle`;

    const data = {
      title: this.translationService.translate(modalTitle),
      footerButtonsType: FooterButton.Download,
      downloadQuery: this.downloadQuery,
      totalElements: this.downloadQuery.paginateBy.totalElements,
      detailedDownload: detailed
    };

    this.dialogService.open(DownloadFileComponent, {
      data,
      apiService: this.refundService
    });
  }

  private onEditRefund(id: number): void {
    const url = this.isRa ? ROUTES.REFUNDS_APP_EDIT.url : ROUTES.REFUNDS_NOTICE_EDIT.url;

    this.router.navigate([url, id]);
  }

  private onSuperviseRefund(id: number): void {
    const url = this.isRa ? ROUTES.REFUNDS_APP_SUPERVISE.url : ROUTES.REFUNDS_NOTICE_SUPERVISE.url;

    this.router.navigate([url, id]);
  }

  public onActionClick(event): void {
    const actionType: GridTableActionType = event.action.actionType;
    const item: RefundBE = event.row;
    let queryUrl = '';
    let url = '';

    switch (actionType) {
      case GridTableActionType.Edit:
        this.onEditRefund(item.id);
        break;
      case GridTableActionType.Supervise:
        this.onSuperviseRefund(item.id);
        break;
      case GridTableActionType.Details:
        this.onViewRefund(item.id);
        break;
      case GridTableActionType.Delete:
        this.refundDialogService.openDialog(RefundActionType.delete, item, this.refundType, new FormGroup({}));
        break;
      case GridTableActionType.Authorize:
        queryUrl = ROUTES.REFUNDS_APP_AUTHORIZE.url;

        url = `${queryUrl}/${item.id}`;
        this.router.navigate([url]);
        break;
      case GridTableActionType.Investigate:
      case GridTableActionType.Exception:
        queryUrl = ROUTES.REFUNDS_APP_INVESTIGATE.url;

        url = `${queryUrl}/${item.id}`;
        this.router.navigate([url]);
        break;
      case GridTableActionType.Reject:
        // TODO Use confirmation dialog
        this.dialogService.open(RefundRejectDialogComponent, {
          data: {
            activatedRoute: {
              data: { refundType: this.refundType }
            },
            footerButtonsType: FooterButton.Reject,
            rowTableContent: item
          }
        });
        break;
      case GridTableActionType.Archive:
        this.store.dispatch(
          ListSubtabsActions.markArchiveMoveToActive(this.getListKey())({
            data: [item],
            markStatus: MarkStatus.Archive
          })
        );

        break;
      case GridTableActionType.MoveToActive:
        this.store.dispatch(
          ListSubtabsActions.markArchiveMoveToActive(this.getListKey())({
            data: [item],
            markStatus: MarkStatus.Read
          })
        );

        break;
      case GridTableActionType.MarkAsRead:
        this.store.dispatch(
          ListSubtabsActions.markReadUnreadStatus(this.getListKey())({
            data: [item],
            markStatus: MarkStatus.Read
          })
        );
        break;
      case GridTableActionType.MarkAsUnread:
        this.store.dispatch(
          ListSubtabsActions.markReadUnreadStatus(this.getListKey())({
            data: [item],
            markStatus: MarkStatus.Unread
          })
        );
        break;
      case GridTableActionType.InternalComment:
        this.refundInternalCommentDialogService.open(item, this.refundType);
        break;
      case GridTableActionType.StartWatching:
        this.store.dispatch(
          ListSubtabsActions.updateWatcher(this.getListKey())({
            data: [item],
            watching: true
          })
        );
        break;
      case GridTableActionType.StopWatching:
        this.store.dispatch(
          ListSubtabsActions.updateWatcher(this.getListKey())({
            data: [item],
            watching: false
          })
        );
        break;
      default:
        this.refundActionsService.getActionCommand(actionType, item, this.refundType)();
        break;
    }
  }

  public onImageClick(event: { event: Event; row: RefundBE; column: GridColumn }): void {
    const { row } = event;

    this.refundInternalCommentDialogService.open(row, this.refundType);
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
