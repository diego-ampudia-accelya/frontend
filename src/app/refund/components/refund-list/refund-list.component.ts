import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { MenuItem } from 'primeng/api';
import { combineLatest, Observable, of } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { RefundBspSelectionDialogComponent } from '../dialogs/refund-bsp-selection-dialog/refund-bsp-selection-dialog.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { getMenuTabs } from '~app/core/reducers';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { RefundActionType } from '~app/refund/models/refund.model';
import {
  createRefundPermission,
  createRefundPermissionUnderSupervision,
  getIssueRefundPendingSupervisionPermission,
  getRefundCreatePermission
} from '~app/refund/shared/helpers/refund-permissions.config';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { BspDto } from '~app/shared/models/bsp.model';
import { User } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-refund-list',
  templateUrl: './refund-list.component.html',
  styleUrls: ['./refund-list.component.scss']
})
export class RefundListComponent implements OnInit {
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  public loggedUser: User;

  public tabs: RoutedMenuItem[];

  public refundType: MasterDataType;

  public title: string;

  public btnDesign = ButtonDesign;

  public issueBtnLbl: string;
  public issueSupervisionBtnLbl: string;
  public showIssueBtn: boolean;
  public showIssuePendingBtn: boolean;
  public issueOptionActions: { options: MenuItem[]; mainOption: MenuItem };

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private translationService: L10nTranslationService,
    private router: Router,
    private menuBuilder: MenuBuilder,
    private dialogService: DialogService,
    private bspsDictionaryService: BspsDictionaryService
  ) {}

  public ngOnInit(): void {
    const data = this.activatedRoute.snapshot.data;

    this.refundType = data.refundType;
    this.title = `REFUNDS.query.${this.refundType}.title`;
    this.issueBtnLbl = `REFUNDS.query.${this.refundType}.issueButton`;
    this.issueSupervisionBtnLbl = `REFUNDS.query.commons.supervisionIssueButton`;

    this.initializeLoggedUser();
    this.initializeTabs();
    this.initializeBtnOptions();
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(loggedUser => (this.loggedUser = loggedUser));
  }

  private initializeTabs(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }

  public create(): void {
    const url =
      this.refundType === MasterDataType.RefundApp ? ROUTES.REFUNDS_APP_ISSUE.url : ROUTES.REFUNDS_NOTICE_ISSUE.url;

    this.router.navigate([url]);
  }

  public navigateToIssue(): void {
    const url: string =
      this.refundType === MasterDataType.RefundApp ? ROUTES.REFUNDS_APP_ISSUE.url : ROUTES.REFUNDS_NOTICE_ISSUE.url;

    this.handleIssueNavigation(RefundActionType.issue, url);
  }

  public navigateToIssueUnderSupervision(): void {
    const url =
      this.refundType === MasterDataType.RefundApp
        ? ROUTES.REFUNDS_APP_ISSUE_PENDING.url
        : ROUTES.REFUNDS_NOTICE_ISSUE_PENDING.url;

    this.handleIssueNavigation(RefundActionType.issuePendingSupervision, url);
  }

  private handleIssueNavigation(action: RefundActionType, url: string): void {
    // Differentiation between issue and issue under supervision permissions
    const permissions: string[] =
      action === RefundActionType.issue
        ? [getRefundCreatePermission(this.refundType)]
        : [getIssueRefundPendingSupervisionPermission(this.refundType)];

    combineLatest([
      this.store.pipe(select(getMenuTabs)),
      this.bspsDictionaryService.getAllBspsByPermissions(this.loggedUser.bspPermissions, permissions)
    ])
      .pipe(
        switchMap(([tabs, bsps]) => {
          const isIssueTabOpen = tabs.some(tab => tab.url === url);
          const dialogConfig: DialogConfig = {
            data: {
              title: this.translationService.translate(`REFUNDS.bspSelectionDialog.title.${action}`, {
                refundType: this.translationService.translate(`REFUNDS.type.${this.refundType}`)
              }),
              footerButtonsType: [{ type: FooterButton.Request, isDisabled: true }],
              bsps
            }
          };

          // If issue tab is not opened and there are multiple BSPs, BSP selection dialog is opened
          // If not, `null` is returned in order to navigate to issue tab as usually
          return !isIssueTabOpen && bsps.length > 1
            ? this.dialogService.open(RefundBspSelectionDialogComponent, dialogConfig)
            : of(null);
        }),
        first()
      )
      .subscribe(result => {
        if (result) {
          const { clickedBtn, contentComponentRef } = result;
          const bsp: BspDto = contentComponentRef.form.value.bsp;

          if (clickedBtn === FooterButton.Request) {
            // Navigating to issue tab with BSP id in query params
            this.router.navigate([url], { queryParams: { bspId: bsp.id } });
          }

          this.dialogService.close();
        } else {
          // Navigating to issue tab as usually
          this.router.navigate([url]);
        }
      });
  }

  private initializeBtnOptions(): void {
    this.showIssueBtn = this.canUserIssueRaRn();
    this.showIssuePendingBtn = this.canUserIssueRaRnUnderSupervision();

    if (this.showIssueBtn && this.showIssuePendingBtn) {
      this.showIssueBtn = false;
      this.showIssuePendingBtn = false;

      this.issueOptionActions = {
        mainOption: {
          label: this.translationService.translate(this.issueBtnLbl),
          command: () => this.navigateToIssue()
        },
        options: [
          {
            label: this.translationService.translate(this.issueSupervisionBtnLbl),
            command: () => this.navigateToIssueUnderSupervision()
          }
        ]
      };
    }
  }

  private canUserIssueRaRn(): boolean {
    const issuePermission =
      this.refundType === MasterDataType.RefundApp ? createRefundPermission.raIssue : createRefundPermission.rnIssue;

    return this.userHasPermission(issuePermission);
  }

  private canUserIssueRaRnUnderSupervision(): boolean {
    const issuePermission =
      this.refundType === MasterDataType.RefundApp
        ? createRefundPermissionUnderSupervision.raIssue
        : createRefundPermissionUnderSupervision.rnIssue;

    return this.userHasPermission(issuePermission);
  }

  private userHasPermission(permission: string): boolean {
    return this.loggedUser?.permissions.includes(permission);
  }
}
