import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { mapValues } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { SEARCH_FORM_DEFAULT_VALUE, SHARED_RA, SHARED_RN } from '../refund-list.constants';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { RefundStatus, StatisticalCodeOptions } from '~app/refund/models/refund-be-aux.model';
import { RefundFilter } from '~app/refund/models/refund-filter.model';
import { RefundBE, RefundFinal, RefundFormOfPayment } from '~app/refund/models/refund.model';
import { RefundDialogService } from '~app/refund/services/refund-dialog.service';
import { RefundFilterFormatter } from '~app/refund/services/refund-filter-formatter.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import {
  getBspFilterRequiredPermissions,
  getRefundEditInternalCommentPermission,
  getRefundReadInternalCommentPermission,
  getRefundViewPermission
} from '~app/refund/shared/helpers/refund-permissions.config';
import { deletedKeys, deletedSelectors, State } from '~app/refund/store/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService, DownloadFileComponent, FooterButton, PeriodOption } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { RangeDropdownUnit } from '~app/shared/components/range-dropdown/models/range-dropdown.model';
import { Permissions } from '~app/shared/constants';
import { FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { AgentSummary, AirlineSummary, GridColumn, IconType } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { EMPTY_VALUE } from '~app/shared/pipes/empty.pipe';
import { AgentDictionaryService, AirlineDictionaryService, CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-refund-deleted-list',
  templateUrl: './refund-deleted-list.component.html',
  styleUrls: ['./refund-deleted-list.component.scss'],
  providers: [RefundFilterFormatter, RefundService]
})
export class RefundDeletedListComponent extends ListSubtabs<RefundBE, RefundFilter> implements OnInit, OnDestroy {
  public refundType: MasterDataType;

  public isRa: boolean;
  public isRn: boolean;
  public isMultiCountryUser: boolean;
  public isUserAgent: boolean;
  public isUserAirline: boolean;

  public masterDataTypes = MasterDataType;
  public userTypes = UserType;

  public tableTitle: string;

  public loggedUser: User;
  public userType: string;

  public searchForm: FormGroup;
  public bspControl: AbstractControl;
  public periodControl: AbstractControl;
  public agentListControl: AbstractControl;
  public currencyListControl: AbstractControl;

  public predefinedFilters: Partial<RefundFilter>;
  public listActions$: Observable<{ action: GridTableActionType; disabled?: boolean; group?: string }[]>;
  public columns: Array<GridColumn> = [];
  public downloadQuery: Partial<DataQuery>;

  public deletionReason$: Observable<string>;

  public airlineCodesList$: Observable<DropdownOption<AirlineSummary>[]>;
  public agentCodesList$: Observable<DropdownOption<AgentSummary>[]>;
  public currencyList: DropdownOption<Currency>[] = [];
  public bspCountriesList: DropdownOption<BspDto>[] = [];
  public periodOptions$: Observable<PeriodOption[]>;
  public statusList: DropdownOption<RefundStatus>[] = [];
  public formsOfPaymentList: DropdownOption<RefundFormOfPayment>[] = [];
  public finalTypeList: DropdownOption<RefundFinal>[] = [];
  public statisticalCodeList: DropdownOption<StatisticalCodeOptions>[] = [];

  public isActionListVisible = false;
  public hasReadInternalCommentPermission = false;
  public hasEditInternalCommentPermission = false;
  public hasInternalCommentList: DropdownOption<boolean>[] = [];

  public isBspFilterMultiple: boolean;
  public isBspFilterLocked: boolean;
  public isPeriodDisabled = true;
  public isCurrencyControlDisabled: boolean;
  private bspSelectedIds: number[] = [];

  private leanUsersYears = 5;
  private nonLeanUsersYears = 2;
  public periodPickerYearsBack: number;
  private hasLeanPermission: boolean;

  public rangeDropdownUnit = RangeDropdownUnit;

  constructor(
    protected store: Store<AppState>,
    protected refundService: RefundService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected permissionsService: PermissionsService,
    public displayFormatter: RefundFilterFormatter,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private agentDictionaryService: AgentDictionaryService,
    private airlineDictionaryService: AirlineDictionaryService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private periodService: PeriodService,
    private dialogService: DialogService,
    private refundInternalCommentDialogService: RefundInternalCommentDialogService,
    private refundDialogService: RefundDialogService,
    protected bspsDictionaryService: BspsDictionaryService
  ) {
    super(store, refundService, actions$);
    this.setDataFromRoute();
  }

  private setDataFromRoute(): void {
    const data = this.activatedRoute.snapshot.data;

    this.refundType = data.refundType;
    this.isRa = this.refundType === MasterDataType.RefundApp;
    this.isRn = this.refundType === MasterDataType.RefundNotice;
    this.tableTitle = `REFUNDS.query.${this.refundType}.listTitle`;
    this.initialSortAttribute = this.isRa ? 'dateOfApplication' : 'dateOfIssue';
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<RefundBE, RefundFilter>,
    DefaultProjectorFn<ListSubtabsState<RefundBE, RefundFilter>>
  > {
    return deletedSelectors[this.refundType];
  }

  protected getListKey(): string {
    return deletedKeys[this.refundType];
  }

  protected adaptSearchQuery(query: DataQuery<RefundFilter>): DataQuery<RefundFilter> {
    return query.filterBy.status
      ? query
      : {
          ...query,
          filterBy: {
            ...query.filterBy,
            status: [RefundStatus.deleted, RefundStatus.supervisionDeleted]
          }
        };
  }

  public ngOnInit(): void {
    this.setInitialFilterValues();

    this.loading$ = of(true);
    this.initializeColumns();

    combineLatest([this.initializeLoggedUser$(), this.permissionsService.permissions$.pipe(first())])
      .pipe(
        tap(() => this.initializePermissions()),
        switchMap(() => this.initializeLeanBspFilter$()),
        tap(() => super.ngOnInit()),
        tap(() => this.setResolversData()),
        tap(() => this.initializeColumns()),
        tap(() => this.updatePeriodControlDisability()),
        tap(() => this.updatePeriodFilterOptions()),
        tap(() => this.setPeriodPickerYearsBack()),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public onGetActionList(refund: RefundBE & { markStatus: string }): void {
    this.listActions$ = this.refundService
      .getRefundActionList(refund.id)
      .pipe(map(actions => this.groupActions(actions)));
  }

  public onActionClick(event): void {
    const actionType: GridTableActionType = event.action.actionType;
    const item: RefundBE = event.row;

    switch (actionType) {
      case GridTableActionType.InternalComment:
        this.refundInternalCommentDialogService.open(item, this.refundType);
        break;
      case GridTableActionType.Activity:
        this.refundDialogService.openActivityHistoryDialog(item, this.refundType);
        break;
    }
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      switch (this.userType) {
        case UserType.AGENT:
          this.initializeAirlineFilterDropdown();
          break;
        case UserType.AIRLINE:
          this.updateAgentList();
          break;
        case UserType.AGENT_GROUP:
          this.initializeAgentGroupFilterDropdown();
          this.initializeAirlineFilterDropdown();
          break;
        default:
          this.initializeAirlineFilterDropdown();
          this.updateAgentList();
      }

      this.updateCurrencyList();
      this.initializeStatusFilterDropdown();
      this.initializeFormsOfPaymentDropdown();
      this.initializeBspCountrieslistener();

      if (this.isRa) {
        this.initializeFinalDropdown();
        this.initializeStatisticalCodeFilterDropdown();
      }

      if (this.hasReadInternalCommentPermission) {
        this.initializeInternalCommentDropdown();
      }
    }
  }

  private initializeBspCountrieslistener(): void {
    this.bspControl.valueChanges
      .pipe(
        tap(bsps => this.updateBspSelectedIds(bsps)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.updateAgentList();
        this.updateCurrencyList();
        this.updatePeriodControlDisability();
        this.updatePeriodFilterOptions();
      });
  }

  private updateBspSelectedIds(bsps: Bsp): void {
    if (bsps) {
      this.bspSelectedIds = Array.isArray(bsps) ? bsps.map(bsp => bsp.id) : [bsps.id];
    } else {
      this.bspSelectedIds = [];
    }
  }

  private updateAgentList(): void {
    this.updateAgentFilterList();

    if (this.isMultiCountryUser) {
      // If multicountry user, agent list should changed on BSP selection
      this.updateAgentListControlValue();
    }
  }

  private updateAgentFilterList(): void {
    const param = { ...(this.bspSelectedIds.length && { bspId: this.bspSelectedIds }) };

    this.agentCodesList$ = this.agentDictionaryService.getDropdownOptions(param);
  }

  private updateAgentListControlValue(): void {
    const agentsSelected: AgentSummary[] = this.agentListControl.value;

    if (this.bspSelectedIds.length && agentsSelected?.length) {
      const agentsToPatch = agentsSelected.filter(agent => this.bspSelectedIds.some(bspId => agent.bsp?.id === bspId));
      this.agentListControl.patchValue(agentsToPatch);
    }
  }

  private updateCurrencyList(): void {
    this.updateCurrencyFilterList();

    if (this.isMultiCountryUser) {
      // If multicountry user, currency list should changed on BSP selection
      this.updateCurrencyListControlValue();
    }
  }

  private updateCurrencyFilterList(): void {
    const param = { ...(this.bspSelectedIds.length && { bspId: this.bspSelectedIds }) };

    this.currencyDictionaryService.getFilteredDropdownOptions(param).subscribe(currencyList => {
      this.currencyList = currencyList;
      this.isCurrencyControlDisabled = currencyList.length === 0;
    });
  }

  private updateCurrencyListControlValue(): void {
    const currenciesSelected: Currency[] = this.currencyListControl.value;

    if (this.bspSelectedIds.length && currenciesSelected?.length) {
      const currenciesToPatch = currenciesSelected.filter(currency =>
        this.currencyList.some(cur => cur.value.id === currency.id)
      );
      this.currencyListControl.patchValue(currenciesToPatch);
    }
  }

  private updatePeriodControlDisability() {
    this.isPeriodDisabled = this.isBspFilterMultiple && this.bspSelectedIds?.length !== 1;
    if (this.isPeriodDisabled) {
      this.periodControl.reset();
    }
  }

  private setPeriodPickerYearsBack(): void {
    this.periodPickerYearsBack = this.hasLeanPermission ? this.leanUsersYears : this.nonLeanUsersYears;
  }

  private initializeLeanBspFilter$(): Observable<DropdownOption<BspDto>[]> {
    return this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, getBspFilterRequiredPermissions(this.refundType))
      .pipe(
        tap(bspList => {
          // Update BSP selected ids and predefined filters with the default BSP
          const onlyOneBsp = bspList.length === 1;

          if (bspList.length && (!this.hasLeanPermission || onlyOneBsp)) {
            const defaultBsp = bspList.find(bsp => bsp.isoCountryCode === this.loggedUser.defaultIsoc);

            const firstListBsp = bspList[0];
            const filterValue = onlyOneBsp ? firstListBsp : defaultBsp || firstListBsp;

            this.bspSelectedIds = [filterValue.id];

            // Agent users have not predefined filters
            if (!this.isUserAgent) {
              this.predefinedFilters = {
                ...this.predefinedFilters,
                bsp: filterValue
              };
            }
          }
        }),
        map(bspList => bspList.map(toValueLabelObjectBsp)),
        tap(bspList => (this.isBspFilterMultiple = this.hasLeanPermission && bspList.length !== 1)),
        tap(bspList => (this.isBspFilterLocked = bspList.length === 1)),
        tap(bspOptions => (this.bspCountriesList = bspOptions))
      );
  }

  private initializePermissions(): void {
    // Internal comment permissions
    this.hasReadInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundReadInternalCommentPermission(this.refundType)
    );

    this.hasEditInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundEditInternalCommentPermission(this.refundType)
    );

    this.isActionListVisible = this.hasEditInternalCommentPermission;

    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeAirlineFilterDropdown(): void {
    this.airlineCodesList$ = this.airlineDictionaryService.getDropdownOptions();
  }

  private initializeAgentGroupFilterDropdown(): void {
    const aGroupUserPermission = getRefundViewPermission(this.refundType);
    const aGroupUserHasPermission = this.loggedUser.permissions.some(perm => aGroupUserPermission === perm);

    this.agentCodesList$ = aGroupUserHasPermission
      ? this.agentDictionaryService.getDropdownOptions({ permission: aGroupUserPermission })
      : of(null);
  }

  private initializeStatusFilterDropdown(): void {
    this.statusList = [RefundStatus.deleted, RefundStatus.supervisionDeleted].map(item => ({
      value: item,
      label: this.translationService.translate(`REFUNDS.status.${item}`)
    }));
  }

  private initializeStatisticalCodeFilterDropdown(): void {
    this.statisticalCodeList = Object.values(StatisticalCodeOptions).map(item => ({
      label: this.translationService.translate(`REFUNDS.query.commons.StatisticalCodeOptions.${item}`),
      value: item
    }));
  }

  private initializeFormsOfPaymentDropdown(): void {
    this.formsOfPaymentList = Object.values(RefundFormOfPayment).map(item => ({
      value: item,
      label: this.translationService.translate(`REFUNDS.formsOfPayment.${item}`)
    }));
  }

  private updatePeriodFilterOptions(): void {
    if (this.bspSelectedIds.length === 1) {
      this.periodOptions$ = this.periodService.getByBsp(this.bspSelectedIds[0]);
    }
  }

  private initializeFinalDropdown(): void {
    this.finalTypeList = Object.values(RefundFinal).map(item => ({
      value: item,
      label: this.translationService.translate(`REFUNDS.finalType.${item}`)
    }));
    this.finalTypeList.unshift({ value: null, label: EMPTY_VALUE });
  }

  private initializeInternalCommentDropdown(): void {
    this.hasInternalCommentList = [true, false].map(item => ({
      label: this.translationService.translate(`REFUNDS.query.commons.yesNoBooleanValue.${item}`),
      value: item
    }));
  }

  private setResolversData(): void {
    //* Initialize specific property observables for row details
    this.deletionReason$ = this.selectedItem$.pipe(map(item => item?.deletionReason || ''));

    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.downloadQuery = this.adaptSearchQuery(query)));
  }

  private initializeColumns(): void {
    this.columns = this.isRa ? SHARED_RA.DELETED_COLUMNS : SHARED_RN.DELETED_COLUMNS;

    const statusColumn = this.columns.find(column => column.prop === 'status');
    if (statusColumn) {
      statusColumn.pipe = {
        transform: value => this.translationService.translate(`REFUNDS.status.${value}`)
      };
    }

    const finalTypeColumn = this.columns.find(column => column.prop === 'finalType');
    if (finalTypeColumn) {
      finalTypeColumn.pipe = {
        transform: value => (value ? this.translationService.translate(`REFUNDS.finalType.${value}`) : null)
      };
    }

    const finalDaysLeftColumn = this.columns.find(column => column.prop === 'finalDaysLeft');
    if (finalDaysLeftColumn) {
      finalDaysLeftColumn.icon = refund => ({
        name: 'check_circle',
        type: IconType.Info,
        hidden: refund.finalProcessStopped !== 0,
        tooltip: this.translationService.translate(`REFUNDS.query.${this.refundType}.daysLeftIconTooltip`)
      });
    }

    if (this.isUserAgent) {
      this.columns = this.columns.filter(column => column.prop !== 'agent.iataCode');
    }

    if (this.isUserAirline) {
      this.columns = this.columns.filter(column => column.prop !== 'airline.iataCode');
    }

    if (this.hasReadInternalCommentPermission) {
      this.setDocumentNumberColumn();
    }

    this.setBspColumn();
  }

  private setDocumentNumberColumn(): void {
    const documentNumberColumn = this.columns.find(column => column.prop === 'ticketDocumentNumber');

    if (documentNumberColumn) {
      documentNumberColumn.image = (refund: RefundBE) =>
        refund.internalComment
          ? {
              src: '/assets/images/utils/discuss.svg',
              tooltipLabel: this.translationService.translate(`REFUNDS.query.commons.internalCommentTooltip`)
            }
          : null;
    }
  }

  private setBspColumn(): void {
    const bspColumn = this.columns.find(column => column.prop === 'agent.bsp.isoCountryCode');

    if (bspColumn) {
      bspColumn.sortable = this.hasLeanPermission;
    }
  }

  private initializeLoggedUser$(): Observable<any> {
    return this.store.pipe(select(getUser), first()).pipe(
      tap(user => {
        this.loggedUser = user;
        this.userType = user.userType;
        this.isUserAgent = this.userType === UserType.AGENT;
        this.isUserAirline = this.userType === UserType.AIRLINE;
        this.isMultiCountryUser = this.loggedUser.bsps?.length > 1;
      })
    );
  }

  private setInitialFilterValues(): void {
    this.searchForm = this.fb.group(mapValues(SEARCH_FORM_DEFAULT_VALUE, value => [value]));

    this.bspControl = FormUtil.get<RefundFilter>(this.searchForm, 'bsp');
    this.periodControl = FormUtil.get<RefundFilter>(this.searchForm, 'period');
    this.agentListControl = FormUtil.get<RefundFilter>(this.searchForm, 'agentId');
    this.currencyListControl = FormUtil.get<RefundFilter>(this.searchForm, 'currencyId');
  }

  private groupActions(
    actions: { action: GridTableActionType; disabled?: boolean; group?: string }[]
  ): { action: GridTableActionType; disabled?: boolean; group?: string }[] {
    // For now, we just need to group `internal comment` action
    return actions.map(action =>
      action.action === GridTableActionType.InternalComment ? { ...action, group: 'comment' } : action
    );
  }

  public onImageClick(event: { event: Event; row: RefundBE; column: GridColumn }): void {
    const { row } = event;

    this.refundInternalCommentDialogService.open(row, this.refundType);
  }

  public onRowToggle(event: { type: string; value: RefundBE }): void {
    const refundId: number = event?.value?.id;

    if (event?.type === 'row' && refundId) {
      this.onSelectedItemChanged(refundId);
    }
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate(`REFUNDS.query.${this.refundType}.downloadTitle`),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery,
        totalElements: this.downloadQuery.paginateBy.totalElements
      },
      apiService: this.refundService
    });
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
