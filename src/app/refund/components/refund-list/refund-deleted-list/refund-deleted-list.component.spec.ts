/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundDeletedListComponent } from './refund-deleted-list.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { PeriodService } from '~app/master-data/periods/shared/period.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { RefundBE, RefundFinal, RefundFormOfPayment } from '~app/refund/models/refund.model';
import { RefundDialogService } from '~app/refund/services/refund-dialog.service';
import { RefundFilterFormatter } from '~app/refund/services/refund-filter-formatter.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import { deletedKeys } from '~app/refund/store/reducers';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { ListSubtabsActions } from '~app/shared/base/list-subtabs/actions';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { EMPTY_VALUE } from '~app/shared/pipes/empty.pipe';
import { PeriodPipe } from '~app/shared/pipes/period.pipe';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  AppConfigurationService,
  CurrencyDictionaryService
} from '~app/shared/services';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('RefundDeletedListComponent', () => {
  let fixture: ComponentFixture<RefundDeletedListComponent>;
  let component: RefundDeletedListComponent;
  let mockStore: MockStore<AppState>;

  const refundServiceSpy = createSpyObject(RefundService);
  const airlineDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
  const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);
  const currencyDictionaryServiceSpy = createSpyObject(CurrencyDictionaryService);
  const periodServiceSpy = createSpyObject(PeriodService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const refundInternalCommentDialogServiceSpy = createSpyObject(RefundInternalCommentDialogService);
  const refundDialogServiceSpy = createSpyObject(RefundDialogService);

  const activatedRouteStub = {
    snapshot: {
      data: {
        title: 'REFUNDS.query.commons.deletedTabTitle',
        refundType: MasterDataType.RefundApp
      }
    }
  };

  const expectedUserDetails = {
    id: 10126,
    email: 'airline1@example.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userType: UserType.AIRLINE,
    permissions: ['rRaQ', 'rRaIntCom'],
    bspPermissions: [],
    bsps: [{ id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          'MENU.REFUNDS.QUERY.app': {},
          activeTabId: 'MENU.REFUNDS.QUERY.app'
        }
      },
      viewListsInfo: {}
    },
    refund: { 'refund-application-deleted-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundDeletedListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({
          initialState,
          selectors: [
            { selector: fromAuth.getUser, value: expectedUserDetails },
            { selector: fromAuth.getUserBsps, value: expectedUserDetails.bsps }
          ]
        }),
        FormBuilder,
        PermissionsService,
        AppConfigurationService,
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: AirlineDictionaryService, useValue: airlineDictionaryServiceSpy },
        { provide: CurrencyDictionaryService, useValue: currencyDictionaryServiceSpy },
        { provide: PeriodService, useValue: periodServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: RefundInternalCommentDialogService, useValue: refundInternalCommentDialogServiceSpy },
        { provide: RefundDialogService, useValue: refundDialogServiceSpy }
      ]
    })
      .overrideComponent(RefundDeletedListComponent, {
        set: {
          providers: [
            RefundFilterFormatter,
            { provide: PeriodPipe, useClass: PeriodPipeMock },
            { provide: RefundService, useValue: refundServiceSpy }
          ]
        }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundDeletedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject(MockStore);
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('should set data from route for RA', () => {
    expect(component.isRa).toBe(true);
    expect(component.isRn).toBe(false);
    expect(component.tableTitle).toBe('REFUNDS.query.refund-application.listTitle');
    expect(component['initialSortAttribute']).toBe('dateOfApplication');
  });

  it('should initialize internal comment permissions', () => {
    const service = TestBed.inject(PermissionsService);

    spyOn(service, 'hasPermission').and.callThrough();
    component.ngOnInit();

    expect(service.hasPermission).toHaveBeenCalledWith('rRaIntCom');
    expect(service.hasPermission).toHaveBeenCalledWith(['cRaIntCom', 'uRaIntCom', 'dRaIntCom']);
    expect(component.hasReadInternalCommentPermission).toBe(true);
    expect(component.hasEditInternalCommentPermission).toBe(false);
  });

  describe('initialize grid table', () => {
    it('should initialize logged user', () => {
      component.ngOnInit();

      expect(component.loggedUser).toEqual(expectedUserDetails as User);
      expect(component.userType).toEqual(UserType.AIRLINE);
    });

    it('should initialize columns as AIRLINE', () => {
      const expectedColumns = jasmine.arrayContaining<any>([
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.isoc',
          prop: 'agent.bsp.isoCountryCode',
          sortable: false
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.docNumber',
          prop: 'ticketDocumentNumber',
          image: jasmine.any(Function)
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.agentCode',
          prop: 'agent.iataCode'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.status',
          prop: 'status',
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.final',
          prop: 'finalType',
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.daysLeft',
          prop: 'finalDaysLeft',
          icon: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.amount',
          prop: 'totalAmount',
          cellClass: 'text-right',
          cellTemplate: 'amountCellTmpl'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.formsOfPayment',
          prop: 'formsOfPayment',
          sortable: false,
          cellTemplate: 'commonPipeSeparatorCellTmpl'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.applicationDate',
          prop: 'dateOfApplication',
          cellTemplate: 'dayMonthYearCellTmpl'
        })
      ]);

      component.ngOnInit();
      expect(component.columns).toEqual(expectedColumns);
    });

    it('should initialize columns as AGENT', () => {
      const expectedColumns = jasmine.arrayContaining<any>([
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.isoc',
          prop: 'agent.bsp.isoCountryCode',
          sortable: false
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.docNumber',
          prop: 'ticketDocumentNumber',
          image: jasmine.any(Function)
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.airlCode',
          prop: 'airline.iataCode'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.status',
          prop: 'status',
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.final',
          prop: 'finalType',
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.daysLeft',
          prop: 'finalDaysLeft',
          icon: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.amount',
          prop: 'totalAmount',
          cellClass: 'text-right',
          cellTemplate: 'amountCellTmpl'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.formsOfPayment',
          prop: 'formsOfPayment',
          sortable: false,
          cellTemplate: 'commonPipeSeparatorCellTmpl'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.applicationDate',
          prop: 'dateOfApplication',
          cellTemplate: 'dayMonthYearCellTmpl'
        })
      ]);

      setUserType(UserType.AGENT);
      component.isUserAgent = true;
      component.isUserAirline = false;
      component['initializeColumns']();

      expect(component.columns).toEqual(expectedColumns);
    });

    it('should initialize columns as IATA', () => {
      const expectedColumns = jasmine.arrayContaining<any>([
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.isoc',
          prop: 'agent.bsp.isoCountryCode'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.docNumber',
          prop: 'ticketDocumentNumber',
          image: jasmine.any(Function)
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.airlCode',
          prop: 'airline.iataCode'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.agentCode',
          prop: 'agent.iataCode'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.status',
          prop: 'status',
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.final',
          prop: 'finalType',
          pipe: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.daysLeft',
          prop: 'finalDaysLeft',
          icon: jasmine.anything()
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.amount',
          prop: 'totalAmount',
          cellClass: 'text-right',
          cellTemplate: 'amountCellTmpl'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.formsOfPayment',
          prop: 'formsOfPayment',
          sortable: false,
          cellTemplate: 'commonPipeSeparatorCellTmpl'
        }),
        jasmine.objectContaining({
          name: 'REFUNDS.tableColumns.applicationDate',
          prop: 'dateOfApplication',
          cellTemplate: 'dayMonthYearCellTmpl'
        })
      ]);

      setUserType(UserType.IATA);
      component.ngOnInit();
      // TODO ngOnInit's initializeLeanBspFilter is not being called properly so initializeColumns is called manually
      component['initializeColumns']();

      expect(component.columns).toEqual(expectedColumns);
    });

    it('should initialize action list visible', () => {
      component.ngOnInit();
      expect(component.isActionListVisible).toBe(false);
    });
  });

  it('should initialize list actions on get action list', fakeAsync(() => {
    let actionList: { action: GridTableActionType; disabled?: boolean; group?: string }[];
    const mockActions = [{ action: GridTableActionType.InternalComment, disabled: false }];

    refundServiceSpy.getRefundActionList.and.returnValue(of(mockActions));
    spyOn<any>(component, 'groupActions').and.callThrough();

    component.onGetActionList({ id: 1 } as RefundBE & { markStatus: string });
    component.listActions$.subscribe(actions => (actionList = actions));
    tick();

    expect(refundServiceSpy.getRefundActionList).toHaveBeenCalledWith(1);
    expect(component['groupActions']).toHaveBeenCalledWith(mockActions);
    expect(actionList).toEqual([{ action: GridTableActionType.InternalComment, disabled: false, group: 'comment' }]);
  }));

  it('should open refund internal comment dialog on internal comment action click', () => {
    const event = {
      action: { actionType: GridTableActionType.InternalComment },
      row: { id: 1 }
    };

    refundInternalCommentDialogServiceSpy.open.and.callThrough();
    component.onActionClick(event);

    expect(refundInternalCommentDialogServiceSpy.open).toHaveBeenCalledWith({ id: 1 }, MasterDataType.RefundApp);
  });

  it('should open refund activity dialog on activity action click', () => {
    const event = {
      action: { actionType: GridTableActionType.Activity },
      row: { id: 1 }
    };

    refundDialogServiceSpy.openActivityHistoryDialog.and.callThrough();
    component.onActionClick(event);

    expect(refundDialogServiceSpy.openActivityHistoryDialog).toHaveBeenCalledWith({ id: 1 }, MasterDataType.RefundApp);
  });

  describe('`onFilterButtonClicked`', () => {
    refundServiceSpy.getBspsList.and.returnValue(of());
    currencyDictionaryServiceSpy.getFilteredDropdownOptions.and.returnValue(of());
    periodServiceSpy.getByBsp.and.returnValue(of());

    afterEach(() => {
      airlineDictionaryServiceSpy.getDropdownOptions.calls.reset();
      agentDictionaryServiceSpy.getDropdownOptions.calls.reset();
      periodServiceSpy.getByBsp.calls.reset();
    });

    it('should NOT initialize any dropdown when `isSearchPanelVisible` is false', () => {
      component.onFilterButtonClicked(false);

      expect(component.airlineCodesList$).toBeUndefined();
      expect(component.agentCodesList$).toBeUndefined();
      expect(component.currencyList).toEqual([]);
      expect(component.statusList).toEqual([]);
    });

    it('should initialize airline dropdown and NOT agent dropdown when user type is AGENT', () => {
      setUserType(UserType.AGENT);
      component.ngOnInit();

      component.onFilterButtonClicked(true);

      expect(component.airlineCodesList$).toBeDefined();
      expect(component.agentCodesList$).toBeUndefined();
      expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
    });

    it('should initialize agent dropdown and NOT airline dropdown when user type is AIRLINE', () => {
      component.onFilterButtonClicked(true);

      expect(component.agentCodesList$).toBeDefined();
      expect(component.airlineCodesList$).toBeUndefined();
      expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
    });

    it('should initialize airline and agent dropdowns when user type is AGENT GROUP and has view query perms', () => {
      setUserType(UserType.AGENT_GROUP);
      component.ngOnInit();

      component.onFilterButtonClicked(true);

      expect(component.airlineCodesList$).toBeDefined();
      expect(component.agentCodesList$).toBeDefined();
      expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
      expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalledWith({ permission: 'rRaQ' });
    });

    it('should initialize airline dropdown and NOT agent dropdown when user type is AGENT GROUP and has NOT view query perms', () => {
      mockStore.overrideSelector(fromAuth.getUser, {
        ...expectedUserDetails,
        userType: UserType.AGENT_GROUP,
        permissions: []
      } as User);
      component.ngOnInit();

      component.onFilterButtonClicked(true);

      expect(component.airlineCodesList$).toBeDefined();
      expect(component.agentCodesList$).toBeDefined();
      expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
      expect(agentDictionaryServiceSpy.getDropdownOptions).not.toHaveBeenCalled();
    });

    it('should initialize airline and agent dropdowns when user type is IATA', () => {
      setUserType(UserType.IATA);
      component.ngOnInit();

      component.onFilterButtonClicked(true);

      expect(component.airlineCodesList$).toBeDefined();
      expect(component.agentCodesList$).toBeDefined();
      expect(airlineDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
      expect(agentDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
    });

    xit('should always initialize currency dropdown', () => {
      component.onFilterButtonClicked(true);

      expect(component.currencyList).toBeDefined();
      expect(currencyDictionaryServiceSpy.getDropdownOptions).toHaveBeenCalled();
    });

    it('should always initialize status dropdown', () => {
      component.onFilterButtonClicked(true);

      expect(component.statusList).toEqual([
        { value: RefundStatus.deleted, label: 'REFUNDS.status.DELETED' },
        { value: RefundStatus.supervisionDeleted, label: 'REFUNDS.status.SUPERVISION_DELETED' }
      ]);
    });

    it('should always initialize forms of payment dropdown', () => {
      component.onFilterButtonClicked(true);

      expect(component.formsOfPaymentList).toEqual([
        { value: RefundFormOfPayment.CA, label: 'REFUNDS.formsOfPayment.CA' },
        { value: RefundFormOfPayment.CC, label: 'REFUNDS.formsOfPayment.CC' },
        { value: RefundFormOfPayment.EP, label: 'REFUNDS.formsOfPayment.EP' },
        { value: RefundFormOfPayment.MSCA, label: 'REFUNDS.formsOfPayment.MSCA' },
        { value: RefundFormOfPayment.MSCC, label: 'REFUNDS.formsOfPayment.MSCC' }
      ]);
    });

    it('should initialize period options if there is 1 BSP selected', fakeAsync(() => {
      component.onFilterButtonClicked(true);
      tick();

      component.bspControl.setValue([{ id: 1 }]);
      component.bspControl.updateValueAndValidity({ emitEvent: true });

      fixture.detectChanges();
      expect(component.periodOptions$).toBeDefined();
      expect(periodServiceSpy.getByBsp).toHaveBeenCalledWith(1);
    }));

    it('should NOT initialize period options if there are 0 or more than 1 BSPs selected', fakeAsync(() => {
      component.onFilterButtonClicked(true);
      tick();

      component.bspControl.setValue([{ id: 1 }, { id: 2 }]);
      component.bspControl.updateValueAndValidity({ emitEvent: true });

      fixture.detectChanges();
      expect(component.periodOptions$).toBeUndefined();
      expect(periodServiceSpy.getByBsp).not.toHaveBeenCalled();
    }));

    it('should initialize final dropdown if `isRa`', () => {
      component.onFilterButtonClicked(true);

      expect(component.finalTypeList).toEqual([
        { value: null, label: EMPTY_VALUE },
        { value: RefundFinal.Final, label: 'REFUNDS.finalType.FINAL' },
        { value: RefundFinal.Exception, label: 'REFUNDS.finalType.EXCEPTION' }
      ]);
    });

    it('should initialize internal comment dropdown if `hasReadInternalCommentPermission`', () => {
      component.onFilterButtonClicked(true);

      expect(component.hasInternalCommentList).toEqual([
        { value: true, label: 'REFUNDS.query.commons.yesNoBooleanValue.true' },
        { value: false, label: 'REFUNDS.query.commons.yesNoBooleanValue.false' }
      ]);
    });
  });

  it('should open refund internal comment dialog on image click', () => {
    const event = { event: null, row: { id: 1 } as RefundBE, column: null };

    refundInternalCommentDialogServiceSpy.open.and.callThrough();
    component.onImageClick(event);

    expect(refundInternalCommentDialogServiceSpy.open).toHaveBeenCalledWith({ id: 1 }, MasterDataType.RefundApp);
  });

  it('should dispatch refund deleted `searchOne` action on row toggle', () => {
    const event = { type: 'row', value: { id: 1 } as RefundBE };

    spyOn(mockStore, 'dispatch');
    component.onRowToggle(event);

    expect(mockStore.dispatch).toHaveBeenCalledWith(
      ListSubtabsActions.searchOne(deletedKeys[MasterDataType.RefundApp])({ id: 1 })
    );
  });

  it('should open download file dialog on download', () => {
    const downloadQuery = {
      ...defaultQuery,
      filterBy: jasmine.objectContaining({
        // Initialized with default query but adapted with `adaptSearchQuery` including deleted status
        status: [RefundStatus.deleted, RefundStatus.supervisionDeleted]
      })
    };

    const data = {
      title: 'REFUNDS.query.refund-application.downloadTitle',
      footerButtonsType: FooterButton.Download,
      downloadQuery,
      totalElements: defaultQuery.paginateBy.totalElements
    };

    component.downloadQuery = downloadQuery;
    dialogServiceSpy.open.and.callThrough();
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(DownloadFileComponent, {
      data,
      apiService: component['refundService']
    });
  });

  function setUserType(userType: UserType): void {
    mockStore.overrideSelector(fromAuth.getUser, { ...expectedUserDetails, userType } as User);
  }
});
