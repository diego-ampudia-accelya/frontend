import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundListComponent } from './refund-list.component';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { MenuBuilder } from '~app/master-data/configuration';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants/routes';
import { createAgentUser } from '~app/shared/mocks/agent-user';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { createIataUser } from '~app/shared/mocks/iata-user';
import { User, UserType } from '~app/shared/models/user.model';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

describe('RefundListComponent', () => {
  let fixture: ComponentFixture<RefundListComponent>;
  let component: RefundListComponent;

  const translationServiceSpy = createSpyObject(L10nTranslationService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const bspsDictionaryServiceSpy = createSpyObject(BspsDictionaryService);

  const menuBuilderMock = jasmine.createSpyObj<MenuBuilder>('MenuBuilder', {
    buildMenuItemsFrom: [
      {
        route: '/refunds/app/query/deleted',
        title: 'REFUNDS.query.commons.deletedTabTitle',
        isAccessible: true
      }
    ]
  });

  const activatedRouteStub = {
    snapshot: {
      data: {
        tab: ROUTES.REFUNDS_APP_QUERY
      }
    }
  };

  const initialState = {
    auth: {}
  };

  let routerMock;

  beforeEach(waitForAsync(() => {
    routerMock = {
      events: of(new NavigationEnd(0, '/deleted', './deleted')),
      navigate: spyOn(Router.prototype, 'navigate'),
      url: './deleted'
    };

    TestBed.configureTestingModule({
      declarations: [RefundListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: MenuBuilder, useValue: menuBuilderMock },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: BspsDictionaryService, useValue: bspsDictionaryServiceSpy },
        provideMockStore({ selectors: [{ selector: fromAuth.isLoggedIn, value: true }], initialState }),
        {
          provide: Router,
          useValue: routerMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should be allowed to issue Refunds according to user permissions', () => {
    component.refundType = MasterDataType.RefundApp;
    const users: User[] = [
      {
        ...createIataUser(),
        permissions: ['cRa', 'cRn']
      } as User,
      {
        ...createAgentUser(),
        permissions: ['cRa', 'cRn']
      } as User,
      {
        ...createAirlineUser(),
        permissions: ['cRn']
      } as User
    ];

    users.forEach(user => {
      component.loggedUser = user;
      expect(component['canUserIssueRaRn']()).toBe(UserType.AIRLINE !== component.loggedUser.userType);
    });
  });
});
