import { DatePipe } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild
} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import moment from 'moment-mini';
import { MenuItem } from 'primeng/api';
import { Observable, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { getUserType } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { AppState } from '~app/reducers';
import { RefundStatus, SendToDpcType } from '~app/refund/models/refund-be-aux.model';
import {
  AgentViewModel,
  AirlineViewModel,
  CurrencyViewModel,
  SecondaryHeaderActionsViewModel,
  StatusHeaderViewModel,
  StatusViewModel
} from '~app/refund/models/refund-view-aux.model';
import {
  RefundActionEmitterType,
  RefundActionType,
  RefundBE,
  RefundHeaderBtnModel
} from '~app/refund/models/refund.model';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundService } from '~app/refund/services/refund.service';
import { getBspName } from '~app/refund/shared/helpers/refund.helper';
import { ButtonDesign } from '~app/shared/components';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { AccessType } from '~app/shared/enums';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { Bsp } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { WatcherDocType } from '~app/shared/models/watcher.model';
import { DocumentPdfExporter, NotificationService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

@Component({
  selector: 'bspl-refund-header-status',
  templateUrl: './refund-header-status.component.html',
  styleUrls: ['./refund-header-status.component.scss'],
  providers: [DatePipe, RefundService]
})
export class RefundHeaderStatusComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('actionsMenuContainer') actionsMenuContainer: ElementRef;
  @ViewChild('statusHeader') statusHeader: ElementRef;

  @Output() actionClick = new EventEmitter<RefundActionType>();
  @Output() actionDiscardClick = new EventEmitter<string>();
  @Output() downloadClick = new EventEmitter();
  @Output() actionsMenuClick = new EventEmitter<any>();
  @Output() heightChange = new EventEmitter<number>();

  @Input() formConfig: RefundConfigService;
  @Input() formData: RefundDataService;
  @Input() refundService: RefundService;

  @Input() accordion: AccordionComponent;

  @Input() form: FormGroup;

  public headerFields: Array<keyof StatusHeaderViewModel>;

  public currency: CurrencyViewModel;
  public refundType: MasterDataType;

  public headerBtn: RefundHeaderBtnModel;
  public headerDiscardBtn: RefundHeaderBtnModel;
  public headerBtnOptions: ButtonMenuOption[] & MenuItem[] = [];
  public showAirlDropdownBtn = false;
  public showAgentDropdownBtn = false;

  public btnDesign = ButtonDesign;

  public iconType: string;
  public iconTooltip: string;
  public iconField: keyof StatusHeaderViewModel;

  public badgeInfoType: string;
  public badgeInfoLeyend: string;
  public badgeInfoField: keyof StatusHeaderViewModel;

  public headerSecondaryActions: Array<SecondaryHeaderActionsViewModel>;
  public isSecondaryActionsMenuDisabled: boolean;
  public isPdfLoading: boolean;
  public showHeaderSecondaryActions: boolean;
  public showFinalSection: boolean;
  public isDaysLeftGreyedOut: boolean;

  public finalControl: AbstractControl;
  public finalDaysLeftControl: AbstractControl;
  public finalProcessStoppedControl: AbstractControl;

  public docNumberField: keyof StatusHeaderViewModel = 'docNumber';

  public watching: boolean;

  private _isHeaderOverlapped = false;
  public get isHeaderOverlapped(): boolean {
    return this._isHeaderOverlapped;
  }

  private statusForm: FormGroup;

  private formFactory: FormUtil;
  private destroy$ = new Subject();
  private loggedUserType: UserType;

  private get loggedUserType$(): Observable<UserType> {
    return this.store.pipe(select(getUserType), first());
  }

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private translationService: L10nTranslationService,
    private activatedRoute: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private store: Store<AppState>,
    private localBspTimeService: LocalBspTimeService,
    private notificationsHeaderService: NotificationsHeaderService,
    private notificationService: NotificationService,
    @Optional() public pdfExporter: DocumentPdfExporter
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  @HostListener('document:click', ['$event'])
  public onDocumentClick(event: MouseEvent): void {
    const target = event.target;
    if (
      this.actionsMenuContainer &&
      this.actionsMenuContainer.nativeElement &&
      !this.actionsMenuContainer.nativeElement.contains(target)
    ) {
      this.toggleHeaderSecondaryActionsMenu(null);
    }
  }

  @HostListener('document:scroll')
  public onScroll(): void {
    this.updateHeaderOverlapState();
  }

  public ngOnInit() {
    if (!this.form) {
      this.buildForm();
    }
    this.setConfig();

    if (this.formConfig.accessType !== AccessType.read) {
      this.initializeChangesListener();
      this.formData.whenFormIsReady().subscribe(() => {
        this.initializeBadgeInfo();
        this.initializeFinalSection();
      });
      this.isSecondaryActionsMenuDisabled = true;
    }

    if (this.formConfig.accessType === AccessType.read) {
      this.formData
        .getSubject(RefundActionEmitterType.currency)
        .pipe(first())
        .subscribe(currency => (this.currency = currency));
      this.initializeBadgeInfo();
      this.initializeUrgencyIcon();
      this.initializeWatchingFlagAndActions();
      this.initializeFinalSection();
    }

    if (this.formConfig.isResubmission) {
      this.formData.whenFormIsReady().subscribe(() => {
        FormUtil.get<StatusHeaderViewModel>(this.form, 'docNumber').setValue(null);
        FormUtil.get<StatusViewModel>(this.statusForm, 'status').setValue(null);
      });
    }
  }

  public ngAfterViewInit(): void {
    this.heightChange.emit(this.statusHeader.nativeElement.offsetHeight);
  }

  public toggleHeaderSecondaryActionsMenu(event: Event): void {
    if (event) {
      event.stopPropagation();
      this.showHeaderSecondaryActions = !this.showHeaderSecondaryActions;
    } else {
      this.showHeaderSecondaryActions = false;
    }
  }

  public onActionClick(secondaryAction: SecondaryHeaderActionsViewModel) {
    const itemRetrieved: RefundBE = this.activatedRoute.snapshot.data.item;

    switch (secondaryAction.action) {
      case GridTableActionType.Download:
        this.downloadPdf();
        break;
      case GridTableActionType.StartWatching:
      case GridTableActionType.StopWatching:
        this.refundService
          .updateWatchers([itemRetrieved], secondaryAction.action === GridTableActionType.StartWatching)
          .subscribe(([{ watching }]) => this.onUpdateWatcherSuccess(watching));
        break;
    }

    this.showHeaderSecondaryActions = false;
  }

  public isBtnDisabled(): boolean {
    return this.headerBtnOptions.every(option => option.isDisabled);
  }

  public onApplyAction(actionType: RefundActionType) {
    this.actionClick.emit(actionType);
  }

  public onDiscardAction(actionType: RefundActionType) {
    this.actionDiscardClick.emit(actionType);
  }

  public getFormattedField(field: keyof StatusHeaderViewModel): string {
    let fieldValue = this.form.get(field).value;

    switch (field) {
      case 'appDate':
        if (fieldValue) {
          fieldValue = this.datePipe.transform(fieldValue, 'dd/MM/yyyy');
        }
        break;
      case 'agentCode':
      case 'airlCode':
        fieldValue = fieldValue ? fieldValue : this.translationService.translate('REFUNDS.statusHeader.notDefinedYet');
        break;
      case 'bspName':
        fieldValue = fieldValue
          ? fieldValue.toUpperCase()
          : this.translationService.translate('REFUNDS.statusHeader.notDefinedYet');
        break;
      case 'docNumber':
        fieldValue = fieldValue ? fieldValue : this.translationService.translate('REFUNDS.statusHeader.notIssuedYet');
        break;
      case 'status':
        fieldValue = fieldValue.status
          ? this.translationService.translate(`REFUNDS.status.${fieldValue.status}`)
          : this.translationService.translate('REFUNDS.statusHeader.notIssuedYet');
        break;
    }

    return fieldValue;
  }

  public isFieldInactive(field: keyof StatusHeaderViewModel): boolean {
    const fieldValue = this.form.get(field).value;

    return (
      !fieldValue ||
      (field === 'status' && !fieldValue.status) ||
      (field === 'amount' && this.numberStringIsZero(fieldValue))
    );
  }

  //TODO Modify when other button actions are added
  public showDropdownBtn(): boolean {
    return this.headerBtnOptions && this.headerBtnOptions.length > 0;
  }

  public async downloadPdf() {
    this.isPdfLoading = true;

    this.expandItems();
    const pdf = await this.pdfExporter.export();
    const document = this.activatedRoute.snapshot.data.item;

    let fileName = '';

    if (this.formConfig.isRa) {
      fileName = `ra_${document.ticketDocumentNumber}`;
    }

    if (this.formConfig.isRn) {
      fileName = `rn_${document.ticketDocumentNumber}`;
    }

    pdf.save(`${fileName}.pdf`);
    this.isPdfLoading = false;
    this.cd.detectChanges();
  }

  public updateBasicBspFields(bsp: Bsp) {
    if (bsp) {
      FormUtil.get<StatusHeaderViewModel>(this.form, 'bspName').setValue(getBspName(bsp), { onlySelf: true });

      if (this.formConfig.accessType === AccessType.create) {
        this.refundService.getBspTime(bsp.id).subscribe(time => {
          FormUtil.get<StatusHeaderViewModel>(this.form, 'appDate').setValue(time.localTime, { emitEvent: false });
        });
      }
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeWatchingFlagAndActions(): void {
    const itemRetrieved: RefundBE = this.activatedRoute.snapshot.data.item;
    const bspId = (itemRetrieved.agent as Agent).bsp.id;

    this.notificationsHeaderService.getWatcher(bspId, itemRetrieved.id, WatcherDocType.REFUND).subscribe(watcher => {
      this.watching = watcher.watching;
      this.initializeSecondaryHeaderActions();
    });
  }

  private initializeSecondaryHeaderActions() {
    const watcherAction = this.watching ? GridTableActionType.StopWatching : GridTableActionType.StartWatching;
    const watcherLabel = this.watching ? 'stopWatching' : 'startWatching';
    const isAirlineOrIataUserType = [UserType.AIRLINE, UserType.IATA].includes(this.loggedUserType);

    this.headerSecondaryActions = [
      {
        label: this.translationService.translate('REFUNDS.statusHeader.secondaryActions.download'),
        action: GridTableActionType.Download
      }
    ];

    if (!isAirlineOrIataUserType) {
      this.headerSecondaryActions.push({
        label: this.translationService.translate(watcherLabel),
        action: watcherAction
      });
    }

    this.isSecondaryActionsMenuDisabled = false;
    this.showHeaderSecondaryActions = false;
  }

  private onUpdateWatcherSuccess(watching: boolean): void {
    this.watching = watching;
    this.initializeSecondaryHeaderActions();
    this.notifyUpdateWatcherSuccess(watching);
    this.cd.markForCheck();
  }

  private notifyUpdateWatcherSuccess(watching: boolean): void {
    const translationKey = watching ? 'startWatchingDocument' : 'stopWatchingDocument';
    this.notificationService.showSuccess(this.translationService.translate(translationKey));
  }

  private expandItems(): void {
    this.accordion.expandItems();
    this.downloadClick.emit();
  }

  private initializeBadgeInfo() {
    const { sentToDpc, status } = this.getStatusValue();

    if (this.formConfig.isRn || status === RefundStatus.authorized || status === RefundStatus.rejected) {
      this.badgeInfoField = 'status';

      this.badgeInfoLeyend = sentToDpc
        ? this.translationService.translate(`REFUNDS.badgeInfo.leyend.${sentToDpc.toUpperCase()}`, {
            refundType: this.translationService.translate(`REFUNDS.type.${this.refundType}`)
          })
        : '';

      switch (sentToDpc) {
        case SendToDpcType.pending:
          this.badgeInfoType = BadgeInfoType.info;
          break;
        case SendToDpcType.sent:
          this.badgeInfoType = BadgeInfoType.success;
          break;
        case SendToDpcType.na:
        case SendToDpcType.n_a:
        default:
          this.badgeInfoType = BadgeInfoType.regular;
          break;
      }
    }
  }

  private initializeUrgencyIcon(): void {
    if (this.formConfig.isRn || this.isUrgencyIconHidden()) {
      return;
    }

    const applicationDate: Date = FormUtil.get<StatusHeaderViewModel>(this.form, 'appDate').value;

    this.iconField = 'appDate';

    /** If date difference is more than 45 days, the refund is urgent and if not it's suppose to be between 30 (checked in `isUrgencyIconHidden`) and 45 days so the refund is semi-urgent */
    if (this.getDateDifference(applicationDate) > 45) {
      this.iconType = 'error';
      this.iconTooltip = this.translationService.translate(
        `REFUNDS.query.${this.refundType}.appDateUrgencyIconTooltip`
      );
    } else {
      this.iconType = 'warning';
      this.iconTooltip = this.translationService.translate(
        `REFUNDS.query.${this.refundType}.appDateSemiUrgencyIconTooltip`
      );
    }
  }

  private isUrgencyIconHidden(): boolean {
    const { status } = this.getStatusValue();
    const applicationDate: Date = FormUtil.get<StatusHeaderViewModel>(this.form, 'appDate').value;

    const isRefundPendingFromAirline: boolean =
      status === RefundStatus.pending || status === RefundStatus.resubmitted || status === RefundStatus.investigated;

    return !isRefundPendingFromAirline || this.getDateDifference(applicationDate) <= 30;
  }

  /** Get difference between BSP date and the specified date in days */
  private getDateDifference(applicationDate: Date): number {
    return moment(this.localBspTimeService.getDate()).diff(applicationDate, 'days');
  }

  private initializeFinalSection() {
    const { status } = this.getStatusValue();

    // Final flag
    this.finalControl = FormUtil.get<StatusHeaderViewModel>(this.form, 'finalType');

    // Final days left
    this.finalDaysLeftControl = FormUtil.get<StatusHeaderViewModel>(this.form, 'finalDaysLeft');
    this.isDaysLeftGreyedOut =
      this.finalDaysLeftControl.value !== 0 &&
      (status === RefundStatus.authorized || status === RefundStatus.rejected || status === RefundStatus.investigated);

    // Final process stopped
    this.finalProcessStoppedControl = FormUtil.get<StatusHeaderViewModel>(this.form, 'finalProcessStopped');

    this.showFinalSection = this.finalControl.value !== null;
  }

  private buildForm() {
    this.statusForm = this.formFactory.createGroup<StatusViewModel>({
      status: '',
      sentToDpc: ''
    });

    this.form = this.formFactory.createGroup<StatusHeaderViewModel>({
      docNumber: '',
      status: this.statusForm,
      bspName: '',
      airlCode: '',
      agentCode: '',
      appDate: new Date(),
      amount: '0',
      finalType: null,
      finalDaysLeft: null,
      finalProcessStopped: null
    });

    this.formData.setSectionFormGroup('statusHeader', this.form);
  }

  private setConfig() {
    this.refundType = this.formConfig.refundType;
    this.headerFields = this.formConfig.getHeaderStatusFields();

    switch (this.formConfig.accessType) {
      case AccessType.read:
        this.initializeBtnOptions();
        break;
      case AccessType.edit:
        this.headerBtn = this.formConfig.getHeaderBtn();
        this.headerDiscardBtn = this.formConfig.getHeaderDiscardButton();
        break;
      default:
        this.headerBtn = this.formConfig.getHeaderBtn();
        break;
    }
  }

  private initializeChangesListener() {
    this.formData
      .getSubject(RefundActionEmitterType.agent)
      .pipe(takeUntil(this.destroy$))
      .subscribe(agent => {
        this.updateBasicAgentFields(agent);
      });
    this.formData
      .getSubject(RefundActionEmitterType.airline)
      .pipe(takeUntil(this.destroy$))
      .subscribe(airline => {
        this.updateBasicAirlineFields(airline);
      });
    this.formData
      .getSubject(RefundActionEmitterType.totalAmount)
      .pipe(takeUntil(this.destroy$))
      .subscribe(amount => {
        FormUtil.get<StatusHeaderViewModel>(this.form, 'amount').setValue(amount, { onlySelf: true });
      });
    this.formData
      .getSubject(RefundActionEmitterType.currency)
      .pipe(takeUntil(this.destroy$))
      .subscribe(currency => {
        this.currency = currency;
      });
    this.formData
      .getSubject(RefundActionEmitterType.bsp)
      .pipe(takeUntil(this.destroy$))
      .subscribe(bsp => {
        this.updateBasicBspFields(bsp);
      });
  }

  private getStatusValue(): StatusViewModel {
    return FormUtil.get<StatusHeaderViewModel>(this.form, 'status').value;
  }

  private numberStringIsZero(number: string): boolean {
    return parseFloat(number) === 0;
  }

  private updateBasicAgentFields(agent: AgentViewModel) {
    if (agent) {
      this.form.patchValue({ agentCode: agent.iataCode }, { emitEvent: false });
    } else {
      this.form.reset({ appDate: this.form.get('appDate').value }, { emitEvent: false });
    }
  }

  private updateBasicAirlineFields(airline: AirlineViewModel) {
    if (airline) {
      this.form.patchValue({ airlCode: airline.iataCode }, { emitEvent: false });
    } else {
      this.form.reset({ appDate: this.form.get('appDate').value }, { emitEvent: false });
    }
  }

  private initializeBtnOptions() {
    const refundItem = this.activatedRoute.snapshot.data.item;

    this.loggedUserType$.pipe(takeUntil(this.destroy$)).subscribe(userType => {
      this.loggedUserType = userType;
      this.showAgentDropdownBtn = userType === UserType.AGENT || userType === UserType.AGENT_GROUP;
      this.showAirlDropdownBtn = userType === UserType.AIRLINE;
    });
    this.refundService
      .getRefundActionList(refundItem.id)
      .pipe(takeUntil(this.destroy$))
      .subscribe(actions => {
        this.headerBtnOptions = this.formConfig.formatHeaderBtnOptions(actions, refundItem);
        this.cd.detectChanges();
      });
  }

  private updateHeaderOverlapState(): void {
    const headerTop: number = this.statusHeader.nativeElement.getBoundingClientRect().top;

    // Status header is not in the viewport and is therefore overlapped if it's top position is 0
    this._isHeaderOverlapped = !headerTop;
  }
}
