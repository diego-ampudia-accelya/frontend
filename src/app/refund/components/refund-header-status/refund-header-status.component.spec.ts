import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundActionType } from '../../models/refund.model';
import { RefundHeaderStatusComponent } from './refund-header-status.component';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { BspTimeModel, SendToDpcType } from '~app/refund/models/refund-be-aux.model';
import { AgentViewModel, AirlineViewModel, CurrencyViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundConfigService } from '~app/refund/shared/helpers/refund.factory';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { AccessType } from '~app/shared/enums';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { Bsp } from '~app/shared/models/bsp.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { AgentUser, UserType } from '~app/shared/models/user.model';
import { NotificationService } from '~app/shared/services';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

const activatedRouteStub = {
  snapshot: {
    data: {
      item: {
        id: 123,
        ticketDocumentNumber: 12345
      }
    }
  }
};

const mockedUser: AgentUser = {
  id: 10126,
  email: 'airline1@example.com',
  firstName: 'firstName',
  lastName: 'lastName',
  userType: UserType.AGENT,
  agent: {
    id: 11,
    iataCode: '1212',
    bsp: {
      id: 9083,
      isoCountryCode: 'ES',
      name: 'Spain',
      effectiveFrom: '2019-01-02'
    },
    effectiveFrom: '2019-01-02',
    name: 'mocked'
  },
  permissions: []
};

const initialState = {
  auth: {
    user: mockedUser
  },
  core: {
    menu: {
      tabs: {}
    }
  },
  refund: {
    refundForm: {}
  }
};

describe('RefundHeaderStatusComponent', () => {
  let component: RefundHeaderStatusComponent;
  let fixture: ComponentFixture<RefundHeaderStatusComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundHeaderStatusComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule,
        NoopAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        RefundService,
        provideMockStore({ initialState }),
        mockProvider(LocalBspTimeService),
        HttpTestingController,
        FormBuilder,
        L10nTranslationService,
        HttpClient,
        RefundActionsService,
        mockProvider(NotificationService),
        mockProvider(NotificationsHeaderService)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundHeaderStatusComponent);
    component = fixture.componentInstance;
    component.refundService = new RefundService(TestBed.inject(HttpClient), TestBed.inject(ActivatedRoute));
    component.formData = new RefundDataService(TestBed.inject(FormBuilder));
    component.formConfig = createRefundConfigService(
      component.formData,
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Store),
      TestBed.inject(RefundActionsService)
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit on issue click', () => {
    spyOn(component.actionClick, 'emit');
    component.onApplyAction(RefundActionType.issue);
    expect(component.actionClick.emit).toHaveBeenCalledWith(RefundActionType.issue);
  });

  it('should emit on modify click', () => {
    spyOn(component.actionClick, 'emit');
    component.onApplyAction(RefundActionType.edit);
    expect(component.actionClick.emit).toHaveBeenCalledWith(RefundActionType.edit);
  });

  it('should emit on change click', () => {
    spyOn(component.actionClick, 'emit');
    component.onApplyAction(RefundActionType.applyChanges);
    expect(component.actionClick.emit).toHaveBeenCalledWith(RefundActionType.applyChanges);
  });

  it('should format a date field correctly', () => {
    const mockDate = new Date('2019');
    component.form.get('appDate').setValue(mockDate);

    expect(component.getFormattedField('appDate')).toBe('01/01/2019');
  });

  it('should format an amount field correctly', () => {
    component.currency = { code: 'EUR' } as any;
    component.form.get('amount').setValue('1');

    expect(component.getFormattedField('amount')).toBe('1');
  });

  it('should identify a field as inactive correctly', () => {
    component.form.get('amount').setValue(null);

    expect(component.isFieldInactive('amount')).toBe(true);
  });

  it('should call initializeChangesListener if AccessType is not read', fakeAsync(() => {
    component.formConfig.accessType = AccessType.edit;
    spyOn<any>(component.formData.isFormReady, 'pipe').and.returnValue(of(true));
    spyOn<any>(component, 'initializeChangesListener');
    spyOn<any>(component, 'initializeBadgeInfo');
    spyOn<any>(component, 'initializeFinalSection');

    component.ngOnInit();

    tick();

    expect(component['initializeChangesListener']).toHaveBeenCalled();
    expect(component['initializeBadgeInfo']).toHaveBeenCalled();
    expect(component['initializeFinalSection']).toHaveBeenCalled();
  }));

  it('should call especific functions if AccessType is read', fakeAsync(() => {
    component.formConfig.accessType = AccessType.read;

    const currency: CurrencyViewModel = {
      code: '',
      decimals: 1,
      id: 1234,
      isDefault: true
    };

    const actions = [
      {
        action: GridTableActionType.Activity
      }
    ];
    spyOn(component.refundService, 'getRefundActionList').and.returnValue(of(actions));
    spyOn<any>(component.formData, 'getSubject').and.returnValue(of(currency));
    spyOn<any>(component, 'initializeBadgeInfo');
    spyOn<any>(component, 'initializeUrgencyIcon');
    spyOn<any>(component, 'initializeWatchingFlagAndActions');
    spyOn<any>(component, 'initializeFinalSection');

    component.ngOnInit();

    tick();

    expect(component['initializeBadgeInfo']).toHaveBeenCalled();
    expect(component['initializeUrgencyIcon']).toHaveBeenCalled();
    expect(component['initializeWatchingFlagAndActions']).toHaveBeenCalled();
    expect(component['initializeFinalSection']).toHaveBeenCalled();
    expect(component.currency).toEqual(currency);
  }));

  it('should call formData whenFormIsReady if is Resubmission', fakeAsync(() => {
    component.formConfig.isResubmission = true;
    spyOn<any>(component.formData, 'whenFormIsReady').and.returnValue(of(null));

    component.ngOnInit();

    tick();

    expect(component.formData['whenFormIsReady']).toHaveBeenCalled();
  }));

  it('should update variables if when initializeSecondaryHeaderActions is called', () => {
    spyOn<any>(component, 'initializeSecondaryHeaderActions').and.callThrough();

    component['initializeSecondaryHeaderActions']();

    expect(component.headerSecondaryActions.length).toBeGreaterThan(0);
  });

  it('should call correct functions when toggleHeaderSecondaryActionsMenu is called with event', () => {
    const event = new Event('onClick');

    spyOn(component, 'toggleHeaderSecondaryActionsMenu').and.callThrough();
    spyOn(event, 'stopPropagation').and.callThrough();

    component.toggleHeaderSecondaryActionsMenu(event);

    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('should set showHeaderSecondaryActions to false when toggleHeaderSecondaryActionsMenu is called without event', () => {
    spyOn(component, 'toggleHeaderSecondaryActionsMenu').and.callThrough();

    component.toggleHeaderSecondaryActionsMenu(undefined);

    expect(component.showHeaderSecondaryActions).toBeFalsy();
  });

  it('should emit actionDiscardClick when onDiscardAction is called', () => {
    const actionType = RefundActionType.issue;

    spyOn(component.actionDiscardClick, 'emit').and.callThrough();

    component.onDiscardAction(actionType);

    expect(component.actionDiscardClick.emit).toHaveBeenCalledWith(actionType);
  });

  it('should call downloadPdf when onActionClick is called', () => {
    spyOn(component, 'downloadPdf').and.callFake(() => {});

    component.onActionClick({ action: GridTableActionType.Download, label: '' });

    expect(component.downloadPdf).toHaveBeenCalled();
    expect(component.showHeaderSecondaryActions).toBeFalsy();
  });

  it('should set correct badgeInfoType Info and BadgeInfoLeyend Pending when initializeBadgeInfo is called', () => {
    const expectedBadgeInfoLeyend = 'REFUNDS.badgeInfo.leyend.PENDING';

    const status = {
      sentToDpc: SendToDpcType.pending,
      status: 'REJECTED'
    };
    component.form.get('status').setValue(status);

    component['initializeBadgeInfo']();

    expect(component.badgeInfoLeyend).toEqual(expectedBadgeInfoLeyend);
    expect(component.badgeInfoType).toEqual(BadgeInfoType.info);
  });

  it('should set correct badgeInfoType success when initializeBadgeInfo is called', () => {
    const status = {
      sentToDpc: SendToDpcType.sent,
      status: 'REJECTED'
    };
    component.form.get('status').setValue(status);

    component['initializeBadgeInfo']();

    expect(component.badgeInfoType).toEqual(BadgeInfoType.success);
  });

  it('should set correct badgeInfoType regular when initializeBadgeInfo is called', () => {
    const status = {
      sentToDpc: SendToDpcType.na,
      status: 'REJECTED'
    };
    component.form.get('status').setValue(status);

    component['initializeBadgeInfo']();

    expect(component.badgeInfoType).toEqual(BadgeInfoType.regular);
  });

  it('should set correct iconType when initializeUrgencyIcon is called and RA is urgent', () => {
    component.formConfig.refundType = MasterDataType.RefundApp;

    spyOn<any>(component, 'isUrgencyIconHidden').and.returnValue(false);
    spyOn<any>(component, 'getDateDifference').and.returnValue(48);

    component['initializeUrgencyIcon']();

    expect(component.iconType).toEqual('error');
  });

  it('should return false when isUrgencyIconHidden is called and status is Pending', () => {
    const status = {
      sentToDpc: SendToDpcType.pending,
      status: 'PENDING'
    };
    component.form.get('status').setValue(status);

    spyOn<any>(component, 'getDateDifference').and.returnValue(48);

    const expected = component['isUrgencyIconHidden']();

    expect(expected).toBeFalsy();
  });

  it('should set correct iconType when initializeUrgencyIcon is called and RA is not urgent', () => {
    component.formConfig.refundType = MasterDataType.RefundApp;

    spyOn<any>(component, 'isUrgencyIconHidden').and.returnValue(false);
    spyOn<any>(component, 'getDateDifference').and.returnValue(15);

    component['initializeUrgencyIcon']();

    expect(component.iconType).toEqual('warning');
  });

  it('should return true when numberStringIsZero is called and number is 0', () => {
    component['numberStringIsZero']('0');

    expect(component['numberStringIsZero']).toBeTruthy();
  });

  it('should set agentCode when updateBasicAgentFields is called and agent exist', () => {
    const agent: AgentViewModel = {
      id: 1234,
      iataCode: 'AgentIataCode',
      name: '',
      vatNumber: '',
      bsp: null,
      contact: null,
      address: null
    };
    component['updateBasicAgentFields'](agent);

    const agentIataControlValue = component.form.get('agentCode').value;

    expect(agentIataControlValue).toEqual('AgentIataCode');
  });

  it('should set airlCode when updateBasicAirlineFields is called and airline exist', () => {
    const airline: AirlineViewModel = {
      id: 1234,
      iataCode: 'AirlineIataCode',
      localName: '',
      globalName: '',
      vatNumber: '',
      bsp: null,
      contact: null,
      address: null
    };

    component['updateBasicAirlineFields'](airline);

    const airlineIataControlValue = component.form.get('airlCode').value;

    expect(airlineIataControlValue).toEqual('AirlineIataCode');
  });

  it('should set bsp value when updateBasicBspFields is called', () => {
    const bsp: Bsp = {
      id: 777,
      name: 'bspName',
      isoCountryCode: 'ES',
      effectiveFrom: '26/05/2022'
    };

    const bspTime: BspTimeModel = {
      id: 222,
      localTime: new Date('26/05/2022')
    };

    component.formConfig.accessType = AccessType.create;

    spyOn(component.refundService, 'getBspTime').and.returnValue(of(bspTime));

    component.updateBasicBspFields(bsp);

    const bspControlValue = component.form.get('bspName').value;

    expect(bspControlValue).toEqual('Bspname (ES)');
    expect(component.refundService.getBspTime).toHaveBeenCalled();
  });
});
