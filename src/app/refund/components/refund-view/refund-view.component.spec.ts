import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, mockProvider, Spectator } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundViewComponent } from './refund-view.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { Commission } from '~app/refund/models/refund-shared.model';
import { RefundActionType, RefundFinal } from '~app/refund/models/refund.model';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundHistoryService } from '~app/refund/services/refund-history.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import { getBspName, getUserDetails } from '~app/refund/shared/helpers/refund.helper';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { AccessType } from '~app/shared/enums';
import { Bsp } from '~app/shared/models/bsp.model';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { AppConfigurationService, DocumentPdfExporter, TabService } from '~app/shared/services';

const mockRefund = {
  id: 69830000,
  type: 'REFUND_APPLICATION',
  ticketDocumentNumber: '0051007',
  bsplinkDocument: '0051007',
  status: 'PENDING',
  sentToDpc: 'PENDING',
  sentToAirline: 'N',
  changedByAgent: 'N',
  airline: {
    id: 6983485,
    iataCode: '075',
    bsp: {
      id: 6188,
      version: 0,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    localName: 'Nombre 5',
    designator: 'IM'
  },
  airlineAddress: {
    address1: 'UPDATE',
    address2: '',
    city: 'UPDATE ',
    state: null,
    country: 'TURKEY',
    postalCode: 'ZIP',
    telephone: '222222_br'
  },
  airlineVatNumber: 'A28017555',
  airlineContact: {
    id: 69835966,
    contactName: 'TEST LINEAS AEREAS',
    phoneFaxNumber: '222211_br',
    email: 'test@test.com'
  },
  agent: {
    id: 698378211,
    iataCode: '7825555',
    bsp: {
      id: 6188,
      version: 0,
      isoCountryCode: 'ES',
      name: 'SPAIN'
    },
    name: 'NOMBRE 55510'
  },
  agentAddress: {
    street: 'UPDATE ON ',
    city: 'UPDATE ON S',
    state: 'KAKA',
    country: 'MRU',
    postalCode: '88020-320',
    telephone: '+67666666666'
  },
  agentVatNumber: '20636074/001255',
  agentContact: {
    contactName: 'subuser1',
    phoneFaxNumber: '+34666666667',
    email: 'test@accelya.com;'
  },
  statisticalCode: 'I',
  passenger: 'PASSENGER',
  reasonForRefund: 'reason',
  airlineCodeRelatedDocument: '075',
  relatedTicketDocument: {
    documentId: null,
    documentNumber: '1111111111',
    coupon1: true,
    coupon2: false,
    coupon3: false,
    coupon4: false
  },
  finalType: RefundFinal.Exception,
  relatedTicketConjunctions: [],
  dateOfIssueRelatedDocument: '2021-04-01',
  waiverCode: '',
  relatedTicketExchange: null,
  originalIssue: null,
  cashPaymentAmount: '1.00',
  totalPaymentCardAmount: '0.00',
  totalCashAmount: '1.00',
  totalPaymentAmount: '1.00',
  cardPayments: [],
  currency: {
    id: 698582,
    code: 'EUR',
    decimals: 2
  },
  tourCode: null,
  settlementAuthorisationCode: null,
  customerFileReference: null,
  partialRefund: false,
  netReporting: false,
  grossFare: '1.00',
  lessGrossFareUsed: '0.00',
  totalGrossFare: '1.00',
  miscellaneousFee: '0.00',
  cancellationPenalty: '0.00',
  taxOnMiscellaneousFee: '0.00',
  taxOnCancellationPenalty: '0.00',
  supplementaryCommission: '0.00',
  totalAmount: '1.00',
  commission: {
    amount: '0.00'
  },
  commissionForCpAndMf: {},
  taxMiscellaneousFees: [],
  final: null,
  finalDaysLeft: null,
  finalProcessStopped: null,
  dateOfLatestAction: '2021-04-30',
  attachmentIds: [],
  version: 0,
  dateOfApplication: '2021-04-30',
  dateOfIssue: '2022-04-30',
  dateOfAuthorization: null,
  airlineRemark: '',
  reportingPeriod: null,
  internalComment: 'test internal comment'
};

const mockAgent = {
  item: mockRefund,
  refundType: MasterDataType.RefundNotice
};

const initialState = {
  auth: {
    user: mockAgent
  },
  core: {
    menu: {
      tabs: {}
    }
  },
  refund: {}
};

const route = { snapshot: { data: mockAgent } as any } as ActivatedRoute;

describe('RefundViewComponent', () => {
  let spectator: Spectator<RefundViewComponent>;

  const createComponent = createComponentFactory({
    component: RefundViewComponent,
    declarations: [DateTimeFormatPipe],
    imports: [RouterTestingModule, L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
    providers: [
      RefundService,
      FormBuilder,
      mockProvider(TabService),
      RefundDataService,
      DocumentPdfExporter,
      RefundHistoryService,
      PermissionsService,
      RefundInternalCommentDialogService,
      { provide: ActivatedRoute, useValue: route },
      mockProvider(AppConfigurationService, { baseApiPath: '', baseUploadPath: '' }),
      provideMockStore({ initialState })
    ],
    mocks: [RefundConfigService],
    schemas: [NO_ERRORS_SCHEMA]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('onDownloadClick - change flags', () => {
    expect(spectator.component.airlineDetailsVisible).toBeFalsy();
    expect(spectator.component.agentDetailsVisible).toBeFalsy();

    spectator.component.onDownloadClick();

    expect(spectator.component.airlineDetailsVisible).toBeTruthy();
    expect(spectator.component.agentDetailsVisible).toBeTruthy();
  });

  it('getCommissionLabel - with commission complete', () => {
    const commission: Commission = { amount: 5, rate: 2 };
    const label = 'test';

    const response = spectator.component.getCommissionLabel(label, commission);

    expect(response).toBe('test, %');
  });

  it('getCommissionLabel - without commission rate ', () => {
    const commission: Commission = { amount: 5, rate: null };
    const label = 'test';

    const response = spectator.component.getCommissionLabel(label, commission);

    expect(response).toBe(`${label}, ${mockRefund.currency.code}`);
  });

  it('getCommissionValue - rate null ', () => {
    const commission: Commission = { amount: 5, rate: null };

    const response = spectator.component.getCommissionValue(commission);

    expect(response).toBe(5);
  });

  it('getCommissionValue - amount null ', () => {
    const commission: Commission = { amount: null, rate: 2 };

    const response = spectator.component.getCommissionValue(commission);

    expect(response).toBe(2);
  });

  it('getCommissionValue -commission null ', () => {
    const commission: Commission = { amount: null, rate: null };

    const response = spectator.component.getCommissionValue(commission);

    expect(response).toBe('N/A');
  });

  it('commissionHasValue - commission null', () => {
    const commission: Commission = { amount: null, rate: null };

    const response = spectator.component.commissionHasValue(commission);

    expect(response).toBeFalsy();
  });

  it('commissionHasValue - rate null ', () => {
    const commission: Commission = { amount: 4, rate: null };

    const response = spectator.component.commissionHasValue(commission);

    expect(response).toBeTruthy();
  });

  it('commissionHasValue - change true value to false', () => {
    spectator.component.airlineDetailsVisible = true;

    spectator.component.toggleAirlineDetails();

    expect(spectator.component.airlineDetailsVisible).toBeFalsy();
  });

  it('toggleAirlineDetails - change false value to true', () => {
    spectator.component.airlineDetailsVisible = false;

    spectator.component.toggleAirlineDetails();

    expect(spectator.component.airlineDetailsVisible).toBeTruthy();
  });

  it('toggleAgentDetails - change false value to true', () => {
    spectator.component.agentDetailsVisible = false;

    spectator.component.toggleAgentDetails();

    expect(spectator.component.agentDetailsVisible).toBeTruthy();
  });

  it('toggleAgentDetails - change true value to false', () => {
    spectator.component.agentDetailsVisible = true;

    spectator.component.toggleAgentDetails();

    expect(spectator.component.agentDetailsVisible).toBeFalsy();
  });

  it('onAccionClick - RefundActionType is on edit', () => {
    const onModifySpy = spyOn<any>(spectator.component, 'onModifyClick').and.callThrough();

    spectator.component.onAccionClick(RefundActionType.edit);

    expect(onModifySpy).toHaveBeenCalled();
  });

  it('onAccionClick - RefundActionType is other', () => {
    const onModifySpy = spyOn<any>(spectator.component, 'onModifyClick').and.callThrough();

    spectator.component.onAccionClick(RefundActionType.applyChanges);

    expect(onModifySpy).not.toHaveBeenCalled();
  });

  it('onSendCommentRefund - call addComment and initializeActivities ', fakeAsync(() => {
    const addCommentSpy = spyOn(spectator.component.refundService, 'addComment').and.callThrough();

    spectator.component.onSendCommentRefund({ reason: [] });
    spectator.tick();

    expect(addCommentSpy).toHaveBeenCalled();
  }));

  it('onDownloadFile -  call service to download file', fakeAsync(() => {
    const downloadFileSpy = spyOn(spectator.component.refundService, 'downloadFile')
      .and.returnValue(of(null))
      .and.callThrough();

    spectator.component.onDownloadFile('url');
    spectator.tick();

    expect(downloadFileSpy).toHaveBeenCalled();
  }));

  it('onHeaderHeightChange - set value successfully', () => {
    spectator.component.onHeaderHeightChange(84);

    expect(spectator.component.headerHeight).toBe(84);
  });

  it('onInternalComment - set value successfully', () => {
    const dialogSpy = spyOn(spectator.component['refundInternalCommentDialogService'], 'open').and.callThrough();
    spectator.component.onInternalComment();

    expect(dialogSpy).toHaveBeenCalled();
  });

  it('isPenaltyAmountVisible - call service from formconfig ', fakeAsync(() => {
    const formConfigSpy = spectator.inject(RefundConfigService);

    formConfigSpy.isPenaltyAmountFieldVisible.and.returnValue(of(true));
    let result;

    spectator.component['isPenaltyAmountVisible']('totalAmount', '5').subscribe(value => (result = value));
    spectator.tick();
    expect(result).toBe(true);
  }));

  it('initializeActivityPanel - Initialize activities and checkFieldsVisibility', () => {
    const initializeActivitiesSpy = spyOn<any>(spectator.component, 'initializeActivities').and.callThrough();
    const checkActivityCommentVisibilitySpy = spyOn<any>(
      spectator.component,
      'checkActivityCommentVisibility'
    ).and.callThrough();

    spectator.component['initializeActivityPanel']();

    expect(initializeActivitiesSpy).toHaveBeenCalled();
    expect(checkActivityCommentVisibilitySpy).toHaveBeenCalled();
  });

  it('checkActivityCommentVisibility - set true to isActivityCommentVisible ', () => {
    spyOn(spectator.component.formConfig, 'accessType').and.returnValue(AccessType.edit);

    spectator.component['checkActivityCommentVisibility']();

    expect(spectator.component.isActivityCommentVisible).toBeTruthy();
  });

  it('getHeaderDate - get header date from dateOfApplication', () => {
    spyOnProperty(spectator.component.formConfig, 'isRa').and.returnValue(true);

    const result = spectator.component['getHeaderDate']();

    expect(result).toBe(mockRefund.dateOfApplication);
  });

  it('getHeaderDate - get header date from ', () => {
    spyOnProperty(spectator.component.formConfig, 'isRa').and.returnValue(false);

    const result = spectator.component['getHeaderDate']();

    expect(result).toBe(mockRefund.dateOfIssue);
  });

  it('onModifyClick - tabService is called ', () => {
    const tabServiceMock = spectator.inject(TabService);

    const tabServiceSpy = tabServiceMock.closeCurrentTabAndNavigate.and.callThrough();

    spectator.component['onModifyClick']();

    expect(tabServiceSpy).toHaveBeenCalled();
  });

  it('getStatCodeName - return statCode label', () => {
    const result = spectator.component['getStatCodeName']();

    expect(result).toBe('REFUNDS.basicInfo.statValues.international');
  });

  it('getStatCodeName - return statisticalCode', () => {
    spyOn(spectator.component.formConfig, 'getDefaultStats').and.returnValue([]);

    const result = spectator.component['getStatCodeName']();

    expect(result).toBe(mockRefund.statisticalCode);
  });

  it('initializeUserDetails - should initialize details of agent and airline', () => {
    spectator.component.agentDetails = null;
    spectator.component.airlineDetails = null;

    expect(spectator.component.agentDetails).toBeNull();
    expect(spectator.component.airlineDetails).toBeNull();

    spectator.component['initializeUserDetails']();

    expect(spectator.component.agentDetails).not.toBeNull();
    expect(spectator.component.agentDetails).toEqual(getUserDetails(mockAgent.item.agentAddress));
    expect(spectator.component.airlineDetails).not.toBeNull();
    expect(spectator.component.airlineDetails).toEqual(getUserDetails(mockAgent.item.airlineAddress));
  });

  it('initializeRouteData -  should initialize router data', () => {
    spectator.component.itemRetrieved = null;
    spectator.component.formConfig.accessType = null;
    spectator.component.formConfig.refundType = null;

    expect(spectator.component.itemRetrieved).toBeNull();
    expect(spectator.component.formConfig.refundType).toBeNull();
    expect(spectator.component.formConfig.accessType).toBeNull();

    spectator.component['initializeRouteData']();

    expect(spectator.component.itemRetrieved).not.toBeNull();
    expect(spectator.component.itemRetrieved.id).toBe(mockAgent.item.id);
    expect(spectator.component.formConfig.refundType).not.toBeNull();
    expect(spectator.component.formConfig.refundType).toBe(mockAgent.refundType);
    expect(spectator.component.formConfig.accessType).not.toBeNull();
    expect(spectator.component.formConfig.accessType).toBe(AccessType.read);
  });

  it('initializeActivities - should initialize activities', () => {
    const activity: Activity = {
      attachments: null,
      author: {
        contact: null,
        type: '2',
        code: '0015'
      },
      creationDate: new Date('01/01/01'),

      params: [{ name: 'hola', value: 'text' }],
      type: 'test'
    };
    spyOn(spectator.component.refundHistoryService, 'getActivities').and.returnValue(of([activity]));

    spectator.component.activities = null;

    expect(spectator.component.activities).toBeNull();

    spectator.component['initializeActivities']();

    expect(spectator.component.activities).not.toBeNull();
    expect(spectator.component.activities[0].type).toBe(activity.type);
  });

  it('emitDataValues - should emit data respectively', () => {
    const formDataSpy = spyOn(spectator.component.formData, 'getSubject').and.callThrough();

    spectator.component['emitDataValues']();

    expect(formDataSpy).toHaveBeenCalledTimes(4);
  });

  it('initializeBspName - should emit data respectively', () => {
    const bsp: Bsp = { id: 3, name: 'test', isoCountryCode: 'TE', effectiveFrom: '01/01/01' };
    spyOn(spectator.component.formData, 'getSubject').and.returnValue(of(bsp));

    spectator.component['initializeBspName']();

    expect(spectator.component.bspName).toBe(getBspName(bsp));
  });

  it('checkFieldsVisibility - should set values in all parts', () => {
    spectator.component['checkFieldsVisibility']();

    expect(spectator.component.showExceptionDisclaimer).not.toBeNull();
    expect(spectator.component.showExceptionDisclaimer).toBeTruthy();
    expect(spectator.component.isRemarksVisible).not.toBeNull();
    expect(spectator.component.isRemarksVisible).toBeFalsy(); // status from mockAgent is PENDING
    expect(spectator.component.areCardsVisible).not.toBeNull();
    expect(spectator.component.areCardsVisible).toBeFalsy(); // array is empty
    expect(spectator.component.isTaxesListVisible).not.toBeNull();
    expect(spectator.component.isTaxesListVisible).toBeFalsy(); // array is empty
  });

  it('initializeFieldsVisibility - should call other functions to initialize fields visibility ', () => {
    const initializeVatFieldsVisibilitySpy = spyOn<any>(
      spectator.component,
      'initializeVatFieldsVisibility'
    ).and.callThrough();
    const initializeContactFieldsVisibilitySpy = spyOn<any>(
      spectator.component,
      'initializeContactFieldsVisibility'
    ).and.callThrough();
    const initializeFormPaymentsConfFieldsVisibilitySpy = spyOn<any>(
      spectator.component,
      'initializeFormPaymentsConfFieldsVisibility'
    ).and.callThrough();
    const initializeNetReportingVisibilitySpy = spyOn<any>(
      spectator.component,
      'initializeNetReportingVisibility'
    ).and.callThrough();
    const initializeAmountTaxFieldsVisibilitySpy = spyOn<any>(
      spectator.component,
      'initializeAmountTaxFieldsVisibility'
    ).and.callThrough();

    spectator.component['initializeFieldsVisibility']();

    expect(initializeVatFieldsVisibilitySpy).toHaveBeenCalled();
    expect(initializeContactFieldsVisibilitySpy).toHaveBeenCalled();
    expect(initializeFormPaymentsConfFieldsVisibilitySpy).toHaveBeenCalled();
    expect(initializeNetReportingVisibilitySpy).toHaveBeenCalled();
    expect(initializeAmountTaxFieldsVisibilitySpy).toHaveBeenCalled();
  });

  it('initializeVatFieldsVisibility - should initialize vat fields', () => {
    const agentVatSpy = spyOn(spectator.component.formConfig, 'isAgentVatFieldVisible').and.callThrough();
    const airlineVatSpy = spyOn(spectator.component.formConfig, 'isAirlineVatFieldVisible').and.callThrough();

    spectator.component['initializeVatFieldsVisibility']();

    expect(agentVatSpy).toHaveBeenCalled();
    expect(agentVatSpy).toHaveBeenCalledWith(mockAgent.item.agentVatNumber);
    expect(airlineVatSpy).toHaveBeenCalled();
    expect(airlineVatSpy).toHaveBeenCalledWith(mockAgent.item.airlineVatNumber);
  });

  it('initializeContactFieldsVisibility - should initialize contact fields', () => {
    const contactSpy = spyOn(spectator.component.formConfig, 'isContactFieldVisible').and.callThrough();

    spectator.component['initializeContactFieldsVisibility']();

    expect(contactSpy).toHaveBeenCalledTimes(2);
  });

  it('initializeFormPaymentsConfFieldsVisibility - should initialize form of payment fields', () => {
    const customerRefSpy = spyOn(spectator.component.formConfig, 'isCustomerReferenceVisible').and.callThrough();
    const eTicketCodeSpy = spyOn(spectator.component.formConfig, 'iseTicketCodeVisible').and.callThrough();

    spectator.component['initializeFormPaymentsConfFieldsVisibility']();

    expect(customerRefSpy).toHaveBeenCalled();
    expect(eTicketCodeSpy).toHaveBeenCalled();
  });

  it('initializeNetReportingVisibility - should initialize net reporting fields', () => {
    const netReportingSpy = spyOn(spectator.component.formConfig, 'getNRRefundsPermitted').and.callThrough();

    spectator.component['initializeNetReportingVisibility']();

    expect(netReportingSpy).toHaveBeenCalled();
  });

  it('initializeAmountTaxFieldsVisibility - should initialize amount tax fields', () => {
    const initializeDiscountAmountVisibilitySpy = spyOn<any>(
      spectator.component,
      'initializeDiscountAmountVisibility'
    ).and.callThrough();
    const isPenaltyAmountVisibleSpy = spyOn<any>(spectator.component, 'isPenaltyAmountVisible').and.callThrough();

    spectator.component['initializeAmountTaxFieldsVisibility']();

    expect(initializeDiscountAmountVisibilitySpy).toHaveBeenCalled();
    expect(isPenaltyAmountVisibleSpy).toHaveBeenCalledTimes(5);
  });

  it('initializeDiscountAmountVisibility - should execute bspRefundNoticeConfiguration ', () => {
    const bspRefundNoticeConfigurationSpy = spyOnProperty(
      spectator.component.formConfig,
      'bspRefundNoticeConfiguration'
    ).and.callThrough();

    spectator.component['initializeDiscountAmountVisibility']();

    expect(bspRefundNoticeConfigurationSpy).toHaveBeenCalled();
  });

  it('initializeDiscountAmountVisibility - should execute getFareAdjustmentAmount', () => {
    const getFareAdjustmentAmountSpy = spyOn(
      spectator.component.formConfig,
      'getFareAdjustmentAmount'
    ).and.callThrough();
    spectator.component.formConfig.refundType = MasterDataType.RefundApp;
    spectator.component['initializeDiscountAmountVisibility']();

    expect(getFareAdjustmentAmountSpy).toHaveBeenCalled();
  });

  it('initializeInternalCommentVisibility - should be isInternalCommentButtonVisible false because doesnt have correct permissions', () => {
    spectator.component['initializeInternalCommentVisibility']();

    expect(spectator.component.isInternalCommentButtonVisible).toBe(false);
  });

  it('initializeInternalCommentVisibility - should be isInternalCommentButtonVisible true because has correct read permission', () => {
    spyOn(spectator.component['permissionsService'], 'hasPermission').and.returnValue(true);

    spectator.component['initializeInternalCommentVisibility']();

    expect(spectator.component.isInternalCommentButtonVisible).toBe(true);
  });
});
