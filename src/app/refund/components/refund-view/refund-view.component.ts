import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import FileSaver from 'file-saver';
import { Observable, of, Subject } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';

import { PermissionsService } from '~app/auth/services/permissions.service';
import { Agent } from '~app/master-data/models';
import { LocalAndGlobalAirline } from '~app/master-data/models/airline/local-and-global-airline.model';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { Commission } from '~app/refund/models/refund-shared.model';
import {
  AgentViewModel,
  AmountTaxViewModel,
  StatusHeaderViewModel,
  StatusViewModel
} from '~app/refund/models/refund-view-aux.model';
import {
  RefundActionEmitterType,
  RefundActionType,
  RefundBE,
  RefundFinal,
  RefundType
} from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundHistoryService } from '~app/refund/services/refund-history.service';
import { RefundInternalCommentDialogService } from '~app/refund/services/refund-internal-comment-dialog.service';
import { RefundService } from '~app/refund/services/refund.service';
import {
  getRefundCreateInternalCommentPermission,
  getRefundReadInternalCommentPermission
} from '~app/refund/shared/helpers/refund-permissions.config';
import { createRefundConfigService } from '~app/refund/shared/helpers/refund.factory';
import { getBspName, getBspNameFromAgent, getUserDetails } from '~app/refund/shared/helpers/refund.helper';
import { ButtonDesign } from '~app/shared/components';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { ROUTES } from '~app/shared/constants/routes';
import { AccessType, AlertMessageType } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';
import { Currency } from '~app/shared/models/currency.model';
import { appConfiguration, DocumentPdfExporter, TabService } from '~app/shared/services';

@Component({
  selector: 'bspl-refund-view',
  templateUrl: './refund-view.component.html',
  styleUrls: ['./refund-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    RefundService,
    RefundDataService,
    {
      provide: RefundConfigService,
      useFactory: createRefundConfigService,
      deps: [RefundDataService, L10nTranslationService, HttpClient, Store, RefundActionsService]
    },
    DocumentPdfExporter
  ]
})
export class RefundViewComponent implements OnInit {
  public activities: Activity[] = [];

  public itemRetrieved: RefundBE;

  public headerFormGroup: FormGroup;

  public isTypeRefundApplication: boolean;

  public isAirlineVatFieldVisible$: Observable<boolean>;
  public isAgentVatFieldVisible$: Observable<boolean>;

  public isAirlineContactFieldVisible$: Observable<boolean>;
  public isAgentContactFieldVisible$: Observable<boolean>;

  public isCustomerRefFieldVisible$: Observable<boolean>;
  public isETicketCodeFieldVisible$: Observable<boolean>;

  public isNetReportingVisible$: Observable<boolean>;

  public isDiscountAmountVisible$: Observable<boolean>;
  public isCPVisible$: Observable<boolean>;
  public isTaxOnCPVisible$: Observable<boolean>;
  public isMiscFeeVisible$: Observable<boolean>;
  public isTaxOnMFVisible$: Observable<boolean>;
  public isCommissionForCpAndMfVisible$: Observable<boolean>;

  public partialRefundEnabled = true;

  public airlineDetailsVisible = false;
  public agentDetailsVisible = false;
  public isRemarksVisible: boolean;

  public isPaymentsVisible: boolean;
  public areCardsVisible: boolean;
  public isCashVisible: boolean;
  public isMiscCashVisible: boolean;
  public isTaxesListVisible: boolean;

  public agentDetails;
  public airlineDetails;

  public statisticalCodeName: string;
  public bspName: string;

  public objectKeys = Object.keys;
  public buttonDesign = ButtonDesign;

  public commentAdded$: Subject<Activity[]>;
  public isActivityCommentVisible = false;
  public fileUploadPath: string;

  public headerHeight: number;

  public showExceptionDisclaimer: boolean;
  public exceptionDisclaimer: { message: string; type: AlertMessageType };
  public showFinalDisclaimer: boolean;
  public finalDisclaimer: { message: string; type: AlertMessageType };

  public hasReadInternalCommentPermission: boolean;
  public isInternalCommentButtonVisible = false;

  public useNewPDFStyle: boolean = appConfiguration.useNewPDFStyle;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public formData: RefundDataService,
    public refundService: RefundService,
    public refundHistoryService: RefundHistoryService,
    public formConfig: RefundConfigService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private tabService: TabService,
    private cd: ChangeDetectorRef,
    private element: ElementRef,
    private pdfExporter: DocumentPdfExporter,
    private permissionsService: PermissionsService,
    private refundInternalCommentDialogService: RefundInternalCommentDialogService
  ) {
    this.initializeRouteData();
  }

  public ngOnInit() {
    this.initializeUserDetails();

    this.statisticalCodeName = this.getStatCodeName();

    const statusForm = new FormUtil(this.fb).createGroup<StatusViewModel>({
      status: this.itemRetrieved.status,
      sentToDpc: this.itemRetrieved.sentToDpc
    });

    this.headerFormGroup = new FormUtil(this.fb).createGroup<StatusHeaderViewModel>({
      docNumber: this.itemRetrieved.ticketDocumentNumber,
      status: statusForm,
      bspName: getBspNameFromAgent(this.itemRetrieved.agent as AgentViewModel),
      airlCode: (this.itemRetrieved.airline as LocalAndGlobalAirline).iataCode,
      agentCode: (this.itemRetrieved.agent as Agent).iataCode,
      appDate: this.getHeaderDate(),
      amount: this.itemRetrieved.totalAmount,
      finalType: this.itemRetrieved.finalType,
      finalDaysLeft: this.itemRetrieved.finalDaysLeft,
      finalProcessStopped: this.itemRetrieved.finalProcessStopped
    });

    this.exceptionDisclaimer = { message: 'REFUNDS.basicInfo.exceptionDisclaimer', type: AlertMessageType.info };
    this.finalDisclaimer = { message: 'REFUNDS.basicInfo.finalDisclaimer', type: AlertMessageType.info };

    this.checkFieldsVisibility();

    this.initializeFieldsVisibility();

    this.initializeActivityPanel();

    this.emitDataValues();

    this.initializeBspName();

    this.pdfExporter.attach(this.element);
  }

  public onDownloadClick() {
    this.airlineDetailsVisible = true;
    this.agentDetailsVisible = true;
    this.cd.detectChanges();
  }

  public getCommissionLabel(label: string, commission: Commission): string {
    const currency = this.itemRetrieved.currency as Currency;
    const suffix = commission.rate ? '%' : currency.code;

    return `${label}, ${suffix}`;
  }

  public getCommissionValue(commission: Commission): any {
    return commission.amount || commission.rate || 'N/A';
  }

  public commissionHasValue(commission: Commission): boolean {
    return commission.amount != null || commission.rate != null;
  }

  public toggleAirlineDetails(): void {
    this.airlineDetailsVisible = !this.airlineDetailsVisible;
  }

  public toggleAgentDetails(): void {
    this.agentDetailsVisible = !this.agentDetailsVisible;
  }

  public onAccionClick(actionType) {
    if (actionType === RefundActionType.edit) {
      this.onModifyClick();
    }
  }

  public onSendCommentRefund(message: { reason: string[]; attachmentIds?: string[] }) {
    this.commentAdded$ = new Subject<Activity[]>();
    this.refundService
      .addComment(message, this.itemRetrieved.id)
      .pipe(tap(() => this.initializeActivities()))
      .subscribe(this.commentAdded$);
  }

  public onDownloadFile(fileUrl) {
    this.refundService.downloadFile(fileUrl).subscribe(file => {
      FileSaver.saveAs(file.blob, file.fileName);
    });
  }

  public onHeaderHeightChange(height: number) {
    this.headerHeight = height;
  }

  public onInternalComment(): void {
    this.refundInternalCommentDialogService.open(this.itemRetrieved, this.formConfig.refundType);
  }

  private isPenaltyAmountVisible(field: keyof AmountTaxViewModel, value?: string): Observable<boolean> {
    return this.formConfig.isPenaltyAmountFieldVisible(field, value);
  }

  private initializeActivityPanel() {
    this.fileUploadPath = `${appConfiguration.baseUploadPath}/refund-management/files`;
    this.initializeActivities();
    this.checkActivityCommentVisibility();
  }

  private checkActivityCommentVisibility() {
    this.isActivityCommentVisible = this.formConfig.accessType !== AccessType.create;
  }

  private getHeaderDate(): string {
    return this.formConfig.isRa ? this.itemRetrieved.dateOfApplication : this.itemRetrieved.dateOfIssue;
  }

  private onModifyClick() {
    const id = this.itemRetrieved.id;
    // TODO Handle RFND Notice edition when is done
    const queryUrl = ROUTES.REFUNDS_APP_EDIT.url;

    const url = `${queryUrl}/${id}`;
    this.tabService.closeCurrentTabAndNavigate(url);
  }

  private getStatCodeName(): string {
    const statCode = this.formConfig.getDefaultStats().find(stat => stat.value === this.itemRetrieved.statisticalCode);

    return statCode ? statCode.label : this.itemRetrieved.statisticalCode;
  }

  private initializeUserDetails() {
    this.agentDetails = getUserDetails(this.itemRetrieved.agentAddress);
    this.airlineDetails = getUserDetails(this.itemRetrieved.airlineAddress);
  }

  private initializeRouteData() {
    const data = this.activatedRoute.snapshot.data;
    this.itemRetrieved = data.item;
    this.formConfig.refundType = data.refundType;
    this.formConfig.accessType = AccessType.read;
  }

  private initializeActivities() {
    this.refundHistoryService
      .getActivities(this.itemRetrieved.id, this.formConfig.refundType)
      .pipe(first())
      .subscribe(activities => {
        this.activities = activities;
        this.cd.markForCheck();
      });
  }

  private emitDataValues(): void {
    this.formData.getSubject(RefundActionEmitterType.agent).next(this.itemRetrieved.agent);
    this.formData.getSubject(RefundActionEmitterType.currency).next(this.itemRetrieved.currency);
    this.formData.getSubject(RefundActionEmitterType.airline).next(this.itemRetrieved.airline);
    this.formData.getSubject(RefundActionEmitterType.bsp).next((this.itemRetrieved.agent as AgentViewModel).bsp);
  }

  private initializeBspName() {
    this.formData
      .getSubject(RefundActionEmitterType.bsp)
      .pipe(first())
      .subscribe(bsp => {
        this.bspName = getBspName(bsp);
      });
  }

  private checkFieldsVisibility() {
    // Basic information
    this.showExceptionDisclaimer = this.itemRetrieved.finalType === RefundFinal.Exception;
    this.showFinalDisclaimer =
      this.itemRetrieved.finalType === RefundFinal.Final &&
      (this.itemRetrieved.status === RefundStatus.pending || this.itemRetrieved.status === RefundStatus.resubmitted);
    this.isTypeRefundApplication = this.itemRetrieved.type === RefundType.RefundApplication;

    // Details section
    this.isRemarksVisible =
      this.itemRetrieved.status === RefundStatus.authorized ||
      this.itemRetrieved.status === RefundStatus.rejected ||
      this.itemRetrieved.status === RefundStatus.investigated;

    // Form of Payment section
    this.areCardsVisible = this.itemRetrieved.cardPayments?.length > 0;
    this.isCashVisible = this.itemRetrieved.cashPaymentAmount != null;
    this.isMiscCashVisible = this.itemRetrieved.miscellaneousCashPaymentAmount != null;
    this.isPaymentsVisible = this.isCashVisible || this.isMiscCashVisible || this.areCardsVisible;

    // Amount and tax section
    this.isTaxesListVisible = this.itemRetrieved.taxMiscellaneousFees?.length > 0;
  }

  private initializeFieldsVisibility() {
    this.initializeVatFieldsVisibility();
    this.initializeContactFieldsVisibility();
    this.initializeFormPaymentsConfFieldsVisibility();
    this.initializeNetReportingVisibility();
    this.initializeAmountTaxFieldsVisibility();
    this.initializeInternalCommentVisibility();
  }

  private initializeVatFieldsVisibility() {
    this.isAgentVatFieldVisible$ = this.formConfig.isAgentVatFieldVisible(this.itemRetrieved.agentVatNumber);
    this.isAirlineVatFieldVisible$ = this.formConfig.isAirlineVatFieldVisible(this.itemRetrieved.airlineVatNumber);
  }

  private initializeContactFieldsVisibility() {
    this.isAgentContactFieldVisible$ = this.formConfig.isContactFieldVisible(this.itemRetrieved.agentContact);
    this.isAirlineContactFieldVisible$ = this.formConfig.isContactFieldVisible(this.itemRetrieved.airlineContact);
  }

  private initializeFormPaymentsConfFieldsVisibility() {
    this.isCustomerRefFieldVisible$ = this.formConfig.isCustomerReferenceVisible(
      this.itemRetrieved.customerFileReference
    );
    this.isETicketCodeFieldVisible$ = this.formConfig.iseTicketCodeVisible(
      this.itemRetrieved.settlementAuthorisationCode
    );
  }

  private initializeNetReportingVisibility() {
    this.isNetReportingVisible$ = this.formConfig.getNRRefundsPermitted();
  }

  private initializeAmountTaxFieldsVisibility() {
    this.initializeDiscountAmountVisibility();

    this.isCPVisible$ = this.isPenaltyAmountVisible(
      'cancellationPenalty',
      this.itemRetrieved.cancellationPenalty.toString()
    );
    this.isTaxOnCPVisible$ = this.isPenaltyAmountVisible(
      'taxOnCancellationPenalty',
      this.itemRetrieved.taxOnCancellationPenalty?.toString()
    );
    this.isMiscFeeVisible$ = this.isPenaltyAmountVisible(
      'miscellaneousFee',
      this.itemRetrieved.miscellaneousFee.toString()
    );
    this.isTaxOnMFVisible$ = this.isPenaltyAmountVisible(
      'taxOnMiscellaneousFee',
      this.itemRetrieved.taxOnMiscellaneousFee?.toString()
    );
    this.isCommissionForCpAndMfVisible$ = this.isPenaltyAmountVisible('commissionForCpAndMf');
  }

  private initializeDiscountAmountVisibility() {
    this.isDiscountAmountVisible$ = of(true);

    if (!+this.itemRetrieved.supplementaryCommission) {
      if (this.formConfig.isRn) {
        this.isDiscountAmountVisible$ = this.formConfig.bspRefundNoticeConfiguration.pipe(
          first(),
          map(config => config.discountAmountEnabled)
        );
      } else {
        this.isDiscountAmountVisible$ = this.formConfig.getFareAdjustmentAmount();
      }
    }
  }

  private initializeInternalCommentVisibility() {
    const hasReadInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundReadInternalCommentPermission(this.formConfig.refundType)
    );

    const hasCreateInternalCommentPermission = this.permissionsService.hasPermission(
      getRefundCreateInternalCommentPermission(this.formConfig.refundType)
    );

    this.isInternalCommentButtonVisible =
      hasReadInternalCommentPermission && (!!this.itemRetrieved.internalComment || hasCreateInternalCommentPermission);
  }
}
