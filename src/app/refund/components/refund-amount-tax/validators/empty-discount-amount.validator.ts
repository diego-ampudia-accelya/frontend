import { AbstractControl } from '@angular/forms';

export function emptyDiscountAmountValidator(
  control: AbstractControl
): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  if (+control.value !== 0) {
    result = {
      emptyDiscount: { key: 'REFUNDS.amountAndTax.errors.emptyDiscountAmount' }
    };
  }

  return result;
}
