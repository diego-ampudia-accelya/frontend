import { AbstractControl, ValidatorFn } from '@angular/forms';

export function commissionAmountValidator(controlsForTotal: AbstractControl[]): ValidatorFn {
  return (amount: AbstractControl): { [key: string]: boolean } => {
    let result = null;
    const commissionTotal = controlsForTotal
      .map(control => control.value)
      .reduce((acum, currentValue) => acum + (parseFloat(currentValue) || 0), 0);

    if (parseFloat(amount.value) > 0 && amount.value >= commissionTotal) {
      result = {
        lowerValue: {
          message: `The value must be lower than the sum of CP and MF` // TODO Translation
        }
      };
    }

    return result;
  };
}

export function commissionRateValidator(commissionControls: AbstractControl[]): ValidatorFn {
  return (amount: AbstractControl): { [key: string]: boolean } => {
    let result = null;

    if (
      parseFloat(amount.value) > 0 &&
      commissionControls.every(control => !control.value || parseFloat(control.value) === 0)
    ) {
      result = {
        requiredFields: {
          message: `At least one commission field (CP or MF) must be filled` // TODO Translation
        }
      };
    }

    return result;
  };
}
