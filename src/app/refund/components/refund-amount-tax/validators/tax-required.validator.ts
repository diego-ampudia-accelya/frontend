import { FormArray, FormGroup, ValidatorFn } from '@angular/forms';

import { TaxMiscellaneousFee } from '~app/refund/models/refund-shared.model';
import { FormUtil } from '~app/shared/helpers';

/**
 * Validates if a tax amount form is valid together with its fields. Condition is:
 * ```
 * a line is partially filled, `ISO Tax Code control` is required.
 ```
 *
 * @returns null since errors are set in child controls
 */
export function taxRequiredValidator(): ValidatorFn {
  return (formArray: FormArray): { [key: string]: boolean } => {
    if (formArray.value) {
      formArray.controls.forEach(lineGroup => {
        const typeControl = FormUtil.get<TaxMiscellaneousFee>(lineGroup as FormGroup, 'type');
        const amountValue = FormUtil.get<TaxMiscellaneousFee>(lineGroup as FormGroup, 'amount').value;

        if (!typeControl.value && amountValue) {
          typeControl.setErrors({ required: true });
          FormUtil.showControlState(typeControl);
        } else if (typeControl.hasError('required')) {
          typeControl.setErrors(null);
        }
      });
    }

    return null;
  };
}
