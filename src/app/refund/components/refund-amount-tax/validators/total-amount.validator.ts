import { FormControl, ValidatorFn } from '@angular/forms';

import { MasterDataType } from '~app/master-shared/models/master.model';
import { UserType } from '~app/shared/models/user.model';

/**
 * Validates if the form of payment total amount matches with the gross fare
 *
 * @returns null if they match or error if they do not
 */
export function totalAmountValidator(
  getPaymentTotalAmount: () => string,
  getRefundType: () => MasterDataType,
  getUserType: () => UserType
): ValidatorFn {
  return (formControl: FormControl) => {
    let error = null;
    const userType = getUserType();

    const isRefundNotice = getRefundType() === MasterDataType.RefundNotice;
    const isAgentOrAgentGroup = userType === UserType.AGENT || userType === UserType.AGENT_GROUP;

    const refundToPassenger = parseFloat(formControl.value || '0');
    const paymentTotalAmount = parseFloat(getPaymentTotalAmount() || '0');

    //* Refund can be zero only when issuing RA and user is an Agent
    if (refundToPassenger === 0 && (isRefundNotice || !isAgentOrAgentGroup)) {
      error = { refundToPassengerNotZero: { key: 'REFUNDS.amountAndTax.errors.refundToPassengerNotZero' } };
    }

    if (refundToPassenger !== paymentTotalAmount && !error) {
      error = { formPaymentRequired: { key: 'REFUNDS.amountAndTax.errors.totalAmountAndPayment' } };
    }

    return error;
  };
}
