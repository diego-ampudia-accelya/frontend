import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormControl } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { RefundAmountTaxComponent } from '../refund-amount-tax/refund-amount-tax.component';
import { RefundActionEmitterType } from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { createRefundConfigService } from '~app/refund/shared/helpers/refund.factory';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { FormUtil } from '~app/shared/helpers';

const mockAgent = {};

const initialState = {
  auth: {
    user: mockAgent
  },
  core: {
    menu: {
      tabs: {}
    }
  }
};

describe('RefundAmountTaxComponent', () => {
  let component: RefundAmountTaxComponent;
  let fixture: ComponentFixture<RefundAmountTaxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundAmountTaxComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule,
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule,
        NoopAnimationsModule
      ],
      providers: [
        provideMockStore({ initialState }),
        HttpTestingController,
        FormBuilder,
        L10nTranslationService,
        HttpClient,
        RefundActionsService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundAmountTaxComponent);
    component = fixture.componentInstance;

    component.formData = new RefundDataService(TestBed.inject(FormBuilder));
    component.formConfig = createRefundConfigService(
      component.formData,
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Store),
      TestBed.inject(RefundActionsService)
    );

    component.formConfig.accessType = AccessType.create;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add tax lines on click', () => {
    expect(component.taxMiscellaneousFees.length).toBe(1);
    component.onAddTaxClick();
    expect(component.taxMiscellaneousFees.length).toBe(2);
  });

  it('should remove tax lines on click', () => {
    component.onAddTaxClick();
    expect(component.taxMiscellaneousFees.length).toBe(2);
    component.onRemoveTaxClick(1);
    expect(component.taxMiscellaneousFees.length).toBe(1);
  });

  it('should reset last tax line when clicking on remove', () => {
    expect(component.taxMiscellaneousFees.length).toBe(1);
    const taxGroup = component.taxMiscellaneousFees.at(0);
    spyOn(taxGroup, 'reset');

    component.onRemoveTaxClick(1);
    expect(taxGroup.reset).toHaveBeenCalled();
  });

  it('should reset field before disabling it', () => {
    const field = new FormControl();
    spyOn(field, 'reset');
    component['toggleFieldEnabled'](field, false);
    expect(field.reset).toHaveBeenCalled();
  });

  // TODO Review why functionality works but not this test
  xit('should emit total amount when it changes', done => {
    const totalAmountSubj = component.formData.getSubject(RefundActionEmitterType.totalAmount);
    spyOn(totalAmountSubj, 'next');
    component['form'].get('totalGrossFare').setValue(2);

    fixture.whenStable().then(() => {
      expect(totalAmountSubj.next).toHaveBeenCalled();
      done();
    });
  });

  describe('setTotals', () => {
    it('should update set value for totalGrossFare', () => {
      component.totalGrossFare.setValue('0');

      FormUtil.get(component.form, 'grossFare').setValue('10.2');
      FormUtil.get(component.form, 'lessGrossFareUsed').enable();
      FormUtil.get(component.form, 'lessGrossFareUsed').setValue('8.1');

      (component as any).setTotals();

      expect(component.totalGrossFare.value).toBe('2.10');
    });

    it('should set new value for totalAmount', () => {
      FormUtil.get(component.form, 'grossFare').setValue('100.2');
      FormUtil.get(component.form, 'lessGrossFareUsed').enable();
      FormUtil.get(component.form, 'lessGrossFareUsed').setValue('8.1');
      (component as any).cancellationPenalty.setValue('7');
      (component as any).miscellaneousFee.setValue('8');
      component.form.get('taxOnCancellationPenalty').setValue('5');
      component.form.get('taxOnMiscellaneousFee').setValue('6');

      (component as any).setTotals();

      // 92.10 + 0 - 5 - 6 - 7 - 8
      expect(component.totalAmount.value).toBe('66.10');
    });
  });
});
