import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { combineLatest, Observable, of, Subject, Subscription } from 'rxjs';
import { filter, first, switchMap, takeUntil, tap } from 'rxjs/operators';

import { commissionAmountValidator, commissionRateValidator } from './validators/commission-cp-mf.validator';
import { emptyDiscountAmountValidator } from './validators/empty-discount-amount.validator';
import { taxIsoCodePatternValidator } from './validators/tax-iso-code-pattern.validator';
import { taxRequiredValidator } from './validators/tax-required.validator';
import { totalAmountValidator } from './validators/total-amount.validator';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { Commission, TaxMiscellaneousFee } from '~app/refund/models/refund-shared.model';
import { AmountTaxViewModel, CurrencyViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionEmitterType } from '~app/refund/models/refund.model';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundTicketService } from '~app/refund/services/refund-ticket.service';
import { ButtonDesign } from '~app/shared/components';
import { InputSelectOption } from '~app/shared/components/input-select/models/input-select.model';
import { AlertMessageType } from '~app/shared/enums';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { FormList } from '~app/shared/helpers/form-helpers/list/form-list.helper';
import { UserType } from '~app/shared/models/user.model';
import { lowerValidator } from '~app/shared/validators/lower.validator';
import { maxLengthRoundedValidator } from '~app/shared/validators/max-length-rounded.validator';
import { positiveNumberValidator } from '~app/shared/validators/positive-number.validator';

@Component({
  selector: 'bspl-refund-amount-tax',
  templateUrl: './refund-amount-tax.component.html',
  styleUrls: ['./refund-amount-tax.component.scss']
})
export class RefundAmountTaxComponent implements OnInit, OnChanges, OnDestroy {
  @Input() formData: RefundDataService;
  @Input() formConfig: RefundConfigService;
  @Input() refundTicket: RefundTicketService;

  @Output() showTaxRepeatedWarning = new EventEmitter<boolean>();

  @Input() paymentTotalAmount;

  public form: FormGroup;

  public formFactory: FormUtil;

  public btnDesign = ButtonDesign;

  public taxMiscellaneousFees: FormArray;
  public totalGrossFare: FormControl;
  public totalAmount: FormControl;
  public amountPlaceholder = '';

  public commissionOptions: InputSelectOption[];
  public commissionForCpAndMfOptions: InputSelectOption[];

  public isNetReportingVisible$: Observable<boolean>;

  public partialRefundEnabled = true;
  public discountAmountEnabled = true;

  public showTaxRepeatedMsg = false;
  public taxesRepeatedMessageType = AlertMessageType.warning;

  public isCommissionForCpAndMfVisible$: Observable<boolean>;
  public amountFieldsVisibility: { [key in keyof AmountTaxViewModel]?: Observable<boolean> } = {};
  public refundAppType: boolean;

  private destroy$ = new Subject();
  private amountFieldKeys: Array<keyof AmountTaxViewModel>;
  private currency: CurrencyViewModel;

  private partialRefund: FormControl;
  private netReporting: FormControl;
  private commission: FormGroup;
  private commissionForCpAndMf: FormGroup;
  private amountCommissionForCpAndMf: AbstractControl;
  private rateCommissionForCpAndMf: AbstractControl;
  private cancellationPenalty: FormControl;
  private miscellaneousFee: FormControl;

  private userType: UserType;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translationService: L10nTranslationService
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit(): void {
    this.buildForm();
    this.setConfig();

    this.store
      .pipe(select(getUserType), first(), takeUntil(this.destroy$))
      .subscribe(userType => (this.userType = userType));

    switch (this.formConfig.accessType) {
      case AccessType.editTicket:
      case AccessType.edit:
      case AccessType.create:
        this.initializeListeners();
        this.fireInitializers();
        this.initializeInputSelectOptions();
        this.initializeFieldsVisibility();

        if (this.formConfig.isEditTicketMode) {
          this.initializeEditTicketListeners();
        }
        if (this.formConfig.isEditMode || this.formConfig.isResubmission) {
          this.initializeEditListeners();
        } else if (this.formConfig.isCreateMode) {
          this.initializeCreateListeners();
        }

        break;
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.paymentTotalAmount && !changes.paymentTotalAmount.firstChange) {
      this.totalAmount.updateValueAndValidity();
    }
  }

  public onAddTaxClick(): void {
    this.taxMiscellaneousFees.push(this.getTaxMiscellaneousFeesLine());
  }

  public onRemoveTaxClick(index: number): void {
    if (this.taxMiscellaneousFees.length > 1) {
      this.taxMiscellaneousFees.removeAt(index);
    } else {
      this.taxMiscellaneousFees.reset({});
    }
  }

  public disableAddBtn(): boolean {
    return FormList.isAddLineDisabled(this.taxMiscellaneousFees);
  }

  public isPenaltyAmountVisible(field: keyof AmountTaxViewModel): Observable<boolean> {
    return this.formData
      .whenFormIsReady()
      .pipe(switchMap(() => this.formConfig.isPenaltyAmountFieldVisible(field, this.form.get(field).value)));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private setConfig(): void {
    this.amountFieldKeys = this.formConfig.getAmountFieldKeys();
    this.refundAppType = this.formConfig.isRa;
  }

  private buildForm(): void {
    this.partialRefund = new FormControl(false);
    this.netReporting = new FormControl(false);

    this.commission = this.getCommissionGroup();
    this.commissionForCpAndMf = this.getCommissionGroup();

    this.taxMiscellaneousFees = new FormArray([this.getTaxMiscellaneousFeesLine()], taxRequiredValidator());
    this.totalGrossFare = new FormControl(0);
    this.totalAmount = new FormControl(0, [
      totalAmountValidator(
        () => this.paymentTotalAmount,
        () => this.formConfig.refundType,
        () => this.userType
      )
    ]);

    this.cancellationPenalty = new FormControl(null, this.getMonetaryAmountValidators());
    this.miscellaneousFee = new FormControl(null, this.getMonetaryAmountValidators());

    this.amountCommissionForCpAndMf = FormUtil.get<Commission>(this.commissionForCpAndMf, 'amount');
    this.rateCommissionForCpAndMf = FormUtil.get<Commission>(this.commissionForCpAndMf, 'rate');

    this.amountCommissionForCpAndMf.setValidators(
      Validators.compose([
        this.amountCommissionForCpAndMf.validator,
        commissionAmountValidator([this.cancellationPenalty, this.miscellaneousFee])
      ])
    );

    this.rateCommissionForCpAndMf.setValidators(
      Validators.compose([
        this.rateCommissionForCpAndMf.validator,
        commissionRateValidator([this.cancellationPenalty, this.miscellaneousFee])
      ])
    );

    this.form = this.formFactory.createGroup<AmountTaxViewModel>({
      partialRefund: this.partialRefund,
      netReporting: this.netReporting,
      grossFare: [null, this.getMonetaryAmountValidators()],
      lessGrossFareUsed: [null, this.getMonetaryAmountValidators()],
      totalGrossFare: this.totalGrossFare,
      commission: this.commission,
      commissionForCpAndMf: this.commissionForCpAndMf,
      supplementaryCommission: [null, this.getMonetaryAmountValidators()],
      cancellationPenalty: this.cancellationPenalty,
      taxOnCancellationPenalty: [null, lowerValidator(this.cancellationPenalty)],
      miscellaneousFee: this.miscellaneousFee,
      taxOnMiscellaneousFee: [null, lowerValidator(this.miscellaneousFee)],
      taxMiscellaneousFees: this.taxMiscellaneousFees,
      totalAmount: this.totalAmount
    });

    this.formData.setSectionFormGroup('amounts', this.form);
  }

  private initializeInputSelectOptions(): void {
    this.commissionOptions = this.generateCommissionOptions(this.commission);
    this.commissionForCpAndMfOptions = this.generateCommissionOptions(this.commissionForCpAndMf);
  }

  private initializeFieldsVisibility(): void {
    this.isNetReportingVisible$ = this.formData
      .whenFormIsReady()
      .pipe(switchMap(() => (this.netReporting.value ? of(true) : this.formConfig.getNRRefundsPermitted())));

    this.isCommissionForCpAndMfVisible$ = this.formConfig.isPenaltyAmountFieldVisible('commissionForCpAndMf');
    this.amountFieldKeys.forEach(
      key => (this.amountFieldsVisibility = { ...this.amountFieldsVisibility, [key]: this.isPenaltyAmountVisible(key) })
    );
  }

  private initializeListeners(): void {
    if (this.formConfig.isRn) {
      this.initializeBspRNConfigListener();
    }

    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => this.updateForm());

    this.formData
      .getSubject(RefundActionEmitterType.currency)
      .pipe(takeUntil(this.destroy$))
      .subscribe(currency => {
        this.currency = currency;
        this.form.updateValueAndValidity({ onlySelf: true });
        this.initializeInputSelectOptions();
      });

    this.cancellationPenalty.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      FormUtil.get<AmountTaxViewModel>(this.form, 'taxOnCancellationPenalty').updateValueAndValidity({
        emitEvent: false
      });
      this.amountCommissionForCpAndMf.updateValueAndValidity({
        onlySelf: true
      });
      this.rateCommissionForCpAndMf.updateValueAndValidity({
        onlySelf: true
      });
    });

    this.miscellaneousFee.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      FormUtil.get<AmountTaxViewModel>(this.form, 'taxOnMiscellaneousFee').updateValueAndValidity({
        emitEvent: false
      });
      this.amountCommissionForCpAndMf.updateValueAndValidity({
        onlySelf: true
      });
      this.rateCommissionForCpAndMf.updateValueAndValidity({
        onlySelf: true
      });
    });

    this.partialRefund.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      this.toggleFieldEnabled(FormUtil.get<AmountTaxViewModel>(this.form, 'lessGrossFareUsed'), value);
      this.formData.getSubject(RefundActionEmitterType.partialRefund).next(value);
    });

    this.netReporting.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (this.discountAmountEnabled) {
        this.toggleFieldEnabled(FormUtil.get<AmountTaxViewModel>(this.form, 'supplementaryCommission'), value);
      }
    });

    this.taxMiscellaneousFees.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value: Array<TaxMiscellaneousFee>) => {
        this.showTaxRepeatedMsg =
          value.length > 1 &&
          value.some((tax: TaxMiscellaneousFee, i: number, taxList: TaxMiscellaneousFee[]) =>
            taxList.some(
              (t: TaxMiscellaneousFee, j: number) =>
                i !== j && t.type && tax.type && t.type.toUpperCase() === tax.type.toUpperCase()
            )
          );
        this.showTaxRepeatedWarning.emit(this.showTaxRepeatedMsg);
      });
  }

  private initializeBspRNConfigListener(): Subscription {
    return this.formData
      .whenFormIsReady()
      .pipe(
        switchMap(() => this.formConfig.bspRefundNoticeConfiguration),
        takeUntil(this.destroy$)
      )
      .subscribe(config => {
        const supplementaryCommissionControl = FormUtil.get<AmountTaxViewModel>(this.form, 'supplementaryCommission');

        this.discountAmountEnabled = config.discountAmountEnabled;

        if (!this.discountAmountEnabled) {
          supplementaryCommissionControl.reset(null, { emitEvent: false });
        } else {
          this.netReporting.updateValueAndValidity({ onlySelf: true });
        }

        this.partialRefundEnabled = config.partialRefundEnabled;

        if (!this.partialRefundEnabled) {
          this.partialRefund.setValue(false);

          const partialControls = [
            FormUtil.get<AmountTaxViewModel>(this.form, 'lessGrossFareUsed'),
            this.totalGrossFare
          ];

          partialControls.forEach(control => {
            control.disable();
            control.reset(null, { emitEvent: false });
          });
        }
      });
  }

  private fireInitializers(): void {
    this.partialRefund.updateValueAndValidity({ onlySelf: true });
    this.netReporting.updateValueAndValidity({ onlySelf: true }); // TODO check if needed
  }

  private initializeEditTicketListeners(): void {
    this.formData
      .whenFormIsReady()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.netReporting.patchValue(this.refundTicket.netReportingTicket);
        FormUtil.get<AmountTaxViewModel>(this.form, 'grossFare').patchValue(this.refundTicket.grossFareTicket);
        FormUtil.get<AmountTaxViewModel>(this.form, 'commission')
          .get('amount')
          .patchValue(this.refundTicket.commissionAmountTicket);
        FormUtil.get<AmountTaxViewModel>(this.form, 'supplementaryCommission').patchValue(
          this.refundTicket.discAmountTicket
        );
      });
  }

  private initializeEditListeners(): void {
    this.formData
      .whenFormIsReady()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.propagateTotalAmount();
        // To fire partial refund and NR initializers when BE is read
        this.fireInitializers();
      });

    if (this.formConfig.isRa) {
      this.formData
        .whenFormIsReady()
        .pipe(
          switchMap(() => this.formConfig.getFareAdjustmentAmount()),
          filter(faAmount => !faAmount),
          takeUntil(this.destroy$)
        )
        .subscribe(() => {
          const supplCommissionControl = FormUtil.get<AmountTaxViewModel>(this.form, 'supplementaryCommission');
          supplCommissionControl.setValidators([supplCommissionControl.validator, emptyDiscountAmountValidator]);

          if (!+supplCommissionControl.value) {
            this.discountAmountEnabled = false;
            this.toggleFieldEnabled(FormUtil.get<AmountTaxViewModel>(this.form, 'supplementaryCommission'), false);
          }
        });
    }
  }

  private initializeCreateListeners(): void {
    if (this.formConfig.isRa) {
      combineLatest([
        this.formConfig.getFareAdjustmentAmount(),
        this.formConfig.getMfFeeAmount(),
        this.formConfig.isPenaltyAmountFieldVisible('miscellaneousFee')
      ])
        .pipe(
          tap(result => this.updateInitialMfFeeAmount(result[1], result[2])),
          filter(result => !result[0]),
          takeUntil(this.destroy$)
        )
        .subscribe(() => {
          this.discountAmountEnabled = false;
          this.toggleFieldEnabled(FormUtil.get<AmountTaxViewModel>(this.form, 'supplementaryCommission'), false);
        });
    }
  }

  private updateInitialMfFeeAmount(mfFeeAmount: number, isVisible: boolean) {
    const valueToPatch = isVisible ? mfFeeAmount : null;
    this.miscellaneousFee.patchValue(valueToPatch);
  }

  private toggleFieldEnabled(field: AbstractControl, isEnabled: boolean): void {
    if (isEnabled) {
      field.enable({ emitEvent: false });
    } else {
      field.reset(null, { emitEvent: false });
      field.disable();
    }
  }

  private updateForm(): void {
    this.updateControls();
    this.setTotals();
  }

  /**
   * Updates controls
   *
   * Sets amount differences in controls and also correct decimals in inputs
   */
  private updateControls(): void {
    this.amountPlaceholder = Number(0).toFixed(this.decimals);

    this.amountFieldKeys.forEach(key => {
      this.updateNumberControl(this.form.get(key));
    });

    this.taxMiscellaneousFees.controls.forEach(group => {
      this.updateNumberControl(FormUtil.get<TaxMiscellaneousFee>(group as FormGroup, 'amount'));
    });

    this.updateNumberControl(FormUtil.get<Commission>(this.commission, 'amount'));
    this.updateNumberControl(FormUtil.get<Commission>(this.commissionForCpAndMf, 'amount'));

    this.updateNumberControl(FormUtil.get<Commission>(this.commission, 'rate'), 2);
    this.updateNumberControl(FormUtil.get<Commission>(this.commissionForCpAndMf, 'rate'), 2);
  }

  private updateNumberControl(control: AbstractControl, roundingDecimals?: number): void {
    FormUtil.regularizeCalculationControls(control);

    if (control.valid && control.value) {
      control.setValue(this.round(control.value, roundingDecimals), { emitEvent: false });
    }
  }

  private setTotals(): void {
    const totalGrossFareValue: string = this.getTotalGrossFareValue();
    this.totalGrossFare.setValue(totalGrossFareValue, { emitEvent: false });

    const totalRefund = this.getTotalRefund(totalGrossFareValue);
    const newTotalAmount = this.round(totalRefund);

    if (newTotalAmount !== this.totalAmount.value) {
      this.totalAmount.setValue(newTotalAmount, { emitEvent: false });
      this.formData.getSubject(RefundActionEmitterType.totalAmount).next(this.totalAmount.value);
    }

    // TODO: in which case will this block be executed?
    if (newTotalAmount !== this.round(this.totalAmount.value)) {
      this.refreshTotalAmountValidity();
    }
  }

  private getTotalGrossFareValue(): string {
    const grossFareControl = FormUtil.get<AmountTaxViewModel>(this.form, 'grossFare');

    const lessGrossFareControl = FormUtil.get<AmountTaxViewModel>(this.form, 'lessGrossFareUsed');
    const lessGrossFareValue =
      lessGrossFareControl.valid && lessGrossFareControl.enabled ? lessGrossFareControl.value : 0;

    return grossFareControl.valid ? this.round(grossFareControl.value - lessGrossFareValue) : this.round(0);
  }

  private getTotalRefund(totalGrossFareValue: string): number {
    const taxesSum = this.getTaxesSum();

    const zeroIfInvalid = control => (control.valid && control.value ? control.value : 0);

    const cpValue = zeroIfInvalid(this.cancellationPenalty);
    const mfValue = zeroIfInvalid(this.miscellaneousFee);

    const taxOnCpValue = zeroIfInvalid(FormUtil.get<AmountTaxViewModel>(this.form, 'taxOnCancellationPenalty'));
    const taxOnMfValue = zeroIfInvalid(FormUtil.get<AmountTaxViewModel>(this.form, 'taxOnMiscellaneousFee'));

    return parseFloat(totalGrossFareValue) + taxesSum - cpValue - mfValue - taxOnCpValue - taxOnMfValue;
  }

  private getTaxesSum(): number {
    let result = 0;

    this.taxMiscellaneousFees.controls.forEach(taxLine => {
      if (taxLine.valid) {
        result += parseFloat(FormUtil.get<TaxMiscellaneousFee>(taxLine as FormGroup, 'amount').value || 0);
      }
    });

    return result;
  }

  private round(value, decimals = this.decimals): string {
    return NumberHelper.roundString(value, decimals);
  }

  private refreshTotalAmountValidity(): void {
    this.totalAmount.updateValueAndValidity();
    FormUtil.showControlState(this.totalAmount);
  }

  private propagateTotalAmount(): void {
    this.formData.getSubject(RefundActionEmitterType.totalAmount).next(this.totalAmount.value);
  }

  private getTaxMiscellaneousFeesLine(): FormGroup {
    return this.formFactory.createGroup<TaxMiscellaneousFee>({
      amount: [null, this.getMonetaryAmountValidators()],
      type: [null, [taxIsoCodePatternValidator]]
    });
  }

  private getCommissionGroup(): FormGroup {
    return this.formFactory.createGroup<Commission>({
      amount: [null, this.getMonetaryAmountValidators()],
      rate: [null, Validators.pattern(this.formConfig.getRatesPattern())]
    });
  }

  private generateCommissionOptions(commissionGroup: FormGroup): InputSelectOption[] {
    const amountLabel = this.translationService.translate('REFUNDS.amountAndTax.dropdownOptionLabel.amount');

    const amountInput = FormUtil.get<Commission>(commissionGroup, 'amount') as FormControl;
    const rateInput = FormUtil.get<Commission>(commissionGroup, 'rate') as FormControl;

    const amountOption = {
      dropdownOption: {
        label: this.currency ? `${amountLabel}, ${this.currency.code}` : amountLabel,
        labelSelected: this.currency ? this.currency.code : this.translationService.translate('common.quantity'),
        value: 'amount'
      },
      placeholder: this.amountPlaceholder,
      input: amountInput
    };

    const rateOption = {
      dropdownOption: {
        label: this.translationService.translate('REFUNDS.amountAndTax.dropdownOptionLabel.rate'),
        labelSelected: '%',
        value: 'rate'
      },
      placeholder: '0.00',
      input: rateInput
    };

    return [amountOption, rateOption];
  }

  private getMonetaryAmountValidators(): ValidatorFn[] {
    return [
      positiveNumberValidator,
      Validators.pattern(this.formConfig.getDecimalsPattern()),
      maxLengthRoundedValidator(this.decimals, NumberHelper.getMonetaryAmountMaxLength(this.decimals))
    ];
  }

  private get decimals(): number {
    return this.currency ? this.currency.decimals : 2;
  }

  public get currencyCodeLabel(): string {
    return this.currency ? `, ${this.currency.code.toUpperCase()}` : '';
  }
}
