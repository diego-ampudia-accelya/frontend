import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundBasicInfoComponent } from './refund-basic-info.component';
import { ContactViewModel } from '~app/adm-acm/models/adm-acm-issue-view-aux.model';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { BasicAgentInfo, BasicAirlineInfo } from '~app/refund/models/refund-be-aux.model';
import { AgentViewModel, AirlineViewModel, BasicInformationViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionEmitterType } from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundConfigService } from '~app/refund/shared/helpers/refund.factory';
import { getAgentContactData, getAirlineContactData } from '~app/refund/store/reducers';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { FormUtil } from '~app/shared/helpers';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { AgentSummary, AirlineSummary, DropdownOption } from '~app/shared/models';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';

const refundServiceSpy = createSpyObject(RefundService);
const airlDictionaryServiceSpy = createSpyObject(AirlineDictionaryService);
const agentDictionaryServiceSpy = createSpyObject(AgentDictionaryService);

const contact: ContactViewModel = { contactName: 'name', email: 'email@email.com', phoneFaxNumber: '1231231' };

const mockAirline: Partial<BasicAirlineInfo> = {
  airlineVatNumber: 'test',
  airline: {
    id: 1,
    localName: 'Iberia',
    iataCode: '1234',
    active: true,
    address: null,
    bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain' },
    contact: null,
    designator: null,
    globalName: '',
    vatNumber: ''
  },
  airlineContact: { ...contact },
  airlineAddress: {
    city: 'Seville',
    country: 'Spain',
    telephone: '666666666'
  }
};

const mockAgent: Partial<BasicAgentInfo> = {
  agent: {
    id: 1,
    name: 'Peter',
    vatNumber: '1234',
    bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' },
    effectiveFrom: '2020-01-01',
    iataCode: '1234567'
  },
  agentAddress: {},
  agentContact: { ...contact }
};

const activatedRouteStub = { snapshot: { data: { refundType: MasterDataType.RefundApp } } };

const initialState = {
  auth: { user: createAirlineUser() },
  core: {
    menu: {
      tabs: {}
    }
  },
  refund: {}
};

const mockAgentDropdownOptions: DropdownOption<AgentSummary>[] = [
  {
    value: { id: '1', name: 'AGENT 1111111', code: '1111111' },
    label: '1111111 / AGENT 1111111'
  },
  {
    value: { id: '2', name: 'AGENT 2222222', code: '2222222' },
    label: '2222222 / AGENT 2222222'
  }
];

const mockAirlineDropdownOptions: DropdownOption<AirlineSummary>[] = [
  {
    value: { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
    label: '001 / AIRLINE 001'
  },
  {
    value: { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' },
    label: '002 / AIRLINE 002'
  }
];

// TODO. Random Error in any test: You provided 'null' where a stream was expected. You can provide an Observable, Promise, Array, or Iterable
xdescribe('RefundBasicInfoComponent', () => {
  let component: RefundBasicInfoComponent;
  let fixture: ComponentFixture<RefundBasicInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundBasicInfoComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule,
        NoopAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: getUser,
              value: initialState.auth.user
            },
            { selector: getAgentContactData, value: contact },
            { selector: getAirlineContactData, value: contact }
          ]
        }),
        { provide: AirlineDictionaryService, useValue: airlDictionaryServiceSpy },
        { provide: AgentDictionaryService, useValue: agentDictionaryServiceSpy },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        HttpTestingController,
        FormBuilder,
        L10nTranslationService,
        HttpClient,
        RefundActionsService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundBasicInfoComponent);
    component = fixture.componentInstance;
    component.formData = new RefundDataService(TestBed.inject(FormBuilder));
    component.refundService = refundServiceSpy;
    component.formConfig = createRefundConfigService(
      component.formData,
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Store),
      TestBed.inject(RefundActionsService)
    );

    airlDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAirlineDropdownOptions));
    agentDictionaryServiceSpy.getDropdownOptions.and.returnValue(of(mockAgentDropdownOptions));
    component.formConfig.accessType = AccessType.create;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize logged user', () => {
    component.ngOnInit();
    expect(component['loggedUser']).toEqual(initialState.auth.user);
    expect(component.isAgentUser).toBeFalsy();
    expect(component.isAgentGroupUser).toBeFalsy();
    expect(component.isAirlineUser).toBeTruthy();
  });

  it('should setConfig when is RA', () => {
    component.ngOnInit();
    component.formConfig.accessType = AccessType.create;
    component.formConfig.refundType = MasterDataType.RefundApp;
    expect(component.issueTicketMode).toBeFalsy();
    expect(component.showBspList).toBeFalsy();
  });

  it('should setConfig when is RN and EditTicketMode', () => {
    component.formConfig.accessType = AccessType.editTicket;
    component.formConfig.refundType = MasterDataType.RefundNotice;
    component.ngOnInit();
    expect(component.issueTicketMode).toBeTruthy();
    expect(component.showBspList).toBeTruthy();
  });

  it('should populateAgentContactDataStored', fakeAsync(() => {
    spyOn(component.agentContactForm, 'patchValue').and.callFake(() => {});

    component['populateAgentContactDataStored']();
    tick();

    expect(component.agentContactForm.patchValue).toHaveBeenCalledWith(contact, { emitEvent: false });
  }));

  it('should populateAirlineContactDataStored', fakeAsync(() => {
    spyOn(component.airlineContactForm, 'patchValue').and.callThrough();

    component['populateAirlineContactDataStored']();
    tick();
    expect(component.airlineContactForm.patchValue).toHaveBeenCalledWith(contact, { emitEvent: false });
  }));

  it('should buildForm', () => {
    const formExpected = {
      airline: jasmine.anything(),
      agent: jasmine.anything(),
      airlineContact: jasmine.anything(),
      agentContact: jasmine.anything(),
      bspName: null
    };
    component['buildForm']();

    expect(formExpected).toEqual(component.form.value);
  });

  it('should toggleAgentDetails', () => {
    component.showAgentDetails = false;
    component.toggleAgentDetails();

    expect(component.showAgentDetails).toBeTruthy();
  });

  it('should toggleAirlineDetails', () => {
    component.showAirlineDetails = false;
    component.toggleAirlineDetails();

    expect(component.showAirlineDetails).toBeTruthy();
  });

  it('should showAirlineControlError', () => {
    spyOn(FormUtil, 'showControlState').and.callThrough();
    component.showAirlineControlError();

    expect(FormUtil.showControlState).toHaveBeenCalled();
  });

  it('should showAgentControlError', () => {
    spyOn(FormUtil, 'showControlState').and.callThrough();

    component.showAgentControlError();

    expect(FormUtil.showControlState).toHaveBeenCalled();
  });

  it('should initializeDropdowns as Airline User', () => {
    spyOn<any>(component, 'initializeDropdownsAsAirline').and.callThrough();

    component.ngOnInit();

    expect(component['initializeDropdownsAsAirline']).toHaveBeenCalled();
  });

  it('should initializeDropdowns as Agent User', () => {
    spyOn<any>(component, 'initializeDropdownsAsAgent').and.callThrough();
    refundServiceSpy.getAgentInfo.and.returnValue(of(mockAgent));
    const userAgent: User = {
      ...initialState.auth.user,
      agent: {
        id: 1,
        name: 'test',
        iataCode: '007',
        effectiveFrom: '2020-01-01',
        bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
      },
      userType: UserType.AGENT
    };
    component.loggedUser = userAgent;

    component['initializeDropdowns']();

    expect(component['initializeDropdownsAsAgent']).toHaveBeenCalled();
  });

  it('should initializeDropdowns as AgentGroup User', () => {
    spyOn<any>(component, 'initializeDropdownsAsAgentGroup');
    refundServiceSpy.getAgentInfo.and.returnValue(of(mockAgent));
    const userGroupAgent: User = {
      ...initialState.auth.user,
      userType: UserType.AGENT_GROUP
    };
    component.loggedUser = userGroupAgent;

    component['initializeDropdowns']();

    expect(component['initializeDropdownsAsAgentGroup']).toHaveBeenCalled();
  });

  it('should initializeDropdownsAsAirline ', () => {
    spyOn(component.formData, 'getSubject').and.callThrough();

    component.formConfig.isSupervise = true;
    component.formData.isFormReady.next(true);

    component['initializeDropdownsAsAirline']();

    expect(component.formData.getSubject).toHaveBeenCalledWith(RefundActionEmitterType.agent);
    expect(component.formData.getSubject).toHaveBeenCalledWith(RefundActionEmitterType.airline);
  });

  it('should initializeAgentDropdownAsAgent ', () => {
    spyOn(component.formData, 'getSubject').and.callThrough();
    spyOn<any>(component, 'onAgentChange').and.callThrough();
    spyOn<any>(component, 'onBspChange').and.callThrough();
    refundServiceSpy.getAgentInfo.and.returnValue(of(mockAgent));

    const userAgent: User = {
      ...initialState.auth.user,
      agent: {
        id: 1,
        name: 'test',
        iataCode: '007',
        effectiveFrom: '2020-01-01',
        bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' }
      },
      userType: UserType.AGENT
    };
    component.loggedUser = userAgent;

    component.formConfig.isSupervise = true;
    component.formData.isFormReady.next(true);

    component['initializeAgentDropdownAsAgent']();

    expect(component['onAgentChange']).toHaveBeenCalled();
    expect(component['onBspChange']).toHaveBeenCalled();
  });

  it('should initializeAgentDropdownAsAgent on edit mode ', () => {
    spyOn(component.formData, 'getSubject').and.callThrough();
    spyOn<any>(component, 'onBspChange').and.callThrough();

    component.formConfig.accessType = AccessType.edit;
    component.issueTicketMode = false;
    component.formConfig.isSupervise = true;
    component.formData.isFormReady.next(true);

    component['initializeAgentDropdownAsAgent']();

    expect(component.formData.getSubject).toHaveBeenCalledWith(RefundActionEmitterType.agent);
    expect(component['onBspChange']).toHaveBeenCalled();
  });

  it('should updateAirlineSelected', () => {
    spyOn<any>(component, 'onAirlineChange').and.callThrough();
    FormUtil.get<AirlineViewModel>(component.airlineForm, 'id').setValue('99');
    component.formData.isFormReady.next(true);

    component['updateAirlineSelected']();

    expect(component['onAirlineChange']).toHaveBeenCalledWith('99');
  });

  it('should updateAgentSelected', () => {
    spyOn<any>(component, 'onAgentChange').and.callThrough();
    FormUtil.get<AgentViewModel>(component.agentForm, 'id').setValue('99');
    component.formData.isFormReady.next(true);

    component['updateAgentSelected']();

    expect(component['onAgentChange']).toHaveBeenCalledWith('99');
  });

  it('should initializeBspChangesListener', fakeAsync(() => {
    const bsp = { id: 1, isoCountryCode: 'ES', name: 'Spain', effectiveFrom: '2020-01-01' };
    spyOn<any>(component, 'onBspChange').and.callThrough();
    spyOn<any>(component, 'getBspAirlineDropdown').and.callThrough();
    spyOn<any>(component, 'getBspAgentDropdown').and.callThrough();
    component.isAirlineDropdownVisible = false;
    component.isAgentDropdownVisible = true;

    component['initializeBspChangesListener']();
    FormUtil.get<BasicInformationViewModel>(component.form, 'bspName').patchValue(bsp);
    tick();

    expect(component['onBspChange']).toHaveBeenCalled();
    expect(component['getBspAgentDropdown']).toHaveBeenCalled();
  }));

  it('should onAirlineChange emit airline when changed select airline, reset airlineForm and populateContact', fakeAsync(() => {
    refundServiceSpy.getAirlineInfo.and.returnValue(of(mockAirline));
    spyOn(component.formConfig, 'isAirlineVatFieldVisible').and.returnValue(of(true));

    const airlineSubject = component.formData.getSubject(RefundActionEmitterType.airline);
    spyOn(airlineSubject, 'next').and.callThrough();

    spyOn(component.airlineForm, 'reset').and.callThrough();
    spyOn<any>(component, 'populateAirlineContactDataStored').and.callThrough();

    component.formConfig.refundType = MasterDataType.RefundNotice;
    component.formConfig.accessType = AccessType.edit;

    component['onAirlineChange'](mockAirline.airline.id);
    tick();

    expect(airlineSubject.next).toHaveBeenCalledWith(mockAirline.airline);
    expect(component.airlineForm.reset).toHaveBeenCalledWith(mockAirline.airline, { emitEvent: false });
    expect(component['populateAirlineContactDataStored']).toHaveBeenCalled();
  }));

  it('should onAirlineChange emit airline when changed select airline, reset airlineForm and patch AirlineContactForm', fakeAsync(() => {
    refundServiceSpy.getAirlineInfo.and.returnValue(of(mockAirline));
    spyOn(component.formConfig, 'isAirlineVatFieldVisible').and.returnValue(of(true));

    const airlineSubject = component.formData.getSubject(RefundActionEmitterType.airline);
    spyOn(airlineSubject, 'next').and.callThrough();

    spyOn(component.airlineForm, 'reset').and.callThrough();
    spyOn(component.airlineContactForm, 'patchValue').and.callThrough();
    spyOn<any>(component, 'populateAirlineContactDataStored').and.callThrough();
    component.isAirlineUser = false;
    component.formConfig.refundType = MasterDataType.RefundNotice;
    component.formConfig.accessType = AccessType.edit;

    component['onAirlineChange'](mockAirline.airline.id);
    tick();

    expect(airlineSubject.next).toHaveBeenCalledWith(mockAirline.airline);
    expect(component.airlineForm.reset).toHaveBeenCalledWith(mockAirline.airline, { emitEvent: false });
    expect(component.airlineContactForm.patchValue).toHaveBeenCalledWith(mockAirline.airlineContact, {
      emitEvent: false
    });
    expect(component['populateAirlineContactDataStored']).not.toHaveBeenCalled();
  }));

  it('should onAirlineChange emit airline when changed select airline, reset airlineForm and add rfndVatNumber', fakeAsync(() => {
    refundServiceSpy.getAirlineInfo.and.returnValue(of(mockAirline));
    spyOn(component.formConfig, 'isAirlineVatFieldVisible').and.returnValue(of(true));

    const airlineSubject = component.formData.getSubject(RefundActionEmitterType.airline);
    spyOn(airlineSubject, 'next').and.callThrough();

    spyOn(component.airlineForm, 'reset').and.callThrough();
    spyOn(component.airlineContactForm, 'patchValue').and.callThrough();
    spyOn<any>(component, 'populateAirlineContactDataStored').and.callThrough();
    component.isAirlineUser = false;
    component.formConfig.refundType = MasterDataType.RefundApp;
    component.formConfig.accessType = AccessType.create;

    component['onAirlineChange'](mockAirline.airline.id);
    tick();

    expect(airlineSubject.next).toHaveBeenCalledWith(mockAirline.airline);
    const rfndVatNumber = mockAirline.airlineVatNumber;
    expect(component.airlineForm.reset).toHaveBeenCalledWith(
      { ...mockAirline.airline, rfndVatNumber },
      { emitEvent: false }
    );
    expect(component.airlineContactForm.patchValue).toHaveBeenCalledWith(mockAirline.airlineContact, {
      emitEvent: false
    });
    expect(component['populateAirlineContactDataStored']).not.toHaveBeenCalled();
  }));

  it('should onAgentChange emit agent when changed and is not AgentUser', fakeAsync(() => {
    refundServiceSpy.getAgentInfo.and.returnValue(of(mockAgent));
    const agentSubject = component.formData.getSubject(RefundActionEmitterType.agent);
    spyOn(agentSubject, 'next').and.callThrough();

    spyOn(component.agentForm, 'reset').and.callThrough();
    spyOn(component.agentContactForm, 'patchValue').and.callThrough();

    const rfndVatNumber = mockAgent.agentVatNumber;

    component['onAgentChange'](mockAgent.agent.id);
    tick();

    expect(agentSubject.next).toHaveBeenCalledWith(mockAgent.agent);
    expect(component.agentForm.reset).toHaveBeenCalledWith({ ...mockAgent.agent, rfndVatNumber }, { emitEvent: false });
    expect(component.agentContactForm.patchValue).toHaveBeenCalledWith(mockAgent.agentContact, {
      emitEvent: false
    });
  }));

  it('should onAgentChange emit agent when changed and is AgentUser', fakeAsync(() => {
    refundServiceSpy.getAgentInfo.and.returnValue(of(mockAgent));
    const agentSubject = component.formData.getSubject(RefundActionEmitterType.agent);
    spyOn(agentSubject, 'next').and.callThrough();

    spyOn(component.agentForm, 'reset').and.callThrough();
    spyOn(component.agentContactForm, 'patchValue').and.callThrough();
    spyOn<any>(component, 'populateAgentContactDataStored').and.callThrough();

    component.isAgentUser = true;

    const rfndVatNumber = mockAgent.agentVatNumber;

    component['onAgentChange'](mockAgent.agent.id);
    tick();

    expect(agentSubject.next).toHaveBeenCalledWith(mockAgent.agent);
    expect(component.agentForm.reset).toHaveBeenCalledWith({ ...mockAgent.agent, rfndVatNumber }, { emitEvent: false });
    expect(component['populateAgentContactDataStored']).toHaveBeenCalled();
  }));

  it('should toggleControlDisable enable control', () => {
    component.agentForm.disable({ emitEvent: false });

    component['toggleControlDisable'](component.agentForm, false);

    expect(component.agentForm.disabled).toBeFalsy();
  });

  it('should toggleControlDisable disable control', () => {
    component.agentForm.disable({ emitEvent: false });

    component['toggleControlDisable'](component.agentForm, true);

    expect(component.agentForm.disabled).toBeTruthy();
  });
});
