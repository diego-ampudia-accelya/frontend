import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of, Subject } from 'rxjs';
import { first, map, takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { Agent } from '~app/master-data/models';
import { AgentAddress } from '~app/master-data/models/agent-address.model';
import { AirlineAddress } from '~app/master-data/models/airline-address.model';
import { LocalAndGlobalAirline } from '~app/master-data/models/airline/local-and-global-airline.model';
import { AppState } from '~app/reducers';
import {
  AgentViewModel,
  AirlineViewModel,
  BasicInformationViewModel,
  ContactViewModel
} from '~app/refund/models/refund-view-aux.model';
import { RefundActionEmitterType } from '~app/refund/models/refund.model';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundService } from '~app/refund/services/refund.service';
import {
  createRefundPermission,
  createRefundPermissionUnderSupervision,
  getRefundCreatePermission,
  updateRefundPermission
} from '~app/refund/shared/helpers/refund-permissions.config';
import { getUserDetails } from '~app/refund/shared/helpers/refund.helper';
import { updateAgentContactData, updateAirlineContactData } from '~app/refund/store/actions/refund-contact.action';
import { getAgentContactData, getAirlineContactData } from '~app/refund/store/reducers';
import { AccessType } from '~app/shared/enums';
import {
  toValueLabelObjectAgentId,
  toValueLabelObjectAirlineId,
  toValueLabelObjectBspId,
  toValueLabelObjectDictionaryId
} from '~app/shared/helpers/filters.helper';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { AgentUser, AirlineUser, User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { multiEmailValidator } from '~app/shared/validators/multi-email.validator';

@Component({
  selector: 'bspl-refund-basic-info',
  templateUrl: './refund-basic-info.component.html',
  styleUrls: ['./refund-basic-info.component.scss'],
  providers: [RefundService]
})
export class RefundBasicInfoComponent implements OnInit, OnDestroy {
  @Input() formData: RefundDataService;
  @Input() formConfig: RefundConfigService;
  @Input() refundService: RefundService;
  @Input() selectedBspId: number;

  public issueTicketMode: boolean;
  public objectKeys = Object.keys;
  public loggedUser: User;

  public form: FormGroup;
  public agentForm: FormGroup;
  public airlineForm: FormGroup;
  public airlineContactForm: FormGroup;
  public agentContactForm: FormGroup;

  public airlineOptions: DropdownOption[];
  public agentOptions: DropdownOption[];
  public bspOptions: DropdownOption<number>[];

  public showAirlineDetails: boolean;
  public showAgentDetails: boolean;
  public showBspList: boolean;

  public isAgentDropdownVisible: boolean;
  public isAirlineDropdownVisible: boolean;

  public isAirlineVatFieldVisible$: Observable<boolean>;
  public isAgentVatFieldVisible$: Observable<boolean>;

  public isAirlineContactFieldVisible$: Observable<boolean>;
  public isAgentContactFieldVisible$: Observable<boolean>;

  public isAirlineContactFieldLocked: boolean;
  public isAgentContactFieldLocked: boolean;

  public airlineSelected: LocalAndGlobalAirline;
  public agentSelected: Agent;
  public bspSelected: Bsp;

  public isAirlineUser: boolean;
  public isAgentUser: boolean;
  public isAgentGroupUser: boolean;

  private formFactory: FormUtil;

  public get isAirlineDropdownLocked(): boolean {
    return (!this.bspSelected || this.airlineOptions?.length === 1) && !this.isAgentGroupUser;
  }

  public get isAgentDropdownLocked(): boolean {
    return (
      ((!this.bspSelected || this.agentOptions?.length === 1) && !this.isAgentGroupUser) ||
      this.formConfig.isResubmission
    );
  }

  private destroy$ = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private airlineDictionaryService: AirlineDictionaryService,
    private agentDictionaryService: AgentDictionaryService
  ) {
    this.formFactory = new FormUtil(this.fb);
    this.initializeLoggedUser();
  }

  public ngOnInit(): void {
    this.setConfig();
    this.buildForm();

    this.initializeDropdowns();
    this.initializeAgentChangesListener();
    this.initializeVatFieldsVisibility();
    this.initializeContactFieldsVisibility();
    this.initializeAirlineChangesListener();
    this.initializeAgentContactDataListener();
    this.initializeAirlineContactDataListener();
  }

  public toggleAgentDetails(): void {
    this.showAgentDetails = !this.showAgentDetails;
  }

  public toggleAirlineDetails(): void {
    this.showAirlineDetails = !this.showAirlineDetails;
  }

  public getUserSelectedDetails(address: AgentAddress | AirlineAddress) {
    return getUserDetails(address);
  }

  public showAirlineControlError(): void {
    FormUtil.showControlState(this.airlineForm.get('id'));
  }

  public showAgentControlError(): void {
    FormUtil.showControlState(this.agentForm.get('id'));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeLoggedUser(): void {
    this.store.pipe(select(getUser), first()).subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.isAgentUser = this.loggedUser.userType === UserType.AGENT;
      this.isAirlineUser = this.loggedUser.userType === UserType.AIRLINE;
      this.isAgentGroupUser = this.loggedUser.userType === UserType.AGENT_GROUP;
    });
  }

  /*
    Method to save Agent Contact data,in cache during the user session, when user modify this fields and if a new Refund is created last value used by the user will be loaded. 
  */
  private populateAgentContactDataStored(): void {
    this.store.pipe(select(getAgentContactData), first()).subscribe(data => {
      const contact: ContactViewModel = data ? data : (this.agentSelected.contact as ContactViewModel);

      this.agentContactForm.patchValue(contact, { emitEvent: false });
    });
  }

  /*
    Method to save Airline Contact data,in cache during the user session, when user modify this fields and if a new Refund is created last value used by the user will be loaded. 
  */
  private populateAirlineContactDataStored(): void {
    this.store.pipe(select(getAirlineContactData), first()).subscribe(data => {
      const contact: ContactViewModel = data ? data : (this.airlineSelected.contact as ContactViewModel);

      this.airlineContactForm.patchValue(contact, { emitEvent: false });
    });
  }

  private setConfig(): void {
    this.issueTicketMode = this.formConfig.isEditTicketMode;

    this.showBspList = this.formConfig.isRn && this.isAirlineUser;
  }

  private buildForm(): void {
    this.initializeSectionForms();

    this.form = this.formFactory.createGroup<BasicInformationViewModel>({
      airline: this.airlineForm,
      agent: this.agentForm,
      airlineContact: this.airlineContactForm,
      agentContact: this.agentContactForm,
      bspName: null
    });

    this.formData.setSectionFormGroup('basicInformation', this.form);
  }

  private initializeDropdowns(): void {
    const userType = this.loggedUser.userType;

    switch (userType) {
      case UserType.AIRLINE:
        this.initializeDropdownsAsAirline();
        break;
      case UserType.AGENT:
        this.initializeDropdownsAsAgent();
        break;
      case UserType.AGENT_GROUP:
        this.initializeDropdownsAsAgentGroup();
        break;
    }
  }

  private initializeDropdownsAsAgentGroup(): void {
    const isCreateAccess = this.formConfig.accessType === AccessType.create;
    const isEditAccess = this.formConfig.accessType === AccessType.edit;
    this.isAgentDropdownVisible = isCreateAccess || isEditAccess || this.issueTicketMode;
    this.isAirlineDropdownVisible = isCreateAccess || isEditAccess || this.issueTicketMode;

    this.onBspChange(this.loggedUser.bsps[0]);
    if (this.issueTicketMode) {
      this.updateAgentSelected();
      this.updateAirlineSelected();
    }

    combineLatest([this.getAgentGroupListObservable(), this.getAgentGroupAirlineListObservable()])
      .pipe(
        first(),
        map(pagedDataArray => pagedDataArray.map(pagedData => (pagedData ? pagedData : [])))
      )
      .subscribe(records => {
        this.agentOptions = toValueLabelObjectDictionaryId(records[0] as DropdownOption<AgentSummary>[]);
        this.airlineOptions = toValueLabelObjectDictionaryId(records[1] as DropdownOption<AirlineSummary>[]);
      });
  }

  private getAgentGroupListObservable(): Observable<DropdownOption<AgentSummary>[]> {
    const aGroupUserPermission = getRefundCreatePermission(this.formConfig.refundType);
    const aGroupUserHasPermission = this.loggedUser.permissions.some(perm => aGroupUserPermission === perm);

    return aGroupUserHasPermission
      ? this.agentDictionaryService.getDropdownOptions({ permission: aGroupUserPermission })
      : of(null);
  }

  private getAgentGroupAirlineListObservable(): Observable<DropdownOption<AirlineSummary>[]> {
    return this.airlineDictionaryService.getDropdownOptions();
  }

  private initializeDropdownsAsAirline(): void {
    this.isAgentDropdownVisible = !this.formConfig.isInvestigate && !this.formConfig.isAuthorize;
    this.isAirlineDropdownVisible = !this.formConfig.isSupervise;

    if (this.formConfig.isInvestigate || this.formConfig.isAuthorize || this.formConfig.isSupervise) {
      this.formData.whenFormIsReady().subscribe(() => {
        this.agentSelected = this.agentForm.value;
        this.formData.getSubject(RefundActionEmitterType.agent).next(this.agentSelected);
        this.onBspChange(this.agentSelected.bsp);

        if (this.formConfig.isSupervise) {
          this.airlineSelected = this.airlineForm.value;
          this.formData.getSubject(RefundActionEmitterType.airline).next(this.airlineSelected);

          this.getBspAgentDropdown(this.agentSelected.bsp.id);
          this.updateAgentSelected();
          this.updateAirlineSelected();
        }
      });
    }

    if (
      this.formConfig.isInvestigate ||
      this.formConfig.isAuthorize ||
      this.formConfig.accessType === AccessType.create ||
      this.issueTicketMode
    ) {
      this.initializeBspChangesListener();
      this.formData.whenFormIsReady().subscribe(() => this.initializeBspDrowpdown());

      if (this.issueTicketMode) {
        this.updateAgentSelected();
      }
    }
  }

  private initializeDropdownsAsAgent(): void {
    this.initializeAgentDropdownAsAgent();
    this.initializeAirlineDropdownAsAgent();
  }

  private initializeAgentDropdownAsAgent(): void {
    this.isAgentDropdownVisible =
      !this.formConfig.isInvestigate && !this.formConfig.isAuthorize && !this.formConfig.isSupervise;

    if ((this.formConfig.accessType === AccessType.create || this.issueTicketMode) && !this.formConfig.isResubmission) {
      const agentUser = this.loggedUser as AgentUser;
      this.onAgentChange(agentUser.agent.id);
      this.onBspChange(agentUser.agent.bsp);
    } else {
      this.formData.whenFormIsReady().subscribe(() => {
        const agentData = this.agentForm.value;
        this.agentSelected = agentData;
        this.agentOptions = [toValueLabelObjectAgentId(agentData)];
        this.formData.getSubject(RefundActionEmitterType.agent).next(this.agentSelected);
        this.onBspChange(this.agentSelected.bsp);
        if (this.formConfig.isSupervise || this.formConfig.isResubmission) {
          this.updateAgentSelected();
        }
      });
    }
  }

  private initializeAirlineDropdownAsAgent(): void {
    this.isAirlineDropdownVisible = !this.formConfig.isInvestigate && !this.formConfig.isAuthorize;

    const agentUser = this.loggedUser as AgentUser;
    // TODO retrieve whole array of bsps when multicountry
    const bspId = agentUser.bsps[0].id;

    this.airlineDictionaryService
      .getDropdownOptions(bspId)
      .pipe(map(airlines => toValueLabelObjectDictionaryId(airlines)))
      .subscribe(res => {
        this.airlineOptions = res;
        if (this.issueTicketMode || this.formConfig.isEditMode || this.formConfig.isResubmission) {
          this.updateAirlineSelected();
        }
      });
  }

  private updateAirlineSelected(): void {
    this.formData.whenFormIsReady().subscribe(() => {
      const airlinePopulatedId = FormUtil.get<AirlineViewModel>(this.airlineForm, 'id').value;
      this.onAirlineChange(airlinePopulatedId);
    });
  }

  private updateAgentSelected(): void {
    this.formData.whenFormIsReady().subscribe(() => {
      const agentPopulatedId = FormUtil.get<AgentViewModel>(this.agentForm, 'id').value;
      if (agentPopulatedId) {
        this.onAgentChange(agentPopulatedId);
      }
    });
  }

  private onAgentChange(agentId: number): void {
    this.refundService.getAgentInfo(agentId).subscribe(info => {
      this.agentSelected = null;
      if (info) {
        this.agentSelected = info.agent;

        const rfndVatNumber = info.agentVatNumber;
        this.agentSelected.address = info.agentAddress;
        this.agentSelected.contact = info.agentContact;
        this.agentSelected.vatNumber = rfndVatNumber;

        this.agentOptions = !this.agentOptions ? [toValueLabelObjectAgentId(this.agentSelected)] : this.agentOptions;

        this.agentForm.reset({ ...this.agentSelected, rfndVatNumber }, { emitEvent: false });

        if (this.isAgentUser || this.isAgentGroupUser) {
          this.populateAgentContactDataStored();
        } else {
          this.agentContactForm.patchValue(this.agentSelected.contact, { emitEvent: false });
        }
      }

      this.formData.getSubject(RefundActionEmitterType.agent).next(this.agentSelected);
    });
  }

  private onAirlineChange(airlineId: number): void {
    const airlineRfndVatNumberCtrl = FormUtil.get<AirlineViewModel>(this.airlineForm, 'rfndVatNumber');

    forkJoin([
      this.formConfig.isAirlineVatFieldVisible(airlineRfndVatNumberCtrl.value).pipe(first()),
      this.refundService.getAirlineInfo(airlineId)
    ]).subscribe(([isAirlineVatFieldVisible, info]) => {
      this.airlineSelected = null;
      if (info) {
        this.airlineSelected = info.airline;

        const rfndVatNumber = info.airlineVatNumber;
        this.airlineSelected.address = info.airlineAddress;
        this.airlineSelected.contact = info.airlineContact;

        if (this.airlineForm && this.airlineContactForm) {
          if (this.formConfig.isRn && this.formConfig.isEditMode) {
            this.airlineForm.reset(this.airlineSelected, { emitEvent: false });
          } else if (isAirlineVatFieldVisible || !(this.formConfig.isAuthorize || this.formConfig.isInvestigate)) {
            // We update refund VAT number if the airline VAT field is visible OR if we are NOT authorizing or investigating the refund
            this.airlineSelected.vatNumber = rfndVatNumber;
            this.airlineForm.reset({ ...this.airlineSelected, rfndVatNumber }, { emitEvent: false });
          }
        }

        if (this.isAirlineUser && this.formConfig.isRn) {
          this.populateAirlineContactDataStored();
        } else {
          this.airlineContactForm.patchValue(this.airlineSelected.contact, { emitEvent: false });
        }
      }

      this.formData.getSubject(RefundActionEmitterType.airline).next(this.airlineSelected);
    });
  }

  private onBspChange(bsp: Bsp): void {
    this.bspSelected = null;
    if (bsp) {
      this.bspSelected = bsp;
      this.bspOptions = !this.bspOptions ? [toValueLabelObjectBspId(bsp)] : this.bspOptions;
      this.formData.getSubject(RefundActionEmitterType.bsp).next(bsp);

      this.form.patchValue({ bspName: this.bspSelected.id }, { emitEvent: false });
    }
  }

  private initializeAgentChangesListener(): void {
    this.agentForm
      .get('id')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(agentId => this.onAgentChange(agentId));
  }

  private initializeAirlineChangesListener(): void {
    this.airlineForm
      .get('id')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(airlineId => this.onAirlineChange(airlineId));
  }

  private initializeBspChangesListener(): void {
    FormUtil.get<BasicInformationViewModel>(this.form, 'bspName')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (value) {
          const selected = this.loggedUser.bsps.find(bsp => bsp.id === value);
          this.onBspChange(selected);

          if (this.isAirlineDropdownVisible) {
            this.getBspAirlineDropdown(value);
          }
          if (this.isAgentDropdownVisible) {
            this.getBspAgentDropdown(value);
          }
        }
      });
  }

  private initializeBspDrowpdown(): void {
    const issueRnPermission = this.formConfig.issuingPendingSupervision
      ? createRefundPermissionUnderSupervision.rnIssue
      : createRefundPermission.rnIssue;

    const bspPermissions = this.loggedUser.bspPermissions.filter(bspPerm =>
      bspPerm.permissions.some(
        perm =>
          ((this.formConfig.isCreateMode || this.issueTicketMode) && perm === issueRnPermission) ||
          (this.formConfig.isEditMode && perm === updateRefundPermission.raUpdate)
      )
    );

    this.bspOptions = bspPermissions.reduce(
      (acc, bspPerm) => [
        ...acc,
        toValueLabelObjectBspId(this.loggedUser.bsps.find(airlBsp => airlBsp.id === bspPerm.bspId))
      ],
      []
    );
    const ticketBsp: BspDto = FormUtil.get<AgentViewModel>(this.agentForm, 'bsp').value;

    const bspId = this.selectedBspId || ticketBsp.id || this.bspOptions[0]?.value;
    FormUtil.get<BasicInformationViewModel>(this.form, 'bspName').patchValue(bspId, { onlySelf: true });
  }

  private getBspAirlineDropdown(bspId: number): void {
    const airlineBsp = (this.loggedUser as AirlineUser).globalAirline.airlines.find(
      airlBsp => airlBsp.bsp.id === bspId
    );
    this.airlineOptions = [
      toValueLabelObjectAirlineId({
        id: airlineBsp.id,
        iataCode: airlineBsp.globalAirline.iataCode,
        localName: airlineBsp.localName
      })
    ];
    this.onAirlineChange(airlineBsp.id);
  }

  private getBspAgentDropdown(bspId: number): void {
    this.agentDictionaryService
      .getDropdownOptions({ bspId })
      .pipe(map(agents => toValueLabelObjectDictionaryId(agents)))
      .subscribe(result => {
        this.agentOptions = result;
      });
  }

  private initializeAgentContactDataListener(): void {
    this.agentContactForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(payload => {
      if (this.agentContactForm.valid) {
        this.store.dispatch(updateAgentContactData({ payload }));
      }
    });
  }

  private initializeAirlineContactDataListener(): void {
    this.airlineContactForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(payload => {
      if (this.airlineContactForm.valid) {
        this.store.dispatch(updateAirlineContactData({ payload }));
      }
    });
  }

  private initializeVatFieldsVisibility(): void {
    this.isAirlineVatFieldVisible$ = of(false);
    this.isAgentVatFieldVisible$ = of(false);

    this.formData.whenFormIsReady().subscribe(() => {
      const airlineRfndVatNumberCtrl = FormUtil.get<AirlineViewModel>(this.airlineForm, 'rfndVatNumber');
      const agentRfndVatNumberCtrl = FormUtil.get<AirlineViewModel>(this.agentForm, 'rfndVatNumber');

      if (this.formConfig.isRn && this.formConfig.isEditMode) {
        this.isAirlineVatFieldVisible$ = of(false);
      } else {
        this.isAirlineVatFieldVisible$ = this.formConfig
          .isAirlineVatFieldVisible(airlineRfndVatNumberCtrl.value)
          .pipe(tap(fieldVisible => this.toggleControlDisable(airlineRfndVatNumberCtrl, !fieldVisible)));
      }

      this.isAgentVatFieldVisible$ = this.formConfig
        .isAgentVatFieldVisible(agentRfndVatNumberCtrl.value)
        .pipe(tap(fieldVisible => this.toggleControlDisable(agentRfndVatNumberCtrl, !fieldVisible)));
    });
  }

  private initializeContactFieldsVisibility(): void {
    this.isAirlineContactFieldLocked = !this.isAirlineUser;
    this.isAgentContactFieldLocked = !this.isAgentUser && !this.isAgentGroupUser;

    this.formData.whenFormIsReady().subscribe(() => {
      this.isAgentContactFieldVisible$ = this.formConfig
        .isContactFieldVisible(this.agentContactForm.value)
        .pipe(
          tap(fieldVisible =>
            this.toggleControlDisable(this.agentContactForm, !fieldVisible || this.isAgentContactFieldLocked)
          )
        );

      this.isAirlineContactFieldVisible$ = this.formConfig
        .isContactFieldVisible(this.airlineContactForm.value)
        .pipe(
          tap(fieldVisible =>
            this.toggleControlDisable(this.airlineContactForm, !fieldVisible || this.isAirlineContactFieldLocked)
          )
        );
    });
  }

  private toggleControlDisable(control: AbstractControl, isDisabled: boolean): void {
    if (isDisabled) {
      control.disable({ emitEvent: false });
    } else {
      control.enable({ emitEvent: false });
    }
  }

  private initializeSectionForms(): void {
    this.airlineForm = this.formFactory.createGroup<AirlineViewModel>({
      id: ['', [Validators.required]],
      iataCode: '',
      localName: '',
      globalName: '',
      vatNumber: ['', [Validators.maxLength(15)]], //Changed to 15 according to BspLink API
      bsp: { id: '', isoCountryCode: '', name: '' },
      address: { address1: '', address2: '', city: '', country: '', postalCode: '', state: '' },
      contact: {
        email: '',
        firstName: '',
        lastName: '',
        telephone: ''
      },
      rfndVatNumber: null
    });

    this.agentForm = this.formFactory.createGroup<AgentViewModel>({
      id: [null, [Validators.required]],
      iataCode: '',
      name: '',
      vatNumber: ['', [Validators.maxLength(15)]], //Changed to 15 according to BspLink API
      bsp: { id: '', isoCountryCode: '', name: '' },
      contact: { email: '', fax: '', telephone: '' },
      address: { street: '', city: '', country: '', postalCode: '' },
      rfndVatNumber: null
    });

    this.airlineContactForm = this.formFactory.createGroup<ContactViewModel>({
      contactName: ['', Validators.maxLength(49)],
      email: ['', [multiEmailValidator]],
      phoneFaxNumber: ['', Validators.maxLength(30)]
    });

    this.agentContactForm = this.formFactory.createGroup<ContactViewModel>({
      contactName: ['', [Validators.maxLength(49)]],
      email: ['', [multiEmailValidator]],
      phoneFaxNumber: ['', [Validators.maxLength(30)]]
    });

    this.setEmailContactValidators();
  }

  private setEmailContactValidators(): void {
    const agentEmailCtrl = FormUtil.get<ContactViewModel>(this.agentContactForm, 'email');
    const airlineEmailCtrl = FormUtil.get<ContactViewModel>(this.airlineContactForm, 'email');

    if (this.isAgentUser) {
      agentEmailCtrl.setValidators([agentEmailCtrl.validator, Validators.required]);
    }
    if (this.isAirlineUser) {
      airlineEmailCtrl.setValidators([airlineEmailCtrl.validator, Validators.required]);
    }
  }
}
