import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { filter, first, skipWhile, switchMap, takeUntil } from 'rxjs/operators';

import { couponConfValidator } from './validators/coupons-configuration.validator';
import { documentNumberValidator } from './validators/document-number.validator';
import { locationCodePatternValidator } from './validators/location-code-pattern.validator';
import { AppState } from '~app/reducers';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { RefundReason } from '~app/refund/models/refund-reason.model';
import { OriginalIssue, RelatedTicketDocument } from '~app/refund/models/refund-shared.model';
import { AirlineViewModel, DetailViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionEmitterType } from '~app/refund/models/refund.model';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundTicketService } from '~app/refund/services/refund-ticket.service';
import { RefundService } from '~app/refund/services/refund.service';
import { getRefundResubmissionRemark } from '~app/refund/store/reducers';
import { ButtonDesign } from '~app/shared/components';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Component({
  selector: 'bspl-refund-details',
  templateUrl: './refund-details.component.html',
  styleUrls: ['./refund-details.component.scss']
})
export class RefundDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() formData: RefundDataService;
  @Input() formConfig: RefundConfigService;
  @Input() refundTicket: RefundTicketService;
  @Input() refundService: RefundService;

  public form: FormGroup;

  public relatedTicketDocument: FormGroup;
  public relatedTicketConjunctions: FormArray;
  public originalIssue: FormGroup;

  public formFactory: FormUtil;

  public maxIssueDate = new Date();
  public reasonRows = 10;
  public reasonLines = 45;
  public maxDocumentNumberLength = 10;

  public issueTicketMode: boolean;

  public exchangeControl: FormControl;
  public resubmissionRemarkControl: FormControl;
  public rejectedDocumentIdControl: FormControl;
  public predefinedReasonControl: FormControl;
  public reasonForRefundControl: FormControl;

  public statOptions: DropdownOption[] = [];
  public reasonRefundOptions$: Observable<DropdownOption<RefundReason>[]>;

  public originalIssueDetailsEnabled = true;

  public btnDesign = ButtonDesign;

  public isRemarksVisible: boolean;

  public isRejectionReasonVisible: boolean;

  public isAirlineCodeFieldLocked: boolean;

  public agentName: string;
  public agentCode: string;

  public isRAWithReason: boolean;

  private isPartialRefund = false;
  private destroy$ = new Subject<any>();

  constructor(private fb: FormBuilder, private store: Store<AppState>) {
    this.formFactory = new FormUtil(this.fb);
  }

  public ngOnInit() {
    this.buildForm();
    this.setConfig();
    this.initializeExchangeListener();
    this.initializeStatListener();
    this.initializeBspRNConfigListener();
    this.initializePartialRefundListener();
    this.initializeCouponsListener();
    this.initializeIssueDate();
    this.initializeAirlineCodeListener();
    this.initializeAgentData();
    this.initializeByAccessType();
    this.initializeRAWithReason();
  }

  public ngAfterViewInit(): void {
    this.exchangeControl.updateValueAndValidity({ onlySelf: true });
  }

  public onRemoveConjunctionClick(index: number) {
    this.relatedTicketConjunctions.removeAt(index);
    this.relatedTicketDocument.updateValueAndValidity();
  }

  public onAddConjunctionClick() {
    if (!this.isAddConjunctionDisabled()) {
      this.relatedTicketConjunctions.push(this.buildRelatedTicketDocumentGroup());
      this.relatedTicketConjunctions.updateValueAndValidity();
    }
  }

  public isAddConjunctionDisabled(): boolean {
    return this.relatedTicketDocument.get('documentNumber').invalid || this.relatedTicketConjunctions.length >= 5;
  }

  public getCouponsErrorMessage(group: FormGroup): string {
    let msg = '';
    if (group.controls && group.errors) {
      const errors = group.errors;
      const errType = Object.keys(errors)[0];
      msg = errors[errType].message;
    }

    return msg;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  //* This method adds a disabled (also not visible in template) group. This group will be enable if any BE data is set
  //* and allows the mapper to replicate an initial group for this data.
  private initializeConjunctionsForBEData() {
    const initialConjunctionsGroup = this.buildRelatedTicketDocumentGroup();
    initialConjunctionsGroup.disable({ emitEvent: false });
    this.relatedTicketConjunctions.push(initialConjunctionsGroup);
  }

  private initializeResubmissionRemark() {
    this.formData
      .whenFormIsReady()
      .pipe(switchMap(() => this.store.pipe(select(getRefundResubmissionRemark(this.rejectedDocumentIdControl.value)))))
      .subscribe(resubmissionRemark => this.resubmissionRemarkControl.setValue(resubmissionRemark));
  }

  private initializeTicketIssueListener() {
    this.formData
      .whenFormIsReady()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.form.patchValue(this.buildRelatedTicketDocs());
        this.refundTicket.conjunctionDocumentTicket.forEach(conjTicket => {
          this.relatedTicketConjunctions.push(this.buildRelatedTicketDocumentGroup(conjTicket));
        });
        this.updateExchange();
      });
  }

  private initializePartialRefundListener() {
    this.formData.getSubject(RefundActionEmitterType.partialRefund).subscribe(value => {
      this.isPartialRefund = value;
      this.relatedTicketDocument.updateValueAndValidity();
    });
  }

  private initializeCouponsListener() {
    let allRelatedCoupons = false;
    this.relatedTicketDocument.valueChanges.subscribe(changes => {
      if (this.isPartialRefund && !this.relatedTicketConjunctions.controls.length) {
        if (changes.coupon1 && changes.coupon2 && changes.coupon3 && changes.coupon4) {
          allRelatedCoupons = true;
          this.relatedTicketDocument.setErrors({
            coupons: { message: `REFUNDS.details.documentsSection.ticketsSection.partialRfndCouponsError` }
          });
          FormUtil.showControlState(this.relatedTicketDocument);
        } else {
          this.relatedTicketDocument.setErrors(null);
        }
      }
    });

    this.relatedTicketConjunctions.valueChanges.subscribe(changes => {
      this.relatedTicketDocument.updateValueAndValidity();
      if (this.isPartialRefund && allRelatedCoupons) {
        const arr = [];
        changes.forEach(element => {
          if (element.coupon1 && element.coupon2 && element.coupon3 && element.coupon4) {
            arr.push(element);
          }
        });

        this.relatedTicketConjunctions.controls.forEach(ctrl => {
          ctrl.setErrors(null);
        });

        if (arr.length && arr.length === changes.length) {
          this.relatedTicketConjunctions.controls[changes.length - 1].setErrors({
            coupons: { message: `REFUNDS.details.documentsSection.ticketsSection.partialRfndCouponsError` }
          });
          FormUtil.showControlState(this.relatedTicketConjunctions);
        } else {
          this.relatedTicketDocument.setErrors(null);
        }
      }
    });
  }

  private initializeByAccessType(): void {
    if (this.formConfig.accessType === AccessType.edit || this.formConfig.isResubmission) {
      this.initializeConjunctionsForBEData();
      if (this.formConfig.isResubmission) {
        this.initializeResubmissionRemark();
      }
    }

    if (this.formConfig.accessType === AccessType.editTicket) {
      this.initializeTicketIssueListener();
    }

    if (this.formConfig.accessType !== AccessType.create) {
      this.initializeAirlineRemarkVisibility();
      this.initializeRejectionReasonVisibility();
    }
  }

  private initializeRAWithReason(): void {
    this.isRAWithReason =
      this.formConfig.isRa &&
      !(this.formConfig.isAuthorize || this.formConfig.isInvestigate) &&
      (this.formConfig.isEditMode || this.formConfig.isResubmission || this.formConfig.isCreateMode);

    if (this.isRAWithReason) {
      this.initializeReasonRefund();
      this.reasonRAChangeListener();
    }
  }

  private updateExchange() {
    if (this.refundTicket.exchangeTicket && this.originalIssueDetailsEnabled) {
      this.exchangeControl.patchValue(this.refundTicket.exchangeTicket);
      this.originalIssue.patchValue(this.refundTicket.originalIssueTicket);
    }
  }

  private buildRelatedTicketDocs(): {
    airlineCodeRelatedDocument: string;
    relatedTicketDocument: RelatedTicketDocument;
  } {
    return {
      airlineCodeRelatedDocument: this.refundTicket.airlineCodeRelatedDocumentTicket,
      relatedTicketDocument: this.refundTicket.documentCouponsTicket
    };
  }

  private setConfig() {
    this.issueTicketMode = this.formConfig.isEditTicketMode;
  }

  private initializeStatListener() {
    if (this.formConfig.accessType !== AccessType.read) {
      this.formConfig
        .getStat()
        .pipe(takeUntil(this.destroy$))
        .subscribe(stats => this.initializeStat(stats));
    }
  }

  private initializeStat(stats: string | { value: string; label: string; selected: boolean }[]) {
    this.statOptions = [];
    let defaultValue = stats;

    if (Array.isArray(stats)) {
      this.statOptions = stats;
      const defaultStat = stats.find(stat => stat.selected);
      defaultValue = defaultStat ? defaultStat.value : null;
    }

    const statControl = FormUtil.get<DetailViewModel>(this.form, 'statisticalCode');
    if (statControl.value === null) {
      statControl.setValue(defaultValue, { emitEvent: false });
    }
  }

  private initializeReasonRefund() {
    this.formData
      .getSubject(RefundActionEmitterType.bsp)
      .pipe(
        skipWhile(bsp => !bsp),
        takeUntil(this.destroy$)
      )
      .subscribe(bsp => {
        this.reasonRefundOptions$ = this.refundService.getRAReasons(bsp.id);
      });
  }

  private initializeBspRNConfigListener() {
    if (this.formConfig.isRn) {
      this.formConfig.bspRefundNoticeConfiguration.subscribe(config => {
        this.originalIssueDetailsEnabled = config.originalIssueDetailsEnabled;
        this.handleBspRnConfig();
      });
    }
  }

  private reasonRAChangeListener() {
    this.predefinedReasonControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((reasonValue: RefundReason) => {
      FormUtil.get<DetailViewModel>(this.form, 'reasonForRefund').setValue(reasonValue.description);
    });
  }

  private handleBspRnConfig() {
    this.formData.whenFormIsReady().subscribe(() => {
      this.onExchangeChange(this.originalIssueDetailsEnabled && this.exchangeControl.value);

      if (!this.originalIssueDetailsEnabled) {
        this.originalIssue.reset({}, { emitEvent: false });
      }
    });
  }

  private buildForm() {
    this.originalIssue = this.formFactory.createGroup<OriginalIssue>({
      airlineCode: ['', [Validators.required, Validators.pattern(/^[0-9A-Za-z]{3}$/)]],
      documentNumber: ['', [Validators.required, Validators.pattern(/^[0-9]{1,10}$/)]],
      agentCode: ['', [Validators.required, Validators.pattern(/^[0-9]{7}[0-6]?$/)]],
      issueDate: ['', Validators.required],
      locationCode: ['', [Validators.required, locationCodePatternValidator]]
    });

    this.relatedTicketDocument = this.buildRelatedTicketDocumentGroup();
    this.relatedTicketConjunctions = new FormArray(
      [],
      this.formConfig.isRa ? documentNumberValidator(this.relatedTicketDocument) : []
    );

    this.exchangeControl = new FormControl(false);
    this.resubmissionRemarkControl = new FormControl('');
    this.rejectedDocumentIdControl = new FormControl('');
    this.predefinedReasonControl = new FormControl(null);
    this.reasonForRefundControl = new FormControl('', this.formConfig.isRa ? Validators.required : []);

    this.form = this.formFactory.createGroup<DetailViewModel>({
      predefinedReason: this.predefinedReasonControl,
      reasonForRefund: this.reasonForRefundControl,
      resubmissionRemark: this.resubmissionRemarkControl,
      rejectedDocumentId: this.rejectedDocumentIdControl,
      airlineRemark: [''],
      rejectionReason: [''],
      statisticalCode: [null, [Validators.pattern(/^( {0,3}|([IDid][ A-Za-z0-9]{0,2})?)$/)]],
      passenger: [
        '',
        [Validators.required, Validators.maxLength(49), Validators.pattern(/^[\x20\x2D-\x39\x41-\x5A\x61-\x7A]*$/)]
      ],
      dateOfIssueRelatedDocument: ['', Validators.required],
      waiverCode: ['', [Validators.maxLength(14), Validators.pattern(/^[A-Za-z0-9 /.-]*$/)]],
      airlineCodeRelatedDocument: ['', [Validators.required, Validators.pattern(/^[0-9A-Za-z]{3}$/)]],
      relatedTicketDocument: this.relatedTicketDocument,
      relatedTicketConjunctions: this.relatedTicketConjunctions,
      relatedTicketExchange: this.exchangeControl,
      originalIssue: this.originalIssue
    });

    this.formData.setSectionFormGroup('details', this.form);

    if (this.formConfig.isRa) {
      this.initializeCouponConfValidator();
    }
  }

  private initializeAirlineCodeListener() {
    this.isAirlineCodeFieldLocked = this.formConfig.isRn || this.formConfig.isInvestigate;

    if (this.formConfig.isRn) {
      this.formData
        .getSubject(RefundActionEmitterType.airline)
        .pipe(takeUntil(this.destroy$))
        .subscribe(airline => {
          this.updateAirlineCode(airline);
        });
    }
  }

  private initializeAgentData() {
    this.formData
      .getSubject(RefundActionEmitterType.agent)
      .pipe(first(agent => !!agent))
      .subscribe(agent => {
        this.agentName = agent.name;
        this.agentCode = agent.iataCode;
      });
  }

  private updateAirlineCode(airline: AirlineViewModel) {
    if (airline) {
      this.form.patchValue({ airlineCodeRelatedDocument: airline.iataCode }, { emitEvent: false });
    }
  }

  private initializeExchangeListener() {
    this.exchangeControl.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(exchange => this.onExchangeChange(exchange));
  }

  private onExchangeChange(value: boolean): void {
    if (value) {
      this.originalIssue.enable({ emitEvent: false });
    } else {
      this.originalIssue.disable({ emitEvent: false });
    }
  }

  private buildRelatedTicketDocumentGroup(relatedTicket?: RelatedTicketDocument): FormGroup {
    return this.formFactory.createGroup<RelatedTicketDocument>({
      documentNumber: [
        relatedTicket ? relatedTicket.documentNumber : '',
        [Validators.required, Validators.pattern('^[0-9]{1,10}$')]
      ],
      documentId: relatedTicket && relatedTicket.documentId !== '' ? relatedTicket.documentId : null,
      coupon1: relatedTicket ? relatedTicket.coupon1 : false,
      coupon2: relatedTicket ? relatedTicket.coupon2 : false,
      coupon3: relatedTicket ? relatedTicket.coupon3 : false,
      coupon4: relatedTicket ? relatedTicket.coupon4 : false
    });
  }

  private initializeCouponConfValidator() {
    this.formConfig.bspRefundConfiguration
      .pipe(
        first(),
        filter(config => !config.withoutCouponsCapture)
      )
      .subscribe(() => this.form.setValidators(couponConfValidator()));
  }

  private initializeIssueDate() {
    this.formData
      .getSubject(RefundActionEmitterType.bsp)
      .pipe(
        skipWhile(bsp => !bsp),
        switchMap(bsp => this.refundService.getBspTime(bsp.id)),
        takeUntil(this.destroy$)
      )
      .subscribe(time => (this.maxIssueDate = time.localTime));
  }

  private initializeAirlineRemarkVisibility() {
    this.formData.getIssueStatus().subscribe(status => {
      this.isRemarksVisible = status?.status !== RefundStatus.pending;
    });
  }

  private initializeRejectionReasonVisibility() {
    this.formData.whenFormIsReady().subscribe(() => {
      const rejections = FormUtil.get<DetailViewModel>(this.form, 'rejectionReason').value;
      this.isRejectionReasonVisible = (this.formConfig.isInvestigate || this.formConfig.isAuthorize) && !!rejections;
    });
  }
}
