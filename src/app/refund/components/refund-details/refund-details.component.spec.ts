import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { RefundDetailsComponent } from '../refund-details/refund-details.component';
import { RelatedTicketDocument } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { AirlineViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundTicketService } from '~app/refund/services/refund-ticket.service';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundConfigService } from '~app/refund/shared/helpers/refund.factory';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { AccessType } from '~app/shared/enums';
import { FormUtil } from '~app/shared/helpers';

const mockAgent = {};

const initialState = {
  auth: {
    user: mockAgent
  },
  core: {
    menu: {
      tabs: {}
    }
  }
};

const permissionsService = createSpyObject(PermissionsService);
const refundTicketServiceSpy = createSpyObject(RefundTicketService);

const refundServiceSpy = jasmine.createSpyObj<RefundService>('RefundService', ['getBspTime', 'getRAReasons']);

describe('RefundDetailsComponent', () => {
  let component: RefundDetailsComponent;
  let fixture: ComponentFixture<RefundDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundDetailsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule,
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule,
        NoopAnimationsModule
      ],
      providers: [
        { provide: PermissionsService, useValue: permissionsService },
        { provide: RefundService, useValue: refundServiceSpy },
        { provide: RefundTicketService, useValue: refundTicketServiceSpy },
        RefundTicketService,
        provideMockStore({ initialState }),
        HttpTestingController,
        FormBuilder,
        L10nTranslationService,
        HttpClient,
        RefundActionsService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundDetailsComponent);
    component = fixture.componentInstance;
    component.refundService = refundServiceSpy;

    component.formData = new RefundDataService(TestBed.inject(FormBuilder));
    component.formConfig = createRefundConfigService(
      component.formData,
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Store),
      TestBed.inject(RefundActionsService)
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable OriginalIssue when onExchange is checked', done => {
    const exchangeControl = component.form.get('relatedTicketExchange');
    expect(component.originalIssue.enabled).toBeFalsy();
    exchangeControl.setValue(true);

    fixture.whenStable().then(() => {
      expect(component.originalIssue.enabled).toBeTruthy();
      done();
    });
  });

  it('should add conjunction lines on click', () => {
    expect(component.relatedTicketConjunctions.length).toBe(0);
    component.relatedTicketDocument.get('documentNumber').setValue('1231231231');
    component.onAddConjunctionClick();
    expect(component.relatedTicketConjunctions.length).toBe(1);
  });

  it('should remove conjunction lines on click', () => {
    component.onAddConjunctionClick();
    component.onRemoveConjunctionClick(0);
    expect(component.relatedTicketConjunctions.length).toBe(0);
  });

  it('should call proper functions when  initializeByAccessType is called with Resubmission', fakeAsync(() => {
    spyOn<any>(component, 'initializeConjunctionsForBEData');
    spyOn<any>(component, 'initializeResubmissionRemark');

    component.formConfig.isResubmission = true;
    component.ngOnInit();

    tick();

    expect(component['initializeConjunctionsForBEData']).toHaveBeenCalled();
    expect(component['initializeResubmissionRemark']).toHaveBeenCalled();
  }));

  it('should call proper functions when  initializeByAccessType is called with editTicket', fakeAsync(() => {
    spyOn<any>(component, 'initializeTicketIssueListener');

    component.formConfig.accessType = AccessType.editTicket;
    component.ngOnInit();

    tick();

    expect(component['initializeTicketIssueListener']).toHaveBeenCalled();
  }));

  it('should call proper functions when  initializeRAWithReason is called', () => {
    spyOn<any>(component, 'initializeReasonRefund');
    spyOn<any>(component, 'reasonRAChangeListener');

    component.formConfig.refundType = MasterDataType.RefundApp;
    component.formConfig.isAuthorize = false;
    component.formConfig.accessType = AccessType.edit;

    component['initializeRAWithReason']();

    expect(component['initializeReasonRefund']).toHaveBeenCalled();
    expect(component['reasonRAChangeListener']).toHaveBeenCalled();
  });

  it('should call proper functions when  initializeConjunctionsForBEData is called', () => {
    const relatedTicketDocument = {
      coupon1: new FormControl(true),
      coupon2: new FormControl(true),
      coupon3: new FormControl(true),
      coupon4: new FormControl(true),
      documentNumber: new FormControl('123456'),
      documentId: new FormControl('65432')
    };

    const initialConjunctionsGroup = new FormGroup(relatedTicketDocument);

    spyOn<any>(component, 'buildRelatedTicketDocumentGroup').and.returnValue(initialConjunctionsGroup);

    component['initializeConjunctionsForBEData']();

    expect(component['buildRelatedTicketDocumentGroup']).toHaveBeenCalled();
  });

  it('should initialize Coupons Listener', fakeAsync(() => {
    const relatedTicketDocument = {
      coupon1: new FormControl(true),
      coupon2: new FormControl(true),
      coupon3: new FormControl(true),
      coupon4: new FormControl(true),
      documentNumber: new FormControl('123456'),
      documentId: new FormControl('65432')
    };

    component['isPartialRefund'] = true;

    const setErrorsSpy = spyOn<any>(component.relatedTicketDocument, 'setErrors');
    const showControlStateSpy = spyOn<any>(FormUtil, 'showControlState');

    component['initializeCouponsListener']();

    const relatedTicketDocumentControl = component.form.get('relatedTicketDocument');
    relatedTicketDocumentControl.patchValue(relatedTicketDocument);

    const relatedTicketConjunctionsControl = component.form.get('relatedTicketConjunctions');
    relatedTicketConjunctionsControl.patchValue([]);

    tick();

    expect(setErrorsSpy).toHaveBeenCalled();
    expect(showControlStateSpy).toHaveBeenCalled();
  }));

  it('should initialize Stats', () => {
    const stats = [
      {
        value: 'value',
        label: 'label',
        selected: true
      }
    ];

    const statisticalCodeControl = component.form.get('statisticalCode');
    statisticalCodeControl.patchValue(null);

    component['initializeStat'](stats);

    expect(component.statOptions).toEqual(stats);
    expect(statisticalCodeControl.value).toEqual(stats[0].value);
  });

  it('should initialize resubmissionRemark Control', fakeAsync(() => {
    spyOn<any>(component.formData, 'whenFormIsReady').and.returnValue(of(true));
    const storePipe = spyOn<any>(component['store'], 'pipe');
    const resubmissionRemarkControlValue = spyOn<any>(component['resubmissionRemarkControl'], 'setValue');

    storePipe.and.returnValue(of(''));
    component['initializeResubmissionRemark']();

    tick();

    expect(storePipe).toHaveBeenCalled();
    expect(resubmissionRemarkControlValue).toHaveBeenCalled();
    expect(component.resubmissionRemarkControl.value).toEqual('');
  }));

  it('should initialize TicketIssue Listener', fakeAsync(() => {
    const relatedTicketDocument: RelatedTicketDocument[] = [
      {
        relatedTicketDocumentNumber: '1234',
        documentId: '4321'
      }
    ];

    (component as any).refundTicket = {
      conjunctionDocumentTicket: relatedTicketDocument
    };

    spyOn<any>(component.formData, 'whenFormIsReady').and.returnValue(of(true));

    const formPatchValueSpy = spyOn<any>(component.form, 'patchValue');
    const refundTicketForEachSpy = spyOn<any>(component.refundTicket.conjunctionDocumentTicket, 'forEach');
    const updateExchangeSpy = spyOn<any>(component, 'updateExchange');

    component['initializeTicketIssueListener']();

    tick();

    expect(formPatchValueSpy).toHaveBeenCalled();
    expect(refundTicketForEachSpy).toHaveBeenCalled();
    expect(updateExchangeSpy).toHaveBeenCalled();
  }));

  it('should initialize Reason Refund', fakeAsync(() => {
    const resBsp = {
      id: '1234'
    };

    spyOn<any>(component.formData, 'getSubject').and.returnValue(of(resBsp));

    component['initializeReasonRefund']();

    tick();

    expect(refundServiceSpy.getRAReasons).toHaveBeenCalledWith('1234');
  }));

  it('should set proper value when reasonRAChangeListener is called', fakeAsync(() => {
    spyOn<any>(component.predefinedReasonControl.valueChanges, 'pipe').and.returnValue(
      of({ title: 'test', description: 'test-description' })
    );

    const reasonForRefundControl = component.form.get('reasonForRefund');

    component['reasonRAChangeListener']();

    tick();

    expect(reasonForRefundControl.value).toEqual('test-description');
  }));

  it('should update Exchange', () => {
    (component as any).refundTicket = {
      exchangeTicket: true,
      originalIssueTicket: true
    };
    component.originalIssueDetailsEnabled = true;

    component['updateExchange']();

    expect(component.exchangeControl.value).toBeTruthy();
    expect(component.originalIssue.value).toBeTruthy();
  });

  it('should handle Bsp Rn Config', fakeAsync(() => {
    component.originalIssueDetailsEnabled = false;
    component.exchangeControl.patchValue(true);

    spyOn<any>(component.formData, 'whenFormIsReady').and.returnValue(of(true));

    const onExchangeChangeSpy = spyOn<any>(component, 'onExchangeChange');
    const originalIssueResetSpy = spyOn<any>(component.originalIssue, 'reset');

    component['handleBspRnConfig']();

    tick();

    expect(onExchangeChangeSpy).toHaveBeenCalledWith(false);
    expect(originalIssueResetSpy).toHaveBeenCalled();
  }));

  it('should initialize Airline Code if Refund Notice', fakeAsync(() => {
    component.formConfig.refundType = MasterDataType.RefundNotice;

    const resAirline = {
      name: 'Airline Name'
    };

    spyOn<any>(component.formData, 'getSubject').and.returnValue(of(resAirline));
    const updateAirlineCodeSpy = spyOn<any>(component, 'updateAirlineCode');

    component['initializeAirlineCodeListener']();

    tick();

    expect(updateAirlineCodeSpy).toHaveBeenCalledWith(resAirline);
  }));

  it('should initialize Agent Data', fakeAsync(() => {
    const resAgent = {
      name: 'Agent Name',
      iataCode: '8976'
    };

    spyOn<any>(component.formData, 'getSubject').and.returnValue(of(resAgent));

    component['initializeAgentData']();

    tick();

    expect(component.agentName).toEqual('Agent Name');
    expect(component.agentCode).toEqual('8976');
  }));

  it('should update Airline Code', () => {
    const airline: AirlineViewModel = {
      id: 123,
      iataCode: '3456',
      localName: '',
      globalName: '',
      vatNumber: '',
      bsp: null,
      address: null,
      contact: null
    };

    const expectedParam = { airlineCodeRelatedDocument: airline.iataCode };

    const formPatchValueSpy = spyOn<any>(component.form, 'patchValue');

    component['updateAirlineCode'](airline);

    expect(formPatchValueSpy).toHaveBeenCalledWith(expectedParam, { emitEvent: false });
  });

  it('should initialize rejection Reason Visibility', fakeAsync(() => {
    spyOn<any>(component.formData, 'whenFormIsReady').and.returnValue(of(true));

    component.formConfig.isInvestigate = true;
    component.form.get('rejectionReason').patchValue('reason');
    component['initializeRejectionReasonVisibility']();

    tick();

    expect(component.isRejectionReasonVisible).toBeTruthy();
  }));

  it('should initialize Airline Remark Visibility', fakeAsync(() => {
    spyOn<any>(component.formData, 'getIssueStatus').and.returnValue(of(RefundStatus.issued));

    component['initializeAirlineRemarkVisibility']();
    tick();

    expect(component.isRemarksVisible).toBeTruthy();
  }));
});
