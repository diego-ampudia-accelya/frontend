import { FormArray, FormGroup, ValidatorFn } from '@angular/forms';

import { RelatedTicketDocument } from '~app/refund/models/refund-shared.model';
import { DetailViewModel } from '~app/refund/models/refund-view-aux.model';
import { FormUtil } from '~app/shared/helpers';

/**
 * @todo We found some problems implementing AsyncValidator here because the dynamic forms and the sync/async validation compatibility.
 * We'll take a look into it in the future with more time
 */
export function couponConfValidator(): ValidatorFn {
  return (form: FormGroup) => {
    const relatedTicketCtrl = FormUtil.get<DetailViewModel>(form, 'relatedTicketDocument') as FormGroup;
    const relatedTicketConjCtrl = FormUtil.get<DetailViewModel>(form, 'relatedTicketConjunctions') as FormArray;

    const isNotMainCouponSelected = areAllCouponsEmpty(relatedTicketCtrl);

    const conjCouponNotSelected = relatedTicketConjCtrl.controls.filter((conjunction: FormGroup) =>
      areAllCouponsEmpty(conjunction)
    );

    if (!relatedTicketConjCtrl.length) {
      if (isDocumentNumberWithoutCouponsSelected(relatedTicketConjCtrl, isNotMainCouponSelected, relatedTicketCtrl)) {
        relatedTicketCtrl.setErrors({
          coupons: { message: `REFUNDS.details.documentsSection.ticketsSection.withoutCouponsCaptureError` }
        });
      }
    } else {
      if (isConjuctionWithoutCouponsSelected(isNotMainCouponSelected, conjCouponNotSelected, relatedTicketConjCtrl)) {
        relatedTicketConjCtrl.controls[relatedTicketConjCtrl.length - 1].setErrors({
          coupons: { message: `REFUNDS.details.documentsSection.ticketsSection.withoutCouponsCaptureError` }
        });
      } else {
        relatedTicketConjCtrl.controls[relatedTicketConjCtrl.length - 1].setErrors(null);
      }
    }

    return null;
  };
}

function isDocumentNumberWithoutCouponsSelected(relatedTicketConjCtrl, isNotMainCouponSelected, relatedTicketCtrl) {
  return (
    !relatedTicketConjCtrl.length && isNotMainCouponSelected && !!getCtrlValue(relatedTicketCtrl, 'documentNumber')
  );
}
function isConjuctionWithoutCouponsSelected(isNotMainCouponSelected, conjCouponNotSelected, relatedTicketConjCtrl) {
  return (
    isNotMainCouponSelected &&
    conjCouponNotSelected.length &&
    conjCouponNotSelected.length === relatedTicketConjCtrl.length
  );
}

function getCtrlValue(group: FormGroup, field: keyof RelatedTicketDocument) {
  return FormUtil.get<RelatedTicketDocument>(group, field).value;
}

function areAllCouponsEmpty(group: FormGroup): boolean {
  return (
    !getCtrlValue(group, 'coupon1') &&
    !getCtrlValue(group, 'coupon2') &&
    !getCtrlValue(group, 'coupon3') &&
    !getCtrlValue(group, 'coupon4')
  );
}
