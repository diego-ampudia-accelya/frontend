import { FormControl } from '@angular/forms';

import { locationCodePatternValidator } from './location-code-pattern.validator';

describe('LocationCodePaternValidator', () => {
  it('null value should return that there are an error', () => {
    const locationValue = null;

    const formControl = new FormControl(locationValue);

    const result = locationCodePatternValidator(formControl);

    expect(result).not.toBeNull();
  });

  it('incorrect pattern should return that there are an error', () => {
    const locationValue = 'a';

    const formControl = new FormControl(locationValue);

    const result = locationCodePatternValidator(formControl);

    expect(result).not.toBeNull();
    expect(result).toEqual({
      locationCodePattern: { key: 'REFUNDS.details.documentsSection.exchangeSection.locationCodeError' }
    });
  });

  it('correct pattern should return that there are not any error', () => {
    const locationValue = 'aBC';

    const formControl = new FormControl(locationValue);

    const result = locationCodePatternValidator(formControl);

    expect(result).toBeNull();
  });
});
