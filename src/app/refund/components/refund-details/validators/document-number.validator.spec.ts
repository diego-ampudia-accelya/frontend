import { FormArray, FormControl, FormGroup } from '@angular/forms';

import { documentNumberValidator } from './document-number.validator';

describe('DocumentNumberValidator', () => {
  it('should return null when empty array', () => {
    const relatedDocumentControl = new FormControl({ documentNumber: null });
    const formArray = new FormArray([]);

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(result).toBeNull();
  });

  it('should return null when empty control value', () => {
    const relatedDocumentControl = new FormControl(null);
    const firstGroup = new FormGroup({ documentNumber: new FormControl('1') });
    const secondGroup = new FormGroup({ documentNumber: new FormControl('2') });
    const formArray = new FormArray([firstGroup, secondGroup]);

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(result).toBeNull();
  });

  it('should set error for 2nd array control when its value less than documentNumber value', () => {
    const relatedDocumentControl = new FormControl({ documentNumber: '10' });
    const firstControl = new FormControl(null);
    const secondControl = new FormControl('8');
    const firstGroup = new FormGroup({ documentNumber: firstControl });
    const secondGroup = new FormGroup({ documentNumber: secondControl });
    const formArray = new FormArray([firstGroup, secondGroup]);
    const error = {
      wrongNumber: { message: `REFUNDS.details.documentsSection.ticketsSection.documentNumberError` }
    };

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(firstControl.errors).toBeNull();
    expect(secondControl.errors).not.toBeNull();
    expect(secondControl.hasError('wrongNumber')).toBeTruthy();
    expect(secondControl.errors).toEqual(error);
    expect(result).toBeNull();
  });

  it('should set error for 2nd array control when its value more than max documentNumber value', () => {
    const relatedDocumentControl = new FormControl({ documentNumber: '10' });
    const firstControl = new FormControl(null);
    const secondControl = new FormControl('17');
    const firstGroup = new FormGroup({ documentNumber: firstControl });
    const secondGroup = new FormGroup({ documentNumber: secondControl });
    const formArray = new FormArray([firstGroup, secondGroup]);
    const error = {
      wrongNumber: { message: `REFUNDS.details.documentsSection.ticketsSection.documentNumberError` }
    };

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(firstControl.errors).toBeNull();
    expect(secondControl.errors).not.toBeNull();
    expect(secondControl.hasError('wrongNumber')).toBeTruthy();
    expect(secondControl.errors).toEqual(error);
    expect(result).toBeNull();
  });

  it('should set error for 2nd array control when its documentNumber less then previous', () => {
    const relatedDocumentControl = new FormControl({ documentNumber: '10' });
    const firstControl = new FormControl('13');
    const secondControl = new FormControl('12');
    const firstGroup = new FormGroup({ documentNumber: firstControl });
    const secondGroup = new FormGroup({ documentNumber: secondControl });
    const formArray = new FormArray([firstGroup, secondGroup]);
    const error = {
      wrongNumber: { message: `REFUNDS.details.documentsSection.ticketsSection.documentNumberGreaterError` }
    };

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(secondControl.errors).not.toBeNull();
    expect(secondControl.hasError('wrongNumber')).toBeTruthy();
    expect(result).toBeNull();
    expect(secondControl.errors).toEqual(error);
  });

  it('should set error for 2nd and 3rd array control when its documentNumber is repeated', () => {
    const relatedDocumentControl = new FormControl({ documentNumber: '10' });
    const firstControl = new FormControl('12');
    const secondControl = new FormControl('13');
    const thirdControl = new FormControl('13');
    const firstGroup = new FormGroup({ documentNumber: firstControl });
    const secondGroup = new FormGroup({ documentNumber: secondControl });
    const thirdGroup = new FormGroup({ documentNumber: thirdControl });
    const formArray = new FormArray([firstGroup, secondGroup, thirdGroup]);
    const error = {
      wrongNumber: { message: `REFUNDS.details.documentsSection.ticketsSection.documentNumberEqualsError` }
    };

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(secondControl.errors).not.toBeNull();
    expect(secondControl.hasError('wrongNumber')).toBeTruthy();
    expect(thirdControl.hasError('wrongNumber')).toBeTruthy();
    expect(secondControl.errors).toEqual(error);
    expect(thirdControl.errors).toEqual(error);
    expect(result).toBeNull();
  });

  it('should set error for 2nd array control when its value is equal to documentNumber', () => {
    const relatedDocumentControl = new FormControl({ documentNumber: '10' });
    const firstControl = new FormControl(null);
    const secondControl = new FormControl('10');
    const firstGroup = new FormGroup({ documentNumber: firstControl });
    const secondGroup = new FormGroup({ documentNumber: secondControl });
    const formArray = new FormArray([firstGroup, secondGroup]);
    const error = {
      wrongNumber: { message: `REFUNDS.details.documentsSection.ticketsSection.documentNumberEqualsError` }
    };

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(secondControl.errors).not.toBeNull();
    expect(secondControl.hasError('wrongNumber')).toBeTruthy();
    expect(secondControl.errors).toEqual(error);
    expect(result).toBeNull();
  });

  it('should removed errors for 2nd control when all conditions passed', () => {
    const relatedDocumentControl = new FormControl({ documentNumber: '10' });
    const firstControl = new FormControl('11');
    firstControl.setErrors({ wrongNumber: {} });
    const formArray = new FormArray([new FormGroup({ documentNumber: firstControl })]);

    const result = documentNumberValidator(relatedDocumentControl)(formArray);

    expect(firstControl.hasError('wrongNumber')).toBeFalsy();
    expect(firstControl.errors).toBeNull();
    expect(result).toBeNull();
  });
});
