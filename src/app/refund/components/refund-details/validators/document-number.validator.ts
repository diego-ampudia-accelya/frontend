import { AbstractControl, FormArray, FormGroup, ValidatorFn } from '@angular/forms';

import { RelatedTicketDocument } from '~app/refund/models/refund-shared.model';
import { FormUtil } from '~app/shared/helpers';

export function documentNumberValidator(relatedDocumentControl: AbstractControl): ValidatorFn {
  return (formArray: FormArray): { [key: string]: boolean } => {
    const relatedDocumentControlValue = relatedDocumentControl.value;

    if (formArray.value.length && relatedDocumentControlValue) {
      const value = parseFloat(relatedDocumentControlValue.documentNumber);
      const maxConjValue = value + 5;

      formArray.controls.forEach((ctrlGroup, i) => {
        const ticketCtrl = FormUtil.get<RelatedTicketDocument>(ctrlGroup as FormGroup, 'documentNumber');

        if (!ticketCtrl.value) {
          return;
        }

        const tickValue = parseFloat(ticketCtrl.value);

        if (value > tickValue || tickValue > maxConjValue) {
          setErrors(ticketCtrl, 'documentNumberError');

          return;
        }

        if (formArray.value.length > 1 && i > 0 && tickValue < parseFloat(formArray.value[i - 1].documentNumber)) {
          setErrors(ticketCtrl, 'documentNumberGreaterError');

          return;
        }

        const isRepeated = getIsRepeated(formArray, ticketCtrl);
        if (isRepeated || value === tickValue) {
          setErrors(ticketCtrl, 'documentNumberEqualsError');

          return;
        }

        if (ticketCtrl.hasError('wrongNumber')) {
          ticketCtrl.setErrors(null);
        }
      });
    }

    return null;
  };
}

function getIsRepeated(formArray: FormArray, ticketCtrl: AbstractControl): boolean {
  const arraySizeMoreThanOne = formArray.length > 1;

  return arraySizeMoreThanOne && formArray.value.filter(f => f.documentNumber === ticketCtrl.value).length > 1;
}

function setErrors(ticketCtrl: AbstractControl, errorMessage: string): void {
  ticketCtrl.setErrors({
    wrongNumber: { message: `REFUNDS.details.documentsSection.ticketsSection.${errorMessage}` }
  });

  FormUtil.showControlState(ticketCtrl);
}
