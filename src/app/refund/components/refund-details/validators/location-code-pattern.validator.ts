import { AbstractControl } from '@angular/forms';

export function locationCodePatternValidator(
  control: AbstractControl
): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  if (!control.value?.match(/^[A-Za-z]{3}$/)) {
    result = {
      locationCodePattern: { key: 'REFUNDS.details.documentsSection.exchangeSection.locationCodeError' }
    };
  }

  return result;
}
