import { TestBed } from '@angular/core/testing';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { couponConfValidator } from './coupons-configuration.validator';
import { RelatedTicketDocument } from '~app/refund/models/refund-shared.model';
import { DetailViewModel } from '~app/refund/models/refund-view-aux.model';
import { FormUtil } from '~app/shared/helpers';

describe('coupons-configurationValidator', () => {
  let formGroup: FormGroup;
  let relatedTicketDocumentFalse: FormGroup;
  let relatedTicketDocumentTrue: FormGroup;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormBuilder]
    });
    formGroup = new FormGroup({
      documentNumber: new FormControl(['232'])
    });

    const builder = TestBed.inject(FormBuilder);
    const formUtil = new FormUtil(builder);

    formGroup = formUtil.createGroup<DetailViewModel>({
      reasonForRefund: [''],
      resubmissionRemark: [''],
      rejectedDocumentId: [''],
      airlineRemark: [''],
      rejectionReason: [''],
      statisticalCode: [''],
      passenger: [''],
      dateOfIssueRelatedDocument: [''],
      waiverCode: [''],
      airlineCodeRelatedDocument: [''],
      relatedTicketDocument: [''],
      relatedTicketConjunctions: [''],
      relatedTicketExchange: [''],
      originalIssue: ['']
    });
    relatedTicketDocumentFalse = formUtil.createGroup<RelatedTicketDocument>({
      coupon1: [false],
      coupon2: [false],
      coupon3: [false],
      coupon4: [false],
      documentNumber: ['123456']
    });

    relatedTicketDocumentTrue = formUtil.createGroup<RelatedTicketDocument>({
      coupon1: [true],
      coupon2: [false],
      coupon3: [false],
      coupon4: [false],
      documentNumber: [123456]
    });
  });

  it('should return that there are coupons errors in relatedTicketDocument', () => {
    formGroup.setControl('relatedTicketDocument', relatedTicketDocumentFalse);
    formGroup.setControl('relatedTicketConjunctions', new FormArray([]));
    formGroup.setValidators(couponConfValidator());
    formGroup.updateValueAndValidity();

    expect(formGroup.errors).toEqual(null);
    expect(formGroup.get('relatedTicketDocument').errors).not.toBeNull();
  });

  it('should return that there are coupons errors in relatedTickedsdtConjunctions', () => {
    formGroup.setControl('relatedTicketDocument', relatedTicketDocumentFalse);
    formGroup.setControl('relatedTicketConjunctions', new FormArray([relatedTicketDocumentFalse]));

    formGroup.setValidators(couponConfValidator());
    formGroup.updateValueAndValidity();

    expect(formGroup.errors).toEqual(null);
    expect((formGroup.get('relatedTicketConjunctions') as FormGroup).controls[0].getError('coupons')).not.toBeNull();
  });

  it('should return that there are not errors', () => {
    formGroup.setControl('relatedTicketDocument', relatedTicketDocumentTrue);
    formGroup.setControl(
      'relatedTicketConjunctions',
      new FormArray([relatedTicketDocumentTrue, relatedTicketDocumentTrue])
    );

    formGroup.setValidators(couponConfValidator());
    formGroup.updateValueAndValidity();

    expect(formGroup.errors).toBeNull();
    expect((formGroup.get('relatedTicketConjunctions') as FormGroup).controls[1].errors).toBeNull();
  });
});
