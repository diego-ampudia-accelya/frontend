import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { partition } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { filter, first, skipWhile, takeUntil, tap } from 'rxjs/operators';

import { Agent } from '~app/master-data/models';
import { BulkUpdateResponse } from '~app/master-data/models/bulk-update-response.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundActionType, RefundBE } from '~app/refund/models/refund.model';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundService } from '~app/refund/shared/helpers/refund.helper';
import { ButtonDesign, DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ButtonsFooterFactory } from '~app/shared/components/dialog/elements/buttons-footer-factory';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { PillInformationType } from '~app/shared/enums/pill-information-type';
import { FormUtil } from '~app/shared/helpers';
import { ResponseErrorBE } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { TabService } from '~app/shared/services';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Component({
  selector: 'bspl-refund-multiple-confirmation-dialog',
  templateUrl: './refund-multiple-confirmation-dialog.component.html',
  styleUrls: ['./refund-multiple-confirmation-dialog.component.scss']
})
export class RefundMultipleConfirmationDialogComponent implements OnInit, OnDestroy {
  @ViewChild(UploadComponent) uploadComponent: UploadComponent;

  public refundType: MasterDataType;
  public actionType: RefundActionType;
  public actionTypeEnum = RefundActionType;
  public items: RefundBE[];

  public keysRoot: string;
  public pillInformationType = PillInformationType;
  public isResponseMode: boolean;

  // Subtitle section
  public itemsTooltip: string;
  public refundPills: Array<{ value: string }>;
  public remainingPill: { value: string };
  // Success section
  public itemsSuccessTooltip: string;
  public refundSuccessPills: Array<{ value: string }>;
  public remainingSuccessPill: { value: string };
  // Error section
  public itemsErrorTooltip: string;
  public refundErrorPills: Array<{ value: string }>;
  public remainingErrorPill: { value: string };

  public dialogForm = new FormGroup({});
  public reasonControl: FormControl;
  public confirmCheckboxControl: FormControl;

  public pillsLimit = 8;
  public reasonRows = 10;
  public reasonLines = 45;

  public uploadMode = UploadModes.Preview;
  public uploadPath: string;

  private refundService: RefundService;
  private bspId: number;

  private isUploadingInProgress = false;

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private translationService: L10nTranslationService,
    private reactiveSubject: ReactiveSubject,
    private http: HttpClient,
    private dialog: DialogService,
    private tabService: TabService,
    private globalErrorHandlerService: ErrorHandler
  ) {
    this.initializeDialogData();
    this.initializeBspId();

    this.refundService = createRefundService(this.http, this.refundType);
  }

  public ngOnInit(): void {
    this.uploadPath = `${appConfiguration.baseUploadPath}/refund-management/files`;

    this.initializePillsSection();
    this.initializeConfirmSection();
    this.initializeSubmitListener();
  }

  public onUploadIsInProgress(value: boolean): void {
    this.isUploadingInProgress = value;
  }

  public setLoading(value: boolean): void {
    this.config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = value));
  }

  public getTranslationKey(key: string): string {
    return `${this.keysRoot}.${key}`;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeDialogData(): void {
    const dialogData = this.config.data;

    this.refundType = dialogData.refundType;
    this.actionType = dialogData.action;
    this.items = dialogData.items;

    switch (this.actionType) {
      case RefundActionType.authorize:
        this.keysRoot = 'REFUNDS.multipleAuthorizeDialog';
        this.reasonControl = new FormControl();
        this.confirmCheckboxControl = new FormControl();
        this.addControlToDialogForm('airlineRemark', this.reasonControl);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        break;

      case RefundActionType.reject:
        this.keysRoot = 'REFUNDS.multipleRejectDialog';
        this.reasonControl = new FormControl();
        this.confirmCheckboxControl = new FormControl();
        this.addControlToDialogForm('rejectionReason', this.reasonControl);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        break;

      case RefundActionType.delete:
        this.keysRoot = 'REFUNDS.multipleDeleteDialog';
        this.reasonControl = new FormControl();
        this.confirmCheckboxControl = new FormControl();
        this.addControlToDialogForm('deletionReason', this.reasonControl);
        break;

      case RefundActionType.comment:
        this.keysRoot = 'REFUNDS.multipleCommentDialog';
        this.reasonControl = new FormControl();
        this.addControlToDialogForm('reason', this.reasonControl);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        break;

      case RefundActionType.supervise:
        this.keysRoot = 'REFUNDS.multipleSuperviseDialog';
        this.reasonControl = new FormControl();
        this.confirmCheckboxControl = new FormControl();
        this.addControlToDialogForm('reasonForRefund', this.reasonControl);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        break;

      default:
        throw new Error('Action not included');
    }

    dialogData.title = this.translationService.translate(`${this.keysRoot}.${this.refundType}.title`);
  }

  private addControlToDialogForm(name: string, control: FormControl): void {
    this.dialogForm.addControl(name, control);
  }

  private setSubmitButtonState(value: boolean): void {
    const submitButton: ModalAction = this.config.data.buttons.find(
      (btn: ModalAction) => btn.type !== FooterButton.Cancel
    );
    submitButton.isDisabled = !value;
  }

  private initializeBspId(): void {
    // Bulk actions can only be performed, at least for now, when filtering by one BSP, so we can obtain that BSP from the filtered items
    // In the future, bulk actions will be supported for multiple BSPs, so this functionality and `bspId` won't be necessary
    const agent = this.items[0].agent as Agent;
    this.bspId = agent.bsp.id;
  }

  private initializePillsSection(): void {
    this.itemsTooltip = this.buildItemsTooltip(this.items);
    this.refundPills = this.buildPills(this.items);

    if (this.items.length > this.pillsLimit) {
      this.remainingPill = { value: `+${this.items.length - this.pillsLimit}` };
    }
  }

  private buildItemsTooltip(items: any): string {
    return items.reduce(
      (acc: string, item: any) =>
        acc + `${this.getTranslatedRefundDetail(item.ticketDocumentNumber || item.data.bsplinkDocument)}\n`,
      ''
    );
  }

  private buildPills(items: any): Array<{ value: string }> {
    return items.slice(0, this.pillsLimit).map((item: any) => ({
      value: this.getTranslatedRefundDetail(item.ticketDocumentNumber || item.data.bsplinkDocument)
    }));
  }

  private getTranslatedRefundDetail(documentNumber: string): string {
    return this.translationService.translate(this.getTranslationKey(`${this.refundType}.detail`), {
      documentNumber
    });
  }

  private initializeConfirmSection(): void {
    if (this.confirmCheckboxControl) {
      this.initializeConfirmCheckboxListener();
    } else {
      this.setSubmitButtonState(true);
    }
  }

  private initializeConfirmCheckboxListener(): void {
    this.confirmCheckboxControl.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => this.setSubmitButtonState(value));
  }

  private initializeSubmitListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        tap(() => this.validateControls()),
        skipWhile(
          event => event.clickedBtn === FooterButton.Cancel || this.isUploadingInProgress || this.dialogForm.invalid
        ),
        tap(() => (this.isUploadingInProgress = true)),
        first(),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.onSubmit());
  }

  private validateControls(): void {
    if (this.dialogForm.invalid) {
      FormUtil.showControlState(this.dialogForm);
    }
  }

  private onSubmit(): void {
    if (this.uploadComponent && this.uploadComponent.hasFilesInPreviewMode) {
      // If there are pending files, they are uploaded before submit
      this.uploadFiles();
    } else {
      // If there is no uploadComponent or pending files in it, submit is managed here
      this.updateRefunds();
    }
  }

  private uploadFiles(): void {
    this.uploadComponent.uploadFiles().subscribe(
      allValidFilesAreUploaded => (allValidFilesAreUploaded ? this.updateRefunds() : this.initializeSubmitListener()),
      () => this.setLoading(false)
    );
  }

  private updateRefunds(): void {
    let update$: Observable<any>;

    switch (this.actionType) {
      case RefundActionType.authorize:
        update$ = this.refundService.bulkAuthorize(this.items, this.dialogForm.value, this.bspId);
        break;

      case RefundActionType.reject:
        update$ = this.refundService.bulkReject(this.items, this.dialogForm.value, this.bspId);
        break;

      case RefundActionType.delete:
        update$ = this.refundService.bulkDelete(this.items, this.dialogForm.value, this.bspId);
        break;

      case RefundActionType.comment:
        update$ = this.refundService.bulkComment(this.items, this.dialogForm.value, this.bspId);
        break;

      case RefundActionType.supervise:
        update$ = this.refundService.bulkSupervise(this.items, this.dialogForm.value, this.bspId);
        break;

      default:
        throw new Error('Action not included');
    }

    this.setLoading(true);

    update$.subscribe(
      response => this.onUpdateStatusSuccess(response),
      response => this.onUpdateStatusError(response)
    );
  }

  private onUpdateStatusSuccess(response: BulkUpdateResponse<{ id: number; bsplinkDocument: string }>[]): void {
    this.buildResponseDialog(response);
    this.isResponseMode = true;

    // Change Cancel button title and remove all other footer buttons
    this.config.data.buttons = ButtonsFooterFactory.getFooterButtonsByType(
      [{ type: FooterButton.Close, buttonDesign: ButtonDesign.Tertiary }],
      false
    );

    // We subscribe again to dialog action CLOSE button so we can close dialog on button click
    this.initializeCloseListener();
  }

  private buildResponseDialog(response: Array<BulkUpdateResponse<{ id: number; bsplinkDocument: string }>>): void {
    const [successRefunds, errorRefunds] = partition(response, { status: 200 });

    // Success section
    if (successRefunds.length) {
      this.itemsSuccessTooltip = this.buildItemsTooltip(successRefunds);
      this.refundSuccessPills = this.buildPills(successRefunds);

      if (successRefunds.length > this.pillsLimit) {
        this.remainingSuccessPill = { value: `+${successRefunds.length - this.pillsLimit}` };
      }
    }

    // Error section
    if (errorRefunds.length) {
      this.refundErrorPills = this.buildPills(errorRefunds);
      this.itemsErrorTooltip = errorRefunds.reduce(
        (acc, item) =>
          acc +
          `${this.getTranslatedRefundDetail(item.data.bsplinkDocument)} ${this.getTranslatedRefundErrorCause(
            item.status,
            item.error
          )}\n`,
        ''
      );

      if (errorRefunds.length > this.pillsLimit) {
        this.remainingErrorPill = { value: `+${errorRefunds.length - this.pillsLimit}` };
      }
    }
  }

  private getTranslatedRefundErrorCause(status: number, error: ResponseErrorBE): string {
    let message: string;

    if (status === 403) {
      message = 'Forbidden';
    } else if (status === 404) {
      message = 'Not Found';
    } else {
      message = error.messages && error.messages[0]?.message;
    }

    return this.translationService.translate(this.getTranslationKey('errorCause'), { message });
  }

  private onUpdateStatusError(response: ResponseErrorBE): void {
    this.isUploadingInProgress = false;
    this.setLoading(false);

    // We subscribe again to dialog action button so errors can be amended and form resent
    this.initializeSubmitListener();

    this.globalErrorHandlerService.handleError(response);
  }

  private initializeCloseListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        filter(event => event.clickedBtn === FooterButton.Close),
        first(),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.dialog.close();

        // Refreshing current query tab
        this.tabService.refreshCurrentTab();
      });
  }
}
