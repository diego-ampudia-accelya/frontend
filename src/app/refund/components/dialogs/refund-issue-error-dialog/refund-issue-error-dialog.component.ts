import { Component, OnInit } from '@angular/core';

import { MasterDialogData } from '~app/master-shared/models/master.model';
import { DialogConfig } from '~app/shared/components';
import { ResponseErrorBE, ResponseErrorMessageBE } from '~app/shared/models/error-response-be.model';

@Component({
  selector: 'bspl-refund-issue-error-dialog',
  templateUrl: './refund-issue-error-dialog.component.html',
  styleUrls: ['./refund-issue-error-dialog.component.scss']
})
export class RefundIssueErrorDialogComponent implements OnInit {
  public errorMessages: ResponseErrorMessageBE[];
  private dialogInjectedData: MasterDialogData;

  constructor(private config: DialogConfig) {}

  ngOnInit() {
    this.initializeDialogData();
  }

  private initializeDialogData() {
    this.dialogInjectedData = this.config.data as MasterDialogData;
    const rowData: ResponseErrorBE = this.dialogInjectedData.rowTableContent;
    this.errorMessages = rowData.messages;
  }
}
