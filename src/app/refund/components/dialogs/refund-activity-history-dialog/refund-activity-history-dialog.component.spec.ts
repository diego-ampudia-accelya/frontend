import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';
import FileSaver from 'file-saver';
import { of } from 'rxjs';

import { RefundActivityHistoryDialogComponent } from './refund-activity-history-dialog.component';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { NotificationsHeaderService } from '~app/notifications/services/notifications-header.service';
import { RefundHistoryService } from '~app/refund/services/refund-history.service';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';

const mockConfig = {
  data: {
    refundItem: { id: '12345' },
    footerButtonsType: FooterButton.Close,
    hasCancelButton: false,
    isClosable: false,
    refundType: MasterDataType.RefundApp,
    title: 'RA'
  }
};

const activatedRouteStub = {
  snapshot: {
    data: { refundType: MasterDataType.RefundApp }
  }
};

describe('RefundActivityHistoryDialogComponent', () => {
  let component: RefundActivityHistoryDialogComponent;
  let fixture: ComponentFixture<RefundActivityHistoryDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundActivityHistoryDialogComponent],
      imports: [HttpClientTestingModule],
      providers: [
        DialogService,
        RefundHistoryService,
        { provide: DialogConfig, useValue: mockConfig },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        ReactiveSubject,
        HttpClient,
        mockProvider(NotificationsHeaderService)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundActivityHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call service to get activity', fakeAsync(() => {
    let activities: Activity[];

    const mockActivities: Activity[] = [
      {
        attachments: [],
        author: null,
        creationDate: null,
        params: [{ name: '', value: '' }],
        type: ''
      }
    ];
    const activitySpy = spyOn(component.refundHistoryService, 'getActivities').and.returnValue(of(mockActivities));
    component.ngOnInit();
    tick();
    component.activities$.subscribe(value => (activities = value));

    expect(activitySpy).toHaveBeenCalled();
    expect(activities).toEqual(mockActivities);
  }));

  it('should call service to download file', fakeAsync(() => {
    const mockFile = {
      blob: 'blob',
      fileName: 'fileName'
    };

    const downloadFileSpy = spyOn(component.refundService, 'downloadFile').and.returnValue(of(mockFile));
    const fileSaverSpy = spyOn(FileSaver, 'saveAs');

    component.onDownloadFile('url');
    tick();

    expect(downloadFileSpy).toHaveBeenCalled();
    expect(fileSaverSpy).toHaveBeenCalledWith('blob', 'fileName');
  }));
});
