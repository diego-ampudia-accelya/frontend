import { Component, OnDestroy, OnInit } from '@angular/core';
import FileSaver from 'file-saver';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundBE } from '~app/refund/models/refund.model';
import { RefundHistoryService } from '~app/refund/services/refund-history.service';
import { RefundService } from '~app/refund/services/refund.service';
import { DialogConfig } from '~app/shared/components';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';

@Component({
  selector: 'bspl-refund-activity-history-dialog',
  templateUrl: './refund-activity-history-dialog.component.html',
  styleUrls: ['./refund-activity-history-dialog.component.scss'],
  providers: [RefundService]
})
export class RefundActivityHistoryDialogComponent implements OnInit, OnDestroy {
  public activities$: Observable<Activity[]>;
  private destroy$ = new Subject();
  private _loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public isLoading$: Observable<boolean> = this._loadingSubject.asObservable();

  constructor(
    private config: DialogConfig,
    private dialog: DialogService,
    private reactiveSubject: ReactiveSubject,
    public refundService: RefundService,
    public refundHistoryService: RefundHistoryService
  ) {}

  ngOnInit(): void {
    this.initializeButtonClickListener();
    this.initializeActivities();
  }

  private initializeActivities(): void {
    const refundItem: RefundBE = this.config.data.refundItem;
    const refundType: MasterDataType = this.config.data.refundType;

    this.activities$ = this.refundHistoryService
      .getActivities(refundItem.id, refundType)
      .pipe(finalize(() => this._loadingSubject.next(false)));
  }

  private initializeButtonClickListener(): void {
    this.reactiveSubject.asObservable.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.dialog.close();
    });
  }

  public onDownloadFile(fileUrl: string): void {
    this.refundService.downloadFile(fileUrl).subscribe(file => {
      FileSaver.saveAs(file.blob, file.fileName);
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
