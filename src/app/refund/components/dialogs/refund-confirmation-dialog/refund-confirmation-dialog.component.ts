import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ErrorHandler,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, first, map, takeUntil, tap } from 'rxjs/operators';
import { getActiveTab } from '~app/core/reducers';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { RefundReason } from '~app/refund/models/refund-reason.model';
import { CurrencyViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionType, RefundBE, RefundConfiguration, RefundFinal } from '~app/refund/models/refund.model';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundService } from '~app/refund/shared/helpers/refund.helper';
import { finalFlagAdapter } from '~app/refund/shared/helpers/view-be-adapter.helper';
import { RefundResubmissionActions } from '~app/refund/store';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { ROUTES } from '~app/shared/constants/routes';
import { AlertMessageType } from '~app/shared/enums';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Bsp } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService, TabService } from '~app/shared/services';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Component({
  selector: 'bspl-refund-confirmation-dialog',
  templateUrl: './refund-confirmation-dialog.component.html',
  styleUrls: ['./refund-confirmation-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class RefundConfirmationDialogComponent implements OnInit, OnDestroy {
  @ViewChild(UploadComponent) uploadComponent: UploadComponent;

  protected dialogInjectedData;

  public refundType: MasterDataType;
  public hasPredefinedReason: boolean;
  public actionType: RefundActionType;
  public actionTypeEnum = RefundActionType;

  public refundNumber;
  public ticketNumber;
  public appDate;
  public amount;
  public currency: CurrencyViewModel;
  public reasonRows = 10;
  public reasonLines = 45;
  public uploadMode = UploadModes.Preview;
  public uploadPath: string;
  public reasonForInvestigateControl: FormControl;
  public filesToUp: FileUpload[] = [];
  public keysRoot: string;

  public dialogForm: FormGroup;
  public reasonControl: FormControl;
  public predefinedReasonControl: FormControl;
  public finalControl: FormControl;
  public finalCheckboxControl: FormControl;

  public isFlagFinalVisible$: Observable<boolean>;

  public showConfirmationAlertMessage = false;
  public confirmationAlertMessageType: AlertMessageType;

  private get refund(): RefundBE {
    return this.dialogInjectedData.rowTableContent;
  }

  private refundService: RefundService;

  private dialogFormSubmitEmitter: Subject<Partial<RefundBE>>;
  private isUploadingInProgress = false;
  private destroy$ = new Subject();

  public reasonRefundOptions$: Observable<DropdownOption[]>;
  public bsp: Bsp;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private config: DialogConfig,
    private translationService: L10nTranslationService,
    private datePipe: DatePipe,
    private reactiveSubject: ReactiveSubject,
    private http: HttpClient,
    private dialog: DialogService,
    private notificationService: NotificationService,
    private tabService: TabService,
    private router: Router,
    private globalErrorHandlerService: ErrorHandler,
    private store: Store<AppState>,
    private cd: ChangeDetectorRef
  ) {
    this.initializeDialogData();
    this.refundService = createRefundService(this.http, this.refundType);
  }

  public ngOnInit() {
    this.uploadPath = `${appConfiguration.baseUploadPath}/refund-management/files`;
    this.initializeIssueClickListener();
    this.initializeFinalCheckboxListener();

    if (this.hasPredefinedReason) {
      this.initializeRAReason();
    }

    this.dialogForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => this.onFormStatusChange());
  }

  public onUploadIsInProgress(value: boolean) {
    this.isUploadingInProgress = value;
    this.updateDialogButtons();
  }

  public getTranslationKey(key: string): string {
    return `${this.keysRoot}.${key}`;
  }

  private sendWithFileIds() {
    this.onUploadIsInProgress(false);
    this.dialogFormSubmitEmitter.next(this.dialogForm.value);
  }

  private onUpdateStatusSuccess(): void {
    const successKey = this.getTranslationKey(`${this.refundType}.saveSuccess`);
    const message = this.translationService.translate(successKey, {
      docNumber: this.refundNumber
    });
    this.dialog.close();
    this.notificationService.showSuccess(message);
    if (this.actionType === RefundActionType.delete) {
      this.navigateQuery();
    } else {
      this.tabService.refreshCurrentTab();
    }
  }

  private navigateQuery() {
    const currentUrl =
      this.refundType === MasterDataType.RefundApp
        ? `${ROUTES.REFUNDS_APP_VIEW.url}/${this.dialogForm.get('id').value}`
        : `${ROUTES.REFUNDS_NOTICE_VIEW.url}/${this.dialogForm.get('id').value}`;

    if (this.router.url.match(currentUrl)) {
      const nextUrl =
        this.refundType === MasterDataType.RefundApp
          ? `${ROUTES.REFUNDS_APP_QUERY.url}/deleted`
          : `${ROUTES.REFUNDS_NOTICE_QUERY.url}/deleted`;
      this.tabService.closeCurrentTabAndNavigate(nextUrl);
    } else {
      this.tabService.refreshCurrentTab();
    }
  }

  private initializeRAReason() {
    this.reasonRefundOptions$ = this.refundService.getRAReasons(this.bsp.id);

    this.predefinedReasonControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((reasonValue: RefundReason) => {
      FormUtil.get<RefundBE>(this.dialogForm, 'reasonForRefund').setValue(reasonValue.description);
    });
  }

  private onUpdateStatusError(response) {
    this.onUploadIsInProgress(false);

    if (response.errorCode === 400) {
      const errorKey = this.getTranslationKey(`${this.refundType}.saveError`);
      const message = this.translationService.translate(errorKey, { docNumber: this.refundNumber });
      this.notificationService.showError(message);
      this.markFieldsWithErrors(response);
    } else {
      this.globalErrorHandlerService.handleError(response);
    }
  }

  private markFieldsWithErrors(response) {
    if (response.messages?.length) {
      response.messages.forEach(messageObj => {
        const { message, messageParams } = messageObj;

        if (message && messageParams?.length) {
          const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
          const invalidControl = this.dialogForm.get(controlName);

          if (invalidControl) {
            invalidControl.setErrors({
              backendError: { message }
            });
          }
        }
      });
    }
  }

  private notUploadCommentOnSubmit(): void {
    if (this.actionType === RefundActionType.startResubmission) {
      this.submitStartResubmissionDialog();
    } else {
      this.refundService.save(this.dialogForm.value).subscribe(
        () => this.onUpdateStatusSuccess(),
        err => this.onUpdateStatusError(err)
      );
    }
  }

  private onSubmit() {
    if (this.uploadComponent) {
      // Submit with uploadComponents is managed by parent component via emitter notification
      if (this.uploadComponent.hasFilesInPreviewMode) {
        this.uploadFiles();
      } else {
        this.sendWithFileIds();
      }
    } else {
      // If no uploadComponent, submit is managed here and depends by action
      this.notUploadCommentOnSubmit();
    }
  }

  private submitStartResubmissionDialog() {
    const resubmissionUrl = `${ROUTES.REFUNDS_APP_RESUBMISSION.url}/${this.refund.id}`;

    this.store.dispatch(
      RefundResubmissionActions.saveResubmissionRemark({
        refundNumber: this.refund.ticketDocumentNumber,
        resubmissionRemark: this.reasonControl.value
      })
    );
    this.dialog.close();

    this.isCurrentTabRAQuery().subscribe(isCurrentTabRAQuery => {
      if (isCurrentTabRAQuery) {
        this.router.navigateByUrl(resubmissionUrl);
      } else {
        this.tabService.closeCurrentTabAndNavigate(resubmissionUrl);
      }
    });
  }

  private initializeDialogData() {
    this.dialogInjectedData = this.config.data;
    this.refundType = this.dialogInjectedData.refundType;
    this.actionType = this.dialogInjectedData.action;
    this.dialogForm = this.dialogInjectedData.dialogForm;
    this.bsp = this.dialogInjectedData.bsp;

    this.isFlagFinalVisible$ = this.dialogInjectedData.bspRefundConfiguration$?.pipe(
      map(config => (config as RefundConfiguration).raFinal),
      takeUntil(this.destroy$)
    );

    this.buildFormByActionType(this.actionType);

    this.dialogInjectedData.title = this.translationService.translate(`${this.keysRoot}.${this.refundType}.title`);

    this.dialogFormSubmitEmitter = this.dialogInjectedData.dialogFormSubmitEmitter;

    const date = this.refundType === MasterDataType.RefundApp ? this.refund.dateOfApplication : this.refund.dateOfIssue;
    this.appDate = this.datePipe.transform(date || new Date(), 'dd/MM/yyyy');

    this.initializeDocNumber(this.refund);
    this.normalizeAmount(this.refund);
  }

  private buildFormByActionType(actionType: RefundActionType) {
    const issueReasonValidator = this.refundType === MasterDataType.RefundApp ? Validators.required : [];
    switch (actionType) {
      case RefundActionType.investigate:
        this.keysRoot = 'REFUNDS.investigateDialog';
        this.confirmationAlertMessageType = AlertMessageType.info;

        // Flags
        this.showConfirmationAlertMessage = this.refund.finalType === RefundFinal.Final;

        // Controls
        this.reasonControl = new FormControl(null);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        this.addControlToDialogForm('status', new FormControl(RefundStatus.investigated));
        this.addControlToDialogForm('airlineRemark', this.reasonControl);

        break;

      case RefundActionType.supervise: {
        this.keysRoot = 'REFUNDS.issueDialog';
        this.confirmationAlertMessageType = AlertMessageType.info;

        // Flags
        const status = this.refundType === MasterDataType.RefundApp ? RefundStatus.pending : RefundStatus.issued;
        this.hasPredefinedReason = this.refundType === MasterDataType.RefundApp;

        // Controls
        this.reasonControl = new FormControl(this.dialogInjectedData.reasonForRefund, issueReasonValidator);
        this.predefinedReasonControl = new FormControl(this.dialogInjectedData.predefinedReason);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        this.addControlToDialogForm('reasonForRefund', this.reasonControl);
        this.addControlToDialogForm('predefinedReason', this.predefinedReasonControl);
        this.addControlToDialogForm('status', new FormControl(status));

        // Final control
        if (this.refundType === MasterDataType.RefundApp) {
          this.finalCheckboxControl = new FormControl();
          this.finalControl = new FormControl();
          this.addControlToDialogForm('finalType', this.finalControl);
        }

        break;
      }
      case RefundActionType.authorize:
        this.keysRoot = 'REFUNDS.authorizeDialog';

        // Controls
        this.reasonControl = new FormControl(null);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        this.addControlToDialogForm('status', new FormControl(RefundStatus.authorized));
        this.addControlToDialogForm('airlineRemark', this.reasonControl);

        break;

      case RefundActionType.applyChanges:
        this.keysRoot = 'REFUNDS.applyChanges';

        // Flags
        this.hasPredefinedReason = this.refundType === MasterDataType.RefundApp;

        // Controls
        this.reasonControl = new FormControl(this.dialogInjectedData.reasonForRefund, issueReasonValidator);
        this.predefinedReasonControl = new FormControl(this.dialogInjectedData.predefinedReason);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        this.addControlToDialogForm('reasonForRefund', this.reasonControl);
        this.addControlToDialogForm('predefinedReason', this.predefinedReasonControl);

        break;

      case RefundActionType.issue:
        this.keysRoot = 'REFUNDS.issueDialog';
        this.confirmationAlertMessageType = AlertMessageType.info;

        // Flags
        this.hasPredefinedReason = this.refundType === MasterDataType.RefundApp;

        // Controls
        this.reasonControl = new FormControl(this.dialogInjectedData.reasonForRefund, issueReasonValidator);
        this.predefinedReasonControl = new FormControl(this.dialogInjectedData.predefinedReason);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        this.addControlToDialogForm('reasonForRefund', this.reasonControl);
        this.addControlToDialogForm('predefinedReason', this.predefinedReasonControl);

        // Final control
        if (this.refundType === MasterDataType.RefundApp) {
          this.finalCheckboxControl = new FormControl();
          this.finalControl = new FormControl();
          this.addControlToDialogForm('finalType', this.finalControl);
        }

        break;

      case RefundActionType.issuePendingSupervision:
        this.keysRoot = 'REFUNDS.issuePendingDialog';

        // Flags
        this.hasPredefinedReason = this.refundType === MasterDataType.RefundApp;

        // Controls
        this.reasonControl = new FormControl(this.dialogInjectedData.reasonForRefund, issueReasonValidator);
        this.predefinedReasonControl = new FormControl(this.dialogInjectedData.predefinedReason);
        this.addControlToDialogForm('attachmentIds', new FormControl([]));
        this.addControlToDialogForm('status', new FormControl(RefundStatus.pendingSupervision));
        this.addControlToDialogForm('reasonForRefund', this.reasonControl);
        this.addControlToDialogForm('predefinedReason', this.predefinedReasonControl);

        break;

      case RefundActionType.delete:
        this.keysRoot = 'REFUNDS.deleteDialog';
        this.confirmationAlertMessageType = AlertMessageType.warning;

        // Flags
        this.showConfirmationAlertMessage = true;

        // Controls
        this.reasonControl = new FormControl(null);
        this.addControlToDialogForm('id', new FormControl(this.refund.id));
        this.addControlToDialogForm('status', new FormControl(RefundStatus.deleted));
        this.addControlToDialogForm('deletionReason', this.reasonControl);

        break;

      case RefundActionType.startResubmission:
        this.keysRoot = 'REFUNDS.startResubmission';
        this.refundNumber = this.refund.ticketDocumentNumber;

        // Controls
        this.reasonControl = new FormControl(null, Validators.required);
        this.addControlToDialogForm('resubmissionRemark', this.reasonControl);

        break;

      case RefundActionType.resubmit:
        this.keysRoot = 'REFUNDS.resubmission';

        // Controls
        this.reasonControl = new FormControl(this.dialogInjectedData.reasonForRefund, issueReasonValidator);
        this.predefinedReasonControl = new FormControl(this.dialogInjectedData.predefinedReason);
        this.addControlToDialogForm('reasonForRefund', this.reasonControl);
        this.addControlToDialogForm('predefinedReason', this.predefinedReasonControl);
        this.addControlToDialogForm('id', new FormControl(null));
        this.addControlToDialogForm('status', new FormControl(RefundStatus.resubmitted));
        this.addControlToDialogForm('rejectedDocumentId' as any, new FormControl(this.refund.id));
        this.addControlToDialogForm('attachmentIds', new FormControl([]));

        break;

      default:
        throw new Error('Action not included');
    }
  }

  private initializeDocNumber(refund: RefundBE) {
    if (
      this.actionType === RefundActionType.issue ||
      this.actionType === RefundActionType.issuePendingSupervision ||
      this.actionType === RefundActionType.applyChanges ||
      this.actionType === RefundActionType.resubmit
    ) {
      this.refundNumber = refund.relatedTicketDocument.documentNumber;
      if (this.actionType !== RefundActionType.resubmit) {
        this.ticketNumber = refund.ticketDocumentNumber;
      }
    } else {
      this.refundNumber = refund.ticketDocumentNumber;
    }
  }

  private normalizeAmount(refund: RefundBE) {
    this.currency = refund.currency as CurrencyViewModel;

    if (this.currency) {
      const decimals = this.currency.decimals;
      const amountValue = refund.totalAmount;
      this.amount = NumberHelper.roundString(amountValue, decimals);
    }
  }

  private addControlToDialogForm(name: keyof RefundBE, control: FormControl): void {
    this.dialogForm.addControl(name, control);
  }

  private initializeIssueClickListener() {
    this.reactiveSubject.asObservable
      .pipe(
        tap(() => this.validateControls()),
        filter(
          event => event.clickedBtn !== FooterButton.Cancel && !this.isUploadingInProgress && this.dialogForm.valid
        ),
        tap(() => this.onUploadIsInProgress(true)),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.onSubmit());
  }

  private initializeFinalCheckboxListener() {
    this.finalCheckboxControl?.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(final => this.finalControl.setValue(finalFlagAdapter(final)));
  }

  private uploadFiles() {
    this.uploadComponent
      .uploadFiles()
      .pipe(
        filter(allValidFilesAreUploaded => allValidFilesAreUploaded),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.sendWithFileIds());
  }

  private validateControls() {
    if (this.dialogForm.invalid) {
      FormUtil.showControlState(this.dialogForm);
    }
  }

  private isCurrentTabRAQuery(): Observable<boolean> {
    return this.store.pipe(
      select(getActiveTab),
      first(),
      map(currentTab => currentTab.url.includes(ROUTES.REFUNDS_APP_QUERY.url))
    );
  }

  private onFormStatusChange(): void {
    this.updateDialogButtons();
    this.cd.markForCheck();
  }

  private updateDialogButtons(): void {
    this.dialogInjectedData.buttons.forEach((button: ModalAction) => {
      if (button.type === FooterButton.Cancel) {
        button.isDisabled = this.isUploadingInProgress;
      } else {
        button.isDisabled = this.isUploadingInProgress || this.dialogForm.invalid;
      }
    });
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
