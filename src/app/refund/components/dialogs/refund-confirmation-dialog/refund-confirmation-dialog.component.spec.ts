import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync } from '@angular/core/testing';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, mockProvider, Spectator, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { NEVER, of, Subject } from 'rxjs';
import { SendToDpcType } from '~app/adm-acm/models/adm-acm-issue-shared-aux.model';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { CurrencyViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionType, RefundBE, RefundFinal } from '~app/refund/models/refund.model';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { NotificationService, TabService } from '~app/shared/services';
import { SharedModule } from '~app/shared/shared.module';
import { RefundConfirmationDialogComponent } from './refund-confirmation-dialog.component';

const mockRefund: RefundBE = {
  id: 12345,
  agent: { id: 1 },
  agentAddress: null,
  agentContact: null,
  airline: { id: 1 },
  airlineAddress: null,
  airlineCodeRelatedDocument: '001',
  airlineContact: null,
  cardPayments: [],
  currency: { id: 1 },
  dateOfIssueRelatedDocument: '2020/06/29',
  finalType: null,
  finalDaysLeft: null,
  finalProcessStopped: null,
  netReporting: false,
  originalIssue: { airlineCode: null, agentCode: null, documentNumber: null, issueDate: null, locationCode: null },
  partialRefund: false,
  passenger: 'test',
  reasonForRefund: 'test reason',
  relatedTicketConjunctions: [],
  relatedTicketDocument: {
    documentNumber: '0011231231231',
    coupon1: false,
    coupon2: false,
    coupon3: false,
    coupon4: false
  },
  relatedTicketExchange: false,
  resubmissionRemark: '',
  sentToDpc: SendToDpcType.pending,
  status: RefundStatus.pending,
  taxMiscellaneousFees: [],
  ticketDocumentNumber: '12345'
};

const mockConfig = {
  data: {
    title: 'title',
    action: RefundActionType.issue,
    footerButtonsType: FooterButton.Issue,
    dialogForm: new FormGroup({}),
    rowTableContent: mockRefund,
    refundType: MasterDataType.RefundApp,
    activatedRoute: { data: { refundType: MasterDataType.RefundApp } },
    dialogFormSubmitEmitter: new Subject<Partial<RefundBE>>(),
    bspRefundConfiguration$: NEVER,
    bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain' },
    predefinedReason: 'test'
  }
};

const initialState = {
  auth: {
    user: {}
  },
  core: {
    menu: {
      tabs: {}
    }
  }
};

// TODO REVIEW: some of these tests are throwing errors related with routing configuration
xdescribe('RefundConfirmationDialogComponent', () => {
  let spectator: Spectator<RefundConfirmationDialogComponent>;

  const createComponent = createComponentFactory({
    component: RefundConfirmationDialogComponent,
    imports: [
      SharedModule,
      HttpClientTestingModule,
      L10nTranslationModule.forRoot(l10nConfig),
      RouterTestingModule.withRoutes([])
    ],
    declarations: [],
    providers: [
      DatePipe,
      ReactiveSubject,
      ErrorHandler,
      provideMockStore({ initialState }),
      mockProvider(DialogConfig, mockConfig)
    ],
    mocks: [NotificationService, DialogService, TabService, MessageService, RouterTestingModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
  });

  it('should create component', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    expect(spectator.component).toBeTruthy();
  });

  it('onUploadIsInProgress - should change isUploadingInProgress to true', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    expect(spectator.component['isUploadingInProgress']).toBeFalsy();

    spectator.component.onUploadIsInProgress(true);

    expect(spectator.component['isUploadingInProgress']).toBeTruthy();
  });

  it('getTranslationKey - return a path when is a issue type', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const key = 'saveSuccess';
    expect(spectator.component.getTranslationKey(key)).toBe('REFUNDS.issueDialog.saveSuccess');
  });

  it('sendWithFileIds - should emit event when sendWithFields', fakeAsync(() => {
    spectator = createComponent();
    spectator.component['dialogFormSubmitEmitter'] = new Subject<Partial<RefundBE>>();

    let result;
    spectator.component['dialogFormSubmitEmitter'].subscribe(value => (result = value));

    spectator.component['sendWithFileIds']();

    spectator.tick();
    expect(result).not.toBe(null);
  }));

  it('onUpdateStatusSuccess - should onUpdateStatusSuccess refreshCurrentTab', fakeAsync(() => {
    mockConfig.data.action = RefundActionType.delete;
    mockConfig.data.activatedRoute.data.refundType = MasterDataType.RefundNotice;

    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const dialogSpy = spectator.inject(DialogService).close.and.callThrough();
    const notificationSpy = spectator.inject(NotificationService).showSuccess.and.callThrough();
    const tabServiceSpy = spectator.inject(TabService).refreshCurrentTab.and.callThrough();

    spectator.component['onUpdateStatusSuccess']();
    spectator.tick();

    expect(dialogSpy).toHaveBeenCalled();
    expect(notificationSpy).toHaveBeenCalled();
    expect(tabServiceSpy).toHaveBeenCalled();
  }));

  it('onUpdateStatusSuccess - should onUpdateStatusSuccess navigateQuery', fakeAsync(() => {
    mockConfig.data.action = RefundActionType.delete;

    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const dialogSpy = spectator.inject(DialogService).close.and.callThrough();
    const notificationSpy = spectator.inject(NotificationService).showSuccess.and.callThrough();
    const navigateQuerySpy = spyOn<any>(spectator.component, 'navigateQuery').and.callThrough();

    spectator.component['onUpdateStatusSuccess']();
    spectator.tick();

    expect(dialogSpy).toHaveBeenCalled();
    expect(notificationSpy).toHaveBeenCalled();
    expect(navigateQuerySpy).toHaveBeenCalled();
  }));

  it('onUpdateStatusError - should call handleError with other errorCode', fakeAsync(() => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const initializeIssueClickListenerSpy = spyOn<any>(
      spectator.component,
      'initializeIssueClickListener'
    ).and.callThrough();
    const makeFieldsWithErrorSpy = spyOn<any>(spectator.component, 'markFieldsWithErrors').and.callThrough();
    const notificationSpy = spectator.inject(NotificationService).showError.and.callThrough();
    const globalHandlerSpy = spyOn(spectator.component['globalErrorHandlerService'], 'handleError').and.callThrough();

    spectator.component.refundType = MasterDataType.RefundApp;
    spectator.component.refundNumber = 12345;

    spectator.component['onUpdateStatusError']({ errorCode: 500 });
    spectator.tick();
    expect(initializeIssueClickListenerSpy).toHaveBeenCalledTimes(1);
    expect(notificationSpy).toHaveBeenCalledTimes(0);
    expect(makeFieldsWithErrorSpy).toHaveBeenCalledTimes(0);
    expect(globalHandlerSpy).toHaveBeenCalledTimes(1);
  }));

  it('onUpdateStatusError - should onUpdateStatusError call markFieldsWithErrors errorCode 400', fakeAsync(() => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const initializeIssueClickListenerSpy = spyOn<any>(
      spectator.component,
      'initializeIssueClickListener'
    ).and.callThrough();
    const makeFieldsWithErrorSpy = spyOn<any>(spectator.component, 'markFieldsWithErrors').and.callThrough();
    const notificationSpy = spectator.inject(NotificationService).showError.and.callThrough();

    spectator.component.refundType = MasterDataType.RefundApp;
    spectator.component.refundNumber = 12345;

    spectator.component['onUpdateStatusError']({ errorCode: 400 });
    spectator.tick();
    expect(initializeIssueClickListenerSpy).toHaveBeenCalledTimes(1);
    expect(notificationSpy).toHaveBeenCalledTimes(1);
    expect(makeFieldsWithErrorSpy).toHaveBeenCalledTimes(1);
  }));

  it('should markFieldsWithErrors set an error', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const errorMessage = 'Invalid reason';
    const response = {
      messages: [
        {
          message: errorMessage,
          messageParams: [{ name: 'fieldName', value: 'reasonForRefund' }]
        }
      ]
    };
    spectator.component['markFieldsWithErrors'](response);

    expect(spectator.component.dialogForm.get('reasonForRefund').errors).not.toBeNull();
    expect(spectator.component.dialogForm.get('reasonForRefund').errors.backendError.message).toBe(errorMessage);
  });

  describe('onSubmit', () => {
    beforeEach(() => {
      mockConfig.data.action = RefundActionType.issue;
      spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });
    });

    it('should not issue if uploading is in progress', () => {
      const saveSpy = spyOn(spectator.component['refundService'], 'save').and.callThrough();

      spectator.component.onUploadIsInProgress(true);
      spectator.component.dialogForm.patchValue({ reasonForRefund: 'Test refund reason' });
      spectator.component['reactiveSubject'].emit({ clickedBtn: FooterButton.Issue });

      expect(saveSpy).toHaveBeenCalledTimes(0);
    });

    it('Not uploadComponent and is not a startResubmission ', () => {
      spectator.component.uploadComponent = null;
      const onUpdateStatusSuccessSpy = spyOn<any>(spectator.component, 'onUpdateStatusSuccess').and.callThrough();
      spyOn(spectator.component['refundService'], 'save').and.returnValue(of('/'));
      spectator.component['onSubmit']();

      expect(onUpdateStatusSuccessSpy).toHaveBeenCalledTimes(1);
    });

    it('uploadComponent not hasFilesInPreviewMode ', () => {
      spectator.component['dialogFormSubmitEmitter'] = new Subject<Partial<RefundBE>>();
      const sendWithFileIdsSpy = spyOn<any>(spectator.component, 'sendWithFileIds').and.callThrough();

      spectator.component.dialogForm.get('attachmentIds').setValue(['324']);
      spectator.detectChanges();

      spectator.component['onSubmit']();

      expect(spectator.component.uploadComponent).toBeDefined();
      expect(sendWithFileIdsSpy).toHaveBeenCalled();
    });

    it('uploadComponent hasFilesInPreviewMode', () => {
      const uploadFilesSpy = spyOn<any>(spectator.component, 'uploadFiles').and.callThrough();

      spectator.component.dialogForm.get('attachmentIds').setValue(['324']);

      spectator.component.uploadMode = UploadModes.Preview;
      spyOnProperty(spectator.component.uploadComponent, 'hasFilesInPreviewMode', 'get').and.returnValue(true);

      spectator.component['onSubmit']();

      expect(spectator.component.uploadComponent).toBeDefined();
      expect(uploadFilesSpy).toHaveBeenCalled();
    });
  });

  it(' onSubmit - Not uploadComponent and is a startResubmission ', () => {
    mockConfig.data.action = RefundActionType.startResubmission;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });
    spectator.component.uploadComponent = null;
    const submitStartResubmissionDialogSpy = spyOn<any>(
      spectator.component,
      'submitStartResubmissionDialog'
    ).and.callThrough();

    spectator.component['onSubmit']();

    expect(submitStartResubmissionDialogSpy).toHaveBeenCalledTimes(1);
  });

  describe('submitStartResubmissionDialog', () => {
    let dialogSpy: jasmine.Spy;
    let routerSpy: jasmine.Spy;
    let storeSpy: jasmine.Spy;
    let store: SpyObject<Store<any>>;
    let router: SpyObject<Router>;

    beforeEach(() => {
      mockConfig.data.action = RefundActionType.issue;
      spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });
      dialogSpy = spectator.inject(DialogService).close.and.callThrough();
      store = spectator.inject(Store);
      router = spectator.inject(Router);
      routerSpy = spyOn(router, 'navigateByUrl').and.callThrough();
      storeSpy = spyOn(store, 'dispatch').and.callThrough();
    });

    it('navigateByUrl to resubmissionURL', () => {
      spyOn<any>(spectator.component, 'isCurrentTabRAQuery').and.returnValue(of(true));

      spectator.component['submitStartResubmissionDialog']();

      expect(dialogSpy).toHaveBeenCalled();
      expect(storeSpy).toHaveBeenCalled();
      expect(routerSpy).toHaveBeenCalled();
    });

    it('close tab and navigate', () => {
      spyOn<any>(spectator.component, 'isCurrentTabRAQuery').and.returnValue(of(false));
      const tabSpy = spectator.inject(TabService).closeCurrentTabAndNavigate.and.callThrough();

      spectator.component['submitStartResubmissionDialog']();

      expect(dialogSpy).toHaveBeenCalled();
      expect(storeSpy).toHaveBeenCalled();
      expect(tabSpy).toHaveBeenCalled();
      expect(routerSpy).not.toHaveBeenCalled();
    });
  });

  it('initializeDialogData - initialize data', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });
    const buildFormByActionTypeSpy = spyOn<any>(spectator.component, 'buildFormByActionType').and.callThrough();
    const initializeDocNumberSpy = spyOn<any>(spectator.component, 'initializeDocNumber').and.callThrough();
    const normalizeAmountSpy = spyOn<any>(spectator.component, 'normalizeAmount').and.callThrough();

    spectator.component['initializeDialogData']();

    expect(spectator.component.refundType).not.toBeEmpty();
    expect(buildFormByActionTypeSpy).toHaveBeenCalled();
    expect(initializeDocNumberSpy).toHaveBeenCalled();
    expect(normalizeAmountSpy).toHaveBeenCalled();
  });

  //* buildFormByActionType

  describe('buildFormByActionType', () => {
    beforeEach(() => {
      mockConfig.data.action = RefundActionType.issue;
      spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });
    });

    it('should create  Refund Investigate', () => {
      spectator.component['buildFormByActionType'](RefundActionType.investigate);

      expect(spectator.component.keysRoot).toBe('REFUNDS.investigateDialog');
    });

    it('should create  Refund Supervision', () => {
      spectator.component['buildFormByActionType'](RefundActionType.supervise);
      expect(spectator.component.keysRoot).toBe('REFUNDS.issueDialog');
    });

    it('should create  Refund Authorize', () => {
      spectator.component['buildFormByActionType'](RefundActionType.authorize);

      expect(spectator.component.keysRoot).toBe('REFUNDS.authorizeDialog');
    });

    it('should create  Refund Apply Changes', () => {
      spectator.component['buildFormByActionType'](RefundActionType.applyChanges);

      expect(spectator.component.keysRoot).toBe('REFUNDS.applyChanges');
    });

    it('should create  Refund Delete', () => {
      spectator.component['buildFormByActionType'](RefundActionType.delete);

      expect(spectator.component.keysRoot).toBe('REFUNDS.deleteDialog');
    });

    it('should create  Refund Issue Pending Supervision', () => {
      spectator.component['buildFormByActionType'](RefundActionType.issuePendingSupervision);

      expect(spectator.component.keysRoot).toBe('REFUNDS.issuePendingDialog');
    });

    it('should create  Refund Resubmission', () => {
      spectator.component['buildFormByActionType'](RefundActionType.startResubmission);

      expect(spectator.component.keysRoot).toBe('REFUNDS.startResubmission');
    });

    it('should create  Refund Resubmit', () => {
      spectator.component['buildFormByActionType'](RefundActionType.resubmit);

      expect(spectator.component.keysRoot).toBe('REFUNDS.resubmission');
    });

    it('should raise error exception', () => {
      expect(() => spectator.component['buildFormByActionType'](RefundActionType.edit)).toThrowError(
        'Action not included'
      );
    });
  });

  it('initializeDocNumber - should call initializeDocNumber with delete type', fakeAsync(() => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    spectator.component.actionType = RefundActionType.delete;

    spectator.component['initializeDocNumber'](mockRefund);

    spectator.tick();
    expect(spectator.component.refundNumber).toBe(mockRefund.ticketDocumentNumber);
  }));

  it('initializeDocNumber - should call initializeDocNumber with resubmit type', fakeAsync(() => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    spectator.component.actionType = RefundActionType.resubmit;

    spectator.component['initializeDocNumber'](mockRefund);

    spectator.tick();
    expect(spectator.component.refundNumber).toBe(mockRefund.relatedTicketDocument.documentNumber);
    expect(spectator.component.ticketNumber).toBe(mockRefund.ticketDocumentNumber);
  }));

  it('normalizeAmount - normalize amount and set value', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const refund: RefundBE = {
      ...mockRefund,
      currency: { id: 1, decimals: 2, code: 'TST', isDefault: false } as CurrencyViewModel,
      totalAmount: 35.6589
    };

    spectator.component['normalizeAmount'](refund);

    expect(spectator.component.amount).toBe('35.66');
  });

  it('normalizeAmount - amount do not have value', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    const refund: RefundBE = {
      ...mockRefund,
      currency: null,
      totalAmount: 35.6589
    };

    spectator.component['normalizeAmount'](refund);

    expect(spectator.component.amount).toBe('NaN');
  });

  it('addControlToDialogForm - add control to dialog form correctly', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });
    const key = 'grossFare';
    const control = new FormControl(null);

    expect(spectator.component.dialogForm.get('grossFare')).toBeNull();

    spectator.component['addControlToDialogForm'](key, control);

    expect(spectator.component.dialogForm.get('grossFare')).toBeDefined();
  });

  describe('initializeIssueClickListener ', () => {
    let saveSpy: jasmine.Spy;

    beforeEach(() => {
      mockConfig.data.action = RefundActionType.issue;
      spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });
      saveSpy = spyOn(spectator.component['refundService'], 'save').and.callThrough();
    });

    it('should not issue if form is invalid', () => {
      spectator.component.dialogForm.patchValue({ reasonForRefund: null });

      spectator.component['reactiveSubject'].emit({ clickedBtn: FooterButton.Issue });

      expect(saveSpy).toHaveBeenCalledTimes(0);
    });

    it('should not issue if isUploadingInProgress is true', () => {
      spectator.component['isUploadingInProgress'] = true;

      spectator.component.dialogForm.patchValue({ reasonForRefund: 'Test' });
      spectator.component['reactiveSubject'].emit({ clickedBtn: FooterButton.Issue });

      expect(saveSpy).toHaveBeenCalledTimes(0);
    });

    it('should not issue if cancel button is clicked', () => {
      spectator.component.dialogForm.patchValue({ reasonForRefund: 'Test' });
      spectator.component['reactiveSubject'].emit({ clickedBtn: FooterButton.Cancel });

      expect(saveSpy).toHaveBeenCalledTimes(0);
    });
  });

  it('initializeFinalCheckboxListener', () => {
    mockConfig.data.action = RefundActionType.issue;
    spectator = createComponent({ providers: [mockProvider(DialogConfig, mockConfig)] });

    spectator.component.finalCheckboxControl.setValue(true);

    expect(spectator.component.finalControl.value).toBe(RefundFinal.Final);
    expect(spectator.component.finalCheckboxControl.value).toBe(true);
  });
});
