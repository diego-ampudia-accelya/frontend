import { Component, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { MasterDialogData } from '~app/master-shared/models/master.model';
import { DialogConfig } from '~app/shared/components';

@Component({
  selector: 'bspl-refund-discard-dialog',
  templateUrl: './refund-discard-dialog.component.html',
  styleUrls: ['./refund-discard-dialog.component.scss']
})
export class RefundDiscardDialogComponent implements OnInit {
  private dialogInjectedData: MasterDialogData;

  constructor(private config: DialogConfig, private translationService: L10nTranslationService) {}

  ngOnInit() {
    this.initializeDialogData();
  }

  private initializeDialogData() {
    this.dialogInjectedData = this.config.data as MasterDialogData;
    this.dialogInjectedData.title = this.translationService.translate(
      'REFUNDS.issueDialog.refund-dialog-discard.title'
    );
  }
}
