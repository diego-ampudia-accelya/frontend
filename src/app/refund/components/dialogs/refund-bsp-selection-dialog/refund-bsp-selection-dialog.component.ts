import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DialogConfig, FooterButton } from '~app/shared/components';
import { toValueLabelObjectBsp } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-refund-bsp-selection-dialog',
  templateUrl: './refund-bsp-selection-dialog.component.html',
  styleUrls: ['./refund-bsp-selection-dialog.component.scss']
})
export class RefundBspSelectionDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public bspDropdownOptions: DropdownOption<BspDto>[];

  private destroy$ = new Subject();

  constructor(private config: DialogConfig, private formBuilder: FormBuilder) {
    this.initializeDialogData();
  }

  public ngOnInit(): void {
    this.buildForm();
    this.initializeFormListener();
  }

  private initializeDialogData(): void {
    const bsps: BspDto[] = this.config.data.bsps;
    this.bspDropdownOptions = bsps.map(toValueLabelObjectBsp);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      bsp: [null, Validators.required]
    });
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const requestButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Request
      );
      requestButton.isDisabled = status === 'INVALID';
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
