import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Message } from 'primeng/api';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { MasterDataType, MasterDialogData } from '~app/master-shared/models/master.model';
import { RefundStatus } from '~app/refund/models/refund-be-aux.model';
import { RefundReason } from '~app/refund/models/refund-reason.model';
import { AgentViewModel, CurrencyViewModel, RejectViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundBE } from '~app/refund/models/refund.model';
import { RefundService } from '~app/refund/services/refund.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { UploadModes, UploadStatus, UploadType } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { appConfiguration, NotificationService, TabService } from '~app/shared/services';

@Component({
  selector: 'bspl-refund-reject-dialog',
  templateUrl: './refund-reject-dialog.component.html',
  styleUrls: ['./refund-reject-dialog.component.scss'],
  providers: [DatePipe]
})
export class RefundRejectDialogComponent implements OnInit, OnDestroy {
  @ViewChild(UploadComponent, { static: true }) uploadComponentRef: UploadComponent;

  public form: FormGroup;

  public refundType: MasterDataType;
  public docNumber = '';
  public appDate = '';
  public amount = '';
  public currency: CurrencyViewModel = null;
  public reasonOptions: DropdownOption<RefundReason>[] = [];

  public uploadPath: string;
  public uploadMode = UploadModes.Preview;
  public filesToUp: FileUpload[] = [];

  protected dialogInjectedData: MasterDialogData;

  private itemRetrieved: RefundBE;
  private refundService: RefundService;
  private formFactory: FormUtil;

  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private http: HttpClient,
    private config: DialogConfig,
    private dialog: DialogService,
    private translationService: L10nTranslationService,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private reactiveSubject: ReactiveSubject,
    private notificationService: NotificationService,
    private tabService: TabService,
    private globalErrorHandlerService: ErrorHandler
  ) {
    this.refundService = new RefundService(this.http, null, this.config.data.activatedRoute);
    this.formFactory = new FormUtil(this.fb);
  }

  ngOnInit() {
    this.uploadPath = `${appConfiguration.baseUploadPath}/refund-management/files`;
    this.initializeDialogData();
    this.initializeReasonsDropdown();
    this.createForm();
    this.subscribeToReject();
    this.initializeReasonSelectedListener();
  }

  public onFilesChange(files: FileUpload[]) {
    this.filesToUp = files;
  }

  public onUploadStarted(event) {
    if (event === UploadType.Retry) {
      this.uploadComponentRef.resetToInitialMode();
    }
  }

  public onUploadFinished(files: FileUpload[]) {
    if (!Array.isArray(files) || !files.length) {
      return;
    }

    files.forEach((file, index) => {
      this.filesToUp[index] = { ...this.filesToUp[index], ...file, isValid: !file.error };
    });

    if (this.filesToUp.every(file => file.isValid)) {
      this.sendWithFileIds();
    }
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }

  private initializeDialogData() {
    this.dialogInjectedData = this.config.data as MasterDialogData;
    this.itemRetrieved = this.dialogInjectedData.rowTableContent;
    this.refundType = this.dialogInjectedData.activatedRoute.data.refundType;
    this.dialogInjectedData.title = this.translationService.translate(`REFUNDS.rejectDialog.${this.refundType}.title`);
    this.docNumber = this.itemRetrieved.ticketDocumentNumber;
    this.appDate = this.datePipe.transform(this.itemRetrieved.dateOfApplication, 'yyyy-MM-dd');

    this.normalizeAmount();
  }

  private normalizeAmount() {
    this.currency = this.itemRetrieved.currency as CurrencyViewModel;
    if (this.currency) {
      const decimals = this.currency.decimals;
      this.amount = NumberHelper.roundString(this.itemRetrieved.totalAmount, decimals);
    }
  }

  private initializeReasonsDropdown() {
    const agent = this.itemRetrieved.agent as AgentViewModel;
    this.refundService.getRejectRAReasons(agent.bsp.id).subscribe(a => (this.reasonOptions = a));
  }

  private createForm() {
    this.form = this.formFactory.createGroup<RejectViewModel>({
      id: [this.itemRetrieved.id, []],
      status: [RefundStatus.rejected, []],
      reason: null,
      rejectionReason: [null, [Validators.required]],
      attachmentIds: null,
      airlineRemark: [this.itemRetrieved.airlineRemark]
    });
  }

  private initializeReasonSelectedListener() {
    const reasonCtrl = FormUtil.get<RejectViewModel>(this.form, 'reason');

    reasonCtrl.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.destroy$)).subscribe((value: RefundReason) => {
      FormUtil.get<RejectViewModel>(this.form, 'rejectionReason').setValue(value.description);
    });
  }

  private subscribeToReject() {
    this.reactiveSubject.asObservable.pipe(takeUntil(this.destroy$)).subscribe(event => {
      if (event.clickedBtn === FooterButton.Reject) {
        if (this.form && this.form.valid) {
          this.sendFiles();
        } else {
          this.validateControls();
        }
      }
    });
  }

  private rejectRefund() {
    this.refundService.save(this.form.value).subscribe(
      () => {
        this.dialog.close();
        this.handleSuccessOnReject();
      },
      response => {
        this.handleErrorOnReject(response);
      }
    );
  }

  private handleSuccessOnReject() {
    const message = this.translationService.translate(`REFUNDS.rejectDialog.${this.refundType}.saveSuccess`, {
      docNumber: this.docNumber
    });
    const notificationOptions: Message = { life: 10000 };
    this.notificationService.showSuccess(message, notificationOptions);
    this.updateView();
  }

  private handleErrorOnReject(response) {
    if (response.errorCode === 400) {
      const message = this.translationService.translate(`REFUNDS.rejectDialog.${this.refundType}.saveError`, {
        docNumber: this.docNumber
      });
      this.notificationService.showError(message);
      this.markFieldsWithErrors(response);
    } else {
      this.globalErrorHandlerService.handleError(response);
    }
  }

  private markFieldsWithErrors(response) {
    if (response.messages?.length) {
      response.messages.forEach(messageObj => {
        const { message, messageParams } = messageObj;

        if (message && messageParams?.length) {
          const controlName = messageParams.find(param => param.name === 'fieldName').value || '';
          const invalidControl = this.form.get(controlName);

          if (invalidControl) {
            invalidControl.setErrors({
              backendError: { message }
            });
          }
        }
      });
    }
  }

  private updateView() {
    if (this.dialogInjectedData.gridTable) {
      this.dialogInjectedData.gridTable.getData();
    } else {
      this.tabService.refreshCurrentTab();
    }
  }

  private sendFiles() {
    this.removeFilesWithErrors();

    const filesNotCompleted = this.filesToUp.every(file => file.status === UploadStatus.uploading);

    if (this.filesToUp.length > 0 && filesNotCompleted) {
      this.uploadFiles();
    } else {
      this.sendWithFileIds();
    }
  }

  private sendWithFileIds() {
    const fileIds = [];
    this.filesToUp.forEach(file => {
      if (!file.error) {
        fileIds.push(file.id);
      }
    });
    FormUtil.get<RejectViewModel>(this.form, 'attachmentIds').patchValue(fileIds);

    this.resetFileUpload();
    this.rejectRefund();
  }

  private resetFileUpload() {
    this.filesToUp = [];
    this.uploadComponentRef.resetToInitialMode();
  }

  private validateControls() {
    const controls = Object.keys(this.form.controls);

    controls.forEach(control => {
      const formControl: any = this.form.controls[control];
      FormUtil.showControlState(formControl);
    });
  }

  private removeFilesWithErrors() {
    this.filesToUp = this.filesToUp.filter(file => file.isValid);
  }

  private uploadFiles() {
    const files = this.filesToUp.filter(file => file.status !== UploadStatus.completed && file.isValid);
    if (files.length) {
      this.uploadComponentRef.uploadFiles(files);
    }
  }
}
