import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, of, throwError } from 'rxjs';

import { RefundRejectDialogComponent } from './refund-reject-dialog.component';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundService } from '~app/refund/services/refund.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { UploadType } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { FormUtil } from '~app/shared/helpers';
import { NotificationService, TabService } from '~app/shared/services';

const notificationServiceSpy = createSpyObject(NotificationService);
const translationServiceSpy = createSpyObject(L10nTranslationService);
const globalErrorHandlerServiceSpy = createSpyObject(ErrorHandler);
const dialogServiceSpy = createSpyObject(DialogService);
const tabServiceSpy = createSpyObject(TabService);

const mockConfig = {
  data: {
    title: 'title',
    hasCancelButton: true,
    footerButtonsType: FooterButton.Reject,
    rowTableContent: {
      id: 0,
      ticketDocumentNumber: 0,
      dateOfApplication: '',
      currency: { decimals: 1, id: 1, code: 'EUR', isDefault: true },
      totalAmount: 10,
      agent: { id: 1, bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain' } }
    },
    activatedRoute: { data: of({}), params: of({}) }
  }
};

const initialState = {
  auth: {
    user: {}
  },
  core: {
    menu: {
      tabs: {}
    }
  }
};

const mockFiles: FileUpload[] = [
  {
    name: 'mockFile1.txt',
    progress: 0,
    size: 21,
    status: 'uploading',
    fileInstance: null,
    isValid: true,
    error: '',
    cancelled: null,
    finished: null
  },
  {
    name: 'mockFile2.txt',
    progress: 0,
    size: 21,
    status: 'uploading',
    fileInstance: null,
    isValid: true,
    error: '',
    cancelled: null,
    finished: null
  }
];

describe('RefundRejectDialogComponent', () => {
  let component: RefundRejectDialogComponent;
  let fixture: ComponentFixture<RefundRejectDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundRejectDialogComponent, UploadComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        { provide: DialogConfig, useValue: mockConfig },
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        {
          provide: RefundService,
          useValue: {
            save: () => of({})
          }
        },
        { provide: ErrorHandler, useValue: globalErrorHandlerServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: TabService, useValue: tabServiceSpy },
        RefundDataService,
        FormBuilder,
        mockProvider(ReactiveSubject, { asObservable: of() })
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundRejectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set files to upload when input file changes', () => {
    component.onFilesChange(mockFiles);
    expect(component.filesToUp.length).toBe(2);
  });

  it('should not upload files and return from method when files variable length is 0', () => {
    const files = [] as FileUpload[];
    component.onUploadFinished(files);

    expect(!files.length).toBe(true);
  });

  it('should send files with id when onUploadFinished is called with an array of files', () => {
    const sendWithFileIdsSpy = spyOn<any>(component, 'sendWithFileIds');
    component.onUploadFinished(mockFiles);

    expect(sendWithFileIdsSpy).toHaveBeenCalled();
  });

  it('should show notification success when handleSuccessOnReject is called', () => {
    component.docNumber = 'test';
    component['refundType'] = MasterDataType.RefundApp;
    component['handleSuccessOnReject']();

    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
  });

  it('should mark fields with errors when the response has errorCode 400', () => {
    const markFieldsWithErrorsSpy = spyOn<any>(component, 'markFieldsWithErrors');
    const response = {
      errorCode: 400
    };
    component['handleErrorOnReject'](response);

    expect(markFieldsWithErrorsSpy).toHaveBeenCalledWith(response);
  });

  it('should handle the Error with the global error handler service when the response has different error than 400', () => {
    const response = {
      errorCode: 401
    };
    component['handleErrorOnReject'](response);

    expect(globalErrorHandlerServiceSpy.handleError).toHaveBeenCalledWith(response);
  });

  it('should reset to initial mode upload component when onUploadStarted is called with retry as param value', () => {
    const event = UploadType.Retry;
    const uploadComponentRefSpy = spyOn(component.uploadComponentRef, 'resetToInitialMode');

    component.onUploadStarted(event);

    expect(uploadComponentRefSpy).toHaveBeenCalled();
  });

  it('should not start the upload when the event is different to retry', () => {
    const event = UploadType.Normal;
    const uploadComponentRefSpy = spyOn(component.uploadComponentRef, 'resetToInitialMode');
    component.onUploadStarted(event);

    expect(uploadComponentRefSpy).not.toHaveBeenCalled();
  });

  it('should upload Files that are on filesToUp array when uploadFiles is called', () => {
    component.filesToUp = mockFiles;
    const uploadComponentRefSpy = spyOn(component.uploadComponentRef, 'uploadFiles');
    component['uploadFiles']();

    expect(uploadComponentRefSpy).toHaveBeenCalled();
  });

  it('should not upload Files when uploadFiles is called with empty array of files in filesToUp array', () => {
    component.filesToUp = [];
    const uploadComponentRefSpy = spyOn(component.uploadComponentRef, 'uploadFiles');
    component['uploadFiles']();

    expect(uploadComponentRefSpy).not.toHaveBeenCalled();
  });

  it('should remove files with errors if the file object has isValid property to false', () => {
    component.filesToUp = mockFiles;
    component['removeFilesWithErrors']();

    expect(component.filesToUp.length).toBe(2);
  });

  it('should upload files when there are files to upload and sendFiles method is called', () => {
    const uploadComponentSpy = spyOn(component.uploadComponentRef, 'uploadFiles');
    component.filesToUp = mockFiles;
    component['sendFiles']();

    expect(uploadComponentSpy).toHaveBeenCalled();
  });

  it('should send files with files id when send files is called', () => {
    const sendWithFileIdsSpy = spyOn<any>(component, 'sendWithFileIds');
    component.filesToUp = [];
    component['sendFiles']();

    expect(sendWithFileIdsSpy).toHaveBeenCalled();
  });

  it('should reset files upload and reject ferund after sendWithFileIds method is called', () => {
    const resetFileUploadSpy = spyOn<any>(component, 'resetFileUpload');
    const rejectRefundSpy = spyOn<any>(component, 'rejectRefund');
    component.filesToUp = mockFiles;
    component['sendWithFileIds']();

    expect(resetFileUploadSpy).toHaveBeenCalled();
    expect(rejectRefundSpy).toHaveBeenCalled();
  });

  it('should reset file Upload to initial mode when resetFileUpload is called', () => {
    const uploadComponentSpy = spyOn(component.uploadComponentRef, 'resetToInitialMode');
    const filesToUpSpy = spyOn(component, 'filesToUp');
    component['resetFileUpload']();

    expect(uploadComponentSpy).toHaveBeenCalled();
    expect(filesToUpSpy.length).toBe(0);
  });

  it('should markFieldsWithErrors set an error', () => {
    const errorMessage = 'Invalid reason';
    const response = {
      messages: [
        {
          message: errorMessage,
          messageParams: [{ name: 'fieldName', value: 'reasonForRefund' }]
        }
      ]
    };
    component['markFieldsWithErrors'](response);

    expect(response.messages.length).toBeGreaterThan(0);
  });

  it('should refresh current tab when updateView method is called and gridTable has no data', () => {
    component['updateView']();

    expect(tabServiceSpy.refreshCurrentTab).toHaveBeenCalled();
  });

  it('should show control state when validate controls', () => {
    const formUtilSpy = spyOn(FormUtil, 'showControlState');
    component['validateControls']();

    expect(formUtilSpy).toHaveBeenCalled();
  });

  it('should call reject refund save method and close the dialog on the response ok', fakeAsync(() => {
    (component as any).refundService.save = jasmine.createSpy().and.returnValue(of({}));
    component['rejectRefund']();

    tick();

    expect(dialogServiceSpy.close).toHaveBeenCalled();
  }));

  it('should handle error on reject call when the subscribe throw an error', fakeAsync(() => {
    const handleErrorOnRejectSpy = spyOn<any>(component, 'handleErrorOnReject');
    (component as any).refundService.save = jasmine.createSpy().and.returnValue(throwError({}));

    component['rejectRefund']();
    tick();

    expect(handleErrorOnRejectSpy).toHaveBeenCalled();
  }));

  it('should subscribe to reject when reject button is clicked and call validateControls method if form is invalid', fakeAsync(() => {
    const validateControlsSpy = spyOn<any>(component, 'validateControls');
    (component as any).reactiveSubject.asObservable = new BehaviorSubject({ clickedBtn: FooterButton.Reject });

    (component as any).subscribeToReject();
    tick();

    expect(validateControlsSpy).toHaveBeenCalled();
  }));
});
