import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';

@Component({
  selector: 'bspl-refund-not-found-dialog',
  templateUrl: './refund-not-found-dialog.component.html',
  styleUrls: ['./refund-not-found-dialog.component.scss']
})
export class RefundNotFoundDialogComponent implements OnInit, OnDestroy {
  protected dialogInjectedData;
  private destroy$ = new Subject();

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private dialog: DialogService,
    private config: DialogConfig,
    private translationService: L10nTranslationService,
    private reactiveSubject: ReactiveSubject
  ) {
    this.initializeDialogData();
  }

  ngOnInit(): void {
    this.initializeButtonClickListener();
  }

  private initializeDialogData() {
    this.dialogInjectedData = this.config.data;
    this.dialogInjectedData.title = this.translationService.translate('REFUNDS.not-found-document-label');
  }

  private initializeButtonClickListener() {
    this.reactiveSubject.asObservable.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.dialog.close();
    });
  }

  public ngOnDestroy() {
    this.destroy$.next();
  }
}
