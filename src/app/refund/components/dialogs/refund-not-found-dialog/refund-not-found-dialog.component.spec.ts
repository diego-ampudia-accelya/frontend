import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { TabService } from '~app/shared/services/tab.service';
import { RefundNotFoundDialogComponent } from './refund-not-found-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    hasCancelButton: false,
    footerButtonsType: FooterButton.Ok_tertiary,
    rowTableContent: { messages: [] },
    activatedRoute: { data: of({}), params: of({}) }
  }
};

const initialState = {
  auth: {
    user: {}
  },
  core: {
    menu: {
      tabs: {}
    }
  }
};

describe('RefundNotFoundDialogComponent', () => {
  let component: RefundNotFoundDialogComponent;
  let fixture: ComponentFixture<RefundNotFoundDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RefundNotFoundDialogComponent],
      imports: [L10nTranslationModule.forRoot(l10nConfig), RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState }),
        { provide: DialogConfig, useValue: mockConfig },
        DialogService,
        TabService,
        ReactiveSubject
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundNotFoundDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
