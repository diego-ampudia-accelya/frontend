import { HttpClient } from '@angular/common/http';
import { Component, ErrorHandler, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';

import { MasterDataType } from '~app/master-shared/models/master.model';
import { RefundBE } from '~app/refund/models/refund.model';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundService } from '~app/refund/shared/helpers/refund.helper';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FormUtil } from '~app/shared/helpers';
import { ResponseErrorBE } from '~app/shared/models';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService, TabService } from '~app/shared/services';

@Component({
  selector: 'bspl-refund-internal-comment-dialog',
  templateUrl: './refund-internal-comment-dialog.component.html',
  styleUrls: ['./refund-internal-comment-dialog.component.scss']
})
export class RefundInternalCommentDialogComponent implements OnInit, OnDestroy {
  public refundType: MasterDataType;
  public refund: RefundBE;

  public form: FormGroup;

  public isReadonlyMode: boolean;

  private refundService: RefundService;

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    private reactiveSubject: ReactiveSubject,
    private http: HttpClient,
    private dialog: DialogService,
    private globalErrorHandlerService: ErrorHandler,
    private tabService: TabService
  ) {
    this.initializeDialogData();

    this.refundService = createRefundService(this.http, this.refundType);
  }

  public ngOnInit(): void {
    this.buildForm();
    this.initializeButtonsListener();
    if (!this.isReadonlyMode) {
      this.initializeFormListener();
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeDialogData(): void {
    this.refundType = this.config.data.refundType;
    this.refund = this.config.data.refund;
    this.isReadonlyMode = this.config.data.isReadonly;

    this.config.data.title = this.translationService.translate('REFUNDS.internalCommentDialog.title');
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      internalComment: [this.refund.internalComment, [Validators.required, Validators.maxLength(250)]]
    });
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const postButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Post
      );
      postButton.isDisabled = status === 'INVALID';
    });
  }

  private initializeButtonsListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        tap(() => this.validateControls()),
        first(({ clickedBtn }) => clickedBtn !== FooterButton.Post || this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe(({ clickedBtn }) => {
        switch (clickedBtn) {
          case FooterButton.Post:
          case FooterButton.Delete:
            this.onSubmit(clickedBtn);
            break;
          case FooterButton.Cancel:
          case FooterButton.Done:
            this.dialog.close();
            break;
        }
      });
  }

  private validateControls(): void {
    if (this.form.invalid) {
      FormUtil.showControlState(this.form);
    }
  }

  private onSubmit(clickedBtn: FooterButton): void {
    this.setLoading(true);

    if (clickedBtn === FooterButton.Delete) {
      this.refundService.deleteInternalComment(this.refund.id).subscribe(
        () => this.onDeleteSuccess(),
        response => this.onDeleteError(response)
      );
    } else if (clickedBtn === FooterButton.Post) {
      if (this.refund.internalComment) {
        this.refundService.editInternalComment(this.refund.id, this.form.value).subscribe(
          () => this.onEditSuccess(),
          response => this.onPostError(response)
        );
      } else {
        this.refundService.addInternalComment(this.refund.id, this.form.value).subscribe(
          () => this.onAddSuccess(),
          response => this.onPostError(response)
        );
      }
    }
  }

  private setLoading(value: boolean): void {
    this.config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = value));
  }

  private onDeleteSuccess(): void {
    const message = this.translationService.translate('REFUNDS.internalCommentDialog.successMessage.delete');

    this.notificationService.showSuccess(message);
    this.tabService.refreshCurrentTab();
    this.dialog.close();
  }

  private onDeleteError(response: ResponseErrorBE): void {
    this.globalErrorHandlerService.handleError(response);
    this.dialog.close();
  }

  private onEditSuccess(): void {
    const message = this.translationService.translate('REFUNDS.internalCommentDialog.successMessage.edit');

    this.notificationService.showSuccess(message);
    this.tabService.refreshCurrentTab();
    this.dialog.close();
  }

  private onAddSuccess(): void {
    const message = this.translationService.translate('REFUNDS.internalCommentDialog.successMessage.add');

    this.notificationService.showSuccess(message);
    this.tabService.refreshCurrentTab();
    this.dialog.close();
  }

  private onPostError(response: ResponseErrorBE): void {
    this.setLoading(false);

    // We subscribe again to dialog action button so errors can be amended and form resent
    this.initializeButtonsListener();

    this.globalErrorHandlerService.handleError(response);
  }
}
