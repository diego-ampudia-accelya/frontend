import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { takeUntil } from 'rxjs/operators';

import { AppState } from '~app/reducers';
import { RefundMassloadAttachmentFilter } from '~app/refund/models/refund-massload-attachment-filter.model';
import { RefundMassloadAttachment } from '~app/refund/models/refund-massload-attachment.model';
import { RefundMassloadAttachmentFormatter } from '~app/refund/services/refund-massload-attachment-formatter.service';
import { RefundMassloadAttachmentService } from '~app/refund/services/refund-massload-attachment.service';
import { massloadAttachmentKey, massloadAttachmentSelector, State } from '~app/refund/store/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ROUTES } from '~app/shared/constants/routes';
import { FormUtil } from '~app/shared/helpers';
import { GridColumn } from '~app/shared/models';

@Component({
  selector: 'bspl-refund-massload-attachment-list',
  templateUrl: './refund-massload-attachment-list.component.html',
  styleUrls: ['./refund-massload-attachment-list.component.scss'],
  providers: [RefundMassloadAttachmentService, RefundMassloadAttachmentFormatter]
})
export class RefundMassloadAttachmentListComponent
  extends ListSubtabs<RefundMassloadAttachment, RefundMassloadAttachmentFilter>
  implements OnInit
{
  public listItems: string;
  public columns: Array<GridColumn>;
  public searchForm: FormGroup;

  private formFactory: FormUtil;

  constructor(
    public displayFormatter: RefundMassloadAttachmentFormatter,
    protected store: Store<AppState>,
    protected dataService: RefundMassloadAttachmentService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected router: Router,
    private formBuilder: FormBuilder
  ) {
    super(store, dataService, actions$, translationService);

    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    super.ngOnInit();

    this.columns = this.buildColumns();
    this.searchForm = this.buildForm();

    this.initializeListTitle();
  }

  public onViewDocument(row: RefundMassloadAttachment) {
    this.router.navigate([ROUTES.REFUNDS_APP_VIEW.url, row.documentId]);
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<RefundMassloadAttachment, RefundMassloadAttachmentFilter>,
    DefaultProjectorFn<ListSubtabsState<RefundMassloadAttachment, RefundMassloadAttachmentFilter>>
  > {
    return massloadAttachmentSelector;
  }

  protected getListKey(): string {
    return massloadAttachmentKey;
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: 'REFUNDS.files.massloadAttachments.columns.filename',
        prop: 'filename',
        flexGrow: 3
      },
      {
        name: 'REFUNDS.files.massloadAttachments.columns.documentNumber',
        prop: 'documentNumber',
        flexGrow: 2,
        cellTemplate: 'commonLinkFromObjCellTmpl'
      },
      {
        name: 'REFUNDS.files.massloadAttachments.columns.uploadDate',
        prop: 'uploadDate',
        flexGrow: 2,
        pipe: { transform: date => moment(date).format('DD/MM/YYYY hh:mm:ss') }
      },
      {
        name: 'REFUNDS.files.massloadAttachments.columns.userName',
        prop: 'userName',
        flexGrow: 2
      },
      {
        name: 'REFUNDS.files.massloadAttachments.columns.email',
        prop: 'email',
        flexGrow: 3
      }
    ];
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<RefundMassloadAttachmentFilter>({
      filename: [],
      documentNumber: [],
      uploadDate: [],
      userName: [],
      email: []
    });
  }

  private initializeListTitle() {
    this.queryTotalItems$.pipe(takeUntil(this.destroy$)).subscribe(queryTotalItems => {
      this.listItems = this.translationService.translate('REFUNDS.files.massloadAttachments.titleItems', {
        total: queryTotalItems
      });
    });
  }
}
