import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';

@Component({
  selector: 'bspl-refund-files',
  templateUrl: './refund-files.component.html',
  styleUrls: ['./refund-files.component.scss']
})
export class RefundFilesComponent implements OnInit {
  public tabs: RoutedMenuItem[];

  constructor(private activatedRoute: ActivatedRoute, private menuBuilder: MenuBuilder) {}

  public ngOnInit(): void {
    this.initializeTabs();
  }

  private initializeTabs(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }
}
