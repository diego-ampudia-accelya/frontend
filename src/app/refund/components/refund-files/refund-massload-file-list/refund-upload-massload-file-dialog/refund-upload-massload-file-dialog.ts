import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import {
  RefundUploadMassloadDialogPayload,
  UploadDialogAction
} from '~app/refund/models/refund-massload-attachment.model';
import { RefundUploadMassloadDialogConfig } from '~app/refund/models/refund-upload-massload-file-dialog-config';
import { RefundMassloadFileService } from '~app/refund/services/refund-massload-file.service';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { FileValidator, FileValidators } from '~app/shared/components/upload/file-validators';
import { UploadModes, UploadStatus } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { AlertMessageType } from '~app/shared/enums';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-refund-upload-massload-file-dialog',
  templateUrl: './refund-upload-massload-file-dialog.html',
  styleUrls: ['./refund-upload-massload-file-dialog.scss']
})
export class RefundUploadMassloadDialogComponent implements OnInit, OnDestroy {
  @ViewChildren(UploadComponent) uploadComponentRef: QueryList<UploadComponent>;

  public filesToMass: File[] = [];
  public filesToAttach: File[] = [];
  public secondaryTittle: string;
  public uploadAttachMode = UploadModes.Preview;
  public uploadMassMode = UploadModes.Preview;
  public alertMessageType = AlertMessageType;
  public isAttachDisabled = false;
  public dialogForm: FormGroup;
  public validationMassRules: FileValidator[];
  public validationAttachRules: FileValidator[];

  private actionEmitter: Subject<any>;
  private atLeastOneFileIsUploaded: boolean;
  private uploadMassResponseId: string;
  private uploadAttachResponseId: string[] = [];
  private validationRules: FileValidator[];
  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig<RefundUploadMassloadDialogConfig>,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private translationService: L10nTranslationService,
    private notificationService: NotificationService,
    protected dataService: RefundMassloadFileService
  ) {}

  ngOnInit() {
    this.onActionExecuted().subscribe();
    const config = this.config.data;
    this.filesToMass = config.filesToMass;
    this.filesToAttach = config.filesToAttach;
    this.secondaryTittle = config.secondaryTittle;
    this.actionEmitter = config.actionEmitter;
    this.initializeValidationRules();
  }

  public onUploadStarted(): void {
    this.config.data.isClosable = false;
    this.setButtonLoading(FooterButton.Done, true);
  }

  public onUploadFinished(): void {
    this.config.data.isClosable = true;
    this.setButtonLoading(FooterButton.Done, false);
    this.clearFiles();
  }

  public onResetInitialMode(): void {
    this.config.data.buttons = [
      {
        title: this.translationService.translate(
          'REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.buttonCancelLabel'
        ),
        type: FooterButton.Cancel,
        buttonDesign: ButtonDesign.Tertiary
      },
      {
        title: this.translationService.translate(
          'REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.buttonUploadLabel'
        ),
        isDisabled: !this.isValid(),
        type: FooterButton.Upload,
        buttonDesign: ButtonDesign.Primary
      }
    ];

    this.reactiveSubject.emit({
      reset: UploadDialogAction.Reset
    });
  }

  public onFilesMassChange(files: File[]) {
    // When the user uploads more than one file, it is necessary to delete the rest and only keep the last one.
    // Also change the upload mode so that new files cannot be uploaded
    if (files.length >= 1) {
      files.splice(1);
      this.uploadMassMode = UploadModes.Upload;
    } else {
      this.uploadMassMode = UploadModes.Preview;
    }
    this.config.data.filesToMass = files;
    this.setButtonDisabled(!this.isValid());
  }

  public onFilesAttachChange(files: File[]) {
    this.config.data.filesToAttach = files;
    this.setButtonDisabled(!this.isValid());
  }

  public onUploadMassFinishedResult(result: FileUpload[]) {
    if (Array.isArray(result)) {
      this.uploadMassResponseId = result[0]?.responseData?.id;
    }
  }

  public onUploadAttachFinishedResult(result: FileUpload[]) {
    if (Array.isArray(result)) {
      this.uploadAttachResponseId = [];
      result.forEach(res => {
        if (res.responseData?.id) {
          this.uploadAttachResponseId.push(res.responseData.id);
        }
      });
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeValidationRules() {
    this.validationRules = [FileValidators.maxFileSize(), FileValidators.minFileSize()];
    this.validationMassRules = [...this.validationRules, FileValidators.filenameMaxLength(90)];
    this.validationAttachRules = [...this.validationRules, FileValidators.filenameMaxLength(120)];
  }

  private onActionExecuted(): Observable<any> {
    return this.reactiveSubject.asObservable.pipe(
      tap(action => {
        if (action.clickedBtn === FooterButton.Cancel) {
          this.clearFilesAndCloseDialog();
        }

        if (action.clickedBtn === FooterButton.Done) {
          this.clearFilesAndCloseDialog();
          if (this.atLeastOneFileIsUploaded) {
            this.dataService.createMassUploadFile(this.createPayloadObject()).subscribe(res => {
              this.actionEmitter.next();
              this.showSuccessNotification(res);
            });
          }
        }

        if (action.clickedBtn === FooterButton.Upload) {
          this.config.data.buttons = [
            {
              title: this.translationService.translate(
                'REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.buttonDoneLabel'
              ),
              isDisabled: false,
              type: FooterButton.Done,
              buttonDesign: ButtonDesign.Tertiary
            }
          ];

          if (!this.config.data.filesToAttach.length) {
            this.isAttachDisabled = true;
          }
          this.uploadAllFiles();
        }
      }),
      takeUntil(this.destroy$)
    );
  }

  private uploadAllFiles() {
    this.uploadComponentRef.forEach(uploadComp => {
      uploadComp.uploadFiles().subscribe(res => {
        if (uploadComp.filesToUpload.length) {
          this.atLeastOneFileIsUploaded = res;
          if (!this.atLeastOneFileIsUploaded) {
            this.showErrorNotification();
          }
        }
      });
    });
  }

  private createPayloadObject(): RefundUploadMassloadDialogPayload {
    return {
      id: this.uploadMassResponseId,
      attachmentIds: this.uploadAttachResponseId
    };
  }

  private clearFilesAndCloseDialog(): void {
    this.clearFiles();
    this.dialogService.close();
  }

  private showErrorNotification() {
    this.notificationService.showError(
      this.translationService.translate(
        'REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.massUploadErrorMessage'
      )
    );
  }

  private showSuccessNotification(result) {
    const textMessage = 'REFUNDS.files.massload.uploadMassFile.uploadMassFileDialog.massUploadSuccessMessage';
    const message = this.translationService.translate(textMessage, {
      originalFileName: result.originalFilename,
      filename: result.filename
    });
    this.notificationService.showSuccess(message);
  }

  private clearFiles(): void {
    this.config.data.files = [];
  }

  private setButtonDisabled(isDisabled: boolean): void {
    const actionButtons = this.config.data.buttons.filter(button => button.type !== FooterButton.Cancel);
    actionButtons.forEach(actionButton => {
      if (actionButton) {
        actionButton.isDisabled = isDisabled;
      }
    });
  }

  private setButtonLoading(buttonType: FooterButton, isLoading: boolean): void {
    const actionButton = this.getButton(buttonType);
    if (actionButton) {
      actionButton.isLoading = isLoading;
    }
  }

  private getButton(buttonType: FooterButton): ModalAction {
    return this.config.data.buttons.find(button => button.type === buttonType);
  }

  private isValid(): boolean {
    const mandatoryCondition =
      this.config.data.filesToMass.length &&
      this.config.data.filesToMass.some((file: any) => file.isValid && file.status !== UploadStatus.failed);
    const secondaryCondition = this.config.data.filesToAttach.length
      ? this.config.data.filesToAttach?.some((file: any) => file.isValid)
      : true;

    return mandatoryCondition && secondaryCondition;
  }
}
