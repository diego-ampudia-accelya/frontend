import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { Subject } from 'rxjs';
import { first, switchMap, takeUntil, tap } from 'rxjs/operators';

import { EMPTY_REFUND_MASSLOAD_FILE_TOTALS, REFUNDS_MASS_FILES_COLUMNS } from './refund-massload-file-list.constants';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { RefundMassloadFileFilter } from '~app/refund/models/refund-massload-file-filter.model';
import {
  RefundMassloadFileModel,
  RefundMassloadFileStatus,
  RefundMassloadFileTotals
} from '~app/refund/models/refund-massload-file.model';
import { RefundMassloadFileFormatter } from '~app/refund/services/refund-massload-file-formatter.service';
import { RefundMassloadFileService } from '~app/refund/services/refund-massload-file.service';
import { RefundUploadMassloadFileDialogService } from '~app/refund/services/refund-upload-massload-file-dialog.service';
import { refundFilesPermission } from '~app/refund/shared/helpers/refund-permissions.config';
import { massloadFileKey, massloadFileSelector, State } from '~app/refund/store/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ButtonDesign, FloatingPanelComponent } from '~app/shared/components';
import { SelectMode } from '~app/shared/enums';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { FormUtil, toValueLabelObject } from '~app/shared/helpers';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { NotificationService } from '~app/shared/services';

@Component({
  selector: 'bspl-refund-massload-file-list',
  templateUrl: './refund-massload-file-list.component.html',
  styleUrls: ['./refund-massload-file-list.component.scss'],
  providers: [RefundMassloadFileFormatter]
})
export class RefundMassloadFileListComponent
  extends ListSubtabs<RefundMassloadFileModel, RefundMassloadFileFilter>
  implements OnInit
{
  @ViewChild(FloatingPanelComponent, { static: true }) public totalsPanel: FloatingPanelComponent;

  public hasCreatePermission: boolean;

  public columns: Array<GridColumn>;
  public searchForm: FormGroup;
  public actions: Array<{ action: GridTableActionType; disabled?: boolean }>;

  public totalItemsMessage: string;
  public noticeMessage: string;

  public selectMode = SelectMode;
  public buttonDesign = ButtonDesign;

  public statusDropdownOptions: DropdownOption<RefundMassloadFileStatus>[];

  public totals: RefundMassloadFileTotals = EMPTY_REFUND_MASSLOAD_FILE_TOTALS;
  public isTotalLoading: boolean;

  private formFactory: FormUtil;
  private dialogSubmitEmitter = new Subject<any>();

  constructor(
    public displayFormatter: RefundMassloadFileFormatter,
    protected store: Store<AppState>,
    protected dataService: RefundMassloadFileService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private permissionsService: PermissionsService,
    private refundUploadMassloadFileDialogService: RefundUploadMassloadFileDialogService
  ) {
    super(store, dataService, actions$, translationService);

    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    super.ngOnInit();

    this.initializePermissions();
    this.initializeListView();
    this.initializeTotals();

    this.dialogSubmitEmitter.pipe(takeUntil(this.destroy$)).subscribe(() => this.onQueryChanged());
  }

  public onActionMenuClick(row: RefundMassloadFileModel): void {
    if (row.evaluationFileId) {
      this.actions = [{ action: GridTableActionType.DownloadEvaluationFile }];
    } else {
      this.actions = [{ action: GridTableActionType.DownloadEvaluationFile, disabled: true }];
    }
  }

  public onActionClick({ action, row }: { event: Event; action: GridTableAction; row: RefundMassloadFileModel }) {
    if (action.actionType === GridTableActionType.DownloadEvaluationFile) {
      this.dataService.downloadEvaluationFile(row.evaluationFileId).subscribe(response => {
        FileSaver.saveAs(response.blob, response.fileName);
        this.notificationService.showSuccess(
          this.translationService.translate('REFUNDS.files.massload.successDownloadMessage', {
            fileName: response.fileName
          })
        );
      });
    }
  }

  public onTotalPanelOpened(): void {
    this.query$
      .pipe(
        first(),
        tap(() => (this.isTotalLoading = true)),
        switchMap(query => this.dataService.findTotals(query)),
        tap(() => (this.isTotalLoading = false))
      )
      .subscribe(totals => (this.totals = totals));
  }

  public onTotalPanelClosed(): void {
    this.totals = EMPTY_REFUND_MASSLOAD_FILE_TOTALS;
  }

  public onUploadMassFileClick(): void {
    this.refundUploadMassloadFileDialogService.openMassDeleteFileDialog(this.dialogSubmitEmitter);
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<RefundMassloadFileModel, RefundMassloadFileFilter>,
    DefaultProjectorFn<ListSubtabsState<RefundMassloadFileModel, RefundMassloadFileFilter>>
  > {
    return massloadFileSelector;
  }

  protected getListKey(): string {
    return massloadFileKey;
  }

  private getTranslatedStatus(status: RefundMassloadFileStatus): string {
    return this.translationService.translate(`REFUNDS.files.massload.status.${status?.toLowerCase()}`);
  }

  private initializePermissions(): void {
    this.hasCreatePermission = this.permissionsService.hasPermission(refundFilesPermission.raMassCreate);
  }

  private initializeListView(): void {
    this.buildColumns();
    this.buildForm();

    this.initializeDropdownOptions();
  }

  private initializeTotals(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => {
      const queryTotalItems = query.paginateBy.totalElements;

      // Update query total items on query change
      this.totalItemsMessage = this.translationService.translate('REFUNDS.files.massload.totalItemsMessage', {
        total: queryTotalItems
      });

      // Close totals panel on query change
      this.totalsPanel.close();
    });
  }

  private buildColumns(): void {
    this.columns = REFUNDS_MASS_FILES_COLUMNS;

    this.setStatusColumn();
  }

  private buildForm(): void {
    if (this.searchForm) {
      this.searchForm.reset();
    } else {
      this.searchForm = this.formFactory.createGroup<RefundMassloadFileFilter>({
        filename: [],
        originalFileName: [],
        uploadDate: [],
        userName: [],
        email: [],
        status: []
      });
    }
  }

  private initializeDropdownOptions(): void {
    this.statusDropdownOptions = Object.values(RefundMassloadFileStatus)
      .map(toValueLabelObject)
      .map(({ value, label }) => ({
        value,
        label: this.getTranslatedStatus(label)
      }));
  }

  private setStatusColumn(): void {
    const statusColumn = this.columns.find(col => col.prop === 'status');

    statusColumn.pipe = { transform: status => this.getTranslatedStatus(status) };
    statusColumn.badgeInfo = {
      hidden: (value: RefundMassloadFileModel) => !value,
      type: (value: RefundMassloadFileModel) => this.getBadgeInfoType(value.status),
      tooltipLabel: (value: RefundMassloadFileModel) => this.getBadgeInfoTooltip(value.status)
    };
  }

  private getBadgeInfoType(status: RefundMassloadFileStatus): BadgeInfoType {
    let type: BadgeInfoType;

    switch (status) {
      case RefundMassloadFileStatus.Pending:
        type = BadgeInfoType.info;
        break;
      case RefundMassloadFileStatus.Processed:
        type = BadgeInfoType.success;
        break;
      case RefundMassloadFileStatus.Rejected:
        type = BadgeInfoType.regular;
        break;
      default:
        type = BadgeInfoType.warning;
        break;
    }

    return type;
  }

  private getBadgeInfoTooltip(status: RefundMassloadFileStatus): string {
    return `${this.translationService.translate('REFUNDS.files.massload.columns.status')}: ${this.getTranslatedStatus(
      status
    )}`;
  }
}
