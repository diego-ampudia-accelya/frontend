import moment from 'moment-mini';

import { RefundMassloadFileTotals } from '~app/refund/models/refund-massload-file.model';
import { GridColumn } from '~app/shared/models';

export const REFUNDS_MASS_FILES_COLUMNS: Array<GridColumn> = [
  {
    name: 'REFUNDS.files.massload.columns.originalFileName',
    prop: 'originalFilename',
    flexGrow: 3
  },
  {
    name: 'REFUNDS.files.massload.columns.fileName',
    prop: 'filename',
    flexGrow: 3
  },
  {
    name: 'REFUNDS.files.massload.columns.uploadDate',
    prop: 'uploadDate',
    pipe: { transform: date => moment(date).format('DD/MM/YYYY hh:mm:ss') },
    flexGrow: 2
  },
  {
    name: 'REFUNDS.files.massload.columns.authorizedRefunds',
    prop: 'authorizedRefunds',
    flexGrow: 2
  },
  {
    name: 'REFUNDS.files.massload.columns.rejectedRefunds',
    prop: 'rejectedRefunds',
    flexGrow: 2
  },
  {
    name: 'REFUNDS.files.massload.columns.acceptedTransactions',
    prop: 'acceptedTransactions',
    flexGrow: 2
  },
  {
    name: 'REFUNDS.files.massload.columns.transactions',
    prop: 'transactions',
    flexGrow: 2
  },
  {
    name: 'REFUNDS.files.massload.columns.status',
    prop: 'status',
    flexGrow: 2,
    cellTemplate: 'badgeInfoCellTmpl'
  }
];

export const EMPTY_REFUND_MASSLOAD_FILE_TOTALS: RefundMassloadFileTotals = {
  authorizedRefunds: null,
  rejectedRefunds: null,
  acceptedTransactions: null,
  transactions: null
};
