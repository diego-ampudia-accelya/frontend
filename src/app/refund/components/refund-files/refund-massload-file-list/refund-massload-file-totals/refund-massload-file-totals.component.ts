import { ChangeDetectionStrategy, Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { RefundMassloadFileTotals } from '~app/refund/models/refund-massload-file.model';
import { SimpleTableColumn } from '~app/shared/components';

@Component({
  selector: 'bspl-refund-massload-file-totals',
  templateUrl: './refund-massload-file-totals.component.html',
  styleUrls: ['./refund-massload-file-totals.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RefundMassloadFileTotalsComponent implements OnInit {
  @ViewChild('numberCellTmpl', { static: true }) numberCellTmpl: TemplateRef<any>;
  @Input() public totals: RefundMassloadFileTotals;

  public columns: SimpleTableColumn<RefundMassloadFileTotals>[];

  public ngOnInit(): void {
    this.columns = this.getColumns();
  }

  private getColumns(): SimpleTableColumn<RefundMassloadFileTotals>[] {
    return [
      {
        header: 'REFUNDS.files.massload.totalsColumns.authorized',
        field: 'authorizedRefunds',
        cellTemplate: this.numberCellTmpl
      },
      {
        header: 'REFUNDS.files.massload.totalsColumns.rejected',
        field: 'rejectedRefunds',
        cellTemplate: this.numberCellTmpl
      },
      {
        header: 'REFUNDS.files.massload.totalsColumns.acceptedTrans',
        field: 'acceptedTransactions',
        cellTemplate: this.numberCellTmpl
      },
      {
        header: 'REFUNDS.files.massload.totalsColumns.totalTrans',
        field: 'transactions',
        cellTemplate: this.numberCellTmpl,
        customStyle: 'font-weight-bold'
      }
    ];
  }
}
