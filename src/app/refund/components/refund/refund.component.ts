import { HttpClient } from '@angular/common/http';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ErrorHandler,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { Observable, Subject, Subscription } from 'rxjs';
import { filter, first, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { DocumentService } from '~app/document/services/document.service';
import { AppState } from '~app/reducers';
import { RefundDiscardDialogComponent } from '~app/refund/components/dialogs/refund-discard-dialog/refund-discard-dialog.component';
import { RefundIssueErrorDialogComponent } from '~app/refund/components/dialogs/refund-issue-error-dialog/refund-issue-error-dialog.component';
import { DetailViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionEmitterType, RefundActionType, RefundBE, RefundViewModel } from '~app/refund/models/refund.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundConfigService } from '~app/refund/services/refund-config.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundDialogService } from '~app/refund/services/refund-dialog.service';
import { RefundHistoryService } from '~app/refund/services/refund-history.service';
import { RefundTicketService } from '~app/refund/services/refund-ticket.service';
import { RefundService } from '~app/refund/services/refund.service';
import { createRefundConfigService, createRefundTicketService } from '~app/refund/shared/helpers/refund.factory';
import { adapterViewFunctionsMapper, fieldMapperRefund } from '~app/refund/shared/helpers/view-be-mapper.config';
import { RefundFormActions } from '~app/refund/store/actions';
import { DialogService, FooterButton } from '~app/shared/components';
import { AccordionItemComponent } from '~app/shared/components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { ROUTES } from '~app/shared/constants/routes';
import { AccessType } from '~app/shared/enums';
import { buildViewFromBEObject, FormUtil } from '~app/shared/helpers';
import { ResponseErrorBE } from '~app/shared/models/error-response-be.model';
import { appConfiguration } from '~app/shared/services';
import { NotificationService } from '~app/shared/services/notification.service';
import { TabService } from '~app/shared/services/tab.service';

@Component({
  selector: 'bspl-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss'],
  providers: [
    RefundService,
    RefundDataService,
    {
      provide: RefundConfigService,
      useFactory: createRefundConfigService,
      deps: [RefundDataService, L10nTranslationService, HttpClient, Store, RefundActionsService]
    },
    {
      provide: RefundTicketService,
      useFactory: createRefundTicketService,
      deps: [DocumentService, RefundDataService]
    }
  ]
})
export class RefundComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(AccordionComponent, { static: true })
  accordion: AccordionComponent;
  @ViewChildren(AccordionItemComponent)
  accordionSections: QueryList<AccordionItemComponent>;

  public sectionsMapper: { [key in keyof Partial<RefundViewModel>]: string } = {
    basicInformation: 'basicInformation',
    details: 'details',
    formPayment: 'formPayment',
    amounts: 'amounts'
  };

  public showSectionsError = false;
  public activities: Activity[] = [];
  public commentAdded$: Subject<Activity[]>;
  public isActivityCommentVisible = false;
  public showTaxRepeatWarning = false;
  public fileUploadPath: string;
  public headerHeight: number;
  public paymentTotalAmount: number;
  public selectedBspId: number;

  private dialogForm: FormGroup;
  private itemRetrieved: RefundBE;
  private documentIdRetrieved: string;
  private dialogFormSubmitEmitter = new Subject<Partial<RefundBE>>();
  private dialogFormSubmitEmitterSubscription: Subscription;
  private destroy$ = new Subject<any>();

  constructor(
    public formData: RefundDataService,
    public formConfig: RefundConfigService,
    public refundTicket: RefundTicketService,
    public refundService: RefundService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private tabService: TabService,
    private activatedRoute: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private dialog: DialogService,
    private store: Store<AppState>,
    private refundDialogService: RefundDialogService,
    private refundHistoryService: RefundHistoryService,
    private globalErrorHandlerService: ErrorHandler
  ) {
    this.initializeRouteData();
    this.initializeRouteParams();
  }

  public ngOnInit(): void {
    this.initializeActivityPanel();
    this.showInfoMessages();
  }

  public ngAfterViewInit(): void {
    if (this.itemRetrieved) {
      buildViewFromBEObject<RefundBE, RefundViewModel>(
        this.itemRetrieved,
        this.formData.getForm(),
        this.formConfig.accessType === AccessType.read,
        fieldMapperRefund,
        adapterViewFunctionsMapper
      );
      this.formData.isFormReady.next(true);
      this.cd.detectChanges(); // TODO Check if needed
      if (this.formConfig.accessType === AccessType.edit) {
        this.initializeEditChangesListener();
      }
    } else if (this.documentIdRetrieved) {
      this.refundTicket.updateSummaryForm(this.documentIdRetrieved);
    } else {
      this.formData.isFormReady.next(true);
      this.cd.detectChanges(); // TODO Check if needed
    }
  }

  public onShowTaxWarning(value: boolean): void {
    this.showTaxRepeatWarning = value;
  }

  public onSendCommentRefund(message: { reason: string[]; attachmentIds?: string[] }): void {
    this.commentAdded$ = new Subject<Activity[]>();
    this.refundService
      .addComment(message, this.itemRetrieved.id)
      .pipe(tap(() => this.initializeActivities()))
      .subscribe(this.commentAdded$);
  }

  public onDownloadFile(fileUrl: string): void {
    this.refundService.downloadFile(fileUrl).subscribe(file => {
      FileSaver.saveAs(file.blob, file.fileName);
    });
  }

  public onActionDiscardClick(): void {
    this.openDiscardConfirmationDialog()
      .pipe(
        first(),
        filter(btn => btn.clickedBtn === FooterButton.Discard)
      )
      .subscribe(() => {
        this.tabService.refreshCurrentTab();
        this.dialog.close();
      });
  }

  public onPaymentTotalAmountChange(totalAmount: number): void {
    this.paymentTotalAmount = totalAmount;
    this.cd.detectChanges();
  }

  public onHeaderHeightChange(height: number): void {
    this.headerHeight = height;
  }

  public onActionClick(actionType: RefundActionType): void {
    switch (actionType) {
      case RefundActionType.issue:
      case RefundActionType.applyChanges:
      case RefundActionType.investigate:
      case RefundActionType.authorize:
      case RefundActionType.supervise:
      case RefundActionType.issuePendingSupervision:
      case RefundActionType.resubmit:
        if (this.formData.isFormValid()) {
          this.openConfirmationDialog(this.formData.getDataForSubmit(), actionType)
            .pipe(takeUntil(this.destroy$))
            .subscribe(btn => {
              if (btn.clickedBtn === FooterButton.Cancel) {
                this.saveDialogData();
              }
            });
        } else {
          this.handlePendingFields();
        }

        //* Error icons are shown in accordion after trying to issue for the first time
        this.showSectionsError = true;
        break;

      case RefundActionType.edit:
        this.onModifyClick();
        break;
    }
  }

  public isSectionInvalid(sectionId: string): boolean {
    const formSection = Object.keys(this.sectionsMapper).find(id => this.sectionsMapper[id] === sectionId);

    return (
      this.formData.isFormReady.value && this.formData.getSectionFormGroup(formSection as keyof RefundViewModel).invalid
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeRouteData(): void {
    const data = this.activatedRoute.snapshot.data;

    this.itemRetrieved = data.item;
    this.formConfig.refundType = data.refundType;
    this.formConfig.accessType = data.access;
    this.formConfig.isInvestigate = data.isInvestigate;
    this.formConfig.isAuthorize = data.isAuthorize;
    this.formConfig.isSupervise = data.isSupervise;
    this.formConfig.issuingPendingSupervision = data.issuingPendingSupervision;
    this.formConfig.isResubmission = data.isResubmission;
  }

  private initializeRouteParams(): void {
    this.activatedRoute.queryParams.pipe(take(1)).subscribe(queryParams => {
      if (queryParams.bspId) {
        this.selectedBspId = +queryParams.bspId;
      }

      if (queryParams.documentId) {
        this.formConfig.accessType = AccessType.editTicket;
        this.documentIdRetrieved = queryParams.documentId;
      }
    });
  }

  private saveRefundData$(data: RefundBE): Observable<RefundBE> {
    return this.refundService.save(data);
  }

  private saveDialogData(): void {
    //* Include properties to be saved from dialogForm
    const { reasonForRefund } = this.dialogForm.value;

    if (reasonForRefund) {
      buildViewFromBEObject(
        { reasonForRefund },
        this.formData.getForm(),
        false,
        fieldMapperRefund,
        adapterViewFunctionsMapper
      );
    }
  }

  private showInfoMessages(): void {
    if (this.formConfig.isResubmission) {
      this.notificationService.showInformation('REFUNDS.resubmit_info');
    }
  }

  private onModifyClick(): void {
    const id = this.formData.getFormValue().id;
    // TODO Handle RFND Notice edition when is done
    const queryUrl = ROUTES.REFUNDS_APP_EDIT.url;

    const url = `${queryUrl}/${id}`;
    this.tabService.closeCurrentTabAndNavigate(url);
  }

  private initializeDialogFormSubmitEmitterListener(action: RefundActionType): void {
    if (this.dialogFormSubmitEmitterSubscription && !this.dialogFormSubmitEmitterSubscription.closed) {
      //* Removing previous subscription when opening again the dialog
      this.dialogFormSubmitEmitterSubscription.unsubscribe();
    }

    this.dialogFormSubmitEmitterSubscription = this.dialogFormSubmitEmitter
      .pipe(
        first(),
        //* Before saving, we build a refund combining the current data with the dialog new data
        switchMap(formData => this.saveRefundData$({ ...this.formData.getDataForSubmit(), ...formData })),
        takeUntil(this.destroy$)
      )
      .subscribe(
        refund => this.handleSuccessOnSubmit(refund, action),
        response => this.handleErrorsOnSubmit(response, action)
      );
  }

  private initializeActivityPanel(): void {
    this.fileUploadPath = `${appConfiguration.baseUploadPath}/refund-management/files`;
    this.initializeActivities();
    this.initializeActivityCommentVisibility();
  }

  private initializeActivities(): void {
    if (this.formConfig.accessType !== AccessType.create && this.itemRetrieved) {
      this.refundHistoryService
        .getActivities(this.itemRetrieved?.id, this.formConfig.refundType)
        .pipe(first())
        .subscribe(activities => {
          this.activities = activities;
          this.cd.markForCheck();
        });
    }
  }

  private initializeActivityCommentVisibility(): void {
    this.isActivityCommentVisible = this.formConfig.accessType !== AccessType.create;
  }

  private initializeEditChangesListener(): void {
    this.formData
      .getForm()
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.store.dispatch(new RefundFormActions.ModifyRefund(this.formData.getFormValue()));
      });
  }

  private handlePendingFields(): void {
    FormUtil.showControlState(this.formData.getForm());
    this.scrollToFirstInvalidSection();
    this.notificationService.showError(this.translationService.translate('REFUNDS.error_submit'));
  }

  private openDiscardConfirmationDialog(): Observable<any> {
    return this.dialog.open(RefundDiscardDialogComponent, {
      data: {
        footerButtonsType: FooterButton.Discard,
        hasCancelButton: true,
        isClosable: true
      }
    });
  }

  private openConfirmationDialog(refund: RefundBE, action: RefundActionType): Observable<any> {
    this.initializeDialogFormSubmitEmitterListener(action);

    const detailsSection = this.formData.getSectionFormGroup('details').value as DetailViewModel;
    const reasonForRefund: string = detailsSection.reasonForRefund;
    const predefinedReason: string = detailsSection.predefinedReason;

    this.dialogForm = new FormGroup({});

    return this.formData.getSubject(RefundActionEmitterType.bsp).pipe(
      first(),
      switchMap(bsp =>
        this.refundDialogService.openDialog(action, refund, this.formConfig.refundType, this.dialogForm, {
          reasonForRefund,
          predefinedReason,
          bsp,
          bspRefundConfiguration$: this.formConfig.bspRefundConfiguration,
          dialogFormSubmitEmitter: this.dialogFormSubmitEmitter
        })
      )
    );
  }

  private handleSuccessOnSubmit(refund: RefundBE, action: RefundActionType): void {
    this.dialog.close();
    if (refund) {
      const msg = `REFUNDS.success_msg.${this.formConfig.refundType}.${action}`;

      this.notificationService.showSuccess(
        this.translationService.translate(msg, {
          number: refund.ticketDocumentNumber,
          prevNumber: this.itemRetrieved?.ticketDocumentNumber
        })
      );
      this.navigateToRefund(refund.id);
    }
  }

  private handleErrorsOnSubmit(error, action: RefundActionType): void {
    //* To preserve the reason text on BE errors
    this.saveDialogData();

    const errorToManage: ResponseErrorBE = error.error;
    const msg = `REFUNDS.error_msg.${action}`;

    if (errorToManage && errorToManage.errorCode === 400) {
      //* We only process errors on Bad Request. Other errors are handled separatedly
      this.showSpecificErrors(errorToManage, this.translationService.translate(msg));

      //* We show errors in dialog form if possible, marking the form as invalid
      this.processDialogFormErrorsOnSubmit(errorToManage);

      if (this.dialogForm.valid) {
        //* There are no errors in dialogForm itself, so we close it and go back to the form
        this.dialog.close();
        this.formData.processErrorsOnSubmit(errorToManage);

        const errorsWithoutParams = errorToManage.messages.filter(
          errorMessage => errorMessage.messageParams.length === 0
        );

        if (errorsWithoutParams.length) {
          this.openErrorsDialog(errorToManage)
            .pipe(takeUntil(this.destroy$))
            .subscribe(event => {
              if (event.clickedBtn === FooterButton.Ok_tertiary) {
                this.dialog.close();
              }
            });
        } else {
          this.scrollToFirstInvalidSection();
        }
      } else {
        //* Dialog form needs to be resubmitted so we initialize again its listener for submission
        this.initializeDialogFormSubmitEmitterListener(action);
      }
    } else {
      this.dialog.close();
      this.globalErrorHandlerService.handleError(error);
    }
  }

  private showSpecificErrors(errorToManage: ResponseErrorBE, defaultErrorMessage: string) {
    //* Add specific messageCodes when we should show their message instead of default message
    const specificErrorCodes = ['00056'];

    // Check if any message received has a specificErrorCode from the previous list
    const specificErrorMessage = errorToManage.messages.find(message =>
      specificErrorCodes.includes(message.messageCode)
    )?.message;

    this.notificationService.showError(specificErrorMessage || defaultErrorMessage);
  }

  private processDialogFormErrorsOnSubmit(errorToManage: ResponseErrorBE): void {
    FormUtil.processErrorsOnForm(errorToManage, this.dialogForm);
  }

  private openErrorsDialog(refundError: ResponseErrorBE): Observable<any> {
    return this.dialog.open(RefundIssueErrorDialogComponent, {
      data: {
        title: this.translationService.translate('REFUNDS.issueDialog.refund-dialog-error.title'),
        footerButtonsType: FooterButton.Ok_tertiary,
        rowTableContent: refundError,
        hasCancelButton: false,
        isClosable: true
      }
    });
  }

  private scrollToFirstInvalidSection(): void {
    const sectionWithError = this.accordionSections.find(section => {
      const id = section.nativeElement.id;

      return Object.keys(this.sectionsMapper).some(
        formMapped =>
          this.sectionsMapper[formMapped] === id && this.formData.getSectionFormGroup(formMapped as any).invalid
      );
    });

    if (sectionWithError) {
      this.accordion.goToItem(sectionWithError);
    }
  }

  private navigateToRefund(id: number): void {
    const queryUrl = this.formConfig.isRa ? ROUTES.REFUNDS_APP_VIEW.url : ROUTES.REFUNDS_NOTICE_VIEW.url;

    const url = `${queryUrl}/${id}`;
    this.tabService.closeCurrentTabAndNavigate(url);
  }
}
