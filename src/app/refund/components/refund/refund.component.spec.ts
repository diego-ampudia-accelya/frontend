import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { of, Subscription } from 'rxjs';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { StatusHeaderViewModel, StatusViewModel } from '~app/refund/models/refund-view-aux.model';
import { RefundActionsService } from '~app/refund/services/refund-actions.service';
import { RefundDataService } from '~app/refund/services/refund-data.service';
import { RefundHistoryService } from '~app/refund/services/refund-history.service';
import { RefundTicketService } from '~app/refund/services/refund-ticket.service';
import { createRefundConfigService } from '~app/refund/shared/helpers/refund.factory';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { FooterButton } from '~app/shared/components';
import { AccordionComponent } from '~app/shared/components/accordion/accordion.component';
import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';
import { AccessType } from '~app/shared/enums';
import * as util from '~app/shared/helpers';
import { FormUtil } from '~app/shared/helpers';
import { ResponseErrorBE } from '~app/shared/models';
import { AppConfigurationService, TabService } from '~app/shared/services';
import { NotificationService } from '~app/shared/services/notification.service';
import { RefundActionType, RefundBE, RefundViewModel } from '../../models/refund.model';
import { RefundAmountTaxComponent } from '../refund-amount-tax/refund-amount-tax.component';
import { RefundBasicInfoComponent } from '../refund-basic-info/refund-basic-info.component';
import { RefundDetailsComponent } from '../refund-details/refund-details.component';
import { RefundFormPaymentComponent } from '../refund-form-payment/refund-form-payment.component';
import { RefundComponent } from './refund.component';

const mockAgent = { bsps: [{ id: 1 }] };

const initialState = {
  auth: {
    user: mockAgent
  },
  core: {
    menu: {
      tabs: {}
    }
  },
  refund: {}
};
const obj: any = { documentId: '12345' };

const activatedRouteStub = {
  snapshot: {
    data: {
      refundType: MasterDataType.RefundApp,
      item: {
        id: 1234
      }
    }
  },
  queryParams: of(obj)
};

const refundItem: RefundBE = {
  agent: null,
  agentAddress: null,
  agentContact: null,
  airline: null,
  airlineAddress: null,
  airlineCodeRelatedDocument: '',
  airlineContact: null,
  cardPayments: [],
  currency: null,
  dateOfIssueRelatedDocument: '',
  id: 12345,
  netReporting: false,
  originalIssue: null,
  partialRefund: false,
  passenger: '',
  reasonForRefund: '',
  relatedTicketConjunctions: [],
  relatedTicketDocument: null,
  relatedTicketExchange: false,
  resubmissionRemark: '',
  sentToDpc: null,
  status: null,
  taxMiscellaneousFees: [],
  ticketDocumentNumber: '1234'
};

const error = {
  error: {
    timestamp: '',
    errorCode: 400,
    errorMessage: '',
    messages: [
      {
        message: '',
        messageCode: '',
        messageParams: [{ name: '', value: '' }]
      }
    ]
  }
};

describe('RefundComponent', () => {
  let component: RefundComponent;
  let fixture: ComponentFixture<RefundComponent>;
  let builder: FormBuilder;

  const routerSpy = createSpyObject(Router);
  const notificationServiceSpy = createSpyObject(NotificationService);
  const refundTicketServiceSpy = createSpyObject(RefundTicketService);
  const tabServiceSpy = createSpyObject(TabService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        RefundComponent,
        RefundDetailsComponent,
        RefundAmountTaxComponent,
        RefundBasicInfoComponent,
        RefundFormPaymentComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule,
        L10nTranslationModule.forRoot(l10nConfig),
        HttpClientTestingModule,
        NoopAnimationsModule
      ],
      providers: [
        provideMockStore({ initialState }),
        HttpTestingController,
        FormBuilder,
        L10nTranslationService,
        HttpClient,
        RefundActionsService,
        RefundHistoryService,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: AppConfigurationService, useValue: { baseApiPath: '', baseUploadPath: '' } },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: RefundTicketService, useValue: refundTicketServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: TabService, useValue: tabServiceSpy },
        mockProvider(PermissionsService)
      ]
    }).compileComponents();
  }));

  beforeEach(done => {
    fixture = TestBed.createComponent(RefundComponent);
    component = fixture.componentInstance;

    component.formData = new RefundDataService(TestBed.inject(FormBuilder));
    component.formConfig = createRefundConfigService(
      component.formData,
      TestBed.inject(L10nTranslationService),
      TestBed.inject(HttpClient),
      TestBed.inject(Store),
      TestBed.inject(RefundActionsService)
    );

    builder = TestBed.inject(FormBuilder);

    component.formData.whenFormIsReady().subscribe(done);

    const formUtil = new FormUtil(builder);

    const statusHeaderStatus = formUtil.createGroup<StatusViewModel>({
      status: 'PENDING',
      sentToDpc: null
    });

    const statusHeader = formUtil.createGroup<StatusHeaderViewModel>({
      docNumber: '',
      status: statusHeaderStatus,
      bspName: '',
      airlCode: '',
      agentCode: '',
      appDate: '',
      amount: '',
      finalType: null,
      finalDaysLeft: null,
      finalProcessStopped: null
    });

    component.formData['form'] = formUtil.createGroup<RefundViewModel>({
      id: 1234,
      statusHeader,
      basicInformation: null,
      details: null,
      formPayment: null,
      amounts: null
    });

    component['dialogForm'] = formUtil.createGroup<any>({
      reasonForRefund: true
    });

    component.formData.isFormReady.next(true);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle pending fields if form is invalid', () => {
    spyOn<any>(component, 'handlePendingFields');

    component.onActionClick(RefundActionType.issue);

    expect(component.formData.isFormValid()).toBe(false);
    expect(component['handlePendingFields']).toHaveBeenCalled();
  });

  it('should navigate to refund edit', () => {
    spyOn<any>(component, 'onModifyClick');
    component.onActionClick(RefundActionType.edit);

    expect(component['onModifyClick']).toHaveBeenCalled();
  });

  it('should scroll to first invalid section', () => {
    component.accordion = new AccordionComponent();
    component.accordionSections = [{ nativeElement: { id: 'basicInformation' } }] as any;

    const firstSection = component.accordionSections[0];
    spyOn<any>(component.accordion, 'goToItem');

    component['scrollToFirstInvalidSection']();

    expect(component.accordion.goToItem).toHaveBeenCalledWith(firstSection);
  });

  it('should call multiple functions if handlePendingFields is called', () => {
    spyOn(FormUtil, 'showControlState');
    spyOn<any>(component, 'scrollToFirstInvalidSection');

    component['handlePendingFields']();

    expect(FormUtil.showControlState).toHaveBeenCalled();
    expect(component['scrollToFirstInvalidSection']).toHaveBeenCalled();
    expect(component['notificationService'].showError).toHaveBeenCalled();
  });

  it('should show a warning icon if tax are repeated', () => {
    component.onShowTaxWarning(true);
    expect(component.showTaxRepeatWarning).toBeTruthy();
  });

  it('should show a warning icon if tax are repeated', () => {
    component.onShowTaxWarning(false);
    expect(component.showTaxRepeatWarning).toBeFalsy();
  });

  it('should set headerHeight properly', () => {
    const testHeight = 30;
    component.onHeaderHeightChange(testHeight);

    expect(component.headerHeight).toBe(30);
  });

  it('should call initializeEditChangesListener if AccessType is edit', fakeAsync(() => {
    component.formConfig.accessType = AccessType.edit;
    spyOn<any>(component, 'initializeEditChangesListener');
    component.ngAfterViewInit();
    tick();

    expect(component['initializeEditChangesListener']).toHaveBeenCalled();
  }));

  it('should not call initializeEditChangesListener if AccessType is not edit', fakeAsync(() => {
    component.formConfig.accessType = AccessType.create;
    spyOn<any>(component, 'initializeEditChangesListener');
    component.ngAfterViewInit();
    tick();

    expect(component['initializeEditChangesListener']).not.toHaveBeenCalled();
  }));

  it('should set documentId retrieved', fakeAsync(() => {
    component['activatedRoute'].queryParams.subscribe();
    tick();

    expect(component['documentIdRetrieved']).toBe('12345');
  }));

  it('should set selected Bsp Id', fakeAsync(() => {
    let bspId: number;
    component['activatedRoute'].queryParams.subscribe(value => (bspId = value.id));

    component['initializeRouteParams']();
    tick();

    expect(component.selectedBspId).toBe(bspId);
  }));

  it('should set the correct message when comment is added', fakeAsync(() => {
    const message = {
      reason: ['message1', 'message2']
    };

    const mockActivities: Activity[] = [
      {
        attachments: [],
        author: null,
        creationDate: null,
        params: [{ name: '', value: '' }],
        type: ''
      }
    ];

    spyOn(component.refundService, 'addComment').and.returnValue(of(mockActivities));
    tick();

    component.onSendCommentRefund(message);

    expect(component.refundService.addComment).toHaveBeenCalledWith(message, 1234);
  }));

  it('should call service to download file', fakeAsync(() => {
    const mockFile = {
      blob: 'blob',
      fileName: 'fileName'
    };

    const downloadFileSpy = spyOn(component.refundService, 'downloadFile').and.returnValue(of(mockFile));
    const fileSaverSpy = spyOn(FileSaver, 'saveAs');

    component.onDownloadFile('url');
    tick();

    expect(downloadFileSpy).toHaveBeenCalled();
    expect(fileSaverSpy).toHaveBeenCalledWith('blob', 'fileName');
  }));

  it('should call showInformation if is Resubmission', fakeAsync(() => {
    component.formConfig.isResubmission = true;
    component.ngOnInit();
    tick();

    expect(notificationServiceSpy.showInformation).toHaveBeenCalled();
  }));

  it('should navigate to correct url when onModifyClick is called ', () => {
    const tabServiceCloseAndNavigateSpy = tabServiceSpy.closeCurrentTabAndNavigate.and.callThrough();
    const expectedUrl = '/refunds/app/edit/1234';

    component['onModifyClick']();

    expect(tabServiceCloseAndNavigateSpy).toHaveBeenCalledWith(expectedUrl);
  });

  it('should open discard confirmation dialog', () => {
    const openDiscardConfirmationDialogSpy = spyOn<any>(component, 'openDiscardConfirmationDialog').and.callThrough();
    const openDialogObservableSpy = spyOn<any>(component['dialog'], 'open');
    const expectedConfigData = {
      data: {
        footerButtonsType: FooterButton.Discard,
        hasCancelButton: true,
        isClosable: true
      }
    };
    component['openDiscardConfirmationDialog']();

    expect(openDiscardConfirmationDialogSpy).toHaveBeenCalled();
    expect(openDialogObservableSpy).toHaveBeenCalledWith(jasmine.anything(), expectedConfigData);
  });

  it('should refresh tab if discard button it is clicked', fakeAsync(() => {
    const btnClicked = {
      clickedBtn: FooterButton.Discard
    };
    const openDiscardConfirmationDialogSpy = spyOn<any>(component, 'openDiscardConfirmationDialog').and.returnValue(
      of(btnClicked)
    );
    const tabServiceSpyRefreshCurrentTab = tabServiceSpy.refreshCurrentTab.and.callThrough();

    component.onActionDiscardClick();
    tick();

    expect(openDiscardConfirmationDialogSpy).toHaveBeenCalled();
    expect(tabServiceSpyRefreshCurrentTab).toHaveBeenCalled();
  }));

  it('should remove previous subscription when opening again the dialog', fakeAsync(() => {
    component['dialogFormSubmitEmitterSubscription'] = new Subscription();
    component['dialogFormSubmitEmitterSubscription'].closed = false;

    const dialogFormSubmitEmitterSubscriptionUnsubscribeSpy = spyOn<any>(
      component['dialogFormSubmitEmitterSubscription'],
      'unsubscribe'
    ).and.returnValue(false);

    component['initializeDialogFormSubmitEmitterListener'](RefundActionType.edit);
    tick();

    expect(dialogFormSubmitEmitterSubscriptionUnsubscribeSpy).toHaveBeenCalled();
  }));

  it('should call save with the correct data', () => {
    const refundServiceSaveSpy = spyOn(component.refundService, 'save').and.callThrough();

    component['saveRefundData$'](refundItem);

    expect(refundServiceSaveSpy).toHaveBeenCalledWith(refundItem);
  });

  it('should call saveDialogData', () => {
    spyOn<any>(component.formData, 'isFormValid').and.returnValue(true);

    const btnClicked = {
      clickedBtn: FooterButton.Cancel
    };
    const openConfirmationDialogSpy = spyOn<any>(component, 'openConfirmationDialog').and.returnValue(of(btnClicked));
    const saveDialogDataSpy = spyOn<any>(component, 'saveDialogData');

    component.onActionClick(RefundActionType.resubmit);

    expect(openConfirmationDialogSpy).toHaveBeenCalled();
    expect(saveDialogDataSpy).toHaveBeenCalled();
  });

  //INFO(PPavlov): https://github.com/jasmine/jasmine/issues/1942
  //TODO(PPavlov): We should rewrite it
  xit('should call buildViewFromBEObject when saveDialogData is called', () => {
    const reasonForRefund = {
      reasonForRefund: true
    };

    const setBuildViewFromBEObjectSpy = jasmine.createSpy('setbuildViewFromBEObjectSpy');

    spyOn(util, 'buildViewFromBEObject').and.returnValue(setBuildViewFromBEObjectSpy);

    component['dialogForm'].patchValue(reasonForRefund);

    component['saveDialogData']();

    expect(setBuildViewFromBEObjectSpy).toHaveBeenCalled();
  });

  it('should call tabService closeCurrentTabAndNavigate when navigateToRefund is called', () => {
    const tabServiceSpyCloseAndNavigate = tabServiceSpy.closeCurrentTabAndNavigate.and.callThrough();
    const expectedUrl = '/refunds/notice/view/1234';

    component['navigateToRefund'](1234);

    expect(tabServiceSpyCloseAndNavigate).toHaveBeenCalledWith(expectedUrl);
  });

  it('should call notification service and navigate when handleSuccessOnSubmit is called', () => {
    const navigateToRefundSpy = spyOn<any>(component, 'navigateToRefund');

    component['handleSuccessOnSubmit'](refundItem, RefundActionType.edit);

    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
    expect(navigateToRefundSpy).toHaveBeenCalledWith(12345);
  });

  it('should open Errors Dialog', () => {
    const openErrorsDialogSpy = spyOn<any>(component, 'openErrorsDialog').and.callThrough();
    const openDialogObservableSpy = spyOn<any>(component['dialog'], 'open');
    const refundError: ResponseErrorBE = {
      timestamp: '',
      errorCode: 500,
      errorMessage: '',
      messages: []
    };

    const expectedConfigData = {
      data: {
        title: 'REFUNDS.issueDialog.refund-dialog-error.title',
        footerButtonsType: FooterButton.Ok_tertiary,
        rowTableContent: refundError,
        hasCancelButton: false,
        isClosable: true
      }
    };

    component['openErrorsDialog'](refundError);

    expect(openErrorsDialogSpy).toHaveBeenCalled();
    expect(openDialogObservableSpy).toHaveBeenCalledWith(jasmine.anything(), expectedConfigData);
  });

  it('handleErrorsOnSubmit error 400', () => {
    const handleErrorsOnSubmitSpy = spyOn<any>(component, 'handleErrorsOnSubmit').and.callThrough();
    const saveDialogDataSpy = spyOn<any>(component, 'saveDialogData');

    const espectedMessage = 'REFUNDS.error_msg.issue';

    component['handleErrorsOnSubmit'](error, RefundActionType.issue);

    expect(handleErrorsOnSubmitSpy).toHaveBeenCalled();
    expect(saveDialogDataSpy).toHaveBeenCalled();
    expect(notificationServiceSpy.showError).toHaveBeenCalledWith(espectedMessage);
  });

  it('handleErrorsOnSubmit error 500', () => {
    const handleErrorsOnSubmitSpy = spyOn<any>(component, 'handleErrorsOnSubmit').and.callThrough();
    const globalErrorHandlerServiceSpy = spyOn<any>(component['globalErrorHandlerService'], 'handleError');

    const error500 = { ...error, error: { ...error, errorCode: 500 } };

    component['handleErrorsOnSubmit'](error500, RefundActionType.issue);

    expect(handleErrorsOnSubmitSpy).toHaveBeenCalled();

    expect(globalErrorHandlerServiceSpy).toHaveBeenCalledWith(error500);
  });
});
