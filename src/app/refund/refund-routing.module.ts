import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RefundFilesComponent } from './components/refund-files/refund-files.component';
import { RefundMassloadAttachmentListComponent } from './components/refund-files/refund-massload-attachment-list/refund-massload-attachment-list.component';
import { RefundMassloadFileListComponent } from './components/refund-files/refund-massload-file-list/refund-massload-file-list.component';
import { RefundActiveListComponent } from './components/refund-list/refund-active-list/refund-active-list.component';
import { RefundArchivedListComponent } from './components/refund-list/refund-archived-list/refund-archived-list.component';
import { RefundDeletedListComponent } from './components/refund-list/refund-deleted-list/refund-deleted-list.component';
import { RefundListComponent } from './components/refund-list/refund-list.component';
import { RefundViewComponent } from './components/refund-view/refund-view.component';
import { RefundComponent } from './components/refund/refund.component';
import { RefundItemResolver } from './resolvers/refund-item-resolver.service';
import { refundFilesPermission } from './shared/helpers/refund-permissions.config';
import { getRefundTabLabel } from './shared/helpers/refund.helper';
import { AccessType } from '~app/shared/enums/access-type.enum';
import { ROUTES } from '~app/shared/constants/routes';
import { MasterDataType } from '~app/master-shared/models/master.model';

const getListSubtabsChildren = (refundType: MasterDataType): Routes => [
  {
    path: 'active',
    component: RefundActiveListComponent,
    data: {
      title: 'REFUNDS.query.commons.activeTabTitle',
      refundType
    }
  },
  {
    path: 'archive',
    component: RefundArchivedListComponent,
    data: {
      title: 'REFUNDS.query.commons.archiveTabTitle',
      refundType
    }
  },
  {
    path: 'deleted',
    component: RefundDeletedListComponent,
    data: {
      title: 'REFUNDS.query.commons.deletedTabTitle',
      refundType
    }
  },
  { path: '', redirectTo: 'active', pathMatch: 'full' }
];

const refundsFilesSubtabsChildren: Routes = [
  {
    path: 'massload',
    component: RefundMassloadFileListComponent,
    data: {
      title: 'REFUNDS.files.massload.tabLabel',
      requiredPermissions: refundFilesPermission.raMassRead
    }
  },
  {
    path: 'massload-attachments',
    component: RefundMassloadAttachmentListComponent,
    data: { title: 'REFUNDS.files.massloadAttachments.tabLabel', requiredPermissions: refundFilesPermission.raAttRead }
  },
  { path: '', redirectTo: 'massload', pathMatch: 'full' }
];

const routes: Routes = [
  {
    path: ROUTES.REFUNDS_APP_QUERY.path,
    component: RefundListComponent,
    data: {
      tab: ROUTES.REFUNDS_APP_QUERY,
      refundType: MasterDataType.RefundApp
    },
    children: getListSubtabsChildren(MasterDataType.RefundApp)
  },
  {
    path: ROUTES.REFUNDS_APP_ISSUE.path,
    component: RefundComponent,
    data: {
      tab: ROUTES.REFUNDS_APP_ISSUE,
      access: AccessType.create,
      refundType: MasterDataType.RefundApp
    }
  },
  {
    path: ROUTES.REFUNDS_APP_ISSUE_TICKET.path,
    component: RefundComponent,
    data: {
      tab: ROUTES.REFUNDS_APP_ISSUE_TICKET,
      access: AccessType.create,
      refundType: MasterDataType.RefundApp
    }
  },
  {
    path: ROUTES.REFUNDS_APP_ISSUE_PENDING.path,
    component: RefundComponent,
    data: {
      tab: ROUTES.REFUNDS_APP_ISSUE_PENDING,
      access: AccessType.create,
      refundType: MasterDataType.RefundApp,
      issuingPendingSupervision: true
    }
  },
  {
    path: ROUTES.REFUNDS_APP_VIEW.path,
    component: RefundViewComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_APP_VIEW, getTabLabel: getRefundTabLabel },
      refundType: MasterDataType.RefundApp
    },
    resolve: {
      item: RefundItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.REFUNDS_APP_EDIT.path,
    component: RefundComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_APP_EDIT, getTabLabel: getRefundTabLabel },
      access: AccessType.edit,
      refundType: MasterDataType.RefundApp
    },
    resolve: {
      item: RefundItemResolver
    }
  },
  {
    path: ROUTES.REFUNDS_APP_SUPERVISE.path,
    component: RefundComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_APP_SUPERVISE, getTabLabel: getRefundTabLabel },
      access: AccessType.edit,
      refundType: MasterDataType.RefundApp,
      isSupervise: true
    },
    resolve: {
      item: RefundItemResolver
    }
  },
  {
    path: ROUTES.REFUNDS_APP_INVESTIGATE.path,
    component: RefundComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_APP_INVESTIGATE, getTabLabel: getRefundTabLabel },
      access: AccessType.edit,
      isInvestigate: true,
      refundType: MasterDataType.RefundApp
    },
    resolve: {
      item: RefundItemResolver
    }
  },
  {
    path: ROUTES.REFUNDS_APP_AUTHORIZE.path,
    component: RefundComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_APP_AUTHORIZE, getTabLabel: getRefundTabLabel },
      access: AccessType.edit,
      isAuthorize: true,
      refundType: MasterDataType.RefundApp
    },
    resolve: {
      item: RefundItemResolver
    }
  },
  {
    path: ROUTES.REFUNDS_APP_RESUBMISSION.path,
    component: RefundComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_APP_RESUBMISSION },
      access: AccessType.create,
      isResubmission: true,
      refundType: MasterDataType.RefundApp
    },
    resolve: {
      item: RefundItemResolver
    }
  },
  {
    path: ROUTES.REFUNDS_NOTICE_VIEW.path,
    component: RefundViewComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_NOTICE_VIEW, getTabLabel: getRefundTabLabel },
      refundType: MasterDataType.RefundNotice
    },
    resolve: {
      item: RefundItemResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: ROUTES.REFUNDS_NOTICE_EDIT.path,
    component: RefundComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_NOTICE_EDIT, getTabLabel: getRefundTabLabel },
      access: AccessType.edit,
      refundType: MasterDataType.RefundNotice
    },
    resolve: {
      item: RefundItemResolver
    }
  },
  {
    path: ROUTES.REFUNDS_NOTICE_SUPERVISE.path,
    component: RefundComponent,
    data: {
      tab: { ...ROUTES.REFUNDS_NOTICE_SUPERVISE, getTabLabel: getRefundTabLabel },
      access: AccessType.edit,
      refundType: MasterDataType.RefundNotice,
      isSupervise: true
    },
    resolve: {
      item: RefundItemResolver
    }
  },
  {
    path: ROUTES.REFUNDS_NOTICE_QUERY.path,
    component: RefundListComponent,
    data: {
      tab: ROUTES.REFUNDS_NOTICE_QUERY,
      refundType: MasterDataType.RefundNotice
    },
    children: getListSubtabsChildren(MasterDataType.RefundNotice)
  },
  {
    path: ROUTES.REFUNDS_NOTICE_ISSUE.path,
    component: RefundComponent,
    data: {
      tab: ROUTES.REFUNDS_NOTICE_ISSUE,
      access: AccessType.create,
      refundType: MasterDataType.RefundNotice
    }
  },
  {
    path: ROUTES.REFUNDS_NOTICE_ISSUE_TICKET.path,
    component: RefundComponent,
    data: {
      tab: ROUTES.REFUNDS_NOTICE_ISSUE_TICKET,
      access: AccessType.create,
      refundType: MasterDataType.RefundNotice
    }
  },
  {
    path: ROUTES.REFUNDS_NOTICE_ISSUE_PENDING.path,
    component: RefundComponent,
    data: {
      tab: ROUTES.REFUNDS_NOTICE_ISSUE_PENDING,
      access: AccessType.create,
      refundType: MasterDataType.RefundNotice,
      issuingPendingSupervision: true
    }
  },
  {
    path: ROUTES.REFUNDS_FILES.path,
    component: RefundFilesComponent,
    data: { tab: ROUTES.REFUNDS_FILES },
    children: refundsFilesSubtabsChildren
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RefundRoutingModule {}
