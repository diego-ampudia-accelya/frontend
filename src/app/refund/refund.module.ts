import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RefundConfirmationDialogComponent } from '~app/refund/components/dialogs/refund-confirmation-dialog/refund-confirmation-dialog.component';
import { RefundDiscardDialogComponent } from '~app/refund/components/dialogs/refund-discard-dialog/refund-discard-dialog.component';
import { RefundInternalCommentDialogComponent } from '~app/refund/components/dialogs/refund-internal-comment-dialog/refund-internal-comment-dialog.component';
import { RefundIssueErrorDialogComponent } from '~app/refund/components/dialogs/refund-issue-error-dialog/refund-issue-error-dialog.component';
import { RefundMultipleConfirmationDialogComponent } from '~app/refund/components/dialogs/refund-multiple-confirmation-dialog/refund-multiple-confirmation-dialog.component';
import { RefundRejectDialogComponent } from '~app/refund/components/dialogs/refund-reject-dialog/refund-reject-dialog.component';
import { RefundUploadMassloadDialogComponent } from '~app/refund/components/refund-files/refund-massload-file-list/refund-upload-massload-file-dialog/refund-upload-massload-file-dialog';
import { ActivityPanelModule } from '~app/shared/components/activity-panel/activity-panel.module';
import { SharedModule } from '~app/shared/shared.module';
import { RefundActivityHistoryDialogComponent } from './components/dialogs/refund-activity-history-dialog/refund-activity-history-dialog.component';
import { RefundBspSelectionDialogComponent } from './components/dialogs/refund-bsp-selection-dialog/refund-bsp-selection-dialog.component';
import { RefundNotFoundDialogComponent } from './components/dialogs/refund-not-found-dialog/refund-not-found-dialog.component';
import { RefundAmountTaxComponent } from './components/refund-amount-tax/refund-amount-tax.component';
import { RefundBasicInfoComponent } from './components/refund-basic-info/refund-basic-info.component';
import { RefundDetailsComponent } from './components/refund-details/refund-details.component';
import { RefundFilesComponent } from './components/refund-files/refund-files.component';
import { RefundMassloadAttachmentListComponent } from './components/refund-files/refund-massload-attachment-list/refund-massload-attachment-list.component';
import { RefundMassloadFileListComponent } from './components/refund-files/refund-massload-file-list/refund-massload-file-list.component';
import { RefundMassloadFileTotalsComponent } from './components/refund-files/refund-massload-file-list/refund-massload-file-totals/refund-massload-file-totals.component';
import { RefundFormPaymentComponent } from './components/refund-form-payment/refund-form-payment.component';
import { RefundHeaderStatusComponent } from './components/refund-header-status/refund-header-status.component';
import { RefundActiveListComponent } from './components/refund-list/refund-active-list/refund-active-list.component';
import { RefundArchivedListComponent } from './components/refund-list/refund-archived-list/refund-archived-list.component';
import { RefundDeletedListComponent } from './components/refund-list/refund-deleted-list/refund-deleted-list.component';
import { RefundListComponent } from './components/refund-list/refund-list.component';
import { RefundViewComponent } from './components/refund-view/refund-view.component';
import { RefundComponent } from './components/refund/refund.component';
import { RefundRoutingModule } from './refund-routing.module';
import { RefundItemResolver } from './resolvers/refund-item-resolver.service';
import { RefundDialogService } from './services/refund-dialog.service';
import { RefundHistoryService } from './services/refund-history.service';
import { RefundInternalCommentDialogService } from './services/refund-internal-comment-dialog.service';
import { RefundMassloadFileService } from './services/refund-massload-file.service';
import { RefundUploadMassloadFileDialogService } from './services/refund-upload-massload-file-dialog.service';
import { RefundContactEffects } from './store/effects/refund-contact.effects';
import * as fromRefund from './store/reducers';

@NgModule({
  declarations: [
    RefundComponent,
    RefundBasicInfoComponent,
    RefundDetailsComponent,
    RefundFormPaymentComponent,
    RefundAmountTaxComponent,
    RefundHeaderStatusComponent,
    RefundListComponent,
    RefundActiveListComponent,
    RefundDeletedListComponent,
    RefundViewComponent,
    RefundIssueErrorDialogComponent,
    RefundDiscardDialogComponent,
    RefundConfirmationDialogComponent,
    RefundMultipleConfirmationDialogComponent,
    RefundInternalCommentDialogComponent,
    RefundRejectDialogComponent,
    RefundActivityHistoryDialogComponent,
    RefundNotFoundDialogComponent,
    RefundArchivedListComponent,
    RefundFilesComponent,
    RefundMassloadFileListComponent,
    RefundMassloadFileTotalsComponent,
    RefundUploadMassloadDialogComponent,
    RefundMassloadAttachmentListComponent,
    RefundBspSelectionDialogComponent
  ],
  imports: [
    CommonModule,
    RefundRoutingModule,
    ActivityPanelModule,
    SharedModule,
    StoreModule.forFeature(fromRefund.refundFeatureKey, fromRefund.reducers),
    EffectsModule.forFeature([RefundContactEffects])
  ],
  providers: [
    RefundItemResolver,
    RefundDialogService,
    RefundHistoryService,
    RefundMassloadFileService,
    RefundUploadMassloadFileDialogService,
    RefundInternalCommentDialogService
  ]
})
export class RefundModule {}
