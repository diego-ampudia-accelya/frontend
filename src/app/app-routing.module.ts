import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundGuard } from './auth/services/not-found.guard';
import { PermissionsGuard } from './auth/services/permissions.guard';
import { UnauthenticatedGuard } from './auth/services/unauthenticated.guard';
import { LoginErrorComponent } from './core/components/login-error/login-error.component';
import { CoreComponent } from './core/core.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { UserFeedbackGuard } from '~app/user-feedback/services/user-feedback-guard.service';
import { ROUTES } from '~app/shared/constants';
import { AuthGuard } from '~app/auth/services/auth.guard';

const routes: Routes = [
  {
    path: ROUTES.TRAINING.path,
    loadChildren: () => import('~app/training/training.module').then(m => m.TrainingModule)
  },
  {
    path: ROUTES.USER_FEEDBACK.path,
    loadChildren: () => import('~app/user-feedback/user-feedback.module').then(m => m.UserFeedbackModule)
  },
  {
    path: 'style-guide',
    loadChildren: () => import('~app/style-guide/style-guide.module').then(m => m.StyleGuideModule)
  },
  {
    path: ROUTES.ERROR.path,
    component: LoginErrorComponent,
    canActivate: [UnauthenticatedGuard]
  },
  {
    path: '',
    canActivate: [AuthGuard, PermissionsGuard, UserFeedbackGuard],
    canActivateChild: [PermissionsGuard],
    component: CoreComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: ROUTES.HELP.path,
        loadChildren: () => import('./help/help.module').then(m => m.HelpModule),
        data: { tab: ROUTES.HELP }
      },
      {
        path: ROUTES.SEARCH.path,
        loadChildren: () => import('./search/search.module').then(m => m.SearchModule)
      },
      {
        path: ROUTES.TERMS.path,
        loadChildren: () =>
          import('./terms-and-conditions/terms-and-conditions.module').then(m => m.TermsAndConditionsModule)
      },
      {
        path: ROUTES.ACDMS.path,
        loadChildren: () => import('./adm-acm/adm-acm.module').then(m => m.AdmAcmModule)
      },
      {
        path: ROUTES.BSP_ADJUSTMENTS.path,
        loadChildren: () => import('./bsp-adjustment/bsp-adjustment.module').then(m => m.BspAdjustmentModule)
      },
      {
        path: ROUTES.REFUNDS.path,
        loadChildren: () => import('./refund/refund.module').then(m => m.RefundModule)
      },
      {
        path: ROUTES.MASTER_DATA.path,
        loadChildren: () => import('./master-data/master-data.module').then(m => m.MasterDataModule)
      },
      {
        path: ROUTES.DOCUMENTS.path,
        loadChildren: () => import('./document/document.module').then(m => m.DocumentModule)
      },
      {
        path: ROUTES.POST_BILLING_DISPUTE_LIST.path,
        loadChildren: () => import('./pbd/pbd.module').then(m => m.PbdModule)
      },
      {
        path: ROUTES.FILES.path,
        loadChildren: () => import('./files/files.module').then(m => m.FilesModule)
      },
      {
        path: ROUTES.SALES.path,
        loadChildren: () => import('./sales/sales.module').then(m => m.SalesModule)
      },
      {
        path: ROUTES.TIP.path,
        loadChildren: () => import('./tip/tip.module').then(m => m.TipModule)
      },
      {
        path: ROUTES.WECHAT.path,
        loadChildren: () => import('./wechat/wechat.module').then(m => m.WechatModule)
      },
      {
        path: ROUTES.USER_PROFILE.path,
        loadChildren: () => import('./user-profile/user-profile.module').then(m => m.UserProfileModule)
      },
      {
        path: ROUTES.NOTIFICATIONS_LIST.path,
        loadChildren: () => import('./notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: '**',
        component: NotFoundComponent,
        canActivate: [NotFoundGuard]
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
