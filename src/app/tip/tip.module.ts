import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import { TipReportService } from '~app/master-data/configuration/tip/tip-reports/services/tip-report.service';
import { SharedModule } from '~app/shared/shared.module';
import * as fromTip from './reducers';
import { TipCardConsentModificationDialogComponent } from './tip-cards/dialogs/tip-card-consent-modification-dialog/tip-card-consent-modification-dialog.component';
import { TipCardConsentModificationDialogService } from './tip-cards/dialogs/tip-card-consent-modification-dialog/tip-card-consent-modification-dialog.service';
import { TipCardConsentTerminationDialogComponent } from './tip-cards/dialogs/tip-card-consent-termination-dialog/tip-card-consent-termination-dialog.component';
import { TipCardConsentTerminationDialogService } from './tip-cards/dialogs/tip-card-consent-termination-dialog/tip-card-consent-termination-dialog.service';
import { TipCardCreationDialogComponent } from './tip-cards/dialogs/tip-card-creation-dialog/tip-card-creation-dialog.component';
import { TipCardCreationDialogService } from './tip-cards/dialogs/tip-card-creation-dialog/tip-card-creation-dialog.service';
import { TipCardModificationDialogComponent } from './tip-cards/dialogs/tip-card-modification-dialog/tip-card-modification-dialog.component';
import { TipCardModificationDialogService } from './tip-cards/dialogs/tip-card-modification-dialog/tip-card-modification-dialog.service';
import { TipCardTerminationDialogComponent } from './tip-cards/dialogs/tip-card-termination-dialog/tip-card-termination-dialog.component';
import { TipCardTerminationDialogService } from './tip-cards/dialogs/tip-card-termination-dialog/tip-card-termination-dialog.service';
import { TipCardConsentAirlineDetailResolver } from './tip-cards/resolvers/tip-card-consent-airline-detail.resolver';
import { TipCardConsentService } from './tip-cards/services/tip-card-consent.service';
import { TipCardService } from './tip-cards/services/tip-card.service';
import { TipCardEffects } from './tip-cards/store/effects/tip-card.effects';
import { TipCardConsentAirlineDetailComponent } from './tip-cards/tip-card-consent-airline-detail/tip-card-consent-airline-detail.component';
import { TipCardConsentListComponent } from './tip-cards/tip-card-consent-list/tip-card-consent-list.component';
import { TipCardListComponent } from './tip-cards/tip-card-list/tip-card-list.component';
import { TipCardsComponent } from './tip-cards/tip-cards.component';
import { TipGlobalFilesDialogComponent } from './tip-global-files/dialog/tip-global-files-dialog.component';
import { TipGlobalFilesGuard } from './tip-global-files/services/tip-global-files.guard';
import { TipGlobalFilesService } from './tip-global-files/services/tip-global-files.service';
import { TipProductAddAgentConsentDialogComponent } from './tip-prov-products/components/dialogs/tip-product-add-agent-consent-dialog/tip-product-add-agent-consent-dialog.component';
import { TipProductModifyConsentDialogComponent } from './tip-prov-products/components/dialogs/tip-product-modify-consent-dialog/tip-product-modify-consent-dialog.component';
import { TipProductTerminateConsentDialogComponent } from './tip-prov-products/components/dialogs/tip-product-terminate-consent-dialog/tip-product-terminate-consent-dialog.component';
import { TipProductConsentEffects } from './tip-prov-products/components/store/effects/tip-product-consent.effects';
import { TipProductConsentListComponent } from './tip-prov-products/components/tip-product-consents-list/tip-product-consent-list.component';
import { TipProductDetailComponent } from './tip-prov-products/components/tip-product-detail/tip-product-detail.component';
import { TipProductListComponent } from './tip-prov-products/components/tip-product-list/tip-product-list.component';
import { TipProductsComponent } from './tip-prov-products/components/tip-products.component';
import { TipProviderDetailComponent } from './tip-prov-products/components/tip-provider-detail/tip-provider-detail.component';
import { TipProductResolver } from './tip-prov-products/resolvers/tip-product-resolver.service';
import { TipProviderResolver } from './tip-prov-products/resolvers/tip-provider-resolver.service';
import { TipProductConsentService } from './tip-prov-products/services/tip-product-consent.service';
import { TipProductDialogService } from './tip-prov-products/services/tip-product-dialog.service';
import { TipProductService } from './tip-prov-products/services/tip-product.service';
import { TipProviderService } from './tip-prov-products/services/tip-provider.service';
import { TipReportsConfigurationListComponent } from './tip-reports-configuration/tip-reports-configuration-list/tip-reports-configuration-list.component';
import { TipRoutingModule } from './tip-routing.module';

@NgModule({
  declarations: [
    TipProductsComponent,
    TipProductListComponent,
    TipCardsComponent,
    TipCardListComponent,
    TipCardConsentListComponent,
    TipProductDetailComponent,
    TipProviderDetailComponent,
    TipCardConsentAirlineDetailComponent,
    TipProductConsentListComponent,
    TipProductAddAgentConsentDialogComponent,
    TipProductModifyConsentDialogComponent,
    TipProductTerminateConsentDialogComponent,
    TipCardCreationDialogComponent,
    TipCardModificationDialogComponent,
    TipCardTerminationDialogComponent,
    TipCardConsentModificationDialogComponent,
    TipCardConsentTerminationDialogComponent,
    TipCardConsentAirlineDetailComponent,
    TipProductConsentListComponent,
    TipGlobalFilesDialogComponent,
    TipReportsConfigurationListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TipRoutingModule,
    StoreModule.forFeature(fromTip.tipFeatureKey, fromTip.reducers),
    EffectsModule.forFeature([TipProductConsentEffects, TipCardEffects])
  ],
  providers: [
    TipCardService,
    TipProductService,
    TipProviderService,
    TipProductResolver,
    TipProviderResolver,
    TipProductConsentService,
    TipCardConsentService,
    TipProductDialogService,
    TipCardCreationDialogService,
    TipCardModificationDialogService,
    TipCardTerminationDialogService,
    TipCardConsentModificationDialogService,
    TipCardConsentTerminationDialogService,
    TipCardConsentAirlineDetailResolver,
    TipProductConsentService,
    TipCardConsentService,
    TipGlobalFilesGuard,
    TipGlobalFilesService,
    TipReportService,
    AdmAcmService
  ]
})
export class TipModule {}
