export interface TipCurrency {
  id: number;
  code: string;
  decimals: number;
  isDefault: boolean;
}
