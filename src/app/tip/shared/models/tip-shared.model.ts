/* eslint-disable @typescript-eslint/naming-convention */
export enum CostRecoveryType {
  FixedAmount = 'fixedAmount',
  Percentage = 'percentage'
}
