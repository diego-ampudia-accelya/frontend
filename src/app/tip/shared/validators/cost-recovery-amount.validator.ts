import { AbstractControl, ValidatorFn } from '@angular/forms';

import { tipLocalAirlineFixAmountValidator } from '~app/master-data/configuration/tip/tip-local-airline/tip-local-airline/validators/tip-local-airline-fix-amount.validator';
import { tipLocalAirlinePercentageValidator } from '~app/master-data/configuration/tip/tip-local-airline/tip-local-airline/validators/tip-local-airline-percentage.validator';
import { Currency } from '~app/shared/models/currency.model';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';

/**
 * Validates if Cost Recovery amount is valid according to Cost Recovery Type and Currency. Conditions are:
 * ```
 * if Type is Percentage, tipLocalAirlinePercentageValidator is applied.
 * if Type is FixedAmount, tipLocalAirlineFixAmountValidator is applied using Currency.
 ```
 *
 * @param costRecoveryTypeControl control holding Cost Recovery Type value.
 * @param currencyControl control holding Currency value.
 * @returns result of applying validator functions or null if none applied.
 */
export function costRecoveryAmountValidator(
  costRecoveryTypeControl: AbstractControl,
  currencyControl: AbstractControl
): ValidatorFn {
  return (amountControl: AbstractControl): { [key: string]: any } => {
    const costRecoveryType: CostRecoveryType = costRecoveryTypeControl.value;
    const currency: Currency = currencyControl.value;

    if (costRecoveryType && amountControl.value) {
      if (costRecoveryType === CostRecoveryType.FixedAmount && currency?.decimals >= 0) {
        return tipLocalAirlineFixAmountValidator(currency.decimals)(amountControl);
      }

      if (costRecoveryType === CostRecoveryType.Percentage) {
        return tipLocalAirlinePercentageValidator(amountControl);
      }
    }

    return null;
  };
}
