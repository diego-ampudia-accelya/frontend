import { MenuTab, MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';

export function getTipCardAirlineTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;

  return { label: tab.tabLabel as string, params: { airlineName: routeData.item?.airlines[0].localName } };
}
