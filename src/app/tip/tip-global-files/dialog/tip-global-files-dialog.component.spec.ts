import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { of, throwError } from 'rxjs';
import { GlobalErrorHandlerService } from '~app/core/services';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { AppConfigurationService, NotificationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';
import { TipGlobalFilesService } from '../services/tip-global-files.service';
import { TipGlobalFilesDialogComponent } from './tip-global-files-dialog.component';

const mockConfig = {
  data: {
    title: 'title',
    footerButtonsType: FooterButton.Accept,
    buttons: [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel'
      },
      {
        title: 'BUTTON.DEFAULT.ACCEPT',
        buttonDesign: 'primary',
        type: 'accept'
      }
    ]
  }
} as DialogConfig;

describe('TipGlobalFilesDialogComponent', () => {
  let component: TipGlobalFilesDialogComponent;
  let fixture: ComponentFixture<TipGlobalFilesDialogComponent>;
  const dialogServiceSpy: SpyObject<DialogService> = createSpyObject(DialogService);
  const tipGlobalFilesServiceSpy: SpyObject<TipGlobalFilesService> = createSpyObject(TipGlobalFilesService);
  const notificationServiceSpy: SpyObject<NotificationService> = createSpyObject(NotificationService);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TipGlobalFilesDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      providers: [
        mockProvider(L10nTranslationService),
        FormBuilder,
        ReactiveSubject,
        AppConfigurationService,
        GlobalErrorHandlerService,
        HttpClient,
        { provide: NotificationService, useValue: notificationServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: DialogConfig, useValue: mockConfig },
        { provide: TipGlobalFilesService, useValue: tipGlobalFilesServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipGlobalFilesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should call notificationService when initializeButtonListeners function is called', fakeAsync(() => {
    spyOn<any>(component['acceptButtonClick$'], 'pipe').and.returnValue(of({}));

    component['initializeButtonListeners']();
    component['acceptButtonClick$'].subscribe();

    tick();

    expect(notificationServiceSpy.showSuccess).toHaveBeenCalled();
  }));

  it('should call handleErrorOnsubmit when initializeButtonListeners function is called', fakeAsync(() => {
    spyOn<any>(component['acceptButtonClick$'], 'pipe').and.returnValue(throwError({ status: 401 }));
    const handleErrorOnsubmitSpy = spyOn<any>(component, 'handleErrorOnsubmit');

    component['initializeButtonListeners']();
    component['acceptButtonClick$'].subscribe();

    tick();

    expect(handleErrorOnsubmitSpy).toHaveBeenCalled();
  }));

  it('should call service requestConsentFile when requestConsentFile is called', () => {
    component['requestConsentFile']();

    expect(tipGlobalFilesServiceSpy.requestConsentFile).toHaveBeenCalled();
  });

  it('should call service requestReportFile when requestReportFile is called', () => {
    component['requestReportFile']();

    expect(tipGlobalFilesServiceSpy.requestReportFile).toHaveBeenCalled();
  });

  it('should call globalErrorHandlerService handleError when handleErrorOnsubmit is called', () => {
    const handleErrorSpy = spyOn<any>(component['globalErrorHandlerService'], 'handleError');
    component['handleErrorOnsubmit'](new Error());

    expect(handleErrorSpy).toHaveBeenCalled();
  });
});
