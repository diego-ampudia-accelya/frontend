import { Component, ErrorHandler, Inject, OnDestroy, OnInit } from '@angular/core';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services/notification.service';
import { TipGlobalFilesService } from '../services/tip-global-files.service';

@Component({
  selector: 'bspl-tip-global-files-dialog',
  templateUrl: './tip-global-files-dialog.component.html'
})
export class TipGlobalFilesDialogComponent implements OnInit, OnDestroy {
  private acceptButton: ModalAction;
  private isConsentType: boolean;

  private destroy$ = new Subject();

  private acceptButtonClick$ = this.reactiveSubject.asObservable.pipe(
    filter(action => action?.clickedBtn === FooterButton.Accept),
    takeUntil(this.destroy$)
  );

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private tipGlobalFilesService: TipGlobalFilesService,
    private globalErrorHandlerService: ErrorHandler
  ) {}

  public ngOnInit() {
    this.initializeCongfigFeatures();
    this.initializeButtonListeners();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeCongfigFeatures(): void {
    this.acceptButton = this.config.data.buttons.find(button => button.type === FooterButton.Accept);
    this.isConsentType = this.config.data.isConsentType;
  }

  private initializeButtonListeners(): void {
    this.acceptButtonClick$
      .pipe(
        switchMap(() => (this.isConsentType ? this.requestConsentFile() : this.requestReportFile())),
        takeUntil(this.destroy$)
      )
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.translationService.translate('TIP.globalFiles.successMessage'));
          this.dialogService.close();
        },
        error => {
          this.handleErrorOnsubmit(error);
        }
      );
  }

  private requestConsentFile(): Observable<any> {
    this.acceptButton.isLoading = true;

    return this.tipGlobalFilesService.requestConsentFile();
  }

  private requestReportFile(): Observable<any> {
    this.acceptButton.isLoading = true;

    return this.tipGlobalFilesService.requestReportFile();
  }

  private handleErrorOnsubmit(error) {
    this.globalErrorHandlerService.handleError(error);
    this.dialogService.close();
  }
}
