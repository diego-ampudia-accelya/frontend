export enum TipGlobalFileType {
  Consent = 'consent',
  Report = 'report'
}
