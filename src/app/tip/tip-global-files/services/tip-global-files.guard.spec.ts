import { TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { DialogService } from '~app/shared/components';
import { TipGlobalFileType } from '../model/tip-global-files.model';
import { TipGlobalFilesGuard } from './tip-global-files.guard';

describe('TipGlobalFilesGuard', () => {
  let tipGlobalFilesGuard: TipGlobalFilesGuard;
  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSnapshotSpy: SpyObject<ActivatedRouteSnapshot> = createSpyObject(ActivatedRouteSnapshot);

  routerSnapshotSpy.data = {
    tipGlobalFilesType: TipGlobalFileType.Consent
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        TipGlobalFilesGuard,
        mockProvider(L10nTranslationService),
        { provide: ActivatedRouteSnapshot, useValue: routerSnapshotSpy },
        { provide: DialogService, useValue: dialogServiceSpy }
      ]
    });

    tipGlobalFilesGuard = TestBed.inject(TipGlobalFilesGuard);
  }));

  it('should create', () => {
    expect(tipGlobalFilesGuard).toBeTruthy();
  });

  it('should open dialog', () => {
    tipGlobalFilesGuard.canActivate(routerSnapshotSpy);

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  });
});
