import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class TipGlobalFilesService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public requestConsentFile(): Observable<any> {
    const postUrl = `${this.baseUrl}/global-consents-file`;

    return this.http.post(postUrl, {});
  }

  public requestReportFile(): Observable<any> {
    const postUrl = `${this.baseUrl}/global-report-file`;

    return this.http.post(postUrl, {});
  }
}
