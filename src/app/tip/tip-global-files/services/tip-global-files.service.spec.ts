import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { AppConfigurationService } from '~app/shared/services';
import { TipGlobalFilesService } from './tip-global-files.service';

describe('TipGlobalFilesService', () => {
  let service: TipGlobalFilesService;
  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        TipGlobalFilesService,
        mockProvider(AppConfigurationService, { baseApiPath: '' }),
        { provide: HttpClient, useValue: httpClientSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(TipGlobalFilesService);
    httpClientSpy.get.calls.reset();
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should send proper requestConsentFile request', fakeAsync(() => {
    const expectedUrl = '/tip-management/global-consents-file';

    service.requestConsentFile();

    tick();

    expect(httpClientSpy.post).toHaveBeenCalledWith(expectedUrl, {});
  }));

  it('should send proper requestReportFile request', fakeAsync(() => {
    const expectedUrl = '/tip-management/global-report-file';

    service.requestReportFile();

    tick();

    expect(httpClientSpy.post).toHaveBeenCalledWith(expectedUrl, {});
  }));
});
