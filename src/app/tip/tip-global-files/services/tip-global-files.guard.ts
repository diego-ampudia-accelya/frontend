import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';
import { DialogService, FooterButton } from '~app/shared/components';
import { TipGlobalFilesDialogComponent } from '../dialog/tip-global-files-dialog.component';
import { TipGlobalFileType } from '../model/tip-global-files.model';

@Injectable()
export class TipGlobalFilesGuard implements CanActivate {
  constructor(private dialogService: DialogService, private translationService: L10nTranslationService) {}

  public canActivate(activatedRoute: ActivatedRouteSnapshot): Observable<boolean> {
    const tipGlobalFilesType = activatedRoute.data.tipGlobalFilesType;

    return this.dialogService.open(TipGlobalFilesDialogComponent, {
      data: {
        title: this.translationService.translate(`TIP.globalFiles.${tipGlobalFilesType}.title`),
        footerButtonsType: FooterButton.Accept,
        disclaimer: this.translationService.translate(`TIP.globalFiles.${tipGlobalFilesType}.disclaimer`),
        isConsentType: tipGlobalFilesType === TipGlobalFileType.Consent
      }
    });
  }
}
