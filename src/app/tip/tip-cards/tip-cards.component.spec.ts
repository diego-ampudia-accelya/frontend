import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { TipCardActions } from './store/actions';
import { canCreateCard } from './store/selectors/tip-card.selectors';
import { TipCardsComponent } from './tip-cards.component';
import { TranslatePipeMock } from '~app/test';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';

describe('TipCardsComponent', () => {
  let component: TipCardsComponent;
  let fixture: ComponentFixture<TipCardsComponent>;
  let mockStore: MockStore;

  const menuBuilderSpy = createSpyObject(MenuBuilder);

  const mockActivatedRoute = {
    snapshot: {
      routeConfig: {
        children: [{ path: 'list' }, { path: 'consent/list' }]
      }
    }
  };

  const initialState = {
    tip: {
      'tip-card-list-list': {}
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TipCardsComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore({ initialState, selectors: [{ selector: canCreateCard, value: true }] }),
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: MenuBuilder, useValue: menuBuilderSpy }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should initialize tabs correctly on init', () => {
    const tabs: RoutedMenuItem[] = [
      { title: 'TIP.cards.list.tabLabel', route: 'list', isAccessible: true },
      { title: 'TIP.cards.consent.tabLabel', route: 'consent/list', isAccessible: true }
    ];

    menuBuilderSpy.buildMenuItemsFrom.and.returnValue(tabs);

    component.ngOnInit();
    expect(component.tabs).toEqual(tabs);
  });

  it('should dispatch create card action on create card button click', () => {
    component.onCreateCard();
    expect(mockStore.dispatch).toHaveBeenCalledWith(TipCardActions.createCard());
  });
});
