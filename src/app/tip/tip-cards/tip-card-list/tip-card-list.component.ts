import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { MenuItem } from 'primeng/api';
import { Observable } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ViewListActions } from '~app/core/actions';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ButtonDesign, DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants';
import { Permissions } from '~app/shared/constants/permissions';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { createTabViewListId } from '~app/shared/helpers/create-tab-view-list-id.helper';
import { AgentDictionaryQueryFilters, AgentSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { listKeys, listSelectors, State } from '~app/tip/reducers';
import { getReadTipCardListPermissions, permissions } from '../config/tip-card-permissions.config';
import { TipCardModificationDialogService } from '../dialogs/tip-card-modification-dialog/tip-card-modification-dialog.service';
import { TipCardTerminationDialogService } from '../dialogs/tip-card-termination-dialog/tip-card-termination-dialog.service';
import { CardLevelConsent } from '../models/tip-card-consent.model';
import { TipCardFilter } from '../models/tip-card-filter.model';
import {
  CardLevelConsentType,
  TipCard,
  TipCardTerminationReason,
  TipCardType,
  TipStatus
} from '../models/tip-card.model';
import { TipCardFormatter } from '../services/tip-card-formatter.service';
import { TipCardService } from '../services/tip-card.service';
import { TipCardActions } from '../store/actions';
import { tipCardNumberValidator } from '../validators/tip-card-number.validator';

@Component({
  selector: 'bspl-tip-card-list',
  templateUrl: './tip-card-list.component.html',
  styleUrls: ['./tip-card-list.component.scss'],
  providers: [TipCardFormatter]
})
export class TipCardListComponent extends ListSubtabs<TipCard, TipCardFilter> implements OnInit, OnDestroy {
  public hasUpdatePermission: boolean;

  public listTitle: string;

  public columns: Array<GridColumn>;
  public downloadQuery: DataQuery<TipCardFilter>;
  public actions: Array<{ action: GridTableActionType; disabled?: boolean }>;

  public btnDesign = ButtonDesign;
  public toolbarButtonOptions: { mainOption: MenuItem; options: MenuItem[] };

  public noticeMessage: string;

  public searchForm: FormGroup;

  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public statusDropdownOptions: DropdownOption<TipStatus>[] = ArrayHelper.toDropdownOptions(Object.values(TipStatus));
  public cardTypeDropdownOptions: DropdownOption<TipCardType>[] = ArrayHelper.toDropdownOptions(
    Object.values(TipCardType)
  );
  public consentTypeDropdownOptions: DropdownOption<CardLevelConsentType>[] = Object.values(CardLevelConsentType).map(
    type => ({ value: type, label: this.translationService.translate(`TIP.cards.cardLevelConsentType.${type}`) })
  );
  public terminationReasonDropdownOptions: DropdownOption[] = Object.values(TipCardTerminationReason).map(value => ({
    value,
    label: this.translationService.translate(`TIP.cards.filters.terminationReason.options.${value}`)
  }));

  public bspList$: Observable<DropdownOption<BspDto>[]>;

  public isAirlineUser: boolean;
  public hasLeanPermission: boolean;
  public isIataUser: boolean;
  public isAgentUser: boolean;

  public get isAgentOrIataUser(): boolean {
    return this.isAgentUser || this.isIataUser;
  }

  protected initialFilters: TipCardFilter = { status: TipStatus.Active };

  private loggedUser: User;
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private formFactory: FormUtil;

  constructor(
    public displayFormatter: TipCardFormatter,
    protected store: Store<AppState>,
    protected dataService: TipCardService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private agentDictionary: AgentDictionaryService,
    private dialogService: DialogService,
    private router: Router,
    private permissionsService: PermissionsService,
    private modificationDialogService: TipCardModificationDialogService,
    private terminationDialogService: TipCardTerminationDialogService,
    private bspsDictionaryService: BspsDictionaryService,
    private datePipe: DatePipe
  ) {
    super(store, dataService, actions$, translationService);
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    super.ngOnInit();

    this.hasUpdatePermission = this.permissionsService.hasPermission(permissions.updateTipCard);
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);

    this.initializeLoggedUser();

    this.initializeListView();

    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.downloadQuery = query));

    if (this.isAirlineUser || this.isIataUser) {
      this.initializeBspList();
    }
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      this.initializeAgentDropdown();
    }
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery
      },
      apiService: this.dataService
    });
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: TipCard }): void {
    switch (action.actionType) {
      case GridTableActionType.CheckCardLevelConsents:
        this.store.dispatch(
          new ViewListActions.FilterChange({
            viewListId: createTabViewListId(ROUTES.TIP_CARDS.tabLabel, 'consent'),
            filter: { cardId: row.id, level: CardLevelConsent.Card }
          })
        );

        this.router.navigate([`${ROUTES.TIP_CARDS.url}/consent/list`]);

        break;
      case GridTableActionType.CheckCardConsents:
        this.store.dispatch(
          new ViewListActions.FilterChange({
            viewListId: createTabViewListId(ROUTES.TIP_CARDS.tabLabel, 'consent'),
            filter: { cardId: row.id }
          })
        );

        this.router.navigate([`${ROUTES.TIP_CARDS.url}/consent/list`]);

        break;
      case GridTableActionType.ModifyOwnCard:
        this.modify([row]);

        break;
      case GridTableActionType.TerminateOwnCard:
        this.terminate([row]);

        break;
    }
  }

  public loadAgentByBsp(bspsSelected: DropdownOption<BspDto>[]) {
    const filter: AgentDictionaryQueryFilters = {};
    if (bspsSelected?.length) {
      filter.bspId = bspsSelected.map(bsp => bsp.value.id);
    }

    this.initializeAgentDropdown(filter);
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();

    this.store.dispatch(TipCardActions.changeCreateCardVisibility({ visibility: false }));
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<TipCard, TipCardFilter>,
    DefaultProjectorFn<ListSubtabsState<TipCard, TipCardFilter>>
  > {
    return listSelectors[MasterDataType.TipCard];
  }

  protected getListKey(): string {
    return listKeys[MasterDataType.TipCard];
  }

  private initializeBspList() {
    this.bspList$ = this.loggedUser$.pipe(
      switchMap(({ bspPermissions }) =>
        this.bspsDictionaryService.getAllBspsByPermissions(bspPermissions, getReadTipCardListPermissions())
      ),
      map(bspList => bspList.map(toValueLabelObjectBsp))
    );
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.pipe(takeUntil(this.destroy$)).subscribe(user => {
      this.loggedUser = user;

      this.isAirlineUser = this.loggedUser.userType === UserType.AIRLINE;
      this.isAgentUser = this.loggedUser.userType === UserType.AGENT;
      this.isIataUser = this.loggedUser.userType === UserType.IATA;

      this.listTitle = `TIP.cards.list.title.${this.loggedUser.userType}`;
      this.noticeMessage = this.isAgentUser ? 'TIP.cards.list.alertMessage' : null;
    });
  }

  private initializeListView(): void {
    this.columns = this.buildColumns();
    this.actions = this.buildActions();
    this.searchForm = this.buildForm();

    this.initializeButtonOptions();
  }

  private buildColumns(): Array<GridColumn> {
    return [
      {
        name: '',
        prop: 'isRowSelected',
        maxWidth: 40,
        flexGrow: 1,
        sortable: false,
        headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
        cellTemplate: 'defaultLabeledCheckboxCellTmpl',
        hidden: this.isAirlineUser || !this.hasUpdatePermission
      },
      {
        name: 'TIP.cards.list.columns.bsp',
        prop: 'bsp.isoCountryCode',
        flexGrow: 1,
        minWidth: 70,
        sortable: true,
        hidden: this.isAgentUser
      },
      {
        name: 'TIP.cards.list.columns.agentCode',
        prop: 'agent.iataCode',
        flexGrow: 2,
        minWidth: 80,
        hidden: this.isAgentUser
      },
      {
        name: 'TIP.cards.list.columns.agentName',
        prop: 'agent.name',
        flexGrow: 2,
        minWidth: 80,
        hidden: !this.isIataUser
      },
      {
        name: 'TIP.cards.list.columns.cardNumber',
        prop: 'cardNumber',
        flexGrow: 3
      },
      {
        name: 'TIP.cards.list.columns.expiryDate',
        prop: 'expiryDate',
        minWidth: 80,
        flexGrow: 2
      },
      {
        name: 'TIP.cards.list.columns.issuerName',
        prop: 'issuerName',
        flexGrow: 3
      },
      {
        name: 'TIP.cards.list.columns.paymentNetwork',
        prop: 'paymentNetwork',
        minWidth: 100,
        flexGrow: 2
      },
      {
        name: this.isIataUser ? 'TIP.cards.list.columns.category' : 'TIP.cards.list.columns.cardType',
        prop: 'cardType',
        flexGrow: 2.5
      },
      {
        name: 'TIP.cards.list.columns.companyName',
        prop: 'companyName',
        flexGrow: 4
      },
      {
        name: 'TIP.cards.list.columns.cardConsent',
        prop: 'cardLevelConsent.type',
        flexGrow: 2,
        minWidth: 100,
        pipe: {
          transform: type => (type ? this.translationService.translate(`TIP.cards.cardLevelConsentType.${type}`) : null)
        },
        hidden: this.isAgentUser || this.isIataUser
      },
      {
        name: 'TIP.cards.list.columns.registerDate',
        prop: 'effectiveFrom',
        flexGrow: 2,
        minWidth: 120,
        hidden: this.isAirlineUser,
        pipe: { transform: value => this.datePipe.transform(value, 'dd/MM/yyyy') }
      },
      {
        name: 'TIP.cards.list.columns.status',
        prop: 'status',
        minWidth: 100,
        flexGrow: 2
      },
      {
        name: this.isIataUser ? 'TIP.cards.list.columns.deactivationDate' : 'TIP.cards.list.columns.terminationDate',
        prop: 'effectiveTo',
        flexGrow: 2,
        minWidth: 120,
        hidden: !this.isAgentOrIataUser,
        pipe: { transform: value => this.datePipe.transform(value, 'dd/MM/yyyy') }
      },
      {
        name: 'TIP.cards.list.columns.terminationReason',
        prop: 'terminationReason',
        flexGrow: 3,
        minWidth: 150,
        hidden: !this.isAgentUser
      }
    ];
  }

  private buildActions(): Array<{ action: GridTableActionType; disabled?: boolean }> {
    let actions: Array<{ action: GridTableActionType; disabled?: boolean }>;

    if (this.isAirlineUser) {
      actions = [{ action: GridTableActionType.CheckCardLevelConsents }];
    }

    if (this.isAgentUser) {
      actions = [{ action: GridTableActionType.CheckCardConsents }];

      if (this.hasUpdatePermission) {
        actions = [
          ...actions,
          { action: GridTableActionType.ModifyOwnCard },
          { action: GridTableActionType.TerminateOwnCard }
        ];
      }
    }

    return actions;
  }

  private initializeButtonOptions(): void {
    const hasCreatePermission = this.permissionsService.hasPermission(permissions.createTipCard);
    const visibility = hasCreatePermission && this.isAgentUser;

    this.store.dispatch(TipCardActions.changeCreateCardVisibility({ visibility }));

    if (this.isAgentUser) {
      this.toolbarButtonOptions = this.buildToolbarButtonOptions();
    }
  }

  private buildToolbarButtonOptions(): { mainOption: MenuItem; options: MenuItem[] } {
    const mainOption: MenuItem = {
      label: this.translationService.translate('TIP.cards.list.selectionToolbar.buttonOptionLabels.modifyCard'),
      command: () => this.bulkModify()
    };

    const options: MenuItem[] = [
      {
        label: this.translationService.translate('TIP.cards.list.selectionToolbar.buttonOptionLabels.terminateCard'),
        command: () => this.bulkTerminate()
      }
    ];

    return { mainOption, options };
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<TipCardFilter>({
      bsps: [],
      agent: [],
      status: [],
      fromCardNumber: [null, tipCardNumberValidator],
      toCardNumber: [null, tipCardNumberValidator],
      expiryDate: [],
      issuerName: [],
      paymentNetwork: [],
      cardType: [],
      companyName: [],
      cardLevelConsentType: [],
      effectiveFrom: [],
      effectiveTo: [],
      terminationReason: []
    });
  }

  private initializeAgentDropdown(filter?: AgentDictionaryQueryFilters): void {
    if (this.isAirlineUser || this.isIataUser) {
      const agentControl = FormUtil.get<TipCardFilter>(this.searchForm, 'agent');
      this.agentDropdownOptions$ = this.agentDictionary
        .getDropdownOptions(filter)
        .pipe(tap(options => this.updateOptionsControl(agentControl, options)));
    }
  }

  private bulkModify(): void {
    this.selectedRows$.pipe(first()).subscribe(rows => this.modify(rows));
  }

  private modify(items: TipCard[]): void {
    this.modificationDialogService.open(items).subscribe(button => {
      if (button === FooterButton.Modify) {
        this.onQueryChanged();
      }
    });
  }

  private bulkTerminate(): void {
    this.selectedRows$.pipe(first()).subscribe(rows => this.terminate(rows));
  }

  private terminate(items: TipCard[]): void {
    this.terminationDialogService.open(items).subscribe(button => {
      if (button === FooterButton.Terminate) {
        this.onQueryChanged();
      }
    });
  }

  private updateOptionsControl(control: AbstractControl, options: DropdownOption<{ id: number | string }>[]) {
    if (control.value?.length) {
      const optionIds = options.map(option => option.value.id);

      control.setValue(control.value.filter(selectedOption => optionIds.includes(selectedOption.id)));
    }
  }
}
