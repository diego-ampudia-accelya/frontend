import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { TipCardConsentFilter } from '../models/tip-card-consent-filter.model';
import { CardLevelConsentType } from '../models/tip-card.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import {
  agentFilterTagMapper,
  airlineFilterTagMapper,
  bspFilterTagMapper,
  rangeDateFilterTagMapper
} from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class TipCardConsentFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: TipCardConsentFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<TipCardConsentFilter> = {
      status: status => `${this.translate('status')} - ${status}`,
      agent: agent => agentFilterTagMapper(agent).join(', '),
      fromCardNumber: fromCardNumber => `${this.translate('fromCardNumber')} - ${fromCardNumber}`,
      toCardNumber: toCardNumber => `${this.translate('toCardNumber')} - ${toCardNumber}`,
      expiryDate: expiryDate => `${this.translate('expiryDate')} - ${expiryDate}`,
      issuerName: issuerName => `${this.translate('issuerName')} - ${issuerName}`,
      paymentNetwork: paymentNetwork => `${this.translate('paymentNetwork')} - ${paymentNetwork}`,
      cardType: cardType => `${this.translate('cardType')} - ${cardType.join(', ')}`,
      companyName: companyName => `${this.translate('companyName')} - ${companyName}`,
      type: type => `${this.translate('cardLevelConsentType')} - ${this.levelConsentTypeMapper(type)}`,
      effectiveFrom: date => `${this.translate('effectiveFrom')} - ${rangeDateFilterTagMapper(date)}`,
      effectiveTo: date => `${this.translate('effectiveTo')} - ${rangeDateFilterTagMapper(date)}`,
      airline: airline => airlineFilterTagMapper(airline).join(', '),
      bsps: bsps => `${this.translate('bsps')} - ${bspFilterTagMapper(bsps).join(', ')}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`TIP.cards.filters.${key}.label`);
  }

  private levelConsentTypeMapper(types: CardLevelConsentType[]): string {
    return types.map(type => this.translationService.translate(`TIP.cards.cardLevelConsentType.${type}`)).join(', ');
  }
}
