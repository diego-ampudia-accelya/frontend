import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { TipCardFilter } from '../models/tip-card-filter.model';
import { CardLevelConsentType } from '../models/tip-card.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { agentFilterTagMapper, bspFilterTagMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class TipCardFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: TipCardFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<TipCardFilter> = {
      agent: agent => agentFilterTagMapper(agent).join(', '),
      status: status => `${this.translate('status')} - ${status}`,
      fromCardNumber: fromCardNumber => `${this.translate('fromCardNumber')} - ${fromCardNumber}`,
      toCardNumber: toCardNumber => `${this.translate('toCardNumber')} - ${toCardNumber}`,
      expiryDate: expiryDate => `${this.translate('expiryDate')} - ${expiryDate}`,
      issuerName: issuerName => `${this.translate('issuerName')} - ${issuerName}`,
      paymentNetwork: paymentNetwork => `${this.translate('paymentNetwork')} - ${paymentNetwork}`,
      cardType: cardType => `${this.translate('cardType')} - ${cardType.join(', ')}`,
      companyName: companyName => `${this.translate('companyName')} - ${companyName}`,
      cardLevelConsentType: consentType =>
        `${this.translate('cardLevelConsentType')} - ${this.cardLevelConsentTypeMapper(consentType)}`,
      effectiveFrom: date => `${this.translate('effectiveFrom')} - ${rangeDateFilterTagMapper(date)}`,
      effectiveTo: date => `${this.translate('effectiveTo')} - ${rangeDateFilterTagMapper(date)}`,
      bsps: bsps => `${this.translate('bsps')} - ${bspFilterTagMapper(bsps)}`,
      terminationReason: terminationReason => {
        const terminationReasonTranslation = this.translationService.translate(
          `TIP.cards.filters.terminationReason.options.${terminationReason}`
        );

        return `${this.translate('terminationReason')} - ${terminationReasonTranslation}`;
      }
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`TIP.cards.filters.${key}.label`);
  }

  private cardLevelConsentTypeMapper(types: CardLevelConsentType[]): string {
    return types.map(type => this.translationService.translate(`TIP.cards.cardLevelConsentType.${type}`)).join(', ');
  }
}
