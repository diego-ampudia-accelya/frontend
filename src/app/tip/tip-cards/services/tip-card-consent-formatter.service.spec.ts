import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { TipCardConsentFilter } from '../models/tip-card-consent-filter.model';
import { CardLevelConsent } from '../models/tip-card-consent.model';
import { CardLevelConsentType, TipCardType, TipStatus } from '../models/tip-card.model';

import { TipCardConsentFormatter } from './tip-card-consent-formatter.service';

describe('TipCardConsentFormatter', () => {
  let formatter: TipCardConsentFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new TipCardConsentFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format status if it exists', () => {
    const filter: TipCardConsentFilter = { status: TipStatus.Active };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['status'],
        label: 'TIP.cards.filters.status.label - Active'
      }
    ]);
  });

  it('should format agents if they exist', () => {
    const filter: TipCardConsentFilter = {
      agent: [
        { id: '1', name: 'AGENT 78200001', code: '78200001' },
        { id: '2', name: 'AGENT 78200002', code: '78200002' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agent'],
        label: '78200001 / AGENT 78200001, 78200002 / AGENT 78200002'
      }
    ]);
  });

  it('should format fromCardNumber if it exists', () => {
    const filter: TipCardConsentFilter = { fromCardNumber: '5000' };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['fromCardNumber'],
        label: 'TIP.cards.filters.fromCardNumber.label - 5000'
      }
    ]);
  });

  it('should format toCardNumber if it exists', () => {
    const filter: TipCardConsentFilter = { toCardNumber: '0005' };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['toCardNumber'],
        label: 'TIP.cards.filters.toCardNumber.label - 0005'
      }
    ]);
  });

  it('should format expiry date if it exists', () => {
    const filter: TipCardConsentFilter = { expiryDate: '01/05' };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['expiryDate'],
        label: 'TIP.cards.filters.expiryDate.label - 01/05'
      }
    ]);
  });

  it('should format issuer name if it exists', () => {
    const filter: TipCardConsentFilter = { issuerName: 'NAME' };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['issuerName'],
        label: 'TIP.cards.filters.issuerName.label - NAME'
      }
    ]);
  });

  it('should format payment network if it exists', () => {
    const filter: TipCardConsentFilter = { paymentNetwork: 'CA' };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['paymentNetwork'],
        label: 'TIP.cards.filters.paymentNetwork.label - CA'
      }
    ]);
  });

  it('should format card type if it exists', () => {
    const filter: TipCardConsentFilter = { cardType: [TipCardType.Corporate, TipCardType.Consumer] };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['cardType'],
        label: 'TIP.cards.filters.cardType.label - Corporate, Consumer'
      }
    ]);
  });

  it('should format company name if it exists', () => {
    const filter: TipCardConsentFilter = { companyName: 'Accelya' };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['companyName'],
        label: 'TIP.cards.filters.companyName.label - Accelya'
      }
    ]);
  });

  it('should format types if they exist', () => {
    const filter: TipCardConsentFilter = {
      type: [CardLevelConsentType.CostRecovery, CardLevelConsentType.Yes]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['type'],
        label:
          'TIP.cards.filters.cardLevelConsentType.label - TIP.cards.cardLevelConsentType.costRecovery, TIP.cards.cardLevelConsentType.yes'
      }
    ]);
  });

  it('should format effective from if it exists with or without range', () => {
    // Date range has been specified
    const filterWithRangeDate: TipCardConsentFilter = {
      effectiveFrom: [new Date('2021/05/14'), new Date('2021/05/21')]
    };
    // Unique date has been specified
    const filterWithUniqueDate: TipCardConsentFilter = { effectiveFrom: [new Date('2021/05/14')] };

    expect(formatter.format(filterWithRangeDate)).toEqual([
      {
        keys: ['effectiveFrom'],
        label: 'TIP.cards.filters.effectiveFrom.label - 14/05/2021 - 21/05/2021'
      }
    ]);

    expect(formatter.format(filterWithUniqueDate)).toEqual([
      {
        keys: ['effectiveFrom'],
        label: 'TIP.cards.filters.effectiveFrom.label - 14/05/2021'
      }
    ]);
  });

  it('should format effective to if it exists with or without range', () => {
    // Date range has been specified
    const filterWithRangeDate: TipCardConsentFilter = { effectiveTo: [new Date('2022/01/14'), new Date('2022/01/22')] };
    // Unique date has been specified
    const filterWithUniqueDate: TipCardConsentFilter = { effectiveTo: [new Date('2022/01/14')] };

    expect(formatter.format(filterWithRangeDate)).toEqual([
      {
        keys: ['effectiveTo'],
        label: 'TIP.cards.filters.effectiveTo.label - 14/01/2022 - 22/01/2022'
      }
    ]);

    expect(formatter.format(filterWithUniqueDate)).toEqual([
      {
        keys: ['effectiveTo'],
        label: 'TIP.cards.filters.effectiveTo.label - 14/01/2022'
      }
    ]);
  });

  it('should format airlines if they exist', () => {
    const filter: TipCardConsentFilter = {
      airline: [
        { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
        { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airline'],
        label: '001 / AIRLINE 001, 002 / AIRLINE 002'
      }
    ]);
  });

  it('should format BSP if it exists with BSP name', () => {
    const filterWithName: TipCardConsentFilter = { bsps: [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }] };

    expect(formatter.format(filterWithName)).toEqual([
      {
        keys: ['bsps'],
        label: 'TIP.cards.filters.bsps.label - SPAIN (ES)'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: TipCardConsentFilter = {
      agent: [],
      status: null
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore filter properties that are not in formatter mapper', () => {
    const filter: TipCardConsentFilter = {
      level: CardLevelConsent.Card,
      cardId: 34241141124
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = {
      status: TipStatus.Active,
      unknown: 'unknown'
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['status'],
        label: 'TIP.cards.filters.status.label - Active'
      }
    ]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
