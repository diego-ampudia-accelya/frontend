import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TipCardBEFilter, TipCardFilter } from '../models/tip-card-filter.model';
import { TipCard } from '../models/tip-card.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class TipCardService implements Queryable<TipCard> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/cards`;

  private sortMapper = [
    { from: 'agent.iataCode', to: 'agentIataCode' },
    { from: 'cardLevelConsent.type', to: 'cardLevelConsentType' }
  ];

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<TipCardFilter>): Observable<PagedData<TipCard>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<TipCard>>(this.baseUrl + requestQuery.getQueryString());
  }

  public getOne(id: number): Observable<TipCard> {
    return this.http.get<TipCard>(`${this.baseUrl}/${id}`);
  }

  public download(
    query: DataQuery<TipCardFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat.toUpperCase() };
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public create(card: Partial<TipCard>): Observable<any> {
    const body = { ...card, issuerName: card.issuerName?.toUpperCase() };

    return this.http.post(this.baseUrl, body);
  }

  public modify(items: TipCard[], modifications: Partial<TipCard>): Observable<any> {
    const body = items.map(({ id }) => ({
      id,
      ...modifications,
      issuerName: modifications.issuerName?.toUpperCase()
    }));

    return this.http.put(this.baseUrl, body);
  }

  public terminate(items: TipCard[], terminationReason: string): Observable<any> {
    const body = items.map(({ id }) => ({ id, terminationReason }));

    return this.http.put(this.baseUrl, body);
  }

  private formatQuery(query: DataQuery<TipCardFilter>): RequestQuery<TipCardBEFilter> {
    const { agent, effectiveFrom, effectiveTo, bsps, terminationReason, ...filterBy } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery<TipCardBEFilter>({
      ...query,
      filterBy: {
        ...filterBy,
        agentId: agent && agent.map(item => item.id),
        fromEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[0]),
        toEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[1] ? effectiveFrom[1] : effectiveFrom[0]),
        fromEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[0]),
        toEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[1] ? effectiveTo[1] : effectiveTo[0]),
        bspId: bsps && bsps.map(({ id }) => id),
        terminationReasonType: terminationReason
      },
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
  }
}
