import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import moment from 'moment-mini';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { TipCardBEConsentFilter, TipCardConsentFilter } from '../models/tip-card-consent-filter.model';
import { TipCardConsent } from '../models/tip-card-consent.model';
import { ConsentRuleModel } from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, extractId, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class TipCardConsentService implements Queryable<TipCardConsent> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/card-consents`;

  private sortMapper = [
    { from: 'card.agent.iataCode', to: 'agentIataCode' },
    { from: 'card.cardNumber', to: 'cardNumber' },
    { from: 'card.expiryDate', to: 'expiryDate' },
    { from: 'card.issuerName', to: 'issuerName' },
    { from: 'card.paymentNetwork', to: 'paymentNetwork' },
    { from: 'card.companyName', to: 'companyName' },
    { from: 'costRecovery.type', to: 'costRecoveryType' },
    { from: 'costRecovery.amount', to: 'costRecoveryAmount' },
    { from: 'costRecovery.currency.code', to: 'costRecoveryCurrency' },
    { from: 'airline.iataCode', to: 'airlineIataCode' }
  ];

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public find(query: DataQuery<TipCardConsentFilter>): Observable<PagedData<TipCardConsent>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<TipCardConsent>>(this.baseUrl + requestQuery.getQueryString());
  }

  public download(
    query: DataQuery<TipCardConsentFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat.toUpperCase() };
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  public modify(items: TipCardConsent[], consent: ConsentRuleModel): Observable<any> {
    const body = this.toDataModel(items, consent);

    return this.http.put(this.baseUrl, body);
  }

  public terminate(items: TipCardConsent[]): Observable<any> {
    const body = items.map(({ id }) => ({ id, effectiveTo: moment().format('YYYY-MM-DD') }));

    return this.http.put(this.baseUrl, body);
  }

  private formatQuery(query: DataQuery<TipCardConsentFilter>): RequestQuery<TipCardBEConsentFilter> {
    const { agent, effectiveFrom, effectiveTo, airline, bsps, ...filterBy } = query.filterBy;

    return RequestQuery.fromDataQuery({
      ...query,
      filterBy: {
        ...filterBy,
        agentId: agent && agent.map(item => item.id),
        fromEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[0]),
        toEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[1] ? effectiveFrom[1] : effectiveFrom[0]),
        fromEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[0]),
        toEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[1] ? effectiveTo[1] : effectiveTo[0]),
        airlineIataCode: airline && airline.map(item => item.code),
        bspId: extractId(bsps)
      },
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
  }

  private toDataModel(items: TipCardConsent[], consent: ConsentRuleModel): Array<{ id: number } & ConsentRuleModel> {
    let data: ConsentRuleModel = consent;
    const { costRecovery } = consent;

    if (costRecovery) {
      if (!costRecovery.currency) {
        data = { ...consent, costRecovery: { ...costRecovery, currency: null } };
      }
    } else {
      data = { ...consent, costRecovery: null };
    }

    return items.map(({ id }) => ({ id, ...data }));
  }
}
