import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import moment from 'moment-mini';
import { of } from 'rxjs';

import { TipCardConsentFilter } from '../models/tip-card-consent-filter.model';
import { CardLevelConsent, TipCardConsent } from '../models/tip-card-consent.model';
import { CardLevelConsentType } from '../models/tip-card.model';
import { TipCardConsentService } from './tip-card-consent.service';
import { ConsentRuleModel } from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';
import { TipStatus } from '~app/tip/tip-prov-products/models/tip-product.model';

describe('TipCardConsentService', () => {
  let service: TipCardConsentService;

  const httpClientSpy = createSpyObject(HttpClient);
  const baseQuery: DataQuery<TipCardConsentFilter> = {
    filterBy: {},
    sortBy: [],
    paginateBy: { size: 20, page: 0 }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TipCardConsentService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });

    service = TestBed.inject(TipCardConsentService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    beforeEach(() => {
      httpClientSpy.get.and.returnValue(of());
      httpClientSpy.get.calls.reset();
    });

    it('should call TIP card consents endpoint', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/card-consents'));
    }));

    it('should include pagination parameters in request', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('page=0&size=20'));
    }));

    it('should include AGENT filters in request when specified', fakeAsync(() => {
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          agent: [
            { id: '1', name: 'AGENT 78200001', code: '78200001' },
            { id: '2', name: 'AGENT 78200002', code: '78200002' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agentId=1,2'));
    }));

    it('should include EFFECTIVE FROM with range filters in request when specified', fakeAsync(() => {
      const rangeEffectiveFrom: Date[] = [new Date('2021/05/14'), new Date('2021/05/21')];
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveFrom: rangeEffectiveFrom
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveFrom=2021-05-14&toEffectiveFrom=2021-05-21')
      );
    }));

    it('should include EFFECTIVE FROM without range filters in request when specified', fakeAsync(() => {
      const uniqueEffectiveFrom: Date[] = [new Date('2021/05/14')];
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveFrom: uniqueEffectiveFrom
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveFrom=2021-05-14&toEffectiveFrom=2021-05-14')
      );
    }));

    it('should include EFFECTIVE TO with range filters in request when specified', fakeAsync(() => {
      const rangeEffectiveTo: Date[] = [new Date('2022/01/14'), new Date('2022/01/22')];
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveTo: rangeEffectiveTo
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveTo=2022-01-14&toEffectiveTo=2022-01-22')
      );
    }));

    it('should include EFFECTIVE TO without range filters in request when specified', fakeAsync(() => {
      const uniqueEffectiveTo: Date[] = [new Date('2022/01/14')];
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveTo: uniqueEffectiveTo
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveTo=2022-01-14&toEffectiveTo=2022-01-14')
      );
    }));

    it('should include AIRLINE filters in request when specified', fakeAsync(() => {
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          airline: [
            { id: 1, name: 'AIRLINE 001', code: '001', designator: 'L1' },
            { id: 2, name: 'AIRLINE 002', code: '002', designator: 'L2' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('airlineIataCode=001,002'));
    }));

    it('should include BSP filters in request when specified', fakeAsync(() => {
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          bsps: [{ id: 11, isoCountryCode: 'ES', name: 'SPAIN' }]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('bspId=1'));
    }));

    // Testing some sort mapper properties

    it('should map AGENT sorting in request with the specified sort mapper', fakeAsync(() => {
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'card.agent.iataCode', sortType: SortOrder.Asc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agentIataCode,ASC'));
    }));

    it('should map COST RECOVERY CURRENCY sorting in request with the specified sort mapper', fakeAsync(() => {
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'costRecovery.currency.code', sortType: SortOrder.Desc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('costRecoveryCurrency,DESC'));
    }));
  });

  describe('download', () => {
    beforeEach(() => {
      httpClientSpy.get.and.returnValue(of());
      httpClientSpy.get.calls.reset();
    });

    it('should call TIP card consent endpoint with correct options', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.any(String), {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));

    it('should include actual query in request', fakeAsync(() => {
      const query: DataQuery<TipCardConsentFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          paymentNetwork: 'CA'
        }
      };

      service.download(query, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('page=0&size=20&paymentNetwork=CA'),
        jasmine.anything()
      );
    }));

    it('should call endpoint with specified download format', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.TXT).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('exportAs=TXT'), jasmine.anything());
    }));
  });

  it('should call TIP card MODIFY endpoint with correct options', fakeAsync(() => {
    const item: TipCardConsent = {
      id: 1,
      airline: { id: 69837820012, iataCode: '001' },
      level: CardLevelConsent.Country,
      card: null,
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      type: CardLevelConsentType.Yes,
      costRecovery: { type: CostRecoveryType.Percentage, amount: 50, currency: null },
      effectiveFrom: new Date('2021/01/14'),
      effectiveTo: new Date('2022/01/14'),
      status: TipStatus.Active
    };
    const consent: ConsentRuleModel = { type: 'No' };

    httpClientSpy.put.and.returnValue(of());
    service.modify([item], consent).subscribe();

    expect(httpClientSpy.put).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/card-consents'), [
      { id: item.id, ...consent, costRecovery: null }
    ]);
  }));

  it('should call TIP card TERMINATE endpoint with correct options', fakeAsync(() => {
    const item: TipCardConsent = {
      id: 1,
      airline: { id: 69837820012, iataCode: '001' },
      level: CardLevelConsent.Country,
      card: null,
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      type: CardLevelConsentType.Yes,
      costRecovery: { type: CostRecoveryType.Percentage, amount: 50, currency: null },
      effectiveFrom: new Date('2021/01/14'),
      effectiveTo: new Date('2022/01/14'),
      status: TipStatus.Active
    };
    const now = moment().format('YYYY-MM-DD');

    httpClientSpy.put.and.returnValue(of());
    service.terminate([item]).subscribe();

    expect(httpClientSpy.put).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/card-consents'), [
      { id: item.id, effectiveTo: now }
    ]);
  }));

  describe('toDataModel', () => {
    const item: TipCardConsent = {
      id: 1,
      airline: { id: 69837820012, iataCode: '001' },
      level: CardLevelConsent.Country,
      card: null,
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      type: CardLevelConsentType.Yes,
      costRecovery: { type: CostRecoveryType.Percentage, amount: 50, currency: null },
      effectiveFrom: new Date('2021/01/14'),
      effectiveTo: new Date('2022/01/14'),
      status: TipStatus.Active
    };

    it('should map to data model with cost recovery and currency', () => {
      const consent: ConsentRuleModel = {
        type: 'costRecovery',
        costRecovery: {
          type: CostRecoveryType.Percentage,
          amount: 50,
          currency: { id: 1, code: 'EUR', decimals: 2 }
        }
      };

      expect(service['toDataModel']([item], consent)).toEqual([{ id: item.id, ...consent }]);
    });

    it('should map to data model with cost recovery and no currency', () => {
      const consent: ConsentRuleModel = {
        type: 'costRecovery',
        costRecovery: {
          type: CostRecoveryType.Percentage,
          amount: 50
        }
      };

      expect(service['toDataModel']([item], consent)).toEqual([
        {
          id: item.id,
          type: consent.type,
          costRecovery: {
            ...consent.costRecovery,
            currency: null
          }
        }
      ]);
    });

    it('should map to data model with cost recovery and no currency', () => {
      const consent: ConsentRuleModel = { type: 'no' };

      expect(service['toDataModel']([item], consent)).toEqual([
        { id: item.id, type: consent.type, costRecovery: null }
      ]);
    });
  });
});
