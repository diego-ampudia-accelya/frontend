import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { TipCardFilter } from '../models/tip-card-filter.model';
import { CardLevelConsentType, TipCard } from '../models/tip-card.model';
import { TipCardService } from './tip-card.service';
import { DataQuery } from '~app/shared/components/list-view';
import { SortOrder } from '~app/shared/enums';
import { DownloadFormat } from '~app/shared/models';
import { AppConfigurationService } from '~app/shared/services';
import { TipCardType, TipStatus } from '~app/tip/tip-prov-products/models/tip-product.model';

describe('TipCardService', () => {
  let service: TipCardService;

  const httpClientSpy = createSpyObject(HttpClient);
  const baseQuery: DataQuery<TipCardFilter> = {
    filterBy: {},
    sortBy: [],
    paginateBy: { size: 20, page: 0 }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TipCardService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });

    service = TestBed.inject(TipCardService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  describe('find', () => {
    beforeEach(() => {
      httpClientSpy.get.and.returnValue(of());
      httpClientSpy.get.calls.reset();
    });

    it('should call TIP cards endpoint', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/cards'));
    }));

    it('should include pagination parameters in request', fakeAsync(() => {
      service.find(baseQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('page=0&size=20'));
    }));

    it('should include AGENT filters in request when specified', fakeAsync(() => {
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          agent: [
            { id: '1', name: 'AGENT 78200001', code: '78200001' },
            { id: '2', name: 'AGENT 78200002', code: '78200002' }
          ]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agentId=1,2'));
    }));

    it('should include EFFECTIVE FROM with range filters in request when specified', fakeAsync(() => {
      const rangeEffectiveFrom: Date[] = [new Date('2021/05/14'), new Date('2021/05/21')];
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveFrom: rangeEffectiveFrom
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveFrom=2021-05-14&toEffectiveFrom=2021-05-21')
      );
    }));

    it('should include EFFECTIVE FROM without range filters in request when specified', fakeAsync(() => {
      const uniqueEffectiveFrom: Date[] = [new Date('2021/05/14')];
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveFrom: uniqueEffectiveFrom
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveFrom=2021-05-14&toEffectiveFrom=2021-05-14')
      );
    }));

    it('should include EFFECTIVE TO with range filters in request when specified', fakeAsync(() => {
      const rangeEffectiveTo: Date[] = [new Date('2022/01/14'), new Date('2022/01/22')];
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveTo: rangeEffectiveTo
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveTo=2022-01-14&toEffectiveTo=2022-01-22')
      );
    }));

    it('should include EFFECTIVE TO without range filters in request when specified', fakeAsync(() => {
      const uniqueEffectiveTo: Date[] = [new Date('2022/01/14')];
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          effectiveTo: uniqueEffectiveTo
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('fromEffectiveTo=2022-01-14&toEffectiveTo=2022-01-14')
      );
    }));

    it('should include BSP filters in request when specified', fakeAsync(() => {
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          bsps: [{ id: 11, isoCountryCode: 'ES', name: 'SPAIN' }]
        }
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('bspId=1'));
    }));

    it('should map AGENT sorting in request with the specified sort mapper', fakeAsync(() => {
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'agent.iataCode', sortType: SortOrder.Asc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agentIataCode,ASC'));
    }));

    it('should map CONSENT TYPE sorting in request with the specified sort mapper', fakeAsync(() => {
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        sortBy: [{ attribute: 'cardLevelConsent.type', sortType: SortOrder.Desc }]
      };

      service.find(query).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('cardLevelConsentType,DESC'));
    }));
  });

  describe('download', () => {
    beforeEach(() => {
      httpClientSpy.get.and.returnValue(of());
      httpClientSpy.get.calls.reset();
    });

    it('should call TIP card endpoint with correct options', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.any(String), {
        responseType: 'arraybuffer' as 'json',
        observe: 'response' as 'body'
      });
    }));

    it('should include actual query in request', fakeAsync(() => {
      const query: DataQuery<TipCardFilter> = {
        ...baseQuery,
        filterBy: {
          ...baseQuery.filterBy,
          companyName: 'Accelya'
        }
      };

      service.download(query, DownloadFormat.CSV).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('page=0&size=20&companyName=Accelya'),
        jasmine.anything()
      );
    }));

    it('should call endpoint with specified download format', fakeAsync(() => {
      service.download(baseQuery, DownloadFormat.TXT).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('exportAs=TXT'), jasmine.anything());
    }));
  });

  it('should call TIP card CREATE endpoint with correct options', fakeAsync(() => {
    const item: Partial<TipCard> = {
      bsp: { id: 6983 },
      cardNumber: '5555000055550000',
      expiryDate: '01/24',
      issuerName: 'Issuer Name',
      paymentNetwork: 'CA',
      cardType: TipCardType.Consumer,
      companyName: 'Accelya'
    };

    httpClientSpy.post.and.returnValue(of());
    service.create(item).subscribe();

    expect(httpClientSpy.post).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/cards'), {
      ...item,
      issuerName: 'ISSUER NAME'
    });
  }));

  it('should call TIP card MODIFY endpoint with correct options', fakeAsync(() => {
    const item: TipCard = {
      id: 1,
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      agent: { id: 69837820012, iataCode: '7820012', name: 'NOMBRE 7820012' },
      cardNumber: '5555000055550000',
      expiryDate: '01/24',
      issuerName: 'Issuer Name',
      paymentNetwork: 'CA',
      cardType: TipCardType.Consumer,
      companyName: 'Accelya',
      cardLevelConsent: { type: CardLevelConsentType.CostRecovery },
      effectiveFrom: new Date('2021/01/14'),
      effectiveTo: new Date('2022/01/14'),
      status: TipStatus.Active
    };
    const modifications: Partial<TipCard> = {
      issuerName: 'Issuer Name Modified',
      cardType: TipCardType.Corporate
    };

    httpClientSpy.put.and.returnValue(of());
    service.modify([item], modifications).subscribe();

    expect(httpClientSpy.put).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/cards'), [
      { id: item.id, ...modifications, issuerName: 'ISSUER NAME MODIFIED' }
    ]);
  }));

  it('should call TIP card TERMINATE endpoint with correct options', fakeAsync(() => {
    const item: TipCard = {
      id: 1,
      bsp: { id: 6983, isoCountryCode: 'ES', name: 'SPAIN' },
      agent: { id: 69837820012, iataCode: '7820012', name: 'NOMBRE 7820012' },
      cardNumber: '5555000055550000',
      expiryDate: '01/24',
      issuerName: 'Issuer Name',
      paymentNetwork: 'CA',
      cardType: TipCardType.Consumer,
      companyName: 'Accelya',
      cardLevelConsent: { type: CardLevelConsentType.CostRecovery },
      effectiveFrom: new Date('2021/01/14'),
      effectiveTo: new Date('2022/01/14'),
      status: TipStatus.Active
    };

    httpClientSpy.put.and.returnValue(of());
    service.terminate([item], 'TERMINATION REASON').subscribe();

    expect(httpClientSpy.put).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/cards'), [
      { id: item.id, terminationReason: 'TERMINATION REASON' }
    ]);
  }));
});
