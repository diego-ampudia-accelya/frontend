import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA, TemplateRef } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { TipCardConsentModificationDialogService } from '../dialogs/tip-card-consent-modification-dialog/tip-card-consent-modification-dialog.service';
import { TipCardConsentTerminationDialogService } from '../dialogs/tip-card-consent-termination-dialog/tip-card-consent-termination-dialog.service';
import { CardLevelConsent, TipCardConsent } from '../models/tip-card-consent.model';
import { CardLevelConsentType } from '../models/tip-card.model';
import { TipCardConsentFormatter } from '../services/tip-card-consent-formatter.service';
import { TipCardConsentService } from '../services/tip-card-consent.service';
import { TipCardConsentListComponent } from './tip-card-consent-list.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { AgentSummary, DropdownOption, GridColumnMultipleLinkClick } from '~app/shared/models';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService, AppConfigurationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { TranslatePipeMock } from '~app/test';

describe('TipProductConsentListComponent', () => {
  let fixture: ComponentFixture<TipCardConsentListComponent>;
  let component: TipCardConsentListComponent;

  const tipCardConsentTerminationDialogServiceSpy = createSpyObject(TipCardConsentTerminationDialogService);
  const tipCardConsentModificationDialogServiceSpy = createSpyObject(TipCardConsentModificationDialogService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);

  const expectedUserDetails = {
    userType: UserType.AIRLINE, // AIRLINE user by default
    permissions: [
      'rTipCrdCnsCoAc',
      'rTipCrdCnsCoIn',
      'rTipCrdCnsCrAc',
      'rTipCrdCnsCrIn',
      'rBsps',
      'rLean',
      'uTipCrdCnsCr',
      'uTipCrdCnsCo'
    ],
    bspPermissions: [
      {
        bspId: 1,
        permissions: [
          'rTipCrdCnsCoAc',
          'rTipCrdCnsCoIn',
          'rTipCrdCnsCrAc',
          'rTipCrdCnsCrIn',
          'rBsps',
          'rLean',
          'uTipCrdCnsCr',
          'uTipCrdCnsCo'
        ]
      }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          'MENU.TIP.products.tabLabel': {},
          activeTabId: 'MENU.TIP.products.tabLabel'
        }
      },
      viewListsInfo: {}
    },
    tip: { 'tip-card-consent-consent-list': fromListSubtabs.initialState }
  };

  const action: GridTableAction = {
    actionType: GridTableActionType.AddModifyCardLevelConsent,
    methodName: ''
  };

  const row: TipCardConsent = {
    id: 1,
    airline: {
      id: 2,
      iataCode: ''
    },
    level: CardLevelConsent.Country,
    card: null,
    bsp: null,
    type: CardLevelConsentType.CostRecovery,
    costRecovery: null,
    effectiveFrom: new Date(),
    effectiveTo: new Date(),
    status: null
  };

  const airlineColumns = jasmine.arrayContaining<any>([
    jasmine.objectContaining({
      name: '',
      prop: 'isRowSelected',
      maxWidth: 40,
      flexGrow: 1,
      sortable: false,
      headerTemplate: jasmine.anything(),
      cellTemplate: jasmine.anything()
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.consent.columns.bsp',
      prop: 'bsp.isoCountryCode',
      flexGrow: 1,
      sortable: true
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.list.columns.agentCode',
      prop: 'card.agent.iataCode',
      flexGrow: 2
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.list.columns.cardNumber',
      prop: 'card.cardNumber',
      flexGrow: 3.5
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.list.columns.expiryDate',
      prop: 'card.expiryDate',
      flexGrow: 2
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.list.columns.paymentNetwork',
      prop: 'card.paymentNetwork',
      flexGrow: 2
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.consent.columns.consent',
      prop: 'type',
      flexGrow: 2
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.consent.columns.costRecoveryType',
      prop: 'costRecovery.type',
      flexGrow: 3
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.consent.columns.costRecoveryAmount',
      prop: 'costRecovery.amount',
      flexGrow: 3,
      cellClass: 'text-right'
    }),
    jasmine.objectContaining({
      name: 'TIP.cards.consent.columns.status',
      prop: 'status',
      flexGrow: 2
    })
  ]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TipCardConsentListComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({ initialState }),
        FormBuilder,
        PermissionsService,
        BspsDictionaryService,
        TipCardConsentService,
        AgentDictionaryService,
        AirlineDictionaryService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: TipCardConsentTerminationDialogService, useValue: tipCardConsentTerminationDialogServiceSpy },
        { provide: TipCardConsentModificationDialogService, useValue: tipCardConsentModificationDialogServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    })
      .overrideComponent(TipCardConsentListComponent, {
        set: { providers: [TipCardConsentFormatter] }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipCardConsentListComponent);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('should set rowDetailsByUserType when user is an AIRLINE and is Card Level', () => {
    const result: TemplateRef<any> = component.rowAirlineDetailTemplate;

    component.selectedLevelConsent = CardLevelConsent.Card;

    expect(component.rowDetailsByUserType).toEqual(result);
  });

  it('should set rowDetailsByUserType when user is an Agent', () => {
    const result: TemplateRef<any> = component.rowAgentDetailTemplate;

    component.isAgentUser = true;

    expect(component.rowDetailsByUserType).toEqual(result);
  });

  it('should call especific functions when ngOnit is called', () => {
    const setPredefinedFiltersSpy = spyOn<any>(component, 'setPredefinedFilters');
    const initializePermissionsSpy = spyOn<any>(component, 'initializePermissions');
    const initializeQuerySpy = spyOn<any>(component, 'initializeQuery');
    const initializeListViewSpy = spyOn<any>(component, 'initializeListView');
    const initializeLabelsSpy = spyOn<any>(component, 'initializeLabels');
    const initializeBspOptionsMulticountrySpy = spyOn<any>(component, 'initializeBspOptionsMulticountry');
    const initializeChangeListenersSpy = spyOn<any>(component, 'initializeChangeListeners');

    component.ngOnInit();

    expect(setPredefinedFiltersSpy).toHaveBeenCalled();
    expect(initializePermissionsSpy).toHaveBeenCalled();
    expect(initializeQuerySpy).toHaveBeenCalled();
    expect(initializeListViewSpy).toHaveBeenCalled();
    expect(initializeLabelsSpy).toHaveBeenCalled();
    expect(initializeBspOptionsMulticountrySpy).toHaveBeenCalled();
    expect(initializeChangeListenersSpy).toHaveBeenCalled();
  });

  it('should call especific functions when onLevelConsentSelected is called', () => {
    const changeLevelConsentSpy = spyOn<any>(component, 'changeLevelConsent');
    const initializeListViewSpy = spyOn<any>(component, 'initializeListView');
    const onQueryChangedSpy = spyOn<any>(component, 'onQueryChanged');
    const level = CardLevelConsent.Country;

    component.onLevelConsentSelected(level);

    expect(changeLevelConsentSpy).toHaveBeenCalledWith(level);
    expect(onQueryChangedSpy).toHaveBeenCalled();
    expect(initializeListViewSpy).toHaveBeenCalled();
  });

  it('should call initializeFilterDropdowns when onFilterButtonClicked is called', () => {
    const initializeFilterDropdownsSpy = spyOn<any>(component, 'initializeFilterDropdowns');
    const isSearchPanelVisible = true;

    component.onFilterButtonClicked(isSearchPanelVisible);

    expect(initializeFilterDropdownsSpy).toHaveBeenCalledWith();
  });

  it('should call dialogService open with specific config when onDownload is called', () => {
    const config = {
      data: {
        title: 'common.download',
        footerButtonsType: FooterButton.Download,
        downloadQuery: component.query
      },
      apiService: component['dataService']
    };

    dialogServiceSpy.open.and.callThrough();
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(DownloadFileComponent, config);
  });

  it('should call modify when onActionClick is called with action AddModifyCardLevelConsent', () => {
    const modifySpy = spyOn<any>(component, 'modify');

    component.onActionClick({ action, row });

    expect(modifySpy).toHaveBeenCalledWith([row]);
  });

  it('should call modify when onActionClick is called with action TerminateCardLevelConsent', () => {
    const terminateSpy = spyOn<any>(component, 'terminate');

    const terminateAction = { ...action, actionType: GridTableActionType.TerminateCardLevelConsent };

    component.onActionClick({ action: terminateAction, row });

    expect(terminateSpy).toHaveBeenCalledWith([row]);
  });

  it('should call navigateToAirlineDetail when onRowLinkClick is called', () => {
    const navigateToAirlineDetailSpy = spyOn<any>(component, 'navigateToAirlineDetail');

    const event: GridColumnMultipleLinkClick<TipCardConsent> = {
      propertyName: 'airline.iataCode',
      propertyValue: '',
      row
    };

    component.onRowLinkClick(event);

    expect(navigateToAirlineDetailSpy).toHaveBeenCalledWith(2);
  });

  it('should call navigateToAirlineDetail when onViewAirline is called', () => {
    const navigateToAirlineDetailSpy = spyOn<any>(component, 'navigateToAirlineDetail');

    component.onViewAirline(3);

    expect(navigateToAirlineDetailSpy).toHaveBeenCalledWith(3);
  });

  it('should call router navigate when navigateToAirlineDetail is called', () => {
    const expectedRoute = ['/tip/cards/airline/view', 3];

    routerSpy.navigate.and.callThrough();

    component.navigateToAirlineDetail(3);

    expect(routerSpy.navigate).toHaveBeenCalledWith(expectedRoute);
  });

  it('should set agentDropdownOptions when updateOptionsControl is called', () => {
    const agentFromBsp = {
      value: {
        id: '1',
        name: 'AGENT 1111111',
        code: '1111111',
        bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
      },
      label: '1111111 / AGENT 1111111'
    };

    const agentValue: DropdownOption<AgentSummary>[] = [
      {
        value: {
          id: '1',
          name: 'AGENT 1111111',
          code: '1111111',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' }
        },
        label: '1111111 / AGENT 1111111'
      },
      {
        value: {
          id: '2',
          name: 'AGENT 22222',
          code: '22222',
          bsp: { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
        },
        label: '22222 / AGENT 22222'
      }
    ];

    component['initializeForm']();
    component.searchForm.get('agent').setValue(agentFromBsp);

    component['updateOptionsControl'](agentValue);

    expect(component.searchForm.get('agent').value).toEqual(agentFromBsp);
  });

  it('should set predefinedFilters when setPredefinedFilters is called', () => {
    const expectedFilters = {
      level: CardLevelConsent.Country
    };
    component.predefinedFilters = null;

    component['setPredefinedFilters']();

    expect(component.predefinedFilters).toEqual(expectedFilters);
  });

  it('should initialize Permissions when initializePermissions is called', () => {
    component['initializePermissions']();

    expect(component.hasLeanPermission).toBeTruthy();
    expect(component.hasUpdateCardLevelPermission).toBeTruthy();
    expect(component.hasUpdateCountryLevelPermission).toBeTruthy();
  });

  it('should initialize Labels when initializeLabels is called and is country level', () => {
    component.selectedLevelConsent = CardLevelConsent.Country;

    component['initializeLabels']();

    expect(component.noticeMessage).toEqual('TIP.cards.consent.alertMessage');
    expect(component.titleLabel).toEqual('TIP.cards.consent.titleAirline');
  });

  it('should initialize Labels when initializeLabels is called and is card level', () => {
    component.selectedLevelConsent = CardLevelConsent.Card;
    component.isAirlineUser = false;
    component['initializeLabels']();

    expect(component.noticeMessage).toEqual(null);
    expect(component.titleLabel).toEqual('TIP.cards.consent.titleAgent');
  });

  it('should initialize ListView when initializeListView is called', () => {
    const buildColumnsSpy = spyOn<any>(component, 'buildColumns');
    const buildActionsSpy = spyOn<any>(component, 'buildActions');
    const buildToolbarButtonOptionsSpy = spyOn<any>(component, 'buildToolbarButtonOptions');
    const initializeFormSpy = spyOn<any>(component, 'initializeForm');

    component.searchForm = null;

    component['initializeListView']();

    expect(buildColumnsSpy).toHaveBeenCalled();
    expect(buildActionsSpy).toHaveBeenCalled();
    expect(buildToolbarButtonOptionsSpy).toHaveBeenCalled();
    expect(initializeFormSpy).toHaveBeenCalled();

    expect(component['buildColumns']).toEqual(buildColumnsSpy);
    expect(component['buildActions']).toEqual(buildActionsSpy);
  });

  it('should build correct columns when initializeListView is called and is Airline User', () => {
    component.isAirlineUser = true;
    component.selectedLevelConsent = CardLevelConsent.Card;

    component['initializeListView']();

    expect(component.columns).toEqual(airlineColumns);
  });

  it('should change selected Level Consent  when changeLevelConsent is called', () => {
    const expectedFilters = {
      level: CardLevelConsent.Country
    };
    const expectedLevel = CardLevelConsent.Country;

    component.predefinedFilters = null;

    component['changeLevelConsent'](CardLevelConsent.Country);

    expect(component.predefinedFilters).toEqual(expectedFilters);
    expect(component.selectedLevelConsent).toEqual(expectedLevel);
  });

  it('should initialize Agent Dropdowns when initializeFilterDropdowns is called and is Airline User', () => {
    component.isAirlineUser = true;

    const agentDictionarySpy = spyOn<any>(component['agentDictionary'], 'getDropdownOptions');

    component['initializeFilterDropdowns']();

    expect(agentDictionarySpy).toHaveBeenCalled();
  });

  it('should initialize Agent Dropdowns when initializeFilterDropdowns is called and is Airline User', () => {
    component.isAirlineUser = false;

    const airlineDictionarySpy = spyOn<any>(component['airlineDictionary'], 'getDropdownOptions');

    component['initializeFilterDropdowns']();

    expect(airlineDictionarySpy).toHaveBeenCalled();
  });

  it('should initialize Agent Dropdowns when initializeFilterDropdowns is called and is Airline User', () => {
    component.isAirlineUser = false;

    const airlineDictionarySpy = spyOn<any>(component['airlineDictionary'], 'getDropdownOptions');

    component['initializeFilterDropdowns']();

    expect(airlineDictionarySpy).toHaveBeenCalled();
  });

  it('should call onQueryChanged when modify is called and button is Modify type', fakeAsync(() => {
    const onQueryChangedSpy = spyOn<any>(component, 'onQueryChanged');

    tipCardConsentModificationDialogServiceSpy.open.and.returnValue(of(FooterButton.Modify));
    component['modify']([row]);
    tick();

    expect(onQueryChangedSpy).toHaveBeenCalled();
  }));

  it('should call onQueryChanged when terminate is called and button is Terminate type', fakeAsync(() => {
    const onQueryChangedSpy = spyOn<any>(component, 'onQueryChanged');

    tipCardConsentTerminationDialogServiceSpy.open.and.returnValue(of(FooterButton.Terminate));
    component['terminate']([row]);
    tick();

    expect(onQueryChangedSpy).toHaveBeenCalled();
  }));
});
