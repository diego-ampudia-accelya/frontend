import { GridColumn } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

export const CARD_LEVEL_COLUMNS = (isAirlineUser: boolean): Array<GridColumn> => [
  {
    name: '',
    prop: 'isRowSelected',
    maxWidth: 40,
    flexGrow: 1,
    sortable: false,
    headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
    cellTemplate: 'defaultLabeledCheckboxCellTmpl',
    hidden: !isAirlineUser
  },
  {
    name: 'TIP.cards.consent.columns.bsp',
    prop: 'bsp.isoCountryCode',
    flexGrow: 1,
    minWidth: 70,
    sortable: true,
    hidden: !isAirlineUser
  },
  {
    name: 'TIP.cards.list.columns.agentCode',
    prop: 'card.agent.iataCode',
    flexGrow: 2,
    hidden: !isAirlineUser
  },
  {
    name: 'TIP.cards.list.columns.airline',
    prop: 'airline.iataCode',
    flexGrow: 2,
    hidden: isAirlineUser,
    cellTemplate: 'multipleLinkCellTmpl'
  },
  {
    name: 'TIP.cards.list.columns.cardNumber',
    prop: 'card.cardNumber',
    flexGrow: 3.5
  },
  {
    name: 'TIP.cards.list.columns.expiryDate',
    prop: 'card.expiryDate',
    flexGrow: 2
  },
  {
    name: 'TIP.cards.list.columns.issuerName',
    prop: 'card.issuerName',
    flexGrow: 3,
    hidden: isAirlineUser
  },
  {
    name: 'TIP.cards.list.columns.paymentNetwork',
    prop: 'card.paymentNetwork',
    flexGrow: 2
  },
  {
    name: 'TIP.cards.list.columns.companyName',
    prop: 'card.companyName',
    flexGrow: 3,
    hidden: isAirlineUser
  },
  {
    name: 'TIP.cards.consent.columns.consent',
    prop: 'type',
    flexGrow: 2
  },
  {
    name: 'TIP.cards.consent.columns.costRecoveryType',
    prop: 'costRecovery.type',
    flexGrow: 3
  },
  {
    name: 'TIP.cards.consent.columns.costRecoveryAmount',
    prop: 'costRecovery.amount',
    flexGrow: 3,
    cellClass: 'text-right'
  },
  {
    name: 'TIP.cards.consent.columns.costRecoveryCurrency',
    prop: 'costRecovery.currency.code',
    flexGrow: 3,
    hidden: !isAirlineUser
  },
  {
    name: 'TIP.cards.consent.columns.status',
    prop: 'status',
    flexGrow: 2
  }
];

export const COUNTRY_LEVEL_COLUMNS: Array<GridColumn> = [
  {
    name: '',
    prop: 'isRowSelected',
    maxWidth: 40,
    flexGrow: 1,
    sortable: false,
    headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
    cellTemplate: 'defaultLabeledCheckboxCellTmpl'
  },
  {
    name: 'TIP.cards.consent.columns.bsp',
    prop: 'bsp.isoCountryCode',
    flexGrow: 1,
    sortable: true
  },
  {
    name: 'TIP.cards.consent.columns.consent',
    prop: 'type',
    flexGrow: 2
  },
  {
    name: 'TIP.cards.consent.columns.costRecoveryType',
    prop: 'costRecovery.type',
    flexGrow: 3
  },
  {
    name: 'TIP.cards.consent.columns.costRecoveryAmount',
    prop: 'costRecovery.amount',
    flexGrow: 3,
    cellClass: 'text-right'
  },
  {
    name: 'TIP.cards.consent.columns.costRecoveryCurrency',
    prop: 'costRecovery.currency.code',
    flexGrow: 3
  },
  {
    name: 'TIP.cards.consent.columns.effectiveFrom',
    prop: 'effectiveFrom',
    cellTemplate: 'dayMonthYearCellTmpl',
    flexGrow: 2
  },
  {
    name: 'TIP.cards.consent.columns.effectiveTo',
    prop: 'effectiveTo',
    cellTemplate: 'dayMonthYearCellTmpl',
    flexGrow: 2
  },
  {
    name: 'TIP.cards.consent.columns.status',
    prop: 'status',
    flexGrow: 1.5
  }
];

export const CARD_LEVEL_ACTIONS: Array<{ action: GridTableActionType }> = [
  { action: GridTableActionType.AddModifyCardLevelConsent },
  { action: GridTableActionType.TerminateCardLevelConsent }
];

export const COUNTRY_LEVEL_ACTIONS: Array<{ action: GridTableActionType }> = [
  { action: GridTableActionType.ModifyCardBspLevelConsent },
  { action: GridTableActionType.TerminateCardBspLevelConsent }
];
