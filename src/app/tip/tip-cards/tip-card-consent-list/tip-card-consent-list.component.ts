import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { MenuItem } from 'primeng/api';
import { Observable } from 'rxjs';
import { first, map, takeUntil, tap } from 'rxjs/operators';

import { getReadTipCardConsentPermissions, permissions } from '../config/tip-card-permissions.config';
import { TipCardConsentModificationDialogService } from '../dialogs/tip-card-consent-modification-dialog/tip-card-consent-modification-dialog.service';
import { TipCardConsentTerminationDialogService } from '../dialogs/tip-card-consent-termination-dialog/tip-card-consent-termination-dialog.service';
import { TipCardConsentFilter } from '../models/tip-card-consent-filter.model';
import { CardLevelConsent, TipCardConsent } from '../models/tip-card-consent.model';
import { CardLevelConsentType, TipCardType, TipStatus } from '../models/tip-card.model';
import { TipCardConsentFormatter } from '../services/tip-card-consent-formatter.service';
import { TipCardConsentService } from '../services/tip-card-consent.service';
import { tipCardNumberValidator } from '../validators/tip-card-number.validator';
import {
  CARD_LEVEL_ACTIONS,
  CARD_LEVEL_COLUMNS,
  COUNTRY_LEVEL_ACTIONS,
  COUNTRY_LEVEL_COLUMNS
} from './tip-card-consent-list.constants';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ButtonDesign, DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants';
import { Permissions } from '~app/shared/constants/permissions';
import { SelectMode } from '~app/shared/enums';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import {
  AgentDictionaryQueryFilters,
  AgentSummary,
  AirlineSummary,
  DropdownOption,
  GridColumn,
  GridColumnMultipleLinkClick
} from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { consentKeys, consentSelectors, State } from '~app/tip/reducers';

@Component({
  selector: 'bspl-tip-card-consent-list',
  templateUrl: './tip-card-consent-list.component.html',
  styleUrls: ['./tip-card-consent-list.component.scss'],
  providers: [TipCardConsentFormatter]
})
export class TipCardConsentListComponent extends ListSubtabs<TipCardConsent, TipCardConsentFilter> implements OnInit {
  @ViewChild('rowAirlineDetailTemplate', { static: true }) rowAirlineDetailTemplate: TemplateRef<any>;
  @ViewChild('rowAgentDetailTemplate', { static: true }) rowAgentDetailTemplate: TemplateRef<any>;

  public predefinedFilters: TipCardConsentFilter;

  public get isCardLevel(): boolean {
    return this.selectedLevelConsent === CardLevelConsent.Card;
  }

  public get isCountryLevel(): boolean {
    return this.selectedLevelConsent === CardLevelConsent.Country;
  }

  public isAirlineUser: boolean;
  public isAgentUser: boolean;
  public isUserMulticountry = false;

  public hasUpdateCardLevelPermission: boolean;
  public hasUpdateCountryLevelPermission: boolean;
  public hasLeanPermission: boolean;

  public get isConsentUpdateAvailable(): boolean {
    return (
      (this.isCardLevel && this.hasUpdateCardLevelPermission) ||
      (this.isCountryLevel && this.hasUpdateCountryLevelPermission)
    );
  }

  public get rowDetailsByUserType(): TemplateRef<any> {
    let result: TemplateRef<any> = null;

    if (this.isAgentUser) {
      result = this.rowAgentDetailTemplate;
    }

    if (this.isCardLevel && this.isAirlineUser) {
      result = this.rowAirlineDetailTemplate;
    }

    return result;
  }

  public query: DataQuery<TipCardConsentFilter>;

  public selectMode = SelectMode;
  public selectedLevelConsent: CardLevelConsent;
  public levelConsentOptions: DropdownOption<CardLevelConsent>[] = Object.values(CardLevelConsent).map(consent => ({
    value: consent,
    label: this.translationService.translate(`TIP.cards.consent.levelConsentDropdown.${consent}Label`)
  }));

  public btnDesign = ButtonDesign;
  public toolbarButtonOptions: { mainOption: MenuItem; options: MenuItem[] };

  public titleLabel: string;
  public noticeMessage: string;

  public searchForm: FormGroup;

  public columns: Array<GridColumn>;
  public actions: Array<{ action: GridTableActionType }>;

  public airlineDropdownOptions$: Observable<DropdownOption<AirlineSummary>[]>;
  public agentDropdownOptions$: Observable<DropdownOption<AgentSummary>[]>;
  public statusDropdownOptions: DropdownOption<TipStatus>[] = ArrayHelper.toDropdownOptions(Object.values(TipStatus));
  public cardTypeDropdownOptions: DropdownOption<TipCardType>[] = ArrayHelper.toDropdownOptions(
    Object.values(TipCardType)
  );
  public consentTypeDropdownOptions: DropdownOption<CardLevelConsentType>[] = Object.values(CardLevelConsentType).map(
    type => ({ value: type, label: this.translationService.translate(`TIP.cards.cardLevelConsentType.${type}`) })
  );

  public bspList$: Observable<DropdownOption<BspDto>[]>;

  public userTypes = UserType;

  protected initialFilters: TipCardConsentFilter = { status: TipStatus.Active };

  private formFactory: FormUtil;
  private bspControl: FormControl;

  private loggedUser: User;

  constructor(
    public displayFormatter: TipCardConsentFormatter,
    protected store: Store<AppState>,
    protected dataService: TipCardConsentService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    private formBuilder: FormBuilder,
    private agentDictionary: AgentDictionaryService,
    private dialogService: DialogService,
    private permissionsService: PermissionsService,
    private modificationDialogService: TipCardConsentModificationDialogService,
    private terminationDialogService: TipCardConsentTerminationDialogService,
    private airlineDictionary: AirlineDictionaryService,
    private router: Router,
    private bspsDictionaryService: BspsDictionaryService
  ) {
    super(store, dataService, actions$, translationService);
    this.formFactory = new FormUtil(this.formBuilder);

    this.initializeLoggedUser();
  }

  public loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  public loggedUserIsOfType(type: UserType): boolean {
    return type.toUpperCase() === this.loggedUser.userType.toUpperCase();
  }

  public ngOnInit(): void {
    this.setPredefinedFilters();
    super.ngOnInit();

    this.isUserMulticountry = this.permissionsService.hasPermission(Permissions.readBsps);

    this.initializePermissions();
    this.initializeQuery();
    this.initializeListView();
    this.initializeLabels();

    if (this.isAirlineUser) {
      this.initializeBspOptionsMulticountry();
    }
    this.initializeChangeListeners();
  }

  public onLevelConsentSelected(option: CardLevelConsent): void {
    this.changeLevelConsent(option);

    this.initializeListView();

    // Reset filters on level consent change
    this.onQueryChanged({ ...this.query, filterBy: { ...this.initialFilters, ...this.predefinedFilters } });
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      this.initializeFilterDropdowns();
    }
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.query
      },
      apiService: this.dataService
    });
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: TipCardConsent }): void {
    switch (action.actionType) {
      case GridTableActionType.AddModifyCardLevelConsent:
      case GridTableActionType.ModifyCardBspLevelConsent:
        this.modify([row]);

        break;
      case GridTableActionType.TerminateCardLevelConsent:
      case GridTableActionType.TerminateCardBspLevelConsent:
        this.terminate([row]);

        break;
    }
  }

  public onRowLinkClick(event: GridColumnMultipleLinkClick<TipCardConsent>): void {
    // To detect when airline column is clicked
    if (event.propertyName === 'airline.iataCode') {
      this.navigateToAirlineDetail(event.row.airline.id);
    }
  }

  public onViewAirline(id: number): void {
    this.navigateToAirlineDetail(id);
  }

  public navigateToAirlineDetail(id: number): void {
    this.router.navigate([ROUTES.TIP_CARDS_AIRLINE_DETAIL.url, id]);
  }

  public loadAgentByBsp(bsps: BspDto[]) {
    const filter: AgentDictionaryQueryFilters = { ...(bsps?.length && { bspId: bsps.map(({ id }) => id) }) };

    this.agentDropdownOptions$ = this.agentDictionary
      .getDropdownOptions(filter)
      .pipe(tap(options => this.updateOptionsControl(options)));
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<TipCardConsent, TipCardConsentFilter>,
    DefaultProjectorFn<ListSubtabsState<TipCardConsent, TipCardConsentFilter>>
  > {
    return consentSelectors[MasterDataType.TipCardConsent];
  }

  protected getListKey(): string {
    return consentKeys[MasterDataType.TipCardConsent];
  }

  private initializeLoggedUser(): void {
    this.loggedUser$().subscribe(user => (this.loggedUser = user));

    this.isAirlineUser = this.loggedUserIsOfType(UserType.AIRLINE);
    this.isAgentUser = this.loggedUserIsOfType(UserType.AGENT);
  }

  private updateOptionsControl(options: DropdownOption<AgentSummary>[]) {
    const agentControl = FormUtil.get<TipCardConsentFilter>(this.searchForm, 'agent');
    if (agentControl.value?.length) {
      const optionIds = options.map(option => option.value.id);

      agentControl.setValue(agentControl.value.filter(selectedOption => optionIds.includes(selectedOption.id)));
    }
  }

  private setPredefinedFilters() {
    this.predefinedFilters = this.isAirlineUser ? { ...this.predefinedFilters, level: CardLevelConsent.Country } : null;
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
    this.hasUpdateCardLevelPermission = this.permissionsService.hasPermission(permissions.updateTipCardCardConsent);
    this.hasUpdateCountryLevelPermission = this.permissionsService.hasPermission(
      permissions.updateTipCardCountryConsent
    );
  }

  private initializeQuery(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.query = query));
    this.query$.pipe(first()).subscribe(query => this.changeLevelConsent(query.filterBy.level));
  }

  private initializeLabels(): void {
    this.noticeMessage = this.isAirlineUser && this.isCountryLevel ? 'TIP.cards.consent.alertMessage' : null;
    this.titleLabel = this.isAirlineUser ? 'TIP.cards.consent.titleAirline' : 'TIP.cards.consent.titleAgent';
  }

  private initializeListView(): void {
    this.columns = this.buildColumns();
    this.actions = this.buildActions();

    if (this.isAirlineUser) {
      this.toolbarButtonOptions = this.buildToolbarButtonOptions();
    }

    if (this.searchForm) {
      this.searchForm.reset();
    } else {
      this.initializeForm();
    }
  }

  private initializeChangeListeners() {
    this.bspControl.valueChanges.subscribe((bsp: BspDto[]) => this.loadAgentByBsp(bsp));
  }

  private buildColumns(): Array<GridColumn> {
    const columns =
      (this.isCardLevel && this.isAirlineUser) || this.isAgentUser
        ? CARD_LEVEL_COLUMNS(this.isAirlineUser)
        : COUNTRY_LEVEL_COLUMNS;

    const typeColumn = columns.find(column => column.prop === 'type');
    typeColumn.pipe = {
      transform: type => (type ? this.translationService.translate(`TIP.cards.cardLevelConsentType.${type}`) : null)
    };

    const costRecoveryTypeColumn = columns.find(column => column.prop === 'costRecovery.type');
    costRecoveryTypeColumn.pipe = {
      transform: type => (type ? this.translationService.translate(`TIP.cards.costRecoveryType.${type}`) : null)
    };

    if (this.isAirlineUser) {
      const checkboxColumn = columns.find(column => column.prop === 'isRowSelected');
      checkboxColumn.hidden = this.isCountryLevel || !this.isConsentUpdateAvailable;
    }

    return columns;
  }

  private buildActions(): Array<{ action: GridTableActionType }> {
    if (!this.isAirlineUser) {
      return [];
    }

    return (this.isCardLevel && this.isAirlineUser) || this.isAgentUser ? CARD_LEVEL_ACTIONS : COUNTRY_LEVEL_ACTIONS;
  }

  private buildToolbarButtonOptions(): { mainOption: MenuItem; options: MenuItem[] } {
    let mainOption: MenuItem;

    const options: MenuItem[] = [
      {
        label: this.translationService.translate(
          'TIP.cards.consent.selectionToolbar.buttonOptionLabels.terminateConsent'
        ),
        command: () => this.bulkTerminate()
      }
    ];

    if (this.isCountryLevel) {
      mainOption = {
        label: this.translationService.translate(
          'TIP.cards.consent.selectionToolbar.buttonOptionLabels.addModifyConsent'
        ),
        command: () => this.bulkModify()
      };
    }

    if (this.isCardLevel) {
      mainOption = {
        label: this.translationService.translate('TIP.cards.consent.selectionToolbar.buttonOptionLabels.modifyConsent'),
        command: () => this.bulkModify()
      };
    }

    return { mainOption, options };
  }

  private initializeForm(): void {
    this.bspControl = this.formBuilder.control(null);

    this.searchForm = this.formFactory.createGroup<TipCardConsentFilter>({
      status: [],
      level: [],
      agent: [],
      fromCardNumber: [null, tipCardNumberValidator],
      toCardNumber: [null, tipCardNumberValidator],
      expiryDate: [],
      issuerName: [],
      paymentNetwork: [],
      cardType: [],
      companyName: [],
      type: [],
      effectiveFrom: [],
      effectiveTo: [],
      cardId: [],
      airline: [],
      bsps: this.bspControl
    });
  }

  private initializeBspOptionsMulticountry() {
    this.bspList$ = this.bspsDictionaryService
      .getAllBspsByPermissions(this.loggedUser.bspPermissions, getReadTipCardConsentPermissions())
      .pipe(map(bspList => bspList.map(toValueLabelObjectBsp)));
  }

  private changeLevelConsent(level: CardLevelConsent): void {
    this.selectedLevelConsent = level;
    this.predefinedFilters = { ...this.predefinedFilters, level };
  }

  private initializeFilterDropdowns(): void {
    if (this.isAirlineUser) {
      this.agentDropdownOptions$ = this.agentDictionary.getDropdownOptions();
    } else {
      this.airlineDropdownOptions$ = this.airlineDictionary.getDropdownOptions();
    }
  }

  private bulkModify(): void {
    this.selectedRows$.pipe(first()).subscribe(rows => this.modify(rows));
  }

  private modify(items: TipCardConsent[]): void {
    this.modificationDialogService.open(items).subscribe(button => {
      if (button === FooterButton.Modify) {
        this.onQueryChanged();
      }
    });
  }

  private bulkTerminate(): void {
    this.selectedRows$.pipe(first()).subscribe(rows => this.terminate(rows));
  }

  private terminate(items: TipCardConsent[]): void {
    this.terminationDialogService.open(items).subscribe(button => {
      if (button === FooterButton.Terminate) {
        this.onQueryChanged();
      }
    });
  }
}
