import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { GlobalAirline } from '~app/master-data/models';
import { GlobalAirlineService } from '~app/master-data/services';

@Injectable()
export class TipCardConsentAirlineDetailResolver implements Resolve<GlobalAirline> {
  private readonly idKey = 'airline-id';

  constructor(private globalAirlineService: GlobalAirlineService) {}

  public resolve(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): Observable<GlobalAirline> {
    const airlineId = Number(route.paramMap.get(this.idKey));

    if (!airlineId) {
      throw new Error(`Missing required path parameter "${this.idKey}" in route ${router.url}`);
    }

    return this.globalAirlineService.getBy(airlineId, true);
  }
}
