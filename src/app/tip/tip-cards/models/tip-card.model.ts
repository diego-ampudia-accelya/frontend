/* eslint-disable @typescript-eslint/naming-convention */
import { BspDto } from '~app/shared/models/bsp.model';

export interface TipCard {
  id: number;
  bsp: BspDto;
  agent: AgentDto;
  cardNumber: string;
  expiryDate: string;
  issuerName: string;
  paymentNetwork: string;
  cardType: TipCardType;
  companyName: string;
  cardLevelConsent: { type: CardLevelConsentType };
  effectiveFrom: Date;
  effectiveTo: Date;
  status: TipStatus;
  terminationReason?: string;
}

interface AgentDto {
  id: number;
  iataCode: string;
  name: string;
}

export enum TipCardType {
  Consumer = 'Consumer',
  Corporate = 'Corporate'
}

export enum TipStatus {
  Active = 'Active',
  Inactive = 'Inactive'
}

export enum CardLevelConsentType {
  Yes = 'yes',
  No = 'no',
  CostRecovery = 'costRecovery'
}

export enum TipCardTerminationReason {
  CardReplacedOrChangedBank = '1',
  AirlineDidntGiveConsent = '2',
  CustomerCardEnrolledByError = '3',
  NotUsingThisCardAnymore = '4',
  OtherReason = '5'
}
