export enum TipCardTerminationReasonText {
  CardReplacedOrChangedBank = 'Card Replaced / Changed Bank',
  AirlineDidntGiveConsent = "Airline(s) didn't give consent",
  CustomerCardEnrolledByError = 'Customer Card enrolled by error',
  NotUsingThisCardAnymore = 'Not using this card anymore'
}
