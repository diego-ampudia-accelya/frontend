import { CardLevelConsent } from './tip-card-consent.model';
import { CardLevelConsentType, TipCardType, TipStatus } from './tip-card.model';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';

export interface TipCardConsentFilter {
  status?: TipStatus;
  level?: CardLevelConsent;
  agent?: AgentSummary[];
  fromCardNumber?: string;
  toCardNumber?: string;
  expiryDate?: string;
  issuerName?: string;
  paymentNetwork?: string;
  cardType?: TipCardType[];
  companyName?: string;
  type?: CardLevelConsentType[];
  effectiveFrom?: Date[];
  effectiveTo?: Date[];
  cardId?: number;
  airline?: AirlineSummary[];
  bsps?: BspDto[];
}

export interface TipCardBEConsentFilter {
  agentId: string[];
  fromEffectiveFrom: string;
  toEffectiveFrom: string;
  fromEffectiveTo: string;
  toEffectiveTo: string;
  airlineIataCode: string[];
  bspId?: number[];
}
