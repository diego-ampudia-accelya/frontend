/* eslint-disable @typescript-eslint/naming-convention */
import { CardLevelConsentType, TipCard, TipStatus } from './tip-card.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';

export interface TipCardConsent {
  id: number;
  airline: AirlineDto;
  level: CardLevelConsent;
  card: TipCard;
  bsp: BspDto;
  type: CardLevelConsentType;
  costRecovery: CostRecovery;
  effectiveFrom: Date;
  effectiveTo: Date;
  status: TipStatus;
}

interface AirlineDto {
  id: number;
  iataCode: string;
}

interface CostRecovery {
  type: CostRecoveryType;
  amount: number;
  currency: Currency;
}

export enum CardLevelConsent {
  Country = 'country',
  Card = 'card'
}
