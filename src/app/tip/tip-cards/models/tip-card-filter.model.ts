import { CardLevelConsentType, TipCardTerminationReason, TipCardType, TipStatus } from './tip-card.model';
import { AgentSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';

export interface TipCardFilter {
  agent?: AgentSummary[];
  status?: TipStatus;
  fromCardNumber?: string;
  toCardNumber?: string;
  expiryDate?: string;
  issuerName?: string;
  paymentNetwork?: string;
  cardType?: TipCardType[];
  companyName?: string;
  cardLevelConsentType?: CardLevelConsentType[];
  effectiveFrom?: Date[];
  effectiveTo?: Date[];
  bsps?: BspDto[];
  terminationReason?: TipCardTerminationReason;
}

export interface TipCardBEFilter {
  agentId?: string[];
  status?: TipStatus;
  fromCardNumber?: string;
  toCardNumber?: string;
  expiryDate?: string;
  issuerName?: string;
  paymentNetwork?: string;
  cardType?: TipCardType[];
  companyName?: string;
  cardLevelConsentType?: CardLevelConsentType[];
  fromEffectiveFrom?: string;
  toEffectiveFrom?: string;
  fromEffectiveTo?: string;
  toEffectiveTo?: string;
  bspId?: number[];
  terminationReasonType?: TipCardTerminationReason;
}
