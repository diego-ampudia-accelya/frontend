export const permissions = {
  // READ
  readActiveTipCards: 'rTipCrdAc',
  readInactiveTipCards: 'rTipCrdIn',
  readActiveTipCardCardConsents: 'rTipCrdCnsCrAc',
  readInactiveTipCardCardConsents: 'rTipCrdCnsCrIn',
  readActiveTipCardCountryConsents: 'rTipCrdCnsCoAc',
  readInactiveTipCardCountryConsents: 'rTipCrdCnsCoIn',
  // UPDATE
  updateTipCard: 'uTipCrd',
  updateTipCardCardConsent: 'uTipCrdCnsCr',
  updateTipCardCountryConsent: 'uTipCrdCnsCo',
  // CREATE
  createTipCard: 'cTipCrd'
};

export function getReadTipCardListPermissions(): Array<string> {
  return [permissions.readActiveTipCards, permissions.readInactiveTipCards];
}

export function getReadTipCardConsentPermissions(): Array<string> {
  return [
    permissions.readActiveTipCardCardConsents,
    permissions.readInactiveTipCardCardConsents,
    permissions.readActiveTipCardCountryConsents,
    permissions.readInactiveTipCardCountryConsents
  ];
}

export function getReadTipCardPermissions(): Array<string> {
  return [...getReadTipCardListPermissions(), ...getReadTipCardConsentPermissions()];
}
