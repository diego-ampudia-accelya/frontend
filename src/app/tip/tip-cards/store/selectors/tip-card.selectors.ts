import { createSelector } from '@ngrx/store';

import { MasterDataType } from '~app/master-shared/models/master.model';
import { listSelectors } from '~app/tip/reducers';

export const canCreateCard = createSelector(listSelectors[MasterDataType.TipCard], state => state.canCreateCard);
