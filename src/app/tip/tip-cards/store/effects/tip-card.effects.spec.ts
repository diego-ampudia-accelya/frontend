import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';

import { TipCardCreationDialogService } from '../../dialogs/tip-card-creation-dialog/tip-card-creation-dialog.service';
import { TipCardFilter } from '../../models/tip-card-filter.model';
import { TipCardActions } from '../actions';
import { TipCardEffects } from './tip-card.effects';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabsActions } from '~app/shared/base/list-subtabs/actions';
import { FooterButton } from '~app/shared/components';
import { listKeys } from '~app/tip/reducers';

describe('TipCardEffects', () => {
  let effects: TipCardEffects;
  let actions$: Observable<Action>;
  let mockStore: MockStore<AppState>;

  const creationDialogServiceSpy = createSpyObject(TipCardCreationDialogService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TipCardEffects,
        provideMockActions(() => actions$),
        provideMockStore(),
        { provide: TipCardCreationDialogService, useValue: creationDialogServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.inject(TipCardEffects);
    mockStore = TestBed.inject<any>(Store);
  });

  it('should create', () => {
    expect(effects).toBeDefined();
  });

  describe('create card effect', () => {
    beforeEach(() => {
      actions$ = of(TipCardActions.createCard());
      creationDialogServiceSpy.open.calls.reset();
    });

    it('should dispatch list subtabs serch action on add dialog button click', fakeAsync(() => {
      creationDialogServiceSpy.open.and.returnValue(of(FooterButton.Add));
      spyOn(mockStore, 'dispatch');

      effects.createCard$.subscribe();

      expect(creationDialogServiceSpy.open).toHaveBeenCalled();
      expect(mockStore.dispatch).toHaveBeenCalledWith(
        ListSubtabsActions.search<TipCardFilter>(listKeys[MasterDataType.TipCard])({})
      );
    }));

    it('should not dispatch any action on cancel dialog button click', fakeAsync(() => {
      creationDialogServiceSpy.open.and.returnValue(of(FooterButton.Cancel));
      spyOn(mockStore, 'dispatch');

      effects.createCard$.subscribe();

      expect(creationDialogServiceSpy.open).toHaveBeenCalled();
      expect(mockStore.dispatch).not.toHaveBeenCalled();
    }));
  });
});
