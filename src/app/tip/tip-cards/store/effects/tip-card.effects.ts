import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, tap } from 'rxjs/operators';

import { TipCardCreationDialogService } from '../../dialogs/tip-card-creation-dialog/tip-card-creation-dialog.service';
import { TipCardFilter } from '../../models/tip-card-filter.model';
import { TipCardActions } from '../actions';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabsActions } from '~app/shared/base/list-subtabs/actions';
import { FooterButton } from '~app/shared/components';
import { listKeys } from '~app/tip/reducers';

@Injectable()
export class TipCardEffects {
  public createCard$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(TipCardActions.createCard),
        switchMap(() => this.creationDialogService.open()),
        tap(result => {
          if (result === FooterButton.Add) {
            this.refreshQuery();
          }
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private creationDialogService: TipCardCreationDialogService
  ) {}

  private refreshQuery(): void {
    this.store.dispatch(ListSubtabsActions.search<TipCardFilter>(listKeys[MasterDataType.TipCard])({}));
  }
}
