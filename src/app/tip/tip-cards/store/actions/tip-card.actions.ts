import { createAction, props } from '@ngrx/store';

export const changeCreateCardVisibility = createAction(
  '[TIP Card] Change Create Card Visibility',
  props<{ visibility: boolean }>()
);

export const createCard = createAction('[TIP Card] Create Card');
