import { TipCardActions } from '../actions';

import * as fromTipCard from './tip-card.reducer';

describe('TIP Card Reducer', () => {
  it('should set `canCreateCard` correctly on CHANGE CREATE CARD VISIBILITY action', () => {
    const action = TipCardActions.changeCreateCardVisibility({ visibility: true });
    const state = fromTipCard.reducer(fromTipCard.initialState, action);

    expect(state.canCreateCard).toBe(true);
  });
});
