import { createReducer, on } from '@ngrx/store';

import { changeCreateCardVisibility } from '../actions/tip-card.actions';

export interface TipCardState {
  canCreateCard: boolean;
}

export const initialState: TipCardState = {
  canCreateCard: false
};

export const reducer = createReducer(
  initialState,
  on(changeCreateCardVisibility, (state, { visibility }) => ({ ...state, canCreateCard: visibility }))
);
