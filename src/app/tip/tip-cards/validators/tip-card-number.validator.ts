import { AbstractControl } from '@angular/forms';

export function tipCardNumberValidator(control: AbstractControl): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  // RegExp to match six digits or less from / to card number
  const sixDigitsCardNumberPattern = /^\d{1,6}$/;

  if (control.value && !control.value.match(sixDigitsCardNumberPattern)) {
    result = {
      cardNumberPattern: { key: 'TIP.cards.errors.cardNumberPattern' }
    };
  }

  return result;
}
