import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Airline } from '~app/master-data/models';
import { appConfiguration } from '~app/shared/services';

@Component({
  selector: 'bspl-tip-card-consent-airline-detail',
  templateUrl: './tip-card-consent-airline-detail.component.html',
  styleUrls: ['./tip-card-consent-airline-detail.component.scss']
})
export class TipCardConsentAirlineDetailComponent implements OnInit {
  public logoUrl: string;
  public airline: Airline;

  constructor(private activatedRoute: ActivatedRoute) {}

  public ngOnInit(): void {
    const data = this.activatedRoute.snapshot.data;

    this.airline = data.item?.airlines[0];
    this.logoUrl = this.retrieveLogoAirline();
  }

  private retrieveLogoAirline() {
    return appConfiguration.getAirlineLogoUrl(this.airline.globalAirline.iataCode);
  }
}
