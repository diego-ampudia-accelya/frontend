import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

import { TipCardActions } from './store/actions';
import { canCreateCard } from './store/selectors/tip-card.selectors';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';

@Component({
  selector: 'bspl-tip-cards',
  templateUrl: './tip-cards.component.html',
  styleUrls: ['./tip-cards.component.scss']
})
export class TipCardsComponent implements OnInit {
  public tabs: RoutedMenuItem[];

  public btnDesign = ButtonDesign;
  public canCreateCard$: Observable<boolean> = this.store.pipe(
    delay(0), // To avoid "ExpressionChangedAfterItHasBeenCheckedError"
    select(canCreateCard)
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    private store: Store<AppState>
  ) {}

  public ngOnInit(): void {
    this.initializeTabs();
  }

  public onCreateCard(): void {
    this.store.dispatch(TipCardActions.createCard());
  }

  private initializeTabs(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }
}
