import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { TipCard } from '../../models/tip-card.model';
import { TipCardService } from '../../services/tip-card.service';
import { TipCardModificationDialogComponent } from './tip-card-modification-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class TipCardModificationDialogService {
  constructor(
    private dialogService: DialogService,
    private dataService: TipCardService,
    private notificationService: NotificationService
  ) {}

  public open(items: TipCard[]): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'TIP.cards.dialogs.card.modify.title',
        footerButtonsType: [{ type: FooterButton.Modify, isDisabled: true }],
        items
      }
    };

    return this.dialogService.open(TipCardModificationDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Modify ? this.modify(dialogConfig, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private modify(config: DialogConfig, component: TipCardModificationDialogComponent) {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.modify(config.data.items, component.form.value).pipe(
        tap(() => this.notificationService.showSuccess('TIP.cards.dialogs.card.modify.successMessage')),
        mapTo(FooterButton.Modify),
        finalize(() => setLoading(false))
      );
    });
  }
}
