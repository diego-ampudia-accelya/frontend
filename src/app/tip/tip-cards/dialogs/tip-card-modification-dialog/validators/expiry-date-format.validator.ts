import { AbstractControl } from '@angular/forms';

export function expiryDateFormatValidator(
  control: AbstractControl
): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  // Regular expression to ensure that expiry date input is on MM/YY format
  const monthYearDateRegexp = new RegExp('^(0[1-9]|1[0-2])/?([0-9]{2})$');

  if (control.value && !control.value.match(monthYearDateRegexp)) {
    result = {
      expiryDateFormat: { key: 'TIP.cards.dialogs.card.modify.fields.expiryDate.error' }
    };
  }

  return result;
}
