import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { TipCard, TipCardType } from '../../models/tip-card.model';
import { expiryDateFormatValidator } from './validators/expiry-date-format.validator';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { FormUtil, toValueLabelObject } from '~app/shared/helpers';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { RadioButton } from '~app/shared/models/radio-button.model';

@Component({
  selector: 'bspl-tip-card-modification-dialog',
  templateUrl: './tip-card-modification-dialog.component.html',
  styleUrls: ['./tip-card-modification-dialog.component.scss']
})
export class TipCardModificationDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public cards: TipCard[];
  public pills: Array<{ value: string; tooltip: string }>;

  public cardTypeRadioButtons: RadioButton[] = Object.values(TipCardType).map(toValueLabelObject);
  public isCompanyNameDisabled = true;

  private formFactory: FormUtil;
  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.cards = this.config.data.items;

    this.pills = this.cards.map(card => ({
      value: card.cardNumber,
      tooltip: this.getTooltipLabel(card)
    }));

    this.form = this.buildForm();

    this.initializeListeners();

    // Form is populated with card info if only one comes
    if (this.cards.length === 1) {
      this.populateForm(this.cards[0]);
    }
  }

  public getTooltipLabel(card: TipCard): string {
    return this.translationService.translate('TIP.cards.dialogs.card.modify.tooltipLabel', { card: card.cardNumber });
  }

  private buildForm(): FormGroup {
    return this.formFactory.createGroup<Partial<TipCard>>({
      expiryDate: [null, [Validators.required, expiryDateFormatValidator]],
      issuerName: [null, Validators.required],
      cardType: [null, Validators.required],
      companyName: [{ value: null, disabled: true }, Validators.required]
    });
  }

  private populateForm(card: TipCard): void {
    this.form.patchValue(card);
  }

  private initializeListeners(): void {
    this.initializeFormListener();
    this.initializeCardTypeListener();
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const modifyButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Modify
      );
      modifyButton.isDisabled = status === 'INVALID';
    });
  }

  private initializeCardTypeListener(): void {
    const cardTypeControl = FormUtil.get<Partial<TipCard>>(this.form, 'cardType');
    const companyNameControl = FormUtil.get<Partial<TipCard>>(this.form, 'companyName');

    cardTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (value === TipCardType.Corporate) {
        this.isCompanyNameDisabled = false;
        companyNameControl.enable();
      } else {
        this.isCompanyNameDisabled = true;
        companyNameControl.disable();
        companyNameControl.reset();
      }
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
