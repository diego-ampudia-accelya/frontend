import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { TipCard } from '../../models/tip-card.model';
import { TipCardTerminationReasonText } from '../../models/tip-card-termination-reason.model';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-tip-card-termination-dialog',
  templateUrl: './tip-card-termination-dialog.component.html',
  styleUrls: ['./tip-card-termination-dialog.component.scss']
})
export class TipCardTerminationDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public cards: TipCard[];
  public pills: Array<{ value: string; tooltip: string }>;

  public terminationReasons = [
    {
      value: TipCardTerminationReasonText.CardReplacedOrChangedBank,
      label: this.translationService.translate('TIP.cards.dialogs.card.terminate.reasons.cardReplaced')
    },
    {
      value: TipCardTerminationReasonText.AirlineDidntGiveConsent,
      label: this.translationService.translate('TIP.cards.dialogs.card.terminate.reasons.airlineConsent')
    },
    {
      value: TipCardTerminationReasonText.CustomerCardEnrolledByError,
      label: this.translationService.translate('TIP.cards.dialogs.card.terminate.reasons.customerCardEnrolled')
    },
    {
      value: TipCardTerminationReasonText.NotUsingThisCardAnymore,
      label: this.translationService.translate('TIP.cards.dialogs.card.terminate.reasons.notUsingCard')
    }
  ];

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService
  ) {}

  public ngOnInit(): void {
    this.cards = this.config.data.items;

    this.pills = this.cards.map(card => ({
      value: card.cardNumber,
      tooltip: this.getTooltipLabel(card)
    }));

    this.form = this.buildForm();

    this.initializeFormListener();
  }

  public getTooltipLabel(card: TipCard): string {
    return this.translationService.translate('TIP.cards.dialogs.card.terminate.tooltipLabel', {
      card: card.cardNumber
    });
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      terminationReason: [null, Validators.required]
    });
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const terminateButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Terminate
      );
      terminateButton.isDisabled = status === 'INVALID';
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
