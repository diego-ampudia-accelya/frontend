import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { TipCard } from '../../models/tip-card.model';
import { TipCardService } from '../../services/tip-card.service';
import { TipCardTerminationDialogComponent } from './tip-card-termination-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class TipCardTerminationDialogService {
  constructor(
    private dialogService: DialogService,
    private dataService: TipCardService,
    private notificationService: NotificationService
  ) {}

  public open(items: TipCard[]): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'TIP.cards.dialogs.card.terminate.title',
        footerButtonsType: [{ type: FooterButton.Terminate, isDisabled: true }],
        items
      }
    };

    return this.dialogService.open(TipCardTerminationDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Terminate ? this.terminate(dialogConfig, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private terminate(config: DialogConfig, component: TipCardTerminationDialogComponent) {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      const { terminationReason } = component.form.value;

      setLoading(true);

      return this.dataService.terminate(config.data.items, terminationReason).pipe(
        tap(() => this.notificationService.showSuccess('TIP.cards.dialogs.card.terminate.successMessage')),
        mapTo(FooterButton.Terminate),
        finalize(() => setLoading(false))
      );
    });
  }
}
