import { Component, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { CardLevelConsent, TipCardConsent } from '../../models/tip-card-consent.model';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';

@Component({
  selector: 'bspl-tip-card-consent-termination-dialog',
  templateUrl: './tip-card-consent-termination-dialog.component.html',
  styleUrls: ['./tip-card-consent-termination-dialog.component.scss']
})
export class TipCardConsentTerminationDialogComponent implements OnInit {
  public consents: TipCardConsent[];
  public level: CardLevelConsent;

  public isCardLevel: boolean;

  public pills: Array<{ value: string; tooltip: string }>;

  constructor(private config: DialogConfig, private translationService: L10nTranslationService) {}

  public ngOnInit(): void {
    this.consents = this.config.data.items;
    this.level = this.consents && this.consents[0]?.level;

    this.isCardLevel = this.level === CardLevelConsent.Card;

    if (this.isCardLevel) {
      this.pills = this.consents.map(consent => ({
        value: `${consent.card.agent.iataCode} / ${consent.card.cardNumber}`,
        tooltip: this.getTooltipLabel(consent)
      }));
    }
  }

  public getTooltipLabel(consent: TipCardConsent): string {
    return this.translationService.translate('TIP.cards.dialogs.consent.terminate.tooltipLabel', {
      agent: consent.card.agent.name,
      card: consent.card.cardNumber
    });
  }
}
