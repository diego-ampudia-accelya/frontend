import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { CardLevelConsent, TipCardConsent } from '../../models/tip-card-consent.model';
import { TipCardConsentService } from '../../services/tip-card-consent.service';
import { TipCardConsentTerminationDialogComponent } from './tip-card-consent-termination-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class TipCardConsentTerminationDialogService {
  constructor(
    private dialogService: DialogService,
    private dataService: TipCardConsentService,
    private notificationService: NotificationService
  ) {}

  public open(items: TipCardConsent[]): Observable<FooterButton> {
    const level: CardLevelConsent = items && items[0]?.level;

    const dialogConfig: DialogConfig = {
      data: {
        title: `TIP.cards.dialogs.consent.terminate.title.${level}`,
        footerButtonsType: [{ type: FooterButton.Terminate }],
        items
      }
    };

    return this.dialogService.open(TipCardConsentTerminationDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn }) =>
        clickedBtn === FooterButton.Terminate ? this.terminate(dialogConfig, items) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private terminate(config: DialogConfig, items: TipCardConsent[]) {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.terminate(items).pipe(
        tap(() => this.notificationService.showSuccess('TIP.cards.dialogs.consent.terminate.successMessage')),
        mapTo(FooterButton.Terminate),
        finalize(() => setLoading(false))
      );
    });
  }
}
