import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { uniqBy } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { CardLevelConsent, TipCardConsent } from '../../models/tip-card-consent.model';
import { CardLevelConsentType } from '../../models/tip-card.model';
import {
  ConsentRuleModel,
  CostRecoveryModel
} from '~app/master-data/configuration/tip/tip-local-airline/models/tip-local-airline.model';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { AlertMessageType } from '~app/shared/enums';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { RadioButton } from '~app/shared/models/radio-button.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';
import { costRecoveryAmountValidator } from '~app/tip/shared/validators/cost-recovery-amount.validator';

@Component({
  selector: 'bspl-tip-card-consent-modification-dialog',
  templateUrl: './tip-card-consent-modification-dialog.component.html',
  styleUrls: ['./tip-card-consent-modification-dialog.component.scss']
})
export class TipCardConsentModificationDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public costRecovery: keyof ConsentRuleModel = 'costRecovery';

  public consents: TipCardConsent[];
  public level: CardLevelConsent;

  public isCardLevel: boolean;
  public isCountryLevel: boolean;

  public pills: Array<{ value: string; tooltip: string }>;

  public alertMessage = {
    message: 'TIP.cards.dialogs.consent.modify.alertMessage',
    type: AlertMessageType.info
  };

  public currencyDropdownOptions$: Observable<DropdownOption<Currency>[]>;
  public consentTypeDropdownOptions: DropdownOption<CardLevelConsentType>[] = Object.values(CardLevelConsentType).map(
    type => ({ value: type, label: this.translationService.translate(`TIP.cards.cardLevelConsentType.${type}`) })
  );
  public costRecoveryTypeRadioButtons: RadioButton[] = Object.values(CostRecoveryType).map(type => ({
    value: type,
    label: this.translationService.translate(`TIP.cards.costRecoveryType.${type}`)
  }));

  public isCostRecoveryDisabled = true;
  public isCurrencyLocked = true;
  public isAmountDisabled = true;

  public amountLabel = `TIP.cards.dialogs.consent.modify.fields.amount.label.${CostRecoveryType.FixedAmount}`;
  public amountPlaceholder = `TIP.cards.dialogs.consent.modify.fields.amount.placeholder.${CostRecoveryType.FixedAmount}`;

  private formUtil: FormUtil;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  private readonly DEFAULT_DECIMALS = 2;

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private translationService: L10nTranslationService,
    private currencyDictionary: CurrencyDictionaryService
  ) {
    this.formUtil = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.consents = this.config.data.items;
    this.level = this.consents && this.consents[0]?.level;

    this.isCardLevel = this.level === CardLevelConsent.Card;
    this.isCountryLevel = this.level === CardLevelConsent.Country;

    if (this.isCardLevel) {
      this.pills = this.consents.map(consent => ({
        value: `${consent.card.agent.iataCode} / ${consent.card.cardNumber}`,
        tooltip: this.getTooltipLabel(consent)
      }));
    }

    this.form = this.buildForm();

    this.initializeDropdownOptions();
    this.initializeListeners();

    // Form is populated with consent info if only one comes
    if (this.consents.length === 1) {
      this.populateForm(this.consents[0]);
    }
  }

  public getTooltipLabel(consent: TipCardConsent): string {
    return this.translationService.translate('TIP.cards.dialogs.consent.modify.tooltipLabel', {
      agent: consent.card.agent.name,
      card: consent.card.cardNumber
    });
  }

  private buildForm(): FormGroup {
    const form = this.formUtil.createGroup<ConsentRuleModel>({
      type: [null, Validators.required],
      costRecovery: this.formUtil.createGroup<CostRecoveryModel>({
        type: [{ value: null, disabled: true }, Validators.required],
        currency: [{ value: null, disabled: true }, Validators.required],
        amount: [{ value: null, disabled: true }] // Validators are set later in this method
      })
    });

    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(form, 'costRecovery') as FormGroup;
    const costRecoveryTypeControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'type');
    const costRecoveryCurrencyControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'currency');
    const costRecoveryAmountControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'amount');

    costRecoveryAmountControl.setValidators([
      costRecoveryAmountValidator(costRecoveryTypeControl, costRecoveryCurrencyControl),
      Validators.required
    ]);

    return form;
  }

  private populateForm(consent: TipCardConsent): void {
    const consentTypeControl = FormUtil.get<ConsentRuleModel>(this.form, 'type');
    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;

    // Setting values
    consentTypeControl.setValue(consent.type);
    if (consent.type === CardLevelConsentType.CostRecovery) {
      costRecoveryGroup.patchValue(consent.costRecovery);
    }
  }

  private initializeDropdownOptions(): void {
    if (this.consents?.length) {
      // Retrieve currencies from consents selected
      const lostCurrencies: DropdownOption<Currency>[] = this.consents
        .filter(consent => consent.costRecovery?.currency)
        .map(item => ({ label: item.costRecovery.currency.code, value: item.costRecovery.currency }));

      this.currencyDropdownOptions$ = this.currencyDictionary.getTipDropdownOptions(this.consents[0].bsp.id, true).pipe(
        map((items: DropdownOption<Currency>[]) =>
          //create an array without elements duplicated of current currencies
          uniqBy([...items, ...lostCurrencies], 'value.id')
        )
      );
    }
  }

  private initializeListeners(): void {
    this.initializeFormListener();
    this.initializeConsentListener();
    this.initializeCostRecoveryListener();
    this.initializeCurrencyListener();
    this.initializeAmountListener();
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const modifyButton: ModalAction = this.config.data.buttons.find(
        (btn: ModalAction) => btn.type === FooterButton.Modify
      );
      modifyButton.isDisabled = status === 'INVALID';
    });
  }

  private initializeConsentListener(): void {
    const consentTypeControl = FormUtil.get<ConsentRuleModel>(this.form, 'type');

    consentTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (value === CardLevelConsentType.CostRecovery) {
        this.enableCostRecoveryForm();
      } else {
        this.disableCostRecoveryForm();
      }
    });
  }

  private enableCostRecoveryForm(): void {
    this.isCostRecoveryDisabled = false;

    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryTypeControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'type');

    costRecoveryTypeControl.enable();
  }

  private disableCostRecoveryForm(): void {
    this.isCostRecoveryDisabled = true;

    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;

    costRecoveryGroup.disable();
    costRecoveryGroup.reset();
  }

  private initializeCostRecoveryListener(): void {
    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryTypeControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'type');

    costRecoveryTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (value === CostRecoveryType.FixedAmount) {
        this.amountLabel = `TIP.cards.dialogs.consent.modify.fields.amount.label.${value}`;
        this.amountPlaceholder = `TIP.cards.dialogs.consent.modify.fields.amount.placeholder.${value}`;

        this.disableAmountControl();
        this.enableCurrencyControl();
      } else if (value === CostRecoveryType.Percentage) {
        this.amountLabel = `TIP.cards.dialogs.consent.modify.fields.amount.label.${value}`;
        this.amountPlaceholder = `TIP.cards.dialogs.consent.modify.fields.amount.placeholder.${value}`;

        this.enableAmountControl();
        this.disableCurrencyControl();
      }
    });
  }

  private enableAmountControl(): void {
    this.isAmountDisabled = false;

    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryAmountControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'amount');

    costRecoveryAmountControl.enable();
  }

  private disableAmountControl(): void {
    this.isAmountDisabled = true;

    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryAmountControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'amount');

    costRecoveryAmountControl.disable();
    costRecoveryAmountControl.reset();
  }

  private enableCurrencyControl(): void {
    this.isCurrencyLocked = false;

    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryCurrencyControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'currency');

    costRecoveryCurrencyControl.enable();
  }

  private disableCurrencyControl(): void {
    this.isCurrencyLocked = true;

    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryCurrencyControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'currency');

    costRecoveryCurrencyControl.disable();
    costRecoveryCurrencyControl.reset();
  }

  private initializeCurrencyListener(): void {
    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryCurrencyControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'currency');
    const costRecoveryAmountControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'amount');

    costRecoveryCurrencyControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (value) {
        this.enableAmountControl();
        if (costRecoveryAmountControl.value) {
          this.formatAmount(costRecoveryAmountControl.value, value.decimals);
        }
      }
    });
  }

  private initializeAmountListener(): void {
    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryCurrencyControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'currency');
    const costRecoveryAmountControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'amount');

    costRecoveryAmountControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (value) {
        // Amount is formatted with currency decimals if currency is set (fixed amount) OR with default decimals if not (percentage)
        const currency = costRecoveryCurrencyControl.value;
        const decimals = currency ? currency.decimals : this.DEFAULT_DECIMALS;

        if (decimals) {
          this.formatAmount(value, decimals);
        }
      }
    });
  }

  private formatAmount(amount: string, decimals: number): void {
    const costRecoveryGroup = FormUtil.get<ConsentRuleModel>(this.form, 'costRecovery') as FormGroup;
    const costRecoveryAmountControl = FormUtil.get<CostRecoveryModel>(costRecoveryGroup, 'amount');

    costRecoveryAmountControl.setValue(NumberHelper.formatNumber(amount, decimals), { emitEvent: false });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
