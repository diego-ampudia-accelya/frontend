import { Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { TipCardService } from '../../services/tip-card.service';
import { TipCardCreationDialogComponent } from './tip-card-creation-dialog.component';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { NotificationService } from '~app/shared/services';

@Injectable()
export class TipCardCreationDialogService {
  constructor(
    private dialogService: DialogService,
    private dataService: TipCardService,
    private notificationService: NotificationService
  ) {}

  public open(): Observable<FooterButton> {
    const dialogConfig: DialogConfig = {
      data: {
        title: 'TIP.cards.dialogs.card.create.title',
        footerButtonsType: [{ type: FooterButton.Add, isDisabled: true }]
      }
    };

    return this.dialogService.open(TipCardCreationDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn, contentComponentRef }) =>
        clickedBtn === FooterButton.Add ? this.create(dialogConfig, contentComponentRef) : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private create(config: DialogConfig, component: TipCardCreationDialogComponent) {
    const setLoading = (loading: boolean) =>
      config.data.buttons.forEach((button: ModalAction) => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return this.dataService.create(component.form.value).pipe(
        tap(() => this.notificationService.showSuccess('TIP.cards.dialogs.card.create.successMessage')),
        mapTo(FooterButton.Add),
        finalize(() => setLoading(false))
      );
    });
  }
}
