import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { TipCard, TipCardType } from '../../models/tip-card.model';
import { creditCardInputValidator } from './validators/credit-card-input.validator';
import { expiryDateFormatValidator } from './validators/expiry-date-format.validator';
import { paymentNetworkValidator } from './validators/payment-network-input.validator';
import * as fromAuth from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { FooterButton } from '~app/shared/components';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { FormUtil, toValueLabelObject } from '~app/shared/helpers';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { RadioButton } from '~app/shared/models/radio-button.model';

@Component({
  selector: 'bspl-tip-card-creation-dialog',
  templateUrl: './tip-card-creation-dialog.component.html',
  styleUrls: ['./tip-card-creation-dialog.component.scss']
})
export class TipCardCreationDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public cardTypeRadioButtons: RadioButton[] = Object.values(TipCardType).map(toValueLabelObject);
  public isCompanyNameDisabled = true;

  private formFactory: FormUtil;

  private userBsps$ = this.store.pipe(
    select(fromAuth.getUserBsps),
    first(bsps => !!bsps?.length)
  );
  private destroy$ = new Subject();

  constructor(private config: DialogConfig, private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.form = this.buildForm();

    this.userBsps$.subscribe(bsps => this.form.patchValue({ bsp: { id: bsps[0].id } }));

    this.initializeListeners();
  }
  private buildForm(): FormGroup {
    return this.formFactory.createGroup<Partial<TipCard>>({
      bsp: [],
      cardNumber: [null, [Validators.required, creditCardInputValidator]],
      expiryDate: [null, [Validators.required, expiryDateFormatValidator]],
      issuerName: [null, Validators.required],
      paymentNetwork: [null, [Validators.required, paymentNetworkValidator]],
      cardType: [null, Validators.required],
      companyName: [{ value: null, disabled: true }, Validators.required]
    });
  }

  private initializeListeners(): void {
    this.initializeFormListener();
    this.initializeCardTypeListener();
  }

  private initializeFormListener(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      const addButton: ModalAction = this.config.data.buttons.find((btn: ModalAction) => btn.type === FooterButton.Add);
      addButton.isDisabled = status === 'INVALID';
    });
  }

  private initializeCardTypeListener(): void {
    const cardTypeControl = FormUtil.get<Partial<TipCard>>(this.form, 'cardType');
    const companyNameControl = FormUtil.get<Partial<TipCard>>(this.form, 'companyName');

    cardTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(value => {
      if (value === TipCardType.Corporate) {
        this.isCompanyNameDisabled = false;
        companyNameControl.enable();
      } else {
        this.isCompanyNameDisabled = true;
        companyNameControl.disable();
        companyNameControl.reset();
      }
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
