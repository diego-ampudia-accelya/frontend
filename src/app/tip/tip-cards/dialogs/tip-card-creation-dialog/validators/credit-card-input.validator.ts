import { AbstractControl } from '@angular/forms';

export function creditCardInputValidator(
  control: AbstractControl
): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  // Regular expression to ensure that card number contain 13-40 digits.
  const creditCardRegexp = new RegExp('^[0-9]{13,40}$');
  if (control.value && !control.value.match(creditCardRegexp)) {
    result = {
      creditCardFormat: { key: 'TIP.cards.dialogs.card.create.fields.cardNumber.error' }
    };
  }

  return result;
}
