import { AbstractControl } from '@angular/forms';

export function paymentNetworkValidator(control: AbstractControl): { [key: string]: { [key: string]: string } } | null {
  let result = null;

  // Regular expression to ensure that Payment network has min lenght and max length of 2.
  const paymentNetworkRangeExp = new RegExp('^[a-zA-Z0-9]{2}$');

  if (control.value && !control.value.match(paymentNetworkRangeExp)) {
    result = {
      expiryDateFormat: { key: 'TIP.cards.dialogs.card.create.fields.paymentNetwork.error' }
    };
  }

  return result;
}
