import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import { TipCardConsentFilter } from '../tip-cards/models/tip-card-consent-filter.model';
import { TipCardConsent } from '../tip-cards/models/tip-card-consent.model';
import { TipCardFilter } from '../tip-cards/models/tip-card-filter.model';
import { TipCard } from '../tip-cards/models/tip-card.model';
import * as fromCardList from '../tip-cards/store/reducers/tip-card.reducer';
import * as fromProductList from '../tip-prov-products/components/store';
import { TipProductConsentFilter } from '../tip-prov-products/models/tip-product-consent-filter.model';
import { TipProductConsent } from '../tip-prov-products/models/tip-product-consent.model';
import { TipProductFilter } from '../tip-prov-products/models/tip-product-filter.model';
import { TipProduct } from '../tip-prov-products/models/tip-product.model';
import { combineReducersCustom } from '~app/shared/helpers';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { AppState } from '~app/reducers';
import { MasterDataType } from '~app/master-shared/models/master.model';

export const tipFeatureKey = 'tip';

// List tab keys
const listTabId = 'list';
export const listKeys = {
  [MasterDataType.TipCard]: fromListSubtabs.getKey(MasterDataType.TipCard, listTabId),
  [MasterDataType.TipProduct]: fromListSubtabs.getKey(MasterDataType.TipProduct, listTabId)
};

// Consent tab keys
const consentTabId = 'consent';
export const consentKeys = {
  [MasterDataType.TipCardConsent]: fromListSubtabs.getKey(MasterDataType.TipCardConsent, consentTabId),
  [MasterDataType.TipProductConsent]: fromListSubtabs.getKey(MasterDataType.TipProductConsent, consentTabId)
};

export interface TipState {
  [key: string]: any;
}

export interface State extends AppState {
  [tipFeatureKey]: TipState;
}

export function reducers(state: TipState | undefined, action: Action) {
  return combineReducers({
    [listKeys[MasterDataType.TipCard]]: combineReducersCustom(
      fromListSubtabs.reducer<TipCard, TipCardFilter>(listKeys[MasterDataType.TipCard]),
      fromCardList.reducer
    ),
    [listKeys[MasterDataType.TipProduct]]: fromListSubtabs.reducer<TipProduct, TipProductFilter>(
      listKeys[MasterDataType.TipProduct]
    ),
    [consentKeys[MasterDataType.TipProductConsent]]: combineReducersCustom(
      fromListSubtabs.reducer<TipProductConsent, TipProductConsentFilter>(
        consentKeys[MasterDataType.TipProductConsent]
      ),
      fromProductList.reducer
    ),
    [consentKeys[MasterDataType.TipCardConsent]]: fromListSubtabs.reducer<TipCardConsent, TipCardConsentFilter>(
      consentKeys[MasterDataType.TipCardConsent]
    )
  })(state, action);
}

export const getTipState = createFeatureSelector<State, TipState>(tipFeatureKey);

export const listSelectors = {
  [MasterDataType.TipCard]: createSelector(getTipState, state => state[listKeys[MasterDataType.TipCard]]),
  [MasterDataType.TipProduct]: createSelector(getTipState, state => state[listKeys[MasterDataType.TipProduct]])
};

export const consentSelectors = {
  [MasterDataType.TipCardConsent]: createSelector(
    getTipState,
    state => state[consentKeys[MasterDataType.TipCardConsent]]
  ),
  [MasterDataType.TipProductConsent]: createSelector(
    getTipState,
    state => state[consentKeys[MasterDataType.TipProductConsent]]
  )
};
