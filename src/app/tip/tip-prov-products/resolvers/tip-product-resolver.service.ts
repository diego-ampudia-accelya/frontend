import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { TipProduct } from '../models/tip-product.model';
import { TipProductService } from '../services/tip-product.service';

@Injectable()
export class TipProductResolver implements Resolve<TipProduct> {
  constructor(private tipProductService: TipProductService) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<TipProduct> {
    return this.tipProductService.getOne(activatedRouteSnapshot.params['id']);
  }
}
