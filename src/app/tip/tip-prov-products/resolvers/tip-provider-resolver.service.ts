import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { TipProvider } from '../models/tip-provider.model';
import { TipProviderService } from '../services/tip-provider.service';

@Injectable()
export class TipProviderResolver implements Resolve<TipProvider> {
  constructor(private tipProviderService: TipProviderService) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<TipProvider> {
    return this.tipProviderService.getOne(activatedRouteSnapshot.params['id']);
  }
}
