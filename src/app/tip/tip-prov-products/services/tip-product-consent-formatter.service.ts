import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { TipProductConsentFilter } from '../models/tip-product-consent-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { bspFilterTagMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class TipProductConsentFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: TipProductConsentFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<TipProductConsentFilter> = {
      status: status => `${this.translate('status')} - ${status}`,
      bsp: bsps => `${this.translate('bsp')} - ${bspFilterTagMapper(bsps).join(', ')}`,
      provider: providers => `${this.translate('provider')} - ${providers.map(provider => provider.code).join(', ')}`,
      product: products => `${this.translate('product')} - ${products.map(product => product.code).join(', ')}`,
      agent: agents => `${this.translate('agent')} - ${agents.map(agent => agent.code).join(', ')}`,
      airline: airlines => `${this.translate('airline')} - ${airlines.map(airline => airline.code).join(', ')}`,
      type: types =>
        `${this.translate('type')} - ${types
          .map(type => this.translate(type, 'TIP.products.levelConsentType'))
          .join(', ')}`,
      costRecoveryType: type =>
        `${this.translate('costRecoveryType')} - ${this.translate(type, 'TIP.products.costRecoveryType')}`,
      costRecoveryCurrency: currencies =>
        `${this.translate('costRecoveryCurrency')} - ${currencies.map(currency => currency.code).join(', ')}`,
      effectiveFrom: date => `${this.translate('effectiveFrom')} - ${rangeDateFilterTagMapper(date)}`,
      effectiveTo: date => `${this.translate('effectiveTo')} - ${rangeDateFilterTagMapper(date)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string, prefix = 'TIP.productConsents.filters.labels'): string {
    return this.translationService.translate(`${prefix}.${key}`);
  }
}
