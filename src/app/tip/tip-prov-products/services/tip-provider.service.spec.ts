import { HttpClient } from '@angular/common/http';
import { fakeAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { TipProvider } from '../models/tip-provider.model';
import { TipProviderService } from './tip-provider.service';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services';

describe('TipProviderService', () => {
  let service: TipProviderService;
  const httpClientSpy = createSpyObject(HttpClient);

  beforeEach(() => {
    httpClientSpy.get.and.returnValue(of());
    service = new TipProviderService(httpClientSpy, { baseApiPath: '' } as AppConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should call GET TIP provider endpoint with id', fakeAsync(() => {
    service.getOne(1).subscribe();
    expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/providers/1'));
  }));

  describe('GET providers', () => {
    it('should call GET providers endpoint with `status param', fakeAsync(() => {
      service.getProviders().subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('/tip-management/providers'),
        jasmine.objectContaining({ params: { status: 'Active' } })
      );
    }));

    it('should call GET providers endpoint without `bspId` param', fakeAsync(() => {
      service.getProviders().subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/providers'), {
        params: { status: 'Active' }
      });
    }));

    it('should call GET providers endpoint with empty `bspId` param', fakeAsync(() => {
      service.getProviders([]).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/providers'), {
        params: { status: 'Active' }
      });
    }));

    it('should call GET providers endpoint with `bspId` param', fakeAsync(() => {
      const bspsMock: BspDto[] = [
        { id: 1, isoCountryCode: 'ES', name: 'Spain' },
        { id: 2, isoCountryCode: 'MT', name: 'Malta' }
      ];

      service.getProviders(bspsMock).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('/tip-management/providers'),
        jasmine.objectContaining({
          params: {
            status: 'Active',
            bspId: '1,2'
          }
        })
      );
    }));

    it('should build providers dropdown options with ISOC prefix', fakeAsync(() => {
      let providersDropdownOptions: DropdownOption[];

      const providersMock: TipProvider[] = [
        {
          id: 1,
          code: 'PROV1',
          name: 'provider1',
          bsp: { id: 1, isoCountryCode: 'ES', name: 'Spain' }
        },
        {
          id: 2,
          code: 'PROV2',
          name: 'provider2',
          bsp: { id: 2, isoCountryCode: 'MT', name: 'Malta' }
        }
      ];

      spyOn(service, 'getProviders').and.returnValue(of(providersMock));

      service.getProvidersDropdownOptions().subscribe(options => (providersDropdownOptions = options));

      expect(providersDropdownOptions).toEqual([
        {
          value: jasmine.anything(),
          label: 'ES - PROV1 / provider1'
        },
        {
          value: jasmine.anything(),
          label: 'MT - PROV2 / provider2'
        }
      ]);
    }));
  });
});
