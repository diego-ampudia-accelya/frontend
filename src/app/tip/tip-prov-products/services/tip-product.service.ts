import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { TipProductBEFilter, TipProductFilter } from '../models/tip-product-filter.model';
import { TipProduct, TipStatus } from '../models/tip-product.model';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat, DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class TipProductService implements Queryable<TipProduct> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/products`;

  private sortMapper = [
    { from: 'provider.name', to: 'providerName' },
    { from: 'name', to: 'productName' },
    { from: 'countryLevelConsent.type', to: 'countryLevelConsentType' }
  ];

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public getProducts(bsps?: BspDto[]): Observable<PagedData<TipProduct>> {
    const params = {
      status: TipStatus.Active,
      ...(bsps?.length && { bspId: bsps.map(({ id }) => '' + id).join() })
    };

    return this.http.get<PagedData<TipProduct>>(this.baseUrl, { params });
  }

  public find(query: DataQuery<TipProductFilter>): Observable<PagedData<TipProduct>> {
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<TipProduct>>(this.baseUrl + requestQuery.getQueryString());
  }

  public getOne(id: number): Observable<TipProduct> {
    return this.http.get<TipProduct>(`${this.baseUrl}/${id}`);
  }

  public getProductsDropdownOptions(bsps?: BspDto[]): Observable<DropdownOption<TipProduct>[]> {
    return this.getProducts(bsps).pipe(
      map(items =>
        items.records.map(item => ({
          value: item,
          label: `${item.bsp.isoCountryCode} - ${item.code} / ${item.name}`
        }))
      )
    );
  }

  public download(
    query: DataQuery<TipProductFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat.toUpperCase() };
    const url = this.baseUrl + requestQuery.getQueryString();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<TipProductFilter>): RequestQuery<TipProductBEFilter> {
    const {
      effectiveFrom,
      effectiveTo,
      products,
      providers,
      bsps,
      countryLevelConsentCostRecoveryCurrency,
      ...filterBy
    } = query.filterBy;

    // TODO: add BE filter model as generic type to `fromDataQuery` factory method
    return RequestQuery.fromDataQuery<TipProductBEFilter>({
      ...query,
      filterBy: {
        ...filterBy,
        bspId: bsps && bsps.map(({ id }) => id),
        productId: products && products.map(({ id }) => id),
        providerId: providers && providers.map(({ id }) => id),
        countryLevelConsentCostRecoveryCurrency:
          countryLevelConsentCostRecoveryCurrency &&
          countryLevelConsentCostRecoveryCurrency.map(currency => currency.code),
        fromEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[0]),
        toEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[1] ? effectiveFrom[1] : effectiveFrom[0]),
        fromEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[0]),
        toEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[1] ? effectiveTo[1] : effectiveTo[0])
      },
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
  }
}
