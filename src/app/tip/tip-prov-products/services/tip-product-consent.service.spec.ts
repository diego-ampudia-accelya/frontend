import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { TipProductConsentFilter } from '../models/tip-product-consent-filter.model';
import { TipProductConsentPostBE, TipProductConsentTerminateBE } from '../models/tip-product-consent.model';
import { CountryLevelConsentType, TipProduct } from '../models/tip-product.model';
import { TipProvider } from '../models/tip-provider.model';
import { TipProductConsentService } from './tip-product-consent.service';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { AgentSummary, AirlineSummary, DownloadFormat } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';
import { TipCurrency } from '~app/tip/shared/models/tip-currency.model';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';

describe('TipProductConsentService', () => {
  let service: TipProductConsentService;
  const httpClientSpy = createSpyObject(HttpClient);

  const initialState = {
    auth: {
      user: {
        userType: UserType.AIRLINE
      }
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TipProductConsentService,
        provideMockStore({ initialState }),
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } }
      ]
    });
  });

  it('should be created', () => {
    service = TestBed.inject(TipProductConsentService);
    expect(service).toBeDefined();
  });

  describe('service URL', () => {
    beforeEach(() => {
      httpClientSpy.get.and.returnValue(of());
      httpClientSpy.get.calls.reset();
    });

    it('should call endpoint for agents when user is an AGENT', () => {
      const store = TestBed.inject<any>(Store);

      store.overrideSelector(getUserType, UserType.AGENT);
      store.refreshState();

      service = TestBed.inject(TipProductConsentService);

      service.find(defaultQuery).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(
        jasmine.stringMatching('/tip-management/product-consents-for-agents')
      );
    });

    it('should call base endpoint when user is NOT an AGENT', () => {
      service = TestBed.inject(TipProductConsentService);

      service.download(defaultQuery, DownloadFormat.TXT).subscribe();
      expect(httpClientSpy.get).not.toHaveBeenCalledWith(
        jasmine.stringMatching('/tip-management/product-consents-for-agents')
      );
    });
  });

  describe('service methods', () => {
    beforeEach(() => {
      service = TestBed.inject(TipProductConsentService);
    });

    describe('find', () => {
      beforeEach(() => {
        httpClientSpy.get.and.returnValue(of());
        httpClientSpy.get.calls.reset();
      });

      it('should include BSP filters in request when specified', fakeAsync(() => {
        const bspsMock: BspDto[] = [
          { id: 1, isoCountryCode: 'ES', name: 'Spain' },
          { id: 2, isoCountryCode: 'MT', name: 'Malta' }
        ];

        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          filterBy: { bsp: bspsMock }
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('bspId=1,2'));
      }));

      it('should include PRODUCT filters in request when specified', fakeAsync(() => {
        const productsMock: TipProduct[] = [
          { id: 1, code: 'PROD1' },
          { id: 2, code: 'PROD2' }
        ] as TipProduct[];

        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          filterBy: { product: productsMock }
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('productId=1,2'));
      }));

      it('should include PROVIDER filters in request when specified', fakeAsync(() => {
        const providersMock: TipProvider[] = [
          { id: 1, code: 'PROV1', name: 'provider1' },
          { id: 2, code: 'PROV2', name: 'provider2' }
        ];

        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          filterBy: { provider: providersMock }
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('providerId=1,2'));
      }));

      it('should include AGENT filters in request when specified', fakeAsync(() => {
        const agentsMock: AgentSummary[] = [
          { id: '1', code: 'AGENT1', name: 'agent1' },
          { id: '2', code: 'AGENT2', name: 'agent2' }
        ];

        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          filterBy: { agent: agentsMock }
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('agentId=1,2'));
      }));

      it('should include AIRLINE filters in request when specified', fakeAsync(() => {
        const airlinesMock: AirlineSummary[] = [
          { id: 1, code: 'AIRL1', name: 'airline1', designator: 'L' },
          { id: 2, code: 'AIRL2', name: 'airline2', designator: 'W' }
        ];

        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          filterBy: { airline: airlinesMock }
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('airlineIataCode=AIRL1,AIRL2'));
      }));

      it('should include COST RECOVERY CURRENCY filters in request when specified', fakeAsync(() => {
        const costRecoveryCurrenciesMock: TipCurrency[] = [
          { id: 1, code: 'EUR', decimals: 2, isDefault: true },
          { id: 2, code: 'ESP', decimals: 0, isDefault: false }
        ];

        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          filterBy: { costRecoveryCurrency: costRecoveryCurrenciesMock }
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('costRecoveryCurrency=EUR,ESP'));
      }));

      it('should include EFFECTIVE FROM filters in request when specified', fakeAsync(() => {
        const effectiveFromUniqueMock = [new Date('2020/07/14')];
        const effectiveFromRangeMock = [new Date('2020/07/14'), new Date('2020/07/21')];

        // Query filter does not include a range for `effectiveFrom`
        service.find({ ...defaultQuery, filterBy: { effectiveFrom: effectiveFromUniqueMock } }).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(
          jasmine.stringMatching('fromEffectiveFrom=2020-07-14&toEffectiveFrom=2020-07-14')
        );

        // Query filter includes a range for `effectiveFrom`
        service.find({ ...defaultQuery, filterBy: { effectiveFrom: effectiveFromRangeMock } }).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(
          jasmine.stringMatching('fromEffectiveFrom=2020-07-14&toEffectiveFrom=2020-07-21')
        );
      }));

      it('should include EFFECTIVE TO filters in request when specified', fakeAsync(() => {
        const effectiveToUniqueMock = [new Date('2020/07/21')];
        const effectiveToRangeMock = [new Date('2020/07/21'), new Date('2020/07/28')];

        // Query filter does not include a range for `effectiveTo`
        service.find({ ...defaultQuery, filterBy: { effectiveTo: effectiveToUniqueMock } }).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(
          jasmine.stringMatching('fromEffectiveTo=2020-07-21&toEffectiveTo=2020-07-21')
        );

        // Query filter includes a range for `effectiveTo`
        service.find({ ...defaultQuery, filterBy: { effectiveTo: effectiveToRangeMock } }).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(
          jasmine.stringMatching('fromEffectiveTo=2020-07-21&toEffectiveTo=2020-07-28')
        );
      }));

      it('should map PROVIDER NAME sort criteria', fakeAsync(() => {
        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          sortBy: [{ attribute: 'product.provider.name', sortType: SortOrder.Desc }]
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('sort=providerName,DESC'));
      }));

      it('should map COST RECOVERY CURRENCY sort criteria', fakeAsync(() => {
        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          sortBy: [{ attribute: 'costRecovery.currency.code', sortType: SortOrder.Asc }]
        };

        service.find(query).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('sort=costRecoveryCurrency,ASC'));
      }));
    });

    it('should include ID in GET `getOne` method request endpoint', fakeAsync(() => {
      httpClientSpy.get.and.returnValue(of());

      service.getOne(1).subscribe();
      expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('/tip-management/product-consents/1'));
    }));

    it('should include consents in POST `createConsent` method request body', fakeAsync(() => {
      const consentsMock: TipProductConsentPostBE[] = [
        {
          agent: { id: 1 },
          bsp: { id: 1 },
          product: { id: 1 },
          type: CountryLevelConsentType.Yes,
          costRecovery: {
            type: CostRecoveryType.FixedAmount,
            amount: 14,
            currency: {
              id: 1,
              code: 'EUR',
              decimals: 2
            }
          },
          effectiveFrom: '2020-01-01'
        }
      ];

      httpClientSpy.post.and.returnValue(of());

      service.createConsent(consentsMock).subscribe();
      expect(httpClientSpy.post).toHaveBeenCalledWith(
        jasmine.stringMatching('/tip-management/product-consents'),
        consentsMock
      );
    }));

    it('should include consents in PUT `modifyConsent` method request body', fakeAsync(() => {
      const consentsMock: TipProductConsentTerminateBE[] = [
        { id: 1, effectiveTo: '2020-01-01' },
        { id: 2, effectiveTo: '2020-02-02' }
      ];

      httpClientSpy.put.and.returnValue(of());

      service.modifyConsent(consentsMock).subscribe();
      expect(httpClientSpy.put).toHaveBeenCalledWith(
        jasmine.stringMatching('/tip-management/product-consents'),
        consentsMock
      );
    }));

    describe('download', () => {
      beforeEach(() => {
        httpClientSpy.get.and.returnValue(of());
        httpClientSpy.get.calls.reset();
      });

      it('should include actual query in request', fakeAsync(() => {
        const bspsMock: BspDto[] = [
          { id: 1, isoCountryCode: 'ES', name: 'Spain' },
          { id: 2, isoCountryCode: 'MT', name: 'Malta' }
        ];

        const query: DataQuery<TipProductConsentFilter> = {
          ...defaultQuery,
          filterBy: { bsp: bspsMock }
        };

        service.download(query, DownloadFormat.CSV).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(
          jasmine.stringMatching('page=0&size=20&bspId=1,2'),
          jasmine.anything()
        );
      }));

      it('should call endpoint with specified download format', fakeAsync(() => {
        service.download(defaultQuery, DownloadFormat.TXT).subscribe();
        expect(httpClientSpy.get).toHaveBeenCalledWith(jasmine.stringMatching('exportAs=TXT'), jasmine.anything());
      }));
    });
  });
});
