import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { TipProductFilter } from '../models/tip-product-filter.model';
import { AppliedFilter, FilterFormatter } from '~app/shared/components/list-view';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { bspFilterTagMapper, rangeDateFilterTagMapper } from '~app/shared/helpers';

type FilterMappers<T> = { [P in keyof T]: (value: T[P]) => string };

@Injectable()
export class TipProductFormatter implements FilterFormatter {
  constructor(private translationService: L10nTranslationService) {}

  public format(filter: TipProductFilter): AppliedFilter[] {
    const filterMappers: FilterMappers<TipProductFilter> = {
      status: status => `${this.translate('status')} - ${status}`,
      bsps: bsps => `${this.translate('bsps')} - ${bspFilterTagMapper(bsps)}`,
      providers: providers =>
        `${this.translate('providers')} - ${providers
          .map(provider => `${provider.code} / ${provider.name}`)
          .join(', ')}`,
      products: products =>
        `${this.translate('products')} - ${products.map(product => `${product.code} / ${product.name}`).join(', ')}`,
      category: category => `${this.translate('category')} - ${category}`,
      issuerName: issuerName => `${this.translate('issuerName')} - ${issuerName}`,
      paymentNetwork: paymentNetwork => `${this.translate('paymentNetwork')} - ${paymentNetwork}`,
      formOfPayment: formOfPayment => `${this.translate('formOfPayment')} - ${formOfPayment}`,
      countryLevelConsentType: countryLevelConsentType =>
        `${this.translate('bspConsent')} - ${countryLevelConsentType
          .map(type => this.translate(type, 'TIP.products.levelConsentType'))
          .join(', ')}`,
      countryLevelConsentCostRecoveryType: type =>
        `${this.translate('bspConsentCostRecoveryType')} - ${this.translate(type, 'TIP.products.costRecoveryType')}`,
      countryLevelConsentCostRecoveryCurrency: currencies =>
        `${this.translate('bspConsentCostRecoveryCurrency')} - ${currencies.map(currency => currency.code).join(', ')}`,
      effectiveFrom: date => `${this.translate('effectiveFrom')} - ${rangeDateFilterTagMapper(date)}`,
      effectiveTo: date => `${this.translate('effectiveTo')} - ${rangeDateFilterTagMapper(date)}`
    };

    return Object.entries(filter || {})
      .map(([key, value]) => ({ key, value, mapper: filterMappers[key] }))
      .filter(item => !isEmpty(item.value) && item.mapper)
      .map(item => ({
        keys: [item.key],
        label: item.mapper(item.value)
      }));
  }

  private translate(key: string, prefix = 'TIP.products.filters.labels'): string {
    return this.translationService.translate(`${prefix}.${key}`);
  }
}
