import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { defer, Observable, of } from 'rxjs';
import { finalize, first, mapTo, switchMap, tap } from 'rxjs/operators';

import { TipProductAddAgentConsentDialogComponent } from '../components/dialogs/tip-product-add-agent-consent-dialog/tip-product-add-agent-consent-dialog.component';
import { TipProductModifyConsentDialogComponent } from '../components/dialogs/tip-product-modify-consent-dialog/tip-product-modify-consent-dialog.component';
import { TipProductTerminateConsentDialogComponent } from '../components/dialogs/tip-product-terminate-consent-dialog/tip-product-terminate-consent-dialog.component';
import {
  TipProductConsent,
  TipProductConsentPostBE,
  TipProductConsentPostView,
  TipProductConsentPutBE,
  TipProductConsentPutView,
  TipProductConsentTerminateBE,
  TipProductConsentTerminateView
} from '../models/tip-product-consent.model';
import { TipProductConsentLevel } from '../models/tip-product.model';

import { TipProductConsentService } from './tip-product-consent.service';
import { NotificationService } from '~app/shared/services';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { DialogConfig, DialogService, FooterButton } from '~app/shared/components';

@Injectable({
  providedIn: 'root'
})
export class TipProductDialogService {
  constructor(
    private dialogService: DialogService,
    private dataService: TipProductConsentService,
    private notificationService: NotificationService
  ) {}

  public openCreateAgentConsentDialog(): Observable<any> {
    const form = new FormGroup({});

    const dialogConfig = {
      data: {
        title: 'TIP.productConsents.dialogs.createAgentConsent.title',
        successMessage: 'TIP.productConsents.dialogs.createAgentConsent.successMessage',
        footerButtonsType: FooterButton.AddConsent,
        hasCancelButton: true,
        isInWideMode: true,
        form
      }
    };

    return this.dialogService.open(TipProductAddAgentConsentDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn }) =>
        clickedBtn === FooterButton.AddConsent
          ? this.apply(dialogConfig, this.formatRequestToCreateConsent(form.value))
          : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  public openModifyConsentDialog(consentLevel: TipProductConsentLevel, consents: TipProductConsent[]): Observable<any> {
    const form = new FormGroup({});

    const title =
      consentLevel === TipProductConsentLevel.Country
        ? 'TIP.productConsents.dialogs.modifyConsent.countryTitle'
        : 'TIP.productConsents.dialogs.modifyConsent.agentTitle';

    const dialogConfig = {
      data: {
        title,
        successMessage: 'TIP.productConsents.dialogs.modifyConsent.successMessage',
        footerButtonsType: FooterButton.Modify,
        hasCancelButton: true,
        consents,
        form,
        consentLevel
      }
    };

    return this.dialogService.open(TipProductModifyConsentDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn }) =>
        clickedBtn === FooterButton.Modify
          ? this.apply(dialogConfig, this.formatRequestToModifyConsent(consents, form.value))
          : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  public openAgentTerminateConsentDialog(consents: TipProductConsent[]): Observable<any> {
    const form = new FormGroup({});

    const dialogConfig = {
      data: {
        title: 'TIP.productConsents.dialogs.terminateConsent.title',
        successMessage: 'TIP.productConsents.dialogs.terminateConsent.successMessage',
        footerButtonsType: FooterButton.Terminate,
        hasCancelButton: true,
        consents,
        form,
        consentLevel: TipProductConsentLevel.Agent
      }
    };

    return this.dialogService.open(TipProductTerminateConsentDialogComponent, dialogConfig).pipe(
      switchMap(({ clickedBtn }) =>
        clickedBtn === FooterButton.Terminate
          ? this.apply(dialogConfig, this.formatRequestToModifyConsent(consents, form.value))
          : of(clickedBtn)
      ),
      finalize(() => this.dialogService.close()),
      first()
    );
  }

  private apply(dialogConfig: DialogConfig, dataService$: Observable<any>): Observable<any> {
    const setLoading = (loading: boolean) => dialogConfig.data.buttons.forEach(button => (button.isDisabled = loading));

    return defer(() => {
      setLoading(true);

      return dataService$.pipe(
        tap(() => this.notificationService.showSuccess(dialogConfig.data.successMessage)),
        mapTo(FooterButton.Apply),
        finalize(() => setLoading(false))
      );
    });
  }

  private formatRequestToModifyConsent(
    consents: TipProductConsent[],
    value: TipProductConsentPutView | TipProductConsentTerminateView
  ): Observable<any> {
    const payload: Array<TipProductConsentPutBE | TipProductConsentTerminateBE> = [];
    const generalValue = value as any;

    consents.forEach(({ id }) => {
      if (generalValue.effectiveTo) {
        generalValue.effectiveTo = toShortIsoDate(generalValue.effectiveTo);
      }
      payload.push({ ...generalValue, id });
    });

    return this.dataService.modifyConsent(payload);
  }

  private formatRequestToCreateConsent(value: TipProductConsentPostView): Observable<any> {
    const consents: Array<TipProductConsentPostBE> = [];
    const agentIds = value.agent.map(agent => agent.id);
    const productIds = value.product.map(product => product.id);

    agentIds.forEach(agentId =>
      productIds.forEach(productId =>
        consents.push({
          ...value,
          effectiveFrom: toShortIsoDate(value.effectiveFrom),
          agent: { id: agentId },
          product: { id: productId }
        })
      )
    );

    return this.dataService.createConsent(consents);
  }
}
