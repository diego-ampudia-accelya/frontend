import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { TipStatus } from '../models/tip-product.model';
import { TipProvider } from '../models/tip-provider.model';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class TipProviderService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/providers`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public getProviders(bsps?: BspDto[]): Observable<Array<TipProvider>> {
    const params = {
      status: TipStatus.Active,
      ...(bsps?.length && { bspId: bsps.map(({ id }) => '' + id).join() })
    };

    return this.http.get<Array<TipProvider>>(this.baseUrl, { params });
  }

  public getOne(id: number): Observable<TipProvider> {
    return this.http.get<TipProvider>(`${this.baseUrl}/${id}`);
  }

  public getProvidersDropdownOptions(bsps?: BspDto[]): Observable<DropdownOption<TipProvider>[]> {
    return this.getProviders(bsps).pipe(
      map(items =>
        items.map(item => ({
          value: item,
          label: `${item.bsp.isoCountryCode} - ${item.code} / ${item.name}`
        }))
      )
    );
  }
}
