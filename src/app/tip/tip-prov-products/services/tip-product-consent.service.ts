import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { TipProductConsentBEFilter, TipProductConsentFilter } from '../models/tip-product-consent-filter.model';
import {
  TipProductConsent,
  TipProductConsentPostBE,
  TipProductConsentPutBE,
  TipProductConsentTerminateBE
} from '../models/tip-product-consent.model';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DataQuery, Queryable } from '~app/shared/components/list-view';
import { downloadRequestOptions, extractId, formatDownloadResponse } from '~app/shared/helpers';
import { toShortIsoDate } from '~app/shared/helpers/datesHelper';
import { formatSortBy } from '~app/shared/helpers/sort-by-formatter';
import { DownloadFormat } from '~app/shared/models';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable()
export class TipProductConsentService implements Queryable<TipProductConsent> {
  private baseUrl = `${this.appConfiguration.baseApiPath}/tip-management/product-consents`;

  private userType: UserType;

  private sortMapper = [
    { from: 'product.provider.name', to: 'providerName' },
    { from: 'product.name', to: 'productName' },
    { from: 'costRecovery.type', to: 'costRecoveryType' },
    { from: 'costRecovery.amount', to: 'costRecoveryAmount' },
    { from: 'costRecovery.currency.code', to: 'costRecoveryCurrency' },
    { from: 'agent.iataCode', to: 'agentIataCode' },
    { from: 'airline.iataCode', to: 'airlineIataCode' },
    { from: 'airline.name', to: 'airlineName' }
  ];

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(getUserType), first()).subscribe(userType => (this.userType = userType));
  }

  public find(query: DataQuery<TipProductConsentFilter>): Observable<PagedData<TipProductConsent>> {
    const url = this.userType === UserType.AGENT ? `${this.baseUrl}-for-agents` : this.baseUrl;
    const requestQuery = this.formatQuery(query);

    return this.http.get<PagedData<TipProductConsent>>(url + requestQuery.getQueryString());
  }

  public getOne(id: number): Observable<TipProductConsent> {
    return this.http.get<TipProductConsent>(`${this.baseUrl}/${id}`);
  }

  public createConsent(consents: Array<TipProductConsentPostBE>): Observable<any> {
    return this.http.post<any>(this.baseUrl, consents);
  }

  public modifyConsent(consents: Array<TipProductConsentPutBE | TipProductConsentTerminateBE>): Observable<any> {
    return this.http.put<any>(this.baseUrl, consents);
  }

  public download(
    query: DataQuery<TipProductConsentFilter>,
    downloadFormat: DownloadFormat
  ): Observable<{ blob: Blob; fileName: string }> {
    const url = this.userType === UserType.AGENT ? `${this.baseUrl}-for-agents` : this.baseUrl;
    const requestQuery = this.formatQuery(query);

    requestQuery.filterBy = { ...requestQuery.filterBy, exportAs: downloadFormat.toUpperCase() };

    return this.http
      .get<HttpResponse<ArrayBuffer>>(url + requestQuery.getQueryString(), downloadRequestOptions)
      .pipe(map(formatDownloadResponse));
  }

  private formatQuery(query: DataQuery<TipProductConsentFilter>): RequestQuery<TipProductConsentBEFilter> {
    const { effectiveFrom, effectiveTo, agent, product, provider, airline, bsp, costRecoveryCurrency, ...filterBy } =
      query.filterBy;

    return RequestQuery.fromDataQuery<TipProductConsentBEFilter>({
      ...query,
      filterBy: {
        ...filterBy,
        bspId: extractId(bsp),
        productId: extractId(product),
        providerId: extractId(provider),
        agentId: extractId(agent),
        airlineIataCode: airline && airline.map(({ code }) => code),
        costRecoveryCurrency: costRecoveryCurrency && costRecoveryCurrency.map(currency => currency.code),
        fromEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[0]),
        toEffectiveFrom: effectiveFrom && toShortIsoDate(effectiveFrom[1] ? effectiveFrom[1] : effectiveFrom[0]),
        fromEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[0]),
        toEffectiveTo: effectiveTo && toShortIsoDate(effectiveTo[1] ? effectiveTo[1] : effectiveTo[0])
      },
      sortBy: formatSortBy(query.sortBy, this.sortMapper)
    });
  }
}
