import { createSpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { TipProductConsentFilter } from '../models/tip-product-consent-filter.model';
import { CountryLevelConsentType, TipProduct, TipStatus } from '../models/tip-product.model';

import { TipProductConsentFormatter } from './tip-product-consent-formatter.service';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';

describe('TipProductConsentFormatter', () => {
  let formatter: TipProductConsentFormatter;
  const translationServiceSpy = createSpyObject(L10nTranslationService);

  beforeEach(() => {
    translationServiceSpy.translate.and.callFake(identity);
    formatter = new TipProductConsentFormatter(translationServiceSpy);
  });

  it('should be created', () => {
    expect(formatter).toBeDefined();
  });

  it('should format `status` if it exists', () => {
    const filter: TipProductConsentFilter = {
      status: TipStatus.Active
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['status'],
        label: 'TIP.productConsents.filters.labels.status - Active'
      }
    ]);
  });

  it('should format `bsp` if it exists', () => {
    const filter: TipProductConsentFilter = {
      bsp: [
        { id: 1, isoCountryCode: 'ES', name: 'Spain' },
        { id: 2, isoCountryCode: 'MT', name: 'Malta' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['bsp'],
        label: 'TIP.productConsents.filters.labels.bsp - Spain (ES), Malta (MT)'
      }
    ]);
  });

  it('should format `provider` if it exists', () => {
    const filter: TipProductConsentFilter = {
      provider: [
        { id: 1, code: 'PROV1', name: 'provider1' },
        { id: 2, code: 'PROV2', name: 'provider2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['provider'],
        label: 'TIP.productConsents.filters.labels.provider - PROV1, PROV2'
      }
    ]);
  });

  it('should format `product` if it exists', () => {
    const filter: TipProductConsentFilter = {
      product: [
        { id: 1, code: 'PROD1' },
        { id: 2, code: 'PROD2' }
      ] as TipProduct[]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['product'],
        label: 'TIP.productConsents.filters.labels.product - PROD1, PROD2'
      }
    ]);
  });

  it('should format `agent` if it exists', () => {
    const filter: TipProductConsentFilter = {
      agent: [
        { id: '1', code: 'AGENT1', name: 'agent1' },
        { id: '2', code: 'AGENT2', name: 'agent2' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['agent'],
        label: 'TIP.productConsents.filters.labels.agent - AGENT1, AGENT2'
      }
    ]);
  });

  it('should format `airline` if it exists', () => {
    const filter: TipProductConsentFilter = {
      airline: [
        { id: 1, code: 'AIRL1', name: 'airline1', designator: 'L' },
        { id: 2, code: 'AIRL2', name: 'airline2', designator: 'W' }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['airline'],
        label: 'TIP.productConsents.filters.labels.airline - AIRL1, AIRL2'
      }
    ]);
  });

  it('should format `type` if it exists', () => {
    const filter: TipProductConsentFilter = {
      type: [CountryLevelConsentType.Yes, CountryLevelConsentType.CostRecovery]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['type'],
        label:
          'TIP.productConsents.filters.labels.type - TIP.products.levelConsentType.yes, TIP.products.levelConsentType.costRecovery'
      }
    ]);
  });

  it('should format `costRecoveryType` if it exists', () => {
    const filter: TipProductConsentFilter = {
      costRecoveryType: CostRecoveryType.Percentage
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['costRecoveryType'],
        label: 'TIP.productConsents.filters.labels.costRecoveryType - TIP.products.costRecoveryType.percentage'
      }
    ]);
  });

  it('should format `costRecoveryCurrency` if it exists', () => {
    const filter: TipProductConsentFilter = {
      costRecoveryCurrency: [
        { id: 1, code: 'EUR', decimals: 2, isDefault: true },
        { id: 2, code: 'ESP', decimals: 0, isDefault: false }
      ]
    };

    expect(formatter.format(filter)).toEqual([
      {
        keys: ['costRecoveryCurrency'],
        label: 'TIP.productConsents.filters.labels.costRecoveryCurrency - EUR, ESP'
      }
    ]);
  });

  it('should format `effectiveFrom` if it exists with or without range', () => {
    // Date range has been specified
    const filterWithRangeDate: TipProductConsentFilter = {
      effectiveFrom: [new Date('2020/07/14'), new Date('2020/07/21')]
    };
    // Unique date has been specified
    const filterWithUniqueDate: TipProductConsentFilter = {
      effectiveFrom: [new Date('2020/07/14')]
    };

    expect(formatter.format(filterWithRangeDate)).toEqual([
      {
        keys: ['effectiveFrom'],
        label: 'TIP.productConsents.filters.labels.effectiveFrom - 14/07/2020 - 21/07/2020'
      }
    ]);

    expect(formatter.format(filterWithUniqueDate)).toEqual([
      {
        keys: ['effectiveFrom'],
        label: 'TIP.productConsents.filters.labels.effectiveFrom - 14/07/2020'
      }
    ]);
  });

  it('should format `effectiveTo` if it exists with or without range', () => {
    // Date range has been specified
    const filterWithRangeDate: TipProductConsentFilter = {
      effectiveTo: [new Date('2020/07/21'), new Date('2020/07/28')]
    };
    // Unique date has been specified
    const filterWithUniqueDate: TipProductConsentFilter = {
      effectiveTo: [new Date('2020/07/21')]
    };

    expect(formatter.format(filterWithRangeDate)).toEqual([
      {
        keys: ['effectiveTo'],
        label: 'TIP.productConsents.filters.labels.effectiveTo - 21/07/2020 - 28/07/2020'
      }
    ]);

    expect(formatter.format(filterWithUniqueDate)).toEqual([
      {
        keys: ['effectiveTo'],
        label: 'TIP.productConsents.filters.labels.effectiveTo - 21/07/2020'
      }
    ]);
  });

  it('should ignore null or empty values', () => {
    const filter: TipProductConsentFilter = {
      status: null,
      costRecoveryCurrency: []
    };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should ignore unknown filter properties', () => {
    const filter: any = { unknown: 'unknown' };

    expect(formatter.format(filter)).toEqual([]);
  });

  it('should return empty array if specified filter is null', () => {
    expect(formatter.format(null)).toEqual([]);
  });
});
