export enum TipProductFormOfPayment {
  EP = 'EP',
  CA = 'CA',
  CC = 'CC'
}
