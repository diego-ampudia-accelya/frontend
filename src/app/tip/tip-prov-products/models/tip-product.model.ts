/* eslint-disable @typescript-eslint/naming-convention */
import { TipProvider } from './tip-provider.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';

export interface TipProduct {
  id: number;
  code: string;
  name: string;
  provider: TipProvider;
  bsp: BspDto;
  issuerName: string;
  payInModel: PayInModel;
  category: string;
  paymentNetwork: string;
  vanType: VanType;
  formOfPayment: string;
  settlementTerms: string;
  pciDssRequired: string;
  maximumChargebackTimeFrame: string;
  interchangeFee: string;
  acquiringNetworkSchemeFee: string;
  license: string;
  gdsIntegration: string;
  airlineUrl: string;
  agentUrl: string;
  effectiveFrom: Date;
  effectiveTo: Date;
  status: TipStatus;
  paymentGuarantees: PaymentGuarantee[];
  acceptedTransactions: AcceptedTransaction[];
  countryLevelConsent: CountryLevelConsent;
  agentLevelConsentsInfo: {
    no: number;
    yes: number;
    costRevovery: number;
  };
}

export interface CountryLevelConsent {
  id: number;
  type: CountryLevelConsentType;
  costRecovery?: CostRecovery;
  effectiveFrom: Date;
  effectiveTo: Date;
}

export interface CostRecovery {
  type: CostRecoveryType;
  amount: number;
  currency?: Currency;
}

export enum TipCardType {
  Consumer = 'Consumer',
  Corporate = 'Corporate'
}

export enum TipStatus {
  Active = 'Active',
  Inactive = 'Inactive'
}

export enum TipProductConsentLevel {
  Country = 'country',
  Agent = 'agent'
}

export enum CountryLevelConsentType {
  Yes = 'yes',
  No = 'no',
  CostRecovery = 'costRecovery'
}

export enum PaymentGuarantee {
  CommercialDispute = 'COMMERCIAL_DISPUTE',
  FraudDispute = 'FRAUD_DISPUTE'
}

export enum AcceptedTransaction {
  Payment = 'PAYMENT',
  Refund = 'REFUND'
}

export enum PayInModel {
  Prepaid = 'PREPAID',
  PayNow = 'PAY_NOW',
  PayLater = 'PAY_LATER'
}

export enum VanType {
  SingleUseVan = 'SINGLE_USE_VAN',
  MultiUseVan = 'MULTI_USE_VAN'
}
