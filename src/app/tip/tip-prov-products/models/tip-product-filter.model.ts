import { CountryLevelConsentType, TipProduct, TipStatus } from './tip-product.model';
import { TipProvider } from './tip-provider.model';
import { BspDto } from '~app/shared/models/bsp.model';
import { TipCurrency } from '~app/tip/shared/models/tip-currency.model';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';

export interface TipProductFilter {
  status: TipStatus;
  bsps: BspDto[];
  providers: TipProvider[];
  products: TipProduct[];
  paymentNetwork: string;
  category?: string;
  countryLevelConsentType?: CountryLevelConsentType[];
  countryLevelConsentCostRecoveryType?: CostRecoveryType;
  countryLevelConsentCostRecoveryCurrency?: TipCurrency[];
  effectiveFrom?: Date[];
  effectiveTo?: Date[];
  issuerName?: string;
  formOfPayment?: string;
}

export interface TipProductBEFilter {
  status: TipStatus;
  bspId: number[];
  providerId: number[];
  productId: number[];
  paymentNetwork: string;
  category: string;
  countryLevelConsentType: CountryLevelConsentType[];
  countryLevelConsentCostRecoveryType: CostRecoveryType;
  countryLevelConsentCostRecoveryCurrency: string[];
  fromEffectiveFrom: string;
  toEffectiveFrom: string;
  fromEffectiveTo: string;
  toEffectiveTo: string;
  issuerName: string;
  formOfPayment: string;
}
