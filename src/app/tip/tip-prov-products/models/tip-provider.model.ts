import { TipStatus } from './tip-product.model';
import { BspDto } from '~app/shared/models/bsp.model';

export interface TipProvider {
  id: number;
  code: string;
  name: string;
  bsp?: BspDto;
  location?: string;
  effectiveFrom?: Date;
  effectiveTo?: Date;
  status?: TipStatus;
  contacts?: Array<ProviderContact>;
}

export interface ProviderContact {
  primary: string;
  title: string;
  position: string;
  firstName: string;
  lastName: string;
  address: string;
  city: string;
  state: string;
  postalCode: string;
  country: string;
  email: string;
  phone: string;
}
