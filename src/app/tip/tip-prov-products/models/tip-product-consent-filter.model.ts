import { CountryLevelConsentType, TipProduct, TipProductConsentLevel, TipStatus } from './tip-product.model';
import { TipProvider } from './tip-provider.model';
import { AgentSummary, AirlineSummary } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { TipCurrency } from '~app/tip/shared/models/tip-currency.model';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';

export interface TipProductConsentFilter {
  status?: TipStatus;
  bsp?: BspDto[];
  level?: TipProductConsentLevel;
  provider?: TipProvider[];
  product?: TipProduct[];
  agent?: AgentSummary[];
  airline?: AirlineSummary[];
  type?: CountryLevelConsentType[];
  costRecoveryType?: CostRecoveryType;
  costRecoveryCurrency?: TipCurrency[];
  effectiveFrom?: Date[];
  effectiveTo?: Date[];
}

export interface TipProductConsentBEFilter {
  status: string;
  bspId: number[];
  level: string;
  providerId: number[];
  productId: number[];
  agentId: string[];
  airlineIataCode: string[];
  type: string[];
  costRecoveryType: string;
  costRecoveryCurrency: string[];
  fromEffectiveFrom: string;
  toEffectiveFrom: string;
  fromEffectiveTo: string;
  toEffectiveTo: string;
}
