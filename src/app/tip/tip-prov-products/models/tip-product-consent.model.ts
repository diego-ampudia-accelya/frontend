import { CostRecovery, CountryLevelConsentType, TipProductConsentLevel, TipStatus } from './tip-product.model';
import { TipProvider } from './tip-provider.model';
import { BspDto } from '~app/shared/models/bsp.model';

export interface TipProductConsent {
  id: number;
  agent: AgentDto;
  airline: AirlineDto;
  bsp: BspDto;
  costRecovery: CostRecovery;
  effectiveFrom: Date;
  effectiveTo: Date;
  level: TipProductConsentLevel;
  product: TipProductDto;
  status: TipStatus;
  type: CountryLevelConsentType;
}

export interface TipProductConsentPostView {
  agent: [{ id: number }];
  bsp: { id: number };
  product: [{ id: number }];
  type: CountryLevelConsentType;
  costRecovery: CostRecovery;
  effectiveFrom: Date;
}

export interface TipProductConsentPostBE {
  agent: { id: number };
  bsp: { id: number };
  product: { id: number };
  type: CountryLevelConsentType;
  costRecovery: CostRecovery;
  effectiveFrom: string;
}

export type TipProductConsentPutView = TipProductConsentPutBE;

export interface TipProductConsentPutBE {
  id: number;
  type: CountryLevelConsentType;
  costRecovery: CostRecovery;
}
export interface TipProductConsentTerminateView {
  id: number;
  effectiveTo: Date;
}
export interface TipProductConsentTerminateBE {
  id: number;
  effectiveTo: string;
}

interface TipProductDto {
  id: number;
  code: string;
  name: string;
  provider: TipProvider;
}

interface AgentDto {
  id: number;
  iataCode: string;
  name: string;
}

interface AirlineDto {
  id: number;
  iataCode: string;
}
