import { TipProductConsentLevel } from '../models/tip-product.model';

export const permissions = {
  // READ
  readTipProducts: 'rTipPrd',
  readActiveTipProductCountryConsents: 'rTipPrdCnsCoAc',
  readInactiveTipProductCountryConsents: 'rTipPrdCnsCoIn',
  readActiveTipProductAgentConsents: 'rTipPrdCnsAgAc',
  readInactiveTipProductAgentConsents: 'rTipPrdCnsAgIn',
  readMulticountryBsps: 'rBsps',

  // UPDATE
  updateTipProductCountryConsent: 'uTipPrdCnsCo',
  createTipProductAgentConsent: 'cTipPrdCnsAg',
  updateTipProductAgentConsent: 'uTipPrdCnsAg'
};

export function getReadTipProductListPermissions(): Array<string> {
  return [permissions.readTipProducts];
}

export function getReadTipProductConsentPermissions(): Array<string> {
  return [...getReadTipProductCountryConsentPermissions(), ...getReadTipProductAgentConsentPermissions()];
}

export function getReadTipProductCountryConsentPermissions(): Array<string> {
  return [permissions.readActiveTipProductCountryConsents, permissions.readInactiveTipProductCountryConsents];
}
export function getReadTipProductAgentConsentPermissions(): Array<string> {
  return [permissions.readActiveTipProductAgentConsents, permissions.readInactiveTipProductAgentConsents];
}

export function getReadTipProductPermissions(): Array<string> {
  return [...getReadTipProductListPermissions(), ...getReadTipProductConsentPermissions()];
}

export function getUpdateTipProductConsentPermission(consentLevel: TipProductConsentLevel): Array<string> {
  return consentLevel === TipProductConsentLevel.Country
    ? [permissions.updateTipProductCountryConsent]
    : [permissions.updateTipProductAgentConsent];
}
