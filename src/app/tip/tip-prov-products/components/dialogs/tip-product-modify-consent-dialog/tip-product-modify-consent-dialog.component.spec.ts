import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TipProductModifyConsentDialogComponent } from './tip-product-modify-consent-dialog.component';

// TODO Tests in separate taks
xdescribe('TipProductModifyConsentDialogComponent', () => {
  let component: TipProductModifyConsentDialogComponent;
  let fixture: ComponentFixture<TipProductModifyConsentDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TipProductModifyConsentDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipProductModifyConsentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
