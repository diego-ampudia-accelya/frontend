import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { uniqBy } from 'lodash';
import { combineLatest, merge, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { DialogConfig, FooterButton } from '~app/shared/components';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { RadioButton } from '~app/shared/models/radio-button.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';
import { costRecoveryAmountValidator } from '~app/tip/shared/validators/cost-recovery-amount.validator';
import {
  TipProductConsent,
  TipProductConsentPutBE,
  TipProductConsentPutView
} from '~app/tip/tip-prov-products/models/tip-product-consent.model';
import {
  CostRecovery,
  CountryLevelConsentType,
  TipProductConsentLevel
} from '~app/tip/tip-prov-products/models/tip-product.model';

@Component({
  selector: 'bspl-tip-product-modify-consent-dialog',
  templateUrl: './tip-product-modify-consent-dialog.component.html',
  styleUrls: ['./tip-product-modify-consent-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipProductModifyConsentDialogComponent implements OnInit, OnDestroy {
  public consentTypes: DropdownOption<CountryLevelConsentType>[];
  public costRecoveryTypes: RadioButton[];
  public currencyList$: Observable<DropdownOption<Currency>[]>;

  public form: FormGroup;
  public costRecoveryGroup: FormGroup;

  public pillsInformation: Array<{ value: string; tooltip: string }>;

  public amountLabel: string;
  public amountPlaceholder: string;

  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    private translationService: L10nTranslationService,
    private currencyDictionary: CurrencyDictionaryService,
    private fb: FormBuilder
  ) {}

  public ngOnInit() {
    this.buildForm();
    this.initializeDropdownOptions();
    this.initializeControlListeners();
    this.populateForm();
    this.initializePillsInformation();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private buildForm() {
    this.form = this.config.data.form;
    this.costRecoveryGroup = this.fb.group({
      type: [null, Validators.required],
      amount: null,
      currency: [null, Validators.required]
    });

    this.addControl('costRecovery', this.costRecoveryGroup);
    this.addControl('type', this.fb.control(null, Validators.required));

    const amountControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'amount');
    const typeControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'type');
    const currencyControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'currency');
    amountControl.setValidators([Validators.required, costRecoveryAmountValidator(typeControl, currencyControl)]);
  }

  private populateForm() {
    const consents: TipProductConsent[] = this.config.data.consents;

    if (consents.length === 1) {
      this.form.patchValue({ ...consents[0], costRecovery: consents[0].costRecovery || {} });
    }
  }

  private initializeDropdownOptions() {
    // TODO Clarify with Pablo if it is possible to have different bsp's selected (if not, code below can be simplified)
    //* List of consents with unique bsp to extract later
    const consents = uniqBy(this.config.data.consents as TipProductConsent[], 'bsp.id');

    //* Retrieve currencies from consents selected
    const lostCurrencies: DropdownOption<Currency>[] = consents
      .filter(consent => consent.costRecovery && consent.costRecovery.currency)
      .map(item => ({ label: item.costRecovery.currency.code, value: item.costRecovery.currency }));

    //* Currency requests for every unique bsp
    const currencyRequests$ = consents.map(consent =>
      this.currencyDictionary.getTipDropdownOptions(consent.bsp.id, true)
    );
    //* List of unique currencies for all different bsps selected to be modified
    this.currencyList$ = combineLatest(currencyRequests$).pipe(
      map(([responses]) => uniqBy([...responses, ...lostCurrencies], 'value.id'))
    );

    this.consentTypes = Object.values(CountryLevelConsentType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.levelConsentType.${value}`)
    }));
    this.costRecoveryTypes = Object.values(CostRecoveryType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.costRecoveryType.${value}`)
    }));
  }

  private initializeControlListeners() {
    //* Logic to enable/disable dialog buttons depending on form validity
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.config.data.buttons.find(({ type }) => type === FooterButton.Modify).isDisabled = this.form.invalid;
    });

    //* Logic to enable/disable cost recovery depending whether ConsentType CostRecovery is selected or not
    const consentTypeControl = FormUtil.get<TipProductConsentPutView>(this.form, 'type');
    consentTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(consentType => {
      if (consentType === CountryLevelConsentType.CostRecovery) {
        this.enableCostRecoveryGroup();
      } else {
        this.disableCostRecoveryGroup();
      }
    });
    consentTypeControl.updateValueAndValidity({ onlySelf: true });

    //* Logic to enable/disable currency and update amount label/placeholder depending whether Percentage or FixedAmount is selected or not
    const costRecoveryTypeControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'type');
    const currencyControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'currency');
    const amountControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'amount');
    costRecoveryTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(costRecoveryType => {
      if (!costRecoveryType || costRecoveryType === CostRecoveryType.Percentage) {
        currencyControl.reset(null, { emitEvent: false });
        currencyControl.disable({ emitEvent: false });
      } else {
        currencyControl.enable({ emitEvent: false });
      }

      this.updateAmountLabels(costRecoveryType);
    });
    costRecoveryTypeControl.updateValueAndValidity({ onlySelf: true });

    //* Logic to enable/disable amount when currency is selected or percentage
    merge(costRecoveryTypeControl.valueChanges, currencyControl.valueChanges)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.checkAmountAvailability(amountControl, costRecoveryTypeControl, currencyControl);
      });

    //* Logic to format amount to required decimals
    amountControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.formatAmount(amountControl, currencyControl);
    });
  }

  private checkAmountAvailability(
    amountControl: AbstractControl,
    costRecoveryTypeControl: AbstractControl,
    currencyControl: AbstractControl
  ) {
    const currencySelected = currencyControl.value;
    const percentageSelected = costRecoveryTypeControl.value === CostRecoveryType.Percentage;

    if (currencySelected || percentageSelected) {
      amountControl.enable({ onlySelf: true });
    } else {
      amountControl.reset(null, { emitEvent: false });
      amountControl.disable({ emitEvent: false });
    }
  }

  private formatAmount(amountControl: AbstractControl, currencyControl: AbstractControl) {
    const defaultDecimals = 2;
    const requiredDecimals = currencyControl.value ? currencyControl.value.decimals : defaultDecimals;
    amountControl.setValue(NumberHelper.formatNumber(amountControl.value, requiredDecimals), { emitEvent: false });
    amountControl.updateValueAndValidity({ emitEvent: false });
  }

  private updateAmountLabels(costRecoveryType: CostRecoveryType) {
    if (!costRecoveryType || costRecoveryType === CostRecoveryType.Percentage) {
      this.amountLabel = 'TIP.productConsents.dialogs.modifyConsent.labels.costRecoveryPercentage';
      this.amountPlaceholder = 'TIP.productConsents.dialogs.modifyConsent.placeholders.percentage';
    } else {
      this.amountLabel = 'TIP.productConsents.dialogs.modifyConsent.labels.costRecoveryAmount';
      this.amountPlaceholder = 'TIP.productConsents.dialogs.modifyConsent.placeholders.amount';
    }
  }

  private enableCostRecoveryGroup() {
    this.costRecoveryGroup.enable();
    this.updateCostRecoveryTypeControl();
  }

  private disableCostRecoveryGroup() {
    this.costRecoveryGroup.reset({ emitEvent: false });
    this.costRecoveryGroup.disable();
  }

  private updateCostRecoveryTypeControl() {
    FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'type').updateValueAndValidity({ onlySelf: true });
  }

  private initializePillsInformation() {
    const consents: TipProductConsent[] = this.config.data.consents;
    const consentLevel: TipProductConsentLevel = this.config.data.consentLevel;
    const translationKey = `TIP.productConsents.dialogs.pillInformation.${consentLevel}`;
    this.pillsInformation = consents.map(consent => ({
      value: this.translationService.translate(`${translationKey}.value`, {
        provCode: consent.product.provider.code,
        prodCode: consent.product.code,
        agentCode: consent.agent?.iataCode
      }),
      tooltip: this.translationService.translate(`${translationKey}.tooltip`, {
        provName: consent.product.provider.name,
        prodName: consent.product.name,
        agentName: consent.agent?.name
      })
    }));
  }

  private addControl(controlName: keyof TipProductConsentPutBE, control?: AbstractControl): void {
    this.form.addControl(controlName, control || this.fb.control(null));
  }
}
