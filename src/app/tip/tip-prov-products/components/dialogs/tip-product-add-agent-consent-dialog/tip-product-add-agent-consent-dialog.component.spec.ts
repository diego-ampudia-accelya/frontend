import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TipProductAddAgentConsentDialogComponent } from './tip-product-add-agent-consent-dialog.component';

// TODO Tests in separate taks
xdescribe('TipProductAddAgentConsentDialogComponent', () => {
  let component: TipProductAddAgentConsentDialogComponent;
  let fixture: ComponentFixture<TipProductAddAgentConsentDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TipProductAddAgentConsentDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipProductAddAgentConsentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
