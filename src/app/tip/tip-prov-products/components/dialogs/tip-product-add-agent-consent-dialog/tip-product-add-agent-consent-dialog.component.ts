import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, merge, Observable, Subject } from 'rxjs';
import { first, map, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';

import { getUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { AppState } from '~app/reducers';
import { DialogConfig, FooterButton } from '~app/shared/components';
import { Permissions } from '~app/shared/constants';
import { FormUtil, NumberHelper } from '~app/shared/helpers';
import { AgentSummary, DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { RadioButton } from '~app/shared/models/radio-button.model';
import { User, UserType } from '~app/shared/models/user.model';
import { AgentDictionaryService, CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';
import { costRecoveryAmountValidator } from '~app/tip/shared/validators/cost-recovery-amount.validator';
import {
  TipProductConsentPostBE,
  TipProductConsentPostView
} from '~app/tip/tip-prov-products/models/tip-product-consent.model';
import { CostRecovery, CountryLevelConsentType, TipProduct } from '~app/tip/tip-prov-products/models/tip-product.model';
import { TipProvider } from '~app/tip/tip-prov-products/models/tip-provider.model';
import { TipProductService } from '~app/tip/tip-prov-products/services/tip-product.service';
import { TipProviderService } from '~app/tip/tip-prov-products/services/tip-provider.service';

@Component({
  selector: 'bspl-tip-product-add-agent-consent-dialog',
  templateUrl: './tip-product-add-agent-consent-dialog.component.html',
  styleUrls: ['./tip-product-add-agent-consent-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipProductAddAgentConsentDialogComponent implements OnInit, OnDestroy {
  public bspList: DropdownOption<BspDto>[];
  public airlineIsMulticountry: boolean;
  public agentList$: Observable<DropdownOption<AgentSummary>[]>;
  public providerList$: Observable<DropdownOption<TipProvider>[]>;
  public productList$: Observable<DropdownOption<TipProduct>[]>;
  public currencyList$: Observable<DropdownOption<Currency>[]>;
  public consentTypes: DropdownOption<CountryLevelConsentType>[];
  public costRecoveryTypes: RadioButton[];

  public form: FormGroup;
  public costRecoveryGroup: FormGroup;
  public providerControl: FormControl;

  public amountLabel: string;
  public amountPlaceholder: string;

  private loggedUser: User;
  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private get defaultBsp(): BspDto {
    return !this.airlineIsMulticountry ? this.loggedUser.bsps[0] : null;
  }

  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    private translationService: L10nTranslationService,
    private tipProviderService: TipProviderService,
    private tipProductService: TipProductService,
    private bspsDictionaryService: BspsDictionaryService,
    private agentDictionaryService: AgentDictionaryService,
    private currencyDictionary: CurrencyDictionaryService,
    private permissionsService: PermissionsService,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {}

  public ngOnInit() {
    this.initializeLoggedUser();
    this.buildForm();
    this.initializeDropdownOptions();
    this.initializeProductFiltering();
    this.initializeControlListeners();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private buildForm() {
    this.form = this.config.data.form;
    this.providerControl = this.fb.control(null);
    this.costRecoveryGroup = this.fb.group({
      type: [null, Validators.required],
      amount: null, // Validators are set at the end of method
      currency: [null, Validators.required]
    });

    this.addControl('bsp', this.fb.control(this.defaultBsp, this.airlineIsMulticountry ? Validators.required : null));
    this.addControl('agent', this.fb.control(null, Validators.required));
    this.addControl('product', this.fb.control(null, Validators.required));
    this.addControl('type', this.fb.control(null, Validators.required));
    this.addControl('costRecovery', this.costRecoveryGroup);
    this.addControl('effectiveFrom', this.fb.control(null, Validators.required));

    const amountControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'amount');
    const typeControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'type');
    const currencyControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'currency');
    amountControl.setValidators([Validators.required, costRecoveryAmountValidator(typeControl, currencyControl)]);
  }

  private initializeDropdownOptions() {
    const agentIdControl = FormUtil.get<TipProductConsentPostBE>(this.form, 'agent');
    const currencyControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'currency');

    this.bspsDictionaryService.getDropdownOptions(this.loggedUser.id).subscribe(bsps => {
      this.bspList = bsps;
    });

    this.agentList$ = this.getCurrentBsp$().pipe(
      switchMap(bspSelected =>
        this.agentDictionaryService.getDropdownOptions({
          active: true,
          ...(bspSelected && { bspId: bspSelected.id })
        })
      ),
      tap(options => this.updateOptionsControl(agentIdControl, options))
    );

    this.initializeProductList();

    this.providerList$ = this.getCurrentBsp$().pipe(
      switchMap(bsp => this.tipProviderService.getProvidersDropdownOptions(bsp ? [bsp] : null)),
      tap(options => this.updateOptionsControl(this.providerControl, options))
    );

    this.currencyList$ = this.getCurrentBsp$().pipe(
      switchMap(bsp =>
        this.currencyDictionary.getFilteredDropdownOptions({
          ...(bsp && { bspId: bsp.id })
        })
      ),
      tap(() => currencyControl.reset())
    );

    this.consentTypes = Object.values(CountryLevelConsentType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.levelConsentType.${value}`)
    }));
    this.costRecoveryTypes = Object.values(CostRecoveryType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.costRecoveryType.${value}`)
    }));
  }

  private updateOptionsControl(control: AbstractControl, options: DropdownOption<{ id: number | string }>[]) {
    if (control.value?.length) {
      const optionIds = options.map(option => option.value.id);

      control.setValue(control.value.filter(selectedOption => optionIds.includes(selectedOption.id)));
    }
  }

  private getCurrentBsp$(): Observable<BspDto> {
    const bspControl = FormUtil.get<TipProductConsentPostBE>(this.form, 'bsp');

    return bspControl.valueChanges.pipe(startWith(this.defaultBsp));
  }

  private initializeProductList() {
    const productControl = FormUtil.get<TipProductConsentPostBE>(this.form, 'product');

    this.productList$ = this.getCurrentBsp$().pipe(
      switchMap(bsp => this.tipProductService.getProductsDropdownOptions(bsp ? [bsp] : null)),
      tap(options => this.updateOptionsControl(productControl, options))
    );
  }

  private initializeProductFiltering(): void {
    combineLatest([this.providerControl.valueChanges, this.getCurrentBsp$()])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([providers, currentBsp]) => {
        const providerCodes = providers.map(provider => provider.code);

        if (providerCodes?.length) {
          this.productList$ = this.tipProductService
            .getProductsDropdownOptions(currentBsp ? [currentBsp] : null)
            .pipe(map(options => options.filter(option => providerCodes.includes(option.value.provider.code))));
        } else {
          this.initializeProductList();
        }
      });
  }

  private initializeControlListeners() {
    //* Logic to enable/disable dialog buttons depending on form validity
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.config.data.buttons.find(({ type }) => type === FooterButton.AddConsent).isDisabled = this.form.invalid;
    });

    //* Logic to enable/disable cost recovery depending whether ConsentType CostRecovery is selected or not
    const consentTypeControl = FormUtil.get<TipProductConsentPostView>(this.form, 'type');
    consentTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(consentType => {
      if (consentType === CountryLevelConsentType.CostRecovery) {
        this.enableCostRecoveryGroup();
      } else {
        this.disableCostRecoveryGroup();
      }
    });
    consentTypeControl.updateValueAndValidity({ onlySelf: true });

    //* Logic to enable/disable currency and update amount label/placeholder depending whether Percentage or FixedAmount is selected or not
    const costRecoveryTypeControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'type');
    const currencyControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'currency');
    const amountControl = FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'amount');
    costRecoveryTypeControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(costRecoveryType => {
      if (!costRecoveryType || costRecoveryType === CostRecoveryType.Percentage) {
        currencyControl.reset(null, { emitEvent: false });
        currencyControl.disable({ emitEvent: false });
      } else {
        currencyControl.enable({ emitEvent: false });
      }

      this.updateAmountLabels(costRecoveryType);
    });
    costRecoveryTypeControl.updateValueAndValidity({ onlySelf: true });

    //* Logic to enable/disable amount when currency is selected or percentage
    merge(costRecoveryTypeControl.valueChanges, currencyControl.valueChanges)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.checkAmountAvailability(amountControl, costRecoveryTypeControl, currencyControl);
      });

    //* Logic to format amount to required decimals
    amountControl.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.formatAmount(amountControl, currencyControl);
    });
  }

  private checkAmountAvailability(
    amountControl: AbstractControl,
    costRecoveryTypeControl: AbstractControl,
    currencyControl: AbstractControl
  ) {
    const currencySelected = currencyControl.value;
    const percentageSelected = costRecoveryTypeControl.value === CostRecoveryType.Percentage;

    if (currencySelected || percentageSelected) {
      amountControl.enable({ onlySelf: true });
    } else {
      amountControl.reset(null, { emitEvent: false });
      amountControl.disable({ emitEvent: false });
    }
  }

  private formatAmount(amountControl: AbstractControl, currencyControl: AbstractControl) {
    const defaultDecimals = 2;
    const currencyDecimals = currencyControl.value?.decimals;
    const requiredDecimals = currencyDecimals || defaultDecimals;
    amountControl.setValue(NumberHelper.formatNumber(amountControl.value, requiredDecimals), { emitEvent: false });
    amountControl.updateValueAndValidity({ emitEvent: false });
  }

  private updateAmountLabels(costRecoveryType: CostRecoveryType) {
    if (!costRecoveryType || costRecoveryType === CostRecoveryType.Percentage) {
      this.amountLabel = 'TIP.productConsents.dialogs.createAgentConsent.labels.costRecoveryPercentage';
      this.amountPlaceholder = 'TIP.productConsents.dialogs.createAgentConsent.placeholders.percentage';
    } else {
      this.amountLabel = 'TIP.productConsents.dialogs.createAgentConsent.labels.costRecoveryAmount';
      this.amountPlaceholder = 'TIP.productConsents.dialogs.createAgentConsent.placeholders.amount';
    }
  }

  private enableCostRecoveryGroup() {
    this.costRecoveryGroup.enable();
    this.updateCostRecoveryTypeControl();
  }

  private disableCostRecoveryGroup() {
    this.costRecoveryGroup.reset({ emitEvent: false });
    this.costRecoveryGroup.disable();
  }

  private updateCostRecoveryTypeControl() {
    FormUtil.get<CostRecovery>(this.costRecoveryGroup, 'type').updateValueAndValidity({ onlySelf: true });
  }

  private addControl(controlName: keyof TipProductConsentPostBE, control?: AbstractControl): void {
    this.form.addControl(controlName, control || this.fb.control(null));
  }

  private initializeLoggedUser(): void {
    this.loggedUser$.subscribe(user => {
      this.loggedUser = user;
      this.airlineIsMulticountry =
        user.userType === UserType.AIRLINE && this.permissionsService.hasPermission(Permissions.readBsps);
    });
  }
}
