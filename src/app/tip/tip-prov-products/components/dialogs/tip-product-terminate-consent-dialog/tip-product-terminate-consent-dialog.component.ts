import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DialogConfig, FooterButton } from '~app/shared/components';
import {
  TipProductConsent,
  TipProductConsentTerminateBE
} from '~app/tip/tip-prov-products/models/tip-product-consent.model';
import { TipProductConsentLevel } from '~app/tip/tip-prov-products/models/tip-product.model';

@Component({
  selector: 'bspl-tip-product-terminate-consent-dialog',
  templateUrl: './tip-product-terminate-consent-dialog.component.html',
  styleUrls: ['./tip-product-terminate-consent-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipProductTerminateConsentDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public pillsInformation: Array<{ value: string; tooltip: string }>;

  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    private translationService: L10nTranslationService,
    private fb: FormBuilder
  ) {}

  public ngOnInit() {
    this.buildForm();
    this.initializeControlListeners();
    this.initializePillsInformation();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private buildForm() {
    this.form = this.config.data.form;

    this.addControl('effectiveTo', this.fb.control(null, Validators.required));
  }

  private initializeControlListeners() {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.config.data.buttons.find(({ type }) => type === FooterButton.Terminate).isDisabled = this.form.invalid;
    });
  }

  private initializePillsInformation() {
    const consents: TipProductConsent[] = this.config.data.consents;
    const consentLevel: TipProductConsentLevel = this.config.data.consentLevel;
    const translationKey = `TIP.productConsents.dialogs.pillInformation.${consentLevel}`;
    this.pillsInformation = consents.map(consent => ({
      value: this.translationService.translate(`${translationKey}.value`, {
        provCode: consent.product.provider.code,
        prodCode: consent.product.code,
        agentCode: consent.agent?.iataCode
      }),
      tooltip: this.translationService.translate(`${translationKey}.tooltip`, {
        provName: consent.product.provider.name,
        prodName: consent.product.name,
        agentName: consent.agent?.name
      })
    }));
  }

  private addControl(controlName: keyof TipProductConsentTerminateBE, control?: AbstractControl): void {
    this.form.addControl(controlName, control || this.fb.control(null));
  }
}
