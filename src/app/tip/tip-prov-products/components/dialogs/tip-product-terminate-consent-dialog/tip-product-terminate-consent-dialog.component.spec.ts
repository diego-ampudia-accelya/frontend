import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TipProductTerminateConsentDialogComponent } from './tip-product-terminate-consent-dialog.component';

// TODO Tests in separate taks
xdescribe('TipProductTerminateConsentDialogComponent', () => {
  let component: TipProductTerminateConsentDialogComponent;
  let fixture: ComponentFixture<TipProductTerminateConsentDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TipProductTerminateConsentDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipProductTerminateConsentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
