import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TipProduct } from '../../models/tip-product.model';

@Component({
  selector: 'bspl-tip-product-detail',
  templateUrl: './tip-product-detail.component.html',
  styleUrls: ['./tip-product-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipProductDetailComponent implements OnInit {
  public product: TipProduct;

  constructor(private activatedRoute: ActivatedRoute) {}

  public ngOnInit(): void {
    const data = this.activatedRoute.snapshot.data;

    this.product = data.item;
  }
}
