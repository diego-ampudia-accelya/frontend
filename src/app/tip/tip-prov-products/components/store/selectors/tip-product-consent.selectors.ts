import { createSelector } from '@ngrx/store';

import { MasterDataType } from '~app/master-shared/models/master.model';
import { consentSelectors } from '~app/tip/reducers';

export const createAgentConsentButtonSelector = createSelector(
  consentSelectors[MasterDataType.TipProductConsent],
  state => state.isCreateAgentConsentButtonVisible
);
