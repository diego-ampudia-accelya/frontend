import { createReducer, on } from '@ngrx/store';

import { changeAgentConsentCreateButtonVisibilityAction } from './actions/tip-product-consent.actions';
import { initialState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';

export const reducer = createReducer(
  initialState,
  on(changeAgentConsentCreateButtonVisibilityAction, (state, { isCreateAgentConsentButtonVisible }) => ({
    ...state,
    isCreateAgentConsentButtonVisible
  }))
);
