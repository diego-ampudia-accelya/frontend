import { createAction, props } from '@ngrx/store';

export const changeAgentConsentCreateButtonVisibilityAction = createAction(
  `List Products Create Button Visibility Change`,
  props<{ isCreateAgentConsentButtonVisible: boolean }>()
);

export const clickAgentConsentCreateButtonAction = createAction(`List Products Create Button Clicked`);
