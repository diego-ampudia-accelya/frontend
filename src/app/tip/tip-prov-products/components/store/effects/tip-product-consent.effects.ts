import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, tap } from 'rxjs/operators';

import { clickAgentConsentCreateButtonAction } from '../actions/tip-product-consent.actions';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabsActions } from '~app/shared/base/list-subtabs/actions';
import { FooterButton } from '~app/shared/components';
import { consentKeys } from '~app/tip/reducers';
import { TipProductDialogService } from '~app/tip/tip-prov-products/services/tip-product-dialog.service';

@Injectable()
export class TipProductConsentEffects {
  @Effect({ dispatch: false })
  clickAgentConsentCreateButtonAction$ = this.actions$.pipe(
    ofType(clickAgentConsentCreateButtonAction),
    switchMap(() => this.tipProductDialogService.openCreateAgentConsentDialog()),
    tap(clickedBtn => {
      if (clickedBtn !== FooterButton.Cancel) {
        this.refreshQuery();
      }
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private tipProductDialogService: TipProductDialogService
  ) {}

  private refreshQuery(): void {
    this.store.dispatch(ListSubtabsActions.search(consentKeys[MasterDataType.TipProductConsent])({}));
  }
}
