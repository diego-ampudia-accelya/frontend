import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';

import { clickAgentConsentCreateButtonAction } from '../actions/tip-product-consent.actions';
import { TipProductConsentEffects } from './tip-product-consent.effects';
import { FooterButton } from '~app/shared/components';
import { TipProductDialogService } from '~app/tip/tip-prov-products/services/tip-product-dialog.service';

describe('TipProductConsentEffects', () => {
  let effects: TipProductConsentEffects;
  let actions$: Observable<Action>;

  const tipProductDialogServiceSpy = createSpyObject(TipProductDialogService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TipProductConsentEffects,
        provideMockStore(),
        provideMockActions(() => actions$),
        { provide: TipProductDialogService, useValue: tipProductDialogServiceSpy }
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.inject(TipProductConsentEffects);
    actions$ = of(clickAgentConsentCreateButtonAction());
  });

  it('should be created', () => {
    expect(effects).toBeDefined();
  });

  it('should open create agent consent dialog and refresh query on successful action', fakeAsync(() => {
    tipProductDialogServiceSpy.openCreateAgentConsentDialog.and.returnValue(of(FooterButton.AddConsent));
    spyOn<any>(effects, 'refreshQuery').and.callThrough();

    effects.clickAgentConsentCreateButtonAction$.subscribe();

    expect(tipProductDialogServiceSpy.openCreateAgentConsentDialog).toHaveBeenCalled();
    expect(effects['refreshQuery']).toHaveBeenCalled();
  }));

  it('should NOT refresh query on cancel dialog action', fakeAsync(() => {
    tipProductDialogServiceSpy.openCreateAgentConsentDialog.and.returnValue(of(FooterButton.Cancel));
    spyOn<any>(effects, 'refreshQuery').and.callThrough();

    effects.clickAgentConsentCreateButtonAction$.subscribe();

    expect(tipProductDialogServiceSpy.openCreateAgentConsentDialog).toHaveBeenCalled();
    expect(effects['refreshQuery']).not.toHaveBeenCalled();
  }));
});
