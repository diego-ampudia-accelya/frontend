import { GridColumn } from '~app/shared/models';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';

export const COUNTRY_LEVEL_COLUMNS: Array<GridColumn> = [
  {
    name: '',
    prop: 'isRowSelected',
    maxWidth: 40,
    flexGrow: 1,
    sortable: false,
    headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
    cellTemplate: 'defaultLabeledCheckboxCellTmpl'
  },
  {
    name: 'TIP.productConsents.columns.bsp',
    prop: 'bsp.isoCountryCode',
    flexGrow: 1
  },
  {
    name: 'TIP.productConsents.columns.providerName',
    prop: 'product.provider.name',
    flexGrow: 3,
    cellTemplate: 'multipleLinkCellTmpl'
  },
  {
    name: 'TIP.productConsents.columns.productName',
    prop: 'product.name',
    flexGrow: 3,
    cellTemplate: 'multipleLinkCellTmpl'
  },
  {
    name: 'TIP.productConsents.columns.bspConsent',
    prop: 'type',
    flexGrow: 1.5
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryType',
    prop: 'costRecovery.type',
    flexGrow: 1.5
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryAmount',
    prop: 'costRecovery.amount',
    flexGrow: 1.5,
    cellClass: 'text-right'
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryCurrency',
    prop: 'costRecovery.currency.code',
    flexGrow: 1.5
  },
  {
    name: 'TIP.productConsents.columns.status',
    prop: 'status',
    flexGrow: 1.5
  }
];

export const AGENT_LEVEL_COLUMNS: Array<GridColumn> = [
  {
    name: '',
    prop: 'isRowSelected',
    maxWidth: 40,
    flexGrow: 1,
    sortable: false,
    headerTemplate: 'checkboxHeaderWithoutLabelTmpl',
    cellTemplate: 'defaultLabeledCheckboxCellTmpl'
  },
  {
    name: 'TIP.productConsents.columns.bsp',
    prop: 'bsp.isoCountryCode',
    flexGrow: 1
  },
  {
    name: 'TIP.productConsents.columns.agentCode',
    prop: 'agent.iataCode',
    flexGrow: 1.5
  },
  {
    name: 'TIP.productConsents.columns.providerName',
    prop: 'product.provider.name',
    flexGrow: 3,
    cellTemplate: 'multipleLinkCellTmpl'
  },
  {
    name: 'TIP.productConsents.columns.productName',
    prop: 'product.name',
    flexGrow: 3,
    cellTemplate: 'multipleLinkCellTmpl'
  },
  {
    name: 'TIP.productConsents.columns.agentConsent',
    prop: 'type',
    flexGrow: 1.5
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryType',
    prop: 'costRecovery.type',
    flexGrow: 1.5
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryAmount',
    prop: 'costRecovery.amount',
    flexGrow: 2,
    cellClass: 'text-right'
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryCurrency',
    prop: 'costRecovery.currency.code',
    flexGrow: 1.5
  },
  {
    name: 'TIP.productConsents.columns.status',
    prop: 'status',
    flexGrow: 1.5
  }
];

export const AGENT_USER_COLUMNS: Array<GridColumn> = [
  {
    name: 'TIP.productConsents.columns.airlineCode',
    prop: 'airline.iataCode',
    flexGrow: 1
  },
  {
    name: 'TIP.productConsents.columns.airlineName',
    prop: 'airline.name',
    flexGrow: 2
  },
  {
    name: 'TIP.productConsents.columns.providerName',
    prop: 'product.provider.name',
    flexGrow: 2,
    cellTemplate: 'multipleLinkCellTmpl'
  },
  {
    name: 'TIP.productConsents.columns.productName',
    prop: 'product.name',
    flexGrow: 2
  },
  {
    name: 'TIP.productConsents.columns.consentType',
    prop: 'type',
    flexGrow: 2
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryType',
    prop: 'costRecovery.type',
    flexGrow: 2
  },
  {
    name: 'TIP.productConsents.columns.costRecoveryAmount',
    prop: 'costRecovery.amount',
    flexGrow: 2,
    cellClass: 'text-right'
  },
  {
    name: 'TIP.productConsents.columns.status',
    prop: 'status',
    flexGrow: 1
  }
];

export const COUNTRY_LEVEL_ACTIONS: Array<{ action: GridTableActionType }> = [
  { action: GridTableActionType.ModifyProductBspLevelConsent }
];

export const AGENT_LEVEL_ACTIONS: Array<{ action: GridTableActionType }> = [
  { action: GridTableActionType.ModifyProductAgentLevelConsent },
  { action: GridTableActionType.TerminateProductAgentLevelConsent }
];
