import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, discardPeriodicTasks, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';

import { TipProductConsent } from '../../models/tip-product-consent.model';
import { CountryLevelConsentType, TipProduct, TipProductConsentLevel, TipStatus } from '../../models/tip-product.model';
import { TipProvider } from '../../models/tip-provider.model';
import { TipProductConsentFormatter } from '../../services/tip-product-consent-formatter.service';
import { TipProductConsentService } from '../../services/tip-product-consent.service';
import { TipProductDialogService } from '../../services/tip-product-dialog.service';
import { TipProductService } from '../../services/tip-product.service';
import { TipProviderService } from '../../services/tip-provider.service';
import { changeAgentConsentCreateButtonVisibilityAction } from '../store/actions/tip-product-consent.actions';
import { TipProductConsentListComponent } from './tip-product-consent-list.component';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ViewListActions } from '~app/core/actions';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService, FooterButton } from '~app/shared/components';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { AgentSummary, AirlineSummary, DropdownOption, GridColumn } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import {
  AgentDictionaryService,
  AirlineDictionaryService,
  AppConfigurationService,
  CurrencyDictionaryService
} from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { TranslatePipeMock } from '~app/test';

describe('TipProductConsentListComponent', () => {
  let fixture: ComponentFixture<TipProductConsentListComponent>;
  let component: TipProductConsentListComponent;

  const tipProductConsentServiceSpy = createSpyObject(TipProductConsentService);
  const tipProductDialogServiceSpy = createSpyObject(TipProductDialogService);
  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);

  const expectedUserDetails = {
    userType: UserType.AIRLINE, // AIRLINE user by default
    permissions: ['rTipPrdCnsCoAc', 'rTipPrdCnsCoIn', 'rTipPrdCnsAgAc', 'rTipPrdCnsAgIn'],
    bspPermissions: [
      { bspId: 1, permissions: ['rTipPrdCnsCoAc', 'rTipPrdCnsCoIn', 'rTipPrdCnsAgAc', 'rTipPrdCnsAgIn'] }
    ],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          'MENU.TIP.products.tabLabel': {},
          activeTabId: 'MENU.TIP.products.tabLabel'
        }
      },
      viewListsInfo: {}
    },
    tip: { 'tip-product-consent-consent-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TipProductConsentListComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({ initialState }),
        FormBuilder,
        PermissionsService,
        BspsDictionaryService,
        TipProviderService,
        TipProductService,
        AgentDictionaryService,
        AirlineDictionaryService,
        CurrencyDictionaryService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: TipProductConsentService, useValue: tipProductConsentServiceSpy },
        { provide: TipProductDialogService, useValue: tipProductDialogServiceSpy },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    })
      .overrideComponent(TipProductConsentListComponent, {
        set: { providers: [TipProductConsentFormatter] }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipProductConsentListComponent);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  describe('notice message', () => {
    it('should be null when user is an AIRLINE', fakeAsync(() => {
      let noticeMessage: string;

      fixture.detectChanges();
      component.noticeMessage$.subscribe(msg => (noticeMessage = msg));

      discardPeriodicTasks();

      expect(noticeMessage).toBeNull();
    }));

    it('should return alert info message when user is an AGENT', fakeAsync(() => {
      let noticeMessage: string;

      const mockStore = TestBed.inject(MockStore);

      mockStore.overrideSelector(getUserType, UserType.AGENT);
      mockStore.refreshState();

      fixture.detectChanges();
      component.noticeMessage$.subscribe(msg => (noticeMessage = msg));

      discardPeriodicTasks();

      expect(noticeMessage).toBe('TIP.productConsents.alertInfo');
    }));
  });

  describe('predefined filters', () => {
    it('should set `Country` consent level filter as predefined when user is an AIRLINE', () => {
      // User is AIRLINE by default, no need to update the store
      fixture.detectChanges();

      expect(component.predefinedFilters).toEqual({ level: TipProductConsentLevel.Country });
    });

    it('should NOT set any predefined filter when user is an AGENT', () => {
      const mockStore = TestBed.inject(MockStore);

      mockStore.overrideSelector(getUserType, UserType.AGENT);
      mockStore.refreshState();

      fixture.detectChanges();

      expect(component.predefinedFilters).toBeNull();
    });
  });

  describe('query listeners', () => {
    it('should update consent level if it exists in the previous stored query', () => {
      const mockStore = TestBed.inject(MockStore);

      mockStore.setState({
        ...initialState,
        tip: {
          'tip-product-consent-consent-list': {
            ...fromListSubtabs.initialState,
            query: {
              ...fromListSubtabs.initialState.query,
              filterBy: { level: TipProductConsentLevel.Agent }
            }
          }
        }
      });
      mockStore.refreshState();

      spyOn<any>(component, 'changeLevelConsent').and.callThrough();

      fixture.detectChanges();
      expect(component['changeLevelConsent']).toHaveBeenCalledWith(TipProductConsentLevel.Agent);
    });

    it('should update consent level if it exists in the previous stored query', () => {
      // Consent level is undefined by default in the store
      spyOn<any>(component, 'changeLevelConsent').and.callThrough();

      fixture.detectChanges();
      expect(component['changeLevelConsent']).not.toHaveBeenCalled();
    });
  });

  describe('initialize columns', () => {
    describe('columns as an AIRLINE', () => {
      it('should initialize country level columns', fakeAsync(() => {
        let columns: Array<GridColumn>;

        const expectedColumns = jasmine.arrayContaining<any>([
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.bspConsent',
            prop: 'type',
            pipe: jasmine.anything()
          }),
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.costRecoveryType',
            prop: 'costRecovery.type',
            pipe: jasmine.anything()
          })
        ]);

        fixture.detectChanges();
        component.columns$.subscribe(cols => (columns = cols));

        discardPeriodicTasks();

        expect(columns).toEqual(expectedColumns);
      }));

      it('should initialize agent level columns', fakeAsync(() => {
        let columns: Array<GridColumn>;

        const expectedColumns = jasmine.arrayContaining<any>([
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.agentCode',
            prop: 'agent.iataCode'
          }),
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.agentConsent',
            prop: 'type',
            pipe: jasmine.anything()
          }),
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.costRecoveryType',
            prop: 'costRecovery.type',
            pipe: jasmine.anything()
          })
        ]);

        fixture.detectChanges();
        component.onLevelConsentSelected(TipProductConsentLevel.Agent);
        component.columns$.subscribe(cols => (columns = cols));

        discardPeriodicTasks();

        expect(columns).toEqual(expectedColumns);
      }));

      it('should set checkbox column when user has update permissions', fakeAsync(() => {
        let columns: Array<GridColumn>;

        const expectedColumns = jasmine.arrayContaining<any>([
          jasmine.objectContaining({
            prop: 'isRowSelected',
            hidden: false
          })
        ]);

        spyOn(component['permissionsService'], 'hasPermission').and.returnValue(true);

        fixture.detectChanges();
        component.columns$.subscribe(cols => (columns = cols));

        discardPeriodicTasks();

        expect(columns).toEqual(expectedColumns);
      }));
    });

    describe('columns as an AGENT', () => {
      beforeEach(() => {
        const mockStore = TestBed.inject(MockStore);

        mockStore.overrideSelector(getUserType, UserType.AGENT);
        mockStore.refreshState();
      });

      it('should initialize agent user columns', fakeAsync(() => {
        let columns: Array<GridColumn>;

        const expectedColumns = jasmine.arrayContaining<any>([
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.airlineCode',
            prop: 'airline.iataCode'
          }),
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.airlineName',
            prop: 'airline.name'
          }),
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.consentType',
            prop: 'type',
            pipe: jasmine.anything()
          }),
          jasmine.objectContaining({
            name: 'TIP.productConsents.columns.costRecoveryType',
            prop: 'costRecovery.type',
            pipe: jasmine.anything()
          })
        ]);

        fixture.detectChanges();
        component.columns$.subscribe(cols => (columns = cols));

        discardPeriodicTasks();

        expect(columns).toEqual(expectedColumns);
      }));
    });
  });

  describe('initialize actions', () => {
    describe('actions as an AIRLINE', () => {
      it('should not be visible without update permissions', fakeAsync(() => {
        let tableActions: Array<{ action: GridTableActionType; disabled?: boolean }>;

        fixture.detectChanges();
        component.tableActions$.subscribe(actions => (tableActions = actions));

        discardPeriodicTasks();

        expect(tableActions).toBeNull();
      }));

      it('should build country level actions with update permissions', fakeAsync(() => {
        let tableActions: Array<{ action: GridTableActionType; disabled?: boolean }>;

        spyOn(component['permissionsService'], 'hasPermission').and.returnValue(true);

        fixture.detectChanges();
        component.onLevelConsentSelected(TipProductConsentLevel.Country);
        component.tableActions$.subscribe(actions => (tableActions = actions));

        discardPeriodicTasks();

        expect(tableActions).toEqual([{ action: GridTableActionType.ModifyProductBspLevelConsent }]);
      }));

      it('should build agent level actions with update permissions', fakeAsync(() => {
        let tableActions: Array<{ action: GridTableActionType; disabled?: boolean }>;

        spyOn(component['permissionsService'], 'hasPermission').and.returnValue(true);

        fixture.detectChanges();
        component.onLevelConsentSelected(TipProductConsentLevel.Agent);
        component.tableActions$.subscribe(actions => (tableActions = actions));

        discardPeriodicTasks();

        expect(tableActions).toEqual([
          { action: GridTableActionType.ModifyProductAgentLevelConsent },
          { action: GridTableActionType.TerminateProductAgentLevelConsent }
        ]);
      }));
    });

    describe('actions as an AGENT', () => {
      beforeEach(() => {
        const mockStore = TestBed.inject(MockStore);

        mockStore.overrideSelector(getUserType, UserType.AGENT);
        mockStore.refreshState();
      });

      it('should not be visible without agent read permissions', fakeAsync(() => {
        let tableActions: Array<{ action: GridTableActionType; disabled?: boolean }>;

        spyOn(component['permissionsService'], 'hasPermission').and.returnValue(false);

        fixture.detectChanges();
        component.tableActions$.subscribe(actions => (tableActions = actions));

        discardPeriodicTasks();

        expect(tableActions).toBeNull();
      }));

      it('should build agent user actions with agent read permissions', fakeAsync(() => {
        let tableActions: Array<{ action: GridTableActionType; disabled?: boolean }>;

        fixture.detectChanges();
        component.tableActions$.subscribe(actions => (tableActions = actions));

        discardPeriodicTasks();

        expect(tableActions).toEqual([{ action: GridTableActionType.CheckConsents }]);
      }));
    });
  });

  describe('initialize button options', () => {
    beforeEach(() => fixture.detectChanges());

    it('should set only the modify consent button option with COUNTRY level', () => {
      expect(component.buttonMenuOptions.length).toBe(1);
    });

    it('should set modify & terminate consent button options with AGENT level', () => {
      component.onLevelConsentSelected(TipProductConsentLevel.Agent);
      expect(component.buttonMenuOptions.length).toBe(2);
    });
  });

  describe('search form behaviour', () => {
    beforeEach(() => {
      spyOn(component['formFactory'], 'createGroup').and.callThrough();
      fixture.detectChanges();
    });

    it('should initialize form on init', () => {
      expect(component['formFactory'].createGroup).toHaveBeenCalledTimes(1);
    });

    it('should reset form instead of initialize it on consent level selected', () => {
      spyOn(component.searchForm, 'reset').and.callThrough();

      component.onLevelConsentSelected(TipProductConsentLevel.Agent);

      expect(component.searchForm.reset).toHaveBeenCalledTimes(1); // 1 time after selecting a consent level
      expect(component['formFactory'].createGroup).toHaveBeenCalledTimes(1); // Only 1 time: on component init
    });
  });

  describe('create agent consent button visibility', () => {
    it('should set button visibility as FALSE if the user has NOT the corresponding permission', () => {
      const mockStore = TestBed.inject(MockStore);

      spyOn(mockStore, 'dispatch').and.callThrough();
      spyOn(component['permissionsService'], 'hasPermission').and.returnValue(false);

      fixture.detectChanges();
      expect(mockStore.dispatch).toHaveBeenCalledWith(
        changeAgentConsentCreateButtonVisibilityAction({ isCreateAgentConsentButtonVisible: false })
      );
    });

    it('should set button visibility as FALSE if agent consent level is NOT selected', () => {
      const mockStore = TestBed.inject(MockStore);

      spyOn(mockStore, 'dispatch').and.callThrough();
      spyOn(component['permissionsService'], 'hasPermission').and.returnValue(true);

      fixture.detectChanges();

      // Country consent level is auto-selected by default
      expect(mockStore.dispatch).toHaveBeenCalledWith(
        changeAgentConsentCreateButtonVisibilityAction({ isCreateAgentConsentButtonVisible: false })
      );
    });

    it('should set button visibility as TRUE if the user has the corresponding permission & agent consent level is selected', () => {
      const mockStore = TestBed.inject(MockStore);

      spyOn(mockStore, 'dispatch').and.callThrough();
      spyOn(component['permissionsService'], 'hasPermission').and.returnValue(true);

      fixture.detectChanges();
      component.onLevelConsentSelected(TipProductConsentLevel.Agent);

      // Country consent level is auto-selected by default
      expect(mockStore.dispatch).toHaveBeenCalledWith(
        changeAgentConsentCreateButtonVisibilityAction({ isCreateAgentConsentButtonVisible: true })
      );
    });
  });

  describe('initialize filter dropdowns', () => {
    it('should NOT initialize filter dropdowns on init, only on filter button click with search panel visible', () => {
      spyOn<any>(component, 'initializeFilterDropdowns').and.callThrough();

      fixture.detectChanges();

      // Not initializing on init
      expect(component['initializeFilterDropdowns']).not.toHaveBeenCalled();

      // Not initializing with search panel not visible
      component.onFilterButtonClicked(false);
      expect(component['initializeFilterDropdowns']).not.toHaveBeenCalled();

      // Initializing with search panel visible
      component.onFilterButtonClicked(true);
      expect(component['initializeFilterDropdowns']).toHaveBeenCalled();
    });

    it('should initialize BSP list taking into account BSP permissions', fakeAsync(() => {
      let bspDropdownOptions: DropdownOption<BspDto>[];

      const bspsMock = [
        { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
        { id: 2, name: 'MALTA', isoCountryCode: 'MT' }
      ];

      const service = TestBed.inject(BspsDictionaryService);
      spyOn(service, 'getAllBsps').and.returnValue(of(bspsMock));

      fixture.detectChanges();
      component.onFilterButtonClicked(true);
      component.bspList$.subscribe(options => (bspDropdownOptions = options));

      discardPeriodicTasks();

      // The user does not have any permission for Malta BSP
      expect(bspDropdownOptions).toEqual([
        {
          value: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
          label: 'ES - SPAIN'
        }
      ]);
    }));

    describe('provider dropdown options', () => {
      it('should initialize provider dropdown options with the current selected BSPs', fakeAsync(() => {
        let providerDropdownOptions: DropdownOption<TipProvider>[];

        const bspsMock: BspDto[] = [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }];
        const providersMock: TipProvider[] = [
          {
            id: 1,
            code: 'PROV1',
            name: 'provider1',
            bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
          }
        ];

        const service = TestBed.inject(TipProviderService);
        spyOn(service, 'getProviders').and.returnValue(of(providersMock));
        spyOn(service, 'getProvidersDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.providerList$.subscribe(options => (providerDropdownOptions = options));

        component['bspControl'].patchValue(bspsMock);
        tick();

        discardPeriodicTasks();

        expect(service.getProvidersDropdownOptions).toHaveBeenCalledWith(bspsMock);
        expect(providerDropdownOptions).toEqual([
          {
            value: {
              id: 1,
              code: 'PROV1',
              name: 'provider1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            },
            label: 'ES - PROV1 / provider1'
          }
        ]);
      }));

      it('should update provider control with the options retrieved', fakeAsync(() => {
        const providerOptionsMock: DropdownOption<TipProvider>[] = [
          {
            value: { id: 1, code: 'PROV1', name: 'provider1', bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' } },
            label: ''
          }
        ];

        const service = TestBed.inject(TipProviderService);
        spyOn(service, 'getProvidersDropdownOptions').and.returnValue(of(providerOptionsMock));

        spyOn<any>(component, 'updateOptionsControl').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.providerList$.subscribe();
        tick();

        discardPeriodicTasks();

        expect(component['updateOptionsControl']).toHaveBeenCalledWith(
          component.searchForm.controls.provider,
          providerOptionsMock
        );
      }));
    });

    describe('product dropdown options', () => {
      it('should initialize product dropdown options with the current selected BSPs', fakeAsync(() => {
        let productDropdownOptions: DropdownOption<TipProduct>[];

        const bspsMock: BspDto[] = [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }];
        const productsMock: TipProduct[] = [
          {
            id: 1,
            code: 'PROD1',
            name: 'product1',
            bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
          }
        ] as TipProduct[];

        const service = TestBed.inject(TipProductService);
        spyOn(service, 'getProducts').and.returnValue(of({ records: productsMock }));
        spyOn(service, 'getProductsDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.productList$.subscribe(options => (productDropdownOptions = options));

        component['bspControl'].patchValue(bspsMock);
        tick();

        discardPeriodicTasks();

        expect(service.getProductsDropdownOptions).toHaveBeenCalledWith(bspsMock);
        expect(productDropdownOptions).toEqual([
          {
            value: {
              id: 1,
              code: 'PROD1',
              name: 'product1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            } as TipProduct,
            label: 'ES - PROD1 / product1'
          }
        ]);
      }));

      it('should update product control with the options retrieved', fakeAsync(() => {
        const productOptionsMock: DropdownOption<TipProduct>[] = [
          {
            value: {
              id: 1,
              code: 'PROD1',
              name: 'product1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            } as TipProduct,
            label: ''
          }
        ];

        const service = TestBed.inject(TipProductService);
        spyOn(service, 'getProductsDropdownOptions').and.returnValue(of(productOptionsMock));

        spyOn<any>(component, 'updateOptionsControl').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.productList$.subscribe();
        tick();

        discardPeriodicTasks();

        expect(component['updateOptionsControl']).toHaveBeenCalledWith(
          component.searchForm.controls.product,
          productOptionsMock
        );
      }));
    });

    describe('agent dropdown options', () => {
      it('should initialize agent dropdown options with the current selected BSPs', fakeAsync(() => {
        let agentDropdownOptions: DropdownOption<AgentSummary>[];

        const bspsMock: BspDto[] = [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }];
        const agentsMock: AgentSummary[] = [
          {
            id: '1',
            code: 'AG1',
            name: 'agent1',
            bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
          }
        ];

        const service = TestBed.inject(AgentDictionaryService);
        spyOn(service, 'get').and.returnValue(of(agentsMock));
        spyOn(service, 'getDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.agentList$.subscribe(options => (agentDropdownOptions = options));

        component['bspControl'].patchValue(bspsMock);
        tick();

        discardPeriodicTasks();

        expect(service.getDropdownOptions).toHaveBeenCalledWith({ active: true, bspId: [1] });
        expect(agentDropdownOptions).toEqual([
          {
            value: {
              id: '1',
              code: 'AG1',
              name: 'agent1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            },
            label: 'ES - AG1 / agent1'
          }
        ]);
      }));

      it('should update product control with the options retrieved', fakeAsync(() => {
        const agentOptionsMock: DropdownOption<AgentSummary>[] = [
          {
            value: {
              id: '1',
              code: 'AG1',
              name: 'agent1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            },
            label: 'ES - AG1 / agent1'
          }
        ];

        const service = TestBed.inject(AgentDictionaryService);
        spyOn(service, 'getDropdownOptions').and.returnValue(of(agentOptionsMock));

        spyOn<any>(component, 'updateOptionsControl').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.agentList$.subscribe();
        tick();

        discardPeriodicTasks();

        expect(component['updateOptionsControl']).toHaveBeenCalledWith(
          component.searchForm.controls.agent,
          agentOptionsMock
        );
      }));
    });

    describe('airline dropdown options', () => {
      it('should initialize airline dropdown options with the first user BSP (agent user)', fakeAsync(() => {
        let airlineDropdownOptions: DropdownOption<AirlineSummary>[];

        const airlinesMock: AirlineSummary[] = [{ id: 1, code: 'IB', name: 'iberia', designator: 'L' }];

        const service = TestBed.inject(AirlineDictionaryService);
        spyOn(service, 'get').and.returnValue(of(airlinesMock));
        spyOn(service, 'getDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.airlineList$.subscribe(options => (airlineDropdownOptions = options));

        discardPeriodicTasks();

        // User bsps ordered by name: Malta -> Spain. So Malta will be the first BSP id selected (2)
        expect(service.getDropdownOptions).toHaveBeenCalledWith(2);
        expect(airlineDropdownOptions).toEqual([
          {
            value: { id: 1, code: 'IB', name: 'iberia', designator: 'L' },
            label: 'IB / iberia'
          }
        ]);
      }));
    });

    describe('currency dropdown options', () => {
      it('should initialize currency dropdown options with the current selected BSPs', fakeAsync(() => {
        let currencyDropdownOptions: DropdownOption<Currency>[];

        const bspsMock: BspDto[] = [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }];
        const currenciesMock: Currency[] = [{ id: 1, code: 'EUR', decimals: 2 }];

        const service = TestBed.inject(CurrencyDictionaryService);
        spyOn<any>(service, 'getFilteredCurrencies').and.returnValue(of(currenciesMock));
        spyOn(service, 'getFilteredDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.currencyList$.subscribe(options => (currencyDropdownOptions = options));

        component['bspControl'].patchValue(bspsMock);
        tick();

        discardPeriodicTasks();

        expect(service.getFilteredDropdownOptions).toHaveBeenCalledWith({ bspId: [1] });
        expect(currencyDropdownOptions).toEqual([
          {
            value: { id: 1, code: 'EUR', decimals: 2 },
            label: 'EUR'
          }
        ]);
      }));

      it('should update cost recovery currency control with the options retrieved', fakeAsync(() => {
        const currencyOptionsMock: DropdownOption<Currency>[] = [
          {
            value: { id: 1, code: 'EUR', decimals: 2 },
            label: 'EUR'
          }
        ];

        const service = TestBed.inject(CurrencyDictionaryService);
        spyOn(service, 'getFilteredDropdownOptions').and.returnValue(of(currencyOptionsMock));

        spyOn<any>(component, 'updateOptionsControl').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.currencyList$.subscribe();
        tick();

        discardPeriodicTasks();

        expect(component['updateOptionsControl']).toHaveBeenCalledWith(
          component.searchForm.controls.costRecoveryCurrency,
          currencyOptionsMock
        );
      }));
    });
  });

  it('`updateOptionsControl` - should update the corresponding options control', () => {
    const providerOptionsMock: DropdownOption<TipProvider>[] = [
      {
        value: { id: 1, code: 'PROV1', name: 'provider1' },
        label: ''
      }
    ];

    fixture.detectChanges();

    // Testing with provider control
    component.searchForm.patchValue({
      provider: [
        { id: 1, code: 'PROV1', name: 'provider1' },
        { id: 2, code: 'PROV2', name: 'provider2' }
      ]
    });
    component['updateOptionsControl'](component.searchForm.controls.provider, providerOptionsMock);

    expect(component.searchForm.value.provider).toEqual([{ id: 1, code: 'PROV1', name: 'provider1' }]);
  });

  describe('detail navigation', () => {
    it('should navigate with the corresponding provider ID on provider name click', () => {
      component.onRowLinkClick({
        propertyValue: '', // It does not matter
        propertyName: 'product.provider.name',
        row: { product: { provider: { id: 23 } } } as TipProductConsent
      });

      expect(routerSpy.navigate).toHaveBeenCalledWith(jasmine.arrayContaining([23]));
    });

    it('should navigate with the corresponding product ID on product name click', () => {
      component.onRowLinkClick({
        propertyValue: '', // It does not matter
        propertyName: 'product.name',
        row: { product: { id: 14 } } as TipProductConsent
      });

      expect(routerSpy.navigate).toHaveBeenCalledWith(jasmine.arrayContaining([14]));
    });
  });

  describe('action click', () => {
    const productConsentMock: TipProductConsent = {
      status: TipStatus.Active,
      product: { id: 1 }
    } as TipProductConsent;

    describe('modify consent dialog', () => {
      it('should open the dialog on `ModifyProductBspLevelConsent` action', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.ModifyProductBspLevelConsent,
          methodName: ''
        };

        component['changeLevelConsent'](TipProductConsentLevel.Agent);
        component.onActionClick({ action: actionMock, row: productConsentMock });

        expect(tipProductDialogServiceSpy.openModifyConsentDialog).toHaveBeenCalledWith(TipProductConsentLevel.Agent, [
          productConsentMock
        ]);
      });

      it('should open the dialog on `ModifyProductAgentLevelConsent` action', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.ModifyProductAgentLevelConsent,
          methodName: ''
        };

        component['changeLevelConsent'](TipProductConsentLevel.Agent);
        component.onActionClick({ action: actionMock, row: productConsentMock });

        expect(tipProductDialogServiceSpy.openModifyConsentDialog).toHaveBeenCalledWith(TipProductConsentLevel.Agent, [
          productConsentMock
        ]);
      });

      it('should change query on submit button dialog response', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.ModifyProductAgentLevelConsent,
          methodName: ''
        };

        spyOn(component, 'onQueryChanged').and.callThrough();
        tipProductDialogServiceSpy.openModifyConsentDialog.and.returnValue(of(FooterButton.Modify));

        component.onActionClick({ action: actionMock, row: productConsentMock });

        expect(component.onQueryChanged).toHaveBeenCalled();
      });

      it('should NOT change query on cancel button dialog response', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.ModifyProductAgentLevelConsent,
          methodName: ''
        };

        spyOn(component, 'onQueryChanged').and.callThrough();
        tipProductDialogServiceSpy.openModifyConsentDialog.and.returnValue(of(FooterButton.Cancel));

        component.onActionClick({ action: actionMock, row: productConsentMock });

        expect(component.onQueryChanged).not.toHaveBeenCalled();
      });
    });

    describe('terminate consent dialog', () => {
      it('should open the dialog on `TerminateProductAgentLevelConsent` action', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.TerminateProductAgentLevelConsent,
          methodName: ''
        };

        component.onActionClick({ action: actionMock, row: productConsentMock });
        expect(tipProductDialogServiceSpy.openAgentTerminateConsentDialog).toHaveBeenCalledWith([productConsentMock]);
      });

      it('should change query on submit button dialog response', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.TerminateProductAgentLevelConsent,
          methodName: ''
        };

        spyOn(component, 'onQueryChanged').and.callThrough();
        tipProductDialogServiceSpy.openAgentTerminateConsentDialog.and.returnValue(of(FooterButton.Terminate));

        component.onActionClick({ action: actionMock, row: productConsentMock });

        expect(component.onQueryChanged).toHaveBeenCalled();
      });
    });

    describe('product list navigation', () => {
      it('should dispatch updated filter to the product list query on `CheckConsents` action', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.CheckConsents,
          methodName: ''
        };

        const mockStore = TestBed.inject(MockStore);
        spyOn(mockStore, 'dispatch').and.callThrough();

        component.onActionClick({ action: actionMock, row: productConsentMock });

        expect(mockStore.dispatch).toHaveBeenCalledWith(
          new ViewListActions.FilterChange({
            viewListId: jasmine.anything() as any,
            filter: { status: TipStatus.Active, products: [{ id: 1 }] }
          })
        );
      });

      it('should navigate to the product list on `CheckConsents` action', () => {
        const actionMock: GridTableAction = {
          actionType: GridTableActionType.CheckConsents,
          methodName: ''
        };

        component.onActionClick({ action: actionMock, row: productConsentMock });

        expect(routerSpy.navigate).toHaveBeenCalledWith(['/tip/products/list']);
      });
    });
  });

  it('should open download dialog with the corresponding query', fakeAsync(() => {
    const expectedQuery = jasmine.objectContaining({
      filterBy: jasmine.objectContaining({ type: [CountryLevelConsentType.Yes] })
    });

    fixture.detectChanges();

    component['query'] = { ...defaultQuery, filterBy: { type: [CountryLevelConsentType.Yes] } };
    component.onDownload();

    discardPeriodicTasks();

    expect(dialogServiceSpy.open).toHaveBeenCalledWith(
      jasmine.anything(),
      jasmine.objectContaining({ data: jasmine.objectContaining({ downloadQuery: expectedQuery }) })
    );
  }));
});
