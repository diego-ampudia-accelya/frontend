import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { getUser, getUserBsps, isAgentUser, isAirlineUser } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ViewListActions } from '~app/core/actions';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { ButtonDesign, DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions, ROUTES } from '~app/shared/constants';
import { SelectMode } from '~app/shared/enums';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { createTabViewListId } from '~app/shared/helpers/create-tab-view-list-id.helper';
import {
  AgentSummary,
  AirlineSummary,
  DropdownOption,
  GridColumn,
  GridColumnMultipleLinkClick
} from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { Currency } from '~app/shared/models/currency.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User } from '~app/shared/models/user.model';
import { AgentDictionaryService, AirlineDictionaryService, CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { consentKeys, consentSelectors, State } from '~app/tip/reducers';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';
import {
  getReadTipProductAgentConsentPermissions,
  getReadTipProductConsentPermissions,
  getUpdateTipProductConsentPermission,
  permissions
} from '../../config/tip-product-permissions.config';
import { TipProductConsentFilter } from '../../models/tip-product-consent-filter.model';
import { TipProductConsent } from '../../models/tip-product-consent.model';
import { TipProductFilter } from '../../models/tip-product-filter.model';
import { CountryLevelConsentType, TipProduct, TipProductConsentLevel, TipStatus } from '../../models/tip-product.model';
import { TipProvider } from '../../models/tip-provider.model';
import { TipProductConsentFormatter } from '../../services/tip-product-consent-formatter.service';
import { TipProductConsentService } from '../../services/tip-product-consent.service';
import { TipProductDialogService } from '../../services/tip-product-dialog.service';
import { TipProductService } from '../../services/tip-product.service';
import { TipProviderService } from '../../services/tip-provider.service';
import { changeAgentConsentCreateButtonVisibilityAction } from '../store/actions/tip-product-consent.actions';
import {
  AGENT_LEVEL_ACTIONS,
  AGENT_LEVEL_COLUMNS,
  AGENT_USER_COLUMNS,
  COUNTRY_LEVEL_ACTIONS,
  COUNTRY_LEVEL_COLUMNS
} from './tip-product-consent-list.constants';

@Component({
  selector: 'bspl-tip-product-consent-list',
  templateUrl: './tip-product-consent-list.component.html',
  styleUrls: ['./tip-product-consent-list.component.scss'],
  providers: [TipProductConsentFormatter]
})
export class TipProductConsentListComponent
  extends ListSubtabs<TipProductConsent, TipProductConsentFilter>
  implements OnInit, OnDestroy
{
  public predefinedFilters: Partial<TipProductConsentFilter>;

  // TODO: all this getters should not be called in template unless we are using onPush strategy.
  // On that case, we should manage selected level consent with obs and subjects

  public get isAgentLevel(): boolean {
    return this.selectedLevelConsent === TipProductConsentLevel.Agent;
  }

  public get isCountryLevel(): boolean {
    return this.selectedLevelConsent === TipProductConsentLevel.Country;
  }

  public get hasUpdatePermission(): boolean {
    return this.permissionsService.hasPermission(getUpdateTipProductConsentPermission(this.selectedLevelConsent));
  }

  public get hasAgentReadPermission(): boolean {
    return this.permissionsService.hasPermission(getReadTipProductAgentConsentPermissions());
  }

  public get hasLeanPermission(): boolean {
    return this.permissionsService.hasPermission(Permissions.lean);
  }

  public query: DataQuery<TipProductConsentFilter>;
  public columns$: Observable<Array<GridColumn>>;
  public tableActions$: Observable<Array<{ action: GridTableActionType; disabled?: boolean }>>;

  public searchForm: FormGroup;

  public selectMode = SelectMode;
  public btnDesign = ButtonDesign;

  public statusList: DropdownOption<TipStatus>[];
  public bspList$: Observable<DropdownOption<BspDto>[]>;
  public providerList$: Observable<DropdownOption<TipProvider>[]>;
  public productList$: Observable<DropdownOption<TipProduct>[]>;
  public agentList$: Observable<DropdownOption<AgentSummary>[]>;
  public airlineList$: Observable<DropdownOption<AirlineSummary>[]>;
  public currencyList$: Observable<DropdownOption<Currency>[]>;
  public typeList: DropdownOption<CountryLevelConsentType>[];
  public costRecoveryTypeList: DropdownOption<CostRecoveryType>[];

  public selectedLevelConsent: TipProductConsentLevel;
  public levelConsentOptions: DropdownOption<TipProductConsentLevel>[] = Object.values(TipProductConsentLevel).map(
    consent => ({
      value: consent,
      label: this.translationService.translate(`TIP.productConsents.levelConsentDropdown.${consent}Label`)
    })
  );

  public buttonMenuOptions: ButtonMenuOption[] = [];

  public isAirlineUser$: Observable<boolean> = this.store.pipe(select(isAirlineUser), first());
  public isAgentUser$: Observable<boolean> = this.store.pipe(select(isAgentUser), first());

  public areActionsVisible$: Observable<boolean> = combineLatest([this.isAirlineUser$, this.isAgentUser$]).pipe(
    map(([isAirline, isAgent]) => (isAirline && this.hasUpdatePermission) || (isAgent && this.hasAgentReadPermission))
  );

  public noticeMessage$: Observable<string> = this.isAgentUser$.pipe(
    map(isAgent => (isAgent ? 'TIP.productConsents.alertInfo' : null))
  );

  protected initialFilters: Partial<TipProductConsentFilter> = { status: TipStatus.Active };

  private loggedUser$: Observable<User> = this.store.pipe(select(getUser), first());

  private userBspId$: Observable<number> = this.store.pipe(
    select(getUserBsps),
    first(),
    map(bsps => bsps[0].id)
  );

  private get currentBsps$(): Observable<BspDto[]> {
    return this.query$.pipe(
      first(),
      map(query => query.filterBy.bsp || null),
      switchMap(filterBsp => this.bspControl.valueChanges.pipe(startWith(filterBsp)))
    );
  }

  private get modifyConsentButtonMenuOption(): ButtonMenuOption {
    return {
      label: this.translationService.translate('TIP.productConsents.selectionToolbar.buttonOptionLabels.modifyConsent'),
      command: () => {
        this.selectedRows$
          .pipe(switchMap(rows => this.productDialogService.openModifyConsentDialog(this.selectedLevelConsent, rows)))
          .subscribe(clickedBtn => this.handleDialogSubmission(clickedBtn));
      }
    };
  }

  private get terminateConsentButtonMenuOption(): ButtonMenuOption {
    return {
      label: this.translationService.translate(
        'TIP.productConsents.selectionToolbar.buttonOptionLabels.terminateConsent'
      ),
      command: () => {
        this.selectedRows$
          .pipe(switchMap(rows => this.productDialogService.openAgentTerminateConsentDialog(rows)))
          .subscribe(clickedBtn => this.handleDialogSubmission(clickedBtn));
      }
    };
  }

  private bspControl: FormControl;

  private formFactory: FormUtil;

  constructor(
    public displayFormatter: TipProductConsentFormatter,
    protected store: Store<AppState>,
    protected tipProductConsentService: TipProductConsentService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    private tipProviderService: TipProviderService,
    private tipProductService: TipProductService,
    private bspsDictionaryService: BspsDictionaryService,
    private productDialogService: TipProductDialogService,
    private agentDictionaryService: AgentDictionaryService,
    private airlineDictionaryService: AirlineDictionaryService,
    private currencyDictionaryService: CurrencyDictionaryService,
    private permissionsService: PermissionsService,
    private formBuilder: FormBuilder,
    private dialogService: DialogService,
    private router: Router
  ) {
    super(store, tipProductConsentService, actions$, translationService);

    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.setPredefinedFilters();

    super.ngOnInit();

    this.initializeQueryListeners();
    this.initializeListView();
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean): void {
    if (isSearchPanelVisible) {
      this.initializeFilterDropdowns();
    }
  }

  public onRowLinkClick(event: GridColumnMultipleLinkClick<TipProductConsent>): void {
    if (event.propertyName === 'product.provider.name') {
      this.navigateToProviderDetail(event.row.product.provider.id);
    }

    if (event.propertyName === 'product.name') {
      this.navigateToProductDetail(event.row.product.id);
    }
  }

  public onActionClick({ action, row }: { action: GridTableAction; row: TipProductConsent }): void {
    let action$: Observable<FooterButton>;

    switch (action.actionType) {
      case GridTableActionType.ModifyProductBspLevelConsent:
      case GridTableActionType.ModifyProductAgentLevelConsent:
        action$ = this.productDialogService.openModifyConsentDialog(this.selectedLevelConsent, [row]);
        break;
      case GridTableActionType.TerminateProductAgentLevelConsent:
        action$ = this.productDialogService.openAgentTerminateConsentDialog([row]);
        break;
      case GridTableActionType.CheckConsents: {
        const updatedFilter: Partial<TipProductFilter> = { status: row.status, products: [row.product as any] };
        this.handleProductListNavigation(updatedFilter);
        break;
      }
    }

    if (action$) {
      action$.subscribe(clickedBtn => this.handleDialogSubmission(clickedBtn));
    }
  }

  public navigateToProviderDetail(providerId: number): void {
    this.router.navigate([ROUTES.TIP_PROVIDER_DETAIL.url, providerId]);
  }

  public navigateToProductDetail(productId: number): void {
    this.router.navigate([ROUTES.TIP_PRODUCT_DETAIL.url, productId]);
  }

  public onLevelConsentSelected(option: TipProductConsentLevel): void {
    this.changeLevelConsent(option);

    // Reset filters on level consent change
    this.onQueryChanged({ ...this.query, filterBy: { ...this.initialFilters, ...this.predefinedFilters } });

    this.initializeListView();
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.query
      },
      apiService: this.tipProductConsentService
    });
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();

    this.store.dispatch(changeAgentConsentCreateButtonVisibilityAction({ isCreateAgentConsentButtonVisible: false }));
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<TipProductConsent, TipProductConsentFilter>,
    DefaultProjectorFn<ListSubtabsState<TipProductConsent, TipProductConsentFilter>>
  > {
    return consentSelectors[MasterDataType.TipProductConsent];
  }

  protected getListKey(): string {
    return consentKeys[MasterDataType.TipProductConsent];
  }

  private handleProductListNavigation(updatedFilter: Partial<TipProductFilter>): void {
    this.store.dispatch(
      new ViewListActions.FilterChange({
        viewListId: createTabViewListId(ROUTES.TIP_PRODUCTS.tabLabel, 'list'),
        filter: updatedFilter
      })
    );

    this.router.navigate([`${ROUTES.TIP_PRODUCTS.url}/list`]);
  }

  private handleDialogSubmission(clickedBtn: FooterButton): void {
    if (clickedBtn !== FooterButton.Cancel) {
      this.onQueryChanged();
    }
  }

  private setPredefinedFilters(): void {
    this.isAirlineUser$.subscribe(isAirline => {
      this.predefinedFilters = isAirline ? { level: TipProductConsentLevel.Country } : null;
    });
  }

  private initializeQueryListeners(): void {
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.query = query));
    this.query$.pipe(first()).subscribe(query => {
      if (query.filterBy.level) {
        // If we have a previous consent level in the query, it's autoselected to keep stored query
        this.changeLevelConsent(query.filterBy.level);
      }
    });
  }

  private initializeListView(): void {
    this.initializeColumns();
    this.initializeActions();
    this.initializeButtonOptions();

    if (this.searchForm) {
      this.searchForm.reset();
    } else {
      this.initializeForm();
    }

    this.setCreateAgentConsentButtonVisibility();
  }

  private initializeColumns(): void {
    this.columns$ = combineLatest([this.isAirlineUser$, this.isAgentUser$]).pipe(
      map(([isAirline, isAgent]) => {
        let columns: Array<GridColumn> = [];

        if (isAirline) {
          // Airline table columns
          columns = this.isAgentLevel ? AGENT_LEVEL_COLUMNS : COUNTRY_LEVEL_COLUMNS;

          const checkboxColumn = columns.find(column => column.prop === 'isRowSelected');
          checkboxColumn.hidden = !this.hasUpdatePermission;
        }

        if (isAgent) {
          // Agent table columns
          columns = AGENT_USER_COLUMNS;
        }

        const costRecoveryTypeColumn = columns.find(column => column.prop === 'costRecovery.type');
        costRecoveryTypeColumn.pipe = {
          transform: type => type && this.translationService.translate(`TIP.products.costRecoveryType.${type}`)
        };

        const typeColumn = columns.find(column => column.prop === 'type');
        typeColumn.pipe = {
          transform: type => this.translationService.translate(`TIP.products.levelConsentType.${type}`)
        };

        return columns;
      })
    );
  }

  private initializeForm(): void {
    this.bspControl = this.formBuilder.control(null);

    this.searchForm = this.formFactory.createGroup<TipProductConsentFilter>({
      status: [],
      bsp: this.bspControl,
      level: [],
      provider: [],
      product: [],
      agent: [],
      airline: [],
      type: [],
      costRecoveryType: [],
      costRecoveryCurrency: [],
      effectiveFrom: [],
      effectiveTo: []
    });
  }

  private initializeFilterDropdowns(): void {
    const providerControl = FormUtil.get<TipProductConsentFilter>(this.searchForm, 'provider');
    const productControl = FormUtil.get<TipProductConsentFilter>(this.searchForm, 'product');
    const agentControl = FormUtil.get<TipProductConsentFilter>(this.searchForm, 'agent');
    const costRecoveryCurrencyControl = FormUtil.get<TipProductConsentFilter>(this.searchForm, 'costRecoveryCurrency');

    this.bspList$ = this.loggedUser$.pipe(
      switchMap(({ bspPermissions }) =>
        this.bspsDictionaryService.getAllBspsByPermissions(bspPermissions, getReadTipProductConsentPermissions())
      ),
      map(bspList => bspList.map(toValueLabelObjectBsp))
    );

    this.providerList$ = this.currentBsps$.pipe(
      switchMap(bsps => this.tipProviderService.getProvidersDropdownOptions(bsps)),
      tap(options => this.updateOptionsControl(providerControl, options))
    );

    this.productList$ = this.currentBsps$.pipe(
      switchMap(bsps => this.tipProductService.getProductsDropdownOptions(bsps)),
      tap(options => this.updateOptionsControl(productControl, options))
    );

    this.agentList$ = this.currentBsps$.pipe(
      switchMap(bsps =>
        this.agentDictionaryService.getDropdownOptions({
          active: true,
          ...(bsps?.length && { bspId: bsps.map(({ id }) => id) })
        })
      ),
      tap(options => this.updateOptionsControl(agentControl, options))
    );

    this.airlineList$ = this.userBspId$.pipe(
      switchMap(bspId => this.airlineDictionaryService.getDropdownOptions(bspId))
    );

    this.currencyList$ = this.currentBsps$.pipe(
      switchMap(bsps =>
        this.currencyDictionaryService.getFilteredDropdownOptions({
          ...(bsps?.length && { bspId: bsps.map(({ id }) => id) })
        })
      ),
      tap(options => this.updateOptionsControl(costRecoveryCurrencyControl, options))
    );

    this.statusList = ArrayHelper.toDropdownOptions(Object.values(TipStatus));

    this.typeList = Object.values(CountryLevelConsentType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.levelConsentType.${value}`)
    }));

    this.costRecoveryTypeList = Object.values(CostRecoveryType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.costRecoveryType.${value}`)
    }));
  }

  private updateOptionsControl(control: AbstractControl, options: DropdownOption<{ id: number | string }>[]): void {
    if (control.value?.length) {
      const optionIds = options.map(option => option.value.id);

      control.setValue(
        control.value.filter((selectedOption: { id: string | number }) => optionIds.includes(selectedOption.id))
      );
    }
  }

  private changeLevelConsent(level: TipProductConsentLevel): void {
    this.selectedLevelConsent = level;
    this.predefinedFilters = { ...this.predefinedFilters, level };
  }

  private initializeActions(): void {
    this.tableActions$ = this.areActionsVisible$.pipe(
      switchMap(areActionsVisible => (areActionsVisible ? this.buildActions$() : of(null)))
    );
  }

  private buildActions$(): Observable<Array<{ action: GridTableActionType; disabled?: boolean }>> {
    return combineLatest([this.isAirlineUser$, this.isAgentUser$]).pipe(
      map(([isAirline, isAgent]) => {
        let actions: Array<{ action: GridTableActionType; disabled?: boolean }> = [];

        if (isAirline) {
          // Airline table actions
          actions = this.isCountryLevel ? COUNTRY_LEVEL_ACTIONS : AGENT_LEVEL_ACTIONS;
        }

        if (isAgent) {
          // Agent table actions
          actions = [{ action: GridTableActionType.CheckConsents }];
        }

        return actions;
      })
    );
  }

  private initializeButtonOptions(): void {
    this.buttonMenuOptions = [
      this.modifyConsentButtonMenuOption,
      ...(this.isAgentLevel ? [this.terminateConsentButtonMenuOption] : [])
    ];
  }

  private setCreateAgentConsentButtonVisibility(): void {
    const isCreateAgentConsentButtonVisible =
      this.permissionsService.hasPermission(permissions.createTipProductAgentConsent) && this.isAgentLevel;
    this.store.dispatch(changeAgentConsentCreateButtonVisibilityAction({ isCreateAgentConsentButtonVisible }));
  }
}
