import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { clickAgentConsentCreateButtonAction } from './store/actions/tip-product-consent.actions';
import { createAgentConsentButtonSelector } from './store/selectors/tip-product-consent.selectors';
import { MenuBuilder, RoutedMenuItem } from '~app/master-data/configuration';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';

@Component({
  selector: 'bspl-tip-products',
  templateUrl: './tip-products.component.html',
  styleUrls: ['./tip-products.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipProductsComponent implements OnInit {
  public tabs: RoutedMenuItem[];

  public btnDesign = ButtonDesign;
  public isCreateAgentConsentButtonVisible$: Observable<boolean>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private menuBuilder: MenuBuilder,
    private store: Store<AppState>
  ) {}

  public ngOnInit(): void {
    this.initializeTabs();
    this.isCreateAgentConsentButtonVisible$ = this.store.pipe(select(createAgentConsentButtonSelector));
  }

  public onCreateAgentConsentClick(): void {
    this.store.dispatch(clickAgentConsentCreateButtonAction());
  }

  private initializeTabs(): void {
    this.tabs = this.menuBuilder.buildMenuItemsFrom(this.activatedRoute.snapshot);
  }
}
