import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, discardPeriodicTasks, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { of } from 'rxjs';
import { getUserType } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ViewListActions } from '~app/core/actions';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService } from '~app/shared/components';
import { ROUTES } from '~app/shared/constants';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { GridTableAction, GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { TranslatePipeMock } from '~app/test';
import { getReadTipProductListPermissions } from '../../config/tip-product-permissions.config';
import { TipProduct, TipProductConsentLevel, TipStatus } from '../../models/tip-product.model';
import { TipProvider } from '../../models/tip-provider.model';
import { TipProductFormatter } from '../../services/tip-product-formatter.service';
import { TipProductService } from '../../services/tip-product.service';
import { TipProviderService } from '../../services/tip-provider.service';
import { TipProductListComponent } from './tip-product-list.component';

describe('TipProductListComponent', () => {
  let fixture: ComponentFixture<TipProductListComponent>;
  let component: TipProductListComponent;

  const dialogServiceSpy = createSpyObject(DialogService);
  const routerSpy = createSpyObject(Router);

  const expectedUserDetails = {
    userType: UserType.AIRLINE, // AIRLINE user by default
    permissions: ['rTipPrdCnsCoAc', 'rTipPrdCnsCoIn', 'rTipPrdCnsAgAc', 'rTipPrdCnsAgIn'],
    bspPermissions: [{ bspId: 1, permissions: getReadTipProductListPermissions() }],
    bsps: [
      { id: 1, name: 'SPAIN', isoCountryCode: 'ES', effectiveFrom: '2000-01-01' },
      { id: 2, name: 'MALTA', isoCountryCode: 'MT', effectiveFrom: '2000-01-01' }
    ]
  };

  const initialState = {
    auth: { user: expectedUserDetails },
    core: {
      menu: {
        tabs: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          'MENU.TIP.products.tabLabel': {},
          activeTabId: 'MENU.TIP.products.tabLabel'
        }
      },
      viewListsInfo: {}
    },
    tip: { 'tip-product-list-list': fromListSubtabs.initialState }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TipProductListComponent, TranslatePipeMock],
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockActions(() => of()),
        provideMockStore({ initialState }),
        FormBuilder,
        PermissionsService,
        BspsDictionaryService,
        TipProductService,
        TipProviderService,
        CurrencyDictionaryService,
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        { provide: DialogService, useValue: dialogServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    })
      .overrideComponent(TipProductListComponent, {
        set: { providers: [TipProductFormatter] }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipProductListComponent);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  describe('info notice message', () => {
    it('should be null when user is not an AGENT', fakeAsync(() => {
      fixture.detectChanges();

      discardPeriodicTasks();

      expect(component.infoMessage).toBeNull();
    }));

    it('should return alert info message when user is an AGENT', fakeAsync(() => {
      const mockStore = TestBed.inject(MockStore);

      mockStore.overrideSelector(getUserType, UserType.AGENT);
      mockStore.refreshState();

      component['initializeLoggedUserType']();
      fixture.detectChanges();

      discardPeriodicTasks();

      expect(component.infoMessage).toBe('TIP.products.alertInfo');
    }));
  });

  describe('initial filters', () => {
    it('should set `Active` tip status filter as initial filter', () => {
      fixture.detectChanges();

      expect(component['initialFilters']).toEqual({ status: TipStatus.Active });
    });
  });

  describe('initialize columns', () => {
    it('should hide Agent columns when user is an Airline', fakeAsync(() => {
      const expectedColumns = jasmine.arrayContaining<any>([
        jasmine.objectContaining({
          name: 'TIP.products.columns.issuerName',
          hidden: true
        }),
        jasmine.objectContaining({
          name: 'TIP.products.columns.paymentNetwork',
          hidden: true
        }),
        jasmine.objectContaining({
          name: 'TIP.products.columns.agentUrl',
          hidden: true
        })
      ]);

      fixture.detectChanges();
      discardPeriodicTasks();

      expect(component.columns).toEqual(expectedColumns);
    }));

    it('should hide airline columns when user is an Agent', fakeAsync(() => {
      const mockStore = TestBed.inject(MockStore);

      mockStore.overrideSelector(getUserType, UserType.AGENT);
      mockStore.refreshState();

      component['initializeLoggedUserType']();

      const expectedColumns = jasmine.arrayContaining<any>([
        jasmine.objectContaining({
          name: 'TIP.products.columns.bsp',
          hidden: true
        }),
        jasmine.objectContaining({
          name: 'TIP.products.columns.category',
          hidden: true
        }),
        jasmine.objectContaining({
          name: 'TIP.products.columns.bspConsent',
          hidden: true
        }),
        jasmine.objectContaining({
          name: 'TIP.products.columns.effectiveFrom',
          hidden: true
        }),
        jasmine.objectContaining({
          name: 'TIP.products.columns.effectiveTo',
          hidden: true
        })
      ]);

      fixture.detectChanges();
      discardPeriodicTasks();

      expect(component.columns).toEqual(expectedColumns);
    }));
  });

  describe('initialize actions', () => {
    it('should initialize Airline actions', fakeAsync(() => {
      const airlineActions = jasmine.arrayContaining([
        { action: GridTableActionType.BspLevelConsentManagement },
        { action: GridTableActionType.AgentLevelConsentManagement }
      ]);

      fixture.detectChanges();
      discardPeriodicTasks();

      expect(component.actions).toEqual(airlineActions);
    }));

    it('should not have any action if user is not an Airline', fakeAsync(() => {
      const mockStore = TestBed.inject(MockStore);

      mockStore.overrideSelector(getUserType, UserType.AGENT);
      mockStore.refreshState();

      component['initializeLoggedUserType']();

      fixture.detectChanges();
      discardPeriodicTasks();

      expect(component.actions).toEqual([]);
    }));
  });

  describe('search form initialized', () => {
    beforeEach(() => {
      spyOn<any>(component, 'buildForm').and.callThrough();
      fixture.detectChanges();
    });

    it('should initialize form on init', () => {
      expect(component['buildForm']).toHaveBeenCalledTimes(1);
      expect(component.searchForm).toBeTruthy();
    });
  });

  describe('initialize filter dropdowns', () => {
    it('should NOT initialize filter dropdowns on init, only on filter button click with search panel visible', () => {
      spyOn<any>(component, 'initializeFilterDropdowns').and.callThrough();

      fixture.detectChanges();

      // Not initializing on init
      expect(component['initializeFilterDropdowns']).not.toHaveBeenCalled();

      // Not initializing with search panel not visible
      component.onFilterButtonClicked(false);
      expect(component['initializeFilterDropdowns']).not.toHaveBeenCalled();

      // Initializing with search panel visible
      component.onFilterButtonClicked(true);
      expect(component['initializeFilterDropdowns']).toHaveBeenCalled();
    });

    it('should initialize BSP list taking into account BSP permissions', fakeAsync(() => {
      let bspDropdownOptions: DropdownOption<BspDto>[];

      const bspsMock = [
        { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
        { id: 2, name: 'MALTA', isoCountryCode: 'MT' }
      ];

      const service = TestBed.inject(BspsDictionaryService);
      spyOn(service, 'getAllBsps').and.returnValue(of(bspsMock));

      fixture.detectChanges();
      component.onFilterButtonClicked(true);
      component.bspList$.subscribe(options => (bspDropdownOptions = options));

      discardPeriodicTasks();

      // The user does not have any permission for Malta BSP
      expect(bspDropdownOptions).toEqual([
        {
          value: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' },
          label: 'ES - SPAIN'
        }
      ]);
    }));

    describe('provider dropdown options', () => {
      it('should initialize provider dropdown options with the current selected BSPs', fakeAsync(() => {
        let providerDropdownOptions: DropdownOption<TipProvider>[];

        const bspsMock: BspDto[] = [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }];
        const providersMock: TipProvider[] = [
          {
            id: 1,
            code: 'PROV1',
            name: 'provider1',
            bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
          }
        ];

        const service = TestBed.inject(TipProviderService);
        spyOn(service, 'getProviders').and.returnValue(of(providersMock));
        spyOn(service, 'getProvidersDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.providerList$.subscribe(options => (providerDropdownOptions = options));

        component['bspControl'].patchValue(bspsMock);
        tick();

        discardPeriodicTasks();

        expect(service.getProvidersDropdownOptions).toHaveBeenCalledWith(bspsMock);
        expect(providerDropdownOptions).toEqual([
          {
            value: {
              id: 1,
              code: 'PROV1',
              name: 'provider1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            },
            label: 'ES - PROV1 / provider1'
          }
        ]);
      }));

      it('should update provider control with the options retrieved', fakeAsync(() => {
        const providerOptionsMock: DropdownOption<TipProvider>[] = [
          {
            value: { id: 1, code: 'PROV1', name: 'provider1', bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' } },
            label: ''
          }
        ];

        const service = TestBed.inject(TipProviderService);
        spyOn(service, 'getProvidersDropdownOptions').and.returnValue(of(providerOptionsMock));

        spyOn<any>(component, 'updateOptionsControl').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.providerList$.subscribe();
        tick();

        discardPeriodicTasks();

        expect(component['updateOptionsControl']).toHaveBeenCalledWith(
          component.searchForm.controls.providers,
          providerOptionsMock
        );
      }));
    });

    describe('product dropdown options', () => {
      it('should initialize product dropdown options with the current selected BSPs', fakeAsync(() => {
        let productDropdownOptions: DropdownOption<TipProduct>[];

        const bspsMock: BspDto[] = [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }];
        const productsMock: TipProduct[] = [
          {
            id: 1,
            code: 'PROD1',
            name: 'product1',
            bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
          }
        ] as TipProduct[];

        const productService = TestBed.inject(TipProductService);
        spyOn(productService, 'getProducts').and.returnValue(of({ records: productsMock }));
        spyOn(productService, 'getProductsDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.productList$.subscribe(options => (productDropdownOptions = options));

        component['bspControl'].patchValue(bspsMock);
        tick();

        discardPeriodicTasks();

        expect(productService.getProductsDropdownOptions).toHaveBeenCalledWith(bspsMock);
        expect(productDropdownOptions).toEqual([
          {
            value: {
              id: 1,
              code: 'PROD1',
              name: 'product1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            } as TipProduct,
            label: 'ES - PROD1 / product1'
          }
        ]);
      }));

      it('should update product control with the options retrieved', fakeAsync(() => {
        const productOptionsMock: DropdownOption<TipProduct>[] = [
          {
            value: {
              id: 1,
              code: 'PROD1',
              name: 'product1',
              bsp: { id: 1, isoCountryCode: 'ES', name: 'SPAIN' }
            } as TipProduct,
            label: ''
          }
        ];

        const productService = TestBed.inject(TipProductService);
        spyOn(productService, 'getProductsDropdownOptions').and.returnValue(of(productOptionsMock));

        spyOn<any>(component, 'updateOptionsControl').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.productList$.subscribe();
        tick();

        discardPeriodicTasks();

        expect(component['updateOptionsControl']).toHaveBeenCalledWith(
          component.searchForm.controls.products,
          productOptionsMock
        );
      }));
    });

    describe('currency dropdown options', () => {
      it('should initialize currency dropdown options with the current selected BSPs', fakeAsync(() => {
        let currencyDropdownOptions: DropdownOption<Currency>[];

        const bspsMock: BspDto[] = [{ id: 1, isoCountryCode: 'ES', name: 'SPAIN' }];
        const currenciesMock: Currency[] = [{ id: 1, code: 'EUR', decimals: 2 }];

        const service = TestBed.inject(CurrencyDictionaryService);
        spyOn<any>(service, 'getFilteredCurrencies').and.returnValue(of(currenciesMock));
        spyOn(service, 'getFilteredDropdownOptions').and.callThrough();

        fixture.detectChanges();

        component.onFilterButtonClicked(true);
        component.currencyList$.subscribe(options => (currencyDropdownOptions = options));

        component['bspControl'].patchValue(bspsMock);
        tick();

        discardPeriodicTasks();

        expect(service.getFilteredDropdownOptions).toHaveBeenCalledWith({ bspId: [1] });
        expect(currencyDropdownOptions).toEqual([
          {
            value: { id: 1, code: 'EUR', decimals: 2 },
            label: 'EUR'
          }
        ]);
      }));
    });
  });

  it('`updateOptionsControl` - should update the corresponding options control', () => {
    const providerOptionsMock: DropdownOption<TipProvider>[] = [
      {
        value: { id: 1, code: 'PROV1', name: 'provider1' },
        label: ''
      }
    ];

    fixture.detectChanges();

    // Testing with provider control
    component.searchForm.patchValue({
      providers: [
        { id: 1, code: 'PROV1', name: 'provider1' },
        { id: 2, code: 'PROV2', name: 'provider2' }
      ]
    });
    component['updateOptionsControl'](component.searchForm.controls.providers, providerOptionsMock);

    expect(component.searchForm.value.providers).toEqual([{ id: 1, code: 'PROV1', name: 'provider1' }]);
  });

  describe('detail navigation', () => {
    it('should navigate with the corresponding provider ID on provider name click', () => {
      component.onRowLinkClick({
        propertyValue: '', // It does not matter
        propertyName: 'provider.name',
        row: { provider: { id: 23 } } as TipProduct
      });

      expect(routerSpy.navigate).toHaveBeenCalledWith(jasmine.arrayContaining([23]));
    });

    it('should navigate with the corresponding product ID on product name click', () => {
      component.onRowLinkClick({
        propertyValue: '', // It does not matter
        propertyName: 'name',
        row: { id: 14 } as TipProduct
      });

      expect(routerSpy.navigate).toHaveBeenCalledWith(jasmine.arrayContaining([14]));
    });
  });

  describe('action click', () => {
    const productMock: TipProduct = {
      id: 1
    } as TipProduct;

    it('should navigate to country level consents on `BspLevelConsentManagement` action with the right filter', () => {
      const actionMock: GridTableAction = {
        actionType: GridTableActionType.BspLevelConsentManagement,
        methodName: ''
      };

      const mockStore = TestBed.inject(MockStore);
      spyOn(mockStore, 'dispatch').and.callThrough();

      component.onActionClick({ action: actionMock, row: productMock });

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterChange({
          viewListId: jasmine.anything() as any,
          filter: { product: [{ id: 1 }], level: TipProductConsentLevel.Country }
        })
      );

      expect(routerSpy.navigate).toHaveBeenCalledWith([`${ROUTES.TIP_PRODUCTS.url}/consents/list`]);
    });

    it('should navigate to agent level consents on `AgentLevelConsentManagement` action with the right filter', () => {
      const actionMock: GridTableAction = {
        actionType: GridTableActionType.AgentLevelConsentManagement,
        methodName: ''
      };

      const mockStore = TestBed.inject(MockStore);
      spyOn(mockStore, 'dispatch').and.callThrough();

      component.onActionClick({ action: actionMock, row: productMock });

      expect(mockStore.dispatch).toHaveBeenCalledWith(
        new ViewListActions.FilterChange({
          viewListId: jasmine.anything() as any,
          filter: { product: [{ id: 1 }], level: TipProductConsentLevel.Agent }
        })
      );

      expect(routerSpy.navigate).toHaveBeenCalledWith([`${ROUTES.TIP_PRODUCTS.url}/consents/list`]);
    });
  });

  it('should open download dialog on click', fakeAsync(() => {
    component.onDownload();

    expect(dialogServiceSpy.open).toHaveBeenCalled();
  }));
});
