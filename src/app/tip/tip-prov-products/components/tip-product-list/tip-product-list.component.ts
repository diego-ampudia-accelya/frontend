import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable } from 'rxjs';
import { first, map, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { getUser, getUserType } from '~app/auth/selectors/auth.selectors';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { ViewListActions } from '~app/core/actions';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { ListSubtabs } from '~app/shared/base/list-subtabs/components/list-subtabs.class';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DialogService, DownloadFileComponent, FooterButton } from '~app/shared/components';
import { DataQuery } from '~app/shared/components/list-view';
import { Permissions, ROUTES } from '~app/shared/constants';
import { ArrayHelper, FormUtil, toValueLabelObjectBsp } from '~app/shared/helpers';
import { createTabViewListId } from '~app/shared/helpers/create-tab-view-list-id.helper';
import { DropdownOption, GridColumn, GridColumnMultipleLinkClick } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { Currency } from '~app/shared/models/currency.model';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { User, UserType } from '~app/shared/models/user.model';
import { CurrencyDictionaryService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';
import { listKeys, listSelectors, State } from '~app/tip/reducers';
import { CostRecoveryType } from '~app/tip/shared/models/tip-shared.model';
import { getReadTipProductListPermissions } from '../../config/tip-product-permissions.config';
import { TipProductCategory } from '../../models/tip-product-category.model';
import { TipProductConsentFilter } from '../../models/tip-product-consent-filter.model';
import { TipProductFilter } from '../../models/tip-product-filter.model';
import { TipProductFormOfPayment } from '../../models/tip-product-form-of-payment.model';
import { CountryLevelConsentType, TipProduct, TipProductConsentLevel, TipStatus } from '../../models/tip-product.model';
import { TipProvider } from '../../models/tip-provider.model';
import { TipProductFormatter } from '../../services/tip-product-formatter.service';
import { TipProductService } from '../../services/tip-product.service';
import { TipProviderService } from '../../services/tip-provider.service';

@Component({
  selector: 'bspl-tip-product-list',
  templateUrl: './tip-product-list.component.html',
  styleUrls: ['./tip-product-list.component.scss'],
  providers: [TipProductFormatter]
})
export class TipProductListComponent extends ListSubtabs<TipProduct, TipProductFilter> implements OnInit {
  public columns: Array<GridColumn>;
  public downloadQuery: DataQuery<TipProductFilter>;
  public actions: Array<any>;
  public searchForm: FormGroup;

  public statusList: Array<TipStatus>;
  public bspList$: Observable<DropdownOption<BspDto>[]>;
  public airlineIsMulticountry: boolean;
  public providerList$: Observable<DropdownOption<TipProvider>[]>;
  public productList$: Observable<DropdownOption<TipProduct>[]>;
  public currencyList$: Observable<DropdownOption<Currency>[]>;
  public countryLevelConsentTypeList: DropdownOption<CountryLevelConsentType>[];
  public costRecoveryTypeList: DropdownOption<CostRecoveryType>[];
  public isAirlineUser: boolean;
  public isAgentUser: boolean;
  public isIataUser: boolean;
  public infoMessage: string;
  public titleLabel: string;

  public hasLeanPermission: boolean;

  public categoriesList: DropdownOption<TipProductCategory>[] = this.getEnumOptions(
    TipProductCategory,
    'category'
  ) as DropdownOption<TipProductCategory>[];
  public formOfPaymentsList: DropdownOption<TipProductFormOfPayment>[] = this.getEnumOptions(
    TipProductFormOfPayment,
    'formOfPayment'
  ) as DropdownOption<TipProductFormOfPayment>[];

  protected initialFilters: Partial<TipProductFilter>;

  private bspControl: FormControl;
  private formUtil: FormUtil;

  constructor(
    public displayFormatter: TipProductFormatter,
    protected store: Store<AppState>,
    protected tipProductService: TipProductService,
    protected tipProviderService: TipProviderService,
    protected bspsDictionaryService: BspsDictionaryService,
    protected currencyDictionaryService: CurrencyDictionaryService,
    protected actions$: Actions,
    protected translationService: L10nTranslationService,
    protected permissionsService: PermissionsService,
    private formBuilder: FormBuilder,
    private dialogService: DialogService,
    private router: Router
  ) {
    super(store, tipProductService, actions$, translationService);
    this.initializeLoggedUserType();
    this.initializeDefaultFilters();
  }

  private get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  private get loggedUserType$(): Observable<UserType> {
    return this.store.pipe(select(getUserType), first());
  }

  public ngOnInit(): void {
    super.ngOnInit();

    this.formUtil = new FormUtil(this.formBuilder);
    this.searchForm = this.buildForm();
    this.initializePermissions();
    this.initializeGridTable();
    this.initializeInfoMessage();
    this.initializeTitleLabel();
    this.query$.pipe(takeUntil(this.destroy$)).subscribe(query => (this.downloadQuery = query));
  }

  public onActionClick({ action, row }) {
    let filter: Partial<TipProductConsentFilter>;
    switch (action.actionType) {
      case GridTableActionType.BspLevelConsentManagement:
        filter = { product: [row], level: TipProductConsentLevel.Country };
        break;
      case GridTableActionType.AgentLevelConsentManagement:
        filter = { product: [row], level: TipProductConsentLevel.Agent };
        break;
    }

    if (filter) {
      this.store.dispatch(
        new ViewListActions.FilterChange({
          viewListId: createTabViewListId(ROUTES.TIP_PRODUCTS.tabLabel, 'consent'),
          filter
        })
      );

      this.router.navigate([`${ROUTES.TIP_PRODUCTS.url}/consents/list`]);
    }
  }

  public onFilterButtonClicked(isSearchPanelVisible: boolean) {
    if (isSearchPanelVisible) {
      this.initializeFilterDropdowns();
    }
  }

  public onRowLinkClick(event: GridColumnMultipleLinkClick<TipProduct>) {
    if (event.propertyName === 'provider.name') {
      this.router.navigate([ROUTES.TIP_PROVIDER_DETAIL.url, event.row.provider.id]);
    }

    if (event.propertyName === 'name') {
      this.router.navigate([ROUTES.TIP_PRODUCT_DETAIL.url, event.row.id]);
    }
  }

  public onDownload(): void {
    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.translationService.translate('common.download'),
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery
      },
      apiService: this.tipProductService
    });
  }

  public getCountryLevelConsentTypeTranslation(type: CountryLevelConsentType): string {
    return type && this.translationService.translate(`TIP.products.levelConsentType.${type}`);
  }

  protected getListSelector(): MemoizedSelector<
    State,
    ListSubtabsState<TipProduct, TipProductFilter>,
    DefaultProjectorFn<ListSubtabsState<TipProduct, TipProductFilter>>
  > {
    return listSelectors[MasterDataType.TipProduct];
  }

  protected getListKey(): string {
    return listKeys[MasterDataType.TipProduct];
  }

  private initializeDefaultFilters(): void {
    if (!this.isIataUser) {
      this.initialFilters = { status: TipStatus.Active };
    }
  }

  private initializePermissions(): void {
    this.hasLeanPermission = this.permissionsService.hasPermission(Permissions.lean);
  }

  private initializeInfoMessage() {
    this.infoMessage = this.isAgentUser ? 'TIP.products.alertInfo' : null;
  }

  private initializeTitleLabel() {
    this.titleLabel = this.isAirlineUser ? 'TIP.products.listTitleAirline' : 'TIP.products.listTitleAgent';
  }

  private initializeGridTable() {
    this.columns = this.buildColumns();
    this.actions = this.initializeActions();
  }

  private initializeActions(): Array<any> {
    return this.isAirlineUser
      ? [
          { action: GridTableActionType.BspLevelConsentManagement },
          { action: GridTableActionType.AgentLevelConsentManagement }
        ]
      : [];
  }

  private initializeLoggedUserType(): void {
    this.loggedUserType$.subscribe(userType => {
      this.isAirlineUser = userType === UserType.AIRLINE;
      this.isAgentUser = userType === UserType.AGENT;
      this.isIataUser = userType === UserType.IATA;
    });
  }

  private buildColumns(): Array<GridColumn> {
    const columns: Array<GridColumn> = [
      {
        name: 'TIP.products.columns.bsp',
        prop: 'bsp.isoCountryCode',
        flexGrow: 0.8,
        hidden: this.isAgentUser
      },
      {
        name: 'TIP.products.rowDetails.providerCode',
        prop: 'provider.code',
        flexGrow: 2
      },
      {
        name: 'TIP.products.columns.providerName',
        prop: 'provider.name',
        flexGrow: 3,
        cellTemplate: this.isIataUser ? null : 'multipleLinkCellTmpl'
      },
      {
        name: 'TIP.products.rowDetails.productCode',
        prop: 'code',
        flexGrow: 2
      },
      {
        name: 'TIP.products.columns.productName',
        prop: 'name',
        flexGrow: 3,
        cellTemplate: this.isAirlineUser ? 'multipleLinkCellTmpl' : null
      },
      {
        name: 'TIP.products.columns.category',
        prop: 'category',
        flexGrow: 1.5,
        hidden: this.isAgentUser
      },
      {
        name: 'TIP.products.columns.issuerName',
        prop: 'issuerName',
        flexGrow: 3,
        hidden: this.isAirlineUser || this.isIataUser
      },
      {
        name: 'TIP.products.columns.paymentNetwork',
        prop: 'paymentNetwork',
        flexGrow: 1.5,
        hidden: this.isAirlineUser || this.isIataUser
      },
      {
        name: 'TIP.products.columns.formOfPayment',
        prop: 'formOfPayment',
        flexGrow: 1.5
      },
      {
        name: 'TIP.products.columns.bspConsent',
        prop: 'countryLevelConsent.type',
        flexGrow: 1.5,
        hidden: this.isAgentUser || this.isIataUser
      },
      {
        name: 'TIP.products.columns.effectiveFrom',
        prop: 'effectiveFrom',
        flexGrow: 2,
        cellTemplate: 'dayMonthYearCellTmpl',
        hidden: this.isAgentUser
      },
      {
        name: 'TIP.products.columns.effectiveTo',
        prop: 'effectiveTo',
        flexGrow: 2,
        cellTemplate: 'dayMonthYearCellTmpl',
        hidden: this.isAgentUser
      },
      {
        name: 'TIP.products.columns.agentUrl',
        prop: 'agentUrl',
        flexGrow: 3,
        hidden: this.isAirlineUser || this.isIataUser
      },
      {
        name: 'TIP.products.columns.status',
        prop: 'status',
        flexGrow: 2,
        hidden: this.isIataUser
      }
    ];

    const typeColumn = columns.find(column => column.prop === 'countryLevelConsent.type');
    typeColumn.pipe = {
      transform: type => this.getCountryLevelConsentTypeTranslation(type)
    };

    return columns;
  }

  private buildForm(): FormGroup {
    this.bspControl = this.formBuilder.control(null);

    return this.formUtil.createGroup<TipProductFilter>({
      status: [],
      bsps: this.bspControl,
      providers: [],
      products: [],
      category: [],
      paymentNetwork: [],
      countryLevelConsentType: [],
      countryLevelConsentCostRecoveryType: [],
      countryLevelConsentCostRecoveryCurrency: [],
      effectiveFrom: [],
      effectiveTo: [],
      issuerName: [],
      formOfPayment: []
    });
  }

  private initializeFilterDropdowns(): void {
    const providersControl = FormUtil.get<TipProductFilter>(this.searchForm, 'providers');
    const productsControl = FormUtil.get<TipProductFilter>(this.searchForm, 'products');
    const consentCostRecoveryCurrencyControl = FormUtil.get<TipProductFilter>(
      this.searchForm,
      'countryLevelConsentCostRecoveryCurrency'
    );

    this.bspList$ = this.loggedUser$.pipe(
      switchMap(({ bspPermissions }) =>
        this.bspsDictionaryService.getAllBspsByPermissions(bspPermissions, getReadTipProductListPermissions())
      ),
      map(bspList => bspList.map(toValueLabelObjectBsp))
    );

    this.statusList = ArrayHelper.toDropdownOptions(Object.values(TipStatus));
    this.providerList$ = this.getCurrentBsps$().pipe(
      switchMap(bsp => this.tipProviderService.getProvidersDropdownOptions(bsp)),
      tap(options => this.updateOptionsControl(providersControl, options))
    );
    this.productList$ = this.getCurrentBsps$().pipe(
      switchMap(bsp => this.tipProductService.getProductsDropdownOptions(bsp)),
      tap(options => this.updateOptionsControl(productsControl, options))
    );
    this.currencyList$ = this.getCurrentBsps$().pipe(
      switchMap(bsps =>
        this.currencyDictionaryService.getFilteredDropdownOptions({
          ...(bsps?.length && { bspId: bsps.map(({ id }) => id) })
        })
      ),
      tap(options => this.updateOptionsControl(consentCostRecoveryCurrencyControl, options))
    );
    this.countryLevelConsentTypeList = Object.values(CountryLevelConsentType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.levelConsentType.${value}`)
    }));
    this.costRecoveryTypeList = Object.values(CostRecoveryType).map(value => ({
      value,
      label: this.translationService.translate(`TIP.products.costRecoveryType.${value}`)
    }));
  }

  private updateOptionsControl(control: AbstractControl, options: DropdownOption<{ id: number }>[]) {
    if (control.value?.length) {
      const optionIds = options.map(option => option.value.id);

      control.setValue(control.value.filter(selectedOption => optionIds.includes(selectedOption.id)));
    }
  }

  private getCurrentBsps$(): Observable<BspDto[]> {
    return this.query$.pipe(
      first(),
      map(query => query.filterBy.bsps || null),
      switchMap(filterBsp => this.bspControl.valueChanges.pipe(startWith(filterBsp)))
    );
  }

  private getEnumOptions(
    options: any,
    filterName: string
  ): DropdownOption<TipProductCategory | TipProductFormOfPayment>[] {
    return Object.keys(options).map((key: string) => ({
      value: options[key],
      label: this.translationService.translate(`TIP.products.filters.options.${filterName}.${key}`)
    }));
  }
}
