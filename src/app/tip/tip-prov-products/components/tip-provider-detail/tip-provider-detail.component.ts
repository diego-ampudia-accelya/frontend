import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProviderContact, TipProvider } from '../../models/tip-provider.model';

@Component({
  selector: 'bspl-tip-provider-detail',
  templateUrl: './tip-provider-detail.component.html',
  styleUrls: ['./tip-provider-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipProviderDetailComponent implements OnInit {
  public provider: TipProvider;

  constructor(private activatedRoute: ActivatedRoute) {}

  public ngOnInit(): void {
    const data = this.activatedRoute.snapshot.data;

    this.provider = data.item;
  }

  public getContactName(contact: ProviderContact): string {
    return `${contact.firstName} ${contact.lastName}`;
  }
}
