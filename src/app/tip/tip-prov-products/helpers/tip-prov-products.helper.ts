import { MenuTab, MenuTabLabelParameterized } from '~app/shared/models/menu-tab.model';

export function getTipProductTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;

  return { label: tab.tabLabel as string, params: { productName: routeData.item.name } };
}

export function getTipProviderTabLabel(routeData: any): MenuTabLabelParameterized {
  const tab: MenuTab = routeData.tab;

  return { label: tab.tabLabel as string, params: { providerName: routeData.item.name } };
}
