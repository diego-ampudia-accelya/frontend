import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTES } from '~app/shared/constants';
import { getTipCardAirlineTabLabel } from './shared/helpers/tip-tabs.helper';
import {
  getReadTipCardConsentPermissions,
  getReadTipCardListPermissions
} from './tip-cards/config/tip-card-permissions.config';
import { TipCardConsentAirlineDetailResolver } from './tip-cards/resolvers/tip-card-consent-airline-detail.resolver';
import { TipCardConsentAirlineDetailComponent } from './tip-cards/tip-card-consent-airline-detail/tip-card-consent-airline-detail.component';
import { TipCardConsentListComponent } from './tip-cards/tip-card-consent-list/tip-card-consent-list.component';
import { TipCardListComponent } from './tip-cards/tip-card-list/tip-card-list.component';
import { TipCardsComponent } from './tip-cards/tip-cards.component';
import { TipGlobalFileType } from './tip-global-files/model/tip-global-files.model';
import { TipGlobalFilesGuard } from './tip-global-files/services/tip-global-files.guard';
import { TipProductConsentListComponent } from './tip-prov-products/components/tip-product-consents-list/tip-product-consent-list.component';
import { TipProductDetailComponent } from './tip-prov-products/components/tip-product-detail/tip-product-detail.component';
import { TipProductListComponent } from './tip-prov-products/components/tip-product-list/tip-product-list.component';
import { TipProductsComponent } from './tip-prov-products/components/tip-products.component';
import { TipProviderDetailComponent } from './tip-prov-products/components/tip-provider-detail/tip-provider-detail.component';
import {
  getReadTipProductConsentPermissions,
  getReadTipProductListPermissions
} from './tip-prov-products/config/tip-product-permissions.config';
import { getTipProductTabLabel, getTipProviderTabLabel } from './tip-prov-products/helpers/tip-prov-products.helper';
import { TipProductResolver } from './tip-prov-products/resolvers/tip-product-resolver.service';
import { TipProviderResolver } from './tip-prov-products/resolvers/tip-provider-resolver.service';
import { TipReportsConfigurationListComponent } from './tip-reports-configuration/tip-reports-configuration-list/tip-reports-configuration-list.component';

const tipCardsChildren: Routes = [
  {
    path: 'list',
    component: TipCardListComponent,
    data: {
      title: 'TIP.cards.list.tabLabel',
      requiredPermissions: getReadTipCardListPermissions()
    }
  },
  {
    path: 'consent/list',
    component: TipCardConsentListComponent,
    data: {
      title: 'TIP.cards.consent.tabLabel',
      requiredPermissions: getReadTipCardConsentPermissions()
    }
  },
  { path: '', redirectTo: 'list', pathMatch: 'full' }
];

const tipProductsChildren: Routes = [
  {
    path: 'list',
    component: TipProductListComponent,
    data: { title: 'TIP.products.listTabLabel', requiredPermissions: getReadTipProductListPermissions() }
  },
  {
    path: 'consents/list',
    component: TipProductConsentListComponent,
    data: { title: 'TIP.productConsents.listTabLabel', requiredPermissions: getReadTipProductConsentPermissions() }
  },
  { path: '', redirectTo: 'list', pathMatch: 'full' }
];

const routes: Routes = [
  { path: '', redirectTo: ROUTES.TIP_PRODUCTS.path, pathMatch: 'full' },
  {
    path: ROUTES.TIP_PRODUCTS.path,
    component: TipProductsComponent,
    data: { tab: ROUTES.TIP_PRODUCTS },
    children: tipProductsChildren
  },
  {
    path: ROUTES.TIP_PROVIDERS_AND_PRODUCTS.path,
    data: { tab: ROUTES.TIP_PROVIDERS_AND_PRODUCTS },
    component: TipProductListComponent
  },
  {
    path: ROUTES.TIP_CARDS_ENROLLED_ATMS.path,
    data: { tab: ROUTES.TIP_CARDS_ENROLLED_ATMS },
    component: TipCardListComponent
  },
  {
    path: ROUTES.TIP_CARDS.path,
    component: TipCardsComponent,
    data: { tab: ROUTES.TIP_CARDS },
    children: tipCardsChildren
  },
  {
    path: ROUTES.TIP_PRODUCT_DETAIL.path,
    component: TipProductDetailComponent,
    data: {
      tab: { ...ROUTES.TIP_PRODUCT_DETAIL, getTabLabel: getTipProductTabLabel }
    },
    resolve: {
      item: TipProductResolver
    }
  },
  {
    path: ROUTES.TIP_PROVIDER_DETAIL.path,
    component: TipProviderDetailComponent,
    data: {
      tab: { ...ROUTES.TIP_PROVIDER_DETAIL, getTabLabel: getTipProviderTabLabel }
    },
    resolve: {
      item: TipProviderResolver
    }
  },
  {
    path: ROUTES.TIP_CARDS_AIRLINE_DETAIL.path,
    component: TipCardConsentAirlineDetailComponent,
    data: {
      tab: { ...ROUTES.TIP_CARDS_AIRLINE_DETAIL, getTabLabel: getTipCardAirlineTabLabel }
    },
    resolve: {
      item: TipCardConsentAirlineDetailResolver
    }
  },
  {
    path: ROUTES.TIP_GLOBAL_FILES_CONSENT.path,
    canActivate: [TipGlobalFilesGuard],
    data: {
      tipGlobalFilesType: TipGlobalFileType.Consent
    }
  },
  {
    path: ROUTES.TIP_GLOBAL_FILES_REPORT.path,
    canActivate: [TipGlobalFilesGuard],
    data: {
      tipGlobalFilesType: TipGlobalFileType.Report
    }
  },
  {
    path: ROUTES.TIP_REPORTS_CONFIGURATION.path,
    component: TipReportsConfigurationListComponent,
    data: { tab: ROUTES.TIP_REPORTS_CONFIGURATION }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipRoutingModule {}
