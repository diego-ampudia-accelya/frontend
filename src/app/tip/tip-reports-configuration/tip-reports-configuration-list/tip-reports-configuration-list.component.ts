import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AdmAcmService } from '~app/adm-acm/services/adm-acm.service';
import {
  TipReport,
  TipReportChannel,
  TipReportFormat,
  TipReportFrequency,
  TipReportOption,
  TipReportViewModel
} from '~app/master-data/configuration/tip/tip-reports/models/tip-report.model';
import { TipReportService } from '~app/master-data/configuration/tip/tip-reports/services/tip-report.service';
import {
  ButtonDesign,
  ConfirmationDialogComponent,
  DialogConfig,
  DialogService,
  FooterButton
} from '~app/shared/components';
import {
  EditableListColumn,
  EditableListColumnControlType
} from '~app/shared/components/editable-list/models/editable-list-column.model';
import { QUERYABLE, QueryableDataSource } from '~app/shared/components/list-view';
import { ROUTES } from '~app/shared/constants';
import { ArrayHelper, FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { NotificationService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-tip-reports-configuration-list',
  templateUrl: './tip-reports-configuration-list.component.html',
  styleUrls: ['./tip-reports-configuration-list.component.scss'],
  providers: [QueryableDataSource, { provide: QUERYABLE, useExisting: TipReportService }]
})
export class TipReportsConfigurationListComponent implements OnInit, OnDestroy {
  public columns: EditableListColumn[];
  public form: FormGroup;
  public translationPrefix = 'menu.masterData.configuration.tipSettings.tipReports';
  public buttonDesign = ButtonDesign;
  public airlineDropdownOptions$ = this.admAcmService.getBspAirlines();

  private formFactory: FormUtil;
  private _optionControlLockedSubject: Subject<boolean> = new BehaviorSubject(false);
  private optionControlLocked$: Observable<boolean> = this._optionControlLockedSubject.asObservable();
  private _formatOptionsSubject: Subject<DropdownOption<TipReportFormat>[]> = new BehaviorSubject([]);
  private formatOptions$: Observable<DropdownOption<TipReportFormat>[]> = this._formatOptionsSubject.asObservable();
  private _frequencyOptionsSubject: Subject<DropdownOption<TipReportFrequency>[]> = new BehaviorSubject([]);
  private frequencyOptions$: Observable<DropdownOption<TipReportFrequency>[]> =
    this._frequencyOptionsSubject.asObservable();
  private _channelOptionsSubject: Subject<DropdownOption<TipReportChannel>[]> = new BehaviorSubject([]);
  private channelOptions$: Observable<DropdownOption<TipReportChannel>[]> = this._channelOptionsSubject.asObservable();
  private bspOptions$ = this.bspDictionary.getAllBspDropdownOptions();
  private _isThresholdLockedSubject: Subject<boolean> = new BehaviorSubject(false);
  private isThresholdLocked$: Observable<boolean> = this._isThresholdLockedSubject.asObservable();
  private bspOptions: DropdownOption<BspDto>[];
  private destroy$ = new Subject();

  constructor(
    public dataSource: QueryableDataSource<TipReport>,
    public dataService: TipReportService,
    private formBuilder: FormBuilder,
    private bspDictionary: BspsDictionaryService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService,
    private dialogService: DialogService,
    private router: Router,
    private admAcmService: AdmAcmService
  ) {
    this.formFactory = new FormUtil(this.formBuilder);
  }

  public ngOnInit(): void {
    this.columns = this.buildColumns();
    this.initializeForm();
    // Load data
    this.dataSource.get();
    this.getBspOptions();
  }

  public onCreateReport(report: TipReportViewModel): void {
    this.dataService
      .create(report)
      .pipe(
        finalize(() => {
          this.initializeForm();
          this.dataSource.get();
        })
      )
      .subscribe(() => {
        this.notificationService.showSuccess(
          this.translationService.translate(`${this.translationPrefix}.actions.create.successMessage`, {
            report: report.option.reportDescription
          })
        );
      });
  }

  public onUpdateReport(report: TipReport & TipReportViewModel): void {
    this.dataService
      .update(report)
      .pipe(
        finalize(() => {
          // Option selection is locked on edit action, so we revert it here
          this._optionControlLockedSubject.next(false);

          this.initializeForm();
          this.dataSource.get();
        })
      )
      .subscribe(() => {
        this.notificationService.showSuccess(
          this.translationService.translate(`${this.translationPrefix}.actions.update.successMessage`, {
            report: report.option.reportDescription
          })
        );
      });
  }

  public onDeleteReport(report: TipReport): void {
    this.confirmDelete(report)
      .pipe(
        filter(isConfirmed => isConfirmed),
        switchMap(() => this.dataService.delete(report))
      )
      .subscribe(() => {
        this.dataSource.get();

        this.notificationService.showSuccess(
          this.translationService.translate(`${this.translationPrefix}.actions.delete.successMessage`, {
            report: report.reportDescription
          })
        );
      });
  }

  public onCancelReport(): void {
    this.initializeForm();
  }

  /** Manages form controls on editable item `create` and `edit` action */
  public onEditModeChange(inCreateEditMode: boolean) {
    if (!inCreateEditMode) {
      return;
    }

    const optionControl = FormUtil.get(this.form, 'option');
    const thresholdControl = FormUtil.get(this.form, 'threshold');

    // If option control has value editable item is in `edit` mode, if not editable item is in `create` mode
    if (optionControl.value) {
      if (thresholdControl.value) {
        this._isThresholdLockedSubject.next(false);
      } else {
        this._isThresholdLockedSubject.next(true);
      }

      // Load dropdown options
      this._formatOptionsSubject.next(ArrayHelper.toDropdownOptions(optionControl.value.formatValues));
      this._frequencyOptionsSubject.next(ArrayHelper.toDropdownOptions(optionControl.value.frequencyValues));
      this._channelOptionsSubject.next(ArrayHelper.toDropdownOptions(optionControl.value.channelValues));
      this.setBspFormValue(optionControl.value.bspCode);

      // Option selection is locked on edit action
      this._optionControlLockedSubject.next(true);
    } else {
      this._optionControlLockedSubject.next(false);
      const formatControl = FormUtil.get<TipReportViewModel>(this.form, 'format');
      const frequencyControl = FormUtil.get<TipReportViewModel>(this.form, 'frequency');
      const channelControl = FormUtil.get<TipReportViewModel>(this.form, 'channel');

      // All these controls are disabled on create action
      formatControl.disable();
      frequencyControl.disable();
      channelControl.disable();
    }
  }

  public checkReports(): void {
    this.router.navigate([ROUTES.REPORTS.url]);
  }
  public ngOnDestroy() {
    this.destroy$.next();
  }

  private getBspOptions(): void {
    this.bspOptions$.pipe(takeUntil(this.destroy$)).subscribe(options => (this.bspOptions = options));
  }

  private setBspFormValue(bspCode: string): void {
    const bsp = this.bspOptions.find(option => option.value.isoCountryCode === bspCode);
    if (bsp) {
      const bspValue = {
        id: bsp.value.id,
        isoCountryCode: bsp.value.isoCountryCode,
        name: ''
      };
      this.form.get('bsp').setValue(bspValue, { emitEvent: false });
    }
  }

  private buildColumns(): EditableListColumn[] {
    const translationColumnPrefix = `${this.translationPrefix}.columns`;

    return [
      {
        name: `${translationColumnPrefix}.description.name`,
        prop: 'reportDescription',
        width: 25,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'option',
          placeholder: `${translationColumnPrefix}.description.placeholder`,
          locked: this.optionControlLocked$,
          options: this.dataService.findOptions()
        }
      },
      {
        name: `${translationColumnPrefix}.format.name`,
        prop: 'format',
        width: 15,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'format',
          placeholder: `${translationColumnPrefix}.format.placeholder`,
          tooltip: `${translationColumnPrefix}.format.tooltip`,
          options: this.formatOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.frequency.name`,
        prop: 'frequency',
        width: 15,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'frequency',
          placeholder: `${translationColumnPrefix}.frequency.placeholder`,
          tooltip: `${translationColumnPrefix}.frequency.tooltip`,
          options: this.frequencyOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.channel.name`,
        prop: 'channel',
        width: 15,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'channel',
          placeholder: `${translationColumnPrefix}.channel.placeholder`,
          tooltip: `${translationColumnPrefix}.channel.tooltip`,
          options: this.channelOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.isoc.name`,
        prop: 'bspCode',
        width: 10,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'bsp',
          placeholder: `${translationColumnPrefix}.isoc.placeholder`,
          options: this.bspOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.user.name`,
        prop: 'user',
        width: 10,
        control: {
          type: EditableListColumnControlType.Select,
          name: 'user',
          placeholder: `${translationColumnPrefix}.user.placeholder`,
          options: this.airlineDropdownOptions$
        }
      },
      {
        name: `${translationColumnPrefix}.threshold.name`,
        prop: 'threshold',
        width: 10,
        control: {
          type: EditableListColumnControlType.Text,
          name: 'threshold',
          placeholder: `${translationColumnPrefix}.threshold.placeholder`,
          locked: this.isThresholdLocked$
        }
      }
    ];
  }

  private initializeForm(): void {
    this.form = this.formFactory.createGroup<TipReportViewModel>({
      option: [null, Validators.required],
      format: [null, Validators.required],
      frequency: [null, Validators.required],
      channel: [null, Validators.required],
      bsp: [null, Validators.required],
      user: [null, Validators.required],
      threshold: [null]
    });

    this.initializeFormListeners();
  }

  private initializeFormListeners(): void {
    const optionControl = FormUtil.get<TipReportViewModel>(this.form, 'option');
    const formatControl = FormUtil.get<TipReportViewModel>(this.form, 'format');
    const frequencyControl = FormUtil.get<TipReportViewModel>(this.form, 'frequency');
    const channelControl = FormUtil.get<TipReportViewModel>(this.form, 'channel');

    optionControl.valueChanges.subscribe((value: TipReportOption) => {
      // Load dropdown options
      this._formatOptionsSubject.next(ArrayHelper.toDropdownOptions(value.formatValues));
      this._frequencyOptionsSubject.next(ArrayHelper.toDropdownOptions(value.frequencyValues));
      this._channelOptionsSubject.next(ArrayHelper.toDropdownOptions(value.channelValues));

      if (value.threshold) {
        this._isThresholdLockedSubject.next(false);
      } else {
        this._isThresholdLockedSubject.next(true);
      }

      formatControl.reset();
      frequencyControl.reset();
      channelControl.reset();

      // Enabling form and child controls when a report option is selected
      this.form.enable({ emitEvent: false });
    });
  }

  private confirmDelete(report: TipReport): Observable<boolean> {
    const config: DialogConfig = {
      data: {
        title: `${this.translationPrefix}.actions.delete.dialog.title`,
        question: `${this.translationPrefix}.actions.delete.dialog.question`,
        pills: [{ value: report.reportDescription, tooltip: report.reportDescription }],
        hasCancelButton: true,
        footerButtonsType: FooterButton.Delete
      }
    };

    return this.dialogService.open(ConfirmationDialogComponent, config).pipe(
      map(action => action.clickedBtn === FooterButton.Delete),
      tap(() => this.dialogService.close())
    );
  }
}
