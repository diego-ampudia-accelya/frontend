import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import FileSaver from 'file-saver';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { HelpConfig } from '../models';
import { downloadRequestOptions, formatDownloadResponse } from '~app/shared/helpers';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class HelpService {
  private baseUrl = this.appConfiguration.help.securePath;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private notificationService: NotificationService,
    private translationService: L10nTranslationService
  ) {}

  public getConfig(userType: UserType): Observable<HelpConfig> {
    const url = `${this.baseUrl}/${userType}/config.json`;

    return this.http.get<HelpConfig>(url);
  }

  public getPage(path: string, userType: UserType): Observable<string> {
    const url = `${this.baseUrl}/${userType}${path}`;

    return this.http
      .get(url, { responseType: 'text' })
      .pipe(map(this.applyBaseUrlToImgSrc), map(this.applyBaseUrlToLinkHref));
  }

  public downloadManual(filePath: string, userType: UserType): Observable<{ blob: Blob; fileName: string }> {
    const url = `${this.baseUrl}/${userType}${filePath}`;
    const fileName = filePath.split('/').pop();

    return this.http.get<HttpResponse<ArrayBuffer>>(url, downloadRequestOptions).pipe(
      map(formatDownloadResponse),
      tap(({ blob }) => {
        FileSaver.saveAs(blob, fileName);

        const successMsg = this.translationService.translate('help.downloadPdfSuccessMsg', { fileName });
        this.notificationService.showSuccess(successMsg);
      })
    );
  }

  private applyBaseUrlToImgSrc = (page: string): string => {
    const imageSrcPattern = /src="(.*)"/g;
    const replaceWith = `src="${this.appConfiguration.help.publicPath}/$1"`;

    return page.replace(imageSrcPattern, replaceWith);
  };

  private applyBaseUrlToLinkHref = (page: string): string => {
    const linkHrefPattern = /href="((?!http).*)"/g;
    const replaceWith = `href="${this.appConfiguration.help.publicPath}/$1"`;

    return page.replace(linkHrefPattern, replaceWith);
  };
}
