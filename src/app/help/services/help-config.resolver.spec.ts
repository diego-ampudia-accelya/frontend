import { TestBed } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

import { HelpConfigResolver } from './help-config.resolver';
import { HelpService } from './help.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { createAirlineUser } from '~app/shared/mocks/airline-user';

describe('HelpConfigResolver', () => {
  let resolver: HelpConfigResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HelpConfigResolver,
        provideMockStore({ selectors: [{ selector: getUser, value: createAirlineUser() }] }),
        mockProvider(HelpService)
      ]
    });

    resolver = TestBed.inject(HelpConfigResolver);
  });

  it('should create', () => {
    expect(resolver).toBeTruthy();
  });
});
