import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { HelpConfig } from '../models';
import { HelpService } from './help.service';
import { UserType } from '~app/shared/models/user.model';
import { AppConfigurationService, NotificationService } from '~app/shared/services';

describe('HelpService', () => {
  let service: HelpService;
  let httpClientSpy: SpyObject<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HelpService,
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, { help: { securePath: '/secure', publicPath: '/public' } }),
        mockProvider(NotificationService),
        mockProvider(L10nTranslationService)
      ]
    });

    service = TestBed.inject(HelpService);
    httpClientSpy = TestBed.inject<any>(HttpClient);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch the config using proper url', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(of({} as HelpConfig));

    service.getConfig(UserType.AIRLINE).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/secure/airline/config.json');
  }));

  it('should fetch a page using proper url and config', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(of('text'));

    service.getPage('/path', UserType.AIRLINE).subscribe();

    expect(httpClientSpy.get).toHaveBeenCalledWith('/secure/airline/path', { responseType: 'text' });
  }));
});
