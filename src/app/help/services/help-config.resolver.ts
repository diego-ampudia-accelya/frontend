import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { HelpConfig } from '../models';
import { HelpService } from './help.service';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { User } from '~app/shared/models/user.model';
import { generateNumberedList } from '~app/shared/utils';

@Injectable({
  providedIn: 'root'
})
export class HelpConfigResolver implements Resolve<HelpConfig> {
  private loggedUser$: Observable<User> = this.store.pipe(select(getUser), first());

  constructor(private store: Store<AppState>, private helpService: HelpService) {}

  public resolve(): Observable<HelpConfig> {
    return this.loggedUser$.pipe(
      switchMap(user => this.helpService.getConfig(user.userType)),
      map(config => ({ ...config, menuItems: generateNumberedList(config.menuItems, 'title', 'children') }))
    );
  }
}
