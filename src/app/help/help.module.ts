import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { EmptyHelpPageComponent } from './components/empty-help-page/empty-help-page.component';
import { HelpPageComponent } from './components/help-page/help-page.component';
import { HelpSideMenuComponent } from './components/help-side-menu/help-side-menu.component';
import { HelpComponent } from './components/help/help.component';
import { HelpRoutingModule } from './help-routing.module';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, HelpRoutingModule],
  declarations: [HelpComponent, HelpSideMenuComponent, EmptyHelpPageComponent, HelpPageComponent],
  providers: []
})
export class HelpModule {}
