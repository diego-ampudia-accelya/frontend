import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HelpComponent } from './components/help/help.component';
import { HelpConfigResolver } from './services/help-config.resolver';

const routes: Routes = [
  {
    path: '',
    component: HelpComponent,
    resolve: {
      helpConfig: HelpConfigResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpRoutingModule {}
