import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { cloneDeep } from 'lodash';

import { HelpSideMenuComponent } from './help-side-menu.component';
import { HelpMenuItem } from '~app/help/models';
import { TranslatePipeMock } from '~app/test';

const menuItems: HelpMenuItem[] = [
  {
    title: 'First',
    children: [
      {
        title: 'Child 1'
      },
      {
        title: 'Child 2'
      }
    ]
  },
  {
    title: 'Second'
  }
];

describe('HelpSideMenuComponent', () => {
  let component: HelpSideMenuComponent;
  let fixture: ComponentFixture<HelpSideMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HelpSideMenuComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpSideMenuComponent);
    component = fixture.componentInstance;
    component.items = cloneDeep(menuItems);

    spyOn(component.select, 'emit');
    spyOn(component.open, 'emit');
    spyOn(component.close, 'emit');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should activate a default item when the items are changed and there is no active one', () => {
    component.ngOnChanges({
      items: new SimpleChange(undefined, menuItems, false)
    });

    expect(component.activeItem).toEqual(menuItems[0]);
    expect(component.select.emit).toHaveBeenCalledWith(menuItems[0]);
  });

  it('should select an item and emit select', () => {
    const item = menuItems[1];
    component.selectItem(item);

    expect(component.activeItem).toEqual(item);
    expect(component.select.emit).toHaveBeenCalledWith(item);
  });

  describe('onItemClick()', () => {
    let item: HelpMenuItem;
    let event: MouseEvent;

    beforeEach(() => {
      item = cloneDeep(menuItems[0]);
      event = new MouseEvent('click');
    });

    it('should stop event propagation', () => {
      spyOn(event, 'stopPropagation');

      component.onItemClick(item, event);

      expect(event.stopPropagation).toHaveBeenCalled();
    });

    it('should select an item', () => {
      component.onItemClick(item, event);

      expect(component.activeItem).toEqual(item);
      expect(component.select.emit).toHaveBeenCalledWith(item);
    });

    it('should open the item when it has children', () => {
      component.onItemClick(item, event);

      expect(item.isOpened).toEqual(true);
      expect(component.open.emit).toHaveBeenCalledWith(item);
    });

    it('should not close the item when it is clicked twice', () => {
      component.onItemClick(item, event);
      expect(item.isOpened).toEqual(true);

      component.onItemClick(item, event);
      expect(item.isOpened).toEqual(true);
    });
  });

  it('should close the item if it is opened', () => {
    const item = menuItems[1];
    item.isOpened = true;

    component.closeItem(item);

    expect(item.isOpened).toEqual(false);
    expect(component.close.emit).toHaveBeenCalledWith(item);
  });

  it('should not close the item if it is already close', () => {
    const item = menuItems[1];
    item.isOpened = false;

    component.closeItem(item);

    expect(item.isOpened).toEqual(false);
    expect(component.close.emit).not.toHaveBeenCalled();
  });

  describe('toggleItem()', () => {
    let item: HelpMenuItem;
    let event: MouseEvent;

    beforeEach(() => {
      item = cloneDeep(menuItems[0]);
      event = new MouseEvent('click');
    });

    it('should stop event propagation', () => {
      spyOn(event, 'stopPropagation');

      component.toggleItem(item, event);

      expect(event.stopPropagation).toHaveBeenCalled();
    });

    it('should open the item if it is closed', () => {
      item.isOpened = false;

      component.toggleItem(item, event);

      expect(item.isOpened).toEqual(true);
      expect(component.open.emit).toHaveBeenCalledWith(item);
    });

    it('should close the item if it is opened', () => {
      item.isOpened = true;

      component.toggleItem(item, event);

      expect(item.isOpened).toEqual(false);
      expect(component.close.emit).toHaveBeenCalledWith(item);
    });
  });

  it('should be able to check if any of the child items is active', () => {
    component.selectItem(menuItems[0].children[0]);

    const actual = component.containsActiveItem(menuItems[0]);

    expect(actual).toEqual(true);
  });
});
