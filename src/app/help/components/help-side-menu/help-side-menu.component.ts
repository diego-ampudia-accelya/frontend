/* eslint-disable @angular-eslint/no-output-native */
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

import { HelpMenuItem } from '~app/help/models';

@Component({
  selector: 'bspl-help-side-menu',
  templateUrl: './help-side-menu.component.html',
  styleUrls: ['./help-side-menu.component.scss']
})
export class HelpSideMenuComponent implements OnChanges {
  @Input() items: HelpMenuItem[];
  @Input() initialItem: HelpMenuItem;

  @Output() select = new EventEmitter<HelpMenuItem>();
  @Output() open = new EventEmitter<HelpMenuItem>();
  @Output() close = new EventEmitter<HelpMenuItem>();

  public activeItem: HelpMenuItem;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.items && (changes.initialItem || !this.activeItem)) {
      this.selectItem(this.initialItem || this.items?.[0]);
    }
  }

  public selectItem(item: HelpMenuItem): void {
    this.activeItem = item;
    this.select.emit(item);
  }

  public onItemClick(item: HelpMenuItem, event: MouseEvent): void {
    event.stopPropagation();

    this.selectItem(item);
    this.openItem(item);
  }

  public openItem(item: HelpMenuItem): void {
    if (item.isOpened || !item.children?.length) {
      return;
    }

    item.isOpened = true;
    this.open.emit(item);
  }

  public closeItem(item: HelpMenuItem): void {
    if (!item.isOpened) {
      return;
    }

    item.isOpened = false;
    this.close.emit(item);
  }

  public toggleItem(item: HelpMenuItem, event: MouseEvent): void {
    event.stopPropagation();

    if (item.isOpened) {
      this.closeItem(item);
    } else {
      this.openItem(item);
    }
  }

  public toggleAll(isOpened: boolean, items: HelpMenuItem[] = this.items): void {
    items
      ?.filter(item => item.children)
      .forEach(item => {
        item.isOpened = isOpened;
        this.toggleAll(isOpened, item.children);
      });
  }

  public areItemsExpanded(): boolean {
    return this.items?.every(item => !item.children || item.isOpened);
  }

  public containsActiveItem(item: HelpMenuItem): boolean {
    return this.activeItem === item || item.children?.some(i => this.containsActiveItem(i));
  }
}
