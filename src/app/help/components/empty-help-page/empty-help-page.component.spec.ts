import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';

import { EmptyHelpPageComponent } from './empty-help-page.component';
import { AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

describe('EmptyHelpPageComponent', () => {
  let component: EmptyHelpPageComponent;
  let fixture: ComponentFixture<EmptyHelpPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EmptyHelpPageComponent, TranslatePipeMock],
      providers: [
        mockProvider(AppConfigurationService, {
          customerServicesLink: 'customer services link'
        })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyHelpPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
