import { Component } from '@angular/core';

import { AppConfigurationService } from '~app/shared/services';

@Component({
  selector: 'bspl-empty-help-page',
  templateUrl: './empty-help-page.component.html',
  styleUrls: ['./empty-help-page.component.scss']
})
export class EmptyHelpPageComponent {
  public get customerServicesLink(): string {
    return this.appConfigurationService.customerServicesLink;
  }

  constructor(private appConfigurationService: AppConfigurationService) {}
}
