import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { HelpPageComponent } from './help-page.component';
import { HelpMenuItem } from '~app/help/models';
import { HelpService } from '~app/help/services/help.service';
import { UserType } from '~app/shared/models/user.model';

describe('HelpPageComponent', () => {
  let component: HelpPageComponent;
  let fixture: ComponentFixture<HelpPageComponent>;
  let helpServiceSpy: SpyObject<HelpService>;
  const httpDelay = 100;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HelpPageComponent],
      providers: [mockProvider(HelpService), mockProvider(L10nTranslationService), mockProvider(Router)],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();

    helpServiceSpy = TestBed.inject<any>(HelpService);
    helpServiceSpy.getPage.and.returnValue(of('page').pipe(delay(httpDelay)));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpPageComponent);
    component = fixture.componentInstance;
    component.userType = UserType.AIRLINE;

    fixture.detectChanges();
  });

  it('should create and have proper default values', () => {
    expect(component).toBeTruthy();
    expect(component.isLoading).toBe(false);
  });

  it('should reload the page if the path is changed', fakeAsync(() => {
    component.item = {
      path: '/new'
    } as HelpMenuItem;

    component.ngOnChanges({ item: new SimpleChange({}, component.item, false) });
    tick(httpDelay);

    expect(helpServiceSpy.getPage).toHaveBeenCalledWith('/new', UserType.AIRLINE);
    expect(component.page).toEqual('page');
    expect(component.isLoading).toBe(false);
  }));

  it('should clear the page and set isLoading to true while loading the page', fakeAsync(() => {
    component.item = {
      path: '/new'
    } as HelpMenuItem;

    component.page = 'page';
    component.isLoading = false;

    component.ngOnChanges({ item: new SimpleChange({}, component.item, false) });

    expect(component.page).toBeNull();
    expect(component.isLoading).toBe(true);

    tick(httpDelay);
  }));

  it('should not reload the page if the path is missing', fakeAsync(() => {
    component.item = {
      path: null
    } as HelpMenuItem;

    component.ngOnChanges({ item: new SimpleChange({}, component.item, false) });
    tick(httpDelay);

    expect(helpServiceSpy.getPage).not.toHaveBeenCalled();
  }));
});
