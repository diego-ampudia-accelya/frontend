import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import { HelpMenuItem } from '~app/help/models';
import { HelpService } from '~app/help/services/help.service';
import { UserType } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-help-page',
  templateUrl: './help-page.component.html',
  styleUrls: ['./help-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class HelpPageComponent implements OnChanges {
  @Input() item: HelpMenuItem;
  @Input() userType: UserType;

  public page: string;
  public isLoading = false;

  constructor(
    private helpService: HelpService,
    private cd: ChangeDetectorRef,
    private translationService: L10nTranslationService,
    private router: Router
  ) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.item) {
      this.loadPage();
    }
  }

  private loadPage(): void {
    this.page = null;

    if (!this.item?.path) {
      return;
    }

    this.isLoading = true;
    this.router.navigate([], { queryParams: { title: this.item.title } });

    this.helpService
      .getPage(this.item.path, this.userType)
      .pipe(
        catchError(() => of(this.translationService.translate('help.pageLoadError'))),
        finalize(() => {
          this.isLoading = false;
          this.cd.detectChanges();
        })
      )
      .subscribe(p => (this.page = p));
  }
}
