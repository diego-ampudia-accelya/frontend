import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { HelpComponent } from './help.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { HelpConfig } from '~app/help/models';
import { HelpService } from '~app/help/services/help.service';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { User } from '~app/shared/models/user.model';
import { TranslatePipeMock } from '~app/test';

const helpConfig: HelpConfig = {
  menuItems: [{ title: 'Title' }]
};

describe('HelpComponent', () => {
  let component: HelpComponent;
  let fixture: ComponentFixture<HelpComponent>;
  const mockedUser = createAirlineUser();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HelpComponent, TranslatePipeMock],
      providers: [
        provideMockStore({ selectors: [{ selector: getUser, value: mockedUser }] }),
        mockProvider(ActivatedRoute, {
          snapshot: {
            data: {
              helpConfig
            }
          },
          queryParams: of({ title: '' })
        }),
        mockProvider(HelpService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create and set the menu items', () => {
    expect(component).toBeTruthy();
    expect(component.helpConfig).toEqual(helpConfig);
  });

  it('should have proper logged user', fakeAsync(() => {
    let user: User;

    component.loggedUser$.subscribe(u => (user = u));

    expect(user).toEqual(mockedUser);
  }));
});
