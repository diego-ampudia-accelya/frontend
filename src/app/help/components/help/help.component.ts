import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { flatten } from 'lodash';
import { Observable } from 'rxjs';
import { finalize, first, switchMap } from 'rxjs/operators';

import { HelpSideMenuComponent } from '../help-side-menu/help-side-menu.component';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { HelpConfig, HelpMenuItem } from '~app/help/models';
import { HelpService } from '~app/help/services/help.service';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components';
import { User } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
  @ViewChild(HelpSideMenuComponent) sideMenu: HelpSideMenuComponent;

  public ButtonDesign = ButtonDesign;
  public selectedMenuItem: HelpMenuItem;
  public previousMenuItem: HelpMenuItem;
  public nextMenuItem: HelpMenuItem;
  public helpConfig: HelpConfig;
  public loggedUser$: Observable<User> = this.store.pipe(select(getUser), first());
  public initialItem: HelpMenuItem;
  public isDownloading = false;

  private flattenedMenuItems: HelpMenuItem[];

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private helpService: HelpService
  ) {}

  public ngOnInit(): void {
    this.helpConfig = this.activatedRoute.snapshot.data.helpConfig;

    this.flattenedMenuItems = this.flattenHelpMenuItems(this.helpConfig.menuItems);
    this.activatedRoute.queryParams.pipe(first()).subscribe(params => {
      this.initialItem = this.flattenedMenuItems.find(item => item.title === params.title);
    });
  }

  public onMenuItemSelect(item: HelpMenuItem): void {
    this.selectedMenuItem = item;

    this.findPreviousAndNextMenuItem(this.selectedMenuItem);
  }

  public onNavigationItemClick(item: HelpMenuItem, event: any) {
    this.sideMenu.onItemClick(item, event);
  }

  public downloadPdfManual(): void {
    if (!this.helpConfig?.pdfManualPath) {
      return;
    }

    this.isDownloading = true;

    this.loggedUser$
      .pipe(
        switchMap(user => this.helpService.downloadManual(this.helpConfig.pdfManualPath, user.userType)),
        finalize(() => (this.isDownloading = false))
      )
      .subscribe();
  }

  private findPreviousAndNextMenuItem(currentItem: HelpMenuItem) {
    const index = this.flattenedMenuItems.findIndex(item => item === currentItem);

    this.previousMenuItem = this.flattenedMenuItems[index - 1];
    this.nextMenuItem = this.flattenedMenuItems[index + 1];
  }

  private flattenHelpMenuItems(items: HelpMenuItem[] = []): HelpMenuItem[] {
    return flatten(
      items.map(item => {
        if (item.children) {
          return [item, ...this.flattenHelpMenuItems(item.children)];
        }

        return item;
      })
    );
  }
}
