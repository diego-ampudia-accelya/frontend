import { HelpMenuItem } from './help-menu-item.model';

export interface HelpConfig {
  menuItems: HelpMenuItem[];
  pdfManualPath?: string;
}
