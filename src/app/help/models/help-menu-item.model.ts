export interface HelpMenuItem {
  title: string;
  children?: HelpMenuItem[];
  isOpened?: boolean;
  path?: string;
}
