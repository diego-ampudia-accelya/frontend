import { ActivatedRouteSnapshot } from '@angular/router';
import { chain, isEmpty, trim } from 'lodash';

export const buildPath = (...args: Array<string | number>) =>
  chain(args)
    .filter(segment => segment != null)
    .map(segment => segment.toString())
    .map(segment => trim(segment, '/'))
    .join('/')
    .value();

// TODO: Can this be obtained somehow from angular?
// TODO: Should throw an error if there are duplicated parameter keys ex: /:id/path/:id
// TODO: Add support for renaming multiple route param keys.
// TODO: Cover every scenario with unit tests.
// TODO: Better naming?
/**
 * Finds the route configuration for url like 'first/5/second/100' (i.e. configuration 'first/:some-id/second/:another-id'),
 * then replaces ':some-id' with the provided value and returns a new url (i.e. 'first/newParamValue/second/100').
 * Both the url and route configuration are obtained from the provided ActivatedRouteSnapshot.
 */
export function replaceRouteParamKey(
  routeSnapshot: ActivatedRouteSnapshot,
  paramKey: string,
  paramValue: string,
  startFromRoot = false
): string {
  let path = '';
  let route = routeSnapshot;
  if (startFromRoot && routeSnapshot.root != null) {
    route = routeSnapshot.root;
  }

  if (route.routeConfig != null && !isEmpty(route.routeConfig.path)) {
    // If it has paramter key ex: something/:id
    // TODO: Can this be checked in a better way?
    const hasParamKeyInRouteConfig = route.routeConfig.path.indexOf(':') !== -1;
    if (hasParamKeyInRouteConfig) {
      path = `/${route.routeConfig.path}`;

      for (const key in route.params) {
        // eslint-disable-next-line no-prototype-builtins
        if (route.params.hasOwnProperty(key)) {
          if (route.routeConfig.path.indexOf(paramKey) !== -1) {
            path = path.replace(paramKey, paramValue);
          } else {
            // Preserve the param keys you don't want to replace
            path = path.replace(`:${key}`, route.params[key]);
          }
        }
      }
    } else {
      path += `/${route.routeConfig.path}`;
    }
  }

  if (!isEmpty(route.children)) {
    // TODO: Is it possible to have more than one here? If so, how it should be hanlded?
    return path + replaceRouteParamKey(route.children[0], paramKey, paramValue);
  }

  return path;
}
