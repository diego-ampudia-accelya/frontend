/**
 * Formats a string value according to a custom word wrapping logic.
 *
 * @param value The string to be formatted.
 * @param lineCharacterLimit The limitation of characters per line
 * @param linesLimit The limitation of lines
 * @returns Array of strings, each string represents a separate line of the formatted value
 */
export function customWordWrap(value: string, lineCharacterLimit, linesLimit): string[] {
  const lines: string[] | string[][] = getSplittedByWhiteSpacesLines(value);

  let nLines = [];
  let nLineIndex = 0;
  nLines[nLineIndex] = '';
  lines.forEach(line => {
    if (linesLimit) {
      if (nLines.filter(nLine => nLine === '\n').length >= linesLimit) {
        return;
      }
    }
    line.forEach((words: any) => {
      if (words === '\n') {
        nLineIndex++;
        nLines[nLineIndex] = '\n';
        nLineIndex++;
        nLines[nLineIndex] = '';
      } else {
        // turn words into array of n words
        const regexp = new RegExp('.{1,' + lineCharacterLimit + '}');
        words = words.match(regexp, 'g');
        if (words) {
          words.forEach(word => {
            const remainingCharacters = lineCharacterLimit - nLines[nLineIndex].length;
            if (word.length > remainingCharacters) {
              nLineIndex++;
              nLines[nLineIndex] = '\n';
              nLineIndex++;
              nLines[nLineIndex] = '';
            }
            nLines[nLineIndex] += word;
          });
        }
      }
    });
  });

  // clear the extra lines in case of more than rowsLimit lines
  if (linesLimit) {
    nLines = nLines.slice(0, linesLimit + (linesLimit - 1));
  }

  return nLines;
}

function getSplittedByWhiteSpacesLines(value: string): string[] | string[][] {
  return value.split(/(\n)/).map(getSplittedLine);
}

const getSplittedLine = line => (line !== '\n' ? line.split(/(\S+\s+)/).filter(Boolean) : [line]);
