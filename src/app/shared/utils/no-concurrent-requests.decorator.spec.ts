import { fakeAsync } from '@angular/core/testing';
import { forkJoin, Observable, of } from 'rxjs';

import { NoConcurrentRequests } from './no-concurrent-requests.decorator';

class TestHost {
  @NoConcurrentRequests()
  execute(): Observable<any> {
    return this.request();
  }

  request(): any {}
}

describe('NoConcurrentRequests', () => {
  let testHost: TestHost;
  let requestSpy: jasmine.Spy;

  beforeEach(() => {
    testHost = new TestHost();
    requestSpy = spyOn(testHost, 'request').and.returnValue(of('test'));
  });

  it('should invoke decorated method', fakeAsync(() => {
    let actualResult: string;

    testHost.execute().subscribe(result => (actualResult = result));

    expect(actualResult).toEqual('test');
    expect(testHost.request).toHaveBeenCalledTimes(1);
  }));

  it('should make a request only once when calling the decorated method multiple times concurrently', fakeAsync(() => {
    let actualResult: string[];

    forkJoin([testHost.execute(), testHost.execute(), testHost.execute()]).subscribe(result => (actualResult = result));

    expect(actualResult).toEqual(['test', 'test', 'test']);
    expect(testHost.request).toHaveBeenCalledTimes(1);
  }));

  it('should throw error when the decorated method does not return an Observable', fakeAsync(() => {
    requestSpy.and.returnValue('test');

    expect(() => testHost.execute()).toThrowError();
  }));

  it('should throw error when the decorated method accepts arguments', fakeAsync(() => {
    expect(() => testHost.execute.apply(testHost, ['test'])).toThrowError();
  }));

  it('should invoke the decorated method multiple times when calling in sequence', fakeAsync(() => {
    let firstResult: string;
    testHost.execute().subscribe(result => (firstResult = result));

    requestSpy.and.returnValue(of('test-2'));
    let secondResult: string;
    testHost.execute().subscribe(result => (secondResult = result));

    expect(firstResult).toEqual('test');
    expect(secondResult).toEqual('test-2');
    expect(testHost.request).toHaveBeenCalledTimes(2);
  }));
});
