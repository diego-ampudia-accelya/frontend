import { PagedData } from '../models/paged-data.model';

import { toPeriodEntityData } from './entity-period-mapper.utils';
import { Period } from '~app/master-data/periods/shared/period.models';

describe('entity-period-mapper', () => {
  const periods: Period[] = [
    {
      id: 1,
      period: '2021011',
      dateFrom: '01/01/2021',
      dateTo: '01/15/2021',
      isoCountryCode: 'ES'
    },
    {
      id: 2,
      period: '2021012',
      dateFrom: '01/16/2021',
      dateTo: '01/31/2021',
      isoCountryCode: 'ES'
    }
  ];

  const pagedData: PagedData<any> = {
    records: [],
    pageSize: 20,
    total: 2,
    totalPages: 1,
    pageNumber: 0
  };

  it('should return original entity data if period already exists', () => {
    const records = [{ billingAnalysisEndingDate: '01/22/2021', period: '2021012' }];

    expect(toPeriodEntityData({ ...pagedData, records }, periods)).toEqual({
      ...pagedData,
      records: [{ billingAnalysisEndingDate: '01/22/2021', period: '2021012' }]
    });
  });

  it('should return original entity data if period and baed not exists', () => {
    const records = [{}];

    expect(toPeriodEntityData({ ...pagedData, records }, periods)).toEqual({
      ...pagedData,
      records: [{}]
    });
  });

  it('should map period entity data correctly if period not exists and baed (date or string format) exists', () => {
    const records: Array<any> = [
      { billingAnalysisEndingDate: '01/14/2021' },
      { billingAnalysisEndingDate: new Date('01/22/2021') }
    ];

    expect(toPeriodEntityData({ ...pagedData, records }, periods)).toEqual({
      ...pagedData,
      records: [
        { billingAnalysisEndingDate: '01/14/2021', period: '2021011' },
        { billingAnalysisEndingDate: new Date('01/22/2021'), period: '2021012' }
      ]
    });
  });
});
