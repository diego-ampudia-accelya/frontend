import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, DetachedRouteHandle, Params, RouteReuseStrategy } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { CoreAppState, getMenuTabs } from '~app/core/reducers';
import { determineTabId } from '~app/core/reducers/menu-tab.reducer';
import { MenuTab } from '~app/shared/models/menu-tab.model';

@Injectable()
export class TabRouteReuseStrategy implements RouteReuseStrategy {
  private handlers: Map<string, DetachedRouteHandle> = new Map<string, DetachedRouteHandle>();
  private tabsSubscription: Subscription;

  constructor(private appStore: Store<CoreAppState>) {
    this.initializeTabsListener();
  }

  public shouldDetach(route: ActivatedRouteSnapshot): boolean {
    // If it is a tab and valid, then it will return true to proceed with storage
    return !!route.routeConfig && this.isRoutePersistent(route) && !!this.getIdFromRoute(route);
  }

  public store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
    const key = this.getIdFromRoute(route);
    this.handlers.set(key, handle);
  }

  // If returns true, then it will proceed with retrieval
  public shouldAttach(route: ActivatedRouteSnapshot): boolean {
    const id = this.getIdFromRoute(route);

    return !!id && this.handlers.has(id);
  }

  public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
    return this.handlers.get(this.getIdFromRoute(route));
  }

  public shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    this.updateTabsSubscription();

    return this.isSameRoute(future, curr);
  }

  private isRouteFromTab(route: ActivatedRouteSnapshot): boolean {
    const tab: MenuTab = route.data.tab;

    return !!tab && !!this.getIdFromRoute(route);
  }

  private isSameRoute(route1: ActivatedRouteSnapshot, route2: ActivatedRouteSnapshot): boolean {
    return this.getIdFromRoute(route1) === this.getIdFromRoute(route2);
  }

  private isRoutePersistent(route: ActivatedRouteSnapshot): boolean {
    let isPersistent = false;

    if (this.isRouteFromTab(route)) {
      isPersistent = route.data.tab.persistent;
    }

    return isPersistent;
  }

  private getIdFromRoute(route: ActivatedRouteSnapshot): string {
    let id: string = '/' + route.url.map(segment => segment.path).join('/');
    const tab: MenuTab = route.data?.tab;

    // In subtab navigation data tab is not provided, so `id` is built with route url path as above initialization
    if (tab) {
      // We add queryParams in tab coming from route
      const tabWithQueryParams = { ...tab, queryParams: route.queryParams };
      // Sometimes tab data is not complete and we have to build the id from route.url
      // Catch block because `determineTabId` could throw error in routes malformations
      try {
        id = this.getIdFromTab(tabWithQueryParams);
      } catch {
        // eslint-disable-next-line no-console
        console.warn(`Could not build id ${id} from route data tab`);
      }
    }

    return id;
  }

  private getIdFromTab(tab: MenuTab): string {
    // Sometimes we build routes like refunds/app/issue?id=1234
    // Including queryParams into tabId, allows us to make a difference between same routes with different queryParams
    return `${determineTabId(tab)}${this.transformQueryParamsIntoString(tab.queryParams)}`;
  }

  private transformQueryParamsIntoString(queryParams: Params = {}): string {
    // Builds a standart query string
    const paramString = Object.keys(queryParams)
      .sort()
      .map(paramKey => `${paramKey}=${queryParams[paramKey]}`)
      .join('&');

    return paramString ? `?${paramString}` : '';
  }

  private updateTabsSubscription(): void {
    if (this.tabsSubscription.closed) {
      this.initializeTabsListener();
    }
  }

  private initializeTabsListener() {
    this.tabsSubscription = this.appStore
      .select(getMenuTabs)
      .pipe(map(tabs => tabs.map(tab => this.getIdFromTab(tab))))
      .subscribe(tabIds => this.cleanTabsCache(tabIds));
  }

  private cleanTabsCache(tabIds: string[]): void {
    // We loop through our handlers to remove those whose tab is not open anymore
    this.handlers.forEach((handler, tab) => {
      if (!tabIds.includes(tab)) {
        handler['componentRef'].destroy(); // Components must be manually destroy when tabs are closed
        this.handlers.delete(tab);
      }
    });
  }
}
