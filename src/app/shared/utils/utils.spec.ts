import { generateNumberedList } from './utils';

describe('generateNumberedList', () => {
  it('should format properly 1st level items', () => {
    const actual = generateNumberedList([{ title: 'Title 1' }, { title: 'Title 2' }], 'title', null);

    expect(actual).toEqual([{ title: '1. Title 1' }, { title: '2. Title 2' }]);
  });

  it('should format properly 2nd level items', () => {
    const actual = generateNumberedList(
      [{ title: 'Title 1' }, { title: 'Title 2', children: [{ title: 'One' }, { title: 'Two' }] }],
      'title',
      'children'
    );

    expect(actual).toEqual([
      { title: '1. Title 1' },
      { title: '2. Title 2', children: [{ title: '2.1 One' }, { title: '2.2 Two' }] }
    ]);
  });

  it('should format properly 3rd level items', () => {
    const actual = generateNumberedList(
      [{ title: 'Title 1' }, { title: 'Title 2', children: [{ title: 'One', children: [{ title: 'Sub one' }] }] }],
      'title',
      'children'
    );

    expect(actual).toEqual([
      { title: '1. Title 1' },
      { title: '2. Title 2', children: [{ title: '2.1 One', children: [{ title: '2.1.1 Sub one' }] }] }
    ]);
  });
});
