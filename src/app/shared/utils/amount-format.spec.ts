import { getAmountFormat } from './amount-format';

describe('amount format', () => {
  it('should return format according to number of decimals when it is specified', () => {
    expect(getAmountFormat(4)).toBe('1.4-4');
  });

  it('should apply default number of decimals when it is not specified', () => {
    expect(getAmountFormat()).toBe('1.2-2');
  });

  it('should apply default number of decimals when `null` is specified', () => {
    expect(getAmountFormat(null)).toBe('1.2-2');
  });

  it('should apply 0 decimals when `0` is specified', () => {
    expect(getAmountFormat(0)).toBe('1.0-0');
  });
});
