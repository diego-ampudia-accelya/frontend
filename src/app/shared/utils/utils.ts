import { clone, range } from 'lodash';
import { Observable, of } from 'rxjs';
import { defaultIfEmpty, first } from 'rxjs/operators';

import { ArrayHelper } from '../helpers';
import { DropdownOption } from '../models/dropdown-option.model';

export function canProceed(canProceed$: Observable<boolean>): Promise<boolean> {
  canProceed$ = canProceed$ || of(true);

  return canProceed$.pipe(defaultIfEmpty(true), first()).toPromise();
}

export function generateYearOptions(start?: Date, end?: Date): DropdownOption[] {
  start = start || new Date(1940, 0);
  end = end || new Date();
  const years = range(end.getFullYear(), start.getFullYear() - 1, -1).map(year => year.toString());

  return ArrayHelper.toDropdownOptions(years);
}

export function isInHorizontalViewport(element: HTMLElement): boolean {
  const rect = element.getBoundingClientRect();

  return rect.left >= 0 && rect.right <= (window.innerWidth || document.documentElement.clientWidth);
}

export function isInVerticalViewport(element: HTMLElement): boolean {
  const rect = element.getBoundingClientRect();

  return rect.top >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight);
}

export function isInViewport(element: HTMLElement): boolean {
  return isInHorizontalViewport(element) && isInVerticalViewport(element);
}

export function generateNumberedList<T>(items: T[], valueKey: keyof T, childrenKey: keyof T, prefix = ''): T[] {
  return items?.map((item, index) => {
    const itemClone = clone(item);
    const numbering = `${prefix}${index + 1}`;
    const suffix = prefix ? ' ' : '. ';

    if (itemClone[valueKey]) {
      itemClone[valueKey] = (numbering + suffix + itemClone[valueKey]) as any;
    }

    if (itemClone[childrenKey]) {
      itemClone[childrenKey] = generateNumberedList(
        itemClone[childrenKey] as any,
        valueKey,
        childrenKey,
        numbering + '.'
      ) as any;
    }

    return itemClone;
  });
}
