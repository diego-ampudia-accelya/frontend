export const DEFAULT_AMOUNT_PRECISION = 2;

export const getAmountFormat = (decimals?: number) => {
  decimals = decimals != null ? decimals : DEFAULT_AMOUNT_PRECISION;

  return `1.${decimals}-${decimals}`;
};
