import { merge, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

export const rethrowError = (selector: (error) => any) =>
  catchError(error => merge(of(selector(error)), throwError(error)));
