import { ActivatedRouteSnapshot } from '@angular/router';
import { createSpyObject } from '@ngneat/spectator';
import { cloneDeep } from 'lodash';

import { buildPath, replaceRouteParamKey } from './path-builder';

describe('buildPath', () => {
  it('should join path segments with "/"', () => {
    const actual = buildPath('service', 'resource', 2);

    expect(actual).toBe('service/resource/2');
  });

  it('should remove "/" from start of segments', () => {
    const actual = buildPath('/service', '/resource', 2);

    expect(actual).toBe('service/resource/2');
  });

  it('should remove "/" from end of segments', () => {
    const actual = buildPath('service/', 'resource/', 2);

    expect(actual).toBe('service/resource/2');
  });

  it('should ignore "null" path parameters', () => {
    const actual = buildPath('service', 'resource', null, 0);

    expect(actual).toBe('service/resource/0');
  });

  it('should ignore "undefined" path parameters', () => {
    const actual = buildPath('service', 'resource', undefined, 0);

    expect(actual).toBe('service/resource/0');
  });
});

describe('replaceRouteParamKey', () => {
  let activatedRouteSnapshotSpy: ActivatedRouteSnapshot;
  beforeEach(() => {
    activatedRouteSnapshotSpy = createSpyObject(ActivatedRouteSnapshot);
  });

  it('should start from root when is needed', () => {
    const customRootSnapshot = {
      ...cloneDeep(activatedRouteSnapshotSpy),
      routeConfig: { path: 'root/snapshot' }
    } as ActivatedRouteSnapshot;
    const childSnapshot = {
      ...activatedRouteSnapshotSpy,
      routeConfig: { path: 'child/snapshot' },
      root: customRootSnapshot
    } as ActivatedRouteSnapshot;

    const rootSnapshotUrl = replaceRouteParamKey(childSnapshot, 'fakeKey', 'fakeValue', true);
    expect(rootSnapshotUrl).toBe('/root/snapshot');
    expect(rootSnapshotUrl).not.toBe('/child/snapshot');
  });

  it('should replace passed :firstId whit passed value 999', () => {
    const customSnapshotRoot: any = {
      ...activatedRouteSnapshotSpy,
      routeConfig: { path: 'first/:firstId' },
      params: { firstId: 10 }
    };

    const replacedFirstIdUrl = replaceRouteParamKey(customSnapshotRoot, ':firstId', '999');
    expect(replacedFirstIdUrl).toBe('/first/999');
    expect(replacedFirstIdUrl).not.toBe('/first/:firstId');
  });

  it("shouldn't replace when routeConfig is null", () => {
    const customSnapshotRoot: any = {
      ...activatedRouteSnapshotSpy,
      routeConfig: null,
      params: { firstId: 10 }
    };

    const emptyUrl = replaceRouteParamKey(customSnapshotRoot, ':firstId', '999');
    expect(emptyUrl).toBe('');
  });

  it('should replace route param key with associated route param value, when passed paramKey can not be found in routeConfig path', () => {
    const customSnapshotRoot: any = {
      ...activatedRouteSnapshotSpy,
      routeConfig: { path: 'first/:firstId' },
      params: { firstId: 10 }
    };

    const replacedUrl = replaceRouteParamKey(customSnapshotRoot, ':justId', '999');
    expect(replacedUrl).toBe('/first/10');
  });

  it('should replace route children path, with corresponding paramKey and paramValue', () => {
    const childrenSnapshot = {
      ...cloneDeep(activatedRouteSnapshotSpy),
      params: { firstId: 10 },
      routeConfig: { path: 'children/:firstId' }
    };
    const customSnapshotRoot: any = {
      ...activatedRouteSnapshotSpy,
      routeConfig: { path: 'route/:firstId' },
      params: { firstId: 10 },
      children: [childrenSnapshot]
    };

    const childrenUrl = replaceRouteParamKey(customSnapshotRoot, ':firstId', '6');
    expect(childrenUrl).toBe('/route/6/children/6');
  });
});
