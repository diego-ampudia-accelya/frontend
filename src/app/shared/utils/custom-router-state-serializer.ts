import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Params, RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';
import { intersection } from 'lodash';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

@Injectable({
  providedIn: 'root'
})
export class CustomRouterSateSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;
    let params: Params = {};

    while (route.firstChild) {
      route = route.firstChild;

      this.ensureNoParamsWithSameName(params, route.params);
      params = { ...params, ...route.params };
    }

    const {
      url,
      root: { queryParams }
    } = routerState;

    // Only return an object including the URL, params and query params
    // instead of the entire snapshot
    return { url, params, queryParams };
  }

  public getParamsFrom(activatedRoute: ActivatedRouteSnapshot) {
    let route = activatedRoute;
    let params: Params = {};

    do {
      this.ensureNoParamsWithSameName(params, route.params);
      params = { ...params, ...route.params };

      route = route.parent;
    } while (route.parent);

    return params;
  }

  private ensureNoParamsWithSameName(allParams: Params, routeParams: Params): void {
    // We cannot store multiple parameters with the same name
    const conflictingParams = intersection(Object.keys(allParams), Object.keys(routeParams));
    if (conflictingParams.length) {
      throw new Error(
        `Cannot serialize router state. The parameter ${conflictingParams} is used in more than one route.`
      );
    }
  }
}
