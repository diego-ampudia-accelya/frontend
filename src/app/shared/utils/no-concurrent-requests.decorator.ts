import { isObservable, Observable } from 'rxjs';
import { finalize, shareReplay } from 'rxjs/operators';

// eslint-disable-next-line @typescript-eslint/naming-convention
export function NoConcurrentRequests(): MethodDecorator {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const originalMethod = descriptor.value;
    let pendingOperation$: Observable<any>;

    descriptor.value = function (...args: any[]): Observable<any> {
      if (args?.length > 0) {
        throw new Error('NoConcurrentRequests does not support decorating methods with arguments');
      }

      if (pendingOperation$ == null) {
        const result$ = originalMethod.apply(this, args);
        if (!isObservable(result$)) {
          throw new Error('NoConcurrentRequests supports only methods that return an Observable.');
        }

        pendingOperation$ = result$.pipe(
          shareReplay(1),
          finalize(() => (pendingOperation$ = null))
        );
      }

      return pendingOperation$;
    };

    return descriptor;
  };
}
