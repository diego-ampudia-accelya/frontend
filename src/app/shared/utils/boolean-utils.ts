export const or = (...variables: boolean[]) => variables.reduce((result, variable) => result || variable, false);

export const and = (...variables: boolean[]) => variables.reduce((result, variable) => result && variable, true);

export const tryParseBoolean = (value: any, fallback: boolean = null): boolean | null => {
  if (value === true || value === 'true') {
    return true;
  } else if (value === false || value === 'false') {
    return false;
  }

  return fallback;
};
