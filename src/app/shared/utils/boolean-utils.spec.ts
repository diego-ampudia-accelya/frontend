import { and, or, tryParseBoolean } from './boolean-utils';

describe('boolean-utils', () => {
  it('or() should return true if any of the provided value is true', () => {
    const actual = or(false, true, false);
    expect(actual).toBe(true);
  });

  it('or() should return false if all of the provided values are false', () => {
    const actual = or(false, false, false);
    expect(actual).toBe(false);
  });

  it('and() should return false if any of the provided value is false', () => {
    const actual = and(true, false, true);
    expect(actual).toBe(false);
  });

  it('and() should return true if all of the provided values are true', () => {
    const actual = and(true, true, true);
    expect(actual).toBe(true);
  });

  describe('tryParseBoolean', () => {
    it('should return true when called with true', () => {
      expect(tryParseBoolean(true)).toBe(true);
    });

    it('should return false when called with false', () => {
      expect(tryParseBoolean(false)).toBe(false);
    });

    it('should return true when called with `true`', () => {
      expect(tryParseBoolean('true')).toBe(true);
    });

    it('should return false when called with `false`', () => {
      expect(tryParseBoolean('false')).toBe(false);
    });

    it('should return null when called with an unknown value', () => {
      expect(tryParseBoolean([])).toBe(null);
    });

    it('should return false when called with an unknown value with fallback false', () => {
      expect(tryParseBoolean([], false)).toBe(false);
    });
  });
});
