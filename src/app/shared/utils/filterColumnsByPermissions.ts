import { TableColumn } from '@swimlane/ngx-datatable';

export function filterColumnsByPermissions(permissions: string[], columns: (TableColumn & { permission?: string })[]) {
  return columns.filter(column => {
    // handle cases where permission name starts with `!`, e.g: !create_user
    if (column.permission && column.permission[0] === '!') {
      // do not show column if the user has the specified permission
      return !permissions.includes(column.permission.slice(1));
    }

    // show the column if there is no permission or the user has the specified permission
    return !column.permission || permissions.includes(column.permission);
  });
}
