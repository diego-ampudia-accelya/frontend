import { customWordWrap } from './custom-wordwrap';

describe('custom-wordwrap', () => {
  it('customWordWrap() should format the text properly', () => {
    const text =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n\nLorem Ipsum has been the industry's " +
      'standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n\n\n\n';
    const expected = [
      'Lorem Ipsum is simply dummy text of the ',
      '\n',
      'printing and typesetting industry. ',
      '\n',
      '',
      '\n',
      "Lorem Ipsum has been the industry's standard ",
      '\n',
      'dummy text ever since the 1500s, when an ',
      '\n',
      'unknown printer took a galley of type and ',
      '\n',
      'scrambled it to make a type specimen book.',
      '\n',
      '',
      '\n',
      '',
      '\n',
      '',
      '\n',
      ''
    ];
    const actual = customWordWrap(text, 45, 20);

    expect(actual).toEqual(expected);
  });
});
