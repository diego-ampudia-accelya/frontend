import { TestBed, TestModuleMetadata } from '@angular/core/testing';
import {} from 'jasmine';

//* Avoids resetting beforeEach improving performance. Use it with caution.

//* Extracted from:
//https://medium.com/front-end-weekly/angular-initialize-your-tests-in-a-beforeall-21e709474632

const resetTestingModule = TestBed.resetTestingModule;
const preventAngularFromResetting = () => (TestBed.resetTestingModule = () => TestBed);
const allowAngularToReset = () => {
  resetTestingModule();
  TestBed.resetTestingModule = resetTestingModule;
};

export const setUpTestBedBeforeAll = (moduleDef: TestModuleMetadata, ...funcs: (() => void)[]) => {
  beforeAll(done =>
    (async () => {
      resetTestingModule();
      preventAngularFromResetting();

      TestBed.configureTestingModule(moduleDef);
      funcs.forEach(func => func());

      TestBed.resetTestingModule = () => TestBed;

      return TestBed.compileComponents();
    })()
      .then(done)
      .catch(done.fail)
  );

  afterAll(() => allowAngularToReset());
};

export const testQuery = { paginateBy: { page: 0, size: 20, totalElements: 0 }, sortBy: [], filterBy: {} };

export const testDownloadRequestQueryBodyNoReduceFilterBy = {
  size: 20,
  sortOrder: 'ASC',
  filterBy: {},
  reduceFilterBy: false,
  page: 0
};

export const testDownloadRequestQueryBody = {
  filterBy: {},
  paginateBy: { page: 0, size: 20, totalElements: 0 },
  sortBy: []
};
