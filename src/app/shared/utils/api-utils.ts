export const isNew = (model: { id: number }): boolean => model.id == null;

export const determineMethodToSave = (model: { id: number }) => (isNew(model) ? 'POST' : 'PUT');

export const getPluralPath = (path: string): string => (path + 's').replace('ys', 'ies');
