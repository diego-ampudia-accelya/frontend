import { determineMethodToSave, getPluralPath, isNew } from './api-utils';

describe('api-utils', () => {
  it('isNew() should return true if the id is null', () => {
    const actual = isNew({ id: null });
    expect(actual).toEqual(true);
  });

  it('isNew() should return false if the id is a number', () => {
    const actual = isNew({ id: 5 });
    expect(actual).toEqual(false);
  });

  it('determineMethodToSave() should return POST if the model is new', () => {
    const actual = determineMethodToSave({ id: null });
    expect(actual).toEqual('POST');
  });

  it('determineMethodToSave() should return PUT if the model is not new', () => {
    const actual = determineMethodToSave({ id: 5 });
    expect(actual).toEqual('PUT');
  });

  it('should return a plural path', () => {
    const pluralPath = getPluralPath('adm/enquiry');
    expect(pluralPath).toBe('adm/enquiries');
  });
});
