import { PeriodUtils } from '../components';
import { PagedData } from '../models/paged-data.model';
import { Period } from '~app/master-data/periods/shared/period.models';

/** Add `period` property, if it does not exist, to data records based on `billingAnalysisEndingDate` property. */
export const toPeriodEntityData = <T>(data: PagedData<T>, periods: Period[]): PagedData<T> => ({
  ...data,
  records: data.records.map(item => (item['period'] ? item : toPeriodEntity<T>(item, periods)))
});

const toPeriodEntity = <T>(entity: T, periods: Period[]): T => {
  const baed = entity['billingAnalysisEndingDate'];

  return {
    ...entity,
    ...(baed && { period: PeriodUtils.findPeriodFrom(periods, new Date(baed))?.period })
  };
};
