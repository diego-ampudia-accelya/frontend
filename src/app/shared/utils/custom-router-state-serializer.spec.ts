import { CustomRouterSateSerializer } from './custom-router-state-serializer';

describe('CustomRouterSateSerializer', () => {
  let serializer: CustomRouterSateSerializer;

  beforeEach(() => {
    serializer = new CustomRouterSateSerializer();
  });

  it('should use only url, params and queryParams when serializing', () => {
    const expected = {
      url: '/some-url/120/some_real_param?test=5',
      params: { 'some-id': '120', 'some-param': 'some_real_param' },
      queryParams: { test: '5' }
    };
    const routerState: any = {
      root: {
        queryParams: { test: '5' },
        firstChild: {
          params: { 'some-id': '120', 'some-param': 'some_real_param' }
        }
      },
      url: '/some-url/120/some_real_param?test=5'
    };
    const actual = serializer.serialize(routerState);

    expect(actual).toEqual(expected);
  });

  it('should extract all params from all activated route chain', () => {
    const expected = {
      param1: '10',
      param2: '20'
    };
    const activatedRoute: any = {
      params: { param1: '10' },
      parent: {
        params: { param2: '20' },
        parent: {}
      }
    };

    const actual = serializer.getParamsFrom(activatedRoute);

    expect(actual).toEqual(expected);
  });

  it('should throw an error if a duplicate param key is met', () => {
    const expected = new Error('Cannot serialize router state. The parameter param1 is used in more than one route.');
    const activatedRoute: any = {
      params: { param1: '10' },
      parent: {
        params: { param1: '20' },
        parent: {}
      }
    };

    expect(() => serializer.getParamsFrom(activatedRoute)).toThrow(expected);
  });
});
