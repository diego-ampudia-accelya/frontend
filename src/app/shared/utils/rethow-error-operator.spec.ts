import { fakeAsync, tick } from '@angular/core/testing';
import { cold } from 'jasmine-marbles';
import { of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { rethrowError } from './rethrow-error-operator';

describe('Rethrow error operator', () => {
  let a = 0;
  const testFunction = () => {
    ++a;
  };

  it('should not execute function passed to rethrow error it the observable does not throw an error', fakeAsync(() => {
    const observable = cold('x--a--w');
    a = 0;
    observable.pipe(rethrowError(testFunction)).subscribe();
    tick();
    expect(a).toEqual(0);
  }));

  it('should execute function passed to rethrow error it the observable throws an error', fakeAsync(() => {
    const observable = throwError('error');
    a = 0;
    observable
      .pipe(
        rethrowError(testFunction),
        catchError(() => of({}))
      )
      .subscribe();
    tick();
    expect(a).toEqual(1);
  }));
});
