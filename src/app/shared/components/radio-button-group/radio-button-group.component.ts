/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @angular-eslint/no-output-native */
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Optional,
  Output,
  QueryList,
  Self,
  ViewChildren
} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { RadioButtonComponent } from '~app/shared/components/radio-button/radio-button.component';
import { FormUtil } from '~app/shared/helpers';
import { RadioButton } from '~app/shared/models/radio-button.model';

@Component({
  selector: 'bspl-radio-button-group',
  templateUrl: './radio-button-group.component.html',
  styleUrls: ['./radio-button-group.component.scss']
})
export class RadioButtonGroupComponent implements ControlValueAccessor {
  @Input() options: RadioButton[];
  @Input() groupName: string;
  @Input() label: string;
  @Input() isVertical = false;
  @Input() tooltipText: string;
  @Input() mayShowError = false;

  @HostBinding('class.control--flex-basis-50')
  @Input()
  hasFlexBasis = false;

  //* The control will be disabled (see Angular docs to know the implications)
  @HostBinding('class.control--disabled')
  @Input()
  get isDisabled() {
    return this.ngControl ? this.ngControl.disabled : this._isDisabled;
  }
  set isDisabled(isDisabled: boolean) {
    this._isDisabled = isDisabled;

    const control = this.ngControl?.control;
    if (control) {
      if (isDisabled) {
        control.disable();
      } else {
        control.enable();
      }
    }
  }

  @HostBinding('class.control--showErrors') get areErrorsShown() {
    return this.mayShowError && !this.isVertical && FormUtil.areErrorsShown(this.ngControl);
  }

  @Output() change = new EventEmitter();

  @ViewChildren(RadioButtonComponent) radioButtons: QueryList<RadioButtonComponent>;

  private value: any;
  private _isDisabled: boolean;

  public onModelChange: Function = () => {
    //This is intentional
  };
  public onModelTouched: Function = () => {
    //This is intentional
  };

  constructor(@Self() @Optional() public ngControl: NgControl, private translationService: L10nTranslationService) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  public isActive(value): boolean {
    return this.value === value;
  }

  writeValue(value) {
    this.value = value;

    // we need to delay writing value because at this point
    // childen components do not have its inputs bound yet
    setTimeout(() => {
      this.radioButtons.forEach(radioButton => {
        radioButton.writeValue(this.value);
      });
    });
  }

  registerOnChange(fn) {
    this.onModelChange = fn;
  }

  registerOnTouched(fn) {
    this.onModelTouched = fn;
  }

  radioButtonClicked(option: RadioButton): void {
    if (this.isOptionDisabled(option.disabled)) {
      return;
    }

    this.value = option.value;

    this.onModelChange(option.value);
    this.change.emit(option.value);
  }

  isOptionDisabled(optionDisabledProperty: boolean): boolean {
    return this.ngControl?.disabled || this._isDisabled || optionDisabledProperty;
  }

  public getErrorMessage(): string {
    return FormUtil.getErrorMessage(this.ngControl, this.translationService);
  }
}
