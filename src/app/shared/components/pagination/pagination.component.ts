import { DecimalPipe } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { isInteger, isNumber, toNumber } from 'lodash';
import { Observable } from 'rxjs';

import { PAGINATION } from './pagination.constants';
import { canProceed } from '~app/shared/utils';

@Component({
  selector: 'bspl-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() currentPage = 1;
  @Input() itemsPerPage = 10;
  @Input() showOptions = true;
  @Input() totalItems: number;
  /**
   * Async action which will be executed before the pagination is changed. The result indicates whether the action should be executed.
   */
  @Input() beforePaginationChange$: Observable<boolean>;

  @Input() itemsPerPageOptions: any[] = PAGINATION.ITEMS_PER_PAGE_DROPDOWN_VALUES;
  @Input() maxSupportedItems: number;

  @Output() pageChange = new EventEmitter();
  @Output() maxSupportedPageExceeded = new EventEmitter();

  dropdownValue = new FormControl();
  totalPages: number;
  pagesArray: any[] = [];
  visibleItemsRange = '';
  inputSize = 1;
  isNumber = isNumber;

  private get totalSupportedPages(): number {
    let supportedPages = Number.MAX_SAFE_INTEGER;
    if (this.maxSupportedItems != null) {
      supportedPages = Math.ceil(this.maxSupportedItems / this.itemsPerPage);
    }

    return supportedPages;
  }

  constructor(private changeDetectorRef: ChangeDetectorRef, private decimalPipe: DecimalPipe) {}

  public ngOnInit(): void {
    this.initialPaginationSetup();
    this.generatePagesArray();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const { itemsPerPage, totalItems } = changes;
    const hasItemsPerPageChanged = itemsPerPage && !itemsPerPage.firstChange;
    const hasTotalItemsChanged = totalItems && !totalItems.firstChange;
    if (hasItemsPerPageChanged || hasTotalItemsChanged) {
      this.initialPaginationSetup();
      this.generatePagesArray();
    }
  }

  public initialPaginationSetup(): void {
    const itemsPerPage = this.itemsPerPage;

    const { value, label } =
      this.itemsPerPageOptions.find(item => item.label === itemsPerPage) || this.itemsPerPageOptions[0];

    if (value !== itemsPerPage) {
      this.itemsPerPage = Number(label);
    }

    this.totalPages = Math.ceil(this.totalItems / itemsPerPage);

    this.dropdownValue.setValue(value);
    this.changeDetectorRef.detectChanges();
  }

  // Generates the string 'Showing: 10-21 of 72'
  public generateItemsRangeString(): void {
    let endRange;
    let startRange;
    endRange = this.currentPage * this.itemsPerPage;
    startRange = endRange - this.itemsPerPage + 1;

    if (endRange > this.totalItems) {
      endRange = this.totalItems;
    }

    if (this.totalItems === 0) {
      startRange = 0;
    }

    this.visibleItemsRange = `${this.decimalPipe.transform(startRange)} - ${this.decimalPipe.transform(endRange)}`;
  }

  public generatePagesArray(): void {
    let pagesArray: any[] = [];
    pagesArray =
      this.totalPages <= PAGINATION.VISIBLE_PAGES_THRESHOLD ? this.generateNumberedPages() : this.generateDottedPages();

    this.generateItemsRangeString();
    this.pagesArray = [...pagesArray];
  }

  public generateNumberedPages(): number[] {
    let numberedPages = Array.from({ length: this.totalPages }, (v, i) => {
      i++;

      return i;
    });

    if (numberedPages && numberedPages.length === 0) {
      numberedPages = [1];
    }

    return numberedPages;
  }

  public generateDottedPages(): any[] {
    const total = this.totalPages;
    const current = this.currentPage;
    if (this.currentPage <= 4) {
      // { 1, 2, [3], 4, 5 ... 20 }
      return [1, 2, 3, 4, 5, PAGINATION.INVISIBLE_RANGE_SIGN, total];
    } else if (this.currentPage > total - 4) {
      // { 1 ... 16 17 [18] 19 20 }
      return [1, PAGINATION.INVISIBLE_RANGE_SIGN, total - 4, total - 3, total - 2, total - 1, total];
    } else {
      // { 1 ... 11 [12] 13 ... 20 }
      return [
        1,
        PAGINATION.INVISIBLE_RANGE_SIGN,
        current - 1,
        current,
        current + 1,
        PAGINATION.INVISIBLE_RANGE_SIGN,
        total
      ];
    }
  }

  public async enterPage(page: string | number, event: any): Promise<void> {
    if (!(await canProceed(this.beforePaginationChange$))) {
      return;
    }

    page = toNumber(page);
    const isDifferentPage = page !== this.currentPage;
    const isValid = isInteger(page) && isDifferentPage;
    let shouldOverride = true;

    let newPage: number;
    if (!isValid) {
      newPage = this.currentPage;
    } else if (page < 1) {
      newPage = 1;
    } else if (page > this.totalSupportedPages) {
      newPage = this.totalSupportedPages;
      this.maxSupportedPageExceeded.emit();
    } else if (page > this.totalPages) {
      newPage = this.totalPages;
    } else {
      newPage = page;
      shouldOverride = false;
    }

    if (shouldOverride) {
      this.inputSize = newPage.toString().length;
      event.currentTarget.value = newPage;
    }

    if (newPage !== this.currentPage) {
      this.setPage(newPage);
    }
  }

  public async goToPage(page: number | string): Promise<void> {
    if (!(await canProceed(this.beforePaginationChange$))) {
      return;
    }

    page = toNumber(page);
    let newPage = this.currentPage;

    if (page > this.totalPages) {
      return;
    }

    if (page > this.totalSupportedPages) {
      this.maxSupportedPageExceeded.emit();
    } else if (page >= 1) {
      newPage = page;
    }

    if (newPage !== this.currentPage) {
      this.setPage(newPage);
    }
  }

  private setPage(page: number): void {
    this.currentPage = page;
    this.generatePagesArray();

    this.pageChange.emit({
      currentPage: this.currentPage,
      itemsPerPage: this.itemsPerPage
    });
  }

  public async goToPreviousPage(): Promise<void> {
    await this.goToPage(this.currentPage - 1);
  }

  public async goToNextPage(): Promise<void> {
    await this.goToPage(this.currentPage + 1);
  }

  public onRecordsChange(event): void {
    const newItemsPerPage = this.itemsPerPageOptions.filter(item => item.value === event.value)[0].label;
    this.itemsPerPage = newItemsPerPage;
    this.currentPage = 1;
    this.inputSize = 1;
    this.calculatePages();
  }

  public calculatePages(): void {
    this.totalPages = Math.ceil(this.totalItems / this.itemsPerPage);
    this.generatePagesArray();

    this.pageChange.emit({
      currentPage: this.currentPage,
      itemsPerPage: this.itemsPerPage
    });
  }

  public isPageSelected(pageValue: number | string): boolean {
    // eslint-disable-next-line eqeqeq
    return pageValue == this.currentPage; //NOSONAR
  }

  public isItemEmpty(pageValue: number | string): boolean {
    // eslint-disable-next-line eqeqeq
    return pageValue == PAGINATION.INVISIBLE_RANGE_SIGN; //NOSONAR
  }

  public changeInputSize(event): void {
    if (event.target.value.length !== 0) {
      this.inputSize = event.target.value.length;
    }
  }

  public async recordsPerPageOpened(): Promise<void> {
    await canProceed(this.beforePaginationChange$);
  }
}
