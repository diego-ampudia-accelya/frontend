/* eslint-disable @typescript-eslint/naming-convention */
export const PAGINATION = {
  ITEMS_PER_PAGE_DROPDOWN_VALUES: [
    {
      value: 1,
      label: 5
    },
    {
      value: 2,
      label: 10
    },
    {
      value: 3,
      label: 20
    },
    {
      value: 4,
      label: 50
    }
  ],
  VISIBLE_PAGES_THRESHOLD: 7,
  DEFAULT_PAGE_SIZE: 20,
  FIRST_PAGE: 0,
  MAX_PAGE_SIZE: 99999,
  INVISIBLE_RANGE_SIGN: '...'
};
