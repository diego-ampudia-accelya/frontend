import { DecimalPipe } from '@angular/common';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationComponent } from './pagination.component';
import { TranslatePipeMock } from '~app/test';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [DecimalPipe],
      declarations: [TranslatePipeMock, PaginationComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    component.currentPage = 2;
    component.itemsPerPage = 10;
    component.showOptions = true;
    component.totalItems = 72;
    component.itemsPerPageOptions = [
      {
        value: 1,
        label: 5
      },
      {
        value: 2,
        label: 10
      },
      {
        value: 3,
        label: 20
      },
      {
        value: 4,
        label: 50
      }
    ];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should exist', () => {
      expect(component.ngOnInit).toBeTruthy();
    });

    it('should invoke generatePagesArray()', () => {
      spyOn(component, 'initialPaginationSetup').and.callThrough();
      spyOn(component, 'generatePagesArray').and.callThrough();
      component.ngOnInit();
      expect(component.initialPaginationSetup).toHaveBeenCalledTimes(1);
    });

    it('should invoke generatePagesArray()', () => {
      spyOn(component, 'initialPaginationSetup').and.callThrough();
      spyOn(component, 'generatePagesArray').and.callThrough();
      component.ngOnInit();
      expect(component.generatePagesArray).toHaveBeenCalledTimes(1);
    });
  });

  describe('ngOnChanges()', () => {
    it('should exist', () => {
      expect(component.ngOnChanges).toBeTruthy();
    });

    it('should invoke generatePagesArray() and initialPaginationSetup() if the totalItems were changed', () => {
      spyOn(component, 'initialPaginationSetup').and.callThrough();
      spyOn(component, 'generatePagesArray').and.callThrough();
      component.totalItems = 30;
      const changes = {
        totalItems: new SimpleChange(null, component.totalItems, false)
      };
      component.ngOnChanges(changes);
      expect(component.generatePagesArray).toHaveBeenCalledTimes(1);
      expect(component.initialPaginationSetup).toHaveBeenCalledTimes(1);
    });

    it('should invoke generatePagesArray() and initialPaginationSetup() if the itemsPerPage were changed', () => {
      spyOn(component, 'initialPaginationSetup').and.callThrough();
      spyOn(component, 'generatePagesArray').and.callThrough();
      component.itemsPerPage = 20;
      const changes = {
        itemsPerPage: new SimpleChange(null, component.itemsPerPage, false)
      };
      component.ngOnChanges(changes);
      expect(component.generatePagesArray).toHaveBeenCalledTimes(1);
      expect(component.initialPaginationSetup).toHaveBeenCalledTimes(1);
    });

    it('should NOT invoke generatePagesArray() and initialPaginationSetup() if the currentPage is changed', () => {
      spyOn(component, 'initialPaginationSetup').and.callThrough();
      spyOn(component, 'generatePagesArray').and.callThrough();
      component.currentPage = 2;
      const changes = {
        currentPage: new SimpleChange(null, component.currentPage, false)
      };
      component.ngOnChanges(changes);
      expect(component.generatePagesArray).toHaveBeenCalledTimes(0);
      expect(component.initialPaginationSetup).toHaveBeenCalledTimes(0);
    });

    it('should NOT invoke generatePagesArray() and initialPaginationSetup() if the itemsPerPage is changed for the first time', () => {
      spyOn(component, 'initialPaginationSetup').and.callThrough();
      spyOn(component, 'generatePagesArray').and.callThrough();
      component.itemsPerPage = 20;
      const changes = {
        itemsPerPage: new SimpleChange(component.itemsPerPage, component.itemsPerPage, true)
      };
      component.ngOnChanges(changes);
      expect(component.generatePagesArray).toHaveBeenCalledTimes(0);
      expect(component.initialPaginationSetup).toHaveBeenCalledTimes(0);
    });
  });

  describe('generateItemsRangeString()', () => {
    it('should exist', () => {
      expect(component.generateItemsRangeString).toBeTruthy();
    });

    it(`should generate correct visibleItemsRange property
    when itemsPerPage are exact divisor of totalItems and the currentPage is the first`, () => {
      component.currentPage = 1;
      component.itemsPerPage = 10;
      component.totalItems = 100;
      component.generateItemsRangeString();
      expect(component.visibleItemsRange).toEqual('1 - 10');
    });

    it(`should generate correct visibleItemsRange property
     when itemsPerPage are exact divisor of totalItems and the currentPage is the last`, () => {
      component.currentPage = 10;
      component.itemsPerPage = 10;
      component.totalItems = 100;
      component.generateItemsRangeString();
      expect(component.visibleItemsRange).toEqual('91 - 100');
    });

    it(`should generate correct visibleItemsRange property with thousands separator`, () => {
      component.currentPage = 150;
      component.itemsPerPage = 10;
      component.totalItems = 2000;
      component.generateItemsRangeString();
      expect(component.visibleItemsRange).toEqual('1,491 - 1,500');
    });

    it(`should generate correct visibleItemsRange property
    when itemsPerPage are not exact divisor of totalItems and the currentPage is the first`, () => {
      component.currentPage = 3;
      component.itemsPerPage = 10;
      component.totalItems = 72;
      component.generateItemsRangeString();
      expect(component.visibleItemsRange).toEqual('21 - 30');
    });

    it(`should generate correct visibleItemsRange property
     when itemsPerPage are not exact divisor of totalItems and the currentPage is the last`, () => {
      component.currentPage = 8;
      component.itemsPerPage = 10;
      component.totalItems = 72;
      component.generateItemsRangeString();
      expect(component.visibleItemsRange).toEqual('71 - 72');
    });

    it(`should set the startRange to 0 if the totalItems are 0`, () => {
      component.currentPage = 1;
      component.itemsPerPage = 10;
      component.totalItems = 0;
      component.generateItemsRangeString();
      expect(component.visibleItemsRange).toEqual('0 - 0');
    });
  });

  describe('generatePagesArray()', () => {
    it('should exist', () => {
      expect(component.generatePagesArray).toBeTruthy();
    });

    it('should invoke generateNumberedPages if the pages are less than 8', () => {
      component.totalPages = 5;
      spyOn(component, 'generateNumberedPages').and.callThrough();
      spyOn(component, 'generateDottedPages').and.callThrough();
      component.generatePagesArray();
      expect(component.generateNumberedPages).toHaveBeenCalledTimes(1);
      expect(component.generateDottedPages).toHaveBeenCalledTimes(0);
    });

    it('should invoke generateDottedPages if the pages are more than 7', () => {
      component.totalPages = 15;
      spyOn(component, 'generateNumberedPages').and.callThrough();
      spyOn(component, 'generateDottedPages').and.callThrough();
      component.generatePagesArray();
      expect(component.generateNumberedPages).toHaveBeenCalledTimes(0);
      expect(component.generateDottedPages).toHaveBeenCalledTimes(1);
    });

    it('should invoke generateItemsRangeString', () => {
      component.totalPages = 15;
      spyOn(component, 'generateItemsRangeString').and.callThrough();
      component.generatePagesArray();
      expect(component.generateItemsRangeString).toHaveBeenCalledTimes(1);
    });
  });

  describe('generateNumberedPages()', () => {
    it('should exist', () => {
      expect(component.generateNumberedPages).toBeTruthy();
    });

    it('should return corect array of pages', () => {
      component.totalPages = 5;
      const result = component.generateNumberedPages();
      expect(result).toEqual([1, 2, 3, 4, 5]);
    });

    it('should return a single page when the numberedPages is empty', () => {
      component.totalPages = 0;
      const result = component.generateNumberedPages();
      expect(result).toEqual([1]);
    });
  });

  describe('generateDottedPages()', () => {
    it('should exist', () => {
      expect(component.generateDottedPages).toBeTruthy();
    });

    it('should return corect array of pages if the current page is 4 or less', () => {
      component.totalPages = 13;
      component.currentPage = 4;
      const result = component.generateDottedPages();
      expect(result).toEqual([1, 2, 3, 4, 5, '...', 13]);
    });

    it('should return corect array of pages if the current page is with offset 4 or less before the last page', () => {
      component.totalPages = 13;
      component.currentPage = 10;
      const result = component.generateDottedPages();
      expect(result).toEqual([1, '...', 9, 10, 11, 12, 13]);
    });

    it('should return corect array of pages if the current page somewhere in the middle of all pages', () => {
      component.totalPages = 13;
      component.currentPage = 7;
      const result = component.generateDottedPages();
      expect(result).toEqual([1, '...', 6, 7, 8, '...', 13]);
    });
  });

  describe('enterPage()', () => {
    it('should invoke generatePagesArray if the value is a number', async () => {
      spyOn(component, 'generatePagesArray').and.callThrough();
      await component.enterPage(3, {});
      expect(component.generatePagesArray).toHaveBeenCalledTimes(1);
    });

    it('should set the inputSize to the current page if you pass an invalid page', async () => {
      component.totalPages = 3;
      component.currentPage = 2;
      await component.enterPage(2.5, {
        currentTarget: { value: '' }
      });
      expect(component.inputSize).toBe(1);
    });

    it('should set the current page to the value', async () => {
      component.totalPages = 13;
      await component.enterPage(5, {});
      expect(component.currentPage).toBe(5);
    });

    it('should not go over the total pages', async () => {
      component.totalPages = 13;
      await component.enterPage(15, {
        currentTarget: { value: '' }
      });
      expect(component.currentPage).toBe(13);
    });

    it('should not go over the last supported page', async () => {
      component.totalPages = 10;
      component.itemsPerPage = 20;
      component.totalItems = 200;
      component.maxSupportedItems = 100;
      await component.enterPage(9, {
        currentTarget: { value: '' }
      });
      expect(component.currentPage).toBe(5);
    });

    it('should not go below the first page', async () => {
      component.totalPages = 13;
      await component.enterPage(0, {
        currentTarget: { value: '' }
      });
      expect(component.currentPage).toBe(1);
    });
  });

  describe('goToPreviousPage()', () => {
    it('should go to the previous page', async () => {
      component.currentPage = 3;
      await component.goToPreviousPage();
      expect(component.currentPage).toBe(2);
    });

    it('should not go before page 1 if the current page is 1', async () => {
      component.currentPage = 1;
      await component.goToPreviousPage();
      expect(component.currentPage).toBe(1);
    });
  });

  describe('goToNextPage()', () => {
    it('should go to the next page', async () => {
      component.currentPage = 3;
      await component.goToNextPage();
      expect(component.currentPage).toBe(4);
    });

    it('should not go to the next page if the next page is more than the total pages', async () => {
      component.totalPages = 10;
      component.currentPage = 10;
      await component.goToNextPage();
      expect(component.currentPage).toBe(10);
    });
  });

  describe('onRecordsChange()', () => {
    it('should exist', () => {
      expect(component.onRecordsChange).toBeTruthy();
    });

    it('should correctly set new itemsPerPage', () => {
      const event = {
        value: 1
      };

      component.onRecordsChange(event);
      expect(component.itemsPerPage).toBe(5);
    });

    it('should set the currentPage to 1', () => {
      const event = {
        value: 1
      };
      component.onRecordsChange(event);
      expect(component.currentPage).toBe(1);
    });

    it('should set the inputSize to 1', () => {
      const event = {
        value: 1
      };
      component.onRecordsChange(event);
      expect(component.inputSize).toBe(1);
    });

    it('should invoke calculatePages()', () => {
      const event = {
        value: 1
      };
      spyOn(component, 'calculatePages').and.callThrough();
      component.onRecordsChange(event);
      expect(component.calculatePages).toHaveBeenCalledTimes(1);
    });
  });

  describe('changeInputSize()', () => {
    it('should exist', () => {
      expect(component.changeInputSize).toBeTruthy();
    });

    it('should correctly set the input size', () => {
      const event = {
        target: {
          value: 'lokosf'
        }
      };
      component.changeInputSize(event);
      expect(component.inputSize).toBe(6);
    });
  });

  describe('calculatePages()', () => {
    it('should exist', () => {
      expect(component.calculatePages).toBeTruthy();
    });

    it('should correctly set the new total pages', () => {
      component.totalItems = 50;
      component.itemsPerPage = 20;
      component.calculatePages();
      expect(component.totalPages).toBe(3);
    });

    it('should invoke generatePagesArray', () => {
      spyOn(component, 'generatePagesArray').and.callThrough();
      component.calculatePages();
      expect(component.generatePagesArray).toHaveBeenCalledTimes(1);
    });
  });

  describe('isPageSelected()', () => {
    it('should exist', () => {
      expect(component.isPageSelected).toBeTruthy();
    });

    it('should return true if the pageValue equals the currentPage', () => {
      component.currentPage = 2;
      const result = component.isPageSelected(2);
      expect(result).toBeTruthy();
    });

    it('should return false if the pageValue does not equal the currentPage', () => {
      component.currentPage = 3;
      const result = component.isPageSelected(2);
      expect(result).toBeFalsy();
    });
  });

  describe('isItemEmpty()', () => {
    it('should exist', () => {
      expect(component.isItemEmpty).toBeTruthy();
    });

    it('should return true if the pageValue equals dots', () => {
      const result = component.isItemEmpty('...');
      expect(result).toBeTruthy();
    });

    it('should return false if the pageValue does not equal the currentPage', () => {
      const result = component.isItemEmpty('2');
      expect(result).toBeFalsy();
    });
  });
});
