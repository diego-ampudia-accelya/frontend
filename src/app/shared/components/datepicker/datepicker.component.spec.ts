import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationModule } from 'angular-l10n';
import { first, last } from 'lodash';
import moment from 'moment-mini';
import { CalendarModule } from 'primeng/calendar';

import { DatepickerComponent } from './datepicker.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';

describe('DatepickerComponent', () => {
  let component: DatepickerComponent;
  let fixture: ComponentFixture<DatepickerComponent>;
  let clock: jasmine.Clock;
  const today = moment('2020-01-01').toDate();
  let localBspTimeServiceMock: SpyObject<LocalBspTimeService>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DatepickerComponent],
      imports: [
        CommonModule,
        FormsModule,
        CalendarModule,
        HttpClientTestingModule,
        L10nTranslationModule.forRoot(l10nConfig)
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [mockProvider(LocalBspTimeService)]
    }).compileComponents();
  }));

  beforeEach(() => {
    clock = jasmine.clock();
    clock.install();
    clock.mockDate(today);

    fixture = TestBed.createComponent(DatepickerComponent);
    localBspTimeServiceMock = TestBed.inject<any>(LocalBspTimeService);
    localBspTimeServiceMock.getDate.and.returnValue(today);
    component = fixture.componentInstance;
    component.monthOptions = [{ label: 'January', value: 0 }];
    fixture.detectChanges();
  });

  afterEach(() => {
    clock.uninstall();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call focus() on ngAfterViewInit() if autofocus is `true`', () => {
    component.autofocus = true;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).toHaveBeenCalled();
  });

  it('should NOT call focus() on ngAfterViewInit() if autofocus is `false`', () => {
    component.autofocus = false;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).not.toHaveBeenCalled();
  });

  describe('when generating year options', () => {
    it('should generate all years from 1940 to current year by default', () => {
      expect(first(component.yearOptions).value).toBe('2020');
      expect(last(component.yearOptions).value).toBe('1940');
    });

    it('should generate all years from 1940 to year of maxDate', () => {
      component.maxDate = moment('2021-01-01').toDate();

      component.ngAfterViewInit();

      expect(first(component.yearOptions).value).toBe('2021');
      expect(last(component.yearOptions).value).toBe('1940');
    });

    it('should generate years between minDate and current year', () => {
      component.minDate = moment('2019-01-01').toDate();

      component.ngAfterViewInit();

      expect(first(component.yearOptions).value).toBe('2020');
      expect(last(component.yearOptions).value).toBe('2019');
    });

    it('should generate years between minDate / maxDate', () => {
      component.minDate = moment('2020-01-01').toDate();
      component.maxDate = moment('2021-01-01').toDate();

      component.ngAfterViewInit();

      expect(component.yearOptions.map(years => years.value)).toEqual(['2021', '2020']);
    });

    it('should generate a single year when minDate == maxDate', () => {
      component.minDate = moment('2020-01-01').toDate();
      component.maxDate = component.minDate;

      component.ngAfterViewInit();

      expect(component.yearOptions.map(years => years.value)).toEqual(['2020']);
    });
  });

  describe('isEnabled', () => {
    it('should return `false` when the input is disabled', () => {
      component.isDisabled = true;

      expect(component.isEnabled).toBe(false);
    });

    it('should return `false` when locked', () => {
      component.locked = true;

      expect(component.isEnabled).toBe(false);
    });

    it('should return `false` when read only', () => {
      component.isReadOnly = true;

      expect(component.isEnabled).toBe(false);
    });

    it('should return `true` when enabled, editable and unlocked', () => {
      component.isReadOnly = false;
      component.locked = false;
      component.isDisabled = false;

      expect(component.isEnabled).toBe(true);
    });
  });

  describe('canClear', () => {
    it('should return `false` when `isEnabled` is `false`', () => {
      component.clearable = true;
      component.isDisabled = true;
      component.value = new Date();

      expect(component.canClear).toBe(false);
    });

    it('should return `true` when a value is selected and editing is enabled', () => {
      component.clearable = true;
      component.value = new Date();
      component.isReadOnly = false;
      component.isDisabled = false;
      component.locked = false;

      expect(component.canClear).toBe(true);
    });

    it('should return `false` when no value is selected', () => {
      component.clearable = true;
      component.isDisabled = false;

      expect(component.canClear).toBe(false);
    });

    it('should return `false` when not clearable', () => {
      component.clearable = false;
      component.value = new Date();

      expect(component.canClear).toBe(false);
    });
  });
});
