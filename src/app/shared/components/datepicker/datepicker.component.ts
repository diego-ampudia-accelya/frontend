import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';
import { Calendar } from 'primeng/calendar';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CustomControl } from '~app/shared/base/custom-control';
import { SelectMode } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { LocalBspTimeService } from '~app/shared/services/local-bsp-time.service';
import { generateYearOptions } from '~app/shared/utils';

@Component({
  selector: 'bspl-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DatepickerComponent extends CustomControl implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @ViewChild(Calendar, { static: true }) calendar: Calendar;

  @HostBinding('class.control--open') get isCalendarOpened(): boolean {
    return this.calendar != null && !!this.calendar.overlayVisible;
  }

  @Input() dateFormat = 'dd/mm/yy';
  @Input() isTypingEnabled = false;
  @Input() dataType: 'string' | 'date' = 'date';
  @Input() showTodayButton = true;
  @Input() showTime = false;
  @Input() clearable = true;
  @Input() transformValue = true;
  @Input() viewMode = 'date';

  @Input() selectionMode: 'single' | 'range' = 'single';
  get numberOfMonths(): number {
    // Currently multi month view in case of a range picker is not supported.
    // There is a limitation that the custom header we are using is being rendered in the last month view only.
    return 1;
  }

  @Input() minDate = moment([1940]).toDate();
  @Input() maxDate = moment().endOf('year').toDate();

  @Output() closed = new EventEmitter<Event>();
  @Output() opened = new EventEmitter<Event>();

  public hoveredDate: any;
  public selectMode = SelectMode;

  public monthOptions: DropdownOption[];
  public yearOptions: DropdownOption[];

  public get startDate(): Date {
    return this.value ? this.value[0] : null;
  }

  public get endDate(): Date {
    return this.value ? this.value[1] : null;
  }

  public get previousMonth(): Date {
    return moment([this.calendar.currentYear, this.calendar.currentMonth]).subtract(1, 'month').toDate();
  }

  public get nextMonth(): Date {
    return moment([this.calendar.currentYear, this.calendar.currentMonth]).add(1, 'month').toDate();
  }

  public get canClear(): boolean {
    return this.isEnabled && this.clearable && !!this.value;
  }

  private destroyed$ = new Subject();
  private documentCaptureMouseDown = fromEvent(this.document, 'mousedown', { capture: true }).pipe(
    takeUntil(this.destroyed$)
  );

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    cd: ChangeDetectorRef,
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: Document,
    translationService: L10nTranslationService,
    private localBspTimeService: LocalBspTimeService
  ) {
    super(ngControl, cd, translationService);
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.minDate || changes.maxDate) {
      this.monthOptions = this.generateMonthOptions();
      this.yearOptions = this.generateYearOptions();
    }
  }

  public ngOnInit(): void {
    this.handleDocumentCaptureMouseDown();
  }

  public ngAfterViewInit(): void {
    this.monthOptions = this.generateMonthOptions();
    this.yearOptions = this.generateYearOptions();

    this.updateDateOptionsState();
    super.ngAfterViewInit();

    this.cd.detectChanges();
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  public writeValue(value: any) {
    // Parsing the date ourselves because primeng uses the dateFormat for both parsing and visualizing.
    // Example: Cannot have dd/mm/yy format and provide yyyy-mm-dd as string for the value.
    if (value && typeof value === 'string') {
      value = moment(value).toDate();
    }

    super.writeValue(value);
  }

  public focusElement() {
    if (this.isEnabled) {
      // Temporarily disabling it so it won't open the popup panel when the calendar is focused.
      // https://github.com/primefaces/primeng/issues/9240
      this.calendar.showOnFocus = false;
      this.calendar.inputfieldViewChild.nativeElement.focus();
      this.calendar.showOnFocus = this.isEnabled;
    }
  }

  /**
   * Determines whether today button is shown or not. Notice that it ignores time when comparing so if
   * timepicker features are added, this should be reconsidered.
   *
   * @returns true if today's date is in range `minDate <= today <= maxDate` and `showTodayButton` Input is set to true
   */
  public isTodayButtonShown(): boolean {
    let enabled = this.showTodayButton;
    const todayDate = this.localBspTimeService.getDate();
    todayDate.setHours(0, 0, 0, 0);

    if (enabled && this.minDate) {
      const minDate = new Date(this.minDate);
      minDate.setHours(0, 0, 0, 0);

      enabled = minDate <= todayDate;
    }

    if (enabled && this.maxDate) {
      const maxDate = new Date(this.maxDate);
      maxDate.setHours(0, 0, 0, 0);

      enabled = maxDate >= todayDate;
    }

    return enabled;
  }

  public onTodayButtonClicked(event) {
    const date = this.localBspTimeService.getDate();
    const dateMeta = {
      day: date.getDate(),
      month: date.getMonth(),
      year: date.getFullYear(),
      otherMonth: date.getMonth() !== this.calendar.currentMonth || date.getFullYear() !== this.calendar.currentYear,
      today: true,
      selectable: true
    };

    this.calendar.onDateSelect(event, dateMeta);
    this.calendar.onTodayClick.emit(event);
  }

  public clear(event) {
    event.stopPropagation();
    this.calendar.updateModel(null);
    this.calendar.updateInputfield();
    this.calendar.hideOverlay();
    this.calendar.onClearClick.emit(event);
  }

  public onCalendarFocused(event: FocusEvent): void {
    this.updateDateOptionsState();
    this.opened.emit(event);
  }

  public navigateToMonth(month: string): void {
    this.calendar.onMonthDropdownChange(month);
    this.updateYearsState();
  }

  public navigateToYear(year: string): void {
    this.calendar.onYearDropdownChange(year);
    this.updateMonthsState();
  }

  public updateDateOptionsState(): void {
    this.updateYearsState();
    this.updateMonthsState();
  }

  public canNavigateToPrevious(): boolean {
    return this.canNavigateTo(this.previousMonth);
  }

  public canNavigateToNext(): boolean {
    return this.canNavigateTo(this.nextMonth);
  }

  // TODO: Temporary workaround, because the BE currently work with local dates only (ex: 2018-5-20).
  public registerOnChange(fn) {
    this.propagateChange = newValue => fn(this.transformValueWithoutTimezone(newValue));
  }

  protected getCellCssClass(date: any): string {
    let result = '';
    const bspDate = this.localBspTimeService.getDate();
    const currentCellDate = this.getStripDate(new Date(date.year, date.month, date.day));

    if (this.selectionMode === 'range' && date.selectable && Array.isArray(this.value) && this.startDate) {
      const startDate = this.getStripDate(this.startDate);
      const endDate = this.getStripDate(this.endDate);

      if (endDate && startDate.valueOf() !== endDate.valueOf()) {
        if (startDate < currentCellDate && currentCellDate < endDate) {
          result = 'ui-datepicker-in-range';
        } else if (startDate.valueOf() === currentCellDate.valueOf()) {
          result = 'ui-datepicker-start-range';
        } else if (endDate.valueOf() === currentCellDate.valueOf()) {
          result = 'ui-datepicker-end-range';
        }
      }

      if (this.hoveredDate && this.endDate == null) {
        const hoveredCellDate = new Date(this.hoveredDate.year, this.hoveredDate.month, this.hoveredDate.day);

        const shouldHighlightDate = startDate < currentCellDate && currentCellDate < hoveredCellDate;
        if (shouldHighlightDate) {
          result = 'ui-datepicker-in-range-highlight';
        }
      }
    }

    if (this.isSameDate(bspDate, currentCellDate)) {
      result += ' ui-datepicker-current-date';
    }

    return result;
  }

  private transformValueWithoutTimezone(value: Date | Date[]): Date | Date[] {
    let result = value;

    if (result && this.transformValue) {
      result = Array.isArray(result)
        ? result.map(date => (date ? this.removeTimezoneFromDate(date) : null))
        : this.removeTimezoneFromDate(result);
    }

    return result;
  }

  private isSameDate(date1, date2) {
    date1 = moment(date1).format('YYYY-MM-DD');
    date2 = moment(date2).format('YYYY-MM-DD');

    return moment(date1).isSame(date2);
  }

  private removeTimezoneFromDate(date: Date): Date {
    const result = new Date(date);
    const timezoneOffset = result.getTimezoneOffset();
    result.setTime(result.getTime() - timezoneOffset * 60 * 1000);

    return result;
  }
  // TODO. End of Workaround. Remove block

  private updateYearsState(): void {
    this.yearOptions = this.yearOptions.map(o => {
      const disabled = !this.canNavigateTo(new Date(o.value, this.calendar.currentMonth));

      return { ...o, disabled };
    });
  }

  private updateMonthsState(): void {
    this.monthOptions = this.monthOptions.map(o => {
      const disabled = !this.canNavigateTo(new Date(this.calendar.currentYear, o.value));

      return { ...o, disabled };
    });
  }

  private canNavigateTo(date: Date): boolean {
    const monthStart = moment(date).startOf('month').toDate();
    const monthEnd = moment(date).endOf('month').toDate();

    const isMinDateWithinMonth = !this.minDate || (monthStart <= this.minDate && this.minDate <= monthEnd);
    const isMaxDateWithinMonth = !this.maxDate || (monthStart <= this.maxDate && this.maxDate <= monthEnd);

    return (isMinDateWithinMonth || this.minDate <= date) && (isMaxDateWithinMonth || date <= this.maxDate);
  }

  private generateMonthOptions(): DropdownOption[] {
    return this.calendar.locale.monthNames.map((month, index) => ({ label: month, value: index.toString() }));
  }

  private generateYearOptions(): DropdownOption[] {
    return generateYearOptions(this.minDate, this.maxDate);
  }

  private getStripDate(date: Date): Date {
    if (date) {
      return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    return date;
  }

  private handleDocumentCaptureMouseDown(): void {
    this.documentCaptureMouseDown.subscribe(event => {
      // FCA-5859 - The calendar relies on document.click in order to be closed. There are cases where the event propagation
      // is stopped (like in sub-menu) and therefore we need to close it ourselves during the capturing phase.
      if (this.shouldHideOverlay(event.target)) {
        this.calendar.hideOverlay();
      }
    });
  }

  private shouldHideOverlay(target: EventTarget): boolean {
    if (!this.isCalendarOpened || !(target instanceof HTMLElement)) {
      return false;
    }

    // The overlay is rendered outside the picker so we need to check it separately
    const isClickedOutsideThePicker = !this.elementRef.nativeElement.contains(target);
    const isClickedOutsideThePanel = !this.calendar.overlay.contains(target);

    return isClickedOutsideThePicker && isClickedOutsideThePanel;
  }
}
