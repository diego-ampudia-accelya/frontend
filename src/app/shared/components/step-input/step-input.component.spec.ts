import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { L10nTranslationModule } from 'angular-l10n';

import { StepInputComponent } from './step-input.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('RangeInputComponent', () => {
  let component: StepInputComponent;
  let fixture: ComponentFixture<StepInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StepInputComponent],
      imports: [FormsModule, L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepInputComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
