/* eslint-disable @angular-eslint/no-output-native */
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Optional,
  Output,
  Self,
  ViewEncapsulation
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { CustomControl } from '~app/shared/base/custom-control';

@Component({
  selector: 'bspl-step-input',
  templateUrl: './step-input.component.html',
  styleUrls: ['./step-input.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StepInputComponent extends CustomControl implements AfterViewInit {
  @Output() enterKeyUp = new EventEmitter<KeyboardEvent>();
  @Output() blur = new EventEmitter<Event>();
  @Output() focus = new EventEmitter<Event>();

  @Input() min: number;
  @Input() max: number;
  @Input() step = 1;

  public get isReadonly(): boolean {
    return this.isReadOnly || this.locked;
  }

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    cd: ChangeDetectorRef,
    translationService: L10nTranslationService
  ) {
    super(ngControl, cd, translationService);
  }

  public ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  public focusElement(): void {
    // TODO Implement
  }

  public onBlur(event): void {
    this.propagateTouched();
    this.blur.emit(event);
  }

  public onFocus(event): void {
    this.focus.emit(event);
  }

  public onEnterKeyUp(event): void {
    this.enterKeyUp.emit(event);
  }
}
