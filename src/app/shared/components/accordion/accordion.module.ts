import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';

import { AccordionItemSelectionComponent } from './accordion-item-selection/accordion-item-selection.component';
import { AccordionItemComponent } from './accordion-item/accordion-item.component';
import { AccordionComponent } from './accordion.component';

@NgModule({
  declarations: [AccordionComponent, AccordionItemComponent, AccordionItemSelectionComponent],
  imports: [CommonModule, L10nTranslationModule],
  exports: [AccordionComponent, AccordionItemComponent, AccordionItemSelectionComponent]
})
export class AccordionModule {}
