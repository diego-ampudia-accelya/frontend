import {
  AfterContentInit,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  QueryList
} from '@angular/core';
import { merge } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

import { AccordionItemComponent } from './accordion-item/accordion-item.component';

const minScrollOffset = 12;
@Component({
  selector: 'bspl-accordion',
  templateUrl: './accordion.component.html'
})
export class AccordionComponent implements AfterContentInit, OnDestroy {
  @ContentChildren(AccordionItemComponent) items: QueryList<AccordionItemComponent>;

  @Input() multipleItemsOpen = false;

  @Output() itemToggled = new EventEmitter<AccordionItemComponent>();
  @Output() isReady = new EventEmitter<boolean>();

  private _scrollOffset = minScrollOffset;
  @Input() set scrollOffset(headerHeight: number) {
    // 12px to give some margin between the section beginning and status header ending
    this._scrollOffset = headerHeight + minScrollOffset;
  }

  private destroy$ = new EventEmitter<boolean>();

  public ngAfterContentInit() {
    this.setUpItems();
    this.isReady.emit(true);
  }

  public expandFirstItem() {
    this.items.first.open = true;
  }

  public collapseItems() {
    this.setOpenItems(false);
  }

  public expandItems() {
    this.setOpenItems(true);
  }

  public goToItem(item: AccordionItemComponent) {
    if (item.open) {
      this.scrollToItem(item);
    } else {
      //* Once the item is open, we scroll
      item.toggleEmitter.pipe(take(1)).subscribe(() => this.scrollToItem(item));
      item.open = true;
    }
  }

  public ngOnDestroy() {
    this.destroy$.emit(true);
  }

  private setUpItems() {
    if (this.items.toArray().every(item => typeof item.open === 'undefined')) {
      //* Every open Input is undefined, we open the first one by default
      this.expandFirstItem();
    }

    merge(...this.items.map(item => item.toggleEmitter.asObservable()))
      .pipe(takeUntil(this.destroy$))
      .subscribe(accordionItem => this.itemToggled.emit(accordionItem));

    if (!this.multipleItemsOpen) {
      this.initializeMultipleItemListener();
    }
  }

  private initializeMultipleItemListener() {
    this.itemToggled.pipe(takeUntil(this.destroy$)).subscribe(item => {
      if (item.open) {
        this.collapseItems();
        item.open = true;
      }
    });
  }

  private setOpenItems(open: boolean) {
    this.items.forEach(item => {
      item.open = open;
    });
  }

  private scrollToItem(item: AccordionItemComponent) {
    const elementRect: Partial<DOMRect> = item.nativeElement.getBoundingClientRect();
    const bodyRect: Partial<DOMRect> = document.body.getBoundingClientRect();
    const topOffset = elementRect.top - bodyRect.top;

    scrollTo({ top: topOffset - this._scrollOffset, left: 0, behavior: 'smooth' });
  }
}
