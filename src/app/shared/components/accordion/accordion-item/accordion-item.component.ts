import { animate, sequence, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'bspl-accordion-item',
  templateUrl: './accordion-item.component.html',
  styleUrls: ['./accordion-item.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('*', style({ height: '*', opacity: 1 })),
      state('void', style({ height: 0, opacity: 0 })),
      transition(
        ':leave',
        sequence([
          style({ overflow: 'hidden' }),
          animate(400, style({ height: 0, opacity: 0 })),
          style({ overflow: 'initial' })
        ])
      ),
      transition(
        ':enter',
        sequence([
          style({ overflow: 'hidden' }),
          animate(400, style({ height: '*', opacity: 1 })),
          style({ overflow: 'initial' })
        ])
      )
    ])
  ]
})
export class AccordionItemComponent implements OnChanges {
  //* This element is set when view is ready after changing the open attribute.
  //* I believe it's a better approach for performance than doing operations on afterviewchecked
  @ViewChild('openListener')
  set listener(listener) {
    this.toggleEmitter.emit(this);
  }

  @ViewChild('iconTempl', { static: true }) iconTemplate: TemplateRef<any>;

  @Input() open: boolean;
  @Input() title: string;
  @Input() titleTemplate: TemplateRef<any>;

  @Input() showInfoIcon = false;
  @Input() showWarningIcon = false;
  @Input() showErrorIcon = false;

  @Output() toggleEmitter: EventEmitter<AccordionItemComponent> = new EventEmitter<AccordionItemComponent>();

  constructor(private element: ElementRef) {}

  public ngOnChanges(): void {
    if (
      (this.showInfoIcon || this.showWarningIcon || this.showErrorIcon) &&
      (!this.titleTemplate || this.titleTemplate === this.iconTemplate)
    ) {
      this.titleTemplate = this.iconTemplate;
    } else {
      this.titleTemplate = null;
    }
  }

  public get nativeElement() {
    return this.element.nativeElement;
  }

  public onClick() {
    this.open = !this.open;
  }
}
