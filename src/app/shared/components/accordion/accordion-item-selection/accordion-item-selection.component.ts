import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
import { merge } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AccordionComponent } from '../accordion.component';

@Component({
  selector: 'bspl-accordion-item-selection',
  templateUrl: './accordion-item-selection.component.html',
  styleUrls: ['./accordion-item-selection.component.scss']
})
export class AccordionItemSelectionComponent implements OnInit, OnDestroy {
  @Input() accordion: AccordionComponent;
  @Input() showStatusIcons = false;
  @Input() showNavSection = true;

  private destroy$ = new EventEmitter<boolean>();

  private _collapseAll: boolean;
  public get collapseAll() {
    return this._collapseAll;
  }
  public set collapseAll(collapseAll: boolean) {
    const previousCollapseAll = this._collapseAll;
    this._collapseAll = collapseAll;

    //* We throw ChangeDetection only if it is needed
    if (previousCollapseAll !== collapseAll) {
      this.cd.detectChanges();
    }
  }

  constructor(private cd: ChangeDetectorRef) {}

  public ngOnInit(): void {
    if (this.accordion) {
      this.initializeListeners();
    }
  }

  public onCollapseClick() {
    if (this.collapseAll) {
      this.accordion.collapseItems();
    } else {
      this.accordion.expandItems();
    }

    this._collapseAll = !this._collapseAll;
  }

  public goToSection(item) {
    this.accordion.goToItem(item);
  }

  public ngOnDestroy(): void {
    this.destroy$.emit(true);
  }

  private initializeListeners() {
    //* Setting collapseAll value when accordion is ready and also when items are toggled
    merge(this.accordion.isReady, this.accordion.itemToggled)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.updateCollapseAll());
  }

  private getCollapseAll(): boolean {
    let itemsOpened = 0;
    this.accordion.items.toArray().forEach(item => (itemsOpened += item.open ? 1 : 0));

    return itemsOpened > this.accordion.items.length / 2;
  }

  private updateCollapseAll() {
    this.collapseAll = this.getCollapseAll();
  }
}
