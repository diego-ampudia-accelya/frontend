import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AccordionItemSelectionComponent } from './accordion-item-selection.component';
import { TranslatePipeMock } from '~app/test';

describe('AccordionItemSelectionComponent', () => {
  let component: AccordionItemSelectionComponent;
  let fixture: ComponentFixture<AccordionItemSelectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AccordionItemSelectionComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionItemSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
