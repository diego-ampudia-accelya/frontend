import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PermissionsViewComponent } from './permissions-view.component';
import { SharedModule } from '~app/shared/shared.module';

@NgModule({
  declarations: [PermissionsViewComponent],
  imports: [SharedModule, CommonModule],
  exports: [PermissionsViewComponent]
})
export class PermissionsViewModule {}
