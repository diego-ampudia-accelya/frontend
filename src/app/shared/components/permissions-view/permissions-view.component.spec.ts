import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { cloneDeep } from 'lodash';

import { PermissionsViewComponent } from './permissions-view.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED } from '~app/shared/mocks/permission-mocks';
import { SharedModule } from '~app/shared/shared.module';
import { setUpTestBedBeforeAll } from '~app/shared/utils/test-utils';

describe('GroupPermissionsComponent', () => {
  let component: PermissionsViewComponent;
  let fixture: ComponentFixture<PermissionsViewComponent>;

  setUpTestBedBeforeAll({
    imports: [SharedModule, L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule],
    declarations: [PermissionsViewComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionsViewComponent);
    component = fixture.componentInstance;
  });

  it('should exist', () => {
    expect(component).toBeTruthy();
  });

  it('should generate side menu items', () => {
    component.menuItems = [];
    component.permissions = cloneDeep(PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED);
    component.generateSideMenu();
    expect(component.menuItems).toEqual([{ title: 'User Management' }, { title: 'Group Management' }]);
  });

  it('should set options for the selected tab', () => {
    component.permissions = cloneDeep(PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED);
    component.activeMenuItem = { title: 'User Management' };
    component.showItemsForSelectedTab();
    expect(component.permissionsTabOptions).toEqual(PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED['User Management']);
  });

  it('should set the active item', () => {
    component.permissions = cloneDeep(PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED);
    component.activeMenuItem = { title: 'Another item ' };
    component.selectMenuItem({ item: { title: 'Group Management' } });
    expect(component.activeMenuItem).toEqual({ title: 'Group Management' });
  });

  it('should toggle the permission value', () => {
    component.permissions = cloneDeep(PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED);
    component.activeMenuItem = { title: 'Group Management' };
    component.toggleOption({ id: 3, enabled: true });
    const result = (PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED['Group Management'][0].enabled = true);
    expect(result).toEqual(true);
  });
});
