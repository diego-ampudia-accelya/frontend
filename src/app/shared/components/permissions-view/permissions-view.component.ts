import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { isEmpty } from 'lodash';

import { Permission, PermissionSaveBEModel } from '~app/shared/models/permissions.model';

@Component({
  selector: 'bspl-permissions-view',
  templateUrl: './permissions-view.component.html',
  styleUrls: ['./permissions-view.component.scss']
})
export class PermissionsViewComponent implements OnInit, OnChanges {
  @Input() public permissions: Record<string, Permission[]>;
  @Input() public sideMenuTitle: string;
  public permissionsNotAvailableMessage = 'Permissions not available.';
  public menuItems = [];
  public activeMenuItem = { title: '' };
  public permissionsTabOptions = [];

  public get permissionsAvailable(): boolean {
    return !isEmpty(this.permissions);
  }

  constructor(public translationService: L10nTranslationService) {}

  ngOnInit() {
    this.generateSideMenu();
  }

  ngOnChanges(change: SimpleChanges) {
    if (change && change.permissions && !change.permissions.firstChange) {
      this.generateSideMenu();
    }
  }

  generateSideMenu() {
    if (!isEmpty(this.permissions)) {
      this.menuItems = Object.keys(this.permissions).map((key: string) => ({
        title: key
      }));
      this.activeMenuItem = this.menuItems[0];
      this.showItemsForSelectedTab();
    }
  }

  showItemsForSelectedTab() {
    this.permissionsTabOptions = this.permissions[this.activeMenuItem.title];
  }

  selectMenuItem({ item }) {
    this.activeMenuItem = item;
    this.showItemsForSelectedTab();
  }

  toggleOption(value: PermissionSaveBEModel) {
    const permissionsForActiveTab = this.permissions[this.activeMenuItem.title];

    permissionsForActiveTab.filter(permission => permission.id === value.id)[0].enabled = value.enabled;
  }
}
