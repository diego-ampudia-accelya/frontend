import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TagComponent } from './tag.component';

describe('TagComponent', () => {
  let component: TagComponent;
  let fixture: ComponentFixture<TagComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TagComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onRemove', () => {
    it('should emit remove event if closeable is true', () => {
      spyOn(component.remove, 'emit');
      component.closeable = true;

      component.onRemove(null);

      expect(component.remove.emit).toHaveBeenCalled();
    });

    it('should not emit remove event if closeable is false', () => {
      spyOn(component.remove, 'emit');
      component.closeable = false;

      component.onRemove(null);

      expect(component.remove.emit).not.toHaveBeenCalled();
    });
  });

  it('should enable contentEditable tag if enableTagEditing is set to true', () => {
    component.enableEditing = true;

    const event = new MouseEvent('dbclick');
    const target = {
      parentElement: {
        classList: {
          value: '',
          add: () => {}
        }
      },
      focus: () => {}
    };
    Object.defineProperty(event, 'target', { value: target, enumerable: true });

    component.onDoubleClick(event);

    expect(component.contentEditableEnabled).toBe(true);
  });

  it('should not enable contentEditable tag if enableTagEditing is set to false', () => {
    component.enableEditing = false;
    const event = new MouseEvent('dbclick');

    component.onDoubleClick(event);

    expect(component.contentEditableEnabled).toBe(false);
  });

  it('should set contentEditable to false on change and to emit new value', () => {
    component.contentEditableEnabled = true;
    spyOn(component.valueChanged, 'emit');

    const event = new MouseEvent('blur');
    const target = {
      innerText: 'newValue'
    };
    Object.defineProperty(event, 'target', { value: target, enumerable: true });

    component.onChange(event);

    expect(component.contentEditableEnabled).toBe(false);
    expect(component.valueChanged.emit).toHaveBeenCalledWith('newValue');
  });
});
