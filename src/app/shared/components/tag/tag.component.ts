import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'bspl-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent {
  @Input() value = '';
  @Input() closeable = true;
  @Input() showTooltip = true;
  @Input() state: 'accent' | 'default' | 'invalid' = 'accent';
  @Input() isDisabled: boolean;
  @Input() enableEditing: boolean;

  @Output() remove = new EventEmitter<string>();
  @Output() valueChanged = new EventEmitter<string>();

  public contentEditableEnabled = false;

  constructor(private cd: ChangeDetectorRef) {}

  public onRemove(event: Event) {
    if (!this.closeable) {
      return;
    }

    if (event != null) {
      event.stopPropagation();
    }

    this.remove.emit(this.value);
  }

  public getTooltipValue() {
    return this.showTooltip ? this.value : '';
  }

  public onChange(event) {
    this.contentEditableEnabled = false;

    this.valueChanged.emit(event.target.innerText);
  }

  public preventSelection(event: MouseEvent): void {
    const clicksCount = event.detail;
    if (!this.contentEditableEnabled && clicksCount > 1) {
      // Prevents selection of the text when the user enters the edit mode.
      event.preventDefault();
    }
  }

  public onDoubleClick(event: MouseEvent): void {
    if (this.enableEditing) {
      const element = event.target as HTMLDivElement;
      const parentClassList = element.parentElement.classList;

      parentClassList.add('content-editable-container');

      this.contentEditableEnabled = true;
      this.cd.detectChanges();
      element.focus();
    }
  }
}
