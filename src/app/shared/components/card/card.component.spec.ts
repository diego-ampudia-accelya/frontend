import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { TranslatePipeMock } from '~app/test';

describe('CardComponent', () => {
  let component: CardComponent;
  let nativeElement: HTMLElement;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent, TranslatePipeMock]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    nativeElement = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render div with class bspl-card', () => {
    const div = nativeElement.querySelector('div.bspl-card');

    expect(div).toBeTruthy();
  });

  it('should render header container', () => {
    const div = nativeElement.querySelector('div div.header');

    expect(div).toBeTruthy();
  });

  it('should render header title, when title is set', () => {
    component.title = 'Test Title';
    fixture.detectChanges();

    const titleElement = nativeElement.querySelector('div div.header h2');

    expect(titleElement).toBeTruthy();
    expect(titleElement.textContent).toBe('Test Title');
  });

  it('should render content div', () => {
    const contentElement = nativeElement.querySelector('div div.card-content');

    expect(contentElement).toBeTruthy();
  });
});
