import { Component, HostBinding, Input } from '@angular/core';

import { BadgeInfoType } from '~app/shared/enums/badge-information-type';

@Component({
  selector: 'bspl-badge-information',
  templateUrl: './badge-information.component.html',
  styleUrls: ['./badge-information.component.scss']
})
export class BadgeInformationComponent {
  @Input() value: string;
  @Input() tooltipLabel?: string;
  @Input() type = BadgeInfoType.regular;
  @Input() showIconType = true;

  public iconTypeMapper = {
    [BadgeInfoType.regular]: 'info_outline',
    [BadgeInfoType.info]: 'schedule',
    [BadgeInfoType.success]: 'done',
    [BadgeInfoType.error]: 'delete',
    [BadgeInfoType.warning]: 'error'
  };

  @HostBinding('class.regularBadge') get isRegularBadge() {
    return this.type && this.type === BadgeInfoType.regular;
  }

  @HostBinding('class.successBadge') get isSuccessBadge() {
    return this.type && this.type === BadgeInfoType.success;
  }

  @HostBinding('class.errorBadge') get isErrorBadge() {
    return this.type && this.type === BadgeInfoType.error;
  }

  @HostBinding('class.infoBadge') get isInfoBadge() {
    return this.type && this.type === BadgeInfoType.info;
  }

  @HostBinding('class.warningBadge') get isWarningBadge() {
    return this.type && this.type === BadgeInfoType.warning;
  }
}
