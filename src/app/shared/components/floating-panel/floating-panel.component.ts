import { Component, ElementRef, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'bspl-floating-panel',
  templateUrl: './floating-panel.component.html',
  styleUrls: ['./floating-panel.component.scss']
})
export class FloatingPanelComponent implements OnInit {
  @Input() fullWidth = true;
  @Input() closable = false;

  @Output() opened = new EventEmitter();
  @Output() closed = new EventEmitter();

  private _isFloating = false;
  public get isFloating(): boolean {
    return this._isFloating;
  }

  @HostBinding('class.opened')
  private _isOpened = false;
  public get isOpened(): boolean {
    return this._isOpened;
  }

  constructor(private element: ElementRef<HTMLElement>) {}

  public ngOnInit(): void {
    if (!this.closable) {
      this.open();
    }
  }

  @HostListener('document:scroll')
  public onScroll(): void {
    this.updateFloatingState();
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.updateFloatingState();
  }

  public open(): void {
    if (this.isOpened) {
      return;
    }

    this._isOpened = true;
    this.updateFloatingState();

    this.opened.emit();
  }

  public close(): void {
    if (!this.isOpened || !this.closable) {
      return;
    }

    this._isOpened = false;
    this.closed.emit();
  }

  public toggle(): void {
    if (this.isOpened) {
      this.close();
    } else {
      this.open();
    }
  }

  private updateFloatingState(): void {
    if (!this.isOpened) {
      return;
    }

    const selfRect = this.element.nativeElement.getBoundingClientRect();
    const parentRect = this.element.nativeElement.parentElement.getBoundingClientRect();

    // Note: This solution won't cover the case when the panel is in the middle of its parent container.
    // If needed consider using https://developers.google.com/web/updates/2017/09/sticky-headers.
    this._isFloating =
      Math.round(selfRect.bottom) < Math.round(parentRect.bottom) &&
      Math.round(selfRect.top) > Math.round(parentRect.top);
  }
}
