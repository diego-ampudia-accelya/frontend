import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FloatingPanelComponent } from './floating-panel.component';

describe('FloatingPanelComponent', () => {
  let component: FloatingPanelComponent;
  let fixture: ComponentFixture<FloatingPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FloatingPanelComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatingPanelComponent);
    component = fixture.componentInstance;
    component.closable = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the panel initially if closable is set to false', () => {
    component.closable = true;
    component.close();

    component.closable = false;
    component.ngOnInit();

    expect(component.isOpened).toBe(true);
  });

  it('should open floating panel on open method', () => {
    spyOn(component.opened, 'emit');
    component.open();
    expect(component.isOpened).toBeTruthy();
    expect(component.opened.emit).toHaveBeenCalled();
  });

  it('should not emit opened event multiple times if the panel is already opened', () => {
    spyOn(component.opened, 'emit');
    component.open();
    component.open();

    expect(component.opened.emit).toHaveBeenCalledTimes(1);
  });

  it('should close panel if it is open', () => {
    component.open();

    spyOn(component.closed, 'emit');

    component.close();

    expect(component.isOpened).toBeFalsy();
    expect(component.closed.emit).toHaveBeenCalled();
  });

  it('should not emit closed if panel is not opened', () => {
    spyOn(component.closed, 'emit');

    component.close();

    expect(component.closed.emit).not.toHaveBeenCalled();
  });

  it('should call open method if panel is not opened', () => {
    spyOn(component, 'open');
    spyOn(component, 'close');

    component.toggle();

    expect(component.open).toHaveBeenCalled();
    expect(component.close).not.toHaveBeenCalled();
  });

  it('should call close method if panel is already opened', () => {
    component.open();

    spyOn(component, 'open');
    spyOn(component, 'close');

    component.toggle();

    expect(component.open).not.toHaveBeenCalled();
    expect(component.close).toHaveBeenCalled();
  });

  it('should update floating state on scroll', () => {
    component.open();

    spyOn<any>(component, 'updateFloatingState');
    component.onScroll();

    expect(component['updateFloatingState']).toHaveBeenCalled();
  });

  it('should update floating state on window resize', () => {
    component.open();

    spyOn<any>(component, 'updateFloatingState');
    component.onWindowResize();

    expect(component['updateFloatingState']).toHaveBeenCalled();
  });
});
