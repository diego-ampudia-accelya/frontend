import { FormControl } from '@angular/forms';

import { DropdownOption } from '~app/shared/models/dropdown-option.model';

export interface InputSelectOption {
  dropdownOption: DropdownOption; //* Value-Label pair to be used in select
  input: FormControl; //* Control instance (good for special behaviour or validators)
  placeholder?: string; //* Optinal placeholder that associated input will have
}
