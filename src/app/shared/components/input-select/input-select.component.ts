import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  QueryList,
  Self,
  SimpleChanges,
  ViewChild,
  ViewChildren,
  ViewEncapsulation
} from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { merge, Subscription } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { InputComponent } from '../input/input.component';
import { InputSelectOption } from './models/input-select.model';
import { CustomControl } from '~app/shared/base/custom-control';
import { SelectComponent } from '~app/shared/components/select/select.component';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Component({
  selector: 'bspl-input-select',
  templateUrl: './input-select.component.html',
  styleUrls: ['./input-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class InputSelectComponent extends CustomControl implements OnInit, OnDestroy, OnChanges, AfterViewInit {
  @ViewChild(SelectComponent) select: SelectComponent;
  @ViewChildren(InputComponent) inputs: QueryList<InputComponent>;

  @Input() options: InputSelectOption[] = [];

  /**
   * If true, it does not reset input controls when Dropdown option changes
   */
  @Input() keepInputValues = false;

  /**
   * If true and no value is provided, then the first option is selected
   */
  @Input() selectFirstOption = true;

  /**
   * If true, the input is focussed when dropdown option change
   */
  @Input() focusOnChange = true;

  //* Overriding getter/setter of value
  //* We also need to set the dropdown according to value provided
  get value(): InputSelectOption {
    return this._value;
  }
  set value(value: InputSelectOption) {
    this._value = value;
    this.propagateChange(this._value);
    this.dropdownControl.setValue(value ? value.dropdownOption.value : value, { emitEvent: false });
  }

  public dropdownOptions: DropdownOption[];
  public dropdownControl = new FormControl(null);
  public dropdownClick = new EventEmitter<Event>();

  private subscriptions: Subscription[] = [];

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    cd: ChangeDetectorRef,
    translationService: L10nTranslationService
  ) {
    super(ngControl, cd, translationService);
  }

  ngOnInit() {
    if (this.options) {
      this.initializeComponent();
    }
  }

  public ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.options && !changes.options.firstChange) {
      this.initializeComponent();
    }
  }

  private initializeComponent() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.subscriptions = [];

    this.initializeDropdown();
    this.initializeChangesListener();
    this.initializeStatusListener();
  }

  private initializeDropdown() {
    this.dropdownOptions = this.options.map(inputOption => inputOption.dropdownOption);

    if (this.selectFirstOption && !this.value) {
      this.value = this.options[0];
    }
  }

  private initializeChangesListener() {
    this.subscriptions.push(
      this.dropdownControl.valueChanges.subscribe(value => {
        if (!this.keepInputValues && this.value) {
          //* We reset previous selected option
          const controlUnchanged = this.value.input.valid && !this.value.input.value;
          this.value.input.reset(null, { emitEvent: !controlUnchanged });
        }

        //* Setting value and propagating the change
        this.value = this.options.find(inputOption => inputOption.dropdownOption.value === value);
      })
    );

    if (this.focusOnChange) {
      //* Apparently, waiting for blur or close events from dropdown to focus the input does not work
      //* Instead, this workaround using click works with no problems
      this.subscriptions.push(
        this.dropdownControl.valueChanges
          .pipe(switchMap(() => this.dropdownClick.asObservable().pipe(first())))
          .subscribe(() => this.focusElement())
      );
    }
  }

  private initializeStatusListener() {
    this.subscriptions.push(
      merge(...this.options.map(option => option.input.statusChanges)).subscribe(() => this.cd.markForCheck())
    );
  }

  private getCurrentInput(): InputComponent {
    return this.inputs ? this.inputs.toArray()[this.options.indexOf(this.value)] : null;
  }

  public isInputOptionShown(inputOption: InputSelectOption) {
    //* Returns true if the inputOption corresponds with dropdown selected option
    return this.dropdownControl.value === inputOption.dropdownOption.value;
  }

  public focusElement() {
    if (!this.isEnabled) {
      return;
    }

    //* Calls focus method in the input with same index as selected dropdown option
    //* It also filters first the options, so we only have the ones with inputs associated
    const inputSelected = this.getCurrentInput();

    if (inputSelected) {
      inputSelected.focusElement();
    }
  }

  public openSelect() {
    if (this.select) {
      this.select.focusElement();
    }
  }

  public writeValue(value: InputSelectOption) {
    super.writeValue(value);
    this.dropdownControl.setValue(value ? value.dropdownOption.value : null, { emitEvent: false });
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach(sub => sub.unsubscribe());
    }
  }
}
