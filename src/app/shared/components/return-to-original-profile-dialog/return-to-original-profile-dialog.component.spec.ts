import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService, L10N_LOCALE } from 'angular-l10n';

import { DialogConfig, FooterButton } from '..';
import { ReactiveSubject } from '../dialog/reactive-subject';

import { ReturnToOriginalProfileDialogComponent } from './return-to-original-profile-dialog.component';
import { TranslatePipeMock } from '~app/test';
import { createAirlineUser } from '~app/shared/mocks/airline-user';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('ReturnToOriginalProfileDialogComponent', () => {
  let component: ReturnToOriginalProfileDialogComponent;
  let fixture: ComponentFixture<ReturnToOriginalProfileDialogComponent>;

  const initialState = {
    auth: {
      user: createAirlineUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Apply,
        texts: {
          description: '',
          header: ''
        },
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Apply
          }
        ]
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ReturnToOriginalProfileDialogComponent, TranslatePipeMock],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject,
        provideMockStore({ initialState }),
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        },
        { provide: L10N_LOCALE, useValue: { language: 'en' } }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnToOriginalProfileDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
