import { Component, Inject } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { DialogConfig } from '../dialog/dialog.config';
import { getUser } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { User } from '~app/shared/models/user.model';

@Component({
  selector: 'bspl-return-to-original-profile-dialog.component',
  templateUrl: './return-to-original-profile-dialog.component.html',
  styleUrls: ['./return-to-original-profile-dialog.component.scss']
})
export class ReturnToOriginalProfileDialogComponent {
  public get loggedUser$(): Observable<User> {
    return this.store.pipe(select(getUser), first());
  }

  constructor(
    public config: DialogConfig,
    public translationService: L10nTranslationService,
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private store: Store<AppState>
  ) {}
}
