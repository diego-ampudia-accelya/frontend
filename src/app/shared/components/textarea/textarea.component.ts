import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
  ViewChild
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';

import { CustomControl } from '~app/shared/base/custom-control';
import { customWordWrap } from '~app/shared/utils/custom-wordwrap';

@Component({
  selector: 'bspl-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent extends CustomControl implements OnInit, AfterViewInit {
  @Input() showNote = false;

  @Input() rowsLimit: number;
  @Input() linesLimit: number;

  @Input() textareaMaxLength: number;
  @Input() rows = 1;

  @Output() onblur = new EventEmitter<Event>();
  @Output() onfocus = new EventEmitter<Event>();

  @ViewChild('textarea') public textarea: ElementRef;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    @Self() @Optional() public ngControl: NgControl,
    cd: ChangeDetectorRef,
    translationService: L10nTranslationService
  ) {
    super(ngControl, cd, translationService);
  }

  ngOnInit() {
    if (!this.textareaMaxLength) {
      this.textareaMaxLength = this.rowsLimit * this.linesLimit;
    }
  }

  public ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  public writeValue(value: any) {
    super.writeValue(value);

    this.rows = value ? this.getRowsFromFormattedLines() : 1;
  }

  public focusElement(): void {
    if (!this.isEnabled) {
      return;
    }

    this.textarea.nativeElement.focus();
  }

  public onChange() {
    if (this.linesLimit && this.rowsLimit) {
      this.applyCustomTextFormat();
    }
  }

  public applyCustomTextFormat(): void {
    const formattedLines = this.getFormattedLines();
    this.rows = this.getRowsFromFormattedLines(formattedLines);

    this._value = formattedLines.join('');
  }

  public onBlur(event: KeyboardEvent) {
    this.propagateTouched();

    this.onblur.emit(event);
  }

  public onFocus(event: any) {
    this.onfocus.emit(event);
  }

  private getRowsFromFormattedLines(formattedLines?: string[]) {
    if (!formattedLines) {
      formattedLines = this.getFormattedLines();
    }

    return formattedLines.filter(line => line === '\n').length + 1;
  }

  private getFormattedLines(): string[] {
    return this._value && typeof this._value === 'string'
      ? customWordWrap(this._value, this.linesLimit, this.rowsLimit)
      : [];
  }
}
