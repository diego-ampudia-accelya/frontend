import { Component, Input, OnChanges } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

@Component({
  selector: 'bspl-error-field',
  templateUrl: './error-field.component.html',
  styleUrls: ['./error-field.component.scss']
})
export class ErrorFieldComponent implements OnChanges {
  @Input() errors?: ValidationErrors;

  @Input() message;

  public errorString: string;

  constructor(private translationService: L10nTranslationService) {}

  ngOnChanges() {
    this.errorString = this.message || (this.errors && this.getValidatorsErrors());
  }

  private getValidatorsErrors(): string {
    let errorString = '';

    const translate = this.translationService.translate.bind(this.translationService);

    if (this.errors.required) {
      errorString += translate('FORM_CONTROL_VALIDATORS.required', {}, 'en') + '\n';
    }
    if (this.errors.pattern) {
      errorString += translate('FORM_CONTROL_VALIDATORS.pattern') + '\n';
    }
    if (this.errors.minlength) {
      errorString += `${translate('FORM_CONTROL_VALIDATORS.minlength', this.errors.minlength)} \n`;
    }
    if (this.errors.maxLength) {
      errorString += `${translate('FORM_CONTROL_VALIDATORS.maxlength', this.errors.maxLength)} \n`;
    }
    if (this.errors.min) {
      errorString += `${translate('FORM_CONTROL_VALIDATORS.min', this.errors.min)} \n`;
    }
    if (this.errors.max) {
      errorString += `${translate('FORM_CONTROL_VALIDATORS.max', this.errors.max)} \n`;
    }
    if (this.errors.customError) {
      errorString += translate(this.errors.customError.message) + '\n';
    }
    if (this.errors.emailWithDomain) {
      errorString += translate('FORM_CONTROL_VALIDATORS.email') + '\n';
    }
    if (this.errors.validateDays) {
      errorString += translate('FORM_CONTROL_VALIDATORS.validateDays') + '\n';
    }
    if (this.errors.backendError) {
      errorString += this.errors.backendError.message + '\n';
    }

    return errorString;
  }
}
