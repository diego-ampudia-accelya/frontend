import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { GRID_TABLE } from '../grid-table/grid-table.constants';
import { SearchPanelComponent } from '../search-panel/search-panel.component';
import { ViewListComponent } from './view-list.component';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { AppState } from '~app/reducers';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { SortingObject } from '~app/shared/models/sorting-object.model';
import { ApiService } from '~app/shared/services/api.service';
import { DataSource } from '~app/shared/services/data-source.service';
import { setUpTestBedBeforeAll } from '~app/shared/utils/test-utils';

// TODO: Shall this be moved in a separate place?
class ApiServiceMock extends ApiService<unknown> {
  constructor(httpClient: HttpClient) {
    super(httpClient, '');
  }
}

const createSpyObj = jasmine.createSpyObj;

const translationServiceSpy = createSpyObj('L10nTranslationService', ['translate']);
const routerSpy = createSpyObj('Router', ['navigate']);
const activatedRouteSpy = {};
const httpClientSpy = jasmine.createSpyObj<HttpClient>('HttpClient', ['get']);
httpClientSpy.get.and.returnValue(of());
const storeSpy = jasmine.createSpyObj<Store<AppState>>('Store', ['dispatch', 'select', 'pipe']);
storeSpy.pipe.and.returnValue(of());

const apiServiceMock = new ApiServiceMock(httpClientSpy);
const dataSourceMock = new DataSource(apiServiceMock);

describe('ViewListComponent', () => {
  let component: ViewListComponent;
  let fixture: ComponentFixture<ViewListComponent>;

  setUpTestBedBeforeAll({
    declarations: [ViewListComponent],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [
      { provide: L10nTranslationService, useValue: translationServiceSpy },
      { provide: Router, useValue: routerSpy },
      { provide: ActivatedRoute, useValue: activatedRouteSpy },
      { provide: HttpClient, useValue: httpClientSpy },
      { provide: Store, useValue: storeSpy }
    ]
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewListComponent);
    component = fixture.componentInstance;

    component.apiService = apiServiceMock;
    component.dataSource = dataSourceMock;
    component.masterDataType = MasterDataType.GDS;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onTableSortChange', () => {
    it('should set downloadQuery sortBy property', () => {
      component.onTableSortChange([{ attribute: 'column', sortType: SortOrder.Asc }]);
      expect(component.downloadQuery.sortBy).toEqual([{ attribute: 'column', sortType: SortOrder.Asc }]);
    });
  });

  describe('onCreate', () => {
    it('should invoke onActionClick', () => {
      spyOn(component, 'onActionClick');
      component.onCreate();
      expect(component.onActionClick).toHaveBeenCalled();
    });
  });

  describe('remove filters', () => {
    it('should invoke apply filters if there are items in the search panel component', () => {
      spyOn(component, 'applyFilters');
      component.searchPanel = new SearchPanelComponent();
      component.removeFilter();
      expect(component.applyFilters).toHaveBeenCalled();
    });

    it('should NOT invoke apply filters if the search panel component is not initialized', () => {
      spyOn(component, 'applyFilters');
      component.searchPanel = null;
      component.removeFilter();
      expect(component.applyFilters).not.toHaveBeenCalled();
    });
  });

  describe('applyFilters', () => {
    it('should set downloadQuery filterBy property', () => {
      const searchCriteria = {
        iataCode: 'BGB',
        country: 'Bulgaria'
      };

      component.applyFilters(searchCriteria);

      expect(component.downloadQuery.filterBy).toEqual({
        iataCode: 'BGB',
        country: 'Bulgaria'
      });
    });
  });

  describe('setSorting', () => {
    it('should set sorting when there is value', () => {
      component.sorting = [];
      const value: Array<SortingObject> = [
        { attribute: 'test 1', sortType: SortOrder.Asc },
        { attribute: 'test 2', sortType: SortOrder.Asc }
      ];
      component.setSorting(value);
      expect(component.sorting).toEqual(value);
    });
  });

  describe('setPagingSizeAndPage', () => {
    it('should set paging size and page when passed as parameters', () => {
      component.paging = { ...GRID_TABLE.DEFAULT_PAGE_INFO };
      component.setPagingSizeAndPage({ size: 10, page: 2, totalElements: 100 });

      expect(component.paging).toEqual({ size: 10, page: 2, totalElements: 0 });
    });

    it('should set default paging size and page when NO parameters are passed', () => {
      component.paging = { ...GRID_TABLE.DEFAULT_PAGE_INFO };
      component.setPagingSizeAndPage();
      expect(component.paging).toEqual(GRID_TABLE.DEFAULT_PAGE_INFO);
    });
  });

  describe('filterBtnClick', () => {
    it('should NOT do anything if there are no filters', () => {
      spyOn(component.filterButtonClicked, 'emit');
      component.hasFiltering = false;
      component.filterBtnClick();
      expect(component.filterButtonClicked.emit).not.toHaveBeenCalled();
    });
  });

  describe('onCheckboxChange', () => {
    it('should emit checkbox rows', () => {
      spyOn(component.checkboxChange, 'emit');
      component.onCheckboxChange(123);
      expect(component.checkboxChange.emit).toHaveBeenCalled();
    });
  });

  describe('clearSearchPanelState', () => {
    it('should invoke clearState on searchPanel when search panel is not null', () => {
      component.searchPanel = new SearchPanelComponent();
      spyOn(component.searchPanel, 'clearState');
      component.clearSearchPanelState();
      expect(component.searchPanel.clearState).toHaveBeenCalled();
    });

    it('should always set isSearchPanelVisible to false', () => {
      component.searchPanel = new SearchPanelComponent();
      spyOn(component.searchPanel, 'clearState');
      component.clearSearchPanelState();
      expect(component.isSearchPanelVisible).toBe(false);
    });
  });

  describe('onGetServiceActions', () => {
    it('should emit id', () => {
      spyOn(component.actionsMenuClick, 'emit');
      component.onGetServiceActions({ id: 123 });
      expect(component.actionsMenuClick.emit).toHaveBeenCalled();
    });
  });

  describe('toggleAdvanceSearchPanel', () => {
    it('should emit event', () => {
      spyOn(component.toggleAdvanceSearch, 'emit');
      component.toggleAdvanceSearchPanel(true);
      expect(component.toggleAdvanceSearch.emit).toHaveBeenCalled();
    });

    it('should set advanced search value to match the event', () => {
      spyOn(component.toggleAdvanceSearch, 'emit');
      component.toggleAdvanceSearchPanel(true);
      expect(component.showAdvancedSearch).toBe(true);
    });
  });

  describe('downloadItems', async () => {
    it('should not open download dialog when hasDowlonading is false', () => {
      spyOn(component.dialogService, 'open');
      component.hasDownloading = false;
      component.downloadItems();
      expect(component.dialogService.open).not.toHaveBeenCalled();
    });
  });
});
