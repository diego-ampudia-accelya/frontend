import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup, FormGroupDirective, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TableColumn } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { delay, first, switchMap, tap } from 'rxjs/operators';

import { GRID_TABLE } from '../grid-table/grid-table.constants';
import { SearchPanelComponent } from '../search-panel/search-panel.component';
import { ViewListActions } from '~app/core/actions';
import { getActiveTab, getViewListState } from '~app/core/reducers';
import { MasterList } from '~app/master-shared/master-list/master-list.class';
import { AppState } from '~app/reducers';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { DownloadFileComponent } from '~app/shared/components/form-dialog-components/download-file/download-file.component';
import { GridTableComponent } from '~app/shared/components/grid-table/grid-table.component';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { GridTableActionType } from '~app/shared/models/grid-table-actions.model';
import { PaginationInfo } from '~app/shared/models/pagination-info.model';
import { RequestQueryBody } from '~app/shared/models/request-query-body.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { SortingObject } from '~app/shared/models/sorting-object.model';
import { ApiService } from '~app/shared/services/api.service';
import { DataSource } from '~app/shared/services/data-source.service';
import { ViewListService } from '~app/shared/services/view-list.service';
import { canProceed } from '~app/shared/utils';

const clearIcon = 'mi md-18 clear';
const filterIcon = 'mi md-18 filter_list';

@Component({
  selector: 'bspl-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.scss'],
  providers: [ViewListService]
})
export class ViewListComponent extends MasterList implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  // TODO: How to handle generics in inputs?
  @Input() apiService: ApiService<any>;
  @Input() dataSource: DataSource<any>;
  @Input() columns: TableColumn[] = [];
  @Input() rowDetailTemplate: TemplateRef<any>;
  // ToDo: Add proper type for predefinedFilters
  @Input() predefinedFilters = [];
  @Input() primaryActions = [];
  @Input() showIconEmptyActions: boolean;
  @Input() title: string;
  @Input() createBtnLabel: string;
  @Input() hasFiltering = true;
  @Input() hasDownloading = true;
  @Input() hasCreating = true;
  @Input() showAdvancedSearchBtn = false;
  @Input() showAdvancedSearch = false;
  @Input() searchCriteriaKeyMappers: { [key: string]: (value: any) => string } = {};
  @Input() searchCriteriaValueMappers: { [key: string]: (value: any) => string } = {};
  @Input() downloadDialogTitle = this.translationService.translate('common.download');

  /**
   * Async action which will be executed before loading the data. The result indicates whether the action should be executed.
   */
  @Input() beforeDataLoad$: Observable<boolean>;

  @Output() tableRowLinkClick = new EventEmitter();
  @Output() toggleAdvanceSearch = new EventEmitter();
  @Output() checkboxChange = new EventEmitter();
  @Output() actionsMenuClick = new EventEmitter();
  @Output() filterButtonClicked = new EventEmitter();

  @ViewChild(SearchPanelComponent, { static: true }) searchPanel: SearchPanelComponent;
  @ViewChild(GridTableComponent, { static: true }) gridTable: GridTableComponent;

  // TODO: Search panel cannot access ContentChild within others component ng-content. Research how to fix it.
  @ContentChild(NgForm) private ngForm: NgForm; // Used in case of template driven forms.
  @ContentChild(FormGroupDirective) private formGroupDirective: FormGroupDirective; // Used in case of reactive driven forms.

  public paging: PaginationInfo = GRID_TABLE.DEFAULT_PAGE_INFO;
  public sorting: Array<SortingObject> = [];
  public sortOrder: SortOrder = SortOrder.Asc;
  public sortedColumn: string;

  public isSearchPanelVisible = false;
  public btnDesign = ButtonDesign;
  public searchForm: NgForm | FormGroup;
  public filter: RequestQueryFilter[] | unknown = [];
  public downloadQuery: RequestQueryBody = {
    filterBy: [],
    paginateBy: {
      page: PAGINATION.FIRST_PAGE,
      size: PAGINATION.MAX_PAGE_SIZE
    },
    sortBy: []
  };

  private subscriptions: Array<Subscription> = [];

  private id = '';

  private activeTab$ = this.store.pipe(
    select(getActiveTab),
    first(),
    tap(({ id }) => (this.id = id))
  );

  private get stateId(): string {
    return `${this.id}:${this.masterDataType}`;
  }

  get filterIcon() {
    return !this.isSearchPanelVisible || this.searchPanel.collapsed ? filterIcon : clearIcon;
  }

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private viewListService: ViewListService,
    public dialogService: DialogService,
    public translationService: L10nTranslationService,
    public route: ActivatedRoute,
    public router: Router,
    private store: Store<AppState>
  ) {
    super(dialogService, translationService, route, router);
  }

  public ngOnInit(): void {
    this.validateRequiredInputs();
    this.applyStoreState();
    super.ngOnInit();
  }

  applyStoreState() {
    this.activeTab$
      .pipe(switchMap(activeTab => this.store.pipe(select(getViewListState(this.stateId)), first())))
      .subscribe(viewListInfo => {
        if (viewListInfo) {
          if (viewListInfo.filter) {
            this.setFilter(viewListInfo.filter);
          }

          if (viewListInfo.sorting) {
            this.setSorting(viewListInfo.sorting);
          }

          if (viewListInfo.paging) {
            this.setPagingSizeAndPage(viewListInfo.paging as any);
          }
        }
      });
  }

  setFilter(value: any[] | { [key: string]: any }): void {
    const filterValue = Array.isArray(value)
      ? [...this.predefinedFilters, ...value]
      : { ...this.predefinedFilters, ...value };

    this.filter = cloneDeep(filterValue);
    this.downloadQuery.filterBy = cloneDeep(filterValue);
  }

  setSorting(value: Array<SortingObject>) {
    if (value) {
      this.sorting = [...value];
      this.downloadQuery.sortBy = [...value];

      // TODO change this logic, when sorting on multiple columns is provided
      const firstSortingObject = this.sorting[0];
      if (firstSortingObject) {
        this.sortedColumn = firstSortingObject.attribute;
        this.sortOrder = firstSortingObject.sortType;
      }
    }
  }

  setPagingSizeAndPage({ size, page } = GRID_TABLE.DEFAULT_PAGE_INFO) {
    this.paging = { ...this.paging, size, page };
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription && subscription.unsubscribe());
  }

  public ngAfterViewInit() {
    this.initializeSearchForm();
    this.changeDetectorRef.detectChanges();

    this.store
      .pipe(
        select(getViewListState(this.stateId)),
        first(),
        delay(0) // ToDo: find why delay 0 is needed to apply change with the filter from the store
      )
      .subscribe(viewListState => {
        if (viewListState) {
          if (viewListState.filterFormValue) {
            this.searchForm.setValue(viewListState.filterFormValue);
          }
          if (viewListState.filter) {
            this.filter = viewListState.filter;
          }
          this.showSearchPanelIfHasAppliedFilter();
        }
      });
  }

  showSearchPanelIfHasAppliedFilter() {
    const searchPanel = this.searchPanel;
    searchPanel.contentCollapsable.close();
    this.isSearchPanelVisible = searchPanel.hasAppliedFilter;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const predefinedFilters = changes.predefinedFilters;

    if (predefinedFilters != null && Object.keys(predefinedFilters.currentValue).length > 0) {
      this.resetFilter();

      // Reset currently applied filters if new filter is applied from outside
      this.clearSearchPanelState();

      this.showSearchPanelIfHasAppliedFilter();
    }
  }

  public onCreate(): void {
    this.onActionClick({
      action: { actionType: GridTableActionType.Add },
      event: null,
      row: null
    });
  }

  /**
   * Removes specific filter from the existing filters.
   * ToDo: Rename this.filter to this.filters
   */
  public removeFilter(): void {
    if (this.searchPanel) {
      this.applyFilters(this.searchPanel.searchCriteria);
    }
  }

  public applyFilters(searchCriteria): void {
    const formattedFilter = this.getFormattedFilter(searchCriteria);
    this.setFilter(formattedFilter);

    this.store.dispatch(
      new ViewListActions.FilterChange({
        viewListId: this.stateId,
        filter: this.filter
      })
    );

    this.store.dispatch(
      new ViewListActions.FilterFormValueChange({
        viewListId: this.stateId,
        value: (this.searchForm && this.searchForm.value) || {}
      })
    );

    if (this.searchPanel != null && !this.searchPanel.hasAppliedFilter) {
      this.isSearchPanelVisible = false;
    }
  }

  public getFormattedFilter(searchCriteria): RequestQueryFilter[] | any {
    if (this.viewListService.config) {
      return this.viewListService.formatRequestFilters(searchCriteria);
    }

    return this.apiService.formatFilter(searchCriteria);
  }

  public filterBtnClick(): void {
    if (!this.hasFiltering) {
      return;
    }

    this.isSearchPanelVisible = !this.isSearchPanelVisible || this.searchPanel.hasAppliedFilter;

    if (this.searchPanel.collapsed || this.searchPanel.hasAppliedFilter) {
      this.searchPanel.contentCollapsable.toggle();
    }

    this.filterButtonClicked.emit(this.isSearchPanelVisible);
  }

  public onTableSortChange(sorting: SortingObject[] = []) {
    this.downloadQuery.sortBy = sorting;

    this.store.dispatch(
      new ViewListActions.SortChange({
        viewListId: this.stateId,
        sorting
      })
    );
  }

  public onTablePageChange(paging: any): void {
    this.store.dispatch(
      new ViewListActions.PageChange({
        viewListId: this.stateId,
        paging
      })
    );
  }

  public onCheckboxChange(checkboxRows): void {
    this.checkboxChange.emit(checkboxRows);
  }

  public async downloadItems(): Promise<void> {
    if (!this.hasDownloading || !this.gridTable.hasData) {
      return;
    }

    if (!(await canProceed(this.beforeDataLoad$))) {
      return;
    }

    this.dialogService.open(DownloadFileComponent, {
      data: {
        title: this.downloadDialogTitle,
        footerButtonsType: FooterButton.Download,
        downloadQuery: this.downloadQuery
      },
      apiService: this.apiService
    });
  }

  /**
   * This will not trigger a search or emit a clear event.
   */
  public clearSearchPanelState() {
    this.isSearchPanelVisible = false;
    if (this.searchPanel != null) {
      this.searchPanel.clearState();
    }
  }

  public onGetServiceActions({ id }) {
    this.actionsMenuClick.emit(id);
  }

  public toggleAdvanceSearchPanel(event) {
    this.showAdvancedSearch = event;
    this.toggleAdvanceSearch.emit(event);
  }

  private resetFilter() {
    const emptyFilterValue = Array.isArray(this.predefinedFilters) ? [] : {};
    this.setFilter(emptyFilterValue);
  }

  private validateRequiredInputs() {
    if (this.hasDownloading && this.apiService == null) {
      throw new Error('apiService is required when downloading is enabled.');
    }

    if (this.dataSource == null) {
      throw new Error('dataSource is required.');
    }
  }

  private initializeSearchForm(): void {
    if (this.ngForm) {
      this.searchForm = this.ngForm;
    } else if (this.formGroupDirective) {
      this.searchForm = this.formGroupDirective.control;
    }
  }
}
