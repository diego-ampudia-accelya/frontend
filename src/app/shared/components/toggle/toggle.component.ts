import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'bspl-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ToggleComponent),
      multi: true
    }
  ]
})
export class ToggleComponent implements ControlValueAccessor, OnInit {
  @Input() checked: boolean;
  @Input() label: string;
  @Input() disabled: boolean;
  @Input() readonly: boolean;
  @Input() labelPosition: 'top' | 'right' = 'top';
  @Input() showValueInLabel = false; // Flag to show value with the label
  @Input() onLabel = 'toggle.onDefault'; // Label that it's appear when checked is true
  @Input() offLabel = 'toggle.offDefault'; // Label that it's appear when checked is false

  @Output() valueChange = new EventEmitter();

  public valueLabel = this.offLabel;

  public onChange = (_: any) => {};
  public onTouch = () => {};

  public ngOnInit() {
    this.updateValueLabel();
  }

  public handleChange(event: any) {
    this.checked = event.checked;
    this.updateValueLabel();
    this.onChange(this.checked);
    this.valueChange.emit(this.checked);
  }

  // Methods that comply with ControlValueAccessor

  // Allows Angular to update the model (checked).
  // Update the model and changes needed for the view here.
  public writeValue(value: boolean): void {
    this.checked = !!value;
  }

  // Allows Angular to register a function to call when the model (checked) changes.
  // Save the function as a property to call later here.
  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the component has been touched.
  // Save the function as a property to call later here.
  public registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  // Allows Angular to disable the component
  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private updateValueLabel() {
    this.valueLabel = this.checked ? this.onLabel : this.offLabel;
  }
}
