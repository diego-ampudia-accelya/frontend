import { ChangeModel } from './change.model';

export interface ChangeGroupModel {
  title: string;
  items: ChangeModel[];
}
