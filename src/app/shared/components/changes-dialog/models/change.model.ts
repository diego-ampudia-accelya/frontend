export interface ChangeModel {
  name: string;
  value: string;
  originalValue?: any;
  group: string;
  tooltip?: string;
}
