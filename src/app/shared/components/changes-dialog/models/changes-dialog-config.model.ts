import { DialogConfig } from '../../dialog/dialog.config';

import { ChangeModel } from './change.model';

export interface ChangesDialogConfig extends DialogConfig {
  message: string;
  changes: ChangeModel[];
  hasInvalidChanges?: boolean;
  invalidChangesMessage?: string;
}
