/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Inject, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { chain, isBoolean } from 'lodash';

import { FooterButton } from '../buttons/enums/footerButton';
import { DialogConfig } from '../dialog/dialog.config';
import { ChangeModel } from './models/change.model';
import { ChangesDialogConfig } from './models/changes-dialog-config.model';
import { ChangeGroupModel } from '~app/shared/components/changes-dialog/models/change-group.model';
import { AlertMessageType } from '~app/shared/enums';
import { BadgeInfoType } from '~app/shared/enums/badge-information-type';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-changes-dialog',
  templateUrl: './changes-dialog.component.html',
  styleUrls: ['./changes-dialog.component.scss']
})
export class ChangesDialogComponent implements OnInit {
  // All value templates must be static: true to prevent an `ExpressionChangedAfterItHasBeenCheckedError`.
  @ViewChild('defaultTmpl', { static: true })
  defaultTmpl: TemplateRef<any>;

  @ViewChild('booleanTmpl', { static: true })
  booleanTmpl: TemplateRef<any>;

  @ViewChild('pillsTmpl', { static: true })
  pillsTmpl: TemplateRef<any>;

  public groupedItems: ChangeGroupModel[];
  public BadgeInfoType = BadgeInfoType;
  public AlertMessageType = AlertMessageType;
  public hideValues: boolean;
  public usePillsForChanges: boolean;

  constructor(@Inject(DialogConfig) public config: ChangesDialogConfig) {}

  public ngOnInit(): void {
    this.groupedItems = this.toViewModel(this.config.changes);

    const applyButton: ModalAction = this.config.data?.buttons?.find(
      (b: ModalAction) => b.type === FooterButton.Apply || b.type === FooterButton.ApplyChanges
    );

    if (applyButton && !applyButton.isDisabled) {
      applyButton.isDisabled = this.config.hasInvalidChanges;
    }

    this.hideValues = this.config.hideValues;
    this.usePillsForChanges = this.config.usePillsForChanges;
  }

  public getValueTemplate(change: ChangeModel): TemplateRef<any> {
    const originalValue = change.originalValue;
    let resulTemplate: TemplateRef<any> = this.defaultTmpl;

    if (isBoolean(originalValue)) {
      resulTemplate = this.booleanTmpl;
    }

    if (this.usePillsForChanges) {
      resulTemplate = this.pillsTmpl;
    }

    return resulTemplate;
  }

  private toViewModel(changes: ChangeModel[]): ChangeGroupModel[] {
    return chain(changes)
      .groupBy('group')
      .entries()
      .map(([title, items]) => ({ title, items }))
      .value();
  }
}
