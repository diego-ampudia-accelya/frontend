import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FooterButton } from '../buttons/enums/footerButton';
import { DialogConfig } from '../dialog/dialog.config';

import { ChangesDialogComponent } from './changes-dialog.component';
import { ChangeModel } from './models/change.model';
import { TranslatePipeMock } from '~app/test';

describe('ChangesDialogComponent', () => {
  let component: ChangesDialogComponent;
  let fixture: ComponentFixture<ChangesDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ChangesDialogComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: DialogConfig, useValue: {} }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should group changes into one group when changes have the same value for the `group` property', () => {
    const changes: ChangeModel[] = [
      { group: 'single-group', name: 'test-1', value: 'value-1' },
      { group: 'single-group', name: 'test-2', value: 'value-2' }
    ];
    component.config = {
      data: null,
      changes,
      message: ''
    };

    component.ngOnInit();

    const expected: any[] = [{ title: 'single-group', items: changes }];
    expect(component.groupedItems).toEqual(expected);
  });

  it('should group changes into two groups when changes have the two different values for the `group` property', () => {
    const firstGroup: ChangeModel[] = [
      { group: 'first-group', name: 'test-1', value: 'value-1' },
      { group: 'first-group', name: 'test-3', value: 'value-3' }
    ];
    const secondGroup: ChangeModel[] = [{ group: 'second-group', name: 'test-2', value: 'value-2' }];
    const changes = [...firstGroup, ...secondGroup];
    component.config = {
      data: null,
      changes,
      message: ''
    };

    component.ngOnInit();

    const expected: any[] = [
      { title: 'first-group', items: firstGroup },
      { title: 'second-group', items: secondGroup }
    ];
    expect(component.groupedItems).toEqual(expected);
  });

  it('should show no changes when none are provided', () => {
    component.config = {
      data: null,
      changes: null,
      message: ''
    };

    component.ngOnInit();

    expect(component.groupedItems).toEqual([]);
  });

  it('should disable the apply changes button if there are invalid changes', () => {
    component.config = {
      data: {
        footerButtonsType: [],
        buttons: [
          {
            type: FooterButton.Apply,
            isDisabled: false
          }
        ]
      },
      changes: null,
      hasInvalidChanges: true,
      message: ''
    };

    component.ngOnInit();

    expect(component.config.data.buttons[0].isDisabled).toEqual(true);
  });

  it('should return boolean template for boolean values', () => {
    const booleanChange: ChangeModel = { group: 'single-group', name: 'test-1', value: 'false', originalValue: false };

    expect(component.getValueTemplate(booleanChange)).toBe(component.booleanTmpl);
  });

  it('should return default template for string values', () => {
    const stringChange: ChangeModel = { group: 'single-group', name: 'test-2', value: 'value-2' };

    expect(component.getValueTemplate(stringChange)).toBe(component.defaultTmpl);
  });

  it('should return pills template when usePillsForChanges is true', () => {
    component.config = {
      data: null,
      changes: null,
      message: '',
      usePillsForChanges: true
    };

    component.ngOnInit();

    const pillChange: ChangeModel = {
      group: 'single-group',
      name: 'pill-name',
      value: 'pill-value',
      tooltip: 'pill-tooltip'
    };

    expect(component.getValueTemplate(pillChange)).toBe(component.pillsTmpl);
  });
});
