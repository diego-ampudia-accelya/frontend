import { Component } from '@angular/core';

import { DialogConfig } from '../dialog/dialog.config';

@Component({
  selector: 'bspl-route-not-found-dialog',
  templateUrl: './route-not-found-dialog.component.html',
  styleUrls: ['./route-not-found-dialog.component.scss']
})
export class RouteNotFoundDialogComponent {
  constructor(public config: DialogConfig) {}
}
