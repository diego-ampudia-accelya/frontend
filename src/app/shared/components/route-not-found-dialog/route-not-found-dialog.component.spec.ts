import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogConfig, FooterButton } from '..';
import { ReactiveSubject } from '../dialog/reactive-subject';

import { RouteNotFoundDialogComponent } from './route-not-found-dialog.component';
import { TranslatePipeMock } from '~app/test';

describe('RouteNotFoundDialogComponent', () => {
  let component: RouteNotFoundDialogComponent;
  let fixture: ComponentFixture<RouteNotFoundDialogComponent>;

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Apply,
        texts: {
          description: '',
          header: ''
        },
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Apply
          }
        ]
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [RouteNotFoundDialogComponent, TranslatePipeMock],
      providers: [{ provide: DialogConfig, useValue: mockConfig }, ReactiveSubject],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteNotFoundDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
