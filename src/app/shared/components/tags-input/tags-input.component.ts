import { AfterViewInit, ChangeDetectorRef, Component, Input, Optional, Self, ViewChild } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { L10nTranslationService } from 'angular-l10n';
import { uniq } from 'lodash';

import { CustomControl } from '~app/shared/base/custom-control';

@Component({
  selector: 'bspl-tags-input',
  templateUrl: './tags-input.component.html',
  styleUrls: ['./tags-input.component.scss']
})
export class TagsInputComponent extends CustomControl implements AfterViewInit {
  @ViewChild(NgSelectComponent, { static: true }) tagInput: NgSelectComponent;

  @Input() enableTagEditing = true;
  @Input() additionalTagSeparators: string[];

  private defaultTagSeparators = ['Enter', 'Tab'];

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    cd: ChangeDetectorRef,
    translationService: L10nTranslationService
  ) {
    super(ngControl, cd, translationService);
  }

  public get allTagSeparators(): string[] {
    return [...this.defaultTagSeparators, ...(this.additionalTagSeparators || [])];
  }

  public keyDownFn = (event: KeyboardEvent): boolean => {
    let shouldAllowOriginalHandler = true;

    // We need to handle adding a tag on Enter, Tab ourselves in order to support pasting text with special separators.
    if (event.key === 'Enter' || event.key === 'Tab') {
      shouldAllowOriginalHandler = false;
    }

    return shouldAllowOriginalHandler;
  };

  public ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  public focusElement(): void {
    if (!this.isEnabled) {
      return;
    }

    this.tagInput.focus();
  }

  public registerOnChange(fn) {
    this.tagInput?.registerOnChange(fn);
    super.registerOnChange(fn);
  }

  public registerOnTouched(fn) {
    this.tagInput?.registerOnTouched(fn);
    super.registerOnTouched(fn);
  }

  public updateTagsValue(previous: string, updated: string): void {
    this.value = uniq(
      this.value.map(v => {
        if (v === previous) {
          return updated;
        }

        return v;
      })
    );
  }

  public getTagState(value: string): string {
    const formControl = this.ngControl?.control;
    const isInvalid = formControl?.validator?.(new FormControl(value));

    return isInvalid ? 'invalid' : 'default';
  }

  public onBlur(): void {
    this.handleAutoTagCreation();
    this.propagateTouched();
  }

  public handleAutoTagCreation(event?: KeyboardEvent, value: string = this.tagInput.searchTerm): void {
    if (event && this.allTagSeparators.every(s => s !== event.key)) {
      return;
    }

    const separators = this.allTagSeparators?.filter(s => s !== 'Enter' && s !== 'Tab').join('');
    const regex = separators ? new RegExp(`[${separators}]+`) : null;
    const tags = value
      ?.split(regex)
      .map(v => v.trim())
      .filter(v => v);

    tags?.forEach(tag => this.selectTag(tag));
  }

  public handleTab(event: KeyboardEvent): void {
    if (this.tagInput.searchTerm) {
      event.preventDefault();
    }
  }

  private selectTag(value: string = this.tagInput.searchTerm): void {
    this.tagInput.searchTerm = value?.trim();
    const canAddTag = this.tagInput.searchTerm && this.tagInput.showAddTag;
    if (canAddTag) {
      this.tagInput.selectTag();
    }
  }
}
