import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormControl, NgControl, Validators } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { L10nTranslatePipe, L10nTranslationService } from 'angular-l10n';
import { MockDeclaration } from 'ng-mocks';

import { TagsInputComponent } from './tags-input.component';
import { multiValueValidator } from '~app/shared/validators/multi-value.validator';

describe('TagsInputComponent', () => {
  let component: TagsInputComponent;
  let fixture: ComponentFixture<TagsInputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TagsInputComponent, MockDeclaration(L10nTranslatePipe)],
      providers: [mockProvider(L10nTranslationService)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsInputComponent);
    component = fixture.componentInstance;

    component.writeValue(['Test']);
    component.tagInput = createSpyObject(NgSelectComponent);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call focus() on ngAfterViewInit() if autofocus is `true`', () => {
    component.autofocus = true;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).toHaveBeenCalled();
  });

  it('should NOT call focus() on ngAfterViewInit() if autofocus is `false`', () => {
    component.autofocus = false;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).not.toHaveBeenCalled();
  });

  it('should call ngSelect registerOnChange', () => {
    const fn = () => {};

    component.registerOnChange(fn);

    expect(component.tagInput.registerOnChange).toHaveBeenCalled();
  });

  it('should call ngSelect registerOnTouched', () => {
    const fn = () => {};

    component.registerOnTouched(fn);

    expect(component.tagInput.registerOnTouched).toHaveBeenCalled();
  });

  it('should update current tag value with the new one', () => {
    component.value = ['one', 'two'];

    component.updateTagsValue('one', 'test1');

    expect(component.value).toEqual(['test1', 'two']);
  });

  it('should value contains only unique elements', () => {
    component.value = ['one', 'two'];

    component.updateTagsValue('two', 'one');

    expect(component.value).toEqual(['one']);
  });

  it('should return default state if there is no ngControl', () => {
    expect(component.getTagState('test')).toEqual('default');
  });

  it('should return default state if there are no validators', () => {
    component.ngControl = createSpyObject(NgControl, {
      control: new FormControl(['test'])
    });

    expect(component.getTagState('test')).toEqual('default');
  });

  it('should return default state if passed value is valid', () => {
    component.ngControl = createSpyObject(NgControl, {
      control: new FormControl(['qq@asa.com'], multiValueValidator(Validators.email))
    });

    expect(component.getTagState('ss@aa.com')).toEqual('default');
  });

  it('should return invalid state if passed value is invalid', () => {
    component.ngControl = createSpyObject(NgControl, {
      control: new FormControl(['qq@asa.com'], multiValueValidator(Validators.email))
    });

    expect(component.getTagState('test')).toEqual('invalid');
  });
});
