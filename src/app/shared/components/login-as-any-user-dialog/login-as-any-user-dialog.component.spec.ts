import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { DialogConfig, FooterButton } from '..';
import { ReactiveSubject } from '../dialog/reactive-subject';
import { LoginAsAnyUserDialogComponent } from './login-as-any-user-dialog.component';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { createAirlineUser } from '~app/shared/mocks/airline-user';
import { AppConfigurationService } from '~app/shared/services';
import { TranslatePipeMock } from '~app/test';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('LoginAsAnyUserDialogComponent', () => {
  let component: LoginAsAnyUserDialogComponent;
  let fixture: ComponentFixture<LoginAsAnyUserDialogComponent>;

  const initialState = {
    auth: {
      user: createAirlineUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Apply,
        texts: {
          description: '',
          header: ''
        },
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Apply
          }
        ]
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [LoginAsAnyUserDialogComponent, TranslatePipeMock],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject,
        provideMockStore({ initialState }),
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        PermissionsService,
        FormBuilder
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAsAnyUserDialogComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
