import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { FooterButton } from '../buttons/enums/footerButton';
import { DialogConfig } from '../dialog/dialog.config';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Permissions } from '~app/shared/constants';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { MainUserInfo, UserType } from '~app/shared/models/user.model';
import { UserProfileService } from '~app/user-profile/services/user-profile.service';

export interface IataUserImpersonationViewModel {
  selectedUserType: UserType;
  selectedMainUser: MainUserInfo;
}

@Component({
  selector: 'bspl-login-as-any-user-dialog.component',
  templateUrl: './login-as-any-user-dialog.component.html',
  styleUrls: ['./login-as-any-user-dialog.component.scss'],
  providers: [UserProfileService]
})
export class LoginAsAnyUserDialogComponent implements OnInit, OnDestroy {
  public userTypesDropdown: DropdownOption[];
  public mainUsersDropdown$: Observable<DropdownOption<MainUserInfo>[]>;
  public form: FormGroup;
  public showMainUsersDropdown = false;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public Permissions = Permissions;

  private formFactory: FormUtil;
  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    public translationService: L10nTranslationService,
    private userProfileService: UserProfileService,
    private fb: FormBuilder,
    private permissionsService: PermissionsService
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  ngOnInit() {
    this.buildForm();
    this.initializeUserTypesDropdown();
    this.initializeUserTypeChangesListener();
    this.initializeMainUserChangesListener();
  }

  private initializeUserTypesDropdown(): void {
    this.userTypesDropdown = [
      {
        value: 0,
        label: this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.airline')
      },
      {
        value: 1,
        label: this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.agent')
      },
      {
        value: 3,
        label: this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.dpc')
      },
      {
        value: 5,
        label: this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.gds')
      },
      {
        value: 7,
        label: this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.thirdParty')
      },
      {
        value: 8,
        label: this.translationService.translate(
          'headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.agentGroupPrimaryAccount'
        )
      }
    ];

    const canImpersonateHOMU = this.permissionsService.hasPermission(Permissions.impersonateHOMU);

    if (canImpersonateHOMU) {
      this.userTypesDropdown.push({
        value: 20,
        label: this.translationService.translate('headerUserInfo.loginAsAnyUserDialog.userTypesDropdown.homu')
      });
    }
  }

  private buildForm() {
    this.form = this.formFactory.createGroup<IataUserImpersonationViewModel>({
      selectedUserType: ['', []],
      selectedMainUser: ['', []]
    });
  }

  private initializeMainUserChangesListener(): void {
    this.form
      .get('selectedMainUser')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.enableLoginButton();
      });
  }

  private initializeUserTypeChangesListener(): void {
    this.form
      .get('selectedUserType')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(userType => {
        this.resetForm();
        // For DPC (value = 3) users the main users request wont be called
        if (userType !== 3) {
          // The dropdown must be hidden if userType is DPC (value = 3)
          this.showMainUsersDropdown = true;

          this.mainUsersDropdown$ = this.userProfileService.getMainUsersByUserType(userType).pipe(
            map((mainUsers: MainUserInfo[]) =>
              mainUsers.map(mainUser => ({
                value: mainUser,
                label: mainUser.userCode
              }))
            )
          );
        } else {
          this.setDPCForm();
        }
      });
  }

  private setDPCForm(): void {
    this.showMainUsersDropdown = false;
    this.enableLoginButton();
  }

  private resetForm(): void {
    this.form.get('selectedMainUser').reset({ emitEvent: false });
    this.disableLoginButton();
  }

  private enableLoginButton() {
    const button = this.config.data.buttons.find(actionButton => actionButton.type === FooterButton.Login);
    button.isDisabled = false;
  }

  private disableLoginButton() {
    const button = this.config.data.buttons.find(actionButton => actionButton.type === FooterButton.Login);
    button.isDisabled = true;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
