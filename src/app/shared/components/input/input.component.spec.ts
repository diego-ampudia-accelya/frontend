import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { L10nTranslationModule } from 'angular-l10n';

import { InputComponent } from './input.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('InputComponent', () => {
  let component: InputComponent;
  let fixture: ComponentFixture<InputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InputComponent],
      imports: [FormsModule, L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call focus() on ngAfterViewInit() if autofocus is `true`', () => {
    component.autofocus = true;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).toHaveBeenCalled();
  });

  it('should NOT call focus() on ngAfterViewInit() if autofocus is `false`', () => {
    component.autofocus = false;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).not.toHaveBeenCalled();
  });

  describe('input type number', () => {
    beforeEach(() => {
      component.type = 'number';
    });

    it('should not allow setting a value below the minimum', () => {
      component.min = 0;
      component.value = -5;

      component.onBlur(null);

      expect(component.value).toEqual(component.min);
    });

    it('should not allow setting a value above the maximum', () => {
      component.max = 10;
      component.value = 20;

      component.onBlur(null);

      expect(component.value).toEqual(component.max);
    });

    it('should not change the value when it is in the valid range allow setting a value above the maximum', () => {
      component.min = 0;
      component.max = 10;
      component.value = 5;

      component.onBlur(null);

      expect(component.value).toEqual(5);
    });
  });

  describe('input type text', () => {
    beforeEach(() => {
      component.type = 'text';
    });

    it('min and max should not affect the value', () => {
      component.min = 0;
      component.max = 10;
      component.value = 'text';
      component.onBlur(null);

      expect(component.value).toEqual('text');
    });
  });

  describe('input type hidden', () => {
    beforeEach(() => {
      component.type = 'hidden';
    });

    it('min and max should not affect the value', () => {
      component.min = 0;
      component.max = 10;
      component.value = 'text';
      component.onBlur(null);

      expect(component.value).toEqual('text');
    });
  });

  describe('input type password', () => {
    beforeEach(() => {
      component.type = 'password';
    });

    it('min and max should not affect the value', () => {
      component.min = 0;
      component.max = 10;
      component.value = 'text';
      component.onBlur(null);

      expect(component.value).toEqual('text');
    });
  });
});
