/* eslint-disable @angular-eslint/no-output-native */
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Optional,
  Output,
  Self,
  ViewChild
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';

import { CustomControl } from '~app/shared/base/custom-control';

@Component({
  selector: 'bspl-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent extends CustomControl implements AfterViewInit {
  @ViewChild('input', { static: true }) private input: ElementRef<HTMLInputElement>;

  @Output() enterKeyUp = new EventEmitter<KeyboardEvent>();
  @Output() blur = new EventEmitter<Event>();
  @Output() focus = new EventEmitter<Event>();

  @Input() type: 'text' | 'hidden' | 'password' | 'number' = 'text';
  @Input() min: number;
  @Input() max: number;

  @Input() prefix: string;
  @Input() maxLength: number;
  @Input() isLoading = false;

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    cd: ChangeDetectorRef,
    translationService: L10nTranslationService
  ) {
    super(ngControl, cd, translationService);
  }

  public ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  public focusElement(): void {
    if (this.isEnabled) {
      this.input.nativeElement.focus();
    }
  }

  public select(): void {
    this.input.nativeElement.select();
  }

  public onEnterKeyUp(event: KeyboardEvent): void {
    this.enterKeyUp.emit(event);
  }

  public onSpaceKeyDown(event: KeyboardEvent): void {
    if (!(event.target as HTMLInputElement).value) {
      event.preventDefault();
    }
  }

  public onBlur(event: KeyboardEvent) {
    this.normalizeNumericValue();
    this.trimWhenAllAreWhitespaces(event);
    this.propagateTouched();

    this.blur.emit(event);
  }

  public onKeyUp(event: KeyboardEvent) {
    this.trimWhenAllAreWhitespaces(event);
  }

  public onFocus(event: any) {
    this.focus.emit(event);
  }

  private trimWhenAllAreWhitespaces(event: KeyboardEvent) {
    if (event && event.target) {
      const value = (event.target as HTMLInputElement).value;
      if (value !== '' && value.trim() === '') {
        this.value = '';
      }
    }
  }

  private normalizeNumericValue(): void {
    if (this.type !== 'number') {
      return;
    }

    const value = Number(this.value);
    if (this.min != null && value < this.min) {
      this.value = this.min;
    } else if (this.max != null && this.max < value) {
      this.value = this.max;
    }
  }
}
