import { Component } from '@angular/core';

import { DialogConfig } from '../../dialog/dialog.config';

@Component({
  selector: 'bspl-permission-error-dialog',
  templateUrl: './permission-error-dialog.component.html',
  styleUrls: ['./permission-error-dialog.component.scss']
})
export class PermissionErrorDialogComponent {
  constructor(public config: DialogConfig) {}
}
