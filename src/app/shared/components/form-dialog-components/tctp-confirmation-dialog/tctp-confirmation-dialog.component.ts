import { Component } from '@angular/core';

import { DialogConfig } from '~app/shared/components/dialog/dialog.config';

@Component({
  selector: 'bspl-confirmation-dialog',
  templateUrl: './tctp-confirmation-dialog.component.html',
  styleUrls: ['./tctp-confirmation-dialog.component.scss']
})
// TODO This component should be removed. Use instead a reusable ConfirmationDialogComponent
export class TctpConfirmationDialogComponent {
  constructor(public config: DialogConfig) {}
}
