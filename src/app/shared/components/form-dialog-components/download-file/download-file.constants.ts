export const DOWNLOAD_FILE_FORM = {
  RADIO_BUTTON_OPTIONS: [
    {
      value: 'txt',
      label: 'TXT'
    },
    {
      value: 'csv',
      label: 'CSV'
    }
  ]
};
