import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import * as FileSaver from 'file-saver';
import { ErrorMessageService } from '~app/master-data/services/error-message.service';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { DialogConfig } from '~app/shared/components/dialog/dialog.config';
import { DialogService } from '~app/shared/components/dialog/dialog.service';
import { ReactiveSubject } from '~app/shared/components/dialog/reactive-subject';
import { NotificationService } from '~app/shared/services/notification.service';
import { DOWNLOAD_FILE_FORM } from './download-file.constants';

@Component({
  selector: 'bspl-download-file',
  templateUrl: './download-file.component.html',
  styleUrls: ['./download-file.component.scss']
})
export class DownloadFileComponent implements OnInit {
  radioButtonOptions = DOWNLOAD_FILE_FORM.RADIO_BUTTON_OPTIONS;

  radioButtonGroup = new FormControl(this.radioButtonOptions[0].value);

  constructor(
    public config: DialogConfig,
    public reactiveSubject: ReactiveSubject,
    public dialogService: DialogService,
    public notificationService: NotificationService,
    public errorMessageService: ErrorMessageService,
    public translationService: L10nTranslationService
  ) {}

  ngOnInit() {
    this.reactiveSubject.asObservable.subscribe(action => {
      if (action && action.clickedBtn === FooterButton.Download) {
        const downloadFormat = this.radioButtonGroup.value;
        const downloadBody = this.config.data.downloadBody ? this.config.data.downloadBody : null;
        const downloadBtn = action.contentComponentRef.config.data.buttons.find(
          button => button.type === FooterButton.Download
        );
        downloadBtn.isLoading = true;

        const { downloadQuery, detailedDownload, totalElements } = this.config.data;

        this.config.apiService
          .download(downloadQuery, downloadFormat, downloadBody, detailedDownload)
          .subscribe(fileInfo => {
            FileSaver.saveAs(fileInfo.blob, fileInfo.fileName);
            this.notificationService.showSuccess(this.getSuccessMessage(totalElements));
          });

        this.notificationService.showInformation('downloadDialog.inProcess');
        this.dialogService.close();
      }
    });
  }

  private getSuccessMessage(totalElements: number): string {
    let successMessage = 'downloadDialog.success';
    let messageParams = {};
    if (totalElements != null) {
      messageParams = { count: totalElements };
      successMessage = totalElements === 1 ? 'downloadDialog.successSingle' : 'downloadDialog.successMany';
    }

    return this.translationService.translate(successMessage, messageParams);
  }
}
