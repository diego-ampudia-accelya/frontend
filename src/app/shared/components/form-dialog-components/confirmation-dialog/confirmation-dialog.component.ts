import { Component, Inject } from '@angular/core';
import { L10nLocale, L10N_LOCALE } from 'angular-l10n';

import { DialogConfig } from '~app/shared/components/dialog/dialog.config';

@Component({
  selector: 'bspl-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent {
  constructor(@Inject(L10N_LOCALE) public locale: L10nLocale, public config: DialogConfig) {}
}
