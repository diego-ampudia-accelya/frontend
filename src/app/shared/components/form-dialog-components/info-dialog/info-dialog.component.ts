import { Component } from '@angular/core';

import { DialogConfig } from '../../dialog/dialog.config';

@Component({
  selector: 'bspl-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.scss']
})
export class InfoDialogComponent {
  constructor(public config: DialogConfig) {}
}
