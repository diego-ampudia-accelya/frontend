import { Component, Input } from '@angular/core';

@Component({
  selector: 'bspl-card-page',
  templateUrl: './card-page.component.html',
  styleUrls: ['./card-page.component.scss']
})
export class CardPageComponent {
  @Input() title = '';
  @Input() loading = false;
}
