export enum UploadFileType {
  pdf = 'pdf',
  bmp = 'bmp',
  gif = 'gif',
  jpg = 'jpg',
  jpeg = 'jpeg',
  png = 'png',
  zip = 'zip'
}

export function fileTypesToAcceptAttribute(fileTypes: UploadFileType[]): string {
  return fileTypes.map(type => `.${type}`).join(',');
}

const extensionMimeTypeMapper: { [key in UploadFileType]: string } = {
  [UploadFileType.pdf]: 'application/pdf',
  [UploadFileType.bmp]: 'image/bmp',
  [UploadFileType.gif]: 'image/gif',
  [UploadFileType.jpg]: 'image/jpeg',
  [UploadFileType.jpeg]: 'image/jpeg',
  [UploadFileType.png]: 'image/png',
  [UploadFileType.zip]: 'application/zip, application/x-zip-compressed, multipart/x-zip'
};

export function fileExtensionToMimeType(extension: UploadFileType): string[] {
  return extensionMimeTypeMapper[extension].replace(' ', '').split(',');
}
