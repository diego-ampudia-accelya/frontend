import { Component, EventEmitter, Input, Output } from '@angular/core';

import { FileUpload } from '../file-upload.model';

@Component({
  selector: 'bspl-file-card',
  templateUrl: './file-card.component.html',
  styleUrls: ['./file-card.component.scss']
})
export class FileCardComponent {
  @Input() file: FileUpload;
  @Input() canRemove: boolean;
  @Input() canRetry: boolean;

  @Output() retry = new EventEmitter();
  @Output() remove = new EventEmitter();
}
