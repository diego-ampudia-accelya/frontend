import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FileUpload } from '../file-upload.model';

import { FileCardComponent } from './file-card.component';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';

describe('FileCardComponent', () => {
  let component: FileCardComponent;
  let fixture: ComponentFixture<FileCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FileCardComponent, BytesFormatPipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileCardComponent);
    component = fixture.componentInstance;
    component.file = {} as FileUpload;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
