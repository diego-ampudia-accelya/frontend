import { HttpClient, HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { differenceBy, isEmpty, property } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { catchError, filter, map, mapTo, switchMap, takeUntil, tap } from 'rxjs/operators';

import { FileUpload } from '../file-upload.model';
import { UploadStatus } from '../upload-mode.enum';
import { ForbiddenError, NotFoundError, ServerError, ValidationError } from '~app/shared/errors';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';

@Injectable()
export class BulkUpload {
  public filesToUpload$: Observable<FileUpload[]>;
  public uploadFinished$: Observable<FileUpload[]>;

  private filesToUploadSubject = new BehaviorSubject<FileUpload[]>([]);
  private destroyed$ = new Subject();
  private isDisposed: boolean;

  constructor(
    private http: HttpClient,
    private messageExtractor: MessageExtractorService,
    private translation: L10nTranslationService,
    private config: { uploadPath: string }
  ) {
    this.filesToUpload$ = this.filesToUploadSubject.asObservable();
    this.uploadFinished$ = this.filesToUpload$.pipe(
      switchMap(filesToUpload => {
        if (isEmpty(filesToUpload)) {
          return of([]);
        }

        return combineLatest(filesToUpload.map(file => file.finished.pipe(filter(Boolean), mapTo(file))));
      }),
      takeUntil(this.destroyed$)
    );
  }

  public cancel(file: FileUpload): void {
    this.ensureNotDisposed('cancel');
    file.status = UploadStatus.cancelled;
    file.cancelled.next();

    const currentFiles = this.filesToUploadSubject.getValue();
    const newFiles = differenceBy(currentFiles, [file], property('name'));
    this.filesToUploadSubject.next(newFiles);
  }

  public upload(file: FileUpload): Observable<FileUpload> {
    this.ensureNotDisposed('upload');
    file.status = UploadStatus.uploading;
    file.progress = 0;
    file.error = null;
    file.responseData = null;
    file.finished.next(false);

    // If the file is new we add it to the filesToUpload
    const currentFiles = this.filesToUploadSubject.getValue();
    if (currentFiles.every(f => f.name !== file.name)) {
      const newFiles = [...currentFiles, file];
      this.filesToUploadSubject.next(newFiles);
    }

    return this.http
      .post(this.config.uploadPath, this.getUploadPayload(file), {
        reportProgress: true,
        observe: 'events'
      })
      .pipe(
        tap(response => this.updateProgress(response, file)),
        filter(response => response.type === HttpEventType.Response),
        map((response: HttpResponse<any>) => {
          file.responseData = response.body;
          file.status = UploadStatus.completed;
          file.finished.next(true);

          return file;
        }),
        catchError(error => {
          file.status = UploadStatus.failed;
          file.error = this.extractError(error);
          file.finished.next(true);
          file.isValid = !this.setValidAccordingErrorType(error);

          return of(file);
        }),
        takeUntil(file.cancelled)
      );
  }

  public dispose(): void {
    this.filesToUploadSubject.complete();
    this.destroyed$.next();
    this.isDisposed = true;

    // Cancel all pending file uploads.
    const filesToUpload = this.filesToUploadSubject.getValue();
    filesToUpload.forEach(file => file.cancelled.next());
  }

  private setValidAccordingErrorType(error): boolean {
    return error instanceof ValidationError || error instanceof ForbiddenError || error instanceof NotFoundError;
  }

  private getUploadPayload(file: FileUpload): FormData {
    const formData = new FormData();
    formData.append('file', file.fileInstance);

    const requestData = file.requestData ?? {};
    Object.entries(requestData).forEach(([propertyName, value]) => {
      formData.append(propertyName, value);
    });

    return formData;
  }

  private updateProgress(event: HttpEvent<any>, file: FileUpload): void {
    if (event.type === HttpEventType.UploadProgress) {
      const progress = Math.round((100 * event.loaded) / event.total);

      file.progress = progress;
    }
  }

  private extractError(error: ServerError): string {
    // TODO: Replace string concatenation with array of errors
    return this.messageExtractor
      .extractMany(error)
      .map(message => this.translation.translate(message))
      .join('<br/>');
  }

  private ensureNotDisposed(operation: string): void {
    if (this.isDisposed) {
      throw new Error(`BulkUpload: Cannot ${operation} after this instance has been disposed.`);
    }
  }
}
