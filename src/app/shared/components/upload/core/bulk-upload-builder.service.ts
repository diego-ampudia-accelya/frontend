import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { BehaviorSubject, Subject } from 'rxjs';

import { FileUpload } from '../file-upload.model';
import { UploadStatus } from '../upload-mode.enum';

import { BulkUpload } from './bulk-upload';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';

@Injectable({
  providedIn: 'root'
})
export class BulkUploadBuilder {
  constructor(
    private http: HttpClient,
    private messageExtractor: MessageExtractorService,
    private translation: L10nTranslationService
  ) {}

  public createBulkUploader(uploadPath: string) {
    return new BulkUpload(this.http, this.messageExtractor, this.translation, { uploadPath });
  }

  public createFileUpload(file: File, validationResult: { isValid: boolean; validationError: string }): FileUpload {
    const { isValid, validationError } = validationResult;

    return {
      name: file.name,
      progress: isValid ? 0 : 100,
      size: file.size,
      status: isValid ? UploadStatus.uploading : UploadStatus.failed,
      fileInstance: file,
      isValid,
      error: validationError,
      cancelled: new Subject(),
      finished: new BehaviorSubject(false)
    };
  }
}
