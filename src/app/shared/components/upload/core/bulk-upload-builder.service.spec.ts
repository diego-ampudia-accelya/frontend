import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { UploadStatus } from '../upload-mode.enum';

import { BulkUpload } from './bulk-upload';
import { BulkUploadBuilder } from './bulk-upload-builder.service';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';

describe('BulkUploadBuilder', () => {
  let service: BulkUploadBuilder;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BulkUploadBuilder, mockProvider(MessageExtractorService), mockProvider(L10nTranslationService)]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(BulkUploadBuilder);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('createBulkUpload', () => {
    it('should create BulkUpload', () => {
      const actual = service.createBulkUploader('path');
      expect(actual instanceof BulkUpload).toBeTruthy();
    });
  });

  describe('createFileUpload', () => {
    it('should map to FileUpload when file is valid', () => {
      const file = new File(['test'], 'test.txt', { type: 'text/plain' });
      const actual = service.createFileUpload(file, { isValid: true, validationError: null });

      expect(actual.name).toBe(file.name);
      expect(actual.fileInstance).toBe(file);
      expect(actual.size).toBe(file.size);
      expect(actual.isValid).toBeTruthy();
      expect(actual.error).toBeFalsy();
      expect(actual.progress).toBe(0);
      expect(actual.status).toBe(UploadStatus.uploading);
    });

    it('should map to FileUpload when file has validation errors', () => {
      const file = new File(['test'], 'test.txt', { type: 'text/plain' });
      const actual = service.createFileUpload(file, { isValid: false, validationError: 'error' });

      expect(actual.name).toBe(file.name);
      expect(actual.fileInstance).toBe(file);
      expect(actual.size).toBe(file.size);
      expect(actual.isValid).toBeFalsy();
      expect(actual.error).toBe('error');
      expect(actual.progress).toBe(100);
      expect(actual.status).toBe(UploadStatus.failed);
    });
  });
});
