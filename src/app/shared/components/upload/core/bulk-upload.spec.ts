import { HttpEventType, HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { FileUpload } from '../file-upload.model';
import { UploadStatus } from '../upload-mode.enum';

import { BulkUpload } from './bulk-upload';
import { BulkUploadBuilder } from './bulk-upload-builder.service';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';

describe('BulkUpload', () => {
  let service: BulkUpload;
  let builder: BulkUploadBuilder;
  let httpController: HttpTestingController;
  let messageExtractorStub: SpyObject<MessageExtractorService>;
  let translationStub: SpyObject<L10nTranslationService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        // It is easier to get a BulkUpload instance using the BulkUploadBuilder
        // so we just provide it here
        BulkUploadBuilder,
        mockProvider(MessageExtractorService),
        mockProvider(L10nTranslationService)
      ]
    });
  });

  beforeEach(() => {
    builder = TestBed.inject(BulkUploadBuilder);
    service = builder.createBulkUploader('');
    httpController = TestBed.inject(HttpTestingController);

    messageExtractorStub = TestBed.inject<any>(MessageExtractorService);
    translationStub = TestBed.inject<any>(L10nTranslationService);
    translationStub.translate.and.callFake(identity);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('when uploading a single file', () => {
    it('should return result when the upload completes successfully', fakeAsync(() => {
      const fileToUpload = createFileUpload();

      let actualUploadFinished: FileUpload[];
      service.uploadFinished$.subscribe(result => (actualUploadFinished = result));
      let actualUploadResult: FileUpload;
      service.upload(fileToUpload).subscribe(result => (actualUploadResult = result));

      httpController.expectOne(matchUploadRequest(fileToUpload)).flush({ id: 1 });

      const expected: any = jasmine.objectContaining({
        status: UploadStatus.completed,
        responseData: { id: 1 }
      });
      expect(actualUploadResult).toEqual(expected);
      expect(fileToUpload).toEqual(expected);
      expect(actualUploadFinished).toEqual([expected]);
    }));

    it('should update progress during upload', fakeAsync(() => {
      const fileToUpload = createFileUpload();

      let actualUploadResult: FileUpload;
      service.upload(fileToUpload).subscribe(result => (actualUploadResult = result));

      const mockRequest = httpController.expectOne(matchUploadRequest(fileToUpload));

      mockRequest.event({
        type: HttpEventType.UploadProgress,
        loaded: 50,
        total: 100
      });
      expect(fileToUpload.progress).toBe(50);
      expect(fileToUpload.status).toBe(UploadStatus.uploading);

      mockRequest.event({
        type: HttpEventType.UploadProgress,
        loaded: 80,
        total: 100
      });
      expect(fileToUpload.progress).toBe(80);
      expect(fileToUpload.status).toBe(UploadStatus.uploading);

      mockRequest.flush({ id: 1 });

      const expected: any = jasmine.objectContaining({
        status: UploadStatus.completed,
        responseData: { id: 1 }
      });
      expect(actualUploadResult).toEqual(expected);
    }));

    it('should return result when upload is cancelled', fakeAsync(() => {
      const fileToUpload = createFileUpload();

      let actualUploadFinished: FileUpload[];
      service.uploadFinished$.subscribe(result => (actualUploadFinished = result));
      let actualUploadResult: FileUpload;
      service.upload(fileToUpload).subscribe(result => (actualUploadResult = result));
      service.cancel(fileToUpload);

      const cancelledRequest = httpController.expectOne(matchUploadRequest(fileToUpload));

      const expected: any = jasmine.objectContaining({
        status: UploadStatus.cancelled,
        responseData: null
      });

      expect(actualUploadResult).toBeFalsy();
      expect(fileToUpload).toEqual(expected);
      expect(actualUploadFinished).toEqual([]);
      expect(cancelledRequest.cancelled).toBeTruthy();
    }));

    it('should return result with error message when the upload fails', fakeAsync(() => {
      const fileToUpload = createFileUpload();
      messageExtractorStub.extractMany.and.returnValue(['error-1', 'error-2']);

      let actualUploadFinished: FileUpload[];
      service.uploadFinished$.subscribe(result => (actualUploadFinished = result));
      let actualUploadResult: FileUpload;
      service.upload(fileToUpload).subscribe(result => (actualUploadResult = result));

      httpController.expectOne(matchUploadRequest(fileToUpload)).flush({}, { status: 500, statusText: 'Failed' });

      const expected: any = jasmine.objectContaining({
        status: UploadStatus.failed,
        error: 'error-1<br/>error-2'
      });

      expect(actualUploadResult).toEqual(expected);
      expect(fileToUpload).toEqual(expected);
      expect(actualUploadFinished).toEqual([expected]);
    }));

    it('should send additional data when they are provided', fakeAsync(() => {
      const fileToUpload = createFileUpload();
      fileToUpload.requestData = {
        expirationDate: '2020-01-01',
        size: '1000'
      };

      let actualUploadResult: FileUpload;
      service.upload(fileToUpload).subscribe(result => (actualUploadResult = result));

      httpController
        .expectOne(
          (request: HttpRequest<FormData>) =>
            request.body.get('expirationDate') === '2020-01-01' && request.body.get('size') === '1000'
        )
        .flush({ id: 1 });

      expect(actualUploadResult).toBeTruthy();
    }));
  });

  describe('when uploading two files in parallel', () => {
    it('should return two completed uploads when both succeed', fakeAsync(() => {
      const file1 = createFileUpload('file-1.txt');
      const file2 = createFileUpload('file-2.txt');

      let actualUploadFinished: FileUpload[];
      service.uploadFinished$.subscribe(result => (actualUploadFinished = result));
      service.upload(file1).subscribe();
      service.upload(file2).subscribe();

      httpController.expectOne(matchUploadRequest(file1)).flush({ id: 1 });
      httpController.expectOne(matchUploadRequest(file2)).flush({ id: 2 });

      const expectedFile1: any = jasmine.objectContaining({
        name: file1.name,
        status: UploadStatus.completed,
        responseData: { id: 1 }
      });
      const expectedFile2: any = jasmine.objectContaining({
        name: file2.name,
        status: UploadStatus.completed,
        responseData: { id: 2 }
      });

      expect(actualUploadFinished).toEqual([expectedFile1, expectedFile2]);
    }));

    it('should return one completed upload when the other one is is cancelled', fakeAsync(() => {
      const fileToUpload = createFileUpload();
      const fileToCancel = createFileUpload('cancel.txt');

      let actualUploadFinished: FileUpload[];
      service.uploadFinished$.subscribe(result => (actualUploadFinished = result));
      let actualUploadResult: FileUpload;
      service.upload(fileToUpload).subscribe(result => (actualUploadResult = result));
      service.upload(fileToCancel).subscribe();
      service.cancel(fileToCancel);

      httpController.expectOne(matchUploadRequest(fileToUpload)).flush({ id: 1 });
      const cancelledRequest = httpController.expectOne(matchUploadRequest(fileToCancel));

      const expectedFileToUpload: any = jasmine.objectContaining({
        status: UploadStatus.completed,
        responseData: { id: 1 }
      });

      const expectedFileToCancel: any = jasmine.objectContaining({
        status: UploadStatus.cancelled,
        responseData: null
      });

      expect(actualUploadResult).toEqual(expectedFileToUpload);
      expect(fileToUpload).toEqual(expectedFileToUpload);
      expect(actualUploadFinished).toEqual([expectedFileToUpload]);
      expect(fileToCancel).toEqual(expectedFileToCancel);
      expect(cancelledRequest.cancelled).toBeTruthy();
    }));

    it('should return two completed uploads when one of them was successfully retried', () => {
      const fileToSucceed = createFileUpload('success.txt');
      const fileToRetry = createFileUpload('error.txt');
      messageExtractorStub.extractMany.and.returnValue(['error-1', 'error-2']);

      let actualUploadFinished: FileUpload[];
      service.uploadFinished$.subscribe(result => (actualUploadFinished = result));
      service.upload(fileToSucceed).subscribe();
      service.upload(fileToRetry).subscribe();

      httpController.expectOne(matchUploadRequest(fileToRetry)).flush({}, { status: 500, statusText: 'Failed' });

      service.upload(fileToRetry).subscribe();
      httpController.expectOne(matchUploadRequest(fileToSucceed)).flush({ id: 1 });
      httpController.expectOne(matchUploadRequest(fileToRetry)).flush({ id: 2 });

      const expectedFileToSucceed: any = jasmine.objectContaining({
        status: UploadStatus.completed,
        responseData: { id: 1 }
      });

      const expectedFileToRetry: any = jasmine.objectContaining({
        status: UploadStatus.completed,
        responseData: { id: 2 }
      });

      expect(fileToSucceed).toEqual(expectedFileToSucceed);
      expect(actualUploadFinished).toEqual([expectedFileToSucceed, expectedFileToRetry]);
      expect(fileToRetry).toEqual(expectedFileToRetry);
    });
  });

  describe('when disposing', () => {
    it('should abort pending uploads', fakeAsync(() => {
      const file = createFileUpload();
      let uploadFinishedResult: FileUpload[];
      service.uploadFinished$.subscribe(result => (uploadFinishedResult = result));

      service.upload(file).subscribe();
      const request = httpController.expectOne(matchUploadRequest(file));
      service.dispose();

      expect(request.cancelled).toBeTruthy();
      expect(uploadFinishedResult).toEqual([]);
    }));

    it('should throw error when cancel is called after dispose', fakeAsync(() => {
      const file = createFileUpload();

      service.dispose();
      expect(() => service.cancel(file)).toThrowError(/BulkUpload/);
    }));

    it('should throw error when upload is called after dispose', fakeAsync(() => {
      const file = createFileUpload();

      service.dispose();
      expect(() => service.upload(file)).toThrowError(/BulkUpload/);
    }));
  });

  function createFileUpload(name = 'text.txt'): FileUpload {
    const file = new File([new Blob()], name, { type: 'text/plain' });
    const fileToUpload = builder.createFileUpload(file, { isValid: true, validationError: null });

    return fileToUpload;
  }

  function matchUploadRequest(fileToUpload: FileUpload): (request: HttpRequest<FormData>) => boolean {
    return (request: HttpRequest<FormData>) => {
      const isSameFile = request.body.get('file') === fileToUpload.fileInstance;
      const isPost = request.method === 'POST';

      return isPost && isSameFile;
    };
  }
});
