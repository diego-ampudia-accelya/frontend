import { FileValidators } from './file-validators';

describe('FileValidators', () => {
  describe('minFileSize', () => {
    it('should return a validation error when the size is less than the minimum', () => {
      const validator = FileValidators.minFileSize(10);
      const file = { size: 1 } as File;

      expect(validator(file)).toBeTruthy();
    });

    it('should return undefined when the size is larger that the minimum', () => {
      const validator = FileValidators.minFileSize(10);
      const file = { size: 20 } as File;

      expect(validator(file)).toBeFalsy();
    });

    it('should return a validation error when the size is equal to the minimum', () => {
      const validator = FileValidators.minFileSize(10);
      const file = { size: 10 } as File;

      expect(validator(file)).toBeTruthy();
    });

    it('should assume a minimum size of 0 when none is specified', () => {
      const validator = FileValidators.minFileSize();
      const file = { size: 0 } as File;

      expect(validator(file)).toEqual({
        message: 'upload.errors.zeroSize',
        textParams: { minFileSize: '0 Bytes' }
      });
    });

    it('should return a validation error in proper format', () => {
      const validator = FileValidators.minFileSize(10);
      const file = { size: 0 } as File;

      expect(validator(file)).toEqual({
        message: 'upload.errors.biggerSize',
        textParams: { minFileSize: '10 B' }
      });
    });
  });

  describe('maxFileSize', () => {
    it('should return a validation error when the size is greater than the maximum', () => {
      const validator = FileValidators.maxFileSize(10);
      const file = { size: 20 } as File;

      expect(validator(file)).toBeTruthy();
    });

    it('should return undefined when the size is smaller that the maximum', () => {
      const validator = FileValidators.maxFileSize(10);
      const file = { size: 1 } as File;

      expect(validator(file)).toBeFalsy();
    });

    it('should return undefined when the size is equal to the maximum', () => {
      const validator = FileValidators.maxFileSize(10);
      const file = { size: 10 } as File;

      expect(validator(file)).toBeFalsy();
    });

    it('should assume a maximum size of 15Mb when none is specified', () => {
      const validator = FileValidators.maxFileSize();
      const file = { size: Math.pow(2, 20) * 20 } as File;

      expect(validator(file)).toEqual({
        message: 'upload.errors.smallerSize',
        textParams: { maxFileSize: '15 Mb' }
      });
    });

    it('should return a validation error in proper format', () => {
      const validator = FileValidators.maxFileSize(10);
      const file = { size: 20 } as File;

      expect(validator(file)).toEqual({
        message: 'upload.errors.smallerSize',
        textParams: { maxFileSize: '10 B' }
      });
    });
  });

  describe('filenamePattern', () => {
    const txtPattern = /(.*)\.txt/;

    it('should return a validation error when the filename does not match the pattern', () => {
      const validator = FileValidators.filenamePattern(txtPattern);
      const file = { name: 'fail.exe' } as File;

      expect(validator(file)).toEqual({
        message: 'upload.errors.nameFormat'
      });
    });

    it('should return undefined when the filename matchs the pattern', () => {
      const validator = FileValidators.filenamePattern(txtPattern);
      const file = { name: 'success.txt' } as File;

      expect(validator(file)).toBeFalsy();
    });
  });

  describe('single', () => {
    it('should return validation error only for the second match when multiple files match the predicate', () => {
      const existingtMatch = { name: 'match' } as File;
      const newMatch = { name: 'match' } as File;
      const notMatching = { name: 'not-a-match' } as File;
      const allFiles = [newMatch, existingtMatch, notMatching];
      const message = 'message';
      const validator = FileValidators.single(f => f.name === 'match', message);

      expect(validator(newMatch, allFiles)).toEqual({ message });
      expect(validator(existingtMatch, allFiles)).toBeFalsy();
    });

    it('should return success when a single file matches the predicate', () => {
      const newMatch = { name: 'match' } as File;
      const notMatching = { name: 'not-a-match' } as File;
      const allFiles = [newMatch, notMatching];
      const validator = FileValidators.single(f => f.name === 'match', 'message');

      expect(validator(newMatch, allFiles)).toBeFalsy();
    });

    it('should return success when a single file matches the predicate but it is not contained in `allFiles`', () => {
      const newMatch = { name: 'match' } as File;
      const notMatching = { name: 'not-a-match' } as File;
      const allFiles = [notMatching];
      const validator = FileValidators.single(f => f.name === 'match', 'message');

      expect(validator(newMatch, allFiles)).toBeFalsy();
    });

    it('should return success when a no files match the predicate', () => {
      const notMatching = { name: 'not-a-match' } as File;
      const allFiles = [{ ...notMatching }, { ...notMatching }];
      const validator = FileValidators.single(f => f.name === 'match', 'message');

      expect(validator(notMatching, allFiles)).toBeFalsy();
    });
  });
});
