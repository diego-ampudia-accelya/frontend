import { Subject } from 'rxjs';

import { UploadStatus } from './upload-mode.enum';

export interface FileUpload {
  id?: string; //* Optional id to save id's coming from BE
  name: string;
  progress: number;
  size: number;
  status: string | UploadStatus;
  fileInstance: File;
  isValid: boolean;
  error: string;
  cancelled: Subject<void>;
  finished: Subject<boolean>;
  responseData?: any;
  requestData?: { [key: string]: string };
}
