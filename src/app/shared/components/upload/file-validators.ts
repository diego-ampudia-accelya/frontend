import { flatten } from 'lodash';
import { filter, flow, last } from 'lodash/fp';

import { fileExtensionToMimeType, UploadFileType } from './upload-filetype.config';
import { BytesFormatPipe } from '~app/shared/pipes/bytes-format.pipe';

export const MAX_FILE_SIZE_10 = Math.pow(2, 20) * 10;
export const MAX_FILE_SIZE_15 = Math.pow(2, 20) * 15;

// eslint-disable-next-line @typescript-eslint/naming-convention
export const FileValidators = {
  maxFileSize:
    (max = MAX_FILE_SIZE_15) =>
    (file: File): ValidationResult => {
      let validationError: ValidationResult;
      const bytesFormatPipe = new BytesFormatPipe();
      if (file.size > max) {
        validationError = {
          message: 'upload.errors.smallerSize',
          textParams: { maxFileSize: bytesFormatPipe.transform(max) }
        };
      }

      return validationError;
    },

  minFileSize:
    (min = 0) =>
    (file: File): ValidationResult => {
      let validationError: ValidationResult;
      const bytesFormatPipe = new BytesFormatPipe();
      if (file.size <= min) {
        validationError = {
          message: min === 0 ? 'upload.errors.zeroSize' : 'upload.errors.biggerSize',
          textParams: { minFileSize: bytesFormatPipe.transform(min) }
        };
      }

      return validationError;
    },

  filenamePattern:
    (pattern: RegExp, message = 'upload.errors.nameFormat') =>
    (file: File): ValidationResult => {
      const hasValidName = pattern.test(file.name);

      return hasValidName ? null : { message };
    },

  filenameMaxLength:
    (maxFileNameLength: number) =>
    (file: File): ValidationResult => {
      let validationError: ValidationResult;
      if (file.name.length > maxFileNameLength) {
        validationError = {
          message: 'upload.errors.nameMaxLength',
          textParams: { maxFileNameLength }
        };
      }

      return validationError;
    },

  single:
    (predicate: FilePredicate, message: string) =>
    (file: File, allFiles: File[]): ValidationResult => {
      let validationResult: ValidationResult;
      if (predicate(file)) {
        // `allFiles` contains files in order of addition. Newly added files are listed first.
        const oldestMatch = flow([filter(predicate), last])(allFiles);
        const hasNewerMatches = oldestMatch != null && oldestMatch !== file;
        validationResult = hasNewerMatches ? { message } : null;
      }

      return validationResult;
    },

  fileType:
    (fileTypesAllowed: UploadFileType[]) =>
    (file: File): ValidationResult => {
      let validationResult: ValidationResult;
      const hasValidFormat = flatten(fileTypesAllowed.map(fileExtensionToMimeType)).some(mimeType =>
        file.type.match(mimeType)
      );

      if (!hasValidFormat) {
        validationResult = {
          message: 'upload.errors.fileFormat'
        };
      }

      return validationResult;
    }
};

export type FilePredicate = (f: File) => boolean;
export type FileValidator = (file: File, allFiles?: File[]) => ValidationResult | null;

export interface ValidationResult {
  message: string;
  textParams?: any;
}
