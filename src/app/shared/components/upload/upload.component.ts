import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Renderer2,
  Self,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { reject } from 'lodash';
import { Observable, of, Subject } from 'rxjs';
import { first, map, share, shareReplay, startWith, takeUntil, tap } from 'rxjs/operators';
import { FileUpload } from '~app/shared/components/upload/file-upload.model';
import { FormUtil } from '~app/shared/helpers';
import { BulkUpload } from './core/bulk-upload';
import { BulkUploadBuilder } from './core/bulk-upload-builder.service';
import { FileValidator, FileValidators } from './file-validators';
import { fileTypesToAcceptAttribute, UploadFileType } from './upload-filetype.config';
import { UploadModes, UploadStatus, UploadType } from './upload-mode.enum';

@Component({
  selector: 'bspl-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy, ControlValueAccessor {
  @ViewChild('upload', { static: true }) uploadInput: ElementRef;
  @ViewChild('inputWrapper', { static: true }) inputWrapper: ElementRef;
  @ViewChild('dropArea', { static: true }) dropArea: ElementRef;

  @Input() public validationRules: FileValidator[];

  @Input()
  public get isDragAndDropActive(): boolean {
    return this._isDragAndDropActive && !this.isDisabled;
  }
  public set isDragAndDropActive(value: boolean) {
    this._isDragAndDropActive = value;
  }

  @Input() public isDisabled = false;
  @Input() public files: File[] = [];
  @Input() public mode = UploadModes.Initial;
  @Input() public resetMode = this.mode;
  @Input() public uploadPath: string;
  @Input() public responseFieldName: string;
  @Input() public label: string;
  @Input() public requestData: { [key: string]: string };
  @Input() public fileTypesAccepted: UploadFileType[];
  @Input() public isRequired: boolean;

  @Output() public uploadPreview = new EventEmitter<void>();
  @Output() public uploadStarted = new EventEmitter<UploadType>();
  @Output() public uploadFinished = new EventEmitter<any[]>();
  @Output() public uploadFinishedResult = new EventEmitter<any[]>();
  @Output() public resetInitialMode = new EventEmitter<void>();
  @Output() public filesChange = new EventEmitter<FileUpload[]>();
  // Use an async EventEmitter to avoid ExpressionChangedAfterItHasBeenCheckedError error
  @Output() public modeChange = new EventEmitter<UploadModes>(true);

  public filesToUpload: FileUpload[] = [];
  public targetDropArea: HTMLElement;
  public uploadStatusEnum = UploadStatus;

  public get hasFiles(): boolean {
    return this.filesToUpload && this.filesToUpload.length > 0;
  }

  public get hasFilesToUpload(): boolean {
    return this.filesToUpload && this.filesToUpload.some(file => file.isValid && file.status !== UploadStatus.failed);
  }

  public get hasFilesInPreviewMode() {
    return this.mode === UploadModes.Preview && this.hasFilesToUpload;
  }

  public get hasFilesWithErrors() {
    return this.filesToUpload && this.filesToUpload.some(file => file.isValid && file.status === UploadStatus.failed);
  }

  public isDraggingOver = false;
  public uploadModes = UploadModes;

  private _isDragAndDropActive = false;

  private hiddenFileInput: ElementRef;
  private responseData = [];

  private bulkUploadFinished$: Observable<FileUpload[]>;
  private bulkUpload: BulkUpload;

  private destroy$ = new Subject();

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    private renderer: Renderer2,
    private cd: ChangeDetectorRef,
    private bulkUploaderBuilder: BulkUploadBuilder,
    private translationService: L10nTranslationService
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this.validationRules = [FileValidators.maxFileSize()];
  }

  @HostListener('dragenter', ['$event']) public onDragEnter(event: DragEvent) {
    if (this.targetDropArea === event.target && this.isDragAndDropActive) {
      this.isDraggingOver = true;
    }
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(event: DragEvent) {
    if (this.targetDropArea === event.target && this.isDragAndDropActive) {
      this.isDraggingOver = false;
    }
  }

  @HostListener('dragover', ['$event']) public onDragOver(event: DragEvent) {
    if (this.isDragAndDropActive) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  @HostListener('drop', ['$event']) public onDrop(event: any) {
    event.stopPropagation();
    event.preventDefault();
    if (this.targetDropArea === event.target && this.isDragAndDropActive) {
      this.isDraggingOver = false;

      const files = event.dataTransfer.files;
      if (files.length > 0) {
        this.addFiles(files);
      }
    }
  }

  public onChange = (_: any) => {};
  public onTouched = () => {};

  public ngOnInit() {
    this.initializeValidationRules();
    this.targetDropArea = this.dropArea.nativeElement;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.files && changes.files.currentValue && changes.files.currentValue.length) {
      this.addFiles(changes.files.currentValue);
    }

    if (changes.requestData) {
      this.filesToUpload?.forEach(file => (file.requestData = changes.requestData.currentValue));
    }

    // When disabling, reset if there are selected files. Keeping it simple for now.
    if (changes.isDisabled && this.hasFiles && changes.isDisabled.currentValue) {
      this.reset();
    }
  }

  public ngAfterViewInit() {
    this.addHiddenFileInput();
  }

  public onFilesChange($event: any): void {
    this.addFiles($event.target.files);
    this.cd.markForCheck();
  }

  public onDropAreaClick(): void {
    if (this.isDisabled) {
      return;
    }

    // open file browser by triggering a click on invisible input element
    const event = new MouseEvent('click', { bubbles: false });
    const uploadInput: HTMLInputElement = this.inputWrapper.nativeElement.querySelector('.upload-input');
    uploadInput.dispatchEvent(event);
  }

  public pauseUpload(): void {
    this.disposeOfBulkUpload();
  }

  public uploadFiles(filesToUpload: FileUpload[] = this.filesToUpload): Observable<boolean> {
    if (!this.uploadPath) {
      throw new Error('UploadComponent: No `uploadPath` provided.');
    }

    const validFiles = filesToUpload.filter(file => file.isValid);
    if (validFiles.length === 0 || this.isDisabled) {
      return of(false);
    }

    // If an upload is already in progress we just cancel it and start a new one
    this.disposeOfBulkUpload();
    this.uploadStarted.emit(UploadType.Normal);
    this.mode = UploadModes.Upload;
    this.modeChange.emit(this.mode);
    this.bulkUpload = this.bulkUploaderBuilder.createBulkUploader(this.uploadPath);

    validFiles.forEach(file => this.bulkUpload.upload(file).subscribe());

    // We upload for each eligible file.
    // The BulkUpload will emit a single event after all pending uploads, cancellations or retries are finished.
    this.bulkUploadFinished$ = this.bulkUpload.uploadFinished$.pipe(
      tap(result => {
        // We always emit based on ALL files to upload regardless of the result of the operation
        this.filesChange.emit(this.filesToUpload);
        this.emitUploadFinished(this.filesToUpload);
        this.uploadFinishedResult.emit(result);
      }),
      share()
    );
    this.bulkUploadFinished$.subscribe();

    // We return a response indicating if all files have uploaded successfully or not
    return this.bulkUploadFinished$.pipe(
      map(files => this.allValidFilesAreUploaded(files)),
      shareReplay(1),
      first()
    );
  }

  public removeFile(fileToUpload: FileUpload): void {
    if (!this.canRemove(fileToUpload)) {
      throw Error(`UploadComponent: The state of '${fileToUpload.name}' does not allow removing.`);
    }

    this.filesToUpload = reject(this.filesToUpload, { name: fileToUpload.name });
    if (this.bulkUpload != null) {
      // TODO: Should we define a separate `cancelUpload` method?
      this.bulkUpload.cancel(fileToUpload);
    }

    this.filesChange.emit(this.filesToUpload);

    if (this.filesToUpload.length === 0) {
      this.resetToInitialMode();
    }
  }

  public retryUpload(fileToUpload: FileUpload): void {
    if (!this.canRetry(fileToUpload)) {
      throw Error(`UploadComponent: The state of '${fileToUpload.name}' does not allow retrying.`);
    }

    this.uploadStarted.emit(UploadType.Retry);
    this.bulkUpload.upload(fileToUpload).subscribe();
  }

  public canRemove(file: FileUpload): boolean {
    const isInvalid = file.status === UploadStatus.failed && !file.isValid;
    const isUploading = file.status === UploadStatus.uploading;

    return !this.isDisabled && (isUploading || isInvalid);
  }

  public canRetry(file: FileUpload): boolean {
    return !this.isDisabled && file.status === UploadStatus.failed && file.isValid;
  }

  public reset() {
    this.onChange(null);
    this.filesToUpload = [];
    this.filesChange.emit([]);
    this.resetToInitialMode();
  }

  public resetToInitialMode() {
    this.mode = this.resetMode;
    this.modeChange.emit(this.mode);
    this.resetInitialMode.emit();
    this.disposeOfBulkUpload();
    // make sure the same file can be removed and again added to the same dialog instance
    this.resetHiddenFileInputElement();
  }

  public writeValue(value: any): void {
    this.responseData = value || [];
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public isRequiredErrorShown(): boolean {
    const control = this.ngControl?.control;

    // If there are files (even with errors) we do not show required error (there will be errors shown on screen already)
    return !!control && !this.filesToUpload?.length && control.invalid && FormUtil.areErrorsShown(control);
  }

  public getRequiredErrorMessage(): string {
    const control = this.ngControl?.control;

    return control ? FormUtil.getErrorMessage(this.ngControl.control, this.translationService) : '';
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeValidationRules(): void {
    this.validationRules = this.validationRules || [];

    if (this.fileTypesAccepted?.length) {
      this.validationRules.push(FileValidators.fileType(this.fileTypesAccepted));
    }

    if (this.isRequired) {
      const requiredError = { required: true };
      this.filesChange
        .pipe(
          startWith([]),
          map(files => files.filter(file => file.isValid)),
          takeUntil(this.destroy$)
        )
        .subscribe(files => {
          const errors = !files?.length && !this.ngControl.value ? requiredError : null;
          this.ngControl?.control.setErrors(errors);
        });
    }
  }

  private addFiles(filesOrList: File[] | FileList) {
    if (this.isDisabled) {
      return;
    }

    const files = Array.isArray(filesOrList) ? filesOrList : Array.from(filesOrList);

    files
      .filter(file => this.filesToUpload.every(fileToUpload => fileToUpload.name !== file.name))
      .forEach(file => {
        const validationResult = this.validateFile(file);
        const fileToUpload = this.bulkUploaderBuilder.createFileUpload(file, validationResult);
        fileToUpload.requestData = this.requestData;
        this.filesToUpload.unshift(fileToUpload);
      });

    if (this.filesToUpload.length) {
      this.filesChange.emit(this.filesToUpload);
      if (this.mode === UploadModes.Initial) {
        this.mode = UploadModes.Preview;
        this.uploadPreview.emit();
        this.modeChange.emit(this.mode);
      }
    }
  }

  private validateFile(file: File): { isValid: boolean; validationError: string } {
    const existingFiles = this.filesToUpload.map(f => f.fileInstance);
    const validationErrors = this.validationRules
      .map(rule => rule(file, existingFiles))
      .filter(result => result != null)
      .map(error => this.translationService.translate(error.message, error.textParams));

    return {
      isValid: validationErrors.length === 0,
      // TODO: Change to an array for more control over how errors are displayed.
      validationError: validationErrors.join('<br/>')
    };
  }

  private allValidFilesAreUploaded(files: FileUpload[]): boolean {
    const uploadedFilesLength = files.filter(file => file.status === UploadStatus.completed).length;

    return uploadedFilesLength > 0 && uploadedFilesLength === this.filesToUpload.filter(file => file.isValid).length;
  }

  private emitUploadFinished(files: FileUpload[]) {
    const availableResponses = files.map(({ responseData }) => responseData).filter(Boolean);
    this.responseData = availableResponses
      .map(responseData => (this.responseFieldName ? responseData[this.responseFieldName] : responseData))
      .filter(Boolean);

    this.onChange(this.responseData);
    this.uploadFinished.emit(availableResponses);
  }

  private disposeOfBulkUpload(): void {
    if (this.bulkUpload) {
      this.bulkUpload.dispose();
      this.bulkUpload = null;
    }
  }

  private addHiddenFileInput() {
    this.hiddenFileInput = this.createHiddenFileInputElement();
    this.renderer.appendChild(this.inputWrapper.nativeElement, this.hiddenFileInput);
  }

  private resetHiddenFileInputElement() {
    this.renderer.removeChild(this.inputWrapper.nativeElement, this.hiddenFileInput);
    this.addHiddenFileInput();
  }

  private createHiddenFileInputElement() {
    const input = this.renderer.createElement('input');
    this.renderer.setStyle(input, 'display', 'none');
    this.renderer.addClass(input, 'upload-input');
    this.renderer.setAttribute(input, 'type', 'file');
    this.renderer.setAttribute(input, 'name', 'upload');
    if (this.fileTypesAccepted?.length) {
      this.renderer.setAttribute(input, 'accept', fileTypesToAcceptAttribute(this.fileTypesAccepted));
    }
    this.renderer.setAttribute(input, 'multiple', 'multiple');
    this.renderer.listen(input, 'change', this.onFilesChange.bind(this));

    return input;
  }
}
