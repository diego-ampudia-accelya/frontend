/* eslint-disable @typescript-eslint/naming-convention */
export enum UploadModes {
  Initial = 'Initial',
  Preview = 'Preview',
  Upload = 'Upload'
}

export enum UploadType {
  Normal = 'Normal',
  Retry = 'Retry'
}

export enum UploadStatus {
  uploading = 'uploading',
  failed = 'failed',
  completed = 'completed',
  cancelled = 'cancelled'
}
