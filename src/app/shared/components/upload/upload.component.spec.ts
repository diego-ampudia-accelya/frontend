import { HttpClient } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { of, Subject } from 'rxjs';

import { BulkUpload } from './core/bulk-upload';
import { BulkUploadBuilder } from './core/bulk-upload-builder.service';
import { FileUpload } from './file-upload.model';
import { UploadModes, UploadStatus, UploadType } from './upload-mode.enum';
import { UploadComponent } from './upload.component';
import { TranslatePipeMock } from '~app/test';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';

const files = [
  new File(['firstSample'], 'firstSample.txt', { type: 'text/plain' }),
  new File(['secondSample'], 'secondSample.txt', { type: 'text/plain' })
];

const fileEvent = {
  target: { files }
};

describe('UploadComponent', () => {
  let component: UploadComponent;
  let fixture: ComponentFixture<UploadComponent>;
  let uploadBuilder: BulkUploadBuilder;

  let translationServiceSpy: SpyObject<L10nTranslationService>;
  let httpClientSpy: SpyObject<HttpClient>;
  let messageExtractorServiceSpy: SpyObject<MessageExtractorService>;
  let bulkUploadSpy: SpyObject<BulkUpload>;
  const bulkUploadFinished = new Subject();

  const dragEnter = new DragEvent('dragenter', { bubbles: true });
  const dragLeave = new DragEvent('dragleave', { bubbles: true });
  const dragOver = new DragEvent('dragover', { bubbles: true });

  beforeEach(waitForAsync(() => {
    translationServiceSpy = createSpyObject(L10nTranslationService);
    httpClientSpy = createSpyObject(HttpClient);
    messageExtractorServiceSpy = createSpyObject(MessageExtractorService);

    httpClientSpy.get.and.returnValue(of({}));
    translationServiceSpy.translate.and.returnValue('translated');

    TestBed.configureTestingModule({
      declarations: [UploadComponent, TranslatePipeMock],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        // The builder can create FileUpload instances
        // Providing it here makes testing a bit easier.
        BulkUploadBuilder,
        { provide: L10nTranslationService, useValue: translationServiceSpy },
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: MessageExtractorService, useValue: messageExtractorServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    uploadBuilder = TestBed.inject(BulkUploadBuilder);
    // We don't want to use the real BulkUpload so we stub it
    bulkUploadSpy = createSpyObject(BulkUpload, {
      uploadFinished$: bulkUploadFinished.asObservable()
    });
    bulkUploadSpy.upload.and.returnValue(of({}));
    spyOn(uploadBuilder, 'createBulkUploader').and.returnValue(bulkUploadSpy);

    spyOn(component.filesChange, 'emit');
    spyOn(component.uploadFinished, 'emit');
    spyOn(component.uploadPreview, 'emit');
    spyOn(component.uploadStarted, 'emit');
    spyOn(component.resetInitialMode, 'emit');
    spyOn(component, 'onChange');
    spyOn(component.modeChange, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('isDragDropActive', () => {
    it('should return `false` when disabled', () => {
      component.isDisabled = true;
      component.isDragAndDropActive = true;

      expect(component.isDragAndDropActive).toBe(false);
    });

    it('should return `true` when active and enabled', () => {
      component.isDisabled = false;
      component.isDragAndDropActive = true;

      expect(component.isDragAndDropActive).toBe(true);
    });

    it('should return `false` when inactive and enabled', () => {
      component.isDisabled = false;
      component.isDragAndDropActive = false;

      expect(component.isDragAndDropActive).toBe(false);
    });
  });

  describe('onChanges', () => {
    it('should map files to fileToUpload type', fakeAsync(() => {
      component.ngOnChanges({ files: new SimpleChange(null, files, false) });

      shouldMapFilesToFileToUpload();
    }));

    it('should emit filesChange', () => {
      component.ngOnChanges({ files: new SimpleChange(null, files, false) });
      expect(component.filesChange.emit).toHaveBeenCalledWith(component.filesToUpload);
    });

    it('should emit uploadPreview', () => {
      component.ngOnChanges({ files: new SimpleChange(null, files, false) });

      expect(component.uploadPreview.emit).toHaveBeenCalledTimes(1);
    });

    it('should emit modeChange', () => {
      component.ngOnChanges({ files: new SimpleChange(null, files, false) });

      expect(component.modeChange.emit).toHaveBeenCalledTimes(1);
      expect(component.modeChange.emit).toHaveBeenCalledWith(UploadModes.Preview);
    });

    it('should have no effect when `files` are not changed', () => {
      component.ngOnChanges({});
      expect(component.filesToUpload).toEqual([]);
    });

    it('should set requestData for each file when it is specified', () => {
      const requestData = { test: '123' };
      component.ngOnChanges({ files: new SimpleChange(null, files, false) });

      component.ngOnChanges({ requestData: new SimpleChange(null, requestData, false) });

      expect(component.filesToUpload.map(file => file.requestData)).toEqual([requestData, requestData]);
    });

    it('should set requestData when new files are added', () => {
      const requestData = { test: '123' };
      component.requestData = requestData;

      component.ngOnChanges({ files: new SimpleChange(null, files, false) });

      expect(component.filesToUpload.map(file => file.requestData)).toEqual([requestData, requestData]);
    });

    it('should reset when setting isDisabled to true and some files are already added', () => {
      component.ngOnChanges({ files: new SimpleChange(null, files, false) });
      component.isDisabled = true;

      component.ngOnChanges({
        isDisabled: new SimpleChange(false, true, false)
      });

      expect(component.filesToUpload).toEqual([]);
      expect(component.mode).toBe(UploadModes.Initial);
    });
  });

  describe('dragenter', () => {
    it('should set `isDraggingOver` to `true` when dragging over the drop area', () => {
      component.isDragAndDropActive = true;

      component.targetDropArea.dispatchEvent(dragEnter);

      expect(component.isDraggingOver).toBe(true);
    });

    it('should leave `isDraggingOver` unchanged when not dragging over the drop area', () => {
      component.isDragAndDropActive = true;

      fixture.nativeElement.dispatchEvent(dragEnter);

      expect(component.isDraggingOver).toBe(false);
    });

    it('should leave `isDraggingOver` unchanged when drag & drop is inactive', () => {
      component.targetDropArea.dispatchEvent(dragEnter);

      expect(component.isDraggingOver).toBe(false);
    });
  });

  describe('dragleave', () => {
    it('should set `isDraggingOver` to `false` when dragging away from the drop area', () => {
      component.isDragAndDropActive = true;
      component.targetDropArea.dispatchEvent(dragEnter);

      component.targetDropArea.dispatchEvent(dragLeave);

      expect(component.isDraggingOver).toBe(false);
    });

    it('should leave `isDraggingOver` unchanged when dragging away from another element', () => {
      component.isDragAndDropActive = true;
      component.targetDropArea.dispatchEvent(dragEnter);

      fixture.nativeElement.dispatchEvent(dragLeave);

      expect(component.isDraggingOver).toBe(true);
    });

    it('should leave `isDraggingOver` unchanged when drag & drop is inactive', () => {
      component.targetDropArea.dispatchEvent(dragLeave);

      expect(component.isDraggingOver).toBe(false);
    });
  });

  describe('dragover', () => {
    it('should prevent default of dragover event when drag&drop is active', () => {
      component.isDragAndDropActive = true;
      const spy = spyOn(dragOver, 'preventDefault');

      fixture.nativeElement.dispatchEvent(dragOver);

      expect(spy).toHaveBeenCalled();
    });

    it('should not prevent default of dragover event when drag&drop is inactive', () => {
      const spy = spyOn(dragOver, 'preventDefault');

      fixture.nativeElement.dispatchEvent(dragOver);

      expect(spy).not.toHaveBeenCalled();
    });
  });

  describe('drop', () => {
    it('should add files to upload when a file is dropped in the drop area', () => {
      const fileToDrop = new File(['test'], 'test.txt');
      const dropEvent = new DragEvent('drop', { bubbles: true, dataTransfer: new DataTransfer() });
      spyOnProperty(dropEvent.dataTransfer, 'files').and.returnValue([fileToDrop] as any);
      component.isDragAndDropActive = true;

      component.targetDropArea.dispatchEvent(dropEvent);

      expect(component.filesToUpload[0].name).toBe('test.txt');
    });

    it('should do nothing when a file is dropped in the drop area and the drag & drop is disabled', () => {
      const fileToDrop = new File(['test'], 'test.txt');
      const dropEvent = new DragEvent('drop', { bubbles: true, dataTransfer: new DataTransfer() });
      spyOnProperty(dropEvent.dataTransfer, 'files').and.returnValue([fileToDrop] as any);

      component.targetDropArea.dispatchEvent(dropEvent);

      expect(component.filesToUpload).toEqual([]);
    });

    it('should do nothing when the event contains no files', () => {
      const dropEvent = new DragEvent('drop', { bubbles: true, dataTransfer: new DataTransfer() });
      spyOnProperty(dropEvent.dataTransfer, 'files').and.returnValue([] as any);
      component.isDragAndDropActive = true;

      component.targetDropArea.dispatchEvent(dropEvent);

      expect(component.filesToUpload).toEqual([]);
    });
  });

  describe('onDropAreaClick', () => {
    let dispatchEventSpy: jasmine.Spy;
    beforeEach(() => {
      const uploadInput: HTMLInputElement = fixture.debugElement.query(By.css('.upload-input')).nativeElement;
      dispatchEventSpy = spyOn(uploadInput, 'dispatchEvent');
    });

    it('should click the hidden file input when enabled', () => {
      component.onDropAreaClick();

      expect(dispatchEventSpy).toHaveBeenCalled();
    });

    it('should have no effect when disabled', () => {
      component.isDisabled = true;
      component.onDropAreaClick();

      expect(dispatchEventSpy).not.toHaveBeenCalled();
    });
  });

  describe('onFilesChange', () => {
    it('should emit uploadPreview', () => {
      component.onFilesChange(fileEvent);

      expect(component.uploadPreview.emit).toHaveBeenCalledTimes(1);
    });

    it('should emit modeChange', () => {
      component.onFilesChange(fileEvent);

      expect(component.modeChange.emit).toHaveBeenCalledTimes(1);
      expect(component.modeChange.emit).toHaveBeenCalledWith(UploadModes.Preview);
    });

    it('should emit filesChange', () => {
      component.onFilesChange(fileEvent);

      expect(component.filesToUpload.length).toBe(2);
      expect(component.filesChange.emit).toHaveBeenCalledWith(component.filesToUpload);
    });

    it('should map passed files to fileToUpload', fakeAsync(() => {
      component.onFilesChange(fileEvent);
      shouldMapFilesToFileToUpload();
    }));

    it('should not do anything when called with empty array', fakeAsync(() => {
      component.onFilesChange({ target: { files: [] } });
      expect(component.filesToUpload).toEqual([]);
    }));

    it('should emit uploadPreview', () => {
      component.onFilesChange(fileEvent);

      expect(component.uploadPreview.emit).toHaveBeenCalledTimes(1);
    });

    it('should should not allow adding files with the same file name', () => {
      component.onFilesChange(fileEvent);
      component.onFilesChange(fileEvent);

      expect(component.filesToUpload.length).toBe(2);
      expect(component.filesChange.emit).toHaveBeenCalledWith(component.filesToUpload);
    });

    it('should map passed files to fileToUpload', fakeAsync(() => {
      component.onFilesChange(fileEvent);
      shouldMapFilesToFileToUpload();
    }));

    it('should mark file as invalid when it violates a validation rule', () => {
      const errorFile = files[0];
      component.validationRules = [file => file === errorFile && { message: 'error' }];

      component.onFilesChange(fileEvent);

      const actualErrorFile = component.filesToUpload.find(file => !file.isValid);
      expect(actualErrorFile).toBeDefined();
      expect(actualErrorFile.error).toBe('translated');
    });

    it('should not add files when disabled', () => {
      component.isDisabled = true;

      component.onFilesChange(fileEvent);

      expect(component.filesToUpload.length).toBe(0);
    });
  });

  describe('removeFile', () => {
    it('should remove file, and trigger filesChange', fakeAsync(() => {
      component.onFilesChange(fileEvent);
      const firstElement = component.filesToUpload[0];
      component.removeFile(firstElement);

      expect(component.filesToUpload).not.toContain(firstElement);
      expect(component.filesChange.emit).toHaveBeenCalledWith(component.filesToUpload);
    }));

    it('should remove file and trigger resetInitialMode, when filesToUpload contains one file', fakeAsync(() => {
      component.onFilesChange({ target: { files: [fileEvent.target.files[0]] } });
      const elementToRemove = component.filesToUpload[0];
      component.removeFile(elementToRemove);

      expect(component.filesToUpload).toEqual([]);
      expect(component.resetInitialMode.emit).toHaveBeenCalledTimes(1);
    }));

    it('should throw if file cannot be removed', () => {
      component.onFilesChange({ target: { files: [fileEvent.target.files[0]] } });
      const elementToRemove = component.filesToUpload[0];
      elementToRemove.status = UploadStatus.failed;

      expect(() => component.removeFile(elementToRemove)).toThrowError(/UploadComponent/);
    });
  });

  describe('retryUpload', () => {
    let file: FileUpload;

    beforeEach(() => {
      component.uploadPath = 'upload';
      component.onFilesChange(fileEvent);
      component.uploadFiles();
      file = component.filesToUpload[0];
      file.status = UploadStatus.failed;
    });

    it('should emit `uploadStarted`', fakeAsync(() => {
      component.retryUpload(file);

      expect(component.uploadStarted.emit).toHaveBeenCalledWith(UploadType.Retry);
    }));

    it('should call BulkUpload.upload', fakeAsync(() => {
      component.retryUpload(file);

      expect(bulkUploadSpy.upload).toHaveBeenCalledWith(file);
    }));

    it('should throw if file cannot be retried', fakeAsync(() => {
      file.isValid = false;

      expect(() => component.retryUpload(file)).toThrowError(/UploadComponent/);
    }));
  });

  describe('uploadFiles', () => {
    beforeEach(() => {
      component.uploadPath = 'test';
    });

    it('should throw if no `uploadPath` is provided', fakeAsync(() => {
      component.uploadPath = null;

      expect(() => component.uploadFiles()).toThrowError(/UploadComponent/);
    }));

    it('should return `false` when there are no valid files to upload', fakeAsync(() => {
      let actual: boolean;
      component.uploadFiles().subscribe(result => (actual = result));

      expect(actual).toBe(false);
    }));

    it('should emit `uploadStarted` when there are valid files to upload', fakeAsync(() => {
      component.onFilesChange(fileEvent);

      component.uploadFiles();

      expect(component.uploadStarted.emit).toHaveBeenCalledWith(UploadType.Normal);
    }));

    it('should emit `modeChange` when there are valid files to upload', fakeAsync(() => {
      component.onFilesChange(fileEvent);

      component.uploadFiles();

      expect(component.modeChange.emit).toHaveBeenCalledWith(UploadModes.Upload);
    }));

    it('should trigger an upload for each valid file', fakeAsync(() => {
      component.onFilesChange(fileEvent);
      const [validFile, invalidFile] = component.filesToUpload;
      invalidFile.isValid = false;

      component.uploadFiles();

      expect(bulkUploadSpy.upload).toHaveBeenCalledTimes(1);
      expect(bulkUploadSpy.upload).toHaveBeenCalledWith(validFile);
    }));

    it('should return all files when all files have finished uploading successfully', fakeAsync(() => {
      component.onFilesChange(fileEvent);
      const [file1, file2] = component.filesToUpload;

      let isSuccessful: boolean;
      component.uploadFiles().subscribe(result => (isSuccessful = result));

      // simulate an upload completion
      file1.status = UploadStatus.completed;
      file1.responseData = { id: 1 };
      file2.status = UploadStatus.completed;
      file2.responseData = { id: 2 };
      bulkUploadFinished.next(component.filesToUpload);

      expect(isSuccessful).toBeTruthy();
      expect(component.onChange).toHaveBeenCalledWith([file1.responseData, file2.responseData]);
      expect(component.uploadFinished.emit).toHaveBeenCalledWith([file1.responseData, file2.responseData]);
    }));

    it('should return only completed files when upload finished with some failures', fakeAsync(() => {
      component.onFilesChange(fileEvent);
      const [file1, file2] = component.filesToUpload;

      let isSuccessful: boolean;
      component.uploadFiles().subscribe(result => (isSuccessful = result));

      // simulate an upload completion
      file1.status = UploadStatus.completed;
      file1.responseData = { id: 1 };
      file2.status = UploadStatus.failed;
      bulkUploadFinished.next(component.filesToUpload);

      expect(isSuccessful).toBeFalsy();
      expect(component.onChange).toHaveBeenCalledWith([file1.responseData]);
      expect(component.uploadFinished.emit).toHaveBeenCalledWith([file1.responseData]);
    }));

    it('should return even if no responseData', fakeAsync(() => {
      component.onFilesChange(fileEvent);
      const [file1, file2] = component.filesToUpload;

      let isSuccessful: boolean;
      component.uploadFiles().subscribe(result => (isSuccessful = result));

      // simulate an upload completion
      file1.status = UploadStatus.completed;
      file1.responseData = null;
      file2.status = UploadStatus.completed;
      file1.responseData = null;
      bulkUploadFinished.next(component.filesToUpload);

      expect(isSuccessful).toBeTruthy();
    }));
  });

  function shouldMapFilesToFileToUpload() {
    component.filesToUpload.forEach(file => {
      expect(file.name).toBeDefined();
      expect(file.progress).toBeDefined();
      expect(file.size).toBeDefined();
      expect(file.cancelled).toBeDefined();
      expect(file.fileInstance).toBeDefined();
      expect(file.error).toBeDefined();
    });
  }
});
