import { Component, EventEmitter, forwardRef, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RadioButton } from 'primeng/radiobutton';

@Component({
  selector: 'bspl-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioButtonComponent),
      multi: true
    }
  ]
})
export class RadioButtonComponent implements ControlValueAccessor {
  @Input() name: string;
  @Input() value: any;
  @Input() label: string;
  @Input() disabled: boolean;

  @ViewChild(RadioButton, { static: true }) radioButton: RadioButton;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() click = new EventEmitter();

  writeValue(value) {
    this.radioButton.writeValue(value);
  }

  registerOnChange(fn) {
    this.radioButton.registerOnChange(fn);
  }

  registerOnTouched(fn) {
    this.radioButton.registerOnTouched(fn);
  }

  clickRadioButton(event: Event) {
    event.preventDefault();
    event.stopPropagation();

    if (this.disabled) {
      return;
    }

    this.radioButton.select(event);
    this.click.emit(this.radioButton.value);
  }
}
