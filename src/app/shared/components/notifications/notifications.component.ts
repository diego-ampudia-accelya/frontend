import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bspl-notification',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationComponent {}
