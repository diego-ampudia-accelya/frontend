import { Component, HostBinding, Input } from '@angular/core';

import { AlertMessageType } from '~app/shared/enums/alert-message-type.enum';

@Component({
  selector: 'bspl-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss']
})
export class AlertMessageComponent {
  @Input() message: string;
  @Input() type: AlertMessageType;
  @Input() errorCode: number | string;

  public iconTypeMapper = {
    [AlertMessageType.warning]: 'warning',
    [AlertMessageType.success]: 'check_circle'
  };

  @HostBinding('class.warningMessage') get isWarningMessage() {
    return this.type && this.type === AlertMessageType.warning;
  }

  @HostBinding('class.infoMessage') get isInfoMessage() {
    return this.type && this.type === AlertMessageType.info;
  }

  @HostBinding('class.errorMessage') get isErrorMessage() {
    return this.type && this.type === AlertMessageType.error;
  }

  @HostBinding('class.successMessage') get isSuccessMessage() {
    return this.type && this.type === AlertMessageType.success;
  }
}
