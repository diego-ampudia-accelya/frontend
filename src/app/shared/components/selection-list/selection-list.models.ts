export interface SelectionList {
  name: string;
  expanded?: boolean;
  hasChanged?: boolean;
  checked?: boolean;
  value?: any;
  disabled: boolean;
  checkboxDesign?: string;
  items: SelectionListItem[];
  tooltip?: string;
}

export interface SelectionListItem {
  name: string;
  checked: boolean;
  value?: any;
  hasChanged: boolean;
  disabled: boolean;
  tooltip?: string;
}
