import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef
} from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { cloneDeep } from 'lodash';

import { CheckboxDesign } from '../buttons/enums/checkboxDesign';

import { SelectionList } from './selection-list.models';

@Component({
  selector: 'bspl-selection-list',
  templateUrl: './selection-list.component.html',
  styleUrls: ['./selection-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectionListComponent implements OnInit, OnChanges {
  @Input() listData: SelectionList[];
  @Input() canEdit: boolean;
  @Output() selectionChange = new EventEmitter<SelectionList[]>();

  @Input() public editItemTemplate: TemplateRef<unknown>;
  @Input() public editCategoryTemplate: TemplateRef<unknown>;

  public originalListData: SelectionList[];

  public get allCategoriesExpanded(): boolean {
    return this.listData.every(category => category.expanded);
  }

  public get allCategoriesSelected(): boolean {
    return this.listData
      .filter(category => category.disabled === false)
      .every(category => {
        const checkForDisabledItems = category.items.every(item =>
          item.disabled ? item.checked === false : item.checked
        );

        return (
          (category.checked && category.checkboxDesign === CheckboxDesign.Normal) ||
          (category.checked && category.checkboxDesign === CheckboxDesign.Minus && checkForDisabledItems)
        );
      });
  }

  constructor(private translationService: L10nTranslationService) {}

  ngOnInit() {
    this.initializeData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.listData && changes.listData.currentValue && changes.listData.previousValue) {
      this.initializeData();
    }
  }

  public initializeData() {
    this.mapAllCategoriesStateByItems();
    this.originalListData = cloneDeep(this.listData);
  }

  public mapAllCategoriesStateByItems(): void {
    this.listData.forEach(category => {
      const checkedItemsCount = category.items.filter(item => item.checked).length;
      this.mapCategoryByCheckedItemsCount(category, checkedItemsCount);
      if (category.disabled) {
        this.disableItemsWithCategoryDisabled(category);
      }

      this.disableCategoriesWithAllItemsDisabled(category);
    });
  }

  public mapCategoryByCheckedItemsCount(category: SelectionList, checkedItemsCount: number): void {
    if (checkedItemsCount === category.items.length) {
      // All items are selected
      category.checked = true;
      category.checkboxDesign = CheckboxDesign.Normal;
    } else if (checkedItemsCount === 0) {
      // No items are checked
      category.checked = false;
      category.checkboxDesign = CheckboxDesign.Normal;
    } else {
      // Some items are selected
      category.checked = true;
      category.checkboxDesign = CheckboxDesign.Minus;
    }
  }

  public disableCategoriesWithAllItemsDisabled(category: SelectionList) {
    category.disabled = category.items.every(item => item.disabled);
  }

  public disableItemsWithCategoryDisabled(category: SelectionList) {
    category.items.forEach(item => (item.disabled = true));
  }

  public onCategoryToggle(newState: boolean, categoryIndex: number): void {
    const category = this.listData[categoryIndex];
    const originalCategory = this.originalListData[categoryIndex];

    category.items.forEach((item, index) => {
      if (!item.disabled) {
        item.checked = newState;
        item.hasChanged = item.checked !== originalCategory.items[index].checked;
      }
    });

    const selectedItemsInCategory = category.items.filter(item => item.checked).length;
    this.mapCategoryByCheckedItemsCount(category, selectedItemsInCategory);

    category.hasChanged =
      category.checked !== originalCategory.checked || category.checkboxDesign !== originalCategory.checkboxDesign;

    this.selectionChange.emit(this.listData);
  }

  public onItemToggle(newState: boolean, categoryIndex: number, itemIndex: number): void {
    const category = this.listData[categoryIndex];
    const categoryItem = category.items[itemIndex];
    const originalCategory = this.originalListData[categoryIndex];
    const originalCategoryItem = originalCategory.items[itemIndex];

    categoryItem.checked = newState;
    categoryItem.hasChanged = categoryItem.checked !== originalCategoryItem.checked;

    const checkedItemsInCategory = category.items.filter(item => item.checked).length;
    this.mapCategoryByCheckedItemsCount(category, checkedItemsInCategory);
    category.hasChanged = category.items.some(item => item.hasChanged);

    this.selectionChange.emit(this.listData);
  }

  public toggleCategoryExpand(categoryIndex: number): void {
    this.listData[categoryIndex].expanded = !this.listData[categoryIndex].expanded;
  }

  public expandAll(): void {
    this.listData.forEach(category => (category.expanded = true));
  }

  public collapseAll(): void {
    this.listData.forEach(category => (category.expanded = false));
  }

  public selectAll(): void {
    this.listData.forEach((category, index) => {
      if (!category.disabled) {
        this.onCategoryToggle(true, index);
      }
    });
    this.selectionChange.emit(this.listData);
  }

  public deselectAll(): void {
    this.listData.forEach((category, index) => {
      if (!category.disabled) {
        this.onCategoryToggle(false, index);
      }
    });
    this.selectionChange.emit(this.listData);
  }

  public getTooltipForCategory(categoryIndex: number) {
    const tooltipKey = this.listData[categoryIndex].tooltip || '';

    return this.translationService.translate(tooltipKey);
  }

  public getTooltipForItem(categoryIndex: number, itemIndex: number) {
    const tooltipKey = this.listData[categoryIndex].items[itemIndex].tooltip || '';

    return this.translationService.translate(tooltipKey);
  }
}
