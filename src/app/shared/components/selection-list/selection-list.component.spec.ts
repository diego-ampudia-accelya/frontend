import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { CheckboxDesign } from '../buttons/enums/checkboxDesign';

import { SelectionListComponent } from './selection-list.component';
import { SELECTION_LIST } from './selection-list.mock';
import { SelectionList } from './selection-list.models';

describe('SelectionListComponent', () => {
  let component: SelectionListComponent;
  let fixture: ComponentFixture<SelectionListComponent>;
  let translationServiceStub: SpyObject<L10nTranslationService>;

  beforeEach(waitForAsync(() => {
    translationServiceStub = createSpyObject(L10nTranslationService);
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, NoopAnimationsModule],
      declarations: [SelectionListComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{ provide: L10nTranslationService, useValue: translationServiceStub }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionListComponent);
    component = fixture.componentInstance;
    component.listData = SELECTION_LIST;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check a category if all items are checked', () => {
    const category: SelectionList = {
      name: 'Category 3',
      expanded: false,
      hasChanged: false,
      disabled: false,
      items: [
        {
          name: 'Item 1',
          checked: true,
          hasChanged: false,
          disabled: false
        },
        {
          name: 'Item 2',
          checked: true,
          hasChanged: false,
          disabled: false
        }
      ]
    };

    component.mapCategoryByCheckedItemsCount(category, 2);
    expect(category.checked).toEqual(true);
    expect(category.checkboxDesign).toEqual(CheckboxDesign.Normal);
  });

  it('should check a category with minus if some items are checked', () => {
    const category: SelectionList = {
      name: 'Category 3',
      expanded: false,
      hasChanged: false,
      disabled: false,
      items: [
        {
          name: 'Item 1',
          checked: false,
          hasChanged: false,
          disabled: false
        },
        {
          name: 'Item 2',
          checked: true,
          hasChanged: false,
          disabled: false
        }
      ]
    };

    component.mapCategoryByCheckedItemsCount(category, 1);
    expect(category.checked).toEqual(true);
    expect(category.checkboxDesign).toEqual(CheckboxDesign.Minus);
  });

  it('should not check a category if no items are checked', () => {
    const category: SelectionList = {
      name: 'Category 3',
      expanded: false,
      hasChanged: false,
      disabled: false,
      items: [
        {
          name: 'Item 1',
          hasChanged: false,
          checked: false,
          disabled: false
        },
        {
          name: 'Item 2',
          hasChanged: false,
          checked: false,
          disabled: false
        }
      ]
    };

    component.mapCategoryByCheckedItemsCount(category, 0);
    expect(category.checked).toEqual(false);
    expect(category.checkboxDesign).toEqual(CheckboxDesign.Normal);
  });

  it('should select all items in category, when category is toggled on', () => {
    component.onCategoryToggle(false, 1);
    component.onCategoryToggle(true, 1);
    expect(component.listData[1].items[0].checked).toEqual(true);
  });

  it('should correctly toggle an item in the list', () => {
    component.onItemToggle(false, 2, 0);
    expect(component.listData[2].items[0].checked).toEqual(false);
  });

  it('should expand a category', () => {
    const originalExpandedState = component.listData[2].expanded;
    component.toggleCategoryExpand(2);
    expect(component.listData[2].expanded).toEqual(!originalExpandedState);
  });

  it('should expand all categories', () => {
    component.expandAll();
    expect(component.listData[0].expanded).toEqual(true);
    expect(component.listData[1].expanded).toEqual(true);
    expect(component.listData[2].expanded).toEqual(true);
  });

  it('should collapse all categories', () => {
    component.expandAll();
    component.collapseAll();
    expect(component.listData[0].expanded).toEqual(false);
    expect(component.listData[1].expanded).toEqual(false);
    expect(component.listData[2].expanded).toEqual(false);
  });
});
