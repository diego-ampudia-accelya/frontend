import { SelectionList } from './selection-list.models';

export const SELECTION_LIST: SelectionList[] = [
  {
    name: 'Category 1',
    expanded: false,
    hasChanged: false,
    disabled: false,
    items: [
      {
        name: 'Item 1',
        hasChanged: false,
        checked: true,
        disabled: false
      },
      {
        name: 'Item 2',
        hasChanged: false,
        checked: true,
        disabled: false
      },
      {
        name: 'Item 3',
        hasChanged: false,
        checked: true,
        disabled: false
      }
    ]
  },
  {
    name: 'Category 2',
    expanded: false,
    hasChanged: false,
    disabled: false,
    items: [
      {
        name: 'Item 1',
        hasChanged: false,
        checked: true,
        disabled: false
      }
    ]
  },
  {
    name: 'Category 3',
    expanded: false,
    hasChanged: false,
    disabled: false,
    items: [
      {
        name: 'Item 1',
        hasChanged: false,
        checked: true,
        disabled: false
      },
      {
        name: 'Item 2',
        hasChanged: false,
        checked: true,
        disabled: false
      }
    ]
  }
];
