import { Injectable } from '@angular/core';

import { ModalAction } from '~app/shared/models/modal-action.model';

@Injectable()
export class DialogConfig<T = any> {
  [key: string]: any;
  data: T & DialogConfigData;
}

export interface DialogConfigData {
  [key: string]: any;
  title?: string;
  footerButtonsType: string | ModalAction | (string | ModalAction)[];
  hasCancelButton?: boolean;
  tableColumnMap?: unknown;
  confirmationData?: any;
  isClosable?: boolean;
  buttons?: ModalAction[];
}
