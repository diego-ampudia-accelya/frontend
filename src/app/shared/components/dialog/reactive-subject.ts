import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class ReactiveSubject {
  private readonly asEmitter = new Subject<any>();
  public readonly asObservable: Observable<any> = this.asEmitter.asObservable();

  emit(data?: any) {
    this.asEmitter.next(data);
  }
}
