import { ButtonDesign } from '../../buttons/enums/buttonDesign';
import { FooterButton } from '../../buttons/enums/footerButton';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { ModalAction } from '~app/shared/models/modal-action.model';

export class ButtonsFooterFactory {
  public static getFooterButtonsByType(
    modalActions: ModalAction[],
    hasCancelButton = true,
    dataType?: MasterDataType
  ): ModalAction[] {
    const footerButtons: ModalAction[] = [];

    if (hasCancelButton) {
      footerButtons.push(this.generateButtonByType({ type: FooterButton.Cancel } as ModalAction));
    }

    modalActions.forEach(button => {
      footerButtons.push(this.generateButtonByType(button, dataType));
    });

    return footerButtons;
  }

  private static generateButtonByType(buttonSetting: ModalAction, dataType?: MasterDataType): ModalAction {
    const action: ModalAction = {
      title: this.getTitleKey(buttonSetting, dataType),
      buttonDesign: buttonSetting.buttonDesign || ButtonDesign.Primary,
      type: buttonSetting.type,
      isDisabled: buttonSetting.isDisabled,
      tooltipText: buttonSetting.tooltipText
    };

    switch (buttonSetting.type) {
      case FooterButton.Cancel:
        action.title = this.getTitleKey(buttonSetting, dataType);
        action.buttonDesign = ButtonDesign.Tertiary;
        break;
      case FooterButton.Again:
      case FooterButton.Ok_tertiary:
        action.title = this.getTitleKey(buttonSetting, dataType, false);
        action.buttonDesign = ButtonDesign.Tertiary;
        break;
    }

    return action;
  }

  private static getTitleKey(footerButton: ModalAction, dataType: MasterDataType, isDefaultButton = true): string {
    let key = `BUTTON.DEFAULT.${footerButton.type}`;
    if ((footerButton as any).title) {
      key = (footerButton as any).title;
    } // TODO For compatibility. Remove when we use FooterButton everywhere

    if (dataType && !isDefaultButton) {
      key = `BUTTON.${dataType}.${footerButton}`;
    }

    return key.toUpperCase();
  }
}
