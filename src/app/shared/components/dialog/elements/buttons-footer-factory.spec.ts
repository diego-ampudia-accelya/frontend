import { ButtonDesign } from '../../buttons/enums/buttonDesign';
import { FooterButton } from '../../buttons/enums/footerButton';

import { ButtonsFooterFactory } from './buttons-footer-factory';
import { ModalAction } from '~app/shared/models/modal-action.model';

describe('ButtonsFooterFactory', () => {
  describe('getFooterButtonsByType()', () => {
    it('should return empty array if no modal actions provided', () => {
      const actual = ButtonsFooterFactory.getFooterButtonsByType([], false, null);

      expect(actual).toEqual([]);
    });

    it('should provide Cancel ModalAction if hasCancelButton is true', () => {
      const expected: ModalAction[] = [
        {
          type: 'cancel',
          title: 'BUTTON.DEFAULT.CANCEL',
          buttonDesign: ButtonDesign.Tertiary,
          isDisabled: undefined,
          tooltipText: undefined
        }
      ];
      const actual = ButtonsFooterFactory.getFooterButtonsByType([], true, null);

      expect(actual).toEqual(expected);
      expect(actual.length).toBe(1);
    });

    it('should return Again ModalAction', () => {
      const expected: ModalAction[] = [
        {
          type: 'again',
          title: 'BUTTON.DEFAULT.AGAIN',
          buttonDesign: ButtonDesign.Tertiary,
          isDisabled: true,
          tooltipText: 'Try again'
        }
      ];
      const actual = ButtonsFooterFactory.getFooterButtonsByType(
        [{ type: FooterButton.Again, isDisabled: true, tooltipText: 'Try again' }],
        false,
        null
      );

      expect(actual).toEqual(expected);
      expect(actual.length).toBe(1);
    });

    it('should return multiple default ModalActions', () => {
      const expected: ModalAction[] = [
        {
          type: 'first_Button',
          title: 'BUTTON.DEFAULT.FIRST_BUTTON',
          buttonDesign: ButtonDesign.Primary,
          isDisabled: undefined,
          tooltipText: undefined
        },
        {
          type: 'second_Button',
          title: 'BUTTON.DEFAULT.SECOND_BUTTON',
          buttonDesign: ButtonDesign.Primary,
          isDisabled: undefined,
          tooltipText: undefined
        }
      ];
      const actual = ButtonsFooterFactory.getFooterButtonsByType(
        [{ type: 'first_Button' }, { type: 'second_Button' }],
        false,
        null
      );

      expect(actual).toEqual(expected);
      expect(actual.length).toBe(2);
    });

    it('should be able to set custom title', () => {
      const expected: ModalAction[] = [
        {
          type: 'my_button',
          title: 'MY.CUSTOM.TITLE',
          buttonDesign: ButtonDesign.Primary,
          isDisabled: undefined,
          tooltipText: undefined
        }
      ];
      const actual = ButtonsFooterFactory.getFooterButtonsByType(
        [{ type: 'my_button', title: 'my.custom.title' }],
        false,
        null
      );

      expect(actual).toEqual(expected);
    });
  });
});
