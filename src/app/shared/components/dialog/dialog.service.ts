import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Injectable,
  Injector,
  Type
} from '@angular/core';
import { Observable, of } from 'rxjs';
import { FooterButton } from '~app/shared/components/buttons/enums/footerButton';
import { ButtonDesign } from '../buttons/enums/buttonDesign';
import { InfoDialogComponent } from '../form-dialog-components/info-dialog/info-dialog.component';
import { PermissionErrorDialogComponent } from '../form-dialog-components/permission-error-dialog/permission-error-dialog.component';
import { RouteNotFoundDialogComponent } from '../route-not-found-dialog/route-not-found-dialog.component';
import { AdditionalTokensInjector } from './additional-tokens.injector';
import { DialogComponent } from './dialog.component';
import { DialogConfig } from './dialog.config';
import { ReactiveSubject } from './reactive-subject';

@Injectable({ providedIn: 'root' })
export class DialogService {
  dialogContentComponentRef: ComponentRef<DialogComponent>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector
  ) {}

  public open<T = any>(componentType: Type<any>, config: DialogConfig<T>): Observable<any> {
    if (this.isDialogOpen()) {
      // eslint-disable-next-line no-console
      console.warn('You are trying to open multiple Dialogs. Only one should be visible at a time.');

      return of();
    }

    // We are using this instance in the parent component.
    config.serviceRef = this;
    const dialogComponentRef = this.createAndAppendDialog(config);
    this.dialogContentComponentRef.instance.childComponentType = componentType;

    return dialogComponentRef;
  }

  public openInfoDialog(contentTranslationKey: string, titleTranslationKey = 'modalDialogs.infoTitle'): void {
    const config: DialogConfig = {
      data: {
        title: titleTranslationKey,
        hasCancelButton: false,
        footerButtonsType: FooterButton.Ok,
        contentTranslationKey
      }
    };

    this.open(InfoDialogComponent, config).subscribe(action => {
      if (action.clickedBtn === FooterButton.Ok) {
        this.close();
      }
    });
  }

  public openPermissionErrorDialog(): void {
    const config: DialogConfig = {
      data: {
        title: 'modalDialogs.permissionError.title',
        hasCancelButton: false,
        footerButtonsType: {
          type: FooterButton.Ok,
          buttonDesign: ButtonDesign.Tertiary
        }
      }
    };
    this.open(PermissionErrorDialogComponent, config).subscribe(action => {
      if (action.clickedBtn === FooterButton.Ok) {
        this.close();
      }
    });
  }

  public openNotFoundDialog(): void {
    const config: DialogConfig = {
      data: {
        title: 'notFoundDialog.title',
        hasCancelButton: false,
        isClosable: true,
        footerButtonsType: [{ type: FooterButton.Ok_tertiary }]
      }
    };
    this.open(RouteNotFoundDialogComponent, config).subscribe(() => {
      this.close();
    });
  }

  private createAndAppendDialog(config: DialogConfig) {
    // prepare injector tokens for DialogConfig and DialogRef
    const injectorTokens = new WeakMap();
    injectorTokens.set(DialogConfig, config);
    const reactiveSubject = new ReactiveSubject();
    injectorTokens.set(ReactiveSubject, reactiveSubject);

    // create component factory and component with custom injector and tokens
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DialogComponent);
    const componentRef = componentFactory.create(new AdditionalTokensInjector(this.injector, injectorTokens));

    // attach to component and dom trees
    this.appRef.attachView(componentRef.hostView);
    const componentDomElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.body.appendChild(componentDomElem);

    this.dialogContentComponentRef = componentRef;

    return reactiveSubject.asObservable;
  }

  public getDynamicInstance(): any {
    return this.dialogContentComponentRef.instance.getComponentRef().instance;
  }

  public isDialogOpen(): boolean {
    let isOpen = false;
    if (this.dialogContentComponentRef) {
      isOpen = !this.dialogContentComponentRef.hostView.destroyed;
    }

    return isOpen;
  }

  public close() {
    if (this.dialogContentComponentRef) {
      this.appRef.detachView(this.dialogContentComponentRef.hostView);
      this.dialogContentComponentRef.destroy();
    }
  }
}
