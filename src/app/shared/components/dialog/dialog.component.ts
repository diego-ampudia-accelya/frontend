import { animate, AnimationEvent, state, style, transition, trigger } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  HostListener,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  Type,
  ViewChild,
  ViewChildren
} from '@angular/core';

import { ButtonComponent } from '../buttons/button/button.component';
import { FooterButton } from '../buttons/enums/footerButton';

import { ButtonsFooterFactory } from './elements/buttons-footer-factory';
import { InsertionDirective } from './insertion.directive';
import { ReactiveSubject } from './reactive-subject';
import { ModalAction } from '~app/shared/models/modal-action.model';

@Component({
  selector: 'bspl-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  animations: [
    trigger('animation', [
      state(
        'void',
        style({
          transform: 'translate3d(-0%, 50%, 0) scale(0.5)',
          opacity: 0
        })
      ),
      state(
        'visible',
        style({
          transform: 'none',
          opacity: 1
        })
      ),
      transition('* => *', animate('{{transitionParams}}'))
    ])
  ]
})
export class DialogComponent implements OnInit, AfterViewInit, OnDestroy {
  public readonly CancelOperation = FooterButton.Cancel;

  public dialogConfig = {
    title: '',
    footerButtonsType: null,
    buttons: [],
    hasCancelButton: true,
    dataType: null,
    isClosable: true,
    mainButtonType: null,
    isInWideMode: false
  };

  public childComponentType: Type<any>;
  public dialogServiceRef;

  public isInWideMode: boolean;

  private modalOpenedClass = 'modal-opened';

  private get mainDialogButton() {
    return this.dialogConfig?.buttons.find(button => button.type === this.dialogConfig.mainButtonType);
  }

  protected componentRef: ComponentRef<any>;

  @ViewChild(InsertionDirective, { static: true }) insertionPoint: InsertionDirective;
  @ViewChild('overlay', { static: true }) overlay: ElementRef;
  @ViewChildren(ButtonComponent) buttons: QueryList<ButtonComponent>;

  @HostListener('window:keydown.enter')
  public submitOnEnter() {
    const shouldSubmitMainButton =
      this.mainDialogButton && !this.mainDialogButton.isDisabled && !this.hasFocusedButton();
    if (shouldSubmitMainButton) {
      this.onButtonClick(this.mainDialogButton.type);
    }
  }

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cd: ChangeDetectorRef,
    public reactiveSubject: ReactiveSubject,
    @Inject(DOCUMENT) private document: Document
  ) {}

  public get isClosable(): boolean {
    return typeof this.dialogConfig.isClosable === 'undefined' || this.dialogConfig.isClosable;
  }

  public loadChildComponent(componentType: Type<any>) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    const viewContainerRef = this.insertionPoint.viewContainerRef;

    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);

    if (this.componentRef.instance && this.componentRef.instance.config) {
      this.dialogServiceRef = this.componentRef.instance.config.serviceRef;
      this.dialogConfig = this.componentRef.instance.config.data;

      // TODO: Move to a separed method.
      let dialogButtons = this.dialogConfig.footerButtonsType;
      if (!Array.isArray(dialogButtons)) {
        dialogButtons = [dialogButtons];
      }

      const buttonsSettings: ModalAction[] = dialogButtons.map(button => {
        if (typeof button !== 'object') {
          return { type: button } as ModalAction;
        }

        return button;
      });

      this.dialogConfig.buttons = ButtonsFooterFactory.getFooterButtonsByType(
        buttonsSettings,
        this.dialogConfig.hasCancelButton,
        this.dialogConfig.dataType
      );

      this.isInWideMode = this.dialogConfig.isInWideMode;
    }
  }

  public ngOnInit(): void {
    this.document.body.classList.add(this.modalOpenedClass);
    this.blurActiveElementBehindDialog();
  }

  public ngAfterViewInit() {
    this.loadChildComponent(this.childComponentType);
    this.cd.detectChanges();
  }

  public ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
    }

    this.document.body.classList.remove(this.modalOpenedClass);
  }

  public onDialogClicked(event: MouseEvent) {
    if (event.target === this.overlay.nativeElement) {
      this.reactiveSubject.emit({});
    }
  }

  public onAnimationStart(event: AnimationEvent) {
    if (event.toState === 'void') {
      this.ngOnDestroy();
    }
  }

  public onAnimationEnd(event: AnimationEvent) {
    if (event.toState === 'void') {
      this.reactiveSubject.emit({});
    }
  }

  public onButtonClick(buttonType: string) {
    this.reactiveSubject.emit({
      clickedBtn: buttonType,
      contentComponentRef: this.componentRef.instance
    });

    if (buttonType === this.CancelOperation) {
      this.close();
    }
  }

  public getComponentRef() {
    return this.componentRef;
  }

  private blurActiveElementBehindDialog() {
    (this.document.activeElement as HTMLElement).blur();
  }

  private hasFocusedButton(): boolean {
    return this.buttons.some(button => button.isFocused());
  }

  // PopState event is fired when the active history entry changes while the user navigates the session history
  // (i.e. user navigates backward or forward with browser buttons)
  @HostListener('document:keydown.escape')
  @HostListener('window:popstate')
  protected close() {
    if (this.isClosable) {
      this.reactiveSubject.emit({
        clickedBtn: FooterButton.Cancel
      });

      this.dialogServiceRef.close();
    }
  }
}
