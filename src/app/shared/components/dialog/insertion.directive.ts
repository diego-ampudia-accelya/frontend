import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[bsplInsertion]'
})
export class InsertionDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
