import { Component, ComponentRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';

import { FooterButton } from '../buttons/enums/footerButton';
import { DialogComponent } from './dialog.component';
import { DialogService } from './dialog.service';
import { ReactiveSubject } from './reactive-subject';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { TooltipDirectiveModule } from '~app/shared/directives/tooltip/tooltip.module';
import { SharedModule } from '~app/shared/shared.module';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

const dialogConfigData = {
  footerButtonsType: FooterButton.Ok,
  title: 'Mocked dialog',
  dataType: null,
  hasCancelButton: true,
  isClosable: true
};

@Component({
  selector: 'bspl-dialog-mock',
  template: '<p>Mock Component</p>'
})
class DialogMockComponent {
  public config = {
    serviceRef: jasmine.createSpyObj('DialogService', ['close']),
    data: dialogConfigData
  };

  public componentRef = { instance: { config: { serviceRef: jasmine.createSpyObj('DialogService', ['close']) } } };
}

// TODO REVIEW: some of these tests are throwing error `TypeError: Cannot read property 'close' of undefined` sometimes in line `dialog.component.ts:209:29`
xdescribe('DialogComponent', () => {
  let component: DialogComponent;
  let fixture: ComponentFixture<DialogComponent>;
  const translationServiceSpy = jasmine.createSpyObj('L10nTranslationService', ['translate']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig), TooltipDirectiveModule, NoopAnimationsModule],
      declarations: [DialogMockComponent, TranslatePipeMock],
      providers: [
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        },
        DialogService,
        FormBuilder,
        ReactiveSubject,
        SharedModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(DialogComponent);
    component = fixture.componentInstance;
    component.childComponentType = DialogMockComponent;
    component.loadChildComponent(DialogMockComponent);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should CancelOperation be with value cancel', () => {
    expect(component.CancelOperation).toBe(FooterButton.Cancel);
  });

  it('should contain dialogConfig keys when mocked component is loaded', () => {
    const createdButton = [
      {
        title: 'BUTTON.DEFAULT.CANCEL',
        buttonDesign: 'tertiary',
        type: 'cancel',
        isDisabled: undefined,
        tooltipText: undefined
      },
      {
        title: 'BUTTON.DEFAULT.OK',
        buttonDesign: 'primary',
        type: 'ok',
        isDisabled: undefined,
        tooltipText: undefined
      }
    ];

    expect(Object.keys(component.dialogConfig)).toContain('title');
    expect(Object.keys(component.dialogConfig)).toContain('footerButtonsType');
    expect(Object.keys(component.dialogConfig)).toContain('buttons');
    expect(Object.keys(component.dialogConfig)).toContain('hasCancelButton');
    expect(Object.keys(component.dialogConfig)).toContain('isClosable');

    expect(component.dialogConfig.buttons).toEqual(createdButton);
    expect(component.dialogConfig.footerButtonsType).toBe(FooterButton.Ok);
    expect(component.dialogConfig.title).toBe(dialogConfigData.title);
    expect(component.dialogConfig.hasCancelButton).toBe(true);
    expect(component.dialogConfig.isClosable).toBe(true);
  });

  it('should emit reactiveSubject when onDialogClicked is called', waitForAsync(() => {
    let event = new MouseEvent('click', { bubbles: true, cancelable: true });
    event = Object.defineProperty(event, 'target', { value: component.overlay.nativeElement });

    const componentEmitter = component.reactiveSubject;
    spyOn(componentEmitter, 'emit');
    fixture.whenStable().then(() => {
      expect(component.reactiveSubject.emit).toHaveBeenCalled();
    });

    component.onDialogClicked(event);
  }));

  it('should reactiveSubject be triggered when onButtonClick is called', waitForAsync(() => {
    const componentEmitter = component.reactiveSubject;
    const buttonType = FooterButton.Done;
    const componentRef = component.getComponentRef();

    spyOn(componentEmitter, 'emit');
    fixture.whenStable().then(() => {
      expect(component.reactiveSubject.emit).toHaveBeenCalledWith({
        clickedBtn: buttonType,
        contentComponentRef: componentRef.instance
      });
    });
    component.onButtonClick(buttonType);
  }));

  it('should close dialog when cancel action is triggered', waitForAsync(() => {
    const componentEmitter = component.reactiveSubject;

    spyOn(componentEmitter, 'emit');
    fixture.whenStable().then(() => {
      expect(component.reactiveSubject.emit).toHaveBeenCalledWith({
        clickedBtn: FooterButton.Cancel
      });

      expect(component.dialogServiceRef.close).toHaveBeenCalled();
    });

    component.onButtonClick(FooterButton.Cancel);
  }));

  it('should return componentRef when getComponentRef is called', () => {
    expect(component.getComponentRef() instanceof ComponentRef).toBe(true);
  });

  it('should close dialog when popstate event is triggered', fakeAsync(() => {
    const event = new PopStateEvent('popstate');

    dispatchEvent(event);
    tick();

    expect(component.dialogServiceRef.close).toHaveBeenCalled();
  }));
});
