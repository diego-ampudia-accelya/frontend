import { Component, Input } from '@angular/core';

@Component({
  selector: 'bspl-locked-state',
  templateUrl: './locked-state.component.html',
  styleUrls: ['./locked-state.component.scss']
})
export class LockedStateComponent {
  @Input() message: string;
  @Input() secondaryMessage: string;
}
