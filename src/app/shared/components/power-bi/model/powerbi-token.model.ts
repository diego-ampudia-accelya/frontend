export interface PowerBITokenDto {
  url: string;
  token: string;
}
