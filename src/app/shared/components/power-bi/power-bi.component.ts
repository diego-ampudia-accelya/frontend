import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IReportEmbedConfiguration, models } from 'powerbi-client';
import { PowerBITokenDto } from './model/powerbi-token.model';

@Component({
  selector: 'bspl-power-bi',
  templateUrl: './power-bi.component.html',
  styleUrls: ['./power-bi.component.scss']
})
export class PowerBIComponent implements OnChanges {
  @Input() token: PowerBITokenDto;

  // CSS Class to be passed to the wrapper
  @Input() reportClass = 'report-container';

  // Flag which specify the type of embedding
  @Input() phasedEmbeddingFlag = false;

  /**
   * Documentation:
   *
   * https://github.com/microsoft/powerbi-client-angular
   */
  // Pass the basic embed configurations to the wrapper to bootstrap the report on first load
  // Values for properties like embedUrl, accessToken and settings will be set on click of button
  public reportConfig: IReportEmbedConfiguration = {
    type: 'report',
    embedUrl: undefined,
    tokenType: models.TokenType.Embed,
    accessToken: undefined,
    settings: undefined,
    id: ''
  };

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.token.currentValue) {
      this.updateReportConfig();
    }
  }

  private updateReportConfig(): void {
    this.reportConfig = {
      ...this.reportConfig,
      embedUrl: this.token.url,
      accessToken: this.token.token
    };
  }
}
