import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';

import { DialogConfig, FooterButton } from '..';
import { ReactiveSubject } from '../dialog/reactive-subject';

import { ChangeLanguageDialogComponent } from './change-language-dialog.component';
import { TranslatePipeMock } from '~app/test';
import { createAirlineUser } from '~app/shared/mocks/airline-user';

const translationServiceSpy = createSpyObject(L10nTranslationService);

describe('ChangeLanguageDialogComponent', () => {
  let component: ChangeLanguageDialogComponent;
  let fixture: ComponentFixture<ChangeLanguageDialogComponent>;

  const initialState = {
    auth: {
      user: createAirlineUser()
    },
    router: null,
    core: {
      menu: {
        tabs: {}
      },
      viewListsInfo: {}
    }
  };

  beforeEach(waitForAsync(() => {
    const mockConfig = {
      data: {
        title: 'test title',
        footerButtonsType: FooterButton.Apply,
        texts: {
          description: '',
          header: ''
        },
        hasCancelButton: true,
        isClosable: true,
        buttons: [
          {
            type: FooterButton.Cancel
          },
          {
            type: FooterButton.Apply
          }
        ]
      }
    } as DialogConfig;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ChangeLanguageDialogComponent, TranslatePipeMock],
      providers: [
        { provide: DialogConfig, useValue: mockConfig },
        ReactiveSubject,
        provideMockStore({ initialState }),
        {
          provide: L10nTranslationService,
          useValue: translationServiceSpy
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeLanguageDialogComponent);
    component = fixture.componentInstance;
    component.selectedLanguage = 'en';

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
