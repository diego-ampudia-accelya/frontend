import { Component, OnInit } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { DialogConfig } from '../dialog/dialog.config';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Component({
  selector: 'bspl-change-language-dialog.component',
  templateUrl: './change-language-dialog.component.html',
  styleUrls: ['./change-language-dialog.component.scss']
})
export class ChangeLanguageDialogComponent implements OnInit {
  public languagesDropdown: DropdownOption[];
  public selectedLanguage: string = this.config.data.selectedLanguage;

  constructor(public config: DialogConfig, public translationService: L10nTranslationService) {}

  ngOnInit() {
    this.languagesDropdown = appConfiguration.languages.map(language => ({
      value: language.locale,
      label: language.name
    }));
  }
}
