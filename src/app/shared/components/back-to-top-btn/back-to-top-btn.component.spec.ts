import { NO_ERRORS_SCHEMA } from '@angular/compiler';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BackToTopBtnComponent } from './back-to-top-btn.component';
import { TranslatePipeMock } from '~app/test';

describe('BackToTopBtnComponent', () => {
  let component: BackToTopBtnComponent;
  let fixture: ComponentFixture<BackToTopBtnComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BackToTopBtnComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackToTopBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call scrollTo the top of the page', () => {
    spyOn(window, 'scrollTo');

    component.backToTop();

    expect(window.scrollTo).toHaveBeenCalledWith({ top: 0, left: 0, behavior: 'smooth' });
  });
});
