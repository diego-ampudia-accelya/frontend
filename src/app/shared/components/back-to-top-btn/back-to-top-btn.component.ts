import { DOCUMENT } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'bspl-back-to-top-btn',
  templateUrl: './back-to-top-btn.component.html',
  styleUrls: ['./back-to-top-btn.component.scss']
})
export class BackToTopBtnComponent {
  public isVisible$ = fromEvent(window, 'scroll').pipe(map(() => !this.isScrolledToTop()));

  constructor(@Inject(DOCUMENT) private document: Document) {}

  public backToTop(): void {
    scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  }

  public isScrolledToTop(): boolean {
    const topOffset = 100;

    return this.document.body.scrollTop <= topOffset && this.document.documentElement.scrollTop <= topOffset;
  }
}
