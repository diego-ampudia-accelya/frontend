import { Component, HostBinding, Input } from '@angular/core';

import { PillInformationType } from '~app/shared/enums/pill-information-type';

@Component({
  selector: 'bspl-pill-information',
  templateUrl: './pill-information.component.html',
  styleUrls: ['./pill-information.component.scss']
})
export class PillInformationComponent {
  @Input() value: string;
  @Input() type = PillInformationType.Regular;
  @Input() tooltip = '';
  @Input() showIcon = true;

  public iconTypeMapper = {
    [PillInformationType.Error]: 'error_outline'
  };

  @HostBinding('class.regular') get isRegularPill() {
    return this.type && this.type === PillInformationType.Regular;
  }

  @HostBinding('class.error') get isErrorPill() {
    return this.type && this.type === PillInformationType.Error;
  }
}
