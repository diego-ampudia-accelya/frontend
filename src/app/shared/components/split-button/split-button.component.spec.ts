import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { L10nTranslationModule } from 'angular-l10n';

import { SplitButtonComponent } from './split-button.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { SharedModule } from '~app/shared/shared.module';

describe('SplitButtonComponent', () => {
  const defaultMock = { label: 'label1', command: () => {} };

  let component: SplitButtonComponent;
  let fixture: ComponentFixture<SplitButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig), HttpClientTestingModule, SharedModule, RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitButtonComponent);
    component = fixture.componentInstance;
    component.mainOption = defaultMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have vision filed', () => {
    expect(component.vision).not.toBeUndefined();
  });
});
