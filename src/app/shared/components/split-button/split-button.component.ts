import { Component, HostBinding, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { SplitButton } from 'primeng/splitbutton';

import { ButtonDesign } from '../buttons/enums/buttonDesign';

@Component({
  selector: 'bspl-split-button',
  templateUrl: './split-button.component.html',
  styleUrls: ['./split-button.component.scss']
})
export class SplitButtonComponent implements OnChanges {
  @ViewChild(SplitButton, { static: true }) splitButton: SplitButton;

  @HostBinding('class.is-active') get isActive() {
    return this.isOpened;
  }

  @Input() options: MenuItem[] = [];
  @Input() mainOption: MenuItem;
  @Input() icon;
  @Input() design: string = ButtonDesign.Primary;
  @Input() vision = '';

  public isOpened = false;

  private _isDisabled = false;

  private _defaultOption: MenuItem;

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.options) {
      this.initializeDefault();
    }
  }

  public get defaultOption(): MenuItem {
    //* MainOption indicated by Input has priority over the _defaultOption calculated
    return this.mainOption || this._defaultOption;
  }

  private initializeDefault() {
    this._defaultOption = null;

    if (!this.mainOption && this.options.length) {
      //* There was no mainOption indicated by Input so we calculate a default one from options Array
      this._defaultOption =
        this.options.find(option => typeof option.command !== 'undefined' && !option.disabled) || this.options[0];
    }
  }

  @Input()
  get isDisabled(): boolean {
    return (
      this._isDisabled ||
      !this.defaultOption ||
      (!this.defaultOption.command && !this.options.length) ||
      (this.defaultOption.disabled && this.options.length < 2) ||
      !this.options.find(opt => !opt.disabled)
    );
  }
  set isDisabled(isDisabled: boolean) {
    this._isDisabled = isDisabled;
  }
}
