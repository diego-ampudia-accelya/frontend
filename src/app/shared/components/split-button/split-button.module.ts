import { NgModule } from '@angular/core';
import { SplitButtonModule } from 'primeng/splitbutton';

import { SplitButtonComponent } from './split-button.component';

@NgModule({
  declarations: [SplitButtonComponent],
  imports: [SplitButtonModule],
  exports: [SplitButtonComponent]
})
export class SplitbuttonModule {}
