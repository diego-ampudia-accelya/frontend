import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DialogConfig } from '~app/shared/components';

@Component({
  selector: 'bspl-deactivate-account-dialog',
  templateUrl: './deactivate-account-dialog.component.html',
  styleUrls: ['./deactivate-account-dialog.component.scss']
})
export class DeactivateAccountDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public confirmCheckboxControl: FormControl;

  private $destroy = new Subject();

  constructor(private config: DialogConfig, private formBuilder: FormBuilder) {}

  public ngOnInit(): void {
    this.form = this.buildForm();
    this.checkboxValueChanges();
  }

  public ngOnDestroy(): void {
    this.$destroy.next();
  }

  private buildForm(): FormGroup {
    this.confirmCheckboxControl = this.formBuilder.control(false);

    return this.formBuilder.group({
      confirmCheckboxControl: this.confirmCheckboxControl
    });
  }

  private checkboxValueChanges(): void {
    this.confirmCheckboxControl.valueChanges.pipe(takeUntil(this.$destroy)).subscribe(value => {
      this.config.data.buttons[1].isDisabled = !value;
    });
  }
}
