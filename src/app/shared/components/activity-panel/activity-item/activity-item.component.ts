import { Component, EventEmitter, Input, Output } from '@angular/core';
import moment from 'moment-mini';

import { Activity } from '~app/shared/components/activity-panel/models/activity-panel.model';

@Component({
  selector: 'bspl-activity-item',
  templateUrl: './activity-item.component.html',
  styleUrls: ['./activity-item.component.scss']
})
export class ActivityItemComponent {
  @Input() activityItem: Activity;
  @Output() downloadFile = new EventEmitter<string>();

  public get isWorkflowItem() {
    return !!this.activityItem.status;
  }

  public get time() {
    return moment(this.activityItem.creationDate).format('HH:mm');
  }

  public getFileType(fileName: string): string {
    let fileType = '';

    if (fileName.includes('.')) {
      //* We get the extension and add a separator
      fileType = fileName.split('.').pop() + ' / ';
    }

    return fileType;
  }

  public onDownloadFile(resourceUrl: string) {
    if (resourceUrl) {
      this.downloadFile.emit(resourceUrl);
    }
  }
}
