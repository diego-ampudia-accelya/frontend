import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { orderBy } from 'lodash';

import { Activity } from '../models/activity-panel.model';

@Component({
  selector: 'bspl-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})
export class ActivityListComponent implements OnChanges {
  @Input() activities: Activity[];
  @Output() downloadFile = new EventEmitter<string>();

  private _activitiesGroupedByDate: Array<Activity[]>;
  public get activitiesGroupedByDate(): Array<Activity[]> {
    return this._activitiesGroupedByDate;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.activities) {
      this._activitiesGroupedByDate = this.groupActivitiesByDate(this.activities);
    }
  }

  public onDownloadFile(fileUrl: string) {
    this.downloadFile.emit(fileUrl);
  }

  private isSameDay(firstDate: Date, secondDate: Date) {
    return (
      firstDate.getFullYear() === secondDate.getFullYear() &&
      firstDate.getMonth() === secondDate.getMonth() &&
      firstDate.getDate() === secondDate.getDate()
    );
  }

  private areActivitiesValid(activities: Activity[]): boolean {
    //TODO Remove 'some' check when new BSPLink API is used all the time. It is used now to avoid errors
    return activities?.some(activity => !!activity.creationDate);
  }

  private groupActivitiesByDate(activities: Activity[]): Array<Activity[]> {
    let result: Array<Activity[]> = [];

    if (this.areActivitiesValid(activities)) {
      result = [[]];
      const orderKey: keyof Activity = 'creationDate';
      const orderedActivities = orderBy(activities, orderKey, 'desc');
      let currentDate = orderedActivities[0].creationDate;

      orderedActivities.forEach(activity => {
        if (this.isSameDay(activity.creationDate, currentDate)) {
          result[result.length - 1].push(activity);
        } else {
          result.push([activity]);
        }

        currentDate = activity.creationDate;
      });
    }

    return result;
  }
}
