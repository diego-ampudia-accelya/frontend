import { Activity } from './activity-panel.model';

export const activitiesMock: Activity[] = [
  {
    author: {
      contact: {
        name: 'a',
        telephone: '1536',
        email: 'user@email.com'
      },
      type: 'Airline',
      code: '220'
    },
    creationDate: new Date(),
    massloadFile: 'example_massloadFile.zip',
    reason: 'Example of reason for a workflow item',
    status: 'PENDING',
    type: 'Rejected Dispute',
    attachments: [
      {
        name: '500100_226408911_93_653239_04507_QGFR.err',
        size: 4121,
        date: new Date(),
        resourceUrl: null
      }
    ]
  },
  {
    author: {
      contact: {
        name: 'Usuario0',
        telephone: 'ttp://pepe',
        email: 'a@iata.org'
      },
      type: 'IATA',
      code: ''
    },
    creationDate: new Date(),
    status: 'APPROVED DISPUTE',
    type: 'Approved Dispute',
    attachments: []
  },
  {
    author: {
      contact: {
        name: 'CUSPIDE VIAJES',
        telephone: '961721643',
        email: 'user@email.com'
      },
      type: 'Agent',
      code: '7820002'
    },
    creationDate: new Date('2019/10/16'),
    status: 'DISPUTED',
    type: 'Disputed Document',
    attachments: [
      {
        name: '500100_257128712_A0_855874_21326_QFQM1.err',
        size: 20561,
        date: new Date(),
        resourceUrl: null
      },
      {
        name: '500100_120715095_51_415396_21797_QTJE.err',
        size: 3072,
        date: new Date(),
        resourceUrl: null
      }
    ]
  },
  {
    author: {
      contact: {
        name: 'CUSPIDE VIAJES',
        telephone: '961721643',
        email: 'user@email.com'
      },
      type: 'Agent',
      code: '7820002'
    },
    creationDate: new Date('2019/10/16'),
    type: 'Comment',
    reason: 'Agent second comment\n',
    attachments: [
      {
        name: '1B471D135I144BGF432AH1569846709049#0930_56452_38#9D_278371_47795_NKPV1.err',
        size: 8623,
        date: new Date(),
        resourceUrl: null
      }
    ]
  },
  {
    author: {
      contact: {
        name: 'Usuario0',
        telephone: 'ttp://pepe',
        email: 'a@iata.org'
      },
      type: 'IATA',
      code: ''
    },
    creationDate: new Date('2019/10/17'),
    type: 'Comment',
    reason: 'Agent First Comment:\n',
    attachments: [
      {
        name: '1B471D135I144BGF432AH1569846709049#0930_52825_07#1D_027439_49575_NVUS1.err',
        size: 8614,
        date: new Date(),
        resourceUrl: null
      }
    ]
  },
  {
    author: {
      contact: {
        name: 'a',
        telephone: '1536',
        email: 'user@email.com'
      },
      type: 'Airline',
      code: '220'
    },
    creationDate: new Date('2019/10/16'),
    type: 'Files attached to the document',
    attachments: [
      {
        name: 'ESvv2203_file.txt',
        size: 8,
        date: new Date(),
        resourceUrl: null
      },
      {
        name: 'file_vss.txt',
        size: 1311,
        date: new Date(),
        resourceUrl: null
      }
    ]
  },
  {
    author: {
      contact: {
        name: 'a',
        telephone: '1536',
        email: 'user@email.com'
      },
      type: 'Airline',
      code: '220'
    },
    creationDate: new Date('2019/10/14'),
    type: 'Comment',
    reason: 'This is an after issue comment.\n',
    attachments: [
      {
        name: 'file.txt',
        size: 12,
        date: new Date(),
        resourceUrl: null
      },
      {
        name: 'file - copy.txt',
        size: 12,
        date: new Date(),
        resourceUrl: null
      }
    ]
  },
  {
    author: {
      contact: {
        name: 'a',
        telephone: '1536',
        email: 'user@email.com'
      },
      type: 'Airline',
      code: '220'
    },
    creationDate: new Date('2019/10/13'),
    type: 'Comment',
    reason: 'This is a comment on issuance.\n',
    attachments: []
  },
  {
    author: {
      contact: {
        name: 'a',
        telephone: '1536',
        email: 'user@email.com'
      },
      type: 'Airline',
      code: '220'
    },
    creationDate: new Date(),
    status: 'PENDING',
    type: 'Document Issued',
    attachments: [
      {
        name: 'document_summary_2.json',
        size: 785,
        date: new Date(),
        resourceUrl: null
      }
    ]
  }
];
