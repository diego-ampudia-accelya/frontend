export interface Activity {
  attachments: ActivityAttachment[];
  author: {
    contact: ActivityContact;
    type: string;
    code: string;
  };
  creationDate: Date;
  massloadFile?: string;
  params: Array<{ name: string; value: string }>;
  reason?: string;
  status?: string;
  type: string;
}

export interface ActivityContact {
  name: string;
  telephone: string;
  email: string;
}

export interface ActivityAttachment {
  name: string;
  size: number;
  date: Date;
  resourceUrl: string;
}

export enum ActivityFilterType {
  All = 'ALL',
  Workflow = 'WORKFLOW_EVENTS',
  Comments = 'COMMENTS',
  Attachments = 'ATTACHMENTS'
}
