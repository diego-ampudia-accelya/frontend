import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';

import { Activity, ActivityFilterType } from './models/activity-panel.model';

@Component({
  selector: 'bspl-activity-panel',
  templateUrl: './activity-panel.component.html'
})
export class ActivityPanelComponent implements OnChanges {
  @Input() isCommentingVisible = true;
  @Input() isCommentingDisabled = false;
  @Input() commentAdded$: Subject<any>;
  @Input() fileUploadPath: string;

  @Input() activities: Activity[] = [];

  @Output() downloadFile = new EventEmitter<string>();
  @Output() sendComment = new EventEmitter<{ reason: string; attachmentIds?: string[] }>();

  public filteredActivities: Activity[] = [];

  private currentFilter: ActivityFilterType;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.activities && this.currentFilter) {
      this.onNewActivities();
    }
  }

  public onFilterChange(filter: ActivityFilterType) {
    this.currentFilter = filter;
    this.filterActivitiesWithCurrentFilter();
  }

  public onCommentClick(event: { reason: string; attachmentIds?: string[] }) {
    this.sendComment.emit(event);
  }

  public onDownloadFile(fileUrl: string) {
    this.downloadFile.emit(fileUrl);
  }

  private onNewActivities() {
    this.filterActivitiesWithCurrentFilter();
  }

  private filterActivitiesWithCurrentFilter() {
    let result: Activity[] = [];

    switch (ActivityFilterType[this.currentFilter]) {
      case ActivityFilterType.Attachments:
        result = this.activities.filter(activity => activity.attachments && activity.attachments.length);
        break;
      case ActivityFilterType.Comments:
        result = this.activities.filter(activity => activity.type === 'Comment');
        break;
      case ActivityFilterType.Workflow:
        result = this.activities.filter(activity => !!activity.status);
        break;
      default:
        result = this.activities;
    }

    this.filteredActivities = result;
  }
}
