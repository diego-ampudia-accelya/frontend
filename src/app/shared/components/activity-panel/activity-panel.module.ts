import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';

import { ActivityHeaderComponent } from './activity-header/activity-header.component';
import { ActivityItemComponent } from './activity-item/activity-item.component';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { ActivityPanelComponent } from './activity-panel.component';
import { SharedModule } from '~app/shared/shared.module';
import { ActivityCommentComponent } from '~app/shared/components/activity-panel/activity-comment/activity-comment.component';

@NgModule({
  declarations: [
    ActivityPanelComponent,
    ActivityListComponent,
    ActivityHeaderComponent,
    ActivityCommentComponent,
    ActivityItemComponent
  ],
  imports: [CommonModule, SharedModule, L10nTranslationModule],
  exports: [ActivityPanelComponent]
})
export class ActivityPanelModule {}
