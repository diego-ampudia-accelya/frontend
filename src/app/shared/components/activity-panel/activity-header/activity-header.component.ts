import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ActivityFilterType } from '../models/activity-panel.model';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';

@Component({
  selector: 'bspl-activity-header',
  templateUrl: './activity-header.component.html',
  styleUrls: ['./activity-header.component.scss']
})
export class ActivityHeaderComponent implements OnInit {
  @Output() filterSelect = new EventEmitter<any>();

  public btnLabel = '';

  public filterOptions: ButtonMenuOption[] = [];

  constructor(private translationService: L10nTranslationService) {}

  public ngOnInit() {
    this.initializeFilterOptions();
    this.onFilterButtonSelect(
      this.filterOptions.find(option => ActivityFilterType[option.id] === ActivityFilterType.All)
    );
  }

  public onFilterButtonSelect(event: ButtonMenuOption) {
    this.filterSelect.emit(event.id);
    this.btnLabel = event.label;
  }

  private initializeFilterOptions() {
    this.filterOptions = Object.keys(ActivityFilterType).map(filterType => ({
      id: filterType,
      label: this.translate(ActivityFilterType[filterType])
    }));
  }

  private translate(key: string): string {
    return this.translationService.translate(`activityPanel.buttonLabels.${key}`);
  }
}
