import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { L10nTranslationModule } from 'angular-l10n';

import { ActivityCommentComponent } from './activity-comment.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { SharedModule } from '~app/shared/shared.module';

describe('ActivityCommentComponent', () => {
  let component: ActivityCommentComponent;
  let fixture: ComponentFixture<ActivityCommentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityCommentComponent],
      imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        L10nTranslationModule.forRoot(l10nConfig)
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
