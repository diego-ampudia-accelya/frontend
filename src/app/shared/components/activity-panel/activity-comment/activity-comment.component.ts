import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { UploadModes } from '~app/shared/components/upload/upload-mode.enum';
import { UploadComponent } from '~app/shared/components/upload/upload.component';

@Component({
  selector: 'bspl-activity-comment',
  templateUrl: './activity-comment.component.html',
  styleUrls: ['./activity-comment.component.scss']
})
export class ActivityCommentComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild(UploadComponent, { static: true }) uploadComponent: UploadComponent;

  @Input() disabled = false;
  @Input() fileUploadPath: string;
  @Input() commentAdded$: Subject<any>;

  @Output() sendComment = new EventEmitter<{ reason: string; attachmentIds?: string[] }>();

  public form: FormGroup;
  public textAreaIsFocused = false;
  public uploadMode = UploadModes.Preview;

  public get isSendBtnDisabled(): boolean {
    const formIsEmpty = !(this.form.value.reason || this.uploadComponent.hasFilesToUpload);

    return this.disabled || this.uploadIsInProgress || formIsEmpty;
  }

  public get active(): boolean {
    return this.textAreaIsFocused || this.form.value.reason || this.uploadComponent.hasFiles;
  }

  private uploadIsInProgress = false;

  private destroy$ = new Subject();

  constructor(private formBuilder: FormBuilder) {}

  public ngOnInit() {
    this.form = this.formBuilder.group({
      reason: [''],
      fileIds: [null]
    });
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.commentAdded$ && this.commentAdded$) {
      this.initializeCommentAddedListener();
    }
  }

  public onTextareaFocus() {
    this.textAreaIsFocused = true;
  }

  public onTextareaBlur() {
    this.textAreaIsFocused = false;
  }

  public onUploadIsInProgress(value: boolean) {
    this.uploadIsInProgress = value;
  }

  public onCancel() {
    this.resetForm();
  }

  public onSendFiles() {
    if (this.uploadComponent.hasFilesInPreviewMode) {
      this.uploadFiles();
    } else {
      this.sendWithFileIds();
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeCommentAddedListener() {
    this.commentAdded$.pipe(takeUntil(this.destroy$)).subscribe(() => this.resetForm());
  }

  private uploadFiles() {
    this.uploadComponent
      .uploadFiles()
      .pipe(takeUntil(this.destroy$))
      .subscribe(allValidFilesAreUploaded => allValidFilesAreUploaded && this.sendWithFileIds());
  }

  private sendWithFileIds() {
    const reason = this.form.value.reason;
    const files = this.form.value.fileIds;

    if (reason || files.length) {
      this.sendComment.emit({ reason, attachmentIds: files });
    }
  }

  private resetForm() {
    this.form.setValue({ reason: '', fileIds: null });
    this.uploadComponent.reset();
    this.onTextareaBlur();
    this.onUploadIsInProgress(false);
  }
}
