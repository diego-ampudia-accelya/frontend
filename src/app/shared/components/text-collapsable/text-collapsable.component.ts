import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

@Component({
  selector: 'bspl-text-collapsable',
  templateUrl: './text-collapsable.component.html',
  styleUrls: ['./text-collapsable.component.scss']
})
export class TextCollapsableComponent implements AfterViewInit {
  @Input() data: { title: string; subtitle: string; content: string } = { title: '', subtitle: '', content: '' };

  @ViewChild('collapsableContent') collapsableContent: ElementRef<HTMLElement>;

  public open = false;
  public showLink = false;

  constructor(public translationService: L10nTranslationService, private cd: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    const content = this.collapsableContent.nativeElement;
    this.showLink = content.offsetWidth < content.scrollWidth;
    this.cd.detectChanges();
  }

  public onClick() {
    this.open = !this.open;
  }
}
