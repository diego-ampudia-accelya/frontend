import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';

import { TextCollapsableComponent } from './text-collapsable.component';

@NgModule({
  declarations: [TextCollapsableComponent],
  imports: [CommonModule, L10nTranslationModule],
  exports: [TextCollapsableComponent]
})
export class TextCollapsableModule {}
