/* eslint-disable @typescript-eslint/ban-types */
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { CheckboxDesign } from '../buttons/enums/checkboxDesign';
import { AutoFocusableControl } from '~app/shared/base/auto-focusable-control';

@Component({
  selector: 'bspl-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})
export class CheckboxComponent extends AutoFocusableControl implements ControlValueAccessor, AfterViewInit {
  @Input() label: string;
  @Input() checked: boolean;
  @Input() disabled: boolean;
  @Input() // TODO original input disabled must change its name to avoid Angular warnings
  set isDisabled(disabled: boolean) {
    this.disabled = disabled;
  }
  @Input() design: string;

  @Output() stateChange = new EventEmitter<boolean>();

  @ViewChild('checkboxInput') checkboxInput: ElementRef<HTMLInputElement>;

  private defaultDesign = {
    [CheckboxDesign.Normal]: true
  };

  private onChange: Function;

  constructor() {
    super();

    this.onChange = (_: any) => {};
  }

  public propagateTouched: Function = () => {
    //This is intentional
  };

  public ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  public focusElement(): void {
    if (this.disabled) {
      return;
    }

    this.checkboxInput.nativeElement.focus();
  }

  public onCheckboxClick(event: any): void {
    event.cancelBubble = true;
    event.stopPropagation();
    this.checked = event.target.checked;
    this.onChange(this.checked);

    this.stateChange.emit(this.checked);
  }

  public getCurrentCheckboxDesign() {
    if (this.design) {
      return { [this.design]: true };
    }

    return this.defaultDesign;
  }

  public writeValue(value: any): void {
    this.checked = value;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: Function) {
    this.propagateTouched = fn;
  }
}
