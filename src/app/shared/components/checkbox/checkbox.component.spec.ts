import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CheckboxComponent } from './checkbox.component';

describe('CheckboxComponent', () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CheckboxComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call focus() on ngAfterViewInit() if autofocus is `true`', () => {
    component.autofocus = true;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).toHaveBeenCalled();
  });

  it('should NOT call focus() on ngAfterViewInit() if autofocus is `false`', () => {
    component.autofocus = false;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).not.toHaveBeenCalled();
  });
});
