/* eslint-disable @angular-eslint/no-output-native */
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NgControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';

@Component({
  selector: 'bspl-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AutocompleteComponent),
      multi: true
    }
  ]
})
export class AutocompleteComponent implements AfterViewInit, ControlValueAccessor, OnDestroy, OnInit {
  @Input() label = '';
  @Input() items;
  @Input() bindValue;
  @Input() bindLabel;
  @Input() loading;
  @Input() ngControl: NgControl;
  @Input() errorMessage;
  @Input() validate = false;
  @Input() debounceTime = 200;
  @Input() searchStringMinLength = 2;
  @Input() virtualScroll = true;

  @Output() typeahead = new EventEmitter<string>();
  @Output() scrollToEnd = new EventEmitter();
  @Output() blur = new EventEmitter();
  @Output() change = new EventEmitter();
  @Output() focus = new EventEmitter();

  @ViewChild(NgSelectComponent, { static: true }) autocomplete: NgSelectComponent;

  public typeahead$ = new Subject<string>();

  public errors: any;
  private subscriptions: Subscription[] = [];
  // eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
  private _errorFieldMessage;

  get errorFieldMessage() {
    return this._errorFieldMessage;
  }

  constructor(
    private injector: Injector,
    private cd: ChangeDetectorRef,
    private el: ElementRef,
    private renderer: Renderer2
  ) {}

  public ngOnInit() {
    this.initTypeahead();
  }

  public visibleErrorContainer(): boolean {
    return (
      (this.ngControl && this.ngControl.pristine && this.ngControl.invalid) ||
      (this.ngControl && this.ngControl.dirty && this.ngControl.invalid)
    );
  }

  public initTypeahead() {
    this.typeahead$
      .pipe(
        filter(str => str && str.length > this.searchStringMinLength),
        debounceTime(this.debounceTime),
        distinctUntilChanged()
      )
      .subscribe(res => this.typeahead.emit(res));
  }

  public writeValue(value) {
    if (this.autocomplete) {
      this.autocomplete.writeValue(value);
    }
  }

  public registerOnChange(fn) {
    if (this.autocomplete) {
      this.autocomplete.registerOnChange(fn);
    }
  }

  public registerOnTouched(fn) {
    if (this.autocomplete) {
      this.autocomplete.registerOnTouched(fn);
    }
  }

  public autocompleteBlurHandler(event): void {
    this.setValidationMessages();
    this.blur.emit(event);
  }

  public autocompleteChangeHandler(event): void {
    this.setValidationMessages();
    this.change.emit(event);
  }

  public ngAfterViewInit(): void {
    if (!this.ngControl) {
      // TODO: Fix ts lint error about get is deprecated: from v4.0.0 use Type<T>. Apply it everywhere where needed.
      // eslint-disable-next-line
      this.ngControl = this.injector.get(NgControl, null); //NOSONAR
      this.cd.detectChanges();
    }

    if (this.validate) {
      this.listenStatusChanges();
    }
  }

  public setValidationMessages() {
    if (this.validate && this.ngControl) {
      this._errorFieldMessage = this.errorMessage;
      this.errors = this.ngControl.errors;
      const autocomplete = this.el.nativeElement.querySelector('.ng-select');

      if (this.visibleErrorContainer()) {
        this.renderer.addClass(autocomplete, 'error-state-border');
      } else {
        this.renderer.removeClass(autocomplete, 'error-state-border');
      }

      this.cd.detectChanges();
    }
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private listenStatusChanges() {
    if (this.ngControl) {
      //* We want to show the state if the control is dirty or is now valid (could have been resetted from an invalid state and now it's pristine and valid)
      this.subscriptions.push(
        this.ngControl.statusChanges
          .pipe(filter(() => this.ngControl.touched || this.ngControl.valid))
          .subscribe(() => this.setValidationMessages())
      );
    }
  }
}
