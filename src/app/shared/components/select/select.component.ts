/* eslint-disable @angular-eslint/no-output-native */
import { DOCUMENT } from '@angular/common';
import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Self,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { DropdownPosition, NgSelectComponent } from '@ng-select/ng-select';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { isEmpty, isEqual } from 'lodash';
import { fromEvent, Observable, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { CustomControl } from '~app/shared/base/custom-control';
import { SelectMode } from '~app/shared/enums';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { IcuMessageFormat } from '~app/shared/models/icu-message-format.model';

@Component({
  selector: 'bspl-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectComponent
  extends CustomControl
  implements OnInit, OnChanges, AfterViewInit, AfterViewChecked, OnDestroy
{
  @ViewChild('select', { static: true }) select: NgSelectComponent;

  @HostBinding('class.control--open') get isSelectOpened(): boolean {
    return !!this.select && !!this.select.isOpen;
  }

  @HostBinding('class.select-heading') get isHeadingMode(): boolean {
    return this.hasMode(SelectMode.Heading);
  }

  @HostBinding('class.select-subheading') get isSubHeadingMode(): boolean {
    return this.hasMode(SelectMode.SubHeading);
  }

  @HostBinding('class.select-small') get isSmallMode(): boolean {
    return this.hasMode(SelectMode.Small);
  }

  @HostBinding('class.select-any-option') get isAnyOptionVisible(): boolean {
    return this.options?.length && !!this.anyOption;
  }

  @Output() open = new EventEmitter<Event>();
  @Output() close = new EventEmitter<Event>();
  @Output() blur = new EventEmitter<Event>();
  @Output() change = new EventEmitter<DropdownOption>();

  @Input() options: DropdownOption[] = [];
  @Input() anyOption: DropdownOption;
  @Input() dropdownPosition: DropdownPosition = 'auto';

  @Input() clearable = true;
  @Input() multiple = false;
  @Input() multiplePlaceholder: IcuMessageFormat = {
    other: 'multipleSelect.multipleItemsSelected'
  };
  @Input() autoDisplayFirst = false;
  @Input() modes: SelectMode[];
  @Input() virtualScroll = false;
  @HostBinding('class.select-append-body')
  @Input()
  appendToBody = false;

  private window: Window;
  private destroyed$ = new Subject();

  public searchable = false;
  public tooltipText: string;

  // Needs to be an arrow function in order to preserve `this` context when passed to NgSelect.
  public keyDownFn = (event: KeyboardEvent): boolean => {
    if (this.select.isOpen) {
      if (event.key === 'Enter' || event.key === 'Escape') {
        event.stopPropagation();
      }
    }

    return true;
  };

  @HostListener('window:resize')
  private closeSelect() {
    if (this.select) {
      this.select.close();
    }
  }

  //* Checks first equality and then compares by id if values are objects
  public compareWith = (opt: DropdownOption, selected: any) =>
    isEqual(opt.value, selected) ||
    (opt.value &&
      selected &&
      typeof opt.value === 'object' &&
      typeof selected === 'object' &&
      opt.value.id &&
      selected.id &&
      opt.value.id === selected.id);

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    @Self() @Optional() public ngControl: NgControl,
    @Inject(DOCUMENT) document: Document,
    cd: ChangeDetectorRef,
    translationService: L10nTranslationService
  ) {
    super(ngControl, cd, translationService);

    this.closeSelect = this.closeSelect.bind(this);
    this.window = document.defaultView;
  }

  public ngOnInit(): void {
    if (this.appendToBody) {
      //* For some reason, using select's appendTo Input in template would break the tooltips
      this.select.appendTo = 'body';
    }

    if (isEmpty(this.placeholder)) {
      this.placeholder = this.translationService.translate('select.choose');
    }

    this.applyWorkaroundForHandleWriteValue();

    if (this.anyOption && this.multiple) {
      this.initializeAnyOptionClickListener();
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.options && this.options?.length && this.anyOption && !this.options.includes(this.anyOption)) {
      this.options = [this.anyOption, ...this.options];
      if (this.value) {
        this.updateAnyOptionOnChange(this.value);
      }
    }
    if (changes.options) {
      this.searchable = this.options?.length > 7;
    }
  }

  public ngAfterViewInit(): void {
    this.enableCloseOnScroll().subscribe();

    super.ngAfterViewInit();
  }

  public ngAfterViewChecked(): void {
    // Regenerate the tooltip text after the selectedItems in ng-select are updated.
    // We need to do it here to avoid an `Expression has changed after it was checked.` error.
    const newTooltipText = this.getTooltipText();
    if (this.tooltipText !== newTooltipText) {
      this.tooltipText = newTooltipText;
      this.cd.detectChanges();
    }
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  public get isDropdownDisabled(): boolean {
    return this.isDisabled || this.isReadOnly || this.locked;
  }

  public getPlaceholder(): string {
    const hasFirst = this.autoDisplayFirst && this.options.length > 0;
    const useFirst = !this.placeholder && hasFirst;

    return useFirst ? this.getOptionLabel(this.options[0]) : this.placeholder;
  }

  public getOptionLabel(option: DropdownOption): string {
    return option.labelSelected || option.label;
  }

  public onSearchableInputKeyDown(event: KeyboardEvent) {
    if (this.appendToBody) {
      this.select.handleKeyDown(event);
    }
  }

  public focusElement() {
    if (this.isEnabled) {
      this.select.focus();
    }
  }

  public onBlur() {
    this.blur.emit();
    this.propagateTouched();
  }

  public onClose(event: Event): void {
    // The component is losing the focus in case of `searchable` so we need to set it.
    if (this.searchable) {
      this.focusElement();
    }

    this.close.emit(event);
  }

  public onSearch(): void {
    // To display the first matching ordered results
    this.scrollDropdownToTop();
  }

  private getTooltipText(): string {
    let tooltipText = '';
    const selectedOptions = this.select.selectedItems;

    if (!this.isSelectOpened && selectedOptions?.length > 1) {
      tooltipText = selectedOptions.map((item: any) => item.value.label).join('<br/>');
    }

    return tooltipText;
  }

  private hasMode(mode: SelectMode): boolean {
    return this.modes && this.modes.includes(mode);
  }

  /**
   * Close the dropdown whenever a scroll event is detected.
   * Ignore scroll events originating from the dropdown itself.
   *
   * ng-select is not properly repositioned when appended to body and placed in a scrollable container.
   */
  private enableCloseOnScroll(): Observable<any> {
    // Add a capturing event listener to close the select when an ancestor is scrolled.
    // Neither @HostListener nor Renderer2 supports setting the `capture` option for event listeners,
    // so fallback to the raw browser api.
    return fromEvent(this.window, 'scroll', { capture: true }).pipe(
      filter($event => {
        const { dropdownPanel } = this.select;
        const scrollElement = dropdownPanel && dropdownPanel.scrollElementRef.nativeElement;
        const originatesFromDropdown = scrollElement === $event.target;

        return !originatesFromDropdown;
      }),
      tap(() => this.select.close()),
      takeUntil(this.destroyed$)
    );
  }

  private scrollDropdownToTop() {
    const { dropdownPanel } = this.select;

    if (dropdownPanel) {
      dropdownPanel.scrollElementRef.nativeElement.scrollTop = 0;
    }
  }

  private applyWorkaroundForHandleWriteValue(): void {
    // Applying selected value before the options are loaded breaks the ng-select.
    // The original ng-select code for this method does not map the selected items correctly
    // to the internal representation.
    // For simplicity we omit the handling of primitives (numbers, strings, etc.) as items, since we are always wrapping them in a DropdownOption.
    // For more info see the original code https://github.com/ng-select/ng-select/blob/0a8cde774130663c64b08f550e685add07d55679/src/ng-select/lib/ng-select.component.ts#L704

    (this.select as any)._handleWriteValue = function (ngModel: any | any[]) {
      if (!this._isValidWriteValue(ngModel)) {
        return;
      }

      const select = (val: any) => {
        let item = this.itemsList.findItem(val);
        if (item) {
          this.itemsList.select(item);
        } else {
          item = {
            [this.bindLabel]: null,
            [this.bindValue]: val
          };
          this.itemsList.select(this.itemsList.mapItem(item, null));
        }
      };

      if (this.multiple) {
        ngModel.forEach(item => select(item));
      } else {
        select(ngModel);
      }
    };
  }

  private initializeAnyOptionClickListener() {
    this.ngControl.valueChanges
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => this.updateAnyOptionOnChange(value));
  }

  private updateAnyOptionOnChange(value: DropdownOption<any>[]): void {
    const selectedValues = value || [];
    const optionAnyIsSelected = selectedValues.findIndex(selectedValue => selectedValue === this.anyOption.value) >= 0;

    if (optionAnyIsSelected) {
      //* Disabling all options since 'any' is selected
      this.options = this.options.map(option => ({
        ...option,
        disabled: option.value !== this.anyOption.value
      }));

      //* We make sure only 'any' option is selected if other values were selected as well
      if (selectedValues.length > 1) {
        this._value = [this.anyOption.value];
      }
    } else {
      //* AnyOption is disabled if another option is selected. Otherwise all options are enabled
      this.options =
        this.options?.map(option => ({
          ...option,
          disabled: option.value === this.anyOption.value && selectedValues.length > 0
        })) || [];
    }
  }
}
