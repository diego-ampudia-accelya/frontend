import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationModule } from 'angular-l10n';

import { l10nConfig } from '~app/shared/base/conf/l10n.config';
import { SelectComponent } from '~app/shared/components/select/select.component';
import { SharedModule } from '~app/shared/shared.module';

describe('SelectComponent', () => {
  let component: SelectComponent;
  let fixture: ComponentFixture<SelectComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, SharedModule, HttpClientTestingModule, L10nTranslationModule.forRoot(l10nConfig)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call focus() on ngAfterViewInit() if autofocus is `true`', () => {
    component.autofocus = true;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).toHaveBeenCalled();
  });

  it('should NOT call focus() on ngAfterViewInit() if autofocus is `false`', () => {
    component.autofocus = false;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).not.toHaveBeenCalled();
  });

  describe('keyDownFn', () => {
    let keyDownEvent: KeyboardEvent;

    beforeEach(() => {
      keyDownEvent = new KeyboardEvent('keydown');
    });

    it('should always return true', () => {
      const actual = component.keyDownFn(keyDownEvent);

      expect(actual).toBe(true);
    });

    it('should return true when ngSelect is opened', () => {
      spyOn(component.select, 'isOpen').and.returnValue(true);
      const actual = component.keyDownFn(keyDownEvent);

      expect(actual).toBe(true);
    });

    it('should stop event propagation when panel is opened and Enter is pressed', () => {
      spyOnProperty(keyDownEvent, 'key').and.returnValue('Enter');
      spyOn(keyDownEvent, 'stopPropagation');
      component.select.isOpen = true;

      component.keyDownFn(keyDownEvent);

      expect(keyDownEvent.stopPropagation).toHaveBeenCalled();
    });

    it('should NOT stop event propagation when panel is closed and Enter is pressed', () => {
      spyOnProperty(keyDownEvent, 'key').and.returnValue('Enter');
      spyOn(keyDownEvent, 'stopPropagation');
      component.select.isOpen = false;

      component.keyDownFn(keyDownEvent);

      expect(keyDownEvent.stopPropagation).not.toHaveBeenCalled();
    });

    it('should stop event propagation when panel is opened and Escape is pressed', () => {
      spyOnProperty(keyDownEvent, 'key').and.returnValue('Escape');
      spyOn(keyDownEvent, 'stopPropagation');
      component.select.isOpen = true;

      component.keyDownFn(keyDownEvent);

      expect(keyDownEvent.stopPropagation).toHaveBeenCalled();
    });

    it('should NOT stop event propagation when panel is closed and Escape is pressed', () => {
      spyOnProperty(keyDownEvent, 'key').and.returnValue('Escape');
      spyOn(keyDownEvent, 'stopPropagation');
      component.select.isOpen = false;

      component.keyDownFn(keyDownEvent);

      expect(keyDownEvent.stopPropagation).not.toHaveBeenCalled();
    });
  });

  it('onSearchableInputKeyDown should call NgSelectComponent keydownHandler, when appendToBody true', () => {
    const keyEvent = new KeyboardEvent('keydown', { code: 'Digit0' });
    component.appendToBody = true;
    spyOn(component.select, 'handleKeyDown').and.callThrough();
    component.onSearchableInputKeyDown(keyEvent);
    expect(component.select.handleKeyDown).toHaveBeenCalledWith(keyEvent);
  });

  it('onSearchableInputKeyDown should not call NgSelectComponent keydownHandler, when appendToBody is false', () => {
    component.appendToBody = false;
    spyOn(component.select, 'handleKeyDown').and.callThrough();
    component.onSearchableInputKeyDown(new KeyboardEvent('keydown', { code: 'Digit0' }));
    expect(component.select.handleKeyDown).not.toHaveBeenCalled();
  });

  describe('onClose()', () => {
    it('should emit close event', () => {
      spyOn(component.close, 'emit');

      component.onClose(null);

      expect(component.close.emit).toHaveBeenCalledWith(null);
    });

    it('should focus the component in case of `searchable` is set to true', () => {
      component.searchable = true;
      spyOn(component, 'focusElement');

      component.onClose(null);

      expect(component.focusElement).toHaveBeenCalled();
    });

    it('should NOT focus the component in case of `searchable` is set to false', () => {
      component.searchable = false;
      spyOn(component, 'focusElement');

      component.onClose(null);

      expect(component.focusElement).not.toHaveBeenCalled();
    });
  });

  describe('show search if items more than 7', () => {
    it('should not show search when options is empty after onChanges', () => {
      component.ngOnChanges({
        options: {
          currentValue: [],
          previousValue: null,
          firstChange: false,
          isFirstChange: () => null
        }
      });
      expect(component.searchable).toBe(false);
    });

    it('should not show search when options is less than 7 after on changes', () => {
      component.options = [];
      component.anyOption = { value: 'any', label: 'ANY' };
      component.searchable = true;
      component.ngOnChanges({
        options: {
          currentValue: [
            { value: 'a', label: 'A' },
            { value: 'b', label: 'B' },
            { value: 'c', label: 'C' },
            { value: 'd', label: 'D' },
            { value: 'e', label: 'E' }
          ],
          previousValue: null,
          firstChange: false,
          isFirstChange: () => null
        }
      });
      expect(component.searchable).toBe(false);
    });

    it('should show search when options is more than 7 after on changes', () => {
      component.options = [{ value: 'a', label: 'A' }];
      component.anyOption = { value: 'any', label: 'ANY' };
      component.searchable = true;
      component.options = [
        { value: 'a', label: 'A' },
        { value: 'b', label: 'B' },
        { value: 'c', label: 'C' },
        { value: 'd', label: 'D' },
        { value: 'e', label: 'E' },
        { value: 'f', label: 'F' },
        { value: 'g', label: 'G' },
        { value: 'h', label: 'H' }
      ];
      component.ngOnChanges({
        options: {
          currentValue: [
            { value: 'a', label: 'A' },
            { value: 'b', label: 'B' },
            { value: 'c', label: 'C' },
            { value: 'd', label: 'D' },
            { value: 'e', label: 'E' },
            { value: 'f', label: 'F' },
            { value: 'g', label: 'G' },
            { value: 'h', label: 'H' }
          ],
          previousValue: null,
          firstChange: false,
          isFirstChange: () => null
        }
      });
      expect(component.searchable).toBe(true);
    });
  });
});
