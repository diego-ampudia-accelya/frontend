import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { L10nLocale, L10N_LOCALE } from 'angular-l10n';

@Component({
  selector: 'bspl-table-selection-toolbar',
  templateUrl: './table-selection-toolbar.component.html',
  styleUrls: ['./table-selection-toolbar.component.scss']
})
export class TableSelectionToolbarComponent {
  @Input() public selectedRowsCount: number;

  @Input() public singleSelectedItemTranslationKey = 'tableSelectionToolbar.singleItemSelected';

  @Input() public multipleSelectedItemsTranslationKey = 'tableSelectionToolbar.multipleItemsSelected';

  @Output() public clearSelectedRows = new EventEmitter();

  constructor(@Inject(L10N_LOCALE) public locale: L10nLocale) {}

  public onClearSelectedRowsClick() {
    this.clearSelectedRows.emit();
  }
}
