import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10N_LOCALE } from 'angular-l10n';

import { TableSelectionToolbarComponent } from './table-selection-toolbar.component';
import { TranslatePipeMock } from '~app/test';

describe('TableSelectionToolbarComponent', () => {
  let component: TableSelectionToolbarComponent;
  let fixture: ComponentFixture<TableSelectionToolbarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TableSelectionToolbarComponent, TranslatePipeMock],
      providers: [{ provide: L10N_LOCALE, useValue: { language: 'en' } }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSelectionToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onClearSelectedRowsClick should emit clearSelectedRows', () => {
    spyOn(component.clearSelectedRows, 'emit').and.callThrough();

    component.onClearSelectedRowsClick();

    expect(component.clearSelectedRows.emit).toHaveBeenCalled();
  });
});
