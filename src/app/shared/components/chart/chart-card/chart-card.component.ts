import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';

import { ChartCardSection } from '../models/chart-card.model';

@Component({
  selector: 'bspl-chart-card',
  templateUrl: './chart-card.component.html',
  styleUrls: ['./chart-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartCardComponent implements OnChanges {
  @Output() seeAllClick = new EventEmitter<ChartCardSection>();
  @Output() downloadClick = new EventEmitter<ChartCardSection>();

  @Input() sections: ChartCardSection[];

  @Input() title: string;
  @Input() sectionsTitle: string;
  @Input() paramTitle: string;
  @Input() paramValue: string;

  @Input() showSeeAll = true;
  @Input() showDownload = true;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.sections && this.sections?.length) {
      // We active first option by default if none are currently active
      this.markDefaultActiveSection();
    }
  }

  public onSectionClick(sectionIndex: number): void {
    if (this.sections.length > 1) {
      const sectionClicked = this.sections[sectionIndex];

      this.sections.forEach((opt, index) => (opt.active = index === sectionIndex));
      sectionClicked.clickEmitter?.emit(sectionClicked);
    }
  }

  public onSeeAllClick(): void {
    this.seeAllClick.emit(this.getActiveSection());
  }

  public onDownloadClick(): void {
    this.downloadClick.emit(this.getActiveSection());
  }

  private getActiveSection(): ChartCardSection {
    return this.sections.find(option => option.active);
  }

  private markDefaultActiveSection(): void {
    this.sections[0].active = this.sections[0].active || this.sections.every(section => !section.active);
  }
}
