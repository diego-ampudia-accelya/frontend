import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockPipe } from 'ng-mocks';

import { BarChartData } from '../models/bar-chart.model';
import { BarChartProperties } from '../models/chart-card.model';

import { ChartCardComponent } from './chart-card.component';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';

describe('ChartCardComponent', () => {
  const barChartDataOptionA: BarChartData = {
    labels: ['2017', '2018', '2019', '2020', '2021'],
    values: [
      { key: 'Serie A', totals: [0, 20, 89, 30, 14] },
      { key: 'Serie B', totals: [138, 92, 82, 50, 2] },
      { key: 'Serie C', totals: [106, 52, 8, 45, 100] },
      { key: 'Serie D', totals: [92, 78, 50, 13, 94] }
    ]
  };
  const barChartA: BarChartProperties = { data: barChartDataOptionA, stacked: true };

  let component: ChartCardComponent;
  let fixture: ComponentFixture<ChartCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChartCardComponent, MockPipe(EmptyPipe)]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable first option if none are active', () => {
    component.sections = [
      { name: 'Option A', barChart: barChartA },
      { name: 'Option B', barChart: barChartA }
    ];
    component['markDefaultActiveSection']();

    expect(component.sections[0].active).toBe(true);
  });

  it('should disable other options when marking one as active', () => {
    component.sections = [
      { name: 'Option A', barChart: barChartA },
      { name: 'Option B', barChart: barChartA, active: true }
    ];
    component['onSectionClick'](0);

    expect(component.sections[1].active).toBe(false);
  });
});
