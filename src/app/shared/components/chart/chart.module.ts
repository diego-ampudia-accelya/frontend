import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { L10nTranslationModule } from 'angular-l10n';
import { ChartsModule } from 'ng2-charts';

import { BarChartComponent } from './bar-chart/bar-chart.component';
import { DoughnutComponent } from './doughnut/doughnut.component';

@NgModule({
  declarations: [DoughnutComponent, BarChartComponent],
  imports: [CommonModule, ChartsModule, L10nTranslationModule],
  exports: [DoughnutComponent, BarChartComponent]
})
export class ChartModule {}
