import { Component, CUSTOM_ELEMENTS_SCHEMA, Input, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { ChartsModule } from 'ng2-charts';

import { DoughnutComponent } from './doughnut.component';
import { DoughnutDataValue } from '~app/shared/components/chart/models/doughnut-chart.model';
import { TranslatePipeMock } from '~app/test/translate.mock.pipe';

@Component({
  template: `
    <bspl-doughnut [showCustomLegend]="showCustomLegend" [doughnutChartData]="doughnutChartData"></bspl-doughnut>
  `
})
class TestHostComponent {
  @ViewChild(DoughnutComponent, { static: true }) doughnutComponent: DoughnutComponent;
  @Input() showCustomLegend = false;
  @Input() doughnutChartData: DoughnutDataValue[];
}

describe('DoughnutComponent', () => {
  let component: DoughnutComponent;
  let hostComponent: TestHostComponent;

  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ChartsModule],
      declarations: [DoughnutComponent, TestHostComponent, TranslatePipeMock],
      providers: [mockProvider(L10nTranslationService)],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    hostComponent = fixture.componentInstance;
    component = fixture.componentInstance.doughnutComponent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(hostComponent).toBeTruthy();
    expect(component).toBeTruthy();
  });

  it('set doughnutChartData to empty array should render no-data item', () => {
    component.doughnutChartData = [];
    fixture.detectChanges();
    const debugElement = fixture.debugElement.nativeElement;

    expect(debugElement.getElementsByClassName('doughnut-chart-legend-item').length).toBe(0);
  });

  it('set doughnutChartData should set data, labels', () => {
    hostComponent.doughnutChartData = [
      { key: 'Billed', y: 20 },
      { key: 'Pending', y: 40 },
      { key: 'Disputed', y: 40 }
    ];
    fixture.detectChanges();

    expect(component.data[0].length).toBe(3);
    expect(component.labels.length).toBe(3);
  });

  it('colors should return parsed backgroundColors', () => {
    component.backgroundColors = ['red', 'blue'];
    const colors = component.colors;
    expect(colors[0].backgroundColor.length).toBe(2);
    expect(colors[0].backgroundColor[0]).toBe('red');
    expect(colors[0].backgroundColor[1]).toBe('blue');
  });

  it('getHiddenRowsTranslationKey should return single if one item', () => {
    expect(component.getHiddenRowsTranslationKey(1)).toBe('doughnutChart.hiddenRow');
  });

  it('getHiddenRowsTranslationKey should return plural if more than one items', () => {
    expect(component.getHiddenRowsTranslationKey(2)).toBe('doughnutChart.hiddenRows');
  });

  describe('when showCustomLegend is set to false', () => {
    beforeEach(() => {
      hostComponent.showCustomLegend = false;
      fixture.detectChanges();
    });

    it('set doughnutChartData should not set legendItems', () => {
      hostComponent.doughnutChartData = [
        { key: 'Billed', y: 20 },
        { key: 'Pending', y: 40 },
        { key: 'Disputed', y: 40 }
      ];
      fixture.detectChanges();

      expect(component.legendItems.length).toBe(0);
    });

    it('ngOnInit should not detach change detection', () => {
      const detachSpy = spyOn(component.changeDetectorRef, 'detach');
      component.ngOnInit();
      expect(detachSpy).not.toHaveBeenCalled();
    });
  });

  describe('when showCustomLegend is set to true', () => {
    beforeEach(() => {
      hostComponent.showCustomLegend = true;
      fixture.detectChanges();
    });

    it('ngOnInit should detach change detection', () => {
      const detachSpy = spyOn(component.changeDetectorRef, 'detach');
      component.ngOnInit();
      expect(detachSpy).toHaveBeenCalled();
    });

    it('set doughnutChartData should set legendItems', () => {
      hostComponent.doughnutChartData = [
        { key: 'Billed', y: 20 },
        { key: 'Pending', y: 40 },
        { key: 'Disputed', y: 40 }
      ];
      fixture.detectChanges();

      expect(component.legendItems.length).toBe(3);
    });

    it('hiddenItemsCount should return 0 if no legendItems', () => {
      expect(component.hiddenItemsCount).toBe(0);
    });

    it('hiddenItemsCount should return 2, if maxVisibleLegendItems is 4 and data items are 6', () => {
      component.maxVisibleLegendItems = 4;
      hostComponent.doughnutChartData = [
        { key: 'item 1', y: 20 },
        { key: 'item 2', y: 40 },
        { key: 'item 3', y: 40 },
        { key: 'item 4', y: 20 },
        { key: 'item 5', y: 40 },
        { key: 'item 6', y: 40 }
      ];
      fixture.detectChanges();

      expect(component.hiddenItemsCount).toBe(2);
    });

    it('legend items should have color even if there is no index in backgroundColors', () => {
      component.backgroundColors = ['red'];
      hostComponent.doughnutChartData = [
        { key: 'item 1', y: 20 },
        { key: 'item 2', y: 40 }
      ];
      fixture.detectChanges();

      expect(component.legendItems[1].color).toBe('red');
    });

    it('legendItem click should toggle item-filtered class', () => {
      hostComponent.doughnutChartData = [
        { key: 'item 1', y: 20 },
        { key: 'item 2', y: 40 },
        { key: 'item 3', y: 40 },
        { key: 'item 4', y: 20 },
        { key: 'item 5', y: 40 },
        { key: 'item 6', y: 40 }
      ];
      fixture.detectChanges();

      const debugElement = fixture.debugElement.nativeElement;
      const firstLegendElement = debugElement.getElementsByClassName('doughnut-chart-legend-item')[0];
      firstLegendElement.click();
      expect(firstLegendElement.classList.contains('item-filtered')).toBe(true);
      firstLegendElement.click();
      expect(firstLegendElement.classList.contains('item-filtered')).toBe(false);
    });

    it('show hidden legend items click should show all legend items', () => {
      component.maxVisibleLegendItems = 4;
      hostComponent.doughnutChartData = [
        { key: 'item 1', y: 20 },
        { key: 'item 2', y: 40 },
        { key: 'item 3', y: 40 },
        { key: 'item 4', y: 20 },
        { key: 'item 5', y: 40 },
        { key: 'item 6', y: 40 }
      ];
      fixture.detectChanges();

      const debugElement = fixture.debugElement.nativeElement;
      const toggleElement = debugElement.getElementsByClassName('doughnut-chart-legend-hidden-items-toggle')[0];

      toggleElement.click();

      expect(toggleElement.classList.contains('item-hidden')).toBe(true);

      Array.from(debugElement.getElementsByClassName('doughnut-chart-legend-item')).forEach(
        (legendItemLiElement: HTMLLIElement) => {
          expect(legendItemLiElement.classList.contains('item-hidden')).toBe(false);
        }
      );
    });
  });

  describe('hasData()', () => {
    it('should return false when the data is null', () => {
      const result = component.hasData(null);

      expect(result).toBeFalsy();
    });

    it('should return false when the data contains empty arrays', () => {
      const result = component.hasData([[], []]);

      expect(result).toBe(false);
    });

    it('should return false when the data contains multiple arrays with only zeroes', () => {
      const result = component.hasData([
        [0, 0, 0],
        [0, 0, 0]
      ]);

      expect(result).toBe(false);
    });

    it('should return true if there is at least one value greater than 0', () => {
      const result = component.hasData([
        [0, 1],
        [0, 0]
      ]);

      expect(result).toBe(true);
    });

    it('should return true if all values are different than 0', () => {
      const result = component.hasData([
        [1, 2, 3],
        [4, 5, 6]
      ]);

      expect(result).toBe(true);
    });
  });
});
