import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { ChartOptions } from 'chart.js';
import { BaseChartDirective, Color, Label, MultiDataSet } from 'ng2-charts';

import { DoughnutDataValue, DoughnutLegendItem } from '~app/shared/components/chart/models/doughnut-chart.model';

@Component({
  selector: 'bspl-doughnut',
  templateUrl: './doughnut.component.html',
  styleUrls: ['./doughnut.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DoughnutComponent implements OnInit, OnChanges {
  @Input() showCustomLegend = false;

  @Input() maxVisibleLegendItems = 5;

  @Input() options: ChartOptions = {
    aspectRatio: 1,
    elements: {
      arc: {
        borderWidth: 0
      }
    },
    animation: {
      duration: 800
    },
    tooltips: {
      backgroundColor: 'rgba(255, 255, 255, 0.8)',
      bodyFontColor: '#000',
      bodyFontSize: 12,
      xPadding: 12,
      yPadding: 12
    },
    layout: {
      padding: {
        left: 0,
        right: 30,
        top: 30,
        bottom: 30
      }
    },
    legend: {
      display: false
    }
  };

  @Input() doughnutChartData: DoughnutDataValue[];

  @ViewChild(BaseChartDirective) baseChart: BaseChartDirective;

  @ViewChild('legendItemsContainer') legendItemsContainer: ElementRef;

  public readonly noData: MultiDataSet = [[1]];
  public readonly noDataColors: Color[] = [{ backgroundColor: '#e4e5eb' }];

  public noDataOptions: ChartOptions = {
    ...this.options,
    tooltips: {
      ...this.options.tooltips,
      callbacks: {
        label: () => this.translationService.translate('doughnutChart.noDataAvailable')
      }
    },
    hover: {
      ...this.options.hover,
      mode: null
    }
  };

  private _data: MultiDataSet = [[]];
  public get data(): MultiDataSet {
    return this._data;
  }

  private _labels: Label[] = [];
  public get labels(): Label[] {
    return this._labels;
  }

  private _legendItems: DoughnutLegendItem[] = [];
  public get legendItems(): DoughnutLegendItem[] {
    return this._legendItems;
  }

  public get hiddenItemsCount(): number {
    if (this.legendItems) {
      return this.legendItems.filter(item => item.hidden).length;
    }

    return 0;
  }

  public get colors(): Color[] {
    return [
      {
        backgroundColor: this.backgroundColors || []
      }
    ];
  }

  private _backgroundColors: string[];
  @Input() set backgroundColors(value: string[]) {
    this._backgroundColors = value;
  }

  get backgroundColors(): string[] {
    if (!this._backgroundColors || this._backgroundColors.length === 0) {
      this._backgroundColors = [
        '#1e32fa',
        '#0f154d',
        '#f54427',
        '#ffc90a',
        '#ff0000',
        '#289632',
        '#5a14a0',
        '#fcde84',
        '#7884fc',
        '#fac832',
        '#9a9ba6',
        '#d2d6fe'
      ];
    }

    return this._backgroundColors;
  }

  constructor(public changeDetectorRef: ChangeDetectorRef, private translationService: L10nTranslationService) {}

  public ngOnInit() {
    if (this.showCustomLegend) {
      // When custom legend is enabled, every click inside it triggers change detection.
      // This is causing the chart to redraw.
      // Change detection must be disabled in this case.
      this.changeDetectorRef.detach();
    }
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.doughnutChartData) {
      this.parseDoughnutChartData(changes.doughnutChartData.currentValue);
      this.changeDetectorRef.detectChanges();
    }
  }

  public getHiddenRowsTranslationKey(count: number) {
    return count === 1 ? 'doughnutChart.hiddenRow' : 'doughnutChart.hiddenRows';
  }

  public hasData(multiDataSet: MultiDataSet): boolean {
    return multiDataSet?.some(set => set.some(v => v !== 0));
  }

  public onLegendItemClick(event: MouseEvent, index: number) {
    const legendItemElement = event.currentTarget as HTMLLIElement;
    legendItemElement.classList.toggle('item-filtered');

    if (this.baseChart) {
      const chart = this.baseChart.chart;
      const dataItems = chart.getDatasetMeta(0).data;
      dataItems[index].hidden = !dataItems[index].hidden;
      chart.update();
    }
  }

  public onShowHiddenItemsClick(event: MouseEvent) {
    const showHiddenItemsElement = event.currentTarget as HTMLLIElement;
    showHiddenItemsElement.classList.add('item-hidden');

    let previousElement = showHiddenItemsElement.previousElementSibling;
    while (previousElement) {
      previousElement.classList.remove('item-hidden');
      previousElement = previousElement.previousElementSibling;
    }
  }

  private parseDoughnutChartData(data: DoughnutDataValue[]) {
    this._labels = [];
    this._data = [[]];
    this._legendItems = [];

    (data || []).forEach((item, index) => {
      const { key: label, y: value } = item;

      this._labels.push(label);

      this._data[0].push(this.formatValue(value));

      if (this.showCustomLegend) {
        this._legendItems.push({
          label,
          value,
          color: this.getLegendItemColor(index),
          hidden: index >= this.maxVisibleLegendItems
        });
      }
    });
  }

  private formatValue(value): number {
    return parseFloat(value.toString().replace(/,/g, ''));
  }

  private getLegendItemColor(index: number) {
    let color = this.backgroundColors[index] || '';

    if (!color) {
      this.backgroundColors = [...this.backgroundColors, ...this.backgroundColors];
      color = this.backgroundColors[index];
    }

    return color;
  }
}
