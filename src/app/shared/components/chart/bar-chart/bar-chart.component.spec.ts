import { Component, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationModule } from 'angular-l10n';
import { ChartsModule } from 'ng2-charts';

import { BarChartData } from '../models/bar-chart.model';

import { BarChartComponent } from './bar-chart.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

@Component({
  template: `<bspl-bar-chart [title]="barChartTitle" [data]="barChartData" [stacked]="true"></bspl-bar-chart>`
})
class TestHostComponent {
  @ViewChild(BarChartComponent, { static: true }) barChartComponent: BarChartComponent;

  public barChartTitle: string;
  public barChartData: BarChartData = {
    labels: ['2017', '2018', '2019', '2020', '2021'],
    values: [
      { key: 'Serie A', totals: [0, 20, 89, 30, 14] },
      { key: 'Serie B', totals: [138, 92, 82, 50, 2] },
      { key: 'Serie C', totals: [106, 52, 8, 45, 100] },
      { key: 'Serie D', totals: [92, 78, 50, 13, 94] }
    ]
  };
}

describe('BarChartComponent', () => {
  let component: BarChartComponent;
  let hostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ChartsModule, L10nTranslationModule.forRoot(l10nConfig)],
      declarations: [BarChartComponent, TestHostComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance.barChartComponent;
    hostComponent = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeDefined();
    expect(hostComponent).toBeDefined();
  });

  it('should set labels from bar chart data', () => {
    fixture.detectChanges();
    expect(component.labels).toEqual(['2017', '2018', '2019', '2020', '2021']);
  });

  it('should set empty dataset when no data is provided', () => {
    hostComponent.barChartData = { labels: [], values: [] };
    fixture.detectChanges();

    expect(component.datasets).toContain(
      jasmine.objectContaining({
        data: [],
        label: 'DASHBOARD.barCharts.noData'
      })
    );
  });

  it('should set datasets stacks when `stacked` is `true` in bar chart data', () => {
    fixture.detectChanges();
    expect(component.datasets).toContain(jasmine.objectContaining({ stack: 'a' }));
  });

  it('should set title in options when `title` input is specified', () => {
    hostComponent.barChartTitle = 'Test title for bar chart';
    fixture.detectChanges();

    expect(component.options).toEqual(
      jasmine.objectContaining({ title: { display: true, text: 'Test title for bar chart', fontSize: 16 } })
    );
  });
});
