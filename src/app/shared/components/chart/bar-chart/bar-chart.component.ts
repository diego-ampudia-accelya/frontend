import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

import { BarChartData, BarChartDataValue } from '../models/bar-chart.model';

import { COLORS } from './bar-chart.constants';

@Component({
  selector: 'bspl-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BarChartComponent implements OnChanges {
  @Input()
  public get type(): 'bar' | 'horizontalBar' {
    return this._type;
  }
  public set type(value: 'bar' | 'horizontalBar') {
    if (value === 'bar' || value === 'horizontalBar') {
      this._type = value;
    }
  }

  @Input() set data(data: BarChartData) {
    if (data) {
      this.labels = data.labels;
      this.datasets = this.getDatasets(data.values);
    }
  }

  public labels: Label[];
  public datasets: ChartDataSets[];

  public colors: Color[] = COLORS;
  public options: ChartOptions = {
    responsive: true,
    responsiveAnimationDuration: 1500
  };

  private _type: 'bar' | 'horizontalBar' = 'bar';
  private _stacked: boolean;
  @Input()
  public get stacked() {
    return this._stacked;
  }
  public set stacked(value: boolean) {
    if (value !== undefined) {
      this._stacked = value;
      this.setDatasetsStacks();
    }
  }

  private _legend = true;
  @Input()
  public get legend() {
    return this._legend;
  }
  public set legend(value) {
    if (value !== undefined) {
      this._legend = value;
    }
  }

  private _title: string;
  @Input()
  public get title(): string {
    return this._title;
  }
  public set title(value: string) {
    if (value !== undefined) {
      this._title = value;
    }
  }

  constructor(private translationService: L10nTranslationService) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.title?.currentValue) {
      this.options = {
        ...this.options,
        title: {
          display: true,
          text: changes.title.currentValue,
          fontSize: 16
        }
      };
    }
  }

  private getDatasets(values: BarChartDataValue[]): ChartDataSets[] {
    const emptyDataSet: ChartDataSets = {
      data: [],
      label: this.translationService.translate('DASHBOARD.barCharts.noData')
    };

    return values.length
      ? values.map(({ key, totals }) => ({
          data: totals,
          label: key,
          ...(this._stacked && { stack: 'a' })
        }))
      : [emptyDataSet];
  }

  private setDatasetsStacks(): void {
    if (!this.datasets) {
      return;
    }

    this.datasets = this.datasets.map(({ stack, ...set }) => (this._stacked ? { ...set, stack: 'a' } : set));
  }
}
