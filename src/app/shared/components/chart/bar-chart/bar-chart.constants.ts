import { Color } from 'ng2-charts';

// IATA blue + accent scale
export const COLORS: Color[] = [
  { backgroundColor: '#0F154D' }, // Blue primary
  { backgroundColor: '#1E32FA' }, // Blue light
  { backgroundColor: '#FAC832' }, // Yellow
  { backgroundColor: '#F04632' }, // Red
  { backgroundColor: '#F050C8' }, // Pink
  { backgroundColor: '#5A14A0' }, // Purple
  { backgroundColor: '#289632' } // Green
];
