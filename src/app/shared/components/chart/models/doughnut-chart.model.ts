export interface DoughnutLegendItem {
  label: string;
  value: string | number;
  hidden: boolean;
  color: string;
}

export interface DoughnutDataValue {
  key: string;
  y: number;
  disabled?: boolean;
}
