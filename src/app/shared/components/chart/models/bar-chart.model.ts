export interface BarChartData {
  labels: string[];
  values: BarChartDataValue[];
}

export interface BarChartDataValue {
  key: string;
  totals: number[];
}
