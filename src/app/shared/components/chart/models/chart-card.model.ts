import { EventEmitter } from '@angular/core';

import { BarChartData } from './bar-chart.model';

export interface ChartCardSection {
  name: string;
  barChart: BarChartProperties;
  active?: boolean;
  clickEmitter?: EventEmitter<ChartCardSection>;
}

export interface BarChartProperties {
  data: BarChartData;
  title?: string;
  type?: 'bar' | 'horizontalBar';
  stacked?: boolean;
  legend?: boolean;
}
