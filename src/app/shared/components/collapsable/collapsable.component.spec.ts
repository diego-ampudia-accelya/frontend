import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { CollapsableComponent } from '~app/shared/components/collapsable/collapsable.component';
import { TranslatePipeMock } from '~app/test';

describe('CollapsableComponent', () => {
  const translationServiceSpy = jasmine.createSpyObj<L10nTranslationService>('L10nTranslationService', ['translate']);
  let component: CollapsableComponent;
  let fixture: ComponentFixture<CollapsableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [CollapsableComponent, TranslatePipeMock],
      providers: [{ provide: L10nTranslationService, useValue: translationServiceSpy }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('open', () => {
    it('should emit opened event', () => {
      spyOn(component.opened, 'emit');
      component.isInitiallyCollapsed = true;
      component.ngOnInit();

      component.open();

      expect(component.opened.emit).toHaveBeenCalled();
    });
  });

  describe('close', () => {
    it('should emit closed event', () => {
      spyOn(component.closed, 'emit');
      component.isInitiallyCollapsed = false;
      component.ngOnInit();

      component.close();

      expect(component.closed.emit).toHaveBeenCalled();
    });
  });
});
