import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

const imgPath = '../../../../assets/images/utils/';

@Component({
  selector: 'bspl-collapsable',
  templateUrl: './collapsable.component.html',
  styleUrls: ['./collapsable.component.scss']
})
export class CollapsableComponent implements OnInit {
  @Input() id: string;
  @Input() isInitiallyCollapsed = false;
  @Input() hideHeaderIfExpanded = false;
  @Input() title: string;
  @Input() headerTemplate: TemplateRef<any>;
  @Input() collapseImage = imgPath + 'ico_collapse.svg';
  @Input() expandImage = imgPath + 'ico_expand.svg';
  @Input() showCollapsableIcon = true;
  @Input() shouldToggleOnHeaderClick = true;

  @Output() opened = new EventEmitter();
  @Output() closed = new EventEmitter();

  private _collapsed = this.isInitiallyCollapsed;

  public get collapsed() {
    return this._collapsed;
  }

  get showHeaderTemplate() {
    return this.headerTemplate && (!this.hideHeaderIfExpanded || this.collapsed);
  }

  get showDefaultHeader() {
    return !this.headerTemplate && (!this.hideHeaderIfExpanded || this.collapsed);
  }

  public ngOnInit() {
    this._collapsed = this.isInitiallyCollapsed;
  }

  public open(): void {
    if (this.collapsed) {
      this._collapsed = false;
      this.opened.emit();
    }
  }

  public close(): void {
    if (!this.collapsed) {
      this._collapsed = true;
      this.closed.emit();
    }
  }

  public toggle(): void {
    if (this.collapsed) {
      this.open();
    } else {
      this.close();
    }
  }

  public getCollapseImgSrc(): string {
    return this.collapsed ? this.expandImage : this.collapseImage;
  }
}
