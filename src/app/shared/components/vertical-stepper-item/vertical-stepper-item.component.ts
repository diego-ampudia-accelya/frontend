import { Component, Input } from '@angular/core';

@Component({
  selector: 'bspl-vertical-stepper-item',
  templateUrl: './vertical-stepper-item.component.html',
  styleUrls: ['./vertical-stepper-item.component.scss']
})
export class VerticalStepperItemComponent {
  @Input() title: string;
  @Input() date: string;
  @Input() isActive: boolean;
  @Input() isDashedLine: boolean;
}
