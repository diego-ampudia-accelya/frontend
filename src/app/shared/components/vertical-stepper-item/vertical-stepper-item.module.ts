import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { VerticalStepperItemComponent } from './vertical-stepper-item.component';

@NgModule({
  declarations: [VerticalStepperItemComponent],
  imports: [CommonModule],
  exports: [VerticalStepperItemComponent]
})
export class VerticalStepperItemModule {}
