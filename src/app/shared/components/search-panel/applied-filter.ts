export interface AppliedFilter {
  key: string;
  label: string;
  closable: boolean;
}
