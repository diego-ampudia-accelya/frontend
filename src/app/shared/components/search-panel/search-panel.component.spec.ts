import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, NgForm } from '@angular/forms';
import { L10nTranslationModule } from 'angular-l10n';

import { SearchPanelComponent } from './search-panel.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

@Component({
  selector: 'bspl-collapsable',
  template: '<p>Mock Collapsable Component</p>'
})
class MockCollapsableComponent {
  collapsed = false;

  close() {}
}

describe('SearchPanelComponent', () => {
  let component: SearchPanelComponent;
  let fixture: ComponentFixture<SearchPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [L10nTranslationModule.forRoot(l10nConfig)],
      declarations: [SearchPanelComponent, MockCollapsableComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call initializeForm on ngAfterViewInit', () => {
    spyOn(component, 'initializeForm').and.callFake(() => {});
    component.ngAfterViewInit();
    expect(component.initializeForm).toHaveBeenCalled();
  });

  it('onSearch should close collapsible panel and emit search with correct data', async () => {
    spyOn(component.contentCollapsable, 'close');
    spyOnProperty(component, 'searchCriteria', 'get').and.returnValue({ name: 'John' });
    spyOn(component.search, 'emit');

    await component.onSearch();

    expect(component.contentCollapsable.close).toHaveBeenCalledTimes(1);
    expect(component.search.emit).toHaveBeenCalledWith({ name: 'John' });
  });

  it('should change the value of showAdvanceSearch on toggleAdvanceSearch', () => {
    expect(component.showAdvanceSearch).toEqual(false);
    component.toggleAdvanceSearch();
    expect(component.showAdvanceSearch).toEqual(true);
  });

  it('should initialize form on initializeForm when form is defined', () => {
    const formBuilder = new FormBuilder();
    component.form = formBuilder.group({
      searchedKey: 'someValue'
    });
    component.initializeForm();
    expect(component.form.value).toEqual({ searchedKey: 'someValue' });
  });

  it('should initialize form on initializeForm when ngForm is defined', () => {
    component.ngForm = new NgForm([], []);
    component.initializeForm();
    expect(component.form).toEqual(component.ngForm);
  });

  describe('isEmpty', () => {
    it('should return true if there is empty string', () => {
      const result = component['isEmpty']('');

      expect(result).toBe(true);
    });

    it('should return true if there is empty array', () => {
      const result = component['isEmpty']([]);

      expect(result).toBe(true);
    });

    it('should return true if there is empty object', () => {
      const result = component['isEmpty']({});

      expect(result).toBe(true);
    });

    it('should return true if value is undefined', () => {
      const result = component['isEmpty'](undefined);

      expect(result).toBe(true);
    });

    it('should return true if value is null', () => {
      const result = component['isEmpty'](null);

      expect(result).toBe(true);
    });

    it('should return false if value is not empty string', () => {
      const result = component['isEmpty']('result');

      expect(result).toBe(false);
    });

    it('should return false if value is not empty array', () => {
      const result = component['isEmpty'](['result']);

      expect(result).toBe(false);
    });

    it('should return false if value is number', () => {
      const result = component['isEmpty'](10);

      expect(result).toBe(false);
    });

    it('should return false if value is object with properties', () => {
      const result = component['isEmpty']({ result: 'result' });

      expect(result).toBe(false);
    });

    it('should return false if value is date', () => {
      const result = component['isEmpty'](new Date());

      expect(result).toBe(false);
    });
  });
});
