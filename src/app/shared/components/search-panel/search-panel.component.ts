import { AfterViewInit, Component, ContentChild, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, NgForm } from '@angular/forms';
import { cloneDeep, startCase } from 'lodash';
import { Observable } from 'rxjs';

import { AppliedFilter } from './applied-filter';
import { ButtonDesign } from '~app/shared/components/buttons/enums/buttonDesign';
import { CollapsableComponent } from '~app/shared/components/collapsable/collapsable.component';
import { canProceed } from '~app/shared/utils';

// TODO: Whe need to completely revise the Search Panel implementation
// TODO: Currently the component does not support a case where you have nested formGroups within the form controls.
// TODO: Later on should be implemented in a different way. Instead of working with the form directly, you should be
//       able to provide some kind of a config to the component.
@Component({
  selector: 'bspl-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.scss']
})
export class SearchPanelComponent implements AfterViewInit {
  @Input() showCollapsableIcon = true;
  @Input() form: NgForm | FormGroup;
  @Input() filters: { [key: string]: any };
  @Input() showAdvancedSearchBtn = false;
  @Input() predefinedFilters: unknown;
  @Input() searchCriteriaKeyMappers: { [key: string]: string } = {};
  @Input() searchCriteriaValueMappers: { [key: string]: (value: any) => string } = {};
  /**
   * Async action which will be executed before a filter is applied. The result indicates whether the action should be executed.
   */
  @Input() beforeFilterApply$: Observable<boolean>;

  @Output() search = new EventEmitter<{ [key: string]: any }>();
  @Output() removeFilter = new EventEmitter<string>();
  @Output() clear = new EventEmitter<{ [key: string]: any }>();
  @Output() toggleAdvanceSearchPanel = new EventEmitter<boolean>();

  // TODO: Research if this can be avoided if we can inject the NgForm so we can avoid using @Input() for the form/formGroup.
  @ContentChild(NgForm) public ngForm: NgForm; // Used in case of template driven forms.
  @ContentChild(FormGroupDirective) public formGroupDirective: FormGroupDirective; // Used in case of reactive driven forms.

  @ViewChild('contentCollapsable', { static: true }) contentCollapsable: CollapsableComponent;

  public buttonDesign = ButtonDesign;
  public showAdvanceSearch = false;

  public get appliedFilters(): AppliedFilter[] {
    return this.getAppliedFilters();
  }

  get collapsed() {
    return this.contentCollapsable.collapsed;
  }

  get hasAppliedFilter(): boolean {
    return !this.isEmpty(this.appliedFilters);
  }

  get canReset(): boolean {
    const { controls = {} } = this.form || {};

    return Object.keys(controls).some(key => this.isFilterPersisted(key));
  }

  /**
   * Returns a new object containing non empty search criteria values.
   */
  get searchCriteria(): any {
    let filters = {};

    // Delete form controls which do not have values.
    if (this.form && this.form.value) {
      filters = cloneDeep(this.form.value);
      for (const key in filters) {
        if (this.isEmpty(filters[key])) {
          delete filters[key];
        }
      }
    }

    return filters;
  }

  public ngAfterViewInit(): void {
    this.initializeForm();
  }

  public async onSearch(): Promise<void> {
    if (!(await canProceed(this.beforeFilterApply$))) {
      return;
    }

    this.contentCollapsable.close();
    this.search.emit(this.searchCriteria);
  }

  public async removeTag(key: string): Promise<void> {
    if (!(await canProceed(this.beforeFilterApply$))) {
      return;
    }

    if (this.form.controls[key]) {
      this.form.controls[key].reset();
    }
    this.removeFilter.emit(key);
  }

  public async onClear(): Promise<void> {
    if (!(await canProceed(this.beforeFilterApply$))) {
      return;
    }

    this.clearState();

    this.clear.emit(this.searchCriteria);
  }

  public onTagClick(): void {
    this.contentCollapsable.open();
  }

  public clearState() {
    if (this.form) {
      this.form.reset(this.predefinedFilters);
    }
  }

  public toggleAdvanceSearch(): void {
    this.showAdvanceSearch = !this.showAdvanceSearch;
    this.toggleAdvanceSearchPanel.emit(this.showAdvanceSearch);
  }

  public initializeForm(): void {
    if (this.form != null) {
      return;
    }

    if (this.ngForm) {
      this.form = this.ngForm;
    } else if (this.formGroupDirective) {
      this.form = this.formGroupDirective.control;
    }
  }

  private getAppliedFilters(): AppliedFilter[] {
    return Object.entries(this.searchCriteria).map(([key, value]) => ({
      key,
      label: this.generateTagValue(key, value),
      closable: !this.isFilterPersisted(key)
    }));
  }

  private generateTagValue(key: string, value: any): string {
    let normalizedValue = '';
    let normalizedKey = '';

    const keyMappers = this.searchCriteriaKeyMappers;
    if (keyMappers && keyMappers[key]) {
      normalizedKey = keyMappers[key];
    } else {
      normalizedKey = this.normalizeKey(key);
    }

    const valueMappers = this.searchCriteriaValueMappers;

    if (valueMappers && valueMappers[key]) {
      normalizedValue = valueMappers[key](value);
    } else {
      normalizedValue = this.normalizeValue(value);
    }

    return `${normalizedKey} - ${normalizedValue}`;
  }

  private isFilterPersisted(filterKey: string): boolean {
    return (this.predefinedFilters || {})[filterKey];
  }

  private isEmpty(value: any): boolean {
    if (typeof value === 'boolean') {
      return false;
    }

    if (Array.isArray(value) && value.length === 0) {
      return true;
    }

    if (value instanceof Date) {
      return false;
    }

    const isEmptyObject = value && typeof value === 'object' && Object.keys(value).length === 0;
    if (isEmptyObject) {
      return true;
    }

    if (value === 0) {
      return false;
    }

    return !value;
  }

  private normalizeKey(key: string): string {
    if (!key) {
      return '';
    }

    return startCase(key);
  }

  private normalizeValue(value: any) {
    if (Array.isArray(value)) {
      value = value.map(v => (typeof v === 'object' ? v.displayValue : v)).join(', ');
    } else if (value instanceof Date) {
      value = value.toLocaleDateString();
    } else if (typeof value === 'object') {
      value = value.displayValue;
    }

    return value;
  }
}
