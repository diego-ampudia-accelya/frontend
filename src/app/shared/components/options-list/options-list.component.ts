import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bspl-options-list',
  templateUrl: './options-list.component.html',
  styleUrls: ['./options-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OptionsListComponent {
  @Input() title?: string;
  @Input() optionsListData: any;

  @Output() valueChange = new EventEmitter<any>();

  public changeOptionValue(enabled: boolean, id: number): void {
    this.valueChange.emit({ id, enabled });
  }
}
