/* eslint-disable @typescript-eslint/naming-convention */
export const GRID_TABLE = {
  DEFAULT_PAGE_INFO: {
    page: 0,
    size: 20,
    totalElements: 0
  },
  EVENT_TYPES: {
    CHECKBOX: 'checkbox',
    CLICK: 'click'
  }
};

export enum EStatus {
  COMPLETED = 'COMPLETED',
  FAILED = 'FAILED',
  PROCESSING = 'PROCESSING',
  REJECTING = 'REJECTING',
  REJECTED = 'REJECTED',
  PENDING = 'PENDING'
}
