import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of, ReplaySubject } from 'rxjs';

import { GridTableComponent } from './grid-table.component';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { ApiService } from '~app/shared/services/api.service';
import { DataSource } from '~app/shared/services/data-source.service';
import { TranslatePipeMock } from '~app/test';

class ApiServiceMock extends ApiService<unknown> {
  constructor(httpClient: HttpClient) {
    super(httpClient, '');
  }
}

describe('GridTableComponent', () => {
  let component: GridTableComponent;
  let fixture: ComponentFixture<GridTableComponent>;

  const httpClientSpy = jasmine.createSpyObj<HttpClient>('HttpClient', ['get']);
  httpClientSpy.get.and.returnValue(of());

  const translationStub: jasmine.SpyObj<L10nTranslationService> = jasmine.createSpyObj('L10nTranslationService', [
    'translate',
    'onChange'
  ]);
  const language$: ReplaySubject<string> = new ReplaySubject(1);
  translationStub.onChange.and.returnValue(language$.asObservable());
  translationStub.translate.and.returnValue('translated');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DateTimeFormatPipe, TranslatePipeMock, GridTableComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: L10nTranslationService, useValue: translationStub }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridTableComponent);
    component = fixture.componentInstance;
    const apiServiceMock = new ApiServiceMock(httpClientSpy);
    component.dataSource = new DataSource(apiServiceMock);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should push to the array the object that were selected', () => {
    component.selected = [];
    component.onSelect({ selected: [{ a: 5 }, { b: 4 }] });

    expect(component.selected).toEqual([{ a: 5 }, { b: 4 }]);
  });

  it('should be empty array if you put null to the method', () => {
    component.selected = [];
    component.onSelect(null);

    expect(component.selected).toEqual([]);
  });

  describe('sort', () => {
    it('should emit sortChange if there is data and a column is sortable', async () => {
      const spy = spyOn(component.sortChange, 'emit');
      component.rows = [{ prop: 'some value' }];

      await component.sort({ prop: 'prop', sortable: true });
      expect(spy).toHaveBeenCalledWith([
        {
          attribute: 'prop',
          sortType: SortOrder.Asc
        }
      ]);
    });

    it('should not emit sortChange if there is no data and a column is sortable', async () => {
      const spy = spyOn(component.sortChange, 'emit');

      component.rows = null;
      await component.sort({ prop: 'prop', sortable: true });
      expect(spy).not.toHaveBeenCalled();

      component.rows = [];
      await component.sort({ prop: 'prop' });
      expect(spy).not.toHaveBeenCalled();
    });

    it('should not emit sortChange if has data but a column is not provided', async () => {
      const spy = spyOn(component.sortChange, 'emit');

      component.rows = [{ prop: 'some value' }];
      await component.sort(null);
      expect(spy).not.toHaveBeenCalled();
    });

    it('should not emit sortChange if has data but a column is not sortable', async () => {
      const spy = spyOn(component.sortChange, 'emit');

      component.rows = [{ prop: 'some value' }];
      await component.sort({ prop: 'prop', sortable: false });
      expect(spy).not.toHaveBeenCalled();
    });
  });

  describe('onPageChange', () => {
    it('should emit pageChange', () => {
      const spy = spyOn(component.pageChange, 'emit');
      component.onPageChange({ currentPage: 0, itemsPerPage: 50, totalElements: 0 });
      expect(spy).toHaveBeenCalledWith({ page: 0, size: 50, totalElements: 0 });
    });
  });
});
