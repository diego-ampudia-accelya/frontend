import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { DataTableBodyRowComponent, DatatableComponent, TableColumnProp } from '@swimlane/ngx-datatable';
import { L10nTranslationService } from 'angular-l10n';
import { clone, groupBy, isEmpty } from 'lodash';
import moment from 'moment-mini';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InputComponent } from '~app/shared/components/input/input.component';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { DropdownOption, GridColumn } from '~app/shared/models';
import { getActionByType, GridTableAction } from '~app/shared/models/grid-table-actions.model';
import { PaginationInfo } from '~app/shared/models/pagination-info.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { OwnSortingObject, SortingObject } from '~app/shared/models/sorting-object.model';
import { DataSource } from '~app/shared/services/data-source.service';
import { canProceed } from '../../utils';
import { ButtonDesign } from '../buttons/enums/buttonDesign';
import { CheckboxDesign } from '../buttons/enums/checkboxDesign';
import { EStatus, GRID_TABLE } from './grid-table.constants';

const actionsColumn: GridColumn = {
  cellTemplate: 'actionsColumnCellTmpl',
  headerTemplate: 'actionsColumnHeaderTmpl',
  cellClass: 'cell-expand-icons',
  headerClass: 'header-expand-icons',
  maxWidth: 100
};

const primaryActionsColumn: GridColumn = {
  cellTemplate: 'primaryActionsColumnCellTmpl',
  headerTemplate: 'primaryActionsColumnHeaderTmpl',
  cellClass: 'cell-expand-icons primary',
  headerClass: 'header-expand-icons primary',
  maxWidth: 50,
  flexGrow: 1
};

@Component({
  selector: 'bspl-grid-table',
  templateUrl: './grid-table.component.html',
  styleUrls: ['./grid-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridTableComponent implements OnInit, OnChanges, OnDestroy, AfterViewChecked {
  @ViewChild('dataTable', { static: true })
  table: DatatableComponent;
  // invisible table fix
  @ViewChild('resizeWorkaround', { static: true }) resizeWorkaround;
  @ViewChild('defaultCellTmpl', { static: true })
  defaultCellTmpl: TemplateRef<any>;
  @ViewChild('newLineTitleCellTmpl', { static: true })
  newLineTitleCellTmpl: TemplateRef<any>;
  @ViewChild('dateTimeCellTmpl', { static: true })
  dateTimeCellTmpl: TemplateRef<any>;
  @ViewChild('dayMonthYearCellTmpl', { static: true })
  dayMonthYearCellTmpl: TemplateRef<any>;
  @ViewChild('dayMonthYearFromMonthDayCellTmpl', { static: true })
  dayMonthYearFromMonthDayCellTmpl: TemplateRef<any>;
  @ViewChild('statusTmpl', { static: true })
  statusTmpl: TemplateRef<any>;
  @ViewChild('statusStringTmpl', { static: true })
  statusStringTmpl: TemplateRef<any>;
  @ViewChild('templateTmpl', { static: true })
  templateTmpl: TemplateRef<any>;
  @ViewChild('commonLinkCellTmpl', { static: true })
  commonLinkCellTmpl: TemplateRef<any>;
  @ViewChild('multipleLinkCellTmpl', { static: true })
  multipleLinkCellTmpl: TemplateRef<any>;
  @ViewChild('commonLinkFromObjCellTmpl', { static: true })
  commonLinkFromObjCellTmpl: TemplateRef<any>;
  @ViewChild('defaultHeaderTmpl', { static: true })
  defaultHeaderTmpl: TemplateRef<any>;
  @ViewChild('selectHeaderTmpl', { static: true })
  selectHeaderTmpl: TemplateRef<any>;
  @ViewChild('selectCellTmpl', { static: true })
  selectCellTmpl: TemplateRef<any>;
  @ViewChild('actionsHeaderTmpl')
  actionsHeaderTmpl: TemplateRef<any>;
  @ViewChild('actionsCellTmpl')
  actionsCellTmpl: TemplateRef<any>;
  @ViewChild('actionsColumnCellTmpl', { static: true })
  actionsColumnCellTmpl: TemplateRef<any>;
  @ViewChild('actionsColumnHeaderTmpl', { static: true })
  actionsColumnHeaderTmpl: TemplateRef<any>;
  @ViewChild('checkboxHeaderTmpl', { static: true })
  checkboxHeaderTmpl: TemplateRef<any>;
  @ViewChild('multiValueByNameCellTmpl', { static: true })
  multiValueByNameCellTmpl: TemplateRef<any>;
  @ViewChild('primaryActionsColumnCellTmpl', { static: true })
  primaryActionsColumnCellTmpl: TemplateRef<any>;
  @ViewChild('primaryActionsColumnHeaderTmpl', { static: true })
  primaryActionsColumnHeaderTmpl: TemplateRef<any>;
  @ViewChild('statusBasicTmpl', { static: true })
  statusBasicTmpl: TemplateRef<any>;
  @ViewChild('inputOutputTmpl', { static: true })
  inputOutputTmpl: TemplateRef<any>;

  @ViewChild('actionsMenuContainer')
  actionsMenuContainer: ElementRef;
  @ViewChild('yesNoTmpl', { static: true }) yesNoTmpl: TemplateRef<any>;
  @ViewChild('linkCellTmplWithObj', { static: true }) linkCellTmplWithObj: TemplateRef<any>;
  @ViewChild('linkCellTmplWithObjDisabled', { static: true }) linkCellTmplWithObjDisabled: TemplateRef<any>;
  @ViewChild('defaultBooleanCellTmpl', { static: true }) defaultBooleanCellTmpl: TemplateRef<any>;
  @ViewChild('defaultCheckboxCellTmpl', { static: true }) defaultCheckboxCellTmpl: TemplateRef<any>;
  @ViewChild('defaultLabeledCheckboxCellTmpl', { static: true }) defaultLabeledCheckboxCellTmpl: TemplateRef<any>;
  @ViewChild('commonLinkTargetCellTmpl', { static: true }) commonLinkTargetCellTmpl: TemplateRef<any>;

  @ViewChild('checkboxWithDisableRowCellTmpl', { static: true }) checkboxWithDisableRowCellTmpl: TemplateRef<any>;
  @ViewChild('checkboxHeaderWithoutLabelTmpl', { static: true }) checkboxHeaderWithoutLabelTmpl: TemplateRef<any>;

  @ViewChild('badgeInfoCellTmpl', { static: true }) badgeInfoCellTmpl: TemplateRef<any>;
  @ViewChild('textWithBadgeInfoCellTmpl', { static: true }) textWithBadgeInfoCellTmpl: TemplateRef<any>;

  @ViewChild('tooltipCellTmpl', { static: true }) tooltipCellTmpl: TemplateRef<any>;

  @ViewChild('amountCellTmpl', { static: true }) amountCellTmpl: TemplateRef<any>;

  @ViewChild('readWriteTmpl', { static: true }) readWriteTmpl: TemplateRef<any>;

  @ViewChild('statusExtendedTmpl', { static: true }) statusExtendedTmpl: TemplateRef<any>;

  @ViewChild('commonPipeSeparatorCellTmpl', { static: true }) commonPipeSeparatorCellTmpl: TemplateRef<any>;

  @ViewChild('dropdownCellTmpl', { static: true }) dropdownCellTmpl: TemplateRef<any>;

  @ViewChildren(InputComponent)
  bsplInputs: QueryList<InputComponent>;

  @Input() dataSource: DataSource<any>;
  @Input() rowClassesModifier: (rowData: any, classes: string[]) => string[];
  @Input() rows = [];
  @Input() isLoading: boolean;
  @Input() columns: Array<GridColumn> = [];
  @Input() rowDetailTemplate: TemplateRef<any>;
  @Input() primaryFilter: any;
  @Input() set actions(val: Array<string>) {
    this.rowActions = val.map(getActionByType);
  }

  @Input() set primaryActions(val: Array<string>) {
    this.primaryRowActions = val ? val.map(getActionByType) : [];

    this.primaryRowActions = this.setDefaultGroup(this.primaryRowActions);

    this.groupedActions = groupBy(this.primaryRowActions, 'group');
  }
  @Input() paging: PaginationInfo = GRID_TABLE.DEFAULT_PAGE_INFO;
  @Input() isPaginationSupported = true;
  @Input() sorting: Array<SortingObject> = [];
  @Input() sortOrder: SortOrder = SortOrder.Asc;
  @Input() sortedColumn: string;
  @Input() hasOwnSorting = false;
  @Input() columnMode = 'force';
  @Input() linkTarget = '';
  /**
   * Async action which will be executed before sorting or paging change. The result indicates whether the action should be executed.
   */
  @Input() beforeQueryChange$: Observable<boolean>;
  @Input() showIconEmptyActions = false;
  @Input() maxSupportedItems: number;

  @Output() actionClick = new EventEmitter();
  @Output() rowLinkClick = new EventEmitter();
  @Output() imageClick = new EventEmitter();
  @Output() rowToggle = new EventEmitter<any>();
  @Output() checkboxChange = new EventEmitter();
  @Output() dropdownChange = new EventEmitter();
  @Output() sortChange = new EventEmitter<SortingObject[]>();
  @Output() pageChange = new EventEmitter<PaginationInfo>(true);
  @Output() actionsMenuClick = new EventEmitter<any>();
  @Output() maxSupportedPageExceeded = new EventEmitter();

  public columnsToDisplay: Array<GridColumn>;
  public rowActions: Array<GridTableAction> = [];
  public primaryRowActions: Array<GridTableAction> = [];
  public filterInput: any;
  public filtering: RequestQueryFilter[] = [];
  public selected = []; // TODO: Rename. Too generic naming.
  public isFilterOpen = false;
  public expandedRowIndex: number;
  public isRowDetailsOpen: boolean;
  public selectedRowIndex: number;
  public buttonDesign = ButtonDesign;
  public checkboxHeaders = [];
  public checkboxColumns = [];
  public translation = {
    enabled: this.translationService.translate('Enabled'),
    disabled: this.translationService.translate('Disabled')
  };
  public defaultMessages = {
    emptyMessage: this.translationService.translate('GRID_TABLE.TABLE.NO_DATA_TO_DISPLAY')
  };
  public todaysDate = new Date();
  public moment = moment;
  public groupedActions: { [key: string]: GridTableAction[] };
  public extendedStatuses = EStatus;

  public ownSorting: OwnSortingObject[] = [];

  private currentComponentWidth;
  private destroy$ = new Subject();

  constructor(private changeDetectorRef: ChangeDetectorRef, private translationService: L10nTranslationService) {}

  @HostListener('document:click', ['$event']) onDocumentClick(event: any): void {
    const target = event.target;
    if (
      this.actionsMenuContainer &&
      this.actionsMenuContainer.nativeElement &&
      !this.actionsMenuContainer.nativeElement.contains(target)
    ) {
      this.toggleActionsMenu(event, null);
    }
  }

  public ngOnInit(): void {
    this.initDataSource(this.dataSource);
    this.getData();
  }

  public get hasData() {
    return this.rows && this.rows.length > 0;
  }

  public get allRowsDisabled() {
    return this.rows.every(row => row.editable === 'DISABLED');
  }

  public get groupedOptionKeys() {
    let res = [];
    if (this.groupedActions) {
      res = Object.keys(this.groupedActions);
    }

    return res;
  }

  ngAfterViewChecked() {
    // Check if the table size has changed,
    if (
      this.table &&
      this.table.recalculate &&
      this.resizeWorkaround.nativeElement.clientWidth !== this.currentComponentWidth
    ) {
      this.currentComponentWidth = this.resizeWorkaround.nativeElement.clientWidth;
      this.table.recalculate();
      this.changeDetectorRef.detectChanges();
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.primaryFilter) {
      this.filtering = changes.primaryFilter.currentValue;
      // Skip reloading the data on first change to prevent duplicated backend calls
      if (!changes.primaryFilter.firstChange) {
        this.paging = { ...this.paging, page: 0 };
        this.emitPageChange();
        this.getData();
      }
    }

    if (changes.rows) {
      this.initCheckboxHeaders();
      this.updateHeaderCheckboxDesign();
      this.resizeColumnsHandler();
      this.initHorizontalScrollBarLogic();

      if (!this.rows?.length && this.paging.page > 0) {
        this.paging = { ...this.paging, page: 0 };
        this.emitPageChange();
      }
    }

    if (changes.rows && changes.dataSource) {
      throw new Error('Use either `rows` or `dataSource`. Using both is not supported.');
    }

    if (changes.columns?.currentValue) {
      this.columnsToDisplay = this.getColumnsToDisplay(changes.columns.currentValue);
    }
  }

  public initDataSource(dataSource: DataSource<any>): void {
    this.initCheckboxHeaders();

    if (dataSource) {
      dataSource.loading$.subscribe(isLoading => {
        this.isLoading = isLoading;
        this.changeDetectorRef.detectChanges();
      });

      dataSource.connect().subscribe(rows => {
        this.rows = rows;
        this.updateHeaderCheckboxDesign();

        this.resizeColumnsHandler();
        this.initHorizontalScrollBarLogic();
      });

      dataSource.paginationInfo$.subscribe(paging => {
        if (paging) {
          this.paging = paging;
        }
      });
    }
  }

  private initHorizontalScrollBarLogic(): void {
    // If horizontal scroll is enabled
    if (this.table.scrollbarH && this.rows.length) {
      this.changeDetectorRef.detectChanges();

      const dataTableElement = this.table.element;
      // Get datatable body because it is where scroll is shown and shadow should be applied
      const dataTableBodyElement = this.table.element.getElementsByTagName('datatable-body')[0];

      // Get maximum scrollLeft value. If zero -> no horizontal scroll present
      const maxScrollLeft = dataTableBodyElement.scrollWidth - dataTableBodyElement.clientWidth;

      // If horizontal scroll is present
      if (maxScrollLeft) {
        // At first shadow is only visible on the right
        dataTableElement.classList.add('shadow-right');
      }

      // Listen to scroll events so we change shadows accordingly during component life
      this.table.scroll.pipe(takeUntil(this.destroy$)).subscribe(() => {
        // Shadows start to appear after scrolling 5 pixels for better looking
        const maxScrollLeftWithMargin = dataTableBodyElement.scrollWidth - dataTableBodyElement.clientWidth - 5;
        const scrollIsInTheMiddle =
          dataTableBodyElement.scrollLeft > 0 && dataTableBodyElement.scrollLeft < maxScrollLeftWithMargin;

        if (dataTableBodyElement.scrollLeft < maxScrollLeftWithMargin || scrollIsInTheMiddle) {
          dataTableElement.classList.add('shadow-right');
        } else {
          dataTableElement.classList.remove('shadow-right');
        }

        if (dataTableBodyElement.scrollLeft > 0 || scrollIsInTheMiddle) {
          dataTableElement.classList.add('shadow-left');
        } else {
          dataTableElement.classList.remove('shadow-left');
        }
      });
    }
  }

  public getData(): void {
    const sortBy = this.sorting[0] ? this.sorting[0].attribute : undefined;
    const sortOrder = this.sorting[0] ? this.sorting[0].sortType : SortOrder.Asc;
    const requestQuery = new RequestQuery(this.paging.page, this.paging.size, sortBy, sortOrder, this.filtering);

    if (this.hasOwnSorting) {
      this.ownSorting = [{ prop: this.sortedColumn, dir: this.sortOrder.toLowerCase() }];
      this.changeDetectorRef.detectChanges();

      // Since the table implements `OnPush` change detection, we need to trigger a change with this assignation
      // More information: https://swimlane.gitbook.io/ngx-datatable/readme/cd
      this.rows = [...this.rows];
    }

    if (this.dataSource) {
      this.dataSource.get(requestQuery);
    }
  }

  private getColumnsToDisplay(columns: Array<GridColumn>): Array<GridColumn> {
    const cloneItems = <T>(items: T[]) => items.map(clone);
    const columnsToDisplay = cloneItems(columns).filter(column => !column.hidden);
    const actionsLength = this.rowActions.length;

    if (actionsLength) {
      const actionsColumnClone = clone(actionsColumn);
      actionsColumnClone.maxWidth = Math.max(actionsLength * 50, 100); // TODO: Do we need this?
      columnsToDisplay.push(actionsColumnClone);
    }

    if (this.primaryRowActions.length || this.showIconEmptyActions) {
      columnsToDisplay.push(clone(primaryActionsColumn));
    }

    // We need to reset checkbox columns in order to prevent an infinite array build removing checkbox columns from prior calls
    this.checkboxColumns = [];
    columnsToDisplay.forEach(col => {
      col.resizeable = false;
      if (col.cellTemplate === 'defaultLabeledCheckboxCellTmpl') {
        this.checkboxColumns.push(col.prop);
      }

      if (col.cellTemplate === 'checkboxHeaderTmpl') {
        col.headerTemplate = this[col.cellTemplate];
      }

      if (col.cellTemplate === 'checkboxWithDisableRowCellTmpl') {
        this.checkboxColumns.push(col.prop);
      }

      if (col.cellTemplate === 'checkboxHeaderWithoutLabelTmpl') {
        col.headerTemplate = this[col.cellTemplate];
      }

      if (typeof col.cellTemplate === 'string') {
        col.cellTemplate = this[col.cellTemplate];
      }

      if (typeof col.headerTemplate === 'string') {
        col.headerTemplate = this[col.headerTemplate];
      }

      if (!col.cellTemplate) {
        col.cellTemplate = this.defaultCellTmpl;
      }

      if (!col.headerTemplate) {
        col.headerTemplate = this.defaultHeaderTmpl;
      }
    });

    const firstColumn = columnsToDisplay[0];
    if (firstColumn && firstColumn.checkboxable) {
      firstColumn.maxWidth = 50;
    }

    // We initialize checkbox headers after modifying checkbox columns
    this.initCheckboxHeaders();

    return columnsToDisplay;
  }

  public onToggle(event) {
    this.rowToggle.emit(event);
  }

  public onSelect(selection: { selected: any[] }): void {
    const selectedRows = selection ? selection.selected : [];
    this.selected.push(...selectedRows);
  }

  public onActivate(event): void {
    if (event.type === GRID_TABLE.EVENT_TYPES.CLICK && this.rowDetailTemplate) {
      if (this.expandedRowIndex === event.row.id) {
        this.table.rowDetail.toggleExpandRow(event.row);
        this.isRowDetailsOpen = false;
      } else {
        if (event.column.prop !== 'isSelected') {
          this.table.rowDetail.collapseAllRows();
          this.expandedRowIndex = event.row.id;
          this.table.rowDetail.toggleExpandRow(event.row);
          this.isRowDetailsOpen = true;
        }
      }
      this.resizeColumnsHandler();
    }
  }

  public toggleFilter(columnName): void {
    this.isFilterOpen = !this.isFilterOpen;
    if (this.isFilterOpen) {
      setTimeout(() => {
        const selectedColumn = this.bsplInputs.toArray().find(input => input.name === columnName);
        if (selectedColumn) {
          selectedColumn.focusElement();
        }
      });
    }
  }

  public filterItems(): void {
    this.getData();
  }

  public addFilterValue({ value }: any, columnName: any): void {
    if (value !== '') {
      const filter = new RequestQueryFilter(columnName, value);

      this.filtering.push(filter);
    }
  }

  public async sort(column: GridColumn): Promise<void> {
    const hasData = this.rows != null && this.rows.length;
    const isColumnSortable = column != null && column.sortable && hasData;
    if (!isColumnSortable) {
      return;
    }

    if (!(await canProceed(this.beforeQueryChange$))) {
      return;
    }

    this.setSorting(column);
    this.sortChange.emit(this.sorting);
    this.getData();
  }

  public setSorting(column: GridColumn): void {
    this.setSortOrder(column);

    this.sorting = [
      {
        attribute: this.sortedColumn,
        sortType: this.sortOrder
      }
    ];
  }

  public applyRowClass() {
    const rowDetailTemplate = this.rowDetailTemplate;
    const rowClassesModifier = this.rowClassesModifier;

    return function (rowData: any) {
      const rowClasses: string[] = [];

      if (rowClassesModifier) {
        rowClassesModifier(rowData, rowClasses);
      }

      if (rowData.editable && rowData.editable === 'DISABLED') {
        rowClasses.push('disabled-row');
      }

      if (rowData.isSelected) {
        rowClasses.push('selected-row');
      }

      if (rowDetailTemplate) {
        rowClasses.push('datatable-body-row-with-details');
      }

      if ((this as DataTableBodyRowComponent).expanded) {
        rowClasses.push('expanded-row');
      }

      return rowClasses.join(' ');
    };
  }

  public onPageChange(pageData: any): void {
    this.paging = {
      ...this.paging,
      size: pageData.itemsPerPage,
      page: Math.max(pageData.currentPage - 1, 0)
    };
    this.emitPageChange();
    this.getData();
  }

  public resizeColumnsHandler(): void {
    setTimeout(() => {
      // NOTE: new Event() is not supported in IE. That's why we are using custom-event-polyfill.
      window.dispatchEvent(new CustomEvent('resize'));
    }, 10);
  }

  public preventDefaultClick(event: Event): void {
    event.stopPropagation();
  }

  // TODO: How should we handle this in case there are multiple link columns in the table?
  //* For this, multipleLinkCellTmpl has been created
  public handleCommonLinkTmplClick(event: Event, row): void {
    event.stopPropagation();

    this.rowLinkClick.emit(row.id);
  }

  public handleMultipleLinkTmplClick(event: Event, row, column, value): void {
    event.stopPropagation();

    this.rowLinkClick.emit({ row, propertyName: column.prop, propertyValue: value });
  }

  public handleCellTmplWithObjClick(event: Event, row): void {
    event.stopPropagation();

    this.rowLinkClick.emit(row);
  }

  public onActionClick(event: Event, row, action): void {
    event.stopPropagation();
    if (!this.isActionDisabled(row, action)) {
      this.actionClick.emit({ event, row, action });
    }
  }

  public onImageClick(event: Event, row: any, column: GridColumn): void {
    event.stopPropagation();

    this.imageClick.emit({ event, row, column });
  }

  public toggleActionsMenu(event: Event, rowIndex): void {
    event.stopPropagation();

    this.selectedRowIndex = rowIndex === this.selectedRowIndex ? null : rowIndex;

    if (this.selectedRowIndex !== null) {
      this.actionsMenuClick.emit(this.rows[rowIndex]);
    }
  }

  public handleCheckboxChange(row: any, column: GridColumn, state: boolean): void {
    const indexOfUpdated = this.rows.indexOf(row);
    this.rows = Object.assign([], this.rows, {
      [indexOfUpdated]: { ...row, [column.prop]: state }
    });

    this.updateHeaderCheckboxDesign();
    this.checkboxChange.emit(this.rows);
  }

  public toggleCheckboxes(column: string, isChecked: boolean): void {
    this.rows = this.rows.map(row => ({
      ...row,
      [column]: this.isRowSelectable(row) ? isChecked : false
    }));

    this.changeDetectorRef.detectChanges();
    this.updateHeaderCheckboxDesign();
    this.checkboxChange.emit(this.rows);
  }

  public updateHeaderCheckboxDesign(): void {
    this.checkboxColumns.forEach(column => {
      const hasCheckedRow = this.rows.some(row =>
        row.editable ? !!row[column] && row.editable === 'ENABLED' : !!row[column]
      );
      const hasUncheckedRow = this.rows.some(row =>
        row.editable ? !row[column] && row.editable === 'ENABLED' : !row[column]
      );
      const hasPartiallyCheckedRows = hasCheckedRow && hasUncheckedRow;

      let design = CheckboxDesign.Normal;
      if (hasPartiallyCheckedRows) {
        design = CheckboxDesign.Minus;
      }

      this.checkboxHeaders[column].design = design;
      this.checkboxHeaders[column].isChecked = hasCheckedRow;
    });
  }

  public isActionHidden(row, action: GridTableAction): boolean {
    if (typeof action.hidden === 'function') {
      return action.hidden(row);
    }

    return action.hidden;
  }

  public hasVisibleAction(row): boolean {
    let hasVisibleAction = isEmpty(this.groupedActions);

    Object.entries(this.groupedActions).forEach(([key, actions]) => {
      hasVisibleAction = hasVisibleAction || actions.some(action => !this.isActionHidden(row, action));
    });

    return hasVisibleAction;
  }

  public isActionDisabled(row, action: GridTableAction): boolean {
    if (typeof action.disabled === 'function') {
      return action.disabled(row);
    }

    return !!action.disabled;
  }

  // TODO: This method is more likely used as isVisible because the boolean logic is once inverted in the sub-users
  // and also in the template. It is used as ngIf="isHidden". Use 'isActionHidden' and handle it within FCA-7630.
  public isActionHiddenWrongImpl(row, action: GridTableAction): boolean {
    if (typeof action.hidden === 'function') {
      return action.hidden(row);
    }

    return true;
  }

  private isRowSelectable(row): boolean {
    return !row.disableSelection;
  }

  public handleSelection(row: any, property: TableColumnProp, $event: DropdownOption): void {
    this.dropdownChange.emit({ ...row, [property]: $event.value });
  }

  private initCheckboxHeaders() {
    this.checkboxColumns.forEach(prop => {
      this.checkboxHeaders[prop] = {
        design: '',
        isChecked: false
      };
    });
  }

  private setSortOrder(column: any): void {
    const sortColumn = column.prop;
    const sortOrder = this.sortOrder;
    const invertOrder = this.sortedColumn === sortColumn && sortOrder === SortOrder.Asc;

    if (invertOrder) {
      this.sortOrder = SortOrder.Desc;
    } else {
      this.sortOrder = SortOrder.Asc;
      this.sortedColumn = sortColumn;
    }
  }

  private emitPageChange() {
    this.pageChange.emit(this.paging);
  }

  private setDefaultGroup(options: GridTableAction[]) {
    options = options || [];

    return options.map(option => ({ ...option, group: option.group || 'default' }));
  }

  public ngOnDestroy(): void {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }

    this.destroy$.next();
  }
}
