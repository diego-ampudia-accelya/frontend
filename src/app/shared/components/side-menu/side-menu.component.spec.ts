import { SimpleChange, SimpleChanges } from '@angular/core';

import { SideMenuItem } from './side-menu-item.model';
import { SideMenuComponent } from './side-menu.component';

describe('SideMenuComponent', () => {
  let component: SideMenuComponent;
  let firstItem: SideMenuItem;
  let secondItem: SideMenuItem;

  beforeEach(() => {
    firstItem = { title: 'item 1', isAccessible: true };
    secondItem = { title: 'item 2', isAccessible: true };

    component = new SideMenuComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when calling ngOnInit', () => {
    it('should select first menu item', () => {
      component.items = [firstItem, secondItem];

      component.ngOnInit();

      expect(component.activatedItem).toBe(firstItem);
    });

    it('should initialize when items are not provided', () => {
      component.ngOnInit();

      expect(component.items.length).toBe(0);
    });
  });

  describe('when calling selectItem', () => {
    it('should mark selected item as active item', () => {
      component.items = [firstItem, secondItem];

      component.selectItem(secondItem);

      expect(component.activatedItem).toBe(secondItem);
    });

    it('should emit select event with the selected item', () => {
      let emittedItem: { item: SideMenuItem; preventDefault: () => any };
      component.items = [firstItem, secondItem];
      component.select.subscribe(selected => (emittedItem = selected));

      component.selectItem(secondItem);

      expect(emittedItem.item).toBe(secondItem);
    });

    it('should emit select event with the selected item and should prevent default selecting of this item', () => {
      let emittedItem: { item: SideMenuItem; preventDefault: () => any };
      component.items = [firstItem, secondItem];
      component.activatedItem = firstItem;
      component.select.subscribe(selected => {
        emittedItem = selected;
        selected.preventDefault();
      });

      component.selectItem(secondItem);

      expect(emittedItem.item).toBe(secondItem);
      expect(component.activatedItem).toBe(firstItem);
    });

    it('should get group collapsed state, when getCollapsedStateOfGroup is called', () => {
      const mockedGroup = { groupOne: true };
      component.groupsCollapsedState = mockedGroup;

      expect(component.getCollapsedStateOfGroup('groupOne')).toBe(mockedGroup.groupOne);
    });

    it('should set activatedItem, when is null and onInit is called', () => {
      component.activatedItem = null;
      component.items = [firstItem, secondItem];
      component.ngOnInit();

      expect(component.activatedItem).toEqual(firstItem);
    });

    it('should not set activatedItem, when activatedItem is not null', () => {
      component.activatedItem = secondItem;
      component.items = [firstItem, secondItem];
      component.ngOnInit();

      expect(component.activatedItem).toEqual(secondItem);
    });

    it('should generate grouped options, when onChanges is called', () => {
      const groupItemOne = { title: 'item 1', isAccessible: true };
      const groupItemTwo = { title: 'item 1', isAccessible: true };

      component.items = [groupItemOne, groupItemTwo];
      component.title = 'groupedItem';
      const changes: SimpleChanges = {
        items: new SimpleChange(null, [groupItemOne, groupItemTwo], true)
      };

      component.ngOnChanges(changes);
      expect(component.groupedOptions['groupedItem']).toEqual([groupItemOne, groupItemTwo]);
      Object.values(component.groupsCollapsedState).forEach(state => {
        expect(state).toBe(false);
      });
    });
  });
});
