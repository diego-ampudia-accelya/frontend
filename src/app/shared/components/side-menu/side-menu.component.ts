import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { first, groupBy } from 'lodash';

import { SideMenuItem } from './side-menu-item.model';

@Component({
  selector: 'bspl-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() items: SideMenuItem[] = [];
  @Input() activatedItem: SideMenuItem;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() select = new EventEmitter<{ item: SideMenuItem; preventDefault: () => any }>();

  public groupedOptions;
  public groupsCollapsedState: { [key: string]: boolean };
  public collapsedIcon = 'keyboard_arrow_up';
  public expandIcon = 'keyboard_arrow_down';

  public getCollapsedStateOfGroup(group: string) {
    return this.groupsCollapsedState[group];
  }

  ngOnInit() {
    if (!this.activatedItem) {
      this.activatedItem = first(this.items);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.items) {
      this.groupedOptions = groupBy(this.items, item => item.group || this.title);
      this.setDefaultGroupsCollapsedState();
    }
  }

  public selectItem(menuItem: SideMenuItem): void {
    if (this.activatedItem === menuItem) {
      return;
    }

    let shouldActivate = true;

    this.select.emit({
      item: menuItem,
      preventDefault: () => (shouldActivate = false)
    });

    if (shouldActivate) {
      this.activatedItem = menuItem;
    }
  }

  public changeCollapsedState(groupName: string) {
    if (!this.activatedItem) {
      this.activatedItem = first(this.items);
    }

    this.groupsCollapsedState[groupName] = !this.groupsCollapsedState[groupName];
  }

  private setDefaultGroupsCollapsedState() {
    this.groupsCollapsedState = {};

    Object.keys(this.groupedOptions).forEach(groupName => {
      this.groupsCollapsedState[groupName] = false;
    });
  }
}
