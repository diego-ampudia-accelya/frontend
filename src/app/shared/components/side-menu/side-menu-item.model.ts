export interface SideMenuItem {
  title: string;
  isAccessible: boolean;
  group?: string;
}
