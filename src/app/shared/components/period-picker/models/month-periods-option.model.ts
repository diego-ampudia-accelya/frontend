import { PeriodOption } from './period-option.model';

export interface MonthPeriodsOption {
  month: Date;
  periods: PeriodOption[];
  disabled?: boolean;
}
