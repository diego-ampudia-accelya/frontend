export interface PeriodOption {
  /**
   * Describes a system specific period with format YYYYMMP.
   *
   * YYYY - year ex. 2018
   * MM - month ex 05
   * P - period between 1-9
   */
  period: string;
  /**
   * Starting period date in format MM/DD/YYYY.
   */
  dateFrom: string;
  /**
   * Ending period date in format MM/DD/YYYY.
   */
  dateTo: string;
  disabled?: boolean;
}
