import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PeriodOption } from './models/period-option.model';
import { PeriodPickerComponent } from './period-picker.component';
import { TranslatePipeMock } from '~app/test';
import { PeriodPipeMock } from '~app/test/period.mock.pipe';

describe('PeriodPickerComponent', () => {
  let component: PeriodPickerComponent;
  let fixture: ComponentFixture<PeriodPickerComponent>;
  const periods: PeriodOption[] = [
    {
      period: '2019071',
      dateFrom: '06/29/2019',
      dateTo: '07/07/2019'
    },
    {
      period: '2019072',
      dateFrom: '07/08/2019',
      dateTo: '07/15/2019'
    },
    {
      period: '2019073',
      dateFrom: '07/16/2019',
      dateTo: '07/17/2019'
    },
    {
      period: '2019074',
      dateFrom: '07/24/2019',
      dateTo: '08/01/2019'
    },
    {
      period: '2019081',
      dateFrom: '08/02/2019',
      dateTo: '08/10/2019'
    }
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PeriodPickerComponent, TranslatePipeMock, PeriodPipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [OverlayModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodPickerComponent);
    component = fixture.componentInstance;
    component.options = periods;
    component.value = null;
    component.periodInput.select = () => {};
    fixture.detectChanges();
  });

  it('should create and have correct default values', () => {
    expect(component).toBeTruthy();
    expect(component.isDisabled).toBeFalsy();
    expect(component.range).toBe(true);
    expect(component.singleMonthSelection).toBe(true);
    expect(component.yearsBack).toBe(2);
  });

  it('should call focus() on ngAfterViewInit() if autofocus is `true`', () => {
    component.autofocus = true;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).toHaveBeenCalled();
  });

  it('should NOT call focus() on ngAfterViewInit() if autofocus is `false`', () => {
    component.autofocus = false;
    spyOn(component, 'focusElement');

    component.ngAfterViewInit();

    expect(component.focusElement).not.toHaveBeenCalled();
  });

  describe('openPicker()', () => {
    it('should emit open and focus the input when open period picker', () => {
      spyOn(component.open, 'emit');
      spyOn(component, 'select');
      component.openPicker();

      expect(component.open.emit).toHaveBeenCalled();
      expect(component.select).toHaveBeenCalled();
    });

    it('should not emit open if it is already opened', () => {
      component.openPicker();
      spyOn(component.open, 'emit');
      spyOn(component, 'select');
      component.openPicker();

      expect(component.open.emit).not.toHaveBeenCalled();
      expect(component.select).not.toHaveBeenCalled();
    });

    it('should not emit open if it is disabled', () => {
      component.isDisabled = true;
      spyOn(component.open, 'emit');
      spyOn(component, 'select');
      component.openPicker();

      expect(component.open.emit).not.toHaveBeenCalled();
      expect(component.select).not.toHaveBeenCalled();
    });
  });

  describe('closePicker()', () => {
    it('should emit close when picker is opened', () => {
      component.openPicker();
      spyOn(component.close, 'emit');
      component.closePicker();

      expect(component.close.emit).toHaveBeenCalled();
    });

    it('should not emit close if picker is not opened', () => {
      spyOn(component.close, 'emit');
      component.closePicker();

      expect(component.close.emit).not.toHaveBeenCalled();
    });

    it('should close the panel and select periodTo in case of single month range picker with only periodFrom selected', () => {
      spyOn(component.close, 'emit');
      spyOn(component, 'selectPeriod');

      component.range = true;
      component.singleMonthSelection = true;
      component.openPicker();
      component.value = [periods[0]];
      component.closePicker();

      expect(component.selectPeriod).toHaveBeenCalledWith(periods[0]);
      expect(component.close.emit).toHaveBeenCalled();
    });
  });

  describe('single mode selectPeriod()', () => {
    beforeEach(() => {
      component.range = false;
    });

    it('should set selected value and close the picker pop up', () => {
      component.value = periods[3];
      component.selectPeriod(periods[0]);

      expect(component.value).toEqual(periods[0]);
      expect(component.selectedOptions[0].period).toEqual('2019071');
    });

    it('should deselect the option if is the same as the selected one', () => {
      component.value = periods[0];
      component.openPicker();
      component.selectPeriod(periods[0]);

      expect(component.value).toBe(null);
      expect(component.selectedOptions).toEqual([]);
    });

    it('should not able change selected option if it is disabled', () => {
      component.isDisabled = true;
      component.value = periods[0];

      component.selectPeriod(periods[1]);

      expect(component.value).toEqual(periods[0]);
      expect(component.selectedOptions[0].period).toEqual('2019071');
    });
  });

  describe('range mode selectPeriod()', () => {
    beforeEach(() => {
      component.range = true;
    });

    it('should be able to select periodFrom without periodTo', () => {
      component.selectPeriod(periods[1]);

      expect(component.value).toEqual([periods[1]]);
      expect(component.selectedOptions).toEqual([periods[1]]);
    });

    it('should be able to select periodFrom and periodTo', () => {
      component.selectPeriod(periods[0]);
      component.selectPeriod(periods[1]);

      expect(component.value).toEqual([periods[0], periods[1]]);
      expect(component.selectedOptions).toEqual([periods[0], periods[1]]);
    });

    it('should select the new period if you have periodFrom and periodTo and select a new period', () => {
      component.selectPeriod(periods[0]);
      component.selectPeriod(periods[1]);
      component.selectPeriod(periods[2]);

      expect(component.value).toEqual([periods[2]]);
      expect(component.selectedOptions).toEqual([periods[2]]);
    });

    it('should be able to select the same periodFrom and periodTo', () => {
      component.selectPeriod(periods[0]);
      component.selectPeriod(periods[0]);

      expect(component.value).toEqual([periods[0], periods[0]]);
      expect(component.selectedOptions).toEqual([periods[0], periods[0]]);
    });

    it('should override periodFrom if periodTo is in the same month but before periodFrom', () => {
      component.selectPeriod(periods[3]);
      component.selectPeriod(periods[2]);

      expect(component.value).toEqual([periods[2]]);
      expect(component.selectedOptions).toEqual([periods[2]]);
    });

    describe('with singleMonthSelection', () => {
      beforeEach(() => {
        component.singleMonthSelection = true;
      });

      it('should override periodFrom if periodTo is set to the previous month', () => {
        component.selectPeriod(periods[4]);
        component.selectPeriod(periods[3]);

        expect(component.value).toEqual([periods[3]]);
        expect(component.selectedOptions).toEqual([periods[3]]);
      });

      it('should override periodFrom if periodTo is set to the next month', () => {
        component.selectPeriod(periods[3]);
        component.selectPeriod(periods[4]);

        expect(component.value).toEqual([periods[4]]);
        expect(component.selectedOptions).toEqual([periods[4]]);
      });

      it('should be able to set periodTo when it is in the same month and after periodFrom', () => {
        component.selectPeriod(periods[0]);
        component.selectPeriod(periods[3]);

        expect(component.value).toEqual([periods[0], periods[3]]);
        expect(component.selectedOptions).toEqual([periods[0], periods[3]]);
      });
    });

    describe('with multiMonthSelection', () => {
      beforeEach(() => {
        component.singleMonthSelection = false;
      });

      it('should override periodFrom if periodTo is set to the previous month', () => {
        component.selectPeriod(periods[4]);
        component.selectPeriod(periods[3]);

        expect(component.value).toEqual([periods[3]]);
        expect(component.selectedOptions).toEqual([periods[3]]);
      });

      it('should set periodTo if the new period selection is in the next month', () => {
        component.selectPeriod(periods[3]);
        component.selectPeriod(periods[4]);

        expect(component.value).toEqual([periods[3], periods[4]]);
        expect(component.selectedOptions).toEqual([periods[3], periods[4]]);
      });
    });
  });

  describe('selectedOptions', () => {
    it('should return empty array if there is no option selected', () => {
      component.value = null;
      expect(component.selectedOptions).toEqual([]);
    });

    it('should return selected period option if there is selected period', () => {
      component.range = false;
      component.value = periods[0];

      expect(component.selectedOptions).toEqual([periods[0]]);
    });
  });
});
