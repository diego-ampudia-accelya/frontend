import moment from 'moment-mini';

import { MonthPeriodsOption } from '../models/month-periods-option.model';
import { PeriodOption } from '../models/period-option.model';

import { PeriodPickerStrategy } from './period-picker-strategy';

export class MonthStrategy extends PeriodPickerStrategy {
  public title = 'periodPicker.monthTitle';
  public currentDateLinkTitle = 'periodPicker.currentMonthLink';

  public get currentDate(): Date {
    return moment().startOf('year').toDate();
  }

  public get previousDate(): Date {
    return (
      moment(this.visibleDate)
        .subtract(1, 'year')
        // return end of year to fall within the [minDate, maxDate] range
        .endOf('year')
        .toDate()
    );
  }

  public get nextDate(): Date {
    return (
      moment(this.visibleDate)
        .add(1, 'year')
        // return start of year to fall within the [minDate, maxDate] range
        .startOf('year')
        .toDate()
    );
  }

  public getVisibleOptions(options: MonthPeriodsOption[]): MonthPeriodsOption[] {
    return options.filter(option => option.month.getFullYear() === this.visibleDate.getFullYear());
  }

  public disableInvalidOptions(options: MonthPeriodsOption[], currentPeriod: PeriodOption): MonthPeriodsOption[] {
    // TODO: Move disable functionality for months from picker panel.
    return options;
  }
}
