import { cloneDeep, findLast, padStart } from 'lodash';
import moment, { Moment } from 'moment-mini';
import { MonthPeriodsOption } from '../models/month-periods-option.model';
import { PeriodOption } from '../models/period-option.model';

export abstract class PeriodUtils {
  public static getPeriodValueFrom(period: string): string {
    return period ? period.split('').pop() : '';
  }

  public static getMonthFrom(period: string): string {
    return period ? period.slice(4, 6) : '';
  }

  public static getYearFrom(period: string): string {
    return period ? period.slice(0, 4) : '';
  }

  public static getYear(date: string): string {
    return date ? date.slice(11, 15) : '';
  }

  public static findPeriodFrom(periods: PeriodOption[], date: Date): PeriodOption {
    // Periods do not have time so we need to remove the time offset from the current date
    const periodDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    return periods.find(opt => {
      const startDate = PeriodUtils.parseDate(opt.dateFrom);
      const endDate = PeriodUtils.parseDate(opt.dateTo);

      return startDate <= periodDate && periodDate <= endDate;
    });
  }

  public static findCurrentPeriod(periods: PeriodOption[]): PeriodOption {
    return this.findPeriodFrom(periods, new Date());
  }

  public static findLastCompleteMonth(periods: PeriodOption[], yearsBack: number): MonthPeriodsOption {
    const startDate = moment().subtract(yearsBack, 'years').startOf('month').toDate();
    const monthOptions = PeriodUtils.generateMonthPeriodsOptions(startDate.getFullYear(), periods);
    const currentPeriod = PeriodUtils.findCurrentPeriod(periods);

    let lastCompleteMonth: MonthPeriodsOption = null;
    if (currentPeriod) {
      const currentPeriodDate = PeriodUtils.getDateFrom(currentPeriod.period);
      lastCompleteMonth = findLast(
        monthOptions,
        option => option.periods.length > 0 && option.month < currentPeriodDate
      );
    }

    return lastCompleteMonth;
  }

  public static getDateFrom(period: string): Date {
    const year = Number(PeriodUtils.getYearFrom(period));
    const month = Number(PeriodUtils.getMonthFrom(period));
    let date = null;
    if (year && month) {
      date = new Date(year, month - 1);
    }

    return date;
  }

  public static getDaysInPeriod(option: PeriodOption): number {
    const oneDay = 24 * 60 * 60 * 1000; // hours * minutes * seconds * milliseconds
    const startDate = PeriodUtils.parseDate(option.dateFrom);
    const endDate = PeriodUtils.parseDate(option.dateTo);

    return Math.round(Math.abs((endDate.getTime() - startDate.getTime()) / oneDay)) + 1;
  }

  public static getPeriodsInMonth(month: Date, periods: PeriodOption[]): PeriodOption[] {
    const period = `${month.getFullYear()}${padStart((month.getMonth() + 1).toString(), 2, '0')}`;
    const periodsInMonth = periods.filter(opt => opt.period.startsWith(period));

    return periodsInMonth || [];
  }

  public static parseDate(date: string): Date {
    return moment(date, 'MM/DD/YYYY').toDate();
  }

  public static isBetweenRange(period: PeriodOption, start: PeriodOption, end: PeriodOption): boolean {
    let isBetweenRange = false;
    if (start && end) {
      const rangeStart = PeriodUtils.parseDate(start.dateFrom);
      const rangeEnd = PeriodUtils.parseDate(end.dateTo);
      const periodStart = PeriodUtils.parseDate(period.dateFrom);
      const periodEnd = PeriodUtils.parseDate(period.dateTo);

      isBetweenRange = rangeStart < periodStart && periodEnd < rangeEnd;
    }

    return isBetweenRange;
  }

  public static areForSameMonth(first: PeriodOption, second: PeriodOption): boolean {
    const hasValues = first && second;

    return (
      hasValues && PeriodUtils.getDateFrom(first.period).getTime() === PeriodUtils.getDateFrom(second.period).getTime()
    );
  }

  public static sort(options: PeriodOption[], ascending = true): PeriodOption[] {
    let sortedOptions: PeriodOption[] = null;
    if (options) {
      const optionsClone = cloneDeep(options);
      optionsClone.sort((first, second) => {
        const firstPeriod = Number(first.period);
        const secondPeriod = Number(second.period);

        return ascending ? firstPeriod - secondPeriod : secondPeriod - firstPeriod;
      });

      sortedOptions = optionsClone;
    }

    return sortedOptions;
  }

  public static generateMonthPeriodsOptions(startYear: number, options: PeriodOption[]): MonthPeriodsOption[] {
    const res: MonthPeriodsOption[] = [];
    const currMonth: Moment = moment([startYear]);
    const endMonth: Moment = moment().startOf('year').add(1, 'year');

    while (currMonth < endMonth) {
      const monthPeriods: PeriodOption[] = PeriodUtils.getPeriodsInMonth(currMonth.toDate(), options);
      res.push({
        month: currMonth.toDate(),
        periods: monthPeriods
      });

      currMonth.add(1, 'month');
    }

    return res;
  }

  public static getPeriodsBetweenDates(periods: PeriodOption[], dateFrom: string, dateTo: string): PeriodOption[] {
    return (
      periods &&
      periods.filter(
        period =>
          PeriodUtils.parseDate(dateFrom) <= PeriodUtils.parseDate(period.dateFrom) &&
          PeriodUtils.parseDate(dateTo) >= PeriodUtils.parseDate(period.dateTo)
      )
    );
  }
}
