import { PeriodUtils } from '../core/period-utils';
import { PeriodOption } from '../models/period-option.model';

const periods: PeriodOption[] = [
  {
    period: '2019071',
    dateFrom: '06/29/2019',
    dateTo: '07/07/2019'
  },
  {
    period: '2019072',
    dateFrom: '07/08/2019',
    dateTo: '07/15/2019'
  },
  {
    period: '2019073',
    dateFrom: '07/16/2019',
    dateTo: '07/17/2019'
  },
  {
    period: '2019074',
    dateFrom: '07/24/2019',
    dateTo: '08/01/2019'
  },
  {
    period: '2019081',
    dateFrom: '08/02/2019',
    dateTo: '08/10/2019'
  }
];

describe('PeriodUtils', () => {
  describe('getPeriodValueFrom()', () => {
    it('should extract period value from period', () => {
      const result = PeriodUtils.getPeriodValueFrom(periods[0].period);
      const expected = '1';

      expect(result).toBe(expected);
    });

    it('should return empty string if there is no period', () => {
      const result = PeriodUtils.getPeriodValueFrom(undefined);

      expect(result).toBe('');
    });
  });

  describe('getMonthFrom()', () => {
    it('should extract month from period', () => {
      const result = PeriodUtils.getMonthFrom(periods[0].period);
      const expected = '07';

      expect(result).toBe(expected);
    });

    it('should return empty string if there is no period', () => {
      const result = PeriodUtils.getMonthFrom(undefined);

      expect(result).toBe('');
    });
  });

  describe('getYearFrom()', () => {
    it('should extract year from period', () => {
      const result = PeriodUtils.getYearFrom(periods[0].period);
      const expected = '2019';

      expect(result).toBe(expected);
    });

    it('should return empty string if there is no period', () => {
      const result = PeriodUtils.getYearFrom(undefined);

      expect(result).toBe('');
    });
  });

  describe('findCurrentPeriod()', () => {});

  describe('getDateFrom()', () => {
    it('should extract a valid date from a period', () => {
      const expected = new Date(2019, 0);
      const result = PeriodUtils.getDateFrom('2019011');

      expect(result).toEqual(expected);
    });

    it('should return null if empty period is provided', () => {
      const result = PeriodUtils.getDateFrom('');

      expect(result).toBe(null);
    });
  });

  describe('getDaysInPeriod()', () => {
    it('should properly calculate how many days are in a period', () => {
      const result = PeriodUtils.getDaysInPeriod(periods[0]);

      expect(result).toBe(9);
    });
  });

  describe('getPeriodsInMonth()', () => {
    it('should return all periods based on a given month', () => {
      const result = PeriodUtils.getPeriodsInMonth(new Date(2019, 7), periods);
      const expected: PeriodOption[] = [
        {
          period: '2019081',
          dateFrom: '08/02/2019',
          dateTo: '08/10/2019'
        }
      ];

      expect(result).toEqual(expected);
    });
  });

  describe('get periods between dates', () => {
    it('should return null when periods are not passed', () => {
      expect(PeriodUtils.getPeriodsBetweenDates(null, null, null)).toBeNull();
    });

    it('should return periods between passed dates', () => {
      expect(PeriodUtils.getPeriodsBetweenDates(periods, '07/01/2019', '07/31/2019')).toEqual([
        {
          period: '2019072',
          dateFrom: '07/08/2019',
          dateTo: '07/15/2019'
        },
        {
          period: '2019073',
          dateFrom: '07/16/2019',
          dateTo: '07/17/2019'
        }
      ]);
    });
  });
});
