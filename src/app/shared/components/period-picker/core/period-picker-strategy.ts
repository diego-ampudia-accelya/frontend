import { MonthPeriodsOption } from '../models/month-periods-option.model';
import { PeriodOption } from '../models/period-option.model';

export abstract class PeriodPickerStrategy {
  public visibleDate: Date;

  public abstract title: string;
  public abstract currentDateLinkTitle: string;

  public abstract get currentDate(): Date;

  public abstract get previousDate(): Date;

  public abstract get nextDate(): Date;

  public abstract getVisibleOptions(options: MonthPeriodsOption[]): MonthPeriodsOption[];

  public abstract disableInvalidOptions(
    options: MonthPeriodsOption[],
    currentPeriod: PeriodOption
  ): MonthPeriodsOption[];
}
