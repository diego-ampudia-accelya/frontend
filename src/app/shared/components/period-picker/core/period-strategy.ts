import { cloneDeep } from 'lodash';
import moment from 'moment-mini';

import { MonthPeriodsOption } from '../models/month-periods-option.model';
import { PeriodOption } from '../models/period-option.model';

import { PeriodPickerStrategy } from './period-picker-strategy';

export class PeriodStrategy extends PeriodPickerStrategy {
  public title = 'periodPicker.periodTitle';
  public currentDateLinkTitle = 'periodPicker.currentPeriodLink';

  public get currentDate(): Date {
    return moment().startOf('month').toDate();
  }

  public get previousDate(): Date {
    return moment(this.visibleDate).subtract(1, 'month').toDate();
  }

  public get nextDate(): Date {
    return moment(this.visibleDate).add(1, 'month').toDate();
  }

  public getVisibleOptions(options: MonthPeriodsOption[]): MonthPeriodsOption[] {
    return options.filter(
      option =>
        option.month.getFullYear() === this.visibleDate.getFullYear() &&
        option.month.getMonth() === this.visibleDate.getMonth()
    );
  }

  public disableInvalidOptions(options: MonthPeriodsOption[], currentPeriod: PeriodOption): MonthPeriodsOption[] {
    const result = cloneDeep(options);

    result.forEach(monthOption => {
      monthOption.periods.forEach(
        periodOption => (periodOption.disabled = currentPeriod != null && currentPeriod.period < periodOption.period)
      );
    });

    return result;
  }
}
