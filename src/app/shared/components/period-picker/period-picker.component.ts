/* eslint-disable @angular-eslint/no-output-native */
import {
  CdkOverlayOrigin,
  ConnectedPosition,
  Overlay,
  OverlayConfig,
  OverlayRef,
  RepositionScrollStrategy,
  ScrollDispatcher,
  ViewportRuler
} from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { isArray } from 'lodash';

import { InputComponent } from '../input/input.component';

import { PeriodUtils } from './core/period-utils';
import { PeriodOption } from './models/period-option.model';
import { PeriodPickerPanelComponent } from './period-picker-panel/period-picker-panel.component';
import { AutoFocusableControl } from '~app/shared/base/auto-focusable-control';

@Component({
  selector: 'bspl-period-picker',
  templateUrl: './period-picker.component.html',
  styleUrls: ['./period-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: PeriodPickerComponent,
      multi: true
    }
  ]
})
export class PeriodPickerComponent
  extends AutoFocusableControl
  implements ControlValueAccessor, OnChanges, OnInit, AfterViewInit, OnDestroy
{
  private _value: PeriodOption | PeriodOption[];
  @Input() public get value(): PeriodOption | PeriodOption[] {
    return this._value;
  }
  public set value(value: PeriodOption | PeriodOption[]) {
    if (this.range && value && !isArray(value)) {
      throw new Error('The value should be an array in case of range selection.');
    }

    this._value = value;

    this.updateSelectedOptions();
  }

  @Input() options: PeriodOption[];
  @Input() label: string;
  @Input() isDisabled: boolean;
  @Input() range = true;
  @Input() singleMonthSelection = true;
  /** Controlling how many years you can navigate in the past. */
  @Input() yearsBack = 2;
  @Input() isMonthPicker = false;
  /** Handles if month's max date is restricted by the end of the current period */
  @Input() isMonthPeriodRestricted = true;

  @Output() open = new EventEmitter();
  @Output() close = new EventEmitter();

  @ViewChild(CdkOverlayOrigin, { static: true }) overlayOrigin: CdkOverlayOrigin;
  @ViewChild('periodPickerPopUp', { static: true }) periodPickerPopUp: TemplateRef<any>;
  @ViewChild('periodInput', { static: true }) periodInput: InputComponent;
  @ViewChild(PeriodPickerPanelComponent) periodPickerPanelComponent: PeriodPickerPanelComponent;

  public get periodFrom(): PeriodOption {
    return this.selectedOptions[0];
  }

  public get periodTo(): PeriodOption {
    return this.selectedOptions[1];
  }

  private _selectedOptions: PeriodOption[];
  public get selectedOptions(): PeriodOption[] {
    return this._selectedOptions;
  }

  private isOverlayOpened = false;
  private overlayRef: OverlayRef;
  private pickerPortal: TemplatePortal;

  private onChange = (value: PeriodOption | PeriodOption[]) => {
    //This is intentional
  };
  private onTouched = () => {
    //This is intentional
  };

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    private scrollDispatcher: ScrollDispatcher,
    private viewPortRuler: ViewportRuler,
    private ngZone: NgZone,
    private cd: ChangeDetectorRef
  ) {
    super();
  }

  public ngOnChanges(simpleChanges: SimpleChanges): void {
    if (simpleChanges.options) {
      this.updateSelectedOptions();
    }
  }

  public ngOnInit(): void {
    const config = this.getOverlayConfig();
    this.overlayRef = this.overlay.create(config);
    this.pickerPortal = new TemplatePortal(this.periodPickerPopUp, this.viewContainerRef);

    if (this.isMonthPicker) {
      this.range = true;
      this.singleMonthSelection = false;
    }
  }

  public ngAfterViewInit(): void {
    this.overlayRef.backdropClick().subscribe(() => this.closePicker());

    super.ngAfterViewInit();
  }

  public ngOnDestroy(): void {
    this.overlayRef.dispose();
  }

  public writeValue(value: PeriodOption | PeriodOption[]): void {
    this.value = value;
    this.cd.markForCheck();
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public focusElement(): void {
    if (this.isDisabled) {
      return;
    }

    this.periodInput.focusElement();
  }

  public openPicker(): void {
    if (this.isOverlayOpened || this.isDisabled) {
      return;
    }

    this.overlayRef.attach(this.pickerPortal);
    this.isOverlayOpened = true;

    this.select();
    this.onTouched();

    this.open.emit();
  }

  public closePicker(): void {
    if (!this.isOverlayOpened) {
      return;
    }

    const shouldPreselectPeriodTo = this.range && this.singleMonthSelection && this.periodFrom && !this.periodTo;
    if (shouldPreselectPeriodTo) {
      this.selectPeriod(this.periodFrom);
    }

    this.overlayRef.detach();
    this.isOverlayOpened = false;

    this.close.emit();
  }

  public selectPeriod(option: PeriodOption): void {
    if (this.isDisabled) {
      return;
    }

    if (this.range) {
      this.handleRangeModeSelection(option);
    } else {
      this.handleSingleModeSelection(option);
    }

    // TODO: Check why its needed and can we handle it in different way.
    this.cd.detectChanges();
    this.select();
  }

  public clearSelectedPeriod(): void {
    if (this.isOverlayOpened) {
      this.value = null;
      this.onChange(this.value);
    }
  }

  public select(): void {
    setTimeout(() => {
      this.periodInput.select();
    });
  }

  private getOverlayConfig(): OverlayConfig {
    const fromBottom: ConnectedPosition = {
      originX: 'start',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'top',
      offsetY: 5
    };
    const fromTop: ConnectedPosition = {
      originX: 'start',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'bottom',
      offsetY: -5
    };

    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.overlayOrigin.elementRef)
      .withPositions([fromBottom, fromTop])
      .withFlexibleDimensions(false)
      .withPush(false);

    const scrollStrategy = new RepositionScrollStrategy(this.scrollDispatcher, this.viewPortRuler, this.ngZone);

    return new OverlayConfig({
      positionStrategy,
      hasBackdrop: true,
      backdropClass: 'cdk-overlay-transparent-backdrop',
      scrollStrategy
    });
  }

  private handleSingleModeSelection(selected: PeriodOption): void {
    const willSelectTheSame = this.selectedOptions.some(o => o.period === selected.period);
    if (willSelectTheSame) {
      this.clearSelectedPeriod();
    } else {
      this.value = selected;
      this.onChange(this.value);
      this.closePicker();
    }
  }

  private handleRangeModeSelection(selected: PeriodOption): void {
    const onlyFromSelected = this.periodFrom && !this.periodTo;
    const isBeforeSelected =
      this.periodFrom && PeriodUtils.parseDate(selected.dateFrom) < PeriodUtils.parseDate(this.periodFrom.dateFrom);
    const areForSameMonth = PeriodUtils.areForSameMonth(this.periodFrom, selected);
    const canSelectMultiMonth = !this.singleMonthSelection || areForSameMonth;

    if (onlyFromSelected && !isBeforeSelected && canSelectMultiMonth) {
      this._selectedOptions = [...this.selectedOptions, { ...selected }];
      this.closePicker();
    } else {
      this._selectedOptions = [{ ...selected }];
    }

    this._value = [...this.selectedOptions];
    this.onChange(this.selectedOptions);
  }

  private updateSelectedOptions() {
    let selected: PeriodOption[] = [];
    if (this.value != null && this.options != null) {
      const values = (this.range ? this.value : [this.value]) as PeriodOption[];
      selected = values.filter(v => !!v).map(v => this.options.find(option => option.period === v.period));
    }

    this._selectedOptions = selected;
  }
}
