import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import moment from 'moment-mini';

import { SelectComponent } from '../../select/select.component';
import { MonthStrategy } from '../core/month-strategy';
import { PeriodPickerStrategy } from '../core/period-picker-strategy';
import { PeriodStrategy } from '../core/period-strategy';
import { PeriodUtils } from '../core/period-utils';
import { MonthPeriodsOption } from '../models/month-periods-option.model';
import { PeriodOption } from '../models/period-option.model';
import { generateYearOptions } from '~app/shared/utils';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { SelectMode } from '~app/shared/enums';

@Component({
  selector: 'bspl-period-picker-panel',
  templateUrl: './period-picker-panel.component.html',
  styleUrls: ['./period-picker-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PeriodPickerPanelComponent implements OnInit, OnChanges {
  @Input() values: PeriodOption[];
  @Input() options: PeriodOption[];
  @Input() range = false;
  @Input() singleMonthSelection = false;
  /** Controlling how many years you can navigate in the past. */
  @Input() yearsBack: number;
  @Input() isMonthPicker = false;
  /** Handles if month's max date is restricted by the end of the current period */
  @Input() isMonthPeriodRestricted = true;

  @Output() selectPeriod = new EventEmitter<PeriodOption>();
  @Output() navigation = new EventEmitter();

  @ViewChildren('periodElement') periodElements: QueryList<ElementRef>;
  @ViewChild('monthNavigator') monthNavigator: SelectComponent;

  public dateFormat = 'dd/MM';
  public hoveredOption: PeriodOption = null;
  public selectMode = SelectMode;

  public monthOptions: DropdownOption[];
  public yearOptions: DropdownOption[];

  public get periodFrom(): PeriodOption {
    return this.values && this.values[0];
  }

  public get periodTo(): PeriodOption {
    return this.values && this.values[1];
  }

  private _periodPickerStrategy: PeriodPickerStrategy;
  public get periodPickerStrategy(): PeriodPickerStrategy {
    return this._periodPickerStrategy;
  }

  private _currentPeriod: PeriodOption;
  public get currentPeriod(): PeriodOption {
    return this._currentPeriod;
  }

  private _monthPeriodsOptions: MonthPeriodsOption[];
  public get monthPeriodsOptions(): MonthPeriodsOption[] {
    return this._monthPeriodsOptions || [];
  }

  private _visibleOptions: MonthPeriodsOption[];
  public get visibleOptions(): MonthPeriodsOption[] {
    return this._visibleOptions;
  }

  private _minDate = new Date(1970, 0, 1);
  public get minDate(): Date {
    return this._minDate;
  }

  private _maxDate: Date;
  public get maxDate(): Date {
    return this._maxDate;
  }

  constructor(private translationService: L10nTranslationService) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.isMonthPicker && changes.isMonthPicker.firstChange) {
      this._periodPickerStrategy = this.isMonthPicker ? new MonthStrategy() : new PeriodStrategy();
      this.periodPickerStrategy.visibleDate = this.periodPickerStrategy.currentDate;
    }

    if (changes.yearsBack) {
      this._minDate = new Date(1970, 0, 1);
      if (changes.yearsBack.currentValue != null) {
        this._minDate = moment().subtract(this.yearsBack, 'years').startOf('month').toDate();
      }

      this.monthOptions = this.generateMonthOptions();
      this.yearOptions = this.generateYearOptions();
    }

    if (changes.options && changes.options.currentValue) {
      this._currentPeriod = PeriodUtils.findCurrentPeriod(changes.options.currentValue);

      this.options = PeriodUtils.sort(this.options);

      const monthsOptions = PeriodUtils.generateMonthPeriodsOptions(this.minDate.getFullYear(), this.options);
      this._monthPeriodsOptions = this.periodPickerStrategy.disableInvalidOptions(monthsOptions, this.currentPeriod);

      this._visibleOptions = this.periodPickerStrategy.getVisibleOptions(this.monthPeriodsOptions);
      this.updateDropdownNavigatorsState();
    }
  }

  public ngOnInit(): void {
    const currentPeriodMonth = this.currentPeriod
      ? PeriodUtils.getDateFrom(this.currentPeriod.period)
      : this.periodPickerStrategy.currentDate;

    this._maxDate = currentPeriodMonth;

    const initialDate = this.periodFrom ? PeriodUtils.getDateFrom(this.periodFrom.period) : currentPeriodMonth;
    this.navigateTo(initialDate);
  }

  public onSelectPeriod(option: PeriodOption): void {
    if (option.disabled) {
      return;
    }

    this.selectPeriod.emit(option);
  }

  public onSelectMonth(monthOption: MonthPeriodsOption): void {
    if (!this.isMonthOptionSelectable(monthOption)) {
      return;
    }

    const from = monthOption.periods[0];
    const to = monthOption.periods[monthOption.periods.length - 1];

    this.selectPeriod.emit(from);
    this.selectPeriod.emit(to);
  }

  /**
   * Check if a month is selectable.
   * A month is selectable when the period of its first period option and the period of its
   * last period option fall within the range [minDate, maxDate)
   *
   * @param monthOption Month Option to select
   */
  public isMonthOptionSelectable(monthOption: MonthPeriodsOption): boolean {
    if (monthOption.periods.length === 0) {
      return false;
    }

    const monthStartPeriod = PeriodUtils.getDateFrom(monthOption.periods[0].period);
    const monthEndPeriod = PeriodUtils.getDateFrom(monthOption.periods[monthOption.periods.length - 1].period);

    return (
      this.minDate <= monthStartPeriod &&
      ((this.isMonthPeriodRestricted && monthEndPeriod < this.maxDate) ||
        (!this.isMonthPeriodRestricted && monthEndPeriod <= this.maxDate))
    );
  }

  public navigateToPrevious(): void {
    this.navigateTo(this.periodPickerStrategy.previousDate);
  }

  public navigateToNext(): void {
    this.navigateTo(this.periodPickerStrategy.nextDate);
  }

  public navigateTo(date: Date): void {
    if (!this.canNavigateTo(date)) {
      // If we cannot navigate, means we are trying to open a disabled month when changing the year.
      // We force the navigation to the closest available date
      date = date <= this.minDate ? this.minDate : this.maxDate;
    }

    this.periodPickerStrategy.visibleDate = date;
    this._visibleOptions = this.periodPickerStrategy.getVisibleOptions(this.monthPeriodsOptions);
    this.updateDropdownNavigatorsState();

    this.navigation.emit();
  }

  public navigateToCurrentPeriod(): void {
    let currentPeriodMonth = this.periodPickerStrategy.currentDate;
    if (this.currentPeriod) {
      currentPeriodMonth = PeriodUtils.getDateFrom(this.currentPeriod.period) || this.periodPickerStrategy.currentDate;
    }

    this.navigateTo(currentPeriodMonth);
  }

  public getDaysInPeriodPlaceholder(option: PeriodOption): string {
    const daysInPeriod = PeriodUtils.getDaysInPeriod(option);
    const daysPlaceholderKey = daysInPeriod === 1 ? 'periodPicker.day' : 'periodPicker.days';

    return `${daysInPeriod} ${this.translationService.translate(daysPlaceholderKey)}`;
  }

  public getPeriodPlaceholder(period: string): string {
    return 'P' + PeriodUtils.getPeriodValueFrom(period);
  }

  public isBetweenSelectedRange(option: PeriodOption): boolean {
    const hasFullSelection = this.periodFrom && this.periodTo;
    if (!this.range || !hasFullSelection) {
      return false;
    }

    return PeriodUtils.isBetweenRange(option, this.periodFrom, this.periodTo);
  }

  public isOptionSelected(option: PeriodOption): boolean {
    return this.values && option && this.values.some(value => value && value.period === option.period);
  }

  public isCurrentMonthPeriod(monthPeriods: MonthPeriodsOption): boolean {
    return this.currentPeriod && monthPeriods.periods.some(p => p.period === this.currentPeriod.period);
  }

  public isMonthOptionSelected(monthOption: MonthPeriodsOption): boolean {
    return this.periodTo && monthOption && monthOption.periods.some(p => p.period === this.periodTo.period);
  }

  public isWithinHoverRange(option: PeriodOption): boolean {
    if (!this.hoveredOption || this.hoveredOption.disabled || !this.range) {
      return false;
    }

    const onlyFromSelected = this.periodFrom && !this.periodTo;
    const isBetweenRange = PeriodUtils.isBetweenRange(option, this.periodFrom, this.hoveredOption);
    const areForSameMonth = PeriodUtils.areForSameMonth(this.hoveredOption, this.periodFrom);

    return onlyFromSelected && isBetweenRange && (!this.singleMonthSelection || areForSameMonth);
  }

  public canNavigateToPrevious(): boolean {
    return this.canNavigateTo(this.periodPickerStrategy.previousDate);
  }

  public canNavigateToNext(): boolean {
    return this.canNavigateTo(this.periodPickerStrategy.nextDate);
  }

  public onDateDropdownNavigatorChange(year: string, month: string): void {
    this.navigateTo(new Date(Number(year), month ? Number(month) : new Date().getMonth()));
  }

  public areOptionsLoading(): boolean {
    return this.options == null;
  }

  private canNavigateTo(date: Date): boolean {
    return this.minDate <= date && date <= this.maxDate;
  }

  private generateMonthOptions(): DropdownOption[] {
    return moment.months().map((month, index) => ({ label: month, value: index.toString() }));
  }

  private generateYearOptions(): DropdownOption[] {
    return generateYearOptions(this.minDate);
  }

  private updateDropdownNavigatorsState(): void {
    this.monthOptions = this.monthOptions.map(o => {
      const disabled = !this.canNavigateTo(new Date(this.periodPickerStrategy.visibleDate.getFullYear(), o.value));

      return { ...o, disabled };
    });
  }
}
