import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { L10nTranslationService } from 'angular-l10n';

import { MonthPeriodsOption } from '../models/month-periods-option.model';
import { PeriodOption } from '../models/period-option.model';
import { PeriodPickerPanelComponent } from '../period-picker-panel/period-picker-panel.component';
import { TranslatePipeMock } from '~app/test';

const periods: PeriodOption[] = [
  {
    period: '2019071',
    dateFrom: '06/29/2019',
    dateTo: '07/07/2019'
  },
  {
    period: '2019072',
    dateFrom: '07/08/2019',
    dateTo: '07/15/2019'
  },
  {
    period: '2019073',
    dateFrom: '07/16/2019',
    dateTo: '07/17/2019'
  },
  {
    period: '2019074',
    dateFrom: '07/24/2019',
    dateTo: '08/01/2019'
  },
  {
    period: '2019081',
    dateFrom: '08/02/2019',
    dateTo: '08/10/2019'
  },
  {
    period: '2019082',
    dateFrom: '08/11/2019',
    dateTo: '08/11/2019'
  }
];

describe('PeriodPickerPanelComponent', () => {
  let component: PeriodPickerPanelComponent;
  let fixture: ComponentFixture<PeriodPickerPanelComponent>;

  const translationServiceSpy = jasmine.createSpyObj('L10nTranslationService', ['translate']);
  translationServiceSpy.translate
    .withArgs('periodPicker.day')
    .and.returnValue('day')
    .withArgs('periodPicker.days')
    .and.returnValue('days');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PeriodPickerPanelComponent, TranslatePipeMock],
      providers: [{ provide: L10nTranslationService, useValue: translationServiceSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodPickerPanelComponent);
    component = fixture.componentInstance;
    component.options = periods;
    const currentPeriod: PeriodOption = {
      period: '2019113',
      dateFrom: '11/21/2019',
      dateTo: '11/25/2019'
    };
    spyOnProperty(component, 'currentPeriod').and.returnValue(currentPeriod);
    component.ngOnChanges({
      yearsBack: new SimpleChange(undefined, undefined, true),
      options: new SimpleChange(undefined, periods, true),
      isMonthPicker: new SimpleChange(undefined, false, true)
    });
    fixture.detectChanges();
  });

  it('should create and have proper default values', () => {
    expect(component).toBeTruthy();

    expect(component.values).toBe(undefined);
    expect(component.range).toBe(false);
    expect(component.singleMonthSelection).toBe(false);
    expect(component.dateFormat).toBe('dd/MM');
    expect(component.yearsBack).toBe(undefined);
  });

  it('should have loading state if the options are null or undefined', () => {
    component.options = null;
    expect(component.areOptionsLoading()).toBe(true);

    component.options = undefined;
    expect(component.areOptionsLoading()).toBe(true);
  });

  it('should not have loading state if the options are available', () => {
    component.options = [];
    expect(component.areOptionsLoading()).toBe(false);
  });

  it('should return proper message when having multiple days in a period', () => {
    const result = component.getDaysInPeriodPlaceholder(periods[0]);
    const expected = '9 days';

    expect(result).toBe(expected);
  });

  it('should return proper message when having 1 day in a period', () => {
    const result = component.getDaysInPeriodPlaceholder(periods[5]);
    const expected = '1 day';

    expect(result).toBe(expected);
  });

  it('should not make a selection if the option is disabled', () => {
    spyOn(component.selectPeriod, 'emit');
    const option = { disabled: true } as PeriodOption;
    component.onSelectPeriod(option);

    expect(component.selectPeriod.emit).not.toHaveBeenCalled();
  });

  it('should make a selection if the option is enabled', () => {
    spyOn(component.selectPeriod, 'emit');
    const option = { disabled: false } as PeriodOption;
    component.onSelectPeriod(option);

    expect(component.selectPeriod.emit).toHaveBeenCalledWith(option);
  });

  it('should navigate to minDate if date is below range', () => {
    const minDate = new Date(2010, 12);
    spyOnProperty(component, 'minDate').and.returnValue(minDate);

    spyOn(component.periodPickerStrategy, 'visibleDate');
    component.navigateTo(new Date(2007, 11, 10));

    expect(component.periodPickerStrategy.visibleDate).toBe(minDate);
  });

  it('should navigate to maxDate if date exceeds range', () => {
    const maxDate = new Date(2019, 11);
    spyOnProperty(component, 'maxDate').and.returnValue(maxDate);

    spyOn(component.periodPickerStrategy, 'visibleDate');
    component.navigateTo(new Date(2019, 11, 10));

    expect(component.periodPickerStrategy.visibleDate).toBe(maxDate);
  });

  it('should properly format the period placeholder', () => {
    const actual = component.getPeriodPlaceholder('2019111');
    expect(actual).toEqual('P1');
  });

  it('isOptionSelected() should return true of an option is selected', () => {
    const period = { period: '2019101' } as PeriodOption;
    component.values = [period];
    const actual = component.isOptionSelected(period);

    expect(actual).toEqual(true);
  });

  it('should return the proper periodFrom and periodTo', () => {
    const periodFrom = { period: '2019072' } as PeriodOption;
    const periodTo = { period: '2019073' } as PeriodOption;
    component.values = [periodFrom, periodTo];

    expect(component.periodFrom).toEqual(periodFrom);
    expect(component.periodTo).toEqual(periodTo);
  });

  it('should navigate to the current period', () => {
    spyOn(component, 'navigateTo');
    component.navigateToCurrentPeriod();

    expect(component.navigateTo).toHaveBeenCalledWith(new Date(2019, 10));
  });

  describe('navigateTo()', () => {
    it('should update the current month and the period options based on that date', () => {
      component.navigateTo(new Date(2019, 7));
      const expectedOptions: PeriodOption[] = [
        {
          period: '2019081',
          dateFrom: '08/02/2019',
          dateTo: '08/10/2019',
          disabled: false
        },
        {
          period: '2019082',
          dateFrom: '08/11/2019',
          dateTo: '08/11/2019',
          disabled: false
        }
      ];

      const actual = component.periodPickerStrategy.getVisibleOptions(component.monthPeriodsOptions)[0].periods;

      expect(component.periodPickerStrategy.visibleDate).toEqual(new Date(2019, 7));
      expect(actual).toEqual(expectedOptions);
    });
  });

  describe('isMonthOptionSelectable()', () => {
    it('should return false if there are no periods', () => {
      const option = {
        periods: []
      } as MonthPeriodsOption;

      const result = component.isMonthOptionSelectable(option);
      expect(result).toBe(false);
    });

    it('should return true if the option is in the valid range', () => {
      const option = {
        periods: [
          {
            dateFrom: '10/10/2019'
          },
          {
            dateTo: '10/13/2019'
          }
        ]
      } as MonthPeriodsOption;

      const result = component.isMonthOptionSelectable(option);
      expect(result).toBe(true);
    });

    it('should return true if the 2 years back period starts in its previous month', () => {
      spyOnProperty(component, 'minDate').and.returnValue(new Date(2017, 11));
      spyOnProperty(component, 'maxDate').and.returnValue(new Date(2019, 11));

      const monthOption: MonthPeriodsOption = {
        month: new Date(2017, 11),
        periods: [
          {
            period: '2017121',
            dateFrom: '11/20/2017',
            dateTo: '11/29/2017'
          }
        ]
      };
      const actual = component.isMonthOptionSelectable(monthOption);
      expect(actual).toBe(true);
    });

    it('should return false if the period is within the current month', () => {
      spyOnProperty(component, 'minDate').and.returnValue(new Date(2017, 11));
      spyOnProperty(component, 'maxDate').and.returnValue(new Date(2019, 11));

      const monthOption: MonthPeriodsOption = {
        month: new Date(2019, 11),
        periods: [
          {
            period: '2019121',
            dateFrom: '11/28/2019',
            dateTo: '12/10/2019'
          }
        ]
      };
      const actual = component.isMonthOptionSelectable(monthOption);
      expect(actual).toBe(false);
    });
  });

  describe('isWithinHoverRange()', () => {
    it('should return false if the hovered option is disabled', () => {
      component.hoveredOption = { disabled: true } as PeriodOption;
      const actual = component.isWithinHoverRange({} as PeriodOption);

      expect(actual).toBe(false);
    });

    it('should return false if there is no hovered option', () => {
      component.hoveredOption = null;
      const actual = component.isWithinHoverRange({} as PeriodOption);

      expect(actual).toBe(false);
    });

    it('should return false if the panel is not in range mode', () => {
      component.hoveredOption = { disabled: false } as PeriodOption;
      component.range = false;
      const actual = component.isWithinHoverRange({} as PeriodOption);

      expect(actual).toBe(false);
    });

    it('should return true when all conditions are met', () => {
      component.values = [periods[0]];
      component.hoveredOption = periods[2];
      component.range = true;

      const actual = component.isWithinHoverRange(periods[1]);

      expect(actual).toBe(true);
    });
  });

  describe('onSelectMonth()', () => {
    it('should not select an option if it is not selectable', () => {
      spyOn(component, 'isMonthOptionSelectable').and.returnValue(false);
      spyOn(component.selectPeriod, 'emit');

      component.onSelectMonth({} as MonthPeriodsOption);

      expect(component.selectPeriod.emit).not.toHaveBeenCalled();
    });
  });
});
