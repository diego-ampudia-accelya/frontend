import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { chain, intersection } from 'lodash';

import { ButtonDesign } from '../buttons/enums/buttonDesign';
import { AppliedFilter, FilterFormatter } from '../list-view';
import { listViewConfig } from '../list-view/list-view/constants.config';

@Component({
  selector: 'bspl-filter-template',
  templateUrl: './filter-template.component.html',
  styleUrls: ['./filter-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterTemplateComponent implements OnChanges {
  @ViewChild('actionBar', { static: true })
  private actionBarElement: ElementRef<HTMLElement>;

  @Input() filters: any;
  @Input() predefinedFilters: { [key: string]: any };
  @Input() filterFormatter: FilterFormatter;
  @Input() filterTitle: string;
  @Input() isFilterValid = true;
  @Input() isFilterEnabled = true;
  @Input() isFilterExpanded: boolean;
  @Input() isResetEnabled = true;

  @Output() searchEvent = new EventEmitter();
  @Output() resetEvent = new EventEmitter();
  @Output() removeTagEvent = new EventEmitter<AppliedFilter>();
  @Output() filterButtonClicked = new EventEmitter<boolean>();

  public appliedFilters: Array<AppliedFilter> = [];

  public buttonDesign = ButtonDesign;

  private clearIcon = 'mi md-18 clear';
  private filterIcon = 'mi md-18 filter_list';

  public labels = listViewConfig.defaultLabels;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.filters) {
      this.appliedFilters = this.getAppliedFilters(this.filters);
    }
  }

  public toggleFilter(): void {
    this.isFilterExpanded = !this.isFilterExpanded;
  }

  public get filterButtonIcon() {
    return !this.isFilterExpanded ? this.filterIcon : this.clearIcon;
  }

  public handleFilterPanelEnterKey(event: KeyboardEvent): void {
    if (event.target instanceof HTMLElement) {
      // We need to blur the element in order to trigger form control value update in case of updateOn: 'blur'.
      // Otherwise for example the input component won't change it's value
      event.target.blur();
    }

    this.search(event);
  }

  public search(event) {
    this.isFilterExpanded = false;
    this.searchEvent.emit(event);
  }

  public resetToPredefined(event): void {
    this.resetEvent.emit(event);
  }

  public removeTag(filter: AppliedFilter): void {
    this.removeTagEvent.next(filter);
  }

  public viewFilters(): void {
    this.isFilterExpanded = true;
    this.actionBarElement.nativeElement.scrollIntoView({ behavior: 'smooth' });
    this.filterButtonClicked.next(true);
  }

  private getAppliedFilters(filters: any): AppliedFilter[] {
    return chain(this.filterFormatter.format(filters))
      .map(filter => ({
        ...filter,
        closable: !this.isAppliedFilterClosable(filter)
      }))
      .orderBy(['closable'], ['asc'])
      .value();
  }

  private isAppliedFilterClosable(filter: AppliedFilter): boolean {
    let result = false;
    if (this.predefinedFilters) {
      const predefinedKeys = Object.keys(this.predefinedFilters);
      const commonKeys = intersection(predefinedKeys, filter.keys);
      result = commonKeys.length > 0;
    }

    return result;
  }
}
