import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FilterTemplateComponent } from './filter-template.component';

describe('FilterTemplateComponent', () => {
  let component: FilterTemplateComponent;
  let fixture: ComponentFixture<FilterTemplateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FilterTemplateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle filter', () => {
    const isFilterOpened = component.isFilterExpanded;
    component.toggleFilter();

    expect(component.isFilterExpanded).toBe(!isFilterOpened);
  });

  it('should emit search event', () => {
    spyOn(component.searchEvent, 'emit');
    component.search(null);

    expect(component.searchEvent.emit).toHaveBeenCalled();
  });

  it('should emit reset event', () => {
    spyOn(component.resetEvent, 'emit');
    component.resetToPredefined(null);

    expect(component.resetEvent.emit).toHaveBeenCalled();
  });

  it('should emit removeTag event', () => {
    spyOn(component.removeTagEvent, 'next');
    component.removeTag(null);

    expect(component.removeTagEvent.next).toHaveBeenCalled();
  });

  it('should expand filter and emit clicked event when view filter', () => {
    spyOn(component.filterButtonClicked, 'next');
    component.viewFilters();

    expect(component.isFilterExpanded).toBe(true);
    expect(component.filterButtonClicked.next).toHaveBeenCalled();
  });

  it('should not allowed to close predefined filters', () => {
    component.predefinedFilters = [{ bsps: null }];
    expect(component['isAppliedFilterClosable']({ keys: ['bsps'], label: null })).toBe(false);
  });
});
