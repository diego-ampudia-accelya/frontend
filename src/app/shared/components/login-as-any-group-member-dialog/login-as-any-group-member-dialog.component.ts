import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { FooterButton } from '../buttons/enums/footerButton';
import { DialogConfig } from '../dialog/dialog.config';
import { AgentGroupService } from '~app/master-data/agent-group/services/agent-group.service';
import { MainUserAgentGroup } from '~app/master-data/models/agent-group.model';
import { FormUtil } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { UserProfileService } from '~app/user-profile/services/user-profile.service';

export interface AgentGroupImpersonationViewModel {
  selectedAgentGroupMember: MainUserAgentGroup;
}

@Component({
  selector: 'bspl-login-as-any-group-member-dialog.component',
  templateUrl: './login-as-any-group-member-dialog.component.html',
  styleUrls: ['./login-as-any-group-member-dialog.component.scss'],
  providers: [UserProfileService]
})
export class LoginAsAnyGroupMemberDialogComponent implements OnInit, OnDestroy {
  public agentGroupMembersDropdown$: Observable<DropdownOption[]>;
  public form: FormGroup;

  private formFactory: FormUtil;
  private destroy$ = new Subject();

  constructor(
    public config: DialogConfig,
    public translationService: L10nTranslationService,
    private agentGroupService: AgentGroupService,
    private fb: FormBuilder
  ) {
    this.formFactory = new FormUtil(this.fb);
  }

  ngOnInit() {
    this.buildForm();
    this.initializeAgentGroupMembersDropdown();
    this.initializeAgentGroupMemberChangesListener();
  }

  private initializeAgentGroupMembersDropdown(): void {
    this.agentGroupMembersDropdown$ = this.agentGroupService.getAgentGroupMembers().pipe(
      map((mainUsers: MainUserAgentGroup[]) =>
        mainUsers.map(mainUser => ({
          value: mainUser.userCode,
          label: mainUser.userCode
        }))
      )
    );
  }

  private buildForm() {
    this.form = this.formFactory.createGroup<AgentGroupImpersonationViewModel>({
      selectedAgentGroupMember: ['', []]
    });
  }

  private initializeAgentGroupMemberChangesListener(): void {
    this.form
      .get('selectedAgentGroupMember')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.enableLoginButton();
      });
  }

  private enableLoginButton() {
    const button = this.config.data.buttons.find(actionButton => actionButton.type === FooterButton.Login);
    button.isDisabled = false;
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
