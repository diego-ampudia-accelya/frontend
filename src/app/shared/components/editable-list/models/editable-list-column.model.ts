import { Observable } from 'rxjs';
import { DropdownOption } from '~app/shared/models';

// TODO: Add `flexGrow` property and implement flex styles in editable list
export interface EditableListColumn {
  name: string;
  /** Column property path, it can be separated with dots if necessary (i.e: `bsp.name`) */
  prop: string;
  /** Column width in percentage, all column widths should add up to 100 */
  width: number;
  control?: EditableListColumnControl;
}

export interface EditableListColumnControl {
  type: EditableListColumnControlType;
  name: string;
  label?: string;
  placeholder?: string;
  /** Tooltip to display when the control is disabled */
  tooltip?: string;
  locked?: Observable<boolean>;
  options?: Observable<DropdownOption[]>;
}

// Enum because we want to build an structure to support new control types in the future (input, textarea...)
export enum EditableListColumnControlType {
  Select = 'select',
  Text = 'text'
}
