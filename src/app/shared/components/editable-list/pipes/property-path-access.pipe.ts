import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'access' })
export class PropertyPathAccessPipe implements PipeTransform {
  /**
   * Returns the value for the specified property path in the item
   *
   * @param item The specified object to be accessed
   * @param prop The specified property path separated by dots (i.e: `bsp.name`)
   */
  transform(item: any, prop: string): any {
    const paths: string[] = prop.split('.');

    return paths.reduce((acc, path) => acc && acc[path], item);
  }
}
