import { ChangeDetectorRef, Component, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { isNew } from '~app/shared/utils/api-utils';
import { ButtonDesign } from '../buttons/enums/buttonDesign';
import { EditableItemComponent } from './editable-item/editable-item.component';
import { EditableListColumn } from './models/editable-list-column.model';

@Component({
  selector: 'bspl-editable-list',
  templateUrl: './editable-list.component.html',
  styleUrls: ['./editable-list.component.scss']
})
export class EditableListComponent {
  @Input() isLoading = true;

  @Input() title: string;
  @Input() createButtonLabel: string;

  @Input() items: any[];
  @Input() columns: EditableListColumn[];

  @Input() itemForm: FormGroup;
  @Input() itemFormMapper: (item: any) => any;

  // Variables for display (or not) action buttons
  @Input() hasCreate: boolean;
  @Input() hasEdit: boolean;
  @Input() hasDelete: boolean;

  @Output() createItem = new EventEmitter<any>();
  @Output() updateItem = new EventEmitter<any>();
  @Output() deleteItem = new EventEmitter<any>();
  @Output() cancelItem = new EventEmitter<any>();
  @Output() editModeChange = new EventEmitter<boolean>();

  @ViewChildren(EditableItemComponent) editableItemComponents: QueryList<EditableItemComponent>;

  public btnDesign = ButtonDesign;
  public createButtonTooltip = 'editableList.finishEditing';

  public hasItemInEditMode = false;

  constructor(private changeDetector: ChangeDetectorRef) {}

  public onCreate(): void {
    this.items.unshift({ id: null });

    // Wait for the newly added EditableItemComponent to be available
    this.changeDetector.detectChanges();
    this.editableItemComponents.first.onEdit();
  }

  public onSaveItem(item: any): void {
    // Handle `create` or `edit` action of items
    if (isNew(item)) {
      this.createItem.emit(item);
      this.items.shift();
    } else {
      this.updateItem.emit(item);
    }
  }

  public onCancelItem(item: any): void {
    // Handle `cancel` action of newly created items
    if (isNew(item)) {
      this.items.shift();
    }

    this.cancelItem.emit();
  }

  public onEditModeChange(inEditMode: boolean) {
    this.hasItemInEditMode = inEditMode;

    this.editModeChange.emit(inEditMode);
  }
}
