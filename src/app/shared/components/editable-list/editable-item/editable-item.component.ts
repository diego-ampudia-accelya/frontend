import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { identity } from 'lodash';
import { isNew } from '~app/shared/utils/api-utils';
import { EditableListColumn, EditableListColumnControlType } from '../models/editable-list-column.model';

@Component({
  selector: 'bspl-editable-item',
  templateUrl: './editable-item.component.html',
  styleUrls: ['./editable-item.component.scss']
})
export class EditableItemComponent {
  @Input() item: any;
  @Input() columns: EditableListColumn[];

  @Input() form: FormGroup;
  @Input() formMapper: (item: any) => any = identity;
  @Input() hasDisabledActions = false;

  // Variables for display (or not) action buttons
  @Input() hasEdit: boolean;
  @Input() hasDelete: boolean;

  @Output() delete = new EventEmitter<any>();
  @Output() save = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<any>();
  @Output() editModeChange = new EventEmitter<boolean>();

  public editableListColumnControlType = EditableListColumnControlType;

  // Flag determines if the component is showing `read` or `edit` mode
  public editMode = false;
  // Flag determines if the `save` button is enabled or disabled (validation)
  public canSave = false;

  // Object used for storing the instance values during editing
  private oldValues: any;

  /** Handles `change` events and controls the state of the `save` button */
  public onNgModelChange(): void {
    this.determineSaveButtonState();
  }

  /** Handles `create` and `edit` action click. Remembers current values and enters `edit` mode */
  public onEdit(): void {
    if (this.hasDisabledActions || this.editMode) {
      return;
    }

    // Disable the `save` button until any value has changed in a valid way (e.g. not empty)
    this.canSave = false;

    // Updating the form with item property values on `edit` action click
    if (!isNew(this.item)) {
      this.form.patchValue(this.formMapper(this.item), { emitEvent: false });
    }

    // Remember form values for later
    this.oldValues = this.form.value;

    this.toggleEditMode();
  }

  /** Handles `delete` action click */
  public onDelete(): void {
    if (!this.hasDisabledActions) {
      this.delete.emit(this.item);
    }
  }

  /** Handles `save` action */
  public onSave(): void {
    if (this.canSave) {
      const data = { ...this.item, ...this.form.value };
      this.save.emit(data);

      this.toggleEditMode();
    }
  }

  /** Handles `cancel` action click. Discards changes and exits `edit` mode */
  public onCancel(): void {
    this.cancel.emit(this.item);

    // Exit edit mode
    this.toggleEditMode();
  }

  /** Changes `save` button state between enabled/disabled */
  private determineSaveButtonState(): void {
    let hasChangedField = false;
    // By default consider it enabled
    this.canSave = true;
    for (const field of Object.keys(this.form.controls)) {
      // When at least one field value is not valid - disable saving
      if (!this.form.controls[field].valid) {
        this.canSave = false;
      } else {
        // Determine if at least one form field has changed
        if (this.form.controls[field].value !== this.oldValues[field]) {
          hasChangedField = true;
        }
      }
    }

    if (!hasChangedField) {
      this.canSave = false;
    }
  }

  /** Switches between `edit` and `read` modes */
  private toggleEditMode(): void {
    this.editMode = !this.editMode;

    this.editModeChange.emit(this.editMode);
  }
}
