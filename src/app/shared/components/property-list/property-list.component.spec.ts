import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { L10nTranslateAsyncPipe } from 'angular-l10n';
import { identity } from 'lodash';
import { MockComponent, MockPipe } from 'ng-mocks';

import { PropertyListComponent } from './property-list.component';
import { PropertyComponent } from './property/property.component';

describe('PropertyListComponent', () => {
  let component: PropertyListComponent;
  let fixture: ComponentFixture<PropertyListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertyListComponent,
        MockComponent(PropertyComponent),
        MockPipe(L10nTranslateAsyncPipe, identity)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set `property-list` class on host element', () => {
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement).toHaveClass('property-list');
  });

  it('should not display header when not specified', () => {
    fixture.detectChanges();
    const header = fixture.debugElement.query(By.css('.property-list-header'));
    expect(header).toBe(null);
  });

  it('should display header when specified', () => {
    const headerText = 'Header';

    component.header = headerText;
    fixture.detectChanges();

    const header = fixture.debugElement.query(By.css('.property-list-header'));
    expect(header).toBeDefined();
    expect(header.nativeElement.textContent).toContain(headerText);
  });
});
