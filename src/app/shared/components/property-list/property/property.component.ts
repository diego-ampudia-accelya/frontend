import { ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bspl-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PropertyComponent {
  @Input() label: string;
  @Input() value: string;

  @Input() labelTooltip: string;

  @HostBinding('class') protected className = 'property';
}
