import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { L10nTranslateAsyncPipe } from 'angular-l10n';
import { identity } from 'lodash';
import { MockDirective, MockPipe } from 'ng-mocks';

import { PropertyComponent } from './property.component';
import { TooltipDirective } from '~app/shared/directives/tooltip/tooltip.directive';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';

@Component({
  selector: 'bspl-test-host',
  template: `<bspl-property label="Label">Value</bspl-property>`
})
class TestHostComponent {}

describe('PropertyComponent', () => {
  let component: PropertyComponent;
  let fixture: ComponentFixture<PropertyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertyComponent,
        TestHostComponent,
        MockDirective(TooltipDirective),
        MockPipe(L10nTranslateAsyncPipe, identity),
        MockPipe(EmptyPipe, identity)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges() works only once for components with OnPush.
    // https://github.com/angular/angular/issues/12313#issuecomment-495981245
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add `property` class on host element', () => {
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement).toHaveClass('property');
  });

  it('should display label when available', () => {
    component.label = 'Label';
    fixture.detectChanges();

    const labelElement = fixture.debugElement.query(By.css('.property-label'));
    expect(labelElement).toBeDefined();
    expect(labelElement.nativeElement.textContent).toContain('Label');
  });

  it('should display value when provided as input', () => {
    component.value = 'Value';
    fixture.detectChanges();

    const valueElement = fixture.debugElement.query(By.css('.property-value'));
    expect(valueElement).toBeDefined();
    expect(valueElement.nativeElement.textContent).toContain('Value');
  });

  it('should display value when provided as content', () => {
    const testHost = TestBed.createComponent(TestHostComponent);
    testHost.detectChanges();

    const valueElement = testHost.debugElement.query(By.css('.property-value'));
    expect(valueElement).toBeDefined();
    expect(valueElement.nativeElement.textContent).toContain('Value');
  });
});
