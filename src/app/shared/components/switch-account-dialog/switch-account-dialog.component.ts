import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, first, map, switchMap, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

import { ButtonDesign } from '../buttons/enums/buttonDesign';
import { FooterButton } from '../buttons/enums/footerButton';
import { DialogConfig } from '../dialog/dialog.config';
import { DialogService } from '../dialog/dialog.service';
import { ButtonsFooterFactory } from '../dialog/elements/buttons-footer-factory';
import { ReactiveSubject } from '../dialog/reactive-subject';
import { NavigateToClassicBspLinkSwitchAccount } from '~app/auth/actions/auth.actions';
import { getUserId } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { ModalAction } from '~app/shared/models/modal-action.model';
import { UserAccount, UserAccountBsp } from '~app/shared/models/user-account.model';
import { UsersProfileManagementService } from '~app/shared/services';
import { BspsDictionaryService } from '~app/shared/services/dictionary/bsps-dictionary.service';

@Component({
  selector: 'bspl-switch-account-dialog.component',
  templateUrl: './switch-account-dialog.component.html',
  styleUrls: ['./switch-account-dialog.component.scss']
})
export class SwitchAccountDialogComponent implements OnInit, OnDestroy {
  public form: FormGroup;

  public bspControl: FormControl;
  public accountControl: FormControl;

  public bspDropdownOptions$: Observable<DropdownOption<UserAccountBsp & { name: string }>[]>;
  public accountDropdownOptions$: Observable<DropdownOption<UserAccount>[]>;

  private _isBspSelectorLockedSubject = new BehaviorSubject(true);
  public isBspSelectorLocked$: Observable<boolean> = this._isBspSelectorLockedSubject.asObservable();

  private _isAccountSelectorLockedSubject = new BehaviorSubject(true);
  public isAccountSelectorLocked$: Observable<boolean> = this._isAccountSelectorLockedSubject.asObservable();

  private _isFormReadySubject = new BehaviorSubject(false);
  public isFormReady$: Observable<boolean> = this._isFormReadySubject.asObservable();

  private _isSupplantingAccountSubject = new BehaviorSubject(false);
  public isSupplantingAccount$: Observable<boolean> = this._isSupplantingAccountSubject.asObservable();

  private destroy$ = new Subject();

  constructor(
    private config: DialogConfig,
    private formBuilder: FormBuilder,
    private usersProfileManagementService: UsersProfileManagementService,
    private bspsDictionaryService: BspsDictionaryService,
    private reactiveSubject: ReactiveSubject,
    private store: Store<AppState>,
    private dialog: DialogService
  ) {}

  public ngOnInit(): void {
    this.buildForm();

    this.initializeDropdownOptions();
    this.initializeListeners();
  }

  private buildForm(): void {
    this.bspControl = this.formBuilder.control(null, Validators.required);
    this.accountControl = this.formBuilder.control(null, Validators.required);

    this.form = this.formBuilder.group({
      bsp: this.bspControl,
      account: this.accountControl
    });
  }

  private initializeDropdownOptions(): void {
    // BSP dropdown options
    this.bspDropdownOptions$ = combineLatest([
      this.usersProfileManagementService.getUserAccountBsps(),
      this.bspsDictionaryService.getAllBsps()
    ]).pipe(
      map(([accountBsps, bsps]) => this.leftInnerJoinConcatingBspNames(accountBsps, bsps)),
      map(bsps => this.toBspDropdownOptions(bsps)),
      // Side effect
      tap(options => {
        if (options.length === 1) {
          this._isBspSelectorLockedSubject.next(true);
          this.bspControl.patchValue(options[0].value);
        } else {
          this._isBspSelectorLockedSubject.next(false);
          this._isFormReadySubject.next(true);
        }
      })
    );

    // Account dropdown options
    this.accountDropdownOptions$ = this.bspControl.valueChanges.pipe(
      filter(bsp => bsp != null),
      distinctUntilChanged(),
      switchMap(bsp => this.usersProfileManagementService.getUserAccounts(bsp.id)),
      withLatestFrom(this.store.pipe(select(getUserId))),
      // Side effect
      tap(([accounts, userId]) => {
        if (accounts.length === 1) {
          this._isAccountSelectorLockedSubject.next(true);
          this.accountControl.patchValue(accounts[0]);

          // Checking if user is supplanting the account. If so, Login is available
          if (userId !== +accounts[0].id) {
            this._isSupplantingAccountSubject.next(true);
          }
        } else {
          this._isAccountSelectorLockedSubject.next(false);
        }

        this._isFormReadySubject.next(true);
      }),
      map(([accounts, _]) => this.toAccountDropdownOptions(accounts)),
      takeUntil(this.destroy$)
    );
  }

  private leftInnerJoinConcatingBspNames(
    accountBsps: UserAccountBsp[],
    bsps: BspDto[]
  ): (UserAccountBsp & { name: string })[] {
    return (
      accountBsps
        // Left inner join
        .filter(accountBsp => bsps.some(bsp => bsp.isoCountryCode === accountBsp.isoc))
        // Concat BSP names
        .map(accountBsp => ({
          ...accountBsp,
          name: bsps.find(bsp => bsp.isoCountryCode === accountBsp.isoc).name
        }))
    );
  }

  private toBspDropdownOptions(
    bsps: (UserAccountBsp & { name: string })[]
  ): DropdownOption<UserAccountBsp & { name: string }>[] {
    return bsps.map(bsp => ({
      value: bsp,
      label: bsp.name ? `${bsp.name} - ${bsp.isoc}` : bsp.isoc
    }));
  }

  private toAccountDropdownOptions(accounts: UserAccount[]): DropdownOption<UserAccount>[] {
    return accounts.map(account => {
      const accountTypeDisplay = account.template ? `${account.type} / ${account.template}` : account.type;

      return {
        value: account,
        label: `${account.code} ${account.name} (${accountTypeDisplay})`
      };
    });
  }

  private initializeListeners(): void {
    this.initializeFormListener();
    this.initializeFormReadyListener();
    this.initializeSubmitListener();
    this.initializeCloseListener();
  }

  private initializeFormListener(): void {
    combineLatest([
      this.isBspSelectorLocked$,
      this.isAccountSelectorLocked$,
      this.isSupplantingAccount$,
      this.form.statusChanges
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([bspLocked, accountLocked, accountSupplanted, formStatus]) => {
        const loginButton: ModalAction = this.config.data.buttons.find(
          (btn: ModalAction) => btn.type === FooterButton.Login
        );

        if (bspLocked && accountLocked && !accountSupplanted) {
          loginButton.isDisabled = true;
        } else {
          loginButton.isDisabled = formStatus === 'INVALID';
        }
      });
  }

  private initializeFormReadyListener(): void {
    this.isFormReady$.pipe(first(ready => ready)).subscribe(() => {
      // If both selectors are locked (user has only one BSP and account) and it's not a supplanted account, user is not able to switch account
      if (
        this._isBspSelectorLockedSubject.value &&
        this._isAccountSelectorLockedSubject.value &&
        !this._isSupplantingAccountSubject.value
      ) {
        // Change dialog title and buttons -> readonly mode
        this.config.data.title = 'headerUserInfo.switchAccountDialog.readonlyTitle';
        this.config.data.buttons = ButtonsFooterFactory.getFooterButtonsByType(
          [{ type: FooterButton.Close, buttonDesign: ButtonDesign.Tertiary }],
          false
        );
      }
    });
  }

  private initializeSubmitListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        first(({ clickedBtn }) => clickedBtn === FooterButton.Login && this.form.valid),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        const account: UserAccount = this.accountControl.value;

        this.store.dispatch(new NavigateToClassicBspLinkSwitchAccount({ accountId: account?.id }));
        this.dialog.close();
      });
  }

  private initializeCloseListener(): void {
    this.reactiveSubject.asObservable
      .pipe(
        first(({ clickedBtn }) => clickedBtn === FooterButton.Cancel || clickedBtn === FooterButton.Close),
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.dialog.close());
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
