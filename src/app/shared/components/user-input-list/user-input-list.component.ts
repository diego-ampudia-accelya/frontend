import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AbstractControlOptions, FormArray, FormBuilder, FormControl, ValidatorFn } from '@angular/forms';

import { ButtonDesign } from '../buttons/enums/buttonDesign';
import { IconButtonType } from '../buttons/enums/iconButtonType';

import { uniqueElementArrayValidator } from './validators/unique-element-array.validator';

@Component({
  selector: 'bspl-user-input-list',
  templateUrl: './user-input-list.component.html',
  styleUrls: ['./user-input-list.component.scss']
})
export class UserInputListComponent implements OnInit {
  @Input() formArray: FormArray; //* Expects an array as value to concat values entered by users

  @Input() label: string;
  @Input() placeholder: string;
  @Input() placeholderInput: string;
  @Input() maxLength: number;

  @Input() validators: ValidatorFn[] = [];
  @Input() uniqueElements = true;
  @Input() updateOn: 'blur' | 'change' | 'submit' = 'blur';

  @Input() isReadOnly = false;

  public input: FormControl;

  public btnDesign = ButtonDesign;
  public iconButtonType = IconButtonType;

  constructor(private fb: FormBuilder, private cd: ChangeDetectorRef) {}

  public ngOnInit() {
    const controlOptions: AbstractControlOptions = {
      updateOn: this.updateOn,
      validators: this.uniqueElements
        ? [...this.validators, uniqueElementArrayValidator(this.formArray)]
        : this.validators
    };
    this.input = this.fb.control('', controlOptions);

    this.initializeStatusChangesListener();
  }

  public onAddClick() {
    if (this.isAddingAllowed()) {
      this.formArray.push(this.fb.control(this.input.value));
      this.input.reset('');
    }
  }

  public onRemoveClick(index: number) {
    this.formArray.removeAt(index);
    this.input.updateValueAndValidity();
  }

  public isAddingAllowed(): boolean {
    return this.input.value && this.input.valid;
  }

  private initializeStatusChangesListener() {
    this.formArray.statusChanges.subscribe(() => {
      if (this.formArray.invalid) {
        const invalidControl = this.formArray.controls.find(control => control.errors !== null);
        this.formArray.setErrors(invalidControl.errors, { emitEvent: false });
      }

      this.cd.markForCheck();
    });
  }
}
