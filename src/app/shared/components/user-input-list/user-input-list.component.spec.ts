import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { UserInputListComponent } from './user-input-list.component';

describe('UserInputListComponent', () => {
  let component: UserInputListComponent;
  let fixture: ComponentFixture<UserInputListComponent>;
  let fb: FormBuilder;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserInputListComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [FormBuilder]
    }).compileComponents();
  }));

  beforeEach(() => {
    fb = TestBed.inject(FormBuilder);
    fixture = TestBed.createComponent(UserInputListComponent);
    component = fixture.componentInstance;
    component.formArray = fb.array([]);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add an element onAddClick', () => {
    const testValue = 'test';
    component.input.setValue(testValue, { emitEvent: false });

    component.onAddClick();

    expect(component.formArray.length).toBe(1);
    expect(component.formArray.at(0).value).toBe(testValue);
  });

  it('should not add an element when input is invalid', () => {
    const currentLength = component.formArray.length;

    component.input.setValue('test', { emitEvent: false });
    component.input.setErrors({ error: true });

    component.onAddClick();

    expect(component.formArray.length).toBe(currentLength);
  });

  it('should remove an element onRemoveClick', () => {
    component.formArray.push(fb.control(''));

    expect(component.formArray.length).toBe(1);
    component.onRemoveClick(0);
    expect(component.formArray.length).toBe(0);
  });
});
