import { AbstractControl, ValidatorFn } from '@angular/forms';

export function uniqueElementArrayValidator(elements: Array<any> | AbstractControl): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    let result = null;
    const array = Array.isArray(elements) ? elements : elements.value;

    if (control.value && array.includes(control.value)) {
      result = {
        duplicated: {
          item: control.value
        }
      };
    }

    return result;
  };
}
