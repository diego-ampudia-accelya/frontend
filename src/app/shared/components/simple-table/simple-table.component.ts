import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { get, isNil } from 'lodash';

import { SimpleTableColumn } from './simple-table-column.model';
import { GLOBALS } from '~app/shared/constants/globals';

/**
 * A simple, lightweight component that displays data in tabular format.
 */
@Component({
  selector: 'bspl-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleTableComponent {
  /**
   * Column configuration
   */
  @Input() columns: SimpleTableColumn[];
  /**
   * Data items to display
   */
  @Input() data: any[];
  /**
   * A heading describing the information displayed in the table
   */
  @Input() title: string;

  /**
   * Determines whether you can click on a table row
   */
  @Input() isRowClickable = false;

  @Output() rowClick: EventEmitter<any> = new EventEmitter();

  constructor(private injector: Injector) {}

  /**
   * Format a data item value based on the column configuration.
   * By default, the value is formatted using the standard `toString` implementation.
   * If the column is configured with a pipe then we use it to get the formatted value.
   * When a value is missing we display N/A.
   *
   * @param dataItem Data item which contains the information to display.
   * @param column Configuration of the specific column to display.
   * @returns Formatted value.
   */
  public getFormattedValue(dataItem: any, column: SimpleTableColumn): string {
    const notAvailable = GLOBALS.LONG_DASH;
    const value = get(dataItem, column.field);
    let formattedValue = value;

    if (column.pipe) {
      const pipe = this.injector.get(column.pipe);
      formattedValue = pipe.transform(value, ...(column.pipeArgs || []));
    }

    const isEmptyValue = isNil(formattedValue) || formattedValue === '';
    formattedValue = isEmptyValue ? notAvailable : formattedValue.toString();

    return formattedValue;
  }

  public onRowClick(dataItem: any): void {
    if (this.isRowClickable) {
      this.rowClick.emit(dataItem);
    }
  }
}
