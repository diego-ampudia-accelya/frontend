import { DecimalPipe } from '@angular/common';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

import { SimpleTableColumn } from './simple-table-column.model';
import { SimpleTableComponent } from './simple-table.component';
import { GLOBALS } from '~app/shared/constants/globals';
import { TooltipDirectiveModule } from '~app/shared/directives/tooltip/tooltip.module';
import { TranslatePipeMock } from '~app/test';

describe('SimpleTableComponent', () => {
  let spectator: Spectator<SimpleTableComponent>;
  const createComponent = createComponentFactory({
    component: SimpleTableComponent,
    imports: [TooltipDirectiveModule],
    declarations: [TranslatePipeMock],
    componentProviders: [DecimalPipe]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  describe('when using a simple column configuration', () => {
    let columns: SimpleTableColumn<TestDataItem>[];

    beforeEach(() => {
      columns = [
        {
          field: 'name',
          header: 'Name'
        },
        {
          field: 'commission',
          header: 'Commission'
        },
        {
          field: 'amount',
          header: 'Amount'
        }
      ];
    });

    it('should create 3 headers when 3 columns are defined', () => {
      spectator.setInput('columns', columns);

      expect(spectator.queryAll('.simple-table-header-cell')).toHaveLength(3);
    });

    it('should display header text based on column configuration', () => {
      spectator.setInput('columns', columns);

      const actual = spectator.queryAll('.simple-table-header-cell').map(cell => cell.textContent.trim());
      expect(actual).toEqual(['Name', 'Commission', 'Amount']);
    });

    it('should create 2 rows when 2 data items are specified', () => {
      const data: any[] = [{}, {}];

      spectator.setInput('columns', columns);
      spectator.setInput('data', data);

      const rows = spectator.queryAll('.simple-table-row');
      expect(rows).toHaveLength(2);
    });

    it('should display values in cells', () => {
      const data: TestDataItem[] = [{ name: 'VAT Tax', commission: 0, amount: 2.4 }];

      spectator.setInput('columns', columns);
      spectator.setInput('data', data);

      const cells = spectator.queryAll('.simple-table-row .simple-table-cell').map(cell => cell.textContent.trim());
      expect(cells).toEqual(['VAT Tax', '0', '2.4']);
    });

    it('should display `em dash` in cells when a value is missing', () => {
      const data: TestDataItem[] = [{ name: 'VAT Tax', commission: null, amount: null }];

      spectator.setInput('columns', columns);
      spectator.setInput('data', data);

      const cells = spectator.queryAll('.simple-table-row .simple-table-cell').map(cell => cell.textContent.trim());
      expect(cells).toEqual(['VAT Tax', GLOBALS.LONG_DASH, GLOBALS.LONG_DASH]);
    });
  });

  it('should display formatted values in cells when using pipes in column configuration', () => {
    const columns: SimpleTableColumn<TestDataItem>[] = [
      {
        field: 'name',
        header: 'Name'
      },
      {
        field: 'commission',
        header: 'Commission',
        pipe: DecimalPipe
      },
      {
        field: 'amount',
        header: 'Amount',
        pipe: DecimalPipe,
        pipeArgs: ['1.3-3']
      }
    ];
    const data: TestDataItem[] = [{ name: 'VAT Tax', commission: 20, amount: 2.4 }];

    spectator.setInput('columns', columns);
    spectator.setInput('data', data);

    const cells = spectator.queryAll('.simple-table-row .simple-table-cell').map(cell => cell.textContent.trim());
    expect(cells).toEqual(['VAT Tax', '20', '2.400']);
  });

  it('should display em dash when a cell value is missing and a pipe is configured for the column', () => {
    const columns: SimpleTableColumn<TestDataItem>[] = [
      {
        field: 'commission',
        header: 'Commission',
        pipe: DecimalPipe
      }
    ];
    const data: TestDataItem[] = [{ name: null, commission: null, amount: null }];

    spectator.setInput('columns', columns);
    spectator.setInput('data', data);

    const cells = spectator.queryAll('.simple-table-row .simple-table-cell').map(cell => cell.textContent.trim());
    expect(cells).toEqual([GLOBALS.LONG_DASH]);
  });

  it('should apply custom CSS class when using custom cell style in column configuration', () => {
    const columns: SimpleTableColumn<TestDataItem>[] = [
      {
        field: 'name',
        header: 'Name'
      },
      {
        field: 'commission',
        header: 'Commission',
        customStyle: 'custom-class'
      }
    ];
    const data: TestDataItem[] = [{ name: 'VAT Tax', commission: 20, amount: 2.4 }];

    spectator.setInput('columns', columns);
    spectator.setInput('data', data);

    expect(spectator.queryAll('.simple-table-header-cell.custom-class')).toHaveLength(1);
    expect(spectator.queryAll('.simple-table-row .custom-class')).toHaveLength(1);
  });
});

interface TestDataItem {
  name: string;
  commission: number;
  amount: number;
}
