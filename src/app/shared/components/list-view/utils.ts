export function isEmpty(value: any): boolean {
  if (typeof value === 'boolean') {
    return false;
  }

  if (Array.isArray(value) && value.length === 0) {
    return true;
  }

  if (value instanceof Date) {
    return false;
  }

  const isEmptyObject = value && typeof value === 'object' && Object.keys(value).length === 0;
  if (isEmptyObject) {
    return true;
  }

  if (value === 0) {
    return false;
  }

  return !value;
}
