/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @angular-eslint/no-output-native */
import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { chain, cloneDeep, intersection } from 'lodash';
import { MenuItem } from 'primeng/api';
import { merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ButtonDesign } from '../../buttons/enums/buttonDesign';
import { FooterButton } from '../../buttons/enums/footerButton';
import { DialogService } from '../../dialog/dialog.service';
import { GridTableComponent } from '../../grid-table/grid-table.component';
import { DataQuery } from '../data-query';
import { AppliedFilter } from '../filter-formatter/applied-filter';
import { FilterFormatter } from '../filter-formatter/filter-formatter';
import { RecordsLimitDialogComponent } from '../records-limit-dialog/records-limit-dialog.component';

import { defaultQuery, listViewConfig } from './constants.config';
import { ListViewFilterDirective } from './list-view-filter.directive';
import { ListViewGridDirective } from './list-view-grid.directive';
import { SortingObject } from '~app/shared/models/sorting-object.model';
import { AlertMessageType } from '~app/shared/enums';

@Component({
  selector: 'bspl-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnChanges, OnInit, AfterViewInit {
  @Input() query: DataQuery;
  @Input() predefinedFilters: { [key: string]: any };
  @Input() filterFormatter: FilterFormatter;

  @Input() title: string;
  @Input() customLabels = listViewConfig.defaultLabels;
  @Input() noticeMessage: string;

  @Input() hasAdvancedSearchFilter = false;
  @Input() hasFiltering: boolean;
  @Input() isActionButtonDisabled = false;
  @Input() canDownload: boolean;
  @Input() hasDownloading: boolean;
  @Input() hasDetailedDownloading: boolean;
  @Input() hasCreating: boolean;
  @Input() canCreating = true;
  @Input() canApply: boolean;
  @Input() hasApply: boolean;
  @Input() isInTab: boolean;
  @Input() maxSupportedItems: number;
  @Input() set hasFilteringExpanded(value: boolean) {
    this.isFilterExpanded = value;
  }

  @Output() reset = new EventEmitter();
  @Output() queryChange = new EventEmitter<DataQuery>();
  @Output() download = new EventEmitter<void>();
  @Output() detailedDownload = new EventEmitter<void>();
  @Output() create = new EventEmitter<void>();
  @Output() apply = new EventEmitter<void>();
  @Output() showAdvancedSearch = new EventEmitter();
  @Output() filterButtonClicked = new EventEmitter();

  public filters: { [key: string]: any };
  public appliedFilters: Array<AppliedFilter>;
  public showAdvancedSearchFilter = false;
  public detailDownloadingOptions: { options: MenuItem[]; mainOption: MenuItem };

  public labels = listViewConfig.defaultLabels;
  public ButtonDesign = ButtonDesign;
  public AlertMessageType = AlertMessageType;

  public isFilterExpanded: boolean;

  public get hasAppliedFilter(): boolean {
    return this.hasFiltering && this.appliedFilters && this.appliedFilters.length > 0;
  }

  public get filterIcon() {
    return !this.isFilterExpanded ? listViewConfig.icons.filter : listViewConfig.icons.clear;
  }

  public get isFilterValid(): boolean {
    return this.filterDirective && this.filterDirective.isValid();
  }

  public get hasTooManyItems(): boolean {
    const max = this.maxSupportedItems;
    const total = this.query?.paginateBy?.totalElements ?? 0;

    return max != null && max < total;
  }

  public get hasDownload(): boolean {
    return this.hasDownloading && !this.hasDetailedDownloading;
  }

  public get hasDownloadOptions(): boolean {
    return this.hasDownloading && this.hasDetailedDownloading;
  }

  private get grid(): GridTableComponent {
    return this.gridDirective && this.gridDirective.grid;
  }

  @ViewChild('actionBar', { static: true })
  private actonBarElement: ElementRef<HTMLElement>;

  @ContentChild(ListViewFilterDirective)
  private filterDirective: ListViewFilterDirective;

  @ContentChild(ListViewGridDirective, { static: true })
  private gridDirective: ListViewGridDirective;

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private dialogService: DialogService,
    private translationService: L10nTranslationService
  ) {}

  public ngOnChanges(changes: SimpleChanges) {
    this.verifyRequiredInputs();

    if (changes.query) {
      const query = changes.query.currentValue || cloneDeep(defaultQuery);
      const { filterBy, sortBy, paginateBy } = query;

      this.setSorting(sortBy);
      this.grid.paging = paginateBy;

      if (!changes.query.isFirstChange()) {
        this.setFormValue(filterBy);
      }
    }

    if (changes.customLabels) {
      this.labels = { ...listViewConfig.defaultLabels, ...changes.customLabels.currentValue };
    }

    if (changes.maxSupportedItems) {
      this.grid.maxSupportedItems = changes.maxSupportedItems.currentValue;
    }
  }

  public ngAfterViewInit(): void {
    this.verifyRequiredContent();

    const queryChange$ = this.observeQueryChange();
    queryChange$.subscribe(query => this.queryChange.emit(query));

    this.grid.maxSupportedPageExceeded.subscribe(() => this.openRecordsLimitDialog());

    // Make sure that we set the initial form value after the form is available.
    // Use setTimeout to avoid an exception.
    setTimeout(() => {
      const { filterBy = null } = this.query || {};
      this.setFormValue(filterBy);
    });
  }

  ngOnInit(): void {
    if (this.hasDownloadOptions) {
      this.initializeBtnOptions();
    }
  }

  public handleFilterPanelEnterKey(event: KeyboardEvent): void {
    if (event.target instanceof HTMLElement) {
      // We need to blur the element in order to trigger form control value update in case of updateOn: 'blur'.
      // Otherwise for example the input component won't change it's value
      event.target.blur();
    }

    this.search();
  }

  public search() {
    if (!this.isFilterValid) {
      return;
    }

    this.isFilterExpanded = false;
    // to show correct result after search
    // we should reset pagination to first page
    this.query = { ...this.query, paginateBy: { ...this.query?.paginateBy, page: 0 } };

    this.updateFilter();
  }

  public removeTag(filter: AppliedFilter): void {
    for (const key of filter.keys) {
      this.filterDirective.clearProp(key);
    }

    this.updateFilter();
  }

  public resetToPredefined() {
    const initialFormValue = this.predefinedFilters || {};

    this.reset.emit();

    this.filterDirective.setValue(initialFormValue);

    this.updateFilter();
  }

  public toggleFilter() {
    this.isFilterExpanded = !this.isFilterExpanded;
    this.filterButtonClicked.emit(this.isFilterExpanded);
  }

  public toggleAdvancedSearch() {
    this.showAdvancedSearchFilter = !this.showAdvancedSearchFilter;
    this.showAdvancedSearch.emit(this.showAdvancedSearchFilter);
  }

  public viewFilter() {
    this.isFilterExpanded = true;
    this.actonBarElement.nativeElement.scrollIntoView({ behavior: 'smooth' });
    this.filterButtonClicked.emit(true);
  }

  public triggerDownload() {
    this.download.emit();
  }

  public triggerDetailedDownload() {
    this.detailedDownload.emit();
  }

  public triggerCreate() {
    this.create.emit();
  }

  public triggerApply() {
    this.apply.emit();
  }

  public handleTooManyItemsAction($event: MouseEvent): void {
    const target = $event.target as HTMLElement;
    if (target.classList.contains('filters-link')) {
      this.viewFilter();
    } else if (target.classList.contains('download-link')) {
      this.triggerDownload();
    }
  }

  private observeQueryChange(): Observable<DataQuery> {
    const paging$ = this.grid.pageChange.pipe(map(paginateBy => ({ ...this.query, paginateBy })));
    const sorting$ = this.grid.sortChange.pipe(map(sortBy => ({ ...this.query, sortBy })));

    return merge(paging$, sorting$);
  }

  private updateFilter(options = { emitEvent: true }): void {
    this.filters = this.filterDirective.getValue();
    this.appliedFilters = this.getAppliedFilters(this.filters);

    if (options.emitEvent) {
      this.queryChange.emit({ ...this.query, filterBy: this.filters });
    }
  }

  private getAppliedFilters(filters: any): AppliedFilter[] {
    return chain(this.filterFormatter.format(filters))
      .map(filter => ({
        ...filter,
        closable: !this.isAppliedFilterClosable(filter)
      }))
      .orderBy(['closable'], ['asc'])
      .value();
  }

  private isAppliedFilterClosable(filter: AppliedFilter): boolean {
    let result = false;
    if (this.predefinedFilters) {
      const predefinedKeys = Object.keys(this.predefinedFilters);
      const commonKeys = intersection(predefinedKeys, filter.keys);
      result = commonKeys.length > 0;
    }

    return result;
  }

  private setSorting(sortBy: SortingObject[]): void {
    // TODO change this logic, when sorting on multiple columns is provided
    sortBy = sortBy || [];
    const { attribute = null, sortType = null } = sortBy[0] || {};
    this.grid.sortedColumn = attribute;
    this.grid.sortOrder = sortType;
  }

  private setFormValue(value: any): void {
    if (this.filterDirective) {
      this.filterDirective.setValue(value || {});
      this.updateFilter({ emitEvent: false });
    }
  }

  private verifyRequiredContent(): void {
    if (!this.grid) {
      throw new Error(
        '[bspl-list-view] Missing required bsplListViewGrid directive. Have you included the bspl-grid-table?'
      );
    }

    if (this.hasFiltering && !this.filterDirective) {
      throw new Error(
        '[bspl-list-view] Filtering is enabled but no bsplListViewFilter was found. Have you specified the content of the filter?'
      );
    }
  }

  private verifyRequiredInputs() {
    if (!this.filterFormatter) {
      throw new Error(
        '[bspl-list-view] No filter formatter found. Have you specified a value for the [filterFormatter] input?'
      );
    }
  }

  private openRecordsLimitDialog(): void {
    this.dialogService
      .open(RecordsLimitDialogComponent, {
        data: {
          title: this.labels.tooManyItemsTitle,
          maxSupportedItems: this.maxSupportedItems,
          message: this.labels.tooManyItems,
          hasCancelButton: false,
          footerButtonsType: FooterButton.Cancel
        }
      })
      .subscribe(action => {
        if (action === FooterButton.Download) {
          this.dialogService.close();
          // Wait a bit, otherwise a vertical scroll appears.
          setTimeout(() => this.triggerDownload());
        } else if (action === FooterButton.Filter) {
          this.dialogService.close();
          this.viewFilter();
        }
      });
  }

  private initializeBtnOptions(): void {
    this.detailDownloadingOptions = {
      mainOption: {
        label: this.translationService.translate(this.labels.download),
        command: () => this.triggerDownload()
      },
      options: [
        {
          label: this.translationService.translate(this.labels.detailedDowload),
          command: () => this.triggerDetailedDownload()
        }
      ]
    };
  }
}
