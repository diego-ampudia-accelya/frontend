import { Directive } from '@angular/core';

@Directive({
  selector: '[bsplListViewTitle]'
})
export class ListViewTitleDirective {}
