import { Directive, Host, Self } from '@angular/core';

import { GridTableComponent } from '../../grid-table/grid-table.component';

@Directive({
  selector: '[bsplListViewGrid]'
})
export class ListViewGridDirective {
  constructor(@Host() @Self() public grid: GridTableComponent) {}
}
