import { Directive, Host, OnInit, Optional, Self } from '@angular/core';
import { FormGroup, FormGroupDirective, NgForm } from '@angular/forms';
import { omitBy } from 'lodash';

import { isEmpty } from '../utils';

@Directive({
  selector: '[bsplListViewFilter]'
})
export class ListViewFilterDirective implements OnInit {
  private form: NgForm | FormGroup;

  constructor(
    @Optional() @Host() @Self() private ngForm: NgForm,
    @Optional() @Host() @Self() private formGroupDirective: FormGroupDirective
  ) {
    if (!ngForm && !formGroupDirective) {
      throw new Error(
        'The ListViewFilterDirective requires to be placed on an element with either NgForm or FormGroupDirective.'
      );
    }
  }

  public ngOnInit(): void {
    if (this.ngForm) {
      this.form = this.ngForm;
    } else if (this.formGroupDirective) {
      this.form = this.formGroupDirective.control;
    }
  }

  public setValue(value: any): void {
    if (!this.form) {
      return;
    }

    if (this.form instanceof FormGroup) {
      this.form.reset();
      this.form.patchValue(value);
    } else {
      this.form.setValue(value);
    }
  }

  public getValue(): any {
    const formValue = this.form ? this.form.value : {};

    // Omit all filter keys without values.
    // Does not handle nested FormGroups
    return omitBy(formValue, isEmpty);
  }

  public isValid(): boolean {
    return this.form && this.form.valid;
  }

  public clearProp(propName: string): void {
    if (!this.form) {
      return;
    }

    this.form.controls[propName].reset();
  }
}
