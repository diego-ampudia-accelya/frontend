import { GRID_TABLE } from '../../grid-table/grid-table.constants';
import { DataQuery } from '../data-query';

export const listViewConfig = {
  icons: {
    clear: 'mi md-18 clear',
    filter: 'mi md-18 filter_list'
  },

  defaultLabels: {
    filter: 'common.filter',
    download: 'common.download',
    detailedDowload: 'common.detailedDowload',
    create: 'common.create',
    apply: 'common.applyChanges',
    search: 'common.search',
    clear: 'common.clear',
    reset: 'common.reset',
    resetFilters: 'common.resetFilters',
    clearFilters: 'common.clearFilters',
    filterBy: 'common.filterBy',
    tooManyItems: 'common.tooManyItems',
    tooManyItemsTitle: 'common.tooManyItemsTitle'
  }
};

export const defaultQuery: DataQuery = {
  paginateBy: GRID_TABLE.DEFAULT_PAGE_INFO,
  sortBy: [],
  filterBy: {}
};
