import { Component, Inject } from '@angular/core';
import { L10nLocale, L10N_LOCALE } from 'angular-l10n';

import { FooterButton } from '../../buttons/enums/footerButton';
import { DialogConfig } from '../../dialog/dialog.config';
import { ReactiveSubject } from '../../dialog/reactive-subject';

@Component({
  selector: 'bspl-records-limit-dialog',
  templateUrl: './records-limit-dialog.component.html'
})
export class RecordsLimitDialogComponent {
  public get maxSupportedItems(): number {
    return this.config.data.maxSupportedItems;
  }

  public get message(): string {
    return this.config.data.message;
  }

  constructor(
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    public config: DialogConfig,
    private reactiveSubject: ReactiveSubject
  ) {}

  public handleLinkClick($event: any): void {
    const target = $event.target as HTMLElement;
    if (target.classList.contains('filters-link')) {
      this.reactiveSubject.emit(FooterButton.Filter);
    } else if (target.classList.contains('download-link')) {
      this.reactiveSubject.emit(FooterButton.Download);
    }
  }
}
