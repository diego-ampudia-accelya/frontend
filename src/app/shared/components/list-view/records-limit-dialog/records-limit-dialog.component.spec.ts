import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslatePipe, L10N_LOCALE } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';

import { DialogConfig } from '../../dialog/dialog.config';
import { ReactiveSubject } from '../../dialog/reactive-subject';

import { RecordsLimitDialogComponent } from './records-limit-dialog.component';
import { AbsoluteDecimalPipe } from '~app/shared/pipes/absolute-decimal.pipe';

describe('RecordsLimitDialogComponent', () => {
  let component: RecordsLimitDialogComponent;
  let fixture: ComponentFixture<RecordsLimitDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RecordsLimitDialogComponent, MockPipe(L10nTranslatePipe), MockPipe(AbsoluteDecimalPipe)],
      providers: [
        mockProvider(DialogConfig, {
          data: { message: 'test', maxSupportedItems: 100 }
        }),
        mockProvider(ReactiveSubject),
        { provide: L10N_LOCALE, useValue: { language: 'en' } }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordsLimitDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
