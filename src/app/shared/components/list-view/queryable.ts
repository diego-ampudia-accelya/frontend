import { Observable } from 'rxjs';

import { DataQuery } from './data-query';
import { PagedData } from '~app/shared/models/paged-data.model';

export interface Queryable<T> {
  find(query: DataQuery): Observable<PagedData<T>>;
}
