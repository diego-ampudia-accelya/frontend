export interface AppliedFilter {
  keys: string[];
  label: string;
  closable?: boolean;
}
