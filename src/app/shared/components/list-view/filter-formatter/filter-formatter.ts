export interface FilterFormatter<T = any> {
  format(filter: { [key: string]: any }): T;
}
