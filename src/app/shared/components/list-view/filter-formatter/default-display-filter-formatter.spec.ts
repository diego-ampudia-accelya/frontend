import { DefaultDisplayFilterFormatter } from './default-display-filter-formatter';

describe('DefaultDisplayFilterFormatter', () => {
  let formatter: DefaultDisplayFilterFormatter;

  beforeEach(() => {
    formatter = new DefaultDisplayFilterFormatter();
  });

  it('should generate filter labels using default convention', () => {
    const filters = {
      airlineCode: '016',
      currency: 'EUR'
    };

    expect(formatter.format(filters)).toEqual([
      { keys: ['airlineCode'], label: 'Airline Code - 016', closable: true },
      { keys: ['currency'], label: 'Currency - EUR', closable: true }
    ]);
  });

  it('should format dates when date filter is specified', () => {
    spyOn(Date.prototype, 'toLocaleDateString').and.returnValue('2018-08-08');
    const filters = {
      validFrom: new Date(2018, 8, 8)
    };

    expect(formatter.format(filters)).toEqual([
      {
        keys: ['validFrom'],
        label: 'Valid From - 2018-08-08',
        closable: true
      }
    ]);
  });

  it('should format a filter when it is an object with displayValue property', () => {
    const filters = {
      airline: {
        displayValue: 'Lufthansa / 016',
        value: '016'
      }
    };

    expect(formatter.format(filters)).toEqual([
      {
        keys: ['airline'],
        label: 'Airline - Lufthansa / 016',
        closable: true
      }
    ]);
  });

  it('should format a filter when it is an array of objects with displayValue property', () => {
    const filters = {
      airlines: [
        {
          displayValue: 'Lufthansa / 016',
          value: '016'
        },
        {
          displayValue: 'Wizz Air / 017',
          value: '017'
        }
      ]
    };

    expect(formatter.format(filters)).toEqual([
      {
        keys: ['airlines'],
        label: 'Airlines - Lufthansa / 016, Wizz Air / 017',
        closable: true
      }
    ]);
  });

  it('should format `aggregateBy` when it is `none`', () => {
    const filters = {
      aggregateBy: 'none'
    };

    expect(formatter.format(filters)).toEqual([]);
  });

  it('should format `aggregateBy` when a criteria is specified', () => {
    const filters = {
      aggregateBy: 'Airline'
    };

    expect(formatter.format(filters)).toEqual([
      {
        keys: ['aggregateBy'],
        label: 'Aggregated by Airline',
        closable: true
      }
    ]);
  });

  it('should return empty result if filters is null/undefined', () => {
    expect(formatter.format(null)).toEqual([]);
    expect(formatter.format(undefined)).toEqual([]);
  });
});
