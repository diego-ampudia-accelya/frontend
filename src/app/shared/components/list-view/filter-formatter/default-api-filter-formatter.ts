import { Injectable } from '@angular/core';

import { FilterFormatter } from './filter-formatter';

@Injectable()
export class DefaultApiFilterFormatter implements FilterFormatter {
  format(filterBy: { [key: string]: any }): any {
    return filterBy;
  }
}
