import { Injectable } from '@angular/core';
import { capitalize, startCase } from 'lodash';

import { AppliedFilter } from './applied-filter';
import { FilterFormatter } from './filter-formatter';

@Injectable({
  providedIn: 'root'
})
export class DefaultDisplayFilterFormatter implements FilterFormatter {
  format(filters: { [key: string]: any }): AppliedFilter[] {
    filters = filters || {};
    // handle aggregation fields
    for (const filterField of Object.keys(filters)) {
      if (filterField === 'aggregateBy') {
        filters[filterField] = capitalize(filters[filterField]);

        // ignore aggregation field if it's set to 'none'
        if (filters[filterField] === 'None') {
          delete filters[filterField];
        }
      }
    }

    return Object.entries(filters).map(([key, value]) => {
      let label = this.generateTagValue(key, value);

      // handle aggregation fields
      // ToDo: Refactor the logic - should not build a display string out of the field key, or at least think of a better way to handle translations and cases like this:
      if (key === 'aggregateBy') {
        label = label.replace('Aggregate By -', 'Aggregated by');
      }

      return { keys: [key], label, closable: true };
    });
  }

  protected generateTagValue(key: string, value: any): string {
    return this.normalizeKey(key) + this.normalizeValue(value);
  }

  protected normalizeKey(key: string): string {
    return key ? startCase(key) + ' - ' : '';
  }

  protected normalizeValue(value: any): any {
    if (Array.isArray(value)) {
      value = value.map(v => (typeof v === 'object' ? v.displayValue : v)).join(', ');
    } else if (value instanceof Date) {
      value = value.toLocaleDateString();
    } else if (typeof value === 'object') {
      value = value.displayValue;
    }

    return value;
  }
}
