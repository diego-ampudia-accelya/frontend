import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { defer, Observable } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';

import { DataQuery } from './data-query';
import { ViewListActions } from '~app/core/actions';
import { getActiveTab, getViewListState } from '~app/core/reducers';
import { AppState } from '~app/reducers';

export const QUERY_STORAGE_ID_SUFFIX = new InjectionToken('QUERY_STORAGE_ID_SUFFIX');

@Injectable()
export class DefaultQueryStorage {
  public storedQuery$: Observable<DataQuery>;

  private get stateId(): string {
    return `${this.getActiveTabId()}:${this.idSuffix}`;
  }

  constructor(private store: Store<AppState>, @Inject(QUERY_STORAGE_ID_SUFFIX) @Optional() private idSuffix?: string) {
    this.idSuffix = idSuffix || 'default';
    this.storedQuery$ = defer(() => this.getQueryBy(this.stateId));
  }

  public get(): DataQuery {
    // Fetching state from the store is a synchroneous operation
    // We use this to simplify getting the stored query.
    let current: DataQuery;
    this.storedQuery$.pipe(first()).subscribe(value => (current = value));

    return current;
  }

  public save(query: Partial<DataQuery>): void {
    this.store.dispatch(
      new ViewListActions.QueryChange({
        viewListId: this.stateId,
        sorting: query.sortBy,
        paging: query.paginateBy,
        filter: query.filterBy,
        filterFormValue: null
      })
    );
  }

  private getQueryBy(stateId: string): Observable<DataQuery> {
    return this.store.pipe(
      select(getViewListState(stateId)),
      map(info => {
        let query = null;
        if (info) {
          query = {
            filterBy: info.filter,
            paginateBy: info.paging,
            sortBy: info.sorting
          };
        }

        return query;
      })
    );
  }

  private getActiveTabId(): string {
    let tabId: string;
    this.store
      .pipe(
        select(getActiveTab),
        first(),
        tap(({ id }) => (tabId = id))
      )
      .subscribe();

    return tabId;
  }
}
