import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { FooterButton } from '../buttons/enums/footerButton';
import { DialogConfig } from '../dialog/dialog.config';
import { DialogService } from '../dialog/dialog.service';

export enum SuccessEventType {
  itemCreated = 'itemCreated',
  itemModified = 'itemModified'
}

@Injectable()
export class EditDialogService {
  constructor(private dialogService: DialogService) {}

  public open(componentType: Type<any>, config: string | DialogConfig, itemData?: any): Observable<any> {
    let dialogConfig: DialogConfig;

    if (typeof config === 'string') {
      dialogConfig = {
        data: {
          title: config,
          footerButtonsType: FooterButton.Create,
          hasCancelButton: true,
          isClosable: true
        }
      };
    } else {
      dialogConfig = config;
    }

    if (itemData) {
      dialogConfig.data.itemData = itemData;
    }

    return this.dialogService.open(componentType, dialogConfig).pipe(filter(result => !!result.successEventType));
  }

  public close() {
    this.dialogService.close();
  }
}
