import { PaginationInfo } from '~app/shared/models/pagination-info.model';
import { SortingObject } from '~app/shared/models/sorting-object.model';

export interface DataQuery<T = { [key: string]: any }> {
  filterBy: Partial<T>;
  paginateBy: PaginationInfo;
  sortBy: SortingObject[];
}
