import { fakeAsync, tick } from '@angular/core/testing';
import { cold } from 'jasmine-marbles';
import { cloneDeep, times } from 'lodash';
import { of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

import { DataQuery } from './data-query';
import { defaultQuery } from './list-view/constants.config';
import { Queryable } from './queryable';
import { QueryableDataSource } from './queryable-data-source';
import { PagedData } from '~app/shared/models/paged-data.model';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { GLOBALS } from '~app/shared/constants/globals';

describe('QueryableDataSource', () => {
  let dataSource: QueryableDataSource<any>;
  let queryableMock: jasmine.SpyObj<Queryable<any>>;

  beforeEach(() => {
    queryableMock = jasmine.createSpyObj('Queryable', ['find']);
    queryableMock.find.and.returnValue(of({}));

    dataSource = new QueryableDataSource(queryableMock);
  });

  it('should create', () => {
    expect(dataSource).toBeDefined();
  });

  it('should have defaults', () => {
    expect(dataSource.loading$).toBeObservable(cold('a', { a: false }));
    expect(dataSource.appliedQuery$).toBeObservable(cold('a', { a: defaultQuery }));
    expect(dataSource.data$).toBeObservable(cold('-'));
    expect(dataSource.hasData$).toBeObservable(cold('-'));
  });

  it('should load data with default query when no query is specified', () => {
    const response: PagedData<number> = {
      records: times(20, n => n),
      pageNumber: 0,
      pageSize: 20,
      totalPages: 1,
      total: 20
    };
    const expectedDataQuery: DataQuery = {
      sortBy: [],
      filterBy: {},
      paginateBy: { page: 0, size: 20, totalElements: 20 }
    };
    queryableMock.find.and.returnValue(of(response));

    dataSource.get({});

    expect(queryableMock.find).toHaveBeenCalledWith(cloneDeep(defaultQuery));
    expect(dataSource.hasData$).toBeObservable(cold('a', { a: true }));
    expect(dataSource.data$.pipe(map(data => data.length))).toBeObservable(cold('a', { a: 20 }));
    expect(dataSource.appliedQuery$).toBeObservable(cold('q', { q: expectedDataQuery }));
  });

  it('should load data when a query is specified', () => {
    const query: DataQuery = {
      paginateBy: { page: 1, size: 15 },
      sortBy: [{ attribute: 'test', sortType: SortOrder.Desc }],
      filterBy: { test: 1 }
    };

    dataSource.get(query);

    expect(queryableMock.find).toHaveBeenCalledWith(query);
  });

  it('should show loading indication for a minimum amount of time', fakeAsync(() => {
    const response: PagedData<number> = {
      records: times(20, n => n),
      pageNumber: 0,
      pageSize: 20,
      totalPages: 1,
      total: 20
    };
    queryableMock.find.and.returnValue(of(response));

    const actualLoading = [];
    dataSource.loading$.subscribe(isLoading => actualLoading.push(isLoading));

    dataSource.get({});
    tick(GLOBALS.LOADING_SPINNER_DELAY);

    expect(actualLoading).toEqual([false, true, false]);
  }));

  it('should show loading indication while the data is loading', fakeAsync(() => {
    const response: PagedData<number> = {
      records: times(20, n => n),
      pageNumber: 0,
      pageSize: 20,
      totalPages: 1,
      total: 20
    };
    queryableMock.find.and.returnValue(of(response).pipe(delay(GLOBALS.LOADING_SPINNER_DELAY + 100)));

    const actualLoading = [];
    dataSource.loading$.subscribe(isLoading => actualLoading.push(isLoading));

    dataSource.get({});
    tick(GLOBALS.LOADING_SPINNER_DELAY);

    expect(actualLoading).toEqual([false, true]);

    tick(100);

    expect(actualLoading).toEqual([false, true, false]);
  }));

  it('should return empty array', () => {
    const response: Partial<PagedData<number>> = {
      pageNumber: 0,
      pageSize: 20,
      total: 0
    };

    let result = [1, 2];

    dataSource.get({});

    queryableMock.find.and.returnValue(of(response));

    dataSource.data$.subscribe(records => {
      result = records;
    });

    expect(result.length).toEqual(0);
  });
});
