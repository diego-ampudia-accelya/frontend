import { Inject, Injectable, InjectionToken } from '@angular/core';
import { cloneDeep, defaultsDeep } from 'lodash';
import { BehaviorSubject, forkJoin, Observable, ReplaySubject, timer } from 'rxjs';
import { finalize, map, tap } from 'rxjs/operators';

import { DataQuery } from './data-query';
import { defaultQuery } from './list-view/constants.config';
import { Queryable } from './queryable';
import { GLOBALS } from '~app/shared/constants/globals';

export const QUERYABLE = new InjectionToken<Queryable<any>>('Queryable');

@Injectable()
export class QueryableDataSource<T> {
  public loading$: Observable<boolean>;
  public appliedQuery$: Observable<DataQuery>;

  public data$: Observable<Array<T>>;
  public hasData$: Observable<boolean>;

  protected loadingSubject = new BehaviorSubject(false);
  protected dataSubject = new ReplaySubject<Array<T>>(1);
  protected appliedQuerySubject = new BehaviorSubject<DataQuery>(defaultQuery);

  constructor(@Inject(QUERYABLE) protected queryable: Queryable<T>) {
    this.loading$ = this.loadingSubject.asObservable();
    this.appliedQuery$ = this.appliedQuerySubject.asObservable();

    this.data$ = this.dataSubject.asObservable();
    this.hasData$ = this.data$.pipe(map(data => data && data.length > 0));
  }

  public get(query?: Partial<DataQuery>): void {
    const defaultDataQuery = cloneDeep(defaultQuery);
    const normalizedQuery = defaultsDeep(query, defaultDataQuery);
    this.withLoading(
      this.queryable.find(cloneDeep(normalizedQuery)).pipe(
        tap(
          result => {
            this.dataSubject.next(result.records || []);
            const appliedQuery: DataQuery = {
              ...normalizedQuery,
              paginateBy: {
                size: result.pageSize,
                page: result.pageNumber,
                totalElements: result.total
              }
            };
            this.appliedQuerySubject.next(appliedQuery);
          },
          () => {
            this.appliedQuerySubject.next(defaultDataQuery);
            this.dataSubject.next([]);
          }
        )
      )
    ).subscribe();
  }

  protected withLoading<U>(observable: Observable<U>): Observable<U> {
    this.loadingSubject.next(true);

    // Make sure that the spinner is shown for at least a short while before hiding it.
    return forkJoin([timer(GLOBALS.LOADING_SPINNER_DELAY), observable]).pipe(
      map(([, result]) => result),
      finalize(() => this.loadingSubject.next(false))
    );
  }
}
