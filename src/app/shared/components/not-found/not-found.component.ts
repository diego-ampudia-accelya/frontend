import { Component } from '@angular/core';

@Component({
  selector: 'bspl-not-found',
  template: ` <ng-content></ng-content> `
})
export class NotFoundComponent {}
