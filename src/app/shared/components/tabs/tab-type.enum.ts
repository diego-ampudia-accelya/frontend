/* eslint-disable @typescript-eslint/naming-convention */
export enum TabType {
  Primary = 'primary',
  Secondary = 'secondary'
}
