import { ChangeDetectorRef, Component, Input } from '@angular/core';

import { Tab } from '../tab.model';

@Component({
  selector: 'bspl-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements Tab {
  @Input() title: string;
  @Input() iconSrc: string;
  @Input() active: boolean;
  @Input() closable: boolean;
  @Input() closed: boolean;
  @Input() disabled: boolean;

  get isContentVisible(): boolean {
    return !this.disabled && this.active && !closed;
  }

  constructor(public changeDetectorRef: ChangeDetectorRef) {}
}
