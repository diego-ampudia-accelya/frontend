import { waitForAsync } from '@angular/core/testing';

import { Tab } from '../tab.model';

import { TabMenuComponent } from './tab-menu.component';

describe('TabMenuComponent', () => {
  let component: TabMenuComponent;
  let enabledTab: Tab;
  let anotherEnabledTab: Tab;
  let disabledTab: Tab;
  let closableTab: Tab;
  let closedTab: Tab;

  beforeEach(() => {
    component = new TabMenuComponent();
    enabledTab = {
      title: 'enabled tab'
    };
    anotherEnabledTab = {
      title: 'another enabled tab'
    };
    disabledTab = {
      title: 'disabled tab',
      disabled: true
    };
    closableTab = {
      title: 'closable tab',
      closable: true
    };
    closedTab = {
      title: 'closed tab',
      closed: true
    };
    component.tabs = [enabledTab, anotherEnabledTab, disabledTab, closableTab, closedTab];
  });

  describe('when calling activateTab', () => {
    it('should mark a as active when it is selected', async () => {
      await component.activateTab(anotherEnabledTab);

      expect(anotherEnabledTab.active).toBeTruthy();
    });

    it('should emit "activated" event when a is selected', async () => {
      let isActivated = false;
      component.activated.subscribe(() => (isActivated = true));

      await component.activateTab(enabledTab);

      expect(isActivated).toBeTruthy();
    });

    it('should ignore disabled tabs', async () => {
      await component.activateTab(disabledTab);

      expect(component.tabs.some(tab => tab.active)).toBeFalsy();
    });

    it('should ignore closed tabs', async () => {
      await component.activateTab(closedTab);

      expect(component.tabs.some(tab => tab.active)).toBeFalsy();
    });

    it('should maintain a single active tab', async () => {
      await component.activateTab(enabledTab);
      await component.activateTab(anotherEnabledTab);

      expect(component.tabs.filter(tab => tab.active).length).toBe(1);
      expect(anotherEnabledTab.active).toBeTruthy();
    });
  });

  describe('when calling closeTab', () => {
    it('should close closable tab', () => {
      component.closeTab(closableTab);

      expect(component.openedTabs.indexOf(closableTab)).toBe(-1);
    });

    it('should activate previous tab when the active is closed', async () => {
      await component.activateTab(closableTab);
      let actual = null;
      component.activated.subscribe(tab => (actual = tab));

      await component.closeTab(closableTab);

      expect(component.openedTabs.find(tab => tab.active)).toBe(anotherEnabledTab);
      expect(actual).toBe(anotherEnabledTab);
    });

    it('should emit closed event when a tab is closed', waitForAsync(() => {
      let actualClosed = null;
      component.closed.subscribe(tab => (actualClosed = tab));

      component.closeTab(closableTab);

      expect(actualClosed).toBe(closableTab);
    }));

    it('should ignore tabs when they are not closable', () => {
      component.closeTab(enabledTab);

      expect(component.openedTabs.length).toBe(4);
    });
  });
});
