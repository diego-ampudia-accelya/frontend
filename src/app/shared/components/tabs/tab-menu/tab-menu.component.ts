import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';

import { TabType } from '../tab-type.enum';
import { Tab } from '../tab.model';
import { canProceed } from '~app/shared/utils';

@Component({
  selector: 'bspl-tab-menu',
  templateUrl: './tab-menu.component.html',
  styleUrls: ['./tab-menu.component.scss']
})
export class TabMenuComponent {
  @Input() tabs: Tab[];
  @Input() type: TabType = TabType.Primary;
  @Input() beforeActivateTab$: Observable<boolean>;

  @Output() activated = new EventEmitter<Tab>();
  @Output() beforeActivated = new EventEmitter<{ tab: Tab; preventDefault: () => void }>();
  @Output() closed = new EventEmitter<Tab>();

  public get openedTabs(): Tab[] {
    return this.tabs.filter(tab => !tab.closed);
  }

  public async activateTab(tab: Tab): Promise<void> {
    // TODO: tab.active should be moved within canActivate but, findPreviousActivatable should be changed.
    if (!this.canActivate(tab) || tab.active) {
      return;
    }

    if (!(await canProceed(this.beforeActivateTab$))) {
      return;
    }

    let shouldActivate = true;
    this.beforeActivated.emit({ tab, preventDefault: () => (shouldActivate = false) });

    if (shouldActivate) {
      this.deactivateAllTabs();
      tab.active = true;
      this.activated.emit(tab);
    }
  }

  public async closeTab(tab: Tab): Promise<void> {
    if (!tab.closable) {
      return;
    }

    if (tab.active) {
      await this.activatePrevious(tab);
    }

    tab.closed = true;
    tab.active = false;
    this.closed.emit(tab);
  }

  public tabMiddleBtnClicked(tab: Tab): void {
    if (tab.closable) {
      this.closeTab(tab);
    }
  }

  private async activatePrevious(tab: Tab): Promise<void> {
    const previous = this.findPreviousActivatable(tab);
    if (previous) {
      await this.activateTab(previous);
    }
  }

  private findPreviousActivatable(tab: Tab): Tab {
    // Capture index of current tab
    let activatableTabs = this.tabs.filter(other => this.canActivate(other));
    const tabIndex = activatableTabs.indexOf(tab);

    // Remove current tab from activatable tabs.
    activatableTabs = activatableTabs.filter(other => other !== tab);

    // Find previous activatable tab
    const previousIndex = Math.max(tabIndex - 1, 0);

    return activatableTabs[previousIndex];
  }

  private canActivate(tab: Tab) {
    return !tab.disabled && !tab.closed;
  }

  private deactivateAllTabs(): void {
    this.tabs.forEach(tab => (tab.active = false));
  }
}
