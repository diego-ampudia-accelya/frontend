import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { createSpyObject, mockProvider, SpyObject } from '@ngneat/spectator';
import { L10nTranslatePipe } from 'angular-l10n';
import { MockPipe } from 'ng-mocks';

import { TabLinkComponent } from './tab-link.component';

describe('TabLinkComponent', () => {
  let component: TabLinkComponent;
  let fixture: ComponentFixture<TabLinkComponent>;
  let routerLinkSpy: SpyObject<RouterLink>;
  let routerSpy: SpyObject<Router>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TabLinkComponent, MockPipe(L10nTranslatePipe)],
      providers: [mockProvider(Router), mockProvider(ActivatedRoute), mockProvider(RouterLink)],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    routerLinkSpy = TestBed.inject<any>(RouterLink);
    routerSpy = TestBed.inject<any>(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('activate', () => {
    it('should navigate when enabled', () => {
      component.disabled = false;

      component.activate();

      expect(routerLinkSpy.onClick).toHaveBeenCalled();
    });

    it('should not navigate when disabled', () => {
      component.disabled = true;

      component.activate();

      expect(routerLinkSpy.onClick).not.toHaveBeenCalled();
    });

    it('should not navigate when already active', () => {
      routerSpy.isActive.and.returnValue(true);

      component.activate();

      expect(routerLinkSpy.onClick).not.toHaveBeenCalled();
    });
  });

  describe('onClick', () => {
    let clickEvent: SpyObject<MouseEvent>;

    beforeEach(() => {
      clickEvent = createSpyObject(MouseEvent);
    });

    it('should stop propagation', () => {
      component.onClick(clickEvent);

      expect(clickEvent.stopPropagation).toHaveBeenCalled();
    });

    it('should navigate', () => {
      component.onClick(clickEvent);

      expect(routerLinkSpy.onClick).toHaveBeenCalled();
    });
  });
});
