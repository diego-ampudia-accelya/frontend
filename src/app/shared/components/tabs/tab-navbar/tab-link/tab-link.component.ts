import { Component, Input } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'bspl-tab-link',
  templateUrl: './tab-link.component.html',
  styleUrls: ['./tab-link.component.scss']
})
export class TabLinkComponent {
  @Input() label: string;
  @Input() disabled: boolean;

  private get canActivate(): boolean {
    return !this.active && !this.disabled;
  }

  public get active(): boolean {
    return !this.disabled && this.router.isActive(this.routerLink.urlTree, false);
  }

  constructor(private routerLink: RouterLink, protected router: Router) {}

  public activate(): void {
    if (this.canActivate) {
      this.routerLink.onClick();
    }
  }

  public onClick($event: MouseEvent) {
    $event.stopPropagation();
    this.activate();
  }
}
