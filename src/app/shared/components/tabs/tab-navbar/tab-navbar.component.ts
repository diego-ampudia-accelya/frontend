import { AfterContentInit, Component, ContentChildren, QueryList } from '@angular/core';

import { TabLinkComponent } from './tab-link/tab-link.component';

@Component({
  selector: 'bspl-tab-navbar',
  templateUrl: './tab-navbar.component.html',
  styleUrls: ['./tab-navbar.component.scss']
})
export class TabNavbarComponent implements AfterContentInit {
  @ContentChildren(TabLinkComponent) tabs: QueryList<TabLinkComponent>;

  public get activeTab(): TabLinkComponent {
    return this.tabs.find(tab => tab.active);
  }

  public ngAfterContentInit(): void {
    if (!this.activeTab) {
      const firstEnabled = this.tabs.find(tab => !tab.disabled);
      firstEnabled?.activate();
    }
  }
}
