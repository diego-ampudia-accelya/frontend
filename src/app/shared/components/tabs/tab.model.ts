export interface Tab {
  title: string;
  iconSrc?: string;
  closable?: boolean;
  active?: boolean;
  closed?: boolean;
  disabled?: boolean;
}
