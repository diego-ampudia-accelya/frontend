import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  Output,
  QueryList
} from '@angular/core';

import { TabType } from '~app/shared/components/tabs/tab-type.enum';
import { TabComponent } from '~app/shared/components/tabs/tab/tab.component';

@Component({
  selector: 'bspl-tab-group',
  templateUrl: './tab-group.component.html'
})
export class TabGroupComponent implements AfterViewInit {
  @Input() type: TabType = TabType.Primary;

  @Output() closed = new EventEmitter();

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  private get openedTabs(): TabComponent[] {
    return this.tabs.filter(tab => !tab.closed) || [];
  }

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  public ngAfterViewInit(): void {
    if (!this.hasActiveTab()) {
      this.activateFirstTab();
    }
  }

  public closeTab(): void {
    this.closed.emit();
  }

  public getCurrentTab() {
    const openedTabs = this.openedTabs.filter(tab => tab.active);

    return openedTabs[0].title;
  }

  public activateFirstTab(): void {
    const firstActivatableTab = this.openedTabs.find(t => !t.disabled);
    if (firstActivatableTab) {
      // need to deactivate all other tabs
      this.closeAllTabs();

      firstActivatableTab.active = true;
      this.detectChangesForTab(firstActivatableTab);
    }
  }

  public activateTab(tabTitle: string): void {
    const tabToActivate = this.tabs.filter(t => t.title === tabTitle)[0];
    if (tabToActivate) {
      this.closeAllTabs();
      tabToActivate.active = true;
      this.detectChangesForTab(tabToActivate);
    }
  }

  private hasActiveTab(): boolean {
    return this.openedTabs.some(tab => tab.active);
  }

  private closeAllTabs() {
    this.openedTabs.filter(t => t.active).forEach(t => (t.active = false));
  }

  private detectChangesForTab(tab) {
    this.changeDetectorRef.detectChanges();
    tab.changeDetectorRef.detectChanges();
  }
}
