import { RangeDropdownUnit } from '../models/range-dropdown.model';

export const unitPluralMapping = {
  [RangeDropdownUnit.Day]: {
    '=0': 'rangeDropdown.units.day.plural',
    '=1': 'rangeDropdown.units.day.singular',
    other: 'rangeDropdown.units.day.plural'
  }
};
