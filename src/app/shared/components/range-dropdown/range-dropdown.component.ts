import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild
} from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { L10nLocale, L10nTranslationService, L10N_LOCALE } from 'angular-l10n';
import { cloneDeep } from 'lodash';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';
import { CustomControl } from '~app/shared/base/custom-control';
import { numericValidator } from '~app/shared/validators/numeric.validator';
import { unitPluralMapping } from './config/range-dropdown.config';
import { RangeDropdownUnit } from './models/range-dropdown.model';
import { rangeFromValidator, rangeToValidator } from './validators/range.validator';

@Component({
  selector: 'bspl-range-dropdown',
  templateUrl: './range-dropdown.component.html',
  styleUrls: ['./range-dropdown.component.scss']
})
export class RangeDropdownComponent extends CustomControl implements OnInit, OnDestroy {
  @ViewChild('overlayPanel') overlayPanel: OverlayPanel;
  @ViewChild('targetElement') targetElement: ElementRef;

  @Input() heading: string;
  @Input() placeholder = 'rangeDropdown.placeholder';
  @Input() min: number;
  @Input() max: number;
  @Input() unit: RangeDropdownUnit;

  public fromControl: FormControl;
  public toControl: FormControl;
  public fromPlaceholder: string;
  public toPlaceholder: string;

  public isSliderVisible: boolean;
  public unitPluralMapping = unitPluralMapping;

  public isOverlayOpened: boolean;

  public isRangeInputFocussed: boolean;

  // TODO Investigate why it does not work
  // private openingOverlay$ = new EventEmitter();
  // private closingOverlay$ = new EventEmitter();
  private destroy$ = new Subject();

  constructor(
    @Self() @Optional() public ngControl: NgControl,
    cd: ChangeDetectorRef,
    translationService: L10nTranslationService,
    @Inject(L10N_LOCALE) public locale: L10nLocale
  ) {
    super(ngControl, cd, translationService);
  }

  public ngOnInit(): void {
    this.runInitialChecks();
    this.buildForm();
    this.initializeListeners();
    this.initializeSliderVisibility();
  }

  // public ngAfterViewInit(): void {
  // this.initializeOverlayListeners();
  // }

  public writeValue(value: any) {
    const clonedValue = cloneDeep(value);

    super.writeValue(clonedValue);
  }

  public clear() {
    this.ngControl.reset(null);
  }

  //* If this method is called coming from a click event, make sure event's propagation has been stopped
  public focusElement(): void {
    this.overlayPanel.show({ target: this.targetElement.nativeElement });
  }

  public onRangeInputFocus() {
    this.isRangeInputFocussed = true;
  }
  public onRangeInputBlur() {
    this.isRangeInputFocussed = false;
  }

  private runInitialChecks() {
    if (!this.ngControl) {
      throw new Error('RangeDropdownComponent must be used within a ReactiveForm, receiving a FormControl instance');
    }

    if ((isNaN(this.min) && !isNaN(this.max)) || (!isNaN(this.min) && isNaN(this.max))) {
      throw new Error('RangeDropdownComponent must receive both a min and max range input values or none at all');
    }
  }

  private buildForm() {
    this.fromControl = new FormControl('', { updateOn: 'change' });
    this.toControl = new FormControl('', { updateOn: 'change' });
    this.fromControl.setValidators([numericValidator, rangeFromValidator(this.toControl, this.min, this.max)]);
    this.toControl.setValidators([numericValidator, rangeToValidator(this.fromControl, this.min, this.max)]);

    this.fromPlaceholder = !isNaN(this.min) ? this.min.toString() : '';
    this.toPlaceholder = !isNaN(this.max) ? this.max.toString() : '';
  }

  private initializeListeners() {
    //* When the main control is updated, we also update the from and to inputs with the new range
    this.ngControl.valueChanges
      .pipe(
        map(value => (Array.isArray(value) ? value : ['', ''])),
        takeUntil(this.destroy$)
      )
      .subscribe(([from, to]) => {
        this.fromControl.setValue(from, { emitEvent: false });
        this.toControl.setValue(to, { emitEvent: false });
      });

    this.fromControl.valueChanges
      .pipe(
        tap(() => {
          //* When the user updates one input, we update the other one with the min/max instead of leaving it blank
          if (!isNaN(this.max) && this.toControl.value === '') {
            this.toControl.setValue(this.max, { emitEvent: false });
          }
        }),
        tap(() => {
          //* Updating validators over controls
          this.fromControl.updateValueAndValidity({ emitEvent: false });
          this.toControl.updateValueAndValidity({ emitEvent: false });
        }),
        filter(() => this.fromControl.valid && this.toControl.valid),
        tap(from => {
          //* We prevent values like 00003 to be typed
          if (this.fromControl.value !== '') {
            this.fromControl.setValue(Number(from), { emitEvent: false });
          }
        }),
        map(() => {
          //* The main control (ngControl) might not have the same values of both inputs
          //* This is to allow the user to leave one input blank but still show From 0 - To 100 (min = 0, max = 100)
          const updatedRange = [this.fromControl.value, this.toControl.value];

          if (this.fromControl.value === '') {
            updatedRange[0] = !isNaN(this.min) ? this.min : null;
          }

          //* We do not want empty strings to be propagated as values. We propagate instead null
          updatedRange[1] = updatedRange[1] !== '' ? updatedRange[1] : null;

          return updatedRange;
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(range => this.ngControl.control.setValue(range, { emitEvent: false }));

    this.toControl.valueChanges
      .pipe(
        tap(() => {
          //* When the user updates one input, , we update the other one with the min/max instead of leaving it blank
          if (!isNaN(this.min) && this.fromControl.value === '') {
            this.fromControl.setValue(this.min, { emitEvent: false });
          }
        }),
        tap(() => {
          //* Updating validators over controls
          this.fromControl.updateValueAndValidity({ emitEvent: false });
          this.toControl.updateValueAndValidity({ emitEvent: false });
        }),
        filter(() => this.fromControl.valid && this.toControl.valid),
        tap(to => {
          //* We prevent values like 00003 to be typed
          if (this.toControl.value !== '') {
            this.toControl.setValue(Number(to), { emitEvent: false });
          }
        }),
        map(() => {
          //* The main control (ngControl) might not have the same values of both inputs
          //* This is to allow the user to leave one input blank but still show From 0 - To 100 (min = 0, max = 100)
          const updatedRange = [this.fromControl.value, this.toControl.value];

          if (this.toControl.value === '') {
            updatedRange[1] = !isNaN(this.max) ? this.max : null;
          }

          //* We do not want empty strings to be propagated as values. We propagate instead null
          updatedRange[0] = updatedRange[0] !== '' ? updatedRange[0] : null;

          return updatedRange;
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(range => this.ngControl.control.setValue(range, { emitEvent: false }));

    //* We update control so from and to inputs are updated with the initial values (if any)
    if (this.value) {
      this.ngControl.control.updateValueAndValidity();
    }
  }

  private initializeSliderVisibility() {
    //* Slider only visible when min and max are provided
    this.isSliderVisible = !isNaN(this.min) && !isNaN(this.max);
  }

  // TODO Investigate why it does not work
  // private initializeOverlayListeners() {
  //   this.overlayPanel.onHide = this.closingOverlay$;
  //   this.overlayPanel.onShow = this.openingOverlay$;

  //   this.closingOverlay$.pipe(takeUntil(this.destroy$)).subscribe(() => (this.isOverlayOpened = true));
  //   this.openingOverlay$.pipe(takeUntil(this.destroy$)).subscribe(() => (this.isOverlayOpened = false));
  // }

  public ngOnDestroy(): void {
    this.destroy$.next();
  }
}
