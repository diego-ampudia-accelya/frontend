import { AbstractControl, FormControl, ValidatorFn } from '@angular/forms';

/**
 * Validates if `From` control value is **greater** than `min` and **lower** than `To` control value
 *
 * @param toControl control for end of range
 * @param min lowest number that can be part of the range
 * @returns error object if requirements above are not met
 */
export function rangeFromValidator(toControl: FormControl, min: number, max: number): ValidatorFn {
  return (fromControl: AbstractControl): { [key: string]: boolean } => {
    const fromHasValue = fromControl.value !== '' && fromControl.value != null;
    const toHasValue = toControl.value !== '' && toControl.value != null;

    //* We only want one range control showing errors and control must have value
    if (toControl.invalid || !fromHasValue) {
      return;
    }

    let result = null;

    //* Values are string so we convert them into numbers for comparison
    const from = Number(fromControl.value);
    const to = Number(toControl.value);

    if (from > to && toHasValue) {
      //* From new value cannot be greater than to
      result = {
        lowerValue: {
          maxValue: to
        }
      };
    } else if (!isNaN(min) && from < min) {
      //* From new value cannot be lower than min
      result = {
        greaterValue: {
          maxValue: min
        }
      };
    } else if (!isNaN(max) && from > max) {
      //* From new value cannot be greater than max
      result = {
        lowerValue: {
          maxValue: max
        }
      };
    }

    return result;
  };
}

/**
 * Validates if `To` control value is **lower** than `max` and **greater** than `From` control value
 *
 * @param fromControl control for beggining of range
 * @param max greatest number that can be part of the range
 * @returns error object if requirements above are not met
 */
export function rangeToValidator(fromControl: FormControl, min: number, max: number): ValidatorFn {
  return (toControl: AbstractControl): { [key: string]: boolean } => {
    const fromHasValue = fromControl.value !== '' && fromControl.value != null;
    const toHasValue = toControl.value !== '' && toControl.value != null;

    //* We only want one range control showing errors and control must have value
    if (fromControl.invalid || !toHasValue) {
      return;
    }

    let result = null;

    //* Values are string so we convert them into numbers for comparison
    const from = Number(fromControl.value);
    const to = Number(toControl.value);

    if (to < from && fromHasValue) {
      //* To new value cannot be lower than from
      result = {
        greaterValue: {
          maxValue: from
        }
      };
    } else if (!isNaN(max) && to > max) {
      //* To new value cannot be greater than max
      result = {
        lowerValue: {
          maxValue: max
        }
      };
    } else if (!isNaN(min) && to < min) {
      //* To new value cannot be lower than min
      result = {
        greaterValue: {
          maxValue: min
        }
      };
    }

    return result;
  };
}
