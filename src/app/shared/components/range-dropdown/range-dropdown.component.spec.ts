import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { L10nTranslationModule, L10nTranslationService } from 'angular-l10n';
import { EMPTY } from 'rxjs';

import { RangeDropdownComponent } from './range-dropdown.component';
import { l10nConfig } from '~app/shared/base/conf/l10n.config';

describe('RangeDropdownComponent', () => {
  let component: RangeDropdownComponent;
  let fixture: ComponentFixture<RangeDropdownComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RangeDropdownComponent],
      providers: [L10nTranslationService],
      imports: [L10nTranslationModule.forRoot(l10nConfig)]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeDropdownComponent);
    component = fixture.componentInstance;
    component.ngControl = { control: new FormControl(), valueChanges: EMPTY } as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
