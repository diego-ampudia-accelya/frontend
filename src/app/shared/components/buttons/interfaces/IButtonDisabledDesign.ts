export interface IButtonDisabledDesign {
  [buttonCssClass: string]: {
    disabledButtonCssClass: string;
  };
}
