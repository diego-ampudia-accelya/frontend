import { DOCUMENT } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';

import { ButtonDesign } from '../enums/buttonDesign';

@Component({
  selector: 'bspl-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ButtonComponent {
  @Input() label: string;
  @Input() type: 'button' | 'submit' | 'reset' = 'button';
  @Input() design = ButtonDesign.Primary;
  @Input() icon: string;
  @Input() iconRight: string;
  @Input() image: string;
  @Input() isDisabled = false;
  @Input() isLoading = false;

  @ViewChild('buttonRef', { static: true }) buttonRef: ElementRef<HTMLElement>;

  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() onClick = new EventEmitter();

  constructor(@Inject(DOCUMENT) private document: Document) {}

  public click(event: Event) {
    if (this.isDisabled || this.isLoading) {
      event.stopPropagation();
    } else {
      this.onClick.emit();
    }
  }

  public focus() {
    this.buttonRef.nativeElement.focus();
  }

  public isFocused() {
    return this.document.activeElement === this.buttonRef.nativeElement;
  }
}
