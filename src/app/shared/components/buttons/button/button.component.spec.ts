import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockComponent } from 'ng-mocks';
import { fromEvent } from 'rxjs';

import { ButtonDesign } from '../enums/buttonDesign';
import { ButtonComponent } from './button.component';
import { IconButtonType, SpinnerComponent } from '~app/shared/components';
import { TranslatePipeMock } from '~app/test';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let button: HTMLButtonElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [ButtonComponent, TranslatePipeMock, MockComponent(SpinnerComponent)],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button')).nativeElement;
    fixture.detectChanges();
  });

  describe('Click:', () => {
    beforeEach(() => {
      spyOn(component.onClick, 'emit');
    });

    it('should not emit onClick if the the button is disabled', () => {
      fromEvent(fixture.nativeElement, 'click').subscribe(() => fail());

      component.isDisabled = true;
      fixture.detectChanges();
      button.click();

      expect(component.onClick.emit).not.toHaveBeenCalled();
    });

    it('should not emit onClick if the the button is in loading state', () => {
      fromEvent(fixture.nativeElement, 'click').subscribe(() => fail());

      component.isLoading = true;
      fixture.detectChanges();
      button.click();

      expect(component.onClick.emit).not.toHaveBeenCalled();
    });

    it('should emit onClick if the the button is clicked', () => {
      let clickPropagated = false;
      fromEvent(fixture.nativeElement, 'click').subscribe(() => (clickPropagated = true));

      fixture.detectChanges();
      button.click();

      expect(component.onClick.emit).toHaveBeenCalled();
      expect(clickPropagated).toBe(true);
    });
  });

  describe('Design type:', function () {
    const classPrefix = 'bspl-btn-';
    it(`Default type should be ${ButtonDesign.Primary}`, () => {
      expect(button.className).toContain(classPrefix + ButtonDesign.Primary);
    });

    it(`Type should be ${ButtonDesign.Secondary}`, () => {
      component.design = ButtonDesign.Secondary;
      fixture.detectChanges();
      expect(button.classList).toContain(classPrefix + ButtonDesign.Secondary);
    });

    it(`Type should be ${ButtonDesign.Tertiary}`, () => {
      component.design = ButtonDesign.Tertiary;
      fixture.detectChanges();
      expect(button.classList).toContain(classPrefix + ButtonDesign.Tertiary);
    });

    it(`Type should be ${ButtonDesign.Icon}`, () => {
      component.design = ButtonDesign.Icon;
      fixture.detectChanges();
      expect(button.classList).toContain(classPrefix + ButtonDesign.Icon);
    });
  });

  describe('Disabled state:', function () {
    it('State should be disabled', () => {
      component.isDisabled = true;
      fixture.detectChanges();
      expect(button.disabled).toBeTruthy();
    });

    it('State should be enabled', () => {
      component.isDisabled = false;
      fixture.detectChanges();
      expect(button.disabled).toBeFalsy();
    });
  });

  describe('Icons:', function () {
    it('Icon should be on left', () => {
      component.icon = IconButtonType.Add;
      fixture.detectChanges();
      expect(By.css('.icon-start')).toBeTruthy();
    });

    it('Icon should be on right', () => {
      component.icon = IconButtonType.Add;
      fixture.detectChanges();
      expect(By.css('.icon-end')).toBeTruthy();
    });
  });
});
