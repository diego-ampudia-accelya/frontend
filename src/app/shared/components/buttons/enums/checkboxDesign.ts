/* eslint-disable @typescript-eslint/naming-convention */
export enum CheckboxDesign {
  Normal = 'normal',
  Minus = 'minus'
}
