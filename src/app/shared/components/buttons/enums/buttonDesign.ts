/* eslint-disable @typescript-eslint/naming-convention */
export enum ButtonDesign {
  Primary = 'primary',
  Secondary = 'secondary',
  Tertiary = 'tertiary',
  Icon = 'icon'
}
