/* eslint-disable @typescript-eslint/naming-convention */
export enum IconButtonType {
  Gearwheel = 'settings',
  Add = 'add',
  Delete = 'delete',
  ArrowDown = 'keyboard_arrow_down'
}
