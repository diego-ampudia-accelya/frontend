import { Component, Input } from '@angular/core';

@Component({
  selector: 'bspl-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss']
})
export class IconButtonComponent {
  @Input() icon: string;
  @Input() type: 'default' | 'secondary' | 'tertiary' = 'default';
  @Input() isDisabled = false;
  @Input() isLoading = false;
  @Input() highlighted = false;
  @Input() color: 'success' | 'danger' | 'dark' | 'default' = 'default';
}
