/* eslint-disable @angular-eslint/no-output-native */
import {
  CdkOverlayOrigin,
  ConnectedPosition,
  Overlay,
  OverlayConfig,
  OverlayRef,
  RepositionScrollStrategy,
  ScrollDispatcher,
  ViewportRuler
} from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  NgZone,
  OnChanges,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { groupBy } from 'lodash';

import { ButtonDesign } from '../buttons/enums/buttonDesign';
import { IconButtonType } from '../buttons/enums/iconButtonType';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';

@Component({
  selector: 'bspl-button-menu',
  templateUrl: './button-menu.component.html',
  styleUrls: ['./button-menu.component.scss']
})
export class ButtonMenuComponent implements AfterViewInit, OnChanges {
  @Input() label: string;
  @Input() options: ButtonMenuOption[] = [];
  @Input() btnDesign = ButtonDesign.Primary;
  @Input() disabled = false;
  @Input() displaySingleOptionAsButton = false;
  @Input() displayTriggerAsText = false;

  @Output() open = new EventEmitter();
  @Output() close = new EventEmitter();
  @Output() select = new EventEmitter();

  @ViewChild(CdkOverlayOrigin, { static: false }) overlayOrigin: CdkOverlayOrigin;
  @ViewChild('buttonMenu', { static: true }) buttonMenu: TemplateRef<any>;

  public btnIcon = IconButtonType.ArrowDown;
  public isOverlayOpened = false;
  public groupedOptions;

  public get groupedOptionKeys() {
    let res = [];
    if (this.groupedOptions) {
      res = Object.keys(this.groupedOptions);
    }

    return res;
  }

  private overlayRef: OverlayRef;
  private buttonMenuPortal: TemplatePortal;

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    private scrollDispatcher: ScrollDispatcher,
    private viewPortRuler: ViewportRuler,
    private ngZone: NgZone
  ) {}

  ngAfterViewInit() {
    if (!this.displaySingleOptionAsButton) {
      const config = this.getOverlayConfig();
      this.overlayRef = this.overlay.create(config);
      this.buttonMenuPortal = new TemplatePortal(this.buttonMenu, this.viewContainerRef);

      this.overlayRef.backdropClick().subscribe(() => this.closeMenu());

      this.options = this.setDefaultGroup(this.options);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.options && changes.options.currentValue !== changes.options.previousValue) {
      this.options = this.setVisibleState(this.options);
      this.options = this.options.filter(option => !option.hidden);
      this.groupedOptions = groupBy(this.options, 'group');
    }
  }

  public openMenu() {
    if (this.isOverlayOpened || this.disabled) {
      return;
    }

    this.overlayRef.attach(this.buttonMenuPortal);
    this.isOverlayOpened = true;
    this.open.emit();
  }

  public closeMenu(): void {
    this.overlayRef.detach();
    this.isOverlayOpened = false;
    this.close.emit();
  }

  public selectOption(option: ButtonMenuOption) {
    if (!option || option.isDisabled) {
      return;
    }

    if (option.command) {
      option.command();
    }

    this.closeMenu();
    this.select.emit(option);
  }

  private setDefaultGroup(options: ButtonMenuOption[]) {
    options = options || [];

    return options.map(option => ({ ...option, group: option.group || 'default' }));
  }

  private setVisibleState(options) {
    options = options || [];

    return options.map(option => ({ ...option, hidden: option.hidden || false }));
  }

  private getOverlayConfig() {
    const fromBottom: ConnectedPosition = {
      originX: 'end',
      originY: 'bottom',
      overlayX: 'end',
      overlayY: 'top',
      offsetY: 5
    };

    const fromTop: ConnectedPosition = {
      originX: 'end',
      originY: 'top',
      overlayX: 'end',
      overlayY: 'bottom',
      offsetY: -5
    };

    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.overlayOrigin.elementRef)
      .withPositions([fromBottom, fromTop])
      .withFlexibleDimensions(false)
      .withPush(false);

    const scrollStrategy = new RepositionScrollStrategy(this.scrollDispatcher, this.viewPortRuler, this.ngZone);

    return new OverlayConfig({
      positionStrategy,
      hasBackdrop: true,
      backdropClass: 'cdk-overlay-transparent-backdrop',
      scrollStrategy
    });
  }
}
