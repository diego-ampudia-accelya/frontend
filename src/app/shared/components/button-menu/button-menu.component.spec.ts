import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ButtonMenuComponent } from './button-menu.component';
import { ButtonMenuOption } from '~app/shared/models/button-menu-option.model';
import { TranslatePipeMock } from '~app/test';

describe('ButtonMenuComponent', () => {
  let component: ButtonMenuComponent;
  let fixture: ComponentFixture<ButtonMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonMenuComponent, TranslatePipeMock],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [OverlayModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set the options to empty array after setDefaultGroup is called with options which are null', () => {
      component.options = null;

      component.ngAfterViewInit();

      expect(component.options).toEqual([]);
    });
  });

  it('should emit when overlay is opened on openMenu', () => {
    spyOn(component.open, 'emit');
    component.openMenu();

    expect(component.open.emit).toHaveBeenCalled();
    expect(component.isOverlayOpened).toBe(true);
  });

  it('should not emit opened if overlay is already opened', () => {
    component.openMenu();
    spyOn(component.open, 'emit');

    component.openMenu();

    expect(component.open.emit).not.toHaveBeenCalled();
  });

  it('should not emit opened if button-menu is disabled', () => {
    component.disabled = true;
    spyOn(component.open, 'emit');

    component.openMenu();

    expect(component.open.emit).not.toHaveBeenCalled();
  });

  it('should emit close when closeMenu method is called', () => {
    component.openMenu();
    spyOn(component.close, 'emit');

    component.closeMenu();

    expect(component.close.emit).toHaveBeenCalled();
    expect(component.isOverlayOpened).toBe(false);
  });

  describe('selectOption', () => {
    it('should emit change with selected option and execute command in the option', () => {
      const option: ButtonMenuOption = {
        label: 'Issue ADM',
        command: () => {}
      };

      spyOn(option, 'command');
      spyOn(component.select, 'emit');

      component.selectOption(option);

      expect(option.command).toHaveBeenCalled();
      expect(component.select.emit).toHaveBeenCalled();
    });

    it('should not execute select.emit when option is null', () => {
      spyOn(component.select, 'emit');

      component.selectOption(null);

      expect(component.select.emit).not.toHaveBeenCalled();
    });
  });
});
