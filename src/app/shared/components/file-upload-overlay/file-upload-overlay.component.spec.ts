import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { FileUploadOverlayComponent } from './file-upload-overlay.component';
import { TranslatePipeMock } from '~app/test';

const files = [
  new File(['firstSample'], 'firstSample.txt', { type: 'text/plain' }),
  new File(['secondSample'], 'secondSample.txt', { type: 'text/plain' })
];

describe('FileUploadOverlayComponent', () => {
  let component: FileUploadOverlayComponent;
  let fixture: ComponentFixture<FileUploadOverlayComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadOverlayComponent, TranslatePipeMock]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadOverlayComponent);
    component = fixture.componentInstance;
    component.isDragAndDropActive = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should stopPropagation and preventDefault be called when trigger dragleave event', fakeAsync(() => {
    const event = new Event('dragleave', { bubbles: true, cancelable: true });

    spyOn(event, 'preventDefault');
    spyOn(event, 'stopPropagation');
    const overlayElement = getOverlayElement();
    overlayElement.nativeElement.dispatchEvent(event);

    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  }));

  it('should stopPropagation and preventDefault be called when trigger dragenter event', fakeAsync(() => {
    const event = new Event('dragenter', { bubbles: true, cancelable: true });

    spyOn(event, 'preventDefault');
    spyOn(event, 'stopPropagation');
    const overlayElement = getOverlayElement();
    overlayElement.nativeElement.dispatchEvent(event);

    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  }));

  it('should attach overlay element to currentTarget, when dragenter event is triggered', () => {
    const overlayElement = getOverlayElement();
    let event = new MouseEvent('dragenter', { bubbles: true, cancelable: true });
    event = Object.defineProperty(event, 'target', { value: overlayElement });
    overlayElement.nativeElement.dispatchEvent(event);
    fixture.detectChanges();

    expect(component.currentTarget).toBe(overlayElement);
  });

  it('should hasOverlay be false when event.target is the same like currentTarget and dragleave is triggered', () => {
    const overlayElement = getOverlayElement();
    component.currentTarget = overlayElement;
    fixture.detectChanges();

    let event = new MouseEvent('dragleave', { bubbles: true, cancelable: true });
    event = Object.defineProperty(event, 'target', { value: overlayElement });

    overlayElement.nativeElement.dispatchEvent(event);

    expect(component.hasOverlay).toBe(false);
  });

  it('should hasOverlay be true when event.target is different from currentTarget and dragleave is triggered', () => {
    const overlayElement = getOverlayElement();
    component.currentTarget = null;
    fixture.detectChanges();

    let event = new MouseEvent('dragleave', { bubbles: true, cancelable: true });
    event = Object.defineProperty(event, 'target', { value: overlayElement });

    overlayElement.nativeElement.dispatchEvent(event);

    expect(component.hasOverlay).toBe(true);
  });

  it('should dragAndDrop change hasOverlay to false when is called', () => {
    component.hasOverlay = true;
    fixture.detectChanges();

    component.dragAndDrop(files);

    expect(component.hasOverlay).toBe(false);
  });

  it('should emit droppedFilesEvent when dragAndDrop is called', done => {
    const componentEmitter = component.droppedFilesEvent;
    spyOn(componentEmitter, 'emit');
    fixture.whenStable().then(() => {
      expect(component.droppedFilesEvent.emit).toHaveBeenCalled();
      done();
    });

    component.dragAndDrop(files);
  });

  function getOverlayElement(): DebugElement {
    component.hasOverlay = true;
    fixture.detectChanges();

    const overlayElement: DebugElement = fixture.debugElement.query(By.css('.overlay'));
    fixture.detectChanges();

    return overlayElement;
  }
});
