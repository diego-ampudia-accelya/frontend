import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Component({
  selector: 'bspl-file-upload-overlay',
  templateUrl: './file-upload-overlay.component.html',
  styleUrls: ['./file-upload-overlay.component.scss']
})
export class FileUploadOverlayComponent {
  @Output() droppedFilesEvent = new EventEmitter<any>();
  @Input() showInParent = false;
  @Input() isDragAndDropActive = true;
  public hasOverlay = false;
  public currentTarget: any;

  @HostListener('document:dragenter', ['$event']) public onDragEnter(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    if (this.isDragAndDropActive) {
      this.currentTarget = event.target;
    }
  }

  @HostListener('document:dragleave', ['$event']) public onDragEnd(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    if (this.isDragAndDropActive && this.currentTarget === event.target) {
      this.hasOverlay = false;
    }
  }

  @HostListener('document:dragover', ['$event']) public onDragStart(event: DragEvent) {
    if (this.isDragAndDropActive && !this.hasOverlay) {
      this.hasOverlay = this.isDraggingFiles(event);
    }
  }

  public dragAndDrop(files: File[]) {
    if (this.isDragAndDropActive) {
      this.hasOverlay = false;
      this.droppedFilesEvent.emit(files);
    }
  }

  private isDraggingFiles(event: DragEvent): boolean {
    return event.dataTransfer.types.some(t => t.toLowerCase() === 'files');
  }
}
