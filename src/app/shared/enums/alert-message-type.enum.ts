export enum AlertMessageType {
  warning = 'warning',
  info = 'info',
  error = 'error',
  success = 'success'
}
