/* eslint-disable @typescript-eslint/naming-convention */
export enum RemittanceFrequency {
  W = 'Weekly',
  T = 'Twice a Week',
  S = 'Super Weekly',
  F = 'Fortnightly',
  D = 'Every 10 days(dual)',
  M = 'Monthly',
  R = 'Every 10 days',
  X = 'TIDS agents'
}
