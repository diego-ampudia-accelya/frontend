/* eslint-disable @typescript-eslint/naming-convention */
export enum LocationType {
  HE = 'Head Entity',
  AE = 'Associate Entity'
}
