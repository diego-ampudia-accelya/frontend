/* eslint-disable @typescript-eslint/naming-convention */
export enum SortOrder {
  Asc = 'ASC',
  Desc = 'DESC'
}
