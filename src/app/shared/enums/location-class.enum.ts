/* eslint-disable @typescript-eslint/naming-convention */
export enum LocationClass {
  A = 'AIRLINE POINTS OF SALE',
  C = 'CARGO',
  D = 'DOMESTIC',
  F = 'ERSP(ELEC.RSCTN.SRV.PROVIDER)',
  G = 'GSA',
  I = 'IMPORTANT AGENTS',
  K = 'CATO',
  L = 'NON-IATA LOCATIONS',
  M = 'MSO',
  N = 'NISI',
  O = 'SATO',
  P = 'PASSENGER',
  Q = 'COURIERS',
  R = 'CASS ASSOCIATES',
  S = 'SSI',
  T = 'TIDS',
  U = 'PUERTO RICO AND US VIRGIN IS.',
  X = 'HANDLING AGTS & SHIPPING LINES',
  Y = 'ASSOCIATIONS',
  Z = 'TRAVEL IND SUPPLIERS'
}
