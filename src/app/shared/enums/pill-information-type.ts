/* eslint-disable @typescript-eslint/naming-convention */
export enum PillInformationType {
  Regular = 'regular',
  Error = 'error'
}
