/* eslint-disable @typescript-eslint/naming-convention */
export enum SelectMode {
  Heading = 'heading',
  SubHeading = 'subheading',
  Small = 'small'
}
