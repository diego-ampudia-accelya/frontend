/* eslint-disable @typescript-eslint/naming-convention */
export enum EnvironmentType {
  RefundIndirect = '1',
  Acdm = '2',
  MasterAirline = '3',
  MasterAgent = '4',
  UserMaintenance = '5'
}
