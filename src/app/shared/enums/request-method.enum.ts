/* eslint-disable @typescript-eslint/naming-convention */
export enum RequestMethod {
  Get,
  Post,
  Put,
  Delete,
  Options,
  Head,
  Patch
}
