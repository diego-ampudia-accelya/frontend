export enum BadgeInfoType {
  warning = 'warn',
  info = 'info',
  error = 'error',
  success = 'success',
  regular = 'regular'
}
