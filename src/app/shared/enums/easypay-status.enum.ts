/* eslint-disable @typescript-eslint/naming-convention */
export enum EasyPayStatus {
  OPTIN = 'OPTIN',
  OPTOUT = 'OPTOUT'
}
