/* eslint-disable @typescript-eslint/naming-convention */
export enum PaymentStatus {
  ACTIVE = 'ACTIVE',
  NON_AUTHORIZED = 'NOT_AUTHORIZED',
  NON_ACTIVE = 'NON_ACTIVE',
  OPTIN = 'OPTIN'
}
