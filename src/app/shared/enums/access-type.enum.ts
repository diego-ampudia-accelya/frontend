export enum AccessType {
  create,
  read,
  edit,
  editTicket
}
