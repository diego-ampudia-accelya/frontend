/* eslint-disable @typescript-eslint/naming-convention */
export enum LongDocumentType {
  EMDS = 'EMDS',
  EMDA = 'EMDA',
  TKTT = 'TKTT',
  ACMA = 'ACMA',
  ACMD = 'ACMD',
  ACNT = 'ACNT',
  ADMA = 'ADMA',
  ADMD = 'ADMD',
  ADNT = 'ADNT',
  CANX = 'CANX',
  RFNC = 'RFNC',
  RFND = 'RFND',
  SPCR = 'SPCR',
  SPDR = 'SPDR',
  SSAC = 'SSAC',
  SSAD = 'SSAD',
  TASF = 'TASF'
}
