/* eslint-disable @typescript-eslint/naming-convention */
import { ROUTES } from '../constants/routes';
import { GridTableActionType } from '../models/grid-table-actions.model';
import { BSP_CURRENCY_ASSIGNATION_LIST } from '~app/master-data/bsp-currency-assignation/bsp-currency-assignation-list/bsp-currency-assignation-list.constants';
import { BspAddEditFormComponent } from '~app/master-data/bsp/bsp-dialog/bsp-add-edit-form-dialog/bsp-add-edit-form.component';
import { BSP_LIST } from '~app/master-data/bsp/bsp-list/bsp-list.constants';
import { CurrencyAddFormComponent } from '~app/master-data/currency/currency-dialog/currency-add-form/currency-add-form.component';
import { CurrencyDeleteFormComponent } from '~app/master-data/currency/currency-dialog/currency-delete-form/currency-delete-form.component';
import { CURRENCY_LIST } from '~app/master-data/currency/currency-list/currency-list.constants';
import { GdsAddEditFormDialogComponent } from '~app/master-data/gds/gds-dialog/add-edit-form/gds-add-edit-form-dialog.component';
import { GdsDeleteFormDialogComponent } from '~app/master-data/gds/gds-dialog/delete-form/gds-delete-form-dialog.component';
import { GDS_LIST } from '~app/master-data/gds/gds-list/gds-list.constants';
import { MasterConfig, MasterDataType } from '~app/master-shared/models/master.model';

export class MasterConfigHelper {
  // This object contains the config for every MasterDataType.
  // This config object defines which dialog components are open when clicking the different actions
  private static configs: { [key in MasterDataType]: MasterConfig } = {
    AIRLINE_TA: { actions: {} },
    AGENT_TA: { actions: {} },
    BSP: {
      actions: {
        [GridTableActionType.Add]: BspAddEditFormComponent,
        [GridTableActionType.Edit]: BspAddEditFormComponent
      },
      tableConfig: BSP_LIST
    },
    BSPCurrencyAssignation: {
      actions: {},
      tableConfig: BSP_CURRENCY_ASSIGNATION_LIST
    },
    Currency: {
      actions: {
        [GridTableActionType.Add]: CurrencyAddFormComponent,
        [GridTableActionType.Edit]: CurrencyAddFormComponent,
        [GridTableActionType.Delete]: CurrencyDeleteFormComponent
      },
      tableConfig: CURRENCY_LIST
    },
    GDS: {
      actions: {
        [GridTableActionType.Add]: GdsAddEditFormDialogComponent,
        [GridTableActionType.Edit]: GdsAddEditFormDialogComponent,
        [GridTableActionType.Delete]: GdsDeleteFormDialogComponent
      },
      tableConfig: GDS_LIST
    },
    Agent: {
      actions: {
        [GridTableActionType.ManageTA]: ROUTES.AGENT_PROFILE.url
      }
    },
    Airline: {
      actions: {
        [GridTableActionType.ManageTA]: ROUTES.AIRLINE_PROFILE.url
      }
    },
    BillingStatement: {
      actions: {
        [GridTableActionType.BillingAnalysis]: ROUTES.BILLING_ANALYSIS.url
      }
    },
    BillingAnalysis: { actions: {} },
    adm: { actions: {} },
    acm: { actions: {} },
    adnt: { actions: {} },
    acnt: { actions: {} },
    'adm-request': { actions: {} },
    'acm-request': { actions: {} },
    'refund-application': { actions: {} },
    'refund-notice': { actions: {} },
    spcr: {
      actions: { [GridTableActionType.Add]: ROUTES.SPDR_ISSUE.url }
    },
    spdr: {
      actions: { [GridTableActionType.Add]: ROUTES.SPCR_ISSUE.url }
    },
    admd: { actions: { [GridTableActionType.Add]: ROUTES.ADMD_ISSUE.url } },
    acmd: { actions: { [GridTableActionType.Add]: ROUTES.ACMD_ISSUE.url } },
    'tip-card': { actions: {} },
    'tip-product': { actions: {} },
    'tip-card-consent': { actions: {} },
    'tip-product-consent': { actions: {} }
  };

  // Method called in MasterList. Retrieves the indexed config depending on the list MasterDataType
  public static getConfig(dataType: MasterDataType): MasterConfig {
    return this.configs[dataType];
  }
}
