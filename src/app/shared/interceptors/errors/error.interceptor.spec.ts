import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import moment from 'moment-mini';

import { NetworkError, ServerError } from '../../errors';
import { ResponseErrorBE } from '../../models';

import { ErrorInterceptor } from './error.interceptor';

describe('ErrorInterceptor', () => {
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let translationSpy: SpyObject<L10nTranslationService>;
  const url = 'test';
  const now = moment().toDate();

  beforeEach(() => {
    translationSpy = createSpyObject(L10nTranslationService);
    translationSpy.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true
        },
        { provide: L10nTranslationService, useValue: translationSpy }
      ]
    });

    jasmine.clock().install();
    jasmine.clock().mockDate(now);
    spyOn(console, 'error');

    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  afterEach(() => {
    jasmine.clock().uninstall();
    httpMock.verify();
  });

  it('should return default error details with message based on statusCode when no error details are available', () => {
    translationSpy.translate.and.returnValue('translated.errorIndex.statusCodes.403');
    httpClient.get<any>(url).subscribe(
      () => fail(),
      (serverError: ServerError) => {
        expect(serverError.error.errorCode).toBe(403);
        expect(serverError.error.errorMessage).toBe('translated.errorIndex.statusCodes.403');
        expect(serverError.error.timestamp).toBe(now.toISOString());
        expect(serverError.error.messages).toEqual([]);
      }
    );

    httpMock.expectOne('test').flush(null, {
      status: 403,
      statusText: 'Forbidden'
    });
  });

  it('should return default error details with default message when no error details are available and the status code is not known', () => {
    httpClient.get<any>(url).subscribe(
      () => fail(),
      (serverError: ServerError) => {
        expect(serverError.error.errorCode).toBe(418);
        expect(serverError.error.errorMessage).toBe('GENERIC_HTTP_ERROR');
        expect(serverError.error.timestamp).toBe(now.toISOString());
        expect(serverError.error.messages).toEqual([]);
      }
    );

    httpMock.expectOne('test').flush(null, {
      status: 418,
      statusText: "I'm a teapot"
    });
  });

  it('should return the error details when they are available in the error response', () => {
    const errorDetails: ResponseErrorBE = {
      errorCode: 403,
      errorMessage: 'Insufficient Permissions',
      timestamp: moment().toISOString(),
      messages: null
    };

    httpClient.get<any>(url).subscribe(
      () => fail(),
      (serverError: ServerError) => {
        expect(serverError.error).toEqual({
          ...errorDetails,
          messages: []
        });
      }
    );

    httpMock.expectOne('test').flush(errorDetails, {
      status: 403,
      statusText: 'Forbidden'
    });
  });

  it('should preserve success response', () => {
    const data = { id: 1 };
    httpClient.get<any>(url).subscribe(
      response => {
        expect(response).toEqual(data);
      },
      () => fail()
    );

    httpMock.expectOne(url).flush(data);
  });

  it('should throw ServerException when an error response is detected', () => {
    httpClient.get<any>(url).subscribe(
      () => fail(),
      (serverError: ServerError) => {
        expect(serverError instanceof ServerError).toBeTruthy();
      }
    );

    httpMock.expectOne('test').flush(null, {
      status: 403,
      statusText: 'Forbidden'
    });
  });

  it('should throw NetworkError when a network error response is detected', () => {
    httpClient.get<any>(url).subscribe(
      () => fail(),
      (serverError: NetworkError) => {
        expect(serverError instanceof NetworkError).toBeTruthy();
      }
    );

    httpMock.expectOne('test').flush(new ProgressEvent('error'), {
      status: 0,
      statusText: 'Network Error'
    });
  });

  it('should parse exception when `responseType` is `arraybuffer`', () => {
    const errorDetails: ResponseErrorBE = {
      errorCode: 403,
      errorMessage: 'Insufficient Permissions',
      timestamp: moment().toISOString(),
      messages: null
    };

    const expected = { ...errorDetails, messages: [] };

    httpClient.get(url, { responseType: 'arraybuffer' }).subscribe(
      () => fail(),
      (serverError: ServerError) => {
        expect(serverError.error).toEqual(expected);
      }
    );

    httpMock.expectOne('test').flush(stringToArrayBuffer(JSON.stringify(errorDetails)), {
      status: 403,
      statusText: 'Forbidden'
    });
  });

  it('should return a default exception when `responseType` is `arraybuffer` and the response parsing fails', () => {
    httpClient.get(url, { responseType: 'arraybuffer' }).subscribe(
      () => fail(),
      (serverError: ServerError) => {
        expect(serverError.error.errorCode).toBe(403);
        // eslint-disable-next-line no-console
        expect(console.error).toHaveBeenCalled();
      }
    );

    httpMock.expectOne('test').flush(stringToArrayBuffer(':}^&UOIOIYTI{'), {
      status: 403,
      statusText: 'Forbidden'
    });
  });

  function stringToArrayBuffer(value: string): ArrayBuffer {
    const buf = new ArrayBuffer(value.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = value.length; i < strLen; i++) {
      bufView[i] = value.charCodeAt(i);
    }

    return buf;
  }
});
