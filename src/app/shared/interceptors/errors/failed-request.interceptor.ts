import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const showErrorMessageKey = 'showErrorMessage';

// TODO: Remove the interceptor since it is deprecated by the `GlobalErrorHandler`.
// TODO: Make sure that the `showErrorMessage` parameter is no longer provided when making HTTP requests.
@Injectable()
/**
 * @deprecated Default error handling is provided by the `GlobalErrorHandler`.
 */
export class FailedRequestInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newRequest = request.clone({
      params: request.params.delete(showErrorMessageKey)
    });

    return next.handle(newRequest);
  }
}
