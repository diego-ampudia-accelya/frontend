import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { concatMap, delay, retryWhen } from 'rxjs/operators';

import { ServerError } from '../../errors';
import { AppConfigurationService } from '~app/shared/services';

// TODO: refactor interceptor, because there is no delay between requests
@Injectable()
export class RetryInterceptor implements HttpInterceptor {
  constructor(private appConfiguration: AppConfigurationService) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      retryWhen((errors$: Observable<ServerError>) =>
        errors$.pipe(
          concatMap((error, retryIndex) => {
            const retryAttempt = retryIndex + 1;
            let result: Observable<any> = throwError(error);
            if (this.canRetry(error, request, retryAttempt)) {
              // The request is retried after a delay.
              // It increases after each unsuccessful attempt.
              const scaledDelay = this.getDelay(retryAttempt);
              result = of(error).pipe(delay(scaledDelay));
            }

            return result;
          })
        )
      )
    );
  }

  private canRetry(error: ServerError, request: HttpRequest<any>, retryCount: number): boolean {
    const isRecoverable = this.isRecoverable(error, request);
    const exceedsMaxRetries = retryCount > this.appConfiguration.retry.maxAttempts;

    return isRecoverable && !exceedsMaxRetries;
  }

  private getDelay(retryCount: number): number {
    const baseDelay = this.appConfiguration.retry.delay;

    return baseDelay * Math.pow(2, retryCount - 1);
  }

  private isRecoverable(error: ServerError, request: HttpRequest<any>): boolean {
    const { rules } = this.appConfiguration.retry;

    return rules.some(rule => {
      const urlPattern = new RegExp(rule.url, 'i');
      const matchesUrl = urlPattern.test(request.url);
      const matchesHttpMethod = rule.methods.includes(request.method);
      const matchesStatusCode = rule.statusCodes.includes(error.status);

      return matchesUrl && matchesHttpMethod && matchesStatusCode;
    });
  }
}
