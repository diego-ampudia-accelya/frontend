import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { defaults, isNil, omitBy } from 'lodash';
import moment from 'moment-mini';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {
  ForbiddenError,
  GatewayTimeoutError,
  InternalServerError,
  NetworkError,
  NotAvailableError,
  NotFoundError,
  ServerError,
  UnauthorizedError,
  ValidationError
} from '../../errors';
import { ResponseErrorBE } from '../../models/error-response-be.model';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  private knownErrors: { [code: number]: typeof ServerError } = {
    400: ValidationError,
    401: UnauthorizedError,
    403: ForbiddenError,
    404: NotFoundError,
    500: InternalServerError,
    503: NotAvailableError,
    504: GatewayTimeoutError,
    0: NetworkError
  };

  constructor(private translationService: L10nTranslationService) {}

  /**
   * Intercept all incoming error responses and normalize the returned error details.
   * Rethrow a typed error based on known status codes to enable filtering errors, using the `instanceof` operator.
   */
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((response: HttpErrorResponse) => {
        response = this.parseErrorResponse(response, req);

        return throwError(this.convertToServerError(response));
      })
    );
  }

  private convertToServerError(response: HttpErrorResponse): ServerError {
    const normalizedDetails = this.normalizeDetails(response);

    return this.mapToError(response, normalizedDetails);
  }

  /**
   * Examine the error response and extract the error description object.
   * If none is provided create one by using some reasonable defaults.
   */
  private normalizeDetails(response: HttpErrorResponse): ResponseErrorBE {
    const defaultError: ResponseErrorBE = {
      timestamp: moment().toISOString(),
      errorCode: response.status,
      errorMessage: this.getMessageFromCode(response.status),
      messages: []
    };

    return defaults({}, omitBy(response.error, isNil), defaultError);
  }

  private parseErrorResponse(response: HttpErrorResponse, request: HttpRequest<any>): HttpErrorResponse {
    // Work-around for https://github.com/angular/angular/issues/19888
    if (request.responseType === 'arraybuffer' && response.error instanceof ArrayBuffer) {
      try {
        const decodedString = String.fromCharCode.apply(null, new Uint8Array(response.error));
        const clonedError = {
          ...response,
          error: JSON.parse(decodedString)
        };
        response = new HttpErrorResponse(clonedError);
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(`Could not parse response for request ${request.urlWithParams}`);
      }
    } else if (response.error instanceof ProgressEvent) {
      // Ignore error details when a network error occurs
      response = new HttpErrorResponse({ ...response, error: null });
    }

    return response;
  }

  /**
   * Create the typed error based on the response status code.
   */
  private mapToError(response: HttpErrorResponse, error: ResponseErrorBE) {
    const knownError = this.knownErrors[response.status] || ServerError;

    return new knownError(error, response.headers, response.url);
  }

  /**
   * Returns a default message based on the HTTP status code
   *
   * @param statusCode Status Code of the HTTP Request
   */
  private getMessageFromCode(statusCode: number): string {
    const key = `errorIndex.statusCodes.${statusCode}`;
    const message = this.translationService.translate(key);
    const isTranslated = message !== key;

    return isTranslated ? message : this.translationService.translate('GENERIC_HTTP_ERROR');
  }
}
