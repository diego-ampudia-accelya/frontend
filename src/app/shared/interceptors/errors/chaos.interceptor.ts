import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import moment from 'moment-mini';
import { Observable, throwError } from 'rxjs';
import { InternalServerError } from '../../errors';
import { environment } from '~env/environment';

@Injectable()
/**
 * The `ChaosInterceptor` is used to simulate unstable backend services.
 * After a predefined period of 30 seconds it starts to randomly throw InternalServerError on 50% of the requests.
 * Useful when implementing error handling related features.
 */
export class ChaosInterceptor implements HttpInterceptor {
  private isInChaosMode = false;
  private enterChaosModeAfter = moment.duration(30, 'seconds').asMilliseconds();

  constructor() {
    setTimeout(() => {
      this.isInChaosMode = true;
      // eslint-disable-next-line no-console
      console.warn('Entered Chaos mode!');
    }, this.enterChaosModeAfter);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let result = next.handle(req);
    if (this.isInChaosMode && this.getSecureRandomNumber() <= environment.chaosModeRate) {
      result = throwError(this.createError());
    }

    return result;
  }

  /**
   *
   * @returns a crypto generated number between 0 and 1 (0 inclusive, 1 exclusive). Mimics the Math.random function in range of results
   */
  private getSecureRandomNumber(): number {
    const array = new Uint32Array(1);
    const max = Math.pow(2, 32);

    return window.crypto.getRandomValues(array)[0] / max;
  }

  private createError() {
    return new InternalServerError({
      timestamp: moment().toISOString(),
      errorCode: 500,
      errorMessage: 'The server could not process the request',
      messages: []
    });
  }
}
