import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockProvider } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';

import { GatewayTimeoutError, ServerError } from '../../errors';

import { ErrorInterceptor } from './error.interceptor';
import { RetryInterceptor } from './retry.interceptor';
import { AppConfigurationService } from '~app/shared/services';

describe('RetryInterceptor', () => {
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;

  const successfulResponse = 'success';
  const serviceUnavailableError = { status: 503, statusText: 'Service Unavailable' };
  const gatewayTimeoutError = { status: 504, statusText: 'Gateway Timeout' };
  const networkError = { status: 0, statusText: 'Network error' };
  const unrecoverableError = { status: 418, statusText: `I'm a teapot` };
  const retryDelay = 1000;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: RetryInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        mockProvider(AppConfigurationService, {
          retry: {
            delay: retryDelay,
            maxAttempts: 3,
            rules: [
              {
                url: 'special',
                methods: ['GET', 'PUT', 'POST', 'DELETE'],
                statusCodes: [503, 504, 0]
              },
              {
                url: '.*',
                methods: ['GET', 'PUT', 'DELETE'],
                statusCodes: [503, 504, 0]
              }
            ]
          }
        }),
        mockProvider(L10nTranslationService)
      ]
    });
  });

  beforeEach(() => {
    httpTestingController = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should pass-through successful responses', fakeAsync(() => {
    let actualResponse: string;
    httpClient.get<string>('/test').subscribe(response => (actualResponse = response));
    tick();
    httpTestingController.expectOne('/test').flush(successfulResponse);

    expect(actualResponse).toEqual(successfulResponse);
  }));

  it('should pass-through errors when no rule matches the status code of response ', fakeAsync(() => {
    let actualError: ServerError;
    httpClient.get<any>('/test').subscribe(
      () => fail(),
      error => (actualError = error)
    );
    tick();
    httpTestingController.expectOne('/test').flush(null, unrecoverableError);

    expect(actualError).toBeDefined();
    expect(actualError instanceof ServerError).toBeTruthy();
  }));

  it('should pass-through errors when no rule matches the method of response ', fakeAsync(() => {
    let actualError: ServerError;
    httpClient.post<any>('/test', {}).subscribe(
      () => fail(),
      error => (actualError = error)
    );
    tick();
    httpTestingController.expectOne('/test').flush(null, gatewayTimeoutError);

    expect(actualError).toBeDefined();
    expect(actualError instanceof ServerError).toBeTruthy();
  }));

  it('should retry when the error matches the first rule', fakeAsync(() => {
    let actualResult: string;
    httpClient.post<any>('/special', {}).subscribe(result => (actualResult = result));
    tick();
    httpTestingController.expectOne('/special').flush(null, serviceUnavailableError);
    tick(retryDelay);
    httpTestingController.expectOne('/special').flush(successfulResponse);

    expect(actualResult).toBeDefined();
    expect(actualResult).toEqual(successfulResponse);
  }));

  it('should retry up to 3 times and return the last error when the error matches the last rule', fakeAsync(() => {
    let actualError: ServerError;
    httpClient.get<any>('/test').subscribe(
      () => fail(),
      error => (actualError = error)
    );
    tick();
    httpTestingController.expectOne('/test').flush(null, serviceUnavailableError);
    tick(retryDelay);
    httpTestingController.expectOne('/test').flush(null, serviceUnavailableError);
    tick(retryDelay * 2);
    httpTestingController.expectOne('/test').flush(null, networkError);
    tick(retryDelay * 4);
    httpTestingController.expectOne('/test').flush(null, gatewayTimeoutError);

    expect(actualError).toBeDefined();
    expect(actualError instanceof GatewayTimeoutError).toBeTruthy();
  }));

  it('should retry and return response when error is recoverable and the second retry succeeds', fakeAsync(() => {
    let actualResult: string;
    httpClient.get<any>('/test').subscribe(result => (actualResult = result));
    tick();
    httpTestingController.expectOne('/test').flush(null, serviceUnavailableError);
    tick(retryDelay);
    httpTestingController.expectOne('/test').flush(null, serviceUnavailableError);
    tick(retryDelay * 2);
    httpTestingController.expectOne('/test').flush(successfulResponse);

    expect(actualResult).toBeDefined();
    expect(actualResult).toEqual(successfulResponse);
  }));

  it('should stop retrying and return error when it is retriable and first retry returns an unrecoverable error', fakeAsync(() => {
    let actualError: ServerError;
    httpClient.get<any>('/test').subscribe(
      () => fail(),
      error => (actualError = error)
    );
    tick();
    httpTestingController.expectOne('/test').flush(null, serviceUnavailableError);
    tick(retryDelay);
    httpTestingController.expectOne('/test').flush(null, unrecoverableError);

    expect(actualError).toBeDefined();
    expect(actualError instanceof ServerError).toBeTruthy();
  }));
});
