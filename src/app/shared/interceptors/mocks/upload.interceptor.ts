import {
  HttpErrorResponse,
  HttpEvent,
  HttpEventType,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash';
import { interval, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable()
export class UploadInterceptor implements HttpInterceptor {
  private urlPattern = new RegExp('^mocks/upload/(fail|success)$');

  private fakeResponse = [
    {
      type: HttpEventType.Sent
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 9355264,
      total: 82732762
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 20217856,
      total: 82732762
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 29818880,
      total: 82732762
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 40648704,
      total: 82732762
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 51871744,
      total: 82732762
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 63160320,
      total: 82732762
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 74416128,
      total: 82732762
    },
    {
      type: HttpEventType.UploadProgress,
      loaded: 82732762,
      total: 82732762
    },
    {
      type: HttpEventType.Response,
      ok: true,
      body: null,
      status: 201,
      statusText: 'Created'
    }
  ];

  public intercept(req: HttpRequest<FormData>, next: HttpHandler): Observable<HttpEvent<any>> {
    const match = this.urlPattern.exec(req.url);
    if (match) {
      const enableRandomFailures = match[1] === 'fail';

      return this.upload(req, enableRandomFailures);
    }

    return next.handle(req);
  }

  private upload(req: HttpRequest<FormData>, enableRandomFailures: boolean): Observable<any> {
    const shouldFail = enableRandomFailures && Math.random() > 0.5;
    const res = interval(Math.floor(Math.random() * 2000)).pipe(
      take(this.fakeResponse.length),
      map(i => {
        if (shouldFail) {
          throw new HttpErrorResponse({
            status: 400,
            statusText: 'Bad Request'
          });
        }
        const response = cloneDeep(this.fakeResponse[i]);
        if (i === this.fakeResponse.length - 1) {
          const file = req.body.get('file') as File;
          response.body = {
            id: file.name
          };
        }

        return response;
      })
    );

    return res;
  }
}
