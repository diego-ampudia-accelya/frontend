import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { defer, Observable, of, throwError } from 'rxjs';
import { catchError, map, switchMap, take, tap } from 'rxjs/operators';

import { Login, Logout } from '~app/auth/actions/auth.actions';
import { AuthService } from '~app/auth/services/auth.service';
import { TokenService } from '~app/auth/services/token.service';
import { AppState } from '~app/reducers';
import { UnauthorizedError } from '~app/shared/errors';
import { NoConcurrentRequests, tryParseBoolean } from '~app/shared/utils';

@Injectable()
/**
 * Include Authorization header with the JWT token.
 * In any HTTP request, this interceptor sends authorization header to the backend.
 */
export class AuthInterceptor implements HttpInterceptor {
  constructor(private tokenService: TokenService, private store: Store<AppState>, private authService: AuthService) {}

  @NoConcurrentRequests()
  private refreshAccessToken(): Observable<any> {
    return this.tokenService.refreshTokenCookies().pipe(
      switchMap(authAction => (authAction ? of(authAction) : this.refreshUserProfile())),
      tap(action => this.store.dispatch(action))
    );
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const noAuth = tryParseBoolean(request.params.get('noAuth'), false);

    // Request should be sent with outgoing credentials (cookies).
    request = request.clone({
      withCredentials: true
    });

    if (noAuth) {
      // Interceptor is skipped
      return next.handle(request);
    }

    return this.handleWithAuthentication(request, next);
  }

  private handleWithAuthentication(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return defer(() => next.handle(request)).pipe(
      catchError(error => {
        if (!(error instanceof UnauthorizedError)) {
          return throwError(error);
        }

        return this.refreshAccessToken().pipe(switchMap(() => next.handle(request)));
      })
    );
  }

  private refreshUserProfile(): Observable<Action> {
    return this.authService.loadUserProfile().pipe(
      map(user => (user ? new Login({ user, skipLocaleUpdate: true }) : new Logout())),
      take(1)
    );
  }
}
