import { HttpClient, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { L10nTranslationService } from 'angular-l10n';
import { of } from 'rxjs';

import { Login } from '~app/auth/actions/auth.actions';
import { AuthService } from '~app/auth/services/auth.service';
import { TokenService } from '~app/auth/services/token.service';
import { ForbiddenError, ServerError } from '~app/shared/errors';
import { AuthInterceptor } from '~app/shared/interceptors/authentication/auth.interceptor';
import { ErrorInterceptor } from '~app/shared/interceptors/errors/error.interceptor';

describe('AuthInterceptor', () => {
  let httpTestController: HttpTestingController;
  let httpClient: HttpClient;

  let tokenServiceSpy: SpyObject<TokenService>;
  let mockStore: MockStore;
  let authServiceSpy: SpyObject<AuthService>;

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        mockProvider(AuthService),
        mockProvider(TokenService),
        mockProvider(L10nTranslationService),
        provideMockStore(),
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
      ]
    })
  );

  beforeEach(() => {
    httpTestController = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

    tokenServiceSpy = TestBed.inject<any>(TokenService);

    mockStore = TestBed.inject<any>(Store);
    spyOn(mockStore, 'dispatch');

    authServiceSpy = TestBed.inject<any>(AuthService);
    authServiceSpy.loadUserProfile.and.returnValue(of({}));
  });

  afterEach(() => {
    httpTestController.verify();
  });

  it('should create', () => {
    expect(httpClient).toBeDefined();
  });

  it('should send requests withCredentials set to true', fakeAsync(() => {
    let actualResult: string;
    httpClient.get<string>('test-url', { params: { noAuth: 'true' } }).subscribe(result => (actualResult = result));

    httpTestController.expectOne(request => request.withCredentials).flush('test-result');

    expect(actualResult).toBe('test-result');
  }));

  it('should refresh token and retry when request fails with status 401', fakeAsync(() => {
    tokenServiceSpy.refreshTokenCookies.and.returnValue(of(null));

    httpClient.get<string>('test-url').subscribe(() => {});

    httpTestController.expectOne('test-url').flush(null, { status: 401, statusText: 'Unauthorized' });

    httpTestController
      .expectOne(
        matchRequestBy({
          url: 'test-url',
          headers: {}
        })
      )
      .flush('test-result');

    expect(tokenServiceSpy.refreshTokenCookies).toHaveBeenCalledTimes(1);
    expect(authServiceSpy.loadUserProfile).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(new Login({ user: {} as any, skipLocaleUpdate: true }));
  }));

  it('should rethrow error when request fails with a status different from 401', fakeAsync(() => {
    let actualError: ServerError;
    httpClient.get<string>('test-url').subscribe(
      () => fail(),
      error => (actualError = error)
    );

    httpTestController.expectOne('test-url').flush(null, { status: 403, statusText: 'Forbidden' });

    expect(actualError instanceof ForbiddenError).toBeTruthy();
  }));

  function matchRequestBy({ url, headers }): (request: HttpRequest<any>) => boolean {
    return (request: HttpRequest<any>) => {
      const matchesUrl = !url || request.url === url;
      const matchesHeaders = Object.entries(headers).every(([name, value]) => {
        if (value == null) {
          return !request.headers.has(name);
        }

        return request.headers.get(name) === value;
      });

      return matchesUrl && matchesHeaders;
    };
  }
});
