import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Provider } from '@angular/core';

import { ChaosInterceptor } from './errors/chaos.interceptor';
import { ErrorInterceptor } from './errors/error.interceptor';
import { FailedRequestInterceptor } from './errors/failed-request.interceptor';
import { RetryInterceptor } from './errors/retry.interceptor';
import { UploadInterceptor } from './mocks/upload.interceptor';
import { AuthInterceptor } from '~app/shared/interceptors/authentication/auth.interceptor';
import { ConfigurationInterceptor } from '~app/master-data/configuration/api/configuration.interceptor';
import { environment } from '~env/environment';

const chaosModeProvider: Provider = { provide: HTTP_INTERCEPTORS, useClass: ChaosInterceptor, multi: true };
const mockProviders: Provider[] = [
  { provide: HTTP_INTERCEPTORS, useClass: ConfigurationInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: UploadInterceptor, multi: true }
];

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: FailedRequestInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: RetryInterceptor, multi: true },
  ...(environment.enableChaosMode ? [chaosModeProvider] : []),
  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ...(environment.production ? [] : mockProviders)
];
