import { FormBuilder, FormGroup } from '@angular/forms';
import { cloneDeep } from 'lodash';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { FormUtil } from '../helpers';
import { generateBEObject } from '../helpers/form-helpers/mapping/mapper.helper';
import { AdapterMapper, FieldMapper } from '../helpers/form-helpers/mapping/types';
import { ResponseErrorBE } from '../models/error-response-be.model';

export abstract class FormDataService<T, U> {
  public isFormReady = new BehaviorSubject<boolean>(false);

  protected form: FormGroup;

  protected subjects: {
    [key: string]: Subject<any>;
  };

  protected abstract get initialForm(): any;
  protected abstract get initialSubjects(): { [key: string]: Subject<any> };

  constructor(protected fb: FormBuilder) {
    this.initializeSubjects();
    this.initializeForm();
  }

  public getForm(): FormGroup {
    return this.form;
  }

  public getFormValue(): U {
    return cloneDeep(this.form.value);
  }

  public whenFormIsReady(): Observable<boolean> {
    return this.isFormReady.pipe(first(ready => ready));
  }

  public isFormValid(): boolean {
    return this.form.valid;
  }

  public getSectionFormGroup(name: keyof U & string): FormGroup {
    return this.form.get(name) as FormGroup;
  }

  /**
   * Sets form group in form. Optionally can patch previous values.
   *
   * @param name Name for the FormGroup
   * @param group FormGroup instance
   * @param [keepValues] `false` by default. Calls `patchValue` method if true to keep previous values of the existing `FormGroup`
   */
  public setSectionFormGroup(name: keyof U & string, group: FormGroup, keepValues = false): void {
    const existingGroup = this.getSectionFormGroup(name);
    this.form.setControl(name, group);

    if (keepValues && existingGroup) {
      this.getSectionFormGroup(name).patchValue(existingGroup.value);
    }
  }

  public getSubject(name: string) {
    return this.subjects[name];
  }

  public getDataForSubmit(fieldMapper: FieldMapper<T, U>, adapterMapper: AdapterMapper<T>): T {
    return generateBEObject<T, U>(this.getForm(), fieldMapper, adapterMapper);
  }

  public processErrorsOnSubmit(errorResponse: ResponseErrorBE, fieldMapper: FieldMapper<T, U>): void {
    FormUtil.processErrorsOnForm<T, U>(errorResponse, this.getForm(), fieldMapper);
  }

  public resetData(): void {
    this.restoreSubjects();
    this.initializeForm();
  }

  private initializeForm(): void {
    this.isFormReady.next(false);

    if (this.form) {
      this.form.reset({}, { emitEvent: false });
    } else {
      this.form = this.fb.group(this.initialForm);
    }
  }

  private initializeSubjects() {
    this.subjects = this.initialSubjects;
  }

  private restoreSubjects() {
    //* Restoring BehaviourSubject values
    Object.keys(this.subjects).forEach(subjKey => {
      const initialSubject = this.initialSubjects[subjKey];

      if (initialSubject instanceof BehaviorSubject) {
        this.subjects[subjKey].next(initialSubject.value);
      }
    });
  }
}
