import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';
import { defaults } from 'lodash';
import { Message, MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private defaultNotificationOptions = {
    life: 5000,
    sticky: false,
    closable: true
  };

  constructor(private translationService: L10nTranslationService, private messageService: MessageService) {}

  public showError(message: string | string[], options: Message = {}) {
    options = {
      ...options,
      sticky: true,
      severity: 'error'
    };

    this.notify(message, options);
  }

  public showSystemError(message: string | string[], data: { errorCode?: any; errorId?: any }, options: Message = {}) {
    options = {
      ...options,
      sticky: true,
      severity: 'error',
      data
    };

    this.notify(message, options);
  }

  public showWarning(message: string | string[], options: Message = {}) {
    options.severity = 'warn';
    this.notify(message, options);
  }

  public showSuccess(message: string | string[], options: Message = {}) {
    options.severity = 'success';
    this.notify(message, options);
  }

  public showInformation(message: string | string[], options: Message = {}) {
    options.severity = 'info';
    this.notify(message, options);
  }

  public addMessages(messages: Message[]) {
    this.messageService.addAll(messages);
  }

  public addMessage(message: Message) {
    this.messageService.add(message);
  }

  public clearMessage(key?: string) {
    this.messageService.clear(key);
  }

  private notify(message: string | string[], notificationOptions: Message = {}) {
    notificationOptions = defaults(notificationOptions, this.defaultNotificationOptions);

    if (!Array.isArray(message)) {
      message = [message];
    }

    const notifications = message.map<Message>(notificationText => ({
      detail: this.translationService.translate(notificationText),
      ...notificationOptions
    }));

    this.addMessages(notifications);
  }
}
