import { BehaviorSubject, forkJoin, interval, Observable } from 'rxjs';
import { finalize, take } from 'rxjs/operators';

import { PaginationInfo } from '../models/pagination-info.model';
import { GLOBALS } from '~app/shared/constants/globals';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { ApiService } from '~app/shared/services/api.service';

export class DataSource<T> {
  public paginationInfo$;

  public loading$;

  protected itemsSubject = new BehaviorSubject<T[]>([]);

  protected paginationInfoSubject = new BehaviorSubject<PaginationInfo>(null);

  protected loadingSubject = new BehaviorSubject<boolean>(false);

  constructor(protected api: ApiService<T>) {
    this.paginationInfo$ = this.paginationInfoSubject.asObservable();
    this.loading$ = this.loadingSubject.asObservable();
  }

  get(queryRequest: RequestQuery): void {
    this.loadingSubject.next(true);

    const apiRequest = this.api.getAll(queryRequest);

    forkJoin([apiRequest, interval(GLOBALS.LOADING_SPINNER_DELAY).pipe(take(1))])
      .pipe(
        finalize(() => {
          this.loadingSubject.next(false);
        })
      )
      .subscribe(value => {
        const result = value[0];
        this.itemsSubject.next(result.records);

        this.paginationInfoSubject.next({
          size: result.pageSize,
          page: result.pageNumber,
          totalElements: result.total
        });
      });
  }

  connect(): Observable<T[]> {
    return this.itemsSubject.asObservable();
  }

  disconnect(): void {
    this.itemsSubject.complete();
    this.loadingSubject.complete();
    this.paginationInfoSubject.complete();
  }
}
