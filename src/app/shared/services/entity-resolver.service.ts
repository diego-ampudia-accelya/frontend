import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';

/**
 * A base resolver to preload a single entity and make it available for a routed component
 */
export abstract class EntityResolverService<T> implements Resolve<T> {
  constructor(protected apiService: ApiService<T>) {}

  resolve(route: ActivatedRouteSnapshot): Observable<T> {
    const id = Number(route.paramMap.get('id'));

    // Load Entity by id.
    return this.apiService.getOne(id);
  }
}
