import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';

import { RequestQueryFilter } from '../models/request-query-filter.model';
import { ViewListServiceConfig } from '~app/shared/models/view-list-service-config.model';

export const VIEW_LIST_SERVICE_CONFIG = new InjectionToken<ViewListServiceConfig>('view-list.service.config');

@Injectable()
export class ViewListService {
  constructor(@Optional() @Inject(VIEW_LIST_SERVICE_CONFIG) public config: ViewListServiceConfig) {}

  formatRequestFilters(data: any): RequestQueryFilter[] | any {
    return this.config
      ? Object.entries<any>(data).map(([key, value]) => {
          const filterMapper = this.config.filterMappers[key];
          if (!filterMapper) {
            throw new Error(`Could not map filter '${key}'. Did you add a mapping rule in VIEW_LIST_SERVICE_CONFIG?`);
          }
          const { attribute, operation } = filterMapper;

          return {
            attribute,
            operation,
            attributeValue: filterMapper.valueMapper(value)
          };
        })
      : [];
  }
}
