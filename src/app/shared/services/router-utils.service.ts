import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterUtilsService {
  constructor(private router: Router) {}

  /** Reloads the current route and runs resolvers and guards. */
  // TODO Why not using TabService.refreshCurrentTab() ??
  public hardReloadCurrentRoute(route: ActivatedRoute): Promise<boolean> {
    const originalOnSameUrlNavigation = this.router.onSameUrlNavigation;
    const originalRunGuardsAndResolvers = route.routeConfig.runGuardsAndResolvers;

    this.router.onSameUrlNavigation = 'reload';
    route.routeConfig.runGuardsAndResolvers = 'always';

    return this.router.navigateByUrl(this.router.url).finally(() => {
      this.router.onSameUrlNavigation = originalOnSameUrlNavigation;
      route.routeConfig.runGuardsAndResolvers = originalRunGuardsAndResolvers;
    });
  }
}
