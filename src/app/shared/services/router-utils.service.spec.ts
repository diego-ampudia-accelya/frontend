import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { mockProvider } from '@ngneat/spectator';

import { RouterUtilsService } from './router-utils.service';

describe('RouterUtilsService', () => {
  let service: RouterUtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouterUtilsService, mockProvider(Router)]
    });

    service = TestBed.inject<RouterUtilsService>(RouterUtilsService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
