/* eslint-disable @typescript-eslint/naming-convention */
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { TabService } from './tab.service';
import { MenuTabActions } from '~app/core/actions';
import { AppState } from '~app/reducers';

describe('TabService', () => {
  let service: TabService;
  let store: Store<AppState>;
  const routerStub = jasmine.createSpyObj<Router>('Router', ['navigateByUrl']);
  const initialState = {
    core: {
      menu: {
        tabs: {
          Tab_One: {
            url: '/tab-one',
            id: 'Tab_One',
            sequenceNumber: 0
          },
          Tab_Two: {
            url: '/tab-two',
            id: 'Tab_Two',
            sequenceNumber: 1
          }
        },
        activeTabId: 'Tab_One'
      }
    }
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), { provide: Router, useValue: routerStub }]
    });

    service = TestBed.inject(TabService);
    store = TestBed.inject<any>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check correctly if a url is opened in current tab', fakeAsync(() => {
    let isCurrentTabOnUrl;
    const testUrl = '/tab-one';
    const currentTab = {
      url: testUrl,
      id: 'Tab_One',
      sequenceNumber: 0
    };
    spyOn<any>(service, 'getCurrentTab').and.returnValue(of(currentTab));

    service.isCurrentTabOnUrl(testUrl).subscribe(result => (isCurrentTabOnUrl = result));
    tick();

    expect(isCurrentTabOnUrl).toBe(true);
  }));

  it('should close current tab', () => {
    spyOn(service, 'closeTab');
    const currentTab = {
      url: '/tab-one',
      id: 'Tab_One',
      sequenceNumber: 0
    };

    service.closeCurrentTab();

    expect(service.closeTab).toHaveBeenCalledWith(currentTab);
  });

  it('should dispatch action Close from MenuTabActions', () => {
    spyOn(store, 'dispatch');
    const tab = {
      url: '/tab-one',
      path: '/tab-one',
      permissionId: null,
      parent: null,
      id: 'Tab_One',
      sequenceNumber: 0
    };

    service.closeTab(tab);

    expect(store.dispatch).toHaveBeenCalledWith(new MenuTabActions.Close(tab));
  });

  it('should open url and then close the current tab', fakeAsync(() => {
    routerStub.navigateByUrl.and.returnValue(Promise.resolve({}));
    spyOn(service, 'closeTab');
    const currentTab = {
      url: '/tab-one',
      id: 'Tab_One',
      sequenceNumber: 0
    };

    service.closeCurrentTabAndNavigate('/tab-two');

    expect(routerStub.navigateByUrl).toHaveBeenCalledWith('/tab-two');
    tick();
    expect(service.closeTab).toHaveBeenCalledWith(currentTab);
  }));
});
