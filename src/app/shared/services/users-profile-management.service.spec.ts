import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { SortOrder } from '../enums';
import { DropdownOption } from '../models';
import { RequestQuery } from '../models/request-query.model';
import { UserType } from '../models/user.model';

import { ApiService } from './api.service';
import { appConfiguration } from './app-configuration.service';
import { UsersProfileManagementService } from './users-profile-management.service';

describe('UsersProfileManagementService', () => {
  const baseApiPath = 'base-api';
  let userProfileService: UsersProfileManagementService;
  let httpClientMock: SpyObject<HttpClient>;

  beforeEach(() => {
    httpClientMock = createSpyObject(HttpClient);
    httpClientMock.get.and.returnValue(of());

    TestBed.configureTestingModule({
      providers: [
        UsersProfileManagementService,
        {
          provide: HttpClient,
          useValue: httpClientMock
        }
      ]
    });

    userProfileService = TestBed.inject(UsersProfileManagementService);
    spyOnProperty(appConfiguration, 'baseApiPath', 'get').and.returnValue(baseApiPath);
  });

  it('should be created', () => {
    expect(userProfileService).toBeTruthy();
  });

  it('should have a GET http request, when getAll is called.', fakeAsync(() => {
    const requestQuery = new RequestQuery(0, 20, 'test', SortOrder.Asc);

    userProfileService.getAll(requestQuery).subscribe();
    expect(httpClientMock.get).toHaveBeenCalled();
  }));

  it('should have GET http request, when getCurrentUserData is called.', fakeAsync(() => {
    const expectedUrl = `${baseApiPath}/user-mgmt-integration/security-integration/users/me`;

    userProfileService.getCurrentUserData().subscribe();
    tick();

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should normalize airline data, when getCurrentUserData is called.', fakeAsync(() => {
    const airlines = [
      { id: 1, localName: 'airline1' },
      { id: 2, localName: 'airline2' }
    ];
    const testUser = {
      userType: UserType.AIRLINE,
      globalAirline: {
        id: 6,
        localName: 'globalAirline',
        airlines
      }
    };
    httpClientMock.get.and.returnValue(of(testUser));
    testUser.globalAirline.airlines = airlines.map(airline => ({
      ...airline,
      globalAirline: { id: 6, localName: 'globalAirline' }
    }));

    let userResponse;
    userProfileService.getCurrentUserData().subscribe(user => (userResponse = user));
    tick();

    expect(userResponse).toEqual(testUser as any);
  }));

  it('should not normalize airline data, when getCurrentUserData is called and user is not airline.', fakeAsync(() => {
    const testUser = {
      id: 1,
      userType: UserType.GDS,
      name: 'Gosho'
    };
    httpClientMock.get.and.returnValue(of(testUser));

    let userData;
    userProfileService.getCurrentUserData().subscribe(user => (userData = user));
    tick();

    expect(userData).toEqual(testUser as any);
  }));

  it('should have GET http request, when getUserAirlines is called', fakeAsync(() => {
    const expectedUrl = `${baseApiPath}/user-mgmt-integration/airlines`;

    userProfileService.getUserAirlines().subscribe();
    tick();

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should have GET http request, when getUserAgents is called', fakeAsync(() => {
    const expectedUrl = `${baseApiPath}/user-mgmt-integration/agents`;

    userProfileService.getUserAgents().subscribe();
    tick();

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should have GET http request, when getUserDetailsByInternalId is called', fakeAsync(() => {
    const userId = 6;
    const expectedUrl = `${baseApiPath}/user-mgmt-integration/security-integration/users/${userId}`;

    userProfileService.getUserDetailsByInternalId(userId).subscribe();
    tick();

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should have GET http request and pass right params, when getUserAccounts is called', fakeAsync(() => {
    const expectedUrl = `${baseApiPath}/user-profile-management/users/me/accounts`;

    userProfileService.getUserAccounts().subscribe();
    tick();

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl, { params: {} });
  }));

  it('should have GET http request and pass right params, when getUserAccounts is called', fakeAsync(() => {
    const expectedUrl = `${baseApiPath}/user-profile-management/users/me/accounts`;

    userProfileService.getUserAccounts(6997).subscribe();
    tick();

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl, { params: { bspId: '6997' } });
  }));

  it('should have GET http request, when getUserAccountBsps is called', fakeAsync(() => {
    const expectedUrl = `${baseApiPath}/user-profile-management/users/me/accounts/bsps`;

    userProfileService.getUserAccountBsps().subscribe();
    tick();

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should returns formatted dropdowns, when getEmailDropDownOptions is called.', fakeAsync(() => {
    spyOn(userProfileService, 'getAll').and.returnValue(
      of({
        records: [
          { id: 1, email: 'first@email.com' },
          { id: 2, email: 'second@email.com' }
        ]
      })
    );

    let dropDownOptions: DropdownOption[];
    userProfileService.getEmailDropDownOptions(UserType.AIRLINE).subscribe(result => (dropDownOptions = result));
    tick();

    expect(dropDownOptions).toEqual([
      { value: 1, label: 'first@email.com' },
      { value: 2, label: 'second@email.com' }
    ]);
  }));

  it('should set filterBy iataCode, when request is called by existing iataCode.', fakeAsync(() => {
    userProfileService.getEmailDropDownOptions(UserType.IATA, '999').subscribe();
    tick();
    const expectedUrl = `${baseApiPath}/user-profile-management/users?page=0&size=99999&filters=userType%3A%3Aeq%3A%3Aiata%2CiataCode%3A%3Aeq%3A%3A999`;

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should not set filterBy iataCode, when iataCode is not passed.', fakeAsync(() => {
    userProfileService.getEmailDropDownOptions(UserType.IATA).subscribe();
    tick();
    const expectedUrl = `${baseApiPath}/user-profile-management/users?page=0&size=99999&filters=userType%3A%3Aeq%3A%3Aiata`;

    expect(httpClientMock.get).toHaveBeenCalledWith(expectedUrl);
  }));

  it('should call download from API service, when download is called', fakeAsync(() => {
    spyOn(ApiService.prototype, 'download').and.returnValue(of());
    userProfileService.download({}, 'csv').subscribe();
    tick();

    expect(ApiService.prototype.download).toHaveBeenCalled();
  }));

  it('should have correct mapping, when the user is agent', fakeAsync(() => {
    spyOn(userProfileService, 'getCurrentUserData').and.returnValue(
      of({
        userType: UserType.AGENT,
        agent: { id: 1, iataCode: 1, name: 'Gosho' }
      })
    );

    let dropDownOptions: DropdownOption[];
    userProfileService
      .getCurrentUserIataCodeDropdownOption(UserType.AGENT)
      .subscribe(result => (dropDownOptions = result));
    tick();

    expect(dropDownOptions).toEqual([{ value: 1, label: '1 / Gosho' }]);
  }));

  it('should have correct mapping, when the user is airline', fakeAsync(() => {
    spyOn(userProfileService, 'getCurrentUserData').and.returnValue(
      of({
        userType: UserType.AIRLINE,
        globalAirline: { id: 1, iataCode: 1, globalName: 'GoshoAir' }
      })
    );

    let dropDownOptions: DropdownOption[];
    userProfileService
      .getCurrentUserIataCodeDropdownOption(UserType.AIRLINE)
      .subscribe(result => (dropDownOptions = result));
    tick();

    expect(dropDownOptions).toEqual([{ value: 1, label: '1 / GoshoAir' }]);
  }));

  it('should have correct mapping, when the user is gds', fakeAsync(() => {
    spyOn(userProfileService, 'getCurrentUserData').and.returnValue(
      of({
        userType: UserType.GDS,
        gds: { id: 1, gdsCode: 1, name: 'GoshoGDS' }
      })
    );

    let dropDownOptions: DropdownOption[];
    userProfileService
      .getCurrentUserIataCodeDropdownOption(UserType.GDS)
      .subscribe(result => (dropDownOptions = result));
    tick();

    expect(dropDownOptions).toEqual([{ value: 1, label: '1 / GoshoGDS' }]);
  }));

  it('should NOT have mapping, when the user is not gds, airline or agent', fakeAsync(() => {
    spyOn(userProfileService, 'getCurrentUserData').and.returnValue(
      of({
        userType: UserType.IATA
      })
    );

    let methodResponse = [{ value: 6, label: 'label' }];
    userProfileService
      .getCurrentUserIataCodeDropdownOption(UserType.IATA)
      .subscribe(result => (methodResponse = result));
    tick();

    expect(methodResponse).toBeFalsy();
  }));
});
