import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { toValueLabelObject } from '~app/shared/helpers';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalAirlineDesignatorDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/airline-management/global-airlines/designators`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public get(): Observable<string[]> {
    return this.http.get<string[]>(this.baseUrl);
  }

  public getDropdownOptions(): Observable<DropdownOption<string>[]> {
    return this.get().pipe(
      map(records => records.map(toValueLabelObject)),
      catchError(() => of([]))
    );
  }
}
