import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { Airline } from '~app/master-data/models';
import { EQ } from '~app/shared/constants/operations';
import { SortOrder } from '~app/shared/enums';
import { AirlineSummary } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { RequestQuery } from '~app/shared/models/request-query.model';

@Injectable({
  providedIn: 'root'
})
export class AirlineDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/airline-management/airlines/dictionary`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public get(bspId?: number): Observable<AirlineSummary[]> {
    const query = new RequestQuery(null, null, 'globalAirline.iataCode', SortOrder.Asc);
    if (!isNaN(bspId)) {
      query.filterBy = [{ attribute: 'bsp.id', attributeValue: bspId, operation: EQ }];
    }
    query.reduceFilterBy = true;
    const url = `${this.baseUrl}${query.getQueryString()}`;

    return this.http.get<AirlineSummary[]>(url);
  }

  public getDropdownOptions(bspId?: number): Observable<DropdownOption<AirlineSummary>[]> {
    return this.get(bspId).pipe(
      map(records =>
        records.map(airline => ({
          value: airline,
          label: `${airline.code} / ${airline.name}`
        }))
      ),
      catchError(() => of([]))
    );
  }

  public summarize(airline: Airline): AirlineSummary {
    if (!airline) {
      return null;
    }

    return {
      id: airline.id,
      code: airline.globalAirline.iataCode,
      name: airline.globalAirline.globalName,
      designator: airline.designator
    };
  }
}
