import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { sortBy } from 'lodash';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { BspCurrencyAssignation } from '~app/master-data/models/bsp-currency-assignation.model';
import { EQ, IN } from '~app/shared/constants';
import { toValueLabelCurrency, toValueLabelCurrencyId } from '~app/shared/helpers';
import { CurrencyDictionaryFiltersAttributes, CurrencyDictionaryQueryFilters } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';

@Injectable({
  providedIn: 'root'
})
export class CurrencyDictionaryService {
  private baseManagementUrl = `${this.appConfiguration.baseApiPath}/bsp-management/bsps`;
  private baseTipUrl = `${this.appConfiguration.baseApiPath}/tip-management/bsps`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  private getCurrencies(): Observable<Currency[]> {
    return this.http.get<PagedData<BspCurrencyAssignation>>(`${this.baseManagementUrl}/currency-assignments`).pipe(
      map(({ records }) =>
        sortBy(
          records.map(({ currency }) => currency),
          'code'
        )
      )
    );
  }

  @Cacheable()
  private getCurrenciesByBsp(bspId: number): Observable<Currency[]> {
    return this.http.get<BspCurrencyAssignation[]>(`${this.baseManagementUrl}/${bspId}/currency-assignments`).pipe(
      map(currencyAssignations =>
        sortBy(
          currencyAssignations.map(({ currency }) => currency),
          'code'
        )
      )
    );
  }

  @Cacheable()
  private getTipCurrenciesByBsp(bspId: number, active: boolean): Observable<Currency[]> {
    const params = { ...(active !== undefined && { active: '' + active }) };

    return this.http
      .get<Currency[]>(`${this.baseTipUrl}/${bspId}/currencies`, { params })
      .pipe(map(currencies => sortBy(currencies, 'code')));
  }

  @Cacheable()
  private getFilteredCurrencies(queryFilters?: CurrencyDictionaryQueryFilters): Observable<Currency[]> {
    const query = new RequestQuery(null, null, null, null);

    if (queryFilters) {
      query.filterBy = this.formatFilter(queryFilters);
    }
    query.reduceFilterBy = true;

    return this.http.get<Currency[]>(`${this.baseManagementUrl}/currencies${query.getQueryString()}`);
  }

  public getDropdownOptions(bspId?: number): Observable<DropdownOption<Currency>[]> {
    const currencies: Observable<Currency[]> = bspId ? this.getCurrenciesByBsp(bspId) : this.getCurrencies();

    return currencies.pipe(
      map(items => items.map(toValueLabelCurrency)),
      catchError(() => of([]))
    );
  }

  public getDropdownOptionsByValueId(bspId?: number): Observable<DropdownOption[]> {
    const currencies: Observable<Currency[]> = bspId ? this.getCurrenciesByBsp(bspId) : this.getCurrencies();

    return currencies.pipe(
      map(items => items.map(toValueLabelCurrencyId)),
      catchError(() => of([]))
    );
  }

  public getOne(bspId: number, currencyCode: string): Observable<Currency> {
    return this.getDropdownOptions(bspId).pipe(
      map(currencies => currencies.find(currency => currencyCode === currency.label)),
      map(currency => currency.value)
    );
  }

  public getTipDropdownOptions(bspId: number, active?: boolean): Observable<DropdownOption<Currency>[]> {
    return this.getTipCurrenciesByBsp(bspId, active).pipe(
      map(items => items.map(toValueLabelCurrency)),
      catchError(() => of([]))
    );
  }

  public getFilteredDropdownOptions(
    queryFilters?: CurrencyDictionaryQueryFilters
  ): Observable<DropdownOption<Currency>[]> {
    return this.getFilteredCurrencies(queryFilters).pipe(
      map(items => items.map(toValueLabelCurrency)),
      catchError(() => of([]))
    );
  }

  private formatFilter(queryFilters: CurrencyDictionaryQueryFilters): RequestQueryFilter[] {
    const formattedRequestFilters: RequestQueryFilter[] = [];

    Object.entries<any>(queryFilters).forEach(([key, value]) => {
      const attributeValue = value;
      let attribute = key;
      let operation = EQ;

      if (key === 'bspId') {
        attribute = CurrencyDictionaryFiltersAttributes.bspId;
        operation = Array.isArray(value) ? IN : EQ;
      }

      formattedRequestFilters.push({
        attribute,
        operation,
        attributeValue
      });
    });

    return formattedRequestFilters;
  }
}
