import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { GlobalAirlineSummary } from '~app/shared/models/dictionary/global-airline-summary.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalAirlineDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/airline-management/global-airlines/dictionary`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public get(): Observable<GlobalAirlineSummary[]> {
    return this.http.get<GlobalAirlineSummary[]>(this.baseUrl);
  }

  public getDropdownOptions(): Observable<DropdownOption<GlobalAirlineSummary>[]> {
    return this.get().pipe(
      map(records =>
        records.map(globalAirline => ({
          value: globalAirline,
          label: `${globalAirline.iataCode} / ${globalAirline.globalName}`
        }))
      ),
      catchError(() => of([]))
    );
  }
}
