import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { of, throwError } from 'rxjs';
import { globalCacheBusterNotifier } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { CurrencyDictionaryService } from './currency-dictionary.service';
import { BspCurrencyAssignation } from '~app/master-data/models/bsp-currency-assignation.model';
import { DropdownOption } from '~app/shared/models';
import { Currency } from '~app/shared/models/currency.model';
import { PagedData } from '~app/shared/models/paged-data.model';

describe('CurrencyDictionaryService', () => {
  let service: CurrencyDictionaryService;
  let httpSpy: SpyObject<HttpClient>;

  const mockCurrencyAssignations: BspCurrencyAssignation[] = [
    {
      id: 1,
      default: false,
      effectiveFrom: '2000-01-01',
      effectiveTo: '2040-01-01',
      active: true,
      bsp: {
        id: 1,
        name: 'SPAIN',
        isoCountryCode: 'ES',
        effectiveFrom: '2000-01-01'
      },
      currency: {
        id: 1,
        code: 'GBP',
        decimals: 2
      },
      decimals: 2
    }
  ];

  const mockPagedData: PagedData<BspCurrencyAssignation> = {
    records: mockCurrencyAssignations,
    pageSize: 20,
    total: 0,
    totalPages: 0,
    pageNumber: 0
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CurrencyDictionaryService,
        mockProvider(HttpClient),
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } }
      ]
    });

    service = TestBed.inject(CurrencyDictionaryService);
    httpSpy = TestBed.inject<any>(HttpClient);

    globalCacheBusterNotifier.next();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be call http.get with proper url - with bsp id param', fakeAsync(() => {
    httpSpy.get.and.returnValue(of(mockCurrencyAssignations));

    service.getDropdownOptions(1).subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith('/bsp-management/bsps/1/currency-assignments');
  }));

  it('should be call http.get with proper url - with no params', fakeAsync(() => {
    httpSpy.get.and.returnValue(of(mockPagedData));

    service.getDropdownOptions().subscribe();

    expect(httpSpy.get).toHaveBeenCalledWith('/bsp-management/bsps/currency-assignments');
  }));

  it('should return currencies - with bsp id param', fakeAsync(() => {
    httpSpy.get.and.returnValue(of(mockCurrencyAssignations));

    let actual: DropdownOption<Currency>[];
    service.getDropdownOptions(1).subscribe(currencies => (actual = currencies));

    expect(actual).toEqual([
      {
        value: {
          id: 1,
          code: 'GBP',
          decimals: 2
        },
        label: 'GBP'
      }
    ]);
  }));

  it('should return currencies - with no params', fakeAsync(() => {
    httpSpy.get.and.returnValue(of(mockPagedData));

    let actual: DropdownOption<Currency>[];
    service.getDropdownOptions().subscribe(currencies => (actual = currencies));

    expect(actual).toEqual([
      {
        value: {
          id: 1,
          code: 'GBP',
          decimals: 2
        },
        label: 'GBP'
      }
    ]);
  }));

  it('should return [] currencies when no currency assignations are available - with bsp id param', fakeAsync(() => {
    httpSpy.get.and.returnValue(of([]));

    let actual: DropdownOption<Currency>[];
    service.getDropdownOptions(1).subscribe(currencies => (actual = currencies));

    expect(actual).toEqual([]);
  }));

  it('should return [] currencies when no currency assignations are available - with no params', fakeAsync(() => {
    const mockEmptyPagedData: PagedData<BspCurrencyAssignation> = {
      records: [],
      pageSize: 20,
      total: 0,
      totalPages: 0,
      pageNumber: 0
    };

    httpSpy.get.and.returnValue(of(mockEmptyPagedData));

    let actual: DropdownOption<Currency>[];
    service.getDropdownOptions().subscribe(currencies => (actual = currencies));

    expect(actual).toEqual([]);
  }));

  it('should return [] currencies when http.get throws an error', fakeAsync(() => {
    httpSpy.get.and.returnValue(throwError(new Error()));

    let actual: DropdownOption<Currency>[];
    service.getDropdownOptions(1).subscribe(currencies => (actual = currencies));

    expect(actual).toEqual([]);
  }));
});
