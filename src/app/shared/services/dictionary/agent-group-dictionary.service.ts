import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { AgentGroup } from '~app/master-data/models/agent-group.model';
import { IN } from '~app/shared/constants/operations';
import {
  AgentGroupDictionaryFiltersAttributes,
  AgentGroupDictionaryQueryFilters,
  AgentGroupSummary
} from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { RequestQuery } from '~app/shared/models/request-query.model';

@Injectable({
  providedIn: 'root'
})
export class AgentGroupDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/agent-management/groups/dictionary`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public get(queryFilters?: AgentGroupDictionaryQueryFilters): Observable<AgentGroupSummary[]> {
    const query = new RequestQuery(null, null, null, null);

    if (queryFilters) {
      query.filterBy = [
        { attribute: AgentGroupDictionaryFiltersAttributes.bspId, attributeValue: queryFilters.bspId, operation: IN }
      ];
    }
    query.reduceFilterBy = true;
    const url = `${this.baseUrl}${query.getQueryString()}`;

    return this.http.get<AgentGroupSummary[]>(url);
  }

  public getDropdownOptions(
    queryFilters?: AgentGroupDictionaryQueryFilters
  ): Observable<DropdownOption<AgentGroupSummary>[]> {
    return this.get(queryFilters).pipe(
      map(records =>
        records.map(agentGroup => ({
          value: agentGroup,
          label: `${agentGroup.bsp.isoCountryCode} - ${agentGroup.code} / ${agentGroup.name}`
        }))
      ),
      catchError(() => of([]))
    );
  }

  public summarize(agentGroup: AgentGroup): AgentGroupSummary {
    if (!agentGroup) {
      return null;
    }

    return {
      id: agentGroup.id,
      code: agentGroup.iataCode,
      name: agentGroup.lastName,
      bsp: agentGroup.bsp
    };
  }
}
