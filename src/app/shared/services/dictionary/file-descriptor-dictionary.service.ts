import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { DropdownOption } from '~app/shared/models';
import { FileDescriptorSummary } from '~app/shared/models/dictionary/file-descriptor-summary';
import { RequestQuery } from '~app/shared/models/request-query.model';

@Injectable({
  providedIn: 'root'
})
export class FileDescriptorDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/file-management/file-descriptors/dictionary`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public get(): Observable<FileDescriptorSummary[]> {
    const query = new RequestQuery(null, null, null, null);
    query.reduceFilterBy = true;

    return this.http.get<FileDescriptorSummary[]>(`${this.baseUrl}${query.getQueryString()}`);
  }

  public getDropdownOptions(): Observable<DropdownOption<FileDescriptorSummary>[]> {
    return this.get().pipe(
      map(records =>
        records.map(fileDescriptor => ({
          value: fileDescriptor,
          label: `${fileDescriptor.descriptor} / ${fileDescriptor.description}`
        }))
      ),
      catchError(() => of([]))
    );
  }
}
