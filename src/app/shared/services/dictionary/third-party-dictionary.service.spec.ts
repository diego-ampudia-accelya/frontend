import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { GlobalCacheConfig } from 'ts-cacheable';

import { DropdownOption, ThirdPartySummary } from '../../models';
import { AppConfigurationService } from '../app-configuration.service';

import { ThirdPartyDictionaryService } from './third-party-dictionary.service';
import { EmptyPipe } from '~app/shared/pipes/empty.pipe';
import { ThirdParty } from '~app/master-data/models';

describe('Third-party dictionary service', () => {
  const httpServiceMock = createSpyObject(HttpClient);
  let service: ThirdPartyDictionaryService;
  httpServiceMock.get.and.returnValue(of([]));

  const thirdPartySummary: ThirdPartySummary[] = [
    {
      id: 234234,
      name: 'Third-party 1',
      code: '2342'
    },
    {
      id: 7689,
      name: null,
      code: '7688'
    },
    {
      id: 7893,
      name: 'Third-party 3',
      code: '7684'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpServiceMock },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        EmptyPipe
      ]
    });

    GlobalCacheConfig.maxCacheCount = 0;
    service = TestBed.inject(ThirdPartyDictionaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http get method with additional parameters when get method is called with bsp', () => {
    const bspId = 'ES';
    service.get(bspId);

    expect(httpServiceMock.get).toHaveBeenCalledWith(`/third-parties/third-parties?bspCode=${bspId}`);
  });

  it('should return list with third-parties as a list dropdown options', () => {
    spyOn(service, 'get').and.returnValue(of(thirdPartySummary));

    const expected: DropdownOption<ThirdPartySummary>[] = [
      {
        value: {
          id: 234234,
          name: 'Third-party 1',
          code: '2342'
        },
        label: '2342 / Third-party 1'
      },
      {
        value: {
          id: 7689,
          name: null,
          code: '7688'
        },
        label: '7688 —'
      },
      {
        value: {
          id: 7893,
          name: 'Third-party 3',
          code: '7684'
        },
        label: '7684 / Third-party 3'
      }
    ];
    service.getDropdownOptions('ES').subscribe(thirdParties => {
      expect(thirdParties).toEqual(expected);
    });
  });

  it('should return [] if get method throws an error', () => {
    spyOn(service, 'get').and.returnValue(of(new Error('Service unavailable')));

    service.getDropdownOptions('ES').subscribe(thirdParties => {
      expect(thirdParties).toEqual([]);
    });
  });

  it('should return summary of the third-party', () => {
    const thirdParty: ThirdParty = {
      id: 234234,
      name: 'Third-party',
      code: '3243',
      active: true,
      organization: 'Organisation'
    };

    const result = service.summarize(thirdParty);

    const expected: ThirdPartySummary = {
      id: 234234,
      name: 'Third-party',
      code: '3243'
    };

    expect(result).toEqual(expected);
  });

  it('should return null if no third-party is provided', () => {
    const thirdParty = null;

    expect(service.summarize(thirdParty)).toEqual(null);
  });
});
