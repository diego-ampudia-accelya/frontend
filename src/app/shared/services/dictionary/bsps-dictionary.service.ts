import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';
import { toValueLabelObjectBsp } from '~app/shared/helpers/filters.helper';
import { DropdownOption } from '~app/shared/models';
import { BspDto } from '~app/shared/models/bsp.model';
import { BspPermissions } from '~app/shared/models/permissions.model';
import { AppConfigurationService } from '../app-configuration.service';

@Injectable({ providedIn: 'root' })
export class BspsDictionaryService {
  private baseUrlUserManagement = `${this.appConfiguration.baseApiPath}/user-management/users`;
  private baseUrlBspManagement = `${this.appConfiguration.baseApiPath}/bsp-management/bsps`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  public getBspsByUserId(userId: number): Observable<BspDto[]> {
    return this.http.get<BspDto[]>(`${this.baseUrlUserManagement}/${userId}/bsps`);
  }

  @Cacheable()
  public getAllBsps(): Observable<BspDto[]> {
    const dictionaryUrl = `${this.baseUrlBspManagement}/dictionary`;

    return this.http.get<BspDto[]>(dictionaryUrl);
  }

  public getAllBspDropdownOptions(): Observable<DropdownOption<BspDto>[]> {
    return this.getAllBsps().pipe(
      map(bsps => bsps.map(toValueLabelObjectBsp)),
      catchError(() => of([]))
    );
  }

  public getDropdownOptions(userId: number): Observable<DropdownOption<BspDto>[]> {
    return this.getAllBspsFromUser(userId).pipe(map(data => data.map(toValueLabelObjectBsp)));
  }

  public getAllDropdownOptions(): Observable<DropdownOption<BspDto>[]> {
    return this.getAllBsps().pipe(map(data => data.map(toValueLabelObjectBsp)));
  }

  public getBspsFromCurrency(currencyId: number): Observable<BspDto[]> {
    const currencyUrl = `${this.baseUrlBspManagement}/currency-assignments/${currencyId}/bsps`;

    return this.http.get<BspDto[]>(currencyUrl);
  }

  public getAllBspsFromUser(userId: number): Observable<BspDto[]> {
    const matchKey: keyof BspDto = 'id';

    return combineLatest([this.getBspsByUserId(userId), this.getAllBsps()]).pipe(
      map(([userBsps, allBsps]) => this.leftJoinByBspKey(userBsps, allBsps, matchKey)),
      catchError(() => of([]))
    );
  }

  /**
   * Gets all BSPs from the dictionary endpoint and then filters them comparing user permissions (`bspPermissions`) and permissions specified (`permissions`)
   *
   * @param bspPermissions All permissions that the user has assigned to each BSP, usually within the `User` model
   * @param permissions Some permissions that the user must have (at least one of them) assigned to the BSP in order to be returned
   * @return All BSPs on which the user has at least one the permissions specified via second parameter
   */
  public getAllBspsByPermissions(bspPermissions: BspPermissions[], permissions: string[]): Observable<BspDto[]> {
    return this.getAllBsps().pipe(
      map(bsps =>
        bsps.filter(bsp =>
          bspPermissions.some(
            bspPerm => bsp.id === bspPerm.bspId && permissions.some(perm => bspPerm.permissions.includes(perm))
          )
        )
      )
    );
  }

  private leftJoinByBspKey(userBsps: BspDto[], allBsps: BspDto[], matchKey: string) {
    return userBsps.map(userBsp => ({
      ...allBsps.find(bsp => userBsp[matchKey] === bsp[matchKey]),
      ...userBsp
    }));
  }
}
