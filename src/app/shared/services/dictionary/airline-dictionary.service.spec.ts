import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { GlobalCacheConfig } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { AirlineDictionaryService } from './airline-dictionary.service';
import { Airline } from '~app/master-data/models';
import { AirlineSummary, DropdownOption } from '~app/shared/models';

describe('Airline dictionary service', () => {
  const httpServiceMock = createSpyObject(HttpClient);
  let service: AirlineDictionaryService;
  httpServiceMock.get.and.returnValue(of([]));

  const airlineSummary: AirlineSummary[] = [
    {
      id: 23423423,
      name: 'Airline Test 1',
      code: '2342',
      designator: 'SD'
    },
    {
      id: 768966,
      name: 'Airline Test 2',
      code: '7688',
      designator: 'HG'
    },
    {
      id: 789345,
      name: 'Airline Test 3',
      code: '7684',
      designator: 'SW'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpServiceMock },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } }
      ]
    });

    GlobalCacheConfig.maxCacheCount = 0;
    service = TestBed.inject(AirlineDictionaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http get method on get method of the service and have default sorting', () => {
    service.get();

    expect(httpServiceMock.get).toHaveBeenCalledWith(
      '/airline-management/airlines/dictionary?sort=globalAirline.iataCode%3A%3AASC'
    );
  });

  it('should call http get method with additional parameters when get method is called with bsp', () => {
    const bspId = 6554;
    service.get(bspId);

    expect(httpServiceMock.get).toHaveBeenCalledWith(
      `/airline-management/airlines/dictionary?sort=globalAirline.iataCode%3A%3AASC&filters=bsp.id%3A%3Aeq%3A%3A${bspId}`
    );
  });

  it('should return airline list as a list dropdown options', () => {
    spyOn(service, 'get').and.returnValue(of(airlineSummary));

    const expected: DropdownOption<AirlineSummary>[] = [
      {
        value: {
          id: 23423423,
          name: 'Airline Test 1',
          code: '2342',
          designator: 'SD'
        },
        label: '2342 / Airline Test 1'
      },
      {
        value: {
          id: 768966,
          name: 'Airline Test 2',
          code: '7688',
          designator: 'HG'
        },
        label: '7688 / Airline Test 2'
      },
      {
        value: {
          id: 789345,
          name: 'Airline Test 3',
          code: '7684',
          designator: 'SW'
        },
        label: '7684 / Airline Test 3'
      }
    ];

    service.getDropdownOptions(3423).subscribe(airlines => {
      expect(airlines).toEqual(expected);
    });
  });

  it('should return [] if get method throws an error', () => {
    spyOn(service, 'get').and.returnValue(of(new Error('Service unavailable')));

    service.getDropdownOptions(5634).subscribe(airlines => {
      expect(airlines).toEqual([]);
    });
  });

  it('should return summary of the airline', () => {
    const airline: Airline = {
      id: 4533,
      localName: 'Airline',
      bsp: null,
      designator: 'DF',
      globalAirline: {
        iataCode: '1232',
        id: 234,
        globalName: 'Global Airline'
      },
      active: true,
      effectiveFrom: '2020-01-23'
    };

    const result = service.summarize(airline);

    const expected: AirlineSummary = {
      id: 4533,
      name: 'Global Airline',
      code: '1232',
      designator: 'DF'
    };

    expect(result).toEqual(expected);
  });

  it('should return null if no airline is provided', () => {
    const airline = null;

    expect(service.summarize(airline)).toEqual(null);
  });
});
