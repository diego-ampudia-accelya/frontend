import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { PermissionsService } from '~app/auth/services/permissions.service';
import { Agent } from '~app/master-data/models';
import { EQ, IN } from '~app/shared/constants/operations';
import { AgentDictionaryFiltersAttributes, AgentDictionaryQueryFilters, AgentSummary } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';

@Injectable({
  providedIn: 'root'
})
export class AgentDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/agent-management/agents/dictionary`;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private permissionsService: PermissionsService
  ) {}

  @Cacheable()
  public get(queryFilters?: AgentDictionaryQueryFilters): Observable<AgentSummary[]> {
    const query = new RequestQuery(null, null, null, null);

    if (queryFilters) {
      query.filterBy = this.formatFilter(queryFilters);
    }
    query.reduceFilterBy = true;

    return this.http.get<AgentSummary[]>(`${this.baseUrl}${query.getQueryString()}`);
  }

  public getDropdownOptions(queryFilters?: AgentDictionaryQueryFilters): Observable<DropdownOption<AgentSummary>[]> {
    return this.get(queryFilters).pipe(
      map(records =>
        records.map(agent => ({
          value: agent,
          label: `${agent.bsp.isoCountryCode} - ${agent.code} / ${agent.name}`
        }))
      ),
      catchError(() => of([]))
    );
  }

  public getAgentGroupUserDropdownOptions(permission: string): Observable<DropdownOption<AgentSummary>[]> {
    const hasPermission = this.permissionsService.hasPermission(permission);
    let agentGroupDropdown: Observable<DropdownOption<AgentSummary>[]> = of([]);

    if (hasPermission) {
      const filter = { permission };

      agentGroupDropdown = this.getDropdownOptions(filter);
    }

    return agentGroupDropdown;
  }

  public summarize(agent: Agent): AgentSummary {
    if (!agent) {
      return null;
    }

    return {
      id: agent.id.toString(),
      name: agent.name,
      code: agent.iataCode,
      bsp: agent.bsp
    };
  }

  private formatFilter(queryFilters: AgentDictionaryQueryFilters): RequestQueryFilter[] {
    const formattedRequestFilters: RequestQueryFilter[] = [];

    Object.entries<any>(queryFilters).forEach(([key, value]) => {
      const attributeValue = value;
      let attribute = key;
      let operation = EQ;

      switch (key) {
        case 'bspId':
          attribute = AgentDictionaryFiltersAttributes.bspId;
          operation = Array.isArray(value) ? IN : EQ;
          break;
        case 'active':
          attribute = AgentDictionaryFiltersAttributes.active;
          break;
        case 'permission':
          attribute = AgentDictionaryFiltersAttributes.permission;
          operation = Array.isArray(value) ? IN : EQ;
          break;
        case 'agentGroupId':
          attribute = AgentDictionaryFiltersAttributes.agentGroupId;
          operation = EQ;
          break;
      }

      formattedRequestFilters.push({
        attribute,
        operation,
        attributeValue
      });
    });

    return formattedRequestFilters;
  }
}
