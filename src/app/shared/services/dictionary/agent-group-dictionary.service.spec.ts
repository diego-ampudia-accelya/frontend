import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { createSpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';
import { GlobalCacheConfig } from 'ts-cacheable';

import { AgentGroupSummary, DropdownOption } from '../../models';
import { AppConfigurationService } from '../app-configuration.service';

import { AgentGroupDictionaryService } from './agent-group-dictionary.service';
import { BspDto } from '~app/shared/models/bsp.model';
import { AgentGroup } from '~app/master-data/models/agent-group.model';

describe('Airline dictionary service', () => {
  const httpServiceMock = createSpyObject(HttpClient);
  let service: AgentGroupDictionaryService;
  httpServiceMock.get.and.returnValue(of([]));

  const agentGroupSummary: AgentGroupSummary[] = [
    {
      id: 23423423,
      name: 'Agent Group Test 1',
      code: '2342',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
    },
    {
      id: 768966,
      name: 'Agent Group Test 2',
      code: '7688',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
    },
    {
      id: 789345,
      name: 'Agent Group Test 3',
      code: '7684',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpServiceMock },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } }
      ]
    });

    GlobalCacheConfig.maxCacheCount = 0;
    service = TestBed.inject(AgentGroupDictionaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http get method with additional parameters when get method is called with bsp', () => {
    const bspId = 6554;
    service.get({ bspId });

    expect(httpServiceMock.get).toHaveBeenCalledWith(
      `/agent-management/groups/dictionary?filters=bsp.id%3A%3Ain%3A%3A${bspId}`
    );
  });

  it('should return agent group list as a list dropdown options', fakeAsync(() => {
    spyOn(service, 'get').and.returnValue(of(agentGroupSummary));

    const expected: DropdownOption<AgentGroupSummary>[] = [
      {
        value: {
          id: 23423423,
          name: 'Agent Group Test 1',
          code: '2342',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
        },
        label: 'ES - 2342 / Agent Group Test 1'
      },
      {
        value: {
          id: 768966,
          name: 'Agent Group Test 2',
          code: '7688',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
        },
        label: 'ES - 7688 / Agent Group Test 2'
      },
      {
        value: {
          id: 789345,
          name: 'Agent Group Test 3',
          code: '7684',
          bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
        },
        label: 'ES - 7684 / Agent Group Test 3'
      }
    ];
    let result;
    service.getDropdownOptions({ bspId: 3423 }).subscribe(agentGroup => {
      result = agentGroup;
    });
    tick();

    expect(result).toEqual(expected);
  }));

  it('should return [] if get method throws an error', () => {
    spyOn(service, 'get').and.returnValue(of(new Error('Service unavailable')));

    service.getDropdownOptions({ bspId: 5634 }).subscribe(agentGroup => {
      expect(agentGroup).toEqual([]);
    });
  });

  it('should return summary of the agent group', () => {
    const agentGroup: AgentGroup = {
      id: 4533,
      iataCode: '12345',
      lastName: 'Agent Group Name',
      active: true,
      organization: 'Agent Group Org',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
    };

    const result = service.summarize(agentGroup);

    const expected: AgentGroupSummary = {
      id: 4533,
      name: 'Agent Group Name',
      code: '12345',
      bsp: { id: 1, name: 'SPAIN', isoCountryCode: 'ES' } as BspDto
    };

    expect(result).toEqual(expected);
  });
});
