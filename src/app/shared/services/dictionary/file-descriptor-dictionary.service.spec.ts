import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of, throwError } from 'rxjs';

import { AppConfigurationService } from '../app-configuration.service';

import { FileDescriptorDictionaryService } from './file-descriptor-dictionary.service';
import { RequestQuery } from '~app/shared/models/request-query.model';

describe('FileDescriptorDictionaryService', () => {
  let service: FileDescriptorDictionaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AppConfigurationService,
          useValue: {
            baseApiPath: 'test'
          }
        }
      ]
    });
    service = TestBed.inject(FileDescriptorDictionaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call get with base url and query', fakeAsync(() => {
    (service as any).http.get = jasmine.createSpy().and.returnValue(of([]));
    const query = new RequestQuery(null, null, null, null);
    query.reduceFilterBy = true;

    service.get().subscribe();
    tick();

    expect((service as any).http.get).toHaveBeenCalledWith(
      `test/file-management/file-descriptors/dictionary${query.getQueryString()}`
    );
  }));

  describe('getDropdownOptions', () => {
    it('should call get', fakeAsync(() => {
      service.get = jasmine.createSpy().and.returnValue(of([]));

      service.getDropdownOptions().subscribe();
      tick();

      expect(service.get).toHaveBeenCalled();
    }));

    it('should get mapped response from get()', fakeAsync(() => {
      service.get = jasmine.createSpy().and.returnValue(
        of([
          {
            descriptor: 'dr1',
            description: 'dn1'
          },
          {
            descriptor: 'dr2',
            description: 'dn2'
          }
        ])
      );
      const expectedResult = [
        {
          value: {
            descriptor: 'dr1',
            description: 'dn1'
          },
          label: 'dr1 / dn1'
        },
        {
          value: {
            descriptor: 'dr2',
            description: 'dn2'
          },
          label: 'dr2 / dn2'
        }
      ];
      let result;

      service.getDropdownOptions().subscribe(data => (result = data));
      tick();

      expect(result).toEqual(expectedResult);
    }));

    it('should get empty array when error is occured', fakeAsync(() => {
      service.get = jasmine.createSpy().and.returnValue(throwError({}));
      let result;

      service.getDropdownOptions().subscribe(data => (result = data));
      tick();

      expect(result).toEqual([]);
    }));
  });
});
