import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { ThirdParty } from '~app/master-data/models';
import { DropdownOption, ThirdPartySummary } from '~app/shared/models';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { EmptyPipe, EMPTY_VALUE } from '~app/shared/pipes/empty.pipe';

@Injectable({
  providedIn: 'root'
})
export class ThirdPartyDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/third-parties/third-parties`;

  constructor(
    private http: HttpClient,
    private appConfiguration: AppConfigurationService,
    private emptyPipe: EmptyPipe
  ) {}

  @Cacheable()
  public get(bspCode: string): Observable<ThirdPartySummary[]> {
    const query = new RequestQuery(null, null, null, null);
    query.filterBy = { bspCode };
    const url = `${this.baseUrl}${query.getQueryString()}`;

    return this.http.get<ThirdPartySummary[]>(url);
  }

  public getDropdownOptions(bspCode: string): Observable<DropdownOption<ThirdPartySummary>[]> {
    return this.get(bspCode).pipe(
      map(records =>
        records.map(thirdParty => {
          const thirdPartyName = this.emptyPipe.transform(thirdParty.name);
          const thirdPartyNameLabel = thirdPartyName !== EMPTY_VALUE ? ` / ${thirdPartyName}` : ` ${thirdPartyName}`;

          return {
            value: thirdParty,
            label: `${thirdParty.code}${thirdPartyNameLabel}`
          };
        })
      ),
      catchError(() => of([]))
    );
  }

  public summarize(thirdParty: ThirdParty): ThirdPartySummary {
    if (!thirdParty) {
      return null;
    }

    return {
      id: thirdParty.id,
      name: thirdParty.name,
      code: thirdParty.code
    };
  }
}
