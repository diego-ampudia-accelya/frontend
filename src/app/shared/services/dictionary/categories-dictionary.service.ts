import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

import { AppConfigurationService } from '../app-configuration.service';
import { Category } from '~app/shared/models';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesDictionaryService {
  private baseUrl = `${this.appConfiguration.baseApiPath}/acdm-management/categories/primary-reasons`;

  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  @Cacheable()
  private get(id?: number): Observable<any[]> {
    const url = isNaN(id) ? this.baseUrl : `${this.baseUrl}/${id}/sub-reasons`;

    return this.http.get<Category[]>(url);
  }

  public getDropdownOptions(id?: number): Observable<DropdownOption<Category>[]> {
    return this.get(id).pipe(
      map(records =>
        records.map(category => ({
          value: category,
          label: `${isNaN(id) ? category.primaryReason : category.subReason}`
        }))
      ),
      catchError(() => of([]))
    );
  }
}
