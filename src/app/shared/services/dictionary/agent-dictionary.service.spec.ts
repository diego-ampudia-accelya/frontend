import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { createSpyObject, mockProvider } from '@ngneat/spectator';
import { of } from 'rxjs';
import { GlobalCacheConfig } from 'ts-cacheable';

import { Agent } from '../../../master-data/models/agent.model';
import { AgentSummary, DropdownOption } from '../../models';
import { AppConfigurationService } from '../app-configuration.service';

import { AgentDictionaryService } from './agent-dictionary.service';
import { PermissionsService } from '~app/auth/services/permissions.service';

describe('Agent dictionary service', () => {
  const httpServiceMock = createSpyObject(HttpClient);
  let service: AgentDictionaryService;
  httpServiceMock.get.and.returnValue(of([]));

  const agentSummary: AgentSummary[] = [
    {
      id: '23423423',
      name: 'Agent Test 1',
      code: '2342',
      bsp: {
        id: 2342,
        isoCountryCode: 'ES'
      }
    },
    {
      id: '768966',
      name: 'Agent Test 2',
      code: '7688',
      bsp: {
        id: 7688,
        isoCountryCode: 'ES'
      }
    },
    {
      id: '789345',
      name: 'Agent Test 3',
      code: '7684',
      bsp: {
        id: 7684,
        isoCountryCode: 'ES'
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpServiceMock },
        { provide: AppConfigurationService, useValue: { baseApiPath: '' } },
        mockProvider(PermissionsService)
      ]
    });

    GlobalCacheConfig.maxCacheCount = 0;
    service = TestBed.inject(AgentDictionaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http get method on get method of the service', () => {
    service.get();

    expect(httpServiceMock.get).toHaveBeenCalledWith('/agent-management/agents/dictionary');
  });

  it('should call http get method with additional parameters when get method is called with bsp', () => {
    const bspId = 6554;
    service.get({ bspId });

    expect(httpServiceMock.get).toHaveBeenCalledWith(
      `/agent-management/agents/dictionary?filters=bsp.id%3A%3Aeq%3A%3A${bspId}`
    );
  });

  it('should return agent list as a list dropdown options', () => {
    spyOn(service, 'get').and.returnValue(of(agentSummary));

    const expected: DropdownOption<AgentSummary>[] = [
      {
        value: {
          id: '23423423',
          name: 'Agent Test 1',
          code: '2342',
          bsp: {
            id: 2342,
            isoCountryCode: 'ES'
          }
        },
        label: 'ES - 2342 / Agent Test 1'
      },
      {
        value: {
          id: '768966',
          name: 'Agent Test 2',
          code: '7688',
          bsp: {
            id: 7688,
            isoCountryCode: 'ES'
          }
        },
        label: 'ES - 7688 / Agent Test 2'
      },
      {
        value: {
          id: '789345',
          name: 'Agent Test 3',
          code: '7684',
          bsp: {
            id: 7684,
            isoCountryCode: 'ES'
          }
        },
        label: 'ES - 7684 / Agent Test 3'
      }
    ];
    service.getDropdownOptions({ bspId: [5634] }).subscribe(agents => {
      expect(agents).toEqual(expected);
    });
  });

  it('should return [] if get method throws an error', () => {
    spyOn(service, 'get').and.returnValue(of(new Error('Service unavailable')));

    service.getDropdownOptions({ bspId: 5634 }).subscribe(agents => {
      expect(agents).toEqual([]);
    });
  });

  it('should return summary of the agent', () => {
    const agent: Agent = {
      id: 234234,
      iataCode: '3243',
      name: 'Agent',
      effectiveFrom: '2020-01-23',
      bsp: {
        id: 7684,
        name: 'AgentBSP',
        isoCountryCode: 'ES',
        effectiveFrom: '2020-01-23'
      }
    };

    const result = service.summarize(agent);

    const expected: AgentSummary = {
      id: '234234',
      name: 'Agent',
      code: '3243',
      bsp: {
        id: 7684,
        name: 'AgentBSP',
        isoCountryCode: 'ES',
        effectiveFrom: '2020-01-23'
      }
    };

    expect(result).toEqual(expected);
  });

  it('should return null if no agent is provided', () => {
    const agent = null;

    expect(service.summarize(agent)).toEqual(null);
  });
});
