import { fakeAsync, TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { UserType } from '../models/user.model';

import { LoggedUserTypeResolver } from './logged-user-type-resolver.service';
import { AuthState } from '~app/auth/reducers/auth.reducer';

describe('LoggedUserTypeResolver', () => {
  const initialState = {
    auth: {
      user: {
        userType: UserType.IATA
      }
    } as AuthState
  };
  let service: LoggedUserTypeResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState })]
    });

    service = TestBed.inject(LoggedUserTypeResolver);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should resolve the user type', fakeAsync(() => {
    let actual: UserType = null;

    service.resolve().subscribe(userType => (actual = userType));

    expect(actual).toEqual(UserType.IATA);
  }));
});
