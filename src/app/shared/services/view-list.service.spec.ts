import { toUpper } from 'lodash';

import { EQ } from '../constants/operations';
import { RequestQueryFilter } from '../models/request-query-filter.model';

import { ViewListService } from './view-list.service';

describe('ViewListService', () => {
  it('should be created', () => {
    const service = new ViewListService(null);
    expect(service).toBeTruthy();
  });

  it('should format filters according to mapping configuration', () => {
    const service = new ViewListService({
      filterMappers: {
        first: {
          attribute: 'first-attribute',
          operation: EQ,
          valueMapper: toUpper
        },
        second: {
          attribute: 'second-attribute',
          operation: EQ,
          valueMapper: toUpper
        }
      }
    });
    const dataToFormat = {
      first: 'first-value',
      second: 'second-value'
    };
    const expected: RequestQueryFilter[] = [
      {
        attribute: 'first-attribute',
        operation: EQ,
        attributeValue: 'FIRST-VALUE'
      },
      {
        attribute: 'second-attribute',
        operation: EQ,
        attributeValue: 'SECOND-VALUE'
      }
    ];

    expect(service.formatRequestFilters(dataToFormat)).toEqual(expected);
  });

  it('should format filters according to mapping configuration', () => {
    const service = new ViewListService({
      filterMappers: {}
    });
    const dataToFormat = {
      missingFilterValue: 'first-value'
    };

    expect(() => service.formatRequestFilters(dataToFormat)).toThrowError(/Could not map filter 'missingFilterValue'./);
  });

  it('should return `[]` when no configuration is provided', () => {
    const service = new ViewListService(null);
    expect(service.formatRequestFilters({})).toEqual([]);
  });
});
