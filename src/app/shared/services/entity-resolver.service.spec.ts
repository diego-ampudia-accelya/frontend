import { Injectable } from '@angular/core';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { EntityResolverService } from './entity-resolver.service';
import { AirlineService } from '~app/master-data/services/airline.service';

@Injectable()
class TestEntityResolverService extends EntityResolverService<any> {
  constructor(apiService: AirlineService) {
    super(apiService);
  }
}

describe('EntityResolverService', () => {
  let airlineServiceSpy: SpyObject<AirlineService>;
  let service: EntityResolverService<any>;

  beforeEach(() => {
    airlineServiceSpy = createSpyObject(AirlineService);

    TestBed.configureTestingModule({
      providers: [TestEntityResolverService, { provide: AirlineService, useValue: airlineServiceSpy }]
    });

    service = TestBed.inject(TestEntityResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return airline when it exists', fakeAsync(() => {
    const airlineId = 123;
    const activeRoute: any = {
      paramMap: jasmine.createSpyObj('paramMap', ['get', 'has'])
    };
    activeRoute.paramMap.get.and.returnValue(airlineId);
    airlineServiceSpy.getOne.and.returnValue(of({ id: airlineId }));

    let actualAirport = null;
    service.resolve(activeRoute).subscribe(airport => (actualAirport = airport));

    expect(actualAirport.id).toBe(123);
    expect(airlineServiceSpy.getOne).toHaveBeenCalledWith(airlineId);
  }));
});
