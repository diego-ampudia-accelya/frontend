import { TestBed, waitForAsync } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';
import { Message, MessageService } from 'primeng/api';

import { NotificationService } from './notification.service';

describe('NotificationService', () => {
  let service: NotificationService;
  let messageServiceSpy: MessageService;
  let translationServiceStub: SpyObject<L10nTranslationService>;

  beforeEach(waitForAsync(() => {
    translationServiceStub = createSpyObject(L10nTranslationService);
    messageServiceSpy = createSpyObject(MessageService);
    translationServiceStub.translate.and.callFake(identity);

    TestBed.configureTestingModule({
      providers: [
        NotificationService,
        { provide: MessageService, useValue: messageServiceSpy },
        { provide: L10nTranslationService, useValue: translationServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    service = TestBed.inject(NotificationService);

    spyOn(service, 'addMessages');
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should translate notification messages', waitForAsync(() => {
    const message = 'test message';
    translationServiceStub.translate.and.returnValue('translated message');
    const expectedMessages = [jasmine.objectContaining<Message>({ detail: 'translated message' })];

    service.showSuccess(message);

    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  }));

  it('should support multiple messages', waitForAsync(() => {
    const messages = ['test message', 'test message 2'];
    const expectedMessages = messages.map(message => jasmine.objectContaining<Message>({ detail: message }));

    service.showSuccess(messages);

    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  }));

  it('should use notification options when they are provided', () => {
    const message = 'test message';
    const options: Message = { life: 4000, closable: false, sticky: true };
    const expectedMessages: Message[] = [{ detail: message, ...options, severity: 'success' }];

    service.showSuccess(message, options);

    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  });

  it('should use default notification options when none are provided', () => {
    const message = 'test message';
    const expectedMessages: Message[] = [
      {
        detail: message,
        life: 5000,
        closable: true,
        sticky: false,
        severity: 'info'
      }
    ];

    service.showInformation(message);

    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  });

  it('should call addMessages with correct params, when showError is called', () => {
    const message = 'error message';
    const options: Message = {
      detail: message,
      life: 5000,
      closable: true,
      sticky: true
    };

    const expectedMessages: Message[] = [{ ...options, severity: 'error' }];
    service.showError(message, options);
    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  });

  it('should call addMessages with correct params, when showWarning is called', () => {
    const message = 'Warning message';
    const options: Message = {
      detail: message,
      life: 5000,
      closable: true,
      sticky: false
    };

    const expectedMessages: Message[] = [{ ...options, severity: 'warn' }];
    service.showWarning(message, options);
    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  });

  it('should call addMessages with correct params, when showSuccess is called', () => {
    const message = 'Success message';
    const options: Message = {
      detail: message,
      life: 5000,
      closable: true,
      sticky: false
    };

    const expectedMessages: Message[] = [{ ...options, severity: 'success' }];
    service.showSuccess(message, options);
    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  });

  it('should call addMessages with correct params, when showInformation is called', () => {
    const message = 'Information message';
    const options: Message = {
      detail: message,
      life: 5000,
      closable: true,
      sticky: false
    };

    const expectedMessages: Message[] = [{ ...options, severity: 'info' }];
    service.showInformation(message, options);
    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  });

  it('should call addMessages with correct params, when showSystemError is called', () => {
    const message = 'Information message';
    const errorCode = 777;
    const options: Message = {
      detail: message,
      life: 5000,
      closable: true,
      sticky: true
    };

    const expectedMessages: Message[] = [{ ...options, severity: 'error', data: { errorCode } }];
    service.showSystemError(message, { errorCode }, options);
    expect(service.addMessages).toHaveBeenCalledWith(expectedMessages);
  });
});
