import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { AppConfiguration, AuthConfiguration, CacheConfiguration, SafsConfiguration } from '../models';
import { RetryConfiguration } from '../models/configuration/retry-configuration.model';
import { TrainingSessionConfiguration } from '../models/configuration/training_session_configuration.model';

import { AppConfigurationService } from './app-configuration.service';

describe('AppConfigurationService', () => {
  let service: AppConfigurationService;
  let fetchSpy: jasmine.Spy;
  beforeEach(() => TestBed.configureTestingModule({ providers: [AppConfigurationService] }));

  beforeEach(() => {
    service = TestBed.inject(AppConfigurationService);
    fetchSpy = spyOn(window, 'fetch').and.returnValue(Promise.resolve());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load configuration', () => {
    const config = { environmentLabel: 'test' };
    loadConfiguration(config);

    expect(service.environmentLabel).toBe('test');
    expect(fetchSpy).toHaveBeenCalledWith('/configuration.json');
  });

  describe('getAirlineLogoUrl', () => {
    const validIataCode = '123';
    const invalidIataCode = null;
    const validConfiguration: Partial<AppConfiguration> = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      image: { airline_uri: '/{iataCode}.png' }
    };

    it('should return airline logo url based on template in configuration', () => {
      loadConfiguration(validConfiguration);

      expect(service.getAirlineLogoUrl(validIataCode)).toBe('/123.png');
    });

    it('should return falsy when no `iataCode` is provided', () => {
      loadConfiguration(validConfiguration);

      expect(service.getAirlineLogoUrl(invalidIataCode)).toBeFalsy();
    });

    it('should return predefined logo for training mode', () => {
      loadConfiguration({ training: true });

      expect(service.getAirlineLogoUrl(validIataCode)).toBe('../../assets/images/dashboard/dummy-airline-logo.gif');
    });
  });

  describe('isTrainingSite', () => {
    it('should return `false` when no configuration is provided', () => {
      expect(service.isTrainingSite).toBe(false);
    });

    it('should return `true` when `training` is `true` in the configuration', () => {
      loadConfiguration({ training: true });

      expect(service.isTrainingSite).toBe(true);
    });

    it('should return `false` when `training` is `false` in the configuration', () => {
      loadConfiguration({ training: false });

      expect(service.isTrainingSite).toBe(false);
    });
  });

  describe('environmentLabel', () => {
    it('should be empty string by default', () => {
      expect(service.environmentLabel).toBe('');
    });

    it('should return the value provided in configuration', () => {
      loadConfiguration({ environmentLabel: 'test' });

      expect(service.environmentLabel).toBe('test');
    });
  });

  describe('disabledPermissions', () => {
    it('should be empty array by default', () => {
      expect(service.disabledPermissions).toEqual([]);
    });

    it('should return the value provided in configuration', () => {
      loadConfiguration({ disabledPermissions: ['1234'] });

      expect(service.disabledPermissions).toEqual(['1234']);
    });

    it('should return empty array when no value is provided in the configuration', () => {
      loadConfiguration({});

      expect(service.disabledPermissions).toEqual([]);
    });
  });

  describe('trainingSessionConfiguration', () => {
    it('should return default value when configuration is missing', () => {
      loadConfiguration({});

      expect(service.trainingSessionConfiguration).toEqual({
        duration: 120,
        expiresIn: 5
      });
    });

    it('should return value when provided in configuration', () => {
      const trainingSession: TrainingSessionConfiguration = {
        duration: 10,
        expiresIn: 1
      };
      loadConfiguration({ trainingSession });

      expect(service.trainingSessionConfiguration).toEqual(trainingSession);
    });
  });

  describe('auth', () => {
    it('should return `undefined` by default', () => {
      loadConfiguration({});

      expect(service.authConfiguration).not.toBeDefined();
    });

    it('should return configured value', () => {
      const auth = {} as AuthConfiguration;
      loadConfiguration({ auth });

      expect(service.authConfiguration).toBe(auth);
    });
  });

  describe('baseApiPath', () => {
    it('should return empty string when no value is provided', () => {
      loadConfiguration({});

      expect(service.baseApiPath).toBe('');
    });

    it('should return proper value when configuration exists', () => {
      loadConfiguration({
        api: { host: 'localhost:3000/api', protocol: 'http' }
      });

      expect(service.baseApiPath).toBe('http://localhost:3000/api');
    });
  });

  describe('baseUploadPath', () => {
    it('should return empty string when no value is provided', () => {
      loadConfiguration({});

      expect(service.baseUploadPath).toBe('');
    });

    it('should return proper value when configuration exists', () => {
      loadConfiguration({
        api: { host: '', uploadPath: 'localhost:3000/api', protocol: 'http' }
      });

      expect(service.baseUploadPath).toBe('http://localhost:3000/api');
    });

    it('should should use `host` when `uploadPath` is not provided', () => {
      loadConfiguration({
        api: { host: 'localhost:3000/api', protocol: 'http' }
      });

      expect(service.baseUploadPath).toBe('http://localhost:3000/api');
    });
  });

  describe('classicBspLinkUrl', () => {
    it('should return empty string when no value is configured', () => {
      loadConfiguration({});

      expect(service.classicBspLinkUrl).toBe('');
    });

    it('should return configured value', () => {
      loadConfiguration({ classicBspLinkUrl: 'test' });

      expect(service.classicBspLinkUrl).toBe('test');
    });
  });

  describe('cache', () => {
    it('should return default value when none is configured', () => {
      loadConfiguration({});

      expect(service.cache).toEqual({
        maxAge: 600000,
        maxCacheCount: 500
      });
    });

    it('should return configured value', () => {
      const cache: CacheConfiguration = { maxAge: 10, maxCacheCount: 1 };
      loadConfiguration({ cache });

      expect(service.cache).toEqual(cache);
    });
  });

  describe('disableLogoutRedirect', () => {
    it('should return `false` by default', () => {
      loadConfiguration({});

      expect(service.disableLogoutRedirect).toBe(false);
    });

    it('should return configured value', () => {
      loadConfiguration({ disableLogoutRedirect: true });

      expect(service.disableLogoutRedirect).toBe(true);
    });
  });

  describe('trainingBspCodes', () => {
    it('should return empty array by default', () => {
      loadConfiguration({});

      expect(service.trainingBspCodes).toEqual([]);
    });

    it('should return configured value', () => {
      const trainingBspCodes = ['A6'];
      loadConfiguration({ trainingBspCodes });

      expect(service.trainingBspCodes).toEqual(trainingBspCodes);
    });
  });

  describe('defaultLoggedUserTemplate', () => {
    it('should return empty string by default', () => {
      loadConfiguration({});

      expect(service.defaultLoggedUserTemplate).toBe('');
    });

    it('should return configured value', () => {
      loadConfiguration({
        subUserManagement: { defaultLoggedUserTemplate: 'test-template' }
      });

      expect(service.defaultLoggedUserTemplate).toBe('test-template');
    });
  });

  describe('retry', () => {
    it('should return default value by default', () => {
      const defaultConf: RetryConfiguration = { delay: 500, maxAttempts: 3, rules: [] };
      expect(service.retry).toEqual(defaultConf);

      loadConfiguration({});

      expect(service.retry).toEqual(defaultConf);
    });

    it('should return configured value', () => {
      const retry: RetryConfiguration = { delay: 1000, maxAttempts: 4, rules: [] };
      loadConfiguration({ retry });

      expect(service.retry).toEqual(retry);
    });

    it('should apply defaults when optional settings are missing', () => {
      const retry: RetryConfiguration = { delay: 200, maxAttempts: undefined, rules: undefined };
      const expected: RetryConfiguration = {
        delay: 200,
        maxAttempts: 3,
        rules: []
      };
      loadConfiguration({ retry });

      expect(service.retry).toEqual(expected);
    });
  });

  describe('privacyLink', () => {
    it('should return default when no value is configured', () => {
      loadConfiguration({});

      expect(service.privacyLink).toBe('https://www.iata.org/en/privacy/');
    });

    it('should return configured value', () => {
      const privacyLink = 'https://example.com';
      loadConfiguration({ privacyLink });

      expect(service.privacyLink).toBe(privacyLink);
    });
  });

  describe('safs', () => {
    it('should return default configuration when no value is configured', () => {
      loadConfiguration({});

      expect(service.safs.delayAfterArchive).toEqual(500);
      expect(service.safs.maxSupportedItems).toEqual(10000);
    });

    it('should return configured value', () => {
      const safs: SafsConfiguration = {
        delayAfterArchive: 700,
        maxSupportedItems: 200
      };

      loadConfiguration({ safs });

      expect(service.safs).toEqual(safs);
    });

    it('should apply default configuration for properties that are not configured', () => {
      const safs: any = {
        delayAfterArchive: 800
      };

      loadConfiguration({ safs });

      expect(service.safs.delayAfterArchive).toEqual(800);
      expect(service.safs.maxSupportedItems).toEqual(10000);
    });
  });

  function loadConfiguration(config: Partial<AppConfiguration>): void {
    const responseStub = { json: jasmine.createSpy().and.returnValue(config) };
    fetchSpy.and.returnValue(Promise.resolve(responseStub));

    fakeAsync(() => {
      service.load().toPromise();
      tick();
    })();
  }
});
