import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { RequestMethod, SortOrder } from '../enums';
import { RequestQuery } from '../models/request-query.model';

import { ApiService } from './api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

const api = 'localhost';
const baseApiPath = 'base-api-path';
const apiServiceName = 'city';

@Injectable()
class TestApiService extends ApiService<any> {
  constructor(http: HttpClient) {
    super(http, apiServiceName);
  }
}

describe('ApiService', () => {
  let service: TestApiService;
  let httpClientMock: SpyObject<HttpClient>;

  beforeEach(() => {
    httpClientMock = createSpyObject(HttpClient);
    httpClientMock.get.and.returnValue(of());
    httpClientMock.put.and.returnValue(of());
    httpClientMock.post.and.returnValue(of());
    httpClientMock.delete.and.returnValue(of());

    TestBed.configureTestingModule({
      providers: [
        TestApiService,
        {
          provide: HttpClient,
          useValue: httpClientMock
        }
      ]
    });

    service = TestBed.inject(TestApiService);
    spyOnProperty(appConfiguration, 'baseApiPath', 'get').and.returnValue(baseApiPath);
    spyOnProperty(appConfiguration, 'api', 'get').and.returnValue(api);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get and set basePath correctly', () => {
    const baseUrlPath = 'base/url';

    service.baseUrl = baseUrlPath;
    expect(service.baseUrl.includes(baseUrlPath)).toBe(true);
  });

  it('should get correct baseUrl, when baseUrl is empty and appConfiguration.api is defined', () => {
    const correctPluralPath = 'cities';
    const expectedPath = `${baseApiPath}/${apiServiceName}-management/${correctPluralPath}`;

    expect(service.baseUrl).toBe(expectedPath);
  });

  it('should returns http observable with concatenated base path and passed id, when getOne is called', () => {
    service.getOne(6).subscribe();

    expect(httpClientMock.get).toHaveBeenCalledWith('base-api-path/city-management/cities/6');
  });

  it('should return http observable with the corresponding url, when getAll is called with GET request method.', () => {
    const requestQuery = new RequestQuery(0, 20, 'test', SortOrder.Asc);
    service.getAll(requestQuery);

    expect(httpClientMock.get).toHaveBeenCalledWith(
      'base-api-path/city-management/cities?page=0&size=20&sort=test,ASC'
    );
  });

  it('should return http observable with the corresponding url and body params, when getAll is called with POST request method.', () => {
    const requestQuery = new RequestQuery(0, 20, null, SortOrder.Asc);
    service.getAll(requestQuery, '/additional-path', RequestMethod.Post);

    expect(httpClientMock.post).toHaveBeenCalledWith(
      'base-api-path/city-management/cities/additional-path',
      jasmine.objectContaining({ paginateBy: { page: 0, size: 20 } })
    );
  });

  it('should call getAll, when getAllItems is called', () => {
    spyOn(service, 'getAll').and.returnValue(of());
    service.getAllItems();

    expect(service.getAll).toHaveBeenCalled();
  });

  it('should return http observable with the corresponding url, when getAllItems is called.', () => {
    service.getAllItems();

    expect(httpClientMock.get).toHaveBeenCalledWith('base-api-path/city-management/cities?page=0&size=99999');
  });

  it('should call getAllItems, when getDropDownOptions is called', () => {
    spyOn(service, 'getAllItems').and.returnValue(of());
    service.getDropDownOptions();

    expect(service.getAllItems).toHaveBeenCalled();
  });

  it('should return http observable with the corresponding url, when getDropDownOptions is called.', () => {
    service.getDropDownOptions();

    expect(httpClientMock.get).toHaveBeenCalledWith('base-api-path/city-management/cities?page=0&size=99999');
  });

  it('should return http observable with defined path and POST request, when getDropDownOptions is called.', () => {
    service.getDropDownOptions('/customPath', RequestMethod.Post);

    expect(httpClientMock.post).toHaveBeenCalledWith(
      'base-api-path/city-management/cities/customPath',
      jasmine.objectContaining({ paginateBy: { page: 0, size: 99999 } })
    );
  });

  it('should returns corresponding observable with GET method, when download is called', () => {
    service.download({ size: 10 }, '/urlPath', RequestMethod.Get);

    expect(httpClientMock.get).toHaveBeenCalledWith('base-api-path/city-management/cities/urlPath', {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  });

  it('should returns corresponding observable with POST method, when download is called', () => {
    service.download(null, '/urlPath', RequestMethod.Post, { size: 10 });

    expect(httpClientMock.post).toHaveBeenCalledWith(
      'base-api-path/city-management/cities/urlPath',
      { size: 10 },
      {
        responseType: 'arraybuffer',
        observe: 'response'
      }
    );
  });

  it('should build download POST request', () => {
    service.download('/requestPayload');
    httpClientMock.post.and.returnValue({
      headers: { get: () => 'filename="test.zip"', body: { id: 54 } }
    });

    expect(httpClientMock.post).toHaveBeenCalledWith('base-api-path/city-management/cities', '/requestPayload', {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  });

  it('should downloadRequest returns blob and fileName, when download is called', () => {
    httpClientMock.post.and.returnValue(of({ headers: { get: () => 'filename="test.zip"', body: { id: 54 } } }));
    service.download('/requestPayload').subscribe(response => {
      expect(response).toEqual({
        blob: jasmine.objectContaining({ size: 9, type: 'application/zip' }),
        fileName: 'test.zip'
      });
    });
  });

  it('should returns corresponding observable with DELETE method, when delete is called.', () => {
    service.delete(6);

    expect(httpClientMock.delete).toHaveBeenCalledWith('base-api-path/city-management/cities/6', {});
  });

  it('should save data, when save is called and id, postParams are not passed', () => {
    service.save({ name: 'testName' });

    expect(httpClientMock.post).toHaveBeenCalledWith('base-api-path/city-management/cities', { name: 'testName' });
  });

  it('should build url with passed post params, and remove postParams from body request', () => {
    service.save({ name: 'testName', postPrams: '5' });

    expect(httpClientMock.post).toHaveBeenCalledWith('base-api-path/city-management/cities/5', { name: 'testName' });
  });

  it('should send PUT request, when save is called and data contains id', () => {
    service.save({ id: 6, postPrams: '6' });

    expect(httpClientMock.put).toHaveBeenCalledWith('base-api-path/city-management/cities/6/6', { id: 6 });
  });

  it('should return what is passed', () => {
    const expectedResult = { strange: 'filter' };

    const returnedFilter = service.formatFilter(expectedResult);
    expect(returnedFilter).toBe(expectedResult);
  });

  it('should construct appropriate request, when downloadFile is called', () => {
    service.downloadFile('/resourceUrl');

    expect(httpClientMock.get).toHaveBeenCalledWith('base-api-path/resourceUrl', {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  });

  it('should http get response be mapped to returns blob and fileName, when downloadFile is called', () => {
    httpClientMock.get.and.returnValue(of({ headers: { get: () => 'filename="file.zip"', body: { id: 54 } } }));
    service.downloadFile('/url').subscribe(response => {
      expect(response).toEqual({
        blob: jasmine.objectContaining({ size: 9, type: 'filename="file.zip"' }),
        fileName: 'file.zip'
      });
    });
  });
});
