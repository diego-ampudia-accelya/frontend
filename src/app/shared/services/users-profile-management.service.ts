import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { omit } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { EQ } from '../constants/operations';
import { RequestQueryFilter } from '../models/request-query-filter.model';
import { UserAccount, UserAccountBsp } from '../models/user-account.model';
import { UserLanguage } from '../models/user-language.model';
import { Agent, Airline } from '~app/master-data/models';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { RequestMethod } from '~app/shared/enums/request-method.enum';
import { ArrayHelper } from '~app/shared/helpers/arrayHelper';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { User, UserType } from '~app/shared/models/user.model';
import { ApiService } from '~app/shared/services/api.service';
import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class UsersProfileManagementService extends ApiService<User> {
  public userType: string;

  constructor(http: HttpClient) {
    super(http, 'user');
  }

  public getAll(requestQuery: RequestQuery): Observable<PagedData<User>> {
    requestQuery.reduceFilterBy = true;

    return super.getAll(requestQuery, '', RequestMethod.Get);
  }

  public getCurrentUserData(): Observable<User> {
    const url = `${appConfiguration.baseApiPath}/user-mgmt-integration/security-integration/users/me`;

    return this.http.get<User>(url).pipe(
      map(user => {
        this.normalizeAirlineData(user);
        this.normalizeUserTemplate(user);

        return user;
      })
    );
  }

  public setUserLanguage(userId: number, language: string): Observable<UserLanguage> {
    const url = `${appConfiguration.baseApiPath}/user-mgmt-integration/users`;

    return this.http.post<UserLanguage>(url, { id: userId, language });
  }

  public getUserAirlines(): Observable<Airline[]> {
    const url = `${appConfiguration.baseApiPath}/user-mgmt-integration/airlines`;

    return this.http.get<Airline[]>(url);
  }

  public getUserAgents(): Observable<Agent[]> {
    const url = `${appConfiguration.baseApiPath}/user-mgmt-integration/agents`;

    return this.http.get<Agent[]>(url);
  }

  public getUserDetailsByInternalId(userInternalId: number): Observable<any> {
    const url = `${appConfiguration.baseApiPath}/user-mgmt-integration/security-integration/users/${userInternalId}`;

    return this.http.get<any>(url);
  }

  public getUserAccounts(bspId?: number): Observable<UserAccount[]> {
    const url = `${appConfiguration.baseApiPath}/user-profile-management/users/me/accounts`;
    const params = { ...(bspId != null && { bspId: '' + bspId }) };

    return this.http.get<UserAccount[]>(url, { params });
  }

  public getUserAccountBsps(): Observable<UserAccountBsp[]> {
    const url = `${appConfiguration.baseApiPath}/user-profile-management/users/me/accounts/bsps`;

    return this.http.get<UserAccountBsp[]>(url);
  }

  public getEmailDropDownOptions(userType: UserType, iataCode?: string): Observable<DropdownOption[]> {
    const requestQuery = new RequestQuery(PAGINATION.FIRST_PAGE, PAGINATION.MAX_PAGE_SIZE);
    requestQuery.sortOrder = null;
    requestQuery.reduceFilterBy = true;
    requestQuery.filterBy = [
      {
        attribute: 'userType',
        attributeValue: userType,
        operation: EQ
      }
    ];
    if (iataCode) {
      (requestQuery.filterBy as RequestQueryFilter[]).push({
        attribute: 'iataCode',
        attributeValue: iataCode,
        operation: EQ
      });
    }

    return this.getAll(requestQuery).pipe(
      map((pagedData: PagedData<User>) => pagedData.records),
      map(items => ArrayHelper.toCustomValueAndLabel(items, 'id', 'email'))
    );
  }

  public download(requestQueryBody, downloadFormat: string) {
    return super.download(requestQueryBody, `/download?type=${downloadFormat}`, RequestMethod.Post);
  }

  public getCurrentUserIataCodeDropdownOption(userType: UserType): Observable<DropdownOption[]> {
    return this.getCurrentUserData().pipe(
      map((user: User) => {
        if (user.userType === UserType.AGENT) {
          const { id, iataCode, name } = user.agent;

          return [
            {
              value: id,
              label: `${iataCode} / ${name}`
            }
          ];
        } else if (user.userType === UserType.AIRLINE) {
          const { id, iataCode, globalName: name } = user.globalAirline;

          return [
            {
              value: id,
              label: `${iataCode} / ${name}`
            }
          ];
        } else if (user.userType === UserType.GDS) {
          const { id, gdsCode, name } = user.gds;

          return [
            {
              value: id,
              label: `${gdsCode} / ${name}`
            }
          ];
        }
      })
    );
  }

  private normalizeAirlineData(user: User) {
    if (user.userType === UserType.AIRLINE && user.globalAirline) {
      const { airlines } = user.globalAirline;
      const globalAirline = omit(user.globalAirline, 'airlines');
      user.globalAirline.airlines = airlines.map(airline => ({ ...airline, globalAirline })) as Airline[];
    }
  }

  private normalizeUserTemplate(user: User) {
    if (user.template === undefined) {
      user.template = appConfiguration.defaultLoggedUserTemplate;
    }
  }
}
