import { Component, Input } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { times } from 'lodash';

import { DocumentPdfExporter } from './document-pdf-exporter.service';

@Component({
  selector: 'bspl-multipage-content',
  styles: [
    `
      section {
        height: 600px;
        width: 600px;
        background-color: black;
      }
    `
  ],
  template: ` <section *ngFor="let element of generateElements()" class="pdf-page-break-inside-avoid"></section> `
})
class MultipageContentComponent {
  @Input() atomicElementCount = 5;

  public generateElements() {
    return times(this.atomicElementCount);
  }
}

describe('DocumentPdfExporter', () => {
  let service: DocumentPdfExporter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentPdfExporter],
      declarations: [MultipageContentComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.inject(DocumentPdfExporter);
    spyOn(service as any, 'createJsPDF').and.returnValue({
      addFileToVFS: jasmine.createSpy().and.callThrough(),
      addFont: jasmine.createSpy().and.callThrough(),
      setFont: jasmine.createSpy().and.callThrough(),
      html: jasmine.createSpy().and.callFake((element, config) => {
        config.html2canvas.onclone(document);
        document.documentElement.appendChild(element);
      })
    });
  });

  afterEach(() => {
    document.querySelector('.pdf-export')?.remove();
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  // TODO REVIEW: this test is throwing error `Expected 4195 to be 5360` sometimes
  xit('should create 5 pages when there are 5 atomic elements and only one can fit on a page', async () => {
    const fixture = TestBed.createComponent(MultipageContentComponent);

    fixture.detectChanges();

    service.attach(fixture.elementRef);

    await service.export();

    const pdfElement: HTMLElement = document.querySelector('.pdf-export');

    expect(pdfElement.clientHeight).toBe(5360);
  });

  it('should throw when no element to export is attached', async () => {
    try {
      await service.export();
      fail();
    } catch (err) {
      expect(err).toBeDefined();
    }
  });

  it('should throw after element to export is detached', async () => {
    const fixture = TestBed.createComponent(MultipageContentComponent);
    fixture.detectChanges();

    service.attach(fixture.elementRef);
    service.detach();
    try {
      await service.export();
      fail();
    } catch (err) {
      expect(err).toBeDefined();
    }
  });
});
