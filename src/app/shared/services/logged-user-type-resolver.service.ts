import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { getUserType } from '~app/auth/selectors/auth.selectors';
import { AppState } from '~app/reducers';
import { UserType } from '~app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoggedUserTypeResolver implements Resolve<UserType> {
  constructor(private store: Store<AppState>) {}

  resolve(): Observable<UserType> {
    return this.store.pipe(select(getUserType), first());
  }
}
