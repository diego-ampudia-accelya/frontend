import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { mockProvider, SpyObject } from '@ngneat/spectator';
import { of } from 'rxjs';

import { LocalBspTime } from '../models/local-bsp-time.model';

import { AppConfigurationService } from './app-configuration.service';
import { LocalBspTimeService } from './local-bsp-time.service';

describe('Local Bsp Time Service', () => {
  let service: LocalBspTimeService;
  let httpMock: SpyObject<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LocalBspTimeService,
        mockProvider(HttpClient),
        mockProvider(AppConfigurationService, {
          baseApiPath: 'localhost:4000'
        })
      ]
    });

    service = TestBed.inject(LocalBspTimeService);
    httpMock = TestBed.inject<any>(HttpClient);
    httpMock.get.and.returnValue(of({ id: 1234, localTime: new Date('2020-02-02') }));
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should return local bsp time', fakeAsync(() => {
    const expectedTime: LocalBspTime = {
      id: 1234,
      localTime: new Date('2020-02-02')
    };

    service.getLocalBspTime(5).subscribe(localBspTime => {
      expect(localBspTime).toEqual(expectedTime);
    });
  }));
});
