import { HttpClient, HttpResponse } from '@angular/common/http';
import { omit } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { RequestMethod } from '~app/shared/enums/request-method.enum';
import { CrudMethods } from '~app/shared/models/crud-methods.model';
import { DropdownOption } from '~app/shared/models/dropdown-option.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { ArrayHelper } from '../helpers/arrayHelper';
import { getPluralPath } from '../utils/api-utils';

const ALL_ITEMS_QUERY = new RequestQuery(PAGINATION.FIRST_PAGE, PAGINATION.MAX_PAGE_SIZE);

const toDropdownOptions = ArrayHelper.toDropdownOptions;

// ToDo: Refactor URL concatenation, code repetition, replace implementation of methods `getAll`, `getOne`, getAllItems` to call single `get` or `find` method.
// Add Doc blocks.

export abstract class ApiService<T> implements CrudMethods<T> {
  public set baseUrl(value: string) {
    this._baseUrl = value;
  }

  public get baseUrl(): string {
    if (!this._baseUrl && appConfiguration.baseApiPath) {
      const serviceName = this.serviceName;
      const pathPlural = getPluralPath(serviceName);
      const pathSingular = this.getSingularPath(serviceName);

      this._baseUrl = `${appConfiguration.baseApiPath}/${pathSingular}-management/${pathPlural}`;
    }

    return this._baseUrl;
  }

  private _baseUrl = '';

  /**
   * Creates instance of ApiService.
   *
   * @param protected http - HttpClient instance
   * @param protected serviceName - Name of the service endpoint in singular form
   */
  constructor(protected http: HttpClient, protected serviceName: string) {}

  public getOne(id: number): Observable<T> {
    return this.http.get<T>(`${this.baseUrl}/${id}`);
  }

  public getAll(requestQuery: RequestQuery, path = '', requestMethod = RequestMethod.Get): Observable<PagedData<T>> {
    let url = this.baseUrl + path;

    if (requestMethod === RequestMethod.Post) {
      return this.http.post<PagedData<T>>(url, requestQuery.getBody());
    }

    url += requestQuery.getQueryString();

    return this.http.get<PagedData<T>>(url);
  }

  public getAllItems(path = '', requestMethod = RequestMethod.Get): Observable<T[]> {
    return this.getAll(ALL_ITEMS_QUERY, path, requestMethod).pipe(map(result => result.records));
  }

  public getDropDownOptions(path = '', requestMethod = RequestMethod.Get): Observable<DropdownOption[]> {
    return this.getAllItems(path, requestMethod).pipe(map(toDropdownOptions));
  }

  public download(
    requestPayload: any,
    urlPath = '',
    requestMethod = RequestMethod.Post,
    requestBody?: any
  ): Observable<any> {
    const url = this.baseUrl + urlPath;

    let downloadRequest$: Observable<any>;

    const requestOptions = {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    };

    requestPayload = requestBody ? { ...requestBody } : requestPayload;

    if (requestMethod === RequestMethod.Get) {
      downloadRequest$ = this.http.get<HttpResponse<ArrayBuffer>>(url, requestOptions);
    } else {
      downloadRequest$ = this.http.post<HttpResponse<ArrayBuffer>>(url, requestPayload, requestOptions);
    }

    return downloadRequest$.pipe(
      map(response => {
        const contentDispositionHeader: string = response.headers.get('Content-Disposition');
        const fileName: string = contentDispositionHeader.match(/filename="(.*)"/)[1];
        const blob = new Blob([response.body], { type: 'application/zip' });

        return { blob, fileName };
      })
    );
  }

  /**
   * Removes existing entity from the database.
   *
   * @param id - The entity to remove
   */
  public delete(id: number): Observable<any> {
    const url = `${this.baseUrl}/${id}`;

    return this.http.delete(url, {});
  }

  /**
   * Saves an entry in the database - can create new, update existing entries.
   *
   * @param data - Entry info to be updated or created in the database.
   * @returns Observable<T[]>
   */
  public save(data?: any): Observable<any> {
    let method = 'post';
    let url = this.baseUrl;

    if (data.postPrams) {
      url += '/' + data.postPrams;
    }

    if (data.id) {
      method = 'put';
      url += '/' + data.id;
    }

    return this.http[method](url, omit(data, 'postPrams'));
  }

  // TODO: Refactor. Maybe this shouldn't be handled in the service. Untie coupling with types.
  public formatFilter(filter: any): RequestQueryFilter[] | any {
    return filter;
  }

  public downloadFile(resourceUrl: string): Observable<any> {
    const requestOptions = {
      responseType: 'arraybuffer' as 'json',
      observe: 'response' as 'body'
    };

    return this.http.get<HttpResponse<any>>(`${appConfiguration.baseApiPath}${resourceUrl}`, requestOptions).pipe(
      map(response => {
        const contentDispositionHeader: string = response.headers.get('Content-Disposition');
        const fileName: string = contentDispositionHeader.match(/filename="(.*)"/)[1];
        const blob = new Blob([response.body], { type: response.headers.get('Content-Type') });

        return { blob, fileName };
      })
    );
  }

  private getSingularPath(pathSingular: string): string {
    if (pathSingular === 'user' || pathSingular === 'group') {
      return 'user-profile';
    } else if (pathSingular === '/currency-assignments') {
      getPluralPath('bsps/currency-assignment');

      return 'bsp';
    } else {
      return pathSingular;
    }
  }
}
