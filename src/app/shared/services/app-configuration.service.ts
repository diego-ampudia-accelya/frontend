import { Injectable } from '@angular/core';
import { defaults, merge } from 'lodash';
import moment from 'moment-mini';
import { from, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { CacheConfiguration, HelpConfiguration, SafsConfiguration } from '../models';
import { LanguagesConfiguration } from '../models/configuration/languages-configuration.model';
import { RetryConfiguration } from '../models/configuration/retry-configuration.model';
import { ApiConfiguration } from '~app/shared/models/configuration/api-configuration.model';
import { AppConfiguration } from '~app/shared/models/configuration/app-configuration.model';
import { AuthConfiguration } from '~app/shared/models/configuration/auth-configuration.model';
import { TrainingSessionConfiguration } from '~app/shared/models/configuration/training_session_configuration.model';

@Injectable()
export class AppConfigurationService {
  private configurationData: AppConfiguration;
  private imgPath = '../../assets/images/dashboard/';
  private configurationPath = '/configuration.json';

  load(): Observable<AppConfiguration> {
    this.configurationData = null;
    const data$ = from(fetch(this.configurationPath).then(response => response.json()));

    return data$.pipe(tap(data => (this.configurationData = data)));
  }

  public getAirlineLogoUrl(iataCode: string): string {
    if (this.isTrainingSite) {
      return this.imgPath + 'dummy-airline-logo.gif';
    }

    return iataCode && `${this.configurationData.image.airline_uri}`.replace('{iataCode}', iataCode);
  }

  get isTrainingSite(): boolean {
    return this.configurationData?.training ?? false;
  }

  get enableL10nFallback(): boolean {
    return this.configurationData?.enableL10nFallback ?? true;
  }

  get environmentLabel(): string {
    return (this.configurationData && this.configurationData.environmentLabel) || '';
  }

  get disabledPermissions(): string[] {
    return (this.configurationData && this.configurationData.disabledPermissions) || [];
  }

  get authConfiguration(): AuthConfiguration {
    return this.configurationData && this.configurationData.auth;
  }

  get api(): ApiConfiguration {
    return this.configurationData && this.configurationData.api;
  }

  get trainingSessionConfiguration(): TrainingSessionConfiguration {
    return (
      (this.configurationData && this.configurationData.trainingSession) || {
        duration: 120,
        expiresIn: 5
      }
    );
  }

  get baseApiPath(): string {
    if (this.api) {
      const { protocol, host } = this.api;

      return `${protocol}://${host}`;
    }

    return '';
  }

  get baseUploadPath(): string {
    if (this.api) {
      const { protocol, host, uploadPath } = this.api;

      return `${protocol}://${uploadPath || host}`;
    }

    return '';
  }

  get classicBspLinkUrl(): string {
    return (this.configurationData && this.configurationData.classicBspLinkUrl) || '';
  }

  get cache(): CacheConfiguration {
    return (
      (this.configurationData && this.configurationData.cache) || {
        maxAge: moment.duration(10, 'minutes').asMilliseconds(),
        maxCacheCount: 500
      }
    );
  }

  get disableLogoutRedirect(): boolean {
    return Boolean(this.configurationData && this.configurationData.disableLogoutRedirect);
  }

  get useNewPDFStyle(): boolean {
    return Boolean(this.configurationData && this.configurationData.useNewPDFStyle);
  }

  get trainingBspCodes(): string[] {
    return (this.configurationData && this.configurationData.trainingBspCodes) || [];
  }

  get defaultLoggedUserTemplate(): string {
    return (
      (this.configurationData &&
        this.configurationData.subUserManagement &&
        this.configurationData.subUserManagement.defaultLoggedUserTemplate) ||
      ''
    );
  }

  get retry(): RetryConfiguration {
    const defaultConfig: RetryConfiguration = {
      delay: 500,
      maxAttempts: 3,
      rules: []
    };

    return defaults({}, this.configurationData?.retry, defaultConfig);
  }

  get privacyLink(): string {
    return this.configurationData?.privacyLink ?? 'https://www.iata.org/en/privacy/';
  }

  get customerServicesLink(): string {
    return this.configurationData?.customerServicesLink ?? 'https://www.iata.org/cs/';
  }

  get safs(): SafsConfiguration {
    const defaultConfig: SafsConfiguration = {
      maxSupportedItems: 10000,
      delayAfterArchive: 500
    };

    return merge(defaultConfig, this.configurationData?.safs);
  }

  get help(): HelpConfiguration {
    const defaultConfig: HelpConfiguration = {
      securePath: '/online-help',
      publicPath: '/online-help'
    };

    return merge(defaultConfig, this.configurationData?.help);
  }

  get languages(): LanguagesConfiguration[] {
    return (this.configurationData && this.configurationData.languages) || [];
  }
}

export const appConfiguration = new AppConfigurationService();
