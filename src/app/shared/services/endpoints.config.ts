/* eslint-disable @typescript-eslint/naming-convention */
export type EndpointConfig = string | DynamicEndpointPath;

//* A function that from an Array of tokens, builds a path
type DynamicEndpointPath = (tokens: { [key: string]: string }) => string;

export enum DynamicEndpointClass {
  AdmAcmService,
  AdmRequestService,
  AcmRequestService,
  RefundService,
  AdmAcmPolicyService
}

export const ENDPOINTS_CONFIG: { [className in DynamicEndpointClass]: EndpointConfig } = {
  [DynamicEndpointClass.AdmAcmService]: tokens => `acdm-management/${tokens.admAcmType}s`,
  [DynamicEndpointClass.AdmRequestService]: 'acdm-management/adm-requests',
  [DynamicEndpointClass.AcmRequestService]: () => `acdm-management/acm-requests`,
  [DynamicEndpointClass.RefundService]: tokens => `refund-management/${tokens.refundType}s`,
  [DynamicEndpointClass.AdmAcmPolicyService]: tokens => `acdm-management/${tokens.admAcmType}-policies`
};
