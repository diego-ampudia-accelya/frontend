import { appConfiguration } from './app-configuration.service';
import { DynamicEndpointClass, ENDPOINTS_CONFIG } from './endpoints.config';

/**
 * Builds endpoint path for a Service using config and tokens
 *
 * @param className index used to find config
 * @param [tokens] tokens used to fill the path. Optional because the path from config can be a static string
 * @returns endpoint path to be set in ApiService
 */
export function buildEndpointPath(className: DynamicEndpointClass, tokens?: { [key: string]: string }): string {
  const endpointConfig = ENDPOINTS_CONFIG[className];
  if (!endpointConfig) {
    throw new Error('You need to define a DynamicEndpointPath function in endpoints.config for the service');
  }

  let basePath: string;
  if (typeof endpointConfig === 'string') {
    basePath = `${appConfiguration.baseApiPath}/${endpointConfig}`;
  } else {
    basePath = `${appConfiguration.baseApiPath}/${endpointConfig(tokens)}`;
  }

  return basePath;
}
