import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import moment, { Duration } from 'moment-mini';
import { Observable, of } from 'rxjs';
import { catchError, map, shareReplay, tap } from 'rxjs/operators';

import { LocalBspTime } from '../models/local-bsp-time.model';
import { AppConfigurationService } from '~app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class LocalBspTimeService {
  private offset = moment.duration();
  constructor(private http: HttpClient, private appConfiguration: AppConfigurationService) {}

  public getLocalBspTime(bspId: number): Observable<LocalBspTime> {
    const baseUrl = `${this.appConfiguration.baseApiPath}/acdm-management/bsps/${bspId}/time`;

    return this.http
      .get<LocalBspTime>(baseUrl)
      .pipe(map(time => ({ ...time, localTime: moment(time.localTime).toDate() })));
  }

  public getDate(date?: string | Date): Date {
    return moment(date).add(this.offset).toDate();
  }

  public setOffset(bspId: number): Observable<LocalBspTime> {
    return this.getLocalBspTime(bspId).pipe(
      tap(({ localTime }) => {
        this.offset = this.calcOffset(localTime);
      }),
      catchError(() => {
        // eslint-disable-next-line no-console
        console.warn('[WARN] Could not get current BSP time');

        return of(null);
      }),
      shareReplay(1)
    );
  }

  private calcOffset(date: Date): Duration {
    const bspDate = moment(date);
    const currentDate = moment();

    return moment.duration(bspDate.diff(currentDate));
  }
}
