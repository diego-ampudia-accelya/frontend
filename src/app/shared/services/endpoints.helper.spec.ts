import { appConfiguration } from './app-configuration.service';
import { DynamicEndpointClass } from './endpoints.config';
import { buildEndpointPath } from './endpoints.helper';

describe('endpoints.helper', () => {
  beforeEach(() => {
    spyOnProperty(appConfiguration, 'baseApiPath', 'get').and.returnValue('');
  });

  it('buildEndpointPath() should throw an error if there is a missing endpoint config', () => {
    const endpointClass: DynamicEndpointClass = 9999;
    const expected = new Error('You need to define a DynamicEndpointPath function in endpoints.config for the service');

    expect(() => buildEndpointPath(endpointClass)).toThrow(expected);
  });

  it('buildEndpointPath() should generate a valid url based on the provided tokens', () => {
    const endpointClass = DynamicEndpointClass.AdmAcmService;
    const tokens = { admAcmType: 'adm' };

    const actual = buildEndpointPath(endpointClass, tokens);
    const expected = '/acdm-management/adms';

    expect(actual).toEqual(expected);
  });

  it('buildEndpointPath() should generate a valid url when the endpointConfig is a string', () => {
    const endpointClass = DynamicEndpointClass.AdmRequestService;

    const actual = buildEndpointPath(endpointClass);
    const expected = '/acdm-management/adm-requests';

    expect(actual).toEqual(expected);
  });
});
