import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { MenuTabActions } from '~app/core/actions';
import { getActiveTab } from '~app/core/reducers';
import { AppState } from '~app/reducers';
import { MenuTab } from '~app/shared/models/menu-tab.model';

@Injectable({
  providedIn: 'root'
})
export class TabService {
  constructor(private store: Store<AppState>, private router: Router) {}

  public closeCurrentTab() {
    this.getCurrentTab().subscribe(currentTab => this.closeTab(currentTab));
  }

  public closeCurrentTabAndNavigate(url: string) {
    this.getCurrentTab().subscribe(currentTab => {
      this.router.navigateByUrl(url).then(() => {
        this.closeTab(currentTab);
      });
    });
  }

  public refreshCurrentTab() {
    this.getCurrentTab().subscribe(currentTab => {
      this.store.dispatch(new MenuTabActions.Refresh(currentTab));
    });
  }

  public isCurrentTabOnUrl(url: string): Observable<boolean> {
    if (!url) {
      return of(false);
    }

    return this.getCurrentTab().pipe(map(currentTab => currentTab.url.includes(url)));
  }

  public closeTab(tab: MenuTab) {
    this.store.dispatch(new MenuTabActions.Close(tab));
  }

  public enableTabEditMode(tab: MenuTab) {
    this.store.dispatch(new MenuTabActions.EnableEditMode(tab));
  }

  public disableTabEditMode(tab: MenuTab) {
    this.store.dispatch(new MenuTabActions.DisableEditMode(tab));
  }

  private getCurrentTab(): Observable<MenuTab> {
    return this.store.pipe(select(getActiveTab), take(1));
  }
}
