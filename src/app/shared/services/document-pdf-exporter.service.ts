import { ElementRef, Injectable } from '@angular/core';
import { jsPDF } from 'jspdf';

import { appConfiguration } from '~app/shared/services/app-configuration.service';

@Injectable()
export class DocumentPdfExporter {
  private contentToExport: ElementRef;
  private dimensionsA4 = appConfiguration.useNewPDFStyle
    ? { height: 843, width: 595, scaleFactor: 0.5 }
    : { height: 595, width: 843, scaleFactor: 0.5 };

  public async export(): Promise<jsPDF> {
    const pdf = this.createJsPDF();
    const width = this.dimensionsA4.width / this.dimensionsA4.scaleFactor;
    const elementToExport: HTMLElement = this.contentToExport.nativeElement;
    const element = elementToExport.cloneNode(true) as HTMLElement;
    element.style.width = width + 'px';
    // Indent the content of the PDF document
    if (appConfiguration.useNewPDFStyle) {
      element.classList.add('pdf-new-export', 'content');
    } else {
      element.classList.add('pdf-export', 'content');
    }

    // Set Roboto font in PDF
    const response: Response = await fetch('/assets/fonts/Roboto/Roboto-Regular.base64');
    const blob: Blob = await response.blob();

    pdf.addFileToVFS('Roboto-Regular-64', await blob.text());
    pdf.addFont('Roboto-Regular-64', 'Roboto', 'normal');
    pdf.setFont('Roboto');

    await pdf.html(element, {
      html2canvas: {
        scale: this.dimensionsA4.scaleFactor,
        // Used to trigger any media queries
        windowWidth: width,
        scrollX: 0,
        scrollY: 0,
        onclone: (document: Document) => this.applyPageBreaks(document.documentElement)
      }
    });

    return pdf;
  }

  public attach(contentToExport: ElementRef): void {
    this.contentToExport = contentToExport;
  }

  public detach(): void {
    this.contentToExport = null;
  }

  private applyPageBreaks(element: HTMLElement): void {
    const pageHeight = this.dimensionsA4.height / this.dimensionsA4.scaleFactor;
    const atomicElements = Array.from<HTMLElement>(element.querySelectorAll('.pdf-page-break-inside-avoid'));
    for (const atomicElement of atomicElements) {
      // Determine if the atomic element can fit on the current page.
      const clientRect = atomicElement.getBoundingClientRect();
      const fromTopOfPage = clientRect.top % pageHeight;
      const remainingPageHeight = pageHeight - fromTopOfPage;
      const canFitOnPage = remainingPageHeight > clientRect.height;

      if (!canFitOnPage) {
        // Create a page break element with the remaining available space on the page
        // to push the atomic element to the next page
        const pageBreak = document.createElement('div');
        pageBreak.style.height = remainingPageHeight + 'px';
        pageBreak.classList.add('pdf-page-break');

        atomicElement.parentElement.insertBefore(pageBreak, atomicElement);
      }
    }
  }

  private createJsPDF(): jsPDF {
    return new jsPDF({
      orientation: appConfiguration.useNewPDFStyle ? 'p' : 'l',
      unit: 'pt',
      format: 'a4',
      compress: true
    });
  }
}
