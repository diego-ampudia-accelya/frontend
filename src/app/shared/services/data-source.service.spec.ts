import { Observable, of } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { GLOBALS } from '../constants/globals';

import { DataSource } from './data-source.service';
import { RequestQuery } from '~app/shared/models/request-query.model';

const createSpyObj = jasmine.createSpyObj;

interface Entity {
  id: number;
  name: string;
}

const apiServiceStub = createSpyObj('ApiService<Entity>', ['getAll']);

const testRows = [{ id: 1, name: 'test name' }];

apiServiceStub.getAll.and.returnValue(
  of({
    records: testRows,
    size: 5,
    page: 0,
    totalElements: 1
  })
);

describe('DataSource', () => {
  let dataSource: DataSource<Entity>;

  beforeEach(() => {
    dataSource = new DataSource<Entity>(apiServiceStub);
  });

  afterEach(() => {
    dataSource.disconnect();
  });

  describe('loading$', () => {
    it('should emit false as first value', done => {
      dataSource.loading$.subscribe(loading => {
        expect(loading).toBe(false);
        done();
      });
    });
  });

  describe('paginationInfo$', () => {
    it('should emit null as first value', done => {
      dataSource.paginationInfo$.subscribe(paginationInfo => {
        expect(paginationInfo).toBeNull();
        done();
      });
    });
  });

  describe('get', () => {
    it('should call api getAll method', () => {
      dataSource.get(new RequestQuery());
      expect(apiServiceStub.getAll).toHaveBeenCalled();
    });

    it('should emit loading$ false, true, and false values', done => {
      const loadingValues: boolean[] = [];

      dataSource.loading$
        .pipe(
          finalize(() => {
            expect(loadingValues).toEqual([false, true, false]);
            done();
          })
        )
        .subscribe(result => {
          loadingValues.push(result);
        });

      dataSource.get(new RequestQuery());

      setTimeout(() => dataSource.disconnect(), GLOBALS.LOADING_SPINNER_DELAY + 100);
    });

    it('should emit next rows value', done => {
      const rows$ = dataSource.connect();
      const emittedRows = [];
      rows$
        .pipe(
          finalize(() => {
            expect(emittedRows).toEqual([[], testRows]);
            done();
          })
        )
        .subscribe(row => {
          emittedRows.push(row);
        });

      dataSource.get(new RequestQuery());

      setTimeout(() => dataSource.disconnect(), GLOBALS.LOADING_SPINNER_DELAY + 100);
    });
  });

  describe('connect', () => {
    it('should return Observable', () => {
      const rows$ = dataSource.connect();
      expect(rows$ instanceof Observable).toBe(true);
    });
  });

  describe('disconnect', () => {
    it('shuld complete all subjects', () => {
      const ds: any = dataSource;
      const itemsSubjectCompleteSpy = spyOn(ds.itemsSubject, 'complete');
      const loadingSubjectCompleteSpy = spyOn(ds.loadingSubject, 'complete');
      const paginationInfoSubjectCompleteSpy = spyOn(ds.paginationInfoSubject, 'complete');

      dataSource.disconnect();

      expect(itemsSubjectCompleteSpy).toHaveBeenCalled();
      expect(loadingSubjectCompleteSpy).toHaveBeenCalled();
      expect(paginationInfoSubjectCompleteSpy).toHaveBeenCalled();
    });
  });
});
