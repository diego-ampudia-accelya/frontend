import { Validators } from '@angular/forms';

export const FIRST_NAME_VALIDATOR = [Validators.required, Validators.pattern('[a-zA-Z\\s\xC0-\uFFFF]{1,49}')];
export const LAST_NAME_VALIDATOR = [Validators.required, Validators.pattern('[a-zA-Z\\s\xC0-\uFFFF]{1,100}')];
