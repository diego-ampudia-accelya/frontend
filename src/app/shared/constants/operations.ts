export const EQ = '=';
export const LT = '<';
export const GT = '>';
export const IN = 'in';
export const LIKE = 'like';
export const LTE = '<=';
export const GTE = '>=';
