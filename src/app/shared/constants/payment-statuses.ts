import { DropdownOption } from '~app/shared/models/dropdown-option.model';

export const paymentStatuses: DropdownOption[] = [
  {
    value: 'ACTIVE',
    label: 'Active'
  },
  {
    value: 'NOT_AUTHORIZED',
    label: 'Not Authorized'
  },
  {
    value: 'NON_ACTIVE',
    label: 'Non Active'
  }
];
