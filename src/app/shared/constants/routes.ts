/* eslint-disable @typescript-eslint/naming-convention */
import {
  commonAcdmPermission,
  createAdmGlobalPolicyFile,
  getAcdmCategorizationRules,
  getAcdmFilesPermissions,
  getAcmQueryPermissions,
  getAdmQueryPermissions,
  getUpdateAcdmCategorizationRules,
  getViewAcdntPermission,
  getViewAdmPolicies,
  issuePendingSupervisionPermission,
  viewAcdmPermission
} from '~app/adm-acm/shared/helpers/adm-acm-permissions.config';
import {
  getAcdmdCreationAgentPermission,
  getAcdmdCreationAirlPermission,
  getAllAcmdQueryPermissions,
  getAllAdmdQueryPermissions
} from '~app/bsp-adjustment/shared/helpers/acdmd-permissions.config';
import {
  creationSpcrPermission,
  creationSpdrPermission,
  getAllSpcrQueryPermissions,
  getAllSpdrQueryPermissions
} from '~app/bsp-adjustment/shared/helpers/spcdr-permissions.config';
import { MasterDataType } from '~app/master-shared/models/master.model';
import {
  createRefundPermission,
  createRefundPermissionUnderSupervision,
  getRaRnMenuItemPermissions,
  getRefundsFilesPermissions,
  modifyRefundPermission,
  updateRefundPermission
} from '~app/refund/shared/helpers/refund-permissions.config';
import { Permissions } from '~app/shared/constants/permissions';
import { getReadTipCardPermissions } from '~app/tip/tip-cards/config/tip-card-permissions.config';
import {
  getReadTipProductListPermissions,
  getReadTipProductPermissions
} from '~app/tip/tip-prov-products/config/tip-product-permissions.config';
import { UserType } from '../models/user.model';
import { FakePermissions } from './fake-permissions';

export const ROUTES = {
  // Main menu items
  DASHBOARD: {
    url: '/dashboards',
    label: 'DASHBOARD.dashboards.tabLabel',
    showMenu: true,
    tabLabel: 'DASHBOARD.dashboards.tabLabel',
    path: 'dashboards',
    permissionId: Permissions.notProtected,
    parent: null
  },
  ERROR: {
    url: '/error',
    label: '',
    showMenu: false,
    tabLabel: '',
    path: 'error',
    permissionId: Permissions.notProtected,
    parent: null
  },
  HELP: {
    url: '/help',
    label: 'help.tabLbl',
    showMenu: false,
    tabLabel: 'help.tabLbl',
    path: 'help',
    permissionId: [
      FakePermissions.readOnlineHelpIATA,
      FakePermissions.readOnlineHelpAirline,
      FakePermissions.readOnlineHelpAgent,
      FakePermissions.readOnlineHelpAgentGroup,
      FakePermissions.readOnlineHelpThirdParty,
      FakePermissions.readOnlineHelpGDS
    ],
    parent: null
  },
  USER_FEEDBACK: {
    url: '/user-feedback',
    label: '',
    showMenu: false,
    tabLabel: '',
    path: 'user-feedback',
    permissionId: Permissions.notProtected,
    parent: null
  },
  TERMS: {
    url: '/terms-and-conditions',
    label: 'termsOfUse',
    showMenu: false,
    tabLabel: 'termsOfUse',
    path: 'terms-and-conditions',
    permissionId: Permissions.notProtected,
    parent: null
  },
  USER_FEEDBACK_DETAILS: {
    url: '/user-feedback/user-details',
    label: '',
    showMenu: false,
    tabLabel: '',
    path: 'user-details',
    permissionId: Permissions.notProtected,
    parent: null
  },
  USER_FEEDBACK_FINISH: {
    url: '/user-feedback/finish',
    label: '',
    showMenu: false,
    tabLabel: '',
    path: 'finish',
    permissionId: Permissions.notProtected,
    parent: null
  },
  LOGOUT_FORM: {
    url: '/logout-form',
    label: 'logout-form',
    showMenu: true,
    tabLabel: 'logout-form',
    path: 'logout-form',
    permissionId: 'logout-form',
    parent: null
  },
  SEARCH: {
    url: '/search',
    label: 'SEARCH_QUERY.tabLbl',
    showMenu: false,
    tabLabel: 'SEARCH_QUERY.tabLbl',
    path: 'search',
    parent: null,
    permissionId: Permissions.notProtected
  },
  SEARCH_LIST: {
    url: '/search/list',
    label: 'SEARCH_QUERY.tabLbl',
    showMenu: false,
    tabLabel: 'SEARCH_QUERY.tabLbl',
    path: 'list/:searchQuery',
    parent: null,
    permissionId: Permissions.notProtected,
    uniqueIdFromUrlRegex: '/search/list/\\d+'
  },
  MY_ORGANIZATION: {
    url: '/master-data/my',
    label: 'MENU.MASTER_DATA.MY.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'my-organization',
    permissionId: Permissions.notProtected,
    parent: null
  },
  MONITOR: {
    url: '/master-data/iata/monitor',
    label: 'MENU.MASTER_DATA.MONITOR.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'monitor',
    permissionId: [Permissions.notProtected, Permissions.readMonitoringTaFop, Permissions.readMonitoringHotData],
    allowedUserTypes: [UserType.IATA],
    parent: 'MY_ORGANIZATION'
  },
  NOTIFICATIONS_LIST: {
    url: '/notifications',
    label: 'MENU.NOTIFICATIONS.LBL',
    showMenu: false,
    tabLabel: 'MENU.NOTIFICATIONS.LBL',
    path: 'notifications',
    permissionId: Permissions.readNotificationSettings,
    parent: null
  },
  MY_AIRLINE: {
    url: '/master-data/airline/my',
    label: 'MENU.MASTER_DATA.SETTINGS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SETTINGS.LBL',
    path: 'master-data',
    allowedUserTypes: UserType.AIRLINE,
    permissionId: Permissions.notProtected,
    parent: 'MY_ORGANIZATION'
  },
  MY_AGENT: {
    url: '/master-data/agent/my',
    label: 'MENU.MASTER_DATA.SETTINGS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SETTINGS.LBL',
    path: 'master-data',
    allowedUserTypes: UserType.AGENT,
    permissionId: Permissions.notProtected,
    parent: 'MY_ORGANIZATION'
  },
  MY_THIRD_PARTY: {
    url: '/master-data/third-party/my',
    label: 'MENU.MASTER_DATA.SETTINGS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SETTINGS.LBL',
    path: 'master-data',
    allowedUserTypes: UserType.THIRD_PARTY,
    permissionId: [Permissions.readThirdPartySettings, Permissions.readACDMAuthorityThirdParty],
    parent: 'MY_ORGANIZATION'
  },
  MY_AGENT_GROUP: {
    url: '/master-data/agent-group/my',
    label: 'MENU.MASTER_DATA.SETTINGS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SETTINGS.LBL',
    path: 'agent-group/my',
    allowedUserTypes: UserType.AGENT_GROUP,
    permissionId: Permissions.readAgentGroupSettings,
    parent: 'MY_ORGANIZATION'
  },
  MY_IATA: {
    url: '/master-data/iata/my',
    label: 'MENU.MASTER_DATA.SETTINGS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SETTINGS.LBL',
    path: 'master-data',
    allowedUserTypes: UserType.IATA,
    permissionId: Permissions.readSftpAccount,
    parent: 'MY_ORGANIZATION'
  },
  MY_GDS: {
    url: '/master-data/gds/my',
    label: 'MENU.MASTER_DATA.SETTINGS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SETTINGS.LBL',
    path: 'master-data',
    allowedUserTypes: UserType.GDS,
    permissionId: Permissions.readAgentGds,
    parent: 'MY_ORGANIZATION'
  },
  MY_DPC: {
    url: '/master-data/dpc/my',
    label: 'MENU.MASTER_DATA.SETTINGS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SETTINGS.LBL',
    path: 'master-data',
    allowedUserTypes: UserType.DPC,
    permissionId: Permissions.readGeneralDpcConfiguration,
    parent: 'MY_ORGANIZATION'
  },
  IATA: {
    url: '/master-data/iata',
    path: 'iata',
    parent: null,
    showMenu: false
  },
  DPC: {
    url: '/master-data/dpc',
    path: 'dpc',
    parent: null,
    showMenu: false
  },
  ACDMS: {
    url: '/acdms',
    label: 'MENU.ACDMS.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'acdms',
    permissionId: Permissions.protectedByChildren,
    parent: null
  },
  REFUNDS: {
    url: '/refunds',
    label: 'MENU.REFUNDS.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'refunds',
    permissionId: Permissions.protectedByChildren,
    parent: null
  },
  DOCUMENTS: {
    url: '/documents',
    label: 'MENU.documents.label',
    showMenu: true,
    tabLabel: null,
    path: 'documents',
    permissionId: Permissions.protectedByChildren,
    parent: null
  },
  FILES: {
    url: '/files',
    label: 'MENU.files.label',
    showMenu: true,
    tabLabel: null,
    path: 'files',
    permissionId: Permissions.protectedByChildren,
    parent: null
  },
  SALES: {
    url: '/sales',
    label: 'MENU.sales.label',
    showMenu: true,
    tabLabel: null,
    path: 'sales',
    permissionId: Permissions.protectedByChildren,
    parent: null
  },
  MASTER_DATA: {
    url: '/master-data',
    label: 'MENU.MASTER_DATA.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'master-data',
    permissionId: Permissions.protectedByChildren,
    parent: null
  },
  TIP: {
    url: '/tip',
    label: 'MENU.TIP.label',
    showMenu: true,
    tabLabel: null,
    path: 'tip',
    permissionId: Permissions.protectedByChildren,
    parent: null
  },
  WECHAT: {
    url: '/wechat',
    label: 'MENU.WECHAT.label',
    showMenu: true,
    tabLabel: 'MENU.WECHAT.tapLabel',
    path: 'wechat',
    permissionId: Permissions.readWechat,
    allowedUserTypes: [UserType.AGENT, UserType.AIRLINE],
    parent: null
  },

  TRAINING: {
    url: '/training',
    label: null,
    showMenu: false,
    tabLabel: null,
    path: 'training',
    permissionId: null,
    parent: null
  },

  TRAINING_LOGIN: {
    url: '/training/login',
    label: null,
    showMenu: false,
    tabLabel: null,
    path: 'login',
    permissionId: null,
    parent: null
  },

  TRAINING_START: {
    url: '/training/start',
    label: null,
    showMenu: false,
    tabLabel: null,
    path: 'start',
    permissionId: null,
    parent: null
  },

  TRAINING_FINISH: {
    url: '/training/finish',
    label: null,
    showMenu: false,
    tabLabel: null,
    path: 'finish',
    permissionId: 'training-finish',
    parent: null
  },

  USER_PROFILE: {
    url: '/user-profile',
    label: 'headerUserInfo.properties',
    path: 'user-profile',
    showMenu: false,
    tabLabel: 'headerUserInfo.propertiesTab.title',
    permissionId: [Permissions.readProfile, Permissions.readProfileAirlineMulticountry],
    parent: null
  },
  // Setting changes
  SETTINGS_CHANGES: {
    url: '/master-data/iata/settings-changes',
    label: 'menu.masterData.settingsChanges.label',
    showMenu: true,
    tabLabel: 'menu.masterData.settingsChanges.label',
    path: 'settings-changes',
    allowedUserTypes: UserType.IATA,
    permissionId: Permissions.readSettingsChanges,
    parent: 'MY_ORGANIZATION'
  },
  // Audit
  AUDIT: {
    url: '/master-data/audit',
    label: 'MENU.AUDIT.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'audit',
    permissionId: Permissions.protectedByChildren,
    allowedUserTypes: [UserType.AIRLINE],
    parent: 'MY_ORGANIZATION'
  },
  AUDIT_APPLICATION_ACCESS: {
    url: '/master-data/audit/application-access',
    label: 'MENU.AUDIT.APPLICATION_ACCESS.LBL',
    showMenu: true,
    tabLabel: 'MENU.AUDIT.APPLICATION_ACCESS.TAB',
    path: 'application-access',
    permissionId: [Permissions.readAuditLog],
    allowedUserTypes: [
      UserType.AIRLINE,
      UserType.IATA,
      UserType.AGENT_GROUP,
      UserType.AGENT,
      UserType.DPC,
      UserType.GDS,
      UserType.THIRD_PARTY
    ],
    parent: 'AUDIT'
  },
  AUDIT_ADM_ACM: {
    url: '/master-data/audit/adm-acm',
    label: 'MENU.AUDIT.ADM/ACM.LBL',
    showMenu: true,
    tabLabel: 'MENU.AUDIT.ADM/ACM.TAB',
    path: 'adm-acm',
    permissionId: [Permissions.readAuditAcdm],
    allowedUserTypes: [UserType.AGENT, UserType.IATA, UserType.AIRLINE, UserType.GDS, UserType.AGENT_GROUP],
    parent: 'AUDIT'
  },
  AUDIT_REFUNDS: {
    url: '/master-data/audit/refunds',
    label: 'MENU.AUDIT.REFUNDS.LBL',
    showMenu: true,
    tabLabel: 'MENU.AUDIT.REFUNDS.TAB',
    path: 'refunds',
    permissionId: [Permissions.readAuditRefund],
    allowedUserTypes: [UserType.IATA, UserType.AIRLINE, UserType.AGENT, UserType.AGENT_GROUP],
    parent: 'AUDIT'
  },
  AUDIT_PBDS: {
    url: '/master-data/audit/pbds',
    label: 'MENU.AUDIT.PBDS.LBL',
    showMenu: true,
    tabLabel: 'MENU.AUDIT.PBDS.TAB',
    path: 'pbds',
    permissionId: [Permissions.readAuditPbds],
    allowedUserTypes: [UserType.IATA, UserType.AIRLINE, UserType.AGENT, UserType.AGENT_GROUP],
    parent: 'AUDIT'
  },
  // Sub users
  USERS: {
    url: '/master-data/user-maintenance',
    label: 'MENU.USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS.TAB',
    path: 'user-maintenance',
    permissionId: Permissions.protectedByChildren,
    allowedUserTypes: [UserType.AIRLINE, UserType.AGENT, UserType.GDS, UserType.THIRD_PARTY, UserType.AGENT_GROUP],
    parent: 'MY_ORGANIZATION'
  },
  //First sublevel sub users
  SUB_USERS: {
    url: '/master-data/user-maintenance/sub-users',
    label: 'MENU.SUBUSERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.SUBUSERS.TABS.LIST',
    path: 'sub-users',
    permissionId: [Permissions.readAllSubusers, Permissions.readSubuser],
    allowedUserTypes: [
      UserType.AIRLINE,
      UserType.AGENT,
      UserType.GDS,
      UserType.THIRD_PARTY,
      UserType.AGENT_GROUP,
      UserType.IATA,
      UserType.DPC
    ],
    parent: 'USERS'
  },
  //User-administration
  USERS_MAINTENANCE: {
    url: '/master-data/iata/users-management',
    label: 'MENU.USERS_MAINTENANCE.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'user-administration',
    permissionId: Permissions.protectedByChildren,
    parent: 'MY_ORGANIZATION'
  },
  SUSPENDED_AIRLINE: {
    url: '/master-data/iata/suspended-airline',
    label: 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.SUSPENDED_AIRLINE.TAB',
    path: 'suspended-airline',
    permissionId: [Permissions.readIataSuspendedAirlines],
    allowedUserTypes: UserType.IATA,
    parent: 'USERS_MAINTENANCE'
  },
  MAIN_USERS: {
    url: '/master-data/iata/main-users',
    label: 'MENU.USERS_MAINTENANCE.MAIN_USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.MAIN_USERS.TAB',
    path: 'main-users',
    permissionId: [Permissions.readMainUsers],
    allowedUserTypes: UserType.IATA,
    parent: 'USERS_MAINTENANCE'
  },
  ACCESS_PERMISSIONS: {
    url: '/master-data/iata/access-permissions',
    label: 'MENU.USERS_MAINTENANCE.ACCESS_PERMISSIONS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.ACCESS_PERMISSIONS.TAB',
    path: 'access-permissions',
    permissionId: [Permissions.readMainUserPermissions],
    allowedUserTypes: [UserType.IATA],
    parent: 'USERS_MAINTENANCE'
  },
  MAIN_USER_DETAILS: {
    url: '/master-data/iata/main-users/details',
    label: 'MENU.USERS_MAINTENANCE.MAIN_USERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.USERS_MAINTENANCE.MAIN_USERS.TABS.DETAILS',
    path: 'main-users/details/:id',
    permissionId: [Permissions.readMainUsers, Permissions.readMainUserPermissions],
    allowedUserTypes: UserType.IATA,
    uniqueIdFromUrlRegex: '/details/\\d+',
    parent: 'USERS_MAINTENANCE'
  },
  GLOBAL_USER_FILE: {
    url: '/master-data/iata/global-user-file',
    label: 'MENU.USERS_MAINTENANCE.GLOBAL_USER_FILE.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'global-user-file',
    permissionId: [Permissions.requestIataGlobalUsersFile],
    allowedUserTypes: UserType.IATA,
    parent: 'USERS_MAINTENANCE'
  },
  SFTP_ACCOUNTS: {
    url: '/master-data/iata/sftp-accounts',
    label: 'MENU.USERS_MAINTENANCE.SFTP_ACCOUNTS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.SFTP_ACCOUNTS.TAB',
    path: 'sftp-accounts',
    permissionId: [Permissions.readSftpAccounts],
    allowedUserTypes: [UserType.IATA],
    parent: 'USERS_MAINTENANCE'
  },
  AGENTS_MAINTENANCE: {
    url: '/master-data/iata',
    label: 'MENU.USERS_MAINTENANCE.AGENTS_MAINTENANCE.LBL',
    showMenu: true,
    tabLabel: null,
    permissionId: Permissions.protectedByChildren,
    parent: 'USERS_MAINTENANCE'
  },
  DEACTIVATED_AGENTS: {
    url: '/master-data/iata/deactivated-agents',
    label: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.DEACTIVATED_AGENTS.TAB',
    path: 'deactivated-agents',
    permissionId: [Permissions.requestIataDeactivatedAgents],
    allowedUserTypes: UserType.IATA,
    parent: 'AGENTS_MAINTENANCE'
  },
  REINSTATED_AGENTS: {
    url: '/master-data/iata/reinstated-agents',
    label: 'MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.REINSTATED_AGENTS.TAB',
    path: 'reinstated-agents',
    permissionId: [Permissions.requestIataReinstatedAgents],
    allowedUserTypes: UserType.IATA,
    parent: 'AGENTS_MAINTENANCE'
  },
  DOWNLOAD_SUB_USERS: {
    url: '/master-data/iata/download-sub-users',
    label: 'MENU.USERS_MAINTENANCE.DOWNLOAD_SUB_USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.DOWNLOAD_SUB_USERS.TAB',
    path: 'download-sub-users',
    permissionId: [Permissions.createSubUsersFile],
    allowedUserTypes: [UserType.IATA],
    parent: 'USERS_MAINTENANCE'
  },
  HOMU_USERS_LIST: {
    url: '/master-data/iata/homu-users',
    label: 'MENU.USERS_MAINTENANCE.HOMU_USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USERS_MAINTENANCE.HOMU_USERS.TAB',
    path: 'homu-users',
    permissionId: [Permissions.readHomu],
    allowedUserTypes: [UserType.IATA],
    parent: 'USERS_MAINTENANCE'
  },
  HOMU_USERS_PROFILE: {
    url: '/master-data/iata/homu-users/details',
    label: 'MENU.USERS_MAINTENANCE.HOMU_USERS.PROFILE.LBL',
    showMenu: false,
    tabLabel: 'MENU.USERS_MAINTENANCE.HOMU_USERS.PROFILE.TAB',
    path: 'homu-users/details/:id',
    permissionId: [Permissions.readHomu, Permissions.readHomuPerm, Permissions.updateHomuPermissions],
    allowedUserTypes: [UserType.IATA],
    uniqueIdFromUrlRegex: '/details/\\d+',
    parent: 'USERS_MAINTENANCE'
  },
  HOMC_SUB_USER: {
    url: '/master-data/homc-sub-users',
    label: 'MENU.HOMCSU.LBL',
    showMenu: true,
    tabLabel: 'MENU.HOMCSU.TAB',
    path: 'homc-sub-users',
    permissionId: [Permissions.readHOMCSU],
    parent: 'USERS'
  },
  HOMC_SUB_USER_DETAILS: {
    url: '/master-data/homc-sub-users/details',
    label: 'MENU.HOMC_SUBUSERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.HOMC_SUBUSERS.TAB',
    path: 'details/:id',
    permissionId: [Permissions.readHOMCSU],
    parent: 'USERS'
  },
  HOSU_SUB_USER: {
    url: '/master-data/user-maintenance/hosu-sub-users',
    label: 'MENU.HOSU_SUBUSERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.HOSU_SUBUSERS.TABS.LIST',
    path: 'hosu-sub-users',
    permissionId: [Permissions.readMulticountrySubuser],
    allowedUserTypes: [UserType.AIRLINE],
    parent: 'USERS'
  },
  LOMU_SUB_USER: {
    url: '/master-data/user-maintenance/lomu-sub-users',
    label: 'MENU.LOMU_SUBUSERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.LOMU_SUBUSERS.TAB',
    path: 'lomu-sub-users',
    permissionId: [Permissions.readLomuProfile, Permissions.readLomuPerm],
    allowedUserTypes: [UserType.AIRLINE],
    parent: 'USERS'
  },
  SUB_USER_HOSU_CREATE: {
    url: '/master-data/user-maintenance/hosu-sub-users/create-hosu',
    label: 'MENU.HOSU_SUBUSERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.HOSU_SUBUSERS.TABS.CREATE_HOSU',
    path: 'hosu-sub-users/create-hosu',
    permissionId: [Permissions.createHosuSubuser],
    allowedUserTypes: [UserType.AIRLINE, UserType.AGENT, UserType.GDS, UserType.THIRD_PARTY, UserType.AGENT_GROUP],
    showBadge: true,
    parent: 'USERS'
  },
  //Other screens child from sub user
  SUB_USER_DETAILS: {
    url: '/master-data/user-maintenance/sub-users/details',
    label: 'MENU.SUBUSERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.SUBUSERS.TABS.DETAILS',
    path: 'sub-users/details/:id',
    permissionId: [Permissions.updateHosuSubuser, Permissions.readMulticountrySubuser, Permissions.createSubuser],
    allowedUserTypes: [
      UserType.AIRLINE,
      UserType.AGENT,
      UserType.GDS,
      UserType.THIRD_PARTY,
      UserType.AGENT_GROUP,
      UserType.IATA,
      UserType.DPC
    ],
    uniqueIdFromUrlRegex: '/details/\\d+',
    parent: 'USERS'
  },
  SUB_USER_HOSU_DETAILS: {
    url: '/master-data/user-maintenance/hosu-sub-users/details',
    label: 'MENU.SUBUSERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.SUBUSERS.TABS.DETAILS',
    path: 'hosu-sub-users/details/:id',
    permissionId: [Permissions.updateHosuSubuser, Permissions.readMulticountrySubuser, Permissions.createHosuSubuser],
    allowedUserTypes: [UserType.AIRLINE, UserType.AGENT, UserType.GDS, UserType.THIRD_PARTY, UserType.AGENT_GROUP],
    uniqueIdFromUrlRegex: '/details/\\d+',
    showBadge: true,
    parent: 'USERS'
  },
  SUB_USER_TEMPLATES: {
    url: '/master-data/user-maintenance/sub-users/templates',
    label: 'MENU.SUBUSERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.SUBUSERS.TABS.TEMPLATES',
    path: 'sub-users/templates',
    // Only Streamlined Airline users can manage templates. They will also have the required permission
    permissionId: Permissions.readSubuserTemplate,
    allowedUserTypes: [UserType.AIRLINE],
    parent: 'USERS'
  },
  SUB_USER_PERMISSIONS: {
    url: '/master-data/user-maintenance/sub-users/permissions',
    label: 'MENU.SUBUSERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.SUBUSERS.TABS.PERMISSIONS',
    path: 'sub-users/permissions',
    permissionId: Permissions.readSubuserPermissions,
    allowedUserTypes: [
      UserType.AIRLINE,
      UserType.AGENT,
      UserType.GDS,
      UserType.THIRD_PARTY,
      UserType.AGENT_GROUP,
      UserType.IATA
    ],
    parent: 'USERS'
  },
  HOMU_SUB_USER: {
    url: '/master-data/user-maintenance/homu-sub-users',
    label: 'MENU.HOMU_SUBUSERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.HOMU_SUBUSERS.TAB',
    path: 'homu-sub-users',
    permissionId: [Permissions.readSubuserHosuPermissions],
    allowedUserTypes: [UserType.AIRLINE],
    parent: 'USERS'
  },

  // Master data menu items
  AIRLINE: {
    url: '/master-data/airline',
    label: 'MENU.MASTER_DATA.AIRLINE.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.AIRLINE.LBL',
    path: 'airline',
    permissionId: Permissions.readAirline,
    parent: 'MASTER_DATA'
  },
  CREDIT_CARDS: {
    url: '/master-data/credit-cards',
    label: 'MENU.MASTER_DATA.CREDIT_CARDS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.CREDIT_CARDS.LBL',
    path: 'credit-cards',
    permissionId: Permissions.readCreditCards,
    allowedUserTypes: [UserType.IATA],
    parent: 'MASTER_DATA'
  },
  MONITOR_HOT_DATA: {
    url: '/master-data/iata/monitor/hot-data',
    label: 'MENU.MASTER_DATA.MONITOR_HOT_DATA.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.MONITOR_HOT_DATA.LBL',
    path: 'monitor/hot-data',
    permissionId: Permissions.readMonitoringHotData,
    allowedUserTypes: [UserType.IATA],
    parent: 'MONITOR'
  },
  MONITOR_MASS_UPLOAD_FILES: {
    url: '/master-data/iata/monitor/mass-upload',
    label: 'MENU.MASTER_DATA.MONITOR_MASS_UPLOAD_FILES.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.MONITOR_MASS_UPLOAD_FILES.LBL',
    path: 'monitor/mass-upload',
    permissionId: Permissions.readMonitoringMassUploadFiles,
    allowedUserTypes: [UserType.IATA],
    parent: 'MONITOR'
  },
  MONITOR_HOT_DATA_DETAILS: {
    url: '/master-data/iata/monitor/hot-details',
    label: 'MENU.MASTER_DATA.MONITOR_HOT_DATA_DETAILS.LBL',
    showMenu: false,
    tabLabel: 'MENU.MASTER_DATA.MONITOR_HOT_DATA_DETAILS.LBL',
    path: 'monitor/hot-details/:id',
    uniqueIdFromUrlRegex: '/hot-details/.+',
    permissionId: Permissions.readMonitoringHotData,
    allowedUserTypes: [UserType.IATA],
    parent: 'MONITOR'
  },
  MONITOR_GLOBAL_TA_AND_FOP_FILES: {
    url: '/master-data/iata/monitor/global-ta-and-fop-files',
    label: 'MENU.MASTER_DATA.MONITOR_GLOBAL_TA_AND_FOP_FILES.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.MONITOR_GLOBAL_TA_AND_FOP_FILES.LBL',
    path: 'monitor/global-ta-and-fop-files',
    permissionId: Permissions.readMonitoringTaFop,
    allowedUserTypes: [UserType.IATA],
    parent: 'MONITOR'
  },
  MONITOR_VARIABLE_REMITTANCE: {
    url: '/master-data/iata/monitor/variable-remittance',
    label: 'MENU.MASTER_DATA.MONITOR_VARIABLE_REMITTANCE.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.MONITOR_VARIABLE_REMITTANCE.LBL',
    path: 'monitor/variable-remittance',
    permissionId: Permissions.readMonitoringVR,
    allowedUserTypes: [UserType.IATA],
    parent: 'MONITOR'
  },
  MONITOR_MASTER_DATA_RET: {
    url: '/master-data/iata/monitor/master-data-ret',
    label: 'MENU.MASTER_DATA.MONITOR_MASTER_DATA_RET.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.MONITOR_MASTER_DATA_RET.LBL',
    path: 'monitor/master-data-ret',
    permissionId: Permissions.readMonitoringMasterDataRet,
    allowedUserTypes: [UserType.IATA],
    parent: 'MONITOR'
  },
  MONITOR_PBD_FILES: {
    url: '/master-data/iata/monitor/pbd_files',
    label: 'MENU.MASTER_DATA.MONITOR_PBD_FILES.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.MONITOR_PBD_FILES.LBL',
    path: 'monitor/pbd_files',
    permissionId: Permissions.readMonitoringPbdFiles,
    allowedUserTypes: [UserType.IATA],
    parent: 'MONITOR'
  },
  GLOBAL_AIRLINE: {
    url: '/master-data/global-airline',
    label: 'MENU.MASTER_DATA.GLOBAL_AIRLINE.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.GLOBAL_AIRLINE.LBL',
    path: 'global-airline',
    permissionId: Permissions.readGlobalAirline,
    parent: 'MASTER_DATA'
  },
  AIRLINE_PROFILE: {
    url: '/master-data/airline',
    label: 'menu.masterData.airlineProfile.label',
    showMenu: false,
    tabLabel: 'menu.masterData.airlineProfile.label',
    path: 'airline',
    permissionId: Permissions.readAirline,
    parent: 'MASTER_DATA'
  },
  AGENT: {
    url: '/master-data/agent',
    label: 'MENU.MASTER_DATA.AGENT.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.AGENT.LBL',
    path: 'agent',
    permissionId: Permissions.readAgent,
    parent: 'MASTER_DATA'
  },
  AGENT_PROFILE: {
    url: '/master-data/agent',
    label: 'menu.masterData.agentProfile.label',
    showMenu: false,
    tabLabel: 'menu.masterData.agentProfile.label',
    path: 'agent',
    permissionId: Permissions.readAgent,
    parent: 'MASTER_DATA'
  },
  THIRD_PARTY: {
    url: '/master-data/third-party',
    label: 'MENU.MASTER_DATA.thirdParty.lbl',
    showMenu: false,
    tabLabel: 'MENU.MASTER_DATA.thirdParty.lbl',
    path: 'third-party',
    parent: 'MASTER_DATA',
    permissionId: Permissions.forbidden
  },
  AGENT_GROUP: {
    url: '/master-data/agent-group',
    label: 'MENU.MASTER_DATA.AGENT_GROUP.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.AGENT_GROUP.LBL',
    path: 'agent-group',
    parent: 'MASTER_DATA',
    allowedUserTypes: [UserType.AIRLINE, UserType.IATA, UserType.THIRD_PARTY, UserType.GDS, UserType.DPC],
    permissionId: Permissions.readAgentGroup
  },
  AGENT_GROUP_MEMBERS: {
    url: '/master-data/agent-group/members',
    label: 'MENU.MASTER_DATA.AGENT_GROUP_MEMBERS.LBL',
    showMenu: false,
    tabLabel: 'MENU.MASTER_DATA.AGENT_GROUP_MEMBERS.LBL',
    path: 'members/:id',
    parent: 'AGENT_GROUP',
    uniqueIdFromUrlRegex: '/agent-group/members/\\d+'
  },
  BSP: {
    url: '/master-data/bsp',
    label: 'MENU.MASTER_DATA.BSP.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.BSP.LBL',
    path: 'bsp',
    permissionId: Permissions.forbidden,
    parent: 'MASTER_DATA'
  },
  BSP_PROFILE: {
    url: '/master-data/bsp',
    label: 'menu.masterData.bspProfile.label',
    showMenu: false,
    tabLabel: 'menu.masterData.bspProfile.label',
    path: 'bsp',
    permissionId: Permissions.readBsp,
    parent: 'MASTER_DATA',
    uniqueIdFromUrlRegex: 'bsp/\\d+'
  },
  CURRENCY: {
    url: '/master-data/currency',
    label: 'MENU.MASTER_DATA.CURRENCY.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.CURRENCY.LBL',
    path: 'currency',
    permissionId: Permissions.readCurrency,
    parent: 'MASTER_DATA'
  },
  BSP_CURRENCY_ASSIGNATION: {
    url: '/master-data/bsp_currency_assignation',
    label: 'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.BSP_CURRENCY_ASSIGNATION.LBL',
    path: 'bsp_currency_assignation',
    permissionId: Permissions.readBspCurrencyAssignation,
    parent: 'MASTER_DATA'
  },
  GDS: {
    url: '/master-data/gds',
    label: 'MENU.MASTER_DATA.GDS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.GDS.LBL',
    path: 'gds',
    permissionId: Permissions.forbidden,
    parent: 'MASTER_DATA'
  },
  BSP_GDS_ASSIGNATION: {
    url: '/master-data/bsp_gds_assignation',
    label: 'MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.BSP_GDS_ASSIGNATION.LBL',
    path: 'bsp_gds_assignation',
    permissionId: Permissions.readGdsBspAssignments,
    parent: 'MASTER_DATA'
  },
  GDS_AGENTS_LIST: {
    url: '/master-data/gds-agents-list',
    label: 'MENU.MASTER_DATA.AGENT_BY_GDS.LBL',
    showMenu: false,
    tabLabel: 'MENU.MASTER_DATA.AGENT_BY_GDS.LBL',
    path: 'gds-agents-list',
    permissionId: Permissions.readAgentGds,
    parent: 'MASTER_DATA'
  },
  PERIODS: {
    url: '/master-data/periods',
    label: 'MENU.MASTER_DATA.PERIODS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.PERIODS.LBL',
    path: 'periods',
    permissionId: Permissions.readPeriod,
    parent: 'MASTER_DATA'
  },
  VARIABLE_REMITTANCE: {
    url: '/master-data/variable-remittance',
    label: 'MENU.MASTER_DATA.VARIABLE_REMITTANCE.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.VARIABLE_REMITTANCE.LBL',
    path: 'variable-remittance',
    permissionId: [
      Permissions.readOverrideFrequency,
      Permissions.createOverrideFrequency,
      Permissions.updateOverrideFrequency,
      Permissions.deleteOverrideFrequency
    ],
    parent: 'MASTER_DATA'
  },
  FILE_DESCRIPTORS: {
    url: '/master-data/file-descriptors',
    label: 'MENU.MASTER_DATA.FILE_DESCRIPTORS.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.FILE_DESCRIPTORS.LBL',
    path: 'file-descriptors',
    permissionId: Permissions.readFileDescriptors,
    allowedUserTypes: [UserType.IATA, UserType.DPC],
    parent: 'MASTER_DATA'
  },
  SAF_TYPES: {
    url: '/master-data/bsplink-safs',
    label: 'MENU.MASTER_DATA.SAF_TYPES.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SAF_TYPES.LBL',
    path: 'bsplink-safs',
    permissionId: [Permissions.readSafTypes, Permissions.readDocumentTypes],
    allowedUserTypes: [UserType.IATA],
    parent: 'MASTER_DATA'
  },
  SAF_SERIES: {
    url: '/master-data/saf-series',
    label: 'MENU.MASTER_DATA.SAF_SERIES.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.SAF_SERIES.LBL',
    path: 'saf-series',
    permissionId: [Permissions.readSafTypes, Permissions.readDocumentTypes, Permissions.readDocumentSeries],
    allowedUserTypes: [UserType.IATA],
    parent: 'MASTER_DATA'
  },
  COMPANY_CALENDAR: {
    url: '/master-data/company-calendar',
    label: 'MENU.MASTER_DATA.COMPANY_CALENDAR.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.COMPANY_CALENDAR.LBL',
    path: 'company-calendar',
    permissionId: [Permissions.readCompanyCalendar],
    allowedUserTypes: [
      UserType.AGENT,
      UserType.AGENT_GROUP,
      UserType.AIRLINE,
      UserType.DPC,
      UserType.GDS,
      UserType.THIRD_PARTY
    ],
    parent: 'MASTER_DATA'
  },
  TICKETING_AUTHORITY_HISTORY: {
    url: '/master-data/ticketing-authority-history',
    label: 'MENU.MASTER_DATA.TICKETING_AUTHORITY_HISTORY.LBL',
    showMenu: true,
    tabLabel: 'MENU.MASTER_DATA.TICKETING_AUTHORITY_HISTORY.LBL',
    path: 'ticketing-authority-history',
    permissionId: [Permissions.readTicketingAuthorityHistory],
    allowedUserTypes: [UserType.AIRLINE, UserType.GDS],
    parent: 'MASTER_DATA'
  },

  // ACDM menu items
  ADMS: {
    url: '/acdms/adm',
    label: 'ADM',
    showMenu: true,
    tabLabel: 'ADM',
    path: 'acdms/adm',
    permissionId: Permissions.protectedByChildren,
    parent: 'ACDMS'
  },
  ADM_QUERY: {
    url: '/acdms/adm/query',
    label: 'MENU.ACDMS.QUERY.adm',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.adm',
    path: 'adm/query',
    permissionId: getAdmQueryPermissions(),
    parent: 'ADMS'
  },
  ADNT_QUERY: {
    url: '/acdms/adnt/query',
    label: 'MENU.ACDMS.QUERY.adnt',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.adnt',
    path: 'adnt/query',
    permissionId: getViewAcdntPermission(MasterDataType.Adnt),
    parent: 'ADMS'
  },
  ADM_REQUEST_QUERY: {
    url: '/acdms/adm-request/query',
    label: 'MENU.ACDMS.QUERY.admq',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.admq',
    path: 'adm-request/query',
    permissionId: [viewAcdmPermission.admqView, commonAcdmPermission.admqIssue],
    parent: 'ADMS'
  },

  ACMS: {
    url: '/acdms/acm',
    label: 'ACM',
    showMenu: true,
    tabLabel: 'ACM',
    path: 'acdms/acm',
    permissionId: Permissions.protectedByChildren,
    parent: 'ACDMS'
  },
  ACM_QUERY: {
    url: '/acdms/acm/query',
    label: 'MENU.ACDMS.QUERY.acm',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.acm',
    path: 'acm/query',
    permissionId: getAcmQueryPermissions(),
    parent: 'ACMS'
  },
  ACNT_QUERY: {
    url: '/acdms/acnt/query',
    label: 'MENU.ACDMS.QUERY.acnt',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.acnt',
    path: 'acnt/query',
    permissionId: getViewAcdntPermission(MasterDataType.Acnt),
    parent: 'ACMS'
  },
  ACM_REQUEST_QUERY: {
    url: '/acdms/acm-request/query',
    label: 'MENU.ACDMS.QUERY.acmq',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.acmq',
    path: 'acm-request/query',
    permissionId: [viewAcdmPermission.acmqView, commonAcdmPermission.acmqIssue],
    parent: 'ACMS'
  },

  ADM_ISSUE: {
    url: '/acdms/adm/issue',
    label: 'MENU.ACDMS.ISSUE.adm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.ISSUE.adm',
    path: 'adm/issue',
    permissionId: commonAcdmPermission.admIssue,
    parent: 'ACDMS',
    persistent: true,
    showBadge: true
  },
  ADM_REQUEST_ISSUE: {
    url: '/acdms/adm-request/issue',
    label: 'MENU.ACDMS.ISSUE.admq',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.ISSUE.admq',
    path: 'adm-request/issue',
    permissionId: commonAcdmPermission.admqIssue,
    parent: 'ACDMS',
    persistent: true,
    showBadge: true
  },
  ACM_ISSUE: {
    url: '/acdms/acm/issue',
    label: 'MENU.ACDMS.ISSUE.acm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.ISSUE.acm',
    path: 'acm/issue',
    permissionId: commonAcdmPermission.acmIssue,
    parent: 'ACDMS',
    persistent: true,
    showBadge: true
  },
  ACM_REQUEST_ISSUE: {
    url: '/acdms/acm-request/issue',
    label: 'MENU.ACDMS.ISSUE.acmq',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.ISSUE.acmq',
    path: 'acm-request/issue',
    permissionId: commonAcdmPermission.acmqIssue,
    parent: 'ACDMS',
    persistent: true,
    showBadge: true
  },
  ADM_ISSUE_PENDING: {
    url: '/acdms/adm/issue-pending',
    label: 'MENU.ACDMS.ISSUE.adm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.ISSUE.adm',
    path: 'adm/issue-pending',
    permissionId: issuePendingSupervisionPermission.admSupIssue,
    parent: 'ACDMS',
    persistent: true,
    uniqueIdFromUrlRegex: '/adm/issue-pending',
    showBadge: true
  },
  ACM_ISSUE_PENDING: {
    url: '/acdms/acm/issue-pending',
    label: 'MENU.ACDMS.ISSUE.acm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.ISSUE.acm',
    path: 'acm/issue-pending',
    permissionId: issuePendingSupervisionPermission.acmSupIssue,
    parent: 'ACDMS',
    persistent: true,
    uniqueIdFromUrlRegex: '/acm/issue-pending',
    showBadge: true
  },
  ADM_VIEW: {
    url: '/acdms/adm/view',
    label: 'MENU.ACDMS.VIEW.adm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.adm',
    path: 'adm/view/:id',
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/adm/view/\\d+'
  },
  ADNT_VIEW: {
    url: '/acdms/adnt/view',
    label: 'MENU.ACDMS.VIEW.adnt',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.adnt',
    path: 'adnt/view/:id',
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/adnt/view/\\d+'
  },
  ADM_REQUEST_VIEW: {
    url: '/acdms/adm-request/view',
    label: 'MENU.ACDMS.VIEW.admq',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.admq',
    path: 'adm-request/view/:id',
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/adm-request/view/\\d+'
  },
  ACM_VIEW: {
    url: '/acdms/acm/view',
    label: 'MENU.ACDMS.VIEW.acm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.acm',
    path: 'acm/view/:id',
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/acm/view/\\d+'
  },
  ACNT_VIEW: {
    url: '/acdms/acnt/view',
    label: 'MENU.ACDMS.VIEW.acnt',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.acnt',
    path: 'acnt/view/:id',
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/acnt/view/\\d+'
  },
  ACM_REQUEST_VIEW: {
    url: '/acdms/acm-request/view',
    label: 'MENU.ACDMS.VIEW.acmq',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.acmq',
    path: 'acm-request/view/:id',
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/acm-request/view/\\d+'
  },
  ADM_EDIT_REACTIVATE: {
    url: '/acdms/adm-edit-reactivate',
    label: 'MENU.ACDMS.VIEW.adm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.adm',
    path: 'adm-edit-reactivate/:id',
    permissionId: null,
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/adm-edit-reactivate/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  ACM_EDIT_REACTIVATE: {
    url: '/acdms/acm-edit-reactivate',
    label: 'MENU.ACDMS.VIEW.acm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.acm',
    path: 'acm-edit-reactivate/:id',
    permissionId: null,
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/acm-edit-reactivate/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  ADM_EDIT_REJECT: {
    url: '/acdms/adm-edit-reject',
    label: 'MENU.ACDMS.VIEW.adm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.adm',
    path: 'adm-edit-reject/:id',
    permissionId: null,
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/adm-edit-reject/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  ACM_EDIT_REJECT: {
    url: '/acdms/acm-edit-reject',
    label: 'MENU.ACDMS.VIEW.acm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.acm',
    path: 'acm-edit-reject/:id',
    permissionId: null,
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/acm-edit-reject/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  ADM_SUPERVISION: {
    url: '/acdms/adm-supervision',
    label: 'MENU.ACDMS.VIEW.adm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.adm',
    path: 'adm-supervision/:id',
    permissionId: null,
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/adm-supervision/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  ACM_SUPERVISION: {
    url: '/acdms/acm-supervision',
    label: 'MENU.ACDMS.VIEW.acm',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.VIEW.acm',
    path: 'acm-supervision/:id',
    permissionId: null,
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/acm-supervision/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  // BSP Adjustments menu items
  BSP_ADJUSTMENTS: {
    url: '/bsp-adjustments',
    label: 'MENU.SPCDRS.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'bsp-adjustments',
    permissionId: Permissions.protectedByChildren,
    parent: 'ACDMS'
  },
  SPDR_QUERY: {
    url: '/bsp-adjustments/spdr/query',
    label: 'MENU.SPCDRS.QUERY.spdr',
    showMenu: true,
    tabLabel: 'MENU.SPCDRS.QUERY.spdr',
    path: 'spdr/query',
    permissionId: getAllSpdrQueryPermissions(),
    parent: 'BSP_ADJUSTMENTS'
  },
  SPCR_QUERY: {
    url: '/bsp-adjustments/spcr/query',
    label: 'MENU.SPCDRS.QUERY.spcr',
    showMenu: true,
    tabLabel: 'MENU.SPCDRS.QUERY.spcr',
    path: 'spcr/query',
    permissionId: getAllSpcrQueryPermissions(),
    parent: 'BSP_ADJUSTMENTS'
  },
  SPDR_ISSUE: {
    url: '/bsp-adjustments/spdr/issue',
    label: 'MENU.SPCDRS.ISSUE.spdr',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.ISSUE.spdr',
    path: 'spdr/issue',
    permissionId: [creationSpdrPermission.createSpdrAgn, creationSpdrPermission.createSpdrAirl],
    parent: 'BSP_ADJUSTMENTS',
    showBadge: true
  },
  SPCR_ISSUE: {
    url: '/bsp-adjustments/spcr/issue',
    label: 'MENU.SPCDRS.ISSUE.spcr',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.ISSUE.spcr',
    path: 'spcr/issue',
    permissionId: [creationSpcrPermission.createSpcrAgn, creationSpcrPermission.createSpcrAirl],
    parent: 'BSP_ADJUSTMENTS',
    showBadge: true
  },
  SPDR_VIEW: {
    url: '/bsp-adjustments/spdr/view',
    label: 'MENU.SPCDRS.VIEW.spdr',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.VIEW.spdr',
    path: 'spdr/view/:id',
    parent: 'BSP_ADJUSTMENTS',
    uniqueIdFromUrlRegex: '/spdr/view/\\d+'
  },
  SPCR_VIEW: {
    url: '/bsp-adjustments/spcr/view',
    label: 'MENU.SPCDRS.VIEW.spcr',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.VIEW.spcr',
    path: 'spcr/view/:id',
    parent: 'BSP_ADJUSTMENTS',
    uniqueIdFromUrlRegex: '/spcr/view/\\d+'
  },
  ADMD_QUERY: {
    url: '/bsp-adjustments/admd/query',
    label: 'MENU.SPCDRS.QUERY.admd',
    showMenu: true,
    tabLabel: 'MENU.SPCDRS.QUERY.admd',
    path: 'admd/query',
    permissionId: getAllAcmdQueryPermissions(),
    parent: 'BSP_ADJUSTMENTS'
  },
  ACMD_QUERY: {
    url: '/bsp-adjustments/acmd/query',
    label: 'MENU.SPCDRS.QUERY.acmd',
    showMenu: true,
    tabLabel: 'MENU.SPCDRS.QUERY.acmd',
    path: 'acmd/query',
    permissionId: getAllAdmdQueryPermissions(),
    parent: 'BSP_ADJUSTMENTS'
  },
  ADMD_ISSUE: {
    url: '/bsp-adjustments/admd/issue',
    label: 'MENU.SPCDRS.ISSUE.admd',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.ISSUE.admd',
    path: 'admd/issue',
    permissionId: [
      getAcdmdCreationAgentPermission(MasterDataType.Admd),
      getAcdmdCreationAirlPermission(MasterDataType.Admd)
    ],
    parent: 'BSP_ADJUSTMENTS',
    persistent: true,
    showBadge: true
  },
  ACMD_ISSUE: {
    url: '/bsp-adjustments/acmd/issue',
    label: 'MENU.SPCDRS.ISSUE.acmd',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.ISSUE.acmd',
    path: 'acmd/issue',
    permissionId: [
      getAcdmdCreationAgentPermission(MasterDataType.Acmd),
      getAcdmdCreationAirlPermission(MasterDataType.Acmd)
    ],
    parent: 'BSP_ADJUSTMENTS',
    persistent: true,
    showBadge: true
  },
  ADMD_VIEW: {
    url: '/bsp-adjustments/admd/view',
    label: 'MENU.SPCDRS.VIEW.admd',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.VIEW.admd',
    path: 'admd/view/:id',
    parent: 'BSP_ADJUSTMENTS',
    uniqueIdFromUrlRegex: '/admd/view/\\d+'
  },
  ACMD_VIEW: {
    url: '/bsp-adjustments/acmd/view',
    label: 'MENU.SPCDRS.VIEW.acmd',
    showMenu: false,
    tabLabel: 'MENU.SPCDRS.VIEW.acmd',
    path: 'acmd/view/:id',
    parent: 'BSP_ADJUSTMENTS',
    uniqueIdFromUrlRegex: '/acmd/view/\\d+'
  },

  ADM_POLICIES_QUERY: {
    url: '/acdms/adm/policies/query',
    label: 'MENU.ACDMS.QUERY.admPolicies',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.admPolicies',
    path: 'adm/policies/query',
    permissionId: getViewAdmPolicies(),
    parent: 'ACDMS'
  },
  GLOBAL_ADM_REPORTS_QUERY: {
    url: '/acdms/adm/global-adm-report/query',
    label: 'MENU.ACDMS.QUERY.globalADMReport.LBL',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.globalADMReport.TAB',
    path: 'adm/global-adm-report/query',
    permissionId: Permissions.readIataGlobalAdmReport,
    parent: 'ACDMS'
  },
  ADM_POLICY_GLOBAL_FILE: {
    url: '/acdms/adm-policy-global-file',
    label: 'MENU.ACDMS.admPolicyGlobalFile.LBL',
    showMenu: true,
    tabLabel: null,
    path: 'adm-policy-global-file',
    permissionId: createAdmGlobalPolicyFile,
    parent: 'ACDMS'
  },
  ACDM_FILES: {
    url: '/acdms/files',
    label: 'MENU.ACDMS.QUERY.acdmFiles',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.acdmFiles',
    path: 'files',
    permissionId: getAcdmFilesPermissions(),
    parent: 'ACDMS'
  },
  ACDM_DOCUMENT_ENQUIRY: {
    url: '/acdms/enquiry',
    label: 'MENU.ACDMS.QUERY.acdmEnquiryDocuments.label',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.acdmEnquiryDocuments.tabLabel',
    path: 'enquiry',
    permissionId: [
      Permissions.readAcdmSupervisionDocument,
      Permissions.readAcdmNotSupervisedDocument,
      Permissions.readAdjustmentDocuments
    ],
    parent: 'ACDMS'
  },
  ACDM_DOCUMENT_ENQUIRY_LIST: {
    url: '/acdms/documents',
    label: 'MENU.ACDMS.QUERY.acdmEnquiryDocuments.label',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.QUERY.acdmEnquiryDocuments.tabLabel',
    path: 'documents/:documentNumber',
    permissionId: [
      Permissions.readAcdmSupervisionDocument,
      Permissions.readAcdmNotSupervisedDocument,
      Permissions.readAdjustmentDocuments
    ],
    parent: 'ACDMS'
  },
  ACDM_CATEGORIZATION_REASONS: {
    url: '/acdms/categorization-reasons',
    label: 'MENU.ACDMS.QUERY.acdmReasons',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.acdmReasons',
    path: 'categorization-reasons',
    permissionId: [Permissions.readAdmCategorizationReason],
    parent: 'ACDMS'
  },
  ACDM_CATEGORIZATION_RULES: {
    url: '/acdms/categorization-rules',
    label: 'MENU.ACDMS.QUERY.acdmRules',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.acdmRules',
    path: 'categorization-rules',
    permissionId: getAcdmCategorizationRules(),
    allowedUserTypes: [UserType.AIRLINE, UserType.IATA],
    parent: 'ACDMS'
  },
  ACDM_CATEGORIZATION_RULES_CREATE: {
    url: '/acdms/categorization-rules/rule-detail',
    label: 'MENU.ACDMS.QUERY.acdmRuleDetail',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.QUERY.acdmRuleDetail',
    path: 'categorization-rules/rule-detail',
    permissionId: getAcdmCategorizationRules(),
    allowedUserTypes: [UserType.AIRLINE, UserType.IATA],
    parent: 'ACDMS',
    persistent: true,
    showBadge: true
  },
  ACDM_CATEGORIZATION_RULES_EDIT: {
    url: '/acdms/categorization-rules/rule-edit',
    label: 'MENU.ACDMS.QUERY.acdmRuleDetail',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.QUERY.acdmRuleDetail',
    path: 'categorization-rules/rule-edit/:id',
    permissionId: getUpdateAcdmCategorizationRules(),
    parent: 'ACDMS',
    persistent: true,
    showBadge: true,
    uniqueIdFromUrlRegex: '/categorization-rules/rule-edit/\\d+'
  },
  ACDM_CATEGORIZATION_RULES_VIEW: {
    url: '/acdms/categorization-rules/view',
    label: 'MENU.ACDMS.QUERY.acdmRuleView',
    showMenu: false,
    tabLabel: 'MENU.ACDMS.QUERY.acdmRuleView',
    path: 'categorization-rules/view/:id',
    permissionId: getAcdmCategorizationRules(),
    parent: 'ACDMS',
    uniqueIdFromUrlRegex: '/acdms/categorization-rules/view/\\d+'
  },
  FORWARDED_ADM_GLOBAL_FILE: {
    url: '/acdms/adm/forwarded-adm-global-file',
    label: 'MENU.ACDMS.QUERY.forwardedAdmGlobalFile',
    showMenu: true,
    tabLabel: 'MENU.ACDMS.QUERY.forwardedAdmGlobalFile',
    path: 'adm/forwarded-adm-global-file',
    permissionId: Permissions.readForwardedAdmGlobalFiles,
    parent: 'ACDMS',
    allowedUserTypes: UserType.GDS
  },

  // Refunds menu items
  REFUNDS_APP_ISSUE: {
    url: '/refunds/app/issue',
    label: 'MENU.REFUNDS.ISSUE.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.ISSUE.app',
    path: 'app/issue',
    permissionId: createRefundPermission.raIssue,
    parent: 'REFUNDS',
    persistent: true,
    showBadge: true
  },
  REFUNDS_APP_ISSUE_PENDING: {
    url: '/refunds/app/issue-pending',
    label: 'MENU.REFUNDS.ISSUE.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.ISSUE.app',
    path: 'app/issue-pending',
    permissionId: createRefundPermissionUnderSupervision.raIssue,
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/issue-pending',
    persistent: true,
    showBadge: true
  },
  REFUNDS_APP_ISSUE_TICKET: {
    url: '/refunds/app/issue-ticket',
    label: 'MENU.REFUNDS.ISSUE_TICKET.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.ISSUE_TICKET.app',
    path: 'app/issue-ticket',
    permissionId: [createRefundPermissionUnderSupervision.raIssue, createRefundPermission.raIssue],
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/issue-ticket',
    persistent: true,
    showBadge: true
  },
  REFUNDS_APP_VIEW: {
    url: '/refunds/app/view',
    label: 'MENU.REFUNDS.VIEW.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.VIEW.app',
    path: 'app/view/:id',
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/view/\\d+'
  },
  REFUNDS_APP_EDIT: {
    url: '/refunds/app/edit',
    label: 'MENU.REFUNDS.EDIT.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.EDIT.app',
    path: 'app/edit/:id',
    permissionId: modifyRefundPermission.raModify,
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/edit/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  REFUNDS_APP_SUPERVISE: {
    url: '/refunds/app/supervise',
    label: 'MENU.REFUNDS.SUPERVISE.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.SUPERVISE.app',
    path: 'app/supervise/:id',
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/supervise/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  REFUNDS_APP_INVESTIGATE: {
    url: '/refunds/app/investigate',
    label: 'MENU.REFUNDS.INVESTIGATE.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.INVESTIGATE.app',
    path: 'app/investigate/:id',
    permissionId: updateRefundPermission.raUpdate,
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/investigate/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  REFUNDS_APP_AUTHORIZE: {
    url: '/refunds/app/authorize',
    label: 'MENU.REFUNDS.AUTHORIZE.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.AUTHORIZE.app',
    path: 'app/authorize/:id',
    permissionId: updateRefundPermission.raUpdate,
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/authorize/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  REFUNDS_APP_RESUBMISSION: {
    url: '/refunds/app/resubmission',
    label: 'MENU.REFUNDS.RESUBMISSION.app',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.RESUBMISSION.app',
    path: 'app/resubmission/:id',
    permissionId: createRefundPermission.raIssue,
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/app/resubmission/\\d+',
    persistent: true,
    showBadge: true
  },
  REFUNDS_NOTICE_VIEW: {
    url: '/refunds/notice/view',
    label: 'MENU.REFUNDS.VIEW.notice',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.VIEW.notice',
    path: 'notice/view/:id',
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/notice/view/\\d+'
  },
  REFUNDS_APP_QUERY: {
    url: '/refunds/app/query',
    label: 'MENU.REFUNDS.QUERY.app',
    showMenu: true,
    tabLabel: 'MENU.REFUNDS.QUERY.app',
    path: 'app/query',
    permissionId: getRaRnMenuItemPermissions(MasterDataType.RefundApp),
    parent: 'REFUNDS'
  },
  REFUNDS_NOTICE_ISSUE: {
    url: '/refunds/notice/issue',
    label: 'MENU.REFUNDS.ISSUE.notice',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.ISSUE.notice',
    path: 'notice/issue',
    permissionId: createRefundPermission.rnIssue,
    parent: 'REFUNDS',
    persistent: true,
    showBadge: true
  },
  REFUNDS_NOTICE_ISSUE_PENDING: {
    url: '/refunds/notice/issue-pending',
    label: 'MENU.REFUNDS.ISSUE.notice',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.ISSUE.notice',
    path: 'notice/issue-pending',
    permissionId: createRefundPermissionUnderSupervision.rnIssue,
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/notice/issue-pending',
    persistent: true,
    showBadge: true
  },
  REFUNDS_NOTICE_ISSUE_TICKET: {
    url: '/refunds/notice/issue-ticket',
    label: 'MENU.REFUNDS.ISSUE_TICKET.notice',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.ISSUE_TICKET.notice',
    path: 'notice/issue-ticket',
    permissionId: [createRefundPermissionUnderSupervision.rnIssue, createRefundPermission.rnIssue],
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/notice/issue-ticket',
    persistent: true,
    showBadge: true
  },
  REFUNDS_NOTICE_EDIT: {
    url: '/refunds/notice/edit',
    label: 'MENU.REFUNDS.EDIT.notice',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.EDIT.notice',
    path: 'notice/edit/:id',
    permissionId: modifyRefundPermission.rnModify,
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/notice/edit/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  REFUNDS_NOTICE_SUPERVISE: {
    url: '/refunds/notice/supervise',
    label: 'MENU.REFUNDS.SUPERVISE.notice',
    showMenu: false,
    tabLabel: 'MENU.REFUNDS.SUPERVISE.notice',
    path: 'notice/supervise/:id',
    parent: 'REFUNDS',
    uniqueIdFromUrlRegex: '/notice/supervise/\\d+',
    persistent: true,
    tooltipLabel: 'tab.tooltip_edit',
    showBadge: true
  },
  REFUNDS_NOTICE_QUERY: {
    url: '/refunds/notice/query',
    label: 'MENU.REFUNDS.QUERY.notice',
    showMenu: true,
    tabLabel: 'MENU.REFUNDS.QUERY.notice',
    path: 'notice/query',
    permissionId: getRaRnMenuItemPermissions(MasterDataType.RefundNotice),
    parent: 'REFUNDS'
  },
  REFUNDS_FILES: {
    url: '/refunds/app/files',
    label: 'MENU.REFUNDS.QUERY.files',
    showMenu: true,
    tabLabel: 'MENU.REFUNDS.QUERY.files',
    path: 'app/files',
    permissionId: getRefundsFilesPermissions(),
    parent: 'REFUNDS'
  },
  DOCUMENT_ENQUIRY: {
    url: '/documents/enquiry',
    label: 'MENU.documents.documentEnquiry.label',
    showMenu: true,
    tabLabel: 'MENU.documents.documentEnquiry.tabLabel',
    path: 'enquiry',
    permissionId: Permissions.readDocumentDetails,
    parent: 'DOCUMENTS'
  },
  DOCUMENT_VIEW_NOT_FOUND: {
    url: '/documents/view/not-found',
    label: 'MENU.documents.documentNotFound.label',
    tabLabel: 'MENU.documents.documentNotFound.tabLabel',
    showMenu: false,
    path: 'view/not-found/:documentNumber',
    permissionId: Permissions.readDocumentDetails,
    parent: 'DOCUMENTS',
    uniqueIdFromUrlRegex: '/view/not-found/\\d+'
  },
  DOCUMENT_VIEW: {
    url: '/documents/view',
    showMenu: false,
    path: 'view',
    permissionId: Permissions.readDocumentDetails,
    parent: 'DOCUMENTS',
    uniqueIdFromUrlRegex: 'view/[a-zA-Z0-9-]+'
  },
  REJECTED_DOCUMENTS: {
    url: '/documents/rejected',
    label: 'MENU.documents.rejectedDocuments.label',
    tabLabel: 'MENU.documents.rejectedDocuments.tabLabel',
    showMenu: true,
    path: 'rejected',
    permissionId: Permissions.readRejectedDocuments,
    parent: 'DOCUMENTS'
  },
  CREDIT_CARD_DOCUMENTS: {
    url: '/documents/credit-card',
    label: 'MENU.documents.creditCardDocuments.label',
    tabLabel: 'MENU.documents.creditCardDocuments.tabLabel',
    showMenu: true,
    path: 'credit-card',
    permissionId: Permissions.readCreditCardDocuments,
    parent: 'DOCUMENTS'
  },
  SEARCH_BY_PAYMENT_CARD: {
    url: '/documents/credit-card/search-by-payment-card',
    label: 'MENU.documents.searchByPaymentCard.label',
    tabLabel: 'MENU.documents.searchByPaymentCard.tabLabel',
    showMenu: true,
    path: 'credit-card/search-by-payment-card',
    permissionId: Permissions.readCreditCardDocuments,
    parent: 'CREDIT_CARD_DOCUMENTS'
  },
  DOCUMENTS_SEARCH: {
    url: '/documents/credit-card/documents-search',
    label: 'MENU.documents.documentsSearch.label',
    tabLabel: 'MENU.documents.documentsSearch.tabLabel',
    showMenu: true,
    path: 'credit-card/documents-search',
    permissionId: Permissions.readCreditCardDocuments,
    parent: 'CREDIT_CARD_DOCUMENTS'
  },
  NET_REMIT_DOCUMENTS: {
    url: '/documents/net-remit',
    label: 'MENU.documents.netRemitDocuments.label',
    tabLabel: 'MENU.documents.netRemitDocuments.tabLabel',
    showMenu: true,
    path: 'net-remit',
    permissionId: Permissions.readNetRemitDocuments,
    parent: 'DOCUMENTS'
  },

  // DOCUMENTS
  // Post Billing Dispute
  POST_BILLING_DISPUTE_LIST: {
    url: '/pbd',
    label: 'MENU.documents.postBillingDispute.label',
    showMenu: true,
    tabLabel: 'MENU.documents.postBillingDispute.tabLabel',
    path: 'pbd',
    permissionId: [Permissions.readPbd, Permissions.readPendingPbds, Permissions.pbdStart],
    parent: 'DOCUMENTS'
  },
  POST_BILLING_DISPUTE_ISSUE: {
    url: '/pbd/issue',
    label: 'MENU.documents.postBillingDisputeDetails.label',
    showMenu: false,
    tabLabel: 'MENU.documents.postBillingDisputeIssue.tabLabel',
    path: 'issue',
    permissionId: Permissions.pbdStart,
    parent: 'DOCUMENTS',
    persistent: true,
    showBadge: true
  },
  POST_BILLING_DISPUTE_DETAILS: {
    url: '/pbd/',
    label: 'MENU.documents.postBillingDisputeDetails.label',
    showMenu: false,
    tabLabel: 'MENU.documents.postBillingDisputeDetails.tabLabel',
    path: ':id',
    permissionId: [Permissions.pbdStart, Permissions.readPbd, Permissions.readPendingPbds],
    parent: 'DOCUMENTS'
  },

  // Stock Application
  STOCK_REQUEST_LIST: {
    url: '/documents/stock-request-list',
    label: 'MENU.documents.stockRequestList.label',
    showMenu: true,
    tabLabel: 'MENU.documents.stockRequestList.tabLabel',
    path: 'stock-request-list',
    permissionId: [Permissions.readStockApplicationRequest, Permissions.createStockApplicationRequest],
    allowedUserTypes: [UserType.AGENT, UserType.AGENT_GROUP, UserType.IATA],
    parent: 'DOCUMENTS'
  },
  STOCK_APPLICATION_REQUEST: {
    url: '/documents/stock-request',
    label: 'MENU.documents.stockRequest.label',
    showMenu: false,
    showBadge: true,
    tabLabel: 'MENU.documents.stockRequest.tabLabel',
    path: 'stock-request',
    allowedUserTypes: [UserType.AGENT, UserType.AGENT_GROUP],
    permissionId: [Permissions.createStockApplicationRequest],
    parent: 'DOCUMENTS'
  },

  // FILES
  REPORTS_COMMUNICATIONS: {
    url: '/files/reports-communications',
    label: 'MENU.files.reports.label',
    showMenu: true,
    tabLabel: 'MENU.files.reports.tabLabel',
    path: 'reports-communications',
    permissionId: Permissions.protectedByChildren,
    parent: 'FILES'
  },
  REPORTS: {
    url: '/files/reports-communications/reports',
    label: 'MENU.files.reports.label',
    showMenu: true,
    tabLabel: 'MENU.files.reports.tabLabel',
    path: 'reports-communications/reports',
    permissionId: [Permissions.readFile, Permissions.readCommunicationsFile, Permissions.archiveFile],
    parent: 'REPORTS_COMMUNICATIONS'
  },
  EVALUATION_FILES: {
    url: '/files/reports-communications/evaluations-files',
    label: 'MENU.files.evaluation.title',
    showMenu: true,
    tabLabel: 'MENU.files.evaluation.title',
    path: 'reports-communications/evaluations-files',
    permissionId: [Permissions.readEvaluationDailyDownloads],
    parent: 'REPORTS_COMMUNICATIONS'
  },
  GLOBAL_TA_FILES: {
    url: '/files/reports-communications/global-ta-files',
    label: 'MENU.files.globalTa.title',
    showMenu: true,
    tabLabel: 'MENU.files.globalTa.title',
    path: 'reports-communications/global-ta-files',
    permissionId: [
      Permissions.readGlobalTicketingAuthority,
      Permissions.createGlobalTicketingAuthority,
      Permissions.readLastGlobalTicketingAuthority,
      Permissions.createNonTicketingAuthority,
      Permissions.createTicketingAuthorityStatus
    ],
    parent: 'REPORTS_COMMUNICATIONS'
  },
  FOP_AGENT_STATUS_REQUEST: {
    url: '/files/fop-agent-status-request',
    label: 'MENU.files.fopAgentStatusRequest.label',
    showMenu: true,
    tabLabel: null,
    path: 'fop-agent-status-request',
    permissionId: [Permissions.requestFopAgentStatus],
    parent: 'REPORTS_COMMUNICATIONS'
  },
  UPLOAD: {
    url: '/files/upload',
    label: 'MENU.files.upload.title',
    showMenu: true,
    tabLabel: null,
    path: 'upload',
    permissionId: Permissions.protectedByChildren,
    parent: 'FILES'
  },
  UPLOAD_FILES: {
    url: '/files/upload-files',
    label: 'MENU.files.uploadFiles.title',
    showMenu: true,
    tabLabel: null,
    path: 'upload-files',
    permissionId: [
      Permissions.uploadFile,
      Permissions.uploadWebLinkFile,
      Permissions.uploadAdmAcmFile,
      Permissions.uploadRefundNoticeFile,
      Permissions.uploadRefundApplicationFile,
      Permissions.uploadAdmCategorizationFile,
      Permissions.uploadPaymentCardFile,
      Permissions.uploadHotConversationFile,
      Permissions.uploadAgentGdsFile,
      Permissions.uploadRemitFile,
      Permissions.uploadAcmFile,
      Permissions.uploadEvFile,
      Permissions.uploadPnrFile
    ],
    parent: 'UPLOAD'
  },
  UPLOAD_COMMUNICATIONS: {
    url: '/files/upload-communications',
    label: 'MENU.files.uploadCommunications.title',
    showMenu: true,
    tabLabel: null,
    path: 'upload-communications',
    permissionId: [Permissions.uploadCommunicationsFile],
    parent: 'UPLOAD'
  },
  DELETED_FILES: {
    url: '/files/deleted-files',
    label: 'MENU.files.deletedFiles.title',
    showMenu: true,
    tabLabel: 'MENU.files.deletedFiles.title',
    path: 'deleted-files',
    permissionId: [Permissions.readDeletedFiles, Permissions.createDeletedFiles],
    parent: 'UPLOAD'
  },
  UPLOADED_FILES: {
    url: '/files/uploaded-files',
    label: 'MENU.files.uploadedFiles.title',
    showMenu: true,
    tabLabel: 'MENU.files.uploadedFiles.title',
    path: 'uploaded-files',
    permissionId: [Permissions.readUploadedFiles],
    parent: 'FILES'
  },
  DOWNLOADED_FILES: {
    url: '/files/downloaded-files',
    label: 'MENU.files.downloadedFiles.title',
    showMenu: true,
    tabLabel: 'MENU.files.downloadedFiles.title',
    path: 'downloaded-files',
    permissionId: [Permissions.readDownloadedFiles],
    parent: 'FILES'
  },
  NOT_DOWNLOADED_FILES: {
    url: '/files/not-downloaded-files',
    label: 'MENU.files.notDownloadedFiles.title',
    showMenu: true,
    tabLabel: 'MENU.files.notDownloadedFiles.title',
    path: 'not-downloaded-files',
    permissionId: [Permissions.readNotDownloadedFiles],
    parent: 'FILES'
  },
  SPECIAL_FILES: {
    url: '/files/special-files',
    label: 'MENU.files.specialFiles.title',
    showMenu: true,
    tabLabel: 'MENU.files.specialFiles.title',
    path: 'special-files',
    allowedUserTypes: [UserType.IATA],
    permissionId: [Permissions.readSpecialFiles],
    parent: 'FILES'
  },
  DOWNLOADED_IATA_COMMUNICATIONS: {
    url: '/files/downloaded-iata-communications',
    label: 'MENU.files.downloadedIataCommunications.title',
    showMenu: true,
    tabLabel: 'MENU.files.downloadedIataCommunications.title',
    path: 'downloaded-iata-communications',
    allowedUserTypes: [UserType.IATA],
    permissionId: [Permissions.readDownloadedIataCommunications],
    parent: 'FILES'
  },
  AGENT_REPORTS_PARAMETERS: {
    url: '/files/agent-reports/parameters',
    label: 'MENU.files.agentReports.title',
    showMenu: true,
    tabLabel: 'MENU.files.agentReports.title',
    path: 'agent-reports/parameters',
    permissionId: [Permissions.readAgentReports],
    parent: 'FILES'
  },
  AGENT_REPORTS: {
    url: '/files/agent-reports',
    label: 'MENU.files.agentReports.title',
    showMenu: false,
    tabLabel: 'MENU.files.agentReports.title',
    path: 'agent-reports',
    permissionId: [Permissions.readAgentReports],
    parent: 'FILES'
  },
  PROCESSED_HOTS: {
    url: '/files/processed-hots',
    label: 'MENU.files.processedHots.label',
    showMenu: true,
    tabLabel: 'MENU.files.processedHots.label',
    path: 'processed-hots',
    allowedUserTypes: [UserType.IATA, UserType.DPC],
    permissionId: [Permissions.readFileHot],
    parent: 'FILES'
  },

  // Sales Query
  BILLING_STATEMENT: {
    url: '/sales/billing-statement',
    label: 'MENU.sales.billingStatement.label',
    showMenu: true,
    tabLabel: 'MENU.sales.billingStatement.label',
    path: 'billing-statement',
    permissionId: Permissions.readBillingStatement,
    parent: 'SALES'
  },
  BILLING_ANALYSIS: {
    url: '/sales/billing-analysis',
    label: 'MENU.sales.billingAnalysis.label',
    showMenu: false,
    tabLabel: 'MENU.sales.billingAnalysis.label',
    path: 'billing-analysis',
    permissionId: Permissions.readBillingAnalysis,
    parent: 'SALES'
  },
  BILLING_ANALYSIS_SETUP: {
    url: '/sales/billing-analysis/setup',
    label: 'MENU.sales.billingAnalysis.label',
    showMenu: true,
    path: 'billing-analysis/setup',
    permissionId: Permissions.readBillingAnalysis,
    parent: 'SALES'
  },
  COMPARATIVE: {
    url: '/sales/comparative',
    label: 'MENU.sales.comparatives.label',
    tabLabel: 'MENU.sales.comparatives.label',
    showMenu: true,
    path: 'comparative',
    permissionId: Permissions.readComparatives,
    allowedUserTypes: UserType.AIRLINE,
    parent: 'SALES'
  },
  ANNUAL_ACCUMULATED: {
    url: '/sales/annual-accumulated',
    label: 'MENU.sales.annualAccumulated.label',
    tabLabel: 'MENU.sales.annualAccumulated.label',
    showMenu: true,
    path: 'annual-accumulated',
    permissionId: Permissions.readAnnualAccumulated,
    allowedUserTypes: UserType.AIRLINE,
    parent: 'SALES'
  },
  NON_COMPARATIVE: {
    url: '/sales/non-comparative',
    label: 'MENU.sales.nonComparatives.label',
    tabLabel: 'MENU.sales.nonComparatives.label',
    showMenu: true,
    path: 'non-comparative',
    permissionId: Permissions.readNonComparatives,
    parent: 'SALES'
  },
  SCU_SUMMARY: {
    url: '/sales/scu',
    label: 'MENU.sales.scuSummary.label',
    tabLabel: 'MENU.sales.scuSummary.label',
    showMenu: true,
    path: 'scu',
    permissionId: Permissions.readScuSummary,
    parent: 'SALES'
  },
  ADVANCED_PAYMENT: {
    url: '/sales/advanced-payment',
    label: 'MENU.sales.advancedPayment.label',
    tabLabel: 'MENU.sales.advancedPayment.label',
    showMenu: true,
    path: 'advanced-payment',
    allowedUserTypes: [UserType.AGENT, UserType.IATA, UserType.AGENT_GROUP],
    permissionId: [Permissions.readAdvancePayment, Permissions.createAdvancePayment],
    parent: 'SALES'
  },
  ADVANCED_PAYMENT_REQUEST: {
    url: '/sales/advanced-payment-request',
    label: 'MENU.sales.advancedPaymentRequest.label',
    tabLabel: 'MENU.sales.advancedPaymentRequest.label',
    showMenu: false,
    showBadge: true,
    path: 'advanced-payment-request',
    allowedUserTypes: [UserType.AGENT, UserType.IATA, UserType.AGENT_GROUP],
    permissionId: Permissions.createAdvancePayment,
    parent: 'SALES'
  },
  ADVANCED_PAYMENT_VIEW: {
    url: '/sales/advanced-payment',
    label: 'MENU.sales.advancedPayment.label',
    tabLabel: 'MENU.sales.advancedPayment.label',
    showMenu: false,
    path: 'advanced-payment/:id',
    uniqueIdFromUrlRegex: '/advanced-payment/\\d+',
    allowedUserTypes: [UserType.AGENT, UserType.IATA, UserType.AGENT_GROUP],
    permissionId: [Permissions.readAdvancePayment],
    parent: 'SALES'
  },

  // TIP
  TIP_PRODUCTS: {
    url: '/tip/products',
    label: 'MENU.TIP.products.label',
    showMenu: true,
    tabLabel: 'MENU.TIP.products.tabLabel',
    path: 'products',
    permissionId: getReadTipProductPermissions(),
    allowedUserTypes: [UserType.AIRLINE, UserType.AGENT],
    parent: 'TIP'
  },
  TIP_CARDS: {
    url: '/tip/cards',
    label: 'MENU.TIP.cards.label',
    showMenu: true,
    tabLabel: 'MENU.TIP.cards.tabLabel',
    path: 'cards',
    permissionId: getReadTipCardPermissions(),
    allowedUserTypes: [UserType.AIRLINE, UserType.AGENT],
    parent: 'TIP'
  },
  TIP_PROVIDER_DETAIL: {
    url: '/tip/providers/view',
    label: 'MENU.TIP.providers.detail.label',
    tabLabel: 'MENU.TIP.providers.detail.tabLabel',
    showMenu: false,
    path: 'providers/view/:id',
    uniqueIdFromUrlRegex: '/providers/view/\\d+',
    permissionId: getReadTipProductPermissions(),
    allowedUserTypes: [UserType.AIRLINE, UserType.AGENT],
    parent: 'TIP'
  },
  TIP_PRODUCT_DETAIL: {
    url: '/tip/products/view',
    label: 'MENU.TIP.products.detail.label',
    tabLabel: 'MENU.TIP.products.detail.tabLabel',
    showMenu: false,
    path: 'products/view/:id',
    uniqueIdFromUrlRegex: '/products/view/\\d+',
    permissionId: getReadTipProductPermissions(),
    allowedUserTypes: [UserType.AIRLINE, UserType.AGENT],
    parent: 'TIP'
  },
  TIP_CARDS_AIRLINE_DETAIL: {
    url: '/tip/cards/airline/view',
    label: 'MENU.TIP.cardsAirlineProfile.detail.label',
    tabLabel: 'MENU.TIP.cardsAirlineProfile.detail.tabLabel',
    showMenu: false,
    path: 'cards/airline/view/:airline-id',
    uniqueIdFromUrlRegex: '/cards/airline/view/\\d+',
    permissionId: Permissions.readAirline,
    parent: 'TIP'
  },
  TIP_GLOBAL_FILES: {
    url: '/tip/global-files',
    label: 'MENU.TIP.globalFiles.label',
    showMenu: true,
    path: 'global-files',
    permissionId: Permissions.protectedByChildren,
    allowedUserTypes: [UserType.IATA],
    parent: 'TIP'
  },
  TIP_GLOBAL_FILES_CONSENT: {
    url: '/tip/consent-file',
    label: 'MENU.TIP.globalFiles.consentLabel',
    showMenu: true,
    tabLabel: 'MENU.TIP.globalFiles.consentLabel',
    path: 'consent-file',
    permissionId: [Permissions.createGlobalConsentFile],
    allowedUserTypes: [UserType.IATA],
    parent: 'TIP_GLOBAL_FILES'
  },
  TIP_GLOBAL_FILES_REPORT: {
    url: '/tip/report-file',
    label: 'MENU.TIP.globalFiles.reportLabel',
    showMenu: true,
    path: 'report-file',
    permissionId: [Permissions.createGlobalReportFile],
    allowedUserTypes: [UserType.IATA],
    parent: 'TIP_GLOBAL_FILES'
  },
  TIP_REPORTS_CONFIGURATION: {
    url: '/tip/reports-configuration',
    label: 'MENU.TIP.reportsConfiguration.label',
    showMenu: true,
    tabLabel: 'MENU.TIP.reportsConfiguration.label',
    path: 'reports-configuration',
    permissionId: Permissions.readTipReport,
    allowedUserTypes: [UserType.IATA],
    parent: 'TIP'
  },
  // TIP for IATA
  GLOBAL_ENROLLED_ATMS: {
    url: '/tip/global-enrolled-atms',
    label: 'MENU.TIP.globalEnrolledAtms.label',
    showMenu: true,
    tabLabel: null,
    path: 'global-enrolled-atms',
    permissionId: Permissions.protectedByChildren,
    allowedUserTypes: [UserType.IATA],
    parent: 'TIP'
  },
  TIP_PROVIDERS_AND_PRODUCTS: {
    url: '/tip/providers-products',
    label: 'MENU.TIP.products.label',
    showMenu: true,
    tabLabel: 'MENU.TIP.products.tabLabel',
    path: 'providers-products',
    permissionId: getReadTipProductListPermissions(),
    allowedUserTypes: [UserType.IATA],
    parent: 'GLOBAL_ENROLLED_ATMS'
  },
  TIP_CARDS_ENROLLED_ATMS: {
    url: '/tip/global-enrolled-atms/agent-own-cards',
    label: 'MENU.TIP.cards.label',
    showMenu: true,
    tabLabel: 'MENU.TIP.cards.tabLabel',
    path: 'global-enrolled-atms/agent-own-cards',
    permissionId: getReadTipCardPermissions(),
    allowedUserTypes: [UserType.IATA],
    parent: 'GLOBAL_ENROLLED_ATMS'
  },

  //WECHAT
  WECHAT_LIST: {
    url: '/wechat/wechat-list',
    label: 'MENU.WECHAT.tabLabel',
    showMenu: false,
    tabLabel: 'MENU.WECHAT.tabLabel',
    path: 'wechat-list',
    permissionId: Permissions.readWechat,
    allowedUserTypes: [UserType.AGENT, UserType.AIRLINE],
    parent: 'WECHAT'
  },
  WECHAT_SETTINGS: {
    url: '/wechat/settings',
    label: 'MENU.WECHAT.settings.tabLabel',
    showMenu: false,
    tabLabel: 'MENU.WECHAT.settings.tabLabel',
    path: 'settings/:user-id/:reg-number',
    uniqueIdFromUrlRegex: '/settings/\\d+',
    permissionId: Permissions.readWechat,
    allowedUserTypes: [UserType.AGENT, UserType.AIRLINE],
    parent: 'WECHAT',
    showBadge: true
  },
  // TODO: the creation of the routing tree must have the parent module on path (for example reports/ + files/ + another) to the correct construction of the path

  // The Old User management

  USER_MANAGEMENT: {
    url: '/user-management',
    label: 'MENU.USER_MANAGEMENT.LBL',
    showMenu: false,
    tabLabel: null,
    path: 'user-management',
    permissionId: Permissions.forbidden,
    parent: null
  },

  IATA_USERS: {
    url: '/user-management/iata-users',
    label: 'MENU.USER_MANAGEMENT.IATA_USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USER_MANAGEMENT.IATA_USERS.LBL',
    path: 'iata-users',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  AIRLINE_USERS: {
    url: '/user-management/airline-users',
    label: 'MENU.USER_MANAGEMENT.AIRLINE_USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USER_MANAGEMENT.AIRLINE_USERS.LBL',
    path: 'airline-users',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  AGENT_USERS: {
    url: '/user-management/agent-users',
    label: 'MENU.USER_MANAGEMENT.AGENT_USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USER_MANAGEMENT.AGENT_USERS.LBL',
    path: 'agent-users',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  GDS_USERS: {
    url: '/user-management/gds-users',
    label: 'MENU.USER_MANAGEMENT.GDS_USERS.LBL',
    showMenu: true,
    tabLabel: 'MENU.USER_MANAGEMENT.GDS_USERS.LBL',
    path: 'gds-users',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  EDIT_IATA_USER: {
    url: '/user-management/iata-users/edit',
    label: 'MENU.USER_MANAGEMENT.EDIT_IATA_USER.LBL',
    showMenu: false,
    tabLabel: 'MENU.USER_MANAGEMENT.EDIT_IATA_USER.LBL',
    path: 'iata-users/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  EDIT_AGENT_USER: {
    url: '/user-management/agent-users/edit',
    label: 'MENU.USER_MANAGEMENT.EDIT_AGENT_USER.LBL',
    showMenu: false,
    tabLabel: 'MENU.USER_MANAGEMENT.EDIT_AGENT_USER.LBL',
    path: 'agent-users/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  EDIT_AIRLINE_USER: {
    url: '/user-management/airline-users/edit',
    label: 'MENU.USER_MANAGEMENT.EDIT_AIRLINE_USER.LBL',
    showMenu: false,
    tabLabel: 'MENU.USER_MANAGEMENT.EDIT_AIRLINE_USER.LBL',
    path: 'airline-users/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  EDIT_GDS_USER: {
    url: '/user-management/gds-users/edit',
    label: 'MENU.USER_MANAGEMENT.EDIT_GDS_USER.LBL',
    showMenu: false,
    tabLabel: 'MENU.USER_MANAGEMENT.EDIT_GDS_USER.LBL',
    path: 'gds-users/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  GROUP_MANAGEMENT: {
    url: '/group-management',
    label: 'MENU.GROUP_MANAGEMENT.LBL',
    showMenu: true,
    tabLabel: 'MENU.GROUP_MANAGEMENT.LBL',
    path: 'group-management',
    permissionId: Permissions.forbidden,
    parent: 'USER_MANAGEMENT'
  },

  IATA_GROUPS: {
    url: '/group-management/iata-groups',
    label: 'MENU.GROUP_MANAGEMENT.IATA_GROUPS.LBL',
    showMenu: true,
    tabLabel: 'MENU.GROUP_MANAGEMENT.IATA_GROUPS.LBL',
    path: 'iata-groups',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },

  AIRLINE_GROUPS: {
    url: '/group-management/airline-groups',
    label: 'MENU.GROUP_MANAGEMENT.AIRLINE_GROUPS.LBL',
    showMenu: true,
    tabLabel: 'MENU.GROUP_MANAGEMENT.AIRLINE_GROUPS.LBL',
    path: 'airline-groups',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },

  AGENT_GROUPS: {
    url: '/group-management/agent-groups',
    label: 'MENU.GROUP_MANAGEMENT.AGENT_GROUPS.LBL',
    showMenu: true,
    tabLabel: 'MENU.GROUP_MANAGEMENT.AGENT_GROUPS.LBL',
    path: 'agent-groups',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },

  GDS_GROUPS: {
    url: '/group-management/gds-groups',
    label: 'MENU.GROUP_MANAGEMENT.GDS_GROUPS.LBL',
    showMenu: true,
    tabLabel: 'MENU.GROUP_MANAGEMENT.GDS_GROUPS.LBL',
    path: 'gds-groups',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },

  EDIT_IATA_GROUP: {
    url: '/group-management/iata-groups/edit',
    label: 'MENU.GROUP_MANAGEMENT.EDIT_IATA_GROUP.LBL',
    showMenu: false,
    tabLabel: 'MENU.GROUP_MANAGEMENT.EDIT_IATA_GROUP.LBL',
    path: 'iata-groups/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },

  EDIT_AGENT_GROUP: {
    url: '/group-management/agent-groups/edit',
    label: 'MENU.GROUP_MANAGEMENT.EDIT_AGENT_GROUP.LBL',
    showMenu: false,
    tabLabel: 'MENU.GROUP_MANAGEMENT.EDIT_AGENT_GROUP.LBL',
    path: 'agent-groups/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },

  EDIT_AIRLINE_GROUP: {
    url: '/group-management/airline-groups/edit',
    label: 'MENU.GROUP_MANAGEMENT.EDIT_AIRLINE_GROUP.LBL',
    showMenu: false,
    tabLabel: 'MENU.GROUP_MANAGEMENT.EDIT_AIRLINE_GROUP.LBL',
    path: 'airline-groups/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },

  EDIT_GDS_GROUP: {
    url: '/group-management/gds-groups/edit',
    label: 'MENU.GROUP_MANAGEMENT.EDIT_GDS_GROUP.LBL',
    showMenu: false,
    tabLabel: 'MENU.GROUP_MANAGEMENT.EDIT_GDS_GROUP.LBL',
    path: 'gds-groups/edit/:id',
    permissionId: Permissions.forbidden,
    parent: 'GROUP_MANAGEMENT'
  },
  CLASSIC_BSP_LINK: {
    url: '',
    label: 'CLASSIC BSPlink',
    showMenu: true,
    tabLabel: '',
    path: '',
    permissionId: Permissions.notProtected,
    parent: null,
    isClassicMenuItem: true,
    openOnClick: false
  }
};
