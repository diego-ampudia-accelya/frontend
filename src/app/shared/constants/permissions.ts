// eslint-disable-next-line @typescript-eslint/naming-convention
export const Permissions = {
  /**
   * Used for menu items that have sub items
   */
  protectedByChildren: 'kRHnurhptKcjIDTWC3sx',
  /**
   * Used for routes that are not protected
   */
  notProtected: null,
  /**
   * Used for routes that the user should never be able to access
   */
  forbidden: 'forbidden',

  // Logged User
  readProfile: 'rProf',
  readProfileAirlineMulticountry: 'rMcProf',
  switchAccount: 'rPortal',
  impersonateMainUser: 'rImprs',
  impersonateAgentGroupMember: 'rAgnImprs',
  impersonateHOMU: 'rHomuImprs',

  // Dashboards
  readAcdmDash: 'rAcdmDash',
  readRefundDash: 'rRefundDash',
  readSalesDash: 'rSalesDash',

  // ADM/ACM/BSP Adjustment
  readAcdmNotSupervisedDocument: 'rAcdmNr',
  readAcdmSupervisionDocument: 'rAcdmSupvNr',
  readAdjustmentDocuments: 'rAdjuNr',
  readAdmCategorizationReason: 'rAdmCatRsn',
  createCategorizationReason: 'cAdmCatRsn',
  updateCategorizationReason: 'uAdmCatRsn',

  // MasterData
  createCreditCards: 'cCc',
  readCreditCards: 'rCc',
  readMonitoringHotData: 'rMonHot',
  readMonitoringMassUploadFiles: 'rMonMass',
  readMonitoringTaFop: 'rMonTaFop',
  readMonitoringVR: 'rMonVr',
  readMonitoringMasterDataRet: 'rMonCentre',
  readMonitoringPbdFiles: 'rMonPbd',
  updateCreditCards: 'uCc',
  deleteCreditCards: 'dCc',
  readAgent: 'rAgn',
  readAgentGroup: 'rAgnGrp',
  createAgentGroup: 'cAgnGrp',
  updateAgentGroup: 'uAgnGrpMod',
  deactivateAgentGroup: 'uAgnGrpDel',
  readAgentGroupSettings: 'rGrpCfg',
  readAirline: 'rAirl',
  readGlobalAirline: 'rGlobAirl',
  readBsp: 'rBsp',
  readGds: 'rGds',
  readGdsByAgent: 'rGdsAgn',
  readThirdPartySettings: 'r3PtyCfg',
  createGds: 'cGds',
  updateGds: 'uGds',
  deleteGds: 'dGds',
  readCurrency: 'rCrnc',
  updateCurrency: 'uCur',
  createCurrency: 'cCur',
  readPeriod: 'rPeriod',
  createPeriod: 'cPeriod',
  updatePeriod: 'uPeriod',
  readBspCurrencyAssignation: 'rCur',
  readGdsBspAssignments: 'rGds',
  readAgentGds: 'rAgnGds',
  readGdsConfiguration: 'rGdsCfg',
  readRemittanceFrequency: 'rRemitFreq',
  updateRemittanceFrequency: 'uRemitFreq',
  readOverrideFrequency: 'rOverFreq',
  createOverrideFrequency: 'cOverFreq',
  updateOverrideFrequency: 'uOverFreq',
  deleteOverrideFrequency: 'dOverFreq',
  readTicketingAuthorityHistory: 'rTaLog',
  readFileDescriptors: 'rFileDscr',
  updateFileDescriptors: 'uFileDscr',
  removeFileDescriptors: 'dFileDscr',
  createFileDescriptors: 'cFileDscr',
  readTaxOnComissionTypes: 'rTctp',
  createTaxOnComissionTypes: 'cTctp',
  updateTaxOnComissionTypes: 'uTctp',
  deleteTaxOnComissionTypes: 'dTctp',
  readSafTypes: 'rSafType',
  readDocumentTypes: 'rDocType',
  readDocumentSeries: 'rDocSerie',
  createSafTypes: 'cSafType',
  createDocumentTypes: 'cDocType',
  updateSafTypes: 'uSafType',
  updateDocumentTypes: 'uDocType',
  deleteSafTypes: 'dSafType',
  deleteDocumentTypes: 'dDocType',
  deleteDocumentSeries: 'dDocSerie',
  createDocumentSeries: 'cDocSerie',

  // Sub Users
  readAllSubusers: 'rAllSubusr', // Query Sub-users of any level below the user making the query
  readSubuser: 'rSubusr', // Query Sub-users created by the user making the query only
  readMulticountrySubuser: 'rMcSubusr',
  readLomuProfile: 'rLomuProf',
  readLomuPerm: 'rLomuPerm',
  updateLomuPerm: 'uLomuPerm',
  createSubuser: 'cSubusr',
  createHosuSubuser: 'cMcSubusr',
  updateSubuser: 'uSubusr',
  updateHosuSubuser: 'uMcSubusr',
  readSubuserTemplate: 'rSubusrTempl',
  updateSubuserTemplate: 'uSubusrTempl',
  readSubuserPermissions: 'rSubusrPerm',
  readSubuserHosuPermissions: 'rMcSubusrPerm',
  updateSubuserPermissions: 'uSubusrPerm',
  updateHosuSubuserPermissions: 'uMcSubusrPerm',

  // PBD
  readPbd: 'rPbd',
  readPendingPbds: 'rPbdPend',
  pbdStart: 'cPbd',
  updatePbdState: 'uPbdState',

  // Stock Application Request
  readStockApplicationRequest: 'rStckApp',
  createStockApplicationRequest: 'cStckApp',

  // Files
  readFile: 'rFile',
  readFileHot: 'rFileHot',
  archiveFile: 'uFile',
  uploadFile: 'cFile',
  readCommunicationsFile: 'rComFile',
  uploadCommunicationsFile: 'cComFile',
  uploadWebLinkFile: 'cWlFile',
  uploadAdmAcmFile: 'cAcdmFile',
  uploadRefundNoticeFile: 'cRnFile',
  uploadRefundApplicationFile: 'cRaFile',
  uploadAdmCategorizationFile: 'cAdmCatFile',
  uploadPaymentCardFile: 'cPymtcardFile',
  uploadHotConversationFile: 'cHotConvFile',
  uploadAgentGdsFile: 'cAgnGdsFile',
  uploadRemitFile: 'cRemitFile',
  uploadAcmFile: 'cAcmFile',
  uploadEvFile: 'cEvFile',
  uploadPnrFile: 'cPnrFile',
  uploadPBDFile: 'cPbdFile',
  readAgentReports: 'rAgnFile',
  readDeletedFiles: 'rDelFile',
  createDeletedFiles: 'cDelFile',
  readEvaluationDailyDownloads: 'rMasslRspFile',
  readGlobalTicketingAuthority: 'rXmlFile',
  createGlobalTicketingAuthority: 'cGlobTaReq',
  readLastGlobalTicketingAuthority: 'rGlobTaReq',
  createNonTicketingAuthority: 'rNonTa',
  createTicketingAuthorityStatus: 'cTaStatus',
  requestFopAgentStatus: 'cFopFileAgnReq',
  readUploadedFiles: 'rUplFiles',
  readDownloadedFiles: 'rDwlFiles',
  readNotDownloadedFiles: 'rNotDwlFiles',
  readSpecialFiles: 'rSplFiles',
  readDownloadedIataCommunications: 'rComFileDld',

  // Airline Configuration
  readTicketingAuthority: 'rTa',
  updateTicketingAuthority: 'uTa',
  readRefundIssueAirline: 'rRfndPermCfg',
  updateRefundIssueAirline: 'uRfndPermCfg',
  readRAIssueAgent: 'rRaPerm',
  readRNIssueAgent: 'rRnPerm',
  readACDMAuthorityAirline: 'r3PtyAcdmPermCfg',
  updateACDMAuthorityAirline: 'u3PtyAcdmPermCfg',
  readAirlineConfiguration: 'rAirlCfg',
  readAirlineSettings: 'rAirlCfgBsc',
  updateAirlineSettings: 'uAirlCfgBsc',
  readPBDAirline: 'rAirlCfgPbd',
  updatePBDAirline: 'uAirlCfgPbd',
  readAirlineAddressMaintenance: 'rAirlAdr',
  updateAirlineAddressMaintenance: 'uAirlAdr',
  readAgentAddressMaintenance: 'rAgnAdr',
  updateAgentAddressMaintenance: 'uAgnAdr',
  readVariableRemittanceSettings: 'rRemitCfg',
  updateVariableRemittanceSettings: 'uRemitCfg',
  readLocalTipConfiguration: 'rTipAirlCfg',
  updateLocalTipConfiguration: 'uTipAirlCfg',
  readBsps: 'rBsps', // Airline multicountry

  // DPC configuration
  readGeneralDpcConfiguration: 'rDpcCfg',
  updateGeneralDpcConfiguration: 'uDpcCfg',

  // Basic DPC configuration
  readBasicDPCConfiguration: 'rBasicCfg',

  // Master Data Reception Settings
  readMasterDataReceptionSettings: 'rMasterTabCfg',
  updateMasterDataReceptionSettings: 'uMasterTabCfg',

  //Agent Configuration
  readAgentSettings: 'rAgnCfg',

  // Iata Settings
  readIataSettings: 'rBasicCfg',
  updateIataSettings: 'uBasicCfg',
  readSettingsChanges: 'rBasicCfgLog',

  // Currency Conversion
  readCurrencyConversion: 'rCurConv',
  createCurrencyConversion: 'cCurConv',

  // Notifications Settings
  readNotificationSettings: 'rNotifCfg',
  updateNotificationSettings: 'uNotifCfg',

  // Tip Consents Settings
  uTipGlobCfg: 'uTipGlobCfg',

  // RA Issue Reasons
  readIataRaIssueReason: 'rRaRsn',
  createIataRaIssueReason: 'cRaRsn',
  updateIataRaIssueReason: 'uRaRsn',
  deleteIataRaIssueReason: 'dRaRsn',
  // RA Rejection Reasons
  readIataRaRejectionReason: 'rRaRejRsn',
  createIataRaRejectionReason: 'cRaRejRsn',
  updateIataRaRejectionReason: 'uRaRejRsn',
  deleteIataRaRejectionReason: 'dRaRejRsn',
  // Suspended Airlines
  readIataSuspendedAirlines: 'rSuspAirl',
  // Download Sub Users
  createSubUsersFile: 'cSubUsersFile',
  // IATA Users - Global Users File
  requestIataGlobalUsersFile: 'cGlobUsersFile',
  // IATA Deactivated and Reinstated Agents
  requestIataDeactivatedAgents: 'rAgnDeact',
  requestIataReinstatedAgents: 'rAgnReinst',
  // IATA Users - Global ADM Report
  readIataGlobalAdmReport: 'rGlobAdmRep',
  // IATA Main Users
  readMainUsers: 'rUsr',
  createMainUsers: 'cUsr',
  updateMainUsers: 'uUsr',
  readMainUserPermissions: 'rUsrPerm',
  updateMainUserPermissions: 'uUsrPerm',
  // IATA - TIP Global Reports Settings Query
  readTipGlobRepStt: 'rTipGlobRepStt',

  // IP Address
  readIpAddressSettings: 'rIp',
  createIpAddressSettings: 'cIp',
  updateIpAddressSettings: 'uIp',
  deleteIpAddressSettings: 'dIp',

  //Email Alert
  readEmailAlerts: 'rEmailAlrt',
  updateEmailAlerts: 'uEmailAlrt',

  //Global TA and FOP Files
  readGlobalTaFopFiles: 'rGlobFileBsp',
  createGlobalTaFopFiles: 'cGlobFileBsp',
  updateGlobalTaFopFiles: 'uGlobFileBsp',
  deleteGlobalTaFopFiles: 'dGlobFileBsp',

  //Forwarded ADM Global files
  readForwardedAdmGlobalFiles: 'cGlobAdmsForwGds',

  // Issue Reasons
  readIssueReason: 'rAcdmRsn',
  createIssueReason: 'cAcdmRsn',
  updateIssueReason: 'uAcdmRsn',
  deleteIssueReason: 'dAcdmRsn',

  // Dispute Reasons
  readDisputeReason: 'rAcdmDispRsn',
  createDisputeReason: 'cAcdmDispRsn',
  updateDisputeReason: 'uAcdmDispRsn',
  deleteDisputeReason: 'dAcdmDispRsn',

  // SFTP
  readSftpAccount: 'rSftpAcc',
  updateSftpAccount: 'uSftpAcc',
  readAdditionalUploadSftpAccount: 'rSftpAdd',
  updateAdditionalUploadSftpAccount: 'uSftpAdd',
  readMassDownloadSftpAccount: 'rSftpMass',
  updateMassDownloadSftpAccount: 'uSftpMass',
  readGlobalSftpAccount: 'rSftpGlobal',
  updateGlobalSftpAccount: 'uSftpGlobal',
  readSftpAccounts: 'rSftp',

  //Multocountry Sub Users
  readHOMCSU: 'rHomcsu',
  createHOMCSU: 'cHomcsu',
  updateHOMCSU: 'uHomcsu',
  readHOMCSUPermissions: 'rHomcsuPerm',
  updateHOMCSUPermissions: 'uHomcsuPerm',

  //Head Office Main Users
  readHomu: 'rHomu',
  readHomuPerm: 'rHomuPerm',
  updateHomuPermissions: 'uHomuPerm',

  // Public API
  readPublicApiAccounts: 'rPublicApiAcc',
  updatePublicApiAccounts: 'cudPublicApiAcc',

  // Sales Query
  readBillingStatement: 'rBillStmt',
  readScuSummary: 'rScu',
  readNonComparatives: 'rSaleNoComp',
  readBillingAnalysis: 'rBillAnls',
  readComparatives: 'rSaleComp',
  readAnnualAccumulated: 'rSaleAnnAcu',
  readAgentDictionary: 'rAgnDic',
  readAdvancePayment: 'rAdvPymt',
  createAdvancePayment: 'cAdvPymt',

  // Document Enquiry
  readDocumentDetails: 'rTaip',
  readDocumentDetailsRejected: 'rTaipRej',
  issueActionAcm: 'cAcm',
  issueActionAdm: 'cAdm',
  issueActionRefundNotice: 'cRn',
  issueActionRefundApplication: 'cRa',

  // Rejected Documents
  readRejectedDocuments: 'rDocRej',

  // Credit Card Documents
  readCreditCardDocuments: 'rDocCc',

  // Net Remit Documents
  readNetRemitDocuments: 'rDocNr',

  // Third Party Profile
  readACDMAuthorityThirdParty: 'r3PtyAcdmPerm',

  // Artificial permissions
  // Sales Query
  readBillingAnalysisTotals: 'rBillAnlsTotals',
  // Document Enquiry
  generateEnquiryPdf: 'gEPdf',

  // TIPs
  readGlobalAirlineConfiguration: 'rTipGblAirlCfg',
  updateGlobalAirlineConfiguration: 'uTipGblAirlCfg',
  readTipReport: 'rTipRep',
  createGlobalConsentFile: 'cGlobConsFile',
  createGlobalReportFile: 'cGlobTipRepFile',

  //WECHAT
  readWechat: 'rWeChat',
  createWechat: 'cWeChat',

  // LEAN
  lean: 'rLean', // For multicountry users, which have multiple BSPs

  // Audit
  readAuditLog: 'rAudLog',
  readAllBsps: 'rAllBsps',
  readAuditAcdm: 'rAudAcdm',
  readAuditPbds: 'rAudPbd',
  readAuditRefund: 'rAudRfnd',

  //COMPANY CALENDAR
  readCompanyCalendar: 'rCalCom'
};
