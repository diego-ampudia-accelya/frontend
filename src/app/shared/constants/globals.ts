/* eslint-disable no-useless-escape */
/* eslint-disable @typescript-eslint/naming-convention */
export const GLOBALS = {
  LOADING_SPINNER_DELAY: 400,
  ICONS: {
    COLLAPSE_SRC: '../../../../assets/images/utils/ico_collapse.svg',
    EXPAND_SRC: '../../../../assets/images/utils/ico_expand.svg'
  },
  PATTERNS: {
    ALPHANUMERIC: /^[a-zA-Z0-9 ]*$/,
    MULTIPLE_OF_10: /^[0-9]*0$/,
    SPECIAL_ALPHANUMERIC: /^[a-zA-Z0-9 \/\.\-\(\)]+$/,
    DECIMALS_PATTERN: /^\d*\.?\d*$/,
    RATE: /^([0-9]{1,2})(\.[0-9]{1,2})?$/,
    TREE_CAPS: /[A-Z]{3}/,
    AGENT: /[0-9]{7}$/,
    AIRLINE: /[A-Z0-9]{3}$/,
    FREE_STAT: /[DI][A-Z0-9]{2}$/,
    PERCENT_MAX_99_99: /^(?:99|[1-9]?[0-9])(([\.][0-9]{1,2})?)$/,
    WAIVER_CODE: /[A-Z0-9 /.-]{0,14}/,
    TOUR_CODE: /[ \!#-~]{0,15}/,
    ELECTRONIC_TICKET_AUTH: /[A-Za-z0-9 ]{0,14}/,
    PASSSENGER: /[0-9A-Za-z\-\.\/ ]{0,49}/,
    EMAIL: /^[^(\.)][a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[\w-]+(\.[\w]+)*(\.[a-z]{2,})$/,
    SFTP_PASSWORD: /^(?=.*[A-Za-z_.])(?=.*\d)[[A-Za-z\d._]{7,14}$/,
    TCTP_TYPE: /^[A-Za-z0-9]*$/,
    TCTP_DESCRIPTION: /^[A-Za-z0-9 ]*$/,
    ACDMS: {
      TAX: /^(?:(?:XF[A-Z]{3}0+(?:\.0+){0,1})|(?:[A-Z0-9]{2})|(?:XF[A-Z]{3}\d+(?:\.\d+){0,1})|(?:O[ABC][A-Z0-9 ./-]{0,6}))$/i //* /i because code is uppercased before BE
    },
    REFUNDS: {
      TOUR_CODE: /^[\x20\x21\x23-\x7E]*$/,
      CUSTOMER_REF: /^[A-Z0-9 ]*$/i, //* /i because code is uppercased before BE
      E_TICKET: /^[A-Z0-9 ]*$/i, //* /i because code is uppercased before BE
      CARD_MASK: /^[\x20\x30-\x39\x41-\x5B\x5D\x61-\x7B\x7D]{1,19}/,
      CARD_TYPE: /^[0-9A-Z]{2}$/i, //* /i because code is uppercased before BE
      TAX: /^(([A-WY-Z][A-Z0-9])|([A-Z][A-SU-Z0-9])|(O[ABC][A-Z0-9 ./-]{0,6})|(XF[A-Z]{3}[0-9]*[1-9][0-9]*(\.[0-9]+)?)|(XF[A-Z]{3}[0-9]+\.[0-9]*[1-9][0-9]*))$/i //* /i because code is uppercased before BE
    },
    URL: /^(ht|f)tp(s?):\/\//,
    IP_ADDRESS: /^(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])(?:\.(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])){3}$/
  },
  HTML_PATTERN: {
    WAIVER_CODE: '^[A-Z0-9 /.-]*$',
    INPUT_PERCENT_MAX_99_99: '^[.0-9]*$',
    CAPITALTEXT: '^[A-Z]*$',
    TEXT: '^[A-Za-z]*$',
    TAX: '^[A-Z0-9./-]*$',
    PASSSENGER: '^[A-Z0-9-.//\\s]*$',
    ALPHANUMERIC_LOWERCASE: '^[a-zA-Z0-9]*$',
    ALPHANUMERIC_LOWERCASE_WITH_SPACE: '^[a-zA-Z0-9 ]*$',
    ALPHANUMERIC_UPPERCASE: '^[A-Z0-9]*$',
    TOUR_CODE: '^[ !#-~]*$',
    NUMERIC: '^[0-9]*$',
    NUMERIC_WITH_SIGN: '^-?[0-9]*$',
    COMPARE_NUMBER: '^[<>=]{0,1}[0-9]+$'
  },
  DATE_FORMAT: {
    DATE: 'ddMy',
    SHORT_ISO: 'YYYY-MM-DD',
    SHORT_ISO_YEAR_MONTH: 'YYYY-MM'
  },
  FORM_OF_PAYMENT: ['CA', 'CC', 'MSCA', 'MSCC', 'EP'],
  FORM_OF_PAYMENT_EP: 'EP',
  TYPES_OF_USER: ['AIRLINE', 'AGENT', 'DPC', 'GDS', 'IATA', 'THIRD_PARTY'],
  TYPES_OF_USER_AIRLINE: 'AIRLINE',
  TYPES_OF_USER_AGENT: 'AGENT',
  REACTIVE_FORMS: {
    EMIT_EVENT_FALSE: {
      emitEvent: false
    }
  },
  ACDM: {
    ADM: ['ADMA', 'SPDR', 'ADMD'],
    ACM: ['ACMA', 'SPCR', 'ACMD'],
    COMPLEX_TYPE: ['SPDR', 'SPCR', 'ACMA', 'ADMA'],
    FOR_REFUND: 'R',
    FOR: ['I', 'R', 'X', 'E'],
    FOR_ISSUE: 'I'
  },
  PAGEABLE: {
    CONSECUTIVE_PAGES: 5,
    PAGE_URL_PARAM: 'page',
    SIZE_URL_PARAM: 'size',
    SORT_PARAM: 'sort',
    SORT_ASC: ',asc',
    SORT_DESC: ',desc',
    SIZES: [10, 25, 50, 100],
    DEFAULT_SIZE: 10,
    INIT_PAGE: 0
  },
  FILE_STATUS: ['DELETED', 'DOWNLOADED', 'NOT_DOWNLOADED', 'TRASHED'],
  STORAGE_KEY: {
    TABS: 'tabsList',
    ACCESS: 'code'
  },
  XFTAX: 'XF',
  LONG_DASH: '\u2014', // em dash
  MAX_DATE: new Date(8.64e15),
  MIN_DATE: new Date(-8.64e15)
};
