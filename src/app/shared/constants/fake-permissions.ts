// eslint-disable-next-line @typescript-eslint/naming-convention
export const FakePermissions = {
  // Faked permission on BSPLink side so Agent Group users can fetch all agents within their group.
  readAgentDictionary: 'rAgnDic',
  readOnlineHelpIATA: 'rOnlHlpIT',
  readOnlineHelpAirline: 'rOnlHlpArl',
  readOnlineHelpAgent: 'rOnlHlpAgn',
  readOnlineHelpAgentGroup: 'rOnlHlpAgnGr',
  readOnlineHelpThirdParty: 'rOnlHlpTP',
  readOnlineHelpGDS: 'rOnlHlpGDS'
};
