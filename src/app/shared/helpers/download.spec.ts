/* eslint-disable @typescript-eslint/naming-convention */
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { formatDownloadResponse } from './download';

describe('download', () => {
  it('should extract the file name from the Content-Disposition header', () => {
    const fileName = 'some_file.txt';
    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({
        'Some-Header': 'some header value',
        'Content-Disposition': `filename="${fileName}"`
      })
    });

    const { blob: actualBlob, fileName: actualFileName } = formatDownloadResponse(httpResponse);

    expect(actualBlob.type).toBeDefined();
    expect(actualFileName).toBe(fileName);
  });

  it('should extract a default file name if Content-Disposition header is missing', () => {
    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({ 'Some-Header': 'some header value' })
    });

    const { blob: actualBlob, fileName: actualFileName } = formatDownloadResponse(httpResponse);

    expect(actualBlob).toBeDefined();
    expect(actualFileName).toBe('downloaded_file');
  });

  it('should extract a default file name if Content-Disposition header is present but has invalid value', () => {
    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({
        'Some-Header': 'some header value',
        'Content-Disposition': 'some invalid value'
      })
    });

    const { blob: actualBlob, fileName: actualFileName } = formatDownloadResponse(httpResponse);

    expect(actualBlob).toBeDefined();
    expect(actualFileName).toBe('downloaded_file');
  });

  it('should extract content type when Content-Type header is present', () => {
    const contentType = 'application/json';
    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({
        'Some-Header': 'some header value',
        'Content-Type': contentType
      })
    });

    const fake = new Blob();
    spyOn(global, 'Blob').and.returnValue(fake);

    const { blob: actualBlob } = formatDownloadResponse(httpResponse);

    expect(actualBlob).toBeDefined();
    expect(Blob).toHaveBeenCalledWith(jasmine.anything(), { type: contentType });
  });

  it('should extract a default content type when Content-Type header is missing', () => {
    const httpResponse = new HttpResponse<any>({
      headers: new HttpHeaders({
        'Some-Header': 'some header value'
      })
    });

    const fake = new Blob();
    spyOn(global, 'Blob').and.returnValue(fake);

    const { blob: actualBlob } = formatDownloadResponse(httpResponse);

    expect(actualBlob).toBeDefined();
    expect(Blob).toHaveBeenCalledWith(jasmine.anything(), { type: 'application/octet-stream' });
  });
});
