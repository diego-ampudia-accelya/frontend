import { chain } from 'lodash';

import { SortingObject } from '../models/sorting-object.model';

export function formatSortBy(sortBy: SortingObject[], sortMapper: { from: string; to: string }[]): SortingObject[] {
  return sortBy.map(sortingItem => {
    const { attribute, sortType } = sortingItem;

    const mappedSortBy = chain(sortMapper)
      .filter(sortMapperOption => sortMapperOption.from === attribute)
      .map(sortMapperOption => ({ attribute: sortMapperOption.to, sortType }))
      .first()
      .value();

    return mappedSortBy || sortingItem;
  });
}
