import { HttpResponse } from '@angular/common/http';
import { Dictionary } from '@ngrx/entity';
import { trim } from 'lodash';

export const downloadRequestOptions = Object.freeze({
  responseType: 'arraybuffer' as 'json',
  observe: 'response' as 'body'
});

export function formatDownloadResponse(response: HttpResponse<ArrayBuffer>): { blob: Blob; fileName: string } {
  const { headers } = response;
  const contentDisposition = parseContentDisposition(headers?.get('Content-Disposition'));
  const fileName = contentDisposition.filename || 'downloaded_file';

  const contentType = parseContentType(headers?.get('Content-Type'));
  const type = contentType.type || 'application/octet-stream';

  const blob = new Blob([response.body], { type });

  return { blob, fileName };
}

const parseContentDisposition = (header: string): { attachment?: boolean; filename?: string; name?: string } =>
  toDictionary(tokenize(header));

const parseContentType = (header: string): { type: string; charset?: string; boundary?: string } => {
  const [[type], ...directives] = tokenize(header);

  return {
    type,
    ...toDictionary(directives)
  };
};

const tokenize = (header: string): string[][] => {
  // The value of a header consists of directives separated by ';'
  // Each directive consists of a name and an optional value separated by '='
  // The value can optionally be wrapped in ""
  header = header ?? '';
  const directives = header.split(';').map(value => value.trim());

  return directives.map(directive => directive.split('=').map(segment => trim(segment, '"')));
};

const toDictionary = (nameValuePairs: string[][]): Dictionary<string | boolean> =>
  nameValuePairs.reduce((result, [key, value]) => {
    result[key] = value ?? true;

    return result;
  }, {});
