import { chain, get, isEmpty, isString, orderBy } from 'lodash';
import { PAGINATION } from '../components/pagination/pagination.constants';
import { SortOrder } from '../enums/sort-order.enum';
import { PagedData } from '../models/paged-data.model';
import { RequestQuery } from '../models/request-query.model';

export interface AggregationOptions<T> {
  aggregateBy: Array<keyof T>;
  aggregate(group: T[]): T;
}

// TODO: Write unit tests.
// NOTE: Do not delete the file even if it is not used at some point, because it might be needed in future.
export class ClientGridTableHelper<T> {
  /**
   * Applies filtering, sorting and paging.
   */
  public query(
    items: T[],
    requestQuery: RequestQuery,
    aggregationOptions?: AggregationOptions<T>,
    customFilterMap?: { [key: string]: (item, filterValue) => boolean }
  ): PagedData<T> {
    items = items || [];

    // Apply the filtering first in order to cycle less items during the sorting.
    items = this.filterItems(requestQuery.filterBy, customFilterMap, items);
    items = this.aggregateItems(aggregationOptions, items);
    items = this.sortItems(requestQuery.sortBy, requestQuery.sortOrder, items);

    return this.paginateItems(items, requestQuery.page, requestQuery.size);
  }

  private filterItems(
    filterBy: { [key: string]: any },
    customFilterMap?: { [key: string]: (item, filterValue) => boolean },
    items: T[] = []
  ): T[] {
    let filteredItems = items;
    for (const filterKey in filterBy) {
      // eslint-disable-next-line no-prototype-builtins
      if (filterBy.hasOwnProperty(filterKey) && filterBy[filterKey] != null) {
        const filterValue = filterBy[filterKey];
        const customFilter = customFilterMap && customFilterMap[filterKey];

        filteredItems = filteredItems.filter(item => {
          if (customFilter) {
            return customFilterMap[filterKey](item[filterKey], filterValue);
          }

          if (Array.isArray(filterValue)) {
            return filterValue.includes(get(item, filterKey));
          }

          return get(item, filterKey) === filterValue;
        });
      }
    }

    return filteredItems;
  }

  /**
   * Handles aggregation of items.
   *
   * @param items The items to be aggregated
   * @param aggregationOptions Contains properties to aggregate by and aggregation function
   */
  private aggregateItems(aggregationOptions: AggregationOptions<T>, items: T[] = []): T[] {
    if (!aggregationOptions) {
      return items;
    }

    // Items with the same aggregation properties are grouped together
    const groupKeySeparator = '-';
    const getGroupKey = (item: T) =>
      chain<Partial<T>>(item).pick(aggregationOptions.aggregateBy).values().join(groupKeySeparator).value();

    // Group items and aggregate each group.
    return chain(items)
      .groupBy(getGroupKey)
      .values()
      .map(group => aggregationOptions.aggregate(group))
      .value();
  }

  private sortItems(sortBy: string, sortOrder: SortOrder = SortOrder.Asc, items: T[] = []): T[] {
    let sortedItems = items;

    if (!isEmpty(items) && sortBy != null) {
      const sortOrderValue = sortOrder === SortOrder.Desc ? 'desc' : 'asc';
      // Needed because lodash orderBy is case sensitive.
      const sortCaseInsensitive = (item: T) => {
        const value: any = get(item, sortBy);
        if (isString(value)) {
          return value.toLowerCase();
        }

        return value;
      };

      sortedItems = orderBy<T>(items, [sortCaseInsensitive], [sortOrderValue]);
    }

    return sortedItems;
  }

  private paginateItems(items: T[] = [], page = 0, pageSize = PAGINATION.DEFAULT_PAGE_SIZE): PagedData<T> {
    let pagedItems = items;
    const pagedData: PagedData<T> = {
      records: [],
      pageSize,
      total: pagedItems.length,
      totalPages: 0,
      pageNumber: 0
    };

    const itemsOffset = page * pageSize;

    pagedData.totalPages = Math.ceil(pagedItems.length / pageSize);
    pagedItems = pagedItems.slice(itemsOffset).slice(0, pageSize);

    // Apply the specified page if there are records for it. Otherwise leave the first page.
    if (pagedItems.length) {
      pagedData.pageNumber = page;
    } else if (page > 0) {
      pagedData.pageNumber = 0;
      pagedItems = items.slice(0, pageSize);
    }

    pagedData.records = pagedItems;

    return pagedData;
  }
}
