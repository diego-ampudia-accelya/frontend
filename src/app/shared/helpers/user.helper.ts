import { SubUser, SubUserUpdateModel } from '~app/sub-users/shared/sub-users.models';

export const toSubUserUpdateModel = (subUser: SubUser): SubUserUpdateModel => {
  const {
    id,
    template,
    portalEmail,
    name,
    countries,
    allCountries,
    contactInfo: { organization, telephone, country, city, address, postCode }
  } = subUser || { contactInfo: {} };

  return {
    id,
    template,
    portalEmail,
    name,
    organization,
    telephone,
    country,
    city,
    address,
    postCode,
    countries: countries ? countries : [],
    allCountries
  };
};
