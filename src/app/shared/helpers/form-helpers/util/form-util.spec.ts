import { FormBuilder } from '@angular/forms';

import { DEFAULT_UPDATE_ON } from '~app/shared/helpers/form-helpers/util/form-util.config';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';

describe('FormFactory', () => {
  const fb = new FormBuilder();

  describe('updateOn property retrieval', () => {
    it('should retrieve DEFAULT_UPDATE value when the property is not set in a control', () => {
      const control = fb.control('');
      expect(FormUtil.getUpdateOn(control)).toBe(DEFAULT_UPDATE_ON);
    });

    it('should retrieve the actual property when it is set in a control', () => {
      const control = fb.control('', { updateOn: 'blur' });
      expect(FormUtil.getUpdateOn(control)).toBe('blur');
    });

    it('should retrieve DEFAULT_UPDATE value when the property is not set in a control or parents', () => {
      const group = fb.group({ test: '' });
      expect(FormUtil.getUpdateOn(group.get('test'))).toBe(DEFAULT_UPDATE_ON);
    });

    it('should retrieve the parent property if it is not set in child', () => {
      const group = fb.group({ test: '' }, { updateOn: 'submit' });
      expect(FormUtil.getUpdateOn(group.get('test'))).toBe('submit');
    });

    it('should retrieve the child property even if parent has also its property set', () => {
      const control = fb.control('', { updateOn: 'change' });
      const group = fb.group({ test: control }, { updateOn: 'blur' });
      expect(FormUtil.getUpdateOn(group.get('test'))).toBe('change');
    });
  });
});
