import { FormArray, FormGroup } from '@angular/forms';

export class FormList {
  /**
   * Determines whether adding a line is disabled
   *
   * @returns true if there are no lines or last one is empty/invalid
   */
  public static isAddLineDisabled(formArray: FormArray): boolean {
    const lastLineGroupControls = formArray.controls.length
      ? (formArray.controls[formArray.controls.length - 1] as FormGroup).controls
      : null;

    return (
      !lastLineGroupControls ||
      formArray.invalid ||
      Object.keys(lastLineGroupControls).every(key => !lastLineGroupControls[key].value)
    );
  }
}
