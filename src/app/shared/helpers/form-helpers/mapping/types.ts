import { RecursivePartial } from '../../recursive-partial';

export interface ArrayField {
  fieldName: string;
  index: number;
}

//* Generic T would represent the origin Object type, while P would be the destination Object type
export type FieldMapper<T, P> = { [key in keyof T]: RecursivePartial<P> | Array<RecursivePartial<P>> };

//* Type for adapter mapper object. It should contain any key of the object plus 'default' use for apply a default function
// eslint-disable-next-line @typescript-eslint/ban-types
export type AdapterMapper<T> = { [key in keyof T | 'default']?: Function };
