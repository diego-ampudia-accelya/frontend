export function uppercaseAdapter(value: string): string {
  if (typeof value === 'string') {
    value = value.toUpperCase();
  }

  return value;
}

export function getDefaultCalculationFieldValueBE(field: number | string): number {
  if (!field) {
    return 0;
  }

  return typeof field === 'string' ? parseFloat(field) : field;
}

export function getNullableCalculationFieldValueBE(field: number | string): number {
  return field ? getDefaultCalculationFieldValueBE(field) : null;
}

export function getCashCalculationFieldValueBE(field: number | string): number {
  if (field === '') {
    return 0;
  }
  if (typeof field === 'string') {
    return parseFloat(field);
  }

  return field;
}

export function getDefaultCalculationFieldValueView(field: number): number {
  return field === 0 ? null : field;
}

export function getDefaultBooleanAdapter(field: any): boolean {
  return !!field;
}
