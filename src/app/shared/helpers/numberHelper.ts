export class NumberHelper {
  /**
   * Rounds specific amount using provided decimals
   *
   * Solution extracted from https://stackoverflow.com/questions/6134039/format-number-to-always-show-2-decimal-places/34796988#34796988
   *
   * Avoids floating point errors
   *
   * @param value number to be rounded
   * @param decimals number of decimals for doing the rounding
   * @returns rounded number
   */
  public static round(value: number, decimals: number): number {
    const roundValueAndDecimals = Math.round(`${value}e${decimals}` as any);

    return Number(`${roundValueAndDecimals}e-${decimals}`);
  }

  /**
   * Rounds string to specific number of decimals
   *
   * @param value if the string provided is not valid, zero will be used by default
   * @param decimals number of decimals to return after rounding
   * @returns string
   */
  public static roundString(value: string | number, decimals: number): string {
    if (isNaN(value as any)) {
      value = 0; // Since this is a method that will receive values typed by users, it would not be a good idea to throw an exception here
    }

    return NumberHelper.round(value as any, decimals).toFixed(decimals as any);
  }

  /**
   * Format number or string to match the required number of decimals.
   * If value has more decimals than required, the value is returned as it is (no rounding applied)
   * Example: 3 -> 3.00, 3.1 -> 3.10, 3.12345 -> 3.12345
   *
   * @param value number or string to be formatted
   * @param decimals minimum number of decimals the value must have
   * @returns string
   */
  public static formatNumber(value: string | number, requiredDecimals: number): string {
    if (value === '' || value == null || isNaN(value as any)) {
      return value as any;
    }

    const number = Number(value);
    const existingDecimals = Math.floor(number) === number ? 0 : number.toString().split('.')[1].length || 0;

    return existingDecimals <= requiredDecimals ? number.toFixed(requiredDecimals) : value.toString();
  }

  /**
   * Gets monetary amount max length
   *
   * @param decimals decimals of the amount
   * @returns 11 if amount has decimals or 10 otherwise
   */
  public static getMonetaryAmountMaxLength(decimals: number): number {
    return decimals > 0 ? 12 : 11;
  }
}
