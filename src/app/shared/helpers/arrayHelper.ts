import { get } from 'lodash';

import { DropdownOption } from '~app/shared/models/dropdown-option.model';

/**
 * Array helper includes variety of convenient functions related to array manipulation.
 */
export class ArrayHelper {
  /**
   * Exchanges all keys with their associated values in an array.
   * Keys from array become values and values from array become keys.
   */
  public static exchangeKeysWithAssociatedValues(collection: []): any[] {
    const swappedKeyValueCollection = [];

    Object.keys(collection).forEach(key => {
      swappedKeyValueCollection[collection[key]] = key;
    });

    return swappedKeyValueCollection;
  }

  // ToDo: Rewrite with types
  public static toDropdownOptions(items: any[], index = -1): any {
    return Array.from(new Set(items)).map(ArrayHelper.toDropdownOption);
  }

  // ToDo: Rewrite with types
  public static toDropdownOption(item: any): DropdownOption {
    let result: DropdownOption;

    if (typeof item === 'string') {
      result = { value: item, label: item };
    } else if (typeof item === 'number') {
      result = { value: item, label: '' + item };
    } else {
      result = {
        value: item.id,
        label: item.name !== undefined ? item.name : item.code
      };
    }

    return result;
  }

  public static toSameValueAndLabelByProperty(items: any, property: string) {
    return items.map(item => ({
      value: item[property],
      label: item[property]
    }));
  }

  public static toSameValueAndLabelByLabel(items: DropdownOption[]) {
    return ArrayHelper.toSameValueAndLabelByProperty(items, 'label');
  }

  public static toCustomValueAndLabel(items: any, value: string, label: string) {
    return items.map(item => ({
      value: item[value],
      label: item[label]
    }));
  }

  public static toCustomValueLabelAndNestedProperties(items: any, value: string, label: string): DropdownOption[] {
    return items.map(item => ({
      value: get(item, value),
      label: get(item, label)
    }));
  }

  public static enumToDropdownValue(enumerator: any): DropdownOption[] {
    return Object.keys(enumerator).map(key => ({
      value: key,
      label: `${key} - ${enumerator[key]}`
    }));
  }

  public static enumToDropdownOptions(enumerator: any): DropdownOption[] {
    return Object.keys(enumerator).map(key => ({
      value: enumerator[key],
      label: enumerator[key]
    }));
  }
}
