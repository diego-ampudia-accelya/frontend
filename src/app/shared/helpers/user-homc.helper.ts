import {
  HeadOfficeSubUser,
  HeadOfficeSubUserUpdateModel
} from '~app/master-data/head-office-sub-users/models/head-office-sub-users.models';

export const toSubUserHomcUpdateModel = (subUser: HeadOfficeSubUser): HeadOfficeSubUserUpdateModel => {
  const {
    id,
    access,
    portalEmail,
    name,
    registerDate,
    expiryDate,
    status,
    contactInfo: { organization, telephone, country, city, address, postCode }
  } = subUser || { contactInfo: {} };

  return {
    id,
    access,
    status,
    registerDate,
    expiryDate,
    portalEmail,
    name,
    organization,
    telephone,
    country,
    city,
    address,
    postCode
  };
};
