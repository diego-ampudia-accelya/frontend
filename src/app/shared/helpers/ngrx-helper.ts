/**
 * This function takes as many reducer funtions as needed and apply each one sequentially to the state.
 *
 * @param reducerFunctions functions receiving (state, action) as params
 * @returns modified state
 */
export const combineReducersCustom =
  (...reducerFunctions) =>
  (state, action) =>
    reducerFunctions.reduce((prev, current) => current(prev, action), state);
