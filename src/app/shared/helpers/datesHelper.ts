import moment from 'moment-mini';

import { GLOBALS } from '../constants/globals';

export const getDaysDiffBetweenDates = (dateInitial, dateFinal) => (dateFinal - dateInitial) / (1000 * 3600 * 24);

export const toShortIsoDate = (date: Date) => moment(date).format(GLOBALS.DATE_FORMAT.SHORT_ISO);

export const toShotIsoYearMonth = (date: Date) => moment(date).format(GLOBALS.DATE_FORMAT.SHORT_ISO_YEAR_MONTH);

export const lastDayOfMonthToShortIsoDate = (date: Date) =>
  toShortIsoDate(new Date(date.getFullYear(), date.getMonth() + 1, 0));
