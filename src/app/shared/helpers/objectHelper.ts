import { differenceWith, fromPairs, isEqual, toPairs } from 'lodash';

/**
 * Object helper includes variety of convenient functions related to object manipulation.
 */
export class ObjectHelper {
  /**
   * Recursively looping through an object to build a collection from all values.
   */
  public static getValuesRecursively(nestedObjects): any {
    const objectValues = [];

    const isObject = element => typeof element === 'object' && !Array.isArray(element);

    const iterate = obj => {
      Object.keys(obj).forEach(key => {
        if (isObject(obj[key])) {
          iterate(obj[key]);

          return;
        }

        objectValues.push(obj[key]);
      });
    };

    iterate(nestedObjects);

    return objectValues;
  }

  public static getDifferenceBetweenTwoObjects(firstObject, secondObject): any {
    return fromPairs(differenceWith(toPairs(firstObject), toPairs(secondObject), isEqual));
  }
}
