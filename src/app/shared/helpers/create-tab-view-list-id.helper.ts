/**
 * Create view list id for core state based on the given tab and subtab labels
 *
 * @param tab Tab label
 * @param subtab Subtab label
 */
export function createTabViewListId(tab: string, subtab?: string): string {
  return `${tab}:${subtab ? subtab : 'default'}`;
}
