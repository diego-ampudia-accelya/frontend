import moment from 'moment-mini';

import { AgentSummary, AirlineSummary } from '../models';
import { DropdownOption } from '../models/dropdown-option.model';
import { RequestQueryFilter } from '../models/request-query-filter.model';
import { Currency } from '~app/shared/models/currency.model';
import { Bsp, BspDto } from '~app/shared/models/bsp.model';

const operatorMap = {
  '=': 'eq',
  '<': 'lt',
  '>': 'gt',
  like: 'like',
  in: 'in',
  '<=': 'lte',
  '>=': 'gte'
};

export class FiltersHelper {
  public static createQueryString(filters: any): string {
    const filterParams = filters.map
      ? filters.map((filter: RequestQueryFilter) => {
          let filterValue = filter.attributeValue;
          if (filterValue instanceof Date) {
            filterValue = filterValue.toISOString();
          } else {
            filterValue = filter.attributeValue.toString().split(',').join('::');
          }

          return `${filter.attribute}::${operatorMap[filter.operation]}::${filterValue}`;
        })
      : [];

    return `${filterParams.join(',')}`;
  }
}

export const toValueLabelObject = (item: any) => ({ value: item, label: item });

export const toValueLabelObjectBspId = (bsp: Bsp) => ({
  value: bsp.id,
  label: `${bsp.isoCountryCode} - ${bsp.name || ''}`
});

export const toValueLabelObjectBsp = (bsp: Bsp | BspDto) => ({
  value: bsp,
  label: `${bsp.isoCountryCode} - ${bsp.name || ''}`
});

export const toValueLabelCurrencyId = (currency: Currency) => ({ value: currency.id, label: currency.code });

export const toValueLabelCurrency = (currency: Currency) => ({ value: currency, label: currency.code });

export const toValueLabelObjectAirlineIataCode = (airline: { iataCode: any; localName: any }) =>
  toValueLabelObjectCustom(airline, { value: 'iataCode', labels: ['iataCode', 'localName'] });

export const toValueLabelObjectAirlineId = (airline: { id: any; iataCode: any; localName: any }) =>
  toValueLabelObjectCustom(airline, { value: 'id', labels: ['iataCode', 'localName'] });

export const toValueLabelObjectAgentId = (agent: { id: any; iataCode: any; name: any }) =>
  toValueLabelObjectCustom(agent, { value: 'id', labels: ['iataCode', 'name'] });

export function toValueLabelObjectDictionary(dropDownSummary: DropdownOption<AirlineSummary | AgentSummary>[]) {
  return dropDownSummary.map(elem => ({
    value: elem.value.code,
    label: elem.label
  }));
}

export function toValueLabelObjectDictionaryDesignator(dropDownSummary: DropdownOption<AirlineSummary>[]) {
  return dropDownSummary.map(elem => ({
    value: elem.value.designator,
    label: elem.value.designator
  }));
}

export function toValueLabelObjectDictionaryId(dropDownSummary: DropdownOption<AirlineSummary | AgentSummary>[]) {
  return dropDownSummary.map(elem => ({
    value: elem.value.id,
    label: elem.label
  }));
}

export function toValueLabelObjectCustom<T>(
  item: T,
  mapper: { value: keyof T; labels: Array<keyof T> },
  separator = ' / '
): DropdownOption {
  return {
    value: item[mapper.value],
    label: mapper.labels.map(label => item[label]).join(separator)
  };
}

/**
 * Format month period, removing last specific period digit
 *
 * @param period with format YYYYMMP
 * @returns `periodYearMonth` with format YYYY-MM
 */
export const formatMonthPeriod = (period: string): string =>
  `${period.substring(0, 4)}-${period.substring(4, period.length - 1)}`;

export const formatPeriod = (date: string): string => (date ? moment(date, 'MM/DD/YYYY').format('YYYY-MM-DD') : null);
