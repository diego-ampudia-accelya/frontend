import moment from 'moment-mini';

import { AgentSummary, AirlineSummary, GdsSummary, GlobalAirlineSummary } from '../models';
import { Bsp, BspDto } from '../models/bsp.model';

export const joinMapper = value => value.join(',');
export const mapJoinMapper = (value, field: string) => value.map(element => element[field]).join(',');
export const mapJoinAndRemoveUnderscoreMapper = (value, field: string) =>
  value.map(element => element[field].replace('_', ' ')).join(', ');
export const dateMapper = (value: Date) => (value ? moment(value).format('YYYY-MM-DD') : null);
export const dateFilterTagMapper = (value: Date) => moment(value).format('DD/MM/YYYY');
export const dateTimeMapper = (value: Date) => value.toISOString().slice(0, 16).replace('T', ' ');
export const rangeDateFilterTagMapper = (value: Array<Date>) =>
  value[1] == null
    ? dateFilterTagMapper(value[0])
    : `${dateFilterTagMapper(value[0])} - ${dateFilterTagMapper(value[1])}`;
export const openRangeDateFilterTagMapper = (value: Array<Date>) =>
  `${dateFilterTagMapper(value[0])} - ${value[1] ? dateFilterTagMapper(value[1]) : '*'}`;

export const isoDateTimeMapper = value => value.toISOString();
export const identityMapper = value => value;
export const booleanStatusMapper = value => (value === 'Active').toString();
export const startOfDayIsoDateTimeMapper = value => moment(value).startOf('day').toISOString();
export const endOfDayIsoDateTimeMapper = value => moment(value).endOf('day').toISOString();

export const bspFilterTagMapper = (value: Bsp | BspDto | Bsp[] | BspDto[]) => {
  const bsps = !Array.isArray(value) ? [value] : value;

  return bsps.map(bsp => `${bsp.name} (${bsp.isoCountryCode})`);
};
export const globalAirlineFilterTagMapper = (globalAirlines: GlobalAirlineSummary[]) =>
  globalAirlines.map(record => `${record.iataCode} / ${record.globalName}`);
export const airlineFilterTagMapper = (airlines: AirlineSummary[]) =>
  airlines.map(record => `${record.code} / ${record.name}`);
export const agentFilterTagMapper = (agents: AgentSummary[]) => agents.map(record => `${record.code} / ${record.name}`);
export const gdsFilterTagMapper = (gdsList: GdsSummary[]) =>
  gdsList.map(record => `${record.gdsCode} / ${record.name}`);

export const rangeFilterTagMapper = (range: number[]) =>
  range
    .map((unit, i) => {
      if (unit != null && range.some(u => u == null)) {
        return i === 0 ? `> ${unit}` : `< ${unit}`;
      } else {
        return unit;
      }
    })
    .filter(unit => unit != null)
    .join(' - ');

export const extractId = (records: Array<{ id: any }>): any[] => records && records.map(record => record?.id);
