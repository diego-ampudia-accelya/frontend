import { omit, times } from 'lodash';

import { SortOrder } from '../enums/sort-order.enum';
import { RequestQuery } from '../models/request-query.model';

import { AggregationOptions, ClientGridTableHelper } from './client-grid-table-helper';

describe('ClientGridTableHelper', () => {
  let helper: ClientGridTableHelper<any>;

  beforeEach(() => {
    helper = new ClientGridTableHelper();
  });

  describe('when paging the items', () => {
    it('should return all items when maximum page size is specified', () => {
      const query = new RequestQuery(0, Number.MAX_SAFE_INTEGER);
      const allItems = times(100, index => ({ index }));

      const actual = helper.query(allItems, query);

      expect(actual.records.length).toBe(100);
      expect(actual.pageSize).toBe(Number.MAX_SAFE_INTEGER);
      expect(actual.total).toBe(100);
      expect(actual.pageNumber).toBe(0);
      expect(actual.totalPages).toBe(1);
    });

    it('should return empty records when an empty items array is specified', () => {
      const query = new RequestQuery(0, 20);

      const actual = helper.query([], query);
      expect(actual.records.length).toBe(0);
      expect(actual.pageNumber).toBe(0);
      expect(actual.pageSize).toBe(20);
      expect(actual.total).toBe(0);
      expect(actual.totalPages).toBe(0);
    });

    it('should return second page when there are 100 items, `pageSize = 20` and `page = 1`', () => {
      const query = new RequestQuery(1, 20);
      const allItems = times(100, index => ({ index }));

      const actual = helper.query(allItems, query);

      expect(actual.records.length).toBe(20);
      expect(actual.pageNumber).toBe(1);
      expect(actual.pageSize).toBe(20);
      expect(actual.total).toBe(100);
      expect(actual.totalPages).toBe(5);
    });
  });

  describe('when sorting', () => {
    it('should sort items in ascending order', () => {
      const query = new RequestQuery(0, 5, 'index', SortOrder.Asc);
      // Get 100 items with index 99 -> 0 (descending order)
      const allItems = times(100, n => ({ index: 99 - n }));
      // Get 5 items with index 1 -> 5 (ascending order)
      const expected = times(5, n => ({ index: n }));

      const actual = helper.query(allItems, query);

      expect(actual.records).toEqual(expected);
    });

    it('should sort items in descending order', () => {
      const query = new RequestQuery(0, 5, 'index', SortOrder.Desc);
      // Get 100 items with index 0 -> 99 (ascending order)
      const allItems = times(100, n => ({ index: n }));
      // Get 5 items with index 99 -> 95 (descending order)
      const expected = times(5, n => ({ index: 99 - n }));

      const actual = helper.query(allItems, query);

      expect(actual.records).toEqual(expected);
    });

    it('should be case insensitive ', () => {
      const query = new RequestQuery(0, 5, 'name', SortOrder.Asc);
      const allItems = [{ name: 'jane' }, { name: 'John' }, { name: 'desmund' }];
      const expected = [{ name: 'desmund' }, { name: 'jane' }, { name: 'John' }];

      const actual = helper.query(allItems, query);

      expect(actual.records).toEqual(expected);
    });
  });

  describe('when filtering', () => {
    it('should find matching items when there is a single filter with one value', () => {
      const query = new RequestQuery(0, 20, null, null, { name: 'Jake' });
      const allItems = [{ name: 'Janet' }, { name: 'Desmund' }, { name: 'Jake' }];
      const expected = [{ name: 'Jake' }];

      const actual = helper.query(allItems, query);

      expect(actual.records).toEqual(expected);
    });

    it('should find matching items when there is a single filter with two values', () => {
      const query = new RequestQuery(0, 20, null, null, { name: ['Jake', 'Desmund'] });
      const allItems = [{ name: 'Janet' }, { name: 'Desmund' }, { name: 'Jake' }];
      const expected = [{ name: 'Desmund' }, { name: 'Jake' }];

      const actual = helper.query(allItems, query);

      expect(actual.records).toEqual(expected);
    });

    it('should find matching items when there are two filters with a single value each', () => {
      const filterBy = { occupation: 'Developer', country: 'Wakanda' };
      const query = new RequestQuery(0, 20, null, null, filterBy);
      const allItems = [
        { name: 'Janet', occupation: 'Paramedic', country: 'USA' },
        { name: 'Desmund', occupation: 'Developer', country: 'Wakanda' },
        { name: 'William', occupation: 'Developer', country: 'USA' },
        { name: 'Jake', occupation: 'Scientist', country: 'Wakanda' }
      ];
      const expected = [jasmine.objectContaining({ name: 'Desmund' })];

      const actual = helper.query(allItems, query);

      expect(actual.records).toEqual(expected);
    });

    it('should return all items when there is a single empty filter', () => {
      const query = new RequestQuery(0, 20, null, null, { name: null });
      const allItems = [{ name: 'Janet' }, { name: 'Desmund' }, { name: 'Jake' }];

      const actual = helper.query(allItems, query);

      expect(actual.records.length).toEqual(3);
    });

    it('should apply custom filter if there is a customFilterMap with filterKey', () => {
      const query = new RequestQuery(0, 20, null, null, { name: 'ja' });
      const allItems = [{ name: 'Janet' }, { name: 'Desmund' }, { name: 'Jake' }, { name: 'Juliet' }];
      let isCalledCustomFilter = false;

      const customFilterMap = {
        name: (item, filterValue) => {
          isCalledCustomFilter = true;

          return item.toLowerCase().includes(filterValue.toLowerCase());
        }
      };

      const actual = helper.query(allItems, query, null, customFilterMap);

      expect(actual.records.length).toEqual(2);
      expect(isCalledCustomFilter).toBe(true);
    });

    it('should not go through custom filter if you not filter by any its properties', () => {
      const query = new RequestQuery(0, 20, null, null, { name: 'ja' });
      const allItems = [{ name: 'Janet' }, { name: 'Desmund' }, { name: 'Jake' }, { name: 'Juliet' }];
      let isCalledCustomFilter = false;

      const customFilterMap = {
        age: (item, filterValue) => {
          isCalledCustomFilter = true;

          return item === filterValue;
        }
      };

      helper.query(allItems, query, null, customFilterMap);

      expect(isCalledCustomFilter).toBe(false);
    });
  });

  describe('when aggregating', () => {
    it('should group items by supplied props and return an aggregated result', () => {
      const query = new RequestQuery(0, 20, null, null);
      const allItems = [
        { name: 'Janet', occupation: 'Paramedic', country: 'USA' },
        { name: 'William', occupation: 'Developer', country: 'USA' },
        { name: 'John', occupation: 'Developer', country: 'USA' },
        { name: 'Desmund', occupation: 'Developer', country: 'Wakanda' },
        { name: 'Jake', occupation: 'Scientist', country: 'Wakanda' },
        { name: 'Lisa', occupation: 'Scientist', country: 'Wakanda' }
      ];
      const aggregator: AggregationOptions<any> = {
        aggregateBy: ['country', 'occupation'],
        aggregate(group: any[]) {
          const first = group[0];

          return { ...omit(first, ['name']), count: group.length };
        }
      };
      const expected = [
        { country: 'USA', occupation: 'Paramedic', count: 1 },
        { country: 'USA', occupation: 'Developer', count: 2 },
        { country: 'Wakanda', occupation: 'Developer', count: 1 },
        { country: 'Wakanda', occupation: 'Scientist', count: 2 }
      ];

      const actual = helper.query(allItems, query, aggregator);

      expect(actual.records).toEqual(expected);
    });
  });
});
