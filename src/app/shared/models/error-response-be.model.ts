export interface ResponseErrorBE {
  timestamp: string;
  errorCode: number;
  errorMajorCode?: string;
  errorId?: string;
  errorMessage: string;
  messages: ResponseErrorMessageBE[];
}

export interface ResponseErrorMessageBE {
  message: string;
  messageCode: string;
  messageParams: Array<{ name: string; value: string }>;
  textParams?: Array<{ name: string; value: string }>;
}
