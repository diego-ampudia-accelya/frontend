/* eslint-disable @typescript-eslint/naming-convention */
export enum GridTableActionType {
  // Basic actions
  Add = 'add',
  Edit = 'edit',
  Modify = 'modify',
  Delete = 'delete',
  Details = 'details',
  Remove = 'remove',
  Download = 'download',
  Archive = 'archive',
  MarkAsRead = 'markAsRead',
  MarkAsUnread = 'markAsUnread',
  View = 'view',
  Loading = 'loading',
  Supervise = 'supervise',
  MoveToActive = 'moveToActive',
  Resubmit = 'resubmit',
  InternalComment = 'internalComment',
  Activity = 'activity',
  CreateParentGroup = 'create_parent',
  CreateChildGroup = 'create_child',
  Create = 'create',
  StartWatching = 'startWatching',
  StopWatching = 'stopWatching',
  // ACDM actions
  DisputeACDM = 'dispute',
  ApproveACDM = 'approveDispute',
  RejectACDM = 'rejectDispute',
  DeactivateACDM = 'deactivateAcdm',
  ReactivateACDM = 'reactivateAcdm',
  ForwardGdsADM = 'forwardGdsAdm',
  EditRejectACDM = 'editRejectAcdm',
  EditReactivateACDM = 'editReactivateAcdm',
  AuthorizeSPCDR = 'autohrizeSpcdr',
  DisputePostBillingACDM = 'disputePostBillingAcdm',
  DeletePostBillingACDM = 'deletePostBillingAcdm',
  AuthorizeRequest = 'authorizeRequest',
  RejectRequest = 'rejectRequest',
  StopProcessing = 'stopProcessing',
  DownloadEvaluationFile = 'downloadEvaluationFile',
  DeactivateADMReason = 'deactivateAdmReason',
  ReactivateADMReason = 'reactivateAdmReason',
  RerunADM = 'rerunAdm',
  ClearanceADM = 'clearanceAdm',
  // Refund actions
  Authorize = 'authorizeEdit',
  Reject = 'reject',
  Investigate = 'investigateEdit',
  Exception = 'exception',
  // TA actions
  ManageTA = 'Manage TA',
  ViewTA = 'View TA',
  // TIP actions
  CheckCardLevelConsents = 'checkCardLevelConsents',
  CheckConsents = 'checkConsents',
  AddModifyCardLevelConsent = 'addModifyCardLevelConsent',
  TerminateCardLevelConsent = 'terminateCardLevelConsent',
  ModifyCardBspLevelConsent = 'modifyCardBspLevelConsent',
  TerminateCardBspLevelConsent = 'terminateCardBspLevelConsent',
  CheckCardConsents = 'checkCardConsents',
  ModifyOwnCard = 'modifyOwnCard',
  TerminateOwnCard = 'terminateOwnCard',
  BspLevelConsentManagement = 'bspLevelConsentManagement',
  AgentLevelConsentManagement = 'agentLevelConsentManagement',
  ModifyProductBspLevelConsent = 'modifyProductBspLevelConsent',
  ModifyProductAgentLevelConsent = 'modifyProductAgentLevelConsent',
  TerminateProductAgentLevelConsent = 'terminateProductAgentLevelConsent',
  // VR actions
  RemoveOverride = 'removeOverride',
  // User actions
  UserProperties = 'userProperties',
  PermissionAndTemplate = 'permissionAndTemplate',
  DeactivateUser = 'deactivateUser',
  ReactivateUser = 'reactivateUser',
  ListOfHOSUs = 'listOfHOSUs',
  ListOfUsers = 'listOfUsers',
  propertiesHOMU = 'propertiesHOMU',
  // Agent group actions
  Members = 'members',
  ModifyIndividualAccess = 'modifyIndividualAccess',
  RemoveMember = 'removeMember',
  CancelInvitation = 'cancelInvitation',
  // Global Airline
  Template = 'template',
  // Billing Statement
  BillingAnalysis = 'Billing Analysis',
  //BSPlink SAFs actions
  SeeSafSeries = 'seeSeries',
  // Monitoring
  HotDetails = 'hotDetails'
}

export enum GridTableActionTypeLigatureNames {
  AddLigatureName = 'add',
  EditLigatureName = 'mode_edit',
  DeleteLigatureName = 'delete',
  DetailsLigatureName = 'visibility',
  ManageTaLigatureName = '',
  CreateParentLigatureName = 'create_parent',
  CreateChildLigatureName = 'create_child',
  BlankLigatureName = '',
  ViewLigatureName = '',
  DisputeACDMLigatureName = 'report',
  Download = 'get_app'
}

export enum GridTableActionMethodNames {
  AddColumn = 'addColumn',
  EditColumn = 'editColumn',
  DeleteColumn = 'deleteColumn',
  ShowDetaiedColumn = 'showDetaiedColumn',
  UserPropertiesEdit = 'userPropertiesEdit',
  PermissionAndTemplateEdit = 'permissionAndTemplateEdit',
  ChangeSubUserStatus = 'changeSubUserStatus'
}

export interface GridTableAction {
  actionType: GridTableActionType;
  ligatureName?: string;
  methodName: string;
  disabled?: boolean | ((row: any) => boolean);
  hidden?: boolean | ((row: any) => boolean);
  group?: string;
}

export function getActionByType(actionTypeObject): GridTableAction {
  switch (actionTypeObject.action) {
    case GridTableActionType.Loading:
      return {
        actionType: actionTypeObject.action,
        methodName: null,
        disabled: true,
        hidden: false
      };
    case GridTableActionType.propertiesHOMU:
    case GridTableActionType.UserProperties:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.BlankLigatureName,
        methodName: GridTableActionMethodNames.UserPropertiesEdit,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.PermissionAndTemplate:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.BlankLigatureName,
        methodName: GridTableActionMethodNames.PermissionAndTemplateEdit,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.DeactivateUser:
    case GridTableActionType.ReactivateUser:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.BlankLigatureName,
        methodName: GridTableActionMethodNames.ChangeSubUserStatus,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.Add:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.AddLigatureName,
        methodName: GridTableActionMethodNames.AddColumn,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.Modify:
    case GridTableActionType.Edit:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.EditLigatureName,
        methodName: GridTableActionMethodNames.EditColumn,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.Delete:
    case GridTableActionType.Remove:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.DeleteLigatureName,
        methodName: GridTableActionMethodNames.DeleteColumn,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.Details:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.DetailsLigatureName,
        methodName: GridTableActionMethodNames.ShowDetaiedColumn,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.CreateParentGroup:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.CreateParentLigatureName,
        methodName: null,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.CreateChildGroup:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.CreateChildLigatureName,
        methodName: null,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.View:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.ViewLigatureName,
        methodName: null,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };

    case GridTableActionType.DisputeACDM:
    case GridTableActionType.ApproveACDM:
    case GridTableActionType.RejectACDM:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.DisputeACDMLigatureName,
        methodName: null,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.Download:
    case GridTableActionType.Members:
    case GridTableActionType.DeactivateACDM:
    case GridTableActionType.ReactivateACDM:
    case GridTableActionType.ForwardGdsADM:
    case GridTableActionType.AuthorizeRequest:
    case GridTableActionType.RejectRequest:
    case GridTableActionType.Archive:
    case GridTableActionType.MarkAsRead:
    case GridTableActionType.MarkAsUnread:
    case GridTableActionType.StartWatching:
    case GridTableActionType.StopWatching:
    case GridTableActionType.Authorize:
    case GridTableActionType.Reject:
    case GridTableActionType.Investigate:
    case GridTableActionType.Exception:
    case GridTableActionType.EditReactivateACDM:
    case GridTableActionType.EditRejectACDM:
    case GridTableActionType.Supervise:
    case GridTableActionType.AuthorizeSPCDR:
    case GridTableActionType.DisputePostBillingACDM:
    case GridTableActionType.DeletePostBillingACDM:
    case GridTableActionType.MoveToActive:
    case GridTableActionType.ModifyIndividualAccess:
    case GridTableActionType.RemoveMember:
    case GridTableActionType.CancelInvitation:
    case GridTableActionType.Resubmit:
    case GridTableActionType.InternalComment:
    case GridTableActionType.Activity:
    case GridTableActionType.CheckCardLevelConsents:
    case GridTableActionType.CheckConsents:
    case GridTableActionType.AddModifyCardLevelConsent:
    case GridTableActionType.TerminateCardLevelConsent:
    case GridTableActionType.ModifyCardBspLevelConsent:
    case GridTableActionType.TerminateCardBspLevelConsent:
    case GridTableActionType.CheckCardConsents:
    case GridTableActionType.ModifyOwnCard:
    case GridTableActionType.TerminateOwnCard:
    case GridTableActionType.BspLevelConsentManagement:
    case GridTableActionType.AgentLevelConsentManagement:
    case GridTableActionType.ModifyProductBspLevelConsent:
    case GridTableActionType.ModifyProductAgentLevelConsent:
    case GridTableActionType.TerminateProductAgentLevelConsent:
    case GridTableActionType.RemoveOverride:
    case GridTableActionType.StopProcessing:
    case GridTableActionType.DownloadEvaluationFile:
    case GridTableActionType.Template:
    case GridTableActionType.DeactivateADMReason:
    case GridTableActionType.ReactivateADMReason:
    case GridTableActionType.RerunADM:
    case GridTableActionType.ClearanceADM:
    case GridTableActionType.ManageTA:
    case GridTableActionType.ListOfHOSUs:
    case GridTableActionType.ListOfUsers:
      return {
        actionType: actionTypeObject.action,
        methodName: null,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
    case GridTableActionType.ViewTA:
    case GridTableActionType.SeeSafSeries:
    case GridTableActionType.HotDetails:
    case GridTableActionType.BillingAnalysis:
      return {
        actionType: actionTypeObject.action,
        ligatureName: GridTableActionTypeLigatureNames.ManageTaLigatureName,
        methodName: null,
        disabled: actionTypeObject.disabled ? actionTypeObject.disabled : null,
        hidden: actionTypeObject.hidden ? actionTypeObject.hidden : null,
        group: actionTypeObject.group ? actionTypeObject.group : null
      };
  }

  // eslint-disable-next-line no-console
  console.warn(`The provided action '${actionTypeObject?.action}' is not supported.`);

  return {
    actionType: null,
    ligatureName: GridTableActionTypeLigatureNames.BlankLigatureName,
    methodName: null,
    disabled: true,
    hidden: true
  };
}
