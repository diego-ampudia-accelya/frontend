export interface FilterMapper {
  attribute: string;
  operation: string;
  valueMapper: (value: any) => string;
}

export interface ViewListServiceConfig {
  filterMappers: { [key: string]: FilterMapper };
}
