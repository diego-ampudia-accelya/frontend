import { Pageable } from '~app/shared/models/pageable.model';
import { Sort } from '~app/shared/models/sort.model';

export class AbstractPageable {
  // indicates if the user is on the first page of the list of items
  first = true;

  // indicates if the user is on the last page of the list of items
  last = true;

  // indicates the current page number. if first, 'number' is 0. If user is on the 3rd page, 'number' is 2
  number = 0;

  // the number of elements per page selected
  numberOfElements = 0;

  pageable: Pageable;

  // the actual number of elements shown
  size = 0;

  // indicates if the table is sorted or not
  sort: Sort;

  totalElements = 0;
  totalPages = 0;
}
