import { SortOrder } from '../enums/sort-order.enum';

// TODO: Object suffix is redundand. Think of better naming.
export interface SortingObject {
  attribute: string;
  sortType: SortOrder;
}

export interface OwnSortingObject {
  prop: string | number;
  dir: string;
}
