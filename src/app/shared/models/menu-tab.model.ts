import { Params } from '@angular/router';

import { UserType } from '~app/shared/models/user.model';

export interface MenuTab {
  url: string;
  path: string;
  permissionId: string | string[];
  sequenceNumber?: number;
  parent: string;
  tabLabel?: string | MenuTabLabelParameterized;
  label?: string;
  id?: string;
  uniqueIdFromUrlRegex?: string;
  showMenu?: boolean;
  getTabLabel?: (routeData: any) => string | MenuTabLabelParameterized;
  allowedUserTypes?: UserType | UserType[];
  tooltipLabel?: string;
  showBadge?: boolean;
  persistent?: boolean;
  queryParams?: Params;
}

export interface MenuTabLabelParameterized {
  label: string;
  params: any;
}
