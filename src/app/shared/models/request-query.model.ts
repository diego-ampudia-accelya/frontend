import { DataQuery } from '../components/list-view';
import { FiltersHelper } from '../helpers/filters.helper';
import { PAGINATION } from '~app/shared/components/pagination/pagination.constants';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { RequestQueryBody } from '~app/shared/models/request-query-body.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { SortingObject } from '~app/shared/models/sorting-object.model';

// `[key: string]: any` is for possible extra download option properties
type RequestQueryFilterParameters<T> = Partial<T> & { [key: string]: any };

export class RequestQuery<T = any> {
  public get page(): number {
    return this._page;
  }
  public set page(value: number) {
    if (value < 0) {
      value = PAGINATION.FIRST_PAGE;
    }

    this._page = value;
  }

  // `size` property
  public size: number;

  // `sortBy` property
  public sortBy: string;

  public get sortOrder(): SortOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: SortOrder) {
    this._sortOrder = value || SortOrder.Asc;
  }

  public get filterBy(): RequestQueryFilter[] | RequestQueryFilterParameters<T> {
    return this._filterBy;
  }
  public set filterBy(value: RequestQueryFilter[] | RequestQueryFilterParameters<T>) {
    this._filterBy = value || [];
  }

  public reduceFilterBy = false;

  // `page` property
  private _page: number;
  // `sortOrder` property
  private _sortOrder: SortOrder;
  // `filterBy` property
  private _filterBy: RequestQueryFilter[] | RequestQueryFilterParameters<T>;

  constructor(
    page?: number,
    size?: number,
    sortBy?: string,
    sortOrder?: SortOrder,
    filterBy?: RequestQueryFilter[] | RequestQueryFilterParameters<T>,
    reduceFilterBy?: boolean
  ) {
    this.page = page;
    this.size = size;
    this.sortBy = sortBy;
    this.sortOrder = sortOrder;
    this.filterBy = filterBy;
    this.reduceFilterBy = reduceFilterBy;
  }

  /** Factory method to instance `RequestQuery` with just a data query */
  public static fromDataQuery<U>(dataQuery: Partial<DataQuery<U>>): RequestQuery<U> {
    const { filterBy, paginateBy, sortBy } = dataQuery;
    const { page, size } = paginateBy || {};
    const sort = sortBy ? sortBy[0] : ({} as SortingObject);

    return new RequestQuery<U>(page, size, sort?.attribute, sort?.sortType, filterBy);
  }

  public getQueryString(options: { omitPaging?: boolean } = {}): string {
    let queryParams: any = {};

    if (!options.omitPaging) {
      queryParams = {
        page: this.page,
        size: this.size
      };
    }

    if (this.reduceFilterBy) {
      const filters = FiltersHelper.createQueryString(this.filterBy);

      queryParams = {
        ...queryParams,
        sort: this.sortBy == null ? null : `${this.sortBy}::${this.sortOrder}`,
        filters
      };
    } else {
      queryParams = {
        ...queryParams,
        sort: this.sortBy == null ? null : [this.sortBy, this.sortOrder],
        ...this.filterBy
      };
    }

    return this.encodeAsQueryString(queryParams);
  }

  public getBody(): RequestQueryBody {
    const filterBy = Array.isArray(this.filterBy)
      ? this.filterBy.filter(RequestQueryFilter.hasAttribute)
      : this.filterBy;

    return {
      filterBy,
      paginateBy: { page: this.page, size: this.size },
      sortBy: this.getSortQuery()
    };
  }

  /**
   * Encodes an object with query parameters into a query string.
   *
   * @param queryParams A key-value map.
   */
  private encodeAsQueryString(queryParams: unknown): string {
    return (
      Object.entries(queryParams)
        .filter(([_, value]) => this.hasValue(value))
        .map(([key, value]) => {
          const values: any[] = Array.isArray(value) ? value : [value];
          const encodedValue = values.map(encodeURIComponent).join(',');

          return `&${key}=${encodedValue}`;
        })
        .join('')
        // Add '?' in the beginning if the query string is not empty
        .replace('&', '?')
    );
  }

  private hasValue(value: string | []): boolean {
    return !(value == null || value.length === 0);
  }

  private getSortQuery(): SortingObject[] {
    // TODO: Extend the sorting if we need to support multiple sort.
    return this.sortBy ? [{ attribute: this.sortBy, sortType: this.sortOrder }] : [];
  }
}
