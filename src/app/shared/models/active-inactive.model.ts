/* eslint-disable @typescript-eslint/naming-convention */
export enum ActiveInactive {
  Active = 'Active',
  Inactive = 'Inactive'
}

export enum ActiveInactiveInUpperCase {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}
