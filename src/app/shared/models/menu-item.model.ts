import { UserType } from './user.model';
import { MenuStatus } from '~app/shared/enums/menu-status.enum';

export class MenuItem {
  key: string;
  label: string;
  parent: any;
  children: MenuItem[] = [];
  check: MenuStatus;
  tabID: string;
  permissionID: string;
  allowedUserTypes?: UserType | UserType[];
  showMenu: boolean;
  route: string;
  enabled: boolean;
  active?: boolean;
  isClassicMenuItem?: boolean;
  openOnClick?: boolean;
  loading?: boolean;
}
