export interface Bsp {
  id: number;
  name: string;
  isoCountryCode: string;
  effectiveFrom?: string;
  effectiveTo?: string;
  version?: number;
  active?: boolean;
  gdsAssignments?: Array<any>;
  currencyAssignments?: Array<any>;
  defaultCurrencyCode?: string;
}

export interface BspDto {
  id: number;
  isoCountryCode?: string;
  name?: string;
}
