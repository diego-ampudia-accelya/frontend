/* eslint-disable @typescript-eslint/naming-convention */
export interface WatcherModel {
  documentType: WatcherDocType;
  documentId: number;
  watching: boolean;
}

export enum WatcherDocType {
  REFUND = 'REFUND',
  ACDM = 'ACDM',
  PBD = 'PBD'
}
