export interface ButtonMenuOption {
  id?: string;
  label: string;
  isDisabled?: boolean;
  group?: string;
  hidden?: boolean;
  command?: () => void;
}
