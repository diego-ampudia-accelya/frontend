export interface BspPermissions {
  bspId: number;
  permissions: string[];
}

export interface PermissionSaveBEModel {
  id: number;
  enabled: boolean;
}

export interface Permission {
  id: number;
  name: string;
  enabled: boolean;
  editable: boolean;
  permissionCategory: PermissionCategory;
}

export interface PermissionCategory {
  id: number;
  name: string;
  version: number;
}
