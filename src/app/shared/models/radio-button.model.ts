export interface RadioButton {
  label: string;
  value: any;
  disabled?: boolean;
}
