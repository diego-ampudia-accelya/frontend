export interface Currency {
  id: number;
  code: string;
  decimals: number;
  version?: number;
  isDefault?: boolean;
}
