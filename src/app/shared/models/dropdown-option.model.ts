export interface DropdownOption<T = any> {
  value: T;
  label: string; //* Label to be shown in dropdown and as selected (if no labelSelected is provided)
  labelSelected?: string; //* Label to be shown when item is selected
  disabled?: boolean;
}
