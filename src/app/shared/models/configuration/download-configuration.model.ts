export interface DownloadConfiguration {
  adm?: boolean;
  acm?: boolean;
  admd?: boolean;
  acmd?: boolean;
  spdr?: boolean;
  spcr?: boolean;
  refundApp?: boolean;
  refundNotice?: boolean;
}
