import { CacheConfiguration } from './cache-configuration.model';
import { DownloadConfiguration } from './download-configuration.model';
import { ImageConfiguration } from './image-configuration.model';
import { LanguagesConfiguration } from './languages-configuration.model';
import { RetryConfiguration } from './retry-configuration.model';
import { SafsConfiguration } from './safs-configuration.model';
import { SubUserManagementConfiguration } from './sub-user-management-configuration.model';
import { TrainingSessionConfiguration } from '~app/shared/models/configuration/training_session_configuration.model';
import { HelpConfiguration } from '~app/shared/models/configuration/help-configuration.model';
import { AuthConfiguration } from '~app/shared/models/configuration/auth-configuration.model';
import { ApiConfiguration } from '~app/shared/models/configuration/api-configuration.model';

export interface AppConfiguration {
  api: ApiConfiguration;
  enableL10nFallback: boolean;
  environmentLabel: string;
  auth: AuthConfiguration;
  environment: string;
  cache: CacheConfiguration;
  image: ImageConfiguration;
  trainingSession: TrainingSessionConfiguration;
  classicBspLinkUrl: string;
  disableLogoutRedirect: boolean;
  useNewPDFStyle: boolean;
  trainingBspCodes: string[];
  disabledPermissions: string[];
  subUserManagement: SubUserManagementConfiguration;
  retry: RetryConfiguration;
  training?: boolean;
  privacyLink?: string;
  customerServicesLink?: string;
  safs: SafsConfiguration;
  help: HelpConfiguration;
  detailedDownload: DownloadConfiguration;
  languages: LanguagesConfiguration[];
}
