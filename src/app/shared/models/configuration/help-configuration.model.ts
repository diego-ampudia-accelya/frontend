export interface HelpConfiguration {
  securePath: string;
  publicPath: string;
}
