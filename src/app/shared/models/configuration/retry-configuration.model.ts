import { RetryRule } from '~app/shared/models/configuration/retry-rule.model';

export interface RetryConfiguration {
  /**
   * Base time in milliseconds to wait before retrying.
   */
  delay: number;
  /**
   * Maximum retry attempts before treating an error as unrecoverable
   */
  maxAttempts: number;

  rules: RetryRule[];
}
