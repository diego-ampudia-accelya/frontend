export interface LanguagesConfiguration {
  name: string;
  locale: string;
}
