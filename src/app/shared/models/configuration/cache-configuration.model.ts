export interface CacheConfiguration {
  maxAge: number;
  maxCacheCount: number;
}
