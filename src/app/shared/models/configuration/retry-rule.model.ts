export interface RetryRule {
  url: string;
  methods: string[];
  statusCodes: number[];
}
