export interface TrainingSessionConfiguration {
  duration: number;
  expiresIn: number;
}
