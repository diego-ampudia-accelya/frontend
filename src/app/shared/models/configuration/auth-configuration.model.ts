/* eslint-disable @typescript-eslint/naming-convention */
export interface AuthConfiguration {
  url: string;
  urlRedirectToken: string;
  enabled: boolean;
  grant_type_authorization: string;
  grant_type_refresh: string;
  client_id: string;
  client_secret: string;
  redirect_uri: string;
  login_uri: string;
  logout_uri: string;
  logout_request_uri?: string;
}
