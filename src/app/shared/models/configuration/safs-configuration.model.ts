export interface SafsConfiguration {
  maxSupportedItems: number;
  delayAfterArchive: number;
}
