export interface ApiConfiguration {
  protocol: string;
  host: string;
  uploadPath?: string;
}
