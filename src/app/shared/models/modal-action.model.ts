// TODO: Create buttons interface, which will extend ModalAction. Remove unneded keys for ModalAction
export interface ModalAction {
  type: string;
  title?: string;
  buttonDesign?: string;
  isDisabled?: boolean;
  isLoading?: boolean;
  tooltipText?: string;
}
