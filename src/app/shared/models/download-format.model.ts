/* eslint-disable @typescript-eslint/naming-convention */
export enum DownloadFormat {
  CSV = 'csv',
  TXT = 'txt'
}
