/**
 * Format being used by Angular I18nPluralPipe:
 *
 * http://userguide.icu-project.org/formatparse/messages
 */
export interface IcuMessageFormat {
  [k: string]: string;
  '=0'?: string;
  '=1'?: string;
  other?: string;
}
