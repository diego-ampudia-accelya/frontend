export interface AgentDto {
  id: number;
  iataCode: string;
  name: string;
}
