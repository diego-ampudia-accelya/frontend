export interface UserLanguage {
  id: number;
  language: string;
}
