/* eslint-disable @typescript-eslint/naming-convention */
export enum UserTypeForFilter {
  Airline = 'Airline',
  Agent = 'Agent',
  IATA = 'IATA',
  DPC = 'DPC',
  EARS = 'EARS',
  GDS = 'GDS',
  ThirdParty = 'Third Party',
  AgentGroup = 'Agent Group'
}
