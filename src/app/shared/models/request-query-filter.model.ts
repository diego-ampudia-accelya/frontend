export class RequestQueryFilter {
  constructor(public attribute: string, public attributeValue: any, public operation = '=') {}

  static hasAttribute(filter: RequestQueryFilter): boolean {
    return filter.attribute != null;
  }
}
