export interface PermissionCategoryBE {
  id?: number;
  code: string;
  name: string;
}

export interface UserPermission<T> {
  id: number;
  code: string;
  name: string;
  category: PermissionCategoryBE;
  enabled: boolean;
  template: T;
}

export type UserPermissionDto<T> = Omit<UserPermission<T>, 'category'> & { catgory: PermissionCategoryBE };

export interface UserPermissionForUpdate {
  id: number;
  enabled: boolean;
}
