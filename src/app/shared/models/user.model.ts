/* eslint-disable @typescript-eslint/naming-convention */
import { Agent, GlobalAirline } from '~app/master-data/models';
import { Gds } from '~app/master-data/models/gds.model';
import { ThirdParty } from '~app/master-data/models/third-party.model';
import { Bsp } from '~app/shared/models/bsp.model';
import { BspPermissions } from '~app/shared/models/permissions.model';

export interface UserDetails {
  email: string;
  firstName: string;
  lastName: string;
  membershipCount: number;
}

export interface UserIataDetails {
  iataCode: string;
  organizationName: string;
}

export enum UserType {
  IATA = 'iata',
  AGENT = 'agent',
  AIRLINE = 'airline',
  GDS = 'gds',
  THIRD_PARTY = 'third_party',
  AGENT_GROUP = 'agent_group',
  DPC = 'dpc'
}

export interface UserBasicInfo {
  id?: number;
  email: string;
  userType: UserType;
  firstName: string;
  lastName: string;
  registerDate?: string;
  expiryDate?: string;
}

export interface UserInfo extends UserBasicInfo {
  language?: string;
  telephone?: string;
  organization?: string;
  active?: boolean;
  address?: string;
  locality?: string;
  city?: string;
  zip?: string;
  country?: string;
  version?: number;
  bspPermissions?: Array<BspPermissions>;
  permissions?: Array<string>;
  roles?: Array<string>;
  iataCode?: string;
  isSuperUser?: boolean;
  userInternalId?: number;
  bsps?: Bsp[];
  isImpersonating?: boolean;
  template?: string;
  defaultIsoc?: string;
  ip?: string;
}

export interface UserInfoEditable {
  name: string;
  organization: string;
  telephone: string;
  country: string;
  city: string;
  address: string;
  postCode: string;
}

export interface MainUserInfo {
  id: number;
  userCode: string;
  userType: number;
  organization: string;
}

export interface DpcUser extends UserInfo {
  userType: UserType.DPC;
}
export interface IataUser extends UserInfo {
  userType: UserType.IATA;
}

export interface IataUserEditModel extends UserInfo {
  userType: UserType.IATA;
}

export interface AgentUser extends UserInfo {
  userType: UserType.AGENT;
  agent: Agent;
}

export interface AgentUserEditModel extends UserInfo {
  userType: UserType.AGENT;
  agentId: number;
}

export interface AirlineUser extends UserInfo {
  userType: UserType.AIRLINE;
  globalAirline: GlobalAirline;
  globalTemplate?: string;
}

export interface AirlineUserEditModel extends UserInfo {
  userType: UserType.AIRLINE;
  globalAirlineId: number;
}

export interface GdsUser extends UserInfo {
  userType: UserType.GDS;
  gds: Gds;
}

export interface GdsUserEditModel extends UserInfo {
  userType: UserType.GDS;
  gdsId: number;
}

export interface ThirdPartyUser extends UserInfo {
  userType: UserType.THIRD_PARTY;
  thirdParty: ThirdParty;
}

export interface AgentGroupUser extends UserInfo {
  userType: UserType.AGENT_GROUP;
}

export interface ThirdPartyUserEditModel extends UserInfo {
  userType: UserType.THIRD_PARTY;
  thirdPartyId: number;
}

export interface UserCreateModel extends UserBasicInfo {
  globalAirlineId?: number;
  agentId?: number;
  gdsId?: number;
}

export type User = IataUser | AirlineUser | AgentUser | GdsUser | ThirdPartyUser | AgentGroupUser | DpcUser;
