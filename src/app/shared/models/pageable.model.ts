import { Sort } from '~app/shared/models/sort.model';

export class Pageable {
  offset = 0;
  pageNumber = 0;
  pageSize = 0;
  paged = true;
  sort: Sort;
  unpaged = true;
}
