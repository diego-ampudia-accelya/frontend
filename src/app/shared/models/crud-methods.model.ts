import { Observable } from 'rxjs';

import { PagedData } from '~app/shared/models/paged-data.model';
import { RequestQuery } from '~app/shared/models/request-query.model';

export interface CrudMethods<T> {
  getOne(id: number): Observable<T>;

  getAll(requestQuery: RequestQuery): Observable<PagedData<T>>;

  save(id?: any, data?: any): Observable<T[]>;

  delete(id: number): void;
}
