export interface UserAccount {
  id: string;
  isoc: string;
  code: string;
  name: string;
  type: string;
  template: string;
}

export interface UserAccountBsp {
  id: number;
  isoc: string;
}
