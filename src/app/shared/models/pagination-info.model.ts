export interface PaginationInfo {
  // The number of elements per page
  size?: number;

  // The current page
  page?: number;

  // The total number of elements
  totalElements?: number;
}
