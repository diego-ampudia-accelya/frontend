export interface PagedData<T> {
  records: Array<T>;
  pageSize: number;
  total: number;
  totalPages?: number;
  pageNumber: number;
}
