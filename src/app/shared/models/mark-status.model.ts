/* eslint-disable @typescript-eslint/naming-convention */
export enum MarkStatus {
  Read = 'READ',
  Unread = 'UNREAD',
  Archive = 'ARCHIVED'
}

export enum MarkStatusFilter {
  Read = 'READ',
  Unread = 'UNREAD'
}

export interface MarkStatusUpdateModel {
  id: number;
  markStatus: MarkStatus;
}
