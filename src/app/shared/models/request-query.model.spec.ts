/* eslint-disable no-prototype-builtins */
import { TestBed } from '@angular/core/testing';
import { SortOrder } from '~app/shared/enums/sort-order.enum';
import { RequestQuery } from '~app/shared/models/request-query.model';
import { EQ } from '../constants/operations';

describe('RequestQuery', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be initialized with default values', () => {
    const requestQuery = new RequestQuery();

    expect(requestQuery.page).toBe(undefined);
    expect(requestQuery.size).toBe(undefined);
    expect(requestQuery.sortBy).toBe(undefined);
    expect(requestQuery.sortOrder).toBe('ASC');
    expect(requestQuery.filterBy).toEqual([]);
  });

  it('should set page to 0 if negative page is provided', () => {
    const requestQuery = new RequestQuery(-1000);

    expect(requestQuery.page).toBe(0);
  });

  describe('getQueryString returned query string', () => {
    it('should contain page=0 if page 0 is provided', () => {
      const requestQuery = new RequestQuery(0);

      expect(requestQuery.getQueryString()).toContain('page=0');
    });

    it('should contain page=1 if page 1 is provided (should not increment/decrement the page)', () => {
      const requestQuery = new RequestQuery(1);

      expect(requestQuery.getQueryString()).toContain('page=1');
    });

    it('should not contain page=x if page is null/undefined', () => {
      const requestQuery = new RequestQuery(null);

      expect(requestQuery.getQueryString()).not.toContain('page=');
    });

    it('should not contain size=x if size is null/undefined', () => {
      const requestQuery = new RequestQuery(1, null);

      expect(requestQuery.getQueryString()).not.toContain('size=');
    });

    it('should not contain sort=x if sortBy is null/undefined', () => {
      const requestQuery = new RequestQuery(1, 10, null);

      expect(requestQuery.getQueryString()).not.toContain('sort=');
    });

    it('should be empty string if all properties are null/undefined', () => {
      const requestQuery = new RequestQuery(null, null, null);

      expect(requestQuery.getQueryString()).toBe('');
    });

    it('should be empty string if any property is defined', () => {
      const requestQuery = new RequestQuery();

      expect(requestQuery.getQueryString()).toBe('');
    });

    it('should be valid query string', () => {
      const requestQuery = new RequestQuery(1, 20, 'name', SortOrder.Desc);

      expect(requestQuery.getQueryString()).toBe('?page=1&size=20&sort=name,DESC');
    });

    it('should omit paging information when `omitPaging` option is enabled', () => {
      const requestQuery = new RequestQuery(1, 20, 'name', SortOrder.Desc);

      expect(requestQuery.getQueryString({ omitPaging: true })).toBe('?sort=name,DESC');
    });

    describe('when using filterBy', () => {
      let requestQuery: RequestQuery;

      beforeEach(() => {
        requestQuery = new RequestQuery(null, null, null);
      });

      it('should correctly append the sort parameter', () => {
        requestQuery.sortBy = 'name';

        expect(requestQuery.getQueryString()).toContain('name,ASC');
      });

      it('should contain "id=1" when id is 1', () => {
        requestQuery.filterBy = { id: 1 };

        expect(requestQuery.getQueryString()).toContain('id=1');
      });

      it('should contain "id=1,2" when id is [1, 2]', () => {
        requestQuery.filterBy = { id: [1, 2] };

        expect(requestQuery.getQueryString()).toContain('id=1,2');
      });

      it('should be empty string if all properties are undefined', () => {
        expect(requestQuery.getQueryString()).toBe('');
      });

      it('should be empty stirng if "id" is null', () => {
        requestQuery.filterBy = { id: null };

        expect(requestQuery.getQueryString()).toBe('');
      });

      it('should be empty string if "id" is []', () => {
        requestQuery.filterBy = { id: [] };

        expect(requestQuery.getQueryString()).toBe('');
      });

      it('should return properly formatted query string containing filters', () => {
        requestQuery.filterBy = { id: 1, agentCode: ['abc', 'xyz'], bspCode: null };

        expect(requestQuery.getQueryString()).toBe('?id=1&agentCode=abc,xyz');
      });
    });

    describe('when using filters get', () => {
      let requestQuery: RequestQuery;

      beforeEach(() => {
        requestQuery = new RequestQuery(null, null, null);
        requestQuery.reduceFilterBy = true;
      });

      it('should correctly append the sort parameter', () => {
        requestQuery.sortBy = 'name';

        expect(requestQuery.getQueryString()).toContain('name%3A%3AASC');
      });
    });

    describe('should encode special characters', () => {
      let queryToEncode: RequestQuery;
      beforeEach(() => {
        queryToEncode = new RequestQuery(null, null, null);
      });

      it('when ":" provided', () => {
        queryToEncode.filterBy = { param: ':' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%3A');
      });

      it('when ":" provided and has multiple values', () => {
        queryToEncode.filterBy = { param: ['val1:', 'val2:'] };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=val1%3A,val2%3A');
      });

      it('when "{}" provided', () => {
        queryToEncode.filterBy = { param: '{val}' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%7Bval%7D');
      });

      it('when "<>" provided', () => {
        queryToEncode.filterBy = { param: '<>' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%3C%3E');
      });

      it('when "#%" provided', () => {
        queryToEncode.filterBy = { param: '#%' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%23%25');
      });

      it('when "|\\" provided', () => {
        queryToEncode.filterBy = { param: '|\\' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%7C%5C');
      });

      it('when "[]" provided', () => {
        queryToEncode.filterBy = { param: '[]' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%5B%5D');
      });

      it('when "?:" provided', () => {
        queryToEncode.filterBy = { param: '?:' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%3F%3A');
      });

      it('when "=&," provided', () => {
        queryToEncode.filterBy = { param: '=&,' };
        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?param=%3D%26%2C');
      });

      it('when when used with reduceFilterBy', () => {
        queryToEncode.reduceFilterBy = true;
        queryToEncode.sortBy = 'param';
        queryToEncode.filterBy = [
          {
            attribute: 'param',
            attributeValue: 'value & value',
            operation: EQ
          }
        ];

        const actual = queryToEncode.getQueryString();

        expect(actual).toEqual('?sort=param%3A%3AASC&filters=param%3A%3Aeq%3A%3Avalue%20%26%20value');
      });
    });
  });

  describe('getBody returned object', () => {
    it('should contain the correct keys', () => {
      const requestQuery = new RequestQuery();
      const body = requestQuery.getBody();

      expect(body.hasOwnProperty('filterBy')).toBe(true);
      expect(body.hasOwnProperty('paginateBy')).toBe(true);
      expect(body.hasOwnProperty('sortBy')).toBe(true);
      expect(Object.keys(body).length).toBe(3);
    });
  });
});
