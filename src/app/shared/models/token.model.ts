/* eslint-disable @typescript-eslint/naming-convention */
export interface Token {
  access_token: string;
  expires_in: number;
  refresh_token: string;
  refresh_expires_in: number;
  token_type: string;
  id_token: string;
  not_before_policy: number;
  session_state: string;
  scope: string;
  expires_at?: string;
}
