/* eslint-disable @typescript-eslint/naming-convention */
export enum UserTemplate {
  Efficient = 'Efficient',
  Streamlined = 'Streamlined'
}

export enum UserTemplateInUpperCase {
  Streamlined = 'STREAMLINED',
  Efficient = 'EFFICIENT'
}
