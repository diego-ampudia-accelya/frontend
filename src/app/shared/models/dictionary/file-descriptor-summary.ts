export interface FileDescriptorSummary {
  id: number;
  descriptor: string;
  description: string;
}
