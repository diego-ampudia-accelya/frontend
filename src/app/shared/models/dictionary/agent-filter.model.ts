export interface AgentDictionaryQueryFilters {
  bspId?: number | number[];
  active?: boolean;
  permission?: string;
  agentGroupId?: number;
}

export enum AgentDictionaryFiltersAttributes {
  bspId = 'bsp.id',
  active = 'active',
  permission = 'permission',
  agentGroupId = 'agentGroupId'
}
