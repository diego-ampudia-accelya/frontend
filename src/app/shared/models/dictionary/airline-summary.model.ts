export interface AirlineSummary {
  id: number;
  name: string;
  code: string;
  designator: string;
}
