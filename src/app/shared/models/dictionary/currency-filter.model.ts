export interface CurrencyDictionaryQueryFilters {
  bspId?: number | number[];
}

export enum CurrencyDictionaryFiltersAttributes {
  bspId = 'bsp.id'
}
