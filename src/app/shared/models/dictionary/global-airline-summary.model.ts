export interface GlobalAirlineSummary {
  id: number;
  iataCode: string;
  globalName: string;
}
