export class GdsSummary {
  id: number;
  gdsCode: string;
  name: string;
  version: number;
}
