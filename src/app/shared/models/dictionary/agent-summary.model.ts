import { Bsp } from '../bsp.model';

export interface AgentSummary {
  id: string; // TODO id is actually a number
  name: string;
  code: string;
  status?: string;
  bsp?: Partial<Bsp>;
}
