export class ThirdPartySummary {
  id: number;
  name: string;
  code: string;
}
