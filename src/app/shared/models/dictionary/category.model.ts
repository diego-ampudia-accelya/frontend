export interface Category {
  id: number;
  primaryReason?: string;
  subReason?: string;
}
