import { BspDto } from '../bsp.model';

export interface AgentGroupSummary {
  id: number;
  code: string;
  name: string;
  bsp?: BspDto;
}

export interface AgentGroupDictionaryQueryFilters {
  bspId?: number | number[];
}

export enum AgentGroupDictionaryFiltersAttributes {
  bspId = 'bsp.id'
}
