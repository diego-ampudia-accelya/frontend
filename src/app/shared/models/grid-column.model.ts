/* eslint-disable @typescript-eslint/naming-convention */
import { TableColumn } from '@swimlane/ngx-datatable';
import { Observable } from 'rxjs';

import { BadgeInfoType } from '../enums/badge-information-type';

import { DropdownOption } from './dropdown-option.model';

// TODO: Investigate how not to run all these functions on every DOM event in the `grid-table` rows
export interface GridColumn extends TableColumn {
  hidden?: boolean;
  tooltip?: (value: any) => string;
  badgeInfo?: BadgeInfo;
  image?: (value: any) => {
    src: string;
    tooltipLabel?: string;
  };
  dropdownOptions?: Observable<DropdownOption[]>;
  icon?: (value?: any) => {
    name: string;
    class?: string;
    /** Corresponds to the class added to the icon in `grid-table` styles */
    type?: IconType;
    hidden: boolean;
    tooltip?: string;
  };
}

export interface BadgeInfo {
  hidden: (value: any) => boolean;
  value?: string;
  type: (value: any) => BadgeInfoType;
  tooltipLabel?: (value: any) => string;
  showIconType?: boolean;
}

export interface GridColumnMultipleLinkClick<T> {
  row: T;
  propertyName: string;
  propertyValue: any;
}

export enum IconType {
  Info = 'info',
  Warning = 'warning',
  Error = 'error'
}
