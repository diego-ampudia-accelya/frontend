import { PaginationInfo } from '~app/shared/models/pagination-info.model';
import { RequestQueryFilter } from '~app/shared/models/request-query-filter.model';
import { SortingObject } from '~app/shared/models/sorting-object.model';

export interface RequestQueryBody {
  filterBy: RequestQueryFilter[] | unknown;
  paginateBy: PaginationInfo;
  sortBy: SortingObject[];
}
