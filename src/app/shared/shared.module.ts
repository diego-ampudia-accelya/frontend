import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { L10nTranslationModule } from 'angular-l10n';
import { PowerBIEmbedModule } from 'powerbi-client-angular';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputSwitchModule } from 'primeng/inputswitch';
import { MultiSelectModule } from 'primeng/multiselect';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SliderModule } from 'primeng/slider';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';

import { AlertMessageComponent } from './components/alert-message/alert-message.component';
import { BadgeInformationComponent } from './components/badge-information/badge-information.component';
import { ButtonMenuComponent } from './components/button-menu/button-menu.component';
import { CardComponent } from './components/card/card.component';
import { ChartCardComponent } from './components/chart/chart-card/chart-card.component';
import { ChartModule } from './components/chart/chart.module';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { InsertionDirective } from './components/dialog/insertion.directive';
import { EditableItemComponent } from './components/editable-list/editable-item/editable-item.component';
import { EditableListComponent } from './components/editable-list/editable-list.component';
import { PropertyPathAccessPipe } from './components/editable-list/pipes/property-path-access.pipe';
import { FileUploadOverlayComponent } from './components/file-upload-overlay/file-upload-overlay.component';
import { FilterTemplateComponent } from './components/filter-template/filter-template.component';
import { DownloadFileComponent } from './components/form-dialog-components/download-file/download-file.component';
import { InfoDialogComponent } from './components/form-dialog-components/info-dialog/info-dialog.component';
import { InputSelectComponent } from './components/input-select/input-select.component';
import {
  ListViewComponent,
  ListViewFilterDirective,
  ListViewGridDirective,
  ListViewTitleDirective
} from './components/list-view';
import { RecordsLimitDialogComponent } from './components/list-view/records-limit-dialog/records-limit-dialog.component';
import { LockedStateComponent } from './components/locked-state/locked-state.component';
import { LoginAsAnyGroupMemberDialogComponent } from './components/login-as-any-group-member-dialog/login-as-any-group-member-dialog.component';
import { LoginAsAnyUserDialogComponent } from './components/login-as-any-user-dialog/login-as-any-user-dialog.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { OptionsListComponent } from './components/options-list/options-list.component';
import { PeriodPickerComponent, PeriodPickerPanelComponent } from './components/period-picker';
import { PillInformationComponent } from './components/pill-information/pill-information.component';
import { PowerBIComponent } from './components/power-bi/power-bi.component';
import { RangeDropdownComponent } from './components/range-dropdown/range-dropdown.component';
import { ReturnToOriginalProfileDialogComponent } from './components/return-to-original-profile-dialog/return-to-original-profile-dialog.component';
import { RouteNotFoundDialogComponent } from './components/route-not-found-dialog/route-not-found-dialog.component';
import { SelectionListComponent } from './components/selection-list/selection-list.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { SplitbuttonModule } from './components/split-button/split-button.module';
import { StepInputComponent } from './components/step-input/step-input.component';
import { SwitchAccountDialogComponent } from './components/switch-account-dialog/switch-account-dialog.component';
import { TableSelectionToolbarComponent } from './components/table-selection-toolbar/table-selection-toolbar.component';
import { TabMenuComponent } from './components/tabs/tab-menu/tab-menu.component';
import { TabLinkComponent } from './components/tabs/tab-navbar/tab-link/tab-link.component';
import { TabNavbarComponent } from './components/tabs/tab-navbar/tab-navbar.component';
import { TagsInputComponent } from './components/tags-input/tags-input.component';
import { TextCollapsableModule } from './components/text-collapsable/text-collapsable.module';
import { TextareaComponent } from './components/textarea/textarea.component';
import { ToggleComponent } from './components/toggle/toggle.component';
import { UserInputListComponent } from './components/user-input-list/user-input-list.component';
import { AutoFocusDirective } from './directives/autofocus.directive';
import { DisableControlDirective } from './directives/disable-control.directive';
import { DragAndDropFilesDirective } from './directives/drag-and-drop-files.directive';
import { HasOverflowDirective } from './directives/has-overflow.directive';
import { IsEllipsisActiveDirective } from './directives/is-ellipsis-active.directive';
import { MiddleClickDirective } from './directives/middle-click.directive';
import { ThrottleClickDirective } from './directives/throttle-click.directive';
import { AbsoluteDecimalPipe } from './pipes/absolute-decimal.pipe';
import { AmountFormatPipe } from './pipes/amount-format.pipe';
import { BytesFormatPipe } from './pipes/bytes-format.pipe';
import { EmptyPipe } from './pipes/empty.pipe';
import { JoinPipe } from './pipes/join.pipe';
import { PeriodPipe } from './pipes/period.pipe';
import { DateTimeFormatPipe } from '~app/shared/pipes/date-time-format.pipe';
import { TooltipDirectiveModule } from '~app/shared/directives/tooltip/tooltip.module';
import { InnerRouteLinkDirective } from '~app/shared/directives/inner-route-link.directive';
import { ViewListComponent } from '~app/shared/components/view-list/view-list.component';
import { VerticalStepperItemModule } from '~app/shared/components/vertical-stepper-item/vertical-stepper-item.module';
import { UploadComponent } from '~app/shared/components/upload/upload.component';
import { FileCardComponent } from '~app/shared/components/upload/file-card/file-card.component';
import { TagComponent } from '~app/shared/components/tag/tag.component';
import { TabComponent } from '~app/shared/components/tabs/tab/tab.component';
import { TabGroupComponent } from '~app/shared/components/tabs/tab-group/tab-group.component';
import { SelectComponent } from '~app/shared/components/select/select.component';
import { SearchPanelComponent } from '~app/shared/components/search-panel/search-panel.component';
import { RadioButtonComponent } from '~app/shared/components/radio-button/radio-button.component';
import { RadioButtonGroupComponent } from '~app/shared/components/radio-button-group/radio-button-group.component';
import { PaginationComponent } from '~app/shared/components/pagination/pagination.component';
import { NotificationComponent } from '~app/shared/components/notifications/notifications.component';
import { InputComponent } from '~app/shared/components/input/input.component';
import { GridTableComponent } from '~app/shared/components/grid-table/grid-table.component';
import { UnsavedChangesComponent } from '~app/shared/components/form-dialog-components/unsaved-changes/unsaved-changes.component';
import { TctpConfirmationDialogComponent } from '~app/shared/components/form-dialog-components/tctp-confirmation-dialog/tctp-confirmation-dialog.component';
import { ConfirmationDialogComponent } from '~app/shared/components/form-dialog-components/confirmation-dialog/confirmation-dialog.component';
import { ErrorFieldComponent } from '~app/shared/components/error-field/error-field.component';
import { DeactivateAccountDialogComponent } from '~app/shared/components/deactivate-account-dialog/deactivate-account-dialog.component';
import { CollapsableComponent } from '~app/shared/components/collapsable/collapsable.component';
import { CheckboxComponent } from '~app/shared/components/checkbox/checkbox.component';
import { ChangeLanguageDialogComponent } from '~app/shared/components/change-language-dialog/change-language-dialog.component';
import { ButtonComponent } from '~app/shared/components/buttons/button/button.component';
import { AutocompleteComponent } from '~app/shared/components/autocomplete/autocomplete.component';
import { AccordionModule } from '~app/shared/components/accordion/accordion.module';
import {
  BackToTopBtnComponent,
  ChangesDialogComponent,
  DialogService,
  FloatingPanelComponent,
  IconButtonComponent,
  PermissionErrorDialogComponent,
  PropertyComponent,
  PropertyListComponent,
  SimpleTableComponent
} from '~app/shared/components';
import { CompanyCalendarGuard } from '~app/master-data/company-calendar/company-calendar.guard';
import { CompanyCalendarDialogComponent } from '~app/master-data/company-calendar/company-calendar-dialog/company-calendar-dialog.component';
import { HeaderComponent } from '~app/core/components/header/header.component';
import { HasPermissionPipe } from '~app/auth/pipes/has-permission.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    L10nTranslationModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    TooltipModule,
    NgxDatatableModule,
    RadioButtonModule,
    TooltipDirectiveModule,
    AccordionModule,
    SplitbuttonModule,
    VerticalStepperItemModule,
    NgSelectModule,
    NgbPopoverModule,
    OverlayModule,
    PortalModule,
    ChartModule,
    TextCollapsableModule,
    CalendarModule,
    ToastModule,
    OverlayPanelModule,
    SliderModule,
    InputSwitchModule,
    PowerBIEmbedModule,
    InputNumberModule
  ],
  declarations: [
    TagsInputComponent,
    CollapsableComponent,
    TabComponent,
    TabGroupComponent,
    TabMenuComponent,
    GridTableComponent,
    TableSelectionToolbarComponent,
    SearchPanelComponent,
    HeaderComponent,
    InputComponent,
    PaginationComponent,
    TagComponent,
    ButtonComponent,
    IconButtonComponent,
    ErrorFieldComponent,
    CheckboxComponent,
    AutocompleteComponent,
    BackToTopBtnComponent,
    CardComponent,
    DialogComponent,
    DownloadFileComponent,
    ViewListComponent,
    UploadComponent,
    FileCardComponent,
    InsertionDirective,
    ThrottleClickDirective,
    DisableControlDirective,
    RadioButtonComponent,
    RadioButtonGroupComponent,
    TctpConfirmationDialogComponent,
    ConfirmationDialogComponent,
    InfoDialogComponent,
    PermissionErrorDialogComponent,
    UnsavedChangesComponent,
    UserInputListComponent,
    SpinnerComponent,
    HasOverflowDirective,
    IsEllipsisActiveDirective,
    AutoFocusDirective,
    TextareaComponent,
    InputSelectComponent,
    SelectComponent,
    SideMenuComponent,
    OptionsListComponent,
    ListViewComponent,
    ListViewTitleDirective,
    ListViewFilterDirective,
    ListViewGridDirective,
    MiddleClickDirective,
    DragAndDropFilesDirective,
    FloatingPanelComponent,
    SimpleTableComponent,
    AlertMessageComponent,
    DatepickerComponent,
    PeriodPickerComponent,
    ButtonMenuComponent,
    FileUploadOverlayComponent,
    PeriodPickerPanelComponent,
    HasPermissionPipe,
    PeriodPipe,
    BytesFormatPipe,
    DateTimeFormatPipe,
    AmountFormatPipe,
    ChangesDialogComponent,
    SelectionListComponent,
    NotificationComponent,
    AbsoluteDecimalPipe,
    BadgeInformationComponent,
    PillInformationComponent,
    PowerBIComponent,
    NotFoundComponent,
    RouteNotFoundDialogComponent,
    ChangeLanguageDialogComponent,
    LoginAsAnyUserDialogComponent,
    LoginAsAnyGroupMemberDialogComponent,
    ReturnToOriginalProfileDialogComponent,
    SwitchAccountDialogComponent,
    PropertyListComponent,
    PropertyComponent,
    EmptyPipe,
    JoinPipe,
    TabNavbarComponent,
    TabLinkComponent,
    RecordsLimitDialogComponent,
    InnerRouteLinkDirective,
    EditableListComponent,
    EditableItemComponent,
    PropertyPathAccessPipe,
    RangeDropdownComponent,
    ToggleComponent,
    FilterTemplateComponent,
    LockedStateComponent,
    ChartCardComponent,
    StepInputComponent,
    DeactivateAccountDialogComponent,
    CompanyCalendarDialogComponent
  ],
  providers: [
    DatePipe,
    BytesFormatPipe,
    DialogService,
    DateTimeFormatPipe,
    PeriodPipe,
    EmptyPipe,
    DecimalPipe,
    AmountFormatPipe,
    CompanyCalendarGuard
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    TagsInputComponent,
    NotFoundComponent,
    CollapsableComponent,
    TabComponent,
    TabGroupComponent,
    TabMenuComponent,
    GridTableComponent,
    TableSelectionToolbarComponent,
    PaginationComponent,
    TagComponent,
    FormsModule,
    ReactiveFormsModule,
    SearchPanelComponent,
    InputComponent,
    HeaderComponent,
    MultiSelectModule,
    TooltipModule,
    ButtonComponent,
    IconButtonComponent,
    ErrorFieldComponent,
    CheckboxComponent,
    AutocompleteComponent,
    BackToTopBtnComponent,
    NgSelectModule,
    CardComponent,
    DialogComponent,
    ThrottleClickDirective,
    DisableControlDirective,
    DownloadFileComponent,
    ViewListComponent,
    UploadComponent,
    RadioButtonComponent,
    RadioButtonModule,
    RadioButtonGroupComponent,
    L10nTranslationModule,
    TooltipDirectiveModule,
    TctpConfirmationDialogComponent,
    ConfirmationDialogComponent,
    InfoDialogComponent,
    UnsavedChangesComponent,
    AccordionModule,
    UserInputListComponent,
    SpinnerComponent,
    HasOverflowDirective,
    IsEllipsisActiveDirective,
    AutoFocusDirective,
    SplitbuttonModule,
    TextareaComponent,
    VerticalStepperItemModule,
    InputSelectComponent,
    SelectComponent,
    SideMenuComponent,
    OptionsListComponent,
    ListViewComponent,
    ListViewTitleDirective,
    ListViewFilterDirective,
    ListViewGridDirective,
    MiddleClickDirective,
    DragAndDropFilesDirective,
    FloatingPanelComponent,
    SimpleTableComponent,
    AlertMessageComponent,
    NgbPopoverModule,
    DatepickerComponent,
    PeriodPickerComponent,
    ButtonMenuComponent,
    FileUploadOverlayComponent,
    ChartModule,
    TextCollapsableModule,
    DateTimeFormatPipe,
    HasPermissionPipe,
    PeriodPipe,
    AbsoluteDecimalPipe,
    BytesFormatPipe,
    AmountFormatPipe,
    ChangesDialogComponent,
    SelectionListComponent,
    NotificationComponent,
    BadgeInformationComponent,
    PillInformationComponent,
    PowerBIComponent,
    PropertyListComponent,
    PropertyComponent,
    EmptyPipe,
    JoinPipe,
    TabNavbarComponent,
    TabLinkComponent,
    InnerRouteLinkDirective,
    EditableListComponent,
    RangeDropdownComponent,
    ToggleComponent,
    FilterTemplateComponent,
    LockedStateComponent,
    ChartCardComponent,
    StepInputComponent
  ]
})
export class SharedModule {}
