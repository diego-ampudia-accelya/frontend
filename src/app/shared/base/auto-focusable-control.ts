import { AfterViewInit, Directive, Input } from '@angular/core';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AutoFocusableControl implements AfterViewInit {
  @Input() autofocus = false;

  public ngAfterViewInit(): void {
    if (this.autofocus) {
      this.focusElement();
    }
  }

  public abstract focusElement(): void;
}
