import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { L10nConfig, L10nLoader, L10nProvider, L10nTranslationLoader } from 'angular-l10n';
import { defaultsDeep } from 'lodash/fp';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { version } from '~app/../../package.json';
import { AppConfigurationService } from '~app/shared/services';

export const l10nConfig: L10nConfig = {
  format: 'language-region',
  providers: [{ name: 'app', asset: './assets/i18n/locale-', options: { version } }],
  cache: true,
  keySeparator: '.',
  defaultLocale: { language: 'en' },
  schema: []
};

export function initL10n(l10nLoader: L10nLoader): () => Promise<void> {
  return () => l10nLoader.init();
}

@Injectable()
export class HttpTranslationLoader implements L10nTranslationLoader {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  private enTranslations: any;

  constructor(@Optional() private http: HttpClient, private appConfigurationService: AppConfigurationService) {}

  public get(language: string, provider: L10nProvider): Observable<{ [key: string]: any }> {
    const url = `${provider.asset}${language}.json`;

    const options = {
      headers: this.headers,
      params: {
        v: provider.options.version,
        noAuth: 'true'
      }
    };

    return this.http.get(url, options).pipe(
      tap((translations: any) => {
        if (language === 'en') {
          this.enTranslations = translations;
        }
      }),
      map(translations => {
        const shouldFallback = this.appConfigurationService.enableL10nFallback && language !== 'en';

        return shouldFallback ? defaultsDeep(this.enTranslations, translations) : translations;
      })
    );
  }
}
