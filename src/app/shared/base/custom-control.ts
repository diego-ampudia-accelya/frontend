/* eslint-disable @typescript-eslint/ban-types */
import { AfterViewInit, ChangeDetectorRef, Directive, HostBinding, Input } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { L10nTranslationService } from 'angular-l10n';
import { generate } from 'shortid';

import { AutoFocusableControl } from './auto-focusable-control';
import { FormUtil } from '~app/shared/helpers/form-helpers/util/form-util.helper';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class CustomControl extends AutoFocusableControl implements ControlValueAccessor, AfterViewInit {
  @HostBinding('class.control--empty') get isControlEmpty() {
    return !(this.value && this.value.toString().trim());
  }

  @HostBinding('class.control--showErrors') get areErrorsShown() {
    return FormUtil.areErrorsShown(this.ngControl, this.showErrorsOn, this.locked);
  }

  @Input() id: string = generate();
  @Input() label: string;
  @Input() placeholder = '';
  @Input() name: string;

  //* The control will be disabled (see Angular docs to know the implications)
  @HostBinding('class.control--disabled')
  @Input()
  get isDisabled() {
    return this.ngControl ? this.ngControl.disabled : false;
  }
  set isDisabled(isDisabled: boolean) {
    if (!this.ngControl) {
      //* Disabled attribute does not have sense if there is no control.
      //* To avoid confusion we set the locked property in that case
      this.locked = isDisabled;

      return;
    }

    // TODO Fix Angular 9 issue https://github.com/angular/angular/issues/35330
    //! control property not available until ngOnChanges
    const control = this.ngControl.control;
    if (control) {
      if (isDisabled) {
        control.disable();
      } else {
        control.enable();
      }
    }
  }

  //* The control will work normally but will not allow modifications (will be shown as usual as well)
  @HostBinding('class.control--readonly')
  @Input()
  isReadOnly = false;

  //* The control will work normally but will not allow modifications (will be shown with gray background)
  @HostBinding('class.control--locked')
  @Input()
  locked = false;

  @Input() tabindex: number;
  @Input() showErrorsOn: 'dirty' | 'touched';
  @Input() customErrorMessages: { [validatorType: string]: string } = {}; // TODO Could be replace by a custom validator. Revert eventually (commit abf612b6) if this is only used once

  @Input()
  get value(): any {
    return this._value;
  }
  set value(value: any) {
    this._value = value;
    this.propagateChange(this._value);
  }
  protected _value: any;

  /**
   * Gets updateOn value from control or `blur` if none defined.
   *
   * Do you want to set it? Do it the right way when building the control/group/array `formBuilder.control('', { updateOn: 'change' })`
   */
  get updateOn() {
    return FormUtil.getUpdateOn(this.ngControl);
  }

  public get isEnabled(): boolean {
    return !this.isDisabled && !this.locked && !this.isReadOnly;
  }

  constructor(
    public ngControl: NgControl,
    protected cd: ChangeDetectorRef,
    protected translationService: L10nTranslationService
  ) {
    super();

    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  public propagateTouched: Function = () => {
    //This is intentional
  }; //* Needed in template for compatibility with updateOn: 'blur' option

  public ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  public getErrorMessage(): string {
    return FormUtil.getErrorMessage(this.ngControl, this.translationService, this.customErrorMessages);
  }

  /**
   * Forces showing the status of the control. Basically it marks the control as `dirty` or `touched` (default) depending on configuration
   */
  public showState(): void {
    FormUtil.showControlState(this.ngControl, this.showErrorsOn);
  }

  public writeValue(value: any) {
    if (value !== undefined) {
      this._value = value;
    }
  }

  public registerOnChange(fn: Function) {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: Function) {
    this.propagateTouched = fn;
  }

  public setDisabledState(isDisabled: boolean) {
    if (this.isDisabled !== isDisabled) {
      this.isDisabled = isDisabled;
    }
  }

  protected propagateChange: Function = () => {
    //This is intentional
  };
}
