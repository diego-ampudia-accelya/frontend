import { createAction, props } from '@ngrx/store';

import { DataQuery } from '~app/shared/components/list-view';
import { MarkStatus, MarkStatusUpdateModel } from '~app/shared/models/mark-status.model';
import { PagedData } from '~app/shared/models/paged-data.model';

export function search<U>(key: string) {
  return createAction(
    `[${key}] Search`,
    props<{
      query?: DataQuery<U>;
      predefinedFilters?: Partial<U>;
      initialFilters?: Partial<U>;
      initialSortingAttribute?: keyof U;
    }>()
  );
}

export function searchSuccess<T>(key: string) {
  return createAction(`[${key}] Search Success`, props<{ data: PagedData<T> }>());
}

export function searchError(key: string) {
  return createAction(`[${key}] Search Error`);
}

export function searchOne(key: string) {
  return createAction(`[${key}] Search One`, props<{ id: number }>());
}

export function searchOneSuccess<T>(key: string) {
  return createAction(`[${key}] Search One Success`, props<{ item: T }>());
}

export function searchOneError(key: string) {
  return createAction(`[${key}] Search One Error`);
}

export function rowsSelectionChange<T>(key: string) {
  return createAction(`[${key}] Rows Selection Change`, props<{ data: T[] }>());
}

export function clearRowsSelection(key: string) {
  return createAction(`[${key}] Clear Rows Selection`);
}

export function markReadUnreadStatus<T>(key: string) {
  return createAction(`[${key}] Mark Read/Unread Status`, props<{ data: T[]; markStatus: MarkStatus }>());
}

export function markReadUnreadStatusSuccess(key: string) {
  return createAction(`[${key}] Mark Read/Unread Status Success`, props<{ data: MarkStatusUpdateModel[] }>());
}

export function markReadUnreadStatusError(key: string) {
  return createAction(`[${key}] Mark Read/Unread Status Error`);
}

export function markArchiveMoveToActive<T>(key: string) {
  return createAction(`[${key}] Mark as`, props<{ data: T[]; markStatus: MarkStatus }>());
}

export function markArchiveMoveToActiveSuccess<T>(key: string) {
  return createAction(`[${key}] Mark Archive/Move to active successful`, props<{ data: T[] }>());
}

export function markArchiveMoveToActiveError<T>(key: string) {
  return createAction(`[${key}] Mark Archive/Move to active error`);
}

export function updateWatcher<T>(key: string) {
  return createAction(`[${key}] Update watcher`, props<{ data: T[]; watching: boolean }>());
}

export function updateWatcherSuccess<T>(key: string) {
  return createAction(`[${key}] Update watcher success`, props<{ data: T[] }>());
}

export function updateWatcherError(key: string) {
  return createAction(`[${key}] Update watcher error`);
}
