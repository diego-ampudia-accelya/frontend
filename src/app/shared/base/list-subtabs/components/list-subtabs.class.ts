import { Directive, OnDestroy, OnInit } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { DefaultProjectorFn, MemoizedSelector, select, Store } from '@ngrx/store';
import { L10nTranslationService } from 'angular-l10n';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, delay, first, map, switchMap, take, takeUntil, tap, withLatestFrom } from 'rxjs/operators';

import { ViewListActions } from '~app/core/actions';
import { getActiveViewListState } from '~app/core/reducers';
import { ListSubtabsActions } from '~app/shared/base/list-subtabs/actions';
import * as fromListSubtabs from '~app/shared/base/list-subtabs/reducers';
import { ListSubtabsState } from '~app/shared/base/list-subtabs/reducers/list-subtabs.reducer';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { isEmpty } from '~app/shared/components/list-view/utils';
import { MarkStatus, MarkStatusUpdateModel } from '~app/shared/models/mark-status.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { WatcherModel } from '~app/shared/models/watcher.model';
import { NotificationService } from '~app/shared/services';
import { appConfiguration } from '~app/shared/services/app-configuration.service';
import { rethrowError } from '~app/shared/utils/rethrow-error-operator';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class ListSubtabs<T, U> implements OnInit, OnDestroy {
  public predefinedFilters: Partial<U>;
  public loading$: Observable<boolean>;
  public loadingItem$: Observable<boolean>;
  public data$: Observable<T[]>;
  public selectedItem$: Observable<T>;
  public selectedRowsCount$: Observable<number>;
  public selectedRows$: Observable<T[]>;
  public query$: Observable<DataQuery<U>>;
  public queryTotalItems$: Observable<number>;
  public hasData$: Observable<boolean>;
  public maxSupportedItems = appConfiguration.safs.maxSupportedItems;

  protected initialSortAttribute: keyof U;
  protected initialFilters: Partial<U>;
  protected destroy$ = new Subject<any>();

  constructor(
    protected store: Store,
    protected baseListService: any,
    protected actions$: Actions,
    protected translationService?: L10nTranslationService,
    protected notificationService?: NotificationService
  ) {}

  /** IMPORTANT: If this method is implemented in a child class, `super` must be called to also create `ListSubtabs` instance. */
  public ngOnInit(): void {
    this.initializeListObs();
    this.initializeSearchEffects();
    this.initializeMarkReadUnreadStatusEffects();
    this.initializeMarkArchiveMoveToActiveEffects();
    this.initializeWatcherEffects();
    this.emitInitialSearch();
  }

  public rowClassesModifier = (dataItem: any, classes: string[]): void => {
    if (dataItem.markStatus === MarkStatus.Unread) {
      classes.push('row-unread');
    }
    if (dataItem.isRowSelected) {
      classes.push('selected-row');
    }
  };

  public onQueryChanged(query?: DataQuery<U>): void {
    this.store.dispatch(ListSubtabsActions.search(this.getListKey())({ query }));
  }

  public onSelectedItemChanged(id: number): void {
    this.store.dispatch(ListSubtabsActions.searchOne(this.getListKey())({ id }));
  }

  public onRowsSelectionChange(data: T[]): void {
    this.store.dispatch(ListSubtabsActions.rowsSelectionChange(this.getListKey())({ data }));
  }

  public clearSelectedRows(): void {
    this.store.dispatch(ListSubtabsActions.clearRowsSelection(this.getListKey())());
  }

  /** IMPORTANT: If this method is implemented in a child class, `super` must be called to also remove `ListSubtabs` instance. */
  public ngOnDestroy(): void {
    this.store.dispatch(ListSubtabsActions.clearRowsSelection(this.getListKey())());
    this.destroy$.next();
  }

  protected adaptSearchQuery(query: DataQuery<U>): DataQuery<U> {
    // Override if needed in component class
    return query;
  }

  protected setMarkStatus(markStatus: MarkStatus): void {
    this.selectedRows$.subscribe(data => {
      this.store.dispatch(ListSubtabsActions.markReadUnreadStatus<T>(this.getListKey())({ data, markStatus }));
    });
  }

  protected markAsArchive(): void {
    this.selectedRows$.subscribe(data => {
      this.store.dispatch(
        ListSubtabsActions.markArchiveMoveToActive<T>(this.getListKey())({
          data,
          markStatus: MarkStatus.Archive
        })
      );
    });
  }

  protected moveToActive(): void {
    this.selectedRows$.subscribe(data => {
      this.store.dispatch(
        ListSubtabsActions.markArchiveMoveToActive<T>(this.getListKey())({
          data,
          markStatus: MarkStatus.Read
        })
      );
    });
  }

  private initializeListObs(): void {
    const listSelector = this.getListSelector();

    this.loading$ = this.store.pipe(select(fromListSubtabs.getListSubtabsLoading(listSelector)));
    this.loadingItem$ = this.store.pipe(select(fromListSubtabs.getListSubtabsLoadingItem(listSelector)));
    this.data$ = this.store.pipe(select(fromListSubtabs.getListSubtabsData<T, U>(listSelector)));
    this.selectedItem$ = this.store.pipe(select(fromListSubtabs.getListSubtabsActiveSelectedItem<T, U>(listSelector)));
    this.selectedRowsCount$ = this.store.pipe(
      select(fromListSubtabs.getListSubtabsSelectedRowsCount<T, U>(listSelector))
    );
    this.selectedRows$ = this.store.pipe(
      select(fromListSubtabs.getListSubtabsSelectedRows<T, U>(listSelector)),
      take(1)
    );
    this.query$ = this.store.pipe(select(fromListSubtabs.getListSubtabsQuery<T, U>(listSelector)));
    this.hasData$ = this.store.pipe(select(fromListSubtabs.listSubtabsHasData(listSelector)));
    this.queryTotalItems$ = this.store.pipe(select(fromListSubtabs.getListSubtabsQueryTotalItems<T, U>(listSelector)));
  }

  private initializeMarkReadUnreadStatusEffects(): void {
    const listKey = this.getListKey();

    const markReadUnreadStatus$ = this.actions$.pipe(
      ofType(ListSubtabsActions.markReadUnreadStatus(listKey)),
      switchMap(({ data, markStatus }) => this.baseListService.setMarkStatus(data, markStatus)),
      tap((data: MarkStatusUpdateModel[]) => {
        this.notifyMarkReadUnreadSuccess(data);
        this.store.dispatch(ListSubtabsActions.markReadUnreadStatusSuccess(listKey)({ data }));
      }),
      catchError(error => {
        this.store.dispatch(ListSubtabsActions.markReadUnreadStatusError(listKey)());

        markReadUnreadStatus$.subscribe();

        return throwError(error);
      }),
      takeUntil(this.destroy$)
    );

    markReadUnreadStatus$.subscribe();
  }

  private initializeSearchEffects(): void {
    const key = this.getListKey();
    const listSelector = this.getListSelector();

    const search$ = this.actions$.pipe(
      ofType(ListSubtabsActions.search(key)),
      withLatestFrom(this.store.pipe(select(fromListSubtabs.getListSubtabsQuery(listSelector)))),
      map(([, query]) => this.adaptSearchQuery(query)),
      switchMap(query => this.baseListService.find(query)),
      tap(data => this.store.dispatch(ListSubtabsActions.searchSuccess<T>(key)({ data: data as PagedData<T> }))), // Side effect
      catchError(error => {
        this.store.dispatch(ListSubtabsActions.searchError(key)());
        search$.subscribe();

        return throwError(error);
      }),
      takeUntil(this.destroy$)
    );

    search$.subscribe();

    const searchOne$ = this.actions$.pipe(
      ofType(ListSubtabsActions.searchOne(key)),
      withLatestFrom(this.store.pipe(select(fromListSubtabs.getListSubtabsSelectedItems(listSelector)))),
      switchMap(([{ id }, selectedItems]) => {
        // The entity's id can be retrieved as string from BE
        // eslint-disable-next-line eqeqeq
        const item = (selectedItems as any).find(selectedItem => selectedItem.id == id);

        return item ? of(item) : this.baseListService.getOne(id);
      }),
      tap(item => this.store.dispatch(ListSubtabsActions.searchOneSuccess(key)({ item }))), // Side effect
      catchError(error => {
        this.store.dispatch(ListSubtabsActions.searchOneError(key)());

        searchOne$.subscribe();

        return throwError(error);
      }),
      takeUntil(this.destroy$)
    );

    searchOne$.subscribe();
  }

  private notifyMarkReadUnreadSuccess(data: MarkStatusUpdateModel[]): void {
    if (this.notificationService && this.translationService) {
      const itemsCount = data?.length || 0;
      const translationKey = itemsCount === 1 ? 'singleItemMarkReadUnread' : 'multipleItemsMarkReadUnread';

      const message = this.translationService.translate(translationKey, {
        itemsCount,
        markStatus: String(data[0].markStatus).toLowerCase()
      });

      this.notificationService.showSuccess(message);
    }
  }

  private initializeMarkArchiveMoveToActiveEffects(): void {
    const key = this.getListKey();

    const markArchiveMoveToActive$ = this.actions$.pipe(
      ofType(ListSubtabsActions.markArchiveMoveToActive(key)),
      switchMap(({ data, markStatus }) => this.baseListService.setMarkStatus(data, markStatus)),
      tap((data: MarkStatusUpdateModel[]) => {
        this.store.dispatch(ListSubtabsActions.markArchiveMoveToActiveSuccess<MarkStatusUpdateModel>(key)({ data }));
      }),
      rethrowError(() => {
        this.store.dispatch(ListSubtabsActions.markArchiveMoveToActiveError(key)());
        markArchiveMoveToActive$.subscribe();
      }),
      takeUntil(this.destroy$)
    );

    markArchiveMoveToActive$.subscribe();

    this.actions$
      .pipe(
        ofType(ListSubtabsActions.markArchiveMoveToActiveSuccess<MarkStatusUpdateModel>(key)),
        delay(appConfiguration.safs.delayAfterArchive),
        tap(({ data }) => {
          this.notifyMarkArchiveMoveToActiveSuccess(data);
          this.store.dispatch(ListSubtabsActions.search(key)({}));
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private initializeWatcherEffects(): void {
    const key = this.getListKey();

    const updateWatcher$ = this.actions$.pipe(
      ofType(ListSubtabsActions.updateWatcher(key)),
      switchMap(({ data, watching }) => this.baseListService.updateWatchers(data, watching)),
      tap((data: WatcherModel[]) => {
        this.store.dispatch(ListSubtabsActions.updateWatcherSuccess(key)({ data }));
      }),
      rethrowError(() => {
        this.store.dispatch(ListSubtabsActions.updateWatcherError(key)());
        updateWatcher$.subscribe();
      }),
      takeUntil(this.destroy$)
    );

    updateWatcher$.subscribe();

    this.actions$
      .pipe(
        ofType(ListSubtabsActions.updateWatcherSuccess<WatcherModel>(key)),
        tap(({ data }) => {
          this.notifyUpdateWatcherSuccess(data);
          this.store.dispatch(ListSubtabsActions.search(key)({}));
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  private notifyMarkArchiveMoveToActiveSuccess(data: MarkStatusUpdateModel[]): void {
    if (this.translationService && this.notificationService) {
      const markStatus = data[0]?.markStatus;
      const status = this.translationService.translate(markStatus === MarkStatus.Archive ? 'archived' : 'activated');
      const dataLength = data.length;

      const message = this.translationService.translate(
        dataLength === 1 ? 'singleArchiveActivateSuccess' : 'multipleArchiveActivateSuccess',
        {
          number: dataLength,
          status: status.toLocaleLowerCase()
        }
      );

      this.notificationService.showSuccess(message);
    }
  }

  private notifyUpdateWatcherSuccess(data: WatcherModel[]): void {
    if (this.translationService && this.notificationService) {
      let translationKey;
      const watching = data[0]?.watching;
      const dataLength = data.length;

      if (watching) {
        translationKey = dataLength === 1 ? 'singleStartWatchingSuccess' : 'multipleStartWatchingSuccess';
      } else {
        translationKey = dataLength === 1 ? 'singleStopWatchingSuccess' : 'multipleStopWatchingSuccess';
      }

      const message = this.translationService.translate(translationKey, {
        number: dataLength
      });

      this.notificationService.showSuccess(message);
    }
  }

  /** Checking if there are filters in viewList and applying them */
  private emitInitialSearch(): void {
    this.store
      .pipe(
        select(getActiveViewListState),
        first(),
        map(viewList => {
          let result = null;

          if (viewList) {
            const [viewListId, { filter }] = viewList;
            result = this.getViewListQueryByFilter(viewListId, filter);
          }

          return result;
        }),
        tap(viewList => {
          if (viewList?.query) {
            this.resetViewListFilters(viewList.id);
          }
        })
      )
      .subscribe(viewList => {
        this.store.dispatch(
          ListSubtabsActions.search<U>(this.getListKey())({
            query: viewList?.query as DataQuery<U>,
            predefinedFilters: this.predefinedFilters,
            initialFilters: this.initialFilters,
            initialSortingAttribute: this.initialSortAttribute
          })
        );
      });
  }

  private getViewListQueryByFilter(viewListId: string, filter: any): { id: string; query: DataQuery } {
    return {
      id: viewListId,
      query: this.getQuery(filter)
    };
  }

  private getQuery(filter: any): DataQuery {
    /** Only if filter is null, a 'See All' link in dashboard has been clicked and subtab query filter has to be removed (defaultQuery is returned) */
    if (filter === null) {
      return defaultQuery;
    }

    /** If filter is not empty, an standard link applying filters in dashboard has been clicked. The corresponding subtab query filter is updated with the new one */
    if (!isEmpty(filter)) {
      return { ...defaultQuery, filterBy: { ...defaultQuery.filterBy, ...filter } };
    }

    /** Null is returned when there is no viewList in app state so there has been no dashboard click, the subtab state takes the previous stored query */
    return null;
  }

  private resetViewListFilters(viewListId: string): void {
    this.store.dispatch(new ViewListActions.FilterChange({ viewListId, filter: {} }));
    this.store.dispatch(new ViewListActions.FilterFormValueChange({ viewListId, value: {} }));
  }

  protected abstract getListSelector(): MemoizedSelector<
    any,
    ListSubtabsState<T, U>,
    DefaultProjectorFn<ListSubtabsState<T, U>>
  >;

  protected abstract getListKey(): string;
}
