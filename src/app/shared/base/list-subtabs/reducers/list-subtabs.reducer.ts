import { Action, ActionReducer, createReducer, on } from '@ngrx/store';
import { isEmpty, omit } from 'lodash';

import { ListSubtabsActions } from '../actions';
import { MasterDataType } from '~app/master-shared/models/master.model';
import { DataQuery } from '~app/shared/components/list-view';
import { defaultQuery } from '~app/shared/components/list-view/list-view/constants.config';
import { SortOrder } from '~app/shared/enums';
import { MarkStatusUpdateModel } from '~app/shared/models/mark-status.model';
import { PagedData } from '~app/shared/models/paged-data.model';
import { SortingObject } from '~app/shared/models/sorting-object.model';

export function getKey(itemType: MasterDataType, subTabId: string): string {
  return `${itemType}-${subTabId}-list`;
}

export interface ListSubtabsState<T, U> {
  query: DataQuery<U>;
  data: T[];
  selectedItems: T[];
  activeSelectedItem: T;
  loading: boolean;
  loadingItem: boolean;
}

export const initialState: ListSubtabsState<any, any> = {
  query: defaultQuery,
  data: [],
  selectedItems: [],
  activeSelectedItem: null,
  loading: false,
  loadingItem: false
};

function loadQuery<T, U>(
  state: ListSubtabsState<T, U>,
  query: DataQuery<U>,
  predefinedFilters: Partial<U>,
  initialFilters: Partial<U>,
  initialSortingAttribute: keyof U
): ListSubtabsState<T, U> {
  query = query || state.query;

  return {
    ...state,
    query: {
      ...query,
      filterBy: applyFilters(query.filterBy, predefinedFilters, initialFilters),
      sortBy: applyInitialSort(query.sortBy, initialSortingAttribute)
    },
    loading: true
  };
}

function applyFilters<U>(filterBy: Partial<U>, predefinedFilters: Partial<U>, initialFilters: Partial<U>): Partial<U> {
  return { ...initialFilters, ...predefinedFilters, ...filterBy };
}

function applyInitialSort<U>(sortBy: SortingObject[], initialSortingAttribute: keyof U): SortingObject[] {
  const defaultSorting = {
    attribute: initialSortingAttribute as string,
    sortType: SortOrder.Desc
  };

  return isEmpty(sortBy) && initialSortingAttribute ? [defaultSorting] : sortBy;
}

function loadData<T, U>(state: ListSubtabsState<T, U>, data: PagedData<T>): ListSubtabsState<T, U> {
  return {
    ...state,
    loading: false,
    data: data.records,
    selectedItems: [],
    activeSelectedItem: null,
    query: {
      ...state.query,
      paginateBy: {
        page: data.pageNumber,
        size: data.pageSize,
        totalElements: data.total
      }
    }
  };
}

function clearDataIsRowSelected<T, U>(state: ListSubtabsState<T, U>): ListSubtabsState<T, U> {
  const data = state.data.map((dataItem: T) => omit(dataItem as any, 'isRowSelected') as T);

  return { ...state, data };
}

function loadItemQuery<T, U>(state: ListSubtabsState<T, U>): ListSubtabsState<T, U> {
  return {
    ...state,
    activeSelectedItem: null,
    loadingItem: true
  };
}

function loadItem<T, U>(state: ListSubtabsState<T, U>, item: T): ListSubtabsState<T, U> {
  return {
    ...state,
    loadingItem: false,
    activeSelectedItem: item,
    selectedItems: state.selectedItems.some(selectedItem => (selectedItem as any).id === (item as any).id)
      ? state.selectedItems
      : [...state.selectedItems, item]
  };
}

function setDataMarkReadUnread<T, U>(
  state: ListSubtabsState<T, U>,
  readUnreadData: MarkStatusUpdateModel[]
): ListSubtabsState<T, U> {
  const rows = state.data;

  return {
    ...state,
    loading: false,
    data: rows.map((dataItem: any) => {
      const updateDataItem = readUnreadData.find(item => item.id === dataItem.id);

      if (updateDataItem) {
        return { ...dataItem, markStatus: updateDataItem.markStatus };
      }

      return dataItem;
    })
  };
}

export function reducer<T, U>(key: string): ActionReducer<ListSubtabsState<T, U>, Action> {
  return createReducer(
    initialState,
    on(
      ListSubtabsActions.search<U>(key),
      (state, { query, predefinedFilters, initialFilters, initialSortingAttribute }) =>
        loadQuery<T, U>(state, query, predefinedFilters, initialFilters, initialSortingAttribute)
    ),
    on(ListSubtabsActions.searchSuccess<T>(key), (state, { data }) => loadData<T, U>(state, data)),
    on(ListSubtabsActions.rowsSelectionChange<T>(key), (state, { data }) => ({ ...state, data })),
    on(ListSubtabsActions.clearRowsSelection(key), state => clearDataIsRowSelected<T, U>(state)),
    on(ListSubtabsActions.searchError(key), ListSubtabsActions.markReadUnreadStatusError(key), state => ({
      ...state,
      loading: false
    })),
    on(ListSubtabsActions.searchOne(key), state => loadItemQuery<T, U>(state)),
    on(ListSubtabsActions.searchOneSuccess<T>(key), (state, { item }) => loadItem<T, U>(state, item)),
    on(ListSubtabsActions.searchOneError(key), state => ({ ...state, loadingItem: false })),
    on(ListSubtabsActions.markReadUnreadStatus(key), state => ({ ...state, loading: true })),
    on(ListSubtabsActions.markArchiveMoveToActive(key), state => ({ ...state, loading: true })),
    on(ListSubtabsActions.markReadUnreadStatusSuccess(key), (state, { data }) => setDataMarkReadUnread(state, data)),
    on(ListSubtabsActions.markArchiveMoveToActiveError(key), state => ({ ...state, loading: false })),
    on(ListSubtabsActions.updateWatcher(key), state => ({ ...state, loading: true })),
    on(ListSubtabsActions.updateWatcherError(key), state => ({ ...state, loading: false }))
  );
}

export function getLoading<T, U>(state: ListSubtabsState<T, U>) {
  return state.loading;
}

export function getData<T, U>(state: ListSubtabsState<T, U>) {
  return state.data;
}

export function getQuery<T, U>(state: ListSubtabsState<T, U>) {
  return state.query;
}

export function getSelectedItems<T, U>(state: ListSubtabsState<T, U>) {
  return state.selectedItems;
}

export function getLoadingItem<T, U>(state: ListSubtabsState<T, U>) {
  return state.loadingItem;
}

export function getActiveSelectedItem<T, U>(state: ListSubtabsState<T, U>) {
  return state.activeSelectedItem;
}

export function getSelectedRows<T, U>(state: ListSubtabsState<T, U>) {
  return state.data.filter((dataItem: any) => Boolean(dataItem.isRowSelected));
}

export function getSelectedRowsCount<T, U>(state: ListSubtabsState<T, U>) {
  return state.data.filter((dataItem: any) => Boolean(dataItem.isRowSelected)).length;
}

export function getQueryTotalItems<T, U>(state: ListSubtabsState<T, U>) {
  return state.query.paginateBy.totalElements;
}
