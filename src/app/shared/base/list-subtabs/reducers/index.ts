import { createSelector, DefaultProjectorFn, MemoizedSelector } from '@ngrx/store';

import * as fromListSubtabs from './list-subtabs.reducer';

export function getListSubtabsLoading<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getLoading);
}

export function getListSubtabsData<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getData);
}

export function getListSubtabsQuery<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getQuery);
}

export function listSubtabsHasData<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(getListSubtabsData(listSelector), data => data && data.length > 0);
}

export function getListSubtabsSelectedItems<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getSelectedItems);
}

export function getListSubtabsLoadingItem<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getLoadingItem);
}

export function getListSubtabsActiveSelectedItem<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getActiveSelectedItem);
}

export function getListSubtabsSelectedRows<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getSelectedRows);
}

export function getListSubtabsSelectedRowsCount<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getSelectedRowsCount);
}

export function getListSubtabsQueryTotalItems<T, U>(
  listSelector: MemoizedSelector<
    any,
    fromListSubtabs.ListSubtabsState<T, U>,
    DefaultProjectorFn<fromListSubtabs.ListSubtabsState<T, U>>
  >
) {
  return createSelector(listSelector, fromListSubtabs.getQueryTotalItems);
}
