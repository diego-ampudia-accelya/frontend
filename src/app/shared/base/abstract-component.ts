import { Directive, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractComponent implements OnDestroy {
  protected subscriptions: Subscription[] = [];

  constructor(public elementRef: ElementRef) {}

  public querySelectorAll(selector) {
    return this.elementRef.nativeElement.querySelectorAll(selector);
  }

  public ngOnDestroy(): void {
    this.removeSubscriptions();
  }

  public setFocus() {
    const errors = this.querySelectorAll('.form-control.error');
    const aux = errors.length > 0 ? (errors[0] as HTMLElement) : null;
    if (aux) {
      aux.focus();
    }
  }

  /**
   * Removes all current subscriptions for this component.
   */
  protected removeSubscriptions() {
    for (const subscription of this.subscriptions) {
      if (subscription && subscription.unsubscribe) {
        subscription.unsubscribe();
      }
    }

    this.subscriptions = [];
  }
}
