import { BytesFormatPipe } from './bytes-format.pipe';

describe('Pipe: BytesFormate', () => {
  it('create an instance', () => {
    const pipe = new BytesFormatPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return correct data for bytes', () => {
    const pipe = new BytesFormatPipe();
    const result = pipe.transform(123);
    expect(result).toBe('123 B');
  });

  it('should return correct data for megabytes', () => {
    const pipe = new BytesFormatPipe();
    const result = pipe.transform(5265734);
    expect(result).toEqual('5.02 Mb');
  });

  it('should return correct data for extremely large files', () => {
    const pipe = new BytesFormatPipe();
    // eslint-disable-next-line @typescript-eslint/no-loss-of-precision
    const result = pipe.transform(12345678901234567890);
    expect(result).toEqual('10.71 Eb');
  });

  it('should return empty string when the value is 0', () => {
    const pipe = new BytesFormatPipe();
    const result = pipe.transform(0);
    expect(result).toEqual('0 Bytes');
  });

  it('should return empty string when the value is negative', () => {
    const pipe = new BytesFormatPipe();
    const result = pipe.transform(-11212121);
    expect(result).toEqual('0 Bytes');
  });
});
