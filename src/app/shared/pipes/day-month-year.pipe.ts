import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment-mini';

@Pipe({
  name: 'dayMonthYear'
})
export class DayMonthYearPipe implements PipeTransform {
  transform(date: any): string {
    if (date) {
      return moment(date).format('DD/MM/YYYY');
    }

    return '';
  }
}
