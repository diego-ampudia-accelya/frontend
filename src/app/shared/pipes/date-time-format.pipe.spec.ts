import { DateTimeFormatPipe } from './date-time-format.pipe';

describe('Pipe: DateTimeFormatPipe', () => {
  it('create an instance', () => {
    const pipe = new DateTimeFormatPipe();
    expect(pipe).toBeTruthy();
  });

  it('return correct date', () => {
    const pipe = new DateTimeFormatPipe();
    const date = new Date(2019, 11, 11, 11, 11);
    const result = pipe.transform(date);
    expect(result).toBe('11/12/2019 - 11:11');
  });

  it('return correct date when there are single-digit values in the date', () => {
    const pipe = new DateTimeFormatPipe();
    const date = new Date(2019, 1, 1, 1, 1);
    const result = pipe.transform(date);
    expect(result).toBe('01/02/2019 - 01:01');
  });
});
