import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { isArray, isNil } from 'lodash';

import { PeriodOption, PeriodUtils } from '../components/period-picker';

@Pipe({
  name: 'period',
  pure: true
})
export class PeriodPipe implements PipeTransform {
  constructor(private datePipe: DatePipe) {}

  public transform(value: PeriodOption | PeriodOption[] | string | string[], monthView = false): string {
    let formattedValues: string[] = [];

    if (value) {
      const values = isArray(value) ? value : [value];
      formattedValues = values
        .filter(v => !isNil(v))
        .map(v => (typeof v === 'string' ? v : v.period))
        .filter(v => !isNil(v))
        .map(selectedPeriod => {
          const period = PeriodUtils.getPeriodValueFrom(selectedPeriod);
          const date = PeriodUtils.getDateFrom(selectedPeriod);
          const formattedDate = this.datePipe.transform(date, 'MMM yyyy');

          return monthView ? formattedDate : `P${period} ${formattedDate}`;
        });
    }

    const formattedValue = monthView ? formattedValues.pop() : formattedValues.join(' - ');

    return formattedValue || '';
  }
}
