import { JoinPipe } from './join.pipe';

describe('JoinPipePipe', () => {
  let pipe: JoinPipe;

  beforeEach(() => {
    pipe = new JoinPipe();
  });

  describe('transform', () => {
    it('should return null, if value is null', () => {
      expect(pipe.transform(null)).toBe(null);
    });

    it('should return undefined, if value is undefined', () => {
      expect(pipe.transform(undefined)).toBe(undefined);
    });

    it('should return value, if value has no join method', () => {
      expect(pipe.transform({ a: 'a' })).toEqual({ a: 'a' });
    });

    it('should call value join with default separator and return joined result', () => {
      const value = [1, 2, 3];
      spyOn(value, 'join').and.callThrough();

      const result = pipe.transform(value);

      expect(value.join).toHaveBeenCalledWith(', ');
      expect(result).toBe('1, 2, 3');
    });

    it('should call value join with provided separator and return joined result', () => {
      const value = [1, 2, 3];
      spyOn(value, 'join').and.callThrough();

      const result = pipe.transform(value, ' ');

      expect(value.join).toHaveBeenCalledWith(' ');
      expect(result).toBe('1 2 3');
    });
  });
});
