import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment-mini';

@Pipe({
  name: 'dateTimeFormat'
})
export class DateTimeFormatPipe implements PipeTransform {
  transform(value: Date, exponent?: 'time' | 'date'): string {
    switch (exponent) {
      case 'time':
        return moment(value).format('HH:mm');
      case 'date':
        if (value) {
          return moment(value).format('DD/MM/YYYY');
        }

        return '';
      default:
        return moment(value).format('DD/MM/YYYY - HH:mm');
    }
  }
}
