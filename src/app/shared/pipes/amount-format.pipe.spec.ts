import { DecimalPipe } from '@angular/common';

import { AmountFormatPipe } from './amount-format.pipe';

describe('AmountFormatPipe', () => {
  const decimalPipe: DecimalPipe = new DecimalPipe('en-US');
  let amountFormatPipe: AmountFormatPipe;

  beforeEach(() => (amountFormatPipe = new AmountFormatPipe(decimalPipe)));

  it('should format correctly according to the decimals specified', () => {
    expect(amountFormatPipe.transform('12.789', 3)).toBe('12.789');
    expect(amountFormatPipe.transform(12.789, 2)).toBe('12.79');
  });

  it('should format correctly when 0 decimals are specified', () => {
    expect(amountFormatPipe.transform('12.789', 0)).toBe('13');
  });

  it('should return original value when decimals param is null', () => {
    expect(amountFormatPipe.transform('12.789', null)).toBe('12.789');
  });

  it('should return original value when decimals param is not specified', () => {
    expect(amountFormatPipe.transform('12.789')).toBe('12.789');
  });
});
