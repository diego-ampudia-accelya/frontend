import { EmptyPipe, EMPTY_VALUE } from './empty.pipe';

describe('EmptyPipe', () => {
  let pipe: EmptyPipe;

  beforeEach(() => {
    pipe = new EmptyPipe();
  });

  it('should return value when it is a non-empty string', () => {
    expect(pipe.transform('valid')).toBe('valid');
  });

  it('should return value when it is `0`', () => {
    expect(pipe.transform(0)).toBe(0);
  });

  it('should return value when it is `5`', () => {
    expect(pipe.transform(5)).toBe(5);
  });

  it('should return value when it is `false`', () => {
    expect(pipe.transform(false)).toBe(false);
  });

  it('should return value when it is `true`', () => {
    expect(pipe.transform(true)).toBe(true);
  });

  it('should return an em dash when value is empty string', () => {
    expect(pipe.transform('')).toBe(EMPTY_VALUE);
  });

  it('should return an em dash when value is `null`', () => {
    expect(pipe.transform(null)).toBe(EMPTY_VALUE);
  });

  it('should return an em dash when value is `undefined`', () => {
    expect(pipe.transform(undefined)).toBe(EMPTY_VALUE);
  });

  it('should return an em dash when value `[]`', () => {
    expect(pipe.transform([])).toBe(EMPTY_VALUE);
  });

  it('should return value when it is an array with one item', () => {
    const value = ['item'];
    expect(pipe.transform(value)).toEqual(value);
  });
});
