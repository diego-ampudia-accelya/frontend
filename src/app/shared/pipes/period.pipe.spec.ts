import { DatePipe } from '@angular/common';

import { PeriodOption } from '../components';

import { PeriodPipe } from './period.pipe';

describe('PeriodPipe', () => {
  const datePipe = new DatePipe('en-US');
  let periodPipe: PeriodPipe = null;

  beforeEach(() => {
    periodPipe = new PeriodPipe(datePipe);
  });

  it('create an instance', () => {
    expect(periodPipe).toBeTruthy();
  });

  it('should format value from 20190701 to P1 Jul 2019', () => {
    const period: PeriodOption = {
      period: '2019071',
      dateFrom: '06/29/2019',
      dateTo: '07/07/2019'
    };
    const result = periodPipe.transform(period);

    expect(result).toEqual('P1 Jul 2019');
  });

  it('should return empty string if there is no selected option', () => {
    const result = periodPipe.transform(null);
    expect(result).toBe('');
  });

  it('should format value from [20190701, 20190702] to P1 Jul 2019 - P2 Jul 2019', () => {
    const fromPeriod: PeriodOption = {
      period: '2019071',
      dateFrom: '06/29/2019',
      dateTo: '07/07/2019'
    };
    const toPeriod: PeriodOption = {
      period: '2019072',
      dateFrom: '07/07/2019',
      dateTo: '07/14/2019'
    };
    const result = periodPipe.transform([fromPeriod, toPeriod]);

    expect(result).toEqual('P1 Jul 2019 - P2 Jul 2019');
  });

  it('should format string period values from [2019071, 2019072] to P1 Jul 2019 - P2 Jul 2019', () => {
    const result = periodPipe.transform(['20190701', '20190702']);

    expect(result).toEqual('P1 Jul 2019 - P2 Jul 2019');
  });
});
