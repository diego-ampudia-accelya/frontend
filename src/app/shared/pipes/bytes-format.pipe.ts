import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bytesFormat'
})
export class BytesFormatPipe implements PipeTransform {
  transform(value: number): string {
    if (value <= 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const decimals = 2;
    const sizes = ['B', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'];

    const i = Math.floor(Math.log(value) / Math.log(k));

    return parseFloat((value / Math.pow(k, i)).toFixed(decimals)) + ' ' + sizes[i];
  }
}
