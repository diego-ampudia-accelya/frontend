import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

import { getAmountFormat } from '~app/shared/utils/amount-format';

@Pipe({
  name: 'amountFormat'
})
export class AmountFormatPipe implements PipeTransform {
  constructor(private decimalPipe: DecimalPipe) {}

  transform(value: any, decimals?: number): string {
    return !isNaN(value) && decimals != null ? this.decimalPipe.transform(value, getAmountFormat(decimals)) : value;
  }
}
