import { Pipe, PipeTransform } from '@angular/core';

export const EMPTY_VALUE = '—';

@Pipe({
  name: 'bsplEmpty'
})
export class EmptyPipe implements PipeTransform {
  public transform(value: any): any {
    let hasValue = value != null && value !== '';
    if (Array.isArray(value)) {
      hasValue = value.length > 0;
    }

    return hasValue ? value : EMPTY_VALUE;
  }
}
