import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'absoluteDecimal' })
export class AbsoluteDecimalPipe extends DecimalPipe implements PipeTransform {
  transform(value: any, digitsInfo?: string, locale?: string): string {
    if (isNaN(value)) {
      return value;
    }

    if (value != null) {
      return super.transform(Math.abs(value), digitsInfo, locale);
    }

    return '';
  }
}
