import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'join'
})
export class JoinPipe implements PipeTransform {
  transform(value: any, separator = ', '): any {
    return value?.join ? value.join(separator) : value;
  }
}
