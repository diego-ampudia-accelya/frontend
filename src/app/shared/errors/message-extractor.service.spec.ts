import { createSpyObject, SpyObject } from '@ngneat/spectator';

import { ServerError } from '~app/shared/errors';
import { ErrorMessageTranslationService } from '~app/shared/errors/error-message-translation.service';
import { MessageExtractorService } from '~app/shared/errors/message-extractor.service';
import { ResponseErrorBE } from '~app/shared/models';

const createResponseErrorBE = (errorCode = 500): ResponseErrorBE => ({
  timestamp: null,
  errorCode,
  errorMessage: null,
  messages: []
});

describe('MessageExtractorService', () => {
  let service: MessageExtractorService;
  let errorMessageTranslationServiceSpy: SpyObject<ErrorMessageTranslationService>;

  beforeEach(() => {
    errorMessageTranslationServiceSpy = createSpyObject(ErrorMessageTranslationService);
    errorMessageTranslationServiceSpy.translate.and.returnValue(null);

    service = new MessageExtractorService(errorMessageTranslationServiceSpy);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  it('should return falsy when no error is provided', () => {
    const actual = service.extract(null);
    expect(actual).toBeFalsy();
  });

  it('should return translated message when `errorMajorCode` and message `messageCode` are defined', () => {
    const serverError = new ServerError({
      timestamp: null,
      errorCode: 500,
      errorMajorCode: '001',
      errorMessage: 'Something went wrong.',
      messages: [
        {
          message: '',
          messageCode: '301',
          messageParams: [],
          textParams: []
        }
      ]
    });
    const expectedMessage = 'translated-message';
    errorMessageTranslationServiceSpy.translate.and.returnValue(expectedMessage);

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should return concatenated by space translated messages when each message is translated', () => {
    const serverError = new ServerError({
      timestamp: null,
      errorCode: 500,
      errorMajorCode: '001',
      errorMessage: 'Something went wrong.',
      messages: [
        {
          message: '',
          messageCode: '301',
          messageParams: [],
          textParams: []
        },
        {
          message: '',
          messageCode: '302',
          messageParams: [],
          textParams: []
        }
      ]
    });

    let translatedMessageIndex = 1;
    errorMessageTranslationServiceSpy.translate.and.callFake(() => `translated-message-${translatedMessageIndex++}`);

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe('translated-message-1 translated-message-2');
  });

  it('should return concatenated by space unique translated messages', () => {
    const serverError = new ServerError({
      timestamp: null,
      errorCode: 500,
      errorMajorCode: '001',
      errorMessage: 'Something went wrong.',
      messages: [
        {
          message: '',
          messageCode: '301',
          messageParams: [],
          textParams: []
        },
        {
          message: '',
          messageCode: '302',
          messageParams: [],
          textParams: []
        }
      ]
    });

    errorMessageTranslationServiceSpy.translate.and.returnValue('translated-message');

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe('translated-message');
  });

  it('should return message, if no translation is provided', () => {
    const expectedMessage = 'Test message';
    const serverError = new ServerError({
      ...createResponseErrorBE(401),
      messages: [
        {
          message: expectedMessage,
          messageCode: '0000',
          messageParams: [
            {
              name: 'associateWith',
              value: '_request:path'
            }
          ],
          textParams: []
        }
      ]
    });

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should return parsed message by ${  }, if no translation is provided', () => {
    const expectedMessage = 'Failed to convert value id to required type Long';
    const serverError = new ServerError({
      ...createResponseErrorBE(401),
      messages: [
        {
          message: 'Failed to convert value ${value} to required type ${type}',
          messageCode: '301',
          messageParams: [
            {
              name: 'associateWith',
              value: '_request:path'
            }
          ],
          textParams: [
            {
              name: 'value',
              value: 'id'
            },
            {
              name: 'type',
              value: 'Long'
            }
          ]
        }
      ]
    });

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should return parsed message by {  }, if no translation is provided', () => {
    const expectedMessage = 'Failed to convert value id to required type Long';
    const serverError = new ServerError({
      ...createResponseErrorBE(401),
      messages: [
        {
          message: 'Failed to convert value {value} to required type {type}',
          messageCode: '301',
          messageParams: [
            {
              name: 'associateWith',
              value: '_request:path'
            }
          ],
          textParams: [
            {
              name: 'value',
              value: 'id'
            },
            {
              name: 'type',
              value: 'Long'
            }
          ]
        }
      ]
    });

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should return concatenated parsed messages , if no translation is provided', () => {
    const expectedMessage = 'First message with params value id and type Long. Second message no params.';
    const serverError = new ServerError({
      ...createResponseErrorBE(401),
      messages: [
        {
          message: 'First message with params value {value} and type {type}.',
          messageCode: '301',
          messageParams: [
            {
              name: 'associateWith',
              value: '_request:path'
            }
          ],
          textParams: [
            {
              name: 'value',
              value: 'id'
            },
            {
              name: 'type',
              value: 'Long'
            }
          ]
        },
        {
          message: 'Second message no params.',
          messageCode: '302',
          messageParams: [
            {
              name: 'associateWith',
              value: '_request:path'
            }
          ],
          textParams: []
        }
      ]
    });

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should fallback to default unauthorized message when other options are not available and error code is 401', () => {
    const expectedMessage = 'GENERIC_UNAUTHORIZED_ERROR';
    const serverError = new ServerError(createResponseErrorBE(401));

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should fallback to default forbidden message when other options are not available and error code is 403', () => {
    const expectedMessage = 'GENERIC_FORBIDDEN_ERROR';
    const serverError = new ServerError(createResponseErrorBE(403));

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should fallback to default not found message when other options are not available and error code is 404', () => {
    const expectedMessage = 'GENERIC_NOT_FOUND_ERROR';
    const serverError = new ServerError(createResponseErrorBE(404));

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should fallback to default message when other options are not available and error code is 400', () => {
    const expectedMessage = 'GENERIC_HTTP_ERROR';
    const serverError = new ServerError(createResponseErrorBE(400));

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });

  it('should fallback to default message when other options are not available', () => {
    const expectedMessage = 'GENERIC_HTTP_ERROR';
    const serverError = new ServerError(createResponseErrorBE(500));

    const actualMessage = service.extract(serverError);
    expect(actualMessage).toBe(expectedMessage);
  });
});
