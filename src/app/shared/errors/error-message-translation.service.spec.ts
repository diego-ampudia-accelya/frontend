import { createSpyObject, SpyObject } from '@ngneat/spectator';
import { L10nTranslationService } from 'angular-l10n';
import { identity } from 'lodash';

import { ErrorMessageTranslationService } from '~app/shared/errors/error-message-translation.service';
import { ResponseErrorMessageBE } from '~app/shared/models';

const messageStub = {
  message: 'test message',
  messageCode: '',
  messageParams: [],
  textParams: [{ name: 'testAttr', value: 'testValue' }]
};

const errorStub = {
  timestamp: '3432142142',
  errorCode: 500,
  errorMajorCode: '',
  errorId: '123-a',
  errorMessage: 'test message',
  messages: [messageStub]
};

const translatedErrorMajorCode = 'translated errorMajorCode';
const translatedMessageCode = 'translated messageCode';

describe('ErrorMessageTranslationService', () => {
  let service: ErrorMessageTranslationService;
  let translationSpy: SpyObject<L10nTranslationService>;

  beforeEach(() => {
    translationSpy = createSpyObject(L10nTranslationService);
    translationSpy.translate.and.callFake((key: string, _params: any) => {
      switch (key) {
        case 'errorIndex.205.301':
        case 'errorIndex.205.303':
          return translatedErrorMajorCode;
        case 'errorIndex.messageCodes.303':
          return translatedMessageCode;
        default:
          return identity(key);
      }
    });

    service = new ErrorMessageTranslationService(translationSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('translate', () => {
    // public translate(error: ResponseErrorBE, message: ResponseErrorMessageBE): string {
    it('should return null if empty objects', () => {
      expect(service.translate(null, null)).toBe(null);
    });

    it('should return null if no messageCode', () => {
      const message = {} as ResponseErrorMessageBE;

      expect(service.translate(message)).toBe(null);
    });

    it('should return translated message for majorCode', () => {
      const message = { ...messageStub, messageCode: '301' };
      const error = { ...errorStub, errorMajorCode: '205' };

      expect(service.translate(message, error)).toBe(translatedErrorMajorCode);
    });

    it('should return translated message for messageCode', () => {
      const message = { ...messageStub, messageCode: '303' };

      expect(service.translate(message, errorStub)).toBe(translatedMessageCode);
    });

    it('should return translated message for majorCode with priority over messageCode', () => {
      const message = { ...messageStub, messageCode: '303' };
      const error = { ...errorStub, errorMajorCode: '205' };

      expect(service.translate(message, error)).toBe(translatedErrorMajorCode);
    });
  });
});
