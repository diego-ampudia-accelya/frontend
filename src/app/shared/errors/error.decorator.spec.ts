import { CustomError } from './error.decorator';

@CustomError()
class TestError extends Error {}

describe('CustomError', () => {
  it('should preserve inheritance chain', () => {
    const error = new TestError();

    expect(error instanceof Error).toBeTruthy();
  });

  it('should enable using the instanceof operator', () => {
    const error = new TestError();

    expect(error instanceof TestError).toBeTruthy();
  });

  it('should execute constructor of decorated class', () => {
    const error = new TestError('Test');

    expect(error.message).toBe('Test');
  });
});
