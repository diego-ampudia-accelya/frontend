/**
 * Enables proper use of `instanceof` operator with custom errors that inherit from the built-in `Error` class.
 * Use to decorate custom error classes.
 */
// eslint-disable-next-line @typescript-eslint/naming-convention
export function CustomError(): ClassDecorator {
  return function (original: any) {
    const decoratedClass: any = function (...args: []) {
      const error = new original(...args);

      // Set the prototype of the error object explicitly.
      // This enables the use of the instanceOf operator with the decorated error class.
      // For more details, see https://github.com/Microsoft/TypeScript/wiki/FAQ#why-doesnt-extending-built-ins-like-error-array-and-map-work
      Object.setPrototypeOf(error, original.prototype);

      return error;
    };

    decoratedClass.prototype = original.prototype;

    return decoratedClass;
  };
}
