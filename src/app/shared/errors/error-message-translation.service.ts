import { Injectable } from '@angular/core';
import { L10nTranslationService } from 'angular-l10n';

import { ResponseErrorBE, ResponseErrorMessageBE } from '~app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessageTranslationService {
  constructor(private translationService: L10nTranslationService) {}

  public translate(message: ResponseErrorMessageBE, error?: ResponseErrorBE): string {
    const errorMajorCode = (error && error.errorMajorCode) || '';
    const messageCode = (message && message.messageCode) || '';
    const translationParams = this.getTranslationParams(message);
    let translatedMessage: string = null;

    translatedMessage = this.translateByMajorCode(errorMajorCode, messageCode, translationParams);

    if (translatedMessage == null) {
      translatedMessage = this.translateByMessageCode(messageCode, translationParams);
    }

    return translatedMessage;
  }

  private getTranslationParams(message: ResponseErrorMessageBE) {
    const textParams = (message && message.textParams) || [];

    return textParams.reduce((translationParams, param) => {
      translationParams[param.name] = param.value;

      return translationParams;
    }, {});
  }

  private translateByMajorCode(errorMajorCode: string | number, messageCode: string, translationParams: any): string {
    if (errorMajorCode && messageCode) {
      return this.getTranslation(`errorIndex.${errorMajorCode}.${messageCode}`, translationParams);
    }

    return null;
  }

  private translateByMessageCode(messageCode: string, translationParams: any) {
    if (messageCode) {
      return this.getTranslation(`errorIndex.messageCodes.${messageCode}`, translationParams);
    }

    return null;
  }

  private getTranslation(key: string, params: any) {
    const translation = this.translationService.translate(key, params);

    return key !== translation ? translation : null;
  }
}
