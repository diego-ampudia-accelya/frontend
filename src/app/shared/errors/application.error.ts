import { CustomError } from './error.decorator';

/**
 * Base type for all application-specific errors
 */
@CustomError()
export class ApplicationError extends Error {
  public name = 'ApplicationError';
}
