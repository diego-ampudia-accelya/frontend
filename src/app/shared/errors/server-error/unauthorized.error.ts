import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class UnauthorizedError extends ServerError {
  readonly name = 'UnauthorizedError';
}
