import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class NotFoundError extends ServerError {
  readonly name = 'NotFoundError';
}
