import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { ResponseErrorBE } from '../../models/error-response-be.model';
import { CustomError } from '../error.decorator';

/**
 * Represents an error that has occurred in response to a request to a remote server.
 * Contains details for the error in a standardized format.
 */
@CustomError()
export class ServerError extends HttpErrorResponse {
  readonly name: any = 'ServerError';
  readonly error: ResponseErrorBE;

  constructor(error: Partial<ResponseErrorBE>, headers?: HttpHeaders, url?: string) {
    super({
      headers,
      url,
      error,
      status: error.errorCode,
      statusText: error.errorMessage
    });
  }
}
