import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class ForbiddenError extends ServerError {
  readonly name = 'ForbiddenError';
}
