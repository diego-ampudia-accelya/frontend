import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class GatewayTimeoutError extends ServerError {
  readonly name = 'GatewayTimeoutError';
}
