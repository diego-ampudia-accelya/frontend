import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class NetworkError extends ServerError {
  readonly name = 'NetworkError';
}
