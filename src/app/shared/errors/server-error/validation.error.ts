import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class ValidationError extends ServerError {
  readonly name = 'ValidationError';
}
