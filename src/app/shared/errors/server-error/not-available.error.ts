import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class NotAvailableError extends ServerError {
  readonly name = 'NotAvailableError';
}
