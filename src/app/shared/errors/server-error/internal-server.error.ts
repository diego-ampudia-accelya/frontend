import { CustomError } from '../error.decorator';

import { ServerError } from './server.error';

@CustomError()
export class InternalServerError extends ServerError {
  readonly name = 'InternalServerError';
}
