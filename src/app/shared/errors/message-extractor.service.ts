import { Injectable } from '@angular/core';
import { constant, uniq } from 'lodash';
import { compact, flow, map } from 'lodash/fp';

import { ServerError } from '~app/shared/errors';
import { ErrorMessageTranslationService } from '~app/shared/errors/error-message-translation.service';
import { ResponseErrorMessageBE } from '~app/shared/models';

interface MessageExtractionStrategy {
  type: typeof ServerError;
  extractionRules: Array<(error: ServerError) => string[]>;
}

@Injectable({
  providedIn: 'root'
})
export class MessageExtractorService {
  public useDefault = constant(['GENERIC_HTTP_ERROR']);

  private extractionStrategies: Array<MessageExtractionStrategy>;

  constructor(private errorMessageTranslationService: ErrorMessageTranslationService) {
    this.extractionStrategies = [
      // Configure different strategies for different type of errors
      {
        type: ServerError,
        extractionRules: [this.useTranslatedMessages, this.useErrorMessages, this.useErrorCode, this.useDefault]
      }
    ];
  }

  /**
   * Extracts a message from the provided `ServerError`.
   * The messages are extracted based on the extractionStrategies configuration
   * 1. translated message based on messages array
   * 2. a default message
   *
   * @param serverError The `ServerError` to extract the message from.
   */
  public extract(serverError: ServerError, extractionStrategies?: MessageExtractionStrategy[]): string {
    return uniq(this.extractMany(serverError, extractionStrategies)).join(' ');
  }

  public extractMany(serverError: ServerError, extractionStrategies?: MessageExtractionStrategy[]): string[] {
    const strategy = (extractionStrategies || this.extractionStrategies).find(s => serverError instanceof s.type);
    let messages: string[] = [];

    // Each extraction rule is executed in order of definition, until we get a message.
    if (strategy) {
      for (const rule of strategy.extractionRules) {
        messages = rule(serverError) || [];
        if (messages.length > 0) {
          break;
        }
      }
    }

    return messages;
  }

  /**
   * Get message based on translated error `messages`.
   */
  public useTranslatedMessages = ({ error }) =>
    flow(
      map((message: ResponseErrorMessageBE) => this.errorMessageTranslationService.translate(message, error)),
      compact
    )((error && error.messages) || []);

  /**
   * Get message based on parsed error `messages`.
   */
  public useErrorMessages = ({ error }) => flow(map(this.parseMessage), compact)((error && error.messages) || []);

  /**
   * Get translated message based on error `errorCode`.
   */
  public useErrorCode = ({ error }) => {
    switch (error.errorCode) {
      case 401:
        return ['GENERIC_UNAUTHORIZED_ERROR'];
      case 403:
        return ['GENERIC_FORBIDDEN_ERROR'];
      case 404:
        return ['GENERIC_NOT_FOUND_ERROR'];
      case 429:
        return ['GENERIC_RATE_LIMIT_ERROR'];
      default:
        return [];
    }
  };

  private parseMessage = ({ message, textParams }) =>
    (textParams || []).reduce((accMessage: string, { name, value }) => {
      const regExp = new RegExp(`\\$?\\{${name}\\}`, 'g');

      return accMessage.replace(regExp, value);
    }, message || '') ||
    message ||
    null;
}
