import { AbstractControl, FormControl, ValidatorFn } from '@angular/forms';
import { chain, isEmpty, merge } from 'lodash';

export function multiValueValidator(validatorFn: ValidatorFn | ValidatorFn[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    const value = Array.isArray(control.value) ? control.value : [control.value];
    const validators = Array.isArray(validatorFn) ? validatorFn : [validatorFn];

    const validationErrors = chain(value)
      .map(v => {
        const val = new FormControl(v);

        return validators.map(validator => validator(val));
      })
      .flatten()
      .compact()
      .value();

    return isEmpty(validationErrors) ? null : merge({}, ...validationErrors);
  };
}
