import { AbstractControl } from '@angular/forms';

/**
 * Validates if control value is a number
 */
export function numericValidator(control: AbstractControl): { [key: string]: boolean } {
  let result = null;

  if (control.value != null && control.value !== '') {
    if (typeof control.value === 'string' && control.value.includes(',')) {
      result = {
        decimalSeparatorNotComma: {}
      };
    } else if (isNaN(control.value)) {
      result = {
        numeric: {}
      };
    }
  }

  return result;
}
