import { FormControl } from '@angular/forms';

import { alphanumericValidator } from './alphanumeric.validator';

describe('alphanumericValidator', () => {
  it('should allow whitespace by default', () => {
    const value = 'hi hi';
    const result = new FormControl(value, [alphanumericValidator()]);

    expect(result.errors).toEqual(null);
  });

  it('should allow whitespace', () => {
    const value = 'hi hi';
    const result = new FormControl(value, [alphanumericValidator(true)]);

    expect(result.errors).toEqual(null);
  });

  it('should not allow spaces', () => {
    const value = 'hi hi';
    const result = new FormControl(value, [alphanumericValidator(false)]);

    expect(result.errors).toEqual({ whitespace: { value } });
  });

  it('should not allow tabs', () => {
    const value = 'hi		hi';
    const result = new FormControl(value, [alphanumericValidator(false)]);

    expect(result.errors).toEqual({ whitespace: { value } });
  });

  it('should allow alphanumeric', () => {
    const value = 'HelloZebra1234567890';
    const result = new FormControl(value, [alphanumericValidator(false)]);

    expect(result.errors).toEqual(null);
  });

  it('should not allow non alphanumeric', () => {
    const value = 'HelloZebra!@#$%^&*()_+/*-зебра';
    const result = new FormControl(value, [alphanumericValidator(false)]);

    expect(result.errors).toEqual({ alphanumeric: { value } });
  });
});
