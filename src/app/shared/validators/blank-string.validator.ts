import { AbstractControl, Validators } from '@angular/forms';

export function blankStringValidator(control: AbstractControl): { [key: string]: boolean } | null {
  return Validators.pattern(/.*\S.*/)(control) ? { blankString: true } : null;
}
