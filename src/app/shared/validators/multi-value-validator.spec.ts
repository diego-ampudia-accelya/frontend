import { FormControl, Validators } from '@angular/forms';

import { multiValueValidator } from './multi-value.validator';

describe('Multi value validator', () => {
  it('should return null if there are no errors', () => {
    const result = new FormControl(
      ['abc', 'asd'],
      [multiValueValidator(Validators.pattern('^[A-Za-z0-9_, ?./-]{0,200}$'))]
    );

    expect(result.valid).toBe(true);
    expect(result.errors).toEqual(null);
  });

  it('should return errors if some of the values are not valid ', () => {
    const result = new FormControl('abv', [multiValueValidator(Validators.email)]);

    expect(result.valid).toBe(false);
    expect(result.errors).toEqual({ email: true });
  });

  it('should validate by multiple validators', () => {
    const result = new FormControl('abv', [
      multiValueValidator([Validators.email, Validators.pattern('^[A-Za-z0-9_, ?./-]{0,2}$')])
    ]);

    expect(result.valid).toBe(false);
    expect(result.errors).toEqual({
      email: true,
      pattern: {
        actualValue: 'abv',
        requiredPattern: '^[A-Za-z0-9_, ?./-]{0,2}$'
      }
    });
  });
});
