import { DecimalPipe } from '@angular/common';
import { AbstractControl, ValidatorFn } from '@angular/forms';

const decimalPipe = new DecimalPipe('en-EN');

/**
 * Validates if control value is lower than a certain number
 *
 * @param max the base number for comparison or the AbstractControl to extract the value from
 * @returns error object if value is equal or greater than `max`
 */
export function lowerAmountValidator(max: number | AbstractControl, currencyDecimals?: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    let result = null;
    const numberOfDecimals = currencyDecimals || 2;
    const maxValue = max instanceof AbstractControl ? parseFloat(max.value) : max;
    const formattedMaxValue = decimalPipe.transform(maxValue, `1.${numberOfDecimals}-${numberOfDecimals}`);

    if (control.value && !isNaN(maxValue) && Number(control.value) > maxValue) {
      result = {
        lowerValue: {
          maxValue: formattedMaxValue
        }
      };
    }

    return result;
  };
}
