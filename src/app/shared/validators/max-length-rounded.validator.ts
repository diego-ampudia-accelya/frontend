import { AbstractControl, ValidatorFn } from '@angular/forms';

import { NumberHelper } from '~app/shared/helpers/numberHelper';

/**
 * Validates if number length is not greater than a certain length -after- rounding it
 *
 * @param decimals the rounded decimals that the final number will contain
 * @param maxLength the maximum length the number can have once it has been rounded (includes the dot symbol)
 * @returns error object if digits and decimal separator (.) length is greater than maxLength specified
 */
export function maxLengthRoundedValidator(decimals: number, maxLength: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    let result = null;

    if (control.value) {
      const rounded = NumberHelper.roundString(control.value, decimals);
      //* A big enough number breaks roundString method so we need to check the original length
      const length = isNaN(rounded as any) ? control.value.length : rounded.length;

      if (length > maxLength) {
        result = {
          maxlength: {
            actualLength: (control.value + '').length,
            requiredLength: maxLength
          }
        };
      }
    }

    return result;
  };
}
