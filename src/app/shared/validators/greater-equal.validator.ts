import { AbstractControl, ValidatorFn } from '@angular/forms';

/**
 * Validates if control value is greater or equal than a certain number
 *
 * @param min the base number for comparison or the AbstractControl to extract the value from
 * @returns error object if value is equal or less than `min`
 */
export function greaterEqualValidator(min: number | AbstractControl): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    let result = null;
    const minValue = min instanceof AbstractControl ? parseFloat(min.value) : min;

    if (control.value && !isNaN(minValue) && Number(control.value) < minValue) {
      result = {
        greaterValue: {
          message: `The value must be greater or equal than ${minValue}` // TODO Translation
        }
      };
    }

    return result;
  };
}
