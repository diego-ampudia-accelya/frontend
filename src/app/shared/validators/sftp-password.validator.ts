/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractControl, ValidatorFn } from '@angular/forms';

import { GLOBALS } from '~app/shared/constants/globals';

export function sftpPasswordValidator(control: AbstractControl): ValidatorFn {
  let result = null;
  const value = control.value;

  if (value && (value.length < 7 || value.length > 14)) {
    result = {
      sftp_password_length: true
    };
  } else if (value && !GLOBALS.PATTERNS.SFTP_PASSWORD.test(value)) {
    result = {
      sftp_password_pattern: true
    };
  }

  return result;
}
