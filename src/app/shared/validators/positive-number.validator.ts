import { AbstractControl } from '@angular/forms';

/**
 * Validates if control value is a positive number
 *
 * @returns error object if value is less than zero or includes non-numeric characters
 */
export function positiveNumberValidator(control: AbstractControl): { [key: string]: boolean } | null {
  let result = null;

  if (control.value && (isNaN(control.value) || control.value < 0)) {
    result = { positiveNumber: true };
  }

  return result;
}
