import { AbstractControl, ValidatorFn } from '@angular/forms';

export function confirmPasswordValidator(control: AbstractControl): ValidatorFn {
  if (!control.parent) {
    return null;
  }

  const passwordValue = control.parent.get('password').value;
  const confirmPasswordValue = control.value;
  let result = null;

  if (passwordValue !== confirmPasswordValue) {
    result = { passwordsMustMatch: true };
  }

  return result;
}
