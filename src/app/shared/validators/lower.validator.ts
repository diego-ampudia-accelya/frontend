import { AbstractControl, ValidatorFn } from '@angular/forms';

/**
 * Validates if control value is lower than a certain number
 *
 * @param max the base number for comparison or the AbstractControl to extract the value from
 * @returns error object if value is equal or greater than `max`
 */
export function lowerValidator(max: number | AbstractControl): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    let result = null;
    const maxValue = max instanceof AbstractControl ? parseFloat(max.value) : max;

    if (control.value && !isNaN(maxValue) && Number(control.value) > maxValue) {
      result = {
        lowerValue: {
          message: `The value must be lower than ${maxValue}` // TODO Translation
        }
      };
    }

    return result;
  };
}
