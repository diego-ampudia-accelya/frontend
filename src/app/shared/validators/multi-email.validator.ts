import { AbstractControl, FormControl, Validators } from '@angular/forms';

export function multiEmailValidator(control: AbstractControl): { [key: string]: any } | null {
  let isInvalid = false;

  if (control.value) {
    const emails = control.value.split(';').map(e => e.trim());
    isInvalid = emails.some(email => Validators.email(new FormControl(email)));
  }

  return isInvalid ? { multiEmail: true } : null;
}
