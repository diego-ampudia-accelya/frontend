import { AbstractControl, ValidatorFn } from '@angular/forms';

import { GLOBALS } from '../constants/globals';

export function alphanumericValidator(allowWhitespace = true, minLetters = 0, minDigits = 0): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const value: string = control.value || '';
    const space = ' ';
    const tab = '	';
    const hasWhitespace = value.includes(space) || value.includes(tab);
    if (!allowWhitespace && hasWhitespace) {
      return { whitespace: { value } };
    }

    if (minLetters) {
      const actualLetters = (value.match(/[a-zA-Z]/g) || []).length;
      if (actualLetters < minLetters) {
        return { minLetters: { value, minLetters } };
      }
    }

    if (minDigits) {
      const actualDigits = (value.match(/[0-9]/g) || []).length;
      if (actualDigits < minDigits) {
        return { minDigits: { value, minDigits } };
      }
    }

    const regex = GLOBALS.PATTERNS.ALPHANUMERIC;
    const isAlphanumeric = regex.test(value);

    return isAlphanumeric ? null : { alphanumeric: { value } };
  };
}
