import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[bsplMiddleClick]'
})
export class MiddleClickDirective {
  @Output() bsplMiddleClick: EventEmitter<MouseEvent> = new EventEmitter();

  private readonly middleMouseKeyId = 1;

  @HostListener('mousedown', ['$event'])
  mouseDownHandler(event: MouseEvent): void {
    if (event.button === this.middleMouseKeyId) {
      // Prevents showing the scrolling menu when middle mouse button is clicked.
      event.preventDefault();
      event.stopPropagation();
    }
  }

  @HostListener('mouseup', ['$event'])
  mouseUpHandler(event: MouseEvent): void {
    if (event.button === this.middleMouseKeyId) {
      event.preventDefault();
      event.stopPropagation();

      this.bsplMiddleClick.emit(event);
    }
  }
}
