import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[bsplDisableControl]'
})
export class DisableControlDirective {
  @Input() set bsplDisableControl(flag: boolean) {
    this.ngControl.control[flag ? 'disable' : 'enable']();
  }

  constructor(private ngControl: NgControl) {}
}
