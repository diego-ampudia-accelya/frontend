import { Directive, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

@Directive({
  selector: '[bsplThrottleClick]'
})
export class ThrottleClickDirective implements OnInit, OnDestroy {
  @Input()
  throttleTime = 1000;

  @Output()
  throttleClick = new EventEmitter();

  private clicks = new Subject();
  private subscription: Subscription;

  @HostListener('click', ['$event'])
  clickEvent(event) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.clicks.next(event);
  }

  public ngOnInit() {
    this.subscription = this.clicks.pipe(throttleTime(this.throttleTime)).subscribe(e => this.throttleClick.emit(e));
  }

  public ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
