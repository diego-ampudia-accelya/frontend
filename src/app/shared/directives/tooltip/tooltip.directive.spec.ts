import { Component, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { TooltipDirective } from './tooltip.directive';

@Component({
  selector: 'bspl-test-host',
  template: `<div [bsplTooltip]>some text</div>`
})
class TestHostComponent {}

describe('TooltipDirective', () => {
  let tooltipDirective: TooltipDirective;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TooltipDirective, TestHostComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    fixture.detectChanges();

    tooltipDirective = fixture.debugElement.query(By.directive(TooltipDirective)).injector.get(TooltipDirective);
  });

  it('should create', () => {
    expect(tooltipDirective).toBeDefined();
  });

  describe('when show on ellipsis is enabled', () => {
    let isOverflowingSpy: jasmine.Spy;

    beforeEach(() => {
      isOverflowingSpy = spyOn<any>(tooltipDirective, 'isOverflowing');
    });

    it('should disable the tooltip when overflow is detected', () => {
      isOverflowingSpy.and.returnValue(false);
      setProperty('showOnEllipsis', true);

      expect(tooltipDirective.disabled).toBe(true);
    });

    it('should enable the tooltip when no overflow is detected', () => {
      isOverflowingSpy.and.returnValue(true);
      setProperty('showOnEllipsis', true);

      expect(tooltipDirective.disabled).toBe(false);
    });

    it('should re-calculate overflow when `showOnEllipsis` is changed', () => {
      setProperty('showOnEllipsis', true);

      expect(isOverflowingSpy).toHaveBeenCalledTimes(1);
    });

    it('should re-calculate overflow', () => {
      tooltipDirective.showOnEllipsis = true;
      tooltipDirective.handleRecheckOverflow();
      fixture.detectChanges();

      expect(isOverflowingSpy).toHaveBeenCalledTimes(1);
    });

    it('should re-calculate overflow when text is changed', () => {
      tooltipDirective.showOnEllipsis = true;
      setProperty('text', 'new text');

      expect(isOverflowingSpy).toHaveBeenCalledTimes(1);
    });

    it('should not re-calculate overflow when disabled is changed', () => {
      tooltipDirective.showOnEllipsis = true;
      setProperty('disabled', false);

      expect(isOverflowingSpy).not.toHaveBeenCalled();
    });

    function setProperty(propertyName: keyof TooltipDirective, value: any): void {
      (tooltipDirective as any)[propertyName] = value;
      tooltipDirective.ngOnChanges({ [propertyName]: new SimpleChange(null, value, false) });
      fixture.detectChanges();
    }
  });
});
