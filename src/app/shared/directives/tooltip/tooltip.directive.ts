import {
  AfterViewChecked,
  Directive,
  ElementRef,
  HostListener,
  Input,
  NgZone,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Tooltip } from 'primeng/tooltip';

@Directive({
  selector: '[bsplTooltip]',
  exportAs: 'bsplTooltip'
})
export class TooltipDirective extends Tooltip implements OnChanges, AfterViewChecked {
  @Input() escape = true; //* Escapes HTML by default. If false, html tags are allowed
  @Input() showOnEllipsis: boolean;

  @Input('bsplTooltip') set textTooltip(value: string) {
    this._text = value;

    if (this.active) {
      if (this._text) {
        if (this.container && this.container.offsetParent) {
          this.updateText();
        } else {
          this.show();
        }
      } else {
        this.hide();
      }
    }
  }

  get textTooltip(): string {
    return this._text;
  }

  private _shouldRecheckOverflow = false;

  constructor(elementRef: ElementRef, zone: NgZone) {
    super(elementRef, zone);

    this.tooltipStyleClass = 'bspl-tooltip';
    this.tooltipPosition = 'bottom';
  }

  @HostListener('mouseover')
  public handleRecheckOverflow(): void {
    this._shouldRecheckOverflow = this.showOnEllipsis;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.text || changes.showOnEllipsis) {
      this._shouldRecheckOverflow = this.showOnEllipsis;
    }
  }

  public ngAfterViewChecked(): void {
    // We wait for the DOM to finish rendering to check for overflow if necessary
    if (this._shouldRecheckOverflow) {
      this._shouldRecheckOverflow = false;
      this.disabled = !this.isOverflowing();
    }
  }

  private isOverflowing(): boolean {
    const nativeElement: HTMLElement = this.el.nativeElement;

    return nativeElement && nativeElement.offsetWidth < nativeElement.scrollWidth;
  }
}
