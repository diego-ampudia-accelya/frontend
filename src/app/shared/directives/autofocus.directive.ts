import { AfterViewInit, Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[bsplAutoFocus]'
})
export class AutoFocusDirective implements AfterViewInit {
  constructor(private element: ElementRef<HTMLElement>) {}

  public ngAfterViewInit() {
    this.element.nativeElement.focus();
  }
}
