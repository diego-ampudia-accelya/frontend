import { Directive, HostListener } from '@angular/core';
import { Router } from '@angular/router';

/* Solution extracted from https://stackoverflow.com/questions/44613069/angular4-routerlink-inside-innerhtml-turned-to-lowercase */
/* Links added through innerHtml do not use routerLink. Therefore when user clicks navigation is normal and does not go through Router */
/* With this solution, click events are being listened and navigation is triggered inside Angular */

@Directive({
  selector: '[bsplInnerRouteLink]'
})
export class InnerRouteLinkDirective {
  constructor(private router: Router) {}

  @HostListener('click', ['$event'])
  public onClick(event) {
    if (event.target.tagName === 'A') {
      this.router.navigate([event.target.getAttribute('href')]);
      event.preventDefault();
    }
  }
}
