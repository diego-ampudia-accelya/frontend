import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[bsplDragAndDropFiles]'
})
export class DragAndDropFilesDirective {
  @Output() bsplDragAndDropFiles = new EventEmitter<any>();
  @Input() isDragAndDropActive = true;

  @HostListener('dragover', ['$event']) public onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();

    if (!this.isDragAndDropActive) {
      return;
    }
  }

  @HostListener('drop', ['$event']) public onDrop(event) {
    event.preventDefault();
    event.stopPropagation();

    if (!this.isDragAndDropActive) {
      return;
    }

    const files = event.dataTransfer.files;
    if (files.length > 0) {
      this.bsplDragAndDropFiles.emit(files);
    }
  }
}
