import { AfterViewInit, Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';
import { debounce } from 'lodash';

@Directive({
  selector: '[bsplHasOverflowChanged]'
})
export class HasOverflowDirective implements AfterViewInit {
  // Use async EventEmitter to avoid ExpressionChangedAfterItHasBeenCheckedError during AfterViewInit.
  @Output('bsplHasOverflowChanged') hasOverflowChanged = new EventEmitter<boolean>(true);

  private hasOverflow: boolean;

  constructor(private element: ElementRef<HTMLElement>) {
    // Limit the frequency of handled resize events
    this.handleResize = debounce(this.handleResize.bind(this), 250);
  }

  @HostListener('window:resize') handleResize() {
    this.updateHasOverflow();
  }

  public ngAfterViewInit() {
    this.updateHasOverflow();
  }

  private updateHasOverflow() {
    const newState = this.checkForOverflow();

    if (this.hasOverflow !== newState) {
      this.hasOverflow = newState;
      this.hasOverflowChanged.emit(newState);
    }
  }

  private checkForOverflow(): boolean {
    const { nativeElement } = this.element;
    const hasOverflowingWidth = nativeElement.clientWidth < nativeElement.scrollWidth;
    const hasOverflowingHeight = nativeElement.clientHeight < nativeElement.scrollHeight;

    return hasOverflowingHeight || hasOverflowingWidth;
  }
}
