import { cloneDeep } from 'lodash';

import { IataUser, UserType } from '~app/shared/models/user.model';

const iataUser: IataUser = {
  email: 'iata-user-nfe@accelya.com',
  language: 'es',
  firstName: 'Thomas',
  lastName: 'Anderson',
  telephone: '+34888888888',
  organization: 'IATA',
  address: 'Paseo de la Castellana, 95',
  locality: 'ES',
  city: 'Madrid',
  zip: '28046',
  country: 'Spain',
  registerDate: '2019-02-02T00:00:00.000Z',
  expiryDate: '2020-02-02T00:00:00.000Z',
  active: true,
  id: 10000,
  version: 1,
  userType: UserType.IATA,
  iataCode: null,
  defaultIsoc: 'ES',
  bsps: [
    {
      id: 10026,
      isoCountryCode: 'BS',
      name: 'Bahamas',
      effectiveFrom: '2019-05-10',
      effectiveTo: null,
      version: 2
    },
    {
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    },
    {
      id: 10006,
      isoCountryCode: 'GR',
      name: 'Greece',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-31',
      version: 3
    }
  ]
};

export function createIataUser(): IataUser {
  return cloneDeep(iataUser);
}
