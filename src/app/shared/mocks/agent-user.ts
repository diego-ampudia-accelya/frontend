import { cloneDeep } from 'lodash';

import { LocationClass } from '~app/shared/enums/location-class.enum';
import { LocationType } from '~app/shared/enums/location-type.enum';
import { PaymentStatus } from '~app/shared/enums/payment-status.enum';
import { RemittanceFrequency } from '~app/shared/enums/remittance-frequency.enum';
import { AgentUser, UserType } from '~app/shared/models/user.model';

const agentUser: AgentUser = {
  email: 'agent-user-nfe@accelya.com',
  firstName: 'Thomas',
  lastName: 'Anderson',
  telephone: '+49888888888',
  organization: 'Expedia',
  address: 'Komturstr. 18',
  locality: 'DE',
  city: 'Berlin',
  zip: '12099',
  country: 'Germany',
  registerDate: '2019-02-02T00:00:00.000Z',
  expiryDate: '2020-02-02T00:00:00.000Z',
  active: true,
  id: 10000,
  version: 1,
  userType: UserType.AGENT,
  iataCode: '4910001',
  bsps: [
    {
      id: 10014,
      isoCountryCode: 'DE',
      name: 'Germany',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-08-22',
      version: 1
    }
  ],
  agent: {
    id: 10004,
    iataCode: '4910001',
    name: 'German Travel Service',
    cash: PaymentStatus.ACTIVE,
    easyPay: PaymentStatus.NON_AUTHORIZED,
    paymentCard: PaymentStatus.NON_ACTIVE,
    locationClass: LocationClass.D,
    locationType: LocationType.HE,
    remittanceFrequency: RemittanceFrequency.W,
    taxId: 'TAX_GTS_DE',
    tradeName: 'GTS German Travel Service GmbH',
    vatNumber: 'VAT_GTS_DE',
    logo: 'Logo_GTS',
    effectiveFrom: '2019-05-10',
    effectiveTo: '2100-03-03',
    active: true,
    version: 0,
    address: {
      city: 'Colone',
      country: 'Germany',
      postalCode: '51147',
      state: 'Colone',
      street: '6, Kennedy Str.'
    },
    contact: {
      telephone: ' +49220355153',
      email: 'office@gts.de',
      fax: 'null'
    },
    bsp: {
      id: 10014,
      isoCountryCode: 'DE',
      name: 'Germany',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-08-22',
      active: true,
      version: 1
    }
  }
};

export function createAgentUser(): AgentUser {
  return cloneDeep(agentUser);
}
