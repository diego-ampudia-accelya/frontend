import { cloneDeep } from 'lodash';

import { GdsUser, UserType } from '~app/shared/models/user.model';

const gdsUser: GdsUser = {
  email: 'a@b',
  firstName: null,
  lastName: 'sad',
  telephone: 'zdfs',
  organization: 'organisation',
  address: 'asd',
  locality: '',
  city: null,
  zip: 'asd',
  country: 'ada',
  registerDate: '2005-04-28',
  expiryDate: null,
  active: true,
  id: 69831000128,
  userType: UserType.GDS,
  iataCode: 'EARS',
  language: null,
  bspPermissions: [
    {
      bspId: 6983,
      permissions: [
        'rAdmBill',
        'rFop',
        'uFile',
        'rGdsCfg',
        'rProf',
        'uSubusrPerm',
        'rAirl',
        'uSubusr',
        'rEmailAlrt',
        'cSubusr',
        'rPymtCard',
        'rSubusr',
        'rAdmPend',
        'rFile',
        'uEmailAlrt',
        'dIp',
        'cFile',
        'cIp',
        'rAdm',
        'uCred',
        'rSubusrPerm',
        'rAllSubusr',
        'uProf',
        'rIp',
        'rAdmDeact',
        'rEp',
        'uIp'
      ]
    }
  ],
  permissions: [
    'rAdmBill',
    'rFop',
    'uFile',
    'rGdsCfg',
    'rProf',
    'uSubusrPerm',
    'rAirl',
    'uSubusr',
    'rEmailAlrt',
    'cSubusr',
    'rPymtCard',
    'rSubusr',
    'rAdmPend',
    'rFile',
    'uEmailAlrt',
    'dIp',
    'cFile',
    'cIp',
    'rAdm',
    'uCred',
    'rSubusrPerm',
    'rAllSubusr',
    'uProf',
    'rIp',
    'rAdmDeact',
    'rEp',
    'uIp'
  ],
  bsps: [
    {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      version: 80,
      defaultCurrencyCode: null,
      active: true
    }
  ],
  gds: { id: 69658283, name: 'EARS', gdsCode: 'EARS', version: 24 }
};

export function createGdsUser(): GdsUser {
  return cloneDeep(gdsUser);
}
