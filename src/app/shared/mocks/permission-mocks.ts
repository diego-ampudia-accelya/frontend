/* eslint-disable @typescript-eslint/naming-convention */
import { Permission } from '../models/permissions.model';

export const PERMISSIONS_GROUPED_BY_CATEGORY_TRANSLATED: Record<string, Permission[]> = {
  'User Management': [
    {
      enabled: false,
      editable: true,
      id: 1,
      name: 'Create Agent User',
      permissionCategory: {
        id: 1,
        name: 'User Management',
        version: 0
      }
    },
    {
      enabled: false,
      editable: true,
      id: 2,
      name: 'Modify Agent User',
      permissionCategory: {
        id: 1,
        name: 'User Management',
        version: 0
      }
    }
  ],
  'Group Management': [
    {
      enabled: false,
      editable: true,
      id: 3,
      name: 'Create Agent Group',
      permissionCategory: {
        id: 3,
        name: 'Group Management',
        version: 0
      }
    },
    {
      enabled: true,
      editable: true,
      id: 4,
      name: 'Modify Agent Group',
      permissionCategory: {
        id: 3,
        name: 'Group Management',
        version: 0
      }
    }
  ]
};
