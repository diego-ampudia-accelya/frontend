import { cloneDeep } from 'lodash';

import { AirlineUser, UserType } from '~app/shared/models/user.model';

const airlineUser: AirlineUser = {
  email: 'airline-user-nfe@accelya.com',
  firstName: 'Tim',
  lastName: 'Scot',
  telephone: '+133143243214321',
  organization: 'Caribbean Airlines',
  address: '128 Old Hope RoadLiguanea',
  locality: 'US',
  city: 'Kingston',
  zip: 'US',
  country: 'Jamaica',
  registerDate: '2019-02-02T00:00:00.000Z',
  expiryDate: '2020-02-02T00:00:00.000Z',
  active: true,
  id: 10024,
  version: 4,
  userType: UserType.AIRLINE,
  iataCode: '623',
  bsps: [
    {
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    }
  ],
  permissions: ['rAgn', 'rAirl'],
  bspPermissions: [
    {
      bspId: 10001,
      permissions: ['rAgn', 'rAirl']
    }
  ],
  globalAirline: {
    globalName: 'Caribbean Airlines',
    iataCode: '623',
    id: 10002,
    logo: 'CaribbeanAirlinesLogo',
    version: 0,
    airlines: [
      {
        id: 1,
        designator: 'DD',
        localName: 'CARIBBEAN AIRWAYS',
        vatNumber: 'VATN002',
        active: true,
        effectiveFrom: '2002-01-01',
        effectiveTo: null,
        address: {
          address1: 'PASEO DE LA CASTELLANA, 22',
          address2: 'MADRID',
          city: 'MADRID',
          state: 'DD',
          country: 'SPAIN',
          postalCode: '28006'
        },
        contact: {
          firstName: null,
          lastName: 'CONTACTO',
          email: 'TESTAIRLINE@TEST.COM',
          telephone: '123123123'
        },
        version: 74,
        bsp: {
          id: 1,
          isoCountryCode: 'ES',
          name: 'SPAIN',
          effectiveFrom: '2000-01-01',
          effectiveTo: null,
          version: 192,
          defaultCurrencyCode: null,
          active: true
        },
        easyPayEffectiveFrom: '2017-04-05',
        // Due to `normalizeAirlineData` method in users-profile-management service
        globalAirline: {
          globalName: 'Caribbean Airlines',
          iataCode: '623',
          id: 10002,
          logo: 'CaribbeanAirlinesLogo',
          version: 0
        }
      },
      {
        id: 2,
        designator: 'IB',
        localName: 'IBERIA',
        vatNumber: 'VATN002',
        active: true,
        effectiveFrom: '2002-01-01',
        effectiveTo: null,
        address: {
          address1: 'PASEO DE LA CASTELLANA, 22',
          address2: 'MADRID',
          city: 'MADRID',
          state: 'IB',
          country: 'SPAIN',
          postalCode: '28006'
        },
        contact: {
          firstName: null,
          lastName: 'CONTACTO',
          email: 'TESTAIRLINE@TEST.COM',
          telephone: '123123123'
        },
        version: 74,
        bsp: {
          id: 10006,
          isoCountryCode: 'GR',
          name: 'Greece',
          effectiveFrom: '2019-05-10',
          effectiveTo: '2019-05-31',
          version: 3
        },
        easyPayEffectiveFrom: '2017-04-05',
        // Due to `normalizeAirlineData` method in users-profile-management service
        globalAirline: {
          globalName: 'Caribbean Airlines',
          iataCode: '623',
          id: 10002,
          logo: 'CaribbeanAirlinesLogo',
          version: 0
        }
      }
    ]
  }
};

export function createAirlineUser(): AirlineUser {
  return cloneDeep(airlineUser);
}

export function createAirlineMultiCountryUser(): AirlineUser {
  const clonnedUser = cloneDeep(airlineUser);
  const bsps = [
    {
      id: 1,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      version: 192
    },
    {
      id: 10000,
      isoCountryCode: 'BG',
      name: 'Bulgaria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-15',
      version: 4
    },
    {
      id: 10006,
      isoCountryCode: 'GR',
      name: 'Greece',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-05-31',
      version: 3
    },
    {
      id: 10018,
      isoCountryCode: 'AU',
      name: 'Austria',
      effectiveFrom: '2019-05-10',
      effectiveTo: '2019-07-26',
      version: 2
    }
  ];

  return { ...clonnedUser, defaultIsoc: 'ES', bsps };
}
