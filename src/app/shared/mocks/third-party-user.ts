import { cloneDeep } from 'lodash';

import { ThirdPartyUser, UserType } from '~app/shared/models/user.model';

const thirdPartyUser: ThirdPartyUser = {
  email: 'mail@mail.com',
  firstName: 'John',
  lastName: 'Bungo',
  telephone: '000000',
  organization: 'accelya',
  address: 'My Address',
  locality: 'My Locality',
  city: null,
  zip: '1000',
  country: 'SPAIN',
  registerDate: '2011-04-14',
  expiryDate: null,
  active: true,
  id: 69831000453,
  userType: UserType.THIRD_PARTY,
  iataCode: 'Bungo888',
  language: null,
  bspPermissions: [
    {
      bspId: 6983,
      permissions: [
        'rAgnGrp',
        'uFile',
        'rFile',
        'uEmailAlrt',
        'dIp',
        'cFile',
        'uSubusrPerm',
        'cIp',
        'uSftpAcc',
        'rSftpAcc',
        'r3PtyCfg',
        'rSubusrPerm',
        'rEmailAlrt',
        'cEvFile',
        'cAcdmFile',
        'rIp',
        'r3PtyAcdmPerm',
        'uIp'
      ]
    }
  ],
  permissions: [
    'rAgnGrp',
    'uFile',
    'rFile',
    'uEmailAlrt',
    'dIp',
    'cFile',
    'uSubusrPerm',
    'cIp',
    'uSftpAcc',
    'rSftpAcc',
    'r3PtyCfg',
    'rSubusrPerm',
    'rEmailAlrt',
    'cEvFile',
    'cAcdmFile',
    'rIp',
    'r3PtyAcdmPerm',
    'uIp'
  ],
  bsps: [
    {
      id: 6983,
      isoCountryCode: 'ES',
      name: 'SPAIN',
      effectiveFrom: '2000-01-01',
      effectiveTo: null,
      defaultCurrencyCode: null,
      active: true
    }
  ],
  thirdParty: {
    id: 1234,
    code: 'QWE',
    name: 'TP Name',
    active: true,
    organization: 'TP Org'
  }
};

export function createThirdPartyUser(): ThirdPartyUser {
  return cloneDeep(thirdPartyUser);
}
