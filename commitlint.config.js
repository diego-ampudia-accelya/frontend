const JIRA_PROJECT_KEY = 'FCA';

const isVersionBump = message => message.includes('Version has been set to');

module.exports = {
  plugins: ['commitlint-plugin-jira-rules'],
  extends: ['jira'],
  rules: {
    'jira-task-id-project-key': [2, 'always', [JIRA_PROJECT_KEY]]
  },
  ignores: [isVersionBump]
};
