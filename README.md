# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4500/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Known Issues

An empty 'reports' directory is included in the root project directory in order to allow 'TSLint' reports to be generated there. As of TSLint current version 5.12.1, the command fails if the output path for the report does not exist in advance [see issue](https://github.com/palantir/tslint/issues/4268), though a fix to this is [expected to be released with version 5.13](https://github.com/palantir/tslint/pull/4507), so this won't be necessary anymore.

## Commit Message Convention

In order to make it easier to navigate the commit history commit messages should always include the JIRA issue key ,that made the change necessary, plus some meaningful description of what was done.

Commit messages should conform to the following format.

```
jira-issue-key: meaningful-commit-summary
```

The format is verified using a git hook when a new commit is created.

### Valid commit message examples

```
FCA-1234: my commit message
```

### Some exceptions

Certain commit messages are ignored to keep things reasonable

- Merge commits - `Merge branch 'xyz' into 'dev'`

- Version bumps - `Version has been set to 0.20.0`

### Invalid commit message examples

- Commit summary only - `Some invalid commit message`, `: no JIRA issue key`

- Missing separator between the JIRA issue key and the commit summary - `FCA-1234 no separator`

- Wrong JIRA project - `AAA-1234: wrong project`

- Incomplete JIRA issue key - `FCA-: no JIRA issue id`

- JIRA issue key only - `FCA-1234:`
