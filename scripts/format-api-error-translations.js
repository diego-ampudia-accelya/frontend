const { chain } = require('lodash');
const { argv } = require('yargs');
const fs = require('fs');

const readMessages = fileName => {
  const file = fs.readFileSync(fileName);

  return JSON.parse(file);
};

const replaceMessageParams = message => message.replace('{', '{{').replace('}', '}}');

const convertToTranslations = messages => {
  return chain(messages)
    .keyBy(m => m.messageCode)
    .mapValues(m => replaceMessageParams(m.message))
    .value();
};

const getDuplicatedMessages = messages => {
  return chain(messages)
    .groupBy(m => m.messageCode)
    .pickBy(g => g.length > 1)
    .value();
};

const stringify = obj => {
  return JSON.stringify(obj, null, '  ');
};

const transform = () => {
  const fileName = argv.file;
  const messages = readMessages(fileName);
  const translations = convertToTranslations(messages);
  const duplications = getDuplicatedMessages(messages);
  if (Object.keys(duplications).length > 0) {
    console.warn('Duplicated message codes detected\n', stringify(duplications));
  }

  console.log(stringify(translations));
};

transform();
