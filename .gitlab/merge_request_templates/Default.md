### Definition of Done

- [ ] Related sub task is properly logged (screen shot and description)

- [ ] Functionality is completed

- [ ] Unit tests are written

- [ ] Lint passes with no errors

- [ ] Tests pass with no errors

- [ ] AOT build passes with no errors
