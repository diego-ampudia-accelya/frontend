# Openshift Installation guide

The service image is based on [Bitnami NGINX image](https://hub.docker.com/r/bitnami/nginx/).

## Prerequisite (Initial setup)

1.  Image stream should exist. An example:

    ```yml
    kind: ImageStream
    apiVersion: v1
    metadata:
      labels:
        app: frontend
      name: frontend
    spec:
      lookupPolicy:
        local: false
    ```

2.  Deployment config should exist ([an example](http://gitlab.nfedev.accelya.com/nfe/devops/blob/master/resources/ansible/playbooks/nfe/dmz/roles/frontend/templates/deployment-config.yml)).
3.  Service should be created. Port 8080 is expected to be exposed. An example

    ```yml
    apiVersion: v1
    kind: Service
    metadata:
      name: frontend
    spec:
      ports:
        - name: 8080-http
          port: 8080
          protocol: TCP
          targetPort: 8080
      selector:
        app: frontend
      sessionAffinity: None
      type: ClusterIP
    ```

4.  Https (in case TLS termination is done on Openshift side)
    [router](https://docs.openshift.com/container-platform/3.11/install_config/router/index.html)
    pointed to 8080 port should be configured.
5.  Next [configMaps](https://docs.openshift.com/container-platform/3.11/dev_guide/configmaps.html)
    should exist and be mounted as a volume into the frontend container: - **frontend-config**. Might be empty. Will be updated during the service
    configuration application stage. Should be mounted to _/usr/share/nginx/app-config_ - **frontend-nginx-config**. It's nginx config. Should be mounted to _/opt/bitnami/nginx/conf/vhosts_.
    Ex.
    ```
    server {
    listen 0.0.0.0:8080;
    server_name localhost;

                charset UTF-8;

                add_header Content-Security-Policy "script-src 'self'";
                add_header Strict-Transport-Security "max-age=63072000; includeSubDomains";
                add_header X-Content-Type-Options "nosniff";
                add_header Cache-Control "public, must-revalidate";

                location /configuration.json {
                    root /usr/share/nginx/app-config;
                }

                location / {
                    root   /usr/share/nginx/html;
                    index  index.html index.htm;

                    try_files $uri $uri/ /index.html;
                }
            }
            ```

## Installation process

1. [Import an image](https://docs.openshift.com/container-platform/3.11/dev_guide/managing_images.html#managing-images-mirror-registry-images) to image stream according release specification;
2. [Apply](http://gitlab.nfedev.accelya.com/nfe/configuration/blob/develop/docs/Service_Configuration.md) the service configuration.
3. Create/update deployment config with the imported image version and listed Environment variables with the proper values;
4. Run deployment config rollout and wait for the readiness state.
